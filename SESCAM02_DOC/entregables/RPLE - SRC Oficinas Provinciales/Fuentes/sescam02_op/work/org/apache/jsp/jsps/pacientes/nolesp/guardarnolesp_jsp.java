package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;
import isf.util.Utilidades;
import isf.exceptions.ExceptionSESCAM;

public class guardarnolesp_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap beanParamDocNolesp = null;
      synchronized (session) {
        beanParamDocNolesp = (java.util.HashMap) pageContext.getAttribute("beanParamDocNolesp", PageContext.SESSION_SCOPE);
        if (beanParamDocNolesp == null){
          try {
            beanParamDocNolesp = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamDocNolesp", beanParamDocNolesp, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");

     NoRPLE norple = new NoRPLE();
     Nolesp nl = new Nolesp();
     Localidades local = new Localidades();
     boolean imprimir = true;

     //Recojida de parametros

     if ((request.getParameter("nombre")!=null)&&(!request.getParameter("nombre").equals(""))){
         nl.setNombre(request.getParameter("nombre").toUpperCase());
         beanParamDocNolesp.put("NOMBRE",request.getParameter("nombre"));
     }

     if ((request.getParameter("apellido1")!=null)&&(!request.getParameter("apellido1").equals(""))){
         nl.setApellido1(request.getParameter("apellido1").toUpperCase());
         beanParamDocNolesp.put("APELLIDO1",request.getParameter("apellido1"));
     }

     if ((request.getParameter("apellido2")!=null)&&(!request.getParameter("apellido2").equals(""))){
         nl.setApellido2(request.getParameter("apellido2").toUpperCase());
         beanParamDocNolesp.put("APELLIDO2",request.getParameter("apellido2"));
     }

     if ((request.getParameter("direccion")!=null)&&(!request.getParameter("direccion").equals(""))){
         nl.setDomicilio(request.getParameter("direccion"));
         beanParamDocNolesp.put("DIRECCION",request.getParameter("direccion"));
     }

     long poblacion=0;
     String[] localprov=null;
     String localidad="";
     String provincia="";
     if ((request.getParameter("poblacion")!=null)&&(!request.getParameter("poblacion").equals(""))){
         poblacion=new Long(request.getParameter("poblacion")).longValue();
         localprov=local.DameProvinciaLocalidad(poblacion);
         if ((localprov[0]!=null)&&(!localprov[0].equals(""))){
            nl.setPoblacion(localprov[0]);
            beanParamDocNolesp.put("POBLACION",localprov[0]);
         }
         if ((localprov[1]!=null)&&(!localprov[1].equals(""))){
            nl.setProvincia(localprov[1]);
            beanParamDocNolesp.put("PROVINCIA",localprov[1]);
         }
     }


     if ((request.getParameter("fechanac")!=null)&&(!request.getParameter("fechanac").equals(""))){
         nl.setFechaNac(Utilidades.stringToDate(request.getParameter("fechanac")));
         beanParamDocNolesp.put("FECHANAC",request.getParameter("fechanac"));
     }

     if ((request.getParameter("telefono")!=null)&&(!request.getParameter("telefono").equals(""))){
         nl.setTelefono(request.getParameter("telefono"));
         beanParamDocNolesp.put("TELEFONO",request.getParameter("telefono"));
     }

     if ((request.getParameter("nss")!=null)&&(!request.getParameter("nss").equals(""))){
         nl.setNss(request.getParameter("nss"));
         beanParamDocNolesp.put("NSS",request.getParameter("nss"));
     }

     if ((request.getParameter("cip")!=null)&&(!request.getParameter("cip").equals(""))){
         nl.setCip(request.getParameter("cip"));
         beanParamDocNolesp.put("CIP",request.getParameter("cip"));
     }


     if (((request.getParameter("centro")!=null))&&(!request.getParameter("centro").equals(""))){
         nl.setCentro(new Double(request.getParameter("centro")).doubleValue());
         beanParamDocNolesp.put("CENTRO",new Double(request.getParameter("centro")));
     }

     if ((request.getParameter("nhc")!=null)&&(!request.getParameter("nhc").equals(""))){
         nl.setNhc(new Double(request.getParameter("nhc")).doubleValue());
         beanParamDocNolesp.put("NHC",new Double(request.getParameter("nhc")));
     }

     if ((request.getParameter("prestacion")!=null)&&(!request.getParameter("prestacion").equals(""))){
         nl.setPrestacion(request.getParameter("prestacion"));
         beanParamDocNolesp.put("PRESTACION",request.getParameter("prestacion"));
     }

     if ((request.getParameter("nomape")!=null)&&(!request.getParameter("nomape").equals(""))){
         nl.setGarReplegApenom(request.getParameter("nomape"));
         beanParamDocNolesp.put("RESP_NOMAPE",request.getParameter("nomape"));
     }

     String nif="";
     if ((request.getParameter("dni")!=null)&&(!request.getParameter("dni").equals(""))){
         nif = request.getParameter("dni")+request.getParameter("letradni");
         nl.setGarReplegDni(nif);
         beanParamDocNolesp.put("NIF",nif);
     }


     if (request.getParameter("type").equals("1")){ // motivo del select
	     if ((request.getParameter("motivo")!=null)&&(!request.getParameter("motivo").equals(""))){
	         nl.setGarMotRechazo(request.getParameter("motivo"));
	         beanParamDocNolesp.put("MOTIVO",request.getParameter("motivo"));
	         //System.out.println("Motivo:"+request.getParameter("motivo"));
	     }
	 }

     if (request.getParameter("type").equals("0")){ // motivo libre
  	     if ((request.getParameter("motivo_libre")!=null)&&(!request.getParameter("motivo_libre").equals(""))){
	         nl.setGarMotRechazo(request.getParameter("motivo_libre"));
	         beanParamDocNolesp.put("MOTIVO",request.getParameter("motivo_libre"));
	         //System.out.println("Motivo libre:"+request.getParameter("motivo_libre"));
	     }
	 }

     String usuario = accesoBeanId.getUsuario();
     nl.setSysUsu(usuario);
     nl.setGarOficina(new Double(OficinaHelper.DameOficinaPk(usuario)).doubleValue());

     //Se realiza la insercion:
     try{
         norple.SetNegEvento(accesoBeanId.getNegEventoInstance(17));
         /*inserto el objeto*/
         norple.insertarNoLesp(nl);
     }catch(ExceptionSESCAM eses){
         imprimir = false;
         System.out.println("Excepcion Sescam capturada en guardarNolesp.jsp");

      out.write("\r\n        ");
      out.write("<script language=\"javascript\">\r\n            alert(\"");
      out.print(eses);
      out.write("\");\r\n        ");
      out.write("</script>\r\n");

     }
     double garpk=nl.getNnolesp();

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\">\r\n    var docaprobacion=\"DOC01\";\r\n    var docdenegacion=\"DOC03\";\r\n    function ImprimirDocAprob(){\r\n        llamado='imprime_doc_nolesp.jsp?documento='+docaprobacion+'&garpk=");
      out.print(garpk);
      out.write("&usuario=");
      out.print(accesoBeanId.getUsuario());
      out.write("&cip=");
      out.print(request.getParameter("cip"));
      out.write("';\r\n        window.showModalDialog(llamado,\"\",\"dialogWidth:80em;dialogHeight:50em\");\r\n    }\r\n\r\n    function ImprimirDocDeneg(){\r\n        llamado='imprime_doc_nolesp.jsp?documento='+docdenegacion+'&garpk=");
      out.print(garpk);
      out.write("&usuario=");
      out.print(accesoBeanId.getUsuario());
      out.write("&cip=");
      out.print(request.getParameter("cip"));
      out.write("';\r\n        window.showModalDialog(llamado,\"\",\"dialogWidth:80em;dialogHeight:50em\");\r\n    }\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"down\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n");

   /* Si la insercion ha sido correcta imprimir documento */
   if (imprimir){

      out.write("\r\n                ");
      out.write("<script language=\"javascript\">\r\n                        ImprimirDocAprob();\r\n                        ImprimirDocDeneg();\r\n                        parent.main.location.href = \"ficha_paciente_nolesp_main.jsp\";\r\n                        parent.down.location.href = \"ficha_paciente_nolesp_down.htm\";\r\n                ");
      out.write("</script>\r\n");

     }

      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
