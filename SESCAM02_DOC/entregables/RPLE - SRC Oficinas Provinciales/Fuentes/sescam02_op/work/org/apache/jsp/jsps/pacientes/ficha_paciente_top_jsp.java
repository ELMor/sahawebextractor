package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;

public class ficha_paciente_top_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\">\r\nvar bMuestraHist=0;\r\nfunction muestraHistorico(){\r\n// Valores posibles:\r\n// 0:valores actuales\r\n// 1:valores historicos y actuales\r\n   var strLlamada=\"\";\r\n// Las siguientes lineas controlan si existen frames cambiando\r\n   if (((parent.frames(\"main\").frames(\"datos\").document.readyState)!=\"complete\")||\r\n       (parent.frames(\"main\").frames(\"datos_tabs\").frames(\"detalle_dat\").document.readyState!=\"complete\")||\r\n       (parent.frames(\"main\").frames(\"datos_tabs\").frames(\"solicitud_index\").frames(\"centro\").document.readyState!=\"complete\")||\r\n       (parent.frames(\"main\").frames(\"datos_tabs\").frames(\"solicitud_index\").frames(\"botonera\").document.readyState!=\"complete\"))\r\n   {\r\n     alert(\"Todavía no se han cargado todas las fichas.\");\r\n     return;\r\n   }\r\n   strLlamada=\"ficha_paciente_main_datos.jsp?cip=\"\r\n   strLlamada+=escape('");
      out.print(request.getParameter("cip"));
      out.write("')\r\n   strLlamada+='&centro=");
      out.print(request.getParameter("centro"));
      out.write("';\r\n   strLlamada+='&norden=");
      out.print(request.getParameter("norden"));
      out.write("';\r\n   strLlamada+='&gar_cq=");
      out.print(request.getParameter("gar_cq"));
      out.write("';\r\n  if (bMuestraHist==0){\r\n    bMuestraHist=1;\r\n    document.all.img_historico.src=\"../../imagenes/fichero_hist.gif\"\r\n    document.all.img_historico.alt=\"Activos e historicos\"\r\n    strLlamada+='&historico=1';\r\n    parent.frames(\"main\").frames(\"datos\").location=strLlamada;\r\n  }\r\n  else{\r\n    bMuestraHist=0;\r\n    document.all.img_historico.src=\"../../imagenes/historia.gif\"\r\n    document.all.img_historico.alt=\"Solo activos\"\r\n    strLlamada+='&historico=0';\r\n    parent.frames(\"main\").frames(\"datos\").location=strLlamada;\r\n    }\r\n}\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"top\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n");
      out.write("<table  class=\"textoTop\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" height=\"100%\">\r\n  ");
      out.write("<tr>\r\n    ");
      out.write("<td align=\"left\" height=\"100%\">\r\n  \t");
      out.write("<img  src=\"../../imagenes/flechavolver.gif\" width=\"14px\" height=\"15px\" border=\"0\" style=\"cursor:hand\" alt=\"Volver\" onclick=\"history.back();\">\r\n   ");
      out.write("</td>\r\n    ");
      out.write("<td align=\"left\" height=\"100%\" >\r\n  \t");
      out.write("<img id=\"img_historico\" name=\"img_historico\" src=\"../../imagenes/historia.gif\" border=\"0\" style=\"cursor:hand\" alt=\"Solo activos\" onclick=\"muestraHistorico();\">\r\n   ");
      out.write("</td>\r\n    ");
      out.write("<td align=\"right\" height=\"100%\">>> Consulta de pacientes >> Ficha Paciente");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
