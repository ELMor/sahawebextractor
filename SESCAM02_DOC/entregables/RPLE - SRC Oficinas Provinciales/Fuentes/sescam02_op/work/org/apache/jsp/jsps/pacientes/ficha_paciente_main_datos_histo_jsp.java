package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.ContinuosDet;
import java.util.Vector;
import java.util.Enumeration;
import beans.ConsultaContinuos;

public class ficha_paciente_main_datos_histo_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../css/estilo.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<body class=\"mainFondoInterno\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\">\r\n  ");
      out.write("<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n   ");
      out.write("<tr>\r\n    ");
      out.write("<td class=\"areaCentralInterno\" >\r\n\t");
      out.write("<table id=\"entradasDet\" class=\"centroTabla\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n  ");

  ConsultaContinuos CC=new ConsultaContinuos();
  ContinuosDet CDet;
  boolean bFilaPar=true;
  int idCounter=0;
  Vector vp;
  if (request.getParameter("centro")!=null && (!request.getParameter("centro").equals(""))){ //si no hay parametros no hace nada.
    CC.setCentro(request.getParameter("centro"));
    CC.setNorden(request.getParameter("norden"));
    CC.setCq(request.getParameter("gar_cq"));
    //CC.SetNegEvento(accesoBeanId.getNegEventoInstance(21));
    vp=CC.resultado();
    if ((vp!=null)){
      Enumeration e = vp.elements();
      while (e.hasMoreElements()) {
	CDet=(ContinuosDet)e.nextElement();
	if (bFilaPar){
          bFilaPar=false;
 
      out.write("\r\n         ");
      out.write("<tr class=\"filaPar\" id=");
      out.print(idCounter);
      out.write(" >\r\n ");
	 }else{
         bFilaPar=true;
 
      out.write("\r\n         ");
      out.write("<tr class=\"filaImpar\" id=");
      out.print(idCounter);
      out.write(" >\r\n ");

	 }
 
      out.write("\r\n           ");
      out.write("<td width=\"13%\">");
      out.print(CDet.getClave());
      out.write("</td>\r\n           ");
      out.write("<td width=\"20%\">");
      out.print(CDet.getFecha_inicio());
      out.write("</td>\r\n           ");
      out.write("<td width=\"20%\">");
      out.print(CDet.getFecha_fin());
      out.write("</td>\r\n           ");
      out.write("<td width=\"10%\">");
      out.print(CDet.getSituacion());
      out.write("</td>\r\n           ");
      out.write("<td width=\"17%\">");
      out.print(CDet.getDias_acumulados());
      out.write("</td>\r\n         ");
      out.write("</tr>\r\n");

         }
	 idCounter++;
    }else{

      out.write("\r\n      ");
      out.write("<script language=\"javascript\">\r\n       alert('Error al realizar la consulta.No se puede acceder a la informacion.');\r\n      ");
      out.write("</script>\r\n");

    }
  }

      out.write("\r\n\t");
      out.write("</table>\r\n    ");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n   ");
      out.write("</table>\r\n   ");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("<!-- DETALLE FIN-->\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
