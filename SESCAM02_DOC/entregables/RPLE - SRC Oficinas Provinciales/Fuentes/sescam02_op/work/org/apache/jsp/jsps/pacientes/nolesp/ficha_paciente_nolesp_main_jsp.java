package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;
import isf.util.*;

public class ficha_paciente_nolesp_main_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../../css/estilo.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/validarNif.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/validarFecha.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/AnchorPosition.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/botones.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/CalendarPopup.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/PopupWindow.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../../javascript/validacion_campos_fecha.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\">\r\n function chequeoFecha (){\r\n    if ((document.formulario.fechanac.value!=null)&&(document.formulario.fechanac.value!=\"\")){\r\n      if (!esStringFecha(document.formulario.fechanac.value)){\r\n         alert('Formato de fecha incorrecto (dd/mm/aaaa)');\r\n         document.formulario.fechanac.focus();\r\n      }else{\r\n         var dia = getDia(document.formulario.fechanac.value);\r\n         var mes = getMes(document.formulario.fechanac.value);\r\n         var anno = getAnno(document.formulario.fechanac.value);\r\n         if (validacion_fecha(dia,mes,anno)) document.formulario.telefono.focus();\r\n         else alert (\"Fecha no valida\");\r\n      }\r\n    }\r\n  }\r\n  \r\n function Deshabitilar_textarea(){\r\n     document.formulario.motivo.disabled=false;\r\n     document.formulario.motivo_libre.disabled=true; \r\n }\r\n \r\n function Deshabitilar_select(){\r\n     document.formulario.motivo_libre.disabled=false;\r\n     document.formulario.motivo.disabled=true; \r\n }\r\n \r\n");
      out.write("</script>\r\n");

    Calendar Cal_fechaActual = Calendar.getInstance();
    String motivo1="No se encuentra en Registro de Pacientes de LE.";
    String motivo1_valor="No se encuentra dado de alta en el Registro de Pacientes en Lista de Espera.";
    String motivo2="La prestación es de control o sucesiva y por lo tanto no garantizada.";
    String motivo2_valor="La prestación por la que se solicita el documento acreditativo, es de control o sucesiva y por lo tanto no garantizada.";

      out.write("\r\n\r\n");
      out.write("<body class=\"mainFondo\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" onload=\"\">\r\n");
      out.write("<form name=\"formulario\" action=\"guardarnolesp.jsp\" target=\"oculto\">\r\n  ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n   ");
      out.write("<tr>\r\n    ");
      out.write("<td>\r\n     ");
      out.write("<FIELDSET >\r\n     ");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>DATOS PERSONALES");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n      ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n       ");
      out.write("<tr>\r\n        ");
      out.write("<td width=\"80%\">\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">*Nombre:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"22%\">");
      out.write("<input type=\"text\" name=\"nombre\" class=\"cajatexto2\" value=\"\" style=\"width:90%\">");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"10%\">*Apel. 1:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"22%\">");
      out.write("<input type=\"text\" name=\"apellido1\" class=\"cajatexto3\" value=\"\" style=\"width:95%\">");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"10%\">*Apel. 2:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"24%\">");
      out.write("<input type=\"text\" name=\"apellido2\" class=\"cajatexto3\" value=\"\" style=\"width:95%\">");
      out.write("</td>\r\n          ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">Direccion:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"40%\">");
      out.write("<input type=\"text\" name=\"direccion\" class=\"cajatexto3\" value=\"\" style=\"width:90%\">");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"15%\">*Poblacion:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"33%\">");
      out.write("<div id=\"divLocalidades\">\r\n             ");
      out.write("<Select name=\"poblacion\" class=\"cajatexto3\" value=\"\" style=\"width:95%\">\r\n             ");

             Localidades loc = new Localidades();
             Vector vl = new Vector();
             /*if (pk_prov_sel>0) {
                vl = loc.busquedaLoc("PROVINCIA_PK="+pk_prov_sel,"LOCALIDAD ASC");
             }else{*/
                vl = loc.busquedaLoc("","LOCALIDAD ASC");
             //}
             for(int int_pos = 0;int_pos < vl.size(); int_pos++){
                SesLocalidades sesLoc=(SesLocalidades)vl.elementAt(int_pos);
             
      out.write("\r\n                   ");
      out.write("<option default value='");
      out.print( sesLoc.getPkLocalidad());
      out.write("'>");
      out.print(sesLoc.getLocalidad() );
      out.write("</option>\r\n             ");

             }
             
      out.write("\r\n             ");
      out.write("</Select>");
      out.write("</div>\r\n           ");
      out.write("</td>\r\n          ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">Fecha Nac.:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"40%\">\r\n                  ");
      out.write("<INPUT type=\"text\" class=\"cajaTextoFecha\" name=\"fechanac\" value =\"\" onKeypress=\"validarEntradaFecha(this.value);\" onBlur=\"chequeoFecha();\">");
      out.write("<font class=\"text\">(dd/mm/aaaa)");
      out.write("</font>\r\n           ");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"15%\">Telefono:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"33%\">");
      out.write("<input type=\"text\" name=\"telefono\" class=\"cajatexto2\" value=\"\" style=\"width:50%\">");
      out.write("</td>\r\n          ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">*C.I.P.:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"32%\">");
      out.write("<input type=\"text\" name=\"cip\" class=\"cajatexto2\" value=\"\">");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">N.S.S.:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"44%\">");
      out.write("<input type=\"text\" name=\"nss\" class=\"cajatexto2\" value=\"\">");
      out.write("</td>\r\n          ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n         ");
      out.write("<br>\r\n        ");
      out.write("</td>\r\n       ");
      out.write("</tr>\r\n      ");
      out.write("</table>\r\n     ");
      out.write("</FIELDSET>\r\n     ");
      out.write("<br>\r\n     ");
      out.write("<FIELDSET >\r\n     ");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>DATOS CLINICOS");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n      ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n       ");
      out.write("<tr>\r\n        ");
      out.write("<td width=\"80%\">\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">*Centro:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"60%\">");
      out.write("<div id=\"centros\">\r\n            ");
      out.write("<Select name=\"centro\" class=\"cajatexto2\" value=\"\" style=\"width:90%\">\r\n            ");

              Centros c = new Centros();
              Vector v = new Vector();
              v = c.busquedaCentro("","DESCENTRO ASC");
              for (int i=0;i<v.size();i++){
                Centrosescam cs = (Centrosescam)v.elementAt(i);
            
      out.write("\r\n             ");
      out.write("<option default value='");
      out.print(cs.getCodcentro());
      out.write("'>");
      out.print(cs.getDescentrolarga());
      out.write("</option>\r\n            ");

              }
            
      out.write("\r\n            ");
      out.write("</div>");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"13%\">Nº Historia:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"15%\">");
      out.write("<input type=\"text\" name=\"nhc\" class=\"cajatexto3\" value=\"\" style=\"width:95%\" onkeypress = \"if (((event.keyCode ");
      out.write("< 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;\">");
      out.write("</td>\r\n          ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">Prestacion:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"88%\">");
      out.write("<input type=\"text\" name=\"prestacion\" class=\"cajatexto2\" value=\"\" style=\"width:80%\">");
      out.write("</td>\r\n          ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td width=\"12%\" class= \"texto\">Motivo Deneg:");
      out.write("</td>\r\n           ");
      out.write("<td class=\"texto\" width=\"20%\">\r\n\t           ");
      out.write("<label for=\"seleccion\" title=\"Elija motivo de denegacion\">\r\n\t            ");
      out.write("<input type=\"radio\" checked name=\"type\" value=\"1\" id=\"sel\" onclick=\"Deshabitilar_textarea();\" />Seleccion\r\n\t           ");
      out.write("</label>\r\n\t       ");
      out.write("</td>\r\n           ");
      out.write("<td class=\"texto\" width=\"68%\">");
      out.write("<div id=\"motivos\">\r\n\t           ");
      out.write("<select name=\"motivo\" class=\"cajatexto2\" value=\"\" style=\"width:100%\" >\r\n\t           \t");
      out.write("<option default value='");
      out.print(motivo1_valor);
      out.write("'>");
      out.print(motivo1);
      out.write("</option>\r\n\t           \t");
      out.write("<option value='");
      out.print(motivo2_valor);
      out.write("'>");
      out.print(motivo2);
      out.write("</option>\t           \t\r\n\t           ");
      out.write("</div>");
      out.write("</td>\t\r\n\t      ");
      out.write("</tr>\r\n\t      ");
      out.write("<tr>\r\n\t\t   ");
      out.write("<td width=\"12%\" class=\"texto\">&nbsp;");
      out.write("</td>\t     \r\n   \t       ");
      out.write("<td class=\"texto\" width=\"20%\">\r\n    \t       ");
      out.write("<label for=\"libre\" title=\"Introduzca motivo denegacion\">\r\n\t            ");
      out.write("<input type=\"radio\" name=\"type\" value=\"0\" id=\"lib\" onclick=\"Deshabitilar_select();\" />Texto Libre\r\n\t           ");
      out.write("</label>\r\n\t       ");
      out.write("</td>\r\n   \t       ");
      out.write("<td class=\"texto\" width=\"68%\">");
      out.write("<textarea name=\"motivo_libre\" disabled class=\"textarea\" rows=\"2\" maxlength = 4000 value=\"\">");
      out.write("</textarea>");
      out.write("</td>\r\n\t      ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n         ");
      out.write("<br>\r\n        ");
      out.write("</td>\r\n       ");
      out.write("</tr>\r\n      ");
      out.write("</table>\r\n     ");
      out.write("</FIELDSET>\r\n     ");
      out.write("<br>\r\n     ");
      out.write("<FIELDSET >\r\n     ");
      out.write("<LEGEND>");
      out.write("<a class=\"texto\">");
      out.write("<b>DATOS RESPONSABLE");
      out.write("</b>");
      out.write("</a>");
      out.write("</LEGEND>\r\n      ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n       ");
      out.write("<tr>\r\n        ");
      out.write("<td width=\"80%\">\r\n         ");
      out.write("<table width=\"97%\" border=\"0\" cellspacing=\"0\" cellpadding=\"1\" vspace=\"0\" hspace=\"0\" align=\"center\">\r\n          ");
      out.write("<tr>\r\n           ");
      out.write("<td class= \"texto\" width=\"12%\">Nombre y Apellidos:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"53%\">");
      out.write("<input type=\"text\" name=\"nomape\" class=\"cajatexto2\" value=\"\" style=\"width:90%\">");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"10%\">N.I.F.:");
      out.write("</td>\r\n           ");
      out.write("<td class= \"texto\" width=\"25%\">");
      out.write("<input maxlength=8 type=\"text\" name=\"dni\"  style=\"width:50%\"class=\"cajatexto2\"  value=\"\" onkeypress = \"if (((event.keyCode ");
      out.write("< 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;\" onBlur=\"CalculaNIF(this.value);\" >&nbsp;\r\n         - ");
      out.write("<input maxlength=1 type=\"text\" name=\"letradni\" style=\"width:10%\" class=\"cajatexto2\" value=\"\" >");
      out.write("</td>\r\n          ");
      out.write("</tr>\r\n         ");
      out.write("</table>\r\n        ");
      out.write("</td>\r\n       ");
      out.write("</tr>\r\n      ");
      out.write("</table>\r\n     ");
      out.write("</FIELDSET>\r\n    ");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n  ");
      out.write("</table>\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
