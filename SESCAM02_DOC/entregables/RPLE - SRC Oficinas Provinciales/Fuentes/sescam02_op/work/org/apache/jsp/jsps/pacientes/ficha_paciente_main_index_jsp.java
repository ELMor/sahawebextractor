package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.*;
import isf.persistencia.*;
import java.util.*;

public class ficha_paciente_main_index_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      java.util.HashMap beanParamListaSolic = null;
      synchronized (session) {
        beanParamListaSolic = (java.util.HashMap) pageContext.getAttribute("beanParamListaSolic", PageContext.SESSION_SCOPE);
        if (beanParamListaSolic == null){
          try {
            beanParamListaSolic = (java.util.HashMap) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "java.util.HashMap");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "java.util.HashMap", exc);
          }
          pageContext.setAttribute("beanParamListaSolic", beanParamListaSolic, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");

 beanParamListaSolic.clear();

      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<script language=\"javascript\">\r\nfunction carga_frames(){\r\n   var strLlamada=\"\";\r\n   strLlamada=\"ficha_paciente_main_cabecera.jsp?cip=\";\r\n   strLlamada+=escape('");
      out.print(request.getParameter("cip"));
      out.write("')\r\n   strLlamada+='&centro=");
      out.print(request.getParameter("centro"));
      out.write("';\r\n   strLlamada+='&norden=");
      out.print(request.getParameter("norden"));
      out.write("';\r\n   strLlamada+='&gar_cq=");
      out.print(request.getParameter("gar_cq"));
      out.write("';\r\n   document.frames(\"cabecera\").location=strLlamada;\r\n   strLlamada=\"ficha_paciente_main_datos.jsp?cip=\"\r\n   strLlamada+=escape('");
      out.print(request.getParameter("cip"));
      out.write("')\r\n   strLlamada+='&centro=");
      out.print(request.getParameter("centro"));
      out.write("';\r\n   strLlamada+='&norden=");
      out.print(request.getParameter("norden"));
      out.write("';\r\n   strLlamada+='&gar_cq=");
      out.print(request.getParameter("gar_cq"));
      out.write("';\r\n   document.frames(\"datos\").location=strLlamada;\r\n}\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<frameset rows=\"90,20,55,*\" frameborder=\"no\" border=\"1\" framespacing=\"0\" onload='carga_frames();'>\r\n    ");
      out.write("<frame name=\"cabecera\" scrolling=\"auto\" src=\"\">\r\n    ");
      out.write("<frame name=\"datos_cab\" scrolling=\"no\" src=\"ficha_paciente_main_datos_tit.htm\">\r\n    ");
      out.write("<frame name=\"datos\" scrolling=\"yes\" src=\"\">\r\n    ");
      out.write("<frame name=\"datos_tabs\" scrolling=\"no\" src='ficha_paciente_main_datos_dettab.htm'>\r\n");
      out.write("</frameset>\r\n");
      out.write("<noframes>\r\n");
      out.write("<body class=\"mainFondo\">\r\n");
      out.write("</body>\r\n");
      out.write("</noframes>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
