package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.jasper.runtime.*;
import isf.negocio.Paciente;
import java.util.Vector;
import java.util.Enumeration;
import beans.ConsultaPacienteBean;

public class busca_paciente_main_lista_det_jsp extends HttpJspBase {


  private static java.util.Vector _jspx_includes;

  static {
    _jspx_includes = new java.util.Vector(1);
    _jspx_includes.add("/jsps/control_acceso.txt");
  }

  public java.util.List getIncludes() {
    return _jspx_includes;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    javax.servlet.jsp.PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"error.jsp", true, 8192, true);
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n\r\n");
      beans.AccesoBean accesoBeanId = null;
      synchronized (session) {
        accesoBeanId = (beans.AccesoBean) pageContext.getAttribute("accesoBeanId", PageContext.SESSION_SCOPE);
        if (accesoBeanId == null){
          try {
            accesoBeanId = (beans.AccesoBean) java.beans.Beans.instantiate(this.getClass().getClassLoader(), "beans.AccesoBean");
          } catch (ClassNotFoundException exc) {
            throw new InstantiationException(exc.getMessage());
          } catch (Exception exc) {
            throw new ServletException("Cannot create bean of class " + "beans.AccesoBean", exc);
          }
          pageContext.setAttribute("accesoBeanId", accesoBeanId, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
 String srtPermAcceso="acceso_LEQ";
      out.write("\r\n");

	response.setHeader("Pragma", "no-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("expires",0);
	response.setHeader("expires","0");
	response.setHeader("Cache-Control","store");
	if (!accesoBeanId.accesoA(srtPermAcceso))
    	{

      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n        \t");
      out.write("<script language=\"javascript\">\r\n\t\t\tdocument.location = \"/crlesp/cierra.html\";\r\n\t\t");
      out.write("</script>\r\n");
      out.write("<head>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</head>\r\n");

		out.flush();
		return;
  	}

      out.write("\r\n");
      out.write("\r\n\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<title>Untitled Document");
      out.write("</title>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("<link rel=stylesheet type=\"text/css\" href=\"../../css/estilo.css\">\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\" src=\"../../javascript/menus.js\">");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\">\r\nfunction seleccionaFila(filas, id){\r\n    filas[id].className = \"filaSelecionada\";\r\n    }\r\nfunction seleccion(pSelCip){\r\n  parent.parent.parent.location='ficha_paciente_index.jsp?cip='+escape(pSelCip);\r\n}\r\n");
      out.write("</script>\r\n");
      out.write("</head>\r\n");
      out.write("<body id=\"juli\" class=\"mainFondo\" bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" onload=\"\">\r\n");
      out.write("<form name=date>\r\n  ");
      out.write("<table height=\"100%\" width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n   ");
      out.write("<tr>\r\n    ");
      out.write("<td class=\"areaCentral\" >\r\n    ");
      out.write("<table style=\"width:100%;\" align=\"left\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n     ");
      out.write("<tr>\r\n      ");
      out.write("<td class=\"areaCentral2\" >\r\n\t");
      out.write("<table class=\"centroTabla\" id=\"tablaPac\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n");

		Paciente pacRet;
		Vector vp;
		boolean bFilaPar=true;
		int idCounter=0;
                long viene_cip = 0;
                if ((request.getParameter("viene_cip") != null) && (!request.getParameter("viene_cip").equals(""))) {
                  viene_cip = 1;
                }
         	ConsultaPacienteBean cp=new ConsultaPacienteBean();
		cp.setNombre(request.getParameter("nombre"));
		cp.setApellido1(request.getParameter("apellido1"));
		cp.setApellido2(request.getParameter("apellido2"));
		cp.setCip(request.getParameter("cip"));
                cp.setCentro(request.getParameter("centro"));
  		cp.setHicl(request.getParameter("hicl"));
                cp.setOrden(request.getParameter("orden"));
                cp.SetNegEvento(accesoBeanId.getNegEventoInstance(20));
		vp=cp.resultado();
		if (vp!=null){

      out.write("\r\n                 ");
      out.write("<script language=\"javascript\">\r\n                    parent.pie.location=\"../pie_buscador.jsp?estado=Generando resultado\";\r\n                 ");
      out.write("</script>\r\n");

        if (vp.size()>0){
	  			Enumeration e = vp.elements();
          pacRet = new Paciente();
          String numhc="S/D"; 
	    		while(e.hasMoreElements()) {
	      			pacRet=(Paciente)e.nextElement();
	      			//System.out.println("NHC:"+pacRet.fHicl);
							if (pacRet.fHicl!=0){
								numhc= new Integer(pacRet.fHicl).toString();
							}
							if ((pacRet.fDomi==null)||(pacRet.fDomi.equals(""))) pacRet.fDomi=" ";
							if ((pacRet.fPobl==null)||(pacRet.fPobl.equals(""))) pacRet.fPobl=" ";
							if ((pacRet.fProv==null)||(pacRet.fPobl.equals(""))) pacRet.fProv=" ";
						
	      			if (bFilaPar){
	      			   	bFilaPar=false;
	
      out.write("\r\n\t\t\t\t\t\t");
      out.write("<tr class=\"filaPar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('");
      out.print(pacRet.fCip);
      out.write("')\" style=\"cursor:hand;\">\r\n\t\t");
				}else{
									bFilaPar=true;

	
      out.write("\r\n\t\t\t\t\t\t");
      out.write("<tr class=\"filaImpar\" id=");
      out.print(idCounter);
      out.write(" onclick=\"seleccionaFila(document.all['tablaPac'].rows, this.id);seleccion('");
      out.print(pacRet.fCip);
      out.write("')\" style=\"cursor:hand;\">\r\n\t");

						}
	
      out.write("\r\n\r\n\t\t\t\t    ");
      out.write("<td width=\"20%\"  height=\"12px\" name=\"cip\">");
      out.print(pacRet.fCip);
      out.write("</td>\r\n\t\t\t\t    ");
      out.write("<td width=\"10%\"  height=\"12px\" name=\"centro\" title=\"");
      out.print(pacRet.fDesCentro);
      out.write("\">");
      out.print(pacRet.fCentro);
      out.write("</td>\r\n\t\t\t\t    ");
      out.write("<td width=\"12%\"  height=\"12px\" name=\"nhc\">");
      out.print(numhc);
      out.write("</td>\r\n\t\t\t\t    ");
      out.write("<td width=\"12%\"  height=\"12px\" name=\"fnac\">");
      out.print(pacRet.fNac);
      out.write("</td>\r\n\t\t\t\t    ");
      out.write("<td width=\"26%\" height=\"12px\" name=\"nom\">");
      out.print(pacRet.fApe1+", "+pacRet.fApe2+", "+pacRet.fNomb);
      out.write("</td>\r\n\t\t\t\t    ");
      out.write("<td width=\"20%\"  height=\"12px\" name=\"dir\">");
      out.print(pacRet.fDomi+", "+pacRet.fPobl+", "+pacRet.fProv);
      out.write("</td>\r\n\t\t\t   \t");
      out.write("</tr>\r\n");

  				idCounter++;
  				}
                                
      out.write("\r\n                                ");
      out.write("<script language=\"javascript\">\r\n                                     parent.pie.location=\"../pie_buscador.jsp?estado=Listo.&filas=");
      out.print(vp.size());
      out.write("\";\r\n                                ");
      out.write("</script>\r\n");

                                if ((vp.size() == 1) && (viene_cip > 0)) {
                                 
      out.write("\r\n                                  ");
      out.write("<script language=\"javascript\">\r\n                                      parent.parent.parent.location=\"ficha_paciente_index.jsp?cip=\"+escape('");
      out.print(pacRet.fCip);
      out.write("');\r\n                                  ");
      out.write("</script>\r\n                                ");


                                }
                  }else{

      out.write("\t\t   ");
      out.write("<Script language=\"javaScript\">\r\n                        parent.pie.location=\"../pie_buscador.jsp?estado=No se han encontrado filas.\";\r\n                   ");
      out.write("</Script>\r\n");

                  }
		}

      out.write("\r\n\t");
      out.write("</table>\r\n    ");
      out.write("</td>\r\n   ");
      out.write("</tr>\r\n   ");
      out.write("</table>\r\n   ");
      out.write("</td>\r\n  ");
      out.write("</tr>\r\n");
      out.write("</table>\r\n");
      out.write("</form>\r\n");
      out.write("</body>\r\n");
      out.write("<HEAD>\r\n");
      out.write("<META HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">\r\n");
      out.write("</HEAD>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      out = _jspx_out;
      if (out != null && out.getBufferSize() != 0)
        out.clearBuffer();
      if (pageContext != null) pageContext.handlePageException(t);
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(pageContext);
    }
  }
}
