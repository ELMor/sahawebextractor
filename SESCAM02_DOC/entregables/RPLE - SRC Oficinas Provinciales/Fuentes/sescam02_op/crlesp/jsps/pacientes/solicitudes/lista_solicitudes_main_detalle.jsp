<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.Utilidades,beans.ConsultaEntradasNoLE" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<jsp:useBean id="EntradaNoLEBean" scope="session" class="java.util.HashMap"/>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
}

function selec(t,garpk,esnolesp){
  if ((t==1)&&(esnolesp==0)){ //solicitud
    parent.parent.centro.location='ficha_vis_solicitud.jsp?garpk='+garpk;
    parent.parent.botonera.location.href = 'ficha_vis_solicitud_botonera.jsp?garpk='+garpk;
  }else if (t!=1){
         parent.parent.centro.location='ficha_vis_renuncia.jsp?garpk='+garpk;
         parent.parent.botonera.location.href = 'ficha_vis_renuncia_botonera.jsp?garpk='+garpk;
        }else{
         parent.parent.centro.location='ficha_vis_denegacion_nolesp.jsp?garpk='+garpk;
         parent.parent.botonera.location.href = 'ficha_vis_denegacion_nolesp_botonera.jsp?garpk='+garpk;
        }
}

</script>
</head>

<body id="sel" class="mainFondoInterno" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<%
  String tipo,estado,oficina,consulta_gar="CENTRO=-1 ",garcq="";
  boolean sol_con_registro=true;
  long gar_pk,l_centro=0,l_norden=0,nnolesp=0;
  Vector vg=new Vector();
  Vector vgt=new Vector();
  //System.out.println(beanParamListaSolic);
  if (beanParamListaSolic.size()>1){
    l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
    garcq = ((String)beanParamListaSolic.get("GAR_CQ")).toString();
    if (garcq.equals("Q")){
       l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
       consulta_gar="CENTRO="+l_centro+" AND NORDEN="+l_norden;
    }else{
         l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
         consulta_gar="CEGA="+l_centro+" AND NCITA="+l_norden;
    }
    if (beanParamListaSolic.get("NNOLESP")!=null){
         nnolesp = ((Long)beanParamListaSolic.get("NNOLESP")).longValue();
    }
    if (nnolesp>0) sol_con_registro=false;
  }
  //System.out.println(l_centro+" "+l_norden);
  oficina = OficinaHelper.DameOficina(accesoBeanId.getUsuario());
  //System.out.println(oficina);
 // System.out.println("CENTRO: "+ l_centro);
 // System.out.println("GAR_CQ: "+garcq);
 // System.out.println("NORDEN: "+l_norden);
 // System.out.println("consulta: "+consulta_gar);
%>


<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentralInterno" >
        <table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
        <%
             boolean bFilaPar=true;
             int idCounter=0;
             long gar_estado,gar_tipo;
             Garantia garan= new Garantia();
             if (sol_con_registro){
                vg = garan.busquedaGaran(consulta_gar,"");
                if (vg.size()>0){
                          //System.out.println("El vector de garantias no es nulo:"+vg.size());
                          for(int int_pos = 0;int_pos < vg.size(); int_pos++){
                                SesGarantia sesGar=(SesGarantia)vg.elementAt(int_pos);
                                gar_estado=sesGar.getGarEstado();
                                gar_pk=sesGar.getGarPk();
                                gar_tipo=sesGar.getGarTipo();
                                //System.out.println("Estado:"+gar_estado);
                                //System.out.println("Gar pk:"+gar_pk);
                                GaranTram gt = new GaranTram();
                                vgt = gt.busquedaGaranTram("GAR_PK="+gar_pk+" AND GAR_OPERACION="+gar_estado,"");
                                SesGaranTram sgt = (SesGaranTram)vgt.elementAt(0);
                                if (bFilaPar){
                                        bFilaPar=false;
        %>
                                        <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec(<%=gar_tipo%>,<%=gar_pk%>,0)" style="cursor:hand;">
        <%				}else{
                                            bFilaPar=true;
        %>
                                        <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec(<%=gar_tipo%>,<%=gar_pk%>,0)" style="cursor:hand;">
        <%
                                        }
                                if (sesGar.getGarTipo()==1) {
                                  tipo="Solicitud";
                                }else{
                                  tipo="Renuncia";
                                }
                                //System.out.println(tipo);
        %>
                                <td width="20%" height="12px"><%=tipo%></td>
        <%
                                 if (tipo!="Renuncia"){
                                    if (sesGar.getGarEstado()==1) {
                                       estado="Solicitada";
                                    }else{
                                       if (sesGar.getGarEstado()==2) {
                                          estado="Aprobada";
                                       }else if (sesGar.getGarEstado()==3) {
                                         estado="Denegada";;
                                       }else {
                                         estado= "Renunciada";
                                       }
                                    }
                                 }else{
                                   estado="";
                                 }
                                 //System.out.println(estado);
        %>
                                <td width="20%" height="12px"><%=estado%></td>
        <%
                                 if (sesGar.getGarTipo()==1) {
                                  tipo="Solicitud";
                                 }else{
                                  tipo="Renuncia";
                                 }
        %>
                                <td width="28%" height="12px"><%=oficina%></td>
                                <td width="14%" height="12px"><%=sesGar.getGarMonto()%></td>
                                <td width="18%" height="12px"><%=Utilidades.dateToString(sgt.getGarFechor())%></td>

                           </tr>
<%
                                  idCounter++;
                                  //System.out.println("Hace el counter++");
                          }
                }
             }else{
                 if (bFilaPar){
                    bFilaPar=false;
        %>
                    <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec(1,<%=nnolesp%>,1)" style="cursor:hand;">
        <%	}else{
                    bFilaPar=true;
        %>
                    <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec(1,<%=nnolesp%>,1)" style="cursor:hand;">
        <%
                }
        %>
                     <td width="20%" height="12px">Solicitud</td>
                     <td width="20%" height="12px">Denegacion</td>
                     <td width="28%" height="12px"><%=EntradaNoLEBean.get("OFICINA")%></td>
                     <td width="14%" height="12px">0</td>
                     <td width="18%" height="12px"><%=EntradaNoLEBean.get("FECHA")%></td>
                    </tr>
<%
              }
%>
        </table>
    </td>
   </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

