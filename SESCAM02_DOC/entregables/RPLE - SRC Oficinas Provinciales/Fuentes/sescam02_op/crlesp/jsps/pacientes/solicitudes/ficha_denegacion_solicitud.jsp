<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/validarNif.js"></script>
</head>
<%
  long l_garpk;
  l_garpk = new Long(request.getParameter("garpk")).longValue();
  String motivoSSG="La prestacion por la que se solicita el documento acreditativo es una prestacion de control o sucesiva, y por lo tanto no garantizada.";
  String valor_motivo="";
  boolean ssg=false;
  String strSit=(String)beanParamListaSolic.get("STRSIT");
  if (strSit.equals("SSG")) {
      valor_motivo=motivoSSG;
      ssg=true;
  }
  if (l_garpk <0){
%>
    <script language="javascript">
      alert("No hay solicitud seleccionada.");
    </script>
<%}
%>

<BODY class="mainFondoInterno">
<form name="denegacion" action="guardarDenegacionSolicitud.jsp" target="oculto1">
<a class="texto"><b><u>FICHA DENEGACION</u><b></a>
<br>
<br>
<FIELDSET >
<LEGEND><a class="texto"><b>RESPONSABLE LEGAL</b></a></LEGEND>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
   <tr>
       <td width="10%" class= "texto">Nombre:</td>
       <td class= "texto"><input type="text" name="nombre" class="cajatexto3" maxlength = 255 value=""></td>
   </tr>
   <tr>
       <td class= "texto">N.I.F:</td>
       <td class= "texto"><input maxlength=8 type="text" name="dni" class="cajatexto2" style="width:12%" value="" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;" onBlur="CalculaNIF(this.value);">
        - <input maxlength=1 type="text" name="letradni" style="width:3%" class="cajatexto2" value="" readonly></td>
   </tr>
</table>
<br>
</FIELDSET>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
   <tr>
       <td width="10%" class= "texto">Motivo Denegacion:</td>
<%
       if (ssg) {
%>
       <td class= "texto"><textarea name="motivo" class="textarea" rows="2" maxlength = 4000 value="<%=valor_motivo%>" readonly><%=valor_motivo%></textarea></td>
<%
       }else{
%>
       <td class= "texto"><textarea name="motivo" class="textarea" rows="2" maxlength = 4000 value=""><%=valor_motivo%></textarea></td>
<%
       }
%>

   </tr>
</table>
<td class= "texto"><input type="hidden" name="garpk" value="<%=l_garpk%>"></td>
<br>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
