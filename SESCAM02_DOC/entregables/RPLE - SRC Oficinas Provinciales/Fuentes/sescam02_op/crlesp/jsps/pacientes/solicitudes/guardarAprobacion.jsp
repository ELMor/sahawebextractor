<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamListaSolic" class="java.util.HashMap" scope="session"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<%
                /***************************/
                /* Definicion de variables */
                /***************************/
                SesGarantia sG = new SesGarantia();
                Vector v=new Vector();
                long l_centro,l_norden=0;
                String s_garcq;
                double d_monto = 0;
                int i_error = 1;
                boolean imprimir=false;

                /**************************/
                /* Recojida de parametros */
                /**************************/
                s_garcq = new String((String)beanParamListaSolic.get("GAR_CQ").toString());
                l_centro = ((Long)beanParamListaSolic.get("CENTRO")).longValue();
                /* Diferenciacion de registro de LESP o LESPCEX */
                if (s_garcq.equals("Q")) {
                  l_norden = ((Long)beanParamListaSolic.get("NORDEN")).longValue();
                } else {
                  l_norden = ((Long)beanParamListaSolic.get("NCITA")).longValue();
                }
                String str_nombre = request.getParameter("nombre");
                String str_dni = request.getParameter("dni");
                String str_letra = request.getParameter("letradni");
                String str_nif = str_dni+str_letra;
                try {
                  d_monto = new Double(request.getParameter("monto")).doubleValue();
                } catch (Exception ex) {
                    i_error = -1;
                    ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);
                    %>
                  <script language="javascript">
                      alert ('<%=es.getMensaje()%>');
                  </script>
                    <%
                }

                /******************************/
                /* Insercion de la Aprobacion */
                /******************************/
                if (i_error > 0) {
                  try{
                    /* Busqueda de la Solicitud asociada */
                    long garpk= new Long(request.getParameter("garpk")).longValue();
                    Garantia gar = new Garantia();
                    v = gar.busquedaGaran("GAR_PK="+garpk,"");
                    sG = (SesGarantia)v.elementAt(0);

                    /*Seteo de datos */
                    sG.setGarEstado(2);
                    sG.setGarMonto(d_monto);

                    /*Insercion*/
                    try{
                    gar.SetNegEvento(accesoBeanId.getNegEventoTramInstance(16),65,"DOC02",l_centro,l_norden);
                    gar.modificarGaran(sG,2,accesoBeanId.getUsuario(),str_nombre,str_nif);
                    }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en guardarAprobacion.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                    }

                   imprimir = true;
                  }catch (Exception e){
                    System.out.println(" Error " +e+ "al generar la aprobacion");
                    imprimir=false;
                  }
                }
%>
<html>
<head>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript">
    var doc="DOC02";
    function ImprimirDoc(){
      //llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&usuario=<%=accesoBeanId.getUsuario()%>';
      llamado='../imprime_doc.jsp?centro=<%=l_centro%>&norden=<%=l_norden%>&cq=<%=s_garcq%>&documento='+doc+'&garpk=<%=sG.getGarPk()%>&operacion=2&usuario=<%=accesoBeanId.getUsuario()%>';
      window.showModalDialog(llamado,"","dialogWidth:80em;dialogHeight:50em");
    }
</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
               /* Si la insercion ha sido correcta imprimir documento */
               if ((i_error > 0) && (imprimir)){
%>

                   <script language="javascript">
                        ImprimirDoc();
                        parent.centro.location.href = "lista_solicitudes_main.jsp"
                        parent.botonera.location.href = "lista_solicitudes_down.jsp"
                   </script>
                <%
                }
                %>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>