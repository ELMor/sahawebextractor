<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.EntradaLE" %>
<%@ page import="isf.negocio.EntradaNoLE" %>
<%@ page import="isf.negocio.Centros" %>
<%@ page import="java.util.Vector" %>
<%@ page import="beans.AccesoBean" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaEntradasLE"%>
<%@ page import="beans.ConsultaEntradasNoLE"%>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="consultaEntradaLEBeanId" scope="session" class="beans.ConsultaEntradasLE"/>
<jsp:useBean id="consultaEntradaNoLEBeanId" scope="session" class="beans.ConsultaEntradasNoLE"/>
<jsp:useBean id="EntradaNoLEBean" scope="session" class="java.util.HashMap"/>

<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../css/estilo.css">
<script language="javascript">
var FilaSelected=-1;
var SelCentro="",SelNOrden="";

function seleccionaFila(id,pNnolesp,pSelCentro,pSelNOrden,pSelTipoCQ,strSit){
  //alert(pSelTipoCQ);
  var nomCheck="";
  SelCentro=pSelCentro;
  SelNOrden=pSelNOrden;
  if (FilaSelected!=-1){
    nomCheck="chequeo"+FilaSelected;
    document.all(nomCheck).style.visibility ="hidden";
  }
  nomCheck="chequeo"+id;
  document.all(nomCheck).style.visibility ="visible";
  FilaSelected=id;
  parent.frames("datos_tabs").document.frames("detalle_dat").location='ficha_paciente_main_datos_det.jsp?centro='+pSelCentro+'&norden='+pSelNOrden+'&gar_cq='+pSelTipoCQ;
  parent.frames("datos_tabs").document.frames("detalle_tit").location='ficha_paciente_main_datos_det_tit.jsp?gar_cq='+pSelTipoCQ;
  parent.frames("datos_tabs").document.frames("historia_dat").location='ficha_paciente_main_datos_histo.jsp?centro='+pSelCentro+'&norden='+pSelNOrden+'&gar_cq='+pSelTipoCQ;;
  parent.frames("datos_tabs").document.frames("solicitud_index").location='solicitudes/lista_solicitudes_index.jsp?centro='+pSelCentro+'&norden='+pSelNOrden+'&gar_cq='+pSelTipoCQ+'&nnolesp='+pNnolesp+'&strsit='+strSit;
}

</script>

</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<!-- DETALLE INI-->
  <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
        <table id="entradasLista" class="centroTabla" cellspacing="0" cellpadding="0" border="0">
  <%
  EntradaLE entLE;
  EntradaNoLE entnoLE;
  boolean bFilaPar=true;
  boolean hayLESP=false;
  int idCounter=0;
  Vector vp;
  Vector vnl;
  String strCIP=request.getParameter("cip");
  consultaEntradaLEBeanId.setCip(strCIP);
  consultaEntradaNoLEBeanId.setTipo(1); //Consulta por CIP
  consultaEntradaNoLEBeanId.setCip(strCIP);
  int iPCentro=0;
  int iPNOrden=0;
  String strPHistorico="0";
  int iCntHist=0;
  int iCntAct=0;
  int iPID=0;
  long pNnolesp=0;
  boolean bRecarga=true;
  String strPCQ="";
  String PSit="";
  strPHistorico=request.getParameter("historico");
  if ((strPHistorico==null)||(strPHistorico.equals(""))){
    strPHistorico="0";
    bRecarga=true;
  }else{
      bRecarga=false;
  }
  try {
      iPCentro=Integer.parseInt(request.getParameter("centro").trim());
      iPNOrden=Integer.parseInt(request.getParameter("norden").trim());
      strPCQ=request.getParameter("gar_cq").trim();
      iPID=-1;
  }catch (Exception e) {
         iPCentro=0;
         iPNOrden=0;
  }
  if (bRecarga){
    vp=consultaEntradaLEBeanId.resultado();
  }else{
    vp=consultaEntradaLEBeanId.vec_EntradasLE;
  }

  long nolesp=0;
  if (vp!=null){
    Enumeration e = vp.elements();
    if ((iPID==-1)&&(consultaEntradaLEBeanId.existeEntrada(iPCentro,iPNOrden,strPCQ)==null)){
      iPID=0;
      }
    Centros c=new Centros();
    while (e.hasMoreElements()) {
        entLE=(EntradaLE)e.nextElement();
        String descentro = c.dameDescCentro(entLE.fCentro);
        idCounter++;
        String strColor="";
        String strSit="";
        String descsit="";
        strSit=ConsultaEntradasLE.STR_SITUACION[entLE.fSituacion];

        if (strSit.equals("HISTOR")) descsit="Prestacion atendida";
        if (strSit.equals("TG")) descsit="Prestacion y Especialidad garantizadas";
        if (strSit.equals("TG-DV")) descsit="TG con Aplazamiento Voluntario";
        if (strSit.equals("TG-DM")) descsit="TG con Aplazamiento Medico";
        if (strSit.equals("SG-RO")) descsit="Sin garantia por rechazo de oferta";
        if (strSit.equals("SG-BPA")) descsit="Sin garantia por varios aplazamientos";
        if (strSit.equals("PSG")) descsit="Prestacion y/o Especialidad sin garantizar";
        if (strSit.equals("NOCAT")) descsit="Situcion no Catalogada.Avisar a informatica.";
        if (strSit.equals("NOMAP")) descsit="Prestacion y/o Especialidad sin mapear.";
        if (strSit.equals("SSG")) descsit="Sucesiva Sin Garantia.";

        if(!entLE.isHistory()){
                iCntAct++;
                if (vp.size()>0) hayLESP=true;
                if(entLE.isOutOfDate()){ //ROJO
                  strColor="style='background-color:#FEA8A2;'";
                }else{
                 //VERDE tanto los no vencidos como los no garantizados
                  strColor="style='background-color:#99D7C6;'";
                }
        }else{
                iCntHist++;
                if (strPHistorico.equals("0")) continue;
                  //GRIS
                strColor="style='background-color:#687CAB;'";
        }
        if (bFilaPar){
          bFilaPar=false;
 %>
         <tr class="filaPar" id="<%=idCounter%>" onclick="seleccionaFila(<%=idCounter%>,<%=nolesp%>,<%=entLE.fCentro%>,<%=entLE.fNorden%>,'<%=entLE.fTipoCQ%>','<%=strSit%>');" style="cursor:hand;">
 <%	 }else{
         bFilaPar=true;
 %>
         <tr class="filaImpar" id="<%=idCounter%>" onclick="seleccionaFila(<%=idCounter%>,<%=nolesp%>,<%=entLE.fCentro%>,<%=entLE.fNorden%>,'<%=entLE.fTipoCQ%>','<%=strSit%>');" style="cursor:hand;">
 <%
         }
 %>
           <td width="2%"><DIV id="chequeo<%=idCounter%>" style="visibility: hidden;"><img src="../../imagenes/verifica.gif" border=0></DIV></td>
           <td width="8%" ><%=entLE.fNorden%></td>
           <td width="10%" title="<%=descentro%>"><%=entLE.fCentro%>/<%=entLE.fHicl%></td>
           <td width="7%"><%=entLE.fCProc%></td>
           <td width="50%"><%=entLE.fDProc%></td>
           <td width="5%"><%=entLE.fTipoCQ%></td>
           <td width="8%"><a title="<%=descsit%>"><%=strSit%></a></td>
           <td width="10%" <%=strColor%> ><%=entLE.fAntiguedad%>/<%=entLE.fMaxEspera%></td>
         </tr>
<%
          if (iPID==0){
            iPID=idCounter;
            iPCentro=entLE.fCentro;
            iPNOrden=entLE.fNorden;
            strPCQ=entLE.fTipoCQ;
            PSit=strSit;
          }else if(iPID==-1){
            if ((iPCentro==entLE.fCentro)&&(iPNOrden==entLE.fNorden)&&(strPCQ.equals(entLE.fTipoCQ))){
              iPID=idCounter;
            }
          }
    }
  }


  int iCntNolesp=0;
  boolean pacNoLesp=false;
  vnl=consultaEntradaNoLEBeanId.resultado();
   if (vnl.size()>0){
    if (strPCQ=="") strPCQ="Q";
    String strSituacion="NOLESP";
    String strColor="style='background-color:#9D9DCE;'";
    Enumeration e = vnl.elements();
    while (e.hasMoreElements()) {
        iCntNolesp++;
        idCounter++;
        if (iPID==0) iPID=idCounter; //iPID controla la seleccion automatica
        entnoLE=(EntradaNoLE)e.nextElement();

        if (bFilaPar){
          bFilaPar=false;
          pNnolesp=entnoLE.getFNnolesp();
          nolesp=pNnolesp;

 %>
         <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(this.id,<%=pNnolesp%>,0,0,'Q')" style="cursor:hand;">
 <%	 }else{
            bFilaPar=true;
            pNnolesp=entnoLE.getFNnolesp();
 %>
         <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(this.id,<%=pNnolesp%>,0,0,'Q')" style="cursor:hand;">
 <%
         }
         //Guardamos infomacion en el bean acerca de la solicitud
          EntradaNoLEBean.put("OFICINA",entnoLE.getFOfi_nombre());
          EntradaNoLEBean.put("FECHA",entnoLE.getFFecha());
          EntradaNoLEBean.put("GARPK",new Long(entnoLE.getFNnolesp()));

 %>
           <td width="2%"><DIV id="chequeo<%=idCounter%>" style="visibility: hidden;"><img src="../../imagenes/verifica.gif" border=0></DIV></td>
           <td width="5%" ><%=entnoLE.getFNnolesp()%></td>
           <td width="10%" title="<%=entnoLE.getFDesCentro()%>"><%=entnoLE.getFCentro()%>/<%=entnoLE.getFHicl()%></td>
           <td width="10%">SD</td>
           <td width="48%"><%=entnoLE.getFPrestacion()%></td>
           <td width="5%">N</td>
           <td width="8%"><a title="Fuera de Lista de Espera"><%=strSituacion%></a></td>
           <td width="12%" <%=strColor%> >0/0</td>
         </tr>
 <%
      }
  }
%>
        </table>
    </td>
   </tr>
   <tr class="pieInferior" width="100%">
        <td width="100%">
         Activos:<%=iCntAct%>&nbsp;&nbsp;Historicos:<%=iCntHist%>&nbsp;&nbsp;Sin Registrar:<%=iCntNolesp%>
        </td>
    </tr>
   </table>
   </td>
  </tr>
</table>

<!-- DETALLE FIN-->
<%

        if ((iPID!=0)&&(iPID!=-1)){
              if (hayLESP) nolesp=0;
%>        <script language="javascript">
              this.seleccionaFila(<%=iPID%>,<%=nolesp%>,<%=iPCentro%>,<%=iPNOrden%>,'<%=strPCQ%>','<%=PSit%>');
          </script>
<%
          }else{
%>        <script language="javascript">
            parent.frames("datos_tabs").document.frames("detalle_dat").location='ficha_paciente_main_datos_det.jsp';
            parent.frames("datos_tabs").document.frames("solicitud_index").location='solicitudes/lista_solicitudes_index.jsp';
            parent.frames("datos_tabs").document.frames("historia_dat").location='ficha_paciente_main_datos_histo.jsp';
          </script>
<%
          }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

