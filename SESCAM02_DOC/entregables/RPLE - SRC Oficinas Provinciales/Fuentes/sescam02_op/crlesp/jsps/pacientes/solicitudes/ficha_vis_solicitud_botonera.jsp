<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>


<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<jsp:useBean id="beanParamListaSolic" scope="session" class="java.util.HashMap"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>
<%
  boolean lb_acceso=false;
  if (accesoBeanId.accesoA("aprob_deneg_SOL")){
    lb_acceso=true;
  }
%>
<html>
<head>
<title>Untitled Document</title>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">

<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">

      function aprobar(garan,permitir,spac){
       if (<%=lb_acceso%>==true){
        if (permitir == true) {
          if (spac == true){
	          parent.centro.document.location.href = "ficha_aprobacion_solicitud.jsp?garpk="+garan;
	          parent.botonera.document.location.href = "ficha_aprobacion_solicitud_botonera.jsp";
	      }else{
	          alert("No es posible aprobar la solicitud ya que la consulta es Sucesiva y por ello no esta garantizada.");
	      }
        }else {
           alert("No es posible aprobar la garantia, no se encuentra en estado Solicitada");
        }
       }else{
           alert("No tiene permiso para realizar la accion");
       }
      }

      function denegar(garan,permitir){
       if (<%=lb_acceso%>==true){
        if (permitir == true) {
          parent.centro.document.location.href = "ficha_denegacion_solicitud.jsp?garpk="+garan;
          parent.botonera.document.location.href = "ficha_denegacion_solicitud_botonera.jsp";
        }else {
         alert("No es posible denegar la garantia no esta en estado Solicitada");
        }
       }else{
        alert("No tiene permiso para realizar la accion");
       }
      }

      function renuncia(garan,renuncia) {
         if (<%=lb_acceso%>==true){
          if (renuncia == true) {
           parent.centro.location.href="ficha_nueva_renuncia.jsp?garpk="+garan;
           parent.botonera.location.href= "ficha_nueva_renuncia_botonera.jsp";
          }else{
           alert("La solicitud no esta aprobada, no se puede renunciar.");
          }
         }else{
           alert("No tiene permiso para realizar la accion");
         }
       }

      function cancelar(){
                      parent.centro.document.location.href = "lista_solicitudes_main.jsp";
                      parent.botonera.document.location.href = "lista_solicitudes_down.jsp";
      }

</script>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
   <%
     boolean lb_permitir = false,lb_renuncia = false, lb_spac=true;
     long l_garpk = new Long(request.getParameter("garpk")).longValue();
     Garantia gar = new Garantia();
     Vector v = new Vector();
     Vector vg = new Vector();
     v = gar.busquedaGaran("(GAR_ESTADO = 1 OR GAR_ESTADO = 2) AND GAR_PK = "+l_garpk,"");
     vg = gar.busquedaGaran("GAR_PK = "+l_garpk,"");
     SesGarantia sesGar=(SesGarantia)vg.elementAt(0);
     if (v.size() == 1){
       if (sesGar.getGarEstado() == 2) {
         lb_renuncia = true;
       }
       else if (sesGar.getGarEstado() == 1){
         lb_permitir = true;
       }
       String situacion = (String)beanParamListaSolic.get("STRSIT");
       //System.out.println("Situacion del paciente:"+ situacion);
       if (situacion.equals("SSG")) lb_spac=false; 
     }
  %>

<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
<%
  if (sesGar.getGarEstado()==1){
%>
    <td  width="33%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="aprobar(<%=l_garpk%>,<%=lb_permitir%>,<%=lb_spac%>);">Aprobar</td>
    <td  width="33%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="denegar(<%=l_garpk%>,<%=lb_permitir%>);">Denegar</td>
    <td  width="33%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar();">Volver</td>
<%
  }else if (sesGar.getGarEstado()==2) {
%>
    <td  width="25%" class="botonAccion">&nbsp</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="renuncia(<%=l_garpk%>,<%=lb_renuncia%>);">Renunciar</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar();">Volver</td>
    <td  width="25%" class="botonAccion">&nbsp</td>
<%
  }else {
%>
    <td  width="33%" class="botonAccion">&nbsp</td>
    <td  width="33%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar();">Volver</td>
    <td  width="33%" class="botonAccion">&nbsp</td>
<%
  }
%>

  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
