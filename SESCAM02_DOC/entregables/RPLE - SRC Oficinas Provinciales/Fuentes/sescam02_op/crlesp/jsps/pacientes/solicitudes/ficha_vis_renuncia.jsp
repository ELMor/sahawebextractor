<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.*,isf.util.Utilidades" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
</head>
<%
    long oficina_pk=0;
    long l_pkgar = new Long(request.getParameter("garpk")).longValue();
    Garantia gar = new Garantia();
    SesGarantia sesGar = new SesGarantia();
    Vector vg = new Vector();
    vg = gar.busquedaGaran("GAR_PK="+l_pkgar,"");

%>
<BODY class="mainFondoInterno">
<form name="renuncia">
<br>
<FIELDSET>
<LEGEND><a class="texto"><b>RENUNCIA</b></a></LEGEND>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
<%
    String nombre_oficina,estado,nif,letra="",dni="",nss;
    if (vg.size()>0){
       sesGar = (SesGarantia)vg.elementAt(0);
       GaranTram gT = new GaranTram();
       Vector vgt = new Vector();
       vgt = gT.busquedaGaranTram("GAR_PK="+sesGar.getGarPk(),"");
       SesGaranTram sesGT = (SesGaranTram)vgt.elementAt(0);
       oficina_pk = sesGT.getOficinaPk();
       nombre_oficina=OficinaHelper.DameOficina(oficina_pk);
       estado = Constantes.EST_TEXTO[new Long(sesGar.getGarEstado()).intValue()-1];
       nif=sesGT.getGarReplegDni();
       nss=sesGar.getPacNss();
       if ((nif!=null) && (nif!="")){
         letra=nif.substring(nif.length()-1);
         dni=nif.substring(0,nif.length()-1);
       }
%>

   <tr>
       <td class= "texto" width="20%">Oficina:</td>
       <td class= "texto" width="20%"><input type="text" name="Oficina" class="cajatexto2" value="<%=nombre_oficina%>" readonly></td>
       <td class= "texto">Fecha:</td>
       <td class= "texto"><input type="text" name="fecha" class="cajatexto2" value="<%=Utilidades.dateToString(sesGT.getGarFechor())%>" readonly ></td>
   </tr>
   <tr>
       <td class= "texto">Responsable Legal:</td>
       <td class= "texto" COLSPAN=4><input type="text" name="nombre" class="cajatexto3" value="<%=sesGT.getGarReplegApenom()%>" readonly></td>
   </tr>
   <td class= "texto">N.I.F:</td>
         <td class= "texto"><input maxlength=9 type="text" name="dni"  style="width:40%"class="cajatexto2"  value="<%=dni%>" readonly>&nbsp;
         - <input maxlength=1 type="text" name="letradni" style="width:8%" class="cajatexto2" value="<%=letra%>" readonly></td>
   <tr>
       <td class= "texto">Estado:</td>
       <td class= "texto"><input type="text" name="estado" class="cajatexto2" value="<%=estado%>" readonly></td>
   </tr>
   <tr>
       <td class= "texto">N.S.S.:</td>
       <td class= "texto"><input type="text" name="nss" class="cajatexto2" value="<%=nss%>" readonly></td>
   </tr>

<%
  if (sesGar.getGarCq().equals("Q")){
%>
<td><input type="hidden" name="centro" value="<%=new Double(sesGar.getCentro()).longValue()%>"></td>
<%
  }else{
%>
<td><input type="hidden" name="centro" value="<%=new Double(sesGar.getCega()).longValue()%>"></td>
<%
  }
%>
<td><input type="hidden" name="norden" value="<%=new Double(sesGar.getNorden()).longValue()%>"></td>
<td><input type="hidden" name="ncita" value="<%=new Double(sesGar.getNcita()).longValue()%>"></td>
<td><input type="hidden" name="gar_cq" value="<%=sesGar.getGarCq()%>"></td>
<td><input type="hidden" name="garpk" value="<%=sesGT.getGarPk()%>"></td>
<td><input type="hidden" name="op" value="<%=sesGT.getGarOperacion()%>"></td>
<%
     }else{
        System.out.println("El vector de garantias es nulo");
     }
%>
</table>
<br>
</FIELDSET>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
