<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_LEQ";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>
<script language="javascript" type="text/javascript" src="../../../javascript/validarNif.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/validarFecha.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/AnchorPosition.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/botones.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/CalendarPopup.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/PopupWindow.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/validacion_campos_fecha.js"></script>
<script language="javascript">
 function chequeoFecha (){
    if ((document.formulario.fechanac.value!=null)&&(document.formulario.fechanac.value!="")){
      if (!esStringFecha(document.formulario.fechanac.value)){
         alert('Formato de fecha incorrecto (dd/mm/aaaa)');
         document.formulario.fechanac.focus();
      }else{
         var dia = getDia(document.formulario.fechanac.value);
         var mes = getMes(document.formulario.fechanac.value);
         var anno = getAnno(document.formulario.fechanac.value);
         if (validacion_fecha(dia,mes,anno)) document.formulario.telefono.focus();
         else alert ("Fecha no valida");
      }
    }
  }
  
 function Deshabitilar_textarea(){
     document.formulario.motivo.disabled=false;
     document.formulario.motivo_libre.disabled=true; 
 }
 
 function Deshabitilar_select(){
     document.formulario.motivo_libre.disabled=false;
     document.formulario.motivo.disabled=true; 
 }
 
</script>
<%
    Calendar Cal_fechaActual = Calendar.getInstance();
    String motivo1="No se encuentra en Registro de Pacientes de LE.";
    String motivo1_valor="No se encuentra dado de alta en el Registro de Pacientes en Lista de Espera.";
    String motivo2="La prestaci�n es de control o sucesiva y por lo tanto no garantizada.";
    String motivo2_valor="La prestaci�n por la que se solicita el documento acreditativo, es de control o sucesiva y por lo tanto no garantizada.";
%>

<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="formulario" action="guardarnolesp.jsp" target="oculto">
  <table width="97%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="center">
   <tr>
    <td>
     <FIELDSET >
     <LEGEND><a class="texto"><b>DATOS PERSONALES</b></a></LEGEND>
      <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
       <tr>
        <td width="80%">
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td class= "texto" width="12%">*Nombre:</td>
           <td class= "texto" width="22%"><input type="text" name="nombre" class="cajatexto2" value="" style="width:90%"></td>
           <td class= "texto" width="10%">*Apel. 1:</td>
           <td class= "texto" width="22%"><input type="text" name="apellido1" class="cajatexto3" value="" style="width:95%"></td>
           <td class= "texto" width="10%">*Apel. 2:</td>
           <td class= "texto" width="24%"><input type="text" name="apellido2" class="cajatexto3" value="" style="width:95%"></td>
          </tr>
         </table>
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td class= "texto" width="12%">Direccion:</td>
           <td class= "texto" width="40%"><input type="text" name="direccion" class="cajatexto3" value="" style="width:90%"></td>
           <td class= "texto" width="15%">*Poblacion:</td>
           <td class= "texto" width="33%"><div id="divLocalidades">
             <Select name="poblacion" class="cajatexto3" value="" style="width:95%">
             <%
             Localidades loc = new Localidades();
             Vector vl = new Vector();
             /*if (pk_prov_sel>0) {
                vl = loc.busquedaLoc("PROVINCIA_PK="+pk_prov_sel,"LOCALIDAD ASC");
             }else{*/
                vl = loc.busquedaLoc("","LOCALIDAD ASC");
             //}
             for(int int_pos = 0;int_pos < vl.size(); int_pos++){
                SesLocalidades sesLoc=(SesLocalidades)vl.elementAt(int_pos);
             %>
                   <option default value='<%= sesLoc.getPkLocalidad()%>'><%=sesLoc.getLocalidad() %></option>
             <%
             }
             %>
             </Select></div>
           </td>
          </tr>
         </table>
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td class= "texto" width="12%">Fecha Nac.:</td>
           <td class= "texto" width="40%">
                  <INPUT type="text" class="cajaTextoFecha" name="fechanac" value ="" onKeypress="validarEntradaFecha(this.value);" onBlur="chequeoFecha();"><font class="text">(dd/mm/aaaa)</font>
           </td>
           <td class= "texto" width="15%">Telefono:</td>
           <td class= "texto" width="33%"><input type="text" name="telefono" class="cajatexto2" value="" style="width:50%"></td>
          </tr>
         </table>
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td class= "texto" width="12%">*C.I.P.:</td>
           <td class= "texto" width="32%"><input type="text" name="cip" class="cajatexto2" value=""></td>
           <td class= "texto" width="12%">N.S.S.:</td>
           <td class= "texto" width="44%"><input type="text" name="nss" class="cajatexto2" value=""></td>
          </tr>
         </table>
         <br>
        </td>
       </tr>
      </table>
     </FIELDSET>
     <br>
     <FIELDSET >
     <LEGEND><a class="texto"><b>DATOS CLINICOS</b></a></LEGEND>
      <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
       <tr>
        <td width="80%">
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td class= "texto" width="12%">*Centro:</td>
           <td class= "texto" width="60%"><div id="centros">
            <Select name="centro" class="cajatexto2" value="" style="width:90%">
            <%
              Centros c = new Centros();
              Vector v = new Vector();
              v = c.busquedaCentro("","DESCENTRO ASC");
              for (int i=0;i<v.size();i++){
                Centrosescam cs = (Centrosescam)v.elementAt(i);
            %>
             <option default value='<%=cs.getCodcentro()%>'><%=cs.getDescentrolarga()%></option>
            <%
              }
            %>
            </div></td>
           <td class= "texto" width="13%">N� Historia:</td>
           <td class= "texto" width="15%"><input type="text" name="nhc" class="cajatexto3" value="" style="width:95%" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;"></td>
          </tr>
         </table>
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td class= "texto" width="12%">Prestacion:</td>
           <td class= "texto" width="88%"><input type="text" name="prestacion" class="cajatexto2" value="" style="width:80%"></td>
          </tr>
         </table>
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td width="12%" class= "texto">Motivo Deneg:</td>
           <td class="texto" width="20%">
	           <label for="seleccion" title="Elija motivo de denegacion">
	            <input type="radio" checked name="type" value="1" id="sel" onclick="Deshabitilar_textarea();" />Seleccion
	           </label>
	       </td>
           <td class="texto" width="68%"><div id="motivos">
	           <select name="motivo" class="cajatexto2" value="" style="width:100%" >
	           	<option default value='<%=motivo1_valor%>'><%=motivo1%></option>
	           	<option value='<%=motivo2_valor%>'><%=motivo2%></option>	           	
	           </div></td>	
	      </tr>
	      <tr>
		   <td width="12%" class="texto">&nbsp;</td>	     
   	       <td class="texto" width="20%">
    	       <label for="libre" title="Introduzca motivo denegacion">
	            <input type="radio" name="type" value="0" id="lib" onclick="Deshabitilar_select();" />Texto Libre
	           </label>
	       </td>
   	       <td class="texto" width="68%"><textarea name="motivo_libre" disabled class="textarea" rows="2" maxlength = 4000 value=""></textarea></td>
	      </tr>
         </table>
         <br>
        </td>
       </tr>
      </table>
     </FIELDSET>
     <br>
     <FIELDSET >
     <LEGEND><a class="texto"><b>DATOS RESPONSABLE</b></a></LEGEND>
      <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
       <tr>
        <td width="80%">
         <table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
          <tr>
           <td class= "texto" width="12%">Nombre y Apellidos:</td>
           <td class= "texto" width="53%"><input type="text" name="nomape" class="cajatexto2" value="" style="width:90%"></td>
           <td class= "texto" width="10%">N.I.F.:</td>
           <td class= "texto" width="25%"><input maxlength=8 type="text" name="dni"  style="width:50%"class="cajatexto2"  value="" onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46)) event.returnValue = false;" onBlur="CalculaNIF(this.value);" >&nbsp;
         - <input maxlength=1 type="text" name="letradni" style="width:10%" class="cajatexto2" value="" ></td>
          </tr>
         </table>
        </td>
       </tr>
      </table>
     </FIELDSET>
    </td>
   </tr>
  </table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
