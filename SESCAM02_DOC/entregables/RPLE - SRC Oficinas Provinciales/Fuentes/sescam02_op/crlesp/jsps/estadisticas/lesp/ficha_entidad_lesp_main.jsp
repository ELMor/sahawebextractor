<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="java.util.*,beans.*,isf.negocio.*,isf.util.Utilidades" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Estad";%>
<%@ include file="/jsps/control_acceso.txt" %>

<jsp:useBean id="datosEstadisticasBean" scope="session" class="java.util.Properties"/>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../javascript/SelecUtil.js"></script>
</head>
<script language="javascript">
</script>
<BODY class="mainFondo">
<%
   String str_criterio = (String) request.getParameter("criterio");
   String str_id_entidad = (String) request.getParameter("idEntidad");


   Vector v_estad = (Vector) datosEstadisticasBean.get("v_estadistica");
	 Vector v_tramos = (Vector)		datosEstadisticasBean.get("v_tramos");

	 if (v_estad != null && v_tramos != null && str_criterio != null  && str_id_entidad != null) {
		 int idEntidad = (new Integer(str_id_entidad)).intValue();
		 DatosEstadisticasLEQ EstEntidad = (DatosEstadisticasLEQ) v_estad.elementAt(idEntidad);

%>
<table class="cabeceraTabla"  border="0" cellspacing="0" cellpadding="0" align="center">
    <tr class="filaCabecera">
    <td align="left" width="30%"><a href="#" class="menuOpcion1"><%=str_criterio%>:</a></td>
    <td align="left" width="70%" ><a href="#" class="menuOpcion1"><%=EstEntidad.DatoGroupby.trim()%></a></td>
</table>
<table class="centroTabla"  border="0" cellspacing="0" cellpadding="0" align="center">
<!-- información de pacientes activos en LE -->
   <tr class="filaImpar">
	 <td align="left" width="30%"><b>Pacientes LE</b></td>
	 <td align="left" width="30%"><%=Utilidades.cambiarFormato(new Long(EstEntidad.pacientes).toString(),",",".",0)%></td>
 	 <td align="left" width="30%"><b>D.M. :</b> (<%=Utilidades.cambiarFormato(new Double(Utilidades.redondea(EstEntidad.demoramedia,2)).toString(),".",",",2)%>)</td>
   </tr>
<!-- información de pacientes activos en LE por tramos -->
<%
	 for (int int_pos = 0; int_pos < EstEntidad.tramos.length; int_pos++) {
%>
   <tr class="filaPar" border="0">
  	 <td align="left" width="30%" ></td>
    <td align="left" width="30%"><b><%=v_tramos.elementAt(int_pos)%></b></td>
    <td align="left" width="30%%"><%=Utilidades.cambiarFormato(new Long(EstEntidad.tramos[int_pos]).toString(),",",".",0)%></td>
	 </tr>
<% } %>

<!-- información de pacientes pendientes en LE -->
   <tr class="filaImpar">
	 <td align="left" width="30%"><b>Pendientes</b></td>
	 <td align="left" width="30%"><%=Utilidades.cambiarFormato(new Long(EstEntidad.pendientes).toString(),",",".",0)%></td>
 	 <td align="left" width="30%"><b>D.M. :</b> (<%=Utilidades.cambiarFormato(new Double(Utilidades.redondea(EstEntidad.demmediapendientes,2)).toString(),".",",",2)%>)</td>
   </tr>
<!-- información de pacientes pendientes por situaciones -->
   <tr class="filaPar" border="0">
  	 <td align="left" width="30%"></td>
    <td align="left" width="30%"><b><%=ConsultaEntradasLE.STR_SITUACION[1]%></b></td>
    <td align="left" width="30%%"><%=Utilidades.cambiarFormato(new Long(EstEntidad.pacientes).toString(),",",".",0)%></td>
	 </tr>
<%
	 for (int int_pos = 0; int_pos < EstEntidad.situaciones.length; int_pos++) {
%>
   <tr class="filaPar" border="0">
  	 <td align="left" width="30%" ></td>
    <td align="left" width="30%"><b><%=ConsultaEntradasLE.STR_SITUACION[int_pos+2]%></b></td>
    <td align="left" width="30%"><%=Utilidades.cambiarFormato(new Long(EstEntidad.situaciones[int_pos]).toString(),",",".",0)%></td>
	 </tr>
<% } %>


</table>
<%
	 }
%>

</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
