<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaEstadisticaLEQ" %>
<%@ page import="isf.exceptions.ExceptionSESCAM" %>
<%@ page import="isf.util.Utilidades" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Estad";%>
<%@ include file="/jsps/control_acceso.txt" %>

<jsp:useBean id="datosEstadisticasBean" scope="session" class="java.util.Properties"/>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<%
String periodo = "";
int anchura = 0;
int anchuracentro = 0;
int ntramos= 0;
 if (request.getParameter("periodo")!=null) {
   periodo = request.getParameter("periodo");
   anchura = new Integer(request.getParameter("anchura")).intValue();
   ntramos = new Integer(request.getParameter("tramos")).intValue();
   anchuracentro = anchura*2;
 }
%>

<form name="consulta">
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
    <table style="width:100%;" align="left" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral2" >
	<table class="centroTabla" id="tablaEst" cellspacing="0" cellpadding="0" border="0">
<%
		DatosEstadisticasLEQ EstDiag = null;
		Vector v=new Vector();
		boolean bFilaPar=true;
		int idCounter=0;
                ConsultaEstadisticaLEQ cestadisticadiag =new ConsultaEstadisticaLEQ();
                try {
                    cestadisticadiag.setPeriodo(periodo);
                    cestadisticadiag.setTramos(ntramos);
                    v=cestadisticadiag.ConsultaTramoDiagnostico();
                  }catch (ExceptionSESCAM eSES){
%>
                    <script language="javascript">
                      alert("<%=eSES%>");
                    </script>
<%
                 }
                 if (v != null) {
 %>
                 <Script language="javaScript">
                   parent.pie.location="../../pie_buscador.jsp?estado=Generando Resultado...";
                 </script>
  <%
      	         // colocar el resultado de la consulta en el bean.
 	  	           datosEstadisticasBean.put("v_estadistica",v);


                  if (v.size() > 0){

			int[] totalTramos = new int[ntramos];
			int totalPacientes = 0, totalPendientes = 0;
			double totalMedia = 0.0, totalMediaPdtes = 0.0;


	  		Enumeration e = v.elements();
	    		while(e.hasMoreElements()) {
	      			EstDiag=(DatosEstadisticasLEQ)e.nextElement();
	      			if (bFilaPar){
	      			   	bFilaPar=false;
%>
					<tr class="filaPar" id=<%=idCounter%> style="cursor:hand;">
<%       			}else{
					bFilaPar=true;
%>
					<tr class="filaImpar" id=<%=idCounter%>  style="cursor:hand;">
<%
				}
%>
                                <td width="<%=anchuracentro%>%"  height="12px" name="diag">
					<a onclick="window.showModalDialog('ficha_entidad_lesp_index.jsp?criterio=Diagnůstico&idEntidad=<%=idCounter%>','','dialogWidth:20em;dialogHeight:17em;status=no');" ><%=EstDiag.DatoGroupby%></a></td>
<%
                                for (int int_pos=0;int_pos<ntramos;int_pos++) {
																	totalTramos[int_pos] += EstDiag.tramos[int_pos];
%>
                                  <td width="<%=anchura%>%"  height="12px" name="tramo"<%=int_pos+1%>><%=Utilidades.cambiarFormato(new Long(EstDiag.tramos[int_pos]).toString(),",",".",0)%></td>
<%
                                }
%>
                                <td width="<%=anchura%>%"  height="12px" name="pacientes"><%=Utilidades.cambiarFormato(new Long(EstDiag.pacientes).toString(),",",".",0)%></td>
                                <td width="<%=anchura%>%"  height="12px" name="media"><%=Utilidades.cambiarFormato(new Double(Utilidades.redondea(EstDiag.demoramedia,2)).toString(),".",",",2)%></td>
                                <td width="<%=anchura%>%"  height="12px" name="pacientesPdtes"><%=Utilidades.cambiarFormato(new Long(EstDiag.pendientes).toString(),",",".",0)%></td>
                                <td width="<%=anchura%>%"  height="12px" name="mediaPdtes"><%=Utilidades.cambiarFormato(new Double(Utilidades.redondea(EstDiag.demmediapendientes,2)).toString(),".",",",2)%></td>
                             </tr>

<%
           	totalPacientes += EstDiag.pacientes;
            totalMedia += EstDiag.demoramedia;
						totalPendientes += EstDiag.pendientes;
		  			totalMediaPdtes += EstDiag.demmediapendientes;
  				  idCounter++;
  				}
					// mostrar los resultados totales.
                         if (idCounter > 0) {
                         %>
                             <tr class="filaTotal" id=<%=idCounter%>  style="cursor:hand;">
                             <td width="<%=anchuracentro%>%"  height="12px" name="centro"><b>Total</b></td>
                           <%
                              for (int int_pos=0;int_pos<ntramos;int_pos++) {
                           %>
                                <td width="<%=anchura%>%"  height="12px" name="tramo"<%=int_pos+1%>><%=Utilidades.cambiarFormato(new Long(totalTramos[int_pos]).toString(),",",".",0)%></td>
                           <% } %>
					                    <td width="<%=anchura%>%"  height="12px" name="pacientes"><%=Utilidades.cambiarFormato(new Long(totalPacientes).toString(),",",".",0)%></td>
                              <td width="<%=anchura%>%"  height="12px" name="media"><%=Utilidades.cambiarFormato(new Double(Utilidades.redondea(totalMedia/idCounter,2)).toString(),".",",",2)%></td>
                              <td width="<%=anchura%>%"  height="12px" name="pacientesPdtes"><%=Utilidades.cambiarFormato(new Long(totalPendientes).toString(),",",".",0)%></td>
                              <td width="<%=anchura%>%"  height="12px" name="mediaPdtes"><%=Utilidades.cambiarFormato(new Double(Utilidades.redondea(totalMediaPdtes/idCounter,2)).toString(),".",",",2)%></td>
                             </tr>
                           <% } %>
                           <script language="javascript">
                              parent.pie.location="../../pie_buscador.jsp?estado=Listo&filas=<%=idCounter%>";
                           </script>
<%
                        }else{
%>
                           <Script language="javaScript">
                              parent.pie.location="../../pie_buscador.jsp?estado=No se han encontrado filas.";
                           </script>
<%			}
                 }
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
