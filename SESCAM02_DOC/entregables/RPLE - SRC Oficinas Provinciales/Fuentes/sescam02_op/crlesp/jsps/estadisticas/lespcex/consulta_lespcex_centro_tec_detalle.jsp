<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaEstadisticaLECEX" %>
<%@ page import="isf.exceptions.ExceptionSESCAM" %>
<%@ page import="isf.util.Utilidades" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Estad";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="consulta">
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
    <table style="width:100%;" align="left" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral2" >
	<table class="centroTabla" id="tablaEst" cellspacing="0" cellpadding="0" border="0">
<%
		Vector v=new Vector();
		boolean bFilaPar=true;
		int idCounter=0;
                ConsultaEstadisticaLECEX cestadisticacentrotec =new ConsultaEstadisticaLECEX();
                try {
                    v=cestadisticacentrotec.ConsultaTramoCentroTecnicas();
                  }catch (ExceptionSESCAM eSES){
%>
                    <script language="javascript">
                      alert("<%=eSES%>");
                    </script>
<%
                 }
%>
		<script language="javascript">
		  parent.pie.document.pieEtado.lEstado.value="Generando resultado...";
		</script>
<%
		if (v.size()>0){
	  		Enumeration e = v.elements();
                        DatosEstadisticasLECEX EstCentroTec = new DatosEstadisticasLECEX();
	    		while(e.hasMoreElements()) {
	      			EstCentroTec=(DatosEstadisticasLECEX)e.nextElement();
	      			if (bFilaPar){
	      			   	bFilaPar=false;
%>
					<tr class="filaPar" id=<%=idCounter%> style="cursor:hand;">
<%       			}else{
					bFilaPar=true;
%>
					<tr class="filaImpar" id=<%=idCounter%>  style="cursor:hand;">
<%
				}
%>
                                <td width="20%"  height="12px" name="centro"><%=EstCentroTec.DatoGroupby%></td>
				<td width="10%"  height="12px" name="tramo1"><%=EstCentroTec.tramo1%></td>
				<td width="10%"  height="12px" name="tramo2"><%=EstCentroTec.tramo2%></td>
				<td width="10%" height="12px" name="tramo3"><%=EstCentroTec.tramo3%></td>
                                <td width="10%" height="12px" name="tramo3"><%=EstCentroTec.tramo4%></td>
                                <td width="10%" height="12px" name="tramo3"><%=EstCentroTec.tramo5%></td>
                                <td width="10%"  height="12px" name="pacientes"><%=EstCentroTec.pacientes%></td>
                                <td width="10%"  height="12px" name="pendientes"><%=0%></td>
                                <td width="10%"  height="12px" name="media"><%=Utilidades.redondea(EstCentroTec.demoramedia,2)%></td>
                             </tr>

<%
  				idCounter++;
  				}
                                %>
                                <script language="javascript">
                                     parent.pie.document.pieEtado.lEstado.value="Listo";
                                     parent.pie.document.pieEtado.lFilas.value=<%=idCounter%>;
                                </script>
<%
			}else{
%>
				<Script language="javaScript">
                                  parent.pie.document.pieEtado.lEstado.value="Ingrese condiciones de busqueda";
                                  parent.pie.document.pieEtado.lFilas.value="0";
				</Script>
<%			}
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
