<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>

<style>
input {background-color: transparent}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanOficina" class="java.util.HashMap" scope="session"/>
</head>
<script language="javascript">

function recargar(){
  this.document.formulario.action="mant_ofi_modificar_datos_oficina.jsp";
  this.document.formulario.target="centro";
  this.document.formulario.submit();
  this.document.formulario.action="modificarOficina.jsp";
  this.document.formulario. target="oculto1";

}

var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(pk,dato_origen,destino){

  parent.centro.location = "../usuarios/mant_usu_modificar_datos_usuario.jsp?login="+pk;
  parent.botonera.location.href = "../usuarios/mant_usu_botones_modificar.jsp?origen="+dato_origen +"&pk_destino="+destino;

}


</script>

<%
  SesOficina sesOfi;
  SesLocalidades sesLoc;
  SysUsu sys_usu;
  String nombre,dir;
  long telef,cp,pk_prov_sel;
  long l_provinciapk=0;
  long l_reqpk=0;
  long l_localidadpk=0;
  String s_login_usu;
  if (request.getParameter("ofipk")!=null){
    beanOficina.clear();
    Oficina ofi= new Oficina();
    Localidades local= new Localidades();
    Vector vec = new Vector();
    Vector vl = new Vector();
    //tomo la Oficina pasada por parametro
    l_reqpk = new Long(request.getParameter("ofipk")).longValue();
    vec = ofi.busquedaOfi("OFICINA_PK="+l_reqpk,"");
    sesOfi=(SesOficina)vec.elementAt(0);
    //tomo la localidad de la oficina
    l_localidadpk = sesOfi.getPkLocalidad();
    //System.out.println("LOCALIDAD PK:"+l_localidadpk);
    //busco la localidad de la oficina para obtener el pk de la provincia.
    vl = local.busquedaLoc("PK_LOCALIDAD="+l_localidadpk,"");
    sesLoc=(SesLocalidades)vl.elementAt(0);
    l_provinciapk= sesLoc.getProvinciaPk();
    //System.out.println("PROVINCIA PK:"+l_provinciapk);

    s_login_usu = sesOfi.getSyslogin();
    beanOficina.put("LOCALIDAD",sesLoc);
    beanOficina.put("OFICINA",sesOfi);
    //System.out.print(beanOficina);
  }else{
    sesOfi=(SesOficina)beanOficina.get("OFICINA");
    l_reqpk = sesOfi.getOficinaPk();
    s_login_usu = sesOfi.getSyslogin();
    //sesLoc=(SesLocalidades)beanOficina.get("LOCALIDADES");

    l_localidadpk=sesOfi.getPkLocalidad();
  }


  if ((request.getParameter("nombre")!=null)&& (!request.getParameter("nombre").equals(""))){
       nombre = request.getParameter("nombre");
  }else{
       nombre = "";
  }

  if  ((request.getParameter("direccion")!=null) && (!request.getParameter("direccion").equals(""))){
      dir = request.getParameter("direccion");
  }else{
     dir = "";
  }

  if  ((request.getParameter("telefono")!=null) && (!request.getParameter("telefono").equals(""))){
     telef = new Long(request.getParameter("telefono")).longValue();
  }else{
     telef = 0;
  }

  if ((request.getParameter("codigopostal")!=null) && (!request.getParameter("codigopostal").equals(""))){
   cp = new Long(request.getParameter("codigopostal")).longValue();
   }else{
   cp = 0;
  }

  if  ((request.getParameter("provincias")!=null) && (!request.getParameter("provincias").equals(""))){
       pk_prov_sel = new Long(request.getParameter("provincias")).longValue();
       }else{
       pk_prov_sel = l_provinciapk;
  }
%>

<BODY class="mainFondo">
<TABLE width="100%" class="textoTop">
  <tr>
           <td bgcolor=#5C5CAD> Modificar Oficina </td>
  </tr>
</table>
<form name="formulario" action="modificarOficina.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
         <td></td>
  </tr>
  <tr>
    <td width="15%" class= "texto">Oficina: </td>
    <%
    if (nombre==""){
    %>
        <td width="30%" class= "normal"><input type="text" name="nombre" class="cajatexto2" maxlength = 20 value="<%=sesOfi.getOficinaNombre()%>"></td>
    <%
    }else{
    %>
        <td width="30%" class= "normal"><input type="text" name="nombre" class="cajatexto2" maxlength = 20 value="<%=nombre%>"></td>
    <%
    }
    %>
    <td width="15%"></td>
    <td width="35%"></td>
  </tr>
  <tr>
  </tr>
  <tr>
    <td class= "texto">Direcci&oacute;n: </td>
<%
    if (dir==""){
%>
    <td class= "normal" colspan=3><input type="text" name="direccion" class="cajatexto3" maxlength = 30 value="<%=sesOfi.getOficinaDireccion()%>" ></td>
<%
    }else{
%>
    <td class= "normal" colspan=3><input type="text" name="direccion" class="cajatexto3" maxlength = 30 value="<%=dir%>" ></td>
<%
    }
%>
  </tr>
  <tr>
    <td class= "texto">Telefono: </td>
<%
    if (telef==0){
      if (sesOfi.getOficinaTelefono()==0){
%>
            <td class= "normal"><input type="text" name="telefono" class="cajatexto4" value="" maxlength=9 onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57))) event.returnValue = false;"></td>
<%
      }else{
%>
            <td class= "normal"><input type="text" name="telefono" class="cajatexto4" value="<%=sesOfi.getOficinaTelefono()%>" maxlength=9 onkeypress = "if (((event.keyCode < 48) || (event.keyCode > 57))) event.returnValue = false;"></td>
<%
      }
    }else{
%>
    <td class= "normal"><input type="text" name="telefono" class="cajatexto4" value="<%=telef%>" maxlength=9></td>
<%
    }
%>

    <td class= "texto">Codigo Postal: </td>
<%
    if (cp==0){
      if (sesOfi.getOficinaCodpostal()==0){
%>
    <td class= "normal"><input type="text" name="codigopostal" class="cajatexto4" value="" maxlength=5></td>
<%
    }else{
%>
    <td class= "normal"><input type="text" name="codigopostal" class="cajatexto4" value="<%=sesOfi.getOficinaCodpostal()%>" maxlength=5></td>
<%
    }
    }else{
%>
    <td class= "normal"><input type="text" name="codigopostal" class="cajatexto4" value="<%=cp%>" maxlength=5></td>
<%
    }
%>

  </tr>
  <tr>
    <td class= "texto">Provincia: </td>
    <td class= "normal"><div id="divProvincias">
           <Select name="provincias" class="cajatexto2" onChange="recargar();">
           <%
           Provincias prov = new Provincias();
           Vector vp = new Vector();
           vp = prov.busquedaProv("","PROVINCIA ASC");
           for(int int_pos = 0;int_pos < vp.size(); int_pos++){
                SesProvincias sesProv=(SesProvincias)vp.elementAt(int_pos);
                if ((pk_prov_sel>0) && (pk_prov_sel==sesProv.getProvinciaPk())){
            %>
                   <option selected value='<%= sesProv.getProvinciaPk()%>'><%=sesProv.getProvincia() %></option>
            <%
                }else{
           %>
                   <option value='<%= sesProv.getProvinciaPk()%>'><%=sesProv.getProvincia() %></option>
            <%
                }
           }
            %>
           </Select></div>
    </td>
    <td class= "texto">Localidad: </td>
    <td class= "normal"><div id="divLocalidades">
           <Select name="localidades" class="cajatexto2">
           <%
           Localidades loc = new Localidades();
           Vector vl = new Vector();
           vl = loc.busquedaLoc("PROVINCIA_PK="+pk_prov_sel,"LOCALIDAD ASC");
           for(int int_pos = 0;int_pos < vl.size(); int_pos++){
                sesLoc=(SesLocalidades)vl.elementAt(int_pos);
                if ((sesLoc.getPkLocalidad()==l_localidadpk)){
           %>
                   <option selected value='<%= sesLoc.getPkLocalidad()%>'><%=sesLoc.getLocalidad() %></option>
           <%
                }else{
           %>
                   <option value='<%= sesLoc.getPkLocalidad()%>'><%=sesLoc.getLocalidad() %></option>
           <%
                }
           }
           %>
           </Select></div>
    </td>
  </tr>
  <tr>
  <td class= "texto">Responsable: </td>
    <td class= "normal"><div id="divresponsable">
           <Select name="responsable" class="cajatexto2" >
           <option selected value="">No disponible</option>
           <%
           Usuarios usu_res = new Usuarios();
           Vector v2 = new Vector();
           v2 = usu_res.busquedaUsu("SYS_CARGOS_PK = 1","APELLIDO1 ASC");

           for(int int_pos = 0;int_pos < v2.size(); int_pos++){
                sys_usu=(SysUsu)v2.elementAt(int_pos);

                if (sys_usu.getSyslogin().equals(s_login_usu)){
           %>
                   <option selected value="<%= sys_usu.getSyslogin()%>"><%=sys_usu.getNombre()%> <%=sys_usu.getApellido1()%></option>
           <%
                }else{
           %>
                   <option value="<%=sys_usu.getSyslogin()%>"><%=sys_usu.getNombre()%> <%=sys_usu.getApellido1()%></option>
           <%

                }
           }
           %>
           </Select></div>
    </td>
  </tr>
</table>
</form>
<form name="seleccion">
<table width="100%" class = "textoTop" border = "0">
  <tr>
    <td bgcolor=#5C5CAD>Usuarios Asociados </td>
  </tr>
</table>
<br>
<table class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="1" width="100%">
        <tr class="filaCabecera">
            <td width="25%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Nombre</a>
             </td>
            <td width="25%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Apellido 1</a>
             </td>
            <td width="25%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Apellido 2</a>
             </td>
            <td width="15%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Estado</a>
             </td>
        </tr>
</table>
     <table height="100%" width="100%" align="center" border="1" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral" >
       <table class="centroTabla" width="100%" id="tablaUsu" cellspacing="0" cellpadding="0" border="1">
       <%
         boolean bFilaPar=true;
         int idCounter=0;
         String s;
         Usuarios usu = new Usuarios();
         Vector v = new Vector();
         v = usu.busquedaUsu("OFICINA_PK =" +l_reqpk,"");
         if (v!=null){
                 for(int int_pos = 0;int_pos < v.size(); int_pos++){
                         SysUsu sysusu=(SysUsu)v.elementAt(int_pos);
                         String login_usu = sysusu.getSyslogin();
                         if (sysusu.getActivoSn()==1) {
                          s = "Alta";
                         }else { s = "Baja"; }
                         if (bFilaPar){
                                          bFilaPar=false;
 %>
                                 <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaUsu'].rows, this.id);selec('<%=login_usu%>',2,<%=l_reqpk%>);" style="cursor:hand;">
 <%				}else{
                                         bFilaPar=true;
 %>
                                 <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaUsu'].rows, this.id);selec('<%=login_usu%>',2,<%=l_reqpk%>);" style="cursor:hand;">
 <%
                         }
 %>
                          <td width="24%" height="12px"><%=sysusu.getNombre()%></td>
                          <td width="25%" height="12px"><%=sysusu.getApellido1()%></td>
                          <td width="25%" height="12px"><%=sysusu.getApellido2()%></td>
                          <td width="13%" height="12px"><%=s%></td>

                    </tr>
<%
                           idCounter++;
                   }
         }
%>
        </table>
        </td>
      </tr>
    </table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>



