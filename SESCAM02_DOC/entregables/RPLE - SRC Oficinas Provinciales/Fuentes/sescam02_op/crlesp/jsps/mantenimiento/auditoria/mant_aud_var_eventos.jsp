<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.VarTipoEvento,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<!-- <jsp:useBean id="beanEventoTipo" class="java.util.HashMap" scope="session"/> -->
<script language="javascript">
function cambia() {
  this.document.seleccion_var.modificado.value = "true"
}
</script>
</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<%
  long evto_var_pk = 0;
//  beanEventoTipo.clear();
  String marcar = "",estado = "";
  boolean marcar_bd = true;
  boolean lb_modificar = false;
  if (request.getParameter("marcar_todas") != null) {
   marcar = request.getParameter("marcar_todas");
   if (marcar.equals("nochecked")) {
     marcar = "";
   }
   marcar_bd = false;
   lb_modificar = true;
  }

  if (request.getParameter("evtopk") != null) {
    evto_var_pk = new Long(request.getParameter("evtopk")).longValue();
  }
  if (request.getParameter("estado") != null) {
   estado   =  request.getParameter("estado");
  }


%>
<form name="seleccion_var" action = "modificarVarEventos.jsp" target = "oculto1">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
		<%
		boolean bFilaPar=true;
		int idCounter=0;

                VarTipoEvento varevento= new VarTipoEvento();
                long TpoEvto = 0;
                Vector vec = new Vector();
                vec = varevento.busquedaVarTipoEvento("TPOEVTO_PK ="+evto_var_pk,"");
		if (vec!=null){
			for(int int_pos = 0;int_pos < vec.size(); int_pos++){
                          	idCounter++;
				EventoTipoVar evtotipovar =(EventoTipoVar)vec.elementAt(int_pos);
                            //    beanEventoTipo.put("VAR"+int_pos,evtotipovar);
				long evto_var = evtotipovar.getTpoevtoVarPk();
                                if (marcar_bd == true) {
                                 if (evtotipovar.getTpoevtoVarAct() == 1) {
                                    marcar = "CHECKED";
                                 }
                                 else {
                                    marcar = "";
                                 }
                                }
				if (bFilaPar){
	      			   	bFilaPar=false;
	%>
					<tr class="filaPar" id=<%=idCounter%> style="cursor:hand;">
	<%				}else{
						bFilaPar=true;
	%>
						<tr class="filaImpar" id=<%=idCounter%>  style="cursor:hand;">
	<%
				}
	%>
                                <td width="20%" height="12px"><%=evto_var%></td>
                                <td width="65%" height="12px"><%=evtotipovar.getTpoevtoVarDesc()%></td>
                                <td width="15%" height="12px"><input type = "checkbox" name="estado<%=idCounter%>" <%=marcar%> <%=estado%> onclick="cambia();"></td>


	   		</tr>
<%

  			}
%>
                  <script language="javascript">
                      if (<%=marcar_bd%> == true) {
                        parent.pie_var_eventos.location = "mant_aud_pie_var_eventos.jsp?estado=<%=estado%>&marcar=<%=marcar_bd%>&pie_estado=Listo&filas=<%=vec.size()%>";
                      }else {
                        parent.pie_var_eventos.location = "mant_aud_pie_var_eventos.jsp?estado=<%=estado%>&marcar=<%=marcar%>&pie_estado=Listo&filas =<%=vec.size()%>";
                      }


 /*                   if (parent.pie_var_eventos.document.readyState=="complete") {
                      parent.pie_var_eventos.document.pieEtado.estado.value="<%=estado%>";
                      parent.pie_var_eventos.document.pieEtado.marcar.value="<%=marcar%>";

                      parent.pie_var_eventos.document.pieEtado.submit();
 */
                      if (<%=marcar_bd%> == true) {
                 //       parent.pie_var_eventos.document.pieEtado.marcar.checked = false;
                      }
/*                      parent.pie_var_eventos.document.pieEtado.lEstado.value="Listo";
                      parent.pie_var_eventos.document.pieEtado.lFilas.value=<%=vec.size()%>;*/
                  </script>
<%
		}
%>
        <td><input type="hidden" name="codigo" value = <%=evto_var_pk%>></td>
	<td><input type="hidden" name="filas" value = <%=idCounter%>></td>
        <td><input type="hidden" name="modificado" value = <%=lb_modificar%>>
        <td><input type="hidden" name="marcar_todas" value = "">
        <td><input type="hidden" name="todas" value = "">
       </table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

