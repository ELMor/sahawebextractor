<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.Servicios,isf.negocio.Especialidades,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(codesp){
  parent.parent.centro.location='mant_esp_modificar_datos_especialidad.jsp?codesp='+codesp;
  parent.parent.botonera.location.href = "mant_esp_botones_modificar.jsp"

}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
		<%
		boolean bFilaPar=true;
		int idCounter=0;
		Especialidades esp = new Especialidades();
		Vector v = new Vector();
		v = esp.busquedaEsp("","SESESP_COD ASC");
		if (v!=null){
			for(int int_pos = 0;int_pos < v.size(); int_pos++){
				SesEspecialidades sesEsp=(SesEspecialidades)v.elementAt(int_pos);
				String codesp = sesEsp.getSesespCod();
				if (bFilaPar){
	      			   	bFilaPar=false;
	%>
					<tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=codesp%>')" style="cursor:hand;">
	<%				}else{
						bFilaPar=true;
	%>
						<tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=codesp%>')" style="cursor:hand;">
	<%
				}
	%>
				<td width="25%" height="12px"><%=sesEsp.getSesespCod()%></td>
	   			<td width="75%" height="12px"><%=sesEsp.getSesespDesc()%></td>
			   	</tr>
<%
  				idCounter++;
  			}
		}
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>