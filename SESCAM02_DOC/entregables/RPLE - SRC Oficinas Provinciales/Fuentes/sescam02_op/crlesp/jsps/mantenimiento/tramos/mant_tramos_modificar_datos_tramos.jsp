<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanTramos" class="java.util.HashMap" scope="session"/>
<script language="javascript" type="text/javascript" src="../../../javascript/validarDecimal.js"></script>
<script language="javascript">
function reajustar() {
  this.document.formulario.submit();
  this.document.location = "mant_tramos_modificar_datos_tramos.jsp?noreajustar=FALSE";
}
function recargar() {
  this.document.formulario.submit();
  this.document.location = "mant_tramos_modificar_datos_tramos.jsp?noreajustar=TRUE";
}
</script>
</head>
<BODY class="mainFondo">
<%
int numTramos = 0;
long l_reqCod = 0;
int MAX_DIAS = 0;

String marcar = "FALSE";
if ((request.getParameter("noreajustar"))!= null) {
  marcar = request.getParameter("noreajustarc");
}
if ((request.getParameter("codigo"))!= null) {
  l_reqCod = new Long(request.getParameter("codigo")).longValue();
  beanTramos.put ("COD_TRAMO", new Long (l_reqCod));
} else {
  Long beanCod = (Long) beanTramos.get("COD_TRAMO");
  l_reqCod = beanCod.longValue();
}
if ((request.getParameter("maxDias")) != null) {
  MAX_DIAS = new Integer(request.getParameter("maxDias")).intValue();
  beanTramos.put ("MAX_DIAS", new Integer (MAX_DIAS));
} else {
  Integer beanMaxDias = (Integer) beanTramos.get("MAX_DIAS");
  MAX_DIAS = beanMaxDias.intValue();
}
Vector vec = new Vector();

if ( beanTramos.get("TRAMOS") != null){
   vec = (Vector) beanTramos.get("TRAMOS");
   numTramos = vec.size();
} else {
// beanTramos.clear();
   Tramos tramo = new Tramos();
   vec = new Vector();
   vec = tramo.busquedaTramo("CODTRAMO="+l_reqCod,"POSICION ASC");
   if (vec.size()>0) {
      numTramos = vec.size();
      beanTramos.put("TRAMOS", vec);
   }
}
%>
<TABLE width="100%" class="textoTop" border = "0">
  <tr>
  	 <td bgcolor=#5C5CAD> Modificar Tramos</td>
  </tr>
</table>
<form name="formulario" action="modificarValores.jsp" target="oculto1">
<table width ="100%" class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="0">
       <tr class="filaCabecera">
             <td width="20%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Tramo</a>
             </td>
            <td width="15%" height="12px" class="menuOpcion1">
             <a href="#" class="menuOpcion1">Inicio</a>
            </td>
             <td width="15%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Fin</a>
             </td>
            <td width="50%" height="12px" class="menuOpcion1">
             <a href="#" class="menuOpcion1">Descripcion</a>
            </td>
        </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0" align="left">
<%
boolean  bFilaPar = false;
boolean ultimoTramo = false;
int i = 0;
//    for (int i = 0; i < numTramos; i++ ) {
    while ((ultimoTramo == false) && (i < numTramos)) {
    SesTramos sesTramo = (SesTramos) vec.elementAt(i);
    long ini = new Double (sesTramo.getInicio()).longValue();
    long fin = new Double (sesTramo.getFin()).longValue();
    if ((int)ini > MAX_DIAS) {
      ini = (long) MAX_DIAS + 1;
      fin = 0;
      ultimoTramo = true;
    } else {
      if ((int) ini == MAX_DIAS) {
        ini = (long) MAX_DIAS;
        fin = 0;
        ultimoTramo = true;
      } else {
        if ((int)fin > MAX_DIAS) {
          fin = (long) MAX_DIAS;
        }
      }
    }
    if (i == numTramos -1) {
      fin = (long) MAX_DIAS;
    }


    if (bFilaPar){
      bFilaPar=false;
%>
      <tr class="filaPar" >
<%
    }else{
      bFilaPar=true;

%>
      <tr class="filaImpar">
<%
    }
%>
    <td width="20%"> Tramo <%=i + 1%> </td>
    <td width="15%">
<%
      String nombre = "inicio" + i;
%>
      <select name="<%=nombre%>" class="cajatexto2" disabled style="width:70%">
        <option value="<%=ini%>"> <%=ini%> </option>
      </select>
    </td>
    <td width="15%">
<%
    nombre = "fin" + i;
%>
      <select name="<%=nombre%>" class="cajatexto2" onChange="reajustar();" style="width:70%">
<%
         if (ultimoTramo == true) {
%>
         <option value="0" disabled> &nbsp </option>
<%
         }else {
           for (int j = (int)ini + 1; j <= MAX_DIAS; j++) {
             if (j == fin) {
%>
              <option selected value="<%=j%>"> <%=j%> </option>
<%
             } else {
%>
              <option value="<%=j%>"> <%=j%> </option>
<%
             }
           }
         }
%>
    </td>
    <%
    nombre = "descripcion" + i;
    %>
    <td width="50%"><input style="width:90%" type="text" name="<%=nombre%>" class="cajatexto3" value="<%= sesTramo.getDescripcion()%>" onChange="reajustar();" onkeypress = "if (event.keyCode == 13) reajustar();"> </td>
    <%
    i++;
    }
//  }

  if ((i == numTramos) && (ultimoTramo==false)) {
    if (bFilaPar){
      bFilaPar=false;
%>
      <tr class="filaPar" >
<%
    }else{
      bFilaPar=true;

%>
      <tr class="filaImpar">
<%
    }
%>
    <td > Tramo <%=i + 1%> </td>
    <td >
<%
    String nombre = "inicio" + i;
%>
      <select name="<%=nombre%>" class="cajatexto2" disabled>
       <option value="<%=MAX_DIAS + 1%>"> <%=MAX_DIAS + 1%> </option>
      </select>
    </td>
    <td >
<%
    nombre = "fin" + i;
%>
      <select name="<%=nombre%>" class="cajatexto2">
       <option value="0" disabled> &nbsp </option>
      </select>
    </td>
<%
    nombre = "descripcion" + i;
%>
    <td ><input type="text" name="<%=nombre%>" class="cajatexto2" value="" onChange="reajustar();"> </td>
    <%
    i++;
  }
%>
    <td><input type="hidden" name="noreajustar"  value = "<%=marcar%>"></td>

    <script language = "javascript">
      if (this.document.all("noreajustar").value == "FALSE") {
	recargar();
     }
    </script>
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
