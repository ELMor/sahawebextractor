<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="beans.ConsultaServMapeados,isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(codserv,centro,descserv){
  parent.parent.centro.location='mant_map_modificar_datos_servicio.jsp?codserv='+codserv+'&descserv='+descserv+'&centro='+centro;
  parent.parent.botonera.location.href = "mant_map_botones_modificar.jsp";

}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral">
	<table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
		<%
                  boolean bFilaPar=true;
                  int idCounter=0;
                  long param_centro = new Double(request.getParameter("valor_centro")).longValue();
                  if (param_centro >= 0) {
                    Vector v = new Vector();
                    String cod_sescam="";
                    String cod_mapeo = "";
                    String descrip = "";
                    String descripmapeo = "";
                    String orden = "";
                    cod_sescam = request.getParameter("codigosescam");
                    cod_mapeo = request.getParameter("codigo");
                    descrip = request.getParameter("descrip");
                    descripmapeo = request.getParameter("descripmapeo");
                    orden = request.getParameter("orden");
                    ConsultaServMapeados consulta = new ConsultaServMapeados();
                    consulta.setCentro(param_centro);
                    consulta.setCod_mapeo(cod_mapeo);
                    consulta.setDescripmapeo(descripmapeo);
                    consulta.setCod_sescam(cod_sescam);
                    consulta.setDescrip(descrip);
                    consulta.setOrden(orden);
                    v = consulta.ServiciosMapeados();
                    String desc_centro="";
                    if (v!=null){
                          for(int int_pos = 0;int_pos < v.size(); int_pos++){
                                  ServMapeados sm=(ServMapeados)v.elementAt(int_pos);
                                  Centros cen = new Centros();
                                  Vector vc = new Vector();
                                  vc = cen.busquedaCentro("CODCENTRO="+sm.CodCentroMapeo,"");
                                  if (vc.size()>0){
                                    Centrosescam cs = (Centrosescam) vc.elementAt(0);
                                    desc_centro = cs.getDescentro();
                                  }
                                  if (bFilaPar){
                                                   bFilaPar=false;
                %>
                                          <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=sm.CodServMap%>','<%=sm.CodCentroMapeo%>','<%=sm.DescServMap%>')" style="cursor:hand;">
                <%
                                  }else{
                                                  bFilaPar=true;
                %>
                                                  <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=sm.CodServMap%>','<%=sm.CodCentroMapeo%>','<%=sm.DescServMap%>')" style="cursor:hand;">
                <%
                                  }
%>
				<td width="10%" height="12px"><%=sm.CodServSESCAM%></td>
	   			<td width="35%" height="12px"><%=sm.DescServSESCAM%></td>
	   			<td width="10%" height="12px"><a title="<%=desc_centro%>"><%=sm.CodCentroMapeo%></a></td>
                                <td width="10%" height="12px"><%=sm.CodServMap%></td>
                                <td width="35%" height="12px"><%=sm.DescServMap%></td>
			   	</tr>
<%
  				idCounter++;
  			}
		}
            }
%>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>