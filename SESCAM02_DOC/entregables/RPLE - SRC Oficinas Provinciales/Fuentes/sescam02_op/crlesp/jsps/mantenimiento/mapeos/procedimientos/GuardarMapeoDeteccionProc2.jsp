<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<jsp:useBean id="beanProcNoSescam" class="java.util.HashMap" scope="session"/>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<form name="guardar">
<%

           long procpk=0,centro=0;
           String codproc="",tipo="";
           if ((request.getParameter("procpk")!=null)&&(!request.getParameter("procpk").equals(""))){
             try{
               procpk =  new Long(request.getParameter("procpk")).longValue();
               centro = new Double(request.getParameter("centro")).longValue();
               codproc = request.getParameter("codproc").trim();
               tipo = request.getParameter("tipo");
             }catch (Exception e) {}
           }

           if (procpk>0){
             MapeoProc mapproc = new MapeoProc();
             SesProcMap spm = new SesProcMap();

             /*INSERCION EN SES_PROC_MAP */

             /* realizo los sets */
             spm.setProcPk(procpk);
             spm.setCentro(centro);
             spm.setProcCenCod(codproc);
             spm.setProcCenTipo(tipo);

             /*inserto el objeto */
             try{
               mapproc.SetNegEvento(accesoBeanId.getNegEventoInstance(38));
               mapproc.insertarProcMap(spm);
             }catch(ExceptionSESCAM eses){
               System.out.println("Excepcion Sescam capturada en guardarMapeoAutomaticoProc2.jsp al guardar mapeo.");
%>
               <script language="javascript">
                 alert("<%=eses%>");
               </script>
<%
             }
           }else{
%>
               <script language="javascript">
                 alert("No ha seleccionado ningun procedimiento.No se realizara el mapeo.");
               </script>
<%
           }
           if (beanProcNoSescam.size()>0){
             for (int i=0;i<beanProcNoSescam.size();i++){
               String campo="CODPROC"+i;
               String codigo=(String)beanProcNoSescam.get(campo);
%>
             <script language="javascript">
                alert("El codigo:"+"<%=codigo%>"+" no corresponde a ningun procedimiento SESCAM. No se guardara el mapeo.");
             </script>
<%
             }
             beanProcNoSescam.clear();
           }
%>
  <script language="javascript">
    parent.parent.document.location = "mant_map_proc_index.htm";
  </script>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>