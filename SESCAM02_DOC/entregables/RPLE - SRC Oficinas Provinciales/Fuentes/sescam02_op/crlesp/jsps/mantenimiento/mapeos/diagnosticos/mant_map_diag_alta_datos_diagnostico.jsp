<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../../javascript/SelecUtil.js"></script>
</head>
<jsp:useBean id="beanMapeoDiagnosticos" class="java.util.HashMap" scope="session"/>
<script language="javascript">
  function abrir_buscador(){
    var retorno=0;
    llamado="buscador_diag_index.htm";
    retorno=window.showModalDialog(llamado,"","dialogWidth:45em;dialogHeight:35em;status=no");
    document.form.diagpk.value=retorno;
    document.formulario.diagpk.value=retorno;
    document.form.submit();
  }

</script>
<BODY class="mainFondo">
<TABLE width="100%" class="textoTop">
  <tr>
           <td bgcolor=#5C5CAD> A&ntilde;adir Mapeo Diagn&oacute;stico</td>
  </tr>
</table>
<form name="form" action="busquedaPkDiagnostico.jsp" target="oculto1">
<input type="hidden" name="diagpk" value="">
</form>
<form name="formulario" action="guardarMapDiagnostico.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width="20%">Codigo Sescam:</td>
    <td class= "normal" width="50%"><input type="text" style="width:12%" name="coddiagses" class="cajatexto2" value="" readonly></td>
    <td class= "normal" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td class= "texto">Diagnostico Sescam: </td>
    <td class= "normal"><input type="text" name="descdiagses" class="cajatexto3" value="" readonly></td>
    <td class= "normal"><a href="#"><img src="../../../../imagenes/lupa_2.gif" onClick="return abrir_buscador();" alt="Buscador" border=0></a></td>
  </tr>
  <tr>
    <td class= "texto">Centro: </td>
    <td  width="65%"  class= "normal"><div id="divCentro">
      <Select name="centro" class="cajatexto2" style="width:35%">
<%
           Centros centro = new Centros();
           Vector vc = new Vector();
           vc = centro.busquedaCentro("","CODCENTRO ASC");
           for(int int_pos = 0;int_pos < vc.size(); int_pos++){
                Centrosescam cs=(Centrosescam)vc.elementAt(int_pos);
                long l_centro = new Double(cs.getCodcentro()).longValue();
%>
                <option  value='<%=l_centro%>'><%=cs.getDescentro()%></option>
<%
           }
%>
           </Select></div>
    </td>
  </tr>
  <tr>
    <td class= "texto">Diagnostico Mapeado: </td>
    <td class= "normal" width="80%"><input type="text" style="width:12%" maxlength=6 name="coddiagmap" class="cajatexto2" value=""></td>
  </tr>
  <input type="hidden" name="diagpk" value="">
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
