<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.ProcTipos,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(pktipo){
  parent.parent.centro.location='mant_proctipo_modificar_datos_tipo.jsp?pktipo='+pktipo;
  parent.parent.botonera.location.href = "mant_proctipo_botones_modificar.jsp"

}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
		<%
		boolean bFilaPar=true;
		int idCounter=0;
		ProcTipos pT = new ProcTipos();
		Vector v = new Vector();
		v = pT.busquedaProcTipo("","PROC_TDESC ASC");
		if (v!=null){
			for(int int_pos = 0;int_pos < v.size(); int_pos++){
				SesProcTipos sesPT=(SesProcTipos)v.elementAt(int_pos);
                                long pktipo = sesPT.getProcTipo();
				String codtipo = sesPT.getProcTcod();
                                String desctipo = sesPT.getProcTdesc();
				if (bFilaPar){
	      			   	bFilaPar=false;
	%>
					<tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=pktipo%>')" style="cursor:hand;">
	<%				}else{
						bFilaPar=true;
	%>
						<tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=pktipo%>')" style="cursor:hand;">
	<%
				}
	%>
				<td width="25%" height="12px"><%=codtipo%></td>
	   			<td width="75%" height="12px"><%=desctipo%></td>
			   	</tr>
<%
  				idCounter++;
  			}
		}
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>