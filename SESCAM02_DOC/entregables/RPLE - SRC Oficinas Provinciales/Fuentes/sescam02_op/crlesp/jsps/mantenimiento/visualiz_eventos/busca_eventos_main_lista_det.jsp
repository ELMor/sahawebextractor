<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="beans.ConsultaEventosBean" %>
<%@ page import="isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">

function seleccion(codsess,codevto){
  window.showModalDialog("ficha_evto_main.jsp?evto_pk="+codevto+ " &sess_pk="+codsess," ","dialogWidth:50em;dialogHeight:35em;status=no");
}
</script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="listaeventos">
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
    <table style="width:100%;" align="left" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral2" >
	<table class="centroTabla" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
<%
		VisualizEventos visevto;
		Vector vp=new Vector();
                long l_rol,l_oficina,l_tipo_evto;
		boolean bFilaPar=true;
		int idCounter=0;
                ConsultaEventosBean cs=new ConsultaEventosBean();
                //System.out.println("Creo el ConsultaSolicitudesBean");
                if (request.getParameter("rol") != null) {
                 l_rol=new Long(request.getParameter("rol")).longValue();
                 l_oficina=new Long(request.getParameter("oficina")).longValue();
                 l_tipo_evto = new Long(request.getParameter("eventos")).longValue();
                 cs.setRol(l_rol);
                 cs.setOficina(l_oficina);
                 cs.setFechaini(request.getParameter("fechaini"));
                 cs.setFechafin(request.getParameter("fechafin"));
                 cs.setUsuario(request.getParameter("usuario"));
                 cs.setTipoEvto(l_tipo_evto);
                 cs.setOrden(request.getParameter("orden"));
                 cs.setIp(request.getParameter("ip"));
                // System.out.println("Seteo los filtros");
                 try {
                  vp=cs.resultado();
                 }catch (ExceptionSESCAM eSES){
%>
                 <script language="javascript">
                     alert("<%=eSES%>");
                 </script>
<%
                }

%>
		<script language="javascript">
        		  parent.pie.location="../../pie_buscador.jsp?estado=Generando resultado...";
		</script>
<%
		if (vp.size()>0) {
	  		Enumeration e = vp.elements();
                        visevto = new VisualizEventos();
	    		while(e.hasMoreElements()) {
	      			visevto=(VisualizEventos)e.nextElement();
	      			if (bFilaPar){
	      			   	bFilaPar=false;
                                        //System.out.println("Estado"+idCounter+": "+solRet.fEstado);
%>
					<tr class="filaPar" id=<%=idCounter%> onclick="seleccion(<%=visevto.evtosess_pk%>,<%=visevto.evtoevto_pk%>);" style="cursor:hand;">
<%       			}else{
					bFilaPar=true;
                                        //System.out.println("Estado"+idCounter+": "+solRet.fEstado);
%>
					<tr class="filaImpar" id=<%=idCounter%> onclick="seleccion(<%=visevto.evtosess_pk%>,<%=visevto.evtoevto_pk%>);" style="cursor:hand;">
<%
				}
%>
				<td width="14%"  height="12px" name="fecha"><%=visevto.evtoFecha%></td>
                                <td width="15%"  height="12px" name="oficina"><%=visevto.evtoLogin%></td>
				<td width="20%"  height="12px" name="centro"><%=visevto.evtoOficina%></td>
				<td width="17%"  height="12px" name="cip"><%=visevto.evtoCodigo%></td>
				<td width="48%" height="12px" name="nom"><%=visevto.evtoDesc%></td>

                             </tr>

<%
  				idCounter++;
  				}
                                %>
                                <script language="javascript">
                      		  parent.pie.location="../../pie_buscador.jsp?estado=Listo&filas=<%=cs.getResultCount()%>";
                                </script>
<%
			}else{
%>
				<Script language="javaScript">
                      		  parent.pie.location="../../pie_buscador.jsp?estado=No se han encontrado filas.";
				</Script>
<%			}
                }
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
