<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../../css/estilo.css">
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<%
                /* Recojo los parametros */
                long proc_pk = new Long(request.getParameter("procpk")).longValue();
                long cod_centro = new Long(request.getParameter("centro")).longValue();
                String proc_cen_cod = request.getParameter("codprocmap");
                String proc_cen_tipo = request.getParameter("tipo").toUpperCase();
                MapeoProc mapproc = new MapeoProc();
                SesProcMap sPM= new SesProcMap();

                /*Realizo los set */
                sPM.setProcPk(proc_pk);
                sPM.setCentro(cod_centro);
                sPM.setProcCenCod(proc_cen_cod);
                sPM.setProcCenTipo(proc_cen_tipo);
                try{
                    mapproc.SetNegEvento(accesoBeanId.getNegEventoInstance(38));
                  /*inserto el objeto*/
                    mapproc.insertarProcMap(sPM);
                }catch(ExceptionSESCAM eses){
                    System.out.println("Excepcion Sescam capturada en insertarMapProcedimiento.jsp");
%>
                    <script language="javascript">
                      alert("<%=eses%>");
                    </script>
<%
                }
%>

                 <script language="javascript">
                    parent.centro.location.href = "mant_map_proc_main.htm"
                    parent.botonera.location.href = "mant_map_proc_down.htm"
                </script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>