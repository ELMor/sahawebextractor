<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanPerfiles" class="java.util.HashMap" scope="session"/>

<script language="javascript">
  function origen(pk) {
    if (pk > 0) {
       parent.centro.location.href = "../permisos/mant_perm_modificar_datos_permiso.jsp?permpk="+pk;
       parent.botonera.location.href = "../permisos/mant_perm_botones_modificar.jsp";
    }
    else {
      parent.centro.location.href = "mant_per_main.htm"
      parent.botonera.location.href = "mant_per_down.htm"
    }
  }
</script>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
                long l_reqpk;
                String[] ssel;

		Vector vorigen= new Vector();
                String str_nombre = request.getParameter("nombre");
                String str_descripcion = request.getParameter("descripcion");

                if ((request.getParameter("pk_destino") != null) && (!request.getParameter("pk_destino").equals(""))) {
                  l_reqpk = new Long(request.getParameter("pk_destino")).longValue();
                }else {
                  l_reqpk = 0;
                }

		Perfiles per = new Perfiles();
		SysRoles sR = (SysRoles)beanPerfiles.get("PERFILES");
		vorigen = (Vector)beanPerfiles.get("PERM_ORIGEN");
		//System.out.println(request.getParameterValues("listaPermisosSelec"));
                if (request.getParameter("listaPermisosSelec") != null)  {
                     ssel = request.getParameterValues("listaPermisosSelec");
                }
                else {
                  ssel = null;
                }

		/*Realizo los set*/
		sR.setSysRolNombre(str_nombre);
		sR.setSysRolDesc(str_descripcion);

		/*modifico el objeto*/
		try{
                per.SetNegEvento(accesoBeanId.getNegEventoInstance(6));
                per.modificarPerfil(sR,vorigen,ssel);
                }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en modificarPerfil.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                }
		beanPerfiles.clear();
		%>

		<script language="javascript">
                    origen (<%=l_reqpk%>);
		</script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>