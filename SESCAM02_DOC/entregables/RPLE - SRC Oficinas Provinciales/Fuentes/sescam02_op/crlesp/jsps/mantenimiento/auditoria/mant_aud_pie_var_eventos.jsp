<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanmarcar" class="java.util.HashMap" scope="session"/>

<script language="javascript">

   function marcar_todas() {
    var valor;
    var codigo;
    valor = this.document.pieEtado.marcar.checked;
    if (this.document.pieEtado.marcar.checked)
    {
      parent.var_eventos.document.seleccion_var.marcar_todas.value = "checked";
    }
    else
    {
      parent.var_eventos.document.seleccion_var.marcar_todas.value = "nochecked";
    }
    valor =parent.var_eventos.document.seleccion_var.marcar_todas.value;
    codigo = parent.var_eventos.document.seleccion_var.codigo.value;
    parent.var_eventos.location = "mant_aud_var_eventos.jsp?evtopk="+codigo+"&marcar_todas="+valor;

 }
</script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" >

  <%
    String estado = "";
    String marcar = "";
 //   System.out.println("Estado"+request.getParameter("estado"));

    if (request.getParameter("estado") != null) {
      estado = request.getParameter("estado");
    }
    if (request.getParameter("marcar") != null) {
     marcar = request.getParameter("marcar");
    }

    int nfilas = 0;
    if (request.getParameter("filas") != null) {
      nfilas = new Integer(request.getParameter("filas")).intValue();
    }
    String pie_estado = " Ingrese condiciones de busqueda. ";
    if (request.getParameter("pie_estado") != null) {
      pie_estado = request.getParameter("pie_estado");
    }
  %>
  <form name="pieEtado">
  <table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td>
	  <table style="width:100%" align="left" border="0" cellspacing="0" cellpadding="0">
	   <tr>
  	    <td>
               <table cellspacing="1" cellpadding="1" border="0">
                   <tr class="pieInferior" height="18px">
                           <td width="20%" style={vertical-align:top;}>Marcar todas:</td>
                           <td width="5%"  ><input type="checkbox" name="marcar" value = "" <%=marcar%> <%=estado%> onClick="marcar_todas();" > </td>
                           <td width="10%" style={vertical-align:top;}>Estado:</td>
                           <td width="40%" ><input type="text" class="cajaTextoLabel" ID="lEstado" style={width:200;} value = <%=pie_estado%> readOnly ></td>
                           <td width="10%" style={vertical-align:top;}>Filas:</td>
                           <td width="15%" ><input type="text" class="cajaTextoLabel" ID="lFilas" value = <%=nfilas%> readOnly ></td>
                   </tr>
		</table>
            </td>
	   </tr>
	   </table>
   </td>
  </tr>
</table>
<input type="hidden" name = "estado" value = "">
</form>
</body>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</html>
