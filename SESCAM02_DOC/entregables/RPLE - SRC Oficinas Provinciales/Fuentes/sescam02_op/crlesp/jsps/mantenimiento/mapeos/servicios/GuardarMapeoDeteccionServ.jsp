<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

<%
   int i,i_filas,i_filas_modificadas,i_modificadas = 0;
   String campo_codigo;
   String campo_codigo_sescam;
   String campo_centro;
   String codserv = "";
   String codigo_sescam ="";
   String tipo = "";
   long centro=0;
   long modificado = 0;
   i_filas = new Integer(request.getParameter("filas")).intValue();
   i_filas_modificadas = new Integer(request.getParameter("filas_modificadas")).intValue();

   Vector v = new Vector();
   for (i=0;i<i_filas;i++) {
       if (i_modificadas < i_filas_modificadas) {
         campo_codigo = "codigo_"+i;
         campo_centro = "centro";
         campo_codigo_sescam = "codservses_"+i;
         if (request.getParameter(campo_codigo) != null) {
           codserv = request.getParameter(campo_codigo).trim();
           centro = new Double(request.getParameter(campo_centro)).longValue();
           if ((request.getParameter(campo_codigo_sescam)==null)||(request.getParameter(campo_codigo_sescam).equals(""))){
%>
           <script languaje="javascript">
             alert("No se pueden guardar codigos en blanco.");
           </script>
<%
            continue;
           }
           codigo_sescam =request.getParameter(campo_codigo_sescam).trim();
           i_modificadas = i_modificadas + 1;
           MapeoServicio mapserv = new MapeoServicio();
           SesSrvMap ssm = new SesSrvMap();
           /*Comprobacion de codigos sescam validos */
           Servicios s = new Servicios();
           Vector vs = new Vector();
           vs = s.busquedaServ("SESSRV_COD='"+codigo_sescam+"'","");
           if (vs.size()<=0){
%>
             <script language="javascript">
                alert("Se ha detectado codigo:"+'<%=codigo_sescam%>'+" que no corresponden a ningun Servicio SESCAM. No se guardara.");
             </script>
<%
              continue;
           }
           /*INSERCION EN SES_SRV_MAP */
           /* realizo los sets */
           ssm.setCentro(centro);
           ssm.setSrvCenCod(codserv);
           ssm.setSessrvCod(codigo_sescam);
           /*inserto el objeto */
             try{
               mapserv.SetNegEvento(accesoBeanId.getNegEventoInstance(29));
               mapserv.insertarMapServ(ssm);
             }catch(ExceptionSESCAM eses){
               System.out.println("Excepcion Sescam capturada en guardarMapeoAutomaticoServ.jsp al guardar mapeo.");
%>
               <script language="javascript">
                 alert("<%=eses%>");
               </script>
<%
             }

           }
       }else{
         if (i_filas_modificadas==0){
%>
               <script language="javascript">
                 alert("No hay filas seleccionadas para el mapeo.");
               </script>
<%
         }
         break;
       }
  }
%>
               <script language="javascript">
                  parent.parent.document.location = "mant_map_index.htm";
               </script>
<%

%>


</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>