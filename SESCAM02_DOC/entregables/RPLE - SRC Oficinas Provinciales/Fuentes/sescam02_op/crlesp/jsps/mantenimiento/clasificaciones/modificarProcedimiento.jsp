<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimiento" class="java.util.HashMap" scope="session"/>

</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */

                String str_codigo = request.getParameter("codigo");
                String str_descrip_corta = request.getParameter("descrip");
                String str_descrip = request.getParameter("descrip_larga");
                long l_activo = 0;
                double d_garantia = 0;
                double d_monto = 0.0;
                int i_error = 1;
                long l_tipo = 0;
               try {
                 l_activo = new Long(request.getParameter("activo")).longValue();
                 d_garantia = new Double(request.getParameter("garantia")).doubleValue();
                 d_monto = new Double (request.getParameter("monto")).doubleValue();
                 l_tipo = new Long(request.getParameter("valor_tipo")).longValue();
               }catch (Exception ex) {

                   ExceptionSESCAM es = new ExceptionSESCAM(ExceptionSESCAM.FORMATO_NUMERO_INCORRECTO);

                   %>
                   <script language="javascript">
                      alert ('<%=es.getMensaje()%>');
                   </script>
                   <%
                   i_error = -1;
                }
               if (i_error > 0) {
                /*Realizo los set */
                 Procedimientos proc = new Procedimientos();
                 SesProcedimientos sO = (SesProcedimientos)beanProcedimiento.get("PROCEDIMIENTO");

                 sO.setProcCod(str_codigo);
                 sO.setProcDescCorta(str_descrip_corta);
                 sO.setProcDesc(str_descrip);
                 sO.setProcActivo(l_activo);
//                 sO.setProcGarantia(l_garantia);
                 sO.setProcMontoMaxEst(d_monto);
                 sO.setProcTipo(l_tipo);
                 /*modifico el objeto*/
                 try{
                  proc.SetNegEvento(accesoBeanId.getNegEventoInstance(2));
                  proc.modificarProced(sO);
                  if (d_garantia >= 0) {
                    ProcGarantias procgar = new ProcGarantias();
                    SesProcGarantia sesprocgar;
                    Vector ve = new Vector();
                    ve = procgar.busquedaProcGarantia("proc_pk = "+sO.getProcPk() +" and sesesp_cod is null ","");
                    if (ve.size() == 1) {
                      sesprocgar = (SesProcGarantia)ve.elementAt(0);
                      sesprocgar.setProcGarantia(d_garantia);
                      procgar.modificarProcGar(sesprocgar);
                    } else {
                      sesprocgar = new SesProcGarantia();
                      sesprocgar.setProcGarantia(d_garantia);
                      sesprocgar.setProcPk(sO.getProcPk());
                      procgar.insertarProcGar(sesprocgar);
                    }
                  }
                 }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en modificarProcedimiento.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                }
                beanProcedimiento.clear();

%>
                 <script language="javascript">
                    top.close();
                 </script>
                <%
                }
                %>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>