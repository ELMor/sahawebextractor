<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

   <%
   int i,i_filas,i_filas_modificadas,i_modificadas = 0;
   String campo_codigo;
   String campo_centro;
   String campo_pk;
   String campo_tipo;
   String campo_modificado;
   String codproc = "";
   String tipo = "";
   long centro=0;
   long procpk = 0;
   long modificado = 0;
   i_filas = new Integer(request.getParameter("filas")).intValue();
   i_filas_modificadas = new Integer(request.getParameter("filas_modificadas")).intValue();

   Vector v = new Vector();
   for (i=0;i<i_filas;i++) {
       if (i_modificadas < i_filas_modificadas) {
         campo_codigo = "codigo_"+i;
         campo_pk = "pk_"+i;
         campo_tipo = "tipo_"+i;
         campo_centro = "centro";
         campo_modificado = "modificado_"+i;
         if (request.getParameter(campo_codigo) != null) {
           codproc = request.getParameter(campo_codigo).trim();
           procpk =  new Long(request.getParameter(campo_pk)).longValue();
           centro = new Double(request.getParameter(campo_centro)).longValue();
           modificado = new Long(request.getParameter(campo_modificado)).longValue();
           tipo = request.getParameter(campo_tipo);
           i_modificadas = i_modificadas + 1;
           MapeoProc mapproc = new MapeoProc();
           SesProcMap spm = new SesProcMap();
           if (modificado == 1){
           /*MODIFICACION EN SES_PROC_MAP */
             Vector vmod = new Vector();
             vmod = mapproc.busquedaProcMap("trim(PROC_CEN_COD)='"+codproc+"' AND CENTRO="+centro+" AND PROC_CEN_TIPO='"+tipo+"'","");
             if (vmod.size()>0){
               spm = (SesProcMap)vmod.elementAt(0);
             }
             try{
               mapproc.SetNegEvento(accesoBeanId.getNegEventoInstance(38));
               spm.setProcPk(procpk);
               mapproc.modificarProcMap(spm);
             }catch (ExceptionSESCAM es) {
               System.out.println("Excepcion Sescam capturada en guardarMapeoAutomaticoProc.jsp al modificar mapeo.");
%>
               <script language="javascript">
                 alert("<%=es%>");
               </script>
<%
             }
           }else{
           /*INSERCION EN SES_PROC_MAP */
           /* realizo los sets */
             spm.setProcPk(procpk);
             spm.setCentro(centro);
             spm.setProcCenCod(codproc);
             spm.setProcCenTipo(tipo);
           /*inserto el objeto */
             try{
               mapproc.SetNegEvento(accesoBeanId.getNegEventoInstance(38));
               mapproc.insertarProcMap(spm);
             }catch(ExceptionSESCAM eses){
               System.out.println("Excepcion Sescam capturada en guardarMapeoAutomaticoProc.jsp al guardar mapeo.");
%>
               <script language="javascript">
                 alert("<%=eses%>");
               </script>
<%
             }
           }
         }
       }else{
         break;
       }
  }
  %>
  <script language="javascript">
    parent.parent.document.location = "mant_map_proc_index.htm";
  </script>
<%

%>


</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>