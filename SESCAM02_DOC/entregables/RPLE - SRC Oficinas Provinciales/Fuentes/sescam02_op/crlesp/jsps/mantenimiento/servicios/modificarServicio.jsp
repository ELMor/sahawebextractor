<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanServicios" class="java.util.HashMap" scope="session"/>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
                long l_pk_perm = 0;
		/* Recojo los parametros */
		String str_descserv = request.getParameter("descserv");
		String str_especialidad = request.getParameter("especialidad");
                SesServicios sesServ =  (SesServicios)beanServicios.get("SERVICIO");
                long l_origen = new Long(request.getParameter("origen")).longValue();
                String str_pk_esp = request.getParameter("pk_esp");
                Servicios ser = new Servicios();

                /*Realizo los set */
                sesServ.setSessrvDesc(str_descserv);
                sesServ.setSesespCod(str_especialidad);

                try{
                   ser.SetNegEvento(accesoBeanId.getNegEventoInstance(24));
                   /*modifico el objeto*/
                   ser.modificarServ(sesServ);
                }catch(ExceptionSESCAM eses){
                      System.out.println("Excepcion Sescam capturada en modificarServicio.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                 }
       		beanServicios.clear();
                if (l_origen==2){
%>
               <script language="javascript">
                 llamada = '../especialidades/mant_esp_modificar_datos_especialidad.jsp?codesp='+"<%=str_pk_esp%>";
                 parent.centro.location.href = llamada;
                 parent.botonera.location.href = "../especialidades/mant_esp_botones_modificar.jsp";
               </script>
<%
               }else{
%>
               <script language="javascript">
                 parent.centro.location.href = "mant_serv_main.htm"
                 parent.botonera.location.href = "mant_serv_down.htm"
               </script>
<%
               }
%>


</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>