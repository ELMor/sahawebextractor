<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanProcedimientos" class="java.util.HashMap" scope="session"/>

<script language="javascript" type="text/javascript" src="../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }
</script>
</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" >
  <%
    boolean lb_no_buscar = true,lb_buscar = false;
    beanProcedimientos.clear();
    String s_where = "";
    String str_codigo_ini = "";
    long l_tipo = 0;

    if (request.getParameter("no_buscar") != null) {
          lb_no_buscar = false;
    }
    if (lb_no_buscar == false) {

     if (request.getParameter("codigo_inicial") != null) {
       str_codigo_ini = request.getParameter("codigo_inicial").toUpperCase();
     }
     if (request.getParameter("tipo")!= null) {
       l_tipo = new Long(request.getParameter("tipo")).longValue();
     }

     if ((str_codigo_ini == null) || (str_codigo_ini.equals(""))) {
       %>
       <script language="javascript">
        alert ('Se debe de especificar codigo de procedimiento para iniciar el proceso de actualizacion');
       </script>
       <%
     }
     else {
       lb_buscar = true;
     }
    }
  %>

<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="1">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" cellspacing="0" cellpadding="0" border="0">
<%
        Vector v1 = new Vector();
        if ((lb_buscar == true) && (lb_no_buscar == false)) {
          %>
            <script language="javascript">
              parent.pie.location="../../pie_buscador.jsp?estado=Generando resultado...";
            </script>
          <%
           boolean bFilaPar=true;
           int idCounter=0;
           double d_garantia = 0;
           String s_tipo ="";
           int i;
           Procedimientos proc = new Procedimientos();
           if (l_tipo>0){
             s_where = " (upper(proc_cod) like '"+str_codigo_ini + "%') and proc_tipo="+l_tipo;
           }else{
             s_where = " (upper(proc_cod) like '"+str_codigo_ini + "%')";
           }
           v1 = proc.busquedaProced(s_where," PROC_COD ASC ");
           //System.out.println("SIZE:"+v1.size());
           if (v1.size() > 0) {
                beanProcedimientos.clear();
                beanProcedimientos.put("VECTOR",v1);
                for (int int_pos = 0;int_pos < v1.size(); int_pos++) {
                  SesProcedimientos sesproc=(SesProcedimientos)v1.elementAt(int_pos);
                  ProcGarantias procgar = new  ProcGarantias();
                  Vector ve = new Vector();
                  ve = procgar.busquedaProcGarantia("proc_pk = "+sesproc.getProcPk()+" and sesesp_cod is null ","");
                  if (ve.size() == 1) {
                    SesProcGarantia sesprocgar = (SesProcGarantia)ve.elementAt(0);
                    d_garantia = sesprocgar.getProcGarantia();
                  }
                  ProcTipos pT = new ProcTipos();
                  ve = pT.busquedaProcTipo("proc_tipo ="+sesproc.getProcTipo(),"");
                  if (ve.size() == 1) {
                     SesProcTipos spt = (SesProcTipos)ve.elementAt(0);
                     s_tipo = spt.getProcTcod();
                  }
                  if (bFilaPar){
                    bFilaPar=false;
                %>
                    <tr class="filaPar" id=<%=idCounter%> style="cursor:hand;">
                <%
                  }else{
                    bFilaPar=true;
                %>
                    <tr class="filaImpar" id=<%=idCounter%> style="cursor:hand;">
                <%
                  }
        	%>
                    <td width="18%" height="12px"><%=sesproc.getProcCod()%></td>
                    <td width="36%" height="12px"><%=sesproc.getProcDescCorta()%></td>
                    <td width="10%" height="12px"><%=s_tipo%></td>
                    <td width="14%" height="12px"><%=sesproc.getProcMontoMaxEst()%></td>
                    <td width="14%" height="12px"><%=d_garantia%></td>
                <%
                  if (sesproc.getProcActivo() == 1) {
                %>
                      <td width="8%" height="12px">Alta</td>
                <%
                  }
                  else {
                %>
                      <td width="8%" height="12px">Baja</td>
                 <%
                  }
                 %>
                  </tr>
<%
                  idCounter++;
                }
                %>
                 <script language="javascript">
                    parent.pie.location="../../pie_buscador.jsp?estado=Listo&filas=<%=v1.size()%>";
                    parent.parent.botonera.location = "mant_pro_botones_actualizar_carga_masiva.jsp"
                 </script>
                <%
              }else {
             %>
                <Script language="javaScript">
                    parent.pie.location="../../pie_buscador.jsp?estado=No se han encontrado filas.";
                </Script>
             <%
              }
           }
%>
     </table>
    </td>
   </tr>
   <input type="hidden" name="nreg" value="<%=v1.size()%>">
   </table>
  </form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>

