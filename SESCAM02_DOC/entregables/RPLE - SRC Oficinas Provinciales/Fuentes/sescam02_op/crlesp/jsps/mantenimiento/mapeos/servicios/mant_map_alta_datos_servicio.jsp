<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/FormUtil.js"></script>
<script language="javascript" type="text/javascript" src="../../../../javascript/SelecUtil.js"></script>
</head>
<jsp:useBean id="beanServicios" class="java.util.HashMap" scope="session"/>
<BODY class="mainFondo">
<TABLE width="100%" class="textoTop">
  <tr>
           <td bgcolor=#5C5CAD> A�adir Mapeo</td>
  </tr>
</table>
<form name="formulario" action="guardarMapServicio.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
    <td class= "texto" width="20%">Codigo Sescam:</td>
    <td class= "normal" width="80%"><input type="text" style="width:10%" maxlength=4 name="codservses" class="cajatexto2" value="" readonly></td>
  </tr>
  <tr>
    <td class= "texto">Servicio Sescam: </td>
    <td class= "normal"><div id="divServicio">
          <Select name="servsescam" class="cajatexto3" style="width:55%" onChange="codservses.value=this.value;">
           <%
           Servicios serv = new Servicios();
           Vector v = new Vector();
           v = serv.busquedaServ("","");
           for(int int_pos = 0;int_pos < v.size(); int_pos++){
                SesServicios sesServ=(SesServicios)v.elementAt(int_pos);
           %>
                    <option value='<%=sesServ.getSessrvCod()%>'><%=sesServ.getSessrvDesc()%></option>
           <%
           }
%>
           </Select></div>
    </td>
  </tr>
  <tr>
    <td class= "texto">Centro: </td>
    <td  width="65%"  class= "normal"><div id="divCentro">
      <Select name="centro" class="cajatexto2" style="width:35%">
<%
           Centros centro = new Centros();
           Vector vc = new Vector();
           vc = centro.busquedaCentro("","CODCENTRO ASC");
           for(int int_pos = 0;int_pos < vc.size(); int_pos++){
                Centrosescam cs=(Centrosescam)vc.elementAt(int_pos);
%>
                <option  value='<%=cs.getCodcentro()%>'><%=cs.getDescentro()%></option>
<%
           }
%>
           </Select></div>
    </td>
  </tr>
  <tr>
    <td class= "texto">Servicio Mapeado: </td>
    <td class= "normal" width="80%"><input type="text" style="width:10%" maxlength=4 name="codservmap" class="cajatexto2" value=""></td>
  </tr>
</table>
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
</table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>