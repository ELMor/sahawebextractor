<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.*,isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
</head>

<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
                String str_pass="";
		String str_nombre = request.getParameter("nombre");
		String str_apellido1 = request.getParameter("apellido1");
		String str_apellido2 = request.getParameter("apellido2");
		String str_login = request.getParameter("login");
		//String str_pass = request.getParameter("password");
                //String str_repetida = request.getParameter("repetida");
		int i_sysrol = new Integer(request.getParameter("perfiles")).intValue();
		long activo_sn = 1;
                long l_oficina = new Long(request.getParameter("oficina")).longValue();
                long l_cargo = new Long(request.getParameter("cargo")).longValue();
                int salir = 0;
		Usuarios user = new Usuarios();
		SysUsu sU = new SysUsu();
                TrataClave tC = new TrataClave();

                /*Comprobacion de las claves */
                if ((request.getParameter("password")!=null)&&(request.getParameter("password")!="")){
                   str_pass = request.getParameter("password");
                   if ((request.getParameter("repetida")!=null)&&(request.getParameter("repetida")!="")&&(request.getParameter("repetida").equals(str_pass))){
                      String str_repetida = request.getParameter("repetida");
                   }else{
                      salir=2;
                   }
                }else{
                   salir=1;
                }

                switch (salir){
                  case 1:
                  {
%>
                  <script language="javascript">
                  alert("La clave no puede ser vacia");
                  </script>
<%
                  break;
                  }
                  case 2:
                  {
%>
                   <script language="javascript">
                   alert("Por favor, la clave y su repeticion ha de ser la misma.");
                   </script>
<%
                   break;
                  }
                  case 0: //clave lista para ser guardada.
                  {
                  /*Realizo los set */
                  sU.setNombre(str_nombre);
                  sU.setApellido1(str_apellido1);
                  sU.setApellido2(str_apellido2);
                  sU.setSyslogin(str_login);
                  sU.setSysRolPk(i_sysrol);
                  sU.setActivoSn(activo_sn);
                  sU.setOficinaPk(l_oficina);
                  sU.setSysCargosPk (l_cargo);
                  /*inserto el objeto*/
                  try{
                    user.SetNegEvento(accesoBeanId.getNegEventoInstance(3));
                    user.insertarUsu(sU,str_pass);
                  }catch ( Exception eses ){
                         System.out.println("Excepcion Sescam capturada en guardarUsuario.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                  }
%>
                  <script language="javascript">
                        parent.centro.location.href = "mant_usu_main.htm"
                        parent.botonera.location.href = "mant_usu_down.htm"
                  </script>
<%
                   break;
                  }
               }
%>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>