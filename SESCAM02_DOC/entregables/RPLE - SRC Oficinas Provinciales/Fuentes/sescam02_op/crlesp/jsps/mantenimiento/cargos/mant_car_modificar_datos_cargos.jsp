<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<style>
input {background-color: transparent}
</style>
   <link rel="stylesheet" type="text/css" href="../../../css/estilo.css">
<jsp:useBean id="beanCargo" class="java.util.HashMap" scope="session"/>

<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(pk,dato_origen,destino){
  parent.centro.location = "../usuarios/mant_usu_modificar_datos_usuario.jsp?login="+pk;
  parent.botonera.location.href = "../usuarios/mant_usu_botones_modificar.jsp?origen="+dato_origen +"&pk_destino="+destino;
}

</script>

</head>

<BODY class="mainFondo">
<%
		beanCargo.clear();
		Cargos cargo= new Cargos();
		Vector vec = new Vector();
		long l_reqpk = new Long(request.getParameter("carpk")).longValue();
		Long pk = new Long(l_reqpk);
                vec = cargo.busquedaCargo("SYS_CARGOS_PK="+l_reqpk,"");
		SysCargos syscargo=(SysCargos)vec.elementAt(0);
		beanCargo.put("CARGO",syscargo);
		//System.out.print(beanCargo);
%>
<TABLE width="100%" class="textoTop" border = "0">
  <tr>
  	 <td bgcolor=#5C5CAD> Modificar Cargo</td>
  </tr>
</table>
<form name="formulario" action="modificarCargo.jsp" target="oculto1">
<table width="97%" border="0" cellspacing="0" cellpadding="1" vspace="0" hspace="0" align="center">
  <tr>
          <td>&nbsp</td>
          <td>&nbsp</td>
  </tr>
  <tr>
    <td class= "texto">Nombre Cargo: </td>
    <td class= "normal"><input type="text" name="nombre" class="cajatexto2" maxlength = 15 value="<%= syscargo.getSysCargosNombre()%>" ></td>
  </tr>
  <tr>
    <td>&nbsp</td>
    <td>&nbsp</td>
  </tr>
</table>
</form>
<form name="seleccion">
<table width="100%" class = "textoTop" border = "0">
  <tr>
    <td bgcolor=#5C5CAD>Usuarios Asociados </td>
  </tr>
</table>
<br>
<table class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="1" width="100%">
        <tr class="filaCabecera">
            <td width="25%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Nombre</a>
             </td>
            <td width="25%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Apellido 1</a>
             </td>
            <td width="25%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Apellido 2</a>
             </td>
            <td width="15%" height="12px" class="menuOpcion1">
                    <a href="#" class="menuOpcion1">Estado</a>
             </td>
        </tr>
</table>
     <table height="100%" width="100%" align="center" border="1" cellspacing="0" cellpadding="0">
     <tr>
      <td class="areaCentral" >
       <table class="centroTabla" width="100%" id="tablaUsu" cellspacing="0" cellpadding="0" border="1">
       <%
         boolean bFilaPar=true;
         int idCounter=0;
         String s;
         Usuarios usu = new Usuarios();
         Vector v = new Vector();
         v = usu.busquedaUsu("SYS_CARGOS_PK =" +l_reqpk,"");
         if (v!=null){
                 for(int int_pos = 0;int_pos < v.size(); int_pos++){
                         SysUsu sysusu=(SysUsu)v.elementAt(int_pos);
                         String login_usu = sysusu.getSyslogin();
                         if (sysusu.getActivoSn()==1) {
                          s = "Alta";
                         }else { s = "Baja"; }
                         if (bFilaPar){
                                          bFilaPar=false;
 %>
                                 <tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaUsu'].rows, this.id);selec('<%=login_usu%>',1,<%=l_reqpk%>);" style="cursor:hand;">
 <%				}else{
                                         bFilaPar=true;
 %>
                                 <tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaUsu'].rows, this.id);selec('<%=login_usu%>',1,<%=l_reqpk%>);" style="cursor:hand;">
 <%
                         }
 %>
                          <td width="24%" height="12px"><%=sysusu.getNombre()%></td>
                          <td width="25%" height="12px"><%=sysusu.getApellido1()%></td>
                          <td width="25%" height="12px"><%=sysusu.getApellido2()%></td>
                          <td width="13%" height="12px"><%=s%></td>

                    </tr>
<%
                           idCounter++;
                   }
         }
%>
	</table>
        </td>
      </tr>
    </table>
</form>
</BODY>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</HTML>
