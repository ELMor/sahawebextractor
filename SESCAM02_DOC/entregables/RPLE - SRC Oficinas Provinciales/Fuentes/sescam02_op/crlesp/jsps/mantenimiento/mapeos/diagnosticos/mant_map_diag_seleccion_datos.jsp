<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,beans.ConsultaMapeosDiag" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
<script language="javascript" type="text/javascript" src="../../../../javascript/menus.js"></script>
<script language="javascript">
var FilaSelected=-1;
function seleccionaFila(filas, id){
    if (FilaSelected!=-1){
        if((FilaSelected%2)==0)
          filas[FilaSelected].className = "filaPar";
        else
          filas[FilaSelected].className = "filaImpar";
       }
    filas[id].className = "filaSelecionada";
    FilaSelected=id;
    }

function selec(coddiag,centro){
  parent.parent.centro.location='mant_map_diag_modificar_datos_servicio.jsp?coddiag='+coddiag+'&centro='+centro;
  parent.parent.botonera.location.href = "mant_map_diag_botones_modificar.jsp"

}

</script>

</head>
<body id="sel" class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" onload="">
<form name="seleccion">
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table height="100%" width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td class="areaCentral" >
	<table class="centroTabla" width="100%" id="tablaPac" cellspacing="0" cellpadding="0" border="0">
		<%
		boolean bFilaPar=true;
		int idCounter=0;
                long param_centro = new Double(request.getParameter("valor_centro")).longValue();
		if (param_centro >= 0) {
                  Vector v = new Vector();
                  String cod_sescam="";
                  String cod_mapeo = "";
                  String descrip = "";
                  String orden = "";
                  cod_sescam = request.getParameter("codigosescam");
                  cod_mapeo = request.getParameter("codigo");
                  descrip = request.getParameter("descrip");
                  orden = request.getParameter("orden");
                  ConsultaMapeosDiag consulta = new ConsultaMapeosDiag();
                  consulta.setCentro(param_centro);
                  consulta.setCodigoMapeo(cod_mapeo);
                  consulta.setCodigoSescam(cod_sescam);
                  consulta.setDescripSescam(descrip);
                  consulta.setOrden(orden);
                  v = consulta.MapeosDiagnosticos();
                  if (v!=null){
			for(int int_pos = 0;int_pos < v.size(); int_pos++){
				DiagnosticosMapeados diag=(DiagnosticosMapeados)v.elementAt(int_pos);

				if (bFilaPar){
	      			   	bFilaPar=false;
	%>
					<tr class="filaPar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=diag.CodDiagMapeado%>',<%=diag.centro%>)" style="cursor:hand;">
	<%
                                }else{
						bFilaPar=true;
	%>
						<tr class="filaImpar" id=<%=idCounter%> onclick="seleccionaFila(document.all['tablaPac'].rows, this.id);selec('<%=diag.CodDiagMapeado%>',<%=diag.centro%>)" style="cursor:hand;">
	<%
				}
	%>
				<td width="20%" height="12px"><%=diag.CodDiagSESCAM%></td>
	   			<td width="40%" height="12px"><%=diag.DiagDesc%></td>
	   			<td width="20%" height="12px"><%=diag.centro%></td>
                                <td width="20%" height="12px"><%=diag.CodDiagMapeado%></td>
			   	</tr>
<%
  				idCounter++;
  			}
		}
        }
%>
	</table>
    </td>
   </tr>
   </table>
   </td>
  </tr>
</table>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>