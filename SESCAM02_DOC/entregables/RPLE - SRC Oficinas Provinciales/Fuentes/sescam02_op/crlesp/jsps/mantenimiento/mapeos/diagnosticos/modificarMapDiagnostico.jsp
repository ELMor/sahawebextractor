<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../../css/estilo.css">
<jsp:useBean id="beanMapeoDiagnosticos" class="java.util.HashMap" scope="session"/>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
                long diag_pk = new Long(request.getParameter("diagpk")).longValue();
                SesDiagMap sDM =  (SesDiagMap)beanMapeoDiagnosticos.get("MAPEODIAGNOSTICO");
                MapeoDiag mapdiag = new MapeoDiag();

                /*Realizo los set */
                sDM.setDiagPk(diag_pk);

                try{
                 mapdiag.SetNegEvento(accesoBeanId.getNegEventoInstance(33));
                 /*modifico el objeto*/
                 mapdiag.modificarDiagMap(sDM);
                }catch(ExceptionSESCAM eses){
                      System.out.println("Excepcion Sescam capturada en modificarMapDiagnostico.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                 }
       		beanMapeoDiagnosticos.clear();
%>

                 <script language="javascript">
                    parent.centro.location.href = "mant_map_diag_main.htm"
                    parent.botonera.location.href = "mant_map_diag_down.htm"
		</script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>