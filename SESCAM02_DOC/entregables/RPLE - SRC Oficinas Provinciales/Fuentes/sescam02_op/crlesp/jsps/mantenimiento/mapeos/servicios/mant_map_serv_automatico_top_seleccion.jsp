<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.exceptions.ExceptionSESCAM" %>
<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
   <link rel="stylesheet" type="text/css" href="../../../../css/estilo.css">
</head>
<%
    long l_centro = new Double (request.getParameter("centro")).longValue();
    String desc_centro="";

    Centros cen = new Centros();
    if (l_centro!=0){
          Vector v = new Vector();
          v = cen.busquedaCentro("CODCENTRO="+l_centro,"");
          if (v.size()>0){
            Centrosescam cs = (Centrosescam) v.elementAt(0);
            desc_centro = cs.getDescentro();
          }
    }else{
      desc_centro = "TODOS";
    }
%>
<body class="mainFondo">
<table class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="1" width="100%">
        <tr class="filaCabecera">
            <td width="65%" height="12px" class="menuOpcion1"><a href="#" class="menuOpcion1">SESCAM</a></td>
            <td width="35%" height="12px" class="menuOpcion1"><a href="#" class="menuOpcion1"><%=desc_centro%></a></td>
        </tr>
</table>
<layer><!-- Ignorado por Explorer, mueve la capa en Navigator o el frame completo en Explorer -->
<table width ="100%" class="cabeceraTabla"  cellspacing="1" cellpadding="1" border="0">
       <tr class="filaCabecera">
            <td width="15%" height="12px" class="menuOpcion1">
	    	<a href="#" class="menuOpcion1">Codigo</a>
            </td>
            <td width="50%" height="12px" class="menuOpcion1">
                <a href="#" class="menuOpcion1">Descripcion </a>
            </td>
            <td width="15%" height="12px" class="menuOpcion1">
                <a href="#" class="menuOpcion1">Centro</a>
            </td>
            <td width="20%" height="12px" class="menuOpcion1">
                <a href="#" class="menuOpcion1">Codigo Mapeado</a>
            </td>
        </tr>
</table>
</layer>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>