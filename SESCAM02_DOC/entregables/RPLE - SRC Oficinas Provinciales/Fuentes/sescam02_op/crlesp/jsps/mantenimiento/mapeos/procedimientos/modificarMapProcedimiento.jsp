<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,isf.util.TrataClave,isf.exceptions.ExceptionSESCAM" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>

<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../../css/estilo.css">
<jsp:useBean id="beanMapeoProcedimientos" class="java.util.HashMap" scope="session"/>
</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

		<%
		/* Recojo los parametros */
                long proc_pk = new Long(request.getParameter("procpk")).longValue();
                SesProcMap sPM =  (SesProcMap)beanMapeoProcedimientos.get("MAPEOPROCEDIMIENTO");
                MapeoProc mapproc = new MapeoProc();

                /*Realizo los set */
                sPM.setProcPk(proc_pk);

                try{
                 mapproc.SetNegEvento(accesoBeanId.getNegEventoInstance(39));
                 /*modifico el objeto*/
                 mapproc.modificarProcMap(sPM);
                }catch(ExceptionSESCAM eses){
                      System.out.println("Excepcion Sescam capturada en modificarMapProcedimiento.jsp");
%>
                        <script language="javascript">
                        alert("<%=eses%>");
                        </script>
<%
                 }
       		beanMapeoProcedimientos.clear();
%>

                 <script language="javascript">
                    parent.centro.location.href = "mant_map_proc_main.htm"
                    parent.botonera.location.href = "mant_map_proc_down.htm"
		</script>

</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>