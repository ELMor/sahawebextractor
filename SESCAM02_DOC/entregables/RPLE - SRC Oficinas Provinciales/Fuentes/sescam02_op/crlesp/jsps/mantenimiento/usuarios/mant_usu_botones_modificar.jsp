<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../css/estilo.css">
<%
  int ncar_min;
  FicheroClave fC = new FicheroClave();
  ncar_min = fC.DameLongitudClaveMin();
%>

</script>

<script language="javascript" type="text/javascript" src="../../../javascript/botonera.js"></script>
<script language="javascript">
  function control_longitud(n){
       var control=false;
       if ((parent.centro.document.formulario.accion.value=="m")&&(parent.centro.document.formulario.repetida.value=="")&&(parent.centro.document.formulario.password.value=="")){
          control=true;
       }else{
          if ((parent.centro.document.formulario.password.value.length>=n) && (parent.centro.document.formulario.repetida.value.length>=n)) {
           control=true;
          }else{
           alert("La longitud minima de la clave ha de ser de "+n+" caracteres");
          }
       }
       return control;
}

  function guardar(dato_origen,pk,pk_perm,n) {
      	if (parent.centro.document.formulario.login.value=="") {
      		alert("El campo LOGIN no pueden ser vacios");
      	}else{
           if (control_longitud(n)){
                respuesta = confirm("Desea guardar los datos?")
      		if (respuesta == true){
                   parent.centro.document.formulario.origen.value = dato_origen;
                   parent.centro.document.formulario.pk_destino.value = pk;
                   parent.centro.document.formulario.pk_perm.value = pk_perm;
                   parent.centro.document.formulario.submit();
               	}
                else {
                  switch (dato_origen) {
                    case 1: {
                      parent.centro.location.href = '../cargos/mant_car_modificar_datos_cargos.jsp?carpk='+pk;
                      parent.botonera.location.href = "../cargos/mant_car_botones_modificar.jsp";
                      break;
                    }
                    case 2:  {
                     parent.centro.location.href = '../oficinas/mant_ofi_modificar_datos_oficina.jsp?ofipk='+pk;
                     parent.botonera.location.href = "../oficinas/mant_ofi_botones_modificar.jsp";
                     break;
                    }
                    case 3:  {
                      if ((pk_perm != null) && (pk_perm > 0)) {
                        parent.centro.location.href = '../perfiles/mant_per_main_modificar.jsp?perpk='+pk+'&pk_destino='+pk_perm;
                      }else {
                        parent.centro.location.href = '../perfiles/mant_per_main_modificar.jsp?perpk='+pk;
                      }
                      parent.botonera.location.href = "../perfiles/mant_per_botones_modificar.jsp";
                    }
                 }
              }
           }
      	}

}

      function cancelar(dato_origen,pk,pk_perm) {
        alert("No se guardar�n los cambios.");
        switch (dato_origen) {
          case 1: {
              parent.centro.location.href = '../cargos/mant_car_modificar_datos_cargos.jsp?carpk='+pk;
              parent.botonera.location.href = "../cargos/mant_car_botones_modificar.jsp";
              break;
          }
         case 2:  {
              parent.centro.location.href = '../oficinas/mant_ofi_modificar_datos_oficina.jsp?ofipk='+pk;
              parent.botonera.location.href = "../oficinas/mant_ofi_botones_modificar.jsp";
              break;
         }
         case 3:  {
           if ((pk_perm != null) && (pk_perm > 0)) {
              parent.centro.location.href = '../perfiles/mant_per_main_modificar.jsp?perpk='+pk+'&pk_destino='+pk_perm;
           }else {
             parent.centro.location.href = '../perfiles/mant_per_main_modificar.jsp?perpk='+pk;
           }
           parent.botonera.location.href = "../perfiles/mant_per_botones_modificar.jsp";
         }
        }
      }
</script>

</head>
<body class="down" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">

<%
  long l_origen = new Long(request.getParameter("origen")).longValue();
  long l_reqpk = new Long(request.getParameter("pk_destino")).longValue();
  long l_perm_pk = 0;
  if (request.getParameter("pk_perm_destino") != null) {
    l_perm_pk = new Long(request.getParameter("pk_perm_destino")).longValue();
  }
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0" height="100%">
  <tr>
    <td  width="20%" class="botonAccion">&nbsp;</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="guardar(<%=l_origen%>,<%=l_reqpk%>,<%=l_perm_pk%>,<%=ncar_min%>);">Guardar</td>
    <td  width="10%" class="botonAccion">&nbsp;</td>
    <td  width="25%" class="botonAccion" onMouseOver="mOver(this)" onMouseOut="mOut(this)" onMouseDown="mDown(this)" onMouseUp="mOver(this)" onclick="cancelar(<%=l_origen%>,<%=l_reqpk%>,<%=l_perm_pk%>);">Cancelar</td>
    <td  width="20%" class="botonAccion">&nbsp;</td>
  </tr>
</table>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
