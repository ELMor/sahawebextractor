<%@ page contentType="text/html;charset=ISO-8859-1"%>
<%@ page language="java" errorPage="error.jsp"%>
<%@ page import="isf.negocio.*,isf.persistencia.*,java.util.*,conf.FicheroClave, isf.util.TrataClave" %>

<jsp:useBean id="accesoBeanId" scope="session" class="beans.AccesoBean"/>
<% String srtPermAcceso="acceso_Mant";%>
<%@ include file="/jsps/control_acceso.txt" %>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<link rel=stylesheet type="text/css" href="../../../../css/estilo.css">
</head>
<script language="javascript">
  function hacer_submit() {
     var cadena_texto;
     cadena_texto = this.document.formulario.codigosescam.value.toUpperCase();
     this.document.formulario.codigosescam.value = cadena_texto;
     cadena_texto = this.document.formulario.descrip.value.toUpperCase();
     this.document.formulario.descrip.value = cadena_texto;
     cadena_texto = this.document.formulario.codigo.value.toUpperCase();
     this.document.formulario.codigo.value = cadena_texto;
     cadena_texto = this.document.formulario.descripmapeo.value.toUpperCase();
     this.document.formulario.descripmapeo.value = cadena_texto;
     this.document.formulario.submit();
   }
</script>
</head>
<body class="mainFondo" bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
<form name="formulario" action="mant_map_seleccion_datos.jsp" target="datos">
<table width="100%" border="0" cellspacing="4" cellpadding="0" vspace="0" hspace="0">
 <tr>
   <td class="texto" width="50%"><a>Sescam</a></td>
   <td class="texto" width="50%"><a>Centro</a></td>
 </tr>
 <tr>
  <td>
   <table width="100%" border="1" cellspacing="1" cellpadding="1" vspace="1" hspace="1">
    <tr>
     <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="1" vspace="0" hspace="0">
       <tr>
        <td class= "texto" width="10%" >C�digo: </td>
        <td class= "normal"width="15%"><input type="text" name="codigosescam" class="cajatexto2" maxlength = 10 style="width:25%" value="" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
       </tr>
       <tr>
        <td class= "texto" width="15%">Descripci�n: </td>
        <td class= "normal" width="55%"><input type="text" name="descrip" class="cajatexto3" maxlength = 80 style="width:100%" value="" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
       </tr>
       <tr>
         <td>&nbsp;</td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
  <td>
   <table width="100%" border="1" cellspacing="1" cellpadding="1" vspace="1" hspace="1">
    <tr>
     <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" vspace="0" hspace="0">
       <tr>
        <td class= "texto" width="15%">C�digo:</td>
        <td class= "normal"width="85%"><input type="text" style="width:25%" name="codigo" class="cajatexto2" maxlength = 10 value="" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
       </tr>
       <tr>
        <td class= "texto" width="15%">Descripci�n: </td>
        <td class= "normal" width="85%"><input type="text" name="descripmapeo" class="cajatexto3" maxlength = 80 style="width:100%" value="" onkeypress = "if (event.keyCode == 13) hacer_submit();"></td>
       </tr>
       <tr>
        <td class= "texto" width="15%">Centro:</td>
        <td class= "normal" width="85%"><div id="divCentro">
          <Select name="valor_centro" class="cajatexto2" style="width:90%">
             <option  value='0' selected>TODOS</option>
<%
           Centros centro = new Centros();
           Vector v = new Vector();
           v = centro.busquedaCentro("","CODCENTRO ASC");
           for(int int_pos = 0;int_pos < v.size(); int_pos++){
                Centrosescam cs=(Centrosescam)v.elementAt(int_pos);
%>
                <option  value='<%=cs.getCodcentro()%>'><%=cs.getDescentro()%></option>
<%
            }
%>
           </Select></div>
        </td>
       </tr>
      </table>
     </td>
    </tr>
   </table>
  </td>
 </tr>
</table>
<td><input type="hidden" value="cod_sescam" name="orden"></td>
</form>
</body>
<HEAD>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
</html>
