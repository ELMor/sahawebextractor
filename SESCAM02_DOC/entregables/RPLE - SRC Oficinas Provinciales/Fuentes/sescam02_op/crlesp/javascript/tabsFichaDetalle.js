function detalle()
{
   document.all.tab_det_entrada.style.visibility = "visible";
   document.all.tab_solic.style.visibility = "hidden";
   document.all.tab_histo.style.visibility = "hidden";
   document.all.sel_detalle.className="pestanaEnableInterno";
   document.all.sel_solicitudes.className="pestanaDisableInterno";
   document.all.sel_historia.className="pestanaDisableInterno";
}

function solicitudes()
{
    document.all.tab_det_entrada.style.visibility = "hidden";
    document.all.tab_solic.style.visibility = "visible";
    document.all.tab_histo.style.visibility = "hidden";
    document.all.sel_detalle.className="pestanaDisableInterno";
    document.all.sel_solicitudes.className="pestanaEnableInterno";
    document.all.sel_historia.className="pestanaDisableInterno";
}

function historia()
{
  document.all.tab_det_entrada.style.visibility = "hidden";
  document.all.tab_solic.style.visibility = "hidden";
  document.all.tab_histo.style.visibility = "visible";
  document.all.sel_detalle.className="pestanaDisableInterno";
  document.all.sel_solicitudes.className="pestanaDisableInterno";
  document.all.sel_historia.className="pestanaEnableInterno";
}

function permisos()
{
   document.all.tab_det_entrada.style.visibility = "visible";
   document.all.tab_usuarios.style.visibility = "hidden";
   document.all.sel_permisos.className="pestanaEnableInterno";
   document.all.sel_usuarios.className="pestanaDisableInterno";
}

function usuarios()
{
    document.all.tab_det_entrada.style.visibility = "hidden";
    document.all.tab_usuarios.style.visibility = "visible";
    document.all.sel_permisos.className="pestanaDisableInterno";
    document.all.sel_usuarios.className="pestanaEnableInterno";
}




