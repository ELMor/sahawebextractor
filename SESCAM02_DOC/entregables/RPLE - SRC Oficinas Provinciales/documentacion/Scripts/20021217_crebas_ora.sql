-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     17/12/2002  09:59                         
-- ============================================================

drop view LESP_UNI_PROCS
/

drop index sys_usu_claves_PK
/

drop index Relation_2290_FK
/

drop table sys_usu_claves cascade constraints
/

drop index eventos_var_PK
/

drop index evtovarevto_FK
/

drop index vartpovar_FK
/

drop table evento_var cascade constraints
/

drop index ses_garan_tram_PK
/

drop index Relation_905_FK
/

drop index Relation_906_FK
/

drop index Relation_907_FK
/

drop table ses_garan_tram cascade constraints
/

drop index sys_perm_rol_PK
/

drop index Relation_28_FK
/

drop index Relation_29_FK
/

drop table sys_perm_rol cascade constraints
/

drop index evento_tipo_var_PK
/

drop index tpovarevto_FK
/

drop table evento_tipo_var cascade constraints
/

drop index eventos_PK
/

drop index evtotipoevto_FK
/

drop index sess_evto_FK
/

drop index Relation_2742_FK
/

drop table evento cascade constraints
/

drop index evento_session_PK
/

drop index sess_usuario_FK
/

drop index Relation_2073_FK
/

drop table evento_session cascade constraints
/

drop index sys_usu_PK
/

drop index Relation_19_FK
/

drop index Relation_423_FK
/

drop index Relation_433_FK
/

drop table sys_usu cascade constraints
/

drop index ses_oficina_PK
/

drop index Relation_504_FK
/

drop index Relation_1047_FK
/

drop table ses_oficina cascade constraints
/

drop index ses_localidades_PK
/

drop index Relation_445_FK
/

drop table ses_localidades cascade constraints
/

drop index evento_tipo_PK
/

drop table evento_tipo cascade constraints
/

drop index ses_garantia_PK
/

drop index ses_gar_centro_norden
/

drop table ses_garantia cascade constraints
/

drop index ses_provincias_PK
/

drop table ses_provincias cascade constraints
/

drop index sys_cargos_PK
/

drop table sys_cargos cascade constraints
/

drop index sys_permisos_PK
/

drop table sys_permisos cascade constraints
/

drop index sys_roles_PK
/

drop table sys_roles cascade constraints
/

drop index ses_mot_salida_PK
/

drop table ses_mot_salida cascade constraints
/

drop index ses_procedimientos_PK
/

drop index ses_proc_cod
/

drop table ses_procedimientos cascade constraints
/

drop table clave cascade constraints
/

-- ============================================================
--   Table: clave                                              
-- ============================================================
create table clave
(
    clave_numero_actual  NUMBER(10)             null    ,
    clave_nombre         VARCHAR2(40)           null    
)
/

-- ============================================================
--   Table: ses_procedimientos                                 
-- ============================================================
create table ses_procedimientos
(
    proc_pk              INTEGER                not null,
    proc_cod             VARCHAR2(10)           null    ,
    proc_desc_corta      VARCHAR2(80)           null    ,
    proc_desc            VARCHAR2(255)          null    ,
    proc_activo          INTEGER                null    ,
    proc_garantia        INTEGER                null    ,
    proc_monto_max_est   NUMBER(19,7)           null    ,
    constraint PK_SES_PROCEDIMIENTOS primary key (proc_pk)
)
/

-- ============================================================
--   Index: ses_proc_cod                                       
-- ============================================================
create index ses_proc_cod on ses_procedimientos (proc_cod asc)
/

-- ============================================================
--   Table: ses_mot_salida                                     
-- ============================================================
create table ses_mot_salida
(
    motsal_cod           INTEGER                not null,
    motsal_desc          VARCHAR2(128)          null    ,
    constraint PK_SES_MOT_SALIDA primary key (motsal_cod)
)
/

-- ============================================================
--   Table: sys_roles                                          
-- ============================================================
create table sys_roles
(
    sys_rol_pk           INTEGER                not null,
    sys_rol_nombre       VARCHAR2(30)           null    ,
    sys_rol_desc         VARCHAR2(255)          null    ,
    constraint PK_SYS_ROLES primary key (sys_rol_pk)
)
/

-- ============================================================
--   Table: sys_permisos                                       
-- ============================================================
create table sys_permisos
(
    sys_perm_pk          INTEGER                not null,
    sys_perm_nombre      VARCHAR2(30)           null    ,
    sys_perm_desc        VARCHAR2(255)          null    ,
    constraint PK_SYS_PERMISOS primary key (sys_perm_pk)
)
/

-- ============================================================
--   Table: sys_cargos                                         
-- ============================================================
create table sys_cargos
(
    sys_cargos_pk        INTEGER                not null,
    sys_cargos_nombre    VARCHAR2(15)           null    ,
    constraint PK_SYS_CARGOS primary key (sys_cargos_pk)
)
/

-- ============================================================
--   Table: ses_provincias                                     
-- ============================================================
create table ses_provincias
(
    provincia_pk         INTEGER                not null,
    provincia            VARCHAR2(20)           null    ,
    constraint PK_SES_PROVINCIAS primary key (provincia_pk)
)
/

-- ============================================================
--   Table: ses_garantia                                       
-- ============================================================
create table ses_garantia
(
    gar_pk               INTEGER                not null,
    NORDEN               NUMBER                 null    ,
    CENTRO               NUMBER                 null    ,
    gar_tipo             INTEGER                null    ,
    gar_estado           INTEGER                null    ,
    gar_monto            NUMBER(19,7)           null    ,
    gar_motivo_rech      VARCHAR2(4000)         null    ,
    CEGA                 NUMBER                 null    ,
    NCITA                NUMBER                 null    ,
    gar_cq               CHAR(1)                null    ,
    pac_nss              VARCHAR2(30)           null    ,
    constraint PK_SES_GARANTIA primary key (gar_pk)
)
/

-- ============================================================
--   Index: ses_gar_centro_norden                              
-- ============================================================
create index ses_gar_centro_norden on ses_garantia (CENTRO asc, NORDEN asc)
/

-- ============================================================
--   Table: evento_tipo                                        
-- ============================================================
create table evento_tipo
(
    tpoevto_pk           INTEGER                not null,
    tpoevto_desc         VARCHAR2(60)           null    ,
    tpoevto_act          SMALLINT               null    ,
    constraint PK_EVENTO_TIPO primary key (tpoevto_pk)
)
/

-- ============================================================
--   Table: ses_localidades                                    
-- ============================================================
create table ses_localidades
(
    pk_localidad         INTEGER                not null,
    provincia_pk         INTEGER                null    ,
    localidad            VARCHAR2(30)           null    ,
    constraint PK_SES_LOCALIDADES primary key (pk_localidad)
)
/

-- ============================================================
--   Index: Relation_445_FK                                    
-- ============================================================
create index Relation_445_FK on ses_localidades (provincia_pk asc)
/

-- ============================================================
--   Table: ses_oficina                                        
-- ============================================================
create table ses_oficina
(
    oficina_pk           INTEGER                not null,
    pk_localidad         INTEGER                null    ,
    syslogin             VARCHAR2(8)            null    ,
    oficina_nombre       VARCHAR2(20)           null    ,
    oficina_direccion    VARCHAR2(30)           null    ,
    oficina_telefono     INTEGER                null    ,
    oficina_codpostal    INTEGER                null    ,
    constraint PK_SES_OFICINA primary key (oficina_pk)
)
/

-- ============================================================
--   Index: Relation_504_FK                                    
-- ============================================================
create index Relation_504_FK on ses_oficina (pk_localidad asc)
/

-- ============================================================
--   Index: Relation_1047_FK                                   
-- ============================================================
create index Relation_1047_FK on ses_oficina (syslogin asc)
/

-- ============================================================
--   Table: sys_usu                                            
-- ============================================================
create table sys_usu
(
    syslogin             VARCHAR2(8)            not null,
    sys_rol_pk           INTEGER                null    ,
    sys_cargos_pk        INTEGER                null    ,
    oficina_pk           INTEGER                null    ,
    syspwd               VARCHAR2(8)            null    ,
    apellido1            VARCHAR2(20)           null    ,
    apellido2            VARCHAR2(20)           null    ,
    nombre               VARCHAR2(20)           null    ,
    activo_sn            INTEGER                null    ,
    fecha_bloqueo        DATE                   null    ,
    constraint PK_SYS_USU primary key (syslogin)
)
/

-- ============================================================
--   Index: Relation_19_FK                                     
-- ============================================================
create index Relation_19_FK on sys_usu (sys_rol_pk asc)
/

-- ============================================================
--   Index: Relation_423_FK                                    
-- ============================================================
create index Relation_423_FK on sys_usu (sys_cargos_pk asc)
/

-- ============================================================
--   Index: Relation_433_FK                                    
-- ============================================================
create index Relation_433_FK on sys_usu (oficina_pk asc)
/

-- ============================================================
--   Table: evento_session                                     
-- ============================================================
create table evento_session
(
    sess_pk              INTEGER                not null,
    syslogin             VARCHAR2(8)            null    ,
    oficina_pk           INTEGER                null    ,
    sess_fecha_inicio    DATE                   default sysdate not null,
    sess_id              VARCHAR2(255)          not null,
    sess_srv_rmt         VARCHAR2(20)           null    ,
    sess_acc_cod         INTEGER                null    ,
    constraint PK_EVENTO_SESSION primary key (sess_pk)
)
/

-- ============================================================
--   Index: sess_usuario_FK                                    
-- ============================================================
create index sess_usuario_FK on evento_session (syslogin asc)
/

-- ============================================================
--   Index: Relation_2073_FK                                   
-- ============================================================
create index Relation_2073_FK on evento_session (oficina_pk asc)
/

-- ============================================================
--   Table: evento                                             
-- ============================================================
create table evento
(
    evto_pk              INTEGER                not null,
    tpoevto_pk           INTEGER                null    ,
    sess_pk              INTEGER                null    ,
    gar_pk               INTEGER                null    ,
    gar_operacion        INTEGER                null    ,
    constraint PK_EVENTO primary key (evto_pk)
)
/

-- ============================================================
--   Index: evtotipoevto_FK                                    
-- ============================================================
create index evtotipoevto_FK on evento (tpoevto_pk asc)
/

-- ============================================================
--   Index: sess_evto_FK                                       
-- ============================================================
create index sess_evto_FK on evento (sess_pk asc)
/

-- ============================================================
--   Index: Relation_2742_FK                                   
-- ============================================================
create index Relation_2742_FK on evento (gar_pk asc)
/

-- ============================================================
--   Table: evento_tipo_var                                    
-- ============================================================
create table evento_tipo_var
(
    tpoevto_var_pk       INTEGER                not null,
    tpoevto_pk           INTEGER                null    ,
    tpoevto_var_desc     VARCHAR2(60)           null    ,
    tpoevto_var_act      SMALLINT               null    ,
    constraint PK_EVENTO_TIPO_VAR primary key (tpoevto_var_pk)
)
/

-- ============================================================
--   Index: tpovarevto_FK                                      
-- ============================================================
create index tpovarevto_FK on evento_tipo_var (tpoevto_pk asc)
/

-- ============================================================
--   Table: sys_perm_rol                                       
-- ============================================================
create table sys_perm_rol
(
    sys_rol_pk           INTEGER                not null,
    sys_perm_pk          INTEGER                not null,
    constraint PK_SYS_PERM_ROL primary key (sys_rol_pk, sys_perm_pk)
)
/

-- ============================================================
--   Index: Relation_28_FK                                     
-- ============================================================
create index Relation_28_FK on sys_perm_rol (sys_perm_pk asc)
/

-- ============================================================
--   Index: Relation_29_FK                                     
-- ============================================================
create index Relation_29_FK on sys_perm_rol (sys_rol_pk asc)
/

-- ============================================================
--   Table: ses_garan_tram                                     
-- ============================================================
create table ses_garan_tram
(
    gar_tram_pk          INTEGER                not null,
    gar_pk               INTEGER                null    ,
    syslogin             VARCHAR2(8)            null    ,
    oficina_pk           INTEGER                null    ,
    gar_fechor           DATE                   default sysdate null    ,
    gar_repleg_dni       VARCHAR2(15)           null    ,
    gar_repleg_apenom    VARCHAR2(256)          null    ,
    gar_operacion        INTEGER                null    ,
    constraint PK_SES_GARAN_TRAM primary key (gar_tram_pk)
)
/

-- ============================================================
--   Index: Relation_905_FK                                    
-- ============================================================
create index Relation_905_FK on ses_garan_tram (gar_pk asc)
/

-- ============================================================
--   Index: Relation_906_FK                                    
-- ============================================================
create index Relation_906_FK on ses_garan_tram (syslogin asc)
/

-- ============================================================
--   Index: Relation_907_FK                                    
-- ============================================================
create index Relation_907_FK on ses_garan_tram (oficina_pk asc)
/

-- ============================================================
--   Table: evento_var                                         
-- ============================================================
create table evento_var
(
    evto_var_pk          INTEGER                not null,
    evto_pk              INTEGER                null    ,
    tpoevto_var_pk       INTEGER                null    ,
    evto_var_valor       VARCHAR2(255)          null    ,
    constraint PK_EVENTO_VAR primary key (evto_var_pk)
)
/

-- ============================================================
--   Index: evtovarevto_FK                                     
-- ============================================================
create index evtovarevto_FK on evento_var (evto_pk asc)
/

-- ============================================================
--   Index: vartpovar_FK                                       
-- ============================================================
create index vartpovar_FK on evento_var (tpoevto_var_pk asc)
/

-- ============================================================
--   Table: sys_usu_claves                                     
-- ============================================================
create table sys_usu_claves
(
    sysclv_pk            INTEGER                not null,
    syslogin             VARCHAR2(8)            null    ,
    sysclv_fecha         DATE                   default sysdate null    ,
    sysclv_clave         VARCHAR2(20)           null    ,
    constraint PK_SYS_USU_CLAVES primary key (sysclv_pk)
)
/

-- ============================================================
--   Index: Relation_2290_FK                                   
-- ============================================================
create index Relation_2290_FK on sys_usu_claves (syslogin asc)
/

-- ============================================================
--   View: LESP_UNI_PROCS                                      
-- ============================================================
create or replace view LESP_UNI_PROCS as
SELECT distinct cpr1 cod, dpr1 descrip 
 FROM lesp
 WHERE cpr1 is not null and cpr1 not in (SELECT proc_cod FROM ses_procedimientos WHERE cpr1=proc_cod) 
UNION 
SELECT distinct prestaci cod, prestaci_des descrip 
FROM lespcex 
WHERE prestaci is not null and prestaci not in (SELECT proc_cod FROM ses_procedimientos WHERE prestaci=proc_cod)
/

alter table ses_localidades
    add constraint FK_SES_LOCA_RELATION__SES_PROV foreign key  (provincia_pk)
       references ses_provincias (provincia_pk)
/

alter table ses_oficina
    add constraint FK_SES_OFIC_RELATION__SES_LOCA foreign key  (pk_localidad)
       references ses_localidades (pk_localidad)
/

alter table ses_oficina
    add constraint FK_SES_OFIC_RELATION__SYS_USU foreign key  (syslogin)
       references sys_usu (syslogin)
/

alter table sys_usu
    add constraint FK_SYS_USU_RELATION__SYS_ROLE foreign key  (sys_rol_pk)
       references sys_roles (sys_rol_pk)
/

alter table sys_usu
    add constraint FK_SYS_USU_RELATION__SYS_CARG foreign key  (sys_cargos_pk)
       references sys_cargos (sys_cargos_pk)
/

alter table sys_usu
    add constraint FK_SYS_USU_RELATION__SES_OFIC foreign key  (oficina_pk)
       references ses_oficina (oficina_pk)
/

alter table evento_session
    add constraint FK_EVENTO_S_RELATION__SES_OFIC foreign key  (oficina_pk)
       references ses_oficina (oficina_pk)
/

alter table evento
    add constraint FK_EVENTO_EVTOTIPOE_EVENTO_T foreign key  (tpoevto_pk)
       references evento_tipo (tpoevto_pk)
/

alter table evento
    add constraint FK_EVENTO_SESS_EVTO_EVENTO_S foreign key  (sess_pk)
       references evento_session (sess_pk)
/

alter table evento
    add constraint FK_EVENTO_RELATION__SES_GARA foreign key  (gar_pk)
       references ses_garantia (gar_pk)
/

alter table evento_tipo_var
    add constraint FK_EVENTO_T_TPOVAREVT_EVENTO_T foreign key  (tpoevto_pk)
       references evento_tipo (tpoevto_pk)
/

alter table sys_perm_rol
    add constraint FK_SYS_PERM_RELATION__SYS_PERM foreign key  (sys_perm_pk)
       references sys_permisos (sys_perm_pk)
/

alter table sys_perm_rol
    add constraint FK_SYS_PERM_RELATION__SYS_ROLE foreign key  (sys_rol_pk)
       references sys_roles (sys_rol_pk)
/

alter table ses_garan_tram
    add constraint FK_SES_GARA_RELATION__SES_GARA foreign key  (gar_pk)
       references ses_garantia (gar_pk)
/

alter table ses_garan_tram
    add constraint FK_SES_GARA_RELATION__SYS_USU foreign key  (syslogin)
       references sys_usu (syslogin)
/

alter table ses_garan_tram
    add constraint FK_SES_GARA_RELATION__SES_OFIC foreign key  (oficina_pk)
       references ses_oficina (oficina_pk)
/

alter table evento_var
    add constraint FK_EVENTO_V_EVTOVAREV_EVENTO foreign key  (evto_pk)
       references evento (evto_pk)
/

alter table evento_var
    add constraint FK_EVENTO_V_VARTPOVAR_EVENTO_T foreign key  (tpoevto_var_pk)
       references evento_tipo_var (tpoevto_var_pk)
/

alter table sys_usu_claves
    add constraint FK_SYS_USU__RELATION__SYS_USU foreign key  (syslogin)
       references sys_usu (syslogin)
/

