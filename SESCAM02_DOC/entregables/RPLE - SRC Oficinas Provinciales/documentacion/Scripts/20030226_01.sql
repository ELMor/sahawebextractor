create table ses_proc_garantia
(
 	procgar_pk		     integer						,
    proc_pk              INTEGER                		,    
    sesesp_cod		     VARCHAR2(4)            		,
	proc_garantia		 number							,
	proc_activo			 number							,
    constraint PK_SES_PROC_GARANTIA primary key (procgar_pk)
)
/

create index ses_proc_garantia_ses_proc_FK on ses_proc_garantia (proc_pk asc)
/

alter table ses_proc_garantia
    add constraint FK_SES_PROC_GAR_SES_PROC foreign key  (proc_pk)
       references ses_procedimientos (proc_pk)
/
create index ses_proc_garantia_ses_esp_FK on ses_proc_garantia (sesesp_cod asc)
/

alter table ses_proc_garantia
    add constraint FK_SES_PROC_GAR_SES_ESP foreign key  (sesesp_cod)
       references ses_especialidades (sesesp_cod)
/
commit;