-- ============================================================
--   Database name:  sescam                                    
--   DBMS name:      ORACLE Version 8                          
--   Created on:     31/01/2003  10:30                         
-- ============================================================

create table ses_diagnosticos
(
    diag_pk              INTEGER                not null,
    diag_cod             VARCHAR2(32)           null    ,
    diag_desc            VARCHAR2(255)          null    ,
    constraint PK_SES_DIAGNOSTICOS primary key (diag_pk)
)
/

create table ses_especialidades
(
    sesesp_cod           VARCHAR2(4)            not null,
    sesesp_desc          VARCHAR2(30)           null    ,
    constraint PK_SES_ESPECIALIDADES primary key (sesesp_cod)
)
/

create table ses_servicios
(
    sessrv_cod           VARCHAR2(4)            not null,
    sesesp_cod           VARCHAR2(4)            null    ,
    sessrv_desc          VARCHAR2(35)           null    ,
    constraint PK_SES_SERVICIOS primary key (sessrv_cod)
)
/

create table ses_srv_map
(
    sessrv_cod           VARCHAR2(4)            not null,
    centro               NUMBER                 not null,
    srv_cen_cod          VARCHAR2(30)           not null,
    constraint PK_SES_SRV_MAP primary key (centro, srv_cen_cod)
)
/

create table ses_proc_map
(
    proc_pk              INTEGER                not null,
    centro               NUMBER                 not null,
    proc_cen_cod         VARCHAR2(32)           not null,
    constraint PK_SES_PROC_MAP primary key (centro, proc_cen_cod)
)
/

create table ses_diag_map
(
    diag_pk              INTEGER                not null,
    centro               NUMBER                 not null,
    diag_cen_cod         VARCHAR2(32)           not null,
    constraint PK_SES_DIAG_MAP primary key (centro, diag_cen_cod)
)
/

create table ses_clasificaciones
(
    sesclas_pk           INTEGER                not null,
    proc_pk              INTEGER                null    ,
    sesclas_pkpadre      INTEGER                null    ,
    sesclas_niveles      INTEGER                null    ,
    sesclas_desc         VARCHAR2(255)          null    ,
    constraint PK_SES_CLASIFICACIONES primary key (sesclas_pk)
)
/

alter table ses_procedimientos
    add     proc_tipo            INTEGER                null    
/

create index Relation_3238_FK on ses_servicios (sesesp_cod asc)
/

create index Relation_2970_FK on ses_srv_map (sessrv_cod asc)
/

create index Relation_2980_FK on ses_proc_map (proc_pk asc)
/

create index Relation_2993_FK on ses_diag_map (diag_pk asc)
/

create index nodo_hoja_FK on ses_clasificaciones (proc_pk asc)
/

create index padre_FK on ses_clasificaciones (sesclas_pkpadre asc)
/

alter table ses_servicios
    add constraint FK_SES_SERV_RELATION__SES_ESPE foreign key  (sesesp_cod)
       references ses_especialidades (sesesp_cod)
/

alter table ses_srv_map
    add constraint FK_SES_SRV__RELATION__SES_SERV foreign key  (sessrv_cod)
       references ses_servicios (sessrv_cod)
/

alter table ses_proc_map
    add constraint FK_SES_PROC_RELATION__SES_PROC foreign key  (proc_pk)
       references ses_procedimientos (proc_pk)
/

alter table ses_diag_map
    add constraint FK_SES_DIAG_RELATION__SES_DIAG foreign key  (diag_pk)
       references ses_diagnosticos (diag_pk)
/

alter table ses_clasificaciones
    add constraint FK_SES_CLAS_NODO_HOJA_SES_PROC foreign key  (proc_pk)
       references ses_procedimientos (proc_pk)
/

alter table ses_clasificaciones
    add constraint FK_SES_CLAS_PADRE_SES_CLAS foreign key  (sesclas_pkpadre)
       references ses_clasificaciones (sesclas_pk)
/

commit
/

