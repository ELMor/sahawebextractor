package ssf.sescam.lesp.regcen.db.patcher;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.Logger;
import ssf.utils.Global;

/**
 *
 * <p>Title: Centralizacion de Listas de Espera</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002-2003</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */
public class Patcher {
  private Hashtable params = null;
  private Connection toSescam = null;
  private Connection toCentro = null;
  private String rootDir = null;
  private Properties prop = null;
  private Logger log = null;

  /**
   * Ejemplo de patch.properties
   * #----------------------------------------------------------------------
   * #Gestion de parches de la BBDD SESCAM ---------------------------------
   * #----------------------------------------------------------------------
   *
   * #Parche maximo soportado ----------------------------------------------
   * patch.level.max=0
   * -----------------------------------------------------------------------
   *
   * #----------------------------------------------------------------------
   * #definicion de los parches
   * #----------------------------------------------------------------------
   *
   * #Parche 1 BEGIN -------------------------------------------------------
   *
   * patch.1.procesos  = 3
   * patch.1.1.tipo    = Text
   * patch.1.1.valor   = ALTER TABLE LESP ADD SITU NUMBER
   * patch.1.1.mensaje = Texto a logear
   * patch.1.2.tipo    = File
   * patch.1.2.valor   = patcher/1/filename.sql
   * patch.1.3.tipo    = Class
   * patch.1.3.valor   = ssf.sescam.lesp.regcen.db.SESCAM.patcher.p1.ApplyPatch
   *
   * #Parche 1 END ---------------------------------------------------------
   * @param params Parametros
   */
  public Patcher(Hashtable params) {
    this.params = params;
    toSescam = (Connection) params.get("toSescam");
    toCentro = (Connection) params.get("toCentro");
    rootDir = (String) params.get("rootDir");
    log = Logger.getLogger(getClass());
  }

  /**
   * Comprueba que la BBDD detr�s de conn es parcheable
   * @return si es Parcheable.
   */
  private boolean isPatcherable() {
    log.debug("Comprobando si es parcheable");
    boolean ret = false;
    Statement st = null;
    ResultSet rs = null;
    try {
      st = (toCentro == null ? toSescam : toCentro).createStatement();
      rs = st.executeQuery("select count(*) from patcherparams");
      rs.next();
      ret = true;
    }
    catch (SQLException ex) {
      ret = false;
    }
    finally {
      try {
        st.close();
      }
      catch (SQLException ex1) {
      }
    }
    return ret;
  }

  /**
   * Crea la tabla patcherparams(level integer)
   */
  private void makePatcherable() {
    Statement st = null;
    try {
      st = (toCentro == null ? toSescam : toCentro).createStatement();
      try {
        st.execute("drop table patcherparams");
      }
      catch (SQLException ex2) {
      }
      st.execute("create table patcherparams(nivel integer)");
      st.execute("insert into patcherparams(nivel) values(0)");
    }
    catch (SQLException ex) {
    }
    finally {
      try {
        st.close();
      }
      catch (SQLException ex1) {
      }
    }
  }

  /**
   *
   * @return retorna el nivel de parche de la BBDD
   */
  private int getCurrentLevel() {
    int ret = -1;
    if (!isPatcherable()) {
      return ret;
    }
    Statement st = null;
    ResultSet rs = null;
    try {
      st = (toCentro == null ? toSescam : toCentro).createStatement();
      rs = st.executeQuery("select nivel from patcherparams");
      rs.next();
      ret = rs.getInt("nivel");
    }
    catch (SQLException ex) {
      ret = -1;
    }
    finally {
      try {
        st.close();
      }
      catch (SQLException ex1) {
      }
    }
    return ret;
  }

  /**
   *
   * @return retorna el maximo nivel de parche que podria tener
   */
  private int getMaximumLevel() {
    int ret = -1;
    prop = new Properties();
    try {
      String propFile = rootDir + "patcher/patch.properties";
      InputStream is = ClassLoader.getSystemResourceAsStream(propFile);
      prop.load(is);
      String maxLevel = prop.getProperty("patch.level.max", "-1");
      ret = Integer.parseInt(maxLevel);
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
    catch (NumberFormatException ex) {
    }
    return ret;
  }

  /**
   * Coloca el nivel de parcheo de la BBDD a nivel
   * @param nivel Nivel de parcheo deseado
   */
  private void setCurrentLevel(int nivel) {
    try {
      Statement st = null;
      st = (toCentro == null ? toSescam : toCentro).createStatement();
      st.execute("update patcherparams set nivel=" + nivel);
      st.close();
    }
    catch (SQLException ex) {
    }
  }

  /**
   * Aplica los parches a la BBDD
   */
  public boolean upgradeLevel() {
    log.info("Iniciando sistema de parcheo de BBDD");
    if (managePatch()) {
      return false;
    }
    int curLev = getCurrentLevel();
    int maxLev = getMaximumLevel();
    log.debug("Patcher: curLev=" + curLev + ", maxLev=" + maxLev);
    if (curLev >= maxLev) {
      log.info(
          "Terminando la comprobacion de parches - Ningun parche aplicable");
      return true;
    }
    int i, j;
    //Nos movemos del nivel actual hasta el maximo con i
    for (i = curLev + 1; i <= maxLev; i++) {
      boolean allOK = true;
      String patchRoot = "patch." + i + ".";
      int maxProc = Integer.parseInt(prop.getProperty(patchRoot + "procesos",
          "0"));
      log.debug(patchRoot + "procesos=" + maxProc);
      //Nos movemos en cada proceso con j
      for (j = 1; allOK && j <= maxProc; j++) {
        //Determinamos el tipo de proceso
        String procRoot = patchRoot + j + ".";
        String procTipo = prop.getProperty(procRoot + "tipo");
        String procValor = prop.getProperty(procRoot + "valor");
        String procMsg = prop.getProperty(procRoot + "mensaje",
                                          "<No hay mensaje>");
        String procIgnore = prop.getProperty(procRoot + "ignore", "false");
        log.info("Parche Nivel " + i + " Proceso " + j + "/" + maxProc + ":" +
                 procMsg);
        allOK = procesa(i, j, procTipo, procValor, procMsg, procIgnore);
      }
      if (allOK) {
        setCurrentLevel(i);
        log.info("Upgrade Nivel " + i + " OK");
      }
      else {
        log.warn("Upgrade Nivel " + i + ":Error en el proceso #" +
                 (j - 1));
        return false;
      }
    }
    log.info("Terminando la comprobacion de parches");
    return true;
  }

  private boolean managePatch() {
    boolean mustReturn = false;
    boolean reTry = false;
    if (!isPatcherable()) {
      log.info("No es parcheable...");
      makePatcherable();
      reTry = true;
    }
    if (reTry && !isPatcherable()) {
      log.warn("No se puede hacer parcheable");
      mustReturn = true; //No se puede hacer parcheable
    }
    if (reTry && isPatcherable()) {
      log.info("Ya es parcheable");
    }
    return mustReturn;
  }

  private boolean procesa(int patchLevel, int procNumber, String tipo,
                          String valor, String mensaje, String ignore) {
    if (tipo == null || valor == null) {
      return false;
    }
    log.debug("patchLevel=" + patchLevel + "." + procNumber + ":" + mensaje);
    if (tipo.equalsIgnoreCase("text")) {
      return procesaText(valor, ignore);
    }
    else if (tipo.equalsIgnoreCase("file")) {
      return procesaFile(valor, ignore);
    }
    else if (tipo.equalsIgnoreCase("class")) {
      return procesaClass(valor);
    }
    else {
      return false;
    }
  }

  private boolean procesaText(String texto, String ignore) {
    boolean ret = true;
    Statement st = null;
    try {
      st = (toCentro == null ? toSescam : toCentro).createStatement();
      st.execute(texto);
      st.close();
    }
    catch (SQLException ex) {
      if (ignore.equalsIgnoreCase("true")) {
        log.warn("Ignorando el fallo:" + ex.getMessage());
        ret = true;
      }
      else {
        log.error("Fallo la ejecucion de " + texto, ex);
        ret = false;
      }
    }
    return ret;
  }

  private boolean procesaFile(String filename, String ignore) {
    String content = null;
    boolean ret = true;
    int numlines;
    try {
      String lines[] = new String[100];
      numlines = Global.getContentOfFileAsScript(rootDir + filename,
                                                 "\n/\n",
                                                 lines);
      if(numlines==0)
        log.warn("El parche no contiene codigo "+rootDir+filename);
      for (int i = 0; i < numlines; i++) {
        ret &= procesaText(lines[i], ignore);
      }
    }
    catch (Exception ex) {
      return false;
    }
    if (ignore.equalsIgnoreCase("true")) {
      return true;
    }
    return ret;
  }

  private boolean procesaClass(String className) {
    try {
      //Creamos una instancia de la clase
      ClassPatcher c = (ClassPatcher) Class.forName(className).newInstance();
      //Se comprueba si es una sincronizadora
      if (c.isDoubleConnectionRequired()) {
        if (toCentro == null) {
          //lo es pero no existe la segunda conexion!
          log.warn("No puedo sincronizar, no existe la segunda conexion");
          return false;
        }
        //Sincronizar
        return c.patchDoubleConnection(params);
      }
      else {
        //Parchear
        return c.patchSingleConnection(params);
      }
    }
    catch (Exception ex) {
      log.warn("No puedo aplicar el parche", ex);
      return false;
    }
  }
}