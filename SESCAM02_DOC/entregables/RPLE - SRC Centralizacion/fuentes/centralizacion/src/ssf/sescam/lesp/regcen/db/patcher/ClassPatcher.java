package ssf.sescam.lesp.regcen.db.patcher;

import java.sql.Connection;
import java.util.Hashtable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author not attributable
 * @version 1.0
 */

public abstract class ClassPatcher {
   public boolean patchSingleConnection(Hashtable params) {
      throw new java.lang.UnsupportedOperationException(
           "No puedes llamar a patchSingleConnection de ClassPatcher.");
   }

   public boolean patchDoubleConnection(Hashtable params) {
      throw new java.lang.UnsupportedOperationException(
           "No puedes llamar a patchDoubleConnection de ClassPatcher.");
   }

   public abstract boolean isDoubleConnectionRequired();
}