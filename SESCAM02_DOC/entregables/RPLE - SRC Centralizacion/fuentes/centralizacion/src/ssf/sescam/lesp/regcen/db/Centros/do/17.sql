create view rcdcexp(
    cip     , cega    , ncita   , hicl    , cias    ,
    ape1    , ape2    , nomb    , fnac    ,
    sexo    , domi    , pobl    , cpos    , prov    ,
    tel1    , tel2    , slab    , fina    , gara    ,
    area    , servproc, medipeti,
    servreal, agen    , sala    , cmedreal, medireal,
    finclusi, prestaci, priorida, fechacit,
    fechader, centrder, especder,
    fsoldem1, fsoldem2, fsoldem3, fsoldem4, fsoldem5,
    tdemsol1, tdemsol2, tdemsol3, tdemsol4, tdemsol5,
    fsalida ,
    motisali,
    indinoga,
		tvisita,
    observac
    ) as
Select
 pacientes.numtis,  kk.n, citas.ncita, pacientes.numerohc, pacientes.cias,
 pacientes.apellid1,  pacientes.apellid2,  pacientes.nombre, pacientes.fechanac,
 pacientes.sexo,  pacientes.domiresi, pacientes.poblares,pacientes.codipost,pacientes.provresi,
 pacientes.telefono, pacientes.telecont,kk.c,citas.tfinan, citas.garante,
 citas.procedede, citas.servpeti, citas.perspeti,
 citas.servreal, citas.idagenda,kk.c,  citas.medico, kk.n,
 citas.grabadia,citas.codipres, citas.tipopaci, citas.fecha,
 kk.d, 'centro derivacion ','Especialidad Derivacion',
 kk.d,kk.d,kk.d,kk.d,kk.d,
 0,0,0,0,0,
 kk.d,
 '-C',
 citas.demora,
 tvisita,
 citas.observ
from
  pacientes,
  citas,
  kk
Where
     citas.numerohc=pacientes.numerohc
