package ssf.sescam.lesp.regcen.db.Centros;

import java.sql.Connection;
import java.sql.DriverManager;
import org.apache.log4j.Logger;

/**
 * <p>Title: Recopilacion Listas de Espera SESCAM</p>
 * <p>Description: Sistema de recopilacion de Informix HP-HIS de listas de espera</p>
 * <p>Copyright: Copyright (c) 2002 Soluziona</p>
 * <p>Company: Soluziona</p>
 * @author Eladio Linares
 * @version 1.0
 */

public class SchemaBase {
  public Connection c=null;

  public SchemaBase(String[] args) throws Exception {
    Logger log=Logger.getLogger(getClass());
    if(args.length!=7){
      Instrucciones();
      throw new IllegalArgumentException();
    }
    log.debug("Cargando controlador JDBC informix");
    Class.forName("com.informix.jdbc.IfxDriver");
    log.debug("Obteniendo una conexion al centro");
    c=DriverManager.getConnection(
        "jdbc:informix-sqli://"+
        args[1]+":"+args[2]+"/"+args[3]+":informixserver="+args[4],
        args[5],
        args[6]);
    log.debug("Conexion obtenida");
  }

  public static void Instrucciones(){
    System.out.println("Uso:\n java -jar SchemaCentro.jar [Create|Drop|Reinstall] "+
                       "IPAddress Port DBName IfxServerName "+
                       "DBUserName Password\n");
  }

}