create trigger {nombre}03
  delete on {nombre}
  referencing old as oldmov
  for each row
  (execute procedure
  	{nombre}_pro(
  	   10
  	  ,current year to second
          ,{oldmov.campos}
        )
  );

