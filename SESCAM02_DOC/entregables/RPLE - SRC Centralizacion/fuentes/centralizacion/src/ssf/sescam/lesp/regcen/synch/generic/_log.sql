create table {nombre}_log
  (
     tipope            smallint
    ,fecope            datetime year to second
    ,estado            smallint
    ,{defCampos}
    ,primary key (tipope,fecope,{primaryKey}) constraint {nombre}_pk
  );