package ssf.sescam.lesp.regcen.db;

import java.util.Hashtable;
import java.sql.Connection;
import java.util.Properties;
import java.sql.Types;
import java.sql.ResultSetMetaData;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.sql.Timestamp;
import ssf.utils.Global;

/**
 * <p>
 * Title: Recopilacion Listas de Espera SESCAM
 * </p><p>
 * Description: Sistema de recopilacion de Informix HP-HIS de listas de espera
 * </p><p>
 * Copyright: Copyright (c) 2002 Soluziona
 * </p><p>
 * Company: Soluziona
 * </p><p>
 * </p><p>
 * Crea registro generico, con tablas auxiliares (Foreign Keys de la tabla)
 * </p><p>
 * </p><p>
 * La definicion de las tablas maestras se toma de un objeto Properties
 * normalmente leido desde un archivo. La definicion de las tablas es como
 * sigue:
 * </p><p>
 * </p><p>
 * <b>Tablas:</b>
 * </p><p>
 * [dirTables].tablas= Numero de tablas a cargar en memoria
 * </p><p>
 * [dirTables].tabla.1= Nombre de la tabla numero 1 ([NomTab1])
 * </p><p>
 * [dirTables].[NomTab1].pk.1= Primer campo que compone el primary key (pk1)
 * </p><p>
 * [dirTables].[NomTab1].pk.2= Segundo campo que compone el PK, etc...
 * </p><p>
 * [dirTables].[NomTab1].des.1= Primer campo no-pk que se quiere recoger
 * (normalmente se trata de descripciones.
 * </p><p>
 * [dirTables].[NomTab1].des.2= Segundo campo no-pk que se quiere cargar en
 * Memoria, etc...
 * </p><p>
 * La definicion de las relaciones de este registro con las tablas anteriores
 * es:
 * </p><p>
 * </p><p>
 * <b>Relaciones:</b>
 * </p><p>
 * [dirRels]= Numero de relaciones que existen en total
 * </p><p>
 * [dirRels].1= Nombre del campo que se quiere sustituir por su descripcion
 * </p><p>
 * [dirRels].1.tabla= Nombre de la tabla Foreign
 * </p><p>
 * [dirRels].1.pk= Numero de campos que componen el PK de la tabla Foreign. Se
 * puede omitir (por defecto=1)
 * </p><p>
 * [dirRels].1.pk.1= Nombre del primer campo.
 * </p><p>
 * [dirRels].1.foreign= Nombre del campo de la tabla Foreign que se quiere leer
 * </p><p>
 * [dirRels].1.out= Nombre del campo en el registro actual donde se quiere
 * guardar el resultado Se puede omitir, en cuyo caso su valor es [dirRels].1
 * </p><p>
 * </p><p>
 * Para ver un <a href="../../internal.properties">ejemplo</a> con
 * dirTables=&quot;mst.cita&quot; y dirRels=&quot;rel.cita&quot;
 * </p>
 *
 * @author Eladio Linares
 *
 * @version 1.0
 */

public class RegGen {
  private Hashtable reg=null;
  private MasterTables mts=null;
  private String dirRelations=null;
  private Properties p=null;

  /**
    * @param conn Conexion a BBDD
   *
   * @param prop Aqui se encuentran las definiciones de las tablas auxiliares y
   *             las relaciones
   *
   * @param dirTables Directorio donde se definen las Tablas Maestras
   *
   * @param dirRels Directorio donde estan especificadas las relaciones
   */
  public RegGen(Connection conn,
                Properties prop,
                String dirTables,
                String dirRels) {
    //Carga de mastertables
    mts=new MasterTables(conn,prop,dirTables);
    dirRelations=dirRels;
    p=prop;
    reg=new Hashtable();
  }

  /**
   * Borra el contenido del registro
   */
  public void reset(){
    reg.clear();
  }

  /**
   * Guarda un campo
   * @param field Nombre del campo
   * @param value Valor
   */
  public void set(String field,Object value){
    String lcfield=field.toLowerCase();
    reg.put(lcfield,value);
  }

  /**
   * Devuelve un campo
   * @param field Nombre del campo
   * @return Valor de campo
   */
  public Object get(String field){
    return reg.get(field.toLowerCase());
  }

  /**
   * Carga los datos de una cita a partir de un ResultSet
   * @param rs ResultSet
   * @throws Exception
   */
  public void loadFromRS(ResultSet rs) throws Exception {
    ResultSetMetaData rsmd=rs.getMetaData();
    int numCols=rsmd.getColumnCount();
    for(int i=1;i<=numCols;i++){
      Object Value=null;
      switch( rsmd.getColumnType(i) ){
        case Types.DATE:
          Value=rs.getDate(i);
          break;
        case Types.TIMESTAMP:
          Value=rs.getTimestamp(i);
          break;
        case Types.INTEGER:
          Value=new Integer(rs.getInt(i));
          break;
        default:
          Value=rs.getString(i);
          if(Value!=null) Value=((String)Value).trim();
          break;
      }
      if(Value==null)
        Value="";
      String fieldName=rsmd.getColumnName(i);
      reg.put(fieldName,Value);
   }
   resolveForeigns();
  }

  /**
   * Resuelve las relaciones entre el registro y las mastertables
   */
  private void resolveForeigns(){
    int numRels=Integer.parseInt(p.getProperty(dirRelations));
    for(int i=1;i<=numRels;i++){
      String cab=dirRelations+"."+i;
      String fieldName=p.getProperty(cab);
      String fTabla=p.getProperty(cab+".tabla");
      String pkFTabla=p.getProperty(cab+".pk","'single'");
      String fValue=p.getProperty(cab+".foreign");
      String out=p.getProperty(cab+".out",fieldName);

      if(pkFTabla.equals("'single'"))
        pkFTabla="#"+get(fieldName);
      else{
        int npk=Integer.parseInt(pkFTabla);
        pkFTabla="";
        for(int j=1;j<=npk;j++)
          pkFTabla+="#"+get(p.getProperty(cab+".pk."+j));
      }
      String value=mts.Results(fTabla,pkFTabla,fValue);
      if(value==null)
        value="";
      else
        value=value.trim();
      set(out,value);
    }
  }

  /**
   * Dada una sql con campos de tipo '#nombre' sustituye #nombre por valor
   * @param sqlInsert SQL a transformar
   * @return sql transformada
   */

  protected String resolveSQLFields(String sqlInsert) {
    //Barremos los campos que contiene cita
    for(Enumeration e=reg.keys();e.hasMoreElements();){
      String regField=(String)e.nextElement();
      //Si alguno coincide con lo que se debe guardar, se pone el valor
      String regFieldSubs="#"+regField+"#";
      int pos=sqlInsert.indexOf(regFieldSubs);
      if( pos>=0 ){
        Object val=get(regField);
        String regValue="";
        if(val instanceof Integer)
          regValue+=((Integer)val).intValue();
        if(val instanceof String){
          val=Global.replaceString((String)val,"'","''");
          regValue+="'"+(String)val+"'";
        }
        if(val instanceof java.sql.Date){
          regValue="to_date('";
          regValue+=((java.sql.Date)val).toString();
          regValue+="','YYYY-MM-DD')";
        }
        if(val instanceof Timestamp){
          regValue="to_date('";
          regValue+=((Timestamp)val).toString();
          regValue+="','YYYY-MM-DD')";
        }
        sqlInsert=Global.replaceString(sqlInsert,regFieldSubs,regValue);
      }
    }
    return sqlInsert;
  }

  protected String getProp(String key){
    return p.getProperty(key);
  }

  protected String getProp(String key, String def){
    return p.getProperty(key,def);
  }

  public MasterTables getMasterTables(){
    return mts;
  }

}