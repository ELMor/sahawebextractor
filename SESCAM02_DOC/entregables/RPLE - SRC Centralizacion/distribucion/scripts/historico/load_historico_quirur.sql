-- SESCAM: Carga Historico
-- Script: load_historico_consultas.sql
-- Creaci�n del esquema (tablas) del hist�rico.
-- 1. crear procedimiento legate.InitHistoricoQuir�gica
CREATE OR REPLACE PROCEDURE InitHistoricoQuirurgica (dias NUMBER) IS

CURSOR histo_lesp (dias NUMBER) IS
SELECT centro,norden
FROM LESP
WHERE (catalog = 'HISTOR' OR CATALOG IS NULL) AND fsal < TRUNC(SYSDATE) - dias;
rc1 histo_lesp%ROWTYPE;

COUNTER NUMBER :=0;

BEGIN
	
	FOR rc1 IN histo_lesp(dias) LOOP
    	-- inserciones
		DELETE FROM LEHPDEMO WHERE centro = rc1.centro AND norden = rc1.norden;
		INSERT INTO LEHPDEMO_HISTO SELECT * FROM LEHPDEMO WHERE centro = rc1.centro AND norden = rc1.norden;
		-- borrados
		DELETE FROM LEHPDEMO WHERE centro = rc1.centro AND norden = rc1.norden;
		DELETE FROM SOLAPES WHERE centro = rc1.centro AND claveinicial = rc1.norden;
		DELETE FROM CONTINUOS WHERE centro = rc1.centro AND claveinicial = rc1.norden;
		-- tratamiento de lesp
    	DELETE FROM LESP_HISTO WHERE centro = rc1.centro AND norden = rc1.norden;
    	INSERT INTO LESP_HISTO
			(norden,centro,cip,cega,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov
			,tel1,tel2,tel3,slab,fina,gara,serv,secc,acli,cmed,apno_med,finc,cdi1,ddi1
			,cdi2,ddi2,cpr1,dpr1,cpr2,dpr2,tcir,tane,prio,cinc,preo,frep,fcap
			,spac,fder,dder,fsde,tdes,fqui,fsal,moti,obse,finchos,motrechazo,fecrechazo,catalog,LINK)
		 SELECT norden,centro,cip,cega,hicl,cias,ape1,ape2,nomb,fnac,sexo,domi,pobl,cpos,prov
			,tel1,tel2,tel3,slab,fina,gara,serv,secc,acli,cmed,apno_med,finc,cdi1,ddi1
			,cdi2,ddi2,cpr1,dpr1,cpr2,dpr2,tcir,tane,prio,cinc,preo,frep,fcap
			,spac,fder,dder,fsde,tdes,fqui,fsal,moti,obse,finchos,motrechazo,fecrechazo,catalog,LINK
    	 FROM LESP WHERE centro = rc1.centro AND norden = rc1.norden;
		DELETE FROM LESP WHERE centro = rc1.centro AND norden = rc1.norden;
		IF COUNTER MOD 1000 = 0 THEN
			COMMIT;
		END IF;
	END LOOP;
	COMMIT;
	-- borrado de masterclave
	Delete From masterclave m Where m.tipo = 1 And Not Exists (Select centro From lesp l Where l.centro = m.centro And l.norden = m.clave);
	commit;
END InitHistoricoQuirurgica;
/
-- 2. Ejecutar procedimiento.
BEGIN legate.InithistoricoQuirurgica(100); END; 