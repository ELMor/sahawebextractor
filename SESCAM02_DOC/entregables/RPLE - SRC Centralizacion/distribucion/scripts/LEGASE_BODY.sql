CREATE OR REPLACE PACKAGE BODY Legase IS
   --Este procedimiento inicializa la tabla masterclave y luego
   -- NO llama a refresh_tle
   PROCEDURE Init_TLE IS
      CURSOR c1 IS
         SELECT DISTINCT centro, norden
                    FROM LESP;

      CURSOR c2 IS
         SELECT  DISTINCT cega, ncita 
                    FROM LESPCEX;

      r1              c1%ROWTYPE;
      r2              c2%ROWTYPE;
      commitcounter   NUMBER       := 0;
   BEGIN
      FOR r1 IN c1 LOOP
         syncmasterclave (r1.centro, 1, r1.norden);
         commitcounter    := commitcounter + 1;

         IF commitcounter > 500 THEN
            COMMIT;
            commitcounter    := 0;
         END IF;
      END LOOP;

      COMMIT;

      FOR r2 IN c2 LOOP
         syncmasterclave (r2.cega, 2, r2.ncita);
         commitcounter    := commitcounter + 1;

         IF commitcounter > 500 THEN
            COMMIT;
            commitcounter    := 0;
         END IF;
      END LOOP;

      COMMIT;
   END init_tle;

   --Este procedimiento recorre masterclave y para cada clave llama a simple
   PROCEDURE Refresh_TLE (vcentro NUMBER) IS
      CURSOR c IS
         SELECT centro, tipo, CLAVE
           FROM MASTERCLAVE
          WHERE estado = 1 AND centro = vcentro;

      rc              c%ROWTYPE;
      commitCounter   NUMBER      := 0;
   BEGIN
      FOR rc IN c LOOP
         simple (rc.centro, rc.tipo, rc.CLAVE);
         commitCounter    := commitCounter + 1;

         UPDATE MASTERCLAVE
            SET estado = 0
          WHERE centro = rc.centro AND tipo = rc.tipo AND CLAVE = rc.CLAVE;

         IF commitCounter > 99 THEN
            COMMIT;
            commitCounter    := 0;
         END IF;
      END LOOP;

      COMMIT;
   END refresh_tle;

   --Este procedimiento cataloga las entradas del registro de pacientes

   FUNCTION simple_catalog (vcentro NUMBER, vtipo NUMBER, vclave NUMBER)
      RETURN VARCHAR2 IS
      codpr      VARCHAR2 (24);
	  idpres	 VARCHAR2 (2);
	  motsal	 VARCHAR2 (128);
      exist      NUMBER;
	  motr 		 NUMBER;
	  fecr 		 DATE;
      suma       NUMBER;
      fecha      DATE;
      servi      VARCHAR2 (128);
      activo     NUMBER;
      garant     NUMBER;
      rechazos   NUMBER;
	  CURSOR     map1(cpr VARCHAR2,vc NUMBER ,srv VARCHAR2) IS
	  SELECT g.SESESP_COD,g.proc_garantia, p.proc_activo
           FROM SES_PROCEDIMIENTOS p
               ,SES_PROC_MAP mp
               ,SES_PROC_GARANTIA g
               ,SES_SERVICIOS s
               ,SES_SRV_MAP sm
          WHERE trim(mp.proc_cen_cod) = cpr
            AND mp.centro = vc
            AND (g.SESESP_COD = s.SESESP_COD OR g.SESESP_COD IS NULL)
            AND p.proc_pk = mp.proc_pk
            AND g.PROC_PK = p.PROC_PK
            AND trim(sm.SRV_CEN_COD) = srv
            AND trim(s.SESSRV_COD) = trim(sm.SESSRV_COD)
            AND sm.CENTRO = vc
		  ORDER BY 1;
 	 map1reg	map1%ROWTYPE;

   BEGIN
      IF vtipo = 1 THEN
	         --Comprobamos que no tiene fecha de salida.

         SELECT activo, dem
      	   INTO exist, codpr
           FROM oltle
          WHERE centro = vcentro AND tipo = 1 AND claveinicial = vclave;

         IF exist = 0 AND codpr IS NULL THEN
            RETURN 'HISTOR';
         END IF;

         --Si cpr1 es nulo, el procedimiento no est� grantizado
         SELECT trim(cpr1), trim(secc)
           INTO codpr, servi
           FROM LESP
          WHERE centro = vcentro AND norden = vclave;

         IF codpr IS NULL THEN
            RETURN 'PSG';
         END IF;

         --Comprobacion de mapeos
         SELECT COUNT (*)
           INTO exist
           FROM SES_PROCEDIMIENTOS p
               ,SES_PROC_MAP mp
               ,SES_PROC_GARANTIA g
               ,SES_SERVICIOS s
               ,SES_SRV_MAP sm
          WHERE trim(mp.proc_cen_cod) = codpr
            AND mp.centro = vcentro
            AND (g.SESESP_COD = s.SESESP_COD OR g.SESESP_COD IS NULL)
            AND p.proc_pk = mp.proc_pk
            AND g.PROC_PK = p.PROC_PK
            AND trim(sm.SRV_CEN_COD) = servi
            AND trim(s.SESSRV_COD) = trim(sm.SESSRV_COD)
            AND sm.CENTRO = vcentro;

         IF exist = 0 THEN
            RETURN 'NOMAP';
        END IF;
		 
    -- Si spac=9 entonces es SG-RO, si motrechazo = 1 y fecrechazo no nulo SG-RO
         SELECT spac,motrechazo,fecrechazo
           INTO exist,motr, fecr
           FROM LESP
          WHERE centro = vcentro AND norden = vclave;

         IF (exist = 9) OR ( motr = 1 AND fecr IS NOT NULL) THEN
            RETURN 'SG-RO';
         END IF;
		 

         -- Si hay mas de 2 demoras voluntarias, 'SG-BPA'
         SELECT COUNT (*)
           INTO exist
           FROM LEHPDEMO
          WHERE centro = vcentro AND norden = vclave AND tipo = 4;

         IF exist > 2 THEN
            RETURN 'SG-BPA';
         END IF;

		 --Comprobamos garantia.
		 OPEN map1(codpr,vcentro,servi);
		 FETCH map1 INTO map1reg;
		 CLOSE map1;
		 garant:=map1reg.proc_garantia;
		 activo:=map1reg.proc_activo;

         IF garant = 0 OR activo = 0 THEN
            RETURN 'PSG';
         END IF;

         SELECT SUM (ndias)
           INTO suma
           FROM LEHPDEMO
          WHERE centro = vcentro AND norden = vclave AND tipo = 4;

         IF suma > garant THEN
            RETURN 'SG-BPA';
         END IF;

         SELECT dem
           INTO codpr
           FROM oltle
          WHERE centro = vcentro AND tipo = 1 AND claveinicial = vclave;

         IF codpr IS NULL THEN
            RETURN 'TG';
         END IF;

         RETURN 'TG-' || codpr;
      ELSE -- Catalogacion de LESPCEX.

    --Es posible que tenga fecha de salida.
         SELECT activo, dem
           INTO exist, codpr
           FROM oltle
          WHERE centro = vcentro AND tipo = 2 AND claveinicial = vclave;

         IF exist = 0 AND codpr IS NULL THEN
            RETURN 'HISTOR';
         END IF;

         --Si prestaci es nulo, el procedimiento no est� grantizado
         SELECT trim(prestaci), trim(servreal),trim(identpres),motisali
           INTO codpr, servi, idpres, motsal
           FROM LESPCEX
          WHERE cega = vcentro AND ncita = vclave;

		 IF motsal LIKE '-A%' THEN
		 	RETURN 'HISTOR';
		 END IF; 

         IF codpr IS NULL THEN
            RETURN 'PSG';
         END IF;
	 
 		 -- Si la prestacion es de Sucesivas -> SSG 
		 IF idpres = 'S' THEN 
		   SELECT COUNT (*)
           INTO exist
           FROM SES_PROCEDIMIENTOS p
               ,SES_PROC_MAP mp
               ,SES_SERVICIOS s
               ,SES_SRV_MAP sm
          WHERE trim(mp.proc_cen_cod) = codpr
            AND mp.centro = vcentro
            AND p.proc_pk = mp.proc_pk
            AND trim(sm.SRV_CEN_COD) = servi
            AND trim(s.SESSRV_COD) = trim(sm.SESSRV_COD)
            AND sm.CENTRO = vcentro;
	     IF exist = 0 THEN
            RETURN 'NOMAP';
         ELSE
		  	RETURN 'SSG';
		 END IF;
 		 
		 ELSE 
         -- Puede que no este mapeado (LESPCEX)

         SELECT COUNT (*)
           INTO exist
           FROM SES_PROCEDIMIENTOS p
               ,SES_PROC_MAP mp
               ,SES_PROC_GARANTIA g
               ,SES_SERVICIOS s
               ,SES_SRV_MAP sm
          WHERE trim(mp.proc_cen_cod) = codpr
            AND mp.centro = vcentro
            AND (g.SESESP_COD = s.SESESP_COD OR g.SESESP_COD IS NULL)
            AND p.proc_pk = mp.proc_pk
            AND g.PROC_PK = p.PROC_PK
            AND trim(sm.SRV_CEN_COD) = servi
            AND trim(s.SESSRV_COD) = trim(sm.SESSRV_COD)
            AND sm.CENTRO = vcentro;

		  IF exist = 0 THEN
            RETURN 'NOMAP';
          END IF;
		 END IF;

         -- Si hay mas de 2 demoras voluntarias, 'SG-BPA' (LESPCEX).
         SELECT COUNT (*)
           INTO exist
           FROM /*CEXINCID*/ CEXAPL c, LESPCEX l
          WHERE c.centro = vcentro
            AND c.centro = l.cega
            AND l.ncita = vclave
            AND c.ncita = l.ncita
            AND l.motisali LIKE '-CP%';

         IF exist > 2 THEN
		 	UPDATE LESPCEX SET motisali = '-CPG' 
			WHERE cega = vcentro AND ncita = vclave;
			
			RETURN 'SG-BPA';
         END IF;

		 OPEN map1(codpr,vcentro,servi);
		 FETCH map1 INTO map1reg;
		 CLOSE map1;
		 garant:=map1reg.proc_garantia;
		 activo:=map1reg.proc_activo;

         IF activo = 0 OR garant = 0 THEN
            RETURN 'PSG';
         END IF;

         SELECT SUM (dias_demo)
           INTO suma
           FROM /*CEXINCID c */ CEXAPL c, LESPCEX l
          WHERE c.centro = vcentro
            AND l.cega = vcentro
            AND c.ncita = vclave
            AND l.ncita = vclave
            AND l.motisali LIKE '-CP%';

         
		 
		 IF suma > garant THEN
		   UPDATE LESPCEX SET motisali = '-CPG' 
			WHERE cega = vcentro AND ncita = vclave;
 	       RETURN 'SG-BPA';
         END IF;

		 --Comprobar los rechazos en CEXINCID.
         SELECT COUNT (*)
           INTO rechazos
           FROM CEXINCID
          WHERE centro = vcentro
            AND ncita = vclave
            AND tipinci = 'R'
            AND perdida = 'S';

         IF rechazos > 0 THEN
		   UPDATE LESPCEX SET motisali = '-CR' 
			WHERE cega = vcentro AND ncita = vclave AND motisali = '-CN';
	       RETURN 'SG-RO';
         END IF;

         SELECT dem
           INTO codpr
           FROM oltle
          WHERE centro = vcentro AND tipo = 2 AND claveinicial = vclave;

         IF codpr IS NULL THEN
            RETURN 'TG';
         END IF;

         RETURN 'TG-' || codpr;
      END IF;
   END simple_catalog;

   PROCEDURE simple_solape (
      vcentro    IN   NUMBER
     ,vtipo      IN   NUMBER
     ,claveini   IN   NUMBER
   ) IS
      CURSOR c_cexapl (vcentro NUMBER, vncita NUMBER) IS
         SELECT *
           FROM CEXAPL
          WHERE centro = vcentro AND ncita = vncita ; --AND tipo = 'A';

      r_cexapl   c_cexapl%ROWTYPE;
      fectopdemo   DATE;
      feciniq      DATE;
      fecendq      DATE;
      rlespcex     LESPCEX%ROWTYPE;
   BEGIN
       --Encontramos la claveinicial. Si es quirurgica (tipo=1) hay que recorrer un arbol
      --si es consulta o prueba (tipo=2) entonces claveinicial=clave
      IF vtipo = 2 THEN
         DELETE FROM SOLAPES
               WHERE claveinicial = claveini AND tipo = 2
                     AND centro = vcentro;

         DELETE FROM CONTINUOS
               WHERE claveinicial = claveini AND tipo = 2
                     AND centro = vcentro;

         --Insertamos valores positivos procedentes de consultas y tecnicas
         --se transforman los motivos '-4' y '-5' a 4 y 5 respectivamente para
         --que sean consecuentes con los de quirurgica
         SELECT cega, ncita, finclusi
               ,fsalida, fechacit
           INTO rlespcex.cega, rlespcex.ncita, rlespcex.finclusi
               ,rlespcex.fsalida, rlespcex.fechacit
           FROM LESPCEX
          WHERE cega = vcentro AND ncita = claveini;

         --Como quedamos, si fsalida es nula pero la fecha de cita ya ha pasado
         --a dia de hoy, suponer que no se ha capturado actividad en el centro
         --y ponerlo como realizado.
         IF rlespcex.fsalida IS NULL THEN
            --Temporalmente, marcar como realizada.
            IF SYSDATE > rlespcex.fechacit THEN
               --Algunas entradas se introducen DESPUES de la cita,
               --Corregimos para no obtener TLEs negativos.
               IF rlespcex.finclusi > rlespcex.fechacit THEN
                  rlespcex.finclusi    := rlespcex.fechacit;
               END IF;

               rlespcex.fsalida    := rlespcex.fechacit;
            END IF;
         END IF;

         INSERT INTO SOLAPES
                     (centro, CLAVE, claveinicial, tipo, signo
                     ,fi, ff, situ
                     )
              VALUES (rlespcex.cega, rlespcex.ncita, rlespcex.ncita, 2, 1
                     ,rlespcex.finclusi, rlespcex.fsalida, 0
                     );

         --Valores negativos de consultas y tecnicas, procedentes de cexincid.
         --Por convencion se toman los tramos de demora uno a continuacion de otro
         --y se colocan en el inicio de inclusion en lista de espera.
         fectopdemo    := NULL;

         FOR r_cexapl IN c_cexapl (vcentro, claveini) LOOP
            IF fectopdemo IS NULL THEN
               fectopdemo    := r_cexapl.fecha_ini;
            END IF;

            INSERT INTO SOLAPES
                        (centro, CLAVE, claveinicial, tipo, signo, fi
                        ,ff, situ
                        )
                 VALUES (vcentro, claveini, claveini, 2, -1, fectopdemo
                        , r_cexapl.fecha_fin, 4
                        );

            fectopdemo    := r_cexapl.fecha_fin;
         END LOOP;
      ELSE --Es Quirurgica. Borramos primero lo que haya
         DELETE FROM SOLAPES
               WHERE claveinicial = claveini AND tipo = 1
                     AND centro = vcentro;

         DELETE FROM CONTINUOS
               WHERE claveinicial = claveini AND tipo = 1
                     AND centro = vcentro;

           --Insertamos valores positivos procedentes de quirurgica
         --Es posible insertar 2 veces el mismo tramo positivo. Es necesario
         --en el caso de registros unidos colocar el min(finc) como finc y el
         --max(fsal) como fsal para registros historicos o NULL para registros
         --activos.
         SELECT     MIN (finchos)
                   ,MAX (NVL (fsal, TO_DATE ('01-01-3000', 'DD-MM-YYYY')))
               INTO FecIniQ
                   ,FecEndQ
               FROM LESP
         START WITH norden = claveini AND centro = vcentro
         CONNECT BY PRIOR LINK = norden AND PRIOR centro = centro;

         IF FecEndQ = TO_DATE ('01-01-3000', 'DD-MM-YYYY') THEN
            FecEndQ    := NULL;
         END IF;

         INSERT INTO SOLAPES
                     (centro, CLAVE, claveinicial, tipo, signo, fi, ff, situ
                     )
              VALUES (vcentro, claveini, claveini, 1, 1, feciniq, fecendq, 0
                     );

         --Valores negativos de quirurgica
         INSERT INTO SOLAPES
                     (centro, CLAVE, claveinicial, tipo, signo, fi, ff, situ)
            SELECT centro, norden, claveini, 1, -1, fec_inicio
                  , fec_inicio + ndias, tipo
              FROM LEHPDEMO
             WHERE centro = vcentro
               AND tipo IN (4, 5)
               AND norden IN (
                             SELECT     norden
                                   FROM LESP
                             START WITH centro = vcentro AND norden = claveini
                             CONNECT BY centro = vcentro
                                        AND PRIOR LINK = norden);
      END IF;
   END simple_solape;

   FUNCTION takeCat (k tend_demor, m NUMBER)
      RETURN VARCHAR2 IS
      i   NUMBER;
   BEGIN
      i    := m;

      IF m < 1 THEN
         RETURN NULL;
      END IF;

      LOOP
         IF k (i) IS NOT NULL THEN
            RETURN k (i);
         END IF;

         i    := i - 1;

         IF i < 1 THEN
            EXIT;
         END IF;
      END LOOP;

      RETURN NULL;
   END takeCat;

   PROCEDURE simple_continuo (
      vcentro     IN   NUMBER
     ,vtipo       IN   NUMBER
     ,vclaveini   IN   NUMBER
   ) IS
      CURSOR c (ce NUMBER, ti NUMBER, clini NUMBER) IS
         SELECT signo, fi, ff, claveinicial, CLAVE, situ
           FROM SOLAPES
          WHERE centro = ce AND tipo = ti AND claveinicial = clini;

      CURSOR c2 (ce NUMBER, ti NUMBER, clini NUMBER) IS
         SELECT        signo, fi, spac
                  FROM CONTINUOS
                 WHERE centro = ce AND tipo = ti AND claveinicial = clini
              ORDER BY fi ASC
         FOR UPDATE OF signo, ff, spac, acu;

      CURSOR c3 (ce NUMBER, ti NUMBER, clini NUMBER) IS
         SELECT   fi
             FROM CONTINUOS
            WHERE centro = ce AND tipo = ti AND claveinicial = clini
         ORDER BY fi ASC;

      rc              c%ROWTYPE;
      rc2             c2%ROWTYPE;
      rc3             c3%ROWTYPE;
      contador        NUMBER;
      cont            NUMBER;
      ultcont         NUMBER;
      sigcont         NUMBER;
      acumulador      NUMBER       := 0;
      demora          VARCHAR2 (6);

      TYPE tend_dates IS TABLE OF DATE
         INDEX BY BINARY_INTEGER;

      end_dates       tend_dates;
      end_demor       tend_demor;
      solCounter      NUMBER       := 0;
      demorCounter    NUMBER       := 0;
      mDemorCounter   NUMBER       := 0;
      acuDias         NUMBER       := 0;
      endDate         DATE;
   BEGIN
      DELETE FROM CONTINUOS
            WHERE centro = vcentro
              AND tipo = vtipo
              AND claveinicial = vclaveini;

      FOR rc IN c (vcentro, vtipo, vclaveini) LOOP
         solCounter    := solCounter + 1;

         INSERT INTO CONTINUOS
                     (centro, tipo, claveinicial, CLAVE, signo
                     ,fi, spac
                     )
              VALUES (vcentro, vtipo, rc.claveinicial, rc.CLAVE, rc.signo
                     ,rc.fi, rc.situ || ',' || solCounter
                     );

         INSERT INTO CONTINUOS
                     (centro, tipo, claveinicial, CLAVE, signo
                     ,fi, spac
                     )
              VALUES (vcentro, vtipo, rc.claveinicial, rc.CLAVE, -rc.signo
                     ,rc.ff, -rc.situ || ',' || solCounter
                     );
      END LOOP;

      contador    := 0;

      FOR rc3 IN c3 (vcentro, vtipo, vclaveini) LOOP
         end_dates (contador + 1)    := rc3.fi;
         contador                    := contador + 1;
         end_demor (contador)        := NULL;
      END LOOP;

      cont        := 1;
      ultcont     := 1;

      IF contador = 1 THEN
         sigcont    := 1;
      ELSE
         sigcont    := 2;
      END IF;

      FOR rc2 IN c2 (vcentro, vtipo, vclaveini) LOOP
         acumulador    := acumulador + rc2.signo;

         IF rc2.spac LIKE '-%' THEN
            demorCounter                := TO_NUMBER (SUBSTR (rc2.spac, 4));
            end_demor (demorCounter)    := NULL;
         ELSE
            demorCounter    := TO_NUMBER (SUBSTR (rc2.spac, 3));

            IF rc2.spac LIKE '4%' THEN
               end_demor (demorCounter)    := 'DV';
            ELSIF rc2.spac LIKE '5%' THEN
               end_demor (demorCounter)    := 'DM';
            ELSE
               end_demor (demorCounter)    := NULL;
            END IF;
         END IF;

         IF demorCounter > mDemorCounter THEN
            mDemorCounter    := demorCounter;
         END IF;

         demora        := takeCat (end_demor, mDemorCounter);

         IF acumulador > 0 AND end_dates (sigcont) IS NOT NULL THEN
            acuDias    := acuDias + (end_dates (sigcont) - rc2.fi);
         END IF;

         IF cont = contador THEN
            endDate    := NULL;
         ELSE
            endDate    := end_dates (sigcont);
         END IF;

         UPDATE CONTINUOS
            SET signo = acumulador
               ,ff = endDate
               ,spac = demora
               ,acu = acuDias
          WHERE CURRENT OF c2;

         ultcont       := cont;
         cont          := cont + 1;

         IF cont + 1 > contador THEN
            sigcont    := cont;
         ELSE
            sigcont    := cont + 1;
         END IF;
      END LOOP;

      DELETE FROM CONTINUOS
            WHERE centro = vcentro
              AND tipo = vtipo
              AND CLAVE = vclaveini
              AND fi IS NULL
              AND ff IS NULL;
   END simple_continuo;

   --Refresca completamente una clave.
   PROCEDURE Simple (vcentro IN NUMBER, vtipo IN NUMBER, vclave IN NUMBER) IS
      vclaveinicial   NUMBER;
      cata            VARCHAR2 (6);
   BEGIN
      vclaveinicial    := clave_inicial (vcentro, vtipo, vclave);
      simple_solape (vcentro, vtipo, vclaveinicial);
      simple_continuo (vcentro, vtipo, vclaveinicial);
      cata             := simple_catalog (vcentro, vtipo, vclaveinicial);

      IF vtipo = 1 THEN
         UPDATE LESP
            SET catalog = cata
          WHERE centro = vcentro AND norden = vclave;
      ELSE
         UPDATE LESPCEX
            SET catalog = cata
          WHERE cega = vcentro AND ncita = vclave;
      END IF;
   END simple; --Refresco diferencial de TLE

   PROCEDURE Simple_CIP (vcip IN VARCHAR2) IS
      CURSOR c1 (estecip VARCHAR) IS
         SELECT centro, norden
           FROM LESP
          WHERE cip = estecip;

      CURSOR c2 (estecip VARCHAR) IS
         SELECT cega, ncita
           FROM LESPCEX
          WHERE cip = estecip;

      r1   c1%ROWTYPE;
      r2   c2%ROWTYPE;
   BEGIN
      FOR r1 IN c1 (vcip) LOOP
         simple (r1.centro, 1, r1.norden);
      END LOOP;

      FOR r2 IN c2 (vcip) LOOP
         simple (r2.cega, 2, r2.ncita);
      END LOOP;

      COMMIT;
   END Simple_CIP;

   PROCEDURE syncMasterclave (
      vcentro   IN   NUMBER
     ,vtipo     IN   NUMBER
     ,vclave    IN   NUMBER
   ) IS
      exist   NUMBER := 0;
   BEGIN
      SELECT COUNT (*)
        INTO exist
        FROM MASTERCLAVE
       WHERE centro = vcentro AND tipo = vtipo AND CLAVE = vclave;

      IF exist > 0 THEN
         UPDATE MASTERCLAVE
            SET estado = 1
          WHERE centro = vcentro AND tipo = vtipo AND CLAVE = vclave;
      ELSE
         INSERT INTO MASTERCLAVE
                     (centro, tipo, CLAVE, estado
                     )
              VALUES (vcentro, vtipo, vclave, 1
                     );
      END IF;
   END syncmasterclave;

   FUNCTION clave_inicial (vcentro IN NUMBER, vtipo IN NUMBER, vclave IN NUMBER)
      RETURN NUMBER IS
      cini   NUMBER := 0;
   BEGIN
      IF vtipo = 2 THEN
         cini    := vclave;
      ELSE
         SELECT     MIN (norden)
               INTO cini
               FROM LESP
         START WITH norden = vclave AND centro = vcentro
         CONNECT BY PRIOR norden = LINK AND PRIOR centro = centro;
      END IF;

      RETURN cini;
   END clave_inicial;
   
   
   FUNCTION clave_inicial_histo (vcentro IN NUMBER, vtipo IN NUMBER, vclave IN NUMBER)
      RETURN NUMBER IS
      cini   NUMBER := 0;
   BEGIN
      IF vtipo = 2 THEN
         cini    := vclave;
      ELSE
         SELECT     MIN (norden)
               INTO cini
               FROM LESP_HISTO
         START WITH norden = vclave AND centro = vcentro
         CONNECT BY PRIOR norden = LINK AND PRIOR centro = centro;
      END IF;

      RETURN cini;
   END clave_inicial_histo;
   
END Legase;
/
