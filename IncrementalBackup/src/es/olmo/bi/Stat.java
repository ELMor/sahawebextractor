package es.olmo.bi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import net.sf.jazzlib.GZIPInputStream;
import net.sf.jazzlib.GZIPOutputStream;
import net.sf.jazzlib.ZipEntry;
import net.sf.jazzlib.ZipException;
import net.sf.jazzlib.ZipFile;

/**
 * Statistic for Backup Dir
 * @author elinares
 *
 */
public class Stat {
	public static final String deleteMark="--$$DELETED";
	File statPath=null;
	protected Hashtable<String, ZipEntry> photoFinish=new Hashtable<>();
	protected Hashtable<String, FileHistory> historyOfFile=new Hashtable<>();
	
	public Stat(String path) throws ZipException, IOException{
		statPath=new File(path).getCanonicalFile();
		if(Main.debug) 
			System.out.println("Stating source "+statPath);
		recurse(statPath.getPath());
	}
	
	private void recurse(String path) throws ZipException, IOException{
		File list[]=new File(path).listFiles();
		int stripPath=statPath.getPath().length()+1;
		for(File file:list){
			String name=file.getCanonicalPath().substring(stripPath);
			if(file.isDirectory()){
				name+="/";
			}
			ZipEntry ze=new ZipEntry(name);
			ze.setSize(file.length());
			ze.setTime(ze.getTime());
			photoFinish.put(name, ze);
			if(file.isDirectory()){
				recurse(file.getCanonicalPath());
			}
		}
	}
	
	public Stat(String parentPath, String subPathZIPs) throws Exception {
		statPath=new File(parentPath+"/"+subPathZIPs).getCanonicalFile();
		statPath.mkdirs();
		//Check is STAT file exist and load, else calculate and save
		if(!loadStatFile()){
			File list[]=statPath.listFiles();
			//Ordenar la lista
			Arrays.sort(list);
			//Procesamos los zips
			for(File file:list) {
				String name=file.getName();
				if(name.length()==19 && name.toLowerCase().endsWith(".zip")){
					if(Main.debug)
						System.out.println("Stating backup "+name);
					String dateAndTime=name.substring(0,15);
					ZipFile zf=new ZipFile(file);
					Enumeration<ZipEntry> enu=zf.entries();
					while(enu.hasMoreElements()){
						boolean deleteMode=false;
						ZipEntry ze=enu.nextElement();
						String zeName=ze.getName();
						if(zeName.endsWith(deleteMark)){
							deleteMode=true;
							zeName=zeName.substring(0,zeName.length()-deleteMark.length());
						}
						FileHistory history=historyOfFile.get(zeName);
						if(history==null)
							history=new FileHistory();
						history.add(ze.getTime(),dateAndTime);
						historyOfFile.put(zeName, history);
						ZipEntry ex=photoFinish.get(zeName);
						if(deleteMode){
							photoFinish.remove(zeName);
						}else if(ex==null || ex.getTime()<ze.getTime()){
							photoFinish.put(zeName, ze);
						}
					}
					zf.close();
				}
			}
			//Save STAT
			saveStatFile();
		}
	}

	public void diff(Stat src, 
				Vector<ZipEntry> news, 
				Vector<ZipEntry> updated, 
				Vector<ZipEntry> untouched,
				Vector<ZipEntry> deleted) throws IOException{
		if(Main.debug)
			System.out.println("Diff from "+this.statPath+" to "+src.statPath);
		for(Enumeration<ZipEntry> zen=this.photoFinish.elements();
				zen.hasMoreElements();)
			deleted.add(zen.nextElement());
		Enumeration<String> srcEnu=src.photoFinish.keys();
		while(srcEnu.hasMoreElements()){
			String srcName=srcEnu.nextElement();
			ZipEntry thisZE=this.photoFinish.get(srcName);
			ZipEntry srcZE = src.photoFinish.get(srcName);
			if(thisZE==null){
				news.add(srcZE);
			}else{
				deleted.remove(thisZE);
				if(thisZE.getTime()<srcZE.getTime()){
					updated.add(srcZE);
				}else{
					untouched.add(srcZE);
				}
			}
				
		}
	}
	
	public File getStatPath(){
		return statPath;
	}
	
	public void saveStatFile() throws Exception{
		File statFile=new File(statPath,"STAT.gz");
		GZIPOutputStream zos=new GZIPOutputStream(new FileOutputStream(statFile));
		ObjectOutputStream oos=new ObjectOutputStream(zos);
		oos.writeObject(statPath);
		oos.writeObject(photoFinish);
		oos.writeObject(historyOfFile);
		oos.close();
	}
	
	@SuppressWarnings("unchecked")
	private boolean loadStatFile() throws IOException, ClassNotFoundException{
		File statFile=new File(statPath,"STAT.gz");
		if(!statFile.exists())
			return false;
		System.out.println("Stating loaded from "+statFile.getAbsolutePath());
		GZIPInputStream zis=new GZIPInputStream(new FileInputStream(statFile));
		ObjectInputStream ois=new ObjectInputStream(zis);
		statPath=(File)ois.readObject();
		photoFinish=(Hashtable<String, ZipEntry>)ois.readObject();
		historyOfFile=(Hashtable<String, FileHistory>)ois.readObject();
		ois.close();
		return true;
	}
	
}
