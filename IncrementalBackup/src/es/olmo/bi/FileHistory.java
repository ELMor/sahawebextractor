package es.olmo.bi;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;

public class FileHistory implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4603386869245078067L;
	Vector<Date> fechas=new Vector<>();
	Vector<String> zips=new Vector<>();
	static final String blanks="                ";
	public void add(Date d, String zip){
		fechas.add(d);
		zips.add(zip);
	}
	public void add(long d, String zip){
		fechas.add(new Date(d));
		zips.add(zip);
	}
	public void show(int level, String fname){
		StringBuffer levelStr=new StringBuffer();
		while(levelStr.length()<3*level)
			levelStr.append(blanks);
		String prep=levelStr.substring(0, 3*level);
		fname=fname.replace('\\', '/');
		System.out.printf("%s%s\n",prep,fname);
		for(int i=0;i<fechas.size();i++){
			long timemilis=fechas.get(i).getTime();
			if(timemilis>0)
				System.out.printf("   %s%2$tD %2$tR en %3$s\n", prep, fechas.get(i),zips.get(i));
			else
				System.out.printf("   %sDELETED- en %s\n", prep, zips.get(i));
		}
	}
}
