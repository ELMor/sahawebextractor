package es.olmo.bi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import net.sf.jazzlib.ZipEntry;
import net.sf.jazzlib.ZipOutputStream;

public class Update extends BackupMgr {

	private static String noDeflate[] = { "mp3", "ogg", "wma", "zip", "tgz",
			"tbz", "rar", "7z", "avi", "mp4", "mkv", "wmv", "cbr", "cbz" };
	
	byte buffer[] = new byte[4 * 1024 * 1024];

	Stat statOri = null;

	public Update(String rDest, String rOri) throws Exception {
		super(rDest);
		init(rOri);
		statOri = new Stat(rOri);
	}

	protected void addFiles(
			Vector<ZipEntry> files, ZipOutputStream zip, String zipName, ProgressShower slp) 
			throws Exception {
		for (int i = 0; i < files.size(); i++) {
			ZipEntry f = files.get(i);
			String fileName = f.getName();
			File toRead = new File(statOri.statPath, fileName);
			fileName=fileName.replace('\\', '/');
			ZipEntry store = new ZipEntry(fileName);
			zip.putNextEntry(store);
			statDes.photoFinish.put(fileName, store);
			if (!toRead.isDirectory()) {
				zip.setLevel(getCanCompress(fileName) ? 5 : 0);
				FileInputStream fis = new FileInputStream(toRead);
				slp.report(f.getSize(), fileName);
				flushFrom(fis, zip);
				fis.close();
			}
			FileHistory fh=statDes.historyOfFile.get(fileName);
			if(fh==null){
				fh=new FileHistory();
				statDes.historyOfFile.put(fileName, fh);
			}
			fh.add(toRead.lastModified(), zipName);
		}
	}

	private void deleteFiles(
			Vector<ZipEntry> deleted, ZipOutputStream newzip, String zipName, ProgressShower slp) 
			throws IOException {
		for (ZipEntry ze : deleted) {
			String name = ze.getName();
			ZipEntry toAdd;
			if (ze.isDirectory()) {
				toAdd = new ZipEntry(name.substring(0, name.length() - 1)
						+ Stat.deleteMark + "/");
			} else {
				toAdd = new ZipEntry(name + Stat.deleteMark);
			}
			newzip.putNextEntry(toAdd);
			statDes.photoFinish.remove(name);
			slp.report(1, name);
			statDes.historyOfFile.get(name).add(0, zipName);
		}
	}

	public void flushFrom(InputStream is, OutputStream os) throws IOException {
		int readed;
		while ((readed = is.read(buffer)) >= 0)
			os.write(buffer, 0, readed);
	}

	protected boolean getCanCompress(String fileName) {
		if (fileName.length() < 4)
			return true;
		boolean compress = true;
		String lower = fileName.toLowerCase();
		for (String ext : noDeflate) {
			if (lower.endsWith(ext)) {
				compress = false;
				break;
			}
		}
		return compress;
	}

	public String getNewZipFileName() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-kkmmss");
		String name = sdf.format(new Date());
		return name + ".zip";
	}

	public void perform() throws Exception {
		Vector<ZipEntry> news = new Vector<>();
		Vector<ZipEntry> updated = new Vector<>();
		Vector<ZipEntry> untouched = new Vector<>();
		Vector<ZipEntry> deleted = new Vector<>();
		ProgressShower slp;

		// BUscamos las diferencias
		statDes.diff(statOri, news, updated, untouched, deleted);
		if (news.size() == 0 && updated.size() == 0 && deleted.size() == 0) {
			System.out
					.println("No changes to backup. Nothing changed, nothing created.");
			return;
		}
		String newZipName=getNewZipFileName();
		File zfo = new File(statDes.getStatPath(), newZipName);
		FileOutputStream fos = new FileOutputStream(zfo);
		ZipOutputStream newzip = new ZipOutputStream(fos);
		// Tama�o de los ficheros a incluir en el zip
		long sz = 0;
		for (ZipEntry ze : news)
			sz += ze.isDirectory() ? 0 : ze.getSize();
		for (ZipEntry ze : updated)
			sz += ze.isDirectory() ? 0 : ze.getSize();
		slp = new ProgressShower(80, sz, "adding ");
		// A�adimos los ficheros
		addFiles(news, newzip, newZipName, slp);
		// Los modificados
		slp = new ProgressShower(80, sz, "updating ", slp.getCurrentValue());
		addFiles(updated, newzip, newZipName, slp);
		// Marcamos como borrados
		slp = new ProgressShower(80, deleted.size(), "deleting ");
		deleteFiles(deleted, newzip, newZipName, slp);
		newzip.close();
		statDes.saveStatFile();
	}
}
