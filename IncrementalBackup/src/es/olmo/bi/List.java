package es.olmo.bi;

import java.io.File;
import java.util.Arrays;

public class List extends BackupMgr {

	public List(String rDest, String rOri) throws Exception {
		super(rDest);
		if(!rOri.startsWith("/") && !rOri.substring(1,1).equals(":")){
			File local=new File(new File("."),rOri);
			rOri=local.getCanonicalPath();
		}
		matchParts(rOri, pthZIPs, pthInsideZIPs);
		init(pthZIPs.toString());
		if(Main.debug)
			System.out.println(
					"List:\n\tzips:"+pthZIPs.toString()+"\n" +
					"\tInside:"+pthInsideZIPs.toString()+"\n");
	}

	public void perform(){
		String name=pthInsideZIPs.toString();
		if(statDes.photoFinish.get(name+"/")!=null){
			name+="/";
			Object sorted[]=(statDes.historyOfFile.keySet().toArray());
			Arrays.sort(sorted);
			int level=0;
			String lastDir="/";
			for(int i=0;i<sorted.length;i++){
				String n=sorted[i].toString();
				if(n.startsWith(name)){
					statDes.historyOfFile.get(n).show(level, n);
					if(n.endsWith("/")){
						if(lastDir.length()>n.length())
							level--;
						else 
							level++;
						lastDir=n;
					}
				}
			}
		}else if(statDes.photoFinish.get(name)!=null){
			FileHistory fh=statDes.historyOfFile.get(name);
			fh.show(0, name);
		}else{
			System.out.println("No history found for "+name+" in "+statDes.statPath.toString()+".");
		}
	}
	
}
