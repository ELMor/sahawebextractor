package es.olmo.bi;

import java.io.File;
import java.io.IOException;
import java.util.StringTokenizer;

public class BackupMgr {

	File root=null;
	protected StringBuffer pthZIPs = new StringBuffer();
	protected StringBuffer pthInsideZIPs = new StringBuffer();
	Stat statDes = null;

	public BackupMgr(String rDest) throws Exception{
		root=new File(rDest);
		if(!root.exists() || !root.isDirectory())
			throw new RuntimeException("Root Destination does not exists:"+rDest);
	}
	
	public void init(String pthZIPs) throws Exception{
		File f=new File(pthZIPs);
		String lastpart=f.getName();
		statDes = new Stat(root.getCanonicalPath() ,lastpart);
	}

	/**
	 * Divide 'path' en dos, destPath directorios que coinciden con rootDest y restPath resto
	 * @param path        d1/d2/d3/lastpart
	 * @param zipPath     d1/d2
	 * @param insidePath  /d3/lastpart
	 * @param rootPath    /d3
	 * @throws IOException 
	 */
	public void matchParts(String path, 
			StringBuffer zipPath, 
			StringBuffer insidePath) throws IOException{
		StringTokenizer st=new StringTokenizer(path,"/\\:");
		while(st.hasMoreElements()){
			String dir=st.nextToken();
			File sd=new File(root,zipPath.toString()+"/"+dir);
			if(sd.exists()){
				zipPath.append(zipPath.length()>0?"/"+dir:dir);
			}else{
				insidePath.append(dir);
				break;
			}
		}
		while(st.hasMoreElements()){
			insidePath.append("/");
			insidePath.append(st.nextToken());
		}
	}
	
	public File getRootFile(){
		return root;
	}
	
	public String normalizeDir(String dirIn) throws IOException{
		if(!dirIn.startsWith("/") && !dirIn.substring(1,1).equals(":")){
			File local=new File(new File("."),dirIn);
			dirIn=local.getCanonicalPath();
		}
		StringBuffer out=new StringBuffer();
		StringTokenizer st=new StringTokenizer(dirIn,"/\\:");
		while(st.hasMoreElements()){
			if(out.length()>0)
				out.append("/");
			out.append(st.nextToken());
		}
		return out.toString();
	}
}
