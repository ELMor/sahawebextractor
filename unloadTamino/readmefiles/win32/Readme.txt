****************************************************************************************************
                                Tamino XMLDB API 4.1.4.4 for Windows                                

                                            README File                                             

The XML:DB API defines a set of common access mechanisms for accessing native XML databases. Having
a common API across native XML databases is attractive to users for the same reason that JDBC is
attractive to users of relational databases: a common API makes the development of applications
supporting different databases much easier.

The Tamino XML:DB API is a Java implementation of the XML:DB API specification, for Tamino. (Please
see http://www.xmldb.org/xapi/index.html for full details of the XML:DB API specification.)

The Tamino implementation meets all the requirements for "Core Level 1" compliance. In fact, almost
all of the Interfaces are implemented - only the update() and updateResource() methods of
XUpdateQueryService are not implemented.

The XML:DB API provides support for nested collections. This feature has been simulated in the
Tamino XML:DB API (by using special naming conventions for collections).

Additionally, the Tamino XML:DB API implementation provides a Service for executing XQuery
statements against a Tamino database.

                                                                          Software AG, March 31 2004
****************************************************************************************************


Table of Contents
=================

1 Prerequisites

2 Platform

3 Installation

4 Getting Started

5 Special Considerations

  5.1 Nested Collections
  5.2 TransactionService
  5.3 Document Naming
  5.4 Tamino specific XQuery Service
  5.5 Quirks

6 Changes since previous release

  6.1 Changes in XML:DB API Specification
  6.2 Functional Changes
  6.3 Bugs Fixed


****************************************************************************************************


1 Prerequisites
===============

To use the Tamino XML:DB API it is necessary to have Tamino 3.1 (or higher), the Tamino API for
Java, and a Java 1.3.1 JVM (or higher).


2 Platform
==========

All platforms upon which the Tamino API for Java is supported.


3 Installation
==============

There is no installation procedure per se. Simply add the following Jar files to the CLASSPATH:

    tamino_xmldb.jar    <--- This is the XML:DB API for Tamino
    xmldb.jar           <--- This contains the XML:DB Interfaces

    TaminoAPI4J.jar     <--- These four Jar files can be found
    log4j-core.jar      <--- in the SDK/TaminoAPI4J/lib  
    xerces.jar          <--- directory of the Tamino 3.1.1.x
    jdom.jar            <--- installation

Please note: later versions of the Tamino API for Java include a version of Xerces which is split
over two jar files:

    xmlParserAPIs.jar
    xercesImpl.jar
  
4 Getting Started
=================

To get started with the XML:DB API and Tamino, it is necessary to know the name of the driver class
to register, and the format of the URI required to connect to Tamino.

Driver Class:
	com.softwareag.tamino.xmldb.api.base.TDatabase

Example URI:
	xmldb:tamino://localhost/tamino/test

The format of the URI following "tamino://" is the same as a normal Tamino URL, that is:
<hostname>/tamino/<db name>

For full documentation of the XML:DB API, please see the XML:DB website:

  http://www.xmldb.org/xapi/index.html


5 Special Considerations
========================


5.1 Nested Collections
----------------------

The XML:DB API contains a number of methods to support nested collections. As Tamino does not
natively support nested collections, the XML:DB driver "simulates" nesting.

This is accomplished through the enforcement of a particular naming scheme for the collections in
Tamino. To create nested collections [from the XML:DB driver's perspective] the Tamino collections
should be given names with the name of the parent collection at the beginning.

For example, if the user creates the following collections in Tamino:

	jive
	jive.users
	jive.messages

The XML:DB driver will view these as nested collections. That is, if a Collection (an XML:DB
Collection) object is created for the "jive" collection, the listChildCollections() method will
return two results: "users" and "messages".

Calling getChildCollectionCount() will return "2", and calling getChildCollection("users") will
return a new XML:DB Collection object for the "jive.users" collection.

(The call could also be for "messages", of course.)

These collections do not have children.

If another collection were introduced: "jive.messages.internal", this would be listed and
accessible from the "messages" Collection.

By default, the period ('.')character is used for separating the different levels within the names
[of the Tamino collection]. If this is a problem, the separator the driver will use can be changed
at runtime by setting a Java property.

The property is "com.softwareag.tamino.xmldb.CollSep", and can be set from the command line like
this:

	java -Dcom.softwareag.tamino.xmldb.CollSep=^

(This would have the effect that the driver would only consider names such as "jive^users" to
denote a child collection.)

Please note: this property is only evaluated at startup, so changing it during runtime will not
have any effect.


5.2 TransactionService
----------------------

It is possible to control the Transaction Isolation Level and Lockwait Mode through the usage of
the setProperty() method.

A number of String constants have been defined in the TTransactionService class:

	ISOLATION_KEY
	ISOLATION_UNPROTECTED
	ISOLATION_PROTECTED
	ISOLATION_SHARED

	LOCKWAIT_KEY
	LOCKWAIT_YES
	LOCKWAIT_NO

Once a TransactionService object has been retrieved from a Collection, its setProperty() method can
be used to set the appropriate level and/or mode, for example:

   // Register Tamino XML:DB Driver
   String driverName = "com.softwareag.tamino.xmldb.api.base.TDatabase";
   Database db = (Database)Class.forName(driverName).newInstance();
   DatabaseManager.registerDatabase(db);

   // Retrieve the "jive" Collection
   Collection coll = DatabaseManager.getCollection("xmldb:tamino://localhost/tamino/test/jive");

   // Get the TransactionService for the jive Collection
   TransactionService txService = (TransactionService)coll.getService("TransactionService",
                                                                      "1.0");

   // Set Transaction Isolation Level to protected
   txService.setProperty(TTransactionService.ISOLATION_KEY, 
                         TTransactionService.ISOLATION_PROTECTED);

   // Set Lockwait Mode to yes
   txService.setProperty(TTransactionService.LOCKWAIT_KEY, 
                         TTransactionService.LOCKWAIT_YES);
  
5.3 Document Naming
-------------------

One notable difference between the XML:DB API and Tamino concepts is that there is no concept of a
document type in the XML:DB API.

This leads to an interesting situation: when retrieving a NonXML object with the Tamino API for
Java, the developer uses a TNonXMLObjectAccessor. As the XML:DB API does not differentiate between
retrieving an XML or NonXML object - it just defines that the result should be different (either an
XMLResource or a BinaryResource) - some way of indicating to the Tamino implementation which
document type should be retrieved is necessary.

If the Tamino Collection only contains XML doctypes (i.e. no NonXML), it is possible to use simple
document names without problems. The user can store different document types using names such as
"Doc1.xml", "John Smith", and so on.

However, if there is a NonXML doctype, a special naming convention needs to be followed for the
documents. This naming convention is: <doctype>/<name>.

So, for example, if there is a doctype called "Image" which is used to store JPG images, the
document names should be something like "Image/Doc1.jpg", "Image/John Smith.jpg", and so forth.

The TCollection class (which implements the XML:DB Collection interface) examines all the doctypes
in the Tamino Collection when it is instantiated. The NonXML doctypes are recorded for later usage.
Supplying the document type as part of the name allows the implementation to check the doctype
against the list of NonXML doctypes which it has identified, and return the correct result.

At present it is necessary to store the documents in Tamino with the complex name (so "Person/John
Smith" rather than "John Smith") - it is not possible to store a Person document with the name
"John Smith" then retrieve it using "Person/John Smith".

(Please note: this only applies to the usage of Collection.getResource() - the XPathQueryService
implementation behaves as one would normally expect with XPath.)

Special care should be taken when choosing document names in a multi-doctype Collection. Tamino
enforces that the document names are unique for a particular doctype, but it is possible to have
different doctypes with the same name.

So, for instance, it is possible to have a Collection which contains Person and Customer doctypes.
Both of these can contain a document called "John Smith".

When attempting to retrieve one of these with collection.getResource("John Smith"), the Tamino
XML:DB API implementation cannot determine whether the Person or the Customer document should be
retrieved, and will throw an exception. If the complex names "Customer/John Smith" and "Person/John
Smith" are used, there is no ambiguity - and no problem with the retrieval.


5.4 Tamino specific XQuery Service
----------------------------------

This version of the Tamino XML:DB API introduces a new Service through which Tamino XQuery
statements can be executed. The name of the Service is "XQueryService", and it can be instantiated
like this:

   XQueryService txqs = (XQueryService)coll.getService("XQueryService", "1.0");

Once an instance has been acquired, any Tamino XQuery expression can be executed via the
query(String queryText) method.

If the XQuery expression needs to have namespace mappings declared in its prolog, the method
setNamespace(String prefix, String uri) can be used. All the namespaces which are set for the
instance of XQueryService will be prepended to the start of the query string passed to the
query(String queryText) method.

Namespace mappings can be removed using the removeNamespace(String prefix) method.


5.5 Quirks
----------

If this is done:

   Resource res1 = coll.createResource("res", "XMLResource");
   res1.setContent("<a>First Value</a>");

   Resource res2 = coll.createResource("res", "XMLResource");
   res2.setContent("<a>Second Value</a>");

This will result in only one document being stored - with the ID "res" - but with the contents of
res2. (This behaviour mimics that of the XML:DB API implementation for the Xindice database.)


6 Changes since previous release
================================


6.1 Changes in XML:DB API Specification
---------------------------------------

The XML:DB API specification has evolved a little since the previous release of the API for Tamino,
so the necessary changes have been made for this release.

Specifically, these changes were:

   * Added support for Exception chaining
   * Implemented new method XMLResource.getSAXFeature(String name)
   * Implemented new method XMLResource.setSAXFeature(String name, String value)

6.2 Functional Changes
----------------------

This release includes some functional changes:

   * Added support for queries which return nonXML text results, such as: count("/Person")
   * Updated some catch blocks so that Exceptions are re-thrown in addition to being logged
   * Updated XPathQueryService.queryResource() so that resourceID no longer has to use the
     special "<doctype>/<ino:id>" format
   * Added support for cursoring query results (Tamino specific).
   * Added new Database.getNames() method from XML:DB API specification.
   * Added automatic creation of open-content schema for insert of document with no corresponding doctype.
   * Added XQueryService interface and TXQueryService implementation to allow execution of XQuery expressions.
   * Improved logging throughout.

6.3 Bugs Fixed
--------------

There are some bugfixes:

   * Fixed username/password bug
   * Fixed bug in ResourceSet.getMembersAsResource() when the member(s) contain entities
   * Fixed SR#530843: resources retrieved via query now return null from the getId() method, as per
     the XML:DB specification.


====================================================================================================
 (c) 2003, Software AG. All rights reserved.
====================================================================================================
