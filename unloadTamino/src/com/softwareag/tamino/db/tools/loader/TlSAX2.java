// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst

package com.softwareag.tamino.db.tools.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

// Referenced classes of package com.softwareag.tamino.db.tools.loader:
//            SAXCopyClass, TlHTTPrequest, SAXReturnDocument, TlVars,
//            TlUtil, SAX2Driver, TaminoLoad, TlResource,
//            TlURLEncoder

public class TlSAX2 extends SAXCopyClass {

    private static StringBuffer lastoutString;

    private static StringBuffer lastMsgString;

    private static boolean bNoTaminoErr = false;

    private boolean ignore;

    private TlVars tlvars;

    private Integer ctr;

    private TlHTTPrequest tlreq;

    public boolean connected;

    public TlSAX2(TlVars tlvars1, TlHTTPrequest tlhttprequest) {
        super(null, null);
        ignore = false;
        ctr = new Integer(0);
        tlreq = null;
        connected = false;
        tlvars = tlvars1;
        tlreq = tlhttprequest;
        super.outString = new StringBuffer(tlvars1.defStrBufSize);
        lastoutString = new StringBuffer(tlvars1.defStrBufSize);
        lastMsgString = new StringBuffer(tlvars1.defStrBufSize);
    }

    private void getApiVersion() throws Exception {
        Float float1 = new Float("1.0");
        try {
            URL url = new URL(tlvars.url + "?_diagnose=apiversion");
            BufferedReader bufferedreader = new BufferedReader(
                    new InputStreamReader(url.openStream()));
            boolean flag = false;
            String s1 = "";
            int i = 0;
            String s;
            while ((s = bufferedreader.readLine()) != null) {
                s = s.trim().toLowerCase();
                i = s.indexOf("ino:response");
                if (i > 0)
                    flag = true;
                if (flag)
                    s1 = s1 + s;
            }
            if (flag) {
                i = s1.indexOf("ino:messageline ino:subject=\"apiversion\"", i);
                if (i > 0) {
                    i = s1.indexOf(">", i) + 1;
                    int j = s1.indexOf("<", i);
                    String s2 = null;
                    String s3 = null;
                    try {
                        StringTokenizer stringtokenizer = new StringTokenizer(
                                s1.substring(i, j), ".");
                        s2 = stringtokenizer.nextToken();
                        s3 = stringtokenizer.nextToken();
                    } catch (NoSuchElementException nosuchelementexception) {
                    }
                    try {
                        float1 = new Float(s2 + "." + s3);
                    } catch (NumberFormatException numberformatexception) {
                    }
                }
            }
            bufferedreader.close();
            if (!flag)
                throw new Exception("No recognised response from Tamino!");
        } catch (Exception exception) {
            System.out
                    .println("ERROR: While checking API Version the following error occurred:");
            System.out.println(exception.toString());
            System.exit(1);
        } finally {
            tlvars.getApiVersion = false;
            tlvars.uniCodeEncoding = (double) float1.floatValue() >= 1.1000000000000001D;
            System.out.println("API Version = " + float1);
        }
    }

    public void init(TlVars tlvars1) throws Exception {
        tlvars = tlvars1;
        try {
            URL url = new URL(tlvars1.url);
            String s1 = url.getProtocol().toString();
            if (!s1.equals("http"))
                throw new MalformedURLException();
        } catch (MalformedURLException malformedurlexception) {
            System.out.println("Invalid URL");
            System.exit(1);
        }
        if (tlvars1.getApiVersion)
            getApiVersion();
        String s = TlVars.defaultParserName;
        if (!tlvars1.noUpload) {
            tlreq = new TlHTTPrequest(tlvars1);
            tlreq.init(true);
        }
        super.init(tlvars1.parserNameSpaces, tlvars1.parserValidating);
    }

    public void connect() throws SAXException {
        try {
            tlreq.init(true);
            tlvars.returnDoc = tlreq.write("_connect=*");
            connected = true;
            if (tlvars.returnDoc != null && tlvars.returnDoc.length() > 0) {
                tlvars.internalDoc = true;
                SAXReturnDocument saxreturndocument = new SAXReturnDocument(
                        tlvars);
                saxreturndocument.init(tlvars);
                saxreturndocument.start();
            }
            tlreq.init(true);
            if (tlvars.preserveIds) {
                tlvars.returnDoc = tlreq
                        .write("_admin=ino:EnableInsertWithInoID(\"ON\")&_sessionid="
                                + tlvars.sessionId.toString()
                                + "&_sessionkey="
                                + tlvars.sessionKey.toString());
                if (tlvars.returnDoc != null && tlvars.returnDoc.length() > 0) {
                    tlvars.internalDoc = true;
                    SAXReturnDocument saxreturndocument1 = new SAXReturnDocument(
                            tlvars);
                    saxreturndocument1.init(tlvars);
                    saxreturndocument1.start();
                }
            }
        } catch (Exception exception) {
            System.out.println("Could not connect...");
            connected = false;
            exception.printStackTrace();
            throw new SAXException(exception.toString());
        }
    }

    public void disconnect() throws SAXException {
        if (connected) {
            Object obj = null;
            TlVars tlvars1 = null;
            tlvars1 = tlvars;
            try {
                if (!tlvars.noUpload) {
                    tlreq.init(true);
                    tlvars1.returnDoc = tlreq.write("_disconnect=*&_sessionid="
                            + tlvars.sessionId.toString() + "&_sessionkey="
                            + tlvars.sessionKey.toString());
                    if (tlvars1.returnDoc != null
                            && tlvars1.returnDoc.length() > 0) {
                        tlvars1.internalDoc = true;
                        SAXReturnDocument saxreturndocument = new SAXReturnDocument(
                                tlvars1);
                        saxreturndocument.init(tlvars1);
                    }
                }
            } catch (Exception exception) {
                throw new SAXException(exception.toString());
            }
        }
    }

    public void start(InputStream is) throws SAXException {
        try {
            if (!tlvars.noUpload)
                connect();
            if (tlvars.virtualDoc) {
                File file = new File(tlvars.fileName);
                String s = TlUtil.fileToURL(file).toString();
                StringReader stringreader = new StringReader(makeVirtualDoc(s,
                        tlvars.charset, "load"));
                super.parser.parse(new InputSource(stringreader));
            } else if (is != null) {
                super.parser.parse(new InputSource(is));
            } else {
                File file = new File(tlvars.fileName);
                String s = TlUtil.fileToURL(file).toString();
                super.parser.parse(s);
            }
            //writeUserInfo();
            Object obj = null;
            TlVars tlvars1 = null;
            tlvars1 = tlvars;
            disconnect();
        } catch (SAXParseException saxparseexception) {
            if (connected)
                disconnect();
            TaminoLoad.k--;
            String s1 = TlResource.getMessage("TlSAXParseErr");
            writeError(MessageFormat.format(s1,
                    new String[] { saxparseexception.toString() }));
            s1 = TlResource.getMessage("TlSAXParseErr1");
            Integer integer = new Integer(saxparseexception.getLineNumber());
            Integer integer1 = new Integer(saxparseexception.getColumnNumber());
            writeError(MessageFormat.format(s1, new String[] {
                    integer.toString(), integer1.toString() }));
            s1 = TlResource.getMessage("TlSAXParseErr2");
            writeError(MessageFormat.format(s1,
                    new String[] { saxparseexception.getSystemId() }));
            if (saxparseexception.getPublicId() != null) {
                String s2 = TlResource.getMessage("TlSAXParseErr3");
                writeError(MessageFormat.format(s2,
                        new String[] { saxparseexception.getPublicId() }));
            }
            if (!tlvars.commandLineStart)
                throw new SAXException("Parser Error");
        } catch (SAXException saxexception) {
            TaminoLoad.k--;
            if (connected)
                disconnect();
            writeError(saxexception.toString());
            writeUserInfo();
            if (!tlvars.commandLineStart)
                throw new SAXException(saxexception.toString());
        } catch (IOException ioexception) {
            if (connected)
                disconnect();
            String s3 = TlResource.getMessage("TlSaxStart");
            writeError(MessageFormat.format(s3, new String[] { ioexception
                    .toString() }));
            if (!tlvars.commandLineStart)
                throw new SAXException("");
        }
    }

    private static String makeVirtualDoc(String s, String s1, String s2) {
        String s3 = "utf-8";
        int i = 0;
        StringBuffer stringbuffer = new StringBuffer("");
        if (s1.length() != 0)
            s3 = s1;
        String as[] = {
                "<?xml version=\"1.0\" encoding=\"" + s3 + "\"?>",
                "<!DOCTYPE dictionary [<!ENTITY extest  SYSTEM \"" + s + "\" >",
                "]>", "<" + s2 + ">&extest;</" + s2 + ">" };
        try {
            while (as[i] != null)
                stringbuffer.append(as[i++] + "\n");
        } catch (ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) {
        }
        return stringbuffer.toString();
    }

    public InputSource resolveEntity(String s, String s1) throws SAXException {
        Object obj = null;
        String s2 = TlUtil.dirFromPath(s1, TlVars.sURLSeparator);
        String s3 = TlUtil.fileFromPath(s1, TlVars.sURLSeparator);
        if (s2 == null || s2.length() == 0) {
            File file = new File(tlvars.entdir, s3);
            return new InputSource(new StringReader(TlUtil.fileToURL(file)
                    .toString()));
        } else {
            return null;
        }
    }

    private boolean loadQueryResult(TlVars tlvars1) {
        return tlvars1.format == "queryResult";
    }

    private boolean loadLoadRequest(TlVars tlvars1) {
        return tlvars1.format == "loadRequest";
    }

    public void startElement(String s, String s1, String s2,
            Attributes attributes) throws SAXException {
        if (loadQueryResult(tlvars)
                && (s2.startsWith("ino:") || s2.startsWith("xql:"))
                && !s2.equals("ino:response") && !s2.equals("xql:result")) {
            ignore = true;
            return;
        }
        if (loadQueryResult(tlvars) && s2.equals("xql:result"))
            super.depth = 0;
        if (loadLoadRequest(tlvars) && s2.equals("ino:object"))
            super.depth = 0;
        ignore = false;
        StringBuffer stringbuffer = new StringBuffer("<" + s2);
        if (!super.namespaceDecls.equals(""))
            stringbuffer.append(super.namespaceDecls);
        super.depth++;
        if (super.depth == 1 && !tlvars.defineS) {
            super.namespaceDecls = "";
            return;
        }
        int i = attributes.getLength();
        if (i > 0) {
            for (int j = 0; j < i; j++)
                processAttribute(stringbuffer, attributes.getQName(j),
                        attributes.getValue(j), s, s1, s2);

        }
        super.namespaceDecls = "";
        stringbuffer.append(">");
        super.outString.append(stringbuffer);
    }

    public void processAttribute(StringBuffer stringbuffer, String s,
            String s1, String s2, String s3, String s4) throws SAXException {
        if (ignore)
            return;
        if (s.equals("ino:id") && tlvars.noInoId) {
            return;
        } else {
            super.processAttribute(stringbuffer, s, s1, s2, s3, s4);
            return;
        }
    }

    public void endElement(String s, String s1, String s2) throws SAXException {
        if (ignore)
            return;
        super.depth--;
        if (super.depth <= 0 && !tlvars.defineS)
            return;
        super.outString.append("</" + s2 + ">");
        if (super.depth <= 1) {
            ctr = new Integer(ctr.intValue() + 1);
            if (tlvars.noUpload)
                tlvars.totalElements = new Integer(tlvars.totalElements
                        .intValue() + 1);
        }
        if (ctr.intValue() >= tlvars.maxctr.intValue() && !tlvars.defineS)
            flushBuffer();
    }

    public void ignorableWhitespace(char ac[], int i, int j)
            throws SAXException {
        characters(ac, i, j);
    }

    public void characters(char ac[], int i, int j) throws SAXException {
        if (ignore)
            return;
        if (tlvars.bMessageLine) {
            lastMsgString = new StringBuffer(tlvars.defStrBufSize);
            while (j-- > 0)
                lastMsgString.append(ac[i++]);
            return;
        } else {
            super.characters(ac, i, j, tlvars.uniCodeEncoding);
            return;
        }
    }

    public void processingInstruction(String s, String s1) throws SAXException {
        if (ignore) {
            return;
        } else {
            super.processingInstruction(s, s1);
            return;
        }
    }

    public void startCDATA() throws SAXException {
        if (ignore) {
            return;
        } else {
            super.startCDATA();
            return;
        }
    }

    public void endCDATA() throws SAXException {
        if (ignore) {
            return;
        } else {
            super.endCDATA();
            return;
        }
    }

    public void startDTD(String s, String s1, String s2) throws SAXException {
    }

    public void endDTD() throws SAXException {
    }

    public void startEntity(String s) throws SAXException {
    }

    public void endEntity(String s) throws SAXException {
    }

    public void unparsedEntityDecl(String s, String s1, String s2, String s3) {
    }

    public void fatalError(SAXParseException saxparseexception)
            throws SAXException {
        throw saxparseexception;
    }

    public void notationDecl(String s, String s1, String s2) {
    }

    public void setDocumentLocator(Locator locator) {
    }

    public void warning(SAXParseException saxparseexception)
            throws SAXException {
        System.out.println("PARSER WARNING " + saxparseexception.getMessage());
    }

    public void error(SAXParseException saxparseexception) throws SAXException {
        System.out.println("PARSER ERROR " + saxparseexception.getMessage());
    }

    public void startPrefixMapping(String s, String s1) throws SAXException {
        super.namespaceDecls += " xmlns";
        if (!s.equals(""))
            super.namespaceDecls += ":" + s;
        super.namespaceDecls += "=\"" + s1 + "\"";
    }

    public void endPrefixMapping(String s) throws SAXException {
    }

    public void skippedEntity(String s) throws SAXException {
    }

    public void comment(char ac[], int i, int j) throws SAXException {
        if (ignore)
            return;
        if (tlvars.parserIncludeComments && j > 0) {
            super.outString.append("<!--");
            characters(ac, i, j);
            super.outString.append("-->");
        }
    }

    public void startDocument() {
    }

    public void endDocument() throws SAXException {
        flushBuffer();
    }

    public void attributeDecl(String s, String s1, String s2, String s3,
            String s4) {
    }

    public void elementDecl(String s, String s1) {
    }

    public void externalEntityDecl(String s, String s1, String s2) {
    }

    public void internalEntityDecl(String s, String s1) {
    }

    public void flushBuffer() throws SAXException {
        if (tlreq == null)
            return;
        String s = super.outString.toString().trim();
        if (s.equals(""))
            return;
        int i = super.outString.length();
        if (i == 0)
            return;
        tlvars.xmlVolume = new Integer(tlvars.xmlVolume.intValue() + i);
        TlSAX2 _tmp = this;
        String s1 = "<?xml version=\"1.0\"?>";
        if (tlvars.uniCodeEncoding)
            s1 = TlURLEncoder.encodeUniCode(s);
        else
            s1 = TlURLEncoder.encodeEscape(s);
        tlvars.uploadVolume = new Integer(tlvars.uploadVolume.intValue()
                + s1.length());
        if (tlvars.doshow) {
            if (tlvars.showEncoded) {
                System.out.println(TlResource.getMessage("TlHTTPedoc"));
                System.out.println(s1 + "\n");
            } else {
                System.out.println(super.outString);
            }
            if (tlvars.progress) {
                String s2 = TlResource.getMessage("TlBlockProgressing");
                System.out.println(MessageFormat.format(s2, new String[] {
                        tlvars.blocks.toString(), ctr.toString() }));
            }
        }
        SAXReturnDocument saxreturndocument = null;
        if (!tlvars.noUpload && !tlvars.defineS) {
            TlVars tlvars1 = null;
            tlvars1 = tlvars;
            try {
                tlreq.init(true);
                tlvars1.returnDoc = tlreq.write("_process=" + s1
                        + "&_sessionid=" + tlvars.sessionId.toString()
                        + "&_sessionkey=" + tlvars.sessionKey.toString());
                tlvars.internalDoc = false;
                if (tlvars1.returnDoc != null && tlvars1.returnDoc.length() > 0) {
                    tlvars1.internalDoc = true;
                    saxreturndocument = new SAXReturnDocument(tlvars1);
                    saxreturndocument.init(tlvars1);
                }
            } catch (Exception exception1) {
                throw new SAXException(exception1.toString());
            }
        } else if (tlvars.defineS)
            defineSchema(s);
        lastoutString = super.outString;
        super.outString = new StringBuffer(tlvars.defStrBufSize);
        ctr = new Integer(0);
        tlvars.blocks = new Integer(tlvars.blocks.intValue() + 1);
        if (!tlvars.noUpload && saxreturndocument != null && !tlvars.defineS)
            saxreturndocument.start();
        try {
            tlreq.init(true);
            tlvars.returnDoc = tlreq.write("_commit=*&_sessionid="
                    + tlvars.sessionId.toString() + "&_sessionkey="
                    + tlvars.sessionKey.toString());
            if (tlvars.returnDoc != null && tlvars.returnDoc.length() > 0) {
                tlvars.internalDoc = true;
                SAXReturnDocument saxreturndocument1 = new SAXReturnDocument(
                        tlvars);
                saxreturndocument1.init(tlvars);
                saxreturndocument1.start();
            }
        } catch (Exception exception) {
            throw new SAXException(exception.toString());
        }
    }

    public void writeUserInfo() {
        if (tlvars.noUpload) {
            String s = TlResource.getMessage("TltotalElements");
            System.out.println(MessageFormat.format(s, new String[] {
                    tlvars.totalElements.toString(), tlvars.fileName }));
        } else if (tlvars.defineS && tlvars.schemOK) {
            System.out.println("\nSchema uploaded");
        } else {
            String s1 = TlResource.getMessage("TltotalElementsUploaded");
            System.out.println(MessageFormat.format(s1, new String[] {
                    tlvars.totalElements.toString(), tlvars.fileName,
                    tlvars.url }));
        }
        String s2 = TlResource.getMessage("TlXMLvolume");
        System.out.println(MessageFormat.format(s2,
                new String[] { tlvars.xmlVolume.toString() }));
        s2 = TlResource.getMessage("TlEscVolume");
        System.out.println(MessageFormat.format(s2,
                new String[] { tlvars.uploadVolume.toString() }));
        s2 = TlResource.getMessage("TlBlocks");
        System.out.println(MessageFormat.format(s2,
                new String[] { tlvars.blocks.toString() }));
        tlvars.blocks = new Integer(0);
    }

    public void defineSchema(String s) throws SAXException {
        Object obj = null;
        TlSAX2 _tmp = this;
        String s1 = "<?xml version=\"1.0\"?>";
        if (tlvars.uniCodeEncoding)
            s1 = TlURLEncoder.encodeUniCode(s);
        else
            s1 = TlURLEncoder.encodeEscape(s);
        TlVars tlvars1 = null;
        tlvars1 = tlvars;
        try {
            tlreq.init(true);
            tlvars1.returnDoc = tlreq.write("_define=" + s1);
            tlvars.internalDoc = false;
            if (tlvars1.returnDoc != null && tlvars1.returnDoc.length() > 0) {
                tlvars1.internalDoc = true;
                SAXReturnDocument saxreturndocument = new SAXReturnDocument(
                        tlvars1);
                saxreturndocument.init(tlvars1);
                saxreturndocument.start();
            }
            tlvars.schemOK = true;
        } catch (Exception exception) {
            tlvars.schemOK = false;
            throw new SAXException(exception.toString());
        }
    }

}
