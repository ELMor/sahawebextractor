// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 

package com.softwareag.xtools.basic.dm;

import com.softwareag.common.instrumentation.contract.Postcondition;
import com.softwareag.common.instrumentation.contract.Precondition;
import com.softwareag.common.instrumentation.logging.Level;
import com.softwareag.common.instrumentation.logging.Logger;
import com.softwareag.common.instrumentation.logging.LoggerFactory;
import com.softwareag.common.instrumentation.logging.LoggerUtil;
import com.softwareag.common.resourceutilities.message.MessageRetrieveException;
import com.softwareag.common.resourceutilities.message.ResourceMessage;
import com.softwareag.common.resourceutilities.message.XMLMessageResourceBundle;
import com.softwareag.tamino.db.api.accessor.TAccessorException;
import com.softwareag.tamino.db.api.common.TException;
import com.softwareag.tamino.db.api.connection.TConnectionCloseException;
import com.softwareag.tamino.db.api.connection.TIsolationDegree;
import com.softwareag.tamino.db.api.connection.TLockMode;
import com.softwareag.tamino.db.api.connection.TLockwaitMode;
import com.softwareag.tamino.db.api.connection.TServerNotAvailableException;
import com.softwareag.tamino.db.api.connection.TTransactionModeChangeException;
import com.softwareag.tamino.db.api.invocation.TAuthorizationException;
import com.softwareag.xtools.basic.MissingServiceException;
import com.softwareag.xtools.basic.TaminoMessage;
import com.softwareag.xtools.basic.URLUtility;
import com.softwareag.xtools.basic.aspect.Collection;
import com.softwareag.xtools.basic.aspect.CollectionAlreadyExistsException;
import com.softwareag.xtools.basic.aspect.ConnectionFailedException;
import com.softwareag.xtools.basic.aspect.Database;
import com.softwareag.xtools.basic.aspect.DatabaseNotConnectedException;
import com.softwareag.xtools.basic.aspect.DisconnectVetoException;
import com.softwareag.xtools.basic.aspect.Document;
import com.softwareag.xtools.basic.aspect.Locatable;
import com.softwareag.xtools.basic.aspect.MetaDataText;
import com.softwareag.xtools.basic.aspect.MetaDataTree;
import com.softwareag.xtools.basic.aspect.QueryContext;
import com.softwareag.xtools.basic.aspect.QuerySupport;
import com.softwareag.xtools.basic.aspect.RefreshException;
import com.softwareag.xtools.basic.aspect.Refreshable;
import com.softwareag.xtools.basic.aspect.Removable;
import com.softwareag.xtools.basic.aspect.Server;
import com.softwareag.xtools.basic.aspect.event.DatabaseEvent;
import com.softwareag.xtools.basic.aspect.event.DatabaseListener;
import com.softwareag.xtools.basic.dm.XDOM.XDOMException;
import com.softwareag.xtools.basic.dm.XDOM.XDOMHierarchyRequestException;
import com.softwareag.xtools.basic.dm.XDOM.XDOMInvalidCharException;
import com.softwareag.xtools.basic.dm.XDOM.XDOMNotModifiableException;
import com.softwareag.xtools.basic.dm.XDOM.XDOMWrongDocumentException;
import com.softwareag.xtools.basic.dm.XDOM.XDocument;
import com.softwareag.xtools.basic.dm.XDOM.XDocumentBuilder;
import com.softwareag.xtools.basic.dm.XDOM.XElement;
import com.softwareag.xtools.basic.ta.AddCollectionException;
import com.softwareag.xtools.basic.ta.TaAccessor;
import com.softwareag.xtools.basic.ta.TaConnection;
import com.softwareag.xtools.basic.ta.TaConnectionAccessor;
import com.softwareag.xtools.basic.ta.TaConnectionFactory;
import com.softwareag.xtools.basic.ta.TaConnectionImpl;
import com.softwareag.xtools.basic.ta.TaQueryException;
import com.softwareag.xtools.basic.ta.TaSchemaType;
import com.softwareag.xtools.basic.ta.TaTimeoutException;
import com.softwareag.xtools.basic.ui.IconCache;
import com.softwareag.xtools.basic.ui.IconConstants;
import com.softwareag.xtools.basic.ui.ResourceBundleHelper;
import com.softwareag.xtools.common.framework.aspect.Aspect;
import com.softwareag.xtools.common.framework.aspect.BeanContextAware;
import com.softwareag.xtools.common.framework.aspect.io.Externalizable;
import com.softwareag.xtools.common.framework.aspect.io.Internalizable;
import com.softwareag.xtools.common.framework.aspect.io.MaterialInput;
import com.softwareag.xtools.common.framework.aspect.io.MaterialInputException;
import com.softwareag.xtools.common.framework.aspect.io.MaterialOutput;
import com.softwareag.xtools.common.framework.aspect.io.MaterialOutputException;
import com.softwareag.xtools.common.framework.service.message.ChoiceType;
import com.softwareag.xtools.common.framework.service.message.SimpleMessageService;
import com.softwareag.xtools.common.framework.service.parameter.ParameterService;
import com.softwareag.xtools.common.standard.util.Condition;
import com.softwareag.xtools.common.standard.util.PiggybackException;
import java.io.CharArrayReader;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.swing.tree.DefaultMutableTreeNode;

// Referenced classes of package com.softwareag.xtools.basic.dm:
//            AbstractTimeoutHandlingNode, XMLDocument, CollectionImpl, OpenFailedException, 
//            ConnectionHandler, AbstractMutableNode, TimeoutExceptionHandler, AbstractServiceAwareNode

public class DatabaseImpl extends AbstractTimeoutHandlingNode
    implements Database, Refreshable, Removable, ConnectionHandler, Externalizable, Internalizable, QueryContext, MetaDataTree, MetaDataText, DatabaseListener, Document, QuerySupport
{
    protected static class CollectionIdentifier
        implements Comparable
    {

        private String collectionName;
        private TaSchemaType schemaType;
        private boolean systemCollection;

        public String getCollectionName()
        {
            return collectionName;
        }

        public TaSchemaType getSchemaType()
        {
            return schemaType;
        }

        public boolean isSystemCollection()
        {
            return systemCollection;
        }

        public int compareTo(Object obj)
        {
            int i = -1;
            String s = null;
            TaSchemaType taschematype = null;
            boolean flag = false;
            if(obj instanceof CollectionIdentifier)
            {
                s = ((CollectionIdentifier)obj).getCollectionName();
                taschematype = ((CollectionIdentifier)obj).getSchemaType();
                flag = ((CollectionIdentifier)obj).isSystemCollection();
            } else
            if(obj instanceof Collection)
            {
                s = ((Collection)obj).getCollectionName();
                taschematype = ((Collection)obj).getSchemaType();
                flag = ((Collection)obj).isSystemCollection();
            }
            if((s != null) & (taschematype != null))
                if(isSystemCollection())
                {
                    if(flag)
                    {
                        i = getSchemaType().compareTo(taschematype);
                        if(i == 0)
                            i = getCollectionName().compareTo(s);
                    } else
                    {
                        i = 1;
                    }
                } else
                if(flag)
                {
                    i = -1;
                } else
                {
                    i = getSchemaType().compareTo(taschematype);
                    if(i == 0)
                        i = getCollectionName().compareTo(s);
                }
            return i;
        }

        public boolean equals(Object obj)
        {
            boolean flag = false;
            String s = null;
            TaSchemaType taschematype = null;
            boolean flag1 = false;
            if(obj instanceof CollectionIdentifier)
            {
                CollectionIdentifier collectionidentifier = (CollectionIdentifier)obj;
                s = collectionidentifier.getCollectionName();
                taschematype = collectionidentifier.getSchemaType();
                flag1 = collectionidentifier.isSystemCollection();
            } else
            if(obj instanceof Collection)
            {
                Collection collection = (Collection)obj;
                s = collection.getCollectionName();
                taschematype = collection.getSchemaType();
                flag1 = collection.isSystemCollection();
            }
            if(getCollectionName() == null)
                flag = s == null;
            else
                flag = s != null && getCollectionName().equals(s);
            if(getSchemaType() == null)
                flag &= taschematype == null;
            else
                flag &= taschematype != null && getSchemaType().equals(taschematype);
            flag &= isSystemCollection() == flag1;
            return flag;
        }

        public int hashCode()
        {
            int i = 0;
            if(getCollectionName() != null)
                i = getCollectionName().hashCode();
            i += 13 * getSchemaType().hashCode();
            if(isSystemCollection())
                i *= 7;
            return i;
        }

        public String toString()
        {
            StringBuffer stringbuffer = new StringBuffer("CollectionIdentifier[name=");
            stringbuffer.append(getCollectionName());
            stringbuffer.append(", schemaType=");
            stringbuffer.append(getSchemaType());
            stringbuffer.append("]");
            return stringbuffer.toString();
        }

        public CollectionIdentifier(String s, TaSchemaType taschematype, boolean flag)
        {
            collectionName = null;
            schemaType = null;
            systemCollection = false;
            collectionName = s;
            schemaType = taschematype;
            systemCollection = flag;
        }
    }

    protected static class CollectionComparator
        implements Comparator
    {

        public int compare(Object obj, Object obj1)
        {
            int i = -1;
            CollectionIdentifier collectionidentifier = null;
            CollectionIdentifier collectionidentifier1 = null;
            if(obj instanceof Collection)
            {
                Collection collection = (Collection)obj;
                collectionidentifier = new CollectionIdentifier(collection.getCollectionName(), collection.getSchemaType(), collection.isSystemCollection());
            } else
            if(obj instanceof CollectionIdentifier)
                collectionidentifier = (CollectionIdentifier)obj;
            if(obj1 instanceof Collection)
            {
                Collection collection1 = (Collection)obj;
                collectionidentifier1 = new CollectionIdentifier(collection1.getCollectionName(), collection1.getSchemaType(), collection1.isSystemCollection());
            } else
            if(obj1 instanceof CollectionIdentifier)
                collectionidentifier1 = (CollectionIdentifier)obj1;
            if(collectionidentifier != null && collectionidentifier1 != null)
                i = collectionidentifier.compareTo(collectionidentifier1);
            return i;
        }

        protected CollectionComparator()
        {
        }
    }


    private static final boolean PRE_CHECK;
    private static final boolean POST_CHECK;
    private static final String LOG_NAME = LoggerUtil.getThisClassName();
    private static Logger logger = LoggerFactory.getLogger(LOG_NAME);
    private static ResourceBundle myMessages = XMLMessageResourceBundle.getBundle("com.softwareag.xtools.xplorer.resources.XPlorerMessages", null, null);
    private static final String RESOURCES = "com.softwareag.xtools.basic.dm.resources.databaseresources";
    private static final String HIDE_SYSTEM_COLLECTIONS_PARAMETER_KEY = "com.softwareag.xtools.xplorer.main.options.HideSystemCollections";
    public static final String CONNECTION_NO_LONGER_AVAILABLE = "Connection no longer available.";
    public static final String DATABASE_URL_IS_NULL = "Database URL is null.";
    public static final String CONNECTION_ALREADY_EXISTS = "Database with differnt userID and password already connected.";
    public static final String CREATE_CONNECTION_FAILED = "Failed to create a connection.";
    public static final String AUTHORIZATION_FAILED = "Authorization failed.";
    private static final String DATABASE_TAG = "database";
    private static final String CONNECTED_AS_TAG = "ConnectedAs";
    private static final String NAME_TAG = "name";
    private static final String USER_ID_TAG = "userID";
    private static final String STATE_TAG = "state";
    private static final String SERVER_TAG = "server";
    private static final String URL_TAG = "URL";
    private static final String VERSION_TAG = "version";
    private static final String CONNECTED_STATE = "connected";
    private static final String DISCONNECTED_STATE = "disconnected";
    private static final String INFO_NOT_AVAILABLE = "Information_not_available";
    private static final String INDENT = "    ";
    private static final String META_DATA_HINT_TAG = ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "META_DATA_HINT", "see properties");
    private static final String META_DATA_TAG = ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "META_DATA_TAG", "ArtificalNode");
    private static final String META_DATA_HINT_NAME = ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "META_DATA_HINT_NAME", "hint");
    private static Set availableAspects;
    private Icon connectedIcon;
    private Icon disconnectedIcon;
    private Server server;
    private String databaseName;
    private String userID;
    private String password;
    private TaConnectionFactory connectionFactory;
    private TaConnection connection;
    private TaConnection xQueryConnection;
    private int nxQueryConnectionCount;
    private Comparator collectionComparator;
    private URL locationURL;
    private Document document;
    private boolean systemCollectionsEnabled;
    private EventListenerList listenerList;
    private XMLDocument metaDataTree;
    private String metaText;
    private Condition initialized;
    private TaminoMessage tm;
    private boolean handlesConnectionException;

    public DatabaseImpl()
    {
        this(null, null);
    }

    public DatabaseImpl(String s)
    {
        this(null, s);
    }

    public DatabaseImpl(Server server1, String s)
    {
        this(server1, s, TaConnectionFactory.getInstance());
    }

    DatabaseImpl(Server server1, String s, TaConnectionFactory taconnectionfactory)
    {
        connectedIcon = IconCache.getTheCache().getIcon(IconConstants.DATABASE_ICON);
        disconnectedIcon = IconCache.getTheCache().getIcon(IconConstants.DATABASE_DICONNECTED_ICON);
        server = null;
        databaseName = "";
        userID = "";
        password = "";
        nxQueryConnectionCount = 0;
        collectionComparator = new CollectionComparator();
        locationURL = null;
        document = null;
        systemCollectionsEnabled = true;
        listenerList = null;
        metaDataTree = null;
        metaText = null;
        initialized = new Condition();
        tm = null;
        handlesConnectionException = false;
        server = server1;
        databaseName = s;
        connectionFactory = taconnectionfactory;
        addDatabaseListener(this);
    }

    public URL getLocationURL()
    {
        if(locationURL == null && (getServer() != null && getServer().getLocationURL() != null) & (getDatabaseName() != null))
            try
            {
                locationURL = URLUtility.concat(getServer().getLocationURL(), getDatabaseName());
            }
            catch(MalformedURLException malformedurlexception)
            {
                if(logger.isLoggable(Level.FINE))
                    logger.throwing(LOG_NAME, "Either the server URL (" + getServer().getLocationURL() + ") or the database name (" + getDatabaseName() + ") is invalid", malformedurlexception);
            }
        return locationURL;
    }

    public boolean hasLocationURL(URL url)
    {
        return URLUtility.areURLsEqual(getLocationURL(), url);
    }

    public Collection createCollection(String s)
        throws DatabaseNotConnectedException, CollectionAlreadyExistsException, AddCollectionException, TaTimeoutException
    {
        Collection collection = null;
        updateChildren();
        if(!isConnected())
            throw new DatabaseNotConnectedException();
        TaConnection taconnection = null;
        try
        {
            taconnection = createAutoCommitConnection();
        }
        catch(ConnectionFailedException connectionfailedexception)
        {
            throw new AddCollectionException(connectionfailedexception, "Connection to database could not be established");
        }
        try
        {
            refresh(false);
        }
        catch(RefreshException refreshexception)
        {
            throw new AddCollectionException(refreshexception);
        }
        if(getCollection(s) != null)
            throw new CollectionAlreadyExistsException(this, s);
        try
        {
            TaAccessor taaccessor = taconnection.getAccessor("ino:collection", taconnection.getPreferredSchemaType());
            taaccessor.addCollection(s);
            refresh(false);
            collection = getCollection(s);
        }
        catch(TaTimeoutException tatimeoutexception)
        {
            throw tatimeoutexception;
        }
        catch(RefreshException refreshexception1)
        {
            throw new AddCollectionException(refreshexception1);
        }
        finally
        {
            try
            {
                taconnection.close();
            }
            catch(TConnectionCloseException tconnectioncloseexception)
            {
                throw new AddCollectionException(tconnectioncloseexception, "Failed to close temporary connection to database");
            }
        }
        return collection;
    }

    public synchronized Collection getCollection(String s)
        throws DatabaseNotConnectedException
    {
        if(!isConnected())
            throw new DatabaseNotConnectedException();
        updateChildren();
        Collection collection = null;
        Iterator iterator = getCollections();
        Object obj = null;
        while((collection == null) & iterator.hasNext()) 
        {
            Object obj1 = iterator.next();
            if((obj1 instanceof Collection) && ((Collection)obj1).getCollectionName().equals(s))
                collection = (Collection)obj1;
        }
        return collection;
    }

    public synchronized boolean hasCollection(String s)
        throws DatabaseNotConnectedException
    {
        return getCollection(s) != null;
    }

    public synchronized Iterator getCollections()
        throws DatabaseNotConnectedException
    {
        if(!isConnected())
            throw new DatabaseNotConnectedException();
        else
            return getCollectionsInternal();
    }

    protected synchronized Iterator getCollectionsInternal()
    {
        return iterator();
    }

    public boolean getSystemCollectionsEnabled()
    {
        return systemCollectionsEnabled;
    }

    public void setSystemCollectionsEnabled(boolean flag)
    {
        if(flag != systemCollectionsEnabled)
            systemCollectionsEnabled = flag;
    }

    public void addDatabaseListener(DatabaseListener databaselistener)
    {
        if(listenerList == null)
            listenerList = new EventListenerList();
        listenerList.add(com.softwareag.xtools.basic.aspect.event.DatabaseListener.class, databaselistener);
    }

    public void removeDatabaseListener(DatabaseListener databaselistener)
    {
        if(listenerList != null)
            listenerList.remove(com.softwareag.xtools.basic.aspect.event.DatabaseListener.class, databaselistener);
    }

    protected void fireConnected()
    {
        if(listenerList != null)
        {
            DatabaseListener adatabaselistener[] = (DatabaseListener[])listenerList.getListeners(com.softwareag.xtools.basic.aspect.event.DatabaseListener.class);
            if(adatabaselistener.length > 0)
            {
                DatabaseEvent databaseevent = new DatabaseEvent(this);
                for(int i = 0; i < adatabaselistener.length; i++)
                    adatabaselistener[i].connectedDatabase(databaseevent);

            }
        }
    }

    protected void fireDisconnecting()
        throws DisconnectVetoException
    {
        if(listenerList != null)
        {
            DatabaseListener adatabaselistener[] = (DatabaseListener[])listenerList.getListeners(com.softwareag.xtools.basic.aspect.event.DatabaseListener.class);
            if(adatabaselistener.length > 0)
            {
                DatabaseEvent databaseevent = new DatabaseEvent(this);
                for(int i = 0; i < adatabaselistener.length; i++)
                    adatabaselistener[i].disconnectingDatabase(databaseevent);

            }
        }
    }

    protected void fireDisconnected()
    {
        if(listenerList != null)
        {
            DatabaseListener adatabaselistener[] = (DatabaseListener[])listenerList.getListeners(com.softwareag.xtools.basic.aspect.event.DatabaseListener.class);
            if(adatabaselistener.length > 0)
            {
                DatabaseEvent databaseevent = new DatabaseEvent(this);
                for(int i = 0; i < adatabaselistener.length; i++)
                    adatabaselistener[i].disconnectedDatabase(databaseevent);

            }
        }
    }

    public Icon getIcon()
    {
        return isConnected() ? connectedIcon : disconnectedIcon;
    }

    public String getRepresentationName()
    {
        return getDatabaseName();
    }

    public String getDatabaseName()
    {
        if(POST_CHECK)
            Postcondition.check("return is null", databaseName != null);
        return databaseName;
    }

    public synchronized void setDatabaseName(String s)
    {
        databaseName = s;
        fireNodeChanged();
    }

    public String getUserID()
    {
        if(POST_CHECK)
            Postcondition.check("return is null", userID != null);
        return userID;
    }

    public synchronized void setUserID(String s)
    {
        userID = s;
        fireNodeChanged();
    }

    public String getPassword()
    {
        if(POST_CHECK)
            Postcondition.check("return is null", password != null);
        return password;
    }

    public synchronized void setPassword(String s)
    {
        if(PRE_CHECK)
            Precondition.check("password is null", s != null);
        password = s;
        fireNodeChanged();
    }

    public Database getDatabase()
    {
        return this;
    }

    public Aspect getAspect(Class class1)
    {
        if(class1.equals(com.softwareag.xtools.basic.aspect.Document.class))
            return this;
        else
            return class1.isAssignableFrom(getClass()) ? this : null;
    }

    public Set getAvailableAspectTypes()
    {
        return availableAspects;
    }

    public boolean hasAspect(Class class1)
    {
        return availableAspects.contains(class1);
    }

    public boolean isSame(Aspect aspect)
    {
        return aspect != null && this == aspect.getAspect(com.softwareag.xtools.basic.aspect.Database.class);
    }

    public boolean equals(Object obj)
    {
        return (obj instanceof Aspect) && this == ((Aspect)obj).getAspect(com.softwareag.xtools.basic.aspect.Database.class);
    }

    public Document getDocument()
    {
        if(document == null)
            setDocument(new XMLDocument(this, getInfoForMetaDataViewer(), true));
        return document;
    }

    protected XElement getInfoForMetaDataViewer()
    {
        XElement xelement = null;
        try
        {
            XDocument xdocument = XDocumentBuilder.newDocument();
            xelement = xdocument.createElement(META_DATA_TAG);
            xelement.setAttribute(META_DATA_HINT_NAME, META_DATA_HINT_TAG);
        }
        catch(XDOMException xdomexception)
        {
            if(logger.isLoggable(Level.FINE))
                logger.throwing(LOG_NAME, "This should not happen since we do not use any special character", xdomexception);
        }
        return xelement;
    }

    public XMLDocument getMetaDataAsXML()
    {
        if(metaDataTree == null)
            metaDataTree = new XMLDocument(this, createXElement(), true);
        return metaDataTree;
    }

    public XMLDocument getMetaData()
    {
        return getMetaDataAsXML();
    }

    public String getMimeTypeOfProperties()
    {
        return "text/text";
    }

    protected void setDocument(Document document1)
    {
        document = document1;
    }

    protected XElement createXElement()
    {
        XElement xelement = null;
        try
        {
            XDocument xdocument = XDocumentBuilder.newDocument();
            xelement = xdocument.createElement(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "database", "database"));
            xelement.setAttribute(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "name", "name"), getDatabaseName());
            XElement xelement1 = xdocument.createElement(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "server", "server"));
            xelement.appendChild(xelement1);
            URL url = getServerURL();
            if(url != null)
            {
                XElement xelement2 = xdocument.createElement(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "URL", "URL"));
                com.softwareag.xtools.basic.dm.XDOM.XText xtext = xdocument.createText(url.toString());
                xelement2.appendChild(xtext);
                xelement1.appendChild(xelement2);
            }
            if(isConnected())
            {
                String s = ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "Information_not_available", "Information_not_available");
                try
                {
                    s = connection.getServerVersion();
                }
                catch(TAccessorException taccessorexception)
                {
                    if(logger.isLoggable(Level.FINE))
                        logger.throwing(LOG_NAME, "Can't determine server version", taccessorexception);
                }
                catch(TaTimeoutException tatimeoutexception)
                {
                    if(logger.isLoggable(Level.FINE))
                        logger.throwing(LOG_NAME, "Can't determine server version, because of timeout", tatimeoutexception);
                }
                XElement xelement4 = xdocument.createElement(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "version", "version"));
                com.softwareag.xtools.basic.dm.XDOM.XText xtext2 = xdocument.createText(s);
                xelement4.appendChild(xtext2);
                xelement1.appendChild(xelement4);
                xelement4 = xdocument.createElement(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "ConnectedAs", "ConnectedAs"));
                xtext2 = xdocument.createText(getUserID());
                xelement4.appendChild(xtext2);
                xelement1.appendChild(xelement4);
                xelement4 = xdocument.createElement(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "state", "state"));
                xtext2 = xdocument.createText(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "connected", "connected"));
                xelement4.appendChild(xtext2);
                xelement.appendChild(xelement4);
            } else
            {
                XElement xelement3 = xdocument.createElement(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "state", "state"));
                com.softwareag.xtools.basic.dm.XDOM.XText xtext1 = xdocument.createText(ResourceBundleHelper.getStringResource("com.softwareag.xtools.basic.dm.resources.databaseresources", "disconnected", "disconnected"));
                xelement3.appendChild(xtext1);
                xelement.appendChild(xelement3);
            }
        }
        catch(XDOMInvalidCharException xdominvalidcharexception)
        {
            if(logger.isLoggable(Level.FINE))
                logger.throwing(LOG_NAME, "This should not happen", xdominvalidcharexception);
        }
        catch(XDOMNotModifiableException xdomnotmodifiableexception)
        {
            if(logger.isLoggable(Level.FINE))
                logger.throwing(LOG_NAME, "This should not happen", xdomnotmodifiableexception);
        }
        catch(XDOMWrongDocumentException xdomwrongdocumentexception)
        {
            if(logger.isLoggable(Level.FINE))
                logger.throwing(LOG_NAME, "This should not happen", xdomwrongdocumentexception);
        }
        catch(XDOMHierarchyRequestException xdomhierarchyrequestexception)
        {
            if(logger.isLoggable(Level.FINE))
                logger.throwing(LOG_NAME, "This should not happen", xdomhierarchyrequestexception);
        }
        return xelement;
    }

    public void remove()
    {
        removeFromParent();
    }

    public synchronized void connect(String s, String s1)
        throws ConnectionFailedException
    {
        if(PRE_CHECK)
        {
            Precondition.check("userID is null", s != null);
            Precondition.check("password is null", s1 != null);
        }
        URL url = getLocationURL();
        if(url == null)
            throw new ConnectionFailedException(null, "Database URL is null.");
        if(connection != null && !connection.isClosed())
            if(getUserID().equals(s) && getPassword().equals(s1))
            {
                if(!((TaConnectionImpl)connection).isAlive())
                    throw new ConnectionFailedException(null, "Connection no longer available.");
                else
                    return;
            } else
            {
                throw new ConnectionFailedException(null, "Database with differnt userID and password already connected.");
            }
        try
        {
            connection = connectionFactory.newConnection(url.toString(), s, s1, false);
            setUserID(s);
            setPassword(s1);
        }
        catch(TServerNotAvailableException tservernotavailableexception)
        {
            if(containsAuthorizationException(tservernotavailableexception))
                throw new ConnectionFailedException(tservernotavailableexception, "Authorization failed.");
            else
                throw new ConnectionFailedException(tservernotavailableexception, "Failed to create a connection.");
        }
        catch(TTransactionModeChangeException ttransactionmodechangeexception)
        {
            if(containsAuthorizationException(ttransactionmodechangeexception))
                throw new ConnectionFailedException(ttransactionmodechangeexception, "Authorization failed.");
            else
                throw new ConnectionFailedException(ttransactionmodechangeexception, "Failed to create a connection.");
        }
        setDocument(null);
        fireConnected();
        fireNodeChanged();
    }

    public synchronized void disconnect()
        throws DisconnectVetoException
    {
        if(isConnected())
        {
            try
            {
                fireDisconnecting();
                connection.close();
                if(xQueryConnection != null && !xQueryConnection.isClosed())
                {
                    xQueryConnection.close();
                    nxQueryConnectionCount = 0;
                }
            }
            catch(TConnectionCloseException tconnectioncloseexception) { }
            removeAll(true);
            initialized.clear();
            connection = null;
            xQueryConnection = null;
            setPassword("");
            setDocument(null);
            fireNodeChanged();
            fireDisconnected();
        }
        if(POST_CHECK)
            Postcondition.check("isConnected()", !isConnected());
    }

    public synchronized boolean isConnected()
    {
        if(connection != null)
            return !connection.isClosed();
        else
            return false;
    }

    public synchronized boolean isAlive()
    {
        if(connection != null)
            return connection.isAlive();
        else
            return false;
    }

    public boolean handleConnectionException(Throwable throwable)
    {
        if(PRE_CHECK)
            Precondition.check("t must not be null", throwable != null);
        if(handlesConnectionException)
            return true;
        Throwable throwable1;
        for(throwable1 = throwable; throwable1 != null && (com.softwareag.xtools.common.standard.util.PiggybackException.class).isAssignableFrom(throwable1.getClass()); throwable1 = ((PiggybackException)throwable1).getContainedException());
        if(!(com.softwareag.tamino.db.api.common.TException.class).isAssignableFrom(throwable1.getClass()))
            return false;
        if(isAlive())
        {
            return false;
        } else
        {
            tm = new TaminoMessage(myMessages, "XPLXPE0064", new ChoiceType[] {
                ChoiceType.OK
            });
            tm.setDetails(throwable);
            handlesConnectionException = true;
            SwingUtilities.invokeLater(new Runnable() {

                public void run()
                {
                    try
                    {
                        displayMessage(tm);
                        disconnect();
                    }
                    catch(DisconnectVetoException disconnectvetoexception)
                    {
                        if(DatabaseImpl.logger.isLoggable(Level.FINE))
                            DatabaseImpl.logger.throwing(DatabaseImpl.LOG_NAME, "DisconnectVetoException!", disconnectvetoexception);
                    }
                    finally
                    {
                        handlesConnectionException = false;
                    }
                }

            });
            return true;
        }
    }

    public void writeTo(MaterialOutput materialoutput)
        throws MaterialOutputException
    {
        materialoutput.writeString("name", databaseName);
        if(getUserID() != null)
            materialoutput.writeString("userID", getUserID());
    }

    public String getClassIdentifier()
    {
        return "database";
    }

    public void readFrom(MaterialInput materialinput)
        throws MaterialInputException
    {
        String s = materialinput.readString("userID");
        String s1 = materialinput.readString("name");
        if(s1 != null)
        {
            setDatabaseName(s1);
            if(s != null)
                setUserID(s);
        }
    }

    public synchronized String toString()
    {
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append(getClass().getName());
        stringbuffer.append("[");
        stringbuffer.append("databaseName=");
        stringbuffer.append(getDatabaseName());
        if(getUserID() != null)
        {
            stringbuffer.append(", userID=");
            stringbuffer.append(getUserID());
        }
        stringbuffer.append("]");
        return stringbuffer.toString();
    }

    public synchronized Object clone()
    {
        DatabaseImpl databaseimpl = new DatabaseImpl(databaseName);
        if(getUserID() != null)
            databaseimpl.setUserID(getUserID());
        if(getPassword() != null)
            databaseimpl.setPassword(getPassword());
        return databaseimpl;
    }

    public Server getServer()
    {
        if(POST_CHECK)
            Postcondition.check("return is null", server != null);
        return server;
    }

    public void setServer(Server server1)
    {
        server = server1;
    }

    public synchronized boolean isLeaf()
    {
        if(initialized.isSet())
            return getChildCount() == 0;
        else
            return false;
    }

    public TaConnection createAutoCommitConnection()
        throws ConnectionFailedException
    {
        URL url = getLocationURL();
        if(url == null)
            throw new ConnectionFailedException(null, "Database URL is null.");
        try
        {
            return connectionFactory.newConnection(url.toString(), userID, password);
        }
        catch(TServerNotAvailableException tservernotavailableexception)
        {
            if(containsAuthorizationException(tservernotavailableexception))
                throw new ConnectionFailedException(tservernotavailableexception, "Authorization failed.");
            else
                throw new ConnectionFailedException(tservernotavailableexception, "Failed to create a connection.");
        }
    }

    public TaConnection createXQueryConnection()
        throws ConnectionFailedException
    {
        URL url = getLocationURL();
        if(url == null)
            throw new ConnectionFailedException(null, "Database URL is null.");
        try
        {
            return connectionFactory.newConnection(url.toString(), userID, password, TIsolationDegree.UNCOMMITTED_DOCUMENT, TLockMode.SHARED, TLockwaitMode.NO);
        }
        catch(TServerNotAvailableException tservernotavailableexception)
        {
            if(containsAuthorizationException(tservernotavailableexception))
                throw new ConnectionFailedException(tservernotavailableexception, "Authorization failed.");
            else
                throw new ConnectionFailedException(tservernotavailableexception, "Failed to create a connection.");
        }
        catch(TTransactionModeChangeException ttransactionmodechangeexception)
        {
            if(containsAuthorizationException(ttransactionmodechangeexception))
                throw new ConnectionFailedException(ttransactionmodechangeexception, "Authorization failed.");
            else
                throw new ConnectionFailedException(ttransactionmodechangeexception, "Failed to create a connection.");
        }
    }

    public void closeXQueryConnection()
    {
        if(nxQueryConnectionCount > 0)
            nxQueryConnectionCount--;
        if(nxQueryConnectionCount == 0 && xQueryConnection != null && !xQueryConnection.isClosed())
            try
            {
                xQueryConnection.close();
                xQueryConnection = null;
            }
            catch(TConnectionCloseException tconnectioncloseexception)
            {
                tconnectioncloseexception.printStackTrace();
            }
    }

    public TaConnection getXQueryConnection()
        throws ConnectionFailedException
    {
        if(xQueryConnection == null || xQueryConnection.isClosed())
            xQueryConnection = createXQueryConnection();
        nxQueryConnectionCount++;
        return xQueryConnection;
    }

    private boolean open()
        throws DatabaseNotConnectedException, OpenFailedException
    {
        boolean flag = false;
        if(!isConnected())
            throw new DatabaseNotConnectedException();
        //updateParameter();
        TaAccessor taaccessor = connection.getAccessor("ino:collection", TaSchemaType.TSD2);
        TaAccessor taaccessor1 = connection.getAccessor("ino:collection", TaSchemaType.TSD3);
        try
        {
            ArrayList arraylist = new ArrayList();
            boolean flag1 = false;
            Object obj = null;
            for(Iterator iterator = taaccessor.getAllCollectionNames(); iterator.hasNext();)
            {
                String s = (String)iterator.next();
                boolean flag2 = connection.getConnectionAccessor().isSystemCollection(s);
                if(!flag2 || getSystemCollectionsEnabled())
                    arraylist.add(new CollectionIdentifier(s, TaSchemaType.TSD2, flag2));
            }

            for(Iterator iterator1 = taaccessor1.getAllCollectionNames(); iterator1.hasNext();)
            {
                String s1 = (String)iterator1.next();
                boolean flag3 = connection.getConnectionAccessor().isSystemCollection(s1);
                if(!flag3 || getSystemCollectionsEnabled())
                    arraylist.add(new CollectionIdentifier(s1, TaSchemaType.TSD3, flag3));
            }

            Collections.sort(arraylist);
            Iterator iterator2 = getCollectionsInternal();
            Object obj1 = null;
            while(iterator2.hasNext()) 
            {
                CollectionImpl collectionimpl = (CollectionImpl)iterator2.next();
                if(!arraylist.contains(new CollectionIdentifier(collectionimpl.getCollectionName(), collectionimpl.getSchemaType(), collectionimpl.isSystemCollection())))
                {
                    remove(collectionimpl);
                    iterator2 = getCollectionsInternal();
                }
            }
            Iterator iterator3 = arraylist.iterator();
            int i = 0;
            Object obj2 = null;
            Object obj3 = null;
            while(iterator3.hasNext()) 
            {
                CollectionIdentifier collectionidentifier = (CollectionIdentifier)iterator3.next();
                CollectionImpl collectionimpl1 = (CollectionImpl)findChild(collectionidentifier, collectionComparator);
                if(collectionimpl1 != null)
                {
                    if(super.children.elementAt(i) != collectionimpl1)
                    {
                        flag = true;
                        super.children.remove(collectionimpl1);
                        super.children.insertElementAt(collectionimpl1, i);
                    }
                } else
                {
                    TaAccessor taaccessor2 = connection.getAccessor(collectionidentifier.getCollectionName(), collectionidentifier.getSchemaType());
                    insertChildNode(new CollectionImpl(this, collectionidentifier.getCollectionName(), taaccessor2, this), i);
                }
                i++;
            }
        }
        catch(TaQueryException taqueryexception)
        {
            throw new OpenFailedException(taqueryexception);
        }
        catch(TaTimeoutException tatimeoutexception)
        {
            getTimeoutExceptionHandler().handleTimeoutException(tatimeoutexception);
        }
        return flag;
    }

    public void refresh(boolean flag)
        throws RefreshException
    {
        if(initialized.isSet())
        {
            try
            {
                if(isConnected() && open())
                    fireNodeStructureChanged();
            }
            catch(OpenFailedException openfailedexception)
            {
                if(logger.isLoggable(Level.FINE))
                    logger.throwing(LOG_NAME, "Open failed...", openfailedexception);
                throw new RefreshException(this, openfailedexception, "Open failed...");
            }
            catch(DatabaseNotConnectedException databasenotconnectedexception)
            {
                if(logger.isLoggable(Level.FINE))
                    logger.throwing(LOG_NAME, "This should not happen since isConnected() returned 'true'!", databasenotconnectedexception);
                throw new RefreshException(this, databasenotconnectedexception, "Database not connected...");
            }
            if(flag)
            {
                Iterator iterator = getCollectionsInternal();
                Object obj = null;
                while(iterator.hasNext()) 
                {
                    Object obj1 = iterator.next();
                    if(obj1 instanceof Refreshable)
                        ((Refreshable)obj1).refresh(flag);
                }
            }
        }
    }

    protected void updateChildren()
    {
        if(isConnected() && !initialized.testAndSet())
            try
            {
                open();
            }
            catch(OpenFailedException openfailedexception)
            {
                if(logger.isLoggable(Level.FINE))
                    logger.throwing(LOG_NAME, "Open failed...", openfailedexception);
                handleExceptionDuringOpen(openfailedexception);
            }
            catch(DatabaseNotConnectedException databasenotconnectedexception)
            {
                if(logger.isLoggable(Level.FINE))
                    logger.throwing(LOG_NAME, "Database not connected.", databasenotconnectedexception);
                handleExceptionDuringOpen(databasenotconnectedexception);
            }
    }

    public URL getServerURL()
    {
        URL url = null;
        try
        {
            url = getServer().getLocationURL();
        }
        catch(NullPointerException nullpointerexception) { }
        return url;
    }

    public Collection getCollection()
    {
        return null;
    }

    public String getCollectionName()
    {
        return null;
    }

    public String getQueryString()
    {
        return null;
    }

    public BeanContextAware getBeanContextAware()
    {
        BeanContextAware beancontextaware = null;
        try
        {
            beancontextaware = (BeanContextAware)getServer().getUniverse().getAspect(com.softwareag.xtools.common.framework.aspect.BeanContextAware.class);
        }
        catch(NullPointerException nullpointerexception) { }
        return beancontextaware;
    }

    protected void updateParameter()
    {
        ParameterService parameterservice = (ParameterService)getService(com.softwareag.xtools.common.framework.service.parameter.ParameterService.class);
        if(parameterservice == null)
            throw new MissingServiceException(com.softwareag.xtools.common.framework.service.parameter.ParameterService.class, this);
        boolean flag = true;
        String s = parameterservice.getParameterValue("com.softwareag.xtools.xplorer.main.options.HideSystemCollections");
        if(s != null && s.trim().equalsIgnoreCase("true"))
            flag = false;
        setSystemCollectionsEnabled(flag);
    }

    public void connectedDatabase(DatabaseEvent databaseevent)
    {
        metaDataTree = null;
    }

    public void disconnectingDatabase(DatabaseEvent databaseevent)
        throws DisconnectVetoException
    {
    }

    public void disconnectedDatabase(DatabaseEvent databaseevent)
    {
        metaDataTree = null;
    }

    public Reader getReader()
    {
        CharArrayReader chararrayreader = null;
        try
        {
            String s = (new TaminoMessage(myMessages, "XPLXPI0025", metaText)).getMessageContent();
            chararrayreader = new CharArrayReader(s.toCharArray());
        }
        catch(MessageRetrieveException messageretrieveexception)
        {
            if(logger.isLoggable(Level.WARNING))
                logger.warning("Message for XPlorerMessageID.XPLXPI0025 could not be retrieved");
        }
        return chararrayreader;
    }

    public String getMimeType()
    {
        return "text/xml";
    }

    public boolean isCharacterData()
    {
        return true;
    }

    public String getEncoding()
    {
        return null;
    }

    public InputStream getInputStream()
    {
        return null;
    }

    public boolean supportsXQuery()
    {
        if(!isConnected())
            return true;
        else
            return connection.supportsXQuery();
    }

    public static void main(String args[])
    {
        CollectionComparator collectioncomparator = new CollectionComparator();
        CollectionIdentifier collectionidentifier = new CollectionIdentifier("a", TaSchemaType.TSD2, true);
        CollectionIdentifier collectionidentifier1 = new CollectionIdentifier("a", TaSchemaType.TSD2, true);
        System.out.println("a2false.equals(b2false): " + collectionidentifier.equals(collectionidentifier1));
        System.out.println("a2false.compareTo(b2false): " + collectionidentifier.compareTo(collectionidentifier1));
        System.out.println("a2false.hashCode(): " + collectionidentifier.hashCode());
        System.out.println("b2false.hashCode(): " + collectionidentifier1.hashCode());
        System.out.println("comparator.compare(a2false, b2false): " + collectioncomparator.compare(collectionidentifier, collectionidentifier1));
    }

    protected void handleExceptionDuringOpen(Exception exception)
    {
        if(PRE_CHECK)
            Precondition.check("exception is null", exception != null);
        if(!handleConnectionException(exception))
        {
            tm = new TaminoMessage(AbstractServiceAwareNode.resourceBundle, "XPLXPE0067", new ChoiceType[] {
                ChoiceType.OK
            }, getDatabaseName());
            tm.setDetails(exception);
            displayMessage(tm);
        }
    }

    protected void displayMessage(TaminoMessage taminomessage)
    {
        if(PRE_CHECK)
            Precondition.check("tm is null", taminomessage != null);
        SimpleMessageService simplemessageservice = (SimpleMessageService)getService(com.softwareag.xtools.common.framework.service.message.SimpleMessageService.class);
        if(simplemessageservice == null)
        {
            throw new MissingServiceException(com.softwareag.xtools.common.framework.service.message.SimpleMessageService.class, this);
        } else
        {
            simplemessageservice.showMessage(taminomessage);
            return;
        }
    }

    private boolean containsAuthorizationException(TException texception)
    {
        if(PRE_CHECK)
            Precondition.check("te is null", texception != null);
        TException texception1 = null;
        texception1 = texception.getRootTCause();
        return texception1 != null && (texception1 instanceof TAuthorizationException);
    }

    public void close()
    {
        try
        {
            if(isConnected())
            {
                connection.close();
                connection = null;
            }
            if(xQueryConnection != null && !xQueryConnection.isClosed())
            {
                xQueryConnection.close();
                xQueryConnection = null;
                nxQueryConnectionCount = 0;
            }
        }
        catch(TConnectionCloseException tconnectioncloseexception) { }
    }

    static 
    {
        PRE_CHECK = Precondition.isEnabled(com.softwareag.xtools.basic.dm.DatabaseImpl.class);
        POST_CHECK = Postcondition.isEnabled(com.softwareag.xtools.basic.dm.DatabaseImpl.class);
        availableAspects = new HashSet();
        availableAspects.add(com.softwareag.xtools.basic.aspect.Connectable.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.MutableNode.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.Database.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.Refreshable.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.Locatable.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.Removable.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.QueryContext.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.QuerySupport.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.Document.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.MetaDataTree.class);
        availableAspects.add(com.softwareag.xtools.basic.aspect.MetaDataText.class);
        availableAspects.add(com.softwareag.xtools.common.framework.aspect.Aspect.class);
        availableAspects = Collections.unmodifiableSet(availableAspects);
    }




}
