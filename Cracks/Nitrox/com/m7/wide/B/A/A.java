// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   A.j

package com.m7.wide.B.A;

import com.m7.A.Q;
import com.m7.A.S;
import com.m7.wide.B.B;
import com.m7.wide.B.F;
import com.m7.wide.B.G;
import com.m7.wide.B.H;
import com.m7.wide.B.J;
import com.m7.wide.B.K;
import com.m7.wide.B.O;
import com.m7.wide.B.P;
import com.m7.wide.ResourceUtil;
import com.m7.wide.doceditor.ImageCache;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import javax.swing.ImageIcon;

public class A
{

    private static Date H = null;
    static final String I = "0001";
    static final String A = "01";
    static final String B = "00";
    public static final Dimension L;
    public static final Dimension C;
    static final String K = "NitroX.l.p.h";
    static final String D = "NitroX.l.p.p";
    static final String J = "NitroX.o.id";
    static final String G = "NitroX.s.nu";
    static ImageIcon E;
    public static RuntimeException F = new RuntimeException("");

    private A()
    {
    }

    public static void A()
    {
        String s = S.A("NitroX.l.p.h");
        s = Q.A(s);
        if(S.A(H))
        {
            throw new RuntimeException(ResourceUtil.getMessage(com.m7.B.C.A.C(), "l4", A(H)));
        } else
        {
            S.A(((Object) (s)), "NitroX.o.id");
            return;
        }
    }

    static void D()
    {
        A(B());
    }

    static void A(G g)
    {
        if(g instanceof com.m7.wide.B.A)
            H = ((com.m7.wide.B.A)g).D();
        else
        if(g instanceof P)
            H = ((P)g).C();
        else
            H = null;
    }

    public static void A(java.util.List list)
        throws IOException
    {
        throw F;
    }

    public static G B()
    {
        return com.m7.wide.B.F.B("0001", "01", "00");
    }

    public static String B(G g)
    {
        return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l5");
    }

    public static String C(G g)
    {
        if(g instanceof J)
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l27");
        if(g instanceof com.m7.wide.B.A)
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l28");
        if(g instanceof P)
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l29");
        if(g instanceof H)
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l30");
        if(g instanceof K)
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l31");
        else
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l32");
    }

    public static String D(G g)
    {
        if(g instanceof J)
        {
            String s = A(((J)g).B().J());
            String s2 = A(((J)g).B().C());
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l40", s, s2);
        }
        if(g instanceof com.m7.wide.B.A)
        {
            long l = B(((com.m7.wide.B.A)g).D());
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l41", Long.toString(l), "buy");
        }
        if(g instanceof P)
        {
            String s1 = A(((P)g).C());
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l42", s1, "buy");
        }
        if(g instanceof H)
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l43", "buy");
        if(g instanceof K)
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l44");
        else
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l32");
    }

    public static String A(O o)
    {
        if(o != null)
        {
            if(o.G())
                return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l35");
            else
                return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l36");
        } else
        {
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l37");
        }
    }

    public static String C()
    {
        return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l38");
    }

    public static String A(B b)
    {
        if(b.B())
        {
            String s = b.getMessage();
            int i = s.indexOf("<body>");
            if(i > 0)
                return "<html>" + s.substring(i);
        } else
        if(b.A())
            return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l33");
        return ResourceUtil.getMessage(com.m7.B.C.A.C(), "l34", b.getMessage());
    }

    private static long B(Date date)
    {
        Date date1 = new Date();
        long l = (date.getTime() - date1.getTime()) / 0x5265c00L;
        return l;
    }

    private static String A(Date date)
    {
        return DateFormat.getDateInstance(1).format(date);
    }

    static 
    {
        L = Toolkit.getDefaultToolkit().getScreenSize();
        C = new Dimension(L.width / 4, L.height / 5);
        java.awt.Image image = ImageCache.getImage(((Object) (com.m7.B.C.A.B())), "app32.gif", com.m7.B.C.A.B());
        E = new ImageIcon(image);
    }
}
