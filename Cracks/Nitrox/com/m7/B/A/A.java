// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package com.m7.B.A;

import java.util.EventObject;
import javax.swing.CellEditor;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;

class A
    implements CellEditor
{

    protected EventListenerList A;

    A()
    {
        A = new EventListenerList();
    }

    public Object getCellEditorValue()
    {
        return ((Object) (null));
    }

    public boolean isCellEditable(EventObject eventobject)
    {
        return true;
    }

    public boolean shouldSelectCell(EventObject eventobject)
    {
        return false;
    }

    public boolean stopCellEditing()
    {
        return true;
    }

    public void cancelCellEditing()
    {
    }

    public void addCellEditorListener(CellEditorListener celleditorlistener)
    {
        A.add(javax.swing.event.CellEditorListener.class, ((java.util.EventListener) (celleditorlistener)));
    }

    public void removeCellEditorListener(CellEditorListener celleditorlistener)
    {
        A.remove(javax.swing.event.CellEditorListener.class, ((java.util.EventListener) (celleditorlistener)));
    }

    protected void B()
    {
        Object aobj[] = A.getListenerList();
        for(int i = aobj.length - 2; i >= 0; i -= 2)
            if(aobj[i] == (javax.swing.event.CellEditorListener.class))
                ((CellEditorListener)aobj[i + 1]).editingStopped(new ChangeEvent(((Object) (this))));

    }

    protected void A()
    {
        Object aobj[] = A.getListenerList();
        for(int i = aobj.length - 2; i >= 0; i -= 2)
            if(aobj[i] == (javax.swing.event.CellEditorListener.class))
                ((CellEditorListener)aobj[i + 1]).editingCanceled(new ChangeEvent(((Object) (this))));

    }
}
