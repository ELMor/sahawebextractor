/*
 * Created on 14-jul-2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package the.vault.crack;

/**
 * @author UF371231
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MyEclipse {

	public static String strDecompilingEtc = "Decompiling this copyrighted software is a violation of both your license agreement and the Digital Millenium Copyright Act of 1998 (http://www.loc.gov/copyright/legislation/dmca.pdf). Under section 1204 of the DMCA, penalties range up to a $500,000 fine or up to five years imprisonment for a first offense. Think about it; pay for a license, avoid prosecution, and feel better about yourself.";

	public static void main(String[] args) {
		String licensee=args.length>0?args[0]:"The Vault Company";
		String key="";
		key+=licensee.substring(0,1); //Primera letra de subscriber
		key+="Y"; //Licencia de Produccion
		key+="E2MY-"; //Product ID
		key+="2"; //Version Major
		key+="11";//Version Minor
		key+="000-";//Numero de Licencias
		key+="991231-"; //End Date
		key+=CRC31(key+strDecompilingEtc+licensee);
		//System.out.println(key);
		key=modularEnmedio(key);
		System.out.println(licensee);
		System.out.println(key);
	}

	public static  int CRC31(String s)
	{
		int i = 0;
		char ac[] = s.toCharArray();
		int j = 0;
		for(int k = ac.length; j < k; j++)
			i = 31 * i + ac[j];

		return Math.abs(i);
	}

	static String modularEnmedio(String s)
	{
		if(s == null || s.length() == 0)
			return s;
		byte abyte0[] = s.getBytes();
		char ac[] = new char[s.length()];
		int i = 0;
		boolean flag = false;
		for(int k = abyte0.length; i < k; i++)
		{
			int j = abyte0[i];
			if(j >= 48 && j <= 57)
				j = ((j - 48) + 5) % 10 + 48;
			else
			if(j >= 65 && j <= 90)
				j = ((j - 65) + 13) % 26 + 65;
			else
			if(j >= 97 && j <= 122)
				j = ((j - 97) + 13) % 26 + 97;
			ac[i] = (char)j;
		}

		return String.valueOf(ac);
	}


}
