
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.zip.CRC32;

public class DecoderEncoder {

	private int internalSeed[];

	private byte seed[];

	private int B;

	public static long calcCRC32(String s) {
		ByteArrayOutputStream baos = null;
		ObjectOutputStream oos = null;
		long retVal = 0;
		try {
			CRC32 crc32 = new CRC32();
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeUTF(s);
			oos.flush();
			crc32.update(baos.toByteArray());
			retVal = crc32.getValue();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (baos != null)
				try {
					baos.close();
				} catch (Throwable throwable) {
				}
			if (oos != null)
				try {
					oos.close();
				} catch (Throwable throwable1) {
				}
		}
		return retVal;
	}

	protected DecoderEncoder(byte abyte0[]) {
		int seedLength = abyte0.length;
		internalSeed = new int[4];
		if (seedLength != 16)
			throw new ArrayIndexOutOfBoundsException(
					"License Key is not 16 bytes: " + seedLength);
		int k = 0;
		for (int j = 0; j < seedLength;) {
			internalSeed[k] = abyte0[j] << 24 | (abyte0[j + 1] & 0xff) << 16
					| (abyte0[j + 2] & 0xff) << 8 | abyte0[j + 3] & 0xff;
			j += 4;
			k++;
		}

		seed = abyte0;
	}

	protected DecoderEncoder(int ai[]) {
		internalSeed = ai;
	}

	public String decode(String encodedString) {
		byte abyte0[] = decodePass1(encodedString);
		return (new String(abyte0)).trim();
	}

	public String undecode(String undecodedString) {
		while (undecodedString.length() % 8 != 0)
			undecodedString += " ";
		byte b[] = undecodedString.getBytes();
		String k = undecodePass1(b);
		return k.trim();
	}

	public int[] decodePass4(int encriptedChunk[]) {
		int int1 = encriptedChunk[0];
		int int2 = encriptedChunk[1];
		int seed1 = 0xc6ef3720;
		int seed2 = 0x9e3779b9;
		int internalSeed1 = internalSeed[0];
		int internalSeed2 = internalSeed[1];
		int internalSeed3 = internalSeed[2];
		int internalSeed4 = internalSeed[3];
		for (int i2 = 32; i2-- > 0;) {
			int2 -= (int1 << 4) + internalSeed3 ^ int1 + seed1 ^ (int1 >>> 5)
					+ internalSeed4;
			int1 -= (int2 << 4) + internalSeed1 ^ int2 + seed1 ^ (int2 >>> 5)
					+ internalSeed2;
			seed1 -= seed2;
		}

		encriptedChunk[0] = int1;
		encriptedChunk[1] = int2;
		return encriptedChunk;
	}

	public int[] undecodePass4(int encriptedChunk[]) {
		int n1 = encriptedChunk[0];
		int n2 = encriptedChunk[1];
		int seed1 = 0;
		int seed2 = 0x9e3779b9;
		int internalSeed1 = internalSeed[0];
		int internalSeed2 = internalSeed[1];
		int internalSeed3 = internalSeed[2];
		int internalSeed4 = internalSeed[3];

		for (int i2 = 32; i2-- > 0;) {
			seed1 += seed2;
			n1 += (n2 << 4) + internalSeed1 ^ n2 + seed1 ^ (n2 >>> 5)
					+ internalSeed2;
			n2 += (n1 << 4) + internalSeed3 ^ n1 + seed1 ^ (n1 >>> 5)
					+ internalSeed4;
		}

		encriptedChunk[0] = n1;
		encriptedChunk[1] = n2;
		return encriptedChunk;
	}

	private int A() {
		return B;
	}

	public String undecodePass1(byte inp[]) {
		byte undec[] = undecodePass2(inp);
		StringBuffer retVal = new StringBuffer();
		for (int k = 0; k < undec.length; k++) {
			retVal.append(getHex(undec[k]));
		}
		return retVal.toString();
	}

	private String getHex(byte c) {
		byte c1 = (byte) (c >> 4);
		byte c2 = (byte) (c & 0x0f);
		return getHexDigit(c1) + getHexDigit(c2);
	}

	private String getHexDigit(int c) {
		if (c < 0)
			c += 16;
		switch (c) {
		case 0:
			return "0";
		case 1:
			return "1";
		case 2:
			return "2";
		case 3:
			return "3";
		case 4:
			return "4";
		case 5:
			return "5";
		case 6:
			return "6";
		case 7:
			return "7";
		case 8:
			return "8";
		case 9:
			return "9";
		case 10:
			return "A";
		case 11:
			return "B";
		case 12:
			return "C";
		case 13:
			return "D";
		case 14:
			return "E";
		case 15:
			return "F";
		}
		return null;
	}

	public byte[] decodePass1(String encodedString) {
		if (encodedString.length() == 0)
			throw new InternalError("Cannot decode empty stringd");
		char charArrayEncoded[] = encodedString.toCharArray();
		int i = charArrayEncoded.length;
		if (i % 2 == 1)
			throw new InternalError("Cannot decode empty stringd");
		byte base16[] = new byte[encodedString.length() / 2];
		int j = 0;
		for (int k = 0; k < i; k++) {
			char c = charArrayEncoded[k];
			if ((c < '0' || c > '9') && (c < 'A' || c > 'F'))
				throw new InternalError("invalid key");
			byte byte0;
			if (c >= '0' && c <= '9') {
				byte0 = (byte) (c - 48);
			} else {
				byte0 = (byte) (c - 65);
				byte0 += 10;
			}
			k++;
			c = charArrayEncoded[k];
			if ((c < '0' || c > '9') && (c < 'A' || c > 'F'))
				throw new InternalError("invalid key");
			byte byte1;
			if (c >= '0' && c <= '9') {
				byte1 = (byte) (c - 48);
			} else {
				byte1 = (byte) (c - 65);
				byte1 += 10;
			}
			byte0 <<= 4;
			byte byte2 = (byte) (byte0 + byte1);
			base16[j++] = byte2;
		}

		return decodePass2(base16, base16.length);
	}

	public byte[] decodePass2(byte base16[], int longitud) {
		int l = longitud / 4;
		int toIntegers[] = new int[l];
		int j = 0;
		for (int k = 0; j < l; k += 8) {
			toIntegers[j] = base16[k] << 24 | (base16[k + 1] & 0xff) << 16
					| (base16[k + 2] & 0xff) << 8 | base16[k + 3] & 0xff;
			toIntegers[j + 1] = base16[k + 4] << 24
					| (base16[k + 5] & 0xff) << 16
					| (base16[k + 6] & 0xff) << 8 | base16[k + 7] & 0xff;
			j += 2;
		}

		return decodePass3(toIntegers);
	}

	public byte[] undecodePass2(byte inp[]) {
		//Retornar byte64
		int uints[] = undecodePass3(inp);
		byte retVal[] = new byte[inp.length];
		for (int i = 0; i < uints.length; i++) {
			retVal[4 * i + 0] = (byte) ((uints[i] >>> 24) & 0xFF);
			retVal[4 * i + 1] = (byte) ((uints[i] >>> 16) & 0xFF);
			retVal[4 * i + 2] = (byte) ((uints[i] >>> 8) & 0xFF);
			retVal[4 * i + 3] = (byte) ((uints[i]) & 0xFF);
		}
		return retVal;
	}

	public byte[] decodePass3(int fromIntegers[]) {
		int inputLength = fromIntegers.length;
		byte retValue[] = new byte[inputLength * 4];
		int encriptedChunk[] = new int[2];
		int k = 0;
		for (int j = 0; j < inputLength;) {
			encriptedChunk[0] = fromIntegers[j];
			encriptedChunk[1] = fromIntegers[j + 1];
			decodePass4(encriptedChunk);
			retValue[k + 0] = (byte) (encriptedChunk[0] >>> 24);
			retValue[k + 1] = (byte) (encriptedChunk[0] >>> 16);
			retValue[k + 2] = (byte) (encriptedChunk[0] >>> 8);
			retValue[k + 3] = (byte) encriptedChunk[0];
			retValue[k + 4] = (byte) (encriptedChunk[1] >>> 24);
			retValue[k + 5] = (byte) (encriptedChunk[1] >>> 16);
			retValue[k + 6] = (byte) (encriptedChunk[1] >>> 8);
			retValue[k + 7] = (byte) encriptedChunk[1];
			j += 2;
			k += 8;
		}

		return retValue;
	}

	public int[] undecodePass3(byte[] inp) {
		int inpLength = inp.length;
		int retValue[] = new int[inpLength / 4];
		int chunk[] = new int[2];
		int powers[] = new int[] { 24, 16, 8, 0 };
		for (int retValueIndex = 0; retValueIndex < retValue.length; retValueIndex += 2) {
			for (int cNu = 0; cNu < 2; cNu++) {
				chunk[cNu] = 0;
				for (int charNu = 0; charNu < 4; charNu++) {
					chunk[cNu] += complement(inp[retValueIndex * 4 + cNu * 4
							+ charNu], powers[charNu]);
				}
			}
			chunk = undecodePass4(chunk);
			retValue[retValueIndex + 0] = chunk[0];
			retValue[retValueIndex + 1] = chunk[1];
		}
		return retValue;
	}

	private int complement(byte in, int off) {
		if (in >= 0)
			return in << off;
		int compl = 2 << (off + 7);
		return (in << off) + compl;
	}

	private String B(int ai[]) throws ArrayIndexOutOfBoundsException {
		if (ai.length % 2 == 1)
			throw new ArrayIndexOutOfBoundsException(
					"Odd number of ints found: " + ai.length);
		StringBuffer stringbuffer = new StringBuffer();
		byte abyte0[] = new byte[8];
		int ai1[] = new int[2];
		int i = ai.length / 2;
		for (int j = 0; j < ai.length; j += 2) {
			abyte0[0] = (byte) (ai[j] >>> 24);
			abyte0[1] = (byte) (ai[j] >>> 16);
			abyte0[2] = (byte) (ai[j] >>> 8);
			abyte0[3] = (byte) ai[j];
			abyte0[4] = (byte) (ai[j + 1] >>> 24);
			abyte0[5] = (byte) (ai[j + 1] >>> 16);
			abyte0[6] = (byte) (ai[j + 1] >>> 8);
			abyte0[7] = (byte) ai[j + 1];
			stringbuffer.append(A(abyte0));
		}

		return stringbuffer.toString();
	}

	private String A(byte abyte0[]) {
		StringBuffer stringbuffer = new StringBuffer();
		char ac[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
				'B', 'C', 'D', 'E', 'F' };
		for (int i = 0; i < abyte0.length; i++) {
			int j = abyte0[i] >>> 4 & 0xf;
			stringbuffer.append(ac[j]);
			j = abyte0[i] & 0xf;
			stringbuffer.append(ac[j]);
		}

		return stringbuffer.toString();
	}

	private String A(String s, char c) {
		StringBuffer stringbuffer = new StringBuffer(s);
		int i = stringbuffer.length() % 8;
		for (int j = 0; j < i; j++)
			stringbuffer.append(c);

		return stringbuffer.toString();
	}

	private String B(String s) {
		return A(s, ' ');
	}

	private static String serialData[][] = {
			{ "0002", "01", "00", "ddd,vvv,aaa" },
			{ "0001", "01", "00", "ddd,vvv,aaa" }, 
			{ "0000", "01", "00", "" } };

	public static String[] checkSerial(String serial) {
		String serialLP=extractSerialLastPart(serial);
		for (int i = 0; i < serialData.length; i++)
			if (checkSerialPass1(serialLP, serialData[i][0]))
				return serialData[i];

		return null;
	}

	public static boolean checkSerialPass1(Object serial, Object serialDataRow) {
		return checkSerialPass2(serial, serialDataRow, false);
	}

	public static boolean checkSerialPass2(Object serial, Object serialDataRow,
			boolean flag) {
		boolean ret;
		if (serial == null || serialDataRow == null)
			ret = serial == serialDataRow;
		else if (flag && (serial instanceof String)
				&& (serialDataRow instanceof String))
			ret = ((String) serial).equalsIgnoreCase((String) serialDataRow);
		else
			ret = serial.equals(serialDataRow);
		return ret;
	}

    private static String extractSerialLastPart(String serial)
    {
        String s = serial;
        if(s != null && s.length() > 4)
            return s.substring(s.length() - 4);
        else
            return "XXXX";
    }
	
}