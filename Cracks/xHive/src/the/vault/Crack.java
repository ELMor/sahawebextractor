package the.vault;

import java.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class Crack {
  public static String Crack(String cName) {
    String company = (cName == null) ? "The Hellraiser Company" : cName;
    long tenYears = (new Date()).getTime() + 10 * 12 * 30 * 24 * 60 * 60 * 1000;
    String lit = "#" + tenYears + "#C" + company + "/V4.0";
    //Primero hacemos el Hash
    lit = lit.replace(' ', '.');
    String litr = (new StringBuffer(lit)).reverse().toString();
    String lith = Check.translateChars(litr, true);
    String lon1 = "0" + lit.length();
    String lithr = (new StringBuffer(lith)).reverse().toString();
    String cryptText = Crypt.crypt(lithr, lithr.substring(0, 2));
    String licenseKey = lon1 + lith + cryptText;
    return licenseKey;
  }

}