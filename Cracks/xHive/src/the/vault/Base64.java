/* Base64 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package the.vault;

import java.io.*;

public final class Base64 {
  private static final int BASELENGTH = 255;
  private static final int LOOKUPLENGTH = 64;
  private static final int TWENTYFOURBITGROUP = 24;
  private static final int EIGHTBIT = 8;
  private static final int SIXTEENBIT = 16;
  private static final int SIXBIT = 6;
  private static final int FOURBYTE = 4;
  private static final int SIGN = -128;
  private static final byte PAD = 61;
  private static final boolean fDebug = false;
  private static byte[] base64Alphabet = new byte[255];
  private static byte[] lookUpBase64Alphabet = new byte[64];

  protected static boolean isWhiteSpace(byte octect) {
    return octect == 32 || octect == 13 || octect == 10 || octect == 9;
  }

  protected static boolean isPad(byte octect) {
    return octect == 61;
  }

  protected static boolean isData(byte octect) {
    return base64Alphabet[octect] != -1;
  }

  protected static boolean isBase64(byte octect) {
    return isWhiteSpace(octect) || isPad(octect) || isData(octect);
  }

  public static byte[] encode(byte[] binaryData) {
    if (binaryData == null) {
      return null;
    }
    int lengthDataBits = binaryData.length * 8;
    int fewerThan24bits = lengthDataBits % 24;
    int numberTriplets = lengthDataBits / 24;
    byte[] encodedData = null;
    if (fewerThan24bits != 0) {
      encodedData = new byte[ (numberTriplets + 1) * 4];
    }
    else {
      encodedData = new byte[numberTriplets * 4];
    }
    byte k = 0;
    byte l = 0;
    byte b1 = 0;
    byte b2 = 0;
    byte b3 = 0;
    int encodedIndex = 0;
    int dataIndex = 0;
    int i = 0;
    for (i = 0; i < numberTriplets; i++) {
      dataIndex = i * 3;
      b1 = binaryData[dataIndex];
      b2 = binaryData[dataIndex + 1];
      b3 = binaryData[dataIndex + 2];
      l = (byte) (b2 & 0xf);
      k = (byte) (b1 & 0x3);
      encodedIndex = i * 4;
      byte val1 = ( (b1 & ~0x7f) == 0 ? (byte) (b1 >> 2)
                   : (byte) (b1 >> 2 ^ 0xc0));
      byte val2 = ( (b2 & ~0x7f) == 0 ? (byte) (b2 >> 4)
                   : (byte) (b2 >> 4 ^ 0xf0));
      byte val3 = ( (b3 & ~0x7f) == 0 ? (byte) (b3 >> 6)
                   : (byte) (b3 >> 6 ^ 0xfc));
      encodedData[encodedIndex] = lookUpBase64Alphabet[val1];
      encodedData[encodedIndex + 1]
          = lookUpBase64Alphabet[val2 | k << 4];
      encodedData[encodedIndex + 2]
          = lookUpBase64Alphabet[l << 2 | val3];
      encodedData[encodedIndex + 3] = lookUpBase64Alphabet[b3 & 0x3f];
    }
    dataIndex = i * 3;
    encodedIndex = i * 4;
    if (fewerThan24bits == 8) {
      b1 = binaryData[dataIndex];
      k = (byte) (b1 & 0x3);
      byte val1 = ( (b1 & ~0x7f) == 0 ? (byte) (b1 >> 2)
                   : (byte) (b1 >> 2 ^ 0xc0));
      encodedData[encodedIndex] = lookUpBase64Alphabet[val1];
      encodedData[encodedIndex + 1] = lookUpBase64Alphabet[k << 4];
      encodedData[encodedIndex + 2] = (byte) 61;
      encodedData[encodedIndex + 3] = (byte) 61;
    }
    else if (fewerThan24bits == 16) {
      b1 = binaryData[dataIndex];
      b2 = binaryData[dataIndex + 1];
      l = (byte) (b2 & 0xf);
      k = (byte) (b1 & 0x3);
      byte val1 = ( (b1 & ~0x7f) == 0 ? (byte) (b1 >> 2)
                   : (byte) (b1 >> 2 ^ 0xc0));
      byte val2 = ( (b2 & ~0x7f) == 0 ? (byte) (b2 >> 4)
                   : (byte) (b2 >> 4 ^ 0xf0));
      encodedData[encodedIndex] = lookUpBase64Alphabet[val1];
      encodedData[encodedIndex + 1]
          = lookUpBase64Alphabet[val2 | k << 4];
      encodedData[encodedIndex + 2] = lookUpBase64Alphabet[l << 2];
      encodedData[encodedIndex + 3] = (byte) 61;
    }
    return encodedData;
  }

  public static byte[] decode(byte[] base64Data) {
    if (base64Data == null) {
      return null;
    }
    if (base64Data.length % 4 != 0) {
      return null;
    }
    int numberQuadruple = base64Data.length / 4;
    if (numberQuadruple == 0) {
      return new byte[0];
    }
    byte[] decodedData = null;
    byte b1 = 0;
    byte b2 = 0;
    byte b3 = 0;
    byte b4 = 0;
    byte marker0 = 0;
    byte marker1 = 0;
    byte d1 = 0;
    byte d2 = 0;
    byte d3 = 0;
    byte d4 = 0;
    int i = 0;
    int encodedIndex = 0;
    int dataIndex = 0;
    decodedData = new byte[numberQuadruple * 3];
    for ( /**/; i < numberQuadruple - 1; i++) {
      if (!isData(d1 = base64Data[dataIndex++])
          || !isData(d2 = base64Data[dataIndex++])
          || !isData(d3 = base64Data[dataIndex++])
          || !isData(d4 = base64Data[dataIndex++])) {
        return null;
      }
      b1 = base64Alphabet[d1];
      b2 = base64Alphabet[d2];
      b3 = base64Alphabet[d3];
      b4 = base64Alphabet[d4];
      decodedData[encodedIndex++] = (byte) (b1 << 2 | b2 >> 4);
      decodedData[encodedIndex++]
          = (byte) ( (b2 & 0xf) << 4 | b3 >> 2 & 0xf);
      decodedData[encodedIndex++] = (byte) (b3 << 6 | b4);
    }
    if (!isData(d1 = base64Data[dataIndex++])
        || !isData(d2 = base64Data[dataIndex++])) {
      return null;
    }
    b1 = base64Alphabet[d1];
    b2 = base64Alphabet[d2];
    d3 = base64Data[dataIndex++];
    d4 = base64Data[dataIndex++];
    if (!isData(d3) || !isData(d4)) {
      if (isPad(d3) && isPad(d4)) {
        if ( (b2 & 0xf) != 0) {
          return null;
        }
        byte[] tmp = new byte[i * 3 + 1];
        System.arraycopy(decodedData, 0, tmp, 0, i * 3);
        tmp[encodedIndex] = (byte) (b1 << 2 | b2 >> 4);
        return tmp;
      }
      if (!isPad(d3) && isPad(d4)) {
        b3 = base64Alphabet[d3];
        if ( (b3 & 0x3) != 0) {
          return null;
        }
        byte[] tmp = new byte[i * 3 + 2];
        System.arraycopy(decodedData, 0, tmp, 0, i * 3);
        tmp[encodedIndex++] = (byte) (b1 << 2 | b2 >> 4);
        tmp[encodedIndex] = (byte) ( (b2 & 0xf) << 4 | b3 >> 2 & 0xf);
        return tmp;
      }
      return null;
    }
    b3 = base64Alphabet[d3];
    b4 = base64Alphabet[d4];
    decodedData[encodedIndex++] = (byte) (b1 << 2 | b2 >> 4);
    decodedData[encodedIndex++] = (byte) ( (b2 & 0xf) << 4 | b3 >> 2 & 0xf);
    decodedData[encodedIndex++] = (byte) (b3 << 6 | b4);
    return decodedData;
  }

  public static String decode(String base64Data) {
    if (base64Data == null) {
      return null;
    }
    byte[] decoded;
    try {
      decoded = decode(base64Data.getBytes("UTF-8"));
    }
    catch (UnsupportedEncodingException e) {
      decoded = null;
    }
    return decoded == null ? null : new String(decoded);
  }

  public static String encode(String unencodedData) {
    if (unencodedData == null) {
      return null;
    }
    byte[] encoded = encode(unencodedData.getBytes());
    String encodedStr = null;
    try {
      encodedStr = encoded == null ? null : new String(encoded, "UTF-8");
    }
    catch (UnsupportedEncodingException e) {
      encodedStr = null;
    }
    return encodedStr;
  }

  static {
    for (int i = 0; i < 255; i++) {
      base64Alphabet[i] = (byte) - 1;
    }
    for (int i = 90; i >= 65; i--) {
      base64Alphabet[i] = (byte) (i - 65);
    }
    for (int i = 122; i >= 97; i--) {
      base64Alphabet[i] = (byte) (i - 97 + 26);
    }
    for (int i = 57; i >= 48; i--) {
      base64Alphabet[i] = (byte) (i - 48 + 52);
    }
    base64Alphabet[43] = (byte) 62;
    base64Alphabet[47] = (byte) 63;
    for (int i = 0; i <= 25; i++) {
      lookUpBase64Alphabet[i] = (byte) (65 + i);
    }
    int i = 26;
    int j = 0;
    while (i <= 51) {
      lookUpBase64Alphabet[i] = (byte) (97 + j);
      i++;
      j++;
    }
    int i_0_ = 52;
    int j_1_ = 0;
    while (i_0_ <= 61) {
      lookUpBase64Alphabet[i_0_] = (byte) (48 + j_1_);
      i_0_++;
      j_1_++;
    }
    lookUpBase64Alphabet[62] = (byte) 43;
    lookUpBase64Alphabet[63] = (byte) 47;
  }
}
