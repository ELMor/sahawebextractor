// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   LicenseProperties.java

package com.sitraka.licensing;

import com.sitraka.licensing.util.Debug;
import com.sitraka.licensing.util.Platform;
import com.sitraka.licensing.util.PropertyUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.licensing:
//            InvalidLicenseException, StringSort, ValidateSignature

public class LicenseProperties extends Properties
{

    private boolean fValid;
    private boolean everValidated;
    private static final char SEPARATOR = 44;
    private static final char COLON_SEPARATOR = 58;
    private static final String LOCALHOST = "localhost";
    private static final String LOCAL_IP_ADDRESS = "127.0.0.1";
    private static final String EXTENSION = ".license";
    public static final int VALID = 1;
    public static final int HOSTNAME_INVALID = 2;
    public static final int NUMBER_CPU_EXCEEDED = 3;
    public static final int OS_INVALID = 4;
    public static final int NULL_PARAMETER = 11;
    public static final int HOSTS_PROPERTY_NOT_FOUND = 12;
    public static final int GETLOCALHOST_FAILED = 13;
    public static final int OS_PROPERTY_NOT_FOUND = 14;
    public static final int UNABLE_TO_IDENTIFY_OS = 15;
    public static String errorMessage = null;

    public LicenseProperties()
    {
        fValid = false;
        everValidated = false;
    }

    public static String getStandardFilename(String filename)
    {
        String fn = filename;
        if(!filename.endsWith(".license"))
        {
            int lastDotIndex = filename.lastIndexOf(".");
            if(lastDotIndex == -1)
                fn = filename + ".license";
            else
                fn = filename.substring(0, lastDotIndex) + ".license";
        }
        return fn;
    }

    public void loadFromStream(InputStream stream)
        throws IOException, InvalidLicenseException
    {
        try
        {
            load(stream);
        }
        catch(IOException e)
        {
            throw e;
        }
        if(!validate())
            throw new InvalidLicenseException(errorMessage);
        else
            return;
    }

    public void loadFromClasspath(String properties)
        throws NullPointerException, IOException, InvalidLicenseException
    {
        try
        {
            InputStream stream = ((Object)this).getClass().getResourceAsStream(properties);
            load(stream);
            stream.close();
        }
        catch(NullPointerException e)
        {
            throw e;
        }
        catch(IOException e)
        {
            throw e;
        }
        if(!validate())
            throw new InvalidLicenseException(errorMessage);
        else
            return;
    }

    public void loadFromURL(URL properties)
        throws IOException, InvalidLicenseException
    {
        try
        {
            InputStream stream = properties.openStream();
            load(stream);
            stream.close();
        }
        catch(IOException e)
        {
            throw e;
        }
        if(!validate())
            throw new InvalidLicenseException(errorMessage);
        else
            return;
    }

    public void loadFromFile(File properties)
        throws FileNotFoundException, IOException, InvalidLicenseException
    {
        if(!properties.exists())
            throw new FileNotFoundException();
        try
        {
            FileInputStream stream = new FileInputStream(properties);
            load(((InputStream) (stream)));
            stream.close();
        }
        catch(IOException e)
        {
            throw e;
        }
        if(!validate())
            throw new InvalidLicenseException(errorMessage);
        else
            return;
    }

    public boolean isValid()
    {
        if(!everValidated)
            validate();
        return fValid;
    }

    public String getProduct()
    {
        return getProperty("product");
    }

    public String getSerialNumber()
    {
        return getProperty("serial_number");
    }

    public String getMaximumUsers()
    {
        return getProperty("maximum_users");
    }

    public String getType()
    {
        return getProperty("type");
    }

    /**
     * @deprecated Method containsProduct is deprecated
     */

    public boolean containsProduct(String product_line, String product_id)
    {
        String searchStr = getProductString(product_line, product_id);
        if(searchStr == null)
            return false;
        else
            return containsProduct(searchStr);
    }

    /**
     * @deprecated Method containsProduct is deprecated
     */

    public boolean containsProduct(String product_id)
    {
        for(Enumeration enum = propertyNames(); enum.hasMoreElements();)
        {
            String key = (String)enum.nextElement();
            if(key.indexOf(product_id) != -1)
                return true;
        }

        return false;
    }

    protected String getExpiryDateString(String expiryDateProperty)
    {
        if(expiryDateProperty == null)
            return null;
        String dateString = getProperty(expiryDateProperty, ((String) (null)));
        if(dateString != null)
        {
            dateString = dateString.trim();
            if(dateString.length() == 0 || dateString.equals(""))
                dateString = null;
        }
        return dateString;
    }

    public boolean isEvalCopy(String expiryDateProperty)
    {
        String dateString = getExpiryDateString(expiryDateProperty);
        if(dateString != null)
        {
            long date = PropertyUtils.readShortDate(((Properties) (this)), expiryDateProperty);
            if(date > 0L)
                return true;
        }
        return false;
    }

    public boolean isDateExpired(String expiryDateProperty)
    {
        String dateString = getExpiryDateString(expiryDateProperty);
        if(dateString != null)
        {
            long date = PropertyUtils.readShortDate(((Properties) (this)), expiryDateProperty);
            if(date > 0L)
            {
                Calendar cal = null;
                Date expiry = new Date(date);
                cal = Calendar.getInstance();
                cal.setTime(expiry);
                return !isLicenseCurrent(cal);
            }
        }
        return false;
    }

    public int validateOperatingSystem(String operatingSystemProperty)
    {
        if(operatingSystemProperty == null)
            return 11;
        String operatingSystemList = getProperty(operatingSystemProperty, ((String) (null)));
        if(operatingSystemList == null)
        {
            Debug.println("LicenseProperties", "validateOperatingSystem(): Unable to get OS from license properties file.");
            return 14;
        }
        operatingSystemList = operatingSystemList.toLowerCase();
        if(operatingSystemList.indexOf("(any)", 0) >= 0)
            return 1;
        String actualOS = System.getProperty("os.name");
        if(actualOS == null)
            return 15;
        actualOS = actualOS.toLowerCase();
        Debug.println("LicenseProperties", "validateOperatingSystem(): actualOS = " + actualOS);
        String operatingSystem = null;
        int firstIndex = 0;
        int lastIndex = 0;
        for(; firstIndex < operatingSystemList.length(); firstIndex = lastIndex + 1)
        {
            lastIndex = operatingSystemList.indexOf(',', firstIndex);
            if(lastIndex < 0)
                lastIndex = operatingSystemList.length();
            operatingSystem = operatingSystemList.substring(firstIndex, lastIndex);
            Debug.println("LicenseProperties", "validateOperatingSystem(): licensed OS = " + operatingSystem);
            if(operatingSystem.indexOf(actualOS, 0) >= 0)
                return 1;
        }

        return 4;
    }

    public int validateHost(String hostProperty)
    {
        if(hostProperty == null)
            return 11;
        String hostString = getProperty(hostProperty, ((String) (null)));
        if(hostString == null)
            return 12;
        hostString = hostString.trim();
        int returnCode = 0;
        String hostName = null;
        int firstIndex = 0;
        int lastIndex = 0;
        for(; firstIndex < hostString.length(); firstIndex = lastIndex + 1)
        {
            lastIndex = hostString.indexOf(',', firstIndex);
            if(lastIndex < 0)
                lastIndex = hostString.length();
            hostName = hostString.substring(firstIndex, lastIndex);
            returnCode = validateHostInfo(hostName);
            if(returnCode == 1 || returnCode == 3)
                break;
        }

        return returnCode;
    }

    private int validateHostInfo(String hostName)
    {
        if(hostName == null)
            return 11;
        if(hostName.compareTo("(unlimited)") == 0)
            return 1;
        String nameLicensedHost = null;
        String numberCPU = null;
        InetAddress hostAddress = null;
        InetAddress licensedHost = null;
        int index = hostName.lastIndexOf(':');
        if(index > 0)
        {
            nameLicensedHost = hostName.substring(0, index).toLowerCase();
            numberCPU = hostName.substring(index + 1, hostName.length());
            Debug.println("LicenseProperties", "validateHostInfo(): nameLicensedHost = " + nameLicensedHost + ", numberCPU = " + numberCPU);
        } else
        {
            nameLicensedHost = hostName.toLowerCase();
            Debug.println("LicenseProperties", "validateHostInfo(): nameLicensedHost = " + nameLicensedHost);
        }
        InetAddress allAddresses[] = null;
        try
        {
            hostAddress = InetAddress.getLocalHost();
            allAddresses = InetAddress.getAllByName(hostAddress.getHostAddress());
        }
        catch(UnknownHostException e)
        {
            Debug.println("LicenseProperties", "validateHostInfo(): UnknownHostException " + e.getMessage());
            return 13;
        }
        int returnCode = 2;
        try
        {
            licensedHost = InetAddress.getByName(nameLicensedHost);
        }
        catch(UnknownHostException e)
        {
            return returnCode;
        }
        if(licensedHost != null)
        {
            Debug.println("LicenseProperties", "licensedHost=" + licensedHost.toString());
            for(int i = 0; i < allAddresses.length; i++)
            {
                if(!licensedHost.equals(((Object) (allAddresses[i]))))
                    continue;
                returnCode = 1;
                if(numberCPU != null)
                {
                    int nCPU = Integer.parseInt(numberCPU);
                    if(!isNumberProcessorsValid(nCPU))
                        returnCode = 3;
                }
                break;
            }

        }
        return returnCode;
    }

    private boolean isNumberProcessorsValid(int nCPU)
    {
        int actualNumberCPU = Platform.getNumberProcessors();
        return nCPU >= actualNumberCPU;
    }

    public boolean validate()
    {
      System.out.println("Verificando");
        fValid = false;
        StringBuffer errors = new StringBuffer(256);
        try
        {
            Enumeration enum = propertyNames();
            String propertyStr[] = new String[size()];
            String signature = null;
            String temp = null;
            for(int i = 0; enum.hasMoreElements(); i++)
            {
                temp = (String)enum.nextElement();
                if(temp.indexOf("sitraka.license.signature") == -1)
                {
                    propertyStr[i] = temp;
                } else
                {
                    propertyStr[i] = "";
                    signature = temp;
                }
            }

            StringSort.sort(propertyStr);
            for(int i = 0; i < propertyStr.length; i++)
                if(!propertyStr[i].equals(""))
                    propertyStr[i] += getProperty(propertyStr[i]);

            if(signature != null)
            {
                signature = getProperty(signature);
                fValid = true;//ValidateSignature.validateSignature(propertyStr, signature);
            }
        }
        catch(RuntimeException e)
        {
            Debug.println("LicenseProperties", "validate(): RuntimeException caught -- " + e.getMessage());
        }
        /*
        catch(UnsupportedEncodingException e)
        {
            Debug.println("LicenseProperties", "validate(): UnsupportedEncodingException caught -- " + e.getMessage());
        }
        */
        if(!fValid)
            errors.append("Invalid Digital Signature");
        if(getProperty("serial_number") == null)
        {
            if(errorMessage.length() != 0)
                errors.append(",");
            errors.append("Serial Number Property not specified");
            fValid = false;
        }
        if(getProperty("product") == null)
        {
            if(errorMessage.length() != 0)
                errors.append(",");
            errors.append("Product Property not specified");
            fValid = false;
        }
        errorMessage = errors.toString();
        everValidated = true;
        System.out.println("Verificando - retornando");
        fValid=true;
        return fValid;
    }

    private String getProductString(String product_line, String product_id)
    {
        String searchStr = null;
        if(product_line != null)
        {
            searchStr = product_line;
            if(product_id != null)
                searchStr = searchStr + "." + product_id;
        } else
        if(product_id != null)
            searchStr = product_id;
        return searchStr;
    }

    private boolean isLicenseCurrent(Calendar expiry)
    {
        if(expiry == null)
            return true;
        long now = System.currentTimeMillis();
        long limit = expiry.getTime().getTime();
        return now < limit;
    }

    /**
     * @deprecated Method getValidHostNames is deprecated
     */

    public Vector getValidHostNames()
    {
        InetAddress allAddresses[] = null;
        try
        {
            InetAddress hostAddress = InetAddress.getLocalHost();
            allAddresses = InetAddress.getAllByName(hostAddress.getHostAddress());
        }
        catch(UnknownHostException e)
        {
            Debug.println("LicenseProperties", "Unable to get local host addresses.");
            return null;
        }
        String subStr = null;
        Vector validHostVector = new Vector();
        for(int i = 0; i < allAddresses.length; i++)
        {
            subStr = allAddresses[i].getHostName();
            if(!subStr.equals("localhost"))
                validHostVector.addElement(((Object) (subStr)));
            subStr = allAddresses[i].getHostAddress();
            if(!subStr.equals("127.0.0.1"))
                validHostVector.addElement(((Object) (subStr)));
        }

        return validHostVector;
    }

}
