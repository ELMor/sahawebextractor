// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ValidateSignature.java

package com.sitraka.licensing;

import com.sitraka.licensing.util.Codecs;
import com.sitraka.licensing.util.Debug;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

// Referenced classes of package com.sitraka.licensing:
//            SitrakaPublicKey, SignableBlock

public class ValidateSignature
{

    public ValidateSignature()
    {
    }

    protected static boolean validate(byte data[], byte signature[])
    {
        if(Debug.isPrintableTag("ValidateSignature"))
            try
            {
                Debug.println("ValidateSignature", "validating from the data: '" + new String(data, "UTF-8") + "'");
            }
            catch(UnsupportedEncodingException e) { }
        Signature dsa = null;
        try
        {
            dsa = Signature.getInstance("SHA/DSA");
        }
        catch(NoSuchAlgorithmException e)
        {
            return false;
        }
        java.security.PublicKey key = ((java.security.PublicKey) (new SitrakaPublicKey()));
        try
        {
            dsa.initVerify(key);
        }
        catch(InvalidKeyException e)
        {
            return false;
        }
        try
        {
            dsa.update(data);
        }
        catch(SignatureException e)
        {
            return false;
        }
        boolean verifies = false;
        try
        {
            verifies = dsa.verify(signature);
        }
        catch(SignatureException e) { }
        return verifies;
    }

    public static boolean validateSignatureBytes(String properties[], byte signature[])
        throws UnsupportedEncodingException
    {
        byte data[] = SignableBlock.createSignableBlock(properties);
        return validate(data, signature);
    }

    public static boolean validateSignature(String properties[], String signature)
        throws UnsupportedEncodingException
    {
        byte byteArray[];
        try
        {
            byteArray = Codecs.base64Decode(signature.getBytes("UTF-8"));
        }
        catch(ArrayIndexOutOfBoundsException aiobe)
        {
            return false;
        }
        return validateSignatureBytes(properties, byteArray);
    }
}
