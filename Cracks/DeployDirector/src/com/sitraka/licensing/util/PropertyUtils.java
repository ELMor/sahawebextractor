// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PropertyUtils.java

package com.sitraka.licensing.util;

import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

// Referenced classes of package com.sitraka.licensing.util:
//            ParameterCheck

public class PropertyUtils
{

    protected static final String MODIFIER_NAMES[] = {
        "second", "seconds", "minute", "minutes", "hour", "hours", "day", "days"
    };
    protected static final long MODIFIER_VALUES[] = {
        1000L, 1000L, 60000L, 60000L, 0x36ee80L, 0x36ee80L, 0x5265c00L, 0x5265c00L
    };
    protected static final String LONG_DATE_FORMATS[] = {
        "yyyy/MM/dd h:mm:ss a z", "yyyy/MM/dd hh:mm:ss z", "yyyy/MM/dd hh:mm:ss", "yyyy/MM/dd hh:mm z", "yyyy/MM/dd hh:mm", "yyyy/MM/dd h:mm:ss a", "yyyy/MM/dd h:mm a z", "yyyy/MM/dd h:mm a", "yyyy.MM.dd h:mm:ss a z", "yyyy.MM.dd hh:mm:ss z", 
        "yyyy.MM.dd hh:mm:ss", "yyyy.MM.dd hh:mm z", "yyyy.MM.dd hh:mm", "yyyy.MM.dd h:mm:ss a", "yyyy.MM.dd h:mm a z", "yyyy.MM.dd h:mm a"
    };
    protected static final String SHORT_DATE_FORMATS[] = {
        "MMM dd, yyyy", "yyyy/MMM/dd", "yyyy.MM.dd", "yyyy/MM/dd"
    };
    protected static final String DATE_NOW = "now";
    protected static final String DATE_TODAY = "today";
    protected static final String DATE_TOMORROW = "tomorrow";
    protected static final String SPECIAL_DATES[] = {
        "now", "today", "tomorrow"
    };

    public PropertyUtils()
    {
    }

    protected static double readBaseInterval(String value)
    {
        int index = value.indexOf(' ');
        double result = 4.9406564584124654E-324D;
        String base = null;
        if(index != -1)
            base = value.substring(0, index);
        else
            base = value;
        try
        {
            result = Double.valueOf(base).doubleValue();
        }
        catch(NumberFormatException e)
        {
            result = 4.9406564584124654E-324D;
        }
        catch(Exception e)
        {
            System.err.print("\n--- DEBUG " + (com.sitraka.licensing.util.PropertyUtils.class).getName() + ": ");
            System.err.println("--- DEBUG parsing double '" + base + "'");
            e.printStackTrace();
            result = 4.9406564584124654E-324D;
        }
        return result;
    }

    protected static long readModifier(String value)
    {
        long result = 1L;
        int index = value.lastIndexOf(' ');
        if(index != -1)
        {
            String modifier = value.substring(index + 1);
            for(int i = 0; i < MODIFIER_NAMES.length; i++)
            {
                if(!modifier.equalsIgnoreCase(MODIFIER_NAMES[i]))
                    continue;
                result = MODIFIER_VALUES[i];
                break;
            }

        }
        return result;
    }

    public static long readInterval(Properties props, String prop_name)
    {
        long result = 0x8000000000000000L;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            double base = readBaseInterval(value);
            long modifier = readModifier(value);
            if(base == 4.9406564584124654E-324D)
            {
                result = 0x8000000000000000L;
            } else
            {
                Double temp = new Double(base * (double)modifier);
                result = temp.longValue();
            }
        }
        return result;
    }

    public static long readInterval(Properties props, String prop_name, long default_value)
    {
        long value = readInterval(props, prop_name);
        if(value == 0x8000000000000000L)
            value = default_value;
        return value;
    }

    protected static long checkForSpecialDate(String value)
    {
        long result = 0L;
        ParameterCheck.nonNull(((Object) (value)));
        if(value == null)
            return 0L;
        if(value.equalsIgnoreCase("now"))
            result = System.currentTimeMillis();
        else
        if(value.equalsIgnoreCase("today"))
        {
            Calendar cal = Calendar.getInstance();
            cal.set(10, 0);
            cal.set(12, 0);
            cal.set(13, 0);
            result = cal.getTime().getTime();
        } else
        if(value.equalsIgnoreCase("tomorrow"))
        {
            Calendar cal = Calendar.getInstance();
            cal.roll(5, true);
            result = cal.getTime().getTime();
        }
        return result;
    }

    public static String[] getLongDateFormats()
    {
        return LONG_DATE_FORMATS;
    }

    public static String[] getShortDateFormats()
    {
        return SHORT_DATE_FORMATS;
    }

    public static long parseLongDate(String value)
    {
        return parseDateInternal(value, LONG_DATE_FORMATS);
    }

    public static String writeLongDate(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(LONG_DATE_FORMATS[0]);
        return formatter.format(date);
    }

    public static String writeShortDate(Date date)
    {
        return writeShortDate(date, SHORT_DATE_FORMATS[0]);
    }

    public static String writeShortDate(Date date, String format)
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date);
    }

    public static long parseShortDate(String value)
    {
        return parseDateInternal(value, SHORT_DATE_FORMATS);
    }

    protected static long parseDateInternal(String value, String formats[])
    {
        long result = 0L;
        ParameterCheck.nonNull(((Object) (formats)));
        ParameterCheck.nonNull(((Object) (value)));
        if(value == null || formats == null)
            return result;
        for(int i = 0; i < formats.length; i++)
            try
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat(formats[i]);
                Date when = dateFormat.parse(value);
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(when);
                result = cal.getTime().getTime();
                break;
            }
            catch(ParseException pe) { }
            catch(Exception e)
            {
                System.err.println("\n--- DEBUG " + (com.sitraka.licensing.util.PropertyUtils.class).getName() + ": ");
                System.err.println("--- DEBUG parsing '" + value + "' against '" + formats[i] + "'");
                e.printStackTrace();
            }

        return result;
    }

    public static long readDate(Properties props, String prop_name)
    {
        long result = 0L;
        ParameterCheck.nonNull(((Object) (props)));
        ParameterCheck.nonNull(((Object) (prop_name)));
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            if((result = checkForSpecialDate(value)) != 0L)
                return result;
            result = parseLongDate(value);
        }
        return result;
    }

    public static long readShortDate(Properties props, String prop_name)
    {
        long result = 0L;
        ParameterCheck.nonNull(((Object) (props)));
        ParameterCheck.nonNull(((Object) (prop_name)));
        if(props == null || prop_name == null)
            return result;
        String value = props.getProperty(prop_name);
        if(value != null)
        {
            value = value.trim();
            if((result = checkForSpecialDate(value)) != 0L)
                return result;
            result = parseShortDate(value);
        }
        return result;
    }

    public static boolean parseLine(String line, String pair[])
    {
        boolean result = false;
        line.trim();
        int equals_location = line.indexOf('=');
        if(equals_location > 0)
        {
            result = true;
            pair[0] = line.substring(0, equals_location);
            pair[1] = null;
            if(equals_location == line.length() - 1)
                pair[1] = "";
            else
                pair[1] = removeEscapeChars(line.substring(equals_location + 1));
        }
        return result;
    }

    protected static String removeEscapeChars(String value)
    {
        int index = value.indexOf('\\');
        if(index == -1)
            return value;
        StringBuffer b;
        for(b = new StringBuffer(value); index < b.length(); index++)
            if(b.charAt(index) == '\\')
                b.deleteCharAt(index);

        return b.toString();
    }

    public static boolean checkLicenseVersion(String licenseVersion)
    {
        int dot = licenseVersion.indexOf('.');
        return dot > 0 && dot < licenseVersion.length() - 1 && onlyDigits(licenseVersion.substring(0, dot)) && onlyDigits(licenseVersion.substring(dot + 1));
    }

    public static boolean onlyDigits(String str)
    {
        for(int i = 0; i < str.length(); i++)
            if(!Character.isDigit(str.charAt(i)))
                return false;

        return true;
    }

    public static void main(String args[])
    {
        System.out.println("com.sitraka.util.PropertyUtils: Testing date parsing routines:");
        long d = parseLongDate("1979/07/13 11:11");
        if(d == -1L || d != 0x4604b4aba0L)
            System.out.println("\tParsing '1979/07/13 11:11' failed. Error!");
        else
            System.out.println("\tParsing '1979/07/13 11:11' succeeded as " + writeLongDate(new Date(d)) + " (" + d + ")");
        System.out.println("\n\tParsing '2000/03/15 2:50:12 PM EST'");
        d = parseLongDate("2000/03/15 2:50:12 PM EST");
        Date date = new Date(d);
        System.out.println("\tParsed to java.util.Date: " + date);
        System.out.println("\tConverted back to: " + writeLongDate(date));
        System.out.println("\n\tParsing '2000/03/15 14:50:12 EST'");
        d = parseLongDate("2000/03/15 14:50:12 EST");
        date = new Date(d);
        System.out.println("\tParsed to java.util.Date: " + date);
        System.out.println("\tConverted back to: " + writeLongDate(date));
    }

}
