// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Platform.java

package com.sitraka.licensing.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.StringTokenizer;

// Referenced classes of package com.sitraka.licensing.util:
//            Debug

public class Platform
{

    private static int numCPUS = -1;
    private static String hostName = null;
    private static final int WINDOWS_9X = 0;
    private static final int WINDOWS = 1;
    private static final int SOLARIS = 2;
    private static final int LINUX = 3;
    private static final int AIX = 4;
    private static final int HPUX = 5;
    private static final int MACOS = 6;
    private static final int OSX = 7;
    private static final int OS2 = 9;
    private static final int ZOS = 10;
    private static int platform = -1;
    private static final String CMD_ENV_WINNT[] = {
        "cmd.exe", "/c", "set"
    };
    private static final String CMD_ENV_WIN9X[] = {
        "command.com", "/c", "set"
    };
    private static final String CMD_CPU_SOLARIS[] = {
        "/usr/bin/uname", "-X"
    };
    private static final String CMD_CPU_AIX[] = {
        "/usr/sbin/lsdev", "-Cc", "processor"
    };
    private static final String CMD_CPU_HPUX[] = {
        "/bin/sh", "-c", "/usr/bin/sar -M 1 1 | awk 'END {print NR-5}'"
    };
    private static final String CMD_CPU_ZOS[] = {
        "license"
    };
    private static final String CMD_HOST_UNIX[] = {
        "/usr/bin/uname", "-n"
    };

    public Platform()
    {
    }

    public static int getNumberProcessors()
    {
        if(numCPUS != -1)
            return numCPUS;
        String data = null;
        switch(getPlatform())
        {
        case 0: // '\0'
            data = executeCommand(CMD_ENV_WIN9X);
            numCPUS = readPropertyInt(data, "NUMBER_OF_PROCESSORS", "=");
            break;

        case 1: // '\001'
            data = executeCommand(CMD_ENV_WINNT);
            numCPUS = readPropertyInt(data, "NUMBER_OF_PROCESSORS", "=");
            break;

        case 2: // '\002'
            data = executeCommand(CMD_CPU_SOLARIS);
            numCPUS = readPropertyInt(data, "NumCPU", "=");
            break;

        case 3: // '\003'
            data = readFile("/proc/cpuinfo");
            numCPUS = countLines(data, "processor");
            break;

        case 4: // '\004'
            data = executeCommand(CMD_CPU_AIX);
            numCPUS = countLines(data, "Available");
            break;

        case 5: // '\005'
            data = executeCommand(CMD_CPU_HPUX);
            try
            {
                numCPUS = Integer.parseInt(data.trim());
            }
            catch(NumberFormatException nfe)
            {
                Debug.println("NumberProcessors", "getNumCPUS()\n" + nfe.toString());
            }
            break;

        case 10: // '\n'
            data = executeCommand(CMD_CPU_ZOS);
            numCPUS = readPropertyInt(data, "cpus", "=");
            break;
        }
        if(numCPUS == 0)
            numCPUS = -1;
        Debug.println("NumberProcessors", "getNumCPUS(): determined that there are " + numCPUS);
        return numCPUS;
    }

    public static String getHostName()
    {
        if(hostName != null)
            return hostName;
        switch(getPlatform())
        {
        case 0: // '\0'
            hostName = readProperty(executeCommand(CMD_ENV_WIN9X), "COMPUTERNAME", "=");
            break;

        case 1: // '\001'
            hostName = readProperty(executeCommand(CMD_ENV_WINNT), "COMPUTERNAME", "=");
            break;

        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        case 7: // '\007'
            hostName = executeCommand(CMD_HOST_UNIX);
            break;
        }
        return hostName;
    }

    private static String readFile(String filename)
    {
        StringBuffer result = new StringBuffer();
        File file = new File(filename);
        if(file.isFile() && file.canRead())
            try
            {
                FileReader reader = new FileReader(file);
                char buffer[] = new char[1024];
                int i;
                while((i = reader.read(buffer, 0, 1023)) != -1) 
                    result.append(buffer, 0, i);
                reader.close();
            }
            catch(FileNotFoundException fnfe)
            {
                Debug.println("NumberProcessors", "readFile(" + filename + ")\n" + fnfe.toString());
            }
            catch(IOException ioe)
            {
                Debug.println("NumberProcessors", "readFile(" + filename + ")\n" + ioe.toString());
            }
        else
            Debug.println("NumberProcessors", "readFile(" + filename + ") cannot read named file");
        return result.toString();
    }

    private static String executeCommand(String commandLine[])
    {
        Process child = null;
        StringBuffer result = new StringBuffer();
        try
        {
            child = Runtime.getRuntime().exec(commandLine);
            InputStreamReader inputStream = new InputStreamReader(child.getInputStream());
            char buffer[] = new char[1024];
            int i;
            while((i = inputStream.read(buffer)) != -1) 
                result.append(buffer, 0, i);
        }
        catch(IOException ioe)
        {
            Debug.println("NumberProcessors", "executeCommand(): " + ioe.toString());
        }
        finally
        {
            if(child != null)
                child.destroy();
        }
        return result.toString();
    }

    private static int readPropertyInt(String data, String propName, String delim)
    {
        if(data == null || propName == null)
            return 0;
        String value = readProperty(data, propName, delim);
        int result = 0;
        if(value != null)
            try
            {
                result = Integer.parseInt(value);
            }
            catch(NumberFormatException nfe)
            {
                result = 0;
                Debug.println("NumberProcessors", "readPropertyInt(" + propName + ")\n" + nfe.toString());
            }
        return result;
    }

    private static String readProperty(String data, String propName, String delim)
    {
        for(StringTokenizer toker = new StringTokenizer(data, "\n"); toker.hasMoreTokens();)
        {
            String currentLine = toker.nextToken();
            int idx;
            if(currentLine != null && currentLine.indexOf(propName) != -1 && (idx = currentLine.indexOf(delim)) != -1)
                return currentLine.substring(idx + 1).trim();
        }

        return null;
    }

    private static int countLines(String data, String line)
    {
        if(data == null || line == null)
            return 0;
        int count = 0;
        String delim = "\n";
        for(StringTokenizer toker = new StringTokenizer(data, delim); toker.hasMoreTokens();)
        {
            String currentLine = toker.nextToken();
            if(currentLine != null && currentLine.indexOf(line) != -1)
                count++;
        }

        return count;
    }

    private static int getPlatform()
    {
        if(platform == -1)
        {
            String osname = System.getProperty("os.name").toLowerCase();
            String osver = System.getProperty("os.version").toLowerCase();
            if(osname.indexOf("mac os") >= 0)
                try
                {
                    int majorVersion = Integer.parseInt(osver.substring(0, osver.indexOf('.')));
                    if(majorVersion >= 10)
                        platform = 7;
                }
                catch(NumberFormatException e)
                {
                    platform = 6;
                }
            else
            if(osname.indexOf("os/2") >= 0)
                platform = 9;
            else
            if(osname.indexOf("indows") >= 0)
            {
                if(osname.indexOf("nt") >= 0 || osname.indexOf("2000") >= 0 || osname.indexOf("xp") >= 0)
                    platform = 1;
                else
                    platform = 0;
            } else
            if(osname.indexOf("linux") != -1)
                platform = 3;
            else
            if(osname.indexOf("aix") != -1)
                platform = 4;
            else
            if(osname.indexOf("sunos") != -1 || osname.indexOf("solaris") != -1)
                platform = 2;
            else
            if(osname.indexOf("hpux") != -1 || osname.indexOf("hp-ux") != -1)
                platform = 5;
            else
            if(osname.indexOf("z/os") != -1 || osname.indexOf("os/390") != -1)
            {
                platform = 10;
            } else
            {
                Debug.println("NumberProcessors", "getPlatform() unsupported OS " + osname);
                platform = -1;
            }
        }
        return platform;
    }

    public static void main(String args[])
    {
        System.out.println("Testing: " + (com.sitraka.licensing.util.Platform.class).getName());
        System.out.println("\tos.name: " + System.getProperty("os.name"));
        System.out.println("\tos.arch: " + System.getProperty("os.arch"));
        System.out.println("\tos.version: " + System.getProperty("os.version"));
        System.out.println("\tPlatform code: " + getPlatform());
        System.out.println("\tNumber of processors: " + getNumberProcessors());
        System.out.println("\tHost name: " + getHostName());
    }

}
