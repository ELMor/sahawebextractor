/* CacheList - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.sitraka.deploy.sam.cache;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import com.sitraka.deploy.sam.AbstractTrackerEntry;
import com.sitraka.deploy.sam.AbstractTrackerList;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.PropertyUtils;

public class CacheList extends AbstractTrackerList
{
    protected static CacheList instance = null;
    private static final long DEFAULT_CACHE_SIZE = 31457280L;
    private static final long DEFAULT_CACHE_AGE = 2592000000L;

    protected AbstractTrackerList getInstance() {
        return instance;
    }

    public static boolean initializeCache(String servlet_dir) {
        if (instance != null) {
            instance.loadCacheProperties(servlet_dir);
            saveAllData();
            instance.loadCachedData();
            instance.removeExpiredEntries();
            instance.trimCacheToSize();
            instance.removeUnknownFiles();
            return false;
        }
        instance = new CacheList(servlet_dir);
        return true;
    }

    public static boolean addRequest(RequestParser parser, File tempFile) {
        String request = instance.convertParserToString(parser);
        return addRequest(request, tempFile);
    }

    public static boolean addRequest(String request, File tempFile) {
        if (instance == null || !instance.isAvailable())
            return false;
        if (!tempFile.canRead())
            return false;
        if (tempFile.length() > instance.getMaxSize())
            return false;
        CacheObject c = new CacheObject(instance.cacheDir, tempFile, request);
        return addRequest(c);
    }

    public static boolean addRequest(AbstractTrackerEntry c) {
        if (instance == null || !instance.isAvailable())
            return false;
        if (c.getRequest() != null) {
            String req = c.getRequest().toLowerCase();
            String adminString = "app=" + "DDAdmin".toLowerCase();
            if (req.indexOf(adminString) != -1)
                throw new IllegalArgumentException
                          ("Requests involving the admin tool must not be cached");
            if (instance.findByRequest(req) != null)
                return true;
        }
        if (!c.isValid())
            return false;
        if (c.getFile().length() > instance.getMaxSize())
            return false;
        if (!c.moveFile(instance.cacheDir))
            return false;
        if (!instance.addEntry(c))
            return false;
        instance.saveCachedData();
        return true;
    }

    private CacheList(String servlet_dir) {
        if (instance == null)
            instance = this;
        loadCacheProperties(servlet_dir);
        if (cache_available) {
            loadCachedData();
            this.removeExpiredEntries();
            this.removeUnknownFiles();
            this.trimCacheToSize();
        }
    }

    protected void loadCacheProperties(String servlet_dir) {
        cacheDir
            = new File(FileUtils.unifyFileSeparator(servlet_dir)
                       + File.separator + "dd" + File.separator + "cache");
        cacheDir.mkdirs();
        max_cache_size
            = PropertyUtils.readLong(Servlet.properties,
                                     "deploy.server.cache.size", 31457280L);
        if (max_cache_size < 0L)
            max_cache_size = 0L;
        max_cache_age
            = PropertyUtils.readInterval(Servlet.properties,
                                         "deploy.server.cache.maxage",
                                         2592000000L);
        if (max_cache_size <= 0L || max_cache_age <= 0L)
            cache_available = false;
        else
            cache_available = true;
    }

    protected void loadCachedData() {
        synchronized (CACHE_LOCK) {
            cache = new Vector();
        }
        if (cache_available) {
            File prop_file = new File(cacheDir, "cache.properties");
            if (prop_file.canRead()) {
                Properties props = new Properties();
                try {
                    FileInputStream fis = new FileInputStream(prop_file);
                    props.load(fis);
                    fis.close();
                    Object object = null;
                } catch (FileNotFoundException e) {
                    /* empty */
                } catch (IOException ioexception) {
                    /* empty */
                }
                int i = 0;
                while (PropertyUtils.containsProperty(props, "file",
                                                      ".file")) {
                    String base = "file" + i + ".file";
                    if (props.containsKey(base)) {
                        CacheObject c = new CacheObject(cacheDir, props, i);
                        if (c.isValid() && getRequestByID(c.getID()) == null) {
                            synchronized (CACHE_LOCK) {
                                cache.addElement(c);
                                total_size += c.getSize();
                            }
                        }
                        i++;
                    }
                }
            }
        }
    }

    public boolean isAvailable() {
        if (instance == null)
            return false;
        return instance.cache_available;
    }

    public static int getNumFiles() {
        if (instance == null)
            return -1;
        return instance.numFiles();
    }

    public static long getCacheSize() {
        if (instance == null)
            return -1L;
        return instance.getSizeofCache();
    }

    public static AbstractTrackerEntry getRequest(RequestParser _request) {
        if (instance == null || !instance.isAvailable())
            return null;
        return instance.getCachedRequest(_request);
    }

    public static AbstractTrackerEntry getRequestByID(long id) {
        if (instance == null || !instance.isAvailable())
            return null;
        return instance.findByID(id);
    }

    public static void saveAllData() {
        if (instance != null && instance.isAvailable())
            instance.saveCachedData();
    }

    public static void removeBundleVersionReference(String bundle_name,
                                                    String version) {
        if (instance != null && instance.isAvailable())
            instance.removeBundleVersion(bundle_name, version);
    }

    public static void removeBundleReference(String bundle_name) {
        if (instance != null && instance.isAvailable())
            instance.removeBundle(bundle_name);
    }

    public static long getMaxCacheAge() {
        if (instance == null)
            return 0L;
        return instance.maxAge();
    }

    public static AbstractTrackerEntry[] getAllRequests() {
        if (instance == null)
            return new AbstractTrackerEntry[0];
        return instance.getAllRequestsInternal();
    }

    public AbstractTrackerEntry[] getAllRequestsInternal() {
        if (!instance.isAvailable())
            return new AbstractTrackerEntry[0];
        AbstractTrackerEntry[] result = null;
        synchronized (CACHE_LOCK) {
            int size = cache == null ? 0 : cache.size();
            result = new AbstractTrackerEntry[size];
            for (int i = 0; i < size; i++)
                result[i] = (AbstractTrackerEntry) cache.elementAt(i);
        }
        return result;
    }
}
