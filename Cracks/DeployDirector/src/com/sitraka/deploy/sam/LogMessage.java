// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogMessage.java

package com.sitraka.deploy.sam;

import java.sql.Timestamp;

public class LogMessage
{

    protected String event;
    protected String remote_id;
    protected String user_id;
    protected String server_id;
    protected Timestamp ts;
    protected String application;
    protected String version;
    protected String notes;
    protected double avg_1_min;
    protected double avg_5_min;
    protected double avg_10_min;
    protected int log_class;
    protected long uptime;
    protected Timestamp lastConnection;
    protected String lastClientIP;
    protected String initialVersion;
    protected String initialUserId;
    protected Timestamp installDate;

    public LogMessage(String event, Timestamp ts, String server)
    {
        this.event = null;
        remote_id = null;
        user_id = null;
        server_id = null;
        this.ts = null;
        application = null;
        version = null;
        notes = null;
        avg_1_min = 0.0D;
        avg_5_min = 0.0D;
        avg_10_min = 0.0D;
        log_class = 0;
        uptime = -1L;
        lastConnection = null;
        lastClientIP = null;
        initialVersion = null;
        initialUserId = null;
        installDate = null;
        this.event = event;
        remote_id = null;
        user_id = null;
        this.ts = ts;
        application = null;
        version = null;
        notes = null;
        server_id = server;
        if(event.toLowerCase().startsWith("serverload"))
            log_class = 3;
        else
        if(event.toLowerCase().startsWith("server"))
            log_class = 2;
        else
        if(event.toLowerCase().startsWith("clientdata"))
            log_class = 4;
        else
        if(event.toLowerCase().startsWith("client"))
            log_class = 1;
    }

    public String getEvent()
    {
        return event;
    }

    public String getServerId()
    {
        return server_id;
    }

    public Timestamp getTimestamp()
    {
        return ts;
    }

    public String getRemoteId()
    {
        return remote_id;
    }

    public String getUserId()
    {
        return user_id;
    }

    public String getApplication()
    {
        return application;
    }

    public String getVersion()
    {
        return version;
    }

    public String getNotes()
    {
        return notes;
    }

    public double getShortTermLoad()
    {
        return avg_1_min;
    }

    public double getMidTermLoad()
    {
        return avg_5_min;
    }

    public double getLongTermLoad()
    {
        return avg_10_min;
    }

    public int getMessageType()
    {
        return log_class;
    }

    public long getUptime()
    {
        return uptime;
    }

    public Timestamp getLastConnection()
    {
        return lastConnection;
    }

    public Timestamp getInstallDate()
    {
        return installDate;
    }

    public String getLastClientIP()
    {
        return lastClientIP;
    }

    public String getInitialVersion()
    {
        return initialVersion;
    }

    public String getInitialUserId()
    {
        return initialUserId;
    }

    public void setUptime(long up)
    {
        uptime = up;
    }

    public void setRemoteId(String id)
    {
        remote_id = id;
    }

    public void setUserId(String id)
    {
        user_id = id;
    }

    public void setApplication(String app)
    {
        application = app;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    public void setLoadAverage(double loads[])
    {
        avg_1_min = loads[0];
        avg_5_min = loads[1];
        avg_10_min = loads[2];
    }

    public void setLastConnection(Timestamp ts)
    {
        lastConnection = ts;
    }

    public void setInstallDate(Timestamp ts)
    {
        installDate = ts;
    }

    public void setLastClientIP(String s)
    {
        lastClientIP = s;
    }

    public void setInitialVersion(String s)
    {
        initialVersion = s;
    }

    public void setInitialUserId(String s)
    {
        initialUserId = s;
    }

    public String toString()
    {
        StringBuffer b = new StringBuffer();
        b.append("\t" + event + " at " + ts.toString() + " on " + server_id + "\n");
        String sample = "\t\tApplication: '" + application + "'                                                 ";
        sample = sample.substring(0, 40);
        b.append(sample + "Version: " + version + "\n");
        sample = "\t\tLocal ID: " + user_id + "                                                 ";
        sample = sample.substring(0, 40);
        b.append(sample + "Remote ID: " + remote_id + "\n");
        b.append("\t\tNotes: " + (notes != null ? notes : "(none)"));
        return b.toString();
    }

    public String formatMessage()
    {
        StringBuffer b = new StringBuffer();
        b.append("The following error has been detected and logged by " + server_id + "\n");
        b.append("\n");
        b.append("      Error: " + event + "\n");
        b.append("       Time: " + ts.toString() + "\n");
        b.append("     Server: " + server_id + "\n");
        b.append("Application: " + application + "\n");
        b.append("    Version: " + version + "\n");
        b.append("    User ID: " + user_id + "\n");
        b.append("  Remote ID: " + remote_id + "\n");
        b.append("      Notes: ");
        b.append((notes != null ? notes : "(none)") + "\n");
        return b.toString();
    }
}
