// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractTrackerEntry.java

package com.sitraka.deploy.sam;

import java.io.File;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam:
//            AbstractTracker, AbstractTrackerList

public abstract class AbstractTrackerEntry
    implements AbstractTracker
{

    protected File file;
    protected long size;
    protected long createDate;
    protected long accessDate;
    protected int refCount;
    protected String request;
    protected long id;
    protected String statusText;
    protected int status;
    protected StringBuffer text;
    protected boolean valid;

    public AbstractTrackerEntry()
    {
        file = null;
        size = 0L;
        createDate = 0L;
        accessDate = 0L;
        refCount = 0;
        request = null;
        id = -1L;
        statusText = null;
        status = 1;
        text = null;
        valid = false;
        id = AbstractTrackerList.createID();
        refCount = 1;
    }

    public AbstractTrackerEntry(File _newFile, String _request)
    {
        this();
        if(_newFile != null && !_newFile.canRead())
        {
            valid = false;
            return;
        } else
        {
            file = _newFile;
            request = _request;
            createDate = System.currentTimeMillis();
            accessDate = System.currentTimeMillis();
            size = file != null ? file.length() : 0L;
            valid = true;
            return;
        }
    }

    public boolean moveFile(File _newDir)
    {
        if(file == null)
            return true;
        String new_file_name = file.getName();
        if(new_file_name.endsWith(".tmp"))
        {
            int index = new_file_name.indexOf(".tmp");
            new_file_name = new_file_name.substring(0, index) + ".zip";
        }
        File tmpFile = new File(_newDir, new_file_name);
        if(!file.renameTo(tmpFile))
            return false;
        if(!tmpFile.canRead())
        {
            return false;
        } else
        {
            file = tmpFile;
            return true;
        }
    }

    public File getFile()
    {
        if(!valid)
            return null;
        else
            return file;
    }

    public void setFile(File f)
    {
        file = f;
        size = file != null ? file.length() : 0L;
    }

    public boolean isValid()
    {
        return valid;
    }

    public boolean isExpired(long max_age)
    {
        if(!valid)
            return true;
        if(status == 4)
            return true;
        if(status != 3)
            return false;
        return accessDate + max_age <= System.currentTimeMillis();
    }

    public long getSize()
    {
        if(!valid)
            return -1L;
        else
            return size;
    }

    public String getRequest()
    {
        if(!valid)
            return null;
        else
            return request;
    }

    public long getAccessTime()
    {
        if(!valid)
            return -1L;
        else
            return accessDate;
    }

    public void updateAccess()
    {
        if(!valid)
        {
            return;
        } else
        {
            accessDate = System.currentTimeMillis();
            return;
        }
    }

    public void addReference()
    {
        refCount++;
    }

    public void removeReference()
    {
        refCount--;
    }

    public int getReferenceCount()
    {
        return refCount;
    }

    public long getID()
    {
        return id;
    }

    public void setStatusText(String text)
    {
        statusText = text;
    }

    public void setStatus(int _status)
    {
        status = _status;
        if(status == 3)
            if(file != null)
                size = file.length();
            else
            if(text != null)
                size = text.length();
            else
                size = 0L;
    }

    public int getStatus()
    {
        return status;
    }

    public void setText(StringBuffer _text)
    {
        text = _text;
        size = text != null ? text.length() : 0L;
    }

    public StringBuffer getText()
    {
        return text;
    }

    public String getStatusText()
    {
        return statusText;
    }

    public void getProperties(Properties props, int base)
    {
        if(!valid)
            return;
        if(file != null)
            ((Hashtable) (props)).put(((Object) ("file" + base + ".file")), ((Object) (file.getName())));
        else
            ((Hashtable) (props)).put(((Object) ("file" + base + ".file")), "");
        ((Hashtable) (props)).put(((Object) ("file" + base + ".request")), ((Object) (request)));
        ((Hashtable) (props)).put(((Object) ("file" + base + ".creationdate")), ((Object) ("" + createDate)));
        ((Hashtable) (props)).put(((Object) ("file" + base + ".accessdate")), ((Object) ("" + accessDate)));
        ((Hashtable) (props)).put(((Object) ("file" + base + ".refcount")), ((Object) ("" + refCount)));
        ((Hashtable) (props)).put(((Object) ("file" + base + ".id")), ((Object) ("" + id)));
        if(statusText != null)
            ((Hashtable) (props)).put(((Object) ("file" + base + ".status")), ((Object) ("" + statusText)));
    }
}
