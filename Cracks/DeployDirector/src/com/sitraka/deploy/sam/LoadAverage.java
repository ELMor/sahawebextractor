// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LoadAverage.java

package com.sitraka.deploy.sam;

import com.sitraka.deploy.common.timer.Timer;
import com.sitraka.deploy.common.timer.TimerEvent;
import com.sitraka.deploy.common.timer.TimerEventListener;

// Referenced classes of package com.sitraka.deploy.sam:
//            LogQueue

public class LoadAverage
    implements TimerEventListener
{
    private class LogEvent extends TimerEvent
    {

        protected boolean log;

        public boolean getLog()
        {
            return log;
        }

        public void setLog(boolean _log)
        {
            log = _log;
        }

        public LogEvent(long start_time, long repeat_interval, TimerEventListener l, boolean log)
        {
            super(start_time, repeat_interval, l);
            this.log = false;
            setLog(log);
        }
    }


    protected long current_count;
    protected Object CURRENT_COUNT_LOCK;
    protected double current_average[];
    protected Object CURRENT_AVERAGE_LOCK;
    protected LogQueue logger;
    protected long frequency;
    protected long startup_time;
    protected static final double PERIOD_LONG = 30D;
    protected static final double PERIOD_MEDIUM = 10D;
    protected static final double PERIOD_SHORT = 2D;
    protected Timer manager;

    public LoadAverage(Timer manager, LogQueue logger, long log_interval, long uptime)
    {
        CURRENT_COUNT_LOCK = new Object();
        CURRENT_AVERAGE_LOCK = new Object();
        this.logger = null;
        frequency = -1L;
        this.manager = null;
        this.manager = manager;
        startup_time = uptime;
        current_count = 0L;
        current_average = new double[3];
        current_average[0] = 0.0D;
        current_average[1] = 0.0D;
        current_average[2] = 0.0D;
        this.logger = logger;
        frequency = log_interval;
        manager.addTimerEvent(((TimerEvent) (new LogEvent(System.currentTimeMillis(), 30000L, ((TimerEventListener) (this)), false))));
        if(frequency != -1L)
            manager.addTimerEvent(((TimerEvent) (new LogEvent(System.currentTimeMillis(), frequency, ((TimerEventListener) (this)), true))));
    }

    public void flushOccured(TimerEvent e)
    {
        eventOccured(e);
    }

    public void eventOccured(TimerEvent e)
    {
        LogEvent le = (LogEvent)e;
        if(le.getLog())
        {
            synchronized(CURRENT_AVERAGE_LOCK)
            {
                if(logger != null)
                    logger.logLoadMessage(current_average, System.currentTimeMillis() - startup_time);
            }
            return;
        }
        long count = 0L;
        double old_average[] = new double[3];
        double new_average[] = new double[3];
        long current_time = System.currentTimeMillis();
        synchronized(CURRENT_COUNT_LOCK)
        {
            count = current_count;
            current_count = 0L;
        }
        synchronized(CURRENT_AVERAGE_LOCK)
        {
            old_average[0] = current_average[0];
            old_average[1] = current_average[1];
            old_average[2] = current_average[2];
        }
        new_average[0] = (0.5D * (double)count) / 30D;
        new_average[0] += 0.5D * old_average[0];
        new_average[1] = (0.10000000000000001D * (double)count) / 30D;
        new_average[1] += 0.90000000000000002D * old_average[1];
        new_average[2] = (0.033333333333333333D * (double)count) / 30D;
        new_average[2] += 0.96666666666666667D * old_average[2];
        synchronized(CURRENT_AVERAGE_LOCK)
        {
            current_average[0] = new_average[0];
            current_average[1] = new_average[1];
            current_average[2] = new_average[2];
        }
    }

    public void registerRequest()
    {
        synchronized(CURRENT_COUNT_LOCK)
        {
            current_count++;
        }
    }

    public double[] getCurrentLoad()
    {
        double value[] = new double[3];
        synchronized(CURRENT_AVERAGE_LOCK)
        {
            value[0] = current_average[0];
            value[1] = current_average[1];
            value[2] = current_average[2];
        }
        return value;
    }
}
