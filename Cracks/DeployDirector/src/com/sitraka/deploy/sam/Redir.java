// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Redir.java

package com.sitraka.deploy.sam;

import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.util.PropertyUtils;
import com.sitraka.licensing.LicenseProperties;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam:
//            ServerActions, LogQueue

public class Redir
{

    public Redir()
    {
    }

    public static boolean isLicensed(ServerActions server, Properties props, int numApplications)
    {
        LicenseProperties licenseProperties = new LicenseProperties();
        String serial_number = PropertyUtils.readDefaultString(props, "serial_number", ((String) (null)));
        if(serial_number == null)
        {
            String msg = "No serial number listed.";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("serial_number", serial_number);
        String hosts = PropertyUtils.readDefaultString(props, "hosts", ((String) (null)));
        if(hosts == null)
        {
            String msg = "No host names specified.";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("hosts", hosts);
        int hostResult = licenseProperties.validateHost("hosts");
        if(hostResult != 1)
        {
            if(hostResult == 12)
            {
                String msg = "Hosts property not found.";
                server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                return false;
            }
            if(hostResult == 3)
            {
                String msg = "Number of CPUs exceeded.";
                server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                return false;
            }
            if(hostResult == 2)
            {
                String msg = "Hostname is invalid.";
                server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                return false;
            } else
            {
                String msg = "Unknown hostname error.";
                server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                return false;
            }
        }
        int num_clients = -1;
        String clients = props.getProperty("clients", ((String) (null)));
        if(clients != null)
        {
            try
            {
                num_clients = Integer.parseInt(clients);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (clients))))
                {
                    num_clients = -1;
                } else
                {
                    String msg = "Maximum number of clients incorrectly specifiedor missing: \"" + clients + "\"";
                    server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                    return false;
                }
            }
            ((Properties) (licenseProperties)).setProperty("clients", clients);
        }
        int client_count = 0;
        try
        {
            client_count = server.getLogQueue().getClientDataLogger().getClients(0xffffffffcf7c5800L);
        }
        catch(LogException le) { }
        catch(NullPointerException npe) { }
        if(num_clients > 0 && client_count > num_clients)
        {
            String msg = "number of current clients (" + client_count + ") exceeds maximum allowed by license (" + num_clients + ")";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        String signature = PropertyUtils.readDefaultString(props, "sitraka.license.signature", ((String) (null)));
        if(signature == null)
        {
            String msg = "License signature is missing";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("sitraka.license.signature", signature);
        boolean checkNumberOfBundles = true;
        String app = props.getProperty("applications", ((String) (null)));
        Vector applications;
        if(app == null)
            applications = new Vector();
        else
        if(app.trim().length() == 0 || app.indexOf("(unlimited)") >= 0)
        {
            ((Properties) (licenseProperties)).setProperty("applications", app);
            applications = new Vector();
        } else
        {
            ((Properties) (licenseProperties)).setProperty("applications", app);
            applications = PropertyUtils.readVector(props, "applications", ',');
            if(applications == null)
                applications = new Vector();
            else
                checkNumberOfBundles = false;
        }
        int num_apps = -1;
        String bundles = props.getProperty("bundles", ((String) (null)));
        if(bundles != null)
        {
            try
            {
                num_apps = Integer.parseInt(bundles);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (bundles))))
                {
                    num_apps = -1;
                } else
                {
                    String msg = "Maximum number of bundles incorrectly specifiedor missing: \"" + bundles + "\"";
                    server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                    return false;
                }
            }
            ((Properties) (licenseProperties)).setProperty("bundles", bundles);
        }
        if(checkNumberOfBundles && num_apps > 0 && numApplications > num_apps)
        {
            String msg = "Number of user bundles (" + numApplications + ") exceeds maximum allowed by license (" + num_apps + ")";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        String date_string = props.getProperty("expiry", ((String) (null)));
        if(date_string != null && !"(never)".equals(((Object) (date_string))))
        {
            ((Properties) (licenseProperties)).setProperty("expiry", date_string);
            if(licenseProperties.isDateExpired("expiry"))
            {
                String msg = "License has expired: " + date_string;
                server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                return false;
            }
        }
        String ddLicenseVersion = PropertyUtils.readDefaultString(props, "license_version", ((String) (null)));
        if(!"2.5".equals(((Object) (ddLicenseVersion))))
        {
            String msg = "License Version is incorrect: '" + ddLicenseVersion + "' vs '" + "2.5" + "'";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("license_version", ddLicenseVersion);
        String licenseVersion = PropertyUtils.readDefaultString(props, "sitraka.license.version", ((String) (null)));
        if(licenseVersion == null)
        {
            String msg = "License Version is missing.";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("sitraka.license.version", licenseVersion);
        String licenseType = PropertyUtils.readDefaultString(props, "type", ((String) (null)));
        if(!"department".equals(((Object) (licenseType))) && !"developer".equals(((Object) (licenseType))) && !"enterprise".equals(((Object) (licenseType))) && !"evaluation".equals(((Object) (licenseType))))
        {
            String msg = "License Type is invalid '" + licenseType + "'";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("type", licenseType);
        Class centinal;
        if("department".equals(((Object) (licenseType))) || "developer".equals(((Object) (licenseType))) || "evaluation".equals(((Object) (licenseType))))
            try
            {
                centinal = Class.forName("org.apache.tomcat.core.Security");
            }
            catch(ClassNotFoundException cnfe)
            {
                String msg = "When using a " + licenseType + " license, " + "DeployDirector must be run with the standalone Tomcat " + "Server shipped by Sitraka.";
                server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
                return false;
            }
        String licenseProduct = PropertyUtils.readDefaultString(props, "product", ((String) (null)));
        if(licenseProduct == null)
        {
            String msg = "License Product is invalid '" + licenseProduct + "'";
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", "REASON: " + msg);
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("product", licenseProduct);
        licenseProperties.validate();
        boolean result = licenseProperties.isValid();
        if(!result)
        {
            server.logMessage((String)null, "ServerLicenseError", "DeploySam", "2.5.0", " REASON: Data does not match signature");
            return false;
        } else
        {
            return true;
        }
    }
}
