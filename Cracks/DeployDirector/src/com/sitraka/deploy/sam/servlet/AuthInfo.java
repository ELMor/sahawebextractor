// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthInfo.java

package com.sitraka.deploy.sam.servlet;

import com.sitraka.deploy.util.Codecs;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AuthInfo
{

    private AuthInfo()
    {
    }

    public static Object getSessionValue(Object request, String field)
    {
        if(request == null)
            return ((Object) (null));
        HttpServletRequest req = (HttpServletRequest)request;
        HttpSession session = req.getSession(false);
        if(session == null)
            session = req.getSession(true);
        Object auth = null;
        if(session != null && field != null)
        {
            if(!session.isNew())
            {
                long created = session.getCreationTime();
                if(System.currentTimeMillis() - created > 0x1b7740L)
                    session.invalidate();
            }
            try
            {
                auth = session.getValue(field);
            }
            catch(IllegalStateException e) { }
        }
        return auth;
    }

    public static void putSessionValue(Object request, String field, Object data)
    {
        if(request == null)
            return;
        HttpServletRequest req = (HttpServletRequest)request;
        HttpSession session = req.getSession(false);
        if(session == null)
            session = req.getSession(true);
        if(session != null && field != null && data != null)
            try
            {
                session.putValue(field, data);
            }
            catch(IllegalStateException e) { }
        else
        if(data == null && session != null && field != null)
            try
            {
                session.removeValue(field);
            }
            catch(IllegalStateException e) { }
    }

    public static Object getUserAuthorization(Object request)
    {
        if(request == null)
            return ((Object) (null));
        Object auth = null;
        if(auth == null)
        {
            String encoded = ((HttpServletRequest)request).getHeader("Authorization");
            if(encoded == null)
                return ((Object) (null));
            if(encoded.startsWith("Basic "))
                encoded = encoded.substring("Basic ".length());
            auth = ((Object) (encoded));
        }
        return auth;
    }

    public static String decodeUser(Object user)
    {
        String decoded = null;
        if(user == null)
            decoded = ":";
        else
            decoded = Codecs.base64Decode(user.toString());
        return decoded;
    }

    public static String getUserName(Object request, String field)
    {
        if(request == null)
            return null;
        HttpServletRequest req = (HttpServletRequest)request;
        String user_id = null;
        if(user_id == null)
        {
            String encodedname = req.getHeader("Authorization");
            if(encodedname == null)
                return null;
            encodedname = encodedname.substring(encodedname.indexOf(" ") + 1);
            String decoded = Codecs.base64Decode(encodedname);
            int index = decoded.indexOf(":");
            user_id = decoded.substring(0, index);
        }
        return user_id;
    }

    public static String getUserPassword(Object request, String field)
    {
        if(request == null)
            return null;
        HttpServletRequest req = (HttpServletRequest)request;
        String password = null;
        if(password == null)
        {
            String encodedname = req.getHeader("Authorization");
            if(encodedname == null)
                return null;
            encodedname = encodedname.substring(encodedname.indexOf(" ") + 1);
            String decoded = Codecs.base64Decode(encodedname);
            int index = decoded.indexOf(":");
            password = decoded.substring(index + 1);
        }
        if(password != null && password.endsWith(":"))
            password = password.substring(0, password.length() - 1);
        return password;
    }
}
