// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServletProcessException.java

package com.sitraka.deploy.sam.servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class ServletProcessException extends ServletException
{

    protected boolean logMessage;
    protected Object request;
    protected Object response;
    protected String category;
    protected String application;
    protected String version;
    protected String remoteName;
    protected boolean showErrorPage;

    public ServletProcessException(String message)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
    }

    public ServletProcessException(String message, Object req, Object resp)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        request = req;
        response = resp;
    }

    public ServletProcessException(String message, Object req, Object resp, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        request = req;
        response = resp;
        logMessage = log;
    }

    public ServletProcessException(String message, Throwable root, Object req, Object resp)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        request = req;
        response = resp;
    }

    public ServletProcessException(String message, Throwable root, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        logMessage = log;
    }

    public ServletProcessException(String message, Throwable root, Object req, Object resp, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        request = req;
        response = resp;
        logMessage = log;
    }

    public ServletProcessException(String message, Throwable root, String cat, Object req, Object resp, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        category = cat;
        if(cat == null)
            category = "ServerExceptionError";
        request = req;
        response = resp;
        logMessage = log;
        application = ((ServletRequest) ((HttpServletRequest)req)).getParameter("APPLICATION");
        version = ((ServletRequest) ((HttpServletRequest)req)).getParameter("VERSION");
    }

    public ServletProcessException(String message, String cat, Object req, Object resp, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        category = cat;
        request = req;
        response = resp;
        logMessage = log;
        application = ((ServletRequest) ((HttpServletRequest)req)).getParameter("APPLICATION");
        version = ((ServletRequest) ((HttpServletRequest)req)).getParameter("VERSION");
    }

    public ServletProcessException(String message, Throwable root, String cat, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        category = cat;
        if(cat == null)
            category = "ServerExceptionError";
        logMessage = log;
    }

    public ServletProcessException(String message, String cat, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        category = cat;
        logMessage = log;
    }

    public ServletProcessException(String message, String cat, String app, String ver, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        category = cat;
        application = app;
        version = ver;
        logMessage = log;
    }

    public ServletProcessException(String message, Throwable root, String cat, String app, String ver, String remote, boolean log)
    {
        super(message, root);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        category = cat;
        application = app;
        version = ver;
        remoteName = remote;
        logMessage = log;
    }

    public ServletProcessException(String message, String cat, String app, String ver, String remote, boolean log)
    {
        super(message);
        logMessage = false;
        category = null;
        application = null;
        version = null;
        remoteName = null;
        showErrorPage = true;
        category = cat;
        application = app;
        version = ver;
        remoteName = remote;
        logMessage = log;
    }

    public boolean mustLog()
    {
        return logMessage;
    }

    public String getApplication()
    {
        return application;
    }

    public void setApplication(String app)
    {
        application = app;
    }

    public String getCategory()
    {
        return category;
    }

    public String getRemoteName()
    {
        return remoteName;
    }

    public void setRemoteName(String name)
    {
        remoteName = name;
    }

    public Object getRequest()
    {
        return request;
    }

    public void setRequest(Object req)
    {
        request = req;
    }

    public Object getResponse()
    {
        return response;
    }

    public void setResponse(Object resp)
    {
        response = resp;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String ver)
    {
        version = ver;
    }

    public boolean isShowErrorPage()
    {
        return showErrorPage;
    }

    public void setShowErrorPage(boolean show)
    {
        showErrorPage = show;
    }
}
