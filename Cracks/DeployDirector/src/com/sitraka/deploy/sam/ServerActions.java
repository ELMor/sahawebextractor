// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerActions.java

package com.sitraka.deploy.sam;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

// Referenced classes of package com.sitraka.deploy.sam:
//            AppVault, RequestParser, LogQueue

public interface ServerActions
{

    public abstract void showErrorPage(String s, Object obj, Object obj1);

    public abstract void showErrorPage(String s, Object obj, Object obj1, boolean flag);

    public abstract void show404Page(Object obj, Object obj1);

    public abstract void showInstallationPage(String s, String s1, String s2, Object obj, Object obj1, boolean flag);

    public abstract AppVault getVault();

    public abstract PrintWriter getTextOutputStream(Object obj);

    public abstract OutputStream getBinaryOutputStream(Object obj);

    public abstract InputStream getBinaryInputStream(Object obj);

    public abstract void setContentType(Object obj, String s);

    public abstract void setContentLength(Object obj, int i);

    public abstract void setHeader(Object obj, String s, String s1);

    public abstract String getRequestUrl(Object obj);

    public abstract String getServerURI(Object obj);

    public abstract String getServerURI(String s);

    public abstract String rebuildURL(Object obj);

    public abstract String getServerPath(Object obj);

    public abstract int getContentLength(Object obj);

    public abstract void logMessage(Object obj, String s, String s1, String s2, String s3);

    public abstract void logMessage(String s, String s1, String s2, String s3, String s4);

    public abstract void logMessage(Object obj, String s, String s1, String s2);

    public abstract void logMessage(String s, String s1, String s2, String s3);

    public abstract String logException(Object obj, Throwable throwable, String s, String s1, String s2);

    public abstract String getUserName(Object obj, String s);

    public abstract Object getUserAuthorization(Object obj);

    public abstract int isUserAuthenticated(Object obj, Object obj1, RequestParser requestparser);

    public abstract void restartServer();

    public abstract String getAdminID();

    public abstract String getAdminPassword();

    public abstract LogQueue getLogQueue();

    public abstract double[] getServerLoad();

    public abstract long getUptime();

    public abstract int getConcurrentRequestCount();

    public abstract int getMaxConcurrentRequestsRecorded();

    public abstract String getServletInfo();
}
