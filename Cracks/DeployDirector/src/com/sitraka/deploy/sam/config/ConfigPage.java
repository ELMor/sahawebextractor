// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConfigPage.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigWizardGUI

public abstract class ConfigPage extends JPanel
{
    private class ButtonListener
        implements ActionListener
    {

        protected ConfigPage buttonPage;

        public void actionPerformed(ActionEvent e)
        {
            if(e.getActionCommand().equals(((Object) (ConfigPage.li.getString("DDConfWizPageNextButton") + " >>"))))
            {
                if(buttonPage.beforeNext())
                    gui.nextPage();
            } else
            if(e.getActionCommand().equals(((Object) ("<< " + ConfigPage.li.getString("DDConfWizPagePrevButton")))))
            {
                if(buttonPage.beforePrevious())
                    gui.prevPage();
            } else
            if(e.getActionCommand().equals(((Object) (ConfigPage.li.getString("DDConfWizPageCancelButton")))))
            {
                if(buttonPage.beforeCancel())
                {
                    ((Window) (gui)).dispose();
                    System.exit(0);
                }
            } else
            if(e.getActionCommand().equals(((Object) (ConfigPage.li.getString("DDConfWizPageOK")))))
            {
                if(buttonPage.beforeOK())
                    gui.showPage("LicensePage");
            } else
            if(e.getActionCommand().equals(((Object) (ConfigPage.li.getString("DDConfWizPageFinishButton")))) && buttonPage.beforeFinish())
            {
                ((Window) (gui)).dispose();
                System.exit(0);
            }
        }

        public ButtonListener(ConfigPage buttonOn)
        {
            buttonPage = buttonOn;
        }
    }


    protected static ResourceBundle li;
    public static final String PREVIOUS_BUTTON = "DDConfWizPagePrevButton";
    public static final String NEXT_BUTTON = "DDConfWizPageNextButton";
    public static final String CANCEL_BUTTON = "DDConfWizPageCancelButton";
    public static final String FINISH_BUTTON = "DDConfWizPageFinishButton";
    public static final String OK_TEXT = "DDConfWizPageOK";
    protected static final int PREVIOUS = 1;
    protected static final int NEXT = 2;
    protected static final int CANCEL = 4;
    protected static final int FINISH = 8;
    protected static final int OK = 16;
    protected static final Font TITLE_FONT = new Font("TitleFont", 1, 18);
    protected JButton previous;
    protected JButton next;
    protected JButton cancel;
    protected JButton finish;
    protected ConfigWizardGUI gui;
    protected JTextArea descriptor;
    protected JPanel contentPanel;
    protected JScrollPane scroller;
    protected JLabel titleLabel;

    public ConfigPage(ConfigWizardGUI wizardGUI, int buttons)
    {
        int buttonNum = buttons;
        gui = wizardGUI;
        descriptor = null;
        Box page = new Box(1);
        JPanel titlePanel = new JPanel();
        titleLabel = new JLabel("");
        ((JComponent) (titleLabel)).setFont(TITLE_FONT);
        ((Container) (titlePanel)).add(((java.awt.Component) (titleLabel)));
        ((Container) (page)).add(((java.awt.Component) (titlePanel)));
        Box subBox = new Box(0);
        JPanel info = new JPanel();
        ((JComponent) (info)).setBorder(((javax.swing.border.Border) (new EtchedBorder())));
        descriptor = new JTextArea();
        ((JTextComponent) (descriptor)).setEditable(false);
        descriptor.setLineWrap(true);
        descriptor.setWrapStyleWord(true);
        ((JTextComponent) (descriptor)).setText(getDocs());
        scroller = new JScrollPane(((java.awt.Component) (descriptor)));
        ((Container) (info)).add(((java.awt.Component) (scroller)));
        Dimension dim = new Dimension(220, 313);
        ((JComponent) (descriptor)).setPreferredSize(dim);
        ((Container) (subBox)).add(((java.awt.Component) (info)));
        ((Container) (subBox)).add(Box.createHorizontalStrut(15));
        contentPanel = new JPanel();
        ((Container) (subBox)).add(((java.awt.Component) (contentPanel)));
        ((Container) (page)).add(((java.awt.Component) (subBox)));
        previous = new JButton("<< " + li.getString("DDConfWizPagePrevButton"));
        next = new JButton(li.getString("DDConfWizPageNextButton") + " >>");
        cancel = new JButton(li.getString("DDConfWizPageCancelButton"));
        finish = new JButton(li.getString("DDConfWizPageFinishButton"));
        Box subBox2 = new Box(0);
        ((Container) (subBox2)).add(Box.createHorizontalGlue());
        ((Container) (subBox2)).add(((java.awt.Component) (previous)));
        ((Container) (subBox2)).add(((java.awt.Component) (next)));
        ((Container) (subBox2)).add(((java.awt.Component) (cancel)));
        ((Container) (subBox2)).add(((java.awt.Component) (finish)));
        ((Container) (page)).add(((java.awt.Component) (subBox2)));
        ((Container)this).add(((java.awt.Component) (page)));
        ButtonListener bListener = new ButtonListener(this);
        if(buttonNum >= 16)
        {
            ((AbstractButton) (finish)).setText(li.getString("DDConfWizPageOK"));
            buttonNum -= 16;
            ((AbstractButton) (finish)).addActionListener(((ActionListener) (bListener)));
            if(buttonNum >= 8)
                buttonNum -= 8;
        } else
        if(buttonNum < 8)
        {
            ((AbstractButton) (finish)).setEnabled(false);
        } else
        {
            buttonNum -= 8;
            ((AbstractButton) (finish)).addActionListener(((ActionListener) (bListener)));
        }
        if(buttonNum < 4)
        {
            ((AbstractButton) (cancel)).setEnabled(false);
        } else
        {
            buttonNum -= 4;
            ((AbstractButton) (cancel)).addActionListener(((ActionListener) (bListener)));
        }
        if(buttonNum < 2)
        {
            ((AbstractButton) (next)).setEnabled(false);
        } else
        {
            buttonNum -= 2;
            ((AbstractButton) (next)).addActionListener(((ActionListener) (bListener)));
        }
        if(buttonNum < 1)
        {
            ((AbstractButton) (previous)).setEnabled(false);
        } else
        {
            buttonNum--;
            ((AbstractButton) (previous)).addActionListener(((ActionListener) (bListener)));
        }
    }

    protected void setTitle(String title)
    {
        titleLabel.setText(title);
    }

    protected void setContentPanel(JPanel content)
    {
        Dimension dim = new Dimension(520, 313);
        ((JComponent) (content)).setPreferredSize(dim);
        ((JComponent) (content)).setMinimumSize(dim);
        ((JComponent) (content)).setMaximumSize(dim);
        ((Container) (contentPanel)).add(((java.awt.Component) (content)));
    }

    protected abstract boolean beforeNext();

    protected abstract boolean beforePrevious();

    protected abstract void refreshPage();

    protected boolean beforeCancel()
    {
        return gui.cancel() == 0;
    }

    protected void genericPageUpdate()
    {
        JScrollBar scrollbar = scroller.getVerticalScrollBar();
        if(scroller != null)
            scrollbar.setValue(0);
    }

    protected boolean beforeFinish()
    {
        return true;
    }

    protected boolean beforeOK()
    {
        return false;
    }

    protected abstract String getDocs();

    static 
    {
        li = LocaleInfo.li;
    }
}
