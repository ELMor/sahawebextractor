// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ProcessingPage.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import com.sitraka.deploy.util.FileUtils;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import org.apache.xerces.parsers.AbstractDOMParser;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.DOMSerializer;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.Serializer;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigPage, HashcodeConverter, ConfigWizardGUI

public class ProcessingPage extends ConfigPage
{
    public static class DTDEntityResolver
        implements EntityResolver
    {

        public InputSource resolveEntity(String publicID, String systemID)
            throws SAXException
        {
            String fileString = "";
            if(systemID.endsWith("server.dtd"))
                fileString = "/org/apache/tomcat/shell/deployment/server.dtd";
            else
            if(systemID.endsWith("web-app_2.2.dtd"))
                fileString = "/org/apache/tomcat/deployment/web.dtd";
            InputStream inStream = ((Object)this).getClass().getResourceAsStream(fileString);
            if(inStream == null)
                throw new SAXException("Could not find " + fileString);
            else
                return new InputSource(inStream);
        }

        public DTDEntityResolver()
        {
        }
    }


    protected static Properties originalClusterProp;
    public static Properties staticClusterProp;
    public static Properties staticServerProp;
    public static Properties staticWizardProp;
    protected Vector fileBackups;
    protected Vector labels;
    protected Vector values;
    protected static String ddDirectory = null;
    protected static String ddTempDirectory = null;
    protected static ResourceBundle li;
    public static final String TITLE = "DDConfWizCoversionTitle";
    public static final String MAIN_DOCS = "DDConfWizCoversionDocs";
    public static final String CONVERSION_ERROR = "DDConfWizCoversionError";
    public static final String CONVERSION_ERROR_MESSAGE = "DDConfWizCoversionErrorTitle";
    public static final String RECOVERY_MESSAGE = "DDConfWizRecoveryMessage";
    public static final String JDK_LABEL = "DDConfWizPPJDKLabel";
    public static final String SERVER_LABEL = "DDConfWizPPServerLabel";
    public static final String LICENSE_LABEL = "DDConfWizPPLicenseLabel";
    public static final String EMAIL_LABEL = "DDConfWizPPEmailLabel";
    public static final String SERIAL_SUBLABEL = "DDConfWizPPSerialSublabel";
    public static final String HOSTS_SUBLABEL = "DDConfWizPPHostsSublabel";
    public static final String BUNDLES_SUBLABEL = "DDConfWizPPBundlesSublabel";
    public static final String CLIENTS_SUBLABEL = "DDConfWizPPClientsSublabel";
    public static final String APPLICATION_SUBLABEL = "DDConfWizPPAppSublabel";
    public static final String EXPIRY_SUBLABEL = "DDConfWizPPExpirySublabel";
    public static final String PRODUCT_SUBLABEL = "DDConfWizPPProductSublabel";
    public static final String TYPE_SUBLABEL = "DDConfWizPPTypeSublabel";
    public static final String LICENSE_SUBLABEL = "DDConfWizPPLicenseSublabel";
    public static final String SITRAKA_LICENSE_SUBLABEL = "DDConfWizPPSitrakaVersion";
    public static final String ERROR_EMAIL_ENABLED = "DDConfWizPPErrorEmail";
    public static final String MAIL_HOST = "DDConfWizPPMailHost";
    public static final String ADMINISTRATOR_EMAIL = "DDConfWizPPAdministratorEmail";
    public static final String RECIPIENT_EMAIL = "DDConfWizPPRecipientEmail";
    public static final String NOT_APPLICABLE = "DDConfWizPPNotApplicable";
    protected JLabel selectedJDK;
    protected JLabel machineName;
    protected JLabel machinePort;
    protected JLabel machineContext;
    protected JLabel sha1Enabled;
    protected JLabel licSerialNumber;
    protected JLabel licHosts;
    protected JLabel licType;
    protected JLabel licProduct;
    protected JLabel licBundles;
    protected JLabel licClients;
    protected JLabel licApps;
    protected JLabel licExpiry;
    protected JLabel licVersion;
    protected JLabel licSitrakaVersion;
    protected JLabel emailEnabled;
    protected JLabel emailHost;
    protected JLabel emailAdmin;
    protected JLabel emailRecipient;

    public ProcessingPage(ConfigWizardGUI gui, int buttons)
    {
        super(gui, buttons);
        fileBackups = new Vector(5);
        selectedJDK = new JLabel();
        machineName = new JLabel();
        machinePort = new JLabel();
        machineContext = new JLabel();
        sha1Enabled = new JLabel();
        licSerialNumber = new JLabel();
        licHosts = new JLabel();
        licType = new JLabel();
        licProduct = new JLabel();
        licBundles = new JLabel();
        licClients = new JLabel();
        licApps = new JLabel();
        licExpiry = new JLabel();
        licVersion = new JLabel();
        licSitrakaVersion = new JLabel();
        emailEnabled = new JLabel();
        emailHost = new JLabel();
        emailAdmin = new JLabel();
        emailRecipient = new JLabel();
        int j = 0;
        JPanel displayPanel = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        JScrollPane scrollPanel = new JScrollPane();
        scrollPanel.setViewportBorder(((javax.swing.border.Border) (new BevelBorder(1))));
        Dimension dim = new Dimension(520, 310);
        ((JComponent) (scrollPanel)).setPreferredSize(dim);
        buildDisplay();
        for(int i = 0; i < labels.size(); i++)
        {
            ((Container) (displayPanel)).add(((java.awt.Component) ((JLabel)labels.elementAt(i))), ((Object) (new GridBagConstraints(0, j++, 1, 1, 0.0D, 0.0D, 17, 0, new Insets(2, 2, 2, 2), 0, 0))));
            Vector temp = (Vector)values.elementAt(i);
            for(int k = 0; k < temp.size(); k++)
                if(((JLabel[])temp.elementAt(k))[1] == null)
                {
                    ((Container) (displayPanel)).add(((java.awt.Component) (((JLabel[])temp.elementAt(k))[0])), ((Object) (new GridBagConstraints(1, j++, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 2, 2, 2), 0, 0))));
                } else
                {
                    ((Container) (displayPanel)).add(((java.awt.Component) (((JLabel[])temp.elementAt(k))[0])), ((Object) (new GridBagConstraints(1, j, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 2, 2, 2), 0, 0))));
                    ((Container) (displayPanel)).add(((java.awt.Component) (((JLabel[])temp.elementAt(k))[1])), ((Object) (new GridBagConstraints(2, j++, 1, 1, 1.0D, 0.0D, 18, 0, new Insets(2, 2, 2, 2), 0, 0))));
                }

        }

        scrollPanel.setViewportView(((java.awt.Component) (displayPanel)));
        JPanel page = new JPanel();
        ((Container) (page)).add(((java.awt.Component) (scrollPanel)));
        ((ConfigPage)this).setContentPanel(page);
        ((ConfigPage)this).setTitle(li.getString("DDConfWizCoversionTitle"));
    }

    protected void buildDisplay()
    {
        values = new Vector();
        labels = new Vector();
        labels.addElement(((Object) (new JLabel(li.getString("DDConfWizPPJDKLabel")))));
        labels.addElement(((Object) (new JLabel(li.getString("DDConfWizPPServerLabel")))));
        labels.addElement(((Object) (new JLabel(li.getString("DDConfWizPPLicenseLabel")))));
        labels.addElement(((Object) (new JLabel(li.getString("DDConfWizPPEmailLabel")))));
        Font boldFont = new Font("Enlarged Text", 1, 16);
        for(int i = 0; i < labels.size(); i++)
            ((JComponent) ((JLabel)labels.elementAt(i))).setFont(boldFont);

        Vector temp = new Vector(1);
        temp.addElement(((Object) (new JLabel[] {
            selectedJDK, null
        })));
        values.addElement(((Object) (temp)));
        temp = new Vector(5);
        temp.addElement(((Object) (new JLabel[] {
            new JLabel("Machine Name:"), machineName
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel("Port:"), machinePort
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel("Context:"), machineContext
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel("SHA-1 Enabled"), sha1Enabled
        })));
        values.addElement(((Object) (temp)));
        temp = new Vector(10);
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPSerialSublabel")), licSerialNumber
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPHostsSublabel")), licHosts
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPBundlesSublabel")), licBundles
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPClientsSublabel")), licClients
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPAppSublabel")), licApps
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPExpirySublabel")), licExpiry
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPProductSublabel")), licProduct
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPTypeSublabel")), licType
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPLicenseSublabel")), licVersion
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPSitrakaVersion")), licSitrakaVersion
        })));
        values.addElement(((Object) (temp)));
        temp = new Vector();
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPErrorEmail")), emailEnabled
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPMailHost")), emailHost
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPAdministratorEmail")), emailAdmin
        })));
        temp.addElement(((Object) (new JLabel[] {
            new JLabel(li.getString("DDConfWizPPRecipientEmail")), emailRecipient
        })));
        values.addElement(((Object) (temp)));
    }

    protected boolean beforeNext()
    {
        return convertServer();
    }

    protected boolean beforePrevious()
    {
        return true;
    }

    protected void refreshPage()
    {
        ((JComponent) (super.finish)).requestFocus();
        super.genericPageUpdate();
        selectedJDK.setText(staticWizardProp.getProperty("deploy.jvm.selected"));
        machineName.setText(staticClusterProp.getProperty("deploy.cluster.host.0.machine"));
        machinePort.setText(staticClusterProp.getProperty("deploy.cluster.host.0.port"));
        String newVal = staticClusterProp.getProperty("deploy.cluster.host.0.page");
        newVal = newVal.substring(0, newVal.lastIndexOf("/deploy"));
        machineContext.setText(newVal);
        newVal = staticClusterProp.getProperty("deploy.hashcode.algorithm");
        if(newVal != null && newVal.equals("SHA"))
            sha1Enabled.setText("true");
        else
            sha1Enabled.setText("false");
        licSerialNumber.setText(licenseValue(staticClusterProp.getProperty("serial_number")));
        licHosts.setText(licenseValue(staticClusterProp.getProperty("hosts")));
        licBundles.setText(licenseValue(staticClusterProp.getProperty("bundles")));
        licClients.setText(licenseValue(staticClusterProp.getProperty("clients")));
        licApps.setText(licenseValue(staticClusterProp.getProperty("applications")));
        licExpiry.setText(licenseValue(staticClusterProp.getProperty("expiry")));
        licProduct.setText(licenseValue(staticClusterProp.getProperty("product")));
        licType.setText(licenseValue(staticClusterProp.getProperty("type")));
        licVersion.setText(licenseValue(staticClusterProp.getProperty("license_version")));
        licSitrakaVersion.setText(licenseValue(staticClusterProp.getProperty("sitraka.license.version")));
        newVal = staticClusterProp.getProperty("deploy.error.email.server");
        if(newVal != null && !newVal.equals(""))
        {
            emailEnabled.setText("true");
            emailHost.setText(staticClusterProp.getProperty("deploy.error.email.server"));
            emailAdmin.setText(staticClusterProp.getProperty("deploy.error.email.0.address"));
            emailRecipient.setText(staticClusterProp.getProperty("deploy.error.email.user"));
        } else
        {
            emailEnabled.setText("false");
            emailHost.setText(li.getString("DDConfWizPPNotApplicable"));
            emailAdmin.setText(li.getString("DDConfWizPPNotApplicable"));
            emailRecipient.setText(li.getString("DDConfWizPPNotApplicable"));
        }
    }

    protected String licenseValue(String value)
    {
        if(value == null || value.equals(""))
            return new String(li.getString("DDConfWizPPNotApplicable"));
        else
            return value;
    }

    protected String getDocs()
    {
        return li.getString("DDConfWizCoversionDocs");
    }

    protected boolean convertServer()
    {
        boolean success;
        try
        {
            String fileSep = System.getProperty("file.separator");
            String baseDir = System.getProperty("DDConfig.home");
            String tmpDir = baseDir + fileSep + "dd" + fileSep + "temp";
            String targetFileName = baseDir + fileSep + "cluster.properties";
            File target = new File(targetFileName);
            File backup = FileUtils.createTempFile(tmpDir);
            if(!FileUtils.copyFiles(target, backup))
                throw new Exception("Failed to backup cluster.properties.");
            fileBackups.addElement(((Object) (new File[] {
                target, backup
            })));
            if(!writeCurrentSettings(targetFileName, staticClusterProp))
                throw new Exception("Failed to write cluster.properties.");
            targetFileName = baseDir + fileSep + "server.properties";
            target = new File(targetFileName);
            backup = FileUtils.createTempFile(tmpDir);
            if(!FileUtils.copyFiles(target, backup))
                throw new Exception("Failed to backup server.properties.");
            fileBackups.addElement(((Object) (new File[] {
                target, backup
            })));
            if(!writeCurrentSettings(targetFileName, staticServerProp))
                throw new Exception("Failed to write server.properties.");
            targetFileName = baseDir + fileSep + "wizard.properties";
            target = new File(targetFileName);
            if(target.exists())
            {
                backup = FileUtils.createTempFile(tmpDir);
                if(!FileUtils.copyFiles(target, backup))
                    throw new Exception("Failed to backup wizard.properties.");
            }
            fileBackups.addElement(((Object) (new File[] {
                target, backup
            })));
            if(!writeCurrentSettings(targetFileName, staticWizardProp))
                throw new Exception("Failed to write wizard.properties.");
            String oldVar = originalClusterProp.getProperty("deploy.cluster.host.0.port");
            String newVar = staticClusterProp.getProperty("deploy.cluster.host.0.port");
            if(!oldVar.equals(((Object) (newVar))))
            {
                targetFileName = baseDir + fileSep + "standalone" + fileSep + "server.xml";
                target = new File(targetFileName);
                backup = FileUtils.createTempFile(tmpDir);
                if(!FileUtils.copyFiles(target, backup))
                    throw new Exception("Failed to backup server.xml.");
                fileBackups.addElement(((Object) (new File[] {
                    target, backup
                })));
                editServerXML(newVar, target);
            }
            oldVar = originalClusterProp.getProperty("deploy.cluster.host.0.page");
            newVar = staticClusterProp.getProperty("deploy.cluster.host.0.page");
            if(!oldVar.equals(((Object) (newVar))))
            {
                targetFileName = baseDir + fileSep + "standalone" + fileSep + "deploydirector";
                target = new File(targetFileName);
                backup = FileUtils.createTempFile(tmpDir);
                if(!FileUtils.copyDirectory(target, backup, true))
                    throw new Exception("Failed to backup servlet.");
                fileBackups.addElement(((Object) (new File[] {
                    target, backup
                })));
                modifyStandaloneDirectory();
            }
            targetFileName = baseDir + fileSep + "vault";
            target = new File(targetFileName);
            backup = FileUtils.createTempFile(tmpDir);
            String convertToType = staticWizardProp.getProperty("wizard.convert.vault");
            if(convertToType != null)
            {
                if(!FileUtils.copyDirectory(target, backup))
                    throw new Exception("Failed to backup deploydirector.war.");
                fileBackups.addElement(((Object) (new File[] {
                    target, backup
                })));
                HashcodeConverter converter = HashcodeConverter.getInstance();
                converter.convert(new File(ddDirectory), convertToType);
            }
            for(int i = 0; i < fileBackups.size(); i++)
            {
                File temp = ((File[])fileBackups.elementAt(i))[1];
                if(temp.isDirectory())
                    FileUtils.deleteDirectory(temp);
                else
                    temp.delete();
            }

            success = true;
        }
        catch(Exception e)
        {
            undoConversion(((Throwable) (e)).toString());
            success = false;
        }
        return success;
    }

    protected void undoConversion(String msg)
    {
        JOptionPane.showMessageDialog(((java.awt.Component) (super.gui)), ((Object) (li.getString("DDConfWizCoversionErrorTitle") + "\n\n" + msg + "\n\n" + li.getString("DDConfWizRecoveryMessage"))), li.getString("DDConfWizCoversionError"), 1);
        for(int i = 0; i < fileBackups.size(); i++)
        {
            File originalFile = ((File[])fileBackups.elementAt(i))[0];
            File backupFile = ((File[])fileBackups.elementAt(i))[1];
            if(originalFile.isDirectory())
            {
                FileUtils.deleteDirectory(originalFile);
                FileUtils.copyDirectory(backupFile, originalFile);
                FileUtils.deleteDirectory(backupFile);
            } else
            {
                originalFile.delete();
                FileUtils.copyFiles(backupFile, originalFile);
                backupFile.delete();
            }
        }

    }

    public static Properties readCurrentSettings(String file)
    {
        try
        {
            FileInputStream fis = new FileInputStream(file);
            Properties props = new Properties();
            props.load(((InputStream) (fis)));
            fis.close();
            return props;
        }
        catch(FileNotFoundException fnfe)
        {
            System.err.println(((Object) (fnfe)));
        }
        catch(IOException ioe)
        {
            System.err.println(((Object) (ioe)));
        }
        return null;
    }

    public static boolean writeCurrentSettings(String file, Properties props)
    {
        String clusterFile = new String("");
        try
        {
            FileOutputStream fos = new FileOutputStream(file);
            props.store(((OutputStream) (fos)), "Last edited by the DDConfigWizard.");
            ((OutputStream) (fos)).flush();
            fos.close();
            return true;
        }
        catch(FileNotFoundException fnfe)
        {
            System.err.println(((Object) (fnfe)));
        }
        catch(IOException ioe)
        {
            System.err.println(((Object) (ioe)));
        }
        return false;
    }

    protected void editXMLFile(String node, String attribute, String value, File xmlFile)
    {
        DOMParser domParser = null;
        Serializer xmlSerializer = null;
        FileOutputStream fos = null;
        try
        {
            domParser = getDOMParser();
            domParser.parse(new InputSource(((InputStream) (new FileInputStream(xmlFile)))));
            Document document = ((AbstractDOMParser) (domParser)).getDocument();
            Element rootElement = document.getDocumentElement();
            Element child = findChild(rootElement, node);
            if(child != null)
                child.setAttribute(attribute, value);
            if(xmlSerializer == null)
                xmlSerializer = ((Serializer) (new XMLSerializer()));
            fos = new FileOutputStream(xmlFile);
            xmlSerializer.setOutputByteStream(((OutputStream) (fos)));
            DOMSerializer domSerializer = xmlSerializer.asDOMSerializer();
            domSerializer.serialize(document);
            domSerializer = null;
            ((OutputStream) (fos)).flush();
            fos.close();
        }
        catch(SAXException saxE)
        {
            ((Throwable) (saxE)).printStackTrace();
        }
        catch(IOException ioe)
        {
            ((Throwable) (ioe)).printStackTrace();
        }
    }

    protected void editServerXML(String newPort, File xmlFile)
    {
        DOMParser domParser = null;
        Serializer xmlSerializer = null;
        FileOutputStream fos = null;
        try
        {
            domParser = getDOMParser();
            domParser.parse(new InputSource(((InputStream) (new FileInputStream(xmlFile)))));
            Document document = ((AbstractDOMParser) (domParser)).getDocument();
            Element rootElement = document.getDocumentElement();
            Element child = findChild(rootElement, "ContextManager");
            if(child != null)
                child.setAttribute("port", newPort);
            if(xmlSerializer == null)
                xmlSerializer = ((Serializer) (new XMLSerializer()));
            fos = new FileOutputStream(xmlFile);
            xmlSerializer.setOutputByteStream(((OutputStream) (fos)));
            DOMSerializer domSerializer = xmlSerializer.asDOMSerializer();
            domSerializer.serialize(document);
            domSerializer = null;
            ((OutputStream) (fos)).flush();
            fos.close();
        }
        catch(SAXException saxE)
        {
            ((Throwable) (saxE)).printStackTrace();
        }
        catch(IOException ioe)
        {
            ((Throwable) (ioe)).printStackTrace();
        }
    }

    protected Document editWebXML(String newPage, InputStream inStream, String name)
    {
        try
        {
            DOMParser domParser = getDOMParser();
            domParser.parse(new InputSource(inStream));
            Document document = ((AbstractDOMParser) (domParser)).getDocument();
            Element rootElement = document.getDocumentElement();
            Element child = getFirstChildElement(rootElement);
            if(!((Node) (child)).getNodeName().equals("servlet-mapping"))
                child = findSibling(child, "servlet-mapping");
            for(; child != null; child = findSibling(child, "servlet-mapping"))
            {
                Element element = findChild(child, "servlet-name");
                if(element == null)
                    break;
                Node textNode = ((Node) (element)).getFirstChild();
                String text = textNode.getNodeValue();
                if(!text.trim().equals(((Object) (name))))
                    continue;
                element = findSibling(element, "url-pattern");
                textNode = ((Node) (element)).getFirstChild();
                textNode.setNodeValue(newPage + name + "/*");
                break;
            }

            return document;
        }
        catch(SAXException saxE)
        {
            ((Throwable) (saxE)).printStackTrace();
        }
        catch(IOException ioe)
        {
            ((Throwable) (ioe)).printStackTrace();
        }
        return null;
    }

    protected void editWebXML(String baseDir, String newPage, String name)
    {
        Serializer xmlSerializer = null;
        try
        {
            String sep = System.getProperty("file.separator");
            String xmlFile = baseDir + sep + "WEB-INF" + sep + "web.xml";
            File webXML = new File(xmlFile);
            InputStream inStream = ((InputStream) (new FileInputStream(webXML)));
            Document domDocument = editWebXML(newPage, inStream, name);
            inStream.close();
            OutputStream outStream = ((OutputStream) (new FileOutputStream(webXML)));
            if(xmlSerializer == null)
                xmlSerializer = ((Serializer) (new XMLSerializer()));
            OutputFormat outputFormat = new OutputFormat();
            outputFormat.setIndenting(true);
            xmlSerializer.setOutputByteStream(outStream);
            xmlSerializer.setOutputFormat(outputFormat);
            DOMSerializer domSerializer = xmlSerializer.asDOMSerializer();
            domSerializer.serialize(domDocument);
            outStream.flush();
            outStream.close();
        }
        catch(IOException ioe)
        {
            ((Throwable) (ioe)).printStackTrace();
        }
    }

    protected boolean adjustDirectoryStructure(String baseDir)
    {
        boolean success = false;
        String sep = System.getProperty("file.separator");
        String oldContext = originalClusterProp.getProperty("deploy.cluster.host.0.page");
        oldContext = oldContext.substring(0, oldContext.lastIndexOf('/') + 1);
        File oldDirectory = new File(baseDir + sep + oldContext);
        String newContext = staticClusterProp.getProperty("deploy.cluster.host.0.page");
        newContext = newContext.substring(0, newContext.lastIndexOf('/') + 1);
        File newDirectory = new File(baseDir + sep + newContext);
        success = FileUtils.copyDirectory(oldDirectory, newDirectory, false);
        if(success)
            FileUtils.deleteDirectory(oldDirectory);
        return success;
    }

    protected boolean modifyStandaloneDirectory()
    {
        String sep = System.getProperty("file.separator");
        String warLocation = System.getProperty("DDConfig.home") + sep + "standalone" + sep + "deploydirector" + sep;
        return modifyWarContents(warLocation);
    }

    protected boolean modifyWarContents(String baseDir)
    {
        String newContext = staticClusterProp.getProperty("deploy.cluster.host.0.page");
        newContext = newContext.substring(0, newContext.lastIndexOf('/') + 1);
        editWebXML(baseDir, newContext, "deploy");
        editWebXML(baseDir, newContext, "Chart");
        return adjustDirectoryStructure(baseDir);
    }

    protected boolean editDDWarFile(File warFile)
    {
        String sep = System.getProperty("file.separator");
        String newContext = staticClusterProp.getProperty("deploy.cluster.host.0.page");
        newContext = newContext.substring(0, newContext.lastIndexOf('/') + 1);
        String tempContext = newContext;
        newContext = newContext + "admin";
        File tmpDir = FileUtils.createTempFile();
        FileUtils.unzipFile(warFile, tmpDir);
        editWebXML(tmpDir.toString(), newContext, "deploy");
        boolean success = FileUtils.createZip(tmpDir, warFile, ((com.sitraka.deploy.common.awt.ProgressBar) (null)));
        if(success)
            warFile.delete();
        FileUtils.deleteDirectory(tmpDir);
        return success;
    }

    protected DOMParser getDOMParser()
    {
        DOMParser domParser = null;
        try
        {
            domParser = new DOMParser();
            domParser.setFeature("http://xml.org/sax/features/validation", false);
            domParser.setEntityResolver(((EntityResolver) (new DTDEntityResolver())));
        }
        catch(Exception e)
        {
            ((Throwable) (e)).printStackTrace();
        }
        return domParser;
    }

    protected Element getFirstChildElement(Element parent)
    {
        Node node;
        for(node = ((Node) (parent)).getFirstChild(); node != null && node.getNodeType() != 1; node = node.getNextSibling());
        if(node != null)
            return (Element)node;
        else
            return null;
    }

    protected Element findChild(Element parent, String name)
    {
        for(Node child = ((Node) (parent)).getFirstChild(); child != null; child = child.getNextSibling())
            if(child.getNodeName().equals(((Object) (name))) && child.getNodeType() == 1)
                return (Element)child;

        return null;
    }

    protected Element findSibling(Element sibling, String name)
    {
        for(Node child = ((Node) (sibling)).getNextSibling(); child != null; child = child.getNextSibling())
            if(child.getNodeName().equals(((Object) (name))) && child.getNodeType() == 1)
                return (Element)child;

        return null;
    }

    public static Properties getClusterProperties()
    {
        return staticClusterProp;
    }

    public static void setClusterProperties(Properties prop)
    {
        staticClusterProp = prop;
    }

    public static Properties getServerProperties()
    {
        return staticServerProp;
    }

    public static void setServerProperties(Properties prop)
    {
        staticServerProp = prop;
    }

    public static Properties getWizardProperties()
    {
        return staticWizardProp;
    }

    public static void setWizardProperties(Properties prop)
    {
        staticWizardProp = prop;
    }

    public static void init()
    {
        String baseName = System.getProperty("DDConfig.home");
        String sep = System.getProperty("file.separator");
        String fileName = baseName + sep + "cluster.properties";
        staticClusterProp = readCurrentSettings(fileName);
        originalClusterProp = readCurrentSettings(fileName);
        fileName = baseName + sep + "server.properties";
        staticServerProp = readCurrentSettings(fileName);
        fileName = baseName + sep + "wizard.properties";
        if((new File(fileName)).exists())
            staticWizardProp = readCurrentSettings(fileName);
        if(staticWizardProp == null)
            staticWizardProp = new Properties();
        ddDirectory = System.getProperty("DDConfig.home");
        ddTempDirectory = ddDirectory + sep + "dd" + sep + "temp";
    }

    static 
    {
        li = LocaleInfo.li;
    }
}
