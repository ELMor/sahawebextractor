// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LicenseCheck.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import com.sitraka.licensing.InvalidLicenseException;
import com.sitraka.licensing.LicenseProperties;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;

public class LicenseCheck
{

    protected static ResourceBundle li;
    public static final String SIGNATURE_INVALID = "DDConfWizLicenseSignatureMissing";
    public static final String LICENSE_ERROR = "DDConfWizLicenseError";

    public LicenseCheck()
    {
    }

    public static String isLicensed(Properties props)
    {
        LicenseProperties licenseProps = new LicenseProperties();
        String propertyValue = "";
        propertyValue = (String)((Hashtable) (props)).get("bundles");
        if(propertyValue != null && !propertyValue.equals(""))
            ((Hashtable) (licenseProps)).put("bundles", ((Object) (propertyValue)));
        propertyValue = (String)((Hashtable) (props)).get("clients");
        if(propertyValue != null && !propertyValue.equals(""))
            ((Hashtable) (licenseProps)).put("clients", ((Object) (propertyValue)));
        propertyValue = (String)((Hashtable) (props)).get("applications");
        if(propertyValue != null && !propertyValue.equals(""))
            ((Hashtable) (licenseProps)).put("applications", ((Object) (propertyValue)));
        propertyValue = (String)((Hashtable) (props)).get("expiry");
        if(propertyValue != null && !propertyValue.equals(""))
            ((Hashtable) (licenseProps)).put("expiry", ((Object) (propertyValue)));
        ((Hashtable) (licenseProps)).put("serial_number", ((Hashtable) (props)).get("serial_number"));
        ((Hashtable) (licenseProps)).put("hosts", ((Hashtable) (props)).get("hosts"));
        ((Hashtable) (licenseProps)).put("sitraka.license.signature", ((Hashtable) (props)).get("sitraka.license.signature"));
        ((Hashtable) (licenseProps)).put("type", ((Hashtable) (props)).get("type"));
        ((Hashtable) (licenseProps)).put("sitraka.license.version", ((Hashtable) (props)).get("sitraka.license.version"));
        ((Hashtable) (licenseProps)).put("product", ((Hashtable) (props)).get("product"));
        ((Properties) (licenseProps)).list(System.out);
        if(licenseProps.isValid())
            return "valid==true";
        else
            return li.getString("DDConfWizLicenseSignatureMissing");
    }

    public static String isLicensed(String licenseText)
    {
        LicenseProperties properties = new LicenseProperties();
        ByteArrayInputStream stream = new ByteArrayInputStream(licenseText.getBytes());
        try
        {
            properties.loadFromStream(((java.io.InputStream) (stream)));
        }
        catch(InvalidLicenseException e)
        {
            return li.getString("DDConfWizLicenseSignatureMissing");
        }
        catch(IOException e)
        {
            return li.getString("DDConfWizLicenseError");
        }
        return "valid==true";
    }

    static 
    {
        li = LocaleInfo.li;
    }
}
