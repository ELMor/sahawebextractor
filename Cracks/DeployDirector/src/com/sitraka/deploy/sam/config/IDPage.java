// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IDPage.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigPage, ProcessingPage, ConfigWizardGUI

public class IDPage extends ConfigPage
    implements ActionListener
{

    protected static ResourceBundle li;
    public static final String TITLE = "DDConfWizIDTitle";
    public static final String MACHINE_NAME = "DDConfWizIDMachineName";
    public static final String USE_DEFAULTS = "DDConfWizIDUseDefaults";
    public static final String PORT = "DDConfWizIDPort";
    public static final String ADMIN_PAGE = "DDConfWizIDAdminPage";
    public static final String PROTOCOL = "DDConfWizIDProtocol";
    public static final String MACHINE_NAME_MISSING = "DDConfWizIDMachineMissing";
    public static final String PORT_MISSING = "DDConfWizIDPortMissing";
    public static final String PAGE_MISSING = "DDConfWizIDPageMissing";
    public static final String PROTOCOL_MISSING = "DDConfWizIDProtocolMissing";
    public static final String PORT_NOT_INTEGER = "DDConfWizIDPortNotInteger";
    public static final String PORT_NEGATIVE = "DDConfWizIDPortNegative";
    public static final String INVALID_PAGE = "DDConfWizIDInvalidPage";
    public static final String MACHINE_NOT_LOCALHOST = "DDConfWizIDMachineNotLocalhost";
    public static final String UNKNOWN_HOST = "DDConfWizUnknownHost";
    public static final String NOT_EXCLUSIVE = "DDConfWizIDNotExclusive";
    public static final String MACHINE_NOT_FOUND = "DDConfWizIDMachineNotFound";
    public static final String MAIN_DOCS = "DDConfWizIDPageMainDocs";
    public static final String NETWORK_ERROR = "DDConfWizIDPageError";
    public static final String ADMIN_PORT = "DDConfWizIDPageAdminPort";
    public static final String ADMIN_PORT_HEADER = "DDConfWizIDPageAdminPortHeader";
    public static final String SERVER_CONFIGURATION = "DDConfWizIDPageServerConfig";
    public static final String ADVANCED_CONFIGURATION = "DDConfWizIDPageAdvancedConfig";
    public static final String SHA1_ENABLE = "DDConfWizIDPageSHA1Enable";
    public static final String ROLE_ADMINISTRATION = "DDConfigWizIDPageGroupAdmin";
    public static final String ENCODING_WARNING_1 = "DDConfigWizIDPageEncodeMsg1";
    public static final String ENCODING_WARNING_2 = "DDConfigWizIDPageEncodeMsg2";
    public static final String ENCODING_WARNING_3 = "DDConfigWizIDPageEncodeMsg3";
    public static final String ENCODING_TITLE = "DDConfigWizIDPageEncodeTitle";
    public static final String HOST_UNKNOWN = "DDConfigWizIDPageHostUnknown";
    protected JTextField portNumber;
    protected JTextField machineName;
    protected JTextField adminPage;
    protected JCheckBox leaveAsDefault;
    protected JCheckBox enabledSHA1;
    protected JCheckBox enabledSSL;
    protected Properties clusterProps;
    protected static String acceptedHostName = null;
    protected static boolean userAcceptedHost = false;
    protected static int acceptedPort = -1;
    protected static boolean userAcceptedPort = false;

    public IDPage(ConfigWizardGUI gui, int buttons)
    {
        super(gui, buttons);
        JPanel page = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        ((Container) (page)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizIDMachineName")))), ((Object) (new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 20, 2, 2), 0, 0))));
        machineName = new JTextField();
        ((Container) (page)).add(((java.awt.Component) (machineName)), ((Object) (new GridBagConstraints(1, 0, 1, 1, 1.0D, 0.0D, 18, 2, new Insets(2, 2, 2, 20), 0, 0))));
        JPanel middle = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        ((JComponent) (middle)).setBorder(((javax.swing.border.Border) (BorderFactory.createTitledBorder(li.getString("DDConfWizIDPageServerConfig")))));
        leaveAsDefault = new JCheckBox(li.getString("DDConfWizIDUseDefaults"), true);
        ((AbstractButton) (leaveAsDefault)).addActionListener(((ActionListener) (this)));
        ((Container) (middle)).add(((java.awt.Component) (leaveAsDefault)), ((Object) (new GridBagConstraints(0, 0, 1, 1, 0.0D, 0.0D, 12, 0, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (middle)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizIDPort")))), ((Object) (new GridBagConstraints(0, 1, 1, 1, 0.0D, 0.0D, 13, 0, new Insets(2, 10, 2, 2), 0, 0))));
        portNumber = new JTextField();
        ((Container) (middle)).add(((java.awt.Component) (portNumber)), ((Object) (new GridBagConstraints(1, 1, 1, 1, 0.5D, 0.0D, 18, 2, new Insets(2, 2, 2, 10), 0, 0))));
        ((Container) (middle)).add(((java.awt.Component) (new JLabel(""))), ((Object) (new GridBagConstraints(2, 0, 1, 1, 1.0D, 0.0D, 13, 0, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (middle)).add(((java.awt.Component) (new JLabel(li.getString("DDConfWizIDAdminPage")))), ((Object) (new GridBagConstraints(0, 2, 1, 1, 0.0D, 0.0D, 13, 0, new Insets(2, 10, 2, 2), 0, 0))));
        adminPage = new JTextField();
        ((Container) (middle)).add(((java.awt.Component) (adminPage)), ((Object) (new GridBagConstraints(1, 2, 2, 1, 1.0D, 0.0D, 18, 2, new Insets(2, 2, 2, 10), 0, 0))));
        JPanel bottom = new JPanel(((java.awt.LayoutManager) (new GridBagLayout())));
        ((JComponent) (bottom)).setBorder(((javax.swing.border.Border) (BorderFactory.createTitledBorder(li.getString("DDConfWizIDPageAdvancedConfig")))));
        enabledSHA1 = new JCheckBox(li.getString("DDConfWizIDPageSHA1Enable"), false);
        ((AbstractButton) (enabledSHA1)).addActionListener(((ActionListener) (this)));
        ((Container) (bottom)).add(((java.awt.Component) (enabledSHA1)), ((Object) (new GridBagConstraints(0, 0, 1, 1, 1.0D, 0.0D, 16, 17, new Insets(2, 10, 2, 2), 0, 0))));
        ((Container) (page)).add(((java.awt.Component) (middle)), ((Object) (new GridBagConstraints(0, 1, 2, 1, 1.0D, 0.0D, 12, 2, new Insets(10, 20, 10, 20), 0, 0))));
        ((Container) (page)).add(((java.awt.Component) (bottom)), ((Object) (new GridBagConstraints(0, 2, 2, 1, 0.0D, 0.0D, 12, 1, new Insets(10, 20, 10, 20), 0, 0))));
        setEnabled();
        getCurrentSettings();
        ((ConfigPage)this).setContentPanel(page);
        ((ConfigPage)this).setTitle(li.getString("DDConfWizIDTitle"));
    }

    protected boolean beforeNext()
    {
        if(validateSettings())
        {
            saveCurrentSettings();
            return true;
        } else
        {
            return false;
        }
    }

    protected boolean beforePrevious()
    {
        return true;
    }

    protected void refreshPage()
    {
        ((JComponent) (super.next)).requestFocus();
        super.genericPageUpdate();
    }

    protected String getDocs()
    {
        return li.getString("DDConfWizIDPageMainDocs");
    }

    protected void setEnabled()
    {
        ((JComponent) (portNumber)).setEnabled(!((AbstractButton) (leaveAsDefault)).isSelected());
        ((JComponent) (adminPage)).setEnabled(!((AbstractButton) (leaveAsDefault)).isSelected());
    }

    protected void readCurrentSettings()
    {
        clusterProps = ProcessingPage.getClusterProperties();
    }

    protected void writeCurrentSettings()
    {
        ProcessingPage.setClusterProperties(clusterProps);
    }

    protected void getCurrentSettings()
    {
        if(clusterProps == null)
            readCurrentSettings();
        ((JTextComponent) (machineName)).setText(clusterProps.getProperty("deploy.cluster.host.0.machine"));
        ((JTextComponent) (portNumber)).setText(clusterProps.getProperty("deploy.cluster.host.0.port"));
        String tempContext = clusterProps.getProperty("deploy.cluster.host.0.page");
        tempContext = tempContext.substring(0, tempContext.indexOf("/deploy", 0));
        ((JTextComponent) (adminPage)).setText(tempContext);
        ((AbstractButton) (enabledSHA1)).setSelected(isSHA1enabled());
        checkIfDefault();
    }

    protected void saveCurrentSettings()
    {
        ((Hashtable) (clusterProps)).put("deploy.cluster.host.0.machine", ((Object) (((JTextComponent) (machineName)).getText())));
        ((Hashtable) (clusterProps)).put("deploy.cluster.host.0.port", ((Object) (((JTextComponent) (portNumber)).getText())));
        ((Hashtable) (clusterProps)).put("deploy.cluster.host.0.page", ((Object) (((JTextComponent) (adminPage)).getText() + "/deploy")));
        writeCurrentSettings();
    }

    protected void checkIfDefault()
    {
        if(((JTextComponent) (portNumber)).getText().equals("8080") && ((JTextComponent) (adminPage)).getText().equals("/servlet"))
            ((AbstractButton) (leaveAsDefault)).setSelected(true);
        else
            ((AbstractButton) (leaveAsDefault)).setSelected(false);
        setEnabled();
    }

    protected void resetToDefault()
    {
        ((JTextComponent) (portNumber)).setText("8080");
        ((JTextComponent) (adminPage)).setText("/servlet");
    }

    protected boolean isGroupAdminEnabled()
    {
        return !((Hashtable) (clusterProps)).get("deploy.admin.authentication").equals("com.sitraka.deploy.admin.auth.AdminAuthentication");
    }

    protected boolean isSHA1enabled()
    {
        return !clusterProps.getProperty("deploy.hashcode.algorithm", "MD5").equals("MD5");
    }

    protected void enableSHA1()
    {
        String toHash;
        String fromHash;
        if(((AbstractButton) (enabledSHA1)).isSelected())
        {
            ((Hashtable) (clusterProps)).put("deploy.hashcode.algorithm", "SHA");
            toHash = "SHA-1";
            fromHash = "MD5";
        } else
        {
            ((Hashtable) (clusterProps)).put("deploy.hashcode.algorithm", "MD5");
            toHash = "MD5";
            fromHash = "SHA-1";
        }
        String message = new String(li.getString("DDConfigWizIDPageEncodeMsg1") + toHash + li.getString("DDConfigWizIDPageEncodeMsg2") + fromHash + li.getString("DDConfigWizIDPageEncodeMsg3"));
        int response = JOptionPane.showConfirmDialog(((java.awt.Component) (super.gui)), ((Object) (message)), li.getString("DDConfigWizIDPageEncodeTitle"), 0);
        Properties wiz = ProcessingPage.getWizardProperties();
        if(response == 0)
        {
            ((Hashtable) (wiz)).put("wizard.convert.vault", ((Object) (toHash)));
        } else
        {
            ((AbstractButton) (enabledSHA1)).setSelected(!isSHA1enabled());
            ((Hashtable) (wiz)).put("wizard.convert.vault", "");
        }
        ProcessingPage.setWizardProperties(wiz);
    }

    protected boolean validateSettings()
    {
        return validateMachineName() && validatePortNumber() && validateServletContext();
    }

    protected boolean validateMachineName()
    {
        boolean error = false;
        InetAddress local = null;
        InetAddress currentMachine[] = null;
        boolean foundHost = false;
        String machineString = ((JTextComponent) (machineName)).getText().trim();
        String hostErrorMessage = "";
        if(acceptedHostName == null || !acceptedHostName.equals(((Object) (machineString))) || !userAcceptedHost)
        {
            try
            {
                local = null;
                local = InetAddress.getLocalHost();
                if(currentMachine == null)
                    currentMachine = InetAddress.getAllByName(machineString);
                for(int i = 0; i < currentMachine.length; i++)
                {
                    if(!currentMachine[i].equals(((Object) (local))) && !currentMachine[i].getHostAddress().equals("127.0.0.1"))
                        continue;
                    foundHost = true;
                    break;
                }

                if(!foundHost)
                {
                    hostErrorMessage = li.getString("DDConfWizIDMachineNotLocalhost") + "\n\t\t\tlocalhost" + "\n\t\t\t" + local.getHostName() + "\n\t\t\t" + local.getHostAddress() + "\n" + li.getString("DDConfWizIDNotExclusive");
                    error = true;
                } else
                {
                    acceptedHostName = machineString;
                    userAcceptedHost = true;
                }
            }
            catch(UnknownHostException e)
            {
                if(local == null)
                {
                    hostErrorMessage = li.getString("DDConfWizIDPageError");
                    error = true;
                } else
                {
                    hostErrorMessage = li.getString("DDConfWizIDMachineNotLocalhost") + "\n      localhost" + "\n      " + local.getHostName() + "\n      " + local.getHostAddress() + "\n" + li.getString("DDConfWizIDNotExclusive");
                    error = true;
                }
            }
            catch(SecurityException se)
            {
                System.err.println(((Object) (se)));
            }
            if(error)
            {
                int response = JOptionPane.showConfirmDialog(((java.awt.Component) (super.gui)), ((Object) (hostErrorMessage + li.getString("DDConfigWizIDPageHostUnknown"))), "Invalid Hostname", 0);
                if(response == 0)
                {
                    acceptedHostName = machineString;
                    userAcceptedHost = true;
                } else
                {
                    return false;
                }
            }
        }
        return true;
    }

    protected boolean validatePortNumber()
    {
        int portNum = -1;
        if(acceptedPort < 0 || acceptedPort != portNum || !userAcceptedPort)
        {
            try
            {
                portNum = Integer.parseInt(((JTextComponent) (portNumber)).getText());
            }
            catch(NumberFormatException e)
            {
                JOptionPane.showMessageDialog(((java.awt.Component) (super.gui)), ((Object) (li.getString("DDConfWizIDPortNotInteger"))), li.getString("DDConfWizIDPort"), 1);
                acceptedPort = -1;
                userAcceptedPort = false;
                return false;
            }
            if(portNum < 1)
            {
                JOptionPane.showMessageDialog(((java.awt.Component) (super.gui)), ((Object) (li.getString("DDConfWizIDPortNegative"))), li.getString("DDConfWizIDPort"), 1);
                acceptedPort = -1;
                userAcceptedPort = false;
                return false;
            }
            if(portNum <= 1000)
            {
                int response = JOptionPane.showConfirmDialog(((java.awt.Component) (super.gui)), ((Object) (li.getString("DDConfWizIDPageAdminPort"))), li.getString("DDConfWizIDPageAdminPortHeader"), 0);
                if(response == 0)
                {
                    acceptedPort = portNum;
                    userAcceptedPort = true;
                    return true;
                } else
                {
                    acceptedPort = -1;
                    userAcceptedPort = false;
                    return false;
                }
            } else
            {
                acceptedPort = portNum;
                userAcceptedPort = true;
                return true;
            }
        } else
        {
            return true;
        }
    }

    protected boolean validateServletContext()
    {
        String adminPageString = ((JTextComponent) (adminPage)).getText();
        if(adminPageString.equals(""))
        {
            JOptionPane.showMessageDialog(((java.awt.Component) (super.gui)), ((Object) (li.getString("DDConfWizIDPageMissing"))), li.getString("DDConfWizIDAdminPage"), 1);
            return false;
        }
        adminPageString = adminPageString.trim();
        if(adminPageString.endsWith("/"))
        {
            adminPageString = adminPageString.substring(0, adminPageString.length() - 1);
            ((JTextComponent) (adminPage)).setText(adminPageString);
        }
        return true;
    }

    public void actionPerformed(ActionEvent e)
    {
        if(((EventObject) (e)).getSource() == leaveAsDefault)
        {
            if(((AbstractButton) (leaveAsDefault)).isSelected())
                resetToDefault();
            setEnabled();
        }
        if(((EventObject) (e)).getSource() == enabledSHA1)
            enableSHA1();
    }

    static 
    {
        li = LocaleInfo.li;
    }
}
