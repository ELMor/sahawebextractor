// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HashcodeConverter.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.platforms.PlatformJRE;
import com.sitraka.deploy.common.platforms.PlatformPlatform;
import com.sitraka.deploy.common.platforms.Platforms;
import com.sitraka.deploy.sam.FileAppVault;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;
import java.util.Vector;

public class HashcodeConverter
{

    protected String errorBundle;
    protected String errorVersion;
    protected String errorJRE;

    protected HashcodeConverter()
    {
        errorBundle = null;
        errorVersion = null;
        errorJRE = null;
    }

    public static HashcodeConverter getInstance()
    {
        return new HashcodeConverter();
    }

    public String errorBundle()
    {
        return errorBundle;
    }

    public String errorVersion()
    {
        return errorVersion;
    }

    public String errorJRE()
    {
        return errorJRE;
    }

    public boolean convert(File vaultLocation, String algorithm)
        throws FileNotFoundException, IOException
    {
        errorBundle = null;
        errorVersion = null;
        Hashcode.setHashcodeAlgorithm(algorithm);
        FileAppVault vault = new FileAppVault();
        vault.setLocation(vaultLocation.getAbsolutePath());
        String bundleList[] = vault.listApplications();
        for(int i = 0; i < bundleList.length; i++)
        {
            String bundleString = bundleList[i];
            bundleString = bundleString.substring(0, bundleString.indexOf(","));
            Application bundle = vault.getApplication(bundleString);
            Vector versionList = bundle.listVersions();
            for(int j = 0; j < versionList.size(); j++)
            {
                Version version = (Version)versionList.elementAt(j);
                boolean result = version.computeFileHashAndSize(new File(vaultLocation, "vault" + File.separator + bundle.getName().toLowerCase()));
                if(!result)
                {
                    errorBundle = bundle.getName();
                    errorVersion = version.getName();
                    return false;
                }
                version.saveXMLToFile(new File(vaultLocation, "vault" + File.separator + bundle.getName().toLowerCase() + File.separator + version.getName().toLowerCase() + File.separator + "version.xml"));
            }

        }

        Platforms platforms = vault.getPlatforms();
        if(!processPlatform(platforms.getRootPlatform(), vault))
        {
            return false;
        } else
        {
            Properties clusterProperties = new Properties();
            File clusterFile = new File(vaultLocation, "cluster.properties");
            clusterProperties.load(((java.io.InputStream) (new FileInputStream(clusterFile))));
            clusterProperties.setProperty("deploy.hashcode.algorithm", algorithm);
            clusterProperties.store(((java.io.OutputStream) (new FileOutputStream(clusterFile))), "");
            return true;
        }
    }

    protected boolean processPlatform(PlatformPlatform platform, FileAppVault vault)
    {
        Vector jreList = platform.getJREList();
        for(int i = 0; i < jreList.size(); i++)
        {
            PlatformJRE jre = (PlatformJRE)jreList.elementAt(i);
            File jreDir = vault.getJREBaseDirectory(platform.getName(), jre.getVendor(), jre.getVersion(), false);
            File prebuiltFile = new File(jreDir, "prebuilt.zip");
            if(prebuiltFile.exists())
            {
                if(!FileUtils.unzipFile(prebuiltFile, jreDir))
                {
                    errorJRE = platform.getName() + "." + jre.getVendor() + "." + jre.getVersion();
                    return false;
                }
                File jreZip = new File(jreDir, "jre.zip");
                if(jreZip.exists())
                {
                    File jreProps = new File(jreDir, "jre.properties");
                    jreProps.delete();
                    prebuiltFile.delete();
                    vault.prebuildJRE(platform.getName(), jre.getVendor(), jre.getVersion());
                } else
                {
                    errorJRE = platform.getName() + "." + jre.getVendor() + "." + jre.getVersion();
                    return false;
                }
            }
        }

        Vector platformList = platform.getPlatformList();
        for(int i = 0; i < platformList.size(); i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platformList.elementAt(i);
            if(!processPlatform(plat, vault))
                return false;
        }

        return true;
    }

    public static void main(String arg[])
    {
        File location = new File(arg[0]);
        String algorithm = arg[1];
        System.out.println("Converting Location: " + location.getAbsolutePath() + " algorithm: " + algorithm);
        HashcodeConverter converter = getInstance();
        try
        {
            if(converter.convert(location, algorithm))
                System.out.println("Successfull Conversion");
            else
                System.out.println("Conversion Failed: " + converter.errorBundle() + " " + converter.errorVersion());
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Vault not found");
            ((Throwable) (e)).printStackTrace();
        }
        catch(IOException e)
        {
            System.out.println("Error in Vault");
            ((Throwable) (e)).printStackTrace();
        }
        System.exit(0);
    }
}
