// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ExternalBrowser.java

package com.sitraka.deploy.sam.config;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ExternalBrowser
{

    private static final String WIN_PATH = "rundll32";
    private static final String WIN_FLAG = "url.dll,FileProtocolHandler";
    private static final String UNIX_PATH = "netscape";
    private static final String UNIX_FLAG = "-remote openURL";
    private static final String WIN_ID = "Windows";

    public ExternalBrowser()
    {
    }

    public static void displayURL(URL url)
    {
        if(url == null)
        {
            return;
        } else
        {
            displayURL(url.toExternalForm());
            return;
        }
    }

    public static void displayURL(String url)
    {
        if(url == null)
            return;
        if(url.startsWith("file"))
            url = convertToLocalFile(url);
        boolean windows = isWindowsPlatform();
        String cmd = null;
        try
        {
            if(windows)
            {
                cmd = "rundll32 url.dll,FileProtocolHandler " + url;
                Process p = Runtime.getRuntime().exec(cmd);
            } else
            {
                cmd = "netscape -remote openURL(" + url + ")";
                Process p = Runtime.getRuntime().exec(cmd);
                try
                {
                    int exitCode = p.waitFor();
                    if(exitCode != 0)
                    {
                        cmd = "netscape " + url;
                        p = Runtime.getRuntime().exec(cmd);
                    }
                }
                catch(InterruptedException x) { }
            }
        }
        catch(IOException x) { }
    }

    public static boolean isWindowsPlatform()
    {
        String os = System.getProperty("os.name");
        return os != null && os.startsWith("Windows");
    }

    private static String convertToLocalFile(String url)
    {
        if(url == null || !url.startsWith("file"))
            return url;
        try
        {
            URL convert = new URL(url);
            String sample = convert.getFile();
            url = sample.replace('|', ':');
            if(isWindowsPlatform())
                for(; url.startsWith("/"); url = url.substring(1));
        }
        catch(MalformedURLException e) { }
        return url;
    }
}
