// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   IntroPage.java

package com.sitraka.deploy.sam.config;

import com.sitraka.deploy.sam.config.resources.LocaleInfo;
import java.awt.Container;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

// Referenced classes of package com.sitraka.deploy.sam.config:
//            ConfigPage, ConfigWizardGUI

public class IntroPage extends ConfigPage
{

    protected static ResourceBundle li;
    public static final String TITLE = "DDConfWizIntroTitle";
    public static final String MAIN_DOCS = "DDConfWizIntroMainDocs";

    public IntroPage(ConfigWizardGUI gui, int buttons)
    {
        super(gui, buttons);
        String img = "/com/sitraka/deploy/sam/config/resources/splashScreen.gif";
        JPanel imagePanel = new JPanel();
        ((Container) (imagePanel)).add(((java.awt.Component) (new JLabel(((javax.swing.Icon) (new ImageIcon(((Object)this).getClass().getResource(img))))))), "Center");
        ((ConfigPage)this).setContentPanel(imagePanel);
        ((ConfigPage)this).setTitle(li.getString("DDConfWizIntroTitle"));
    }

    protected boolean beforeNext()
    {
        return true;
    }

    protected boolean beforePrevious()
    {
        return false;
    }

    protected void refreshPage()
    {
        ((JComponent) (super.next)).requestFocus();
        super.genericPageUpdate();
    }

    protected String getDocs()
    {
        return li.getString("DDConfWizIntroMainDocs");
    }

    static 
    {
        li = LocaleInfo.li;
    }
}
