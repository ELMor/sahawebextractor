// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo.java

package com.sitraka.deploy.sam.config.resources;

import java.util.ListResourceBundle;
import java.util.ResourceBundle;

public class LocaleInfo extends ListResourceBundle
{

    public static ResourceBundle li = ResourceBundle.getBundle("com.sitraka.deploy.sam.config.resources.LocaleInfo");
    public static final Object strings[][] = {
        {
            "DDConfWizPagePrevButton", "Previous"
        }, {
            "DDConfWizPageNextButton", "Next"
        }, {
            "DDConfWizPageCancelButton", "Cancel"
        }, {
            "DDConfWizPageFinishButton", "Finish"
        }, {
            "DDConfWizPageOK", "OK"
        }, {
            "DDConfWizGUIFileNotFound", "DeployDirector''s Configuration Wizard cannot proceed because {0} was not found. \n Re-install DeployDirector for this Configuration Wizard to work."
        }, {
            "DDConfWizGUIFileNotFoundHeader", "File Not Found!"
        }, {
            "DDConfWizGUIValidateErrorHeader", "Invalid Entry"
        }, {
            "DDConfWizGUIConfirmCancel", "Configuration is not complete. Are you sure you would like to exit now?"
        }, {
            "DDConfWizGUIConfirmCancelHeader", "Cancel?"
        }, {
            "DDConfWizGUIPropertyError", "DDConfig.home not set properly in bundle configuration. Configuration Wizard could not be loaded."
        }, {
            "DDConfWizGUIConfigurationError", "Configuration Error"
        }, {
            "DDConfWizNoAdminPageSwitchTitle", "Could not change Admin page!"
        }, {
            "DDConfWizNoAdminPageSwitchMessage", "The directory structure in the standalone directory could not be altered. Admin page will be reset to: {0}"
        }, {
            "DDConfWizEmailTitle", "Administrator Email"
        }, {
            "DDConfWizEmailSourceTitle", "Source Configuration"
        }, {
            "DDConfWizEmailRecipientTitle", "Recipient Configuration"
        }, {
            "DDConfWizEmailDisableEmail", "Disable Administrator Email"
        }, {
            "DDConfWizEmailUserAccount", "User Email Address"
        }, {
            "DDConfWizEmailPassword", "Password"
        }, {
            "DDConfWizEmailNotification", "Notification Level"
        }, {
            "DDConfiWizEmailTest", "Test Configuration"
        }, {
            "DDConfWizEmailHost", "Host"
        }, {
            "DDConfWizEmailAdminAccount", "Administrator Email Address"
        }, {
            "DDConfWizEmailContinue", "Continue?"
        }, {
            "DDConfWizEmailInvalidAdminAccount", "Invalid Administrator Account"
        }, {
            "DDConfWizEmailInvalidUserAccount", "Invalid User Account"
        }, {
            "DDConfWizEmailHostNotFound", "The supplied mail host could not be found on the network. Do you wish to continue?"
        }, {
            "DDConfWizEmailValidationError", "There is a validation error."
        }, {
            "DDConfWizEmailNotificationUnselected", "No notification levels have been selected.  Do you wish to continue?"
        }, {
            "DDConfWizEmailAdminAccountRequired", "Administrator account is required."
        }, {
            "DDConfWizEmailUserAccountRequired", "User account is required."
        }, {
            "DDConfWizEmailMailHostRequired", "Mail host required."
        }, {
            "DDConfWizEmailNoPassword", "No password for the Administrator's account was provided.  Do you wish to continue?"
        }, {
            "DDConfWizEmailUntestedConfig", "This configuration is untested.  Do you wish to continue?"
        }, {
            "DDConfWizEmailTestFailed", "The test failed due to the following reason:"
        }, {
            "DDConfWizEmailEmailSubject", "DeployDirector Administrator Emailing"
        }, {
            "DDConfWizEmailEmailContext", "This email address has been supplied by the DeployDirector Administrator as a recipient of DeployDirector's error email.\n\nPlease contact the sender of this email for more information."
        }, {
            "DDConfWizEmailError", "Error"
        }, {
            "DDConfWizEmailEmailDisabled", "Administrator emailing is currently disabled."
        }, {
            "DDConfWizEmailMainDoc", "A useful feature of DeployDirector is its error emailing ability. When an error occurs on the client or server side, an email will be sent to all appropriate recipients, based on the Notification Levels of the recipients.\n\nThis email is sent from the supplied Administrator's Account through the provided SMTP mail host.  Please note that depending on the configuration mail host, the Administrator's password may be required.\n\nOnce the Source of the emails is properly configured, you may specify a recipient for the emails.  The recipient's email address must be provided, and the desired Notification Levels selected.\n\nOnce you have the initial configuration that you desire, please test it via the \"Test Configuration\" button.  It will attempt to send a confirmation email to the recipient from the Administrator to verify your settings.  In the event that it fails for a known reason, the error or exception will be displayed to you.  Also, if you have not attempted to test your configuration, you will be prompted to do so before proceeding with this Wizard, because while it is not mandatory, it is advised."
        }, {
            "DDConfWizExitTitle", "Configuration Complete"
        }, {
            "DDConfWizExitFinishedConf", "Finished Configuring DeployDirector."
        }, {
            "DDConfWizExitPressFinish", "Press 'Finish' to accept the new configuration."
        }, {
            "DDConfWizExitStartServerQuestion", "Would you like to have the default server started now?"
        }, {
            "DDConfWizExitStartServer", "Start Server?"
        }, {
            "DDConfWizExitPleaseWait", "Please Wait"
        }, {
            "DDConfWizExitWaitAFewMinutes", "This may take a few minutes."
        }, {
            "DDConfWizExitStartingServer", "Starting the server. When the server has started an empty console may appear."
        }, {
            "DDConfWizExitPageMainDocs", "Congratulations, DeployDirector has been configured to run the server-side component on this machine. Select finish to finalize the configuration.  The server may be started using the installed shortcuts.\n\nOnce the server is started, you may go to the Remote Administrator Pages.  If you have not modified the defaults for this server, the Remote Administrator Pages can be found at http://localhost:8080/servlet/deploy/admin.  The default user name is \"ddadmin\" and the default password is \"f3nd3r\"."
        }, {
            "DDConfWizJDKTitle", "JDK Selection"
        }, {
            "DDConfWizJDKUpdate", "Update"
        }, {
            "DDConfWizJDKLocation", "Location"
        }, {
            "DDConfWizJDKNote", "Enter the location of the JDK to use for the DeployDirector server."
        }, {
            "DDConfWizJDKSearch", "JDKs found on system"
        }, {
            "DDConfWizJDKError", "Invalid JDK Selection."
        }, {
            "DDConfWizJDKReselect", "The selected directory does not contain a valid JDK.  Please choose another."
        }, {
            "DDConfWizJDKDoc", "If you know where a supported JDK is installed on your system please enter it in the \"Location\" field.\n\nIf you are unsure please click the \"Update\" button and a search for supported JDKs will be performed on your system and the results displayed to you.  Please note that if you are running on a Unix based system this search can take several minutes.\n\nChoose a supported JDK from the list and click \"Next>>\"."
        }, {
            "DDConfWizIDTitle", "Identification of Server"
        }, {
            "DDConfWizIDMachineName", "Machine name"
        }, {
            "DDConfWizIDUseDefaults", "Use Default Settings"
        }, {
            "DDConfWizIDPort", "Port"
        }, {
            "DDConfWizIDAdminPage", "Admin Page"
        }, {
            "DDConfWizIDProtocol", "Protocol"
        }, {
            "DDConfWizIDMachineMissing", "Machine name is required."
        }, {
            "DDConfWizIDPortMissing", "Port is required."
        }, {
            "DDConfWizIDPageMissing", "Admin Page is required."
        }, {
            "DDConfWizIDPageServerConfig", "Server Configuration"
        }, {
            "DDConfWizIDPageAdvancedConfig", "Advanced Server Configuration"
        }, {
            "DDConfWizIDPageSHA1Enable", "Enable SHA-1 encoding?"
        }, {
            "DDConfigWizIDPageGroupAdmin", "Enable Group Role Administration?"
        }, {
            "DDConfWizIDProtocolMissing", "Protocol is required."
        }, {
            "DDConfWizIDPortNotInteger", "Port must be an integer."
        }, {
            "DDConfWizIDPortNegative", "Port number must be > 0"
        }, {
            "DDConfWizIDInvalidPage", "First character of Admin Page must be '/' and must be at least two characters long."
        }, {
            "DDConfWizIDMachineNotLocalhost", "(IP Validation)Machine supplied is not localhost. Valid machine names are:"
        }, {
            "DDConfWizIDNotExclusive", "Note: This is not an exclusive list."
        }, {
            "DDConfWizIDMachineNotFound", "Machine not found on network! Valid machine names include: "
        }, {
            "DDConfWizUnknownHost", "This machine could not be found on the network. Check your network connection and configuration. This configuration wizard requires that your machine be networked and thus, cannot continue. Run this wizard again after establishing a network connection."
        }, {
            "DDConfWizIDPageError", "Network Failure!"
        }, {
            "DDConfWizIDPageAdminPort", "Administrator access will be required to start the server on this port. Continue and use this port?"
        }, {
            "DDConfWizIDPageAdminPortHeader", "Administrator Port (all ports below 1000)"
        }, {
            "DDConfigWizIDPageEncodeMsg1", "You are about to convert this instance of the DeployDirector Server to use "
        }, {
            "DDConfigWizIDPageEncodeMsg2", " encoding instead of "
        }, {
            "DDConfigWizIDPageEncodeMsg3", " encoding.\n\nThis is a very lengthy procedure and it is not recomended\nunless there are legitimate security reasons for doing so.\n\nDo you wish to continue?"
        }, {
            "DDConfigWizIDPageEncodeTitle", "Security Warning"
        }, {
            "DDConfigWizIDPageHostUnknown", "\nWould you like to continue with this name anyway?"
        }, {
            "DDConfWizIDPageMainDocs", "Welcome to the server identification page. This information should correspond to the machine that you are currently working on, since it should be the intended server. \n\nThe machine name that you enter should be registered with the network to this machine. \n\nUse the default settings if you wish to use the web server supplied with DeployDirector. Otherwise customize the information to the web server that you intend to run. \n\nClick \"Next>>\" after entering your server information."
        }, {
            "DDConfWizIntroTitle", "Welcome to DeployDirector's Configuration Wizard"
        }, {
            "DDConfWizIntroMainDocs", "This wizard will guide you through the configuration that suits your deployment needs. It's as easy as clicking through the pages and filling in the fields.\n\nOnce completed, please start the server and surf to the Remote Administrator Pages."
        }, {
            "DDConfWizLicenseSignatureMissing", "License signature is invalid"
        }, {
            "DDConfWizLicenseError", "Error loading license."
        }, {
            "License", "License"
        }, {
            "DDConfWizLicenseSerialNumber", "Serial Number"
        }, {
            "DDConfWizLicenseExpiry", "Expiry"
        }, {
            "DDConfWizLicenseBundles", "Bundles"
        }, {
            "DDConfWizLicenseClients", "Clients"
        }, {
            "DDConfWizLicenseHosts", "Hosts"
        }, {
            "DDConfWizLicenseApplications", "Applications"
        }, {
            "DDConfWizLicenseSignature", "Signature"
        }, {
            "DDConfWizLicenseType", "Type"
        }, {
            "DDConfWizLicenseDDVersion", "License Version"
        }, {
            "DDConfWizLicenseVersion", "Sitraka License Version"
        }, {
            "DDConfWizLicenseNotApplicable", "Not Applicable"
        }, {
            "DDConfWizLicenseAddLicense", "Add License File"
        }, {
            "DDConfWizLicenseReplaceLicense", "Replace License File"
        }, {
            "DDConfWizLicenseBuyLicense", "Buy Now"
        }, {
            "DDConfWizLicenseObtainLicense", "Obtain License Now"
        }, {
            "DDConfWizLicenseProductSite", "Product Site"
        }, {
            "DDConfWizLicneseError", "Error"
        }, {
            "DDConfWizLicenseErrorReading", "Error reading license from file"
        }, {
            "DDConfWizLicenseErrorFNFE", "License File could not be found."
        }, {
            "DDConfWizLicenseErrorCorrupt", "This license file is either invalid or corrupt.  Please contact direct@sitraka.com"
        }, {
            "DDConfWizLicenseNotAvailable", "Not Available"
        }, {
            "DDConfWizLicenseContinueWithInvalid", "The current license is invalid. DeployDirector will not operate without a valid license. Do you wish to continue?"
        }, {
            "DDConfWizLicenseInvalid", "Invalid License"
        }, {
            "DDConfWizLicensePageMainDocs", "If you have a new license to enter, it is easiest to allow the Configuration Wizard to read it directly from a file.  If you do not have a license, please use the \"Obtain License Nile\" button to recieve an evaluation license.  If you have an evaluation license, you can use the \"Buy Now\" option to contact our Sales Representitives."
        }, {
            "DDConfWizCoversionTitle", "Configuration Summary"
        }, {
            "DDConfWizCoversionError", "Configuration Error"
        }, {
            "DDConfWizCoversionErrorTitle", "An error has occured:"
        }, {
            "DDConfWizRecoveryMessage", "This wizard will now attempt to set the installation to its original configuration."
        }, {
            "DDConfWizPPJDKLabel", "Selected JDK:"
        }, {
            "DDConfWizPPServerLabel", "Server Configuration:"
        }, {
            "DDConfWizPPLicenseLabel", "License:"
        }, {
            "DDConfWizPPEmailLabel", "Email Settings:"
        }, {
            "DDConfWizPPSerialSublabel", "Serial Number:"
        }, {
            "DDConfWizPPHostsSublabel", "Hosts:"
        }, {
            "DDConfWizPPBundlesSublabel", "Bundles:"
        }, {
            "DDConfWizPPClientsSublabel", "Clients:"
        }, {
            "DDConfWizPPAppSublabel", "Applications:"
        }, {
            "DDConfWizPPExpirySublabel", "Expiry:"
        }, {
            "DDConfWizPPProductSublabel", "Product:"
        }, {
            "DDConfWizPPTypeSublabel", "Type:"
        }, {
            "DDConfWizPPLicenseSublabel", "License Version:"
        }, {
            "DDConfWizPPSitrakaVersion", "Sitraka License Version:"
        }, {
            "DDConfWizPPErrorEmail", "Error Emailing Enabled"
        }, {
            "DDConfWizPPMailHost", "Mail Server:"
        }, {
            "DDConfWizPPAdministratorEmail", "Administrators Email Address:"
        }, {
            "DDConfWizPPRecipientEmail", "Recipients Email Address:"
        }, {
            "DDConfWizPPNotApplicable", "Not Applicable"
        }, {
            "DDConfWizCoversionDocs", "Please review the settings that you have choosen for this installation of DeployDirector.  If you desire to make any changes, please do so now.\n\nWhen you are satisfied with your choices, click \"Next>>\" to initiate the configuration."
        }
    };

    public LocaleInfo()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
