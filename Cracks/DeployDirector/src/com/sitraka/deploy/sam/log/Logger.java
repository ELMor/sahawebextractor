// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Logger.java

package com.sitraka.deploy.sam.log;

import java.beans.BeanInfo;
import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            LogException, FileDatabase, JDBCDatabase, Database, 
//            Field

public abstract class Logger
{

    protected StringBuffer workingBuffer;
    protected static DecimalFormat decimalFormat = new DecimalFormat("#.#");
    protected String serverID;
    protected Field fields[];
    protected Database database;

    public Logger(String server_id)
    {
        workingBuffer = new StringBuffer(30);
        serverID = null;
        serverID = server_id;
    }

    protected void init(Field fields[], Database database)
    {
        this.fields = fields;
        this.database = database;
    }

    protected void log(Object fields[])
        throws LogException
    {
        database.log(fields);
    }

    protected void logObject(Object o)
        throws LogException
    {
        log(getValuesFromObject(o));
    }

    protected void write(Object fields[])
        throws LogException
    {
        database.write(fields);
    }

    protected void writeObject(Object o)
        throws LogException
    {
        write(getValuesFromObject(o));
    }

    protected Object[] getValuesFromObject(Object o)
    {
        Class c = o.getClass();
        Object values[] = new Object[fields.length];
        BeanInfo info = null;
        try
        {
            info = Introspector.getBeanInfo(c);
        }
        catch(Exception ex)
        {
            System.out.println("\tERROR: Introspection Error");
        }
        PropertyDescriptor properties[] = info.getPropertyDescriptors();
        Object value = null;
        for(int i = 0; i < fields.length;)
        {
            Method read_method = null;
            for(int j = 0; j < properties.length; j++)
                if(fields[i].name.equalsIgnoreCase(((FeatureDescriptor) (properties[j])).getName()))
                    read_method = properties[j].getReadMethod();

            if(read_method == null)
            {
                System.out.println("Can't patch property");
                break;
            }
            try
            {
                value = read_method.invoke(o, ((Object []) (null)));
                break;
            }
            catch(Exception ex)
            {
                System.out.println("ERROR while invoking read method");
                values[i] = value;
                i++;
            }
        }

        return values;
    }

    public static Object[] textToRecord(Field fields[], String text)
    {
        Vector v = parseCSVLine(text);
        if(v.size() < fields.length)
            throw new NullPointerException("Not enough fields in transfer record (" + v.size() + "<" + fields.length + ")\n original text = <" + text + ">");
        Object values[] = new Object[fields.length];
        for(int i = 0; i < fields.length; i++)
        {
            String token = (String)v.elementAt(i);
            if(fields[i].javaType == (java.lang.String.class))
                values[i] = ((Object) (token));
            else
            if(fields[i].javaType == (java.sql.Timestamp.class))
                values[i] = ((Object) (Timestamp.valueOf(token)));
            else
            if(fields[i].javaType == (java.lang.Float.class))
            {
                Number f = null;
                try
                {
                    f = ((NumberFormat) (decimalFormat)).parse(token);
                }
                catch(ParseException pe) { }
                if(f instanceof Float)
                    values[i] = ((Object) (f));
                else
                    values[i] = ((Object) (new Float(f.floatValue())));
            } else
            if(fields[i].javaType == (java.lang.Long.class))
            {
                Number l = null;
                try
                {
                    l = ((NumberFormat) (decimalFormat)).parse(token);
                }
                catch(ParseException pe) { }
                if(l instanceof Long)
                    values[i] = ((Object) (l));
                else
                    values[i] = ((Object) (new Long(l.longValue())));
            } else
            {
                throw new IllegalArgumentException(values[i] + " is not of a supported data type");
            }
        }

        return values;
    }

    protected static Vector parseCSVLine(String str)
    {
        Vector v = new Vector();
        StringTokenizer parser = new StringTokenizer(str, ",\"", true);
        String token = null;
        String new_delimiter = null;
        boolean look_for_matching_quote = false;
        boolean last_token_was_separator = false;
        try
        {
            while(parser.hasMoreTokens()) 
            {
                if(new_delimiter != null)
                {
                    token = parser.nextToken(new_delimiter);
                    new_delimiter = null;
                } else
                {
                    token = parser.nextToken();
                }
                if(token.equals(","))
                {
                    if(last_token_was_separator)
                        v.addElement("");
                    last_token_was_separator = true;
                } else
                {
                    last_token_was_separator = false;
                    if(token.equals("\""))
                    {
                        if(look_for_matching_quote)
                        {
                            new_delimiter = ",\"";
                            look_for_matching_quote = false;
                        } else
                        {
                            new_delimiter = "\"";
                            look_for_matching_quote = true;
                        }
                    } else
                    {
                        v.addElement(((Object) (token)));
                    }
                }
            }
            if(last_token_was_separator)
                v.addElement(((Object) (null)));
        }
        catch(NoSuchElementException e) { }
        return v;
    }

    protected boolean accept(Object incoming_record[])
        throws LogException
    {
        return !ourRecord(incoming_record);
    }

    protected boolean select(Object record[], Timestamp from, Timestamp to)
        throws LogException
    {
        if(record[0] == null)
            return false;
        if(!(record[0] instanceof Timestamp))
            return false;
        if(!ourRecord(record))
            return false;
        if(((Date) ((Timestamp)record[0])).getTime() < ((Date) (from)).getTime())
            return false;
        if(to == null)
            return true;
        return ((Date) ((Timestamp)record[0])).getTime() <= ((Date) (to)).getTime();
    }

    protected boolean ourRecord(Object record[])
    {
        Object element = record[1];
        if(element == null)
            return false;
        else
            return element.equals(((Object) (getServerID())));
    }

    public abstract String getName();

    public void receiveLog(InputStream input_stream)
        throws LogException
    {
        BufferedReader reader = new BufferedReader(((java.io.Reader) (new InputStreamReader(input_stream))));
        String line = null;
        try
        {
            while((line = reader.readLine()) != null) 
            {
                Object record[] = textToRecord(fields, line);
                if(accept(record))
                    write(record);
            }
        }
        catch(IOException ioe)
        {
            throw new LogException("read error", ((Exception) (ioe)));
        }
    }

    public void sendLog(OutputStream output_stream, Timestamp from, Timestamp to)
        throws LogException
    {
        Vector records = database.getRecords();
        PrintWriter print_writer = new PrintWriter(output_stream, true);
        for(Enumeration e = records.elements(); e.hasMoreElements();)
        {
            Object record[] = (Object[])e.nextElement();
            if(select(record, from, to))
                database.writePortableEntry(print_writer, record);
        }

    }

    public void saveDatabaseToFile()
        throws LogException
    {
        ((FileDatabase)database).saveDatabaseToFile();
    }

    public boolean saveDatabaseToFile(File file)
        throws LogException
    {
        return ((JDBCDatabase)database).saveDatabaseToFile(file);
    }

    public String getServerID()
    {
        return serverID;
    }

    public void dispose()
        throws LogException
    {
        database.dispose();
    }

    public Field[] getFields()
    {
        return fields;
    }

}
