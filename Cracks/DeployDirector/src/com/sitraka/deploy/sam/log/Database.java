// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Database.java

package com.sitraka.deploy.sam.log;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            LogException, Field, LogLimit

public abstract class Database
{

    public static final int LESS_THAN = 1;
    public static final int LESS_THAN_EQUAL_TO = 2;
    public static final int EQUAL_TO = 3;
    public static final int GREATER_THAN = 4;
    public static final int GREATER_THAN_EQUAL_TO = 5;
    public static final int NOT_EQUALS = 6;

    public Database()
    {
    }

    public abstract void init(Field afield[], String s, int ai[], int ai1[], LogLimit loglimit)
        throws LogException;

    public abstract int[] getKeyColumnIndices();

    public abstract boolean[] getUpdateColumnIndices();

    public abstract Field[] getFields();

    public abstract int getLogCount()
        throws LogException;

    public abstract void compactLog(int i, int j, int k)
        throws LogException;

    public abstract void compactLog(Timestamp timestamp, int i)
        throws LogException;

    public abstract void compactDatabase(int i, int j, int k)
        throws LogException;

    public abstract void compactDatabase(Timestamp timestamp, int i)
        throws LogException;

    public abstract int getUniqueRecordCount(int i, Timestamp timestamp, int j)
        throws LogException;

    public abstract void log(Object aobj[])
        throws LogException;

    public abstract void dispose()
        throws LogException;

    public abstract void write(Object aobj[])
        throws LogException;

    public abstract Object[] getRecord(int ai[], int ai1[], Object aobj[])
        throws LogException;

    public abstract Vector getRecords(int ai[], int ai1[], Object aobj[])
        throws LogException;

    public abstract int getRecordCount(int ai[], int ai1[], Object aobj[])
        throws LogException;

    public abstract Vector getRecords()
        throws LogException;

    public abstract int getRecordCount()
        throws LogException;

    public boolean[] buildUpdateIndices(int length, int ignore_on_update_indices[])
    {
        boolean updateIndices[] = new boolean[length];
        for(int i = 0; i < length; i++)
            updateIndices[i] = true;

        for(int i = 0; i < ignore_on_update_indices.length; i++)
            updateIndices[ignore_on_update_indices[i]] = false;

        return updateIndices;
    }

    public Object[] getRecord(Object record[])
        throws LogException
    {
        int key_indices[] = getKeyColumnIndices();
        int comparators[] = new int[key_indices.length];
        Object key_values[] = new Object[key_indices.length];
        for(int i = 0; i < key_indices.length; i++)
        {
            comparators[i] = 3;
            key_values[i] = record[key_indices[i]];
        }

        return getRecord(comparators, key_indices, key_values);
    }

    public Object[] getRecord(int comparator, int compare_field, Object compare_value)
        throws LogException
    {
        int comparators[] = {
            comparator
        };
        int compare_fields[] = {
            compare_field
        };
        Object compare_values[] = {
            compare_value
        };
        return getRecord(comparators, compare_fields, compare_values);
    }

    public Vector getRecords(int comparator, int compare_field, Object compare_value)
        throws LogException
    {
        int comparators[] = {
            comparator
        };
        int compare_fields[] = {
            compare_field
        };
        Object compare_values[] = {
            compare_value
        };
        return getRecords(comparators, compare_fields, compare_values);
    }

    public int getRecordCount(int comparator, int compare_field, Object compare_value)
        throws LogException
    {
        int comparators[] = {
            comparator
        };
        int compare_fields[] = {
            compare_field
        };
        Object compare_values[] = {
            compare_value
        };
        return getRecordCount(comparators, compare_fields, compare_values);
    }

    public void writePortableEntry(PrintWriter writer, Object values[])
    {
        Field fields[] = getFields();
        for(int i = 0; i < values.length; i++)
        {
            String s;
            if(values[i] == null)
                s = "";
            else
            if(fields[i].javaType == (java.lang.String.class))
                s = convertToCSVString((String)values[i]);
            else
            if(fields[i].javaType == (java.sql.Timestamp.class))
            {
                if(values[i] == null)
                    s = "";
                else
                    s = values[i].toString();
            } else
            if(fields[i].javaType == (java.lang.Float.class))
            {
                if(values[i] == null)
                    s = "0";
                else
                    s = values[i].toString();
            } else
            if(fields[i].javaType == (java.lang.Long.class))
            {
                if(values[i] == null)
                    s = "0";
                else
                    s = values[i].toString();
            } else
            {
                throw new IllegalArgumentException(fields[i] + " is not of a supported data type");
            }
            writer.print(s);
            if(i != fields.length - 1)
                writer.print(",");
        }

        writer.println();
    }

    public String convertToCSVString(String s)
    {
        if(s.indexOf('\n') >= 0 || s.indexOf(',') >= 0 || s.indexOf('"') >= 0 || s.indexOf('\t') >= 0 || s.indexOf('\r') >= 0)
        {
            StringBuffer buffer = new StringBuffer(s);
            int output = 0;
            buffer.setCharAt(output, '"');
            output++;
            for(int input = 0; input < s.length(); input++)
            {
                buffer.setLength(output + 3);
                if(s.charAt(input) == '\n')
                {
                    buffer.setCharAt(output, '\\');
                    output++;
                    buffer.setCharAt(output, 'n');
                } else
                {
                    if(s.charAt(input) == '\r')
                        continue;
                    if(s.charAt(input) == '\t')
                    {
                        buffer.setLength(buffer.length() + 3);
                        buffer.setCharAt(output, ' ');
                        output++;
                        buffer.setCharAt(output, ' ');
                        output++;
                        buffer.setCharAt(output, ' ');
                        output++;
                        buffer.setCharAt(output, ' ');
                    } else
                    if(s.charAt(input) == '"')
                    {
                        buffer.setCharAt(output, '"');
                        output++;
                        buffer.setCharAt(output, '"');
                    } else
                    {
                        buffer.setCharAt(output, s.charAt(input));
                    }
                }
                output++;
            }

            buffer.setCharAt(output, '"');
            output++;
            buffer.setLength(output);
            s = new String(buffer);
        }
        return s;
    }
}
