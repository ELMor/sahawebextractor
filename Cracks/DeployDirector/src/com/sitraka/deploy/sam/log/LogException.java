// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogException.java

package com.sitraka.deploy.sam.log;


public class LogException extends Exception
{

    protected Exception wrappedException;

    public LogException(String message, Exception wrapped_exception)
    {
        super(message);
        wrappedException = null;
        wrappedException = wrapped_exception;
    }

    public Exception getWrappedException()
    {
        return wrappedException;
    }

    public String toString()
    {
        return ((Throwable)this).toString() + ((Throwable) (wrappedException)).toString();
    }
}
