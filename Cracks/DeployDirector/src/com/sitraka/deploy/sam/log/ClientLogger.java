// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClientLogger.java

package com.sitraka.deploy.sam.log;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            BaseLogger, Field, LogStats, LogException, 
//            Logger, Database, LogLimit

public class ClientLogger extends BaseLogger
{

    public static final String TABLE = "DDClientLog";
    public static final int TIMESTAMP_ID = 0;
    public static final int SERVER_ID = 1;
    public static final int EVENT = 2;
    public static final int CLIENT_ID = 3;
    public static final int USER_ID = 4;
    public static final int BUNDLE_NAME = 5;
    public static final int BUNDLE_VERSION = 6;
    public static final int NOTES = 7;
    public Field FIELDS[];
    public static final int KEY_COLUMNS[] = {
        0, 1
    };
    public static final int IGNORE_ON_UPDATE_COLUMNS[] = new int[0];
    public Object values[];

    public ClientLogger(String server_id, Database database, LogLimit logLimit)
        throws LogException
    {
        super(server_id);
        FIELDS = (new Field[] {
            new Field("Timestamp", "TS", java.sql.Timestamp.class, "DATETIME", 23), new Field("Server ID", "ServerID", java.lang.String.class, "VARCHAR(64)", 10), new Field("Event", "Event", java.lang.String.class, "VARCHAR(64)", 20), new Field("Client ID", "ClientID", java.lang.String.class, "VARCHAR(64)", 10), new Field("User ID", "UserID", java.lang.String.class, "VARCHAR(64)", 10), new Field("Bundle Name", "BundleName", java.lang.String.class, "VARCHAR(64)", 10), new Field("Bundle Version", "BundleVersion", java.lang.String.class, "VARCHAR(64)", 12), new Field("Notes", "Notes", java.lang.String.class, "VARCHAR(255)", 30)
        });
        values = new Object[FIELDS.length];
        ((Logger)this).init(FIELDS, database);
        database.init(FIELDS, "DDClientLog", KEY_COLUMNS, IGNORE_ON_UPDATE_COLUMNS, logLimit);
    }

    public String getName()
    {
        return "DDClientLog";
    }

    public static String getFileName()
    {
        return "DDClientLog";
    }

    public void log(String event, String client_id, String user_id, String app_name, String version_id, String notes)
        throws LogException
    {
        log(new Timestamp(System.currentTimeMillis()), event, client_id, user_id, app_name, version_id, notes);
    }

    public synchronized void log(Timestamp ts, String event, String client_id, String user_id, String app_name, String version_id, String notes)
        throws LogException
    {
        values[0] = ((Object) (ts));
        values[1] = ((Object) (((Logger)this).getServerID()));
        values[2] = ((Object) (event));
        values[3] = ((Object) (client_id));
        values[4] = ((Object) (user_id));
        values[5] = ((Object) (app_name));
        values[6] = ((Object) (version_id));
        values[7] = ((Object) (notes));
        ((Logger)this).log(values);
    }

    public LogStats queryLogStats(String bundles[], Timestamp startTime, Timestamp endTime)
        throws LogException
    {
        Vector records = null;
        Vector eventNames = new Vector();
        Vector eventCount = new Vector();
        records = ((Logger)this).database.getRecords(new int[] {
            1, 4
        }, new int[] {
            0, 0
        }, new Object[] {
            startTime, endTime
        });
        if(records != null)
        {
            for(int i = 0; i < records.size(); i++)
            {
                Object record[] = (Object[])records.elementAt(i);
                String event = (String)record[2];
                String bundle = (String)record[5];
                if(bundles != null)
                {
                    boolean match = false;
                    for(int j = 0; j < bundles.length; j++)
                    {
                        if(!bundle.equalsIgnoreCase(bundles[j]))
                            continue;
                        match = true;
                        break;
                    }

                    if(!match)
                        continue;
                }
                int index = eventNames.indexOf(((Object) (event)));
                if(index == -1)
                {
                    eventNames.addElement(((Object) (event)));
                    eventCount.addElement(((Object) (new Integer(1))));
                } else
                {
                    Integer num = (Integer)eventCount.elementAt(index);
                    num = new Integer(num.intValue() + 1);
                    eventCount.setElementAt(((Object) (num)), index);
                }
            }

        }
        return new LogStats(eventNames, eventCount);
    }

    public long[] queryDownloadTimes(String bundle, String fromVersion, String toVersion, long maximumAge)
        throws LogException
    {
        Vector records = null;
        Vector times = new Vector();
        Timestamp limitAge;
        if(maximumAge > 0L)
            limitAge = new Timestamp(System.currentTimeMillis() - maximumAge);
        else
            limitAge = new Timestamp(0L);
        records = ((Logger)this).database.getRecords(new int[] {
            1, 3
        }, new int[] {
            0, 5
        }, new Object[] {
            limitAge, bundle
        });
        HashMap clients = new HashMap();
        for(int i = 0; i < records.size(); i++)
        {
            Object record[] = (Object[])records.elementAt(i);
            String version = (String)record[6];
            String clientID = (String)record[3];
            String event = (String)record[2];
            if((version.equalsIgnoreCase(toVersion) || version.equals(((Object) (fromVersion)))) && ("ClientBundleUpdateStarted".equals(((Object) (event))) || "ClientBundleUpdateComplete".equals(((Object) (event)))))
            {
                Vector clientRecords = (Vector)clients.get(((Object) (clientID)));
                if(clientRecords == null)
                {
                    clientRecords = new Vector();
                    clients.put(((Object) (clientID)), ((Object) (clientRecords)));
                }
                clientRecords.add(((Object) (record)));
            }
        }

        for(Iterator clientLoop = clients.keySet().iterator(); clientLoop.hasNext();)
        {
            Object key = clientLoop.next();
            Vector recordList = (Vector)clients.get(key);
            for(int i = 0; i < recordList.size(); i++)
            {
                Object record[] = (Object[])recordList.elementAt(i);
                String event = (String)record[2];
                if(!"ClientBundleUpdateStarted".equals(((Object) (event))))
                    continue;
                String recordFromVersion = (String)record[6];
                if(!recordFromVersion.equals(((Object) (fromVersion))))
                    continue;
                Timestamp fromTime = (Timestamp)record[0];
                if(i >= recordList.size())
                    break;
                i++;
                record = (Object[])recordList.elementAt(i);
                event = (String)record[2];
                if(!"ClientBundleUpdateComplete".equals(((Object) (event))))
                {
                    i--;
                } else
                {
                    String recordToVersion = (String)record[6];
                    if(recordToVersion.equals(((Object) (toVersion))))
                    {
                        Timestamp toTime = (Timestamp)record[0];
                        long updateTime = ((Date) (toTime)).getTime() - ((Date) (fromTime)).getTime();
                        if(updateTime > 0L)
                            times.add(((Object) (new Long(updateTime))));
                    }
                }
            }

        }

        long timeArray[] = new long[times.size()];
        for(int i = 0; i < timeArray.length; i++)
            timeArray[i] = ((Long)times.elementAt(i)).longValue();

        Arrays.sort(timeArray);
        return timeArray;
    }

}
