// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClientDataLogger.java

package com.sitraka.deploy.sam.log;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.log:
//            Logger, Field, LogException, Database, 
//            LogLimit

public class ClientDataLogger extends Logger
{

    public static final String TABLE = "DDClients";
    public static final int ACTIVE_DAYS_WINDOW = 30;
    public static final int SERVER_ID = 0;
    public static final int CLIENT_ID = 1;
    public static final int USER_ID = 2;
    public static final int BUNDLE_NAME = 3;
    public static final int BUNDLE_VERSION = 4;
    public static final int LAST_CONNECTION = 5;
    public static final int LAST_CLIENT_IP = 6;
    public static final int INITIAL_VERSION = 7;
    public static final int INITIAL_USER_ID = 8;
    public static final int INSTALL_DATE = 9;
    public Field FIELDS[];
    public static final int KEY_COLUMNS[] = {
        1, 3
    };
    public static final int IGNORE_ON_UPDATE_COLUMNS[] = {
        7, 8, 9
    };
    public Object values[];

    public ClientDataLogger(String server_id, Database database, LogLimit logLimit)
        throws LogException
    {
        super(server_id);
        FIELDS = (new Field[] {
            new Field("Server ID", "ServerID", java.lang.String.class, "VARCHAR(64)", 10), new Field("Client ID", "ClientID", java.lang.String.class, "VARCHAR(64)", 10), new Field("User ID", "UserID", java.lang.String.class, "VARCHAR(64)", 10), new Field("Bundle Name", "BundleName", java.lang.String.class, "VARCHAR(64)", 12), new Field("Bundle Version", "BundleVersion", java.lang.String.class, "VARCHAR(64)", 10), new Field("Last Connection", "LastConnection", java.sql.Timestamp.class, "DATETIME", 23), new Field("Last Client IP", "LastClientIP", java.lang.String.class, "VARCHAR(32)", 10), new Field("Initial Version", "InitialVersion", java.lang.String.class, "VARCHAR(64)", 10), new Field("Initial User ID", "InitialUserID", java.lang.String.class, "VARCHAR(64)", 10), new Field("Install Date", "InstallDate", java.sql.Timestamp.class, "DATETIME", 23)
        });
        values = new Object[FIELDS.length];
        ((Logger)this).init(FIELDS, database);
        database.init(FIELDS, "DDClients", KEY_COLUMNS, IGNORE_ON_UPDATE_COLUMNS, logLimit);
    }

    public String getName()
    {
        return "DDClients";
    }

    public static String getFileName()
    {
        return "DDClients";
    }

    public synchronized void write(String client_id, String user_id, String bundle_name, String bundle_version, Timestamp last_connection, String last_client_IP, String initial_version, 
            String initial_user_id, Timestamp install_date)
        throws LogException
    {
        if(bundle_name.equalsIgnoreCase("DDCAM"))
            return;
        if(client_id.indexOf('@') == -1)
        {
            return;
        } else
        {
            values[0] = ((Object) (((Logger)this).getServerID()));
            values[1] = ((Object) (client_id));
            values[2] = ((Object) (user_id));
            values[3] = ((Object) (bundle_name));
            values[4] = ((Object) (bundle_version));
            values[5] = ((Object) (last_connection));
            values[6] = ((Object) (last_client_IP));
            values[7] = ((Object) (initial_version));
            values[8] = ((Object) (initial_user_id));
            values[9] = ((Object) (install_date));
            ((Logger)this).write(values);
            return;
        }
    }

    public static int getTimestampIndex()
    {
        return 5;
    }

    public static int getUserIDIndex()
    {
        return 2;
    }

    public static int getClientIDIndex()
    {
        return 1;
    }

    protected Object getLastConnection(Object values[])
    {
        return values[getTimestampIndex()];
    }

    public int getClients(long interval)
        throws LogException
    {
        Timestamp oldestRecordTimestamp;
        if(interval == 0L)
            oldestRecordTimestamp = new Timestamp(0L);
        else
            oldestRecordTimestamp = new Timestamp(System.currentTimeMillis() - interval);
        return super.database.getUniqueRecordCount(getUserIDIndex(), oldestRecordTimestamp, getTimestampIndex());
    }

    public boolean hasClientConnected(String userID, long interval)
        throws LogException
    {
        Timestamp oldestRecordTimestamp = new Timestamp(System.currentTimeMillis() - interval);
        int clientCount = super.database.getRecordCount(3, getUserIDIndex(), ((Object) (userID)));
        return clientCount > 0;
    }

    protected boolean accept(Object incoming_record[])
        throws LogException
    {
        Object local_record[] = super.database.getRecord(incoming_record);
        if(local_record == null)
            return true;
        else
            return select(incoming_record, (Timestamp)getLastConnection(local_record));
    }

    protected boolean select(Object record[], Timestamp from)
        throws LogException
    {
        Object lastConnection = getLastConnection(record);
        if(lastConnection == null)
            return false;
        if(!(lastConnection instanceof Timestamp))
            return false;
        return ((Date) (from)).getTime() < ((Date) ((Timestamp)lastConnection)).getTime();
    }

    protected boolean select(Object record[], Timestamp from, Timestamp to)
        throws LogException
    {
        Object lastConnection = getLastConnection(record);
        if(lastConnection == null)
            return false;
        if(!(lastConnection instanceof Timestamp))
            return false;
        if(((Date) ((Timestamp)lastConnection)).getTime() < ((Date) (from)).getTime())
            return false;
        if(to == null)
            return true;
        return ((Date) ((Timestamp)lastConnection)).getTime() <= ((Date) (to)).getTime();
    }

    public int queryUsersForBundle(String bundle, Timestamp startDate, Timestamp endDate)
        throws LogException
    {
        int count = 0;
        Vector records = null;
        if("DDCAM".equalsIgnoreCase(bundle))
            records = super.database.getRecords(new int[] {
                1, 4
            }, new int[] {
                5, 5
            }, new Object[] {
                startDate, endDate
            });
        else
            records = super.database.getRecords(new int[] {
                3, 1, 4
            }, new int[] {
                3, 5, 5
            }, new Object[] {
                bundle, startDate, endDate
            });
        if(records == null)
            count = 0;
        else
            count = records.size();
        return count;
    }

    public int queryUsersForBundleVersion(String bundle, String version, Timestamp startDate, Timestamp endDate)
        throws LogException
    {
        int count = 0;
        Vector records = null;
        records = super.database.getRecords(new int[] {
            3, 3, 1, 4
        }, new int[] {
            3, 4, 5, 5
        }, new Object[] {
            bundle, version, startDate, endDate
        });
        if(records == null)
            return 0;
        else
            return records.size();
    }

}
