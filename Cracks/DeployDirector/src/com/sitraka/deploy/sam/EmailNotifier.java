// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EmailNotifier.java

package com.sitraka.deploy.sam;

import com.klg.jclass.util.JCStringTokenizer;
import com.sitraka.deploy.common.LoggingEnums;
import com.sitraka.deploy.common.SendEmailMessage;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;

// Referenced classes of package com.sitraka.deploy.sam:
//            AbstractMessageQueue, LogMessage, LogQueue

public class EmailNotifier extends AbstractMessageQueue
{

    protected boolean isAvailable;
    protected String our_name;
    protected String our_password;
    protected String email_server;
    protected Hashtable directorTable;
    protected LogQueue logger;

    public EmailNotifier(LogQueue logQueue, Properties properties)
    {
        isAvailable = false;
        our_name = null;
        our_password = null;
        email_server = null;
        directorTable = new Hashtable();
        logger = null;
        logger = logQueue;
        our_name = PropertyUtils.readDefaultString(properties, "deploy.error.email.user", ((String) (null)));
        if(our_name == null)
            our_name = "DeploySam";
        our_password = PropertyUtils.readPassword(properties, "deploy.error.email.password", ((String) (null)));
        email_server = PropertyUtils.readDefaultString(properties, "deploy.error.email.server", ((String) (null)));
        if(email_server == null || email_server.length() == 0)
        {
            isAvailable = false;
            return;
        } else
        {
            readRecipientData(properties);
            isAvailable = true;
            return;
        }
    }

    public synchronized boolean isAvailable()
    {
        return isAvailable;
    }

    protected void readRecipientData(Properties properties)
    {
        int i = 0;
        do
        {
            String target = PropertyUtils.readDefaultString(properties, "deploy.error.email." + i + ".address", ((String) (null)));
            if(target == null)
                break;
            Vector target_list = new Vector();
            for(JCStringTokenizer st = new JCStringTokenizer(target); st.hasMoreTokens(); target_list.add(((Object) (st.nextToken(',')))));
            String types = PropertyUtils.readDefaultString(properties, "deploy.error.email." + i + ".notification", ((String) (null)));
            if(types == null)
                break;
            types = types.toLowerCase();
            for(JCStringTokenizer toker = new JCStringTokenizer(types); toker.hasMoreTokens();)
            {
                String category = toker.nextToken(',');
                if(!directorTable.containsKey(((Object) (category))))
                    directorTable.put(((Object) (category)), ((Object) (new Vector())));
                Vector addressList = (Vector)directorTable.get(((Object) (category)));
                for(int j = 0; j < target_list.size(); j++)
                {
                    String address = (String)target_list.elementAt(j);
                    if(!addressList.contains(((Object) (address))))
                        addressList.addElement(((Object) (address)));
                }

            }

            i++;
        } while(true);
    }

    protected void processMessage(Object message)
    {
        if(!isAvailable())
            return;
        LogMessage m = (LogMessage)message;
        String event_name = m.getEvent();
        String category = LoggingEnums.getEventClass(event_name);
        if(category == null)
            if(!event_name.startsWith("Client") && !event_name.startsWith("Server"))
                category = event_name;
            else
                return;
        Vector recipients = null;
        if(LoggingEnums.isApplicationEvent(event_name))
        {
            String bundle_category = "bundle." + m.getApplication().toLowerCase();
            recipients = (Vector)directorTable.get(((Object) (bundle_category)));
            if(recipients == null || recipients.size() == 0)
                recipients = (Vector)directorTable.get("bundle.all");
        }
        if(recipients == null || recipients.size() == 0)
            recipients = (Vector)directorTable.get(((Object) (category)));
        if(recipients == null || recipients.size() == 0)
            return;
        String to[] = new String[recipients.size()];
        for(int i = 0; i < recipients.size(); i++)
            to[i] = (String)recipients.elementAt(i);

        String subject = "DeploySam: Error " + event_name + " Logged";
        try
        {
            SendEmailMessage.sendMessage(email_server, subject, our_name, our_password, our_name, to, m.formatMessage());
        }
        catch(SendFailedException sfe)
        {
            Address addresses[] = sfe.getInvalidAddresses();
            String log_message = "The following addresses were invalid: ";
            for(int i = 0; addresses != null && i < addresses.length; i++)
                log_message = log_message + addresses[i].toString() + ", ";

            if(addresses.length > 0)
                logger.logMessage("ServerEmailFailed", ((String) (null)), ((String) (null)), ((String) (null)), ((String) (null)), log_message);
            addresses = sfe.getValidSentAddresses();
            log_message = "\tValid addresses already sent: ";
            for(int i = 0; addresses != null && i < addresses.length; i++)
                log_message = log_message + addresses[i].toString() + ", ";

            addresses = sfe.getValidUnsentAddresses();
            String new_recipients[] = new String[addresses.length];
            log_message = "\tValid addresses still unsent: ";
            for(int i = 0; addresses != null && i < addresses.length; i++)
            {
                log_message = log_message + addresses[i].toString() + ", ";
                new_recipients[i] = addresses[i].toString();
            }

            try
            {
                SendEmailMessage.sendMessage(email_server, subject, our_name, our_password, our_name, new_recipients, m.formatMessage());
            }
            catch(MessagingException me2)
            {
                logger.logMessage("ServerEmailFailed", ((String) (null)), ((String) (null)), ((String) (null)), ((String) (null)), extractStackTrace(((Throwable) (me2))));
            }
        }
        catch(MessagingException me)
        {
            logger.logMessage("ServerEmailFailed", ((String) (null)), ((String) (null)), ((String) (null)), ((String) (null)), extractStackTrace(((Throwable) (me))));
        }
    }

    protected String extractStackTrace(Throwable t)
    {
        StringWriter writer = new StringWriter();
        PrintWriter w = new PrintWriter(((java.io.Writer) (writer)));
        t.printStackTrace(w);
        return writer.toString();
    }
}
