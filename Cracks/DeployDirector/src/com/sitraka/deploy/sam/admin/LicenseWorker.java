// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LicenseWorker.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.PropertyUtils;
import com.sitraka.licensing.LicenseProperties;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public class LicenseWorker extends JSPPropertiesSupport
{

    protected String importFile;

    public LicenseWorker()
    {
    }

    public void refresh()
    {
        super.refresh();
        importFile = null;
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(importFile == null)
        {
            ((JSPSupport)this).errMessage = "You must specify a file to import";
            return false;
        }
        FileInputStream fis = null;
        try
        {
            fis = new FileInputStream(importFile);
        }
        catch(FileNotFoundException fnfe)
        {
            ((JSPSupport)this).errMessage = "Unable to find license file \"" + importFile + "\"<br>\n" + ((Throwable) (fnfe)).getMessage();
            return false;
        }
        Properties props = new Properties();
        try
        {
            props.load(((java.io.InputStream) (fis)));
        }
        catch(IOException ioe)
        {
            ((JSPSupport)this).errMessage = "Can't load file into properties object<br>\n" + ((Throwable) (ioe)).getMessage();
            return false;
        }
        Servlet servlet = ((JSPSupport)this).getServlet();
        if(servlet == null)
            return false;
        AppVault vault = servlet.getVault();
        int numApplications = vault.countApplications();
        LicenseProperties licenseProperties = new LicenseProperties();
        String serial_number = PropertyUtils.readDefaultString(props, "serial_number", ((String) (null)));
        if(serial_number == null)
        {
            ((JSPSupport)this).errMessage = "No serial number listed.";
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("serial_number", serial_number);
        String hosts = PropertyUtils.readDefaultString(props, "hosts", ((String) (null)));
        if(hosts == null)
        {
            ((JSPSupport)this).errMessage = "No host names specified.";
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("hosts", hosts);
        int hostResult = licenseProperties.validateHost("hosts");
        if(hostResult != 1)
        {
            if(hostResult == 12)
            {
                ((JSPSupport)this).errMessage = "Hosts property not found.";
                return false;
            }
            if(hostResult == 3)
            {
                ((JSPSupport)this).errMessage = "Number of CPUs exceeded.";
                return false;
            }
            if(hostResult == 2)
            {
                ((JSPSupport)this).errMessage = "Hostname is invalid.";
                return false;
            } else
            {
                ((JSPSupport)this).errMessage = "Unknown hostname error.";
                return false;
            }
        }
        int num_clients = -1;
        String clients = props.getProperty("clients", ((String) (null)));
        if(clients != null)
        {
            ((Properties) (licenseProperties)).setProperty("clients", clients);
            try
            {
                num_clients = Integer.parseInt(clients);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (clients))))
                {
                    num_clients = -1;
                } else
                {
                    ((JSPSupport)this).errMessage = "Maximum number of clients incorrectly specified or missing: " + clients;
                    return false;
                }
            }
        }
        int client_count = 0;
        try
        {
            client_count = servlet.getLogQueue().getClientDataLogger().getClients(0xffffffffcf7c5800L);
        }
        catch(LogException le) { }
        catch(NullPointerException npe) { }
        if(num_clients > 0 && client_count > num_clients)
        {
            ((JSPSupport)this).errMessage = "number of current clients (" + client_count + ") exceeds maximum allowed by license (" + num_clients + ")";
            return false;
        }
        String signature = PropertyUtils.readDefaultString(props, "sitraka.license.signature", ((String) (null)));
        if(signature == null)
        {
            ((JSPSupport)this).errMessage = "License signature is missing.";
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("sitraka.license.signature", signature);
        boolean checkNumberOfBundles = true;
        String app = props.getProperty("applications", ((String) (null)));
        Vector applications;
        if(app == null)
            applications = new Vector();
        else
        if(app.trim().length() == 0 || app.indexOf("(unlimited)") >= 0)
        {
            ((Properties) (licenseProperties)).setProperty("applications", app);
            applications = new Vector();
        } else
        {
            ((Properties) (licenseProperties)).setProperty("applications", app);
            applications = PropertyUtils.readVector(props, "applications", ',');
            if(applications == null)
                applications = new Vector();
            else
                checkNumberOfBundles = false;
        }
        int num_apps = -1;
        String bundles = props.getProperty("bundles", ((String) (null)));
        if(bundles != null)
        {
            ((Properties) (licenseProperties)).setProperty("bundles", bundles);
            try
            {
                num_apps = Integer.parseInt(bundles);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (bundles))))
                {
                    num_apps = -1;
                } else
                {
                    ((JSPSupport)this).errMessage = "Maximum number of bundles incorrectly specified or missing: " + bundles;
                    return false;
                }
            }
        }
        if(checkNumberOfBundles && num_apps > 0 && numApplications > num_apps)
        {
            ((JSPSupport)this).errMessage = "number of user bundles (" + numApplications + ") exceeds maximum allowed by license (" + num_apps + ")";
            return false;
        }
        String date_string = props.getProperty("expiry", ((String) (null)));
        if(date_string != null && !"(never)".equals(((Object) (date_string))))
        {
            ((Properties) (licenseProperties)).setProperty("expiry", date_string);
            if(licenseProperties.isDateExpired("expiry"))
            {
                ((JSPSupport)this).errMessage = "License has expired: " + date_string;
                return false;
            }
        }
        String ddLicenseVersion = PropertyUtils.readDefaultString(props, "license_version", ((String) (null)));
        if(!"2.5".equals(((Object) (ddLicenseVersion))))
        {
            ((JSPSupport)this).errMessage = "DD Version is incompatible: " + ddLicenseVersion + " vs " + "2.5";
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("license_version", ddLicenseVersion);
        String licenseVersion = PropertyUtils.readDefaultString(props, "sitraka.license.version", ((String) (null)));
        if(licenseVersion == null)
        {
            ((JSPSupport)this).errMessage = "Licensing version is missing.";
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("sitraka.license.version", licenseVersion);
        String licenseType = PropertyUtils.readDefaultString(props, "type", ((String) (null)));
        if(!"department".equals(((Object) (licenseType))) && !"enterprise".equals(((Object) (licenseType))) && !"developer".equals(((Object) (licenseType))) && !"evaluation".equals(((Object) (licenseType))))
        {
            ((JSPSupport)this).errMessage = "License Type is invalid: " + licenseType;
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("type", licenseType);
        Class centinal;
        if("department".equals(((Object) (licenseType))) || "developer".equals(((Object) (licenseType))) || "evaluation".equals(((Object) (licenseType))))
            try
            {
                centinal = Class.forName("org.apache.tomcat.core.Security");
            }
            catch(ClassNotFoundException cnfe)
            {
                ((JSPSupport)this).errMessage = "When using a " + licenseType + " license, " + "DeployDirector must be run with the standalone Tomcat " + "Server shipped by Sitraka.";
                return false;
            }
        String licenseProduct = PropertyUtils.readDefaultString(props, "product", ((String) (null)));
        if(licenseProduct == null)
        {
            ((JSPSupport)this).errMessage = "Required license field \"product\" is invalid '" + licenseProduct + "'";
            return false;
        }
        ((Properties) (licenseProperties)).setProperty("product", licenseProduct);
        licenseProperties.validate();
        boolean result = licenseProperties.isValid();
        if(!result)
        {
            ((JSPSupport)this).errMessage = "Data does not match signature";
            return false;
        }
        ((Hashtable) (super.properties)).remove("applications");
        ((Hashtable) (super.properties)).remove("bundles");
        ((Hashtable) (super.properties)).remove("clients");
        ((Hashtable) (super.properties)).remove("expiry");
        ((Hashtable) (super.properties)).remove("hosts");
        ((Hashtable) (super.properties)).remove("license_version");
        ((Hashtable) (super.properties)).remove("serial_number");
        ((Hashtable) (super.properties)).remove("sitraka.license.signature");
        ((Hashtable) (super.properties)).remove("sitraka.license.version");
        ((Hashtable) (super.properties)).remove("type");
        ((Hashtable) (super.properties)).remove("product");
        String key;
        for(Enumeration e = ((Hashtable) (licenseProperties)).keys(); e.hasMoreElements(); ((Hashtable) (super.properties)).put(((Object) (key)), ((Object) (((Properties) (licenseProperties)).getProperty(key)))))
            key = (String)e.nextElement();

        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    public void setFile(String infile)
    {
        importFile = infile;
    }

    public String getFile()
    {
        return importFile;
    }
}
