// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerHostBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport

public abstract class ServerHostBase extends JSPPropertiesSupport
{

    protected Vector hosts;

    public ServerHostBase()
    {
        hosts = null;
    }

    public void refresh()
    {
        super.refresh();
        hosts = null;
    }

    public boolean validate()
    {
        return true;
    }

    protected void getExistingValues()
    {
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    public void setHosts(String name)
    {
        if(hosts == null)
            hosts = new Vector();
        if(name.indexOf(',') != -1)
        {
            for(StringTokenizer st = new StringTokenizer(name, ","); st.hasMoreTokens(); storeName(st.nextToken()));
        } else
        {
            storeName(name);
        }
    }

    protected void storeName(String name)
    {
        for(int i = 0; i < hosts.size(); i++)
            if(((String)hosts.elementAt(i)).equals(((Object) (name))))
                return;

        hosts.addElement(((Object) (name)));
    }

    public void setHosts(Vector list)
    {
        hosts = new Vector();
        for(int i = 0; i < list.size(); i++)
            hosts.addElement(list.elementAt(i));

    }

    public Vector getHostGroup(String groupName)
    {
        hosts = new Vector();
        ((JSPPropertiesSupport)this).initializeProperties();
        int index = 0;
        String s;
        for(String p_name = "deploy." + groupName + ".host." + index + ".machine"; (s = super.properties.getProperty(p_name)) != null; p_name = "deploy." + groupName + ".host." + index + ".machine")
        {
            if(s.trim().length() != 0)
                hosts.addElement(((Object) (s)));
            index++;
        }

        return hosts;
    }
}
