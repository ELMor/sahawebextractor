// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerStatistics.java

package com.sitraka.deploy.sam.admin;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            LogAccessBase

public class ServerStatistics extends LogAccessBase
{

    public ServerStatistics()
    {
    }

    public Vector getServerStatistics()
    {
        return ((LogAccessBase)this).getLogEntries("DDLoadLog");
    }

    public int getNotesField()
    {
        return 6;
    }

    public String columnAlign(int column)
    {
        if(column > 1)
            return "right";
        else
            return "left";
    }

    public String formatCol(String value, int column)
    {
        if(column < 2)
            return value;
        if(column == 2)
            try
            {
                long milli = Long.valueOf(value).longValue();
                milli /= 1000L;
                DecimalFormat df = new DecimalFormat("00");
                String sec = ((NumberFormat) (df)).format(milli % 60L).toString();
                milli /= 60L;
                String min = ((NumberFormat) (df)).format(milli % 60L).toString();
                milli /= 60L;
                df = new DecimalFormat("#0");
                String hr = ((NumberFormat) (df)).format(milli % 24L).toString();
                milli /= 24L;
                String time = null;
                if(milli == 0L)
                    time = "" + hr + ":" + min + ":" + sec;
                else
                if(milli == 1L)
                    time = "1 day " + hr + ":" + min + ":" + sec;
                else
                    time = "" + milli + " days " + hr + ":" + min + ":" + sec;
                return time;
            }
            catch(NumberFormatException nfe)
            {
                return value;
            }
        try
        {
            double num = Double.valueOf(value).doubleValue();
            DecimalFormat df = new DecimalFormat("0.000");
            return ((NumberFormat) (df)).format(num).toString();
        }
        catch(NumberFormatException nfe)
        {
            return value;
        }
    }
}
