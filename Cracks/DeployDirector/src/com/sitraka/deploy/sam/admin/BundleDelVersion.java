// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleDelVersion.java

package com.sitraka.deploy.sam.admin;


// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class BundleDelVersion extends JSPSupport
{

    protected String bundle;

    public BundleDelVersion()
    {
        bundle = null;
    }

    public void refresh()
    {
        super.refresh();
        bundle = null;
    }

    public boolean validate()
    {
        if(bundle == null)
        {
            super.errMessage = "You must select an app to continue<br>";
            return false;
        } else
        {
            return true;
        }
    }

    public void setBundle(String name)
    {
        bundle = name;
    }

    public String getBundle()
    {
        return bundle;
    }
}
