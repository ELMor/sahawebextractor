// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LicenseGet.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.servlet.Servlet;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public class LicenseGet extends JSPPropertiesSupport
{

    protected String serial;
    protected String expiry;
    protected String maxBundles;
    protected String maxClients;
    protected String hostNames;
    protected String appNames;
    protected String signature;
    protected String type;
    protected String product;
    protected String version;
    protected String sitraka_version;
    protected boolean hasLicense;

    public LicenseGet()
    {
        serial = null;
        expiry = null;
        maxBundles = null;
        maxClients = null;
        hostNames = null;
        appNames = null;
        signature = null;
        type = null;
        product = null;
        version = null;
        sitraka_version = null;
        hasLicense = false;
    }

    public void refresh()
    {
        super.refresh();
        serial = null;
        expiry = null;
        maxBundles = null;
        maxClients = null;
        hostNames = null;
        appNames = null;
        signature = null;
        type = null;
        product = null;
        version = null;
        sitraka_version = null;
        hasLicense = false;
    }

    public String checkLicense()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        Servlet servlet = ((JSPSupport)this).getServlet();
        if(servlet == null)
            return ((JSPSupport)this).errMessage;
        if(Servlet.isLicensed())
            return "valid==true";
        else
            return "Not Licensed";
    }

    public boolean validate()
    {
        return true;
    }

    protected void getExistingValues()
    {
        serial = super.properties.getProperty("serial_number", "");
        expiry = super.properties.getProperty("expiry", "");
        maxBundles = super.properties.getProperty("bundles", "");
        maxClients = super.properties.getProperty("clients", "");
        hostNames = super.properties.getProperty("hosts", "");
        appNames = super.properties.getProperty("applications", "");
        signature = super.properties.getProperty("sitraka.license.signature", "");
        type = super.properties.getProperty("type", "");
        product = super.properties.getProperty("product", "");
        version = super.properties.getProperty("license_version", "");
        sitraka_version = super.properties.getProperty("sitraka.license.version", "");
        hasLicense = signature.length() != 0;
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    protected String quoted(boolean addQuote, String input)
    {
        if(addQuote)
            return "\"" + input + "\"";
        else
            return input;
    }

    public String getSerialNumber(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return quoted(useQuotes, serial);
    }

    public String getLicenseExpiry(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(!hasLicense)
            return "";
        if(expiry == null || expiry.trim().length() == 0)
            expiry = "(never)";
        return quoted(useQuotes, expiry);
    }

    public String getMaxBundles(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(!hasLicense)
            return "";
        try
        {
            if(maxBundles == null || maxBundles.trim().length() == 0 || maxBundles.trim().equals("(unlimited)") || Integer.parseInt(maxBundles) == -1)
                maxBundles = "(unlimited)";
        }
        catch(NumberFormatException e)
        {
            maxBundles = "Invalid Value";
        }
        return quoted(useQuotes, maxBundles);
    }

    public String getMaxClients(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(!hasLicense)
            return "";
        try
        {
            if(maxClients == null || maxClients.trim().length() == 0 || maxClients.trim().equals("(unlimited)") || Integer.parseInt(maxClients) == -1)
                maxClients = "(unlimited)";
        }
        catch(NumberFormatException e)
        {
            maxClients = "Invalid Value";
        }
        return quoted(useQuotes, maxClients);
    }

    protected String addSpaces(String input)
    {
        String output = "";
        if(input == null || input.trim().length() == 0)
            return output;
        int start = 0;
        int last = input.length();
        int i;
        while((i = input.indexOf(',', start)) != -1) 
        {
            if(++i < last && input.charAt(i) == ' ')
            {
                i++;
                output = output + input.substring(start, i);
            } else
            {
                output = output + input.substring(start, i) + " ";
            }
            start = i;
        }
        if(start < last)
            output = output + input.substring(start);
        return output;
    }

    public String getHostNames(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(!hasLicense)
        {
            return "";
        } else
        {
            String hosts = addSpaces(hostNames);
            return quoted(useQuotes, hosts);
        }
    }

    public String getAppNames(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(!hasLicense)
            return "";
        if(appNames == null || appNames.trim().length() == 0)
            appNames = "(unlimited)";
        String applications = addSpaces(appNames);
        return quoted(useQuotes, applications);
    }

    public String getLicenseSignature(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return quoted(useQuotes, signature);
    }

    public String getProductName(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return quoted(useQuotes, product);
    }

    public String getLicenseType(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return quoted(useQuotes, type);
    }

    public String getLicenseTypeTitle(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        Servlet servlet = ((JSPSupport)this).getServlet();
        if(servlet == null)
            return quoted(useQuotes, "Servlet Error");
        if(Servlet.isLicensed())
        {
            if("department".equals(((Object) (type))))
                return quoted(useQuotes, "Department Edition");
            if("enterprise".equals(((Object) (type))))
                return quoted(useQuotes, "Enterprise Edition");
            if("evaluation".equals(((Object) (type))))
                return quoted(useQuotes, "Evaluation");
            else
                return quoted(useQuotes, "License Error");
        } else
        {
            return quoted(useQuotes, "Not Licensed");
        }
    }

    public String getLicenseTypeShort(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        Servlet servlet = ((JSPSupport)this).getServlet();
        if(servlet == null)
            return quoted(useQuotes, "Servlet Error");
        if(Servlet.isLicensed())
        {
            if("department".equals(((Object) (type))))
                return quoted(useQuotes, "DE");
            if("enterprise".equals(((Object) (type))))
                return quoted(useQuotes, "EE");
            if("evaluation".equals(((Object) (type))))
                return quoted(useQuotes, "EV");
            else
                return quoted(useQuotes, "LE");
        } else
        {
            return quoted(useQuotes, "NL");
        }
    }

    public String getLicenseVersion(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return quoted(useQuotes, version);
    }

    public String getSitrakaVersion(boolean useQuotes)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return quoted(useQuotes, sitraka_version);
    }
}
