// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerPassword.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.util.Crypt;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public class ServerPassword extends JSPPropertiesSupport
{

    protected String adminPassword;
    protected String oldPassword;
    protected String newPassword;
    protected String confirmPassword;

    public ServerPassword()
    {
        adminPassword = null;
        oldPassword = null;
        newPassword = null;
        confirmPassword = null;
    }

    public void refresh()
    {
        super.refresh();
        adminPassword = null;
        oldPassword = null;
        newPassword = null;
        confirmPassword = null;
    }

    protected boolean checkPassword(String pass)
    {
        if(adminPassword == null || adminPassword.length() == 0)
        {
            return pass == null || pass.length() == 0;
        } else
        {
            String pass_crypt = Crypt.crypt(adminPassword, pass);
            return pass_crypt.equals(((Object) (adminPassword)));
        }
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        if(oldPassword == null || newPassword == null || confirmPassword == null)
        {
            ((JSPSupport)this).errMessage = "You must provide the old password and enter the new password twice<br>";
            return false;
        }
        if(!checkPassword(oldPassword))
        {
            ((JSPSupport)this).errMessage = "The old admin password is not correct.<br>";
            return false;
        }
        if(!newPassword.equals(((Object) (confirmPassword))))
        {
            ((JSPSupport)this).errMessage = "The new and confirmation passwords did not match.<br>";
            return false;
        } else
        {
            newPassword = Crypt.crypt(adminPassword, newPassword);
            super.properties.setProperty("deploy.admin.password", newPassword);
            return ((JSPPropertiesSupport)this).storeProperties();
        }
    }

    protected void getExistingValues()
    {
        adminPassword = super.properties.getProperty("deploy.admin.password", "");
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    public void setOldPassword(String old)
    {
        oldPassword = old;
    }

    public String getOldPassword()
    {
        return oldPassword;
    }

    public void setNewPassword(String pass)
    {
        newPassword = pass;
    }

    public String getNewPassword()
    {
        return newPassword;
    }

    public void setConfirmPassword(String confirm)
    {
        confirmPassword = confirm;
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }
}
