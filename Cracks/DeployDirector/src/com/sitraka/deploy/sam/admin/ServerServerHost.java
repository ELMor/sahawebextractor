// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerServerHost.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport

public class ServerServerHost extends JSPPropertiesSupport
{

    protected String host;
    protected String accessProtocol;
    protected String accessPort;
    protected String rootPage;

    public ServerServerHost()
    {
        host = null;
        accessProtocol = null;
        accessPort = null;
        rootPage = null;
    }

    public void refresh()
    {
        super.refresh();
        host = null;
        accessProtocol = null;
        accessPort = null;
        rootPage = null;
    }

    protected void getExistingValues()
    {
        host = super.properties.getProperty("deploy.local.host.machine", "");
        accessProtocol = super.properties.getProperty("deploy.local.host.protocol", "");
        accessPort = super.properties.getProperty("deploy.local.host.port", "");
        rootPage = super.properties.getProperty("deploy.local.host.page", "");
    }

    protected String getPropFile()
    {
        return "server.properties";
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        super.properties.setProperty("deploy.local.host.machine", host);
        super.properties.setProperty("deploy.local.host.page", rootPage);
        super.properties.setProperty("deploy.local.host.port", accessPort);
        super.properties.setProperty("deploy.local.host.protocol", accessProtocol);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    public void setHost(String name)
    {
        host = name;
    }

    public String getHost()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return host;
    }

    public void setAccessProtocol(String protocol)
    {
        accessProtocol = protocol;
    }

    public String getAccessProtocol()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return accessProtocol;
    }

    public void setAccessPort(String port)
    {
        accessPort = port;
    }

    public String getAccessPort()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return accessPort;
    }

    public void setRootPage(String path)
    {
        rootPage = path;
    }

    public String getRootPage()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return rootPage;
    }
}
