// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerStatusLoggingBase.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.util.PropertyUtils;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public abstract class ServerStatusLoggingBase extends JSPPropertiesSupport
{

    protected boolean clusterLog;
    protected boolean localLog;
    protected String aggregationStartDate;
    protected String aggregationInterval;
    protected boolean logToFile;
    protected boolean logToDatabase;
    protected String logLocation;
    protected String jdbcDriver;
    protected String jdbcURL;
    protected String jdbcUser;
    protected String jdbcPassword;
    protected String logFrequency;
    protected String updateStartDate;
    protected String updateInterval;
    protected String minimumSize;
    protected String maximumSize;
    protected String maximumAge;

    public ServerStatusLoggingBase()
    {
        clusterLog = false;
        localLog = false;
        aggregationStartDate = null;
        aggregationInterval = null;
        logToFile = false;
        logToDatabase = false;
        logLocation = null;
        jdbcDriver = null;
        jdbcURL = null;
        jdbcUser = null;
        jdbcPassword = null;
        logFrequency = null;
        updateStartDate = null;
        updateInterval = null;
        minimumSize = null;
        maximumSize = null;
        maximumAge = null;
    }

    public void refresh()
    {
        super.refresh();
        clusterLog = false;
        localLog = false;
        aggregationStartDate = "";
        aggregationInterval = "";
        logToFile = false;
        logToDatabase = false;
        logLocation = "";
        jdbcDriver = "";
        jdbcURL = "";
        jdbcUser = "";
        jdbcPassword = "";
        logFrequency = "";
        updateStartDate = "";
        updateInterval = "";
        minimumSize = "";
        maximumSize = "";
        maximumAge = "";
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties(false);
        if(clusterLog && !logToDatabase)
        {
            ((JSPSupport)this).errMessage = "Cluster logging must be to a JDBC database.";
            return false;
        }
        if(logToDatabase)
        {
            ((JSPSupport)this).errMessage = "";
            if(jdbcDriver == null || jdbcDriver.trim().length() == 0)
                ((JSPSupport)this).errMessage = "You must specify a JDBC driver for database logging.";
            if(jdbcURL == null || jdbcURL.trim().length() == 0)
                ((JSPSupport)this).errMessage += "You must specify a URL for database logging.";
            if(jdbcUser == null || jdbcUser.trim().length() == 0)
                ((JSPSupport)this).errMessage += "You must specify a username for database logging.";
            if(jdbcPassword == null || jdbcPassword.trim().length() == 0)
                ((JSPSupport)this).errMessage += "You must specify a password for database logging.";
            if(((JSPSupport)this).errMessage.length() > 0)
                return false;
        }
        if(logToFile && (logLocation == null || logLocation.length() == 0))
        {
            ((JSPSupport)this).errMessage = "A location must be specified for logging to files.";
            return false;
        }
        Properties test_prop = new Properties();
        long date_or_interval;
        if(localLog)
        {
            test_prop.setProperty("test.date", aggregationStartDate);
            date_or_interval = PropertyUtils.readDate(test_prop, "test.date");
            if(aggregationStartDate == null || aggregationStartDate.trim().length() == 0)
            {
                ((JSPSupport)this).errMessage = "You must specify the start date for log aggregation.";
                return false;
            }
            if(date_or_interval <= 0L)
            {
                ((JSPSupport)this).errMessage = "Log aggregation start date was not valid.";
                return false;
            }
            test_prop.setProperty("interval", aggregationInterval);
            date_or_interval = PropertyUtils.readInterval(test_prop, "interval");
            if(aggregationInterval == null || aggregationInterval.trim().length() == 0)
            {
                ((JSPSupport)this).errMessage = "You must specify a log aggregation interval.";
                return false;
            }
            if(date_or_interval <= 0L)
            {
                ((JSPSupport)this).errMessage = "Log aggregation interval was not valid.";
                return false;
            }
        }
        try
        {
            int max_val = Integer.parseInt(maximumSize);
            int min_val = Integer.parseInt(minimumSize);
            if(min_val <= 0 || max_val < min_val)
                throw new NumberFormatException("invalid max or min");
        }
        catch(NumberFormatException nfe)
        {
            ((JSPSupport)this).errMessage = "You must specify a non-zero number for maximum and minimum log size, and max must be greater than min.";
            return false;
        }
        test_prop.setProperty("interval", maximumAge);
        date_or_interval = PropertyUtils.readDate(test_prop, "test.date");
        if(maximumAge == null || maximumAge.trim().length() == 0)
            maximumAge = "30 days";
        else
        if(date_or_interval <= 0L)
        {
            ((JSPSupport)this).errMessage = "Maximum log entry age was not valid.";
            return false;
        }
        test_prop.setProperty("test.date", updateStartDate);
        date_or_interval = PropertyUtils.readDate(test_prop, "test.date");
        if(updateStartDate == null || updateStartDate.trim().length() == 0)
            updateStartDate = "today";
        else
        if(date_or_interval <= 0L)
        {
            ((JSPSupport)this).errMessage = "Update start date was not valid.";
            return false;
        }
        test_prop.setProperty("interval", updateInterval);
        date_or_interval = PropertyUtils.readInterval(test_prop, "interval");
        if(updateInterval == null || updateInterval.trim().length() == 0)
            updateInterval = "-1";
        else
        if(date_or_interval == 0L || date_or_interval < -1L)
        {
            ((JSPSupport)this).errMessage = "Update interval was not valid.";
            return false;
        }
        test_prop.setProperty("interval", logFrequency);
        date_or_interval = PropertyUtils.readInterval(test_prop, "interval");
        if(logFrequency == null || logFrequency.trim().length() == 0)
            logFrequency = "2 hours";
        else
        if(date_or_interval <= 0L)
        {
            ((JSPSupport)this).errMessage = "Load logging interval was not valid.";
            return false;
        }
        String logToCluster = "" + clusterLog;
        super.properties.setProperty("deploy.log.cluster", logToCluster);
        if(localLog)
        {
            super.properties.setProperty("deploy.log.aggregate.start", aggregationStartDate);
            super.properties.setProperty("deploy.log.aggregate.interval", aggregationInterval);
        } else
        {
            super.properties.setProperty("deploy.log.aggregate.start", "");
            super.properties.setProperty("deploy.log.aggregate.interval", "");
        }
        if(logToFile)
        {
            super.properties.setProperty("deploy.log.type", "file");
            super.properties.setProperty("deploy.log.file.location", logLocation);
            super.properties.setProperty("deploy.log.jdbc.driver", "");
            super.properties.setProperty("deploy.log.jdbc.password", "");
            super.properties.setProperty("deploy.log.jdbc.url", "");
            super.properties.setProperty("deploy.log.jdbc.user", "");
        } else
        {
            super.properties.setProperty("deploy.log.type", "jdbc");
            super.properties.setProperty("deploy.log.file.location", "");
            super.properties.setProperty("deploy.log.jdbc.driver", jdbcDriver);
            super.properties.setProperty("deploy.log.jdbc.password", jdbcPassword);
            super.properties.setProperty("deploy.log.jdbc.url", jdbcURL);
            super.properties.setProperty("deploy.log.jdbc.user", jdbcUser);
        }
        super.properties.setProperty("deploy.log.limit.max", maximumSize);
        super.properties.setProperty("deploy.log.limit.min", minimumSize);
        super.properties.setProperty("deploy.log.limit.interval", maximumAge);
        super.properties.setProperty("deploy.log.update.start", updateStartDate);
        super.properties.setProperty("deploy.log.update.interval", updateInterval);
        super.properties.setProperty("deploy.log.load.interval", logFrequency);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
        logLocation = super.properties.getProperty("deploy.log.file.location", "");
        jdbcDriver = super.properties.getProperty("deploy.log.jdbc.driver", "");
        jdbcURL = super.properties.getProperty("deploy.log.jdbc.url", "");
        jdbcUser = super.properties.getProperty("deploy.log.jdbc.user", "");
        jdbcPassword = super.properties.getProperty("deploy.log.jdbc.password", "");
        minimumSize = super.properties.getProperty("deploy.log.limit.min", "");
        maximumSize = super.properties.getProperty("deploy.log.limit.max", "");
        maximumAge = super.properties.getProperty("deploy.log.limit.interval", "");
        logFrequency = super.properties.getProperty("deploy.log.load.interval", "");
        updateStartDate = super.properties.getProperty("deploy.log.update.start", "");
        updateInterval = super.properties.getProperty("deploy.log.update.interval", "");
        clusterLog = super.properties.getProperty("deploy.log.cluster", "").equals("true");
        localLog = !clusterLog;
        logToFile = super.properties.getProperty("deploy.log.type", "").equals("file");
        logToDatabase = !logToFile;
        aggregationStartDate = super.properties.getProperty("deploy.log.aggregate.start", "");
        aggregationInterval = super.properties.getProperty("deploy.log.aggregate.interval", "");
    }

    public void setClusterLog(boolean cluster)
    {
        clusterLog = cluster;
    }

    public String getClusterLogSet()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return clusterLog ? "checked" : "";
    }

    public boolean isClusterLog()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return clusterLog;
    }

    public void setLocalLog(boolean local)
    {
        localLog = local;
    }

    public String getLocalLogSet()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return localLog ? "checked" : "";
    }

    public boolean isLocalLog()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return localLog;
    }

    public void setUpdateStartDate(String date)
    {
        updateStartDate = date;
    }

    public String getUpdateStartDate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return updateStartDate;
    }

    public void setUpdateInterval(String interval)
    {
        updateInterval = interval;
    }

    public String getUpdateInterval()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return updateInterval;
    }

    public void setAggregateStartDate(String date)
    {
        aggregationStartDate = date;
    }

    public String getAggregateStartDate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return aggregationStartDate;
    }

    public void setAggregateInterval(String interval)
    {
        aggregationInterval = interval;
    }

    public String getAggregateInterval()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return aggregationInterval;
    }

    public void setLogToFile(boolean file)
    {
        logToFile = file;
    }

    public boolean isLogToFile()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return logToFile;
    }

    public String getLogToFileSet()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return logToFile ? "checked" : "";
    }

    public void setLogToDatabase(boolean database)
    {
        logToDatabase = database;
    }

    public boolean isLogToDatabase()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return logToDatabase;
    }

    public String getLogToDatabaseSet()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return logToDatabase ? "checked" : "";
    }

    public void setLogLocation(String path)
    {
        logLocation = path;
    }

    public String getLogLocation()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return logLocation;
    }

    public void setJdbcDriver(String driver)
    {
        jdbcDriver = driver;
    }

    public String getJdbcDriver()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return jdbcDriver;
    }

    public void setJdbcUrl(String url)
    {
        jdbcURL = url;
    }

    public String getJdbcUrl()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return jdbcURL;
    }

    public void setJdbcUser(String user)
    {
        jdbcUser = user;
    }

    public String getJdbcUser()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return jdbcUser;
    }

    public void setJdbcPassword(String passwd)
    {
        jdbcPassword = passwd;
    }

    public String getJdbcPassword()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return jdbcPassword;
    }

    public void setLogFrequency(String frequency)
    {
        logFrequency = frequency;
    }

    public String getLogFrequency()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return logFrequency;
    }

    public void setMinimumSize(String size)
    {
        minimumSize = size;
    }

    public String getMinimumSize()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return minimumSize;
    }

    public void setMaximumSize(String size)
    {
        maximumSize = size;
    }

    public String getMaximumSize()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return maximumSize;
    }

    public void setMaximumAge(String age)
    {
        maximumAge = age;
    }

    public String getMaximumAge()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return maximumAge;
    }
}
