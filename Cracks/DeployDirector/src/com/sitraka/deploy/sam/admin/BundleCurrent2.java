// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleCurrent2.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.checkpoint.ApplicationObject;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.worker.AbstractWorker;
import com.sitraka.deploy.sam.worker.ReplicationThread;
import com.sitraka.deploy.util.SortUtils;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class BundleCurrent2 extends JSPSupport
{

    protected String application;
    protected Vector versionSequence;

    public BundleCurrent2()
    {
        application = null;
        versionSequence = null;
    }

    public void refresh()
    {
        super.refresh();
        application = null;
        versionSequence = null;
    }

    public boolean validate()
    {
        if(application == null || versionSequence == null)
        {
            super.errMessage = "Error processing your list<br>";
            return false;
        }
        if(((JSPSupport)this).getServlet() == null)
        {
            super.errMessage = "Unable to contact servlet.";
            return false;
        }
        AppVault vault = ((JSPSupport)this).getServlet().getVault();
        Application app = vault.getApplication(application);
        if(app == null)
        {
            super.errMessage = "The application \"" + application + "\" was not found in the vault.";
            return false;
        }
        Vector old_versions = app.listVersions();
        Vector old_names = new Vector();
        for(int i = 0; i < old_versions.size(); i++)
        {
            Version ver = (Version)old_versions.elementAt(i);
            old_names.addElement(((Object) (ver.getName())));
        }

        if(versionSequence.size() != old_names.size())
        {
            super.errMessage = "Current page is out of synch with the vault.";
            return false;
        }
        Vector new_sorted = SortUtils.sortStringVector(versionSequence, true, true);
        Vector old_sorted = SortUtils.sortStringVector(old_names, true, true);
        for(int i = 0; i < new_sorted.size(); i++)
        {
            String new_name = (String)new_sorted.elementAt(i);
            String old_name = (String)old_sorted.elementAt(i);
            if(new_name == null && old_name != null || !new_name.equalsIgnoreCase(old_name))
            {
                super.errMessage = "Current page is out of synch with the vault.";
                return false;
            }
        }

        StringBuffer v_list = new StringBuffer();
        for(int i = 0; i < versionSequence.size(); i++)
        {
            v_list.append((String)versionSequence.elementAt(i));
            v_list.append("\n");
        }

        if(!app.resyncVersionOrder(v_list.toString()))
        {
            super.errMessage = "Unable to reorder the versions in the vault.";
            return false;
        }
        if(!vault.createApplicationVersionsFile(app, ((java.io.File) (null)), false))
        {
            super.errMessage = "Unable to save the updated versions file.";
            return false;
        }
        if(vault.listOtherServers() != null)
        {
            String localhost = Servlet.properties.getProperty("deploy.localhost");
            ReplicationThread rt = AbstractWorker.getReplicationQueue(((com.sitraka.deploy.sam.ServerActions) (((JSPSupport)this).getServlet())));
            ApplicationObject app_obj = new ApplicationObject(localhost, application);
            rt.sendVersionReorder(localhost, app_obj, ((Object) (super.authData)));
        }
        return true;
    }

    public String getVersionSequence()
    {
        if(versionSequence == null || versionSequence.size() == 0)
            return "unset";
        StringBuffer buf = new StringBuffer((String)versionSequence.elementAt(0));
        for(int i = 1; i < versionSequence.size(); i++)
        {
            buf.append("<br>");
            buf.append((String)versionSequence.elementAt(i));
        }

        return versionSequence.toString();
    }

    public void setVersionSequence(String ord)
    {
        versionSequence = new Vector();
        int end;
        for(int start = ord.indexOf("vn="); start != -1; start = end)
        {
            String version;
            if((end = ord.indexOf("vn=", start + 3)) != -1)
                version = ord.substring(start + 3, end);
            else
                version = ord.substring(start + 3);
            versionSequence.addElement(((Object) (version)));
        }

    }

    public String getBundle()
    {
        return application;
    }

    public void setBundle(String bundle)
    {
        application = bundle;
    }
}
