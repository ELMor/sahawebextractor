// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleDelVersion2.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.common.checkpoint.VersionObject;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.worker.AbstractWorker;
import com.sitraka.deploy.sam.worker.ReplicationThread;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class BundleDelVersion2 extends JSPSupport
{

    protected String bundle;
    protected String version;

    public BundleDelVersion2()
    {
        bundle = null;
        version = null;
    }

    public void refresh()
    {
        super.refresh();
        version = null;
        bundle = null;
    }

    public boolean validate()
    {
        if(bundle == null)
        {
            super.errMessage = "Bundle to delete not set<br>";
            return false;
        }
        if(version == null)
        {
            super.errMessage = "Version to delete not set<br>";
            return false;
        }
        if(((JSPSupport)this).getServlet() == null)
            return false;
        AppVault vault = ((JSPSupport)this).getServlet().getVault();
        if(!vault.deleteVersion(bundle, version))
        {
            super.errMessage = "Vault could not remove the version<br>Check the server log for more details<br>";
            return false;
        }
        if(vault.listOtherServers() != null)
        {
            String localhost = Servlet.properties.getProperty("deploy.localhost");
            ReplicationThread rt = AbstractWorker.getReplicationQueue(((com.sitraka.deploy.sam.ServerActions) (((JSPSupport)this).getServlet())));
            VersionObject ver_obj = new VersionObject(localhost, bundle, version);
            rt.sendVersionDeletion(localhost, ver_obj, ((Object) (super.authData)));
        }
        return true;
    }

    public void setVersion(String name)
    {
        version = name;
    }

    public String getVersion()
    {
        return version;
    }

    public void setBundle(String name)
    {
        bundle = name;
    }

    public String getBundle()
    {
        return bundle;
    }
}
