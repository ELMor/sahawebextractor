// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleImport.java

package com.sitraka.deploy.sam.admin;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import com.sitraka.deploy.common.app.CheckNames;
import com.sitraka.deploy.common.dar.DarException;
import com.sitraka.deploy.common.dar.UploadDAR;
import com.sitraka.deploy.util.Codecs;
import com.sitraka.deploy.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class BundleImport extends JSPSupport
{
    protected class DarUploader extends Thread
    {

        protected String uploadID;
        protected int status;
        protected String errInfo;

        public int getStatus()
        {
            int i;
            synchronized(this)
            {
                i = status;
            }
            return i;
        }

        public void setStatus(int new_status)
        {
            synchronized(this)
            {
                status = new_status;
            }
        }

        public String getErrorInfo()
        {
            return errInfo;
        }

        public void run()
        {
            setStatus(BundleImport.UPLOAD_IN_PROGRESS);
            if(appName != null && appName.trim().length() <= 0)
                appName = null;
            if(versionName != null && versionName.trim().length() <= 0)
                versionName = null;
            if(appName == null && !useDefaultApp)
            {
                errInfo = "Application name must be specified or \"use default\" must be selected.";
                setStatus(BundleImport.UPLOAD_FAILED);
            }
            if(versionName == null && !useDefaultVersion)
            {
                errInfo = "Version name must be specified or \"use default\" must be selected.";
                setStatus(BundleImport.UPLOAD_FAILED);
            }
            if(getStatus() == BundleImport.UPLOAD_FAILED)
            {
                System.err.println(errInfo);
                if(tempDir != null)
                    tempDir.delete();
                return;
            }
            if(storeDAR(servlet))
                setStatus(BundleImport.UPLOAD_SUCCEEDED);
            else
                setStatus(BundleImport.UPLOAD_FAILED);
        }

        public boolean storeDAR(String servletPath)
        {
            Vector args = new Vector();
            args.addElement("--url");
            args.addElement(((Object) (servletPath)));
            if(authData == null)
                return false;
            String encoded = authData.substring(authData.indexOf(" ") + 1);
            String decoded = Codecs.base64Decode(encoded);
            String adminUser = null;
            String adminPassword = null;
            int divider_pos = decoded.indexOf(":");
            if(divider_pos == -1)
            {
                adminUser = decoded;
                adminPassword = "";
            } else
            {
                adminUser = decoded.substring(0, divider_pos);
                adminPassword = decoded.substring(divider_pos + 1);
            }
            args.addElement("--user");
            args.addElement(((Object) (adminUser)));
            args.addElement("--password");
            args.addElement(((Object) (adminPassword)));
            String lowercaseFileName = darFile.getAbsolutePath().toLowerCase();
            if(lowercaseFileName.endsWith("war"))
                args.addElement("--war");
            else
            if(lowercaseFileName.endsWith("dar"))
            {
                args.addElement("--dar");
            } else
            {
                errInfo = "\"" + darFile.getAbsolutePath() + "\" is not a valid " + "dar or war file<br>";
                System.err.println(errInfo);
                cleanup();
                return false;
            }
            args.addElement(((Object) (darFile.getAbsolutePath())));
            if(appName != null && appName.trim().length() > 0)
            {
                appName = CheckNames.fixBundleName(appName);
                args.addElement("--bundle");
                args.addElement(((Object) (appName)));
            }
            if(versionName != null && versionName.trim().length() > 0)
            {
                versionName = CheckNames.fixVersionName(versionName);
                args.addElement("--version");
                args.addElement(((Object) (versionName)));
            }
            String cmd_args[] = new String[args.size()];
            for(int i = 0; i < args.size(); i++)
                cmd_args[i] = (String)args.elementAt(i);

            UploadDAR ud = new UploadDAR(cmd_args);
            try
            {
                ud.start();
            }
            catch(DarException de)
            {
                errInfo = "Exception loading DAR: " + ((Throwable) (de)).getMessage();
                System.err.println(errInfo);
                boolean flag = false;
                return flag;
            }
            finally
            {
                cleanup();
            }
            return true;
        }

        protected void cleanup()
        {
            if(darFile == null)
            {
                return;
            } else
            {
                File parent = new File(darFile.getParent());
                darFile.delete();
                parent.delete();
                return;
            }
        }

        public DarUploader()
        {
            this(String.valueOf((new Random()).nextLong()));
        }

        public DarUploader(String id)
        {
            status = BundleImport.UPLOAD_STARTING;
            uploadID = id;
        }
    }


    protected File tempDir;
    protected File darFile;
    protected String appName;
    protected String versionName;
    protected String servlet;
    protected String authData;
    protected boolean useDefaultApp;
    protected boolean useDefaultVersion;
    protected long startTime;
    protected String storedID;
    protected static Hashtable uploadTable = new Hashtable();
    public static int UPLOAD_FAILED = -1;
    public static int UPLOAD_STARTING = 0;
    public static int UPLOAD_IN_PROGRESS = 1;
    public static int UPLOAD_SUCCEEDED = 2;

    public BundleImport()
    {
        tempDir = null;
        darFile = null;
        appName = null;
        versionName = null;
        servlet = null;
        authData = null;
        useDefaultApp = false;
        useDefaultVersion = false;
        storedID = null;
    }

    public void refresh()
    {
        super.refresh();
        darFile = null;
        appName = null;
        versionName = null;
        useDefaultApp = false;
        useDefaultVersion = false;
    }

    public boolean validate()
    {
        return false;
    }

    public String loadDAR(HttpServletRequest request)
    {
        String uploadID = String.valueOf(((Object) (request)).hashCode());
        startTime = System.currentTimeMillis();
        try
        {
            parseUploadData(request);
        }
        catch(IOException ioe)
        {
            super.errMessage = "Error parsing upload: " + ((Throwable) (ioe)).getMessage();
            System.err.println(super.errMessage);
            return "0";
        }
        servlet = ((ServletRequest) (request)).getScheme() + "://" + ((ServletRequest) (request)).getServerName() + ":" + ((ServletRequest) (request)).getServerPort();
        String base = request.getContextPath() + request.getServletPath();
        if(base.length() == 0)
            base = "/";
        else
            base = base.substring(0, base.indexOf("admin"));
        servlet = servlet + base + "deploy";
        authData = request.getHeader("Authorization");
        DarUploader uploader = new DarUploader(uploadID);
        uploadTable.put(((Object) (uploadID)), ((Object) (uploader)));
        ((Thread) (uploader)).start();
        storedID = uploadID;
        return uploadID;
    }

    public String getUploadID()
    {
        return storedID;
    }

    public int checkDARStatus(String id)
    {
        DarUploader uploader = (DarUploader)uploadTable.get(((Object) (id)));
        if(uploader == null)
        {
            super.errMessage = "No upload request matching the given upload ID";
            System.err.println(super.errMessage);
            return UPLOAD_FAILED;
        }
        int status = uploader.getStatus();
        if(status != UPLOAD_IN_PROGRESS && status != UPLOAD_STARTING)
            uploadTable.remove(((Object) (id)));
        if(status == UPLOAD_FAILED)
        {
            super.errMessage = "Uploader Error: " + uploader.getErrorInfo();
            System.err.println(super.errMessage);
        }
        return status;
    }

    protected void parseUploadData(HttpServletRequest uploadData)
        throws IOException
    {
        MultipartParser parser = new MultipartParser(uploadData, 0x40000000);
        File tempDir = FileUtils.createTempFile();
        tempDir.mkdir();
        Part form_part = null;
        boolean found_file = false;
        try
        {
            while((form_part = parser.readNextPart()) != null) 
            {
                String name = form_part.getName();
                if(form_part.isParam())
                {
                    if(name != null && name.length() != 0)
                    {
                        String value = ((ParamPart)form_part).getStringValue();
                        if(value != null && value.length() != 0)
                            if(name.equals("appName"))
                                appName = value;
                            else
                            if(name.equals("versionName"))
                                versionName = value;
                            else
                            if(name.equals("useDefaultName"))
                                useDefaultApp = value.equalsIgnoreCase("true") || value.equalsIgnoreCase("on") || value.equalsIgnoreCase("yes");
                            else
                            if(name.equals("useDefaultVersion"))
                                useDefaultVersion = value.equalsIgnoreCase("true") || value.equalsIgnoreCase("on") || value.equalsIgnoreCase("yes");
                    }
                    continue;
                }
                if(!form_part.isFile())
                    continue;
                FilePart part = (FilePart)form_part;
                String file_name = part.getFileName();
                if(file_name == null || file_name.length() == 0)
                    continue;
                try
                {
                    part.writeTo(tempDir);
                }
                catch(IOException ioe)
                {
                    super.errMessage = "Error reading inputStream: \n" + ((Throwable) (ioe)).getMessage();
                    System.err.println(super.errMessage);
                    continue;
                }
                darFile = new File(tempDir, file_name);
                found_file = true;
            }
        }
        catch(IOException ioe)
        {
            if(tempDir != null)
                tempDir.delete();
            throw ioe;
        }
        if(!found_file)
        {
            super.errMessage = "No DAR file data found in upload.";
            throw new IOException("File part not found in form data.");
        } else
        {
            return;
        }
    }

    public int getWaitTime()
    {
        return (int)((System.currentTimeMillis() - startTime) / 1000L);
    }

    public void setFileName(String name)
    {
        darFile = new File(name);
    }

    public String getFileName()
    {
        return darFile.getName();
    }

    public void setAppName(String name)
    {
        appName = name;
    }

    public String getAppName()
    {
        return appName != null ? appName : "(unspecified)";
    }

    public void setVersionName(String name)
    {
        versionName = name;
    }

    public String getVersionName()
    {
        return versionName != null ? versionName : "(unspecified)";
    }

    public void setUseDefaultApp(boolean useDefault)
    {
        useDefaultApp = useDefault;
    }

    public boolean getUseDefaultApp()
    {
        return useDefaultApp;
    }

    public void setUseDefaultVersion(boolean useDefault)
    {
        useDefaultVersion = useDefault;
    }

    public boolean getUseDefaultVersion()
    {
        return useDefaultVersion;
    }

}
