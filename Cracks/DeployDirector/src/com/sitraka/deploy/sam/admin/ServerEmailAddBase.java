// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEmailAddBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            ServerEmailAddEditBase, JSPPropertiesSupport, JSPSupport

public abstract class ServerEmailAddBase extends ServerEmailAddEditBase
{

    public ServerEmailAddBase()
    {
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        ((JSPSupport)this).errMessage = "";
        boolean valid = true;
        if(super.address == null || super.address.trim().length() == 0)
        {
            ((JSPSupport)this).errMessage += "You must supply a destination address.<br>\n";
            valid = false;
        }
        int use_index = -1;
        int index = 0;
        String s;
        for(String p_name = "deploy.error.email." + index + ".address"; (s = ((JSPPropertiesSupport)this).properties.getProperty(p_name)) != null; p_name = "deploy.error.email." + index + ".address")
        {
            if(s.equals(((Object) (super.address))))
            {
                use_index = index;
                break;
            }
            if(s.trim().length() == 0 && use_index == -1)
                use_index = index;
            index++;
        }

        if(use_index == -1)
            use_index = index;
        String notification = ServerEmailAddEditBase.getNotificationString(super.errorLevels);
        if(notification == null || notification.trim().length() == 0)
        {
            notification = "";
            super.address = "";
        }
        ((JSPPropertiesSupport)this).properties.setProperty("deploy.error.email." + use_index + ".notification", notification);
        ((JSPPropertiesSupport)this).properties.setProperty("deploy.error.email." + use_index + ".address", super.address);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
    }
}
