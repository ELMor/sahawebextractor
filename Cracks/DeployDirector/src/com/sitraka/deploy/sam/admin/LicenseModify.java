// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LicenseModify.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.PropertyUtils;
import com.sitraka.licensing.LicenseProperties;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public class LicenseModify extends JSPPropertiesSupport
{

    protected String serialNumber;
    protected String licenseExpiry;
    protected String maxBundles;
    protected String maxClients;
    protected String hostNames;
    protected String appNames;
    protected String licenseSignature;
    protected String type;
    protected String product;
    protected String version;
    protected String sitrakaVersion;

    public LicenseModify()
    {
        serialNumber = null;
        licenseExpiry = null;
        maxBundles = null;
        maxClients = null;
        hostNames = null;
        appNames = null;
        licenseSignature = null;
        type = null;
        product = null;
        version = null;
        sitrakaVersion = null;
    }

    public void refresh()
    {
        super.refresh();
        serialNumber = null;
        licenseExpiry = null;
        maxBundles = null;
        maxClients = null;
        hostNames = null;
        appNames = null;
        licenseSignature = null;
        type = null;
        product = null;
        version = null;
        sitrakaVersion = null;
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        Servlet servlet = ((JSPSupport)this).getServlet();
        if(servlet == null)
            return false;
        AppVault vault = servlet.getVault();
        int numApplications = vault.countApplications();
        LicenseProperties licenseProps = new LicenseProperties();
        if(serialNumber == null || serialNumber.length() == 0)
        {
            ((JSPSupport)this).errMessage = "No serial number given.";
            return false;
        }
        ((Properties) (licenseProps)).setProperty("serial_number", serialNumber);
        if(hostNames == null || hostNames.length() == 0)
        {
            ((JSPSupport)this).errMessage = "No host names specified.";
            return false;
        }
        ((Properties) (licenseProps)).setProperty("hosts", hostNames);
        int hostResult = licenseProps.validateHost("hosts");
        if(hostResult != 1)
        {
            if(hostResult == 12)
            {
                ((JSPSupport)this).errMessage = "Hosts property not given.";
                return false;
            }
            if(hostResult == 3)
            {
                ((JSPSupport)this).errMessage = "Number of CPUs exceeded.";
                return false;
            }
            if(hostResult == 2)
            {
                ((JSPSupport)this).errMessage = "Hostname is invalid.";
                return false;
            } else
            {
                ((JSPSupport)this).errMessage = "Unknown hostname error.";
                return false;
            }
        }
        int num_clients = -1;
        if(maxClients != null && maxClients.length() > 0)
        {
            ((Properties) (licenseProps)).setProperty("clients", maxClients);
            try
            {
                num_clients = Integer.parseInt(maxClients);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (maxClients))))
                {
                    num_clients = -1;
                } else
                {
                    ((JSPSupport)this).errMessage = "Maximum number of clients incorrectly specified or missing: " + maxClients;
                    return false;
                }
            }
        }
        int client_count = 0;
        try
        {
            client_count = servlet.getLogQueue().getClientDataLogger().getClients(0xffffffffcf7c5800L);
        }
        catch(LogException le) { }
        catch(NullPointerException npe) { }
        if(num_clients > 0 && client_count > num_clients)
        {
            ((JSPSupport)this).errMessage = "number of current clients (" + client_count + ") exceeds maximum allowed by license (" + num_clients + ")";
            return false;
        }
        if(licenseSignature == null || licenseSignature.length() == 0)
        {
            ((JSPSupport)this).errMessage = "License signature is missing.";
            return false;
        }
        ((Properties) (licenseProps)).setProperty("sitraka.license.signature", licenseSignature);
        boolean checkNumberOfBundles = true;
        Vector applications;
        if(appNames == null || appNames.length() == 0)
            applications = new Vector();
        else
        if(appNames.trim().length() == 0 || appNames.indexOf("(unlimited)") >= 0)
        {
            ((Properties) (licenseProps)).setProperty("applications", appNames);
            applications = new Vector();
        } else
        {
            ((Properties) (licenseProps)).setProperty("applications", appNames);
            applications = PropertyUtils.readVector(((Properties) (licenseProps)), "applications", ',');
            if(applications == null)
                applications = new Vector();
            else
                checkNumberOfBundles = false;
        }
        int num_apps = -1;
        if(maxBundles != null && maxBundles.length() > 0)
        {
            ((Properties) (licenseProps)).setProperty("bundles", maxBundles);
            try
            {
                num_apps = Integer.parseInt(maxBundles);
            }
            catch(NumberFormatException e)
            {
                if("(unlimited)".equals(((Object) (maxBundles))))
                {
                    num_apps = -1;
                } else
                {
                    ((JSPSupport)this).errMessage = "Maximum number of bundles incorrectly specified or missing: " + maxBundles;
                    return false;
                }
            }
        }
        if(checkNumberOfBundles && num_apps > 0 && numApplications > num_apps)
        {
            ((JSPSupport)this).errMessage = "number of user bundles (" + numApplications + ") exceeds maximum allowed by license (" + num_apps + ")";
            return false;
        }
        if(licenseExpiry != null && licenseExpiry.length() > 0 && !"(never)".equals(((Object) (licenseExpiry))))
        {
            ((Properties) (licenseProps)).setProperty("expiry", licenseExpiry);
            if(licenseProps.isDateExpired("expiry"))
            {
                ((JSPSupport)this).errMessage = "License has expired: " + licenseExpiry;
                return false;
            }
        }
        if(!"2.5".equals(((Object) (version))))
        {
            ((JSPSupport)this).errMessage = "DD Version is incompatible: " + version + " vs " + "2.5";
            return false;
        }
        ((Properties) (licenseProps)).setProperty("license_version", version);
        if(sitrakaVersion == null || sitrakaVersion.length() == 0)
        {
            ((JSPSupport)this).errMessage = "Licensing version is missing.";
            return false;
        }
        ((Properties) (licenseProps)).setProperty("sitraka.license.version", sitrakaVersion);
        if(!"department".equals(((Object) (type))) && !"developer".equals(((Object) (type))) && !"enterprise".equals(((Object) (type))) && !"evaluation".equals(((Object) (type))))
        {
            ((JSPSupport)this).errMessage = "License Type is invalid: " + type;
            return false;
        }
        ((Properties) (licenseProps)).setProperty("type", type);
        Class centinal;
        if("department".equals(((Object) (type))) || "developer".equals(((Object) (type))) || "evaluation".equals(((Object) (type))))
            try
            {
                centinal = Class.forName("org.apache.tomcat.core.Security");
            }
            catch(ClassNotFoundException cnfe)
            {
                ((JSPSupport)this).errMessage = "When using a " + type + " license, " + "DeployDirector must be run with the standalone " + "Tomcat Server shipped by Sitraka.";
                return false;
            }
        if(product == null || product.length() == 0)
        {
            ((JSPSupport)this).errMessage = "Required license field \"product\" is invalid '" + product + "'";
            return false;
        }
        ((Properties) (licenseProps)).setProperty("product", product);
        licenseProps.validate();
        boolean result = licenseProps.isValid();
        if(!result)
        {
            ((JSPSupport)this).errMessage = "Data does not match signature";
            return false;
        }
        super.properties.setProperty("applications", appNames);
        super.properties.setProperty("bundles", maxBundles);
        super.properties.setProperty("clients", maxClients);
        super.properties.setProperty("expiry", licenseExpiry);
        super.properties.setProperty("hosts", hostNames);
        super.properties.setProperty("license_version", version);
        super.properties.setProperty("product", product);
        super.properties.setProperty("serial_number", serialNumber);
        super.properties.setProperty("sitraka.license.signature", licenseSignature);
        super.properties.setProperty("type", type);
        super.properties.setProperty("sitraka.license.version", sitrakaVersion);
        String key;
        for(Enumeration key_enum = ((Hashtable) (licenseProps)).keys(); key_enum.hasMoreElements(); ((Hashtable) (super.properties)).put(((Object) (key)), ((Object) (((Properties) (licenseProps)).getProperty(key)))))
            key = (String)key_enum.nextElement();

        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    public void setSerialNumber(String serial)
    {
        serialNumber = serial;
    }

    public void setLicenseExpiry(String expiry)
    {
        if(expiry != null && expiry.equalsIgnoreCase("(never)"))
            expiry = "";
        licenseExpiry = expiry;
    }

    public void setMaxBundles(String bundles)
    {
        if(bundles != null && bundles.equalsIgnoreCase("(unlimited)"))
            bundles = "-1";
        maxBundles = bundles;
    }

    public void setMaxClients(String clients)
    {
        if(clients != null && clients.equalsIgnoreCase("(unlimited)"))
            clients = "-1";
        maxClients = clients;
    }

    public void setHostNames(String hosts)
    {
        hostNames = hosts;
    }

    public void setAppNames(String applications)
    {
        if(applications != null && applications.equalsIgnoreCase("(unlimited)"))
            applications = "";
        appNames = applications;
    }

    public void setLicenseSignature(String signature)
    {
        licenseSignature = signature;
    }

    public void setLicenseType(String type)
    {
        this.type = type;
    }

    public void setProductName(String name)
    {
        product = name;
    }

    public void setLicenseVerison(String version)
    {
        this.version = version;
    }

    public void setSitrakaVerison(String version)
    {
        sitrakaVersion = version;
    }
}
