// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JSPPropertiesSupport.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.common.checkpoint.ConfigObject;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.worker.AbstractWorker;
import com.sitraka.deploy.sam.worker.ReplicationThread;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.MissingResourceException;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public abstract class JSPPropertiesSupport extends JSPSupport
{

    protected Properties properties;
    protected File propFile;

    public JSPPropertiesSupport()
    {
        properties = null;
        propFile = null;
    }

    public void refresh()
    {
        super.refresh();
        properties = null;
    }

    protected void initializeProperties()
    {
        initializeProperties(true);
    }

    protected void initializeProperties(boolean loadExisting)
    {
        if(properties != null)
            return;
        try
        {
            loadProperties();
        }
        catch(MissingResourceException mre)
        {
            return;
        }
        if(loadExisting)
            getExistingValues();
    }

    protected void loadProperties()
    {
        if(properties != null)
            return;
        Servlet servlet = ((JSPSupport)this).getServlet();
        if(servlet == null)
            throw new MissingResourceException("Error communicating with servlet.", "com.sitraka.deploy.sam.servlet.Servlet", "DeployDirector servlet");
        File vaultBase = servlet.getVault().getVaultBaseDir();
        propFile = new File(vaultBase, getPropFile());
        properties = new Properties();
        try
        {
            properties.load(((java.io.InputStream) (new FileInputStream(propFile))));
        }
        catch(IOException ioe)
        {
            super.errMessage = "Unable to read " + getPropFile() + " from the server";
            properties = null;
            throw new MissingResourceException("Can't read " + getPropFile(), "java.util.Properties", "DeployDirector " + getPropFile());
        }
    }

    protected abstract void getExistingValues();

    protected boolean storeProperties()
    {
        try
        {
            properties.store(((java.io.OutputStream) (new FileOutputStream(propFile))), "Administrator modified file from admin pages");
        }
        catch(IOException ioe)
        {
            super.errMessage = "Unable to rewrite properties file \"" + propFile.getAbsolutePath() + "\"<br>\n" + ((Throwable) (ioe)).getMessage() + "<br>";
            return false;
        }
        Servlet servlet = ((JSPSupport)this).getServlet();
        if("cluster.properties".equals(((Object) (getPropFile()))) && servlet.getVault().listOtherServers() != null)
        {
            String localhost = Servlet.properties.getProperty("deploy.localhost");
            AbstractWorker.getReplicationQueue(((com.sitraka.deploy.sam.ServerActions) (servlet))).sendConfigReplication(localhost, new ConfigObject(localhost), ((Object) (super.authData)));
        }
        return true;
    }

    protected abstract String getPropFile();
}
