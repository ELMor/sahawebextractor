// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerServerServers.java

package com.sitraka.deploy.sam.admin;

import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            ServerHostBase

public class ServerServerServers extends ServerHostBase
{

    public ServerServerServers()
    {
    }

    public Vector getHosts()
    {
        return ((ServerHostBase)this).getHostGroup("server");
    }

    protected String getPropFile()
    {
        return "server.properties";
    }
}
