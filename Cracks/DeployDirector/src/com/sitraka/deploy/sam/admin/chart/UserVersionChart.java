// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UserVersionChart.java

package com.sitraka.deploy.sam.admin.chart;

import com.klg.jclass.chart.ChartDataView;
import com.klg.jclass.chart.JCChart;
import com.klg.jclass.chart.data.JCDefaultDataSource;
import com.klg.jclass.schart.JCServerChart;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.log.ClientDataLogger;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package com.sitraka.deploy.sam.admin.chart:
//            AbstractChart, ChartException

public class UserVersionChart extends AbstractChart
{

    public UserVersionChart(HttpServletRequest request, HttpServletResponse response, String extension)
        throws ChartException
    {
        super(request, response, extension, "userVersion.xml");
        ((AbstractChart)this).extractBundle();
        ((AbstractChart)this).extractVersionList();
    }

    public void generateChart()
        throws ChartException
    {
        String bundle = ((AbstractChart)this).getBundle();
        AppVault vault = Servlet.getUniqueInstance().getVault();
        if(bundle == null)
            bundle = vault.listBundles()[1];
        String versions[] = ((AbstractChart)this).getVersionList();
        if(versions == null)
        {
            Application app = vault.getApplication(bundle);
            if(app == null)
                throw new ChartException("Chart Error: Unable to find provided bundle: " + bundle, 400);
            Vector verList = app.listVersions();
            versions = new String[verList.size()];
            for(int i = 0; i < versions.length; i++)
                versions[i] = ((Version)verList.elementAt(i)).getName();

        }
        double users[][] = new double[1][versions.length];
        JCDefaultDataSource ds = null;
        try
        {
            ClientDataLogger logger = Servlet.getUniqueInstance().getLogQueue().getClientDataLogger();
            for(int i = 0; i < versions.length; i++)
                users[0][i] = logger.queryUsersForBundleVersion(bundle, versions[i], super.startTimestamp, super.endTimestamp);

            ds = new JCDefaultDataSource(((double [][]) (null)), users, versions, versions, "Bundle Users Source");
        }
        catch(LogException le)
        {
            throw new ChartException("Chart Error: Reading Logs: " + ((Throwable) (le)).getMessage(), 500);
        }
        ((JCChart) (super.chart)).getDataView(0).setDataSource(((com.klg.jclass.chart.ChartDataModel) (ds)));
        try
        {
            if(super.extension == null)
            {
                ((ServletResponse) (super.response)).setContentType("image/png");
                super.chart.encodeAsPNG(((OutputStream) (((ServletResponse) (super.response)).getOutputStream())));
            } else
            if(!"imagemap".equals(((Object) (super.extension))));
            ((OutputStream) (((ServletResponse) (super.response)).getOutputStream())).flush();
        }
        catch(IOException ioe)
        {
            throw new ChartException("Chart Error: IOException generating chart.", 500);
        }
    }
}
