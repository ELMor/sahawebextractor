// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerServerMisc.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport

public class ServerServerMisc extends JSPPropertiesSupport
{

    protected String deployCompressClient;
    protected String deployCompressServer;
    protected String deployHttpSslsocketfactory;
    protected String deployHttpTimeout;
    protected String deployHttpClientTimeout;
    protected String deployHttpClientTimeoutDefault;
    protected String deployServerCacheMaxage;
    protected String deployServerCacheSize;
    public String deployServerName;

    public ServerServerMisc()
    {
        deployCompressClient = "";
        deployCompressServer = "";
        deployHttpSslsocketfactory = "";
        deployHttpTimeout = "";
        deployHttpClientTimeout = "";
        deployHttpClientTimeoutDefault = "";
        deployServerCacheMaxage = "";
        deployServerCacheSize = "";
        deployServerName = "";
    }

    public void refresh()
    {
        super.refresh();
        deployCompressClient = "";
        deployCompressServer = "";
        deployHttpSslsocketfactory = "";
        deployHttpClientTimeout = "";
        deployHttpTimeout = "";
        deployHttpClientTimeoutDefault = "";
        deployServerCacheMaxage = "";
        deployServerCacheSize = "";
        deployServerName = "";
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties(false);
        if(deployCompressClient != null && deployCompressClient.length() > 0)
            super.properties.setProperty("deploy.compress.client", deployCompressClient);
        if(deployCompressServer != null && deployCompressServer.length() > 0)
            super.properties.setProperty("deploy.compress.server", deployCompressServer);
        if(deployHttpSslsocketfactory != null && deployHttpSslsocketfactory.length() > 0)
            super.properties.setProperty("deploy.http.sslsocketfactory", deployHttpSslsocketfactory);
        if(deployHttpClientTimeout != null && deployHttpClientTimeout.length() > 0)
            super.properties.setProperty("deploy.http.client.timeout", deployHttpClientTimeout);
        if(deployHttpClientTimeoutDefault != null && deployHttpClientTimeoutDefault.length() > 0)
            super.properties.setProperty("deploy.http.client.timeout.default", deployHttpClientTimeoutDefault);
        else
            super.properties.setProperty("deploy.http.client.timeout.default", "");
        if(deployHttpTimeout != null && deployHttpTimeout.length() > 0)
            super.properties.setProperty("deploy.http.timeout", deployHttpTimeout);
        if(deployServerCacheMaxage != null && deployServerCacheMaxage.length() > 0)
            super.properties.setProperty("deploy.server.cache.maxage", deployServerCacheMaxage);
        if(deployServerCacheSize != null && deployServerCacheSize.length() > 0)
            super.properties.setProperty("deploy.server.cache.size", deployServerCacheSize);
        if(deployServerName != null && deployServerName.length() > 0)
            super.properties.setProperty("deploy.server.name", deployServerName);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
        deployCompressClient = super.properties.getProperty("deploy.compress.client", "");
        deployCompressServer = super.properties.getProperty("deploy.compress.server", "");
        deployHttpSslsocketfactory = super.properties.getProperty("deploy.http.sslsocketfactory", "");
        deployHttpClientTimeout = super.properties.getProperty("deploy.http.client.timeout", "");
        deployHttpClientTimeoutDefault = super.properties.getProperty("deploy.http.client.timeout.default", "");
        deployHttpTimeout = super.properties.getProperty("deploy.http.timeout", "");
        deployServerCacheMaxage = super.properties.getProperty("deploy.server.cache.maxage", "");
        deployServerCacheSize = super.properties.getProperty("deploy.server.cache.size", "");
        deployServerName = super.properties.getProperty("deploy.server.name", "");
    }

    protected String getPropFile()
    {
        return "server.properties";
    }

    public void setDeployCompressClient(String level)
    {
        deployCompressClient = level;
    }

    public String getDeployCompressClient()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployCompressClient;
    }

    public void setDeployCompressServer(String level)
    {
        deployCompressServer = level;
    }

    public String getDeployCompressServer()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployCompressServer;
    }

    public void setDeployHttpSslsocketfactory(String factory)
    {
        deployHttpSslsocketfactory = factory;
    }

    public String getDeployHttpSslsocketfactory()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployHttpSslsocketfactory;
    }

    public void setDeployHttpClientTimeout(String timeout)
    {
        deployHttpClientTimeout = timeout;
    }

    public String getDeployHttpClientTimeout()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployHttpClientTimeout;
    }

    public void setDeployHttpClientTimeoutDefault(String timeout)
    {
        deployHttpClientTimeoutDefault = timeout;
    }

    public String getDeployHttpClientTimeoutDefault()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployHttpClientTimeoutDefault;
    }

    public void setDeployHttpTimeout(String timeout)
    {
        deployHttpTimeout = timeout;
    }

    public String getDeployHttpTimeout()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployHttpTimeout;
    }

    public void setDeployServerCacheMaxage(String age)
    {
        deployServerCacheMaxage = age;
    }

    public String getDeployServerCacheMaxage()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployServerCacheMaxage;
    }

    public void setDeployServerCacheSize(String size)
    {
        deployServerCacheSize = size;
    }

    public String getDeployServerCacheSize()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployServerCacheSize;
    }

    public void setDeployServerName(String name)
    {
        deployServerName = name;
    }

    public String getDeployServerName()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployServerName;
    }
}
