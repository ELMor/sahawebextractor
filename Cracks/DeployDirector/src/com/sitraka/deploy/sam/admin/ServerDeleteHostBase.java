// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerDeleteHostBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public abstract class ServerDeleteHostBase extends JSPPropertiesSupport
{

    protected String removedHost;

    public ServerDeleteHostBase()
    {
        removedHost = null;
    }

    public void refresh()
    {
        super.refresh();
        removedHost = null;
    }

    public boolean validate()
    {
        return true;
    }

    protected void getExistingValues()
    {
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    protected abstract String getGroupName();

    public void removeHost(String name)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        String prop_base = "deploy." + getGroupName() + ".host.";
        int index = 0;
        String prop_name = prop_base + index + ".machine";
        String prop_value;
        for(prop_value = super.properties.getProperty(prop_name); prop_value != null && !prop_value.equals(((Object) (name))); prop_value = super.properties.getProperty(prop_name))
        {
            index++;
            prop_name = prop_base + index + ".machine";
        }

        if(prop_value == null)
        {
            ((JSPSupport)this).errMessage = "Unable to find server \"" + name + "\"";
            return;
        }
        for(prop_name = prop_base + (index + 1) + ".machine"; super.properties.getProperty(prop_name) != null; prop_name = prop_base + (index + 1) + ".machine")
        {
            super.properties.setProperty(prop_base + index + ".machine", super.properties.getProperty(prop_base + (index + 1) + ".machine"));
            super.properties.setProperty(prop_base + index + ".port", super.properties.getProperty(prop_base + (index + 1) + ".port"));
            super.properties.setProperty(prop_base + index + ".protocol", super.properties.getProperty(prop_base + (index + 1) + ".protocol"));
            super.properties.setProperty(prop_base + index + ".page", super.properties.getProperty(prop_base + (index + 1) + ".page"));
            index++;
        }

        if(index > 0)
        {
            ((Hashtable) (super.properties)).remove(((Object) (prop_base + index + ".machine")));
            ((Hashtable) (super.properties)).remove(((Object) (prop_base + index + ".page")));
            ((Hashtable) (super.properties)).remove(((Object) (prop_base + index + ".port")));
            ((Hashtable) (super.properties)).remove(((Object) (prop_base + index + ".protocol")));
        } else
        {
            super.properties.setProperty(prop_base + index + ".machine", "");
            super.properties.setProperty(prop_base + index + ".port", "8080");
            super.properties.setProperty(prop_base + index + ".protocol", "http");
            super.properties.setProperty(prop_base + index + ".page", "/servlet/deploy");
        }
        removedHost = name;
        ((JSPPropertiesSupport)this).storeProperties();
    }

    public String getRemovedHost()
    {
        return removedHost;
    }
}
