// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LicenseUpload.java

package com.sitraka.deploy.sam.admin;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.Part;
import com.sitraka.deploy.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import javax.servlet.http.HttpServletRequest;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, LicenseWorker, JSPSupport

public class LicenseUpload extends JSPPropertiesSupport
{

    protected File importFile;

    public LicenseUpload()
    {
    }

    public void refresh()
    {
        super.refresh();
        importFile = null;
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        boolean valid = true;
        LicenseWorker lw = new LicenseWorker();
        lw.refresh();
        lw.setFile(importFile.getAbsolutePath());
        if(!lw.validate())
        {
            ((JSPSupport)this).errMessage = ((JSPSupport) (lw)).getErrMessage();
            System.err.println("License was not valid");
            System.err.println(((JSPSupport)this).errMessage);
            valid = false;
        }
        return valid;
    }

    public boolean loadLicense(HttpServletRequest request)
    {
        MultipartParser parser = null;
        try
        {
            parser = new MultipartParser(request, 32768);
        }
        catch(IOException ioe)
        {
            ((JSPSupport)this).errMessage = "Error creating parser:\n" + ((Throwable) (ioe)).getMessage();
            System.err.println(((JSPSupport)this).errMessage);
            return false;
        }
        File temp_dir = FileUtils.createTempFile();
        temp_dir.mkdir();
        Part form_part = null;
        boolean found_file = false;
        try
        {
            while((form_part = parser.readNextPart()) != null) 
            {
                String name = form_part.getName();
                if(form_part.isParam())
                {
                    System.err.println("Unrecognised parameter: name = " + name);
                    continue;
                }
                if(!form_part.isFile())
                    continue;
                FilePart part = (FilePart)form_part;
                String file_name = part.getFileName();
                if(file_name == null || file_name.length() == 0)
                    continue;
                try
                {
                    part.writeTo(temp_dir);
                }
                catch(IOException ioe)
                {
                    ((JSPSupport)this).errMessage = "Error reading inputStream: \n" + ((Throwable) (ioe)).getMessage();
                    System.err.println(((JSPSupport)this).errMessage);
                    continue;
                }
                importFile = new File(temp_dir, file_name);
                found_file = true;
            }
        }
        catch(IOException ioe)
        {
            ((JSPSupport)this).errMessage = "Error reading multipart data: \n" + ((Throwable) (ioe)).getMessage();
            return false;
        }
        if(!found_file)
        {
            ((JSPSupport)this).errMessage = "No license file data found in upload.";
            return false;
        } else
        {
            boolean status = validate();
            importFile.delete();
            temp_dir.delete();
            return status;
        }
    }

    protected void getExistingValues()
    {
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    public void setImportFile(String infile)
    {
        importFile = new File(infile);
    }

    public String getImportFile()
    {
        if(importFile == null)
            return "unknown";
        else
            return importFile.getName();
    }
}
