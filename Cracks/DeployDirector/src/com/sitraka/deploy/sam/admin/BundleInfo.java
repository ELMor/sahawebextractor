// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleInfo.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public class BundleInfo extends JSPPropertiesSupport
{

    protected String servletPage;

    public BundleInfo()
    {
        servletPage = null;
    }

    public void refresh()
    {
        super.refresh();
        servletPage = null;
    }

    public boolean validate()
    {
        return true;
    }

    protected void getExistingValues()
    {
        servletPage = super.properties.getProperty("deploy.cluster.host.0.page");
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    protected Vector setupBundleList()
    {
        if(((JSPSupport)this).getServlet() == null)
            return null;
        AppVault vault = ((JSPSupport)this).getServlet().getVault();
        String bundleList[] = vault.listApplications();
        Vector bundles = new Vector();
        for(int i = 0; i < bundleList.length; i++)
        {
            String bundle_name;
            int location;
            if((location = bundleList[i].indexOf(',')) != -1)
                bundle_name = bundleList[i].substring(0, location);
            else
                bundle_name = bundleList[i];
            bundles.addElement(((Object) (bundle_name)));
        }

        return bundles;
    }

    public String getBundleList()
    {
        String list_data = "";
        Vector bundles = setupBundleList();
        if(bundles == null)
            return list_data;
        for(int i = 0; i < bundles.size(); i++)
        {
            String value = (String)bundles.elementAt(i);
            list_data = list_data + "<option value=\"" + value + "\">" + value + "</option>\n";
        }

        return list_data;
    }

    public Vector getBundleListVector()
    {
        return setupBundleList();
    }

    protected Vector setupVersionList(String bundleName)
    {
        if(((JSPSupport)this).getServlet() == null)
            return null;
        AppVault vault = ((JSPSupport)this).getServlet().getVault();
        Application app = vault.getApplication(bundleName);
        if(app == null)
        {
            ((JSPSupport)this).errMessage = "The application \"" + bundleName + "\" was not found in the vault.";
            return null;
        }
        Vector version_list = app.listVersions();
        Vector name_list = new Vector();
        for(int i = 0; i < version_list.size(); i++)
        {
            Version ver = (Version)version_list.elementAt(i);
            name_list.addElement(((Object) (ver.getName())));
        }

        return name_list;
    }

    public String getVersionList(String bundleName)
    {
        String list_data = "";
        Vector versions = setupVersionList(bundleName);
        if(versions == null)
            return list_data;
        for(int i = 0; i < versions.size(); i++)
        {
            String value = (String)versions.elementAt(i);
            list_data = list_data + "<option value=\"" + value + "\">" + value + "</option>\n";
        }

        return list_data;
    }

    public Vector getVersionListVector(String bundleName)
    {
        return setupVersionList(bundleName);
    }

    public String getCreationDate(String bundleName)
    {
        if(((JSPSupport)this).getServlet() == null)
            return null;
        AppVault vault = ((JSPSupport)this).getServlet().getVault();
        Application app = vault.getApplication(bundleName);
        Date date = app.getCreationDate();
        if(date == null)
            return "Unset";
        else
            return date.toString();
    }

    public String getLinks(String server, String bundleName)
    {
        if(bundleName.equals("DDCAM"))
        {
            return "";
        } else
        {
            String serverPage = getRootPage(server);
            String install_link = serverPage + "/" + bundleName + "/install";
            String output = "(<a href=\"" + install_link + "\">install</a>)";
            return output;
        }
    }

    protected String getRootPage(String serverName)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        String machine = null;
        int count = -1;
        do
        {
            count++;
            machine = super.properties.getProperty("deploy.server.host." + count + ".machine");
        } while(machine != null && machine.trim().length() > 0 && !machine.equals(((Object) (serverName))));
        if(machine != null && machine.trim().length() > 0)
            return super.properties.getProperty("deploy.server.host." + count + ".page");
        count = -1;
        do
        {
            count++;
            machine = super.properties.getProperty("deploy.cluster.host." + count + ".machine");
        } while(machine != null && machine.trim().length() > 0 && !machine.equals(((Object) (serverName))));
        if(machine == null || machine.trim().length() == 0)
            return super.properties.getProperty("deploy.cluster.host.0.page");
        else
            return super.properties.getProperty("deploy.cluster.host." + count + ".page");
    }
}
