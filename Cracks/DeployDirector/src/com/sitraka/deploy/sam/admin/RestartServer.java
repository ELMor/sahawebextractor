// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RestartServer.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.servlet.Servlet;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class RestartServer extends JSPSupport
{

    public RestartServer()
    {
    }

    public boolean validate()
    {
        return true;
    }

    public void restart()
    {
        if(((JSPSupport)this).getServlet() == null)
        {
            return;
        } else
        {
            ((JSPSupport)this).getServlet().restartServer();
            return;
        }
    }
}
