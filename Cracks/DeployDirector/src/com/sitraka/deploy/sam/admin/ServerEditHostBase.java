// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEditHostBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, JSPSupport

public abstract class ServerEditHostBase extends JSPPropertiesSupport
{

    protected int hostToEdit;
    protected String host;
    protected String accessProtocol;
    protected String accessPort;
    protected String rootPage;

    public ServerEditHostBase()
    {
        hostToEdit = -1;
        host = null;
        accessProtocol = null;
        accessPort = null;
        rootPage = null;
    }

    public void refresh()
    {
        super.refresh();
        host = null;
        accessProtocol = null;
        accessPort = null;
        rootPage = null;
        hostToEdit = -1;
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties(false);
        if(hostToEdit == -1)
            findHostProperties();
        String prop_base = "deploy." + getGroupName() + ".host." + hostToEdit;
        super.properties.setProperty(prop_base + ".machine", host);
        super.properties.setProperty(prop_base + ".page", rootPage);
        super.properties.setProperty(prop_base + ".port", accessPort);
        super.properties.setProperty(prop_base + ".protocol", accessProtocol);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    protected void getExistingValues()
    {
        if(hostToEdit == -1)
            return;
        String prop_base = "deploy." + getGroupName() + ".host." + hostToEdit;
        if(accessProtocol == null || accessProtocol.trim().length() == 0)
            accessProtocol = super.properties.getProperty(prop_base + ".protocol", "");
        if(accessPort == null || accessPort.trim().length() == 0)
            accessPort = super.properties.getProperty(prop_base + ".port", "");
        if(rootPage == null || rootPage.trim().length() == 0)
            rootPage = super.properties.getProperty(prop_base + ".page", "");
    }

    protected abstract String getGroupName();

    protected void findHostProperties()
    {
        if(host == null)
        {
            ((JSPSupport)this).errMessage = "Can't retrieve properties for an unknown host";
            host = "";
        }
        String prop_base = "deploy." + getGroupName() + ".host.";
        hostToEdit = 0;
        String prop_name = prop_base + hostToEdit + ".machine";
        String prop_value;
        for(prop_value = super.properties.getProperty(prop_name); prop_value != null && !host.equals(((Object) (prop_value))); prop_value = super.properties.getProperty(prop_name))
        {
            hostToEdit++;
            prop_name = prop_base + hostToEdit + ".machine";
        }

        if(prop_value == null)
        {
            if(host.length() != 0)
                ((JSPSupport)this).errMessage = "No server matching name \"" + host + "\" was found.";
            return;
        } else
        {
            return;
        }
    }

    public void setHost(String name)
    {
        host = name;
        ((JSPPropertiesSupport)this).initializeProperties(false);
        findHostProperties();
        getExistingValues();
    }

    public String getHost()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return host;
    }

    public void setAccessProtocol(String protocol)
    {
        accessProtocol = protocol;
    }

    public String getAccessProtocol(String hostName)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return accessProtocol;
    }

    public void setAccessPort(String port)
    {
        accessPort = port;
    }

    public String getAccessPort(String hostName)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return accessPort;
    }

    public void setRootPage(String path)
    {
        rootPage = path;
    }

    public String getRootPage(String hostName)
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return rootPage;
    }
}
