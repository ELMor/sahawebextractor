// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PageHeader.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.common.roles.BundleAdminRole;
import com.sitraka.deploy.common.roles.Role;
import com.sitraka.deploy.common.roles.RoleAuthorization;
import com.sitraka.deploy.common.roles.ServerAdminRole;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.servlet.AuthInfo;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.util.Vector;

public class PageHeader
{

    public PageHeader()
    {
    }

    public boolean touchServer(String proto, String server, int port, String servlet)
    {
        if(Servlet.getUniqueInstance() != null)
        {
            return true;
        } else
        {
            String base = servlet.substring(0, servlet.indexOf("admin")) + "deploy";
            String servletURL = proto + "://" + server + ":" + port + base;
            HttpConnection conn = new HttpConnection();
            ((AbstractHttpConnection) (conn)).setServer(servletURL);
            ((AbstractHttpConnection) (conn)).setCommand("SERVERS");
            String response = ((AbstractHttpConnection) (conn)).executeTextCommand();
            return ((AbstractHttpConnection) (conn)).isConnected();
        }
    }

    public boolean isAuthorized(String auth)
    {
        return isAuthorized(((String []) (null)), auth);
    }

    public boolean isAuthorized(String context[], String auth)
    {
        if(auth == null)
            return false;
        String app = "DDAdmin";
        String ver = "";
        Servlet servlet = Servlet.getUniqueInstance();
        AppVault vault = servlet.getVault();
        if(auth.startsWith("Basic "))
            auth = auth.substring("Basic ".length());
        String decoded = AuthInfo.decodeUser(((Object) (auth)));
        Authentication authenticate = vault.getAuthenticationObject(app, ver);
        if(!authenticate.isAuthentic(((Object) (decoded))))
        {
            return false;
        } else
        {
            String user = decoded.substring(0, decoded.indexOf(":"));
            RoleAuthorization authorize = (RoleAuthorization)vault.getAuthorizationObject(app, ver);
            Role roles[] = getRoles(context);
            int is_authorized = authorize.isRoleAuthorized(user, roles);
            return is_authorized == 1;
        }
    }

    protected Role[] getRoles(String context[])
    {
        Vector role_list = new Vector();
        for(int i = 0; i < context.length; i++)
            if(context[i] != null && !context[i].equals("Admin") && context[i].startsWith("Bundle"))
            {
                String bundle = "any";
                if(context[i].indexOf(":") != -1)
                    bundle = context[i].substring(context[i].indexOf(":") + 1);
                role_list.addElement(((Object) (new BundleAdminRole(bundle))));
            }

        if(role_list.size() == 0)
            role_list.addElement(((Object) (new ServerAdminRole())));
        Role roles[] = new Role[role_list.size()];
        for(int i = 0; i < role_list.size(); i++)
            roles[i] = (Role)role_list.elementAt(i);

        return roles;
    }
}
