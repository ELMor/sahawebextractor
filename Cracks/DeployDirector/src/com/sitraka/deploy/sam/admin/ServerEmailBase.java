// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEmailBase.java

package com.sitraka.deploy.sam.admin;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport, EmailEntry, BundleInfo

public abstract class ServerEmailBase extends JSPPropertiesSupport
{

    protected Vector addresses;
    protected static int LEVEL_COUNT = 5;
    protected static String LEVEL_NAMES[][] = {
        {
            "client.local", "Local client (CAM) events"
        }, {
            "client.connection", "Client connections to server"
        }, {
            "server.local", "Server events"
        }, {
            "server.connection", "Inter-server connections"
        }, {
            "bundle.all", "All client bundle events"
        }
    };

    public ServerEmailBase()
    {
        addresses = null;
    }

    public void refresh()
    {
        super.refresh();
        addresses = null;
    }

    public boolean validate()
    {
        return true;
    }

    protected void getExistingValues()
    {
    }

    public Vector getRecipients()
    {
        addresses = new Vector();
        ((JSPPropertiesSupport)this).initializeProperties();
        int index = 0;
        String s;
        for(String p_name = "deploy.error.email." + index + ".address"; (s = super.properties.getProperty(p_name)) != null; p_name = "deploy.error.email." + index + ".address")
        {
            if(s.trim().length() != 0)
                addresses.addElement(((Object) (s)));
            index++;
        }

        return addresses;
    }

    public static String getNotificationString(Hashtable levels)
    {
        String notification = null;
        for(Enumeration level_list = levels.keys(); level_list.hasMoreElements();)
        {
            String level = (String)level_list.nextElement();
            boolean is_set = (Boolean)levels.get(((Object) (level))) == Boolean.TRUE;
            if(is_set)
                if(notification == null)
                    notification = level;
                else
                    notification = notification + "," + level;
        }

        return notification;
    }

    public static Hashtable getNotificationLevels(String levelNames)
    {
        if(levelNames == null)
            levelNames = "";
        Hashtable notificationFlags = new Hashtable();
        String level;
        for(StringTokenizer st = new StringTokenizer(levelNames, " ,\t\n\r"); st.hasMoreTokens(); notificationFlags.put(((Object) (level)), ((Object) (Boolean.TRUE))))
            level = st.nextToken();

        return notificationFlags;
    }

    public static Vector getFieldList()
    {
        Vector categories = new Vector();
        for(int i = 0; i < LEVEL_COUNT; i++)
            categories.addElement(((Object) (new EmailEntry(LEVEL_NAMES[0][i], LEVEL_NAMES[1][i]))));

        Vector bundles = (new BundleInfo()).getBundleListVector();
        for(int i = 0; i < bundles.size(); i++)
        {
            String bundle_name = (String)bundles.elementAt(i);
            String bundle_label = "Bundle " + bundle_name;
            bundle_name = "bundle." + bundle_name;
            categories.addElement(((Object) (new EmailEntry(bundle_name, bundle_label))));
        }

        return categories;
    }

}
