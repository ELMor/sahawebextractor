// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleExportRetry.java

package com.sitraka.deploy.sam.admin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class BundleExportRetry extends JSPSupport
{

    protected String application;
    protected String version;
    protected String storedTicket;
    protected String storedURL;
    protected int waitTime;
    public static final int RETRY = 0;
    public static final int FAILED = -1;
    public static final int SUCCEEDED = 1;

    public BundleExportRetry()
    {
        storedTicket = null;
        storedURL = null;
        waitTime = 0;
    }

    public void refresh()
    {
        super.refresh();
        application = null;
        version = null;
        waitTime = 0;
        storedURL = null;
        storedTicket = null;
    }

    public boolean validate()
    {
        boolean valid = true;
        if(application == null || version == null)
        {
            super.errMessage = "You must select an application and version<br>";
            valid = false;
        }
        return valid;
    }

    public int requestDAR(String proto, String server, int port, String servlet, String authData, HttpServletResponse response)
    {
        String base = servlet.substring(0, servlet.indexOf("admin")) + "deploy";
        String servletURL = proto + "://" + server + ":" + port + base;
        String query = servletURL + "?COMMAND=DAR&BUNDLE=" + application + "&VERSION=" + version;
        storedURL = query;
        URL query_url = null;
        try
        {
            query_url = new URL(query);
        }
        catch(MalformedURLException mre)
        {
            super.errMessage = "Error generating servlet URL: \n" + ((Throwable) (mre)).getMessage();
            System.err.println(super.errMessage);
            return -1;
        }
        URLConnection conn = null;
        try
        {
            conn = query_url.openConnection();
        }
        catch(IOException ioe)
        {
            super.errMessage = "Error creating servlet connection.\n" + ((Throwable) (ioe)).getMessage();
            System.err.println(super.errMessage);
            return -1;
        }
        if(authData != null)
            conn.setRequestProperty("Authorization", authData);
        conn.setUseCaches(false);
        if(storedTicket != null)
        {
            waitTime += 5;
            conn.setRequestProperty("DeploySam-ID", storedTicket);
        }
        try
        {
            conn.connect();
        }
        catch(IOException ioe)
        {
            super.errMessage = "Error connecting to servlet.\n" + ((Throwable) (ioe)).getMessage();
            System.err.println(super.errMessage);
            return -1;
        }
        String status = conn.getHeaderField("Status");
        int status_code = Integer.parseInt(status);
        if(status_code >= 400)
        {
            super.errMessage = "Error status " + status_code + "; aborting.";
            System.err.println(super.errMessage);
            return -1;
        }
        if(status_code >= 300)
        {
            super.errMessage = "Informational status " + status_code + "; aborting.";
            System.err.println(super.errMessage);
            return -1;
        }
        String ticket = conn.getHeaderField("DeploySam-ID");
        if(ticket != null)
        {
            storedTicket = ticket;
            return 0;
        }
        if(!conn.getContentType().equals("text/plain"))
        {
            InputStream dar_input = null;
            try
            {
                dar_input = conn.getInputStream();
                if(dar_input == null)
                {
                    super.errMessage = "Got return with no ticket, but no data!!";
                    System.err.println(super.errMessage);
                    return -1;
                }
            }
            catch(IOException ioe)
            {
                super.errMessage = "Error getting input stream:" + ((Throwable) (ioe)).getMessage();
                System.err.println(super.errMessage);
                return -1;
            }
            ((ServletResponse) (response)).reset();
            ((ServletResponse) (response)).setContentType("application/x-deploy-dar");
            String file = getApplication().toLowerCase() + ".dar";
            response.setHeader("Content-Disposition", "inline; filename=" + file);
            int content_length = conn.getContentLength();
            if(content_length >= 0)
                ((ServletResponse) (response)).setContentLength(content_length);
            OutputStream out_stream = null;
            try
            {
                out_stream = ((OutputStream) (((ServletResponse) (response)).getOutputStream()));
            }
            catch(IOException ioe)
            {
                super.errMessage = "Error getting output stream:" + ((Throwable) (ioe)).getMessage();
                System.err.println(super.errMessage);
                return -1;
            }
            int bytes = 0;
            int byte_count = 0;
            byte buffer[] = new byte[2048];
            try
            {
                for(bytes = dar_input.read(buffer); bytes >= 0; bytes = dar_input.read(buffer))
                {
                    byte_count += bytes;
                    if(bytes > 0)
                        out_stream.write(buffer, 0, bytes);
                }

                if(content_length < 0)
                    ((ServletResponse) (response)).setContentLength(content_length);
                else
                if(content_length != byte_count)
                    System.err.println("Received content length of " + content_length + " does not agree with " + "the actual number of bytes " + byte_count);
                out_stream.flush();
                out_stream.close();
                ((ServletResponse) (response)).flushBuffer();
            }
            catch(IOException ioe)
            {
                super.errMessage = "Error copying input to output:" + ((Throwable) (ioe)).getMessage();
                System.err.println(super.errMessage);
                return -1;
            }
            storedTicket = null;
            waitTime = 0;
            return 1;
        } else
        {
            System.err.println("Received no ticket, but also no data.  Try again.");
            return 0;
        }
    }

    public void setVersion(String name)
    {
        version = name;
    }

    public String getVersion()
    {
        return version;
    }

    public void setApplication(String name)
    {
        application = name;
    }

    public String getApplication()
    {
        return application;
    }

    public void setTicket(String ticket)
    {
        storedTicket = ticket;
    }

    public String getTicket()
    {
        return storedTicket;
    }

    public void setUrl(String url)
    {
        storedURL = url;
    }

    public String getUrl()
    {
        return storedURL;
    }

    public int getWaitTime()
    {
        return waitTime;
    }
}
