// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerEventsChart.java

package com.sitraka.deploy.sam.admin.chart;

import com.klg.jclass.chart.ChartDataView;
import com.klg.jclass.chart.JCChart;
import com.klg.jclass.chart.data.JCDefaultDataSource;
import com.klg.jclass.schart.JCServerChart;
import com.sitraka.deploy.sam.LogQueue;
import com.sitraka.deploy.sam.log.LogException;
import com.sitraka.deploy.sam.log.LogStats;
import com.sitraka.deploy.sam.log.ServerLogger;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package com.sitraka.deploy.sam.admin.chart:
//            AbstractChart, ChartException

public class ServerEventsChart extends AbstractChart
{

    public ServerEventsChart(HttpServletRequest request, HttpServletResponse response, String extension)
        throws ChartException
    {
        super(request, response, extension, "serverStats.xml");
        ((AbstractChart)this).extractEventList();
    }

    public void generateChart()
        throws ChartException
    {
        LogStats logStats = null;
        try
        {
            ServerLogger logger = Servlet.getUniqueInstance().getLogQueue().getServerLogger();
            logStats = logger.queryLogStats(super.startTimestamp, super.endTimestamp);
        }
        catch(LogException e)
        {
            throw new ChartException("Chart Error: Reading Logs: " + ((Throwable) (e)).getMessage(), 500);
        }
        Vector eventNames = logStats.getEvents();
        Vector eventCounts = logStats.getCount();
        double countArray[][];
        String namesArray[];
        if(super.eventList == null)
        {
            countArray = new double[1][eventNames.size()];
            namesArray = new String[eventNames.size()];
            ((AbstractChart)this).fillEventCountArray(logStats, namesArray, countArray);
        } else
        {
            countArray = new double[1][super.eventList.length];
            namesArray = super.eventList;
            ((AbstractChart)this).fillLimitedEventCountArray(logStats, super.eventList, countArray);
        }
        JCDefaultDataSource ds = null;
        ds = new JCDefaultDataSource(((double [][]) (null)), countArray, namesArray, namesArray, "Client Event Numbers");
        ((JCChart) (super.chart)).getDataView(0).setDataSource(((com.klg.jclass.chart.ChartDataModel) (ds)));
        try
        {
            if(super.extension == null)
            {
                ((ServletResponse) (super.response)).setContentType("image/png");
                super.chart.encodeAsPNG(((OutputStream) (((ServletResponse) (super.response)).getOutputStream())));
            } else
            if("imagemap".equals(((Object) (super.extension))))
                throw new ChartException("Chart Error: Image maps not implemented", 501);
            ((OutputStream) (((ServletResponse) (super.response)).getOutputStream())).flush();
        }
        catch(IOException ioe)
        {
            throw new ChartException("Chart Error: IOException generating chart.", 500);
        }
    }
}
