// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JSPSupport.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.Codecs;

public abstract class JSPSupport
{

    protected String errMessage;
    protected String authData;

    public JSPSupport()
    {
        errMessage = null;
        authData = null;
    }

    public void refresh()
    {
        errMessage = null;
        authData = null;
    }

    protected Servlet getServlet()
    {
        Servlet servlet = Servlet.getUniqueInstance();
        if(servlet == null)
        {
            errMessage = "Unable to find or contact the servlet.";
            return null;
        } else
        {
            return servlet;
        }
    }

    public String getAuthData()
    {
        return authData;
    }

    public void setAuthData(String auth)
    {
        if(auth.startsWith("Basic "))
            auth = auth.substring("Basic ".length());
        authData = Codecs.base64Decode(auth);
    }

    public abstract boolean validate();

    public String getErrMessage()
    {
        return errMessage;
    }
}
