// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EmailEntry.java

package com.sitraka.deploy.sam.admin;


public class EmailEntry
{

    protected String fieldName;
    protected String fieldLabel;

    public EmailEntry()
    {
        fieldName = null;
        fieldLabel = null;
    }

    public EmailEntry(String name, String label)
    {
        fieldName = null;
        fieldLabel = null;
        fieldName = name;
        fieldLabel = label;
    }

    public String getName()
    {
        return fieldName;
    }

    public void setName(String name)
    {
        fieldName = name;
    }

    public String getLabel()
    {
        return fieldLabel;
    }

    public void setLabel(String label)
    {
        fieldLabel = label;
    }
}
