// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerValidHosts.java

package com.sitraka.deploy.sam.admin;

import com.sitraka.deploy.sam.worker.AdminPageWorker;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPSupport

public class ServerValidHosts extends JSPSupport
{

    public ServerValidHosts()
    {
    }

    public boolean validate()
    {
        return true;
    }

    public String getValidHosts()
    {
        AdminPageWorker apw = new AdminPageWorker();
        Vector v = apw.getValidHostNames();
        String out_data = "<ul>\n";
        for(int i = 0; i < v.size(); i++)
            out_data = out_data + "<li>" + (String)v.elementAt(i) + "</li>\n";

        out_data = out_data + "</ul>\n";
        return out_data;
    }
}
