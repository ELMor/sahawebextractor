// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerClusterMisc.java

package com.sitraka.deploy.sam.admin;

import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam.admin:
//            JSPPropertiesSupport

public class ServerClusterMisc extends JSPPropertiesSupport
{

    protected String deployAppletJavapluginIeClassid;
    protected String deployAppletJavapluginIeCodebase;
    protected String deployAppletJavapluginNsPluginspage;
    protected String deployAppletJavapluginType;
    protected String deployCompressClient;
    protected String deployCompressServer;
    protected String deployServerName;

    public ServerClusterMisc()
    {
        deployAppletJavapluginIeClassid = "";
        deployAppletJavapluginIeCodebase = "";
        deployAppletJavapluginNsPluginspage = "";
        deployAppletJavapluginType = "";
        deployCompressClient = "";
        deployCompressServer = "";
        deployServerName = "";
    }

    public void refresh()
    {
        super.refresh();
        deployAppletJavapluginIeClassid = "";
        deployAppletJavapluginIeCodebase = "";
        deployAppletJavapluginNsPluginspage = "";
        deployAppletJavapluginType = "";
        deployCompressClient = "";
        deployCompressServer = "";
        deployServerName = "";
    }

    public boolean validate()
    {
        ((JSPPropertiesSupport)this).initializeProperties(false);
        if(deployAppletJavapluginIeClassid != null && deployAppletJavapluginIeClassid.length() > 0)
            super.properties.setProperty("deploy.applet.javaplugin.ie.classid", deployAppletJavapluginIeClassid);
        if(deployAppletJavapluginIeCodebase != null && deployAppletJavapluginIeCodebase.length() > 0)
            super.properties.setProperty("deploy.applet.javaplugin.ie.codebase", deployAppletJavapluginIeCodebase);
        if(deployAppletJavapluginNsPluginspage != null && deployAppletJavapluginNsPluginspage.length() > 0)
            super.properties.setProperty("deploy.applet.javaplugin.ns.pluginspage", deployAppletJavapluginNsPluginspage);
        if(deployAppletJavapluginType != null && deployAppletJavapluginType.length() > 0)
            super.properties.setProperty("deploy.applet.javaplugin.type", deployAppletJavapluginType);
        if(deployCompressClient != null && deployCompressClient.length() > 0)
            super.properties.setProperty("deploy.compress.client", deployCompressClient);
        if(deployCompressServer != null && deployCompressServer.length() > 0)
            super.properties.setProperty("deploy.compress.server", deployCompressServer);
        if(deployServerName != null && deployServerName.length() > 0)
            super.properties.setProperty("deploy.server.name", deployServerName);
        return ((JSPPropertiesSupport)this).storeProperties();
    }

    protected void getExistingValues()
    {
        deployAppletJavapluginIeClassid = super.properties.getProperty("deploy.applet.javaplugin.ie.classid", "");
        deployAppletJavapluginIeCodebase = super.properties.getProperty("deploy.applet.javaplugin.ie.codebase", "");
        deployAppletJavapluginNsPluginspage = super.properties.getProperty("deploy.applet.javaplugin.ns.pluginspage", "");
        deployAppletJavapluginType = super.properties.getProperty("deploy.applet.javaplugin.type", "");
        deployCompressClient = super.properties.getProperty("deploy.compress.client", "");
        deployCompressServer = super.properties.getProperty("deploy.compress.server", "");
        deployServerName = super.properties.getProperty("deploy.server.name", "");
    }

    protected String getPropFile()
    {
        return "cluster.properties";
    }

    public void setDeployAppletJavapluginIeClassid(String classID)
    {
        deployAppletJavapluginIeClassid = classID;
    }

    public String getDeployAppletJavapluginIeClassid()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployAppletJavapluginIeClassid;
    }

    public void setDeployAppletJavapluginIeCodebase(String codebase)
    {
        deployAppletJavapluginIeCodebase = codebase;
    }

    public String getDeployAppletJavapluginIeCodebase()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployAppletJavapluginIeCodebase;
    }

    public void setDeployAppletJavapluginNsPluginspage(String page)
    {
        deployAppletJavapluginNsPluginspage = page;
    }

    public String getDeployAppletJavapluginNsPluginspage()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployAppletJavapluginNsPluginspage;
    }

    public void setDeployAppletJavapluginType(String type)
    {
        deployAppletJavapluginType = type;
    }

    public String getDeployAppletJavapluginType()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployAppletJavapluginType;
    }

    public void setDeployCompressClient(String level)
    {
        deployCompressClient = level;
    }

    public String getDeployCompressClient()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployCompressClient;
    }

    public void setDeployCompressServer(String level)
    {
        deployCompressServer = level;
    }

    public String getDeployCompressServer()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployCompressServer;
    }

    public void setDeployServerName(String name)
    {
        deployServerName = name;
    }

    public String getDeployServerName()
    {
        ((JSPPropertiesSupport)this).initializeProperties();
        return deployServerName;
    }
}
