// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RequestParser.java

package com.sitraka.deploy.sam;

import com.klg.jclass.util.JCStringTokenizer;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Platform;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.roles.BundleAdminRole;
import com.sitraka.deploy.common.roles.Role;
import com.sitraka.deploy.common.roles.ServerAdminRole;
import com.sitraka.deploy.util.Codecs;
import com.sitraka.deploy.util.ParseException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam:
//            AppVault

public class RequestParser
    implements Requests
{

    protected int request;
    protected int authorization;
    protected boolean is_administrator;
    protected boolean unlicensedCommand;
    protected String full_text;
    protected Hashtable params;
    protected static Hashtable map = new Hashtable();

    public RequestParser(String request_text, int type, String post_params)
    {
        request = -1;
        is_administrator = false;
        unlicensedCommand = false;
        full_text = null;
        params = null;
        full_text = request_text;
        parseParams(request_text, post_params);
        String what = getCommand();
        if(what != null)
        {
            parseGet(request_text);
            request = stringToCommand(what);
            type = 0;
        } else
        {
            switch(type)
            {
            case 1: // '\001'
                parsePut(request_text);
                break;

            case 2: // '\002'
                parsePost(request_text);
                break;

            case 0: // '\0'
                parseGet(request_text);
                break;

            default:
                request = -1;
                break;
            }
        }
        if(isAdminCommand())
            setAdministrator(true);
        else
        if(getApplication() != null)
        {
            String app = getApplication().toUpperCase();
            if(app.equals(((Object) ("DDAdmin".toUpperCase()))))
                setAdministrator(true);
        }
    }

    public int getRequest()
    {
        return request;
    }

    public String getRequestText()
    {
        return full_text;
    }

    public String getName()
    {
        return getParam("NAME");
    }

    public String getFormat()
    {
        return getParam("FORMAT");
    }

    public String getClasspaths()
    {
        return getParam("CLASSPATHS");
    }

    public String getVendor()
    {
        return getParam("VENDOR");
    }

    public String getPlatform()
    {
        return getParam("PLATFORM");
    }

    public String getApplication()
    {
        return getParam("BUNDLE");
    }

    public String getParameters()
    {
        return getParam("PARAMETERS");
    }

    public void setParameters(String parameters)
    {
        if(parameters != null)
            params.put("PARAMETERS", ((Object) (parameters)));
    }

    public String getCookieData()
    {
        return getParam("COOKIE");
    }

    public void setCookieData(String cookie_data)
    {
        if(cookie_data != null)
            params.put("COOKIE", ((Object) (cookie_data)));
    }

    public String getFrom()
    {
        return getParam("FROM");
    }

    public String getTo()
    {
        return getParam("TO");
    }

    public String getFile()
    {
        return getParam("FILE");
    }

    public void setFile(String _files)
    {
        if(_files == null)
            params.remove("FILE");
        else
            params.put("FILE", ((Object) (_files)));
    }

    public String getMessage()
    {
        return getParam("MESSAGE");
    }

    public String getServer()
    {
        return getParam("SERVER");
    }

    public String getSequence()
    {
        return getParam("DD-Sequence");
    }

    public String getRequestID()
    {
        return getParam("DeploySam-ID");
    }

    public void setRequestID(String id)
    {
        if(id == null || id.trim().length() == 0)
            params.remove(((Object) ("DeploySam-ID".toUpperCase())));
        else
            params.put(((Object) ("DeploySam-ID".toUpperCase())), ((Object) (id)));
    }

    public String getVersion()
    {
        String result = getParam("VERSION");
        if(result == null)
            return getFrom();
        else
            return result;
    }

    public String getCommand()
    {
        return getParam("COMMAND");
    }

    public boolean isSanctionedCommand()
    {
        int request = getRequest();
        return request >= 1000 && request <= 1010;
    }

    public boolean isAuthorizableCommand()
    {
        int request = getRequest();
        return request >= 2000 && request <= 2016;
    }

    public boolean isAdminUnlicensedCommand()
    {
        int request = getRequest();
        return request >= 4000 && request <= 4003;
    }

    public boolean isAdminCommand()
    {
        int request = getRequest();
        return request >= 3000 && request <= 3313;
    }

    public boolean isBundleAdminCommand()
    {
        int request = getRequest();
        return request >= 3100 && request <= 3106;
    }

    public boolean isPlatformAdminCommand()
    {
        int request = getRequest();
        return request >= 3200 && request <= 3203;
    }

    public boolean isGeneralAdminCommand()
    {
        int request = getRequest();
        return request >= 3000 && request <= 3002;
    }

    public boolean isServerAdminCommand()
    {
        int request = getRequest();
        return request >= 3300 && request <= 3000;
    }

    public boolean isUnlicensedCommand()
    {
        return unlicensedCommand;
    }

    public Role[] getRoles()
    {
        int request = getRequest();
        String app = getApplication();
        if(app != null && app.equalsIgnoreCase("any"))
            return (new Role[] {
                new ServerAdminRole()
            });
        if(app == null || app.trim().length() == 0)
            app = "DDAdmin";
        if(isAuthorizableCommand() || isSanctionedCommand() || isUnlicensedCommand())
        {
            BundleAdminRole bundleAdmin = new BundleAdminRole(app);
            if(app.equalsIgnoreCase("DDAdmin"))
                bundleAdmin.addBundle("any");
            return (new Role[] {
                bundleAdmin
            });
        }
        if(isPlatformAdminCommand())
        {
            if(request == 3201)
                return (new Role[] {
                    new BundleAdminRole("any")
                });
        } else
        {
            if(isBundleAdminCommand())
            {
                BundleAdminRole bundleAdmin = new BundleAdminRole(app);
                if(request == 3102)
                    bundleAdmin.addBundle("any");
                return (new Role[] {
                    bundleAdmin
                });
            }
            if(isGeneralAdminCommand())
            {
                BundleAdminRole bundleAdmin = new BundleAdminRole(app);
                if(request == 3002)
                    bundleAdmin.addBundle("any");
                return (new Role[] {
                    bundleAdmin
                });
            }
            if(request == 3313)
                return (new Role[] {
                    new BundleAdminRole(app)
                });
        }
        return (new Role[] {
            new ServerAdminRole()
        });
    }

    public boolean isAdministrator()
    {
        return is_administrator;
    }

    public void setAdministrator(boolean is_admin)
    {
        is_administrator = is_admin;
    }

    public void setAuthorization(int auth)
    {
        authorization = auth;
    }

    public int getAuthorization()
    {
        return authorization;
    }

    public void restrictPlatform(AppVault vault)
    {
        if(getRequest() != 2006 && getRequest() != 2004)
            return;
        if(getApplication() == null || getVersion() == null)
            return;
        if(getPlatform() == null)
        {
            params.put("PLATFORM", "all");
            return;
        }
        if("DeploySam".equals(((Object) (getPlatform()))))
            return;
        Application app = vault.getApplication(getApplication());
        if(app == null)
            return;
        Version ver = app.getVersion(getVersion());
        if(ver == null)
            return;
        Vector platforms = ver.listPlatforms();
        if(platforms == null)
            return;
        String currentPlatform = "";
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            Platform platform = (Platform)platforms.elementAt(i);
            if(getPlatform().toLowerCase().startsWith(platform.getFullName()) && platform.getFullName().length() >= currentPlatform.length())
                currentPlatform = platform.getFullName();
        }

        if(currentPlatform.equals(""))
            currentPlatform = "all";
        params.put("PLATFORM", ((Object) (currentPlatform)));
    }

    protected void parsePost(String request_text)
    {
        if(getApplication() != null)
            request = 2008;
    }

    protected void parsePut(String request_text)
    {
        parseGet(request_text);
    }

    public boolean isSpecialFile()
    {
        String requestText = getRequestText();
        return requestText.endsWith("deploydirector.gif") || requestText.endsWith("deploydirector_ss.gif") || requestText.endsWith("install.jar") || requestText.endsWith("install.cab") || requestText.endsWith("launch.jar") || requestText.endsWith("launch.cab") || requestText.endsWith(".html");
    }

    protected void parseGet(String request_text)
    {
        request = -1;
        String command = getParam("COMMAND");
        if(command == null)
        {
            if(isSpecialFile())
            {
                params.put("FILE", ((Object) (request_text)));
                request = 2004;
                unlicensedCommand = true;
                return;
            }
            int index = request_text.indexOf("/");
            if((request_text.equalsIgnoreCase("admin-old") || request_text.toLowerCase().endsWith("/admin-old")) && index == -1)
            {
                request = 4002;
                return;
            }
            if((request_text.equalsIgnoreCase("ddinfo") || request_text.toLowerCase().endsWith("/ddinfo")) && index == -1)
            {
                request = 4003;
                return;
            }
            if(request_text.toLowerCase().endsWith("/install"))
            {
                if(index == -1)
                {
                    request = -1;
                    return;
                }
                String other = request_text.substring(index + 1);
                String appname = request_text.substring(0, index);
                try
                {
                    appname = Codecs.URLDecode(appname);
                }
                catch(ParseException e) { }
                params.put("BUNDLE", ((Object) (appname)));
                request = 2008;
                return;
            }
            if(request_text.toLowerCase().endsWith("/launch"))
            {
                if(index == -1)
                {
                    request = -1;
                    return;
                }
                String other = request_text.substring(index + 1);
                String appname = request_text.substring(0, index);
                try
                {
                    appname = Codecs.URLDecode(appname);
                }
                catch(ParseException e) { }
                params.put("BUNDLE", ((Object) (appname)));
                request = 2010;
                return;
            }
            if(request_text.toLowerCase().endsWith("/install-plugin"))
            {
                if(index == -1)
                {
                    request = -1;
                    return;
                }
                String other = request_text.substring(index + 1);
                String appname = request_text.substring(0, index);
                try
                {
                    appname = Codecs.URLDecode(appname);
                }
                catch(ParseException e) { }
                params.put("BUNDLE", ((Object) (appname)));
                request = 2013;
                return;
            }
            if(request_text.toLowerCase().endsWith("/launch-plugin"))
            {
                if(index == -1)
                {
                    request = -1;
                    return;
                }
                String other = request_text.substring(index + 1);
                String appname = request_text.substring(0, index);
                try
                {
                    appname = Codecs.URLDecode(appname);
                }
                catch(ParseException e) { }
                params.put("BUNDLE", ((Object) (appname)));
                request = 2014;
                return;
            }
            if(index == -1)
            {
                request = -1;
                return;
            } else
            {
                params.put("FILE", ((Object) (request_text)));
                request = 2004;
                return;
            }
        } else
        {
            request = stringToCommand(command);
            return;
        }
    }

    public String getParam(String key)
    {
        if(params == null)
            return null;
        key = key.toUpperCase();
        String value = (String)params.get(((Object) (key)));
        if(value == null)
            return null;
        if(value.equalsIgnoreCase("") && !key.equalsIgnoreCase("CLASSPATHS"))
            value = null;
        return value;
    }

    protected void parseParams(String request_text, String post_params)
    {
        params = new Hashtable();
        int index = request_text.indexOf("?");
        if(index != -1)
        {
            String request_params = request_text.substring(index + 1);
            parseParamString(request_params);
        }
        if(post_params == null || post_params.equals(""))
        {
            return;
        } else
        {
            parseParamString(post_params);
            return;
        }
    }

    protected void parseParamString(String param_string)
    {
        if(param_string == null)
            return;
        for(JCStringTokenizer toker = new JCStringTokenizer(param_string); toker.hasMoreTokens();)
        {
            String pair = toker.nextToken('&');
            if(pair != null)
            {
                if(pair.startsWith("?"))
                    pair = pair.substring(1);
                int index = pair.indexOf("=");
                if(index != -1)
                {
                    String name = pair.substring(0, index);
                    String value = pair.substring(index + 1);
                    name = name.toUpperCase();
                    if(name.equals("ADMINCOMMAND") && value.toLowerCase().equals("true"))
                        setAdministrator(true);
                    String oldvalue = (String)params.get(((Object) (name)));
                    if(oldvalue == null)
                        params.put(((Object) (name)), ((Object) (value)));
                    else
                        params.put(((Object) (name)), ((Object) (oldvalue + "," + value)));
                }
            }
        }

    }

    private static void setCommand(String k, Integer v)
    {
        map.put(((Object) (k)), ((Object) (v)));
    }

    private static void fillMap()
    {
        setCommand("ACCESSPOLICY", new Integer(2000));
        setCommand("ADMINPAGE", new Integer(4002));
        setCommand("BUNDLEPROPERTIES", new Integer(1001));
        setCommand("BUNDLES", new Integer(1000));
        setCommand("CONNECTWHEN", new Integer(2002));
        setCommand("DELETEBUNDLE", new Integer(3107));
        setCommand("DELETEJRE", new Integer(3200));
        setCommand("DELETEVERSION", new Integer(3101));
        setCommand("DIFF", new Integer(2003));
        setCommand("DIST", new Integer(2006));
        setCommand("ERROR", new Integer(1009));
        setCommand("FILE", new Integer(2004));
        setCommand("FILES", new Integer(2005));
        setCommand("INSTALLDIFF", new Integer(2007));
        setCommand("ISAUTHORIZED", new Integer(2012));
        setCommand("JRE", new Integer(2009));
        setCommand("LATESTVERSION", new Integer(2016));
        setCommand("LICENSEFILE", new Integer(1005));
        setCommand("LOG", new Integer(3002));
        setCommand("MESSAGE", new Integer(1008));
        setCommand("NEWPASSWORD", new Integer(4001));
        setCommand("PLATFORMXML", new Integer(3201));
        setCommand("RESTART", new Integer(3301));
        setCommand("SERVERBUNDLE", new Integer(3307));
        setCommand("SERVERCONFIG", new Integer(3308));
        setCommand("SERVERJRE", new Integer(3309));
        setCommand("SERVERLOAD", new Integer(3300));
        setCommand("SERVERNEWBUNDLE", new Integer(3108));
        setCommand("SERVERNEWCONFIG", new Integer(3302));
        setCommand("SERVERNEWIHTML", new Integer(3311));
        setCommand("SERVERNEWINSTALLER", new Integer(3310));
        setCommand("SERVERNEWJRE", new Integer(3202));
        setCommand("SERVERNEWLOG", new Integer(3304));
        setCommand("SERVERNEWVERSION", new Integer(3100));
        setCommand("SERVERS", new Integer(1010));
        setCommand("SERVERUPDATELOG", new Integer(3312));
        setCommand("SERVERVERSION", new Integer(3313));
        setCommand("UPDATEPOLICY", new Integer(2011));
        setCommand("UPLOADBUNDLE", new Integer(3109));
        setCommand("UPLOADCONFIG", new Integer(3303));
        setCommand("UPLOADHTML", new Integer(3305));
        setCommand("UPLOADINSTALLER", new Integer(3306));
        setCommand("UPLOADJRE", new Integer(3203));
        setCommand("UPLOADSTATUS", new Integer(3001));
        setCommand("UPLOADVERSION", new Integer(3104));
        setCommand("VERSION", new Integer(1004));
        setCommand("VERSIONREORDER", new Integer(3103));
        setCommand("VERSIONS", new Integer(1002));
        setCommand("VERSIONXML", new Integer(3102));
        setCommand("CLIENTCOMPRESSLEVEL", new Integer(1003));
        setCommand("CONNECTPOLICY", new Integer(2001));
        setCommand("NEWLICENSE", new Integer(4000));
        setCommand("SERVERCOMPRESSLEVEL", new Integer(1006));
        setCommand("DAR", new Integer(3106));
        setCommand("DOWNLOADDAR", new Integer(2015));
        setCommand("UPLOADDAR", new Integer(3105));
    }

    public String commandToString(int command)
    {
        Integer tester = new Integer(command);
        for(Enumeration e = map.keys(); e.hasMoreElements();)
        {
            String name = (String)e.nextElement();
            Integer x = (Integer)map.get(((Object) (name)));
            if(tester.equals(((Object) (x))))
                return name;
        }

        return null;
    }

    protected int stringToCommand(String name)
    {
        Integer value = (Integer)map.get(((Object) (name.toUpperCase())));
        if(value == null)
            return -1;
        else
            return value.intValue();
    }

    static 
    {
        fillMap();
    }
}
