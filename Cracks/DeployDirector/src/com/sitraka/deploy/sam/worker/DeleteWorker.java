// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DeleteWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.checkpoint.ApplicationObject;
import com.sitraka.deploy.common.checkpoint.JREObject;
import com.sitraka.deploy.common.checkpoint.VersionObject;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ClientIdentifier;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.cache.CacheList;
import com.sitraka.deploy.sam.request.RequestList;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.Codecs;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker, ReplicationThread

public class DeleteWorker extends AbstractWorker
{

    boolean is_server;

    public DeleteWorker()
    {
        is_server = false;
        super.type = 9;
    }

    public void startWorking()
        throws ServletProcessException, ServletNotFoundException
    {
        synchronized(super.STATE_LOCK)
        {
            boolean status = false;
            ((AbstractWorker)this).setOrigServer();
            is_server = ClientIdentifier.identifyClientType(super.request) == 1;
            boolean performReplication = super.origServer.equals("DeployAdmin");
            try
            {
                switch(super.parser.getRequest())
                {
                case 3107: 
                    deleteBundle(performReplication);
                    break;

                case 3101: 
                    deleteVersion(performReplication);
                    break;

                case 3200: 
                    deleteJRE(performReplication);
                    break;
                }
            }
            finally
            {
                is_server = false;
            }
        }
    }

    protected void sendACK()
    {
        sendACK("Delete message acknowledged and processed. Have a nice day ;-)");
    }

    protected void sendACK(String message)
    {
        ((AbstractWorker)this).sendText(message);
    }

    protected void deleteBundle(boolean performReplication)
        throws ServletProcessException
    {
        String appname = super.parser.getApplication();
        if(appname == null)
        {
            String msg = "Unable to delete application from: " + super.origServer + ".  No application specified.";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication(super.none);
            spe.setVersion(super.none);
            throw spe;
        }
        CacheList.removeBundleReference(appname);
        RequestList.removeBundleReference(appname);
        if(super.vault.getApplication(appname) == null)
            if(is_server)
            {
                sendACK();
                return;
            } else
            {
                String msg = "Bundle '" + appname + "' does not exist in the " + "vault. Cannot delete it from: " + super.origServer;
                ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
                spe.setApplication(appname);
                spe.setVersion(super.none);
                throw spe;
            }
        if(!super.vault.deleteApplication(appname))
        {
            String msg = "Unable to delete application " + appname + " from the vault on server: " + super.origServer;
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication(appname);
            spe.setVersion(super.none);
            throw spe;
        }
        super.server.logMessage(super.request, "ServerBundleDeleted", appname, super.none, "(" + appname + " deleted) by: " + super.origServer);
        sendACK();
        if(super.vault.listOtherServers() != null && performReplication)
        {
            ApplicationObject applicationObject = new ApplicationObject(super.server.getRequestUrl(super.request), appname);
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendApplicationDeletion(super.server.getRequestUrl(super.request), applicationObject, ((Object) (auth)));
        }
    }

    protected void deleteVersion(boolean performReplication)
        throws ServletProcessException
    {
        String appname = super.parser.getApplication();
        String vername = super.parser.getVersion();
        if(appname == null || vername == null)
        {
            String msg = "Unable to delete version from: " + super.origServer + ". No";
            if(appname == null)
                msg = msg + " application";
            if(vername == null)
            {
                if(appname == null)
                    msg = msg + " or";
                msg = msg + " version";
            }
            msg = msg + " name was specified.";
            throw new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
        }
        CacheList.removeBundleVersionReference(appname, vername);
        RequestList.removeBundleVersionReference(appname, vername);
        if(super.vault.getApplication(appname) == null)
            if(is_server)
            {
                sendACK();
                return;
            } else
            {
                String msg = "Cannot delete version '" + vername + "' from bundle '" + appname + "'. '" + appname + "' does not exist in the vault on: " + super.origServer;
                throw new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            }
        if(super.vault.getApplication(appname).getVersion(vername) == null)
            if(is_server)
            {
                sendACK();
                return;
            } else
            {
                String msg = "Cannot delete version '" + vername + "' from bundle '" + appname + "'. '" + appname + "' does not contain that version on: " + super.origServer;
                throw new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            }
        if(!super.vault.deleteVersion(appname, vername))
        {
            String msg = "Unable to delete version " + appname + ", version " + vername + " from the vault on: " + super.origServer;
            throw new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
        }
        super.server.logMessage(super.request, "ServerVersionDeleted", appname, vername, "(" + appname + " " + vername + " deleted) by: " + super.origServer);
        sendACK();
        if(super.vault.listOtherServers() != null && performReplication)
        {
            VersionObject versionObject = new VersionObject(super.server.getRequestUrl(super.request), appname, vername);
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendVersionDeletion(super.server.getRequestUrl(super.request), versionObject, ((Object) (auth)));
        }
    }

    protected void deleteJRE(boolean performReplication)
        throws ServletProcessException
    {
        String vendor = super.parser.getVendor();
        String version = super.parser.getVersion();
        String platform = super.parser.getPlatform();
        if(vendor == null || version == null || platform == null)
        {
            String msg = "Unable to delete JRE from " + super.origServer + ", request was missing ";
            boolean prev = false;
            if(vendor == null)
            {
                msg = msg + "the vendor name";
                prev = true;
            }
            if(version == null)
            {
                if(prev)
                    msg = msg + (platform != null ? " and " : ", ");
                msg = msg + "the version name";
                prev = true;
            }
            if(platform == null)
            {
                if(prev)
                    msg = msg + " and ";
                msg = msg + "the platform name";
            }
            msg = msg + ".";
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication(vendor + " JRE");
            throw spe;
        }
        if(super.vault.getPlatformJRE(platform, version, vendor) == null)
            if(is_server)
            {
                sendACK();
                return;
            } else
            {
                String msg = "Unable to delete JRE (" + vendor + " " + version + " for " + platform + ") from server " + super.origServer;
                ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
                spe.setApplication(vendor + " JRE");
                throw spe;
            }
        if(!super.vault.deleteJRE(vendor, version, platform))
        {
            String msg = "Unable to delete JRE (" + vendor + " " + version + " for " + platform + ") from server " + super.origServer;
            ServletProcessException spe = new ServletProcessException(msg, "ServerRequestFailed", super.request, super.response, true);
            spe.setApplication(vendor + " JRE");
            throw spe;
        }
        super.server.logMessage(super.request, "ServerJREDeleted", vendor + " JRE", version, "(JRE " + platform + ": " + vendor + " " + version + " deleted) by: " + super.origServer);
        sendACK();
        if(super.vault.listOtherServers() != null && performReplication)
        {
            JREObject jreObject = new JREObject(super.server.getRequestUrl(super.request), vendor, version, platform, ((String) (null)), ((String) (null)), ((String) (null)));
            String auth = Codecs.base64Decode(super.server.getUserAuthorization(super.request).toString());
            AbstractWorker.getReplicationQueue(super.server).sendJREDeletion(super.server.getRequestUrl(super.request), jreObject, ((Object) (auth)));
        }
    }
}
