// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BundleReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.ApplicationCheckpoint;
import com.sitraka.deploy.common.checkpoint.ApplicationObject;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class BundleReplicationMessage extends ReplicationMessage
{

    public BundleReplicationMessage(String remote_server, int type, ApplicationObject applicationObject, ApplicationCheckpoint checkpoint, Object _auth, String otherServers)
    {
        super(remote_server, type, ((Object) (applicationObject)), ((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), _auth, otherServers);
    }

    public ApplicationObject getApplicationObject()
    {
        return (ApplicationObject)((ReplicationMessage)this).getUserData();
    }

    public ApplicationCheckpoint getApplicationCheckpoint()
    {
        return (ApplicationCheckpoint)((ReplicationMessage)this).getCheckpoint();
    }

    public String getApplication()
    {
        return getApplicationObject().getApplication();
    }
}
