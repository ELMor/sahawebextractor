// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PolicyWorker.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.app.ConnectionPolicy;
import com.sitraka.deploy.common.app.UpdatePolicy;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.sam.RequestParser;
import com.sitraka.deploy.sam.servlet.ServletNotFoundException;
import com.sitraka.deploy.sam.servlet.ServletProcessException;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            AbstractWorker

public class PolicyWorker extends AbstractWorker
{

    public PolicyWorker()
    {
        super.type = 1;
    }

    public void startWorking()
        throws ServletProcessException, ServletNotFoundException
    {
        String content = "";
        Version ver = ((AbstractWorker)this).getVersion();
        if(ver == null)
        {
            String msg = "No version specified to get policy for.";
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        }
        switch(super.parser.getRequest())
        {
        case 2001: 
            content = ver.getConnectionPolicy().toString();
            break;

        case 2002: 
            content = ver.getConnectionPolicy().toString();
            break;

        case 2011: 
            content = ver.getUpdatePolicy().toString();
            break;

        case 2000: 
            content = ver.getConnectionPolicy().toString();
            break;

        default:
            content = null;
            break;
        }
        if(content == null)
        {
            String msg = "No policy found for version " + ver.getName();
            throw new ServletNotFoundException(msg, super.request, super.response, true);
        } else
        {
            ((AbstractWorker)this).sendText(content);
            return;
        }
    }
}
