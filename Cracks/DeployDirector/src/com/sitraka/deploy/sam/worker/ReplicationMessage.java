// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.Checkpoint;
import java.io.Serializable;

public abstract class ReplicationMessage
    implements Serializable
{

    protected String remote_server;
    protected Object userData;
    protected int type;
    protected Checkpoint checkpoint;
    protected Object authorization;
    protected String otherServers;
    protected int retryCount;

    public ReplicationMessage(String remote_server, int type, Object userData, Checkpoint checkpoint, Object _auth, String otherServers)
    {
        this.remote_server = null;
        this.userData = null;
        this.type = 0;
        this.checkpoint = null;
        authorization = null;
        this.otherServers = null;
        retryCount = 0;
        this.remote_server = remote_server;
        this.type = type;
        this.userData = userData;
        this.checkpoint = checkpoint;
        authorization = _auth;
        this.otherServers = otherServers;
        retryCount = 0;
    }

    public String getServer()
    {
        return remote_server;
    }

    public int getType()
    {
        return type;
    }

    public Object getUserData()
    {
        return userData;
    }

    public Object getAuthorization()
    {
        return authorization;
    }

    public Checkpoint getCheckpoint()
    {
        return checkpoint;
    }

    public String getOtherServers()
    {
        return otherServers;
    }

    public void setOtherServers(String otherServers)
    {
        this.otherServers = otherServers;
    }

    public int getRetryCount()
    {
        return retryCount;
    }

    public void setRetryCount(int retryCount)
    {
        this.retryCount = retryCount;
    }
}
