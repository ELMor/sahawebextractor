// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JREReplicationMessage.java

package com.sitraka.deploy.sam.worker;

import com.sitraka.deploy.common.checkpoint.JRECheckpoint;
import com.sitraka.deploy.common.checkpoint.JREObject;

// Referenced classes of package com.sitraka.deploy.sam.worker:
//            ReplicationMessage

public class JREReplicationMessage extends ReplicationMessage
{

    public JREReplicationMessage(String remote_server, int type, JREObject jreObject, JRECheckpoint checkpoint, Object _auth, String otherServers)
    {
        super(remote_server, type, ((Object) (jreObject)), ((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), _auth, otherServers);
    }

    public String getVendor()
    {
        return getJREObject().getVendor();
    }

    public String getVersion()
    {
        return getJREObject().getVersion();
    }

    public String getPlatform()
    {
        return getJREObject().getPlatform();
    }

    public String getName()
    {
        return getJREObject().getName();
    }

    public String getFormat()
    {
        return getJREObject().getFormat();
    }

    public String getClasspaths()
    {
        return getJREObject().getClasspaths();
    }

    public JREObject getJREObject()
    {
        return (JREObject)((ReplicationMessage)this).getUserData();
    }

    public JRECheckpoint getJRECheckpoint()
    {
        return (JRECheckpoint)((ReplicationMessage)this).getCheckpoint();
    }
}
