// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractTrackerList.java

package com.sitraka.deploy.sam;

import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.SortUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam:
//            AbstractTrackerEntry, RequestParser

public abstract class AbstractTrackerList
    implements Requests
{

    protected Object CACHE_LOCK;
    protected File cacheDir;
    protected Vector cache;
    protected boolean cache_available;
    protected long max_cache_size;
    protected long max_cache_age;
    protected long total_size;

    public AbstractTrackerList()
    {
        CACHE_LOCK = new Object();
        cacheDir = null;
        cache = new Vector();
        cache_available = true;
        max_cache_size = -1L;
        max_cache_age = -1L;
        total_size = 0L;
    }

    protected void saveData()
    {
        if(!isAvailable() || cacheDir == null)
            return;
        Properties props = new Properties();
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = 0; i < size; i++)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                c.getProperties(props, i);
            }

        }
        File prop_file = new File(cacheDir, "cache.properties");
        try
        {
            FileOutputStream fos = new FileOutputStream(prop_file);
            props.save(((OutputStream) (fos)), "CACHE PROPERTIES");
            ((OutputStream) (fos)).flush();
            fos.close();
            fos = null;
        }
        catch(FileNotFoundException e) { }
        catch(IOException e) { }
    }

    protected String convertParserToString(RequestParser parser)
    {
        StringBuffer buffer = new StringBuffer();
        String command = parser.getCommand();
        buffer.append("command=");
        if(command != null)
            buffer.append(command + ",");
        buffer.append("app=");
        if(parser.getApplication() != null)
            buffer.append(parser.getApplication() + ",");
        buffer.append("version=");
        if(parser.getVersion() != null)
            buffer.append(parser.getVersion() + ",");
        buffer.append("platform=");
        if(parser.getPlatform() != null)
            buffer.append(parser.getPlatform() + ",");
        String result = buffer.toString().toLowerCase();
        buffer = new StringBuffer();
        buffer.append(result);
        buffer.append("file=");
        if(command != null && command.equalsIgnoreCase("FILE") && parser.getFile() != null)
        {
            String sorted = SortUtils.sortDelimitedString(parser.getFile(), ',');
            buffer.append(Hashcode.computeHash(sorted));
        }
        return buffer.toString();
    }

    protected boolean addEntry(AbstractTrackerEntry c)
    {
        if(!isAvailable())
            return false;
        if(c.isValid())
        {
            synchronized(CACHE_LOCK)
            {
                cache.addElement(((Object) (c)));
                total_size += c.getSize();
            }
            removeExpiredEntries();
            trimCacheToSize();
            return true;
        } else
        {
            return false;
        }
    }

    protected void removeEntry(AbstractTrackerEntry c)
    {
        if(c == null)
            return;
        if(!isAvailable())
            return;
        cache.removeElement(((Object) (c)));
        total_size -= c.getSize();
        if(total_size > 0L || cache.size() == 0)
            total_size = 0L;
        saveData();
    }

    protected void deleteEntry(AbstractTrackerEntry c)
    {
        if(c == null)
            return;
        removeEntry(c);
        if(!isAvailable())
            return;
        if(c.getFile() == null)
        {
            return;
        } else
        {
            if(c.getFile().delete());
            return;
        }
    }

    protected void removeExpiredEntries()
    {
        if(!isAvailable())
            return;
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = size - 1; i >= 0; i--)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                if(c.isExpired(max_cache_age))
                    deleteEntry(c);
                else
                if(c.getFile() != null && !c.getFile().canRead())
                    deleteEntry(c);
            }

        }
        saveData();
    }

    protected void trimCacheToSize()
    {
        if(!isAvailable())
            return;
        if(max_cache_size == 0x7fffffffffffffffL)
            return;
        synchronized(CACHE_LOCK)
        {
            if(total_size <= max_cache_size)
                return;
            while(total_size > max_cache_size) 
            {
                AbstractTrackerEntry c = findOldest();
                deleteEntry(c);
                saveData();
            }
        }
    }

    protected void removeUnknownFiles()
    {
        if(!isAvailable())
            return;
        if(cacheDir == null)
            return;
        String filenames[] = cacheDir.list();
        for(int i = 0; i < filenames.length; i++)
            if(!"cache.properties".equalsIgnoreCase(filenames[i]))
            {
                File f = new File(cacheDir, filenames[i]);
                AbstractTrackerEntry c = findByFile(f);
                if(c == null)
                    f.delete();
            }

    }

    protected AbstractTrackerEntry findOldest()
    {
        if(!isAvailable())
            return null;
        long last_date = System.currentTimeMillis();
        AbstractTrackerEntry found = null;
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = 0; i < size; i++)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                if(c.getAccessTime() < last_date || found == null)
                {
                    found = c;
                    last_date = c.getAccessTime();
                }
            }

        }
        return found;
    }

    protected void removeEntries(String search)
    {
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = size - 1; i >= 0; i--)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                if(c.getRequest().indexOf(search) != -1)
                    deleteEntry(c);
            }

        }
        saveCachedData();
        removeUnknownFiles();
    }

    protected void removeAllEntries()
    {
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = size - 1; i >= 0; i--)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                deleteEntry(c);
            }

        }
        saveCachedData();
        removeUnknownFiles();
    }

    protected AbstractTrackerEntry findByFile(File f)
    {
        if(!cache_available)
            return null;
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = 0; i < size; i++)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                if(c.getFile() == null && f == null)
                {
                    AbstractTrackerEntry abstracttrackerentry = c;
                    return abstracttrackerentry;
                }
                if(c.getFile().equals(((Object) (f))))
                {
                    AbstractTrackerEntry abstracttrackerentry1 = c;
                    return abstracttrackerentry1;
                }
            }

        }
        return null;
    }

    protected AbstractTrackerEntry findByID(long id)
    {
        if(!cache_available)
            return null;
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = 0; i < size; i++)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                if(c.getID() == id)
                {
                    AbstractTrackerEntry abstracttrackerentry = c;
                    return abstracttrackerentry;
                }
            }

        }
        return null;
    }

    protected AbstractTrackerEntry findByRequest(String request)
    {
        if(!cache_available)
            return null;
        synchronized(CACHE_LOCK)
        {
            int size = cache != null ? cache.size() : 0;
            for(int i = 0; i < size; i++)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                if(c.getRequest().equals(((Object) (request))))
                {
                    AbstractTrackerEntry abstracttrackerentry = c;
                    return abstracttrackerentry;
                }
            }

        }
        return null;
    }

    protected void removeBundle(String bundle_name)
    {
        if(!isAvailable())
            return;
        if(bundle_name == null)
        {
            return;
        } else
        {
            String search = "app=" + bundle_name.toLowerCase();
            removeEntries(search);
            return;
        }
    }

    protected void removeBundleVersion(String bundle_name, String version)
    {
        if(!isAvailable())
            return;
        if(bundle_name == null)
        {
            return;
        } else
        {
            String search = "app=" + bundle_name.toLowerCase() + ",version=" + version.toLowerCase();
            removeEntries(search);
            return;
        }
    }

    protected AbstractTrackerEntry getCachedRequest(RequestParser parser)
    {
        if(!isAvailable())
            return null;
        String request = convertParserToString(parser);
        if(request == null)
            return null;
        AbstractTrackerEntry c = findByRequest(request);
        if(c == null)
            return null;
        if(c.getFile() != null && !c.getFile().canRead())
        {
            removeEntry(c);
            return null;
        } else
        {
            c.updateAccess();
            saveCachedData();
            return c;
        }
    }

    public static long createID()
    {
        long l;
        synchronized(com.sitraka.deploy.sam.AbstractTrackerList.class)
        {
            l = System.currentTimeMillis();
        }
        return l;
    }

    protected long getMaxSize()
    {
        return max_cache_size;
    }

    protected void saveCachedData()
    {
        if(!isAvailable())
        {
            return;
        } else
        {
            saveData();
            return;
        }
    }

    protected int numFiles()
    {
        if(!isAvailable())
            return 0;
        int size = 0;
        synchronized(CACHE_LOCK)
        {
            size = cache.size();
        }
        return size;
    }

    protected long maxAge()
    {
        return max_cache_age;
    }

    protected long getSizeofCache()
    {
        if(!isAvailable())
            return 0L;
        long size = 0L;
        synchronized(CACHE_LOCK)
        {
            int count = cache != null ? cache.size() : 0;
            for(int i = 0; i < count; i++)
            {
                AbstractTrackerEntry c = (AbstractTrackerEntry)cache.elementAt(i);
                size += c.getSize();
            }

        }
        return size;
    }

    protected abstract boolean isAvailable();
}
