// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractMessageQueue.java

package com.sitraka.deploy.sam;

import java.util.Vector;

public abstract class AbstractMessageQueue extends Thread
{

    private Vector queue;
    protected boolean done;

    public AbstractMessageQueue()
    {
        queue = new Vector();
        done = false;
    }

    protected abstract void processMessage(Object obj);

    public void run()
    {
        do
        {
            if(done)
                break;
            Object m = getNextMessage();
            if(m == null || done)
                break;
            processMessage(m);
        } while(true);
    }

    public synchronized void addMessage(Object message)
    {
        if(done)
            return;
        synchronized(queue)
        {
            queue.addElement(message);
        }
        ((Object)this).notify();
    }

    protected synchronized Object getNextMessage()
    {
        if(done)
            return ((Object) (null));
        while(queue.size() == 0) 
        {
            if(done)
                return ((Object) (null));
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException e) { }
        }
        Object m = queue.elementAt(0);
        queue.removeElementAt(0);
        return m;
    }

    public synchronized void flushQueue()
    {
        Object m;
        for(; queue.size() > 0; processMessage(m))
        {
            m = queue.elementAt(0);
            queue.removeElementAt(0);
        }

    }

    public synchronized void stopQueue(boolean flush)
    {
        done = true;
        if(flush)
            flushQueue();
        ((Object)this).notify();
    }

    public synchronized void stopQueue()
    {
        stopQueue(true);
    }

    public String toString()
    {
        StringBuffer b = new StringBuffer();
        b.append(((Thread)this).getName() + "(priority: " + ((Thread)this).getPriority() + ")");
        b.append(" (group: " + ((Thread)this).getThreadGroup().getName() + ")");
        b.append(" (" + queue.size() + " elements)\n");
        return b.toString();
    }
}
