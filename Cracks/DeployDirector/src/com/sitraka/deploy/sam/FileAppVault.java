// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileAppVault.java

package com.sitraka.deploy.sam;

import com.klg.jclass.util.JCStringTokenizer;
import com.sitraka.deploy.AuthGroups;
import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.Authorization;
import com.sitraka.deploy.GroupAuthorization;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.JRE;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.platforms.PlatformJRE;
import com.sitraka.deploy.common.platforms.PlatformPlatform;
import com.sitraka.deploy.common.platforms.Platforms;
import com.sitraka.deploy.common.roles.BundleAdminRole;
import com.sitraka.deploy.common.roles.RoleAuthorization;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Vector;
import java.util.zip.ZipOutputStream;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.sam:
//            AppVault

public class FileAppVault extends AppVault
    implements Requests
{

    protected String app_directory;
    protected File basedir;
    protected Vector all_applications;
    protected String computed_servers;
    protected String computed_cluster_servers;
    protected String computed_other_servers;
    protected Platforms platforms;
    protected static final int BUF_SIZE = 8096;

    public FileAppVault()
    {
        app_directory = null;
        basedir = null;
        all_applications = new Vector();
        computed_servers = null;
        computed_cluster_servers = null;
        computed_other_servers = null;
        platforms = null;
        super.vault_name = "file";
    }

    public boolean isLocationSet()
    {
        return basedir != null && app_directory != null && platforms != null;
    }

    public void setLocation(String location)
        throws FileNotFoundException
    {
        if(location == null || location.equals(""))
            throw new FileNotFoundException("Provided base directory name is null or empty.");
        basedir = new File(location);
        if(!basedir.exists() || !basedir.isDirectory())
            throw new FileNotFoundException("Provided base directory does not exist or is not a directory");
        app_directory = location + File.separator + "vault";
        File app_file = new File(app_directory, "bundles.lst");
        if(!app_file.exists() || !app_file.canRead() || app_file.length() == 0L)
            throw new FileNotFoundException("Applications file \"" + app_file.getAbsolutePath() + "\" does not exist or cannot be read");
        BufferedReader parser = new BufferedReader(((java.io.Reader) (new FileReader(app_file))));
        String current_app = null;
        try
        {
            while(parser.ready()) 
            {
                String line = parser.readLine();
                if(line != null && !line.startsWith("#") && !line.equals(""))
                {
                    JCStringTokenizer tokenizer = new JCStringTokenizer(line);
                    current_app = null;
                    String appId = null;
                    String appCreationDate = null;
                    try
                    {
                        current_app = tokenizer.nextToken(',');
                        appId = tokenizer.nextToken(',');
                        appCreationDate = tokenizer.nextToken(',');
                    }
                    catch(NoSuchElementException e) { }
                    if(current_app != null && !current_app.equals(""))
                        addApplication(current_app, appId, appCreationDate);
                }
            }
        }
        catch(FileNotFoundException e)
        {
            throw new FileNotFoundException(((Throwable) (e)).getMessage() + " while loading '" + current_app + "'");
        }
        catch(IOException e)
        {
            throw new FileNotFoundException(((Throwable) (e)).getMessage() + " while loading '" + current_app + "'");
        }
        if(all_applications == null || all_applications.size() == 0)
            throw new FileNotFoundException("No applications found in the applications list");
        File platform_file = new File(basedir, "platform.xml");
        if(!platform_file.exists() || !platform_file.canRead())
            throw new FileNotFoundException("Platform file \"" + platform_file.getAbsolutePath() + "\" does not exist or cannot be read");
        try
        {
            platforms = new Platforms(platform_file);
        }
        catch(IOException e)
        {
            ((Throwable) (e)).printStackTrace();
            throw new FileNotFoundException("Error accessing file: " + ((Throwable) (e)).getMessage());
        }
        catch(SAXException saxy)
        {
            ((Throwable) (saxy)).printStackTrace();
            throw new FileNotFoundException("Error parsing file: " + saxy.getMessage());
        }
    }

    public boolean addApplication(String application_name, String app_id, String creation_date)
        throws FileNotFoundException
    {
        File appDir = new File(app_directory, application_name.toLowerCase());
        if(!appDir.exists() || !appDir.isDirectory())
            return false;
        try
        {
            Application app = new Application(application_name, appDir);
            app.setID(app_id);
            app.setCreationDate(creation_date);
            return addApplication(app);
        }
        catch(SAXException e)
        {
            throw new FileNotFoundException(e.getMessage());
        }
        catch(IOException e)
        {
            throw new FileNotFoundException(((Throwable) (e)).getMessage());
        }
    }

    public boolean addApplication(Application app)
    {
        if(all_applications == null)
            all_applications = new Vector();
        if(app != null)
        {
            all_applications.addElement(((Object) (app)));
            return true;
        } else
        {
            return false;
        }
    }

    public Application getApplication(String name)
    {
        Application app = null;
        if(name == null || name.equals(""))
            return null;
        for(int i = 0; i < all_applications.size(); i++)
        {
            Application this_app = (Application)all_applications.elementAt(i);
            if(!this_app.getName().equalsIgnoreCase(name))
                continue;
            app = this_app;
            break;
        }

        return app;
    }

    public boolean deleteApplication(String appname)
    {
        Application app = getApplication(appname);
        if(app == null)
            return false;
        File app_dir = getApplicationBaseDir(appname);
        if(!all_applications.removeElement(((Object) (app))))
            return false;
        if(!createApplicationsFile(((File) (null)), false))
            return false;
        else
            return FileUtils.deleteDirectory(app_dir);
    }

    public int countApplications()
    {
        int size = all_applications != null ? all_applications.size() : 0;
        int num_apps = size;
        for(int i = 0; i < size; i++)
        {
            Application app = (Application)all_applications.elementAt(i);
            String name = app.getName();
            if(name.equalsIgnoreCase("DDCAM") || name.equalsIgnoreCase("DDSDK") || name.equalsIgnoreCase("DDAdmin"))
                num_apps--;
        }

        return num_apps;
    }

    public boolean deleteVersion(String appname, String vername)
    {
        Application app = getApplication(appname);
        if(app == null)
            return false;
        Version ver = app.getVersion(vername);
        if(ver == null)
            return false;
        Vector all_versions = app.listVersions();
        if(all_versions.indexOf(((Object) (ver))) != -1 && all_versions.size() == 1)
            return deleteApplication(appname);
        File dir = new File(getApplicationBaseDir(appname), ver.getName().toLowerCase());
        if(!all_versions.removeElement(((Object) (ver))))
            return false;
        if(!createApplicationVersionsFile(app, ((File) (null)), false))
            return false;
        else
            return FileUtils.deleteDirectory(dir);
    }

    public boolean deleteJRE(String vendor, String version, String platform)
    {
        if(vendor == null || version == null || platform == null)
            return false;
        File jreDir = getJREBaseDirectory(platform, vendor, version, false);
        if(!jreDir.exists())
            return false;
        if(!getPlatforms().removePlatform(platform, vendor, version))
            return false;
        if(!createPlatformXMLFile(((File) (null)), true))
            return false;
        else
            return FileUtils.deleteDirectory(jreDir);
    }

    public boolean createPlatformXMLFile(File outputFile, boolean createBackup)
    {
        File sourceFile = getPlatformXML();
        if(sourceFile == null)
            return false;
        if(createBackup)
        {
            File destinationFile = new File(getServerTempDir(), "platform.xml");
            if(destinationFile.exists() && !destinationFile.delete())
                return false;
            if(!FileUtils.copyFiles(sourceFile, destinationFile))
                return false;
        }
        if(outputFile == null)
            outputFile = sourceFile;
        try
        {
            getPlatforms().saveXMLToFile(outputFile);
        }
        catch(IOException e)
        {
            ((Throwable) (e)).printStackTrace();
            if(createBackup)
            {
                File destinationFile = new File(getServerTempDir(), "platform.xml");
                FileUtils.copyFiles(destinationFile, sourceFile);
            }
            return false;
        }
        return true;
    }

    public boolean createApplicationsFile(File outputFile, boolean createBackup)
    {
        File sourceFile = new File(getVaultBaseDir(), "vault" + File.separator + "bundles.lst");
        if(createBackup)
        {
            File destinationFile = new File(getServerTempDir(), "bundles.lst");
            if(destinationFile.exists() && !destinationFile.delete())
                return false;
            if(!FileUtils.copyFiles(sourceFile, destinationFile))
                return false;
        }
        if(outputFile == null)
            outputFile = sourceFile;
        try
        {
            BufferedWriter writer = new BufferedWriter(((Writer) (new FileWriter(outputFile))));
            for(int i = 0; i < all_applications.size(); i++)
            {
                Application app = (Application)all_applications.elementAt(i);
                ((Writer) (writer)).write(app.getName());
                writer.newLine();
            }

            writer.flush();
            writer.close();
        }
        catch(IOException e)
        {
            if(createBackup)
            {
                if(outputFile.exists())
                    outputFile.delete();
                File backupFile = new File(getServerTempDir(), "bundles.lst");
                FileUtils.copyFiles(backupFile, outputFile);
            }
            boolean flag = false;
            return flag;
        }
        finally
        {
            if(createBackup)
            {
                File backupFile = new File(getServerTempDir(), "bundles.lst");
                backupFile.delete();
            }
        }
        return true;
    }

    public boolean createApplicationVersionsFile(Application app, File outputFile, boolean createBackup)
    {
        File sourceFile = new File(getApplicationBaseDir(app.getName()), "versions.lst");
        File backupFile = null;
        boolean appDirCreated = false;
        if(createBackup)
        {
            backupFile = new File(getServerTempDir(), app.getName().toLowerCase());
            if(!backupFile.exists())
                appDirCreated = true;
            backupFile = new File(backupFile, "versions.lst");
            if(backupFile.exists() && !backupFile.delete())
                return false;
            if(!FileUtils.copyFiles(sourceFile, backupFile))
                return false;
        }
        if(outputFile == null)
            outputFile = sourceFile;
        try
        {
            BufferedWriter writer = new BufferedWriter(((Writer) (new FileWriter(outputFile))));
            for(Enumeration e = app.listVersions().elements(); e.hasMoreElements(); writer.newLine())
            {
                Version v = (Version)e.nextElement();
                ((Writer) (writer)).write(v.getName());
            }

            writer.flush();
            writer.close();
        }
        catch(IOException e)
        {
            ((Throwable) (e)).printStackTrace();
            if(createBackup)
            {
                if(!backupFile.canRead())
                    return false;
                if(!FileUtils.copyFiles(backupFile, outputFile))
                    return false;
            }
            return false;
        }
        if(appDirCreated)
        {
            backupFile.delete();
            backupFile = new File(getServerTempDir(), app.getName().toLowerCase());
            backupFile.delete();
        }
        return true;
    }

    public File getServerTempDir()
    {
        File f = new File(getVaultBaseDir(), "dd" + File.separator + "temp");
        f.mkdirs();
        return f;
    }

    public File getServerCheckpointDir()
    {
        File f = new File(getVaultBaseDir(), "dd" + File.separator + "checkpt");
        f.mkdirs();
        return f;
    }

    public File getServerEtcDir()
    {
        File f = new File(getVaultBaseDir(), "etc");
        f.mkdirs();
        return f;
    }

    public File getVaultBaseDir()
    {
        return basedir;
    }

    public File getApplicationBaseDir(String app_name)
    {
        Application app = getApplication(app_name);
        if(app == null)
            return null;
        File app_base = new File(app_directory, app.getName().toLowerCase());
        if(!app_base.canRead())
            return null;
        else
            return app_base;
    }

    public File getVersionBaseDir(String app_name, String version_name)
    {
        Application app = getApplication(app_name);
        if(app == null)
            return null;
        Version ver = app.getVersion(version_name);
        if(ver == null)
            return null;
        File appbase = getApplicationBaseDir(app_name);
        File req_file = new File(appbase, ver.getName().toLowerCase());
        if(!req_file.canRead())
            return null;
        else
            return req_file;
    }

    public String[] listApplications()
    {
        String app_names[] = new String[all_applications.size()];
        for(int i = 0; i < all_applications.size(); i++)
            app_names[i] = ((Application)all_applications.elementAt(i)).getApplicationFileString();

        return app_names;
    }

    public String[] listBundles()
    {
        String bundles[] = new String[all_applications.size()];
        for(int i = 0; i < all_applications.size(); i++)
            bundles[i] = ((Application)all_applications.elementAt(i)).getName();

        return bundles;
    }

    public Platforms getPlatforms()
    {
        return platforms;
    }

    public File getPlatformXML()
    {
        File xml_file = new File(basedir, "platform.xml");
        if(!xml_file.canRead())
            return null;
        else
            return xml_file;
    }

    public File getVersionXML(String app_name, String version)
    {
        if(app_name == null || version == null)
        {
            return null;
        } else
        {
            File xml_file = new File(app_directory, app_name.toLowerCase() + File.separator + version.toLowerCase() + File.separator + "version.xml");
            return xml_file;
        }
    }

    public void setServers(Properties props)
    {
        String result = PropertyUtils.readDefaultString(props, "deploy.server.processedhosts", "");
        computed_servers = result.trim();
    }

    public synchronized String listServers()
    {
        if(computed_servers != null && computed_servers.length() != 0)
            return computed_servers;
        else
            return null;
    }

    public void setClusterServers(Properties props)
    {
        String result = PropertyUtils.readDefaultString(props, "deploy.cluster.processedhosts", "");
        computed_cluster_servers = result.trim();
    }

    public synchronized String listClusterServers()
    {
        if(computed_cluster_servers != null && computed_cluster_servers.length() != 0)
            return computed_cluster_servers;
        else
            return null;
    }

    public void setOtherServers(Properties props)
    {
        String result = PropertyUtils.readDefaultString(props, "deploy.otherhosts", "");
        computed_other_servers = result.trim();
    }

    public synchronized String listOtherServers()
    {
        if(computed_other_servers != null && computed_other_servers.length() != 0)
            return computed_other_servers;
        else
            return null;
    }

    public String getLatestVersion(String app_name)
    {
        Application app = getApplication(app_name);
        if(app == null)
            return null;
        Vector versions = app.listVersions();
        if(versions == null)
            return null;
        Version ver = (Version)versions.elementAt(0);
        if(ver == null)
            return null;
        else
            return ver.getName();
    }

    public Authorization getAuthorizationObject(String bundleName, String version)
    {
        Authorization authorization = super.getAuthorizationObject(bundleName, version);
        File authDir = new File(getVaultBaseDir(), "vault" + File.separator + "auth");
        if(authorization.usesDataFile())
        {
            String authFileName = ((AppVault)this).getAuthorizationFileName(bundleName, version);
            try
            {
                authorization.setDataFile(new File(authDir, authFileName));
            }
            catch(IOException e)
            {
                return null;
            }
        }
        if((authorization instanceof GroupAuthorization) && ((GroupAuthorization)authorization).getAuthGroups().usesDataFile())
        {
            String authFileName = ((AppVault)this).getAuthGroupsFileName(bundleName, version);
            AuthGroups groups = ((GroupAuthorization)authorization).getAuthGroups();
            try
            {
                groups.setDataFile(new File(authDir, authFileName));
            }
            catch(IOException e) { }
        }
        return authorization;
    }

    public Authentication getAuthenticationObject(String bundleName, String version)
    {
        Authentication authentication = super.getAuthenticationObject(bundleName, version);
        if(authentication.usesDataFile())
        {
            File vault_base = getVaultBaseDir();
            String authFileName = ((AppVault)this).getAuthenticationFileName(bundleName, version);
            try
            {
                authentication.setDataFile(new File(vault_base + File.separator + "vault" + File.separator + "auth" + File.separator + authFileName));
            }
            catch(IOException e)
            {
                return null;
            }
        }
        return authentication;
    }

    public String getLatestAuthorizedVersion(String bundleName, String userData)
    {
        Application app = getApplication(bundleName);
        if(app == null)
            return null;
        Vector versions = app.listVersions();
        if(versions == null || versions.size() == 0)
            return null;
        Version ver = (Version)versions.elementAt(0);
        Authentication authentication = getAuthenticationObject(bundleName, ver.getName());
        Authorization authorization = getAuthorizationObject(bundleName, ver.getName());
        RoleAuthorization adminAuthorization = (RoleAuthorization)getAuthorizationObject("DDAdmin", ((String) (null)));
        String userName = authentication.getUserID(((Object) (userData)));
        if(adminAuthorization.isRoleAuthorized(((com.sitraka.deploy.common.roles.Role) (new BundleAdminRole(userName, bundleName)))) == 1)
            return ver.getName();
        String authVersion = null;
        for(int i = 0; i < versions.size(); i++)
        {
            ver = (Version)versions.elementAt(i);
            if(authorization.isAuthorized(userName, bundleName, ver.getName()) != 1)
                continue;
            authVersion = ver.getName();
            break;
        }

        return authVersion;
    }

    public File getLicenseFile(String app_name, String version)
    {
        Application app = getApplication(app_name);
        if(app == null)
            return null;
        Version ver = app.getVersion(version);
        if(ver == null)
            return null;
        File req_file = new File(app_directory, app_name.toLowerCase() + File.separator + ver.getName().toLowerCase() + File.separator + ver.getLicenseFilename());
        if(!req_file.exists() || !req_file.canRead())
            return null;
        else
            return req_file;
    }

    public File getApplicationFile(String app_name, String version, String filename, String platform)
    {
        Application app = getApplication(app_name);
        if(app == null)
            return null;
        Version ver = app.getVersion(version);
        if(ver == null)
            return null;
        String file = ver.getPathToFile(filename, platform);
        if(file == null)
            return null;
        File req_file = new File(app_directory, app_name.toLowerCase() + File.separator + file);
        if(!req_file.exists() || !req_file.canRead())
            return null;
        else
            return req_file;
    }

    public File getAbsoluteFile(String fileName)
    {
        return new File(basedir, fileName);
    }

    public File getInstaller(int client_type)
    {
        File req_file = null;
        String filename = basedir.getAbsolutePath() + File.separator + "installer" + File.separator + "install.";
        if(client_type == 3)
            filename = filename + "cab";
        else
            filename = filename + "jar";
        req_file = new File(filename);
        if(!req_file.exists() || !req_file.canRead())
            return null;
        else
            return req_file;
    }

    public File getInstaller(String filename)
    {
        File installerFile = null;
        File installerDir = new File(basedir, "installer");
        if(filename.toLowerCase().endsWith("install.cab"))
            installerFile = new File(installerDir, "install.cab");
        else
        if(filename.toLowerCase().endsWith("install.jar"))
            installerFile = new File(installerDir, "install.jar");
        if(installerFile != null && installerFile.exists() && installerFile.canRead())
            return installerFile;
        else
            return null;
    }

    public File getLauncher(int client_type)
    {
        File req_file = null;
        String filename = basedir.getAbsolutePath() + File.separator + "installer" + File.separator + "launch.";
        if(client_type == 3)
            filename = filename + "cab";
        else
            filename = filename + "jar";
        req_file = new File(filename);
        if(!req_file.exists() || !req_file.canRead())
            return null;
        else
            return req_file;
    }

    public File getLauncher(String filename)
    {
        File launcherFile = null;
        File launcherDir = new File(basedir, "installer");
        if(filename.toLowerCase().endsWith("launch.cab"))
            launcherFile = new File(launcherDir, "launch.cab");
        else
        if(filename.toLowerCase().endsWith("launch.jar"))
            launcherFile = new File(launcherDir, "launch.jar");
        if(launcherFile != null && launcherFile.exists() && launcherFile.canRead())
            return launcherFile;
        else
            return null;
    }

    public File getJREZip(String platform_name, String application, String version)
    {
        if(platform_name == null || application == null || version == null)
            return null;
        Application a = getApplication(application);
        if(a == null)
            return null;
        Version v = a.getVersion(version);
        if(v == null)
            return null;
        JRE jres = v.getJRE(platform_name);
        if(jres == null)
            return null;
        PlatformJRE j = getPlatformJRE(platform_name, jres.getVersion(), jres.getVendor());
        if(j == null)
            return null;
        else
            return prebuildJRE(platform_name, j.getVendor(), j.getVersion());
    }

    public File prebuildJRE(String platform_name, String vendor, String version)
    {
        PlatformJRE j = getPlatformJRE(platform_name, version, vendor);
        if(j != null);
        File jre_directory = new File(basedir, "jres" + File.separator + j.getDirectory());
        if(!jre_directory.exists())
            jre_directory = new File(basedir, "jres" + File.separator + j.getDirectory().replace('.', '_'));
        File prebuilt_jre = new File(jre_directory, "prebuilt.zip");
        if(prebuilt_jre.canRead())
            return prebuilt_jre;
        if(!(new File(jre_directory, "jre.zip")).canRead())
            throw new IllegalArgumentException("JRE version '" + version + "' from '" + vendor + "' on '" + platform_name + "' was listed in " + "the platform.xml, but does not " + "exist in the vault.");
        File prop_file = new File(jre_directory, "jre.properties");
        j.writePropertiesFile(basedir.getAbsolutePath() + File.separator + "jres");
        Properties props = new Properties();
        try
        {
            props.load(((java.io.InputStream) (new FileInputStream(prop_file))));
        }
        catch(FileNotFoundException fnfe)
        {
            return null;
        }
        catch(IOException ioe)
        {
            return null;
        }
        File jre = new File(jre_directory, "jre.zip");
        String hashcode = Hashcode.computeHash(jre);
        ((Hashtable) (props)).put("java.checksum", ((Object) (hashcode)));
        ((Hashtable) (props)).put("java.size", ((Object) ("" + jre.length())));
        ((Hashtable) (props)).put("java.uncompressedsize", ((Object) ("" + FileUtils.computeUncompressedZipSize(jre))));
        try
        {
            props.save(((java.io.OutputStream) (new FileOutputStream(prop_file))), "JRE INFO");
        }
        catch(IOException e)
        {
            return null;
        }
        File tmp_zip_file = FileUtils.createTempFile();
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(tmp_zip_file);
        }
        catch(IOException ioe)
        {
            return null;
        }
        ZipOutputStream zipper = new ZipOutputStream(((java.io.OutputStream) (fos)));
        zipper.setLevel(0);
        FileUtils.addToZipStream(zipper, prop_file, prop_file.getName());
        FileUtils.addToZipStream(zipper, jre, jre.getName());
        try
        {
            ((FilterOutputStream) (zipper)).flush();
            zipper.close();
        }
        catch(IOException e) { }
        tmp_zip_file.renameTo(prebuilt_jre);
        System.gc();
        tmp_zip_file.delete();
        prop_file.delete();
        jre.delete();
        return prebuilt_jre;
    }

    public File getJREBaseDirectory(String platform_name, String vendor, String version, boolean create)
    {
        if(platform_name == null || vendor == null || version == null)
            return null;
        File jredir = new File(basedir, "jres" + File.separator + FileUtils.convertNameToFile(platform_name, '\0') + File.separator + FileUtils.convertNameToFile(vendor, '\0') + File.separator + version.toLowerCase());
        if(create && !jredir.exists() && !jredir.mkdirs())
            return null;
        else
            return jredir;
    }

    public PlatformJRE getPlatformJRE(String platformName, String jreVersion, String jreVendor)
    {
        PlatformPlatform p = platforms.findMatchingPlatform(platformName);
        if(p == null)
        {
            return null;
        } else
        {
            PlatformJRE j = null;
            j = p.findMatchingJRE(jreVersion, jreVendor);
            return j;
        }
    }
}
