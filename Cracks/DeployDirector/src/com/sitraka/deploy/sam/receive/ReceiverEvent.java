// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ReceiverEvent.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.checkpoint.MetaInfo;
import java.util.EventObject;

public class ReceiverEvent extends EventObject
{

    protected Object source;
    protected MetaInfo metaInfo;
    protected boolean status;

    public ReceiverEvent(Object source, MetaInfo metaInfo)
    {
        super(source);
        this.metaInfo = metaInfo;
    }

    public MetaInfo getMetaInfo()
    {
        return metaInfo;
    }

    public boolean getStatus()
    {
        return status;
    }
}
