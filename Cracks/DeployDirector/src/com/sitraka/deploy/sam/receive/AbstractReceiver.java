// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractReceiver.java

package com.sitraka.deploy.sam.receive;

import com.klg.jclass.util.JCListenerList;
import com.sitraka.deploy.common.checkpoint.Checkpoint;
import com.sitraka.deploy.common.checkpoint.MetaInfo;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.File;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            ReceiverListener, ReceiverEvent

public abstract class AbstractReceiver
{

    public static final int MAX_DOWNLOAD_ATTEMPTS = 3;
    protected ServerActions server;
    protected AppVault vault;
    protected Object adminIDObject;
    protected JCListenerList listeners;
    protected static JCListenerList globalListeners = null;
    protected String none;
    protected String unknown;

    public AbstractReceiver(ServerActions server, AppVault vault)
    {
        this.server = null;
        this.vault = null;
        adminIDObject = null;
        listeners = null;
        none = "(none)";
        unknown = "(unknown)";
        this.server = server;
        this.vault = vault;
    }

    public void setAdminIDObject(Object id)
    {
        adminIDObject = id;
    }

    public synchronized void addListener(ReceiverListener l)
    {
        listeners = JCListenerList.add(listeners, ((Object) (l)));
    }

    public synchronized void removeListener(ReceiverListener l)
    {
        listeners = JCListenerList.remove(listeners, ((Object) (l)));
    }

    public static synchronized void addGlobalListener(ReceiverListener l)
    {
        globalListeners = JCListenerList.add(globalListeners, ((Object) (l)));
    }

    public static synchronized void removeGlobalListener(ReceiverListener l)
    {
        globalListeners = JCListenerList.remove(globalListeners, ((Object) (l)));
    }

    protected void fireReceiveZipComplete(ReceiverEvent event)
    {
        Enumeration e = JCListenerList.elements(listeners);
        for(int i = 0; i < 2; i++)
        {
            while(e.hasMoreElements()) 
            {
                ReceiverListener listener = (ReceiverListener)e.nextElement();
                listener.receiveZipComplete(event);
            }
            e = JCListenerList.elements(globalListeners);
        }

    }

    protected Vector getOtherServers()
    {
        Vector servers = PropertyUtils.readVector(Servlet.properties, "deploy.otherhosts", ',');
        if(servers.size() == 0)
            return null;
        else
            return servers;
    }

    protected void receiveZip(Checkpoint checkpoint, HttpConnection conn, InputStream is, boolean moveFiles)
        throws ServletProcessException
    {
        if(is == null && conn != null)
            is = ((AbstractHttpConnection) (conn)).executeCommand();
        if(is == null)
        {
            String msg = "INTERNAL receiveZip failed; could not obtain input stream to read from";
            listeners = null;
            throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
        }
        if(checkpoint.getSourceDir() == null)
            checkpoint.setSourceDir(FileUtils.createTempFile(checkpoint.getTempDir()));
        File targetDir = checkpoint.getSourceDir();
        if(targetDir == null)
        {
            String msg = "INTERNAL receiveZip failed; No target directory specified";
            listeners = null;
            throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
        }
        File tempFile = FileUtils.createTempFile(checkpoint.getTempDir());
        if(checkpoint.isIncomplete(300))
        {
            if(!FileUtils.saveStreamToFile(is, tempFile))
            {
                tempFile.delete();
                listeners = null;
                String msg = "INTERNAL receiveZip failed; could not save stream to file: " + tempFile.getAbsolutePath();
                throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
            }
            fireReceiveZipComplete(new ReceiverEvent(((Object) (this)), (MetaInfo)checkpoint.getUserData()));
            if(!checkpoint.extractFileDataFromZip(tempFile))
            {
                tempFile.delete();
                listeners = null;
                String msg = "INTERNAL receiveZip failed; could not extract file data from: " + tempFile.getAbsolutePath();
                throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
            }
            checkpoint.setState(300);
            if(!FileUtils.unzipFile(tempFile, targetDir))
            {
                tempFile.delete();
                listeners = null;
                String msg = "INTERNAL receiveZip failed; could not unzip: " + tempFile.getAbsolutePath();
                throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
            }
        }
        if(checkpoint.isIncomplete(400))
        {
            checkpoint.validateFiles();
            String badFiles = checkpoint.listInvalidFiles();
            if(badFiles != null)
            {
                if(conn == null)
                {
                    tempFile.delete();
                    listeners = null;
                    String msg = "INTERNAL receiveZip failed; unable to redownload invalid files: '" + badFiles + "' as this is an upload from the admin tool.";
                    throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
                }
                try
                {
                    downloadInvalidFiles(checkpoint, conn);
                }
                catch(ServletProcessException spe)
                {
                    tempFile.delete();
                    listeners = null;
                    throw spe;
                }
            }
            checkpoint.setState(400);
        }
        if(checkpoint.isIncomplete(500))
        {
            if(!checkpoint.getSourceDir().equals(((Object) (checkpoint.getDestinationDir()))))
            {
                boolean status = false;
                if(!moveFiles)
                    status = FileUtils.renameTo(checkpoint.getSourceDir(), checkpoint.getDestinationDir());
                if(!status || moveFiles)
                {
                    status = checkpoint.moveFiles();
                    FileUtils.deleteDirectory(checkpoint.getSourceDir());
                }
                if(!status)
                {
                    tempFile.delete();
                    listeners = null;
                    FileUtils.deleteDirectory(checkpoint.getSourceDir());
                    String msg = "INTERNAL receiveZip failed; cannot move files to destination: " + checkpoint.getDestinationDir();
                    throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
                }
                FileUtils.deleteDirectory(checkpoint.getSourceDir());
            }
            checkpoint.setState(500);
        }
        tempFile.delete();
        listeners = null;
    }

    protected void downloadInvalidFiles(Checkpoint checkpoint, HttpConnection conn)
        throws ServletProcessException
    {
        if(conn == null || checkpoint == null)
        {
            String msg = "Unable to redownload invalid files; connection orcheckpoint is null.";
            throw new ServletProcessException(msg, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
        }
        File tempFile = null;
        boolean finished = false;
        for(int numberOfTries = 0; numberOfTries < 3 && !finished; numberOfTries++)
        {
            String badFiles = checkpoint.listInvalidFiles();
            if(badFiles == null)
            {
                finished = true;
            } else
            {
                tempFile = FileUtils.createTempFile(checkpoint.getTempDir());
                ((AbstractHttpConnection) (conn)).setFilename(FileUtils.makeInternalPath(badFiles));
                ((AbstractHttpConnection) (conn)).setPlatform("DeploySam");
                ((AbstractHttpConnection) (conn)).setIsAdmin(true);
                ((AbstractHttpConnection) (conn)).setUserIDObject(adminIDObject);
                InputStream is = ((AbstractHttpConnection) (conn)).executeCommand("FILE");
                if(is == null)
                    server.logMessage((String)null, "ServerRequestFailed", unknown, unknown, "INTERNAL receiveZip failed; unable to redownload invalid files: " + ((AbstractHttpConnection) (conn)).getResponseExplanation());
                else
                if(!FileUtils.saveStreamToFile(is, tempFile))
                    server.logMessage((String)null, "ServerRequestFailed", unknown, unknown, "INTERNAL receiveZip failed; could not save stream to file: " + tempFile.getAbsolutePath());
                else
                if(FileUtils.unzipFile(tempFile, checkpoint.getSourceDir()))
                {
                    int goodFileCount = checkpoint.validateFiles();
                    if(goodFileCount > 0)
                        numberOfTries = -1;
                    else
                    if(goodFileCount == -1)
                        finished = true;
                }
            }
            tempFile.delete();
        }

        if(!finished)
        {
            String text = "Unable to re-download corrupted files after standard number of attempts, aborting";
            throw new ServletProcessException(text, "ServerRequestFailed", unknown, unknown, ((String) (null)), true);
        } else
        {
            return;
        }
    }

}
