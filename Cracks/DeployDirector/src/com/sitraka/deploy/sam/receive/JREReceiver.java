// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JREReceiver.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.JRECheckpoint;
import com.sitraka.deploy.common.checkpoint.JREObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.common.platforms.PlatformClassPath;
import com.sitraka.deploy.common.platforms.PlatformJRE;
import com.sitraka.deploy.common.platforms.PlatformPlatform;
import com.sitraka.deploy.common.platforms.Platforms;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.File;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver

public class JREReceiver extends AbstractReceiver
{

    protected String name;
    protected String format;
    protected String classpaths;

    public JREReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
        name = null;
        format = null;
        classpaths = null;
    }

    public Vector makeClassPaths(String classpaths)
    {
        Vector v = PropertyUtils.readVector(classpaths, ',');
        Vector paths = new Vector(v.size());
        PlatformClassPath classPath;
        for(Enumeration e = v.elements(); e.hasMoreElements(); paths.addElement(((Object) (classPath))))
        {
            classPath = new PlatformClassPath();
            classPath.setFileName((String)e.nextElement());
        }

        return paths;
    }

    public synchronized void processMessage(JRECheckpoint checkpoint)
        throws ServletProcessException
    {
        PlatformPlatform platform = null;
        JREObject jo = checkpoint.getJREObject();
        String remoteServerName = super.server.getServerURI(jo.getServer());
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            File destinationDir = super.vault.getJREBaseDirectory(jo.getPlatform(), jo.getVendor(), jo.getVersion(), false);
            if(destinationDir.exists())
            {
                String msg = "JRE upload from " + remoteServerName + " failed; " + "JRE directory for " + jo.getVendor() + " ver " + jo.getVersion() + " for " + jo.getPlatform() + " already exists.";
                throw new ServletProcessException(msg, "ServerRequestFailed", jo.getVendor() + " JRE", jo.getVersion(), remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setSourceDir(destinationDir);
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(destinationDir);
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection http = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setCommand("SERVERJRE");
                ((AbstractHttpConnection) (http)).setVendor(jo.getVendor());
                ((AbstractHttpConnection) (http)).setVersion(jo.getVersion());
                ((AbstractHttpConnection) (http)).setPlatform(jo.getPlatform());
                ((AbstractHttpConnection) (http)).setUserIDObject(super.adminIDObject);
                Vector others = ((AbstractReceiver)this).getOtherServers();
                if(others == null)
                {
                    String msg = "JRE replication: Could not create HTTP connection to other servers.  No other servers listed in the cluster. Request came from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", super.none, super.none, remoteServerName, true);
                }
                ((AbstractHttpConnection) (http)).setServerList(others);
            }
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), http, is, true);
            }
            catch(ServletProcessException spe)
            {
                String msg = "JRE upload from " + remoteServerName + " failed; " + "receiveZip failed, upload rejected.";
                spe.setApplication(jo.getVendor() + " JRE");
                spe.setVersion(jo.getVersion());
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
            ((BaseCheckpoint) (checkpoint)).setState(500);
        }
        super.server.logMessage(remoteServerName, "ServerJREReceived", jo.getVendor() + " JRE", jo.getVersion(), "Received all files for JRE from: " + remoteServerName);
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
        {
            platform = super.vault.getPlatforms().addPlatformSubtree(jo.getPlatform());
            if(platform == null)
            {
                String msg = "JRE upload from " + remoteServerName + " failed; " + "addPlatformSubtree failed; upload rejected.";
                throw new ServletProcessException(msg, "ServerRequestFailed", jo.getVendor() + " JRE", jo.getVersion(), remoteServerName, true);
            }
            PlatformJRE jre = new PlatformJRE();
            jre.setVendor(jo.getVendor());
            jre.setVersion(jo.getVersion());
            jre.setExecutableName(jo.getName());
            jre.setFormat(jo.getFormat());
            jre.setClassPaths(makeClassPaths(jo.getClasspaths()));
            platform.addJRE(jre);
            if(!super.vault.createPlatformXMLFile(((File) (null)), true))
            {
                String msg = "JRE upload from " + remoteServerName + " failed; " + "createPlatformXMLFile failed; upload rejected.";
                throw new ServletProcessException(msg, "ServerRequestFailed", jo.getVendor() + " JRE", jo.getVersion(), remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setState(600);
        }
        super.vault.prebuildJRE(jo.getPlatform(), jo.getVendor(), jo.getVersion());
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
    }
}
