// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ReceiverListener.java

package com.sitraka.deploy.sam.receive;


// Referenced classes of package com.sitraka.deploy.sam.receive:
//            ReceiverEvent

public interface ReceiverListener
{

    public abstract void receiveZipComplete(ReceiverEvent receiverevent);

    public abstract void receiveFileComplete(ReceiverEvent receiverevent);
}
