// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AppReceiver.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.checkpoint.ApplicationCheckpoint;
import com.sitraka.deploy.common.checkpoint.ApplicationObject;
import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver

public class AppReceiver extends AbstractReceiver
{

    public AppReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
    }

    public synchronized void processMessage(ApplicationCheckpoint checkpoint)
        throws ServletProcessException
    {
        ApplicationObject ao = checkpoint.getApplicationObject();
        String app_name = ao.getApplication();
        String remoteServerName = super.server.getServerURI(ao.getServer());
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            File destinationDir = new File(super.vault.getVaultBaseDir(), "vault");
            if(super.vault.getApplication(app_name) != null)
            {
                String msg = "Error uploading application from " + remoteServerName + ".  Application \"" + app_name + "\" already exists in vault; upload rejected";
                throw new ServletProcessException(msg, "ServerRequestFailed", app_name, super.none, remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(destinationDir);
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection conn = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                conn = new HttpConnection();
                ((AbstractHttpConnection) (conn)).setIsAdmin(true);
                ((AbstractHttpConnection) (conn)).setCommand("SERVERBUNDLE");
                ((AbstractHttpConnection) (conn)).setApplication(app_name);
                ((AbstractHttpConnection) (conn)).setUserIDObject(super.adminIDObject);
                Vector others = ((AbstractReceiver)this).getOtherServers();
                if(others == null)
                {
                    String msg = "Bundle replication: Could not create HTTP connection to other servers.  No other servers listed in the cluster. Request came from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", app_name, super.none, remoteServerName, true);
                }
                ((AbstractHttpConnection) (conn)).setServerList(others);
            }
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), conn, is, false);
            }
            catch(ServletProcessException spe)
            {
                String msg = "receiveZip failed; upload rejected from \"" + remoteServerName + "\".";
                spe.setApplication(app_name);
                spe.setVersion(super.none);
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
        }
        super.server.logMessage(remoteServerName, "ServerBundleReceived", app_name, "(none - new bundle)", "(" + app_name + ") " + "From: " + remoteServerName);
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
        {
            Application app = null;
            File appDir = null;
            try
            {
                appDir = new File(((BaseCheckpoint) (checkpoint)).getDestinationDir(), app_name.toLowerCase());
                app = new Application(app_name, appDir);
                if(!super.vault.addApplication(app))
                {
                    appDir.delete();
                    String msg = "addApplication failed for " + app_name + "; upload rejected from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", app_name, super.none, remoteServerName, true);
                }
                Version ver = (Version)app.listVersions().elementAt(0);
                ver.computeFileHashAndSize(appDir);
                ver.saveXMLToFile(super.vault.getVersionXML(app_name, ver.getName()));
            }
            catch(SAXException se)
            {
                appDir.delete();
                String msg = "SAXException during creation of new application \"" + app_name + "\"; upload rejected from: " + remoteServerName;
                throw new ServletProcessException(msg, ((Throwable) (se)), "ServerRequestFailed", app_name, super.none, remoteServerName, true);
            }
            catch(IOException ioe)
            {
                appDir.delete();
                String msg = "IOException during creation of new application \"" + app_name + "\"; upload rejected from: " + remoteServerName;
                throw new ServletProcessException(msg, ((Throwable) (ioe)), "ServerRequestFailed", app_name, super.none, remoteServerName, true);
            }
            if(!super.vault.createApplicationsFile(((File) (null)), true))
            {
                appDir.delete();
                String msg = "createApplicationsFile failed for " + app_name + "; upload rejected from " + remoteServerName;
                throw new ServletProcessException(msg, "ServerRequestFailed", app_name, super.none, remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setState(600);
        }
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
    }
}
