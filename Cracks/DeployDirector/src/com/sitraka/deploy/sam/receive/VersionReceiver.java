// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   VersionReceiver.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.VersionCheckpoint;
import com.sitraka.deploy.common.checkpoint.VersionObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import java.io.File;
import java.io.IOException;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver

public class VersionReceiver extends AbstractReceiver
{

    public VersionReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
    }

    public synchronized void processMessage(VersionCheckpoint checkpoint)
        throws ServletProcessException
    {
        VersionObject vo = checkpoint.getVersionObject();
        String remoteServerName = super.server.getServerURI(vo.getServer());
        String app_name = vo.getApplication();
        String ver_name = vo.getVersion();
        Application app = super.vault.getApplication(app_name);
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            if(app == null)
            {
                String msg = "Version upload from " + remoteServerName + " failed:  Application " + app_name + " does not exist in the vault.";
                throw new ServletProcessException(msg, "ServerRequestFailed", app_name, ver_name, remoteServerName, true);
            }
            if(app.getVersion(ver_name) != null)
            {
                String msg = "Version upload from " + remoteServerName + " failed:  version " + ver_name + " of application " + app_name + " already exists in the vault.";
                throw new ServletProcessException(msg, "ServerRequestFailed", app_name, ver_name, remoteServerName, true);
            }
            File destinationDir = new File(super.vault.getVaultBaseDir(), "vault" + File.separator + app_name.toLowerCase());
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(destinationDir);
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection http = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                http = new HttpConnection();
                ((AbstractHttpConnection) (http)).setIsAdmin(true);
                ((AbstractHttpConnection) (http)).setCommand("SERVERVERSION");
                ((AbstractHttpConnection) (http)).setApplication(app_name);
                ((AbstractHttpConnection) (http)).setVersion(ver_name);
                ((AbstractHttpConnection) (http)).setUserIDObject(super.adminIDObject);
                java.util.Vector others = ((AbstractReceiver)this).getOtherServers();
                if(others == null)
                {
                    String msg = "Version replication: Could not create HTTP connection to other servers.  No other servers listed in the cluster. Request came from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", app_name, ver_name, remoteServerName, true);
                }
                ((AbstractHttpConnection) (http)).setServerList(others);
            }
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), http, is, false);
            }
            catch(ServletProcessException spe)
            {
                String msg = "receiveZip failed; upload rejected from: " + remoteServerName;
                spe.setApplication(app_name);
                spe.setVersion(ver_name);
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
        }
        super.server.logMessage(remoteServerName, "ServerVersionReceived", app_name, ver_name, "(" + app_name + " " + ver_name + ") " + "From: " + remoteServerName);
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
        {
            File versionXML = super.vault.getVersionXML(app_name, ver_name);
            try
            {
                Version newVersion = new Version(versionXML);
                app.addVersionToHead(newVersion);
                newVersion.computeFileHashAndSize(super.vault.getApplicationBaseDir(app_name));
                newVersion.saveXMLToFile(versionXML);
            }
            catch(IOException ioe)
            {
                String msg = "Version upload from " + remoteServerName + " failed:  IOException during creation of new " + "version " + ver_name + " of " + app_name;
                throw new ServletProcessException(msg, ((Throwable) (ioe)), "ServerRequestFailed", app_name, ver_name, remoteServerName, true);
            }
            catch(XmlException xmle)
            {
                String msg = "Version upload from " + remoteServerName + " failed:  XMLException during creation of new " + "version " + ver_name + " of " + app_name;
                throw new ServletProcessException(msg, ((Throwable) (xmle)), "ServerRequestFailed", app_name, ver_name, remoteServerName, true);
            }
            if(!super.vault.createApplicationVersionsFile(app, ((File) (null)), true))
            {
                String msg = "Version upload from " + remoteServerName + " failed:  Error creating application versions file " + "for " + app_name;
                throw new ServletProcessException(msg, "ServerRequestFailed", app_name, ver_name, remoteServerName, true);
            }
            ((BaseCheckpoint) (checkpoint)).setState(600);
        }
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
    }
}
