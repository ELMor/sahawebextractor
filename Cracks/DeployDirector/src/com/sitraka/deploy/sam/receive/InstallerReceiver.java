// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   InstallerReceiver.java

package com.sitraka.deploy.sam.receive;

import com.sitraka.deploy.common.checkpoint.BaseCheckpoint;
import com.sitraka.deploy.common.checkpoint.InstallerCheckpoint;
import com.sitraka.deploy.common.checkpoint.InstallerObject;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.sam.AppVault;
import com.sitraka.deploy.sam.ServerActions;
import com.sitraka.deploy.sam.servlet.ServletProcessException;
import com.sitraka.deploy.util.FileUtils;
import java.io.File;

// Referenced classes of package com.sitraka.deploy.sam.receive:
//            AbstractReceiver

public class InstallerReceiver extends AbstractReceiver
{

    public InstallerReceiver(ServerActions server, AppVault vault)
    {
        super(server, vault);
    }

    public synchronized void processMessage(InstallerCheckpoint checkpoint)
        throws ServletProcessException
    {
        InstallerObject io = checkpoint.getInstallerObject();
        String remoteServerName = super.server.getServerURI(io.getServer());
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(200))
        {
            ((BaseCheckpoint) (checkpoint)).setSourceDir(FileUtils.createTempFile(((BaseCheckpoint) (checkpoint)).getTempDir()));
            File fullPath = new File(super.vault.getVaultBaseDir(), io.getFilename());
            File destinationDir = new File(fullPath.getParent());
            ((BaseCheckpoint) (checkpoint)).setDestinationDir(destinationDir);
            ((BaseCheckpoint) (checkpoint)).setState(200);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(500))
        {
            HttpConnection conn = null;
            java.io.InputStream is = ((BaseCheckpoint) (checkpoint)).getInputStream();
            if(is == null)
            {
                conn = new HttpConnection();
                ((AbstractHttpConnection) (conn)).setIsAdmin(true);
                ((AbstractHttpConnection) (conn)).setUserIDObject(super.adminIDObject);
                ((AbstractHttpConnection) (conn)).setCommand("FILE");
                ((AbstractHttpConnection) (conn)).setFilename(FileUtils.makeInternalPath(io.getFilename()));
                ((AbstractHttpConnection) (conn)).setPlatform("DeploySam");
                java.util.Vector others = ((AbstractReceiver)this).getOtherServers();
                if(others == null)
                {
                    String msg = "JRE replication: Could not create HTTP connection to other servers.  No other servers listed in the cluster.  Request came from: " + remoteServerName;
                    throw new ServletProcessException(msg, "ServerRequestFailed", super.none, super.none, remoteServerName, true);
                }
                ((AbstractHttpConnection) (conn)).setServerList(others);
            }
            try
            {
                ((AbstractReceiver)this).receiveZip(((com.sitraka.deploy.common.checkpoint.Checkpoint) (checkpoint)), conn, is, true);
            }
            catch(ServletProcessException spe)
            {
                String msg = "receiveZip failed; upload rejected from \"" + remoteServerName + "\".";
                spe.setRemoteName(remoteServerName);
                throw spe;
            }
            super.server.logMessage(remoteServerName, "ServerInstallerReceived", super.none, super.none, "From: " + remoteServerName);
            ((BaseCheckpoint) (checkpoint)).setState(500);
        }
        if(((BaseCheckpoint) (checkpoint)).isIncomplete(600))
            ((BaseCheckpoint) (checkpoint)).setState(600);
        ((BaseCheckpoint) (checkpoint)).setState(0xf423f);
        super.server.logMessage(remoteServerName, "ServerInstallerUpdateComplete", super.none, super.none, "From: " + remoteServerName);
    }
}
