// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AppVault.java

package com.sitraka.deploy.sam;

import com.sitraka.deploy.AuthContext;
import com.sitraka.deploy.AuthContextAware;
import com.sitraka.deploy.AuthGroups;
import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.Authorization;
import com.sitraka.deploy.GroupAuthorization;
import com.sitraka.deploy.admin.auth.AdminAuthentication;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.app.AccessControl;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.platforms.PlatformJRE;
import com.sitraka.deploy.common.platforms.Platforms;
import com.sitraka.deploy.sam.servlet.Servlet;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Properties;

// Referenced classes of package com.sitraka.deploy.sam:
//            FileAppVault, StandAloneAppVault, AdminAccessControl, SamAuthContext

public abstract class AppVault
    implements Requests
{

    protected String vault_name;
    protected Hashtable authObjects;
    protected static AccessControl adminAccessControl = new AdminAccessControl();
    protected static AuthContext authContext = null;

    public AppVault()
    {
        authObjects = new Hashtable();
    }

    public static final AppVault getInstance(String instance_name)
    {
        AppVault app = null;
        if(instance_name.equalsIgnoreCase("file"))
            app = ((AppVault) (new FileAppVault()));
        if(instance_name.equalsIgnoreCase("standalone"))
            app = ((AppVault) (new StandAloneAppVault()));
        else
            app = ((AppVault) (new FileAppVault()));
        return app;
    }

    public abstract void setLocation(String s)
        throws FileNotFoundException;

    public abstract boolean isLocationSet();

    public abstract Application getApplication(String s);

    public abstract boolean addApplication(String s, String s1, String s2)
        throws FileNotFoundException;

    public abstract boolean addApplication(Application application);

    public void releaseApplication(String s)
    {
    }

    public void releaseApplication(Application app)
    {
        releaseApplication(app.getName());
    }

    public abstract boolean deleteApplication(String s);

    public abstract int countApplications();

    public abstract boolean deleteVersion(String s, String s1);

    public abstract boolean deleteJRE(String s, String s1, String s2);

    public abstract String[] listApplications();

    public abstract String[] listBundles();

    public String getVaultType()
    {
        return vault_name;
    }

    public abstract boolean createPlatformXMLFile(File file, boolean flag);

    public abstract boolean createApplicationsFile(File file, boolean flag);

    public abstract boolean createApplicationVersionsFile(Application application, File file, boolean flag);

    public abstract File getServerTempDir();

    public abstract File getServerCheckpointDir();

    public abstract File getServerEtcDir();

    public abstract File getVaultBaseDir();

    public abstract File getApplicationBaseDir(String s);

    public abstract File getVersionBaseDir(String s, String s1);

    public abstract String getLatestVersion(String s);

    public abstract String getLatestAuthorizedVersion(String s, String s1);

    public abstract File getPlatformXML();

    public abstract Platforms getPlatforms();

    public abstract File getVersionXML(String s, String s1);

    public abstract File getLicenseFile(String s, String s1);

    public abstract File getApplicationFile(String s, String s1, String s2, String s3);

    public abstract File getAbsoluteFile(String s);

    public abstract void setServers(Properties properties);

    public abstract String listServers();

    public abstract void setClusterServers(Properties properties);

    public abstract String listClusterServers();

    public abstract void setOtherServers(Properties properties);

    public abstract String listOtherServers();

    public abstract File getInstaller(int i);

    public abstract File getInstaller(String s);

    public abstract File getLauncher(int i);

    public abstract File getLauncher(String s);

    public abstract File getJREZip(String s, String s1, String s2);

    public abstract File prebuildJRE(String s, String s1, String s2);

    public abstract File getJREBaseDirectory(String s, String s1, String s2, boolean flag);

    public abstract PlatformJRE getPlatformJRE(String s, String s1, String s2);

    public Authentication getAuthenticationObject(String app_name, String version)
    {
        AccessControl access_control = getAccessControl(app_name, version);
        if(access_control == null)
            throw new IllegalArgumentException("Internal Error, AccessControl object for " + app_name + " " + version + " is null");
        Object auth_obj = getAuthObject(access_control.getAuthentication());
        if(auth_obj == null)
            throw new IllegalArgumentException("Authentication class for " + app_name + ", version " + version + " is null");
        if(!(auth_obj instanceof Authentication))
            throw new IllegalArgumentException("Authentication class for " + app_name + " " + version + " does not implement the com.sitraka.deploy.Authentication" + " interface\nThe invalid class name was '" + access_control.getAuthentication() + "'");
        if(access_control instanceof AdminAccessControl)
            auth_obj = ((Object) (new AdminAuthentication((Authentication)auth_obj)));
        if(auth_obj instanceof AuthContextAware)
        {
            if(authContext == null)
                authContext = ((AuthContext) (new SamAuthContext(Servlet.getUniqueInstance())));
            ((AuthContextAware)auth_obj).setAuthContext(authContext);
        }
        return (Authentication)auth_obj;
    }

    public String getAuthenticationFileName(String app_name, String version)
    {
        AccessControl access_control = getAccessControl(app_name, version);
        if(access_control == null)
            return null;
        else
            return access_control.getAuthenticationFileName();
    }

    public Authorization getAuthorizationObject(String app_name, String version)
    {
        AccessControl access_control = getAccessControl(app_name, version);
        if(access_control == null)
        {
            String msg = "Internal Error, AccessControl object for " + app_name + " " + version + " is null";
            throw new IllegalArgumentException(msg);
        }
        Object tmp_auth = getAuthObject(access_control.getAuthorization());
        if(tmp_auth == null)
        {
            String msg = "Authorization class for " + app_name + ", version " + version + " is null";
            throw new IllegalArgumentException(msg);
        }
        if(!(tmp_auth instanceof Authorization))
        {
            String msg = "Authorization class for " + app_name + " " + version + " does not implement the com.sitraka.deploy.Authorization" + " interface\nThe invalid class name was '" + access_control.getAuthorization() + "'";
            throw new IllegalArgumentException(msg);
        }
        if(tmp_auth instanceof GroupAuthorization)
        {
            Object tmp_group = getAuthObject(access_control.getAuthGroups());
            if(tmp_group == null)
            {
                String msg = "AuthGroups class for " + app_name + ", version " + version + " is null";
                throw new IllegalArgumentException(msg);
            }
            if(!(tmp_group instanceof AuthGroups))
            {
                String msg = "AuthGroups class for " + app_name + " " + version + " does not implement the com.sitraka.deploy.AuthGroups" + " interface\nThe invalid class name was '" + access_control.getAuthGroups() + "'";
                throw new IllegalArgumentException(msg);
            }
            ((GroupAuthorization)tmp_auth).setAuthGroups((AuthGroups)tmp_group);
        }
        if(tmp_auth instanceof AuthContextAware)
        {
            if(authContext == null)
                authContext = ((AuthContext) (new SamAuthContext(Servlet.getUniqueInstance())));
            ((AuthContextAware)tmp_auth).setAuthContext(authContext);
        }
        return (Authorization)tmp_auth;
    }

    private AccessControl getAccessControl(String app_name, String version)
    {
        AccessControl accessControl;
        if(app_name.equalsIgnoreCase("DDAdmin") || app_name.equalsIgnoreCase("DDSDK"))
        {
            String authentication = Servlet.properties.getProperty("deploy.admin.authentication", "com.sitraka.deploy.admin.auth.AdminAuthentication");
            String authorization = Servlet.properties.getProperty("deploy.admin.authorization", "com.sitraka.deploy.admin.auth.AdminAuthorization");
            String clientAuthentication = Servlet.properties.getProperty("deploy.admin.clientauthentication", "com.sitraka.deploy.authentication.ClientUsernamePassword");
            String authGroups = Servlet.properties.getProperty("deploy.admin.authgroups", "com.sitraka.deploy.authentication.SimpleAuthGroups");
            if(!authorization.equals("com.sitraka.deploy.admin.auth.AdminAuthorization") && !authorization.equals("com.sitraka.deploy.authorization.AuthorizeAll"))
                authorization = "com.sitraka.deploy.admin.auth.AdminAuthorization";
            if(!clientAuthentication.equals("com.sitraka.deploy.authentication.ClientUsernamePassword") && !clientAuthentication.equals("com.sitraka.deploy.authentication.ClientAuthenticateAll"))
                clientAuthentication = "com.sitraka.deploy.authentication.ClientUsernamePassword";
            adminAccessControl.setAuthentication(authentication);
            adminAccessControl.setAuthorization(authorization);
            adminAccessControl.setClientAuthentication(clientAuthentication);
            adminAccessControl.setAuthGroups(authGroups);
            accessControl = adminAccessControl;
        } else
        {
            Application app = getApplication(app_name);
            if(app == null)
                throw new IllegalArgumentException("Unable to access access control data for bundle " + app_name);
            Version v = app.getVersion(version);
            if(v == null)
                throw new IllegalArgumentException("Unable to access access control data for  version " + version + " of " + app_name);
            accessControl = v.getAccessPolicy();
        }
        return accessControl;
    }

    public String getAuthorizationFileName(String app_name, String version)
    {
        AccessControl a = getAccessControl(app_name, version);
        if(a == null)
            return null;
        else
            return a.getAuthorizationFileName();
    }

    public String getAuthGroupsFileName(String app_name, String version)
    {
        AccessControl a = getAccessControl(app_name, version);
        if(a == null)
            return null;
        else
            return a.getAuthGroupsFileName();
    }

    private Object getAuthObject(String class_name)
    {
        if(class_name == null || class_name.length() == 0)
            return ((Object) (null));
        Object auth_object = authObjects.get(((Object) (class_name)));
        if(auth_object == null)
        {
            Class c = null;
            try
            {
                c = Class.forName(class_name);
            }
            catch(ClassNotFoundException xnfe)
            {
                return ((Object) (null));
            }
            try
            {
                auth_object = c.newInstance();
            }
            catch(IllegalAccessException iae)
            {
                return ((Object) (null));
            }
            catch(InstantiationException ie)
            {
                return ((Object) (null));
            }
            if(auth_object != null)
                authObjects.put(((Object) (class_name)), auth_object);
        }
        return auth_object;
    }

}
