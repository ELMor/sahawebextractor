// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SamAuthContext.java

package com.sitraka.deploy.sam;

import com.sitraka.deploy.AuthGroups;
import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.Authorization;
import com.sitraka.deploy.NonPublicAuthContext;
import com.sitraka.deploy.sam.servlet.Servlet;
import com.sitraka.deploy.util.PropertyUtils;
import java.util.List;
import java.util.Properties;

public class SamAuthContext
    implements NonPublicAuthContext
{

    protected Servlet servlet;

    public SamAuthContext()
    {
        servlet = Servlet.getUniqueInstance();
    }

    public SamAuthContext(Servlet servlet)
    {
        this.servlet = servlet;
    }

    public List getBundles()
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public List getBundlesForAuthorization(String className)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public List getBundlesForAuthentication(String className)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public List getVersions(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public List getUsers(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public AuthGroups getGroups(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public String getAuthorizationClassName(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public String getAuthenticationClassName(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public String getLatestVersion(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public String getProperty(String key)
    {
        return getProperty(key, ((String) (null)));
    }

    public String getProperty(String key, String defaultValue)
    {
        return PropertyUtils.readDefaultString(getProperties(), key, defaultValue);
    }

    public Properties getProperties()
    {
        SamAuthContext _tmp = this;
        return Servlet.properties;
    }

    public Authorization getAuthorizationObject(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public Authentication getAuthenticationObject(String bundle)
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }

    public Object getCurrentUser()
    {
        throw new UnsupportedOperationException("No server-side implementation");
    }
}
