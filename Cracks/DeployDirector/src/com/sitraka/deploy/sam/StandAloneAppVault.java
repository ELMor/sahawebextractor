/* StandAloneAppVault - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.sitraka.deploy.sam;
import java.applet.Applet;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.klg.jclass.util.JCStringTokenizer;
import com.klg.jclass.util.swing.JCGridLayout;
import com.sitraka.deploy.common.Requests;
import com.sitraka.deploy.common.app.Application;
import com.sitraka.deploy.common.app.Version;
import com.sitraka.deploy.common.awt.ProgressBar;
import com.sitraka.deploy.common.platforms.Platforms;
import com.sitraka.deploy.util.FileUtils;

import org.xml.sax.SAXException;

public class StandAloneAppVault extends FileAppVault implements Requests
{
    protected File server_directory = null;
    protected File bundle_directory = null;
    protected Vector app_list = new Vector();
    protected Applet applet = null;

    public class IsUnDARDone
    {
        public synchronized void doTheWait() {
            try {
                this.wait();
            } catch (InterruptedException interruptedexception) {
                /* empty */
            }
        }

        public synchronized void stopTheWait() {
            this.notify();
        }
    }

    public class unZipDar extends Thread
    {
        private IsUnDARDone s = null;
        private File DAR_file = null;
        private File unpackDir = null;
        private String title = null;
        private ProgressBar meter = null;
        private Dialog p = null;

        public unZipDar(IsUnDARDone s, File DAR_file, File unpackDir,
                        String title) {
            this.s = s;
            this.DAR_file = DAR_file;
            this.unpackDir = unpackDir;
            this.title = title;
        }

        public void run() {
            meter = new ProgressBar();
            p = getProgressDialog(meter);
            p.setTitle(title);
            p.setVisible(true);
            FileUtils.unzipFile(DAR_file, unpackDir, meter);
            p.setVisible(false);
            s.stopTheWait();
        }

        public void setVisible(boolean b) {
            p.setVisible(b);
        }

        private Dialog getProgressDialog(ProgressBar meter) {
            Dialog d = new Dialog(new Frame(), false);
            meter.setAutoLabel(false);
            meter.setBarColor(Color.blue);
            meter.setBarCount(0);
            meter.setLabelPosition(2);
            meter.setLabelWidth(30);
            d.setLayout(new JCGridLayout(1, 1));
            d.add(meter);
            d.pack();
            d.setResizable(false);
            Dimension size = d.getSize();
            Dimension screen_size = d.getToolkit().getScreenSize();
            int left = screen_size.width / 2 - size.width / 2;
            int top = screen_size.height / 2 - size.height / 2;
            d.setLocation(left, top);
            d.setSize(200, 100);
            return d;
        }
    }

    public class SuffixFileFilter implements FileFilter
    {
        protected String suffix;

        public SuffixFileFilter() {
            suffix = null;
        }

        public SuffixFileFilter(String suffix) {
            this.suffix = suffix;
        }

        public boolean accept(File testfile) {
            if (testfile == null)
                return false;
            if (suffix == null)
                return true;
            String filename = testfile.getName();
            if (filename == null || filename.length() < suffix.length())
                return false;
            String terminal
                = filename.substring(filename.length() - suffix.length());
            return suffix.equalsIgnoreCase(terminal);
        }
    }

    public class AppVersion
    {
        protected String version = null;
        protected File darFile = null;

        public AppVersion() {
            /* empty */
        }

        public AppVersion(String version, File darFile) {
            this.version = version;
            this.darFile = darFile;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String label) {
            version = label;
        }

        public File getDarFile() {
            return darFile;
        }

        public void setDarFile(File dar) {
            darFile = dar;
        }
    }

    public class AppListEntry
    {
        protected String applicationName;
        protected String applicationID = null;
        protected String creationDate = null;
        protected Vector versions = null;
        protected boolean isAppSource = true;

        public AppListEntry() {
            applicationName = null;
        }

        public AppListEntry(String appName) {
            applicationName = appName;
        }

        public AppListEntry(String appName, String id, String date) {
            applicationName = appName;
            applicationID = id;
            creationDate = date;
            isAppSource = true;
        }

        public AppListEntry(String appName, String version, File darFile) {
            applicationName = appName;
            isAppSource = false;
            versions = new Vector();
            versions.addElement(new AppVersion(version, darFile));
        }

        public void setName(String name) {
            applicationName = name;
        }

        public String getName() {
            return applicationName;
        }

        public void setApplicationID(String id) {
            applicationID = id;
        }

        public String getApplicationID() {
            return applicationID;
        }

        public void setCreationDate(String date) {
            creationDate = date;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setAppSource(boolean isDir) {
            isAppSource = isDir;
        }

        public boolean isAppSource() {
            return isAppSource;
        }

        public void addVersion(AppVersion version) {
            if (versions == null)
                versions = new Vector();
            if (version != null)
                versions.addElement(version);
        }

        public void addVersion(String version, File darFile) {
            if (versions == null)
                versions = new Vector();
            if (version != null && version.length() != 0 && darFile != null
                && darFile.exists())
                versions.addElement(new AppVersion(version, darFile));
        }

        public Vector getVersions() {
            return versions;
        }

        public AppVersion getVersion(String version) {
            if (versions == null || versions.size() == 0)
                return null;
            if (version == null || version.length() == 0)
                return (AppVersion) versions.elementAt(0);
            for (int i = 0; i < versions.size(); i++) {
                AppVersion av = (AppVersion) versions.elementAt(i);
                String name = av.getVersion();
                if (version.equalsIgnoreCase(name))
                    return av;
            }
            return null;
        }
    }

    public StandAloneAppVault() {
        vault_name = "standalone";
        server_directory
            = FileUtils.createTempFile((String) null, "dd", "vault");
        server_directory.deleteOnExit();
        server_directory.mkdir();
    }

    public void setLocation(String location) throws FileNotFoundException {
        if (location == null || location.equals(""))
            throw new FileNotFoundException
                      ("provided base directory name is null or empty");
        basedir = new File(location);
        if (!basedir.exists() || !basedir.isDirectory())
            throw new FileNotFoundException
                      ("provided base directory does not exist or is not a directory");
        app_directory = location + File.separator + "vault";
        bundle_directory = new File(server_directory, "vault");
        bundle_directory.mkdir();
        String app_file_name = basedir.getAbsolutePath();
        File app_file = new File(app_directory, "bundles.lst");
        if (app_file.exists() && app_file.canRead()
            && app_file.length() > 0L) {
            BufferedReader parser
                = new BufferedReader(new FileReader(app_file));
            String current_app = null;
            try {
                while (parser.ready()) {
                    String line = parser.readLine();
                    if (line != null && !line.startsWith("#")
                        && !line.equals("")) {
                        JCStringTokenizer tokenizer
                            = new JCStringTokenizer(line);
                        current_app = null;
                        String appID = null;
                        String appCreationDate = null;
                        try {
                            current_app = tokenizer.nextToken(',');
                            appID = tokenizer.nextToken(',');
                            appCreationDate = tokenizer.nextToken(',');
                        } catch (NoSuchElementException nosuchelementexception) {
                            /* empty */
                        }
                        if (current_app != null && !current_app.equals("")) {
                            File testApp
                                = new File(app_directory, current_app);
                            if (testApp.exists() && testApp.isDirectory())
                                addToAppList(current_app, appID,
                                             appCreationDate);
                        }
                    }
                }
            } catch (IOException e) {
                throw new FileNotFoundException(e.getMessage()
                                                + " while loading '"
                                                + "bundles.lst" + "'");
            }
        } else {
            File dar_loc = new File(app_directory);
            File[] vaultEntries
                = dar_loc.listFiles(new SuffixFileFilter(".dar"));
            int i = 0;
            for (/**/; i < vaultEntries.length; i++) {
                ZipFile darZip = null;
                try {
                    darZip = new ZipFile(vaultEntries[i]);
                } catch (IOException ioe) {
                    continue;
                }
                String bundleFN = "META-INF/bundlename.txt";
                ZipEntry bundleZE = darZip.getEntry(bundleFN);
                if (bundleZE == null) {
                    bundleFN = "META-INF/bundlename.txt";
                    bundleZE = darZip.getEntry(bundleFN);
                }
                InputStream bundleIS = null;
                try {
                    bundleIS = darZip.getInputStream(bundleZE);
                } catch (IOException ioe) {
                    continue;
                }
                BufferedReader bundleBR
                    = new BufferedReader(new InputStreamReader(bundleIS));
                String appName = null;
                try {
                    appName = bundleBR.readLine();
                } catch (IOException ioe) {
                    continue;
                }
                if (appName != null) {
                    String versionFN = "META-INF/version.xml";
                    ZipEntry versionZE = darZip.getEntry(versionFN);
                    if (versionZE == null) {
                        versionFN = "META-INF/version.xml";
                        versionZE = darZip.getEntry(versionFN);
                    }
                    InputStream versionIS = null;
                    try {
                        versionIS = darZip.getInputStream(versionZE);
                    } catch (IOException ioe) {
                        continue;
                    }
                    String version = null;
                    boolean done = false;
                    BufferedReader versionBR
                        = new BufferedReader(new InputStreamReader(versionIS));
                    while (!done) {
                        try {
                            String line = versionBR.readLine();
                            if (line != null && line.startsWith("<VERSION ")) {
                                version = line.substring(line.indexOf("NAME=")
                                                         + 6);
                                version
                                    = version.substring(0,
                                                        version.indexOf("\""));
                                if (version != null && version.length() > 0)
                                    done = true;
                            }
                        } catch (IOException ioe) {
                            break;
                        }
                    }
                    addToAppList(appName, version, vaultEntries[i]);
                }
            }
        }
        String platform_file_name
            = basedir.getAbsolutePath() + File.separator + "platform.xml";
        File platform_file = new File(platform_file_name);
        if (!platform_file.exists() || !platform_file.canRead())
            throw new FileNotFoundException
                      ("Platform file \"" + platform_file_name
                       + "\" does not exist or cannot be read");
        try {
            platforms = new Platforms(platform_file);
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileNotFoundException("Error accessing file: "
                                            + e.getMessage());
        } catch (SAXException saxy) {
            saxy.printStackTrace();
            throw new FileNotFoundException("Error parsing file: "
                                            + saxy.getMessage());
        }
    }

    public boolean addApplication
        (String application_name, String app_id, String creation_date)
        throws FileNotFoundException {
        AppListEntry ale = findInAppList(application_name);
        if (ale == null)
            return false;
        AppVersion av = ale.getVersion("");
        String version_name = av.getVersion();
        File DAR_file = av.getDarFile();
        File unpackDir = bundle_directory;
        boolean moveXML = true;
        ZipFile darZip = null;
        try {
            darZip = new ZipFile(DAR_file);
        } catch (IOException ioe) {
            return false;
        }
        ZipEntry ze = darZip.getEntry(application_name.toLowerCase() + "/");
        if (ze == null) {
            unpackDir = new File(unpackDir, application_name.toLowerCase());
            ze = darZip.getEntry(version_name.toLowerCase() + "/");
            if (ze == null) {
                unpackDir = new File(unpackDir, version_name.toLowerCase());
                moveXML = false;
            }
        }
        unpackDir.mkdirs();
        IsUnDARDone d = new IsUnDARDone();
        unZipDar uzd = new unZipDar(d, DAR_file, unpackDir,
                                    application_name + " " + version_name);
        uzd.start();
        d.doTheWait();
        if (moveXML) {
            File meta_source = new File(unpackDir, "META-INF");
            String destination
                = (application_name.toLowerCase() + File.separator
                   + version_name.toLowerCase() + File.separator + "META-INF");
            File meta_dest = new File(bundle_directory, destination);
            FileUtils.copyDirectory(meta_source, meta_dest);
        }
        File appDir
            = new File(bundle_directory, application_name.toLowerCase());
        if (!appDir.exists() || !appDir.isDirectory())
            return false;
        File version_file = new File(appDir, "versions.lst");
        try {
            FileOutputStream fos = new FileOutputStream(version_file);
            fos.write(version_name.toLowerCase().getBytes());
            fos.close();
        } catch (IOException ioe) {
            return false;
        }
        try {
            Application app = new Application(application_name, appDir);
            app.setID(app_id);
            app.setCreationDate(creation_date);
            return this.addApplication(app);
        } catch (SAXException e) {
            throw new FileNotFoundException(e.getMessage());
        } catch (IOException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }

    public Application getApplication(String name) {
        Application app = null;
        if (name == null || name.equals(""))
            return null;
        for (int i = 0; i < all_applications.size(); i++) {
            Application this_app = (Application) all_applications.elementAt(i);
            if (this_app.getName().equalsIgnoreCase(name))
                return this_app;
        }
        for (int i = 0; i < app_list.size(); i++) {
            AppListEntry app_le = (AppListEntry) app_list.elementAt(i);
            if (app_le.getName().equalsIgnoreCase(name)) {
                try {
                    addApplication(app_le.getName(), app_le.getApplicationID(),
                                   app_le.getCreationDate());
                } catch (FileNotFoundException fnf) {
                    return null;
                }
                app = (Application) all_applications.lastElement();
            }
        }
        return app;
    }

    public void releaseApplication(String name) {
        if (name != null && name.length() != 0) {
            AppListEntry ale = findInAppList(name);
            if (ale != null && !ale.isAppSource()) {
                File app_base = new File(bundle_directory, name.toLowerCase());
                if (app_base.exists())
                    FileUtils.deleteDirectory(app_base);
            }
        }
    }

    public boolean deleteApplication(String appname) {
        return false;
    }

    public int countApplications() {
        int size = app_list == null ? 0 : app_list.size();
        int num_apps = size;
        for (int i = 0; i < size; i++) {
            AppListEntry apple = (AppListEntry) app_list.elementAt(i);
            String name = apple.getName();
            if (name.equalsIgnoreCase("DDCAM")
                || name.equalsIgnoreCase("DDSDK")
                || name.equalsIgnoreCase("DDAdmin"))
                num_apps--;
        }
        return num_apps;
    }

    public boolean deleteVersion(String appname, String vername) {
        return false;
    }

    public boolean deleteJRE(String vendor, String version, String platform) {
        return false;
    }

    public boolean createPlatformXMLFile(File outputFile,
                                         boolean createBackup) {
        return true;
    }

    public boolean createApplicationsFile(File outputFile,
                                          boolean createBackup) {
        return true;
    }

    public boolean createApplicationVersionsFile
        (Application app, File outputFile, boolean createBackup) {
        return true;
    }

    public File getServerTempDir() {
        File f = new File(server_directory, "temp");
        f.mkdirs();
        return f;
    }

    public File getServerCheckpointDir() {
        File f = new File(server_directory, "checkpt");
        f.mkdirs();
        return f;
    }

    public File getServerEtcDir() {
        File f = new File(getVaultBaseDir(), "etc");
        f.mkdirs();
        return f;
    }

    public File getVaultBaseDir() {
        return basedir;
    }

    public File getApplicationBaseDir(String app_name) {
        Application app = getApplication(app_name);
        if (app == null)
            return null;
        AppListEntry ale = findInAppList(app_name);
        if (ale == null)
            return null;
        File app_base = null;
        if (ale.isAppSource())
            app_base = new File(app_directory, app.getName().toLowerCase());
        else
            app_base = new File(bundle_directory, app.getName().toLowerCase());
        if (!app_base.canRead())
            return null;
        return app_base;
    }

    public File getVersionBaseDir(String app_name, String version_name) {
        Application app = getApplication(app_name);
        if (app == null)
            return null;
        Version ver = app.getVersion(version_name);
        if (ver == null)
            return null;
        File appbase = getApplicationBaseDir(app_name);
        File req_file = new File(appbase, ver.getName().toLowerCase());
        if (!req_file.canRead())
            return null;
        return req_file;
    }

    public String[] listApplications() {
        AppListEntry app_le = null;
        String[] app_names = new String[app_list.size()];
        for (int i = 0; i < app_list.size(); i++) {
            app_le = (AppListEntry) app_list.elementAt(i);
            app_names[i] = (app_le.getName() + "," + app_le.getApplicationID()
                            + "," + app_le.getCreationDate());
        }
        return app_names;
    }

    public String[] listBundles() {
        String[] bundles = new String[app_list.size()];
        for (int i = 0; i < app_list.size(); i++) {
            AppListEntry app_le = (AppListEntry) app_list.elementAt(i);
            bundles[i] = app_le.getName();
        }
        return bundles;
    }

    public File getVersionXML(String app_name, String version) {
        if (app_name == null || version == null)
            return null;
        AppListEntry ale = findInAppList(app_name);
        File appDir = null;
        if (ale == null)
            appDir = new File(app_directory, app_name.toLowerCase());
        else if (ale.isAppSource())
            appDir = new File(app_directory, app_name.toLowerCase());
        else
            appDir = new File(bundle_directory, app_name.toLowerCase());
        if (!appDir.exists()) {
            Application app = getApplication(app_name);
            if (app == null)
                return null;
        }
        File versionDir = new File(appDir, version.toLowerCase());
        File xml_file = new File(versionDir, "version.xml");
        if (!xml_file.exists()) {
            File meta_dir = new File(versionDir, "META-INF");
            if (meta_dir.exists())
                xml_file = new File(meta_dir, "version.xml");
        }
        return xml_file;
    }

    public String getLatestVersion(String app_name) {
        Application app = getApplication(app_name);
        if (app == null)
            return null;
        Vector versions = app.listVersions();
        if (versions == null)
            return null;
        Version ver = (Version) versions.elementAt(0);
        if (ver == null)
            return null;
        return ver.getName();
    }

    public File getLicenseFile(String app_name, String version) {
        AppListEntry ale = findInAppList(app_name);
        File app_base = null;
        if (ale == null)
            app_base = new File(app_directory, app_name.toLowerCase());
        else if (ale.isAppSource())
            app_base = new File(app_directory, app_name.toLowerCase());
        else
            app_base = new File(bundle_directory, app_name.toLowerCase());
        Application app = getApplication(app_name);
        if (app == null)
            return null;
        Version ver = app.getVersion(version);
        if (ver == null)
            return null;
        File versionDir = new File(app_base, ver.getName().toLowerCase());
        File req_file = new File(versionDir, ver.getLicenseFilename());
        if (!req_file.exists()) {
            File meta_dir = new File(versionDir, "META-INF");
            if (meta_dir.exists())
                req_file = new File(meta_dir, ver.getLicenseFilename());
        }
        if (!req_file.exists() || !req_file.canRead())
            return null;
        return req_file;
    }

    public File getApplicationFile(String app_name, String version,
                                   String filename, String platform) {
        Application app = getApplication(app_name);
        if (app == null)
            return null;
        Version ver = app.getVersion(version);
        if (ver == null)
            return null;
        String file = ver.getPathToFile(filename, platform);
        if (file == null)
            return null;
        AppListEntry ale = findInAppList(app_name);
        File app_base = null;
        if (ale == null)
            app_base = new File(app_directory, app_name.toLowerCase());
        else if (ale.isAppSource())
            app_base = new File(app_directory, app_name.toLowerCase());
        else
            app_base = new File(bundle_directory, app_name.toLowerCase());
        File req_file = new File(app_base, file);
        if (!req_file.exists() || !req_file.canRead())
            return null;
        return req_file;
    }

    public File getAbsoluteFile(String fileName) {
        if (fileName.indexOf("vault") == -1)
            return new File(basedir, fileName);
        String temp = fileName.substring(fileName.indexOf("vault")
                                         + "vault".length() + 1);
        if (temp.indexOf(File.separator) > 0)
            temp = temp.substring(0, temp.indexOf(File.separator));
        else
            return new File(basedir, fileName);
        File tempFile = new File(basedir, temp);
        if (tempFile.exists())
            return new File(basedir, fileName);
        return new File(server_directory, fileName);
    }

    protected void addToAppList(String name, String id, String date) {
        AppListEntry ale = findInAppList(name);
        if (ale == null) {
            ale = new AppListEntry(name);
            ale.setApplicationID(id);
            ale.setCreationDate(date);
            ale.setAppSource(true);
            app_list.addElement(ale);
        }
    }

    protected void addToAppList(String name, String version, File dar) {
        AppListEntry ale = findInAppList(name);
        if (ale == null) {
            ale = new AppListEntry(name);
            ale.setAppSource(false);
            ale.addVersion(new AppVersion(version, dar));
            app_list.addElement(ale);
        } else {
            AppVersion av = ale.getVersion(version);
            if (!ale.isAppSource() && av == null) {
                av = new AppVersion(version, dar);
                ale.addVersion(av);
            }
        }
    }

    protected AppListEntry findInAppList(String appName) {
        if (app_list == null)
            return null;
        for (int i = 0; i < app_list.size(); i++) {
            AppListEntry ale = (AppListEntry) app_list.elementAt(i);
            String name = ale.getName();
            if (name.equalsIgnoreCase(appName))
                return ale;
        }
        return null;
    }

    public boolean isCAMPresent() {
        if (findInAppList("DDCAM") == null)
            return false;
        return true;
    }

    public void finalize() throws Throwable {
        FileUtils.deleteDirectory(server_directory);
        FileUtils.deleteDirectory(bundle_directory);
        super.finalize();
    }

    public void setApplet(Applet a) {
        applet = a;
    }
}
