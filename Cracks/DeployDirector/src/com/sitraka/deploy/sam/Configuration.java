// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Configuration.java

package com.sitraka.deploy.sam;

import HTTPClient.URI;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

public class Configuration
{

    public static final String NO_HOST_STRING = "Both cluster and server host lists (deploy.[cluster|server].host.*)\nare missing and no default could be determined";
    protected static final String VAULTDIR = "$(VAULTDIR)";
    protected String base;
    protected String cluster_file;
    protected Properties cluster_props;
    protected String server_file;
    protected Properties server_props;
    protected Properties merged_props;
    protected static final String mergers[] = new String[0];

    public static Properties loadConfiguration(String base_directory)
        throws FileNotFoundException
    {
        Configuration c = new Configuration(base_directory);
        Properties p = c.getMergedProperties();
        ((Hashtable) (System.getProperties())).put("http.agent", "DeploySam/2.5.0");
        if(PropertyUtils.containsPropertyValue(p, "deploy.http.sslsocketfactory", ((String) (null))))
        {
            String factory = PropertyUtils.readDefaultString(p, "deploy.http.sslsocketfactory", ((String) (null)));
            if(factory != null)
                ((Hashtable) (System.getProperties())).put("deploy.http.sslsocketfactory", ((Object) (factory)));
        }
        if(PropertyUtils.containsPropertyValue(p, "deploy.http.timeout", ((String) (null))))
        {
            String timeout = PropertyUtils.readDefaultString(p, "deploy.http.timeout", ((String) (null)));
            if(timeout != null)
                ((Hashtable) (System.getProperties())).put("deploy.http.timeout", ((Object) (timeout)));
        }
        return p;
    }

    public static Properties loadConfiguration(Properties clusterProps, Properties serverProps)
    {
        Configuration c = new Configuration(clusterProps, serverProps);
        try
        {
            Properties p = c.getMergedProperties();
            return p;
        }
        catch(FileNotFoundException e)
        {
            return null;
        }
    }

    protected Properties getMergedProperties()
        throws FileNotFoundException
    {
        if(merged_props == null)
            loadAndMerge();
        if(merged_props == null)
            throw new FileNotFoundException("Unable to load required server configuration files");
        else
            return merged_props;
    }

    protected Configuration(Properties clusterProps, Properties serverProps)
    {
        base = null;
        cluster_file = null;
        cluster_props = null;
        server_file = null;
        server_props = null;
        merged_props = null;
        cluster_props = clusterProps;
        server_props = serverProps;
        try
        {
            mergePropertyFiles();
        }
        catch(FileNotFoundException e) { }
    }

    protected Configuration(String base)
        throws FileNotFoundException
    {
        this.base = null;
        cluster_file = null;
        cluster_props = null;
        server_file = null;
        server_props = null;
        merged_props = null;
        this.base = base;
        cluster_file = base + File.separator + "cluster.properties";
        server_file = base + File.separator + "server.properties";
        File cluster = new File(cluster_file);
        File server = new File(server_file);
        if(!cluster.canRead())
            throw new FileNotFoundException("Cannot access " + cluster.getAbsolutePath());
        if(!server.canRead())
            throw new FileNotFoundException("Cannot access " + server.getAbsolutePath());
        else
            return;
    }

    protected void loadAndMerge()
        throws FileNotFoundException
    {
        if(!readPropertyFiles())
        {
            throw new FileNotFoundException("Unable to read required configuration files");
        } else
        {
            mergePropertyFiles();
            return;
        }
    }

    protected boolean readPropertyFiles()
        throws FileNotFoundException
    {
        File cluster = new File(cluster_file);
        File server = new File(server_file);
        try
        {
            FileInputStream fis = new FileInputStream(cluster);
            cluster_props = new Properties();
            cluster_props.load(((java.io.InputStream) (fis)));
            fis.close();
            fis = null;
            fis = new FileInputStream(server);
            server_props = new Properties();
            server_props.load(((java.io.InputStream) (fis)));
            fis.close();
            fis = null;
            return true;
        }
        catch(IOException ioe)
        {
            merged_props = null;
            cluster_props = null;
            server_props = null;
            throw new FileNotFoundException("IOException reading property files: " + ((Throwable) (ioe)).getMessage());
        }
    }

    protected void mergePropertyFiles()
        throws FileNotFoundException
    {
        if(cluster_props == null || server_props == null)
        {
            merged_props = null;
            throw new FileNotFoundException("Cluster or Server properties were not loaded correctly");
        }
        merged_props = new Properties();
        Hashtable email = new Hashtable();
        extractEmailProperties(email, cluster_props);
        extractEmailProperties(email, server_props);
        int i = 0;
        for(Enumeration e = email.keys(); e.hasMoreElements();)
        {
            String key = (String)e.nextElement();
            String value = (String)email.get(((Object) (key)));
            ((Hashtable) (merged_props)).put(((Object) ("deploy.error.email." + i + ".address")), ((Object) (key)));
            ((Hashtable) (merged_props)).put(((Object) ("deploy.error.email." + i + ".notification")), ((Object) (value)));
            i++;
        }

        for(Enumeration ce = ((Hashtable) (cluster_props)).keys(); ce.hasMoreElements();)
        {
            String key = (String)ce.nextElement();
            if(key.trim().length() != 0)
                if(((Hashtable) (server_props)).containsKey(((Object) (key))))
                {
                    String result = mergeProperty(key);
                    if(result != null)
                        ((Hashtable) (merged_props)).put(((Object) (key)), ((Object) (result)));
                    ((Hashtable) (server_props)).remove(((Object) (key)));
                } else
                {
                    String value = (String)((Hashtable) (cluster_props)).get(((Object) (key)));
                    value = replacePlaceHolders(value);
                    ((Hashtable) (merged_props)).put(((Object) (key)), ((Object) (value)));
                }
        }

        for(Enumeration se = ((Hashtable) (server_props)).keys(); se.hasMoreElements();)
        {
            String key = (String)se.nextElement();
            if(key.trim().length() != 0)
            {
                String value = (String)((Hashtable) (server_props)).get(((Object) (key)));
                value = replacePlaceHolders(value);
                ((Hashtable) (merged_props)).put(((Object) (key)), ((Object) (value)));
            }
        }

        rewriteHostProperties();
        server_props = null;
        cluster_props = null;
    }

    protected void extractEmailProperties(Hashtable h, Properties local)
    {
        int i = 0;
        while(PropertyUtils.containsProperty(local, "deploy.error.email.", ".address")) 
        {
            String key = "deploy.error.email." + i + ".address";
            String email = (String)((Hashtable) (local)).get(((Object) (key)));
            if(email == null || email.trim().length() == 0)
            {
                removeEmail(local, "deploy.error.email." + i);
                i++;
            } else
            {
                key = "deploy.error.email." + i + ".notification";
                String type = (String)((Hashtable) (local)).get(((Object) (key)));
                if(type != null && type.trim().length() != 0)
                {
                    if(h.containsKey(((Object) (email))))
                    {
                        String old_type = (String)h.get(((Object) (email)));
                        type = old_type + "," + type;
                    }
                    h.put(((Object) (email)), ((Object) (type)));
                }
                removeEmail(local, "deploy.error.email." + i);
                i++;
            }
        }
    }

    protected void removeEmail(Properties props, String base)
    {
        String key = base + ".address";
        ((Hashtable) (props)).remove(((Object) (key)));
        key = base + ".notification";
        ((Hashtable) (props)).remove(((Object) (key)));
    }

    protected String mergeProperty(String key)
    {
        String s_prop = (String)((Hashtable) (server_props)).get(((Object) (key)));
        if(s_prop == null)
            return null;
        s_prop = replacePlaceHolders(s_prop);
        String c_prop = (String)((Hashtable) (cluster_props)).get(((Object) (key)));
        if(c_prop == null)
            return null;
        c_prop = replacePlaceHolders(c_prop);
        if(s_prop.trim().length() == 0)
            return c_prop;
        else
            return s_prop;
    }

    protected void rewriteHostProperties()
    {
        String cluster_base = "deploy.cluster.host";
        String server_base = "deploy.server.host";
        StringBuffer processed_cluster = extractHosts(cluster_props, "deploy.cluster.host");
        StringBuffer processed_server = null;
        if(PropertyUtils.containsPropertyValue(server_props, "deploy.server.host", "machine"))
            processed_server = extractHosts(server_props, "deploy.server.host");
        else
            processed_server = extractHosts(cluster_props, "deploy.server.host");
        if(processed_cluster.length() == 0 && processed_server.length() > 0)
            processed_cluster = processed_server;
        else
        if(processed_server.length() == 0 && processed_cluster.length() > 0)
            processed_server = processed_cluster;
        else
        if(processed_server.length() == 0 && processed_cluster.length() == 0)
        {
            String env = PropertyUtils.readDefaultString(System.getProperties(), "deploy.appserver", ((String) (null)));
            if(env == null || env.toLowerCase().indexOf("tomcat") == -1)
                throw new IllegalArgumentException("Both cluster and server host lists (deploy.[cluster|server].host.*)\nare missing and no default could be determined");
            String hostname = null;
            InetAddress localhost = null;
            try
            {
                localhost = InetAddress.getLocalHost();
            }
            catch(UnknownHostException e)
            {
                throw new IllegalArgumentException("Both cluster and server host lists (deploy.[cluster|server].host.*)\nare missing and no default could be determined");
            }
            Vector licHosts = PropertyUtils.readVector(cluster_props, "hosts", ',');
            int size = licHosts.size();
            for(int i = 0; i < size;)
            {
                String sample = (String)licHosts.elementAt(i);
                try
                {
                    InetAddress sampleAddr = InetAddress.getByName(sample);
                    if(!sampleAddr.equals(((Object) (localhost))))
                        continue;
                    hostname = sample;
                    break;
                }
                catch(UnknownHostException e)
                {
                    i++;
                }
            }

            if(hostname == null)
                hostname = localhost.getHostName();
            String defaultHost = "http://" + hostname + ":8080/servlet/deploy";
            processed_cluster.append(defaultHost);
            processed_server.append(defaultHost);
            ((Hashtable) (merged_props)).put("deploy.config.errormessage", ((Object) ("No value provided for deploy.*.host, using default of '" + defaultHost + "'. Please update the properties files.")));
        }
        ((Hashtable) (merged_props)).put("deploy.cluster.processedhosts", ((Object) (processed_cluster.toString())));
        ((Hashtable) (merged_props)).put("deploy.server.processedhosts", ((Object) (processed_server.toString())));
    }

    protected StringBuffer extractHosts(Properties props, String base)
    {
        int i = 0;
        StringBuffer processedhosts = new StringBuffer();
        boolean firstTime = true;
        while(PropertyUtils.containsProperty(props, base + ".", ((String) (null)))) 
        {
            String key = base + "." + i + ".protocol";
            String protocol = PropertyUtils.readDefaultString(props, key, ((String) (null)));
            if(protocol == null)
            {
                removeHost(props, base + "." + i);
                i++;
            } else
            {
                key = base + "." + i + ".machine";
                String machine = PropertyUtils.readDefaultString(props, key, ((String) (null)));
                if(machine == null)
                {
                    removeHost(props, base + "." + i);
                    i++;
                } else
                {
                    key = base + "." + i + ".port";
                    String default_port = "" + URI.defaultPort(protocol);
                    String port = PropertyUtils.readDefaultString(props, key, ((String) (null)));
                    if(port == null)
                        port = default_port;
                    key = base + "." + i + ".page";
                    String page = PropertyUtils.readDefaultString(props, key, ((String) (null)));
                    if(page == null)
                    {
                        removeHost(props, base + "." + i);
                        i++;
                    } else
                    {
                        removeHost(props, base + "." + i);
                        i++;
                        if(firstTime)
                            firstTime = false;
                        else
                            processedhosts.append(",");
                        processedhosts.append(protocol + "://");
                        processedhosts.append(machine);
                        if(!port.trim().equals(((Object) (default_port))))
                            processedhosts.append(":" + port);
                        processedhosts.append(page);
                    }
                }
            }
        }
        return processedhosts;
    }

    protected void removeHost(Properties props, String base)
    {
        String key = base + ".protocol";
        ((Hashtable) (props)).remove(((Object) (key)));
        key = base + ".machine";
        ((Hashtable) (props)).remove(((Object) (key)));
        key = base + ".port";
        ((Hashtable) (props)).remove(((Object) (key)));
        key = base + ".page";
        ((Hashtable) (props)).remove(((Object) (key)));
    }

    protected String replacePlaceHolders(String input)
    {
        if(input == null)
            return null;
        if(base == null)
            return input;
        String updated = input;
        for(int index = updated.indexOf("$(VAULTDIR)"); index != -1; index = updated.indexOf("$(VAULTDIR)"))
        {
            String left = updated.substring(0, index);
            String right = updated.substring(index + "$(VAULTDIR)".length());
            updated = left + base + right;
        }

        return updated;
    }

}
