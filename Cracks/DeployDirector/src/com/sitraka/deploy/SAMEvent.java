// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SAMEvent.java

package com.sitraka.deploy;

import java.sql.Timestamp;

public class SAMEvent
{

    public static final String CLIENT_CHECKED_STATUS = "ClientCheckedStatus";
    public static final String CLIENT_BUNDLE_UPDATE_STARTED = "ClientBundleUpdateStarted";
    public static final String CLIENT_BUNDLE_RECEIVED = "ClientBundleReceived";
    public static final String CLIENT_BUNDLE_UPDATE_COMPLETE = "ClientBundleUpdateComplete";
    public static final String CLIENT_BUNDLE_DELETED = "ClientBundleDeleted";
    public static final String SERVER_STARTUP_COMPLETE = "ServerStartupComplete";
    public static final String SERVER_SHUTDOWN_COMPLETE = "ServerShutdownComplete";
    public static final String SERVER_RELOAD_START = "ServerReloadStart";
    public static final String SERVER_RELOAD_COMPLETE = "ServerReloadComplete";
    public static final String SERVER_BUNDLE_UPDATE_STARTED = "ServerBundleUpdateStarted";
    public static final String SERVER_VERSION_UPDATE_STARTED = "ServerVersionUpdateStarted";
    public static final String SERVER_DAR_UPLOAD_STARTED = "ServerDARUploadStarted";
    public static final String SERVER_JRE_UPDATE_STARTED = "ServerJREUpdateStarted";
    public static final String SERVER_CONFIG_UPDATE_STARTED = "ServerConfigUpdateStarted";
    public static final String SERVER_CONFIG_RECEIVED = "ServerConfigReceived";
    public static final String SERVER_CONFIG_UPDATE_COMPLETE = "ServerConfigUpdateComplete";
    public static final String SERVER_BUNDLE_RECEIVED = "ServerBundleReceived";
    public static final String SERVER_VERSION_RECEIVED = "ServerVersionReceived";
    public static final String SERVER_DAR_RECEIVED = "ServerDARReceived";
    public static final String SERVER_JRE_RECEIVED = "ServerJREReceived";
    public static final String SERVER_BUNDLE_UPDATE_COMPLETE = "ServerBundleUpdateComplete";
    public static final String SERVER_VERSION_UPDATE_COMPLETE = "ServerVersionUpdateComplete";
    public static final String SERVER_DAR_UPLOAD_COMPLETE = "ServerDARUploadComplete";
    public static final String SERVER_JRE_UPDATE_COMPLETE = "ServerJREUpdateComplete";
    public static final String SERVER_LOG_AGGREGATE_START = "ServerLogAggregateStart";
    public static final String SERVER_LOG_AGGREGATE_COMPLETE = "ServerLogAggregateComplete";
    public static final String SERVER_HTML_UPDATE_STARTED = "ServerHTMLUpdateStarted";
    public static final String SERVER_HTML_RECEIVED = "ServerHTMLReceived";
    public static final String SERVER_HTML_UPDATE_COMPLETE = "ServerHTMLUpdateComplete";
    public static final String SERVER_INSTALLER_UPDATE_STARTED = "ServerInstallerUpdateStarted";
    public static final String SERVER_INSTALLER_RECEIVED = "ServerInstallerReceived";
    public static final String SERVER_INSTALLER_UPDATE_COMPLETE = "ServerInstallerUpdateComplete";
    public static final String SERVER_BUNDLE_DELETED = "ServerBundleDeleted";
    public static final String SERVER_VERSION_DELETED = "ServerVersionDeleted";
    public static final String SERVER_JRE_DELETED = "ServerJREDeleted";
    public static final String SERVER_EMAIL_FAILED = "ServerEmailFailed";
    public static final String CLIENT_CANNOT_MODIFY_REGISTRY = "ClientCannotModifyRegistry";
    public static final String CLIENT_CANNOT_CREATE_SHORTCUT = "ClientCannotCreateShortcut";
    public static final String CLIENT_FILE_IOEXCEPTION = "ClientFileIOException";
    public static final String CLIENT_APP_EXCEPTION = "ClientAppException";
    public static final String CLIENT_REQUEST_FAILED = "ClientRequestFailed";
    public static final String CLIENT_LOGIN_FAILED = "ClientLoginFailed";
    public static final String SERVER_LOGIN_FAILED = "ServerLoginFailed";
    public static final String SERVER_DISK_FULL = "ServerDiskFull";
    public static final String SERVER_CANNOT_MODIFY_FILE = "ServerCannotModifyFile";
    public static final String SERVER_LOG_FAILURE = "ServerLogFailure";
    public static final String SERVER_REQUEST_FAILED = "ServerRequestFailed";
    public static final String SERVER_CONFIGURATION_ERROR = "ServerConfigurationError";
    public static final String SERVER_INTERNAL_ERROR = "ServerInternalError";
    public static final String SERVER_LICENSE_ERROR = "ServerLicenseError";
    public static final String SERVER_EXCEPTION_ERROR = "ServerExceptionError";
    public static final String SERVER_REPLICATION_REJECTED = "ServerReplicationRejected";
    public static final String SERVER_REPLICATION_DELAYED = "ServerReplicationDelayed";
    protected String event;
    protected String serverID;
    protected String remoteID;
    protected String userID;
    protected String bundleName;
    protected String bundleVersion;
    protected Timestamp timestamp;
    protected String notes;
    protected long uptime;
    protected double shortTermLoad;
    protected double midTermLoad;
    protected double longTermLoad;

    public SAMEvent(String event, String server_id, String remote_id, String user_id, String bundle_name, String bundle_version, Timestamp timestamp, 
            String notes)
    {
        this.event = event;
        serverID = server_id;
        remoteID = remote_id;
        userID = user_id;
        bundleName = bundle_name;
        bundleVersion = bundle_version;
        this.timestamp = timestamp;
        this.notes = notes;
    }

    public SAMEvent(String event, String server_id, String remote_id, Timestamp timestamp, String notes)
    {
        this(event, server_id, remote_id, ((String) (null)), ((String) (null)), ((String) (null)), timestamp, notes);
    }

    public SAMEvent(String event, String server_id, Timestamp timestamp, long uptime, double shortLoad, 
            double midLoad, double longLoad)
    {
        this.event = event;
        serverID = server_id;
        this.timestamp = timestamp;
        this.uptime = uptime;
        shortTermLoad = shortLoad;
        midTermLoad = midLoad;
        longTermLoad = longLoad;
    }

    public String getBundleName()
    {
        return bundleName;
    }

    public String getBundleVersion()
    {
        return bundleVersion;
    }

    public String getEvent()
    {
        return event;
    }

    public String getNotes()
    {
        return notes;
    }

    public String getRemoteID()
    {
        return remoteID;
    }

    public String getServerID()
    {
        return serverID;
    }

    public Timestamp getTimestamp()
    {
        return timestamp;
    }

    public String getUserID()
    {
        return userID;
    }

    public long getUptime()
    {
        return uptime;
    }

    public double getShortTermLoad()
    {
        return shortTermLoad;
    }

    public double getMidTermLoad()
    {
        return midTermLoad;
    }

    public double getLongTermLoad()
    {
        return longTermLoad;
    }

    public String toString()
    {
        StringBuffer buffer = new StringBuffer();
        buffer.append(((Object)this).getClass().getName());
        buffer.append(" [");
        buffer.append(((Object)this).hashCode());
        buffer.append("]\n");
        buffer.append("\tEvent: '");
        buffer.append(getEvent());
        buffer.append("'\n");
        buffer.append("\tTimeStamp: '");
        buffer.append(((Object) (getTimestamp())));
        buffer.append("'\n");
        buffer.append("\tBundleName: '");
        buffer.append(getBundleName());
        buffer.append("'\n");
        buffer.append("\tBundleVersion: '");
        buffer.append(getBundleVersion());
        buffer.append("'\n");
        buffer.append("\tServerID: '");
        buffer.append(getServerID());
        buffer.append("'\n");
        buffer.append("\tRemoteID: '");
        buffer.append(getRemoteID());
        buffer.append("'\n");
        buffer.append("\tUserID: '");
        buffer.append(getUserID());
        buffer.append("'\n");
        buffer.append("\tNotes: '");
        buffer.append(getNotes());
        buffer.append("'\n");
        buffer.append("\tUptime: '");
        buffer.append(getUptime());
        buffer.append("'\n");
        buffer.append("\tShort term load: '");
        buffer.append(getShortTermLoad());
        buffer.append("'\n");
        buffer.append("\tMid term load: '");
        buffer.append(getMidTermLoad());
        buffer.append("'\n");
        buffer.append("\tLong term load: '");
        buffer.append(getLongTermLoad());
        buffer.append("'\n");
        return buffer.toString();
    }
}
