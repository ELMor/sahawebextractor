// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MutableAuthorizationEditor.java

package com.sitraka.deploy;

import java.awt.Component;

// Referenced classes of package com.sitraka.deploy:
//            MutableAuthorization

public interface MutableAuthorizationEditor
{

    public abstract void setAlternateEditor(Component component);

    public abstract void setKeywords(java.util.List list);

    public abstract void setAuthorization(MutableAuthorization mutableauthorization);

    public abstract void reset();
}
