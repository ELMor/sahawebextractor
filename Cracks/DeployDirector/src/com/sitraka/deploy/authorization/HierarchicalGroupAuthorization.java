// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HierarchicalGroupAuthorization.java

package com.sitraka.deploy.authorization;

import com.sitraka.deploy.AuthGroups;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.authorization:
//            DefaultGroupAuthorization

public class HierarchicalGroupAuthorization extends DefaultGroupAuthorization
{

    public HierarchicalGroupAuthorization()
    {
    }

    public HierarchicalGroupAuthorization(AuthGroups groups)
    {
        super.groupData = groups;
    }

    protected Vector getVersionForUserApp(String user, String app)
    {
        if(super.appHash == null)
            ((DefaultGroupAuthorization)this).buildIndices();
        Vector group_list = (Vector)super.appHash.get(((Object) (app)));
        if(group_list == null)
            return new Vector();
        String target_group = null;
        for(int i = 0; i < group_list.size(); i++)
        {
            DefaultGroupAuthorization.GroupVersion gv = (DefaultGroupAuthorization.GroupVersion)group_list.elementAt(i);
            String check_group = gv.getGroup();
            if(((DefaultGroupAuthorization)this).belongsTo(user, check_group) && (target_group == null || ((DefaultGroupAuthorization)this).belongsTo(check_group, target_group)))
                target_group = check_group;
        }

        Vector versions = new Vector();
        if(target_group == null)
            return versions;
        for(int i = 0; i < group_list.size(); i++)
        {
            DefaultGroupAuthorization.GroupVersion gv = (DefaultGroupAuthorization.GroupVersion)group_list.elementAt(i);
            if(target_group.equals(((Object) (gv.getGroup()))))
                versions.addElement(((Object) (gv.getVersion())));
        }

        return versions;
    }
}
