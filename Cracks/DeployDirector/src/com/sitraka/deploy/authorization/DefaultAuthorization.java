// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefaultAuthorization.java

package com.sitraka.deploy.authorization;

import com.sitraka.deploy.AuthContext;
import com.sitraka.deploy.AuthModelEvent;
import com.sitraka.deploy.AuthModelListener;
import com.sitraka.deploy.AuthorizationModel;
import com.sitraka.deploy.MutableAuthorization;
import com.sitraka.deploy.MutableAuthorizationEditor;
import com.sitraka.deploy.admin.auth.EditorFactory;
import com.sitraka.deploy.common.jclass.util.JCStringTokenizer;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.event.EventListenerList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

// Referenced classes of package com.sitraka.deploy.authorization:
//            Version, DefaultEditor

public class DefaultAuthorization
    implements MutableAuthorization, AuthorizationModel, TableModelListener
{
    class UserApp
    {

        private String user_app;

        public int hashCode()
        {
            if(user_app == null)
                return super.hashCode();
            else
                return user_app.hashCode();
        }

        public boolean equals(Object obj)
        {
            if(user_app == null)
                return super.equals(obj);
            if(obj instanceof UserApp)
                return user_app.equals(((Object) (((UserApp)obj).user_app)));
            else
                return user_app.equals(obj);
        }

        UserApp(String u, String a)
        {
            user_app = u + a.toLowerCase();
        }
    }


    protected DefaultTableModel data;
    protected boolean isChanged;
    protected Hashtable appHash;
    protected File dataFile;
    protected java.awt.Component editor;
    public static final String ANY_APPLICATION = "ALL";
    protected long lastModified;
    private EventListenerList listenerList;
    protected AuthContext authContext;

    public DefaultAuthorization()
    {
        isChanged = false;
        appHash = null;
        dataFile = null;
        editor = null;
        lastModified = 0L;
        listenerList = new EventListenerList();
        authContext = null;
    }

    public int isAuthorized(String user_id, String app_name, String version)
    {
        if(user_id == null)
            return 2;
        if(app_name == null)
            app_name = "ALL";
        Version tmp_version = null;
        if("ALL".equalsIgnoreCase(app_name))
        {
            String vStr = getVersionForUserApp(user_id, "ALL");
            if(vStr == null)
                return 2;
            tmp_version = new Version(vStr);
        } else
        {
            String vStr = getVersionForUserApp(user_id, app_name);
            if(vStr == null)
            {
                vStr = getVersionForUserApp(user_id, "ALL");
                if(vStr == null)
                    return 2;
            }
            tmp_version = new Version(vStr);
        }
        Version v = new Version(version);
        return !v.implies(tmp_version) ? 2 : 1;
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        if(dataFile == null || data_file == null)
            appHash = null;
        else
        if(dataFile.lastModified() > lastModified)
            appHash = null;
        dataFile = data_file;
        Vector rows = null;
        if(dataFile == null || !dataFile.exists())
        {
            data = new DefaultTableModel();
            rows = new Vector();
            lastModified = 0L;
        } else
        {
            if(lastModified >= dataFile.lastModified())
                return;
            lastModified = dataFile.lastModified();
            BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(dataFile)))))));
            int num_rows = 0;
            try
            {
                num_rows = Integer.parseInt(in.readLine());
            }
            catch(NumberFormatException nfe) { }
            rows = new Vector(num_rows);
            int i = 0;
            for(String line = in.readLine(); i < num_rows; line = in.readLine())
            {
                String tokens[] = JCStringTokenizer.parse(line, ',');
                Vector row = new Vector(3);
                for(int j = 0; j < tokens.length && j < 3; j++)
                    row.addElement(((Object) (tokens[j])));

                if(tokens.length < 3)
                {
                    for(int j = tokens.length; j < 3; j++)
                        row.addElement("");

                }
                rows.addElement(((Object) (row)));
                i++;
            }

            in.close();
        }
        Vector columns = new Vector(2);
        columns.addElement("User ID");
        columns.addElement("Version");
        columns.addElement("Bundle");
        if(data == null)
        {
            data = new DefaultTableModel(rows, columns);
        } else
        {
            data.setNumRows(0);
            data.setDataVector(rows, columns);
        }
        ((AbstractTableModel) (data)).addTableModelListener(((TableModelListener) (this)));
        fireAuthModelReset();
    }

    public File getDataFile()
    {
        return dataFile;
    }

    public boolean hasEditor()
    {
        return true;
    }

    public java.awt.Component getEditorComponent(String app_name)
    {
        if(editor == null)
        {
            editor = EditorFactory.createBundleAuthorizationEditor(((MutableAuthorization) (this)));
            if(editor instanceof MutableAuthorizationEditor)
            {
                ((MutableAuthorizationEditor)editor).setAlternateEditor(((java.awt.Component) (new DefaultEditor(this))));
                ArrayList keywords = new ArrayList(1);
                keywords.add(((Object) (Version.ANY_RELEASE)));
                ((MutableAuthorizationEditor)editor).setKeywords(((List) (keywords)));
            }
        }
        return editor;
    }

    public boolean commitChanges()
    {
        if(!isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(dataFile))));
            for(int i = 0; i < data.getRowCount(); i++)
            {
                String user = (String)data.getValueAt(i, 0);
                String version = (String)data.getValueAt(i, 1);
                String app = (String)data.getValueAt(i, 2);
                if((user == null || user.trim().length() < 1) && (version == null || version.trim().length() < 1) && (app == null || app.trim().length() < 1))
                {
                    data.removeRow(i);
                    i--;
                }
            }

            int row_count = data.getRowCount();
            out.println(row_count);
            for(int i = 0; i < row_count; i++)
            {
                String user = (String)data.getValueAt(i, 0);
                String version = (String)data.getValueAt(i, 1);
                String app = (String)data.getValueAt(i, 2);
                out.println((String)data.getValueAt(i, 0) + "," + (String)data.getValueAt(i, 1) + "," + (String)data.getValueAt(i, 2));
            }

            out.close();
        }
        catch(IOException ioe)
        {
            return false;
        }
        isChanged = false;
        fireAuthModelSaved();
        return true;
    }

    public boolean isModified()
    {
        return isChanged;
    }

    public void tableChanged(TableModelEvent e)
    {
        isChanged = true;
        fireAuthModelChanged();
    }

    public void setAuthContext(AuthContext context)
    {
        authContext = context;
    }

    public AuthContext getAuthContext()
    {
        return authContext;
    }

    public AuthorizationModel getAuthorizationModel()
    {
        return ((AuthorizationModel) (this));
    }

    public List getContexts()
    {
        ArrayList contexts = new ArrayList();
        String c = null;
        for(int i = 0; i < data.getRowCount(); i++)
        {
            c = (String)data.getValueAt(i, 2);
            if(c != null && c.trim().length() > 0 && !contexts.contains(((Object) (c))))
                contexts.add(((Object) (c)));
        }

        return ((List) (contexts));
    }

    public List getUsers(Object context)
    {
        return getValues(findRows(context.toString(), 2), 0);
    }

    public List getAccess(Object context, Object user)
    {
        return getValues(findRows(user.toString(), 0, findRows(context.toString(), 2)), 1);
    }

    public void setAccess(Object context, Object user, List access)
    {
        List rows = findRows(user.toString(), 0, findRows(context.toString(), 2));
        if(access != null && access.size() > 0)
        {
            if(rows.size() == 0)
            {
                data.addRow(new Object[] {
                    user, access.get(0), context
                });
            } else
            {
                data.setValueAt(access.get(0), ((Integer)rows.get(0)).intValue(), 1);
                for(int i = 1; i < rows.size(); i++)
                    data.removeRow(((Integer)rows.get(i)).intValue());

            }
        } else
        {
            for(Iterator it = rows.iterator(); it.hasNext(); data.removeRow(((Integer)it.next()).intValue()));
        }
        isChanged = true;
    }

    private List findRows(String search, int column, List rows)
    {
        ArrayList results = new ArrayList();
        int count = rows != null ? rows.size() : data.getRowCount();
        int row = 0;
        for(int i = 0; i < count; i++)
        {
            row = rows != null ? ((Integer)rows.get(i)).intValue() : i;
            if(((String)data.getValueAt(row, column)).equals(((Object) (search))))
                results.add(((Object) (new Integer(row))));
        }

        return ((List) (results));
    }

    private List findRows(String search, int column)
    {
        return findRows(search, column, ((List) (null)));
    }

    private List getValues(List rows, int column)
    {
        ArrayList results = new ArrayList();
        String value = null;
        for(int i = 0; i < rows.size(); i++)
        {
            value = (String)data.getValueAt(((Integer)rows.get(i)).intValue(), column);
            if(value != null && value.trim().length() > 0 && !results.contains(((Object) (value))))
                results.add(((Object) (value)));
        }

        return ((List) (results));
    }

    public void addAuthModelListener(AuthModelListener aml)
    {
        listenerList.add(((Object) (aml)).getClass(), ((java.util.EventListener) (aml)));
    }

    public void removeAuthModelListener(AuthModelListener aml)
    {
        listenerList.remove(((Object) (aml)).getClass(), ((java.util.EventListener) (aml)));
    }

    public void fireAuthModelChanged()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelChanged(e);

    }

    public void fireAuthModelReset()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelReset(e);

    }

    public void fireAuthModelSaved()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelSaved(e);

    }

    protected void buildIndices()
    {
        appHash = new Hashtable(data.getRowCount());
        for(int i = 0; i < data.getRowCount(); i++)
        {
            String tmp_user = (String)data.getValueAt(i, 0);
            if(tmp_user != null)
            {
                String tmp_version = (String)data.getValueAt(i, 1);
                String tmp_app = (String)data.getValueAt(i, 2);
                tmp_app = normalizeAppName(tmp_app);
                appHash.put(((Object) (new UserApp(tmp_user, tmp_app))), ((Object) (tmp_version)));
            }
        }

    }

    protected String normalizeAppName(String tmp_app)
    {
        if(tmp_app == null || tmp_app.equals("") || "ALL".equalsIgnoreCase(tmp_app))
            tmp_app = "ALL";
        return tmp_app;
    }

    protected String getVersionForUserApp(String user, String app)
    {
        if(appHash == null)
            buildIndices();
        String version = (String)appHash.get(((Object) (new UserApp(user, app))));
        return version;
    }
}
