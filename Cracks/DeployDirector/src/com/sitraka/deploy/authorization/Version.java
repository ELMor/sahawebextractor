// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Version.java

package com.sitraka.deploy.authorization;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class Version
{

    public static String ANY_RELEASE = "Any Release";
    public static String BETA_ONLY = "Beta Releases";
    public static String QUALIFIED_ONLY = "Qualified Releases";
    protected String version;

    public Version(String version)
    {
        this.version = version;
    }

    public boolean isBeta()
    {
        Integer version_values[] = parseVersion();
        for(int i = 0; i < 3; i++)
            if(version_values[i] == null)
                return true;

        return false;
    }

    public Integer[] parseVersion()
    {
        Integer version_values[] = new Integer[3];
        StringTokenizer st = new StringTokenizer(version, ".");
        try
        {
            for(int i = 0; i < 3; i++)
                try
                {
                    version_values[i] = Integer.valueOf(st.nextToken());
                }
                catch(NumberFormatException nfe) { }

        }
        catch(NoSuchElementException nsee) { }
        return version_values;
    }

    public int compare(Object version)
    {
        if(!(version instanceof Version))
            throw new IllegalArgumentException("Object not of type version");
        Version version2 = (Version)version;
        Integer version1_values[] = parseVersion();
        Integer version2_values[] = version2.parseVersion();
        for(int i = 0; i < 3; i++)
        {
            Integer i1 = version1_values[i];
            Integer i2 = version2_values[i];
            if(i1 == i2)
                break;
            if(i1 == null)
                return i2.intValue();
            if(i2 == null)
                return i1.intValue();
            int diff = i1.intValue() - i2.intValue();
            if(diff != 0)
                return diff;
        }

        return 0;
    }

    public String toString()
    {
        return version;
    }

    public boolean implies(Version version)
    {
        if(version == null)
            return false;
        if(version.toString().equals(((Object) (ANY_RELEASE))))
            return true;
        boolean is_beta = isBeta();
        if(version.toString().equals(((Object) (BETA_ONLY))))
            return is_beta;
        if(version.toString().equals(((Object) (QUALIFIED_ONLY))))
            return !is_beta;
        return version.compare(((Object) (this))) >= 0;
    }

}
