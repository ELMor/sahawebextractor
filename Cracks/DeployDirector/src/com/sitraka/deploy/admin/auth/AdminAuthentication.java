// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AdminAuthentication.java

package com.sitraka.deploy.admin.auth;

import com.sitraka.deploy.AuthContext;
import com.sitraka.deploy.AuthContextAware;
import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.NonPublicAuthContext;
import com.sitraka.deploy.util.Crypt;
import com.sitraka.deploy.util.PropertyUtils;
import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class AdminAuthentication
    implements Authentication, AuthContextAware
{

    protected Authentication userAuthentication;
    protected String defaultAdminName;
    protected String defaultAdminPwd;
    protected final Object LOCK;
    protected AuthContext authContext;

    public AdminAuthentication()
    {
        userAuthentication = null;
        defaultAdminName = null;
        defaultAdminPwd = null;
        LOCK = new Object();
        authContext = null;
    }

    public AdminAuthentication(Authentication userAuthentication)
    {
        this.userAuthentication = null;
        defaultAdminName = null;
        defaultAdminPwd = null;
        LOCK = new Object();
        authContext = null;
        if(!(userAuthentication instanceof AdminAuthentication))
            this.userAuthentication = userAuthentication;
    }

    public boolean isAuthentic(Object user)
    {
        if(isDefaultAdmin(user))
            return true;
        if(userAuthentication != null)
            return userAuthentication.isAuthentic(user);
        else
            return false;
    }

    protected boolean isDefaultAdmin(Object user)
    {
        if(authContext != null && (authContext instanceof NonPublicAuthContext) && ((NonPublicAuthContext)authContext).getProperties() != null)
            readProperties();
        else
            readPropertiesFromDisk();
        String name = null;
        String pass = null;
        synchronized(LOCK)
        {
            name = defaultAdminName;
            pass = defaultAdminPwd;
        }
        if(user == null || name == null || pass == null)
            return false;
        String userName = getUserID(user);
        if(userName == null)
            return false;
        if(!userName.equals(((Object) (name))))
        {
            return false;
        } else
        {
            String userPass = extractPassword(user);
            userPass = Crypt.crypt(pass, userPass);
            return pass.equals(((Object) (userPass)));
        }
    }

    protected void readProperties()
    {
        synchronized(LOCK)
        {
            if(authContext != null && (authContext instanceof NonPublicAuthContext))
            {
                defaultAdminName = ((NonPublicAuthContext)authContext).getProperty("deploy.admin.username");
                defaultAdminPwd = ((NonPublicAuthContext)authContext).getProperty("deploy.admin.password");
            }
        }
    }

    protected void readPropertiesFromDisk()
    {
        try
        {
            String basedir = System.getProperty("StandAloneServerBasedir");
            File cluster = new File(basedir, "cluster.properties");
            FileInputStream fis = new FileInputStream(cluster);
            Properties cluster_props = new Properties();
            cluster_props.load(((java.io.InputStream) (fis)));
            fis.close();
            defaultAdminName = PropertyUtils.readDefaultString(cluster_props, "deploy.admin.username", ((String) (null)));
            defaultAdminPwd = PropertyUtils.readDefaultString(cluster_props, "deploy.admin.password", ((String) (null)));
        }
        catch(Exception e) { }
    }

    protected String extractPassword(Object user)
    {
        String line = (String)user;
        int index = line.indexOf(":");
        if(index == -1)
            return "";
        if(index + 1 >= line.length())
            return "";
        else
            return line.substring(index + 1);
    }

    public String getUserID(Object user)
    {
        String line = (String)user;
        int index = line.indexOf(":");
        if(index == -1)
            return (String)user;
        else
            return line.substring(0, index);
    }

    public boolean usesDataFile()
    {
        if(userAuthentication != null)
            return userAuthentication.usesDataFile();
        else
            return false;
    }

    public File getDataFile()
    {
        if(userAuthentication != null)
            return userAuthentication.getDataFile();
        else
            return null;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        if(userAuthentication != null)
            userAuthentication.setDataFile(data_file);
    }

    public boolean hasEditor()
    {
        if(userAuthentication != null)
            return userAuthentication.hasEditor();
        else
            return false;
    }

    public Component getEditorComponent()
    {
        if(userAuthentication != null)
            return userAuthentication.getEditorComponent();
        else
            return null;
    }

    public boolean commitChanges()
    {
        if(userAuthentication != null)
            return userAuthentication.commitChanges();
        else
            return false;
    }

    public boolean isModified()
    {
        if(userAuthentication != null)
            return userAuthentication.isModified();
        else
            return false;
    }

    public void setAuthContext(AuthContext context)
    {
        authContext = context;
    }

    public AuthContext getAuthContext()
    {
        return authContext;
    }
}
