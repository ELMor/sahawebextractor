// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo_fr.java

package com.sitraka.deploy.authentication.resources;

import java.util.ListResourceBundle;

public class LocaleInfo_fr extends ListResourceBundle
{

    public static final String CANCEL = "Cancel";
    public static final String OK = "Ok";
    public static final String CLEAR = "Clear";
    public static final String USERNAME = "Username";
    public static final String PASSWORD = "Password";
    public static final String SERIAL_NO = "Serial#";
    public static final String ENTER_USER_INFO = "EnterUserInfo";
    public static final String ENTER_SERIAL_INFO = "EnterSerialInfo";
    public static final String ENTER_PROXY_USER_INFO = "EnterProxyUserInfo";
    public static final String PROXY_HOST_UNKNOWN = "ProxyHostUnkown";
    static final Object strings[][] = {
        {
            "Cancel", "Annuler"
        }, {
            "Ok", "OK"
        }, {
            "Clear", "Effacer"
        }, {
            "Username", "Nom d'Utilisateur"
        }, {
            "Password", "Mot de Passe"
        }, {
            "Serial#", "Num\351ro de Licence"
        }, {
            "EnterUserInfo", "Entrer Information Utilisateur"
        }, {
            "EnterSerialInfo", "Entrer le Num\351ro de Licence"
        }, {
            "EnterProxyUserInfo", "Entrer l''information relative au proxy pour l''h\364te {0}"
        }, {
            "ProxyHostUnkown", "(inconnu)"
        }
    };

    public LocaleInfo_fr()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
