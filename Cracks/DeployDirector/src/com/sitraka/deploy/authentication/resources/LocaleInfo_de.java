// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo_de.java

package com.sitraka.deploy.authentication.resources;

import java.util.ListResourceBundle;

public class LocaleInfo_de extends ListResourceBundle
{

    public static final String CANCEL = "Cancel";
    public static final String OK = "Ok";
    public static final String CLEAR = "Clear";
    public static final String USERNAME = "Username";
    public static final String PASSWORD = "Password";
    public static final String SERIAL_NO = "Serial#";
    public static final String ENTER_USER_INFO = "EnterUserInfo";
    public static final String ENTER_SERIAL_INFO = "EnterSerialInfo";
    public static final String ENTER_PROXY_USER_INFO = "EnterProxyUserInfo";
    public static final String PROXY_HOST_UNKNOWN = "ProxyHostUnkown";
    static final Object strings[][] = {
        {
            "Cancel", "Abbrechen"
        }, {
            "Ok", "OK"
        }, {
            "Clear", "L\366schen"
        }, {
            "Username", "Benutzername"
        }, {
            "Password", "Passwort"
        }, {
            "Serial#", "Seriennummer"
        }, {
            "EnterUserInfo", "Benutzerdaten Eingeben"
        }, {
            "EnterSerialInfo", "Seriennummer Eingeben"
        }, {
            "EnterProxyUserInfo", "Geben Sie die Proxy-Benutzerinformationen f\374r den Host {0} ein"
        }, {
            "ProxyHostUnkown", "(unbekannt)"
        }
    };

    public LocaleInfo_de()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
