// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   WindowsAuthEditor.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.authentication.resources.LocaleInfo;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.sitraka.deploy.authentication:
//            WindowsAuthentication

public class WindowsAuthEditor extends JPanel
    implements ActionListener, FocusListener
{

    protected JLabel domainLabel;
    protected JTextField domainEntry;
    protected boolean editable;
    protected WindowsAuthentication authInfo;

    public WindowsAuthEditor(WindowsAuthentication src)
    {
        init();
        setAuthInfo(src);
    }

    public void setAuthInfo(WindowsAuthentication src)
    {
        authInfo = src;
        ((JTextComponent) (domainEntry)).setText(authInfo.getDomain());
    }

    public WindowsAuthentication getAuthInfo()
    {
        authInfo.setDomain(((JTextComponent) (domainEntry)).getText());
        return authInfo;
    }

    public void setEditable(boolean editable)
    {
        this.editable = editable;
        ((JTextComponent) (domainEntry)).setEditable(editable);
    }

    public boolean isEditable()
    {
        return editable;
    }

    protected void init()
    {
        JPanel fixedSizePanel = new JPanel();
        ((Container)this).add(((Component) (fixedSizePanel)));
        ((Container) (fixedSizePanel)).setLayout(((java.awt.LayoutManager) (new GridLayout(1, 2, 3, 3))));
        domainLabel = new JLabel(LocaleInfo.li.getString("Windows Auth Editor Domain"));
        ((Container) (fixedSizePanel)).add(((Component) (domainLabel)));
        domainEntry = new JTextField();
        domainEntry.addActionListener(((ActionListener) (this)));
        ((Component) (domainEntry)).addFocusListener(((FocusListener) (this)));
        ((Container) (fixedSizePanel)).add(((Component) (domainEntry)));
    }

    public void actionPerformed(ActionEvent e)
    {
        getAuthInfo();
    }

    public void focusLost(FocusEvent fe)
    {
        getAuthInfo();
    }

    public void focusGained(FocusEvent focusevent)
    {
    }
}
