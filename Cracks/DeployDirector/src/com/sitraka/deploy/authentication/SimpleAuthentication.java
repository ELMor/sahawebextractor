// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SimpleAuthentication.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.ReadableUserList;
import com.sitraka.deploy.authentication.resources.LocaleInfo;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

// Referenced classes of package com.sitraka.deploy.authentication:
//            AbstractAuthentication, SimpleAuthEditor

public class SimpleAuthentication extends AbstractAuthentication
    implements TableModelListener, ReadableUserList
{

    protected DefaultTableModel data;
    protected Hashtable userHash;

    public SimpleAuthentication()
    {
        userHash = null;
        super.encryptionEnabled = true;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        if(super.dataFile == null || data_file == null)
            userHash = null;
        else
        if(super.dataFile.lastModified() > super.lastModified)
            userHash = null;
        super.dataFile = data_file;
        if(super.dataFile != null && super.dataFile.exists() && data != null && super.dataFile.lastModified() <= super.lastModified)
        {
            return;
        } else
        {
            reload();
            return;
        }
    }

    public void reload()
        throws IOException
    {
        Vector rows = null;
        if(super.dataFile == null || !super.dataFile.exists())
        {
            if(data == null)
                data = new DefaultTableModel();
            rows = new Vector();
        } else
        {
            super.lastModified = super.dataFile.lastModified();
            BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(super.dataFile)))))));
            int num_rows = 0;
            try
            {
                num_rows = Integer.parseInt(in.readLine());
            }
            catch(NumberFormatException nfe) { }
            rows = new Vector(num_rows);
            int i = 0;
            for(String line = in.readLine(); i < num_rows; line = in.readLine())
            {
                StringTokenizer st = new StringTokenizer(line, ":");
                Vector row = new Vector(2);
                try
                {
                    row.addElement(((Object) (st.nextToken())));
                    row.addElement(((Object) (st.nextToken())));
                }
                catch(NoSuchElementException nsee)
                {
                    for(; row.size() < 2; row.addElement(""));
                }
                rows.addElement(((Object) (row)));
                i++;
            }

            in.close();
        }
        Vector columns = new Vector(2);
        columns.addElement(((Object) (LocaleInfo.li.getString("Simple Auth User ID"))));
        columns.addElement(((Object) (LocaleInfo.li.getString("Simple Auth Password"))));
        if(data == null)
        {
            data = new DefaultTableModel(rows, columns);
        } else
        {
            data.setNumRows(0);
            data.setDataVector(rows, columns);
        }
        ((AbstractTableModel) (data)).addTableModelListener(((TableModelListener) (this)));
    }

    public File getDataFile()
    {
        return super.dataFile;
    }

    public Component getEditorComponent()
    {
        if(super.editor == null)
            super.editor = ((Component) (new SimpleAuthEditor(this)));
        return super.editor;
    }

    public boolean commitChanges()
    {
        if(!((AbstractAuthentication)this).isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(super.dataFile))));
            for(int i = 0; i < data.getRowCount(); i++)
            {
                String user = (String)data.getValueAt(i, 0);
                String password = (String)data.getValueAt(i, 1);
                if((user == null || user.trim().length() <= 0) && (password == null || password.length() <= 0))
                {
                    data.removeRow(i);
                    i--;
                }
            }

            int row_count = data.getRowCount();
            out.println(row_count);
            for(int i = 0; i < row_count; i++)
            {
                String user = (String)data.getValueAt(i, 0);
                String password = (String)data.getValueAt(i, 1);
                out.println(user + ":" + password);
            }

            out.close();
        }
        catch(IOException ioe)
        {
            return false;
        }
        super.isChanged = false;
        return true;
    }

    public void tableChanged(TableModelEvent e)
    {
        super.isChanged = true;
    }

    protected String getPasswordForUserID(String user_id)
    {
        if(userHash == null)
        {
            userHash = new Hashtable(data.getRowCount());
            for(int i = 0; i < data.getRowCount(); i++)
            {
                String tmp_user = (String)data.getValueAt(i, 0);
                String tmp_password = (String)data.getValueAt(i, 1);
                if(tmp_password == null)
                    tmp_password = "";
                userHash.put(((Object) (tmp_user)), ((Object) (tmp_password)));
            }

        }
        String password = (String)userHash.get(((Object) (user_id)));
        return password;
    }

    public java.util.List getUsers()
    {
        int rowCount = data.getRowCount();
        ArrayList users = new ArrayList(rowCount);
        for(int i = 0; i < rowCount; i++)
            users.add(data.getValueAt(i, 0));

        return ((java.util.List) (users));
    }
}
