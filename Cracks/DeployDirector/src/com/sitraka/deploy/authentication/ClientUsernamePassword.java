// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClientUsernamePassword.java

package com.sitraka.deploy.authentication;

import java.awt.Component;
import java.awt.TextComponent;
import java.awt.TextField;

// Referenced classes of package com.sitraka.deploy.authentication:
//            AbstractClientHTTPAuthentication, UsernamePasswordEditor

public class ClientUsernamePassword extends AbstractClientHTTPAuthentication
{

    protected UsernamePasswordEditor editor;
    protected boolean permitEditor;

    public ClientUsernamePassword()
    {
        editor = null;
        permitEditor = true;
    }

    public Object getAuthenticationInfo()
    {
        if(!super.dataInitialized && permitEditor)
        {
            getEditorComponent();
            editor.popup();
        }
        return super.getAuthenticationInfo();
    }

    public boolean hasEditor()
    {
        return permitEditor;
    }

    public boolean getAllowEditor()
    {
        return permitEditor;
    }

    public void setAllowEditor(boolean allowEdit)
    {
        permitEditor = allowEdit;
    }

    public void initEditor()
    {
        if(editor != null && permitEditor)
        {
            editor.username.setText(super.username);
            editor.password.setText(super.password);
        }
    }

    public void commitEdits()
    {
        if(!permitEditor || editor == null)
        {
            return;
        } else
        {
            ((AbstractClientHTTPAuthentication)this).setData(((TextComponent) (editor.username)).getText(), ((TextComponent) (editor.password)).getText());
            ((AbstractClientHTTPAuthentication)this).writeAuthenticationInfo();
            return;
        }
    }

    public Component getEditorComponent()
    {
        if(editor == null && permitEditor)
            editor = new UsernamePasswordEditor(this);
        return ((Component) (editor));
    }
}
