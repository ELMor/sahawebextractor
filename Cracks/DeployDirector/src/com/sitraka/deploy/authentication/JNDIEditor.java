// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JNDIEditor.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.authentication.resources.LocaleInfo;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.sitraka.deploy.authentication:
//            JNDIAuthentication

public class JNDIEditor extends JPanel
    implements ActionListener, FocusListener
{

    protected JLabel factoryLabel;
    protected JTextField factoryEntry;
    protected JLabel providerLabel;
    protected JTextField providerEntry;
    protected JLabel pwServiceLabel;
    protected JTextField pwServiceEntry;
    protected JLabel pwAttributeLabel;
    protected JTextField pwAttributeEntry;
    protected boolean editable;
    protected JNDIAuthentication authInfo;

    public JNDIEditor(JNDIAuthentication src)
    {
        init();
        setAuthInfo(src);
    }

    public void setAuthInfo(JNDIAuthentication src)
    {
        authInfo = src;
        ((JTextComponent) (factoryEntry)).setText(authInfo.getInitialContextFactory());
        ((JTextComponent) (providerEntry)).setText(authInfo.getProviderURL());
        ((JTextComponent) (pwServiceEntry)).setText(authInfo.getPasswordService());
        ((JTextComponent) (pwAttributeEntry)).setText(authInfo.getPasswordAttribute());
    }

    public JNDIAuthentication getAuthInfo()
    {
        authInfo.setInitialContextFactory(((JTextComponent) (factoryEntry)).getText());
        authInfo.setProviderURL(((JTextComponent) (providerEntry)).getText());
        authInfo.setPasswordService(((JTextComponent) (pwServiceEntry)).getText());
        authInfo.setPasswordAttribute(((JTextComponent) (pwAttributeEntry)).getText());
        return authInfo;
    }

    public void setEditable(boolean editable)
    {
        this.editable = editable;
        ((JTextComponent) (factoryEntry)).setEditable(editable);
        ((JTextComponent) (providerEntry)).setEditable(editable);
        ((JTextComponent) (pwServiceEntry)).setEditable(editable);
        ((JTextComponent) (pwAttributeEntry)).setEditable(editable);
    }

    public boolean isEditable()
    {
        return editable;
    }

    protected void init()
    {
        JPanel fixedSizePanel = new JPanel();
        ((Container)this).add(((Component) (fixedSizePanel)));
        ((Container) (fixedSizePanel)).setLayout(((java.awt.LayoutManager) (new GridLayout(4, 2, 3, 3))));
        factoryLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor Context Factory"));
        ((Container) (fixedSizePanel)).add(((Component) (factoryLabel)));
        factoryEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (factoryEntry)));
        factoryEntry.addActionListener(((ActionListener) (this)));
        ((Component) (factoryEntry)).addFocusListener(((FocusListener) (this)));
        providerLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor Naming Service"));
        ((Container) (fixedSizePanel)).add(((Component) (providerLabel)));
        providerEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (providerEntry)));
        providerEntry.addActionListener(((ActionListener) (this)));
        ((Component) (providerEntry)).addFocusListener(((FocusListener) (this)));
        pwServiceLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor Password Service"));
        ((Container) (fixedSizePanel)).add(((Component) (pwServiceLabel)));
        pwServiceEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (pwServiceEntry)));
        pwServiceEntry.addActionListener(((ActionListener) (this)));
        ((Component) (pwServiceEntry)).addFocusListener(((FocusListener) (this)));
        pwAttributeLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor Password Attribute"));
        ((Container) (fixedSizePanel)).add(((Component) (pwAttributeLabel)));
        pwAttributeEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (pwAttributeEntry)));
        pwAttributeEntry.addActionListener(((ActionListener) (this)));
        ((Component) (pwAttributeEntry)).addFocusListener(((FocusListener) (this)));
    }

    public void actionPerformed(ActionEvent e)
    {
        getAuthInfo();
    }

    public void focusLost(FocusEvent fe)
    {
        getAuthInfo();
    }

    public void focusGained(FocusEvent focusevent)
    {
    }
}
