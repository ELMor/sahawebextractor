// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClientSerialNumber.java

package com.sitraka.deploy.authentication;

import java.awt.Component;
import java.awt.TextComponent;
import java.awt.TextField;

// Referenced classes of package com.sitraka.deploy.authentication:
//            AbstractClientHTTPAuthentication, SerialNumberEditor

public class ClientSerialNumber extends AbstractClientHTTPAuthentication
{

    protected SerialNumberEditor editor;

    public ClientSerialNumber()
    {
        editor = null;
    }

    public Object getAuthenticationInfo()
    {
        if(!super.dataInitialized)
        {
            getEditorComponent();
            editor.popup();
        }
        return super.getAuthenticationInfo();
    }

    public boolean hasEditor()
    {
        return true;
    }

    public void initEditor()
    {
        editor.serialNumber.setText(super.username);
    }

    public void commitEdits()
    {
        ((AbstractClientHTTPAuthentication)this).setData(((TextComponent) (editor.serialNumber)).getText(), ((String) (null)));
        ((AbstractClientHTTPAuthentication)this).writeAuthenticationInfo();
    }

    public Component getEditorComponent()
    {
        if(editor == null)
            editor = new SerialNumberEditor(this);
        return ((Component) (editor));
    }
}
