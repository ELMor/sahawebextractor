// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   WindowsAuthentication.java

package com.sitraka.deploy.authentication;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

// Referenced classes of package com.sitraka.deploy.authentication:
//            AbstractAuthentication, WindowsAuthEditor

public class WindowsAuthentication extends AbstractAuthentication
{

    public static final String DOMAIN = "domain";
    protected String domain;

    public WindowsAuthentication()
    {
        domain = "";
        setDefaultValues();
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String newDom)
    {
        if(((AbstractAuthentication)this).unchanged(newDom, domain))
        {
            return;
        } else
        {
            domain = newDom;
            super.isChanged = true;
            return;
        }
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        super.dataFile = data_file;
        if(super.dataFile == null || !super.dataFile.exists())
            return;
        if(super.dataFile.lastModified() <= super.lastModified)
            return;
        super.lastModified = super.dataFile.lastModified();
        BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(super.dataFile)))))));
        for(String line = in.readLine(); line != null; line = in.readLine())
            try
            {
                StringTokenizer st = new StringTokenizer(line, "=");
                if(st.hasMoreTokens())
                {
                    String property = st.nextToken();
                    if(st.hasMoreTokens())
                    {
                        String value = st.nextToken();
                        if("domain".equals(((Object) (property))))
                            domain = value;
                    }
                }
            }
            catch(Exception e)
            {
                ((Throwable) (e)).printStackTrace(System.out);
                setDefaultValues();
                line = null;
            }

        in.close();
    }

    protected void setDefaultValues()
    {
        domain = null;
    }

    public Component getEditorComponent()
    {
        if(super.editor == null)
            super.editor = ((Component) (new WindowsAuthEditor(this)));
        return super.editor;
    }

    public boolean commitChanges()
    {
        if(!((AbstractAuthentication)this).isModified())
            return true;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(super.dataFile))));
            out.println("domain=" + domain);
            out.close();
        }
        catch(IOException e)
        {
            return false;
        }
        super.isChanged = false;
        return true;
    }

    public boolean isAuthentic(Object user)
    {
        String store[] = ((AbstractAuthentication)this).getUserIDandPassword(user);
        String user_id = store[0];
        String password = store[1];
        if(user_id == null)
            user_id = "";
        if(password == null)
            password = "";
        if(domain == null)
            domain = "";
        return isAuthentic(user_id, password, domain);
    }

    public native boolean isAuthentic(String s, String s1, String s2);

    protected String getPasswordForUserID(String user_id)
    {
        return null;
    }

    static 
    {
        String os = System.getProperty("os.name").toLowerCase();
        if(os.startsWith("win"))
            System.loadLibrary("winauth");
    }
}
