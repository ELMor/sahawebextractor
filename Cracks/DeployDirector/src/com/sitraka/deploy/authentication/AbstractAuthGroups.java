// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractAuthGroups.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.AuthGroups;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

public abstract class AbstractAuthGroups
    implements AuthGroups
{

    protected Component editor;
    protected boolean isChanged;
    protected File dataFile;
    protected long lastModified;

    public AbstractAuthGroups()
    {
        editor = null;
        isChanged = false;
        dataFile = null;
        lastModified = 0L;
    }

    public abstract Vector listAllGroups();

    public abstract boolean isGroup(String s);

    public abstract Vector getGroupMembers(String s);

    public Vector listAllMembers(String group)
    {
        if(group == null || group.length() == 0)
            return new Vector();
        Vector members = new Vector();
        Vector history = new Vector();
        if(!isGroup(group))
        {
            members.addElement(((Object) (group)));
            return members;
        }
        history.addElement(((Object) (group)));
        for(int group_index = 0; group_index < history.size(); group_index++)
        {
            String name = (String)history.elementAt(group_index);
            Vector group_members = getGroupMembers(name);
            for(int i = 0; i < group_members.size(); i++)
            {
                String member_name = (String)group_members.elementAt(i);
                if(isGroup(member_name))
                {
                    if(!history.contains(((Object) (member_name))))
                        history.addElement(((Object) (member_name)));
                } else
                {
                    members.addElement(((Object) (member_name)));
                }
            }

        }

        return members;
    }

    public boolean belongsTo(String user, String group)
    {
        return listAllMembers(group).contains(((Object) (user)));
    }

    public abstract boolean usesDataFile();

    public File getDataFile()
    {
        return dataFile;
    }

    public abstract void setDataFile(File file)
        throws IOException;

    public abstract boolean hasEditor();

    public abstract Component getEditorComponent();

    public boolean isModified()
    {
        return isChanged;
    }

    public abstract boolean commitChanges();

    protected boolean unchanged(String a, String b)
    {
        if(a == b)
            return true;
        if(a == null || b == null)
            return false;
        else
            return a.equals(((Object) (b)));
    }
}
