// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   NISAuthGroupsEditor.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.authentication.resources.LocaleInfo;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

// Referenced classes of package com.sitraka.deploy.authentication:
//            NISAuthGroups

public class NISAuthGroupsEditor extends JPanel
    implements ActionListener, FocusListener
{

    protected JLabel factoryLabel;
    protected JTextField factoryEntry;
    protected JLabel providerLabel;
    protected JTextField providerEntry;
    protected JLabel gpServiceLabel;
    protected JTextField gpServiceEntry;
    protected JLabel gpAttributeLabel;
    protected JTextField gpAttributeEntry;
    protected JLabel usrServiceLabel;
    protected JTextField usrServiceEntry;
    protected JLabel usrAttributeLabel;
    protected JTextField usrAttributeEntry;
    protected boolean editable;
    protected NISAuthGroups authInfo;

    public NISAuthGroupsEditor(NISAuthGroups src)
    {
        init();
        setAuthInfo(src);
    }

    public void setAuthInfo(NISAuthGroups src)
    {
        authInfo = src;
        ((JTextComponent) (factoryEntry)).setText(authInfo.getInitialContextFactory());
        ((JTextComponent) (providerEntry)).setText(authInfo.getProviderURL());
        ((JTextComponent) (gpServiceEntry)).setText(authInfo.getGroupService());
        ((JTextComponent) (gpAttributeEntry)).setText(authInfo.getGroupAttribute());
        ((JTextComponent) (usrServiceEntry)).setText(authInfo.getUserService());
        ((JTextComponent) (usrAttributeEntry)).setText(authInfo.getUserAttribute());
    }

    public NISAuthGroups getAuthInfo()
    {
        authInfo.setInitialContextFactory(((JTextComponent) (factoryEntry)).getText());
        authInfo.setProviderURL(((JTextComponent) (providerEntry)).getText());
        authInfo.setGroupService(((JTextComponent) (gpServiceEntry)).getText());
        authInfo.setGroupAttribute(((JTextComponent) (gpAttributeEntry)).getText());
        authInfo.setUserService(((JTextComponent) (usrServiceEntry)).getText());
        authInfo.setUserAttribute(((JTextComponent) (usrAttributeEntry)).getText());
        return authInfo;
    }

    public void setEditable(boolean editable)
    {
        this.editable = editable;
        ((JTextComponent) (factoryEntry)).setEditable(editable);
        ((JTextComponent) (providerEntry)).setEditable(editable);
        ((JTextComponent) (gpServiceEntry)).setEditable(editable);
        ((JTextComponent) (gpAttributeEntry)).setEditable(editable);
        ((JTextComponent) (usrServiceEntry)).setEditable(editable);
        ((JTextComponent) (usrAttributeEntry)).setEditable(editable);
    }

    public boolean isEditable()
    {
        return editable;
    }

    protected void init()
    {
        JPanel fixedSizePanel = new JPanel();
        ((Container)this).add(((Component) (fixedSizePanel)));
        ((Container) (fixedSizePanel)).setLayout(((java.awt.LayoutManager) (new GridLayout(6, 2, 3, 3))));
        factoryLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor Context Factory"));
        ((Container) (fixedSizePanel)).add(((Component) (factoryLabel)));
        factoryEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (factoryEntry)));
        factoryEntry.addActionListener(((ActionListener) (this)));
        ((Component) (factoryEntry)).addFocusListener(((FocusListener) (this)));
        providerLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor Naming Service"));
        ((Container) (fixedSizePanel)).add(((Component) (providerLabel)));
        providerEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (providerEntry)));
        providerEntry.addActionListener(((ActionListener) (this)));
        ((Component) (providerEntry)).addFocusListener(((FocusListener) (this)));
        gpServiceLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor User Service"));
        ((Container) (fixedSizePanel)).add(((Component) (gpServiceLabel)));
        gpServiceEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (gpServiceEntry)));
        gpServiceEntry.addActionListener(((ActionListener) (this)));
        ((Component) (gpServiceEntry)).addFocusListener(((FocusListener) (this)));
        gpAttributeLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor User Attribute"));
        ((Container) (fixedSizePanel)).add(((Component) (gpAttributeLabel)));
        gpAttributeEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (gpAttributeEntry)));
        gpAttributeEntry.addActionListener(((ActionListener) (this)));
        ((Component) (gpAttributeEntry)).addFocusListener(((FocusListener) (this)));
        usrServiceLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor User Service"));
        ((Container) (fixedSizePanel)).add(((Component) (usrServiceLabel)));
        usrServiceEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (usrServiceEntry)));
        usrServiceEntry.addActionListener(((ActionListener) (this)));
        ((Component) (usrServiceEntry)).addFocusListener(((FocusListener) (this)));
        usrAttributeLabel = new JLabel(LocaleInfo.li.getString("JNDI Editor User Attribute"));
        ((Container) (fixedSizePanel)).add(((Component) (usrAttributeLabel)));
        usrAttributeEntry = new JTextField();
        ((Container) (fixedSizePanel)).add(((Component) (usrAttributeEntry)));
        usrAttributeEntry.addActionListener(((ActionListener) (this)));
        ((Component) (usrAttributeEntry)).addFocusListener(((FocusListener) (this)));
    }

    public void actionPerformed(ActionEvent e)
    {
        getAuthInfo();
    }

    public void focusLost(FocusEvent fe)
    {
        getAuthInfo();
    }

    public void focusGained(FocusEvent focusevent)
    {
    }
}
