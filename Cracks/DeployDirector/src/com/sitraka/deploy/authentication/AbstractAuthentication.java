// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractAuthentication.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.util.Crypt;
import java.awt.Component;
import java.io.File;
import java.io.IOException;

public abstract class AbstractAuthentication
    implements Authentication
{

    public boolean encryptionEnabled;
    protected Component editor;
    protected boolean isChanged;
    protected File dataFile;
    protected long lastModified;

    public AbstractAuthentication()
    {
        encryptionEnabled = false;
        editor = null;
        isChanged = false;
        dataFile = null;
        lastModified = 0L;
    }

    public boolean isAuthentic(Object user)
    {
        String store[] = getUserIDandPassword(user);
        String user_id = store[0];
        String password = store[1];
        String real_password = getPasswordForUserID(user_id);
        return doesPasswordMatch(password, real_password);
    }

    public String getUserID(Object user)
    {
        String store[] = getUserIDandPassword(user);
        return store[0];
    }

    protected String[] getUserIDandPassword(Object user)
    {
        String store[] = new String[2];
        if(user != null && (user instanceof String))
        {
            String provided = (String)user;
            int index = provided.indexOf(":");
            if(index == -1)
            {
                store[0] = provided;
            } else
            {
                store[0] = provided.substring(0, index);
                store[1] = provided.substring(index + 1);
            }
        }
        if(store[0] == null)
            store[0] = "";
        if(store[1] == null)
            store[1] = "";
        return store;
    }

    protected boolean doesPasswordMatch(String userProvidedPW, String systemPW)
    {
        if(systemPW == userProvidedPW)
            return true;
        if(systemPW == null)
            return false;
        if(encryptionEnabled)
        {
            String enc = userProvidedPW;
            if(systemPW.length() >= 2)
                enc = Crypt.crypt(systemPW.substring(0, 2), userProvidedPW);
            if(enc.equals(((Object) (systemPW))))
                return true;
        } else
        if(systemPW.equals(((Object) (userProvidedPW))))
            return true;
        return false;
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public File getDataFile()
    {
        return dataFile;
    }

    public boolean hasEditor()
    {
        return true;
    }

    public boolean isModified()
    {
        return isChanged;
    }

    protected abstract String getPasswordForUserID(String s);

    protected boolean unchanged(String a, String b)
    {
        if(a == b)
            return true;
        return a != null && b != null && a.equals(((Object) (b)));
    }

    public abstract boolean commitChanges();

    public abstract Component getEditorComponent();

    public abstract void setDataFile(File file)
        throws IOException;
}
