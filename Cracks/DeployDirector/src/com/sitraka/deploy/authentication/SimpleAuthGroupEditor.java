// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SimpleAuthGroupEditor.java

package com.sitraka.deploy.authentication;

import com.klg.jclass.util.swing.JCAction;
import com.klg.jclass.util.swing.JCSortableTable;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.util.EventObject;
import javax.swing.CellEditor;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

// Referenced classes of package com.sitraka.deploy.authentication:
//            SimpleAuthGroups

public class SimpleAuthGroupEditor extends JPanel
    implements TableModelListener
{
    protected class ActionDeleteRow extends JCAction
        implements ListSelectionListener
    {

        public void actionPerformed(ActionEvent e)
        {
            int rows[] = ((JTable) (table)).getSelectedRows();
            ((JTable) (table)).clearSelection();
            if(((JTable) (table)).getCellEditor() != null)
                ((CellEditor) (((JTable) (table)).getCellEditor())).cancelCellEditing();
            for(int i = rows.length - 1; i >= 0; i--)
                groups.data.removeRow(rows[i]);

        }

        public void valueChanged(ListSelectionEvent e)
        {
            if(((ListSelectionModel)((EventObject) (e)).getSource()).isSelectionEmpty())
                ((JCAction)this).setEnabled(false);
            else
                ((JCAction)this).setEnabled(true);
        }

        public ActionDeleteRow()
        {
            super("ActionDelete");
            ((JCAction)this).setEnabled(false);
        }
    }

    protected class ActionNewRow extends JCAction
    {

        public void actionPerformed(ActionEvent e)
        {
            Object row[] = new Object[2];
            row[0] = "";
            row[1] = "";
            groups.data.addRow(row);
        }

        public ActionNewRow()
        {
            super("ActionNew");
        }
    }


    protected JCSortableTable table;
    protected SimpleAuthGroups groups;
    protected boolean isChanged;
    protected ActionNewRow newrowAction;
    protected ActionDeleteRow deleterowAction;

    public SimpleAuthGroupEditor(SimpleAuthGroups groupDefs)
    {
        isChanged = false;
        groups = groupDefs;
        ((Container)this).setLayout(((java.awt.LayoutManager) (new BorderLayout())));
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        table = new JCSortableTable(((javax.swing.table.TableModel) (groups.data)));
        for(int i = 0; i < groups.data.getColumnCount(); i++)
        {
            String name = groups.data.getColumnName(i);
            TableColumn col = ((JTable) (table)).getColumn(((Object) (name)));
            DefaultTableCellRenderer ren = new DefaultTableCellRenderer();
            ((JLabel) (ren)).setHorizontalAlignment(2);
            col.setCellRenderer(((javax.swing.table.TableCellRenderer) (ren)));
        }

        ((AbstractTableModel) (groups.data)).addTableModelListener(((TableModelListener) (this)));
        newrowAction = new ActionNewRow();
        toolbar.add(((javax.swing.Action) (newrowAction)));
        deleterowAction = new ActionDeleteRow();
        toolbar.add(((javax.swing.Action) (deleterowAction)));
        ((JTable) (table)).getSelectionModel().addListSelectionListener(((ListSelectionListener) (deleterowAction)));
        ((Container)this).add(((java.awt.Component) (toolbar)), "North");
        ((Container)this).add(((java.awt.Component) (new JScrollPane(((java.awt.Component) (table))))), "Center");
    }

    public void tableChanged(TableModelEvent e)
    {
        isChanged = true;
    }
}
