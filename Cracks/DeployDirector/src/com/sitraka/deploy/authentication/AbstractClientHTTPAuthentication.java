// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AbstractClientHTTPAuthentication.java

package com.sitraka.deploy.authentication;

import com.sitraka.deploy.ClientAuthentication;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public abstract class AbstractClientHTTPAuthentication
    implements ClientAuthentication
{

    protected boolean dataInitialized;
    protected String username;
    protected String password;
    protected File dataFile;
    private static byte Base64EncMap[];
    private static byte Base64DecMap[];

    public AbstractClientHTTPAuthentication()
    {
        dataInitialized = false;
        username = null;
        password = null;
        dataFile = null;
    }

    public Object getAuthenticationInfo()
    {
        if(username == null && password == null)
            return ((Object) (""));
        if(password == null)
            password = "";
        return ((Object) (username + ":" + password));
    }

    public void setAuthenticationInfo(Object obj)
    {
        String info = obj.toString();
        if(info == null || info.length() == 0)
            return;
        int index = info.indexOf(":");
        String user = null;
        String pass = null;
        if(index >= 0)
        {
            user = info.substring(0, index);
            if(index + 1 < info.length())
                pass = info.substring(index + 1);
        } else
        {
            user = info;
        }
        setData(user, pass);
    }

    public boolean writeAuthenticationInfo()
    {
        if(!usesDataFile() || dataFile == null)
            return false;
        try
        {
            PrintWriter out = new PrintWriter(((java.io.OutputStream) (new FileOutputStream(dataFile))));
            out.println(base64Encode(username));
            out.println(base64Encode(password));
            out.close();
        }
        catch(IOException ioe) { }
        return true;
    }

    public boolean usesDataFile()
    {
        return true;
    }

    public void setDataFile(File data_file)
        throws IOException
    {
        dataFile = data_file;
        if(dataFile.exists())
        {
            BufferedReader in = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(dataFile)))))));
            String user = base64Decode(in.readLine());
            String password = base64Decode(in.readLine());
            setData(user, password);
            in.close();
        }
    }

    public void setData(String un, String p)
    {
        boolean change = false;
        if(username != un && (un == null || !un.equals(((Object) (username)))))
        {
            username = un;
            change = true;
        }
        if(password != p && (password != null || p != null && !p.equals(((Object) (password)))))
        {
            password = p;
            change = true;
        }
        if(change)
            dataInitialized = true;
    }

    public void setAuthenticationInfoAvailable(boolean value)
    {
        dataInitialized = value;
    }

    public boolean isAuthenticationInfoAvailable()
    {
        return dataInitialized;
    }

    public abstract boolean hasEditor();

    public abstract void initEditor();

    public abstract void commitEdits();

    public abstract Component getEditorComponent();

    public static final String base64Encode(String str)
    {
        if(str == null)
        {
            return null;
        } else
        {
            byte data[] = new byte[str.length()];
            data = str.getBytes();
            return new String(base64Encode(data));
        }
    }

    public static final byte[] base64Encode(byte data[])
    {
        if(data == null)
            return null;
        byte dest[] = new byte[((data.length + 2) / 3) * 4];
        int sidx = 0;
        int didx = 0;
        for(; sidx < data.length - 2; sidx += 3)
        {
            dest[didx++] = Base64EncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = Base64EncMap[data[sidx + 1] >>> 4 & 0xf | data[sidx] << 4 & 0x3f];
            dest[didx++] = Base64EncMap[data[sidx + 2] >>> 6 & 3 | data[sidx + 1] << 2 & 0x3f];
            dest[didx++] = Base64EncMap[data[sidx + 2] & 0x3f];
        }

        if(sidx < data.length)
        {
            dest[didx++] = Base64EncMap[data[sidx] >>> 2 & 0x3f];
            if(sidx < data.length - 1)
            {
                dest[didx++] = Base64EncMap[data[sidx + 1] >>> 4 & 0xf | data[sidx] << 4 & 0x3f];
                dest[didx++] = Base64EncMap[data[sidx + 1] << 2 & 0x3f];
            } else
            {
                dest[didx++] = Base64EncMap[data[sidx] << 4 & 0x3f];
            }
        }
        for(; didx < dest.length; didx++)
            dest[didx] = 61;

        return dest;
    }

    public static final String base64Decode(String str)
    {
        if(str == null || str.equals(""))
        {
            return null;
        } else
        {
            byte data[] = new byte[str.length()];
            data = str.getBytes();
            return new String(base64Decode(data));
        }
    }

    public static final byte[] base64Decode(byte data[])
    {
        if(data == null || data.length == 0)
            return null;
        int tail;
        for(tail = data.length; data[tail - 1] == 61; tail--);
        byte dest[] = new byte[tail - data.length / 4];
        for(int idx = 0; idx < data.length; idx++)
            data[idx] = Base64DecMap[data[idx]];

        int sidx = 0;
        int didx;
        for(didx = 0; didx < dest.length - 2; didx += 3)
        {
            dest[didx] = (byte)(data[sidx] << 2 & 0xff | data[sidx + 1] >>> 4 & 3);
            dest[didx + 1] = (byte)(data[sidx + 1] << 4 & 0xff | data[sidx + 2] >>> 2 & 0xf);
            dest[didx + 2] = (byte)(data[sidx + 2] << 6 & 0xff | data[sidx + 3] & 0x3f);
            sidx += 4;
        }

        if(didx < dest.length)
            dest[didx] = (byte)(data[sidx] << 2 & 0xff | data[sidx + 1] >>> 4 & 3);
        if(++didx < dest.length)
            dest[didx] = (byte)(data[sidx + 1] << 4 & 0xff | data[sidx + 2] >>> 2 & 0xf);
        return dest;
    }

    static 
    {
        byte map[] = {
            65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 
            75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 
            85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 
            101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 
            111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 
            121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 
            56, 57, 43, 47
        };
        Base64EncMap = map;
        Base64DecMap = new byte[128];
        for(int idx = 0; idx < Base64EncMap.length; idx++)
            Base64DecMap[Base64EncMap[idx]] = (byte)idx;

    }
}
