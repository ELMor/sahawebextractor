// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Authentication.java

package com.sitraka.deploy;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

public interface Authentication
{

    public static final String AUTHENTICATION_FILENAME = "authentication.dat";

    public abstract boolean isAuthentic(Object obj);

    public abstract String getUserID(Object obj);

    public abstract boolean usesDataFile();

    public abstract File getDataFile();

    public abstract void setDataFile(File file)
        throws IOException;

    public abstract boolean hasEditor();

    public abstract Component getEditorComponent();

    public abstract boolean commitChanges();

    public abstract boolean isModified();
}
