// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileUtils.java

package com.sitraka.deploy.util;

import HTTPClient.MD5;
import com.sitraka.deploy.common.awt.ProgressBar;
import java.applet.Applet;
import java.awt.Component;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Reader;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

// Referenced classes of package com.sitraka.deploy.util:
//            FileUtilException, Codecs, Hashcode

public class FileUtils
{

    public static final int OK = 0;
    public static final int PARAM_ERROR = 1;
    public static final int READ_ERROR = 2;
    public static final int WRITE_ERROR = 3;
    public static final int CREATE_ERROR = 4;
    public static final String DEFAULT_PREFIX = "ss";
    public static final String DEFAULT_SUFFIX = "dd.tmp";
    protected static final int BUFFER_SIZE = 32768;
    protected static final int MIN_BUFFER = 1024;
    protected static String invalid_chars = "#?!";
    protected static Random random = new Random();
    private static boolean unix = false;
    private static String osname = null;

    public FileUtils()
    {
    }

    public static String convertNameToFile(String name, char char_to_remove)
    {
        StringBuffer b = new StringBuffer();
        name = name.toLowerCase();
        int size = name.length();
        for(int i = 0; i < size; i++)
        {
            char c = name.charAt(i);
            if(char_to_remove == 0 || c != char_to_remove)
                if(c == '.')
                    b.append(File.separatorChar);
                else
                if(invalid_chars.indexOf(((int) (c))) == -1)
                    b.append(c);
        }

        return b.toString();
    }

    public static boolean createNewFile(File f)
        throws IOException
    {
        FileOutputStream fos;
        try
        {
            fos = new FileOutputStream(f);
        }
        catch(IOException e)
        {
            return false;
        }
        return true;
    }

    public static File createTempFile()
    {
        return createTempFile((String)null);
    }

    public static File createTempFile(String base_dir)
    {
        return createTempFile(base_dir, "ss", "dd.tmp");
    }

    public static File createTempFile(String base_dir, String prefix, String suffix)
    {
        if(base_dir == null)
            if(System.getProperty("deploy.tmpdir") != null)
                base_dir = System.getProperty("deploy.tmpdir");
            else
            if(System.getProperty("java.io.tmpdir") != null)
                base_dir = System.getProperty("java.io.tmpdir");
            else
            if(System.getProperty("user.home") != null)
                base_dir = System.getProperty("user.home");
            else
            if(System.getProperty("user.dir") != null)
                base_dir = System.getProperty("user.dir");
        if(base_dir == null)
            return null;
        if(base_dir.equals("/"))
            base_dir = ".";
        base_dir = makeSystemPath(base_dir);
        if(base_dir.endsWith(File.separator))
            if(base_dir.length() == 1)
                base_dir = ".";
            else
                base_dir = base_dir.substring(0, base_dir.length() - 1);
        File temp_file = null;
        do
        {
            int base = 0;
            int tempCounter = random.nextInt() & 0xffff;
            if(base_dir == null)
                temp_file = new File(prefix + Integer.toString(tempCounter) + suffix);
            else
                temp_file = new File(base_dir, prefix + Integer.toString(tempCounter) + suffix);
        } while(temp_file.exists());
        return temp_file;
    }

    public static File createTempFile(File base_dir)
    {
        return createTempFile(base_dir, "ss", "dd.tmp");
    }

    public static File createTempFile(File base_dir, String prefix, String suffix)
    {
        if(base_dir == null)
            return null;
        if(!base_dir.isDirectory())
            base_dir.mkdirs();
        return createTempFile(base_dir.toString(), prefix, suffix);
    }

    public static boolean isValidOutputFile(File target)
    {
        if(target == null)
            return false;
        if(target.exists())
        {
            if(!target.isFile() || !target.canWrite())
                return false;
        } else
        {
            File parent;
            for(parent = new File(target.getAbsolutePath()); parent != null && !parent.exists(); parent = new File(parent.getParent()));
            if(parent == null)
                return false;
            if(!parent.exists() || !parent.isDirectory() || !parent.canWrite())
                return false;
        }
        return true;
    }

    public static boolean saveStreamToFile(InputStream is, String file_name)
    {
        if(file_name == null || file_name.length() == 0)
            return false;
        else
            return saveStreamToFile(is, new File(file_name));
    }

    public static boolean saveStreamToFile(InputStream is, File output)
    {
        return saveStreamToFile(is, output, ((ProgressBar) (null)));
    }

    public static boolean saveStreamToFile(InputStream is, File output, ProgressBar meter)
    {
        return saveStreamToFile(is, output, meter, false);
    }

    public static boolean saveStreamToFile(InputStream is, File output, ProgressBar meter, boolean overwrite)
    {
        return saveStreamToFileDetailed(is, output, meter, overwrite, false) == null;
    }

    public static FileUtilException saveStreamToFileDetailed(InputStream is, File output, ProgressBar meter, boolean overwrite, boolean append)
    {
        if(is == null)
            return new FileUtilException(1, ((Exception) (null)));
        if(!overwrite && output.exists() && !append)
            return new FileUtilException(4, ((Exception) (null)));
        try
        {
            FileOutputStream out = new FileOutputStream(output.getPath(), append);
            FileUtilException result = saveStream(is, ((OutputStream) (out)), meter);
            ((OutputStream) (out)).flush();
            out.close();
            out = null;
            return result;
        }
        catch(IOException e)
        {
            return new FileUtilException(4, ((Exception) (e)));
        }
    }

    public static FileUtilException saveStreamToFileDetailed(InputStream is, FileOutputStream out, ProgressBar meter, boolean overwrite, boolean append)
    {
        if(is == null)
            return new FileUtilException(1, ((Exception) (null)));
        if(out == null)
        {
            return new FileUtilException(1, ((Exception) (null)));
        } else
        {
            FileUtilException result = saveStream(is, ((OutputStream) (out)), meter);
            return result;
        }
    }

    public static boolean isUnix()
    {
        if(osname == null)
        {
            synchronized(com.sitraka.deploy.util.FileUtils.class)
            {
                osname = System.getProperty("os.name").toLowerCase();
            }
            if(osname == null || !osname.startsWith("win"))
                unix = true;
            else
                unix = false;
        }
        return unix;
    }

    protected static boolean isBrowser(Component component)
    {
        if(component == null)
            return false;
        if(component instanceof Applet)
            return ((Applet)component).isActive();
        java.awt.Container parent = component.getParent();
        do
        {
            if(parent == null)
                return false;
            if(parent instanceof Applet)
                return ((Applet)parent).isActive();
            parent = ((Component) (parent)).getParent();
        } while(true);
    }

    public static FileUtilException saveStream(InputStream is, OutputStream os, ProgressBar meter)
    {
        if(is == null || os == null)
            return new FileUtilException(1, ((Exception) (null)));
        boolean unixBrowser = isUnix() && isBrowser(((Component) (meter)));
        byte buffer[] = new byte[32768];
        int n = -1;
        int size = 0;
        if(meter != null)
            size = meter.getValue();
        int i = 0;
        do
        {
            try
            {
                if(unixBrowser && meter != null)
                {
                    if(i % 20 == 0)
                    {
                        Thread.currentThread();
                        Thread.yield();
                    }
                    int avail = 0;
                    try
                    {
                        avail = is.available();
                    }
                    catch(IOException e)
                    {
                        break;
                    }
                    if(avail <= 0)
                    {
                        try
                        {
                            Thread.currentThread();
                            Thread.sleep(50L);
                        }
                        catch(InterruptedException ie) { }
                        i = 0;
                    }
                    try
                    {
                        avail = is.available();
                    }
                    catch(IOException e)
                    {
                        break;
                    }
                    if(avail <= 1024)
                        avail = 1024;
                    if(++i % 20 == 0)
                    {
                        System.out.print(".");
                        if(i % 80 == 0)
                            System.out.print("\n");
                    }
                    n = is.read(buffer, 0, Math.min(32768, avail));
                } else
                {
                    n = is.read(buffer, 0, 32768);
                }
            }
            catch(IOException e)
            {
                return new FileUtilException(2, ((Exception) (e)));
            }
            if(n == -1)
                break;
            try
            {
                os.write(buffer, 0, n);
            }
            catch(IOException e)
            {
                return new FileUtilException(3, ((Exception) (e)));
            }
            if(meter != null)
            {
                size += n;
                meter.setValue(size);
            }
        } while(true);
        try
        {
            os.flush();
            os.close();
        }
        catch(IOException e)
        {
            return new FileUtilException(3, ((Exception) (e)));
        }
        return null;
    }

    public static boolean saveReaderToFile(Reader reader, File file)
    {
        return saveReaderToFile(reader, file, ((ProgressBar) (null)), false);
    }

    public static boolean saveReaderToFile(Reader reader, File file, boolean overwrite)
    {
        return saveReaderToFile(reader, file, ((ProgressBar) (null)), overwrite);
    }

    public static boolean saveReaderToFile(Reader reader, String filename)
    {
        if(filename == null || filename.length() == 0)
            return false;
        else
            return saveReaderToFile(reader, new File(filename));
    }

    public static boolean saveReaderToFile(Reader reader, String filename, boolean overwrite)
    {
        if(filename == null || filename.length() == 0)
            return false;
        else
            return saveReaderToFile(reader, new File(filename), false);
    }

    public static boolean saveReaderToFile(Reader reader, File output, ProgressBar meter, boolean overwrite)
    {
        if(reader == null)
            return false;
        if(output.exists() && !overwrite)
            return false;
        try
        {
            FileWriter out = new FileWriter(output);
            char buffer[] = new char[32768];
            int n = -1;
            int size = 0;
            while((n = reader.read(buffer, 0, 32768)) != -1) 
            {
                ((OutputStreamWriter) (out)).write(buffer, 0, n);
                if(meter != null)
                {
                    size += n;
                    meter.setValue(size);
                }
            }
            ((OutputStreamWriter) (out)).flush();
            ((OutputStreamWriter) (out)).close();
            out = null;
            System.gc();
            return true;
        }
        catch(IOException e)
        {
            output.delete();
        }
        return false;
    }

    public static boolean createZip(File basedir, File zip_file)
    {
        return createZip(basedir, zip_file, ((ProgressBar) (null)));
    }

    public static boolean createZip(File basedir, File zip_file, ProgressBar meter)
    {
        if(basedir == null || !basedir.isDirectory())
            return false;
        if(zip_file == null || zip_file.exists() && !zip_file.canWrite())
            return false;
        if(!zip_file.exists())
        {
            File zip_dir = new File(zip_file.getParent());
            if(!zip_dir.exists() && !zip_dir.mkdirs() || !zip_dir.canWrite())
                return false;
        }
        FileOutputStream fos = null;
        try
        {
            fos = new FileOutputStream(zip_file);
        }
        catch(IOException ioe)
        {
            return false;
        }
        ZipOutputStream zos = new ZipOutputStream(((OutputStream) (fos)));
        addDirToZipStream(zos, basedir, "");
        try
        {
            zos.finish();
            ((FilterOutputStream) (zos)).flush();
        }
        catch(IOException ioe)
        {
            return false;
        }
        return true;
    }

    protected static void addDirToZipStream(ZipOutputStream zos, File directory, String path)
    {
        boolean status = true;
        String file_names[] = directory.list();
        for(int i = 0; i < file_names.length; i++)
        {
            File file = new File(directory, file_names[i]);
            String entry_name = path + File.separator + file_names[i];
            addToZipStream(zos, file, entry_name);
            if(file.isDirectory())
                addDirToZipStream(zos, file, entry_name);
        }

    }

    public static boolean unzipFile(File zip_file, File basedir)
    {
        return unzipFile(zip_file, basedir, ((ProgressBar) (null)));
    }

    public static boolean unzipFile(File zip_file, File basedir, ProgressBar meter)
    {
        return unzipFile(zip_file, basedir, meter, false);
    }

    public static boolean unzipFile(File zip_file, File basedir, ProgressBar meter, boolean append)
    {
        return unzipFile(zip_file, basedir, meter, append, false);
    }

    public static boolean unzipFile(File zip_file, File basedir, ProgressBar meter, boolean append, boolean verbose)
    {
        ZipInputStream zis = null;
        boolean result = false;
        long current_file_size = 0L;
        ZipFile zipFile = null;
        try
        {
            zipFile = new ZipFile(zip_file);
            FileInputStream fis = new FileInputStream(zip_file);
            zis = new ZipInputStream(((InputStream) (fis)));
            ZipEntry entry = null;
            entry = zis.getNextEntry();
            if(entry == null)
            {
                try
                {
                    zis.close();
                    fis = new FileInputStream(zip_file);
                    byte inbuffer[] = new byte[80];
                    int bytes = fis.read(inbuffer);
                    if(bytes < 0)
                    {
                        boolean flag1 = false;
                        return flag1;
                    }
                    String first = new String(inbuffer);
                    if(first.indexOf("HTTP/1") != -1)
                    {
                        int offset = 0;
                        while(bytes >= 0) 
                        {
                            int new_bytes = fis.read(inbuffer);
                            String next = new String(inbuffer);
                            String total = first + next;
                            int pos = total.indexOf("\r\n\r\n");
                            if(pos != -1)
                            {
                                offset += pos + 4;
                                break;
                            }
                            offset += bytes;
                            bytes = new_bytes;
                            first = next;
                        }
                        fis.close();
                        fis = new FileInputStream(zip_file);
                        fis.skip(offset);
                        zis = new ZipInputStream(((InputStream) (fis)));
                        entry = zis.getNextEntry();
                    }
                }
                catch(IOException ioe) { }
                if(entry == null)
                {
                    zis.close();
                    zis = null;
                    boolean flag = false;
                    return flag;
                }
            }
            String entry_name = entry.getName();
            Properties props = null;
            if(entry_name.equals("deploy.properties"))
            {
                props = new Properties();
                try
                {
                    props.load(((InputStream) (zis)));
                }
                catch(IOException e)
                {
                    boolean flag2 = false;
                    return flag2;
                }
            } else
            {
                zis.closeEntry();
                zis.close();
                fis = new FileInputStream(zip_file);
                zis = new ZipInputStream(((InputStream) (fis)));
            }
            int progress_size = 0;
            if(meter != null)
                if(props == null)
                    meter.setValues(0, 0, zipFile.size());
                else
                    meter.setValues(0, 0, (new Long(zip_file.length())).intValue());
            while((entry = zis.getNextEntry()) != null) 
            {
                entry_name = entry.getName();
                current_file_size = 0L;
                int index = 0;
                entry_name = makeInternalPath(entry_name);
                if(verbose)
                    System.out.println("Extracting: " + entry_name);
                if(!entry.isDirectory())
                    if(props == null)
                        current_file_size = entry.getSize();
                    else
                        current_file_size = matchPropertyEntry(entry_name, props);
                String filename = basedir.getAbsolutePath();
                if(!filename.endsWith(File.separator))
                    filename = filename + File.separator;
                filename = filename + entry_name;
                entry_name = makeSystemPath(entry_name);
                filename = makeSystemPath(filename);
                File output_file = new File(filename);
                String parent = output_file.getParent();
                if(parent != null)
                {
                    File p = new File(parent);
                    if(!p.exists())
                        p.mkdirs();
                }
                if(entry.isDirectory())
                {
                    if(!output_file.exists() && !output_file.mkdir())
                    {
                        boolean flag3 = false;
                        return flag3;
                    }
                } else
                {
                    FileOutputStream out = null;
                    if(!append && output_file.exists())
                    {
                        output_file.delete();
                        out = new FileOutputStream(output_file);
                    } else
                    {
                        out = new FileOutputStream(output_file.getAbsolutePath(), true);
                    }
                    FileUtilException fue = saveStream(((InputStream) (zis)), ((OutputStream) (out)), ((ProgressBar) (null)));
                    if(fue == null);
                    if(meter != null && current_file_size != 0L)
                    {
                        if(props == null)
                            progress_size++;
                        else
                            progress_size += (new Long(current_file_size)).intValue();
                        meter.setValue(progress_size);
                    }
                }
            }
            if(meter != null && props == null)
                meter.setValue(zipFile.size());
            result = true;
        }
        catch(IOException e)
        {
            result = false;
        }
        finally
        {
            try
            {
                if(zis != null)
                {
                    zis.close();
                    zis = null;
                }
                if(zipFile != null)
                {
                    zipFile.close();
                    zipFile = null;
                }
            }
            catch(IOException e) { }
            zis = null;
        }
        return result;
    }

    protected static long matchPropertyEntry(String filename, Properties props)
        throws IOException
    {
        int index = 0;
        do
        {
            String name = (String)((Hashtable) (props)).get(((Object) ("file" + index + ".name")));
            if(name == null)
                throw new IOException("Could not read verification data for " + filename);
            String parent = (String)((Hashtable) (props)).get(((Object) ("file" + index + ".parent")));
            if(parent == null)
                parent = "";
            else
                parent = parent + "/";
            name = parent + name;
            name = makeInternalPath(name);
            if(name.equals(((Object) (filename))))
            {
                String length = (String)((Hashtable) (props)).get(((Object) ("file" + index + ".size")));
                return Long.parseLong(length);
            }
            index++;
        } while(true);
    }

    public static void addToZipStream(ZipOutputStream zipper, String text, String fileName)
    {
        if(text == null)
            return;
        if(fileName == null)
            fileName = "sample.txt";
        ByteArrayInputStream bis = new ByteArrayInputStream(text.getBytes());
        addToZipStream(zipper, ((InputStream) (bis)), fileName);
    }

    public static void addToZipStream(ZipOutputStream zipper, File file, String fileName)
    {
        if(zipper == null)
            return;
        if(file != null && (!file.exists() || !file.canRead()) && (fileName == null || !fileName.endsWith("/")))
            return;
        String name_of_file = fileName != null ? fileName : file.getName();
        try
        {
            FileInputStream is = null;
            if(file == null && fileName != null && fileName.endsWith("/"))
                is = null;
            else
            if(!file.isDirectory())
                is = new FileInputStream(file);
            addToZipStream(zipper, ((InputStream) (is)), name_of_file);
        }
        catch(FileNotFoundException e) { }
    }

    public static void addToZipStream(ZipOutputStream zipper, InputStream stream, String fileName)
    {
        if(zipper == null)
            return;
        fileName = makeInternalPath(fileName);
        int BUF_SIZE = 8192;
        byte buf[] = new byte[BUF_SIZE];
        try
        {
            if(stream == null && !fileName.endsWith("/"))
                fileName = fileName + "/";
            ZipEntry entry = new ZipEntry(fileName);
            entry.setTime((new Date()).getTime());
            zipper.putNextEntry(entry);
            if(stream != null)
            {
                int i;
                while((i = stream.read(buf, 0, BUF_SIZE)) != -1) 
                    zipper.write(buf, 0, i);
                stream.close();
            }
            ((FilterOutputStream) (zipper)).flush();
            zipper.closeEntry();
        }
        catch(IOException e)
        {
            return;
        }
        finally
        {
            buf = null;
        }
    }

    public static long computeUncompressedZipSize(File file)
    {
        if(file == null || !file.canRead())
            return -1L;
        long total = 0L;
        ZipFile zipFile = null;
        try
        {
            zipFile = new ZipFile(file);
            for(Enumeration e = zipFile.entries(); e.hasMoreElements();)
            {
                ZipEntry entry = (ZipEntry)e.nextElement();
                long size = entry.getSize();
                if(size != -1L)
                    total += size;
            }

        }
        catch(IOException e)
        {
            total = -1L;
        }
        finally
        {
            try
            {
                zipFile.close();
            }
            catch(Exception e) { }
        }
        return total;
    }

    public static boolean copyDirectory(File srcDir, File destDir)
    {
        return copyDirectory(srcDir, destDir, true);
    }

    public static boolean copyDirectory(File srcDir, File destDir, boolean overwrite)
    {
        boolean ret = true;
        if(srcDir == null || destDir == null)
            return false;
        if(!srcDir.isDirectory())
            return false;
        if(!destDir.exists() && !destDir.mkdirs())
            return false;
        String fileNames[] = srcDir.list();
        for(int i = 0; i < fileNames.length; i++)
        {
            File f = new File(srcDir, fileNames[i]);
            if(f.isDirectory())
                ret = copyDirectory(f, new File(destDir, fileNames[i]));
            else
                ret = copyFiles(f, new File(destDir, fileNames[i]), overwrite);
            if(!ret)
                break;
        }

        return ret;
    }

    public static boolean deleteDirectory(File file)
    {
        if(!file.exists())
            return false;
        if(file.isFile())
            return file.delete();
        if(file.isDirectory())
        {
            String file_names[] = file.list();
            if(file_names == null)
                return false;
            for(int i = 0; i < file_names.length; i++)
            {
                File f = new File(file, file_names[i]);
                if(deleteDirectory(f));
            }

            return file.delete();
        } else
        {
            return file.delete();
        }
    }

    public static boolean copyFiles(File src, File dest)
    {
        return copyFiles(src, dest, false);
    }

    public static boolean copyFiles(File src, File dest, boolean overwrite)
    {
        if(src == null || dest == null)
            return false;
        if(!src.exists())
            return false;
        File destParent = new File(dest.getParent());
        if(destParent == null)
            return false;
        if(!destParent.exists() && !destParent.mkdirs())
            return false;
        try
        {
            FileInputStream inputStream = new FileInputStream(src);
            boolean result = saveStreamToFile(((InputStream) (inputStream)), dest, ((ProgressBar) (null)), overwrite);
            inputStream.close();
            return result;
        }
        catch(FileNotFoundException e)
        {
            return false;
        }
        catch(IOException e)
        {
            return false;
        }
    }

    public static File findFileByExtension(File startingDir, String extension)
    {
        if(startingDir == null || !startingDir.isDirectory())
            return null;
        String file_names[] = startingDir.list();
        for(int i = 0; i < file_names.length; i++)
        {
            File f = new File(startingDir, file_names[i]);
            if(f.isFile() && f.getAbsolutePath().endsWith(extension))
                return f;
            if(f.isDirectory())
            {
                File subF = findFileByExtension(f, extension);
                if(subF != null)
                    return subF;
            }
        }

        return null;
    }

    public static boolean checkHash(File f, String correct_hash)
    {
        String browser = System.getProperty("java.vendor");
        if(browser.indexOf("Netscape") >= 0)
        {
            boolean retval = false;
            if(f == null || !f.exists())
                return retval;
            try
            {
                MD5 hash = new MD5(f);
                byte b[] = hash.Final();
                byte encoded_hash[] = Codecs.base64Encode(b);
                String new_hash = new String(encoded_hash);
                if(new_hash.equals(((Object) (correct_hash))))
                    retval = true;
            }
            catch(IOException e) { }
            return retval;
        }
        String hash = Hashcode.computeHash(f);
        return hash != null && hash.equals(((Object) (correct_hash)));
    }

    public static boolean compareIgnoreCaseAndSep(String left, String right)
    {
        if(left == null || right == null)
            return false;
        char left_chars[] = left.toCharArray();
        char right_chars[] = right.toCharArray();
        if(left_chars.length != right_chars.length)
            return false;
        int len = left_chars.length;
        int index = 0;
        while(len-- > 0) 
        {
            char c1 = left_chars[index];
            char c2 = right_chars[index];
            index++;
            if(c1 != c2 && Character.toUpperCase(c1) != Character.toUpperCase(c2) && (c1 != '/' && c1 != '\\' || c2 != '/' && c2 != '\\') && Character.toLowerCase(c1) != Character.toLowerCase(c2))
                return false;
        }
        return true;
    }

    public static String unifyFileSeparator(String path)
    {
        if(path == null)
            return null;
        char orig[] = path.toCharArray();
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < orig.length; i++)
            if(i <= 0 || orig[i - 1] != '/' && orig[i - 1] != '\\' || orig[i] != '/' && orig[i] != '\\')
                sb.append(orig[i]);

        char chars[] = sb.toString().toCharArray();
        boolean changed = false;
        int length = chars.length;
        for(int i = 0; i < length; i++)
            if(chars[i] == '/' || chars[i] == '\\')
            {
                chars[i] = File.separatorChar;
                changed = true;
            }

        if(changed)
            return new String(chars);
        else
            return path;
    }

    public static String makeInternalPath(String path)
    {
        if(path == null)
        {
            return null;
        } else
        {
            path = path.replace(File.separatorChar, '/');
            path = path.replace('\\', '/');
            return path.trim();
        }
    }

    public static String makeSystemPath(String path)
    {
        if(path == null)
        {
            return null;
        } else
        {
            path = unifyFileSeparator(path);
            return path.trim();
        }
    }

    public static File[] listFiles(File directory)
    {
        if(directory == null)
            return null;
        if(!directory.isDirectory())
            directory.mkdirs();
        if(!directory.isDirectory())
            return null;
        String list[] = directory.list();
        if(list == null)
            return null;
        File files[] = new File[list.length];
        for(int i = 0; i < list.length; i++)
            files[i] = new File(directory, list[i]);

        return files;
    }

    public static boolean renameTo(File sourceDir, File destinationDir)
    {
        File filesToMove[] = listFiles(sourceDir);
        if(filesToMove == null)
            return false;
        boolean result = true;
        for(int i = 0; i < filesToMove.length; i++)
        {
            File movedFile = new File(destinationDir, filesToMove[i].getName());
            if(movedFile.exists())
                deleteDirectory(movedFile);
            boolean status = filesToMove[i].renameTo(movedFile);
            result = result && status;
        }

        return result;
    }

}
