// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileUtilException.java

package com.sitraka.deploy.util;


public class FileUtilException
{

    protected Exception exception;
    protected int statusCode;

    public FileUtilException(int code, Exception e)
    {
        statusCode = code;
        exception = e;
    }

    public int getStatusCode()
    {
        return statusCode;
    }

    public Exception getException()
    {
        return exception;
    }
}
