// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SortUtils.java

package com.sitraka.deploy.util;

import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.util:
//            SortInterface, StringCompare

public class SortUtils
{

    public SortUtils()
    {
    }

    public static String sortDelimitedString(String to_sort, char delimiter)
    {
        return sortDelimitedString(to_sort, delimiter, true);
    }

    public static String sortDelimitedString(String to_sort, char delimiter, boolean increasing)
    {
        StringTokenizer toker = new StringTokenizer(to_sort, (new Character(delimiter)).toString());
        int count = toker.countTokens();
        SortInterface v[] = new SortInterface[count];
        for(int i = 0; i < count; i++)
        {
            String next = toker.nextToken();
            StringCompare c = new StringCompare(next);
            v[i] = ((SortInterface) (c));
        }

        sortArray(v, 0, count - 1, increasing);
        StringBuffer buffer = new StringBuffer();
        for(int i = 0; i < count; i++)
        {
            buffer.append(((StringCompare)v[i]).getString());
            if(i < count - 1)
                buffer.append(delimiter);
        }

        return buffer.toString();
    }

    public static String[] sortStringArray(String to_sort[], boolean increasing, boolean case_insensitive_sort)
    {
        SortInterface v[] = new SortInterface[to_sort.length];
        for(int i = 0; i < to_sort.length; i++)
            v[i] = ((SortInterface) (new StringCompare(to_sort[i], case_insensitive_sort)));

        sortArray(v, 0, to_sort.length - 1, increasing);
        String new_array[] = new String[v.length];
        for(int i = 0; i < v.length; i++)
            new_array[i] = ((StringCompare)v[i]).getString();

        return new_array;
    }

    public static Vector sortStringVector(Vector to_sort)
    {
        return sortStringVector(to_sort, true, false);
    }

    public static Vector sortStringVector(Vector to_sort, boolean increasing, boolean case_insensitive_sort)
    {
        int count = to_sort.size();
        SortInterface array[] = new SortInterface[count];
        for(int i = 0; i < count; i++)
            array[i] = ((SortInterface) (new StringCompare((String)to_sort.elementAt(i), case_insensitive_sort)));

        sortArray(array, 0, count - 1, increasing);
        Vector result = new Vector(count);
        for(int i = 0; i < count; i++)
            result.insertElementAt(((Object) (((StringCompare)array[i]).getString())), i);

        return result;
    }

    public static Vector sortVector(Vector to_sort)
    {
        return sortVector(to_sort, true);
    }

    public static Vector sortVector(Vector to_sort, boolean increasing)
    {
        if(!(to_sort.elementAt(0) instanceof SortInterface))
            throw new IllegalArgumentException("Elements in the Vector to be sorted must implement SortInterface");
        int size = to_sort.size();
        if(size == 0 || size == 1)
            return to_sort;
        SortInterface array[] = new SortInterface[size];
        to_sort.copyInto(((Object []) (array)));
        sortArray(array, 0, size - 1, increasing);
        Vector result = new Vector(size);
        for(int i = 0; i < size; i++)
            result.insertElementAt(((Object) (array[i])), i);

        return result;
    }

    public static int insertSorted(Vector target, SortInterface object, boolean increasing)
    {
        int where = findInsertIndex(target, object, increasing);
        target.insertElementAt(((Object) (object)), where);
        return where;
    }

    public static int searchVector(Vector target, SortInterface object, boolean increasing)
    {
        if(target.size() == 0)
            return -1;
        if(target.size() == 1)
        {
            SortInterface candidate = (SortInterface)target.elementAt(0);
            return object.compareTo(((Object) (candidate)), increasing) == 0 ? 0 : -1;
        }
        int where = findInsertIndex(target, object, increasing);
        if(where < 0 || where >= target.size())
            return -1;
        SortInterface candidate = (SortInterface)target.elementAt(where);
        if(object.compareTo(((Object) (candidate)), increasing) != 0)
            return -1;
        else
            return where;
    }

    public static int findInsertIndex(Vector target, SortInterface object, boolean increasing)
    {
        int size = target.size();
        if(size == 0)
            return 0;
        if(size == 1)
        {
            SortInterface candidate = (SortInterface)target.elementAt(0);
            return object.compareTo(((Object) (candidate)), increasing) < 0 ? 0 : 1;
        }
        int left = -1;
        int right = size;
        int mid = -1;
        while(right - left > 1) 
        {
            mid = (right + left) / 2;
            SortInterface candidate = (SortInterface)target.elementAt(mid);
            int compare = object.compareTo(((Object) (candidate)), increasing);
            if(compare == 0)
                return mid;
            if(compare < 0)
                right = mid;
            else
                left = mid;
        }
        return right;
    }

    public static boolean isVectorSorted(Vector target, boolean increasing)
    {
        int size = target.size();
        if(size == 0 || size == 1)
            return true;
        SortInterface previous = null;
        for(int i = 0; i < size; i++)
        {
            SortInterface current = (SortInterface)target.elementAt(i);
            if(i == 0)
            {
                previous = current;
            } else
            {
                int value = previous.compareTo(((Object) (current)), increasing);
                if(increasing && value > 0)
                    return false;
                if(!increasing && value < 0)
                    return false;
                previous = current;
            }
        }

        return true;
    }

    protected static void sortArray(SortInterface v[], int start, int end, boolean increasing)
    {
        if(start >= end)
            return;
        int last = start;
        for(int i = start + 1; i <= end; i++)
            if(v[start].compareTo(((Object) (v[i])), increasing) > 0)
                swap(v, ++last, i);

        if(v[start].compareTo(((Object) (v[last])), increasing) > 0)
            swap(v, start, last);
        sortArray(v, start, last - 1, increasing);
        sortArray(v, last + 1, end, increasing);
    }

    private static synchronized void swap(SortInterface v[], int i, int j)
    {
        SortInterface tmp = v[i];
        v[i] = v[j];
        v[j] = tmp;
    }
}
