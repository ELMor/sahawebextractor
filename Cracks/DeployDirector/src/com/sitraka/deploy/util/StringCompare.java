// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SortUtils.java

package com.sitraka.deploy.util;


// Referenced classes of package com.sitraka.deploy.util:
//            SortInterface

class StringCompare
    implements SortInterface
{

    protected String source;
    protected String lowercase_source;
    protected boolean insensitive_sort;

    StringCompare(String _source, boolean case_insensitive_sort)
    {
        source = null;
        lowercase_source = null;
        insensitive_sort = false;
        insensitive_sort = case_insensitive_sort;
        source = _source;
    }

    StringCompare(String _source)
    {
        this(_source, false);
    }

    public String getString()
    {
        return source;
    }

    public int compareTo(Object obj, boolean increasing)
    {
        if(obj == null)
            return 0;
        String c = null;
        if(obj instanceof StringCompare)
            c = ((StringCompare)obj).getString();
        else
        if(obj instanceof String)
            c = (String)obj;
        String str = c;
        String new_source = source;
        if(insensitive_sort)
        {
            new_source = source.toLowerCase();
            str = c.toLowerCase();
        }
        if(increasing)
            return new_source.compareTo(str);
        else
            return new_source.compareTo(str) * -1;
    }
}
