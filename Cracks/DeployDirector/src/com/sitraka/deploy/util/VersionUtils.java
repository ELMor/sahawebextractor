// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   VersionUtils.java

package com.sitraka.deploy.util;

import com.sitraka.deploy.common.jclass.util.JCStringTokenizer;
import java.util.Vector;

// Referenced classes of package com.sitraka.deploy.util:
//            VersionInterface

public class VersionUtils
{

    private static final char SEPARATOR = 46;
    private static final String SEPARATOR_STRING = ".";

    public VersionUtils()
    {
    }

    public static int compareVersionNames(String one, String two)
    {
        int result = 0;
        one = one.trim();
        two = two.trim();
        if(one.equalsIgnoreCase(two))
            return result;
        one = normalizeVersion(one, two, true);
        two = normalizeVersion(two, one, true);
        JCStringTokenizer left = new JCStringTokenizer(one);
        for(JCStringTokenizer right = new JCStringTokenizer(two); left.hasMoreTokens() || right.hasMoreTokens();)
        {
            String left_string = left.nextToken('.');
            String right_string = right.nextToken('.');
            if(right_string == null && left_string != null)
            {
                result = 1;
                break;
            }
            if(left_string == null && right_string != null)
            {
                result = -1;
                break;
            }
            if(!left_string.equalsIgnoreCase(right_string))
            {
                result = compareSegment(left_string, right_string);
                break;
            }
        }

        if(result > 0)
            result = 1;
        else
        if(result < 0)
            result = -1;
        return result;
    }

    public static boolean isPointRelease(String one, String two)
    {
        one = one.trim();
        two = two.trim();
        if(one.equalsIgnoreCase(two))
            return true;
        one = normalizeVersion(one, two, false);
        two = normalizeVersion(two, one, false);
        int left_index = one.lastIndexOf(".");
        int right_index = two.lastIndexOf(".");
        if(left_index == -1 || right_index == -1)
        {
            return left_index == right_index;
        } else
        {
            String left_start = one.substring(0, left_index);
            String right_start = two.substring(0, right_index);
            return left_start.equalsIgnoreCase(right_start);
        }
    }

    protected static int compareSegment(String left, String right)
    {
        int left_c = ((int) (left.charAt(0)));
        int right_c = ((int) (right.charAt(0)));
        if(left_c == right_c)
            return 0;
        if(isDigit(left_c) && !isDigit(right_c))
            return 1;
        if(!isDigit(left_c) && isDigit(right_c))
            return -1;
        else
            return left_c - right_c;
    }

    public static boolean isDigit(int c)
    {
        return c >= 48 && c <= 57;
    }

    public static int countSegments(String version)
    {
        int result = 0;
        for(int i = 0; i < version.length(); i++)
            if(version.charAt(i) == '.')
                result++;

        return result;
    }

    public static String normalizeVersion(String version, String reference, boolean expand)
    {
        StringBuffer result = new StringBuffer();
        version = version.replace('/', '.');
        version = version.replace('|', '.');
        version = version.replace('\\', '.');
        if(expand)
        {
            char previous_char = '\0';
            for(int i = 0; i < version.length(); i++)
            {
                char c = version.charAt(i);
                if(c > '0' && c < '9')
                {
                    if(previous_char != 0 && previous_char != '.')
                        result.append('.');
                    result.append(c);
                } else
                if(c == '.')
                {
                    result.append(c);
                } else
                {
                    if(previous_char != 0 && previous_char != '.')
                        result.append('.');
                    result.append(c);
                }
                previous_char = c;
            }

        } else
        {
            result.append(version);
        }
        if(reference != null)
        {
            for(String fixed_ref = normalizeVersion(reference, ((String) (null)), expand); countSegments(result.toString()) < countSegments(fixed_ref.toString()); result.append(".0"));
        }
        return result.toString();
    }

    public static Object findMatchingVersion(String version, String extra, Vector versions)
    {
        Object result = null;
        Vector filtered = null;
        if(extra != null && extra.trim().length() > 0)
        {
            filtered = new Vector();
            int size = versions != null ? versions.size() : 0;
            for(int i = 0; i < size; i++)
            {
                VersionInterface checker = (VersionInterface)versions.elementAt(i);
                if(extra == null || extra.equalsIgnoreCase(checker.getSpecificData()))
                    filtered.addElement(((Object) (checker)));
            }

        } else
        {
            filtered = versions;
        }
        if(version.endsWith("-"))
        {
            String stripped = version.substring(0, version.length() - 1);
            result = findNextVersionMatch(stripped, filtered);
        } else
        if(version.endsWith("*"))
        {
            String stripped = version.substring(0, version.length() - 1);
            result = findLatestVersionMatch(stripped, filtered);
        } else
        if(version.endsWith("+"))
        {
            String stripped = version.substring(0, version.length() - 1);
            result = findLatestPointVersion(stripped, filtered);
        } else
        {
            result = findExactVersionMatch(version, filtered);
        }
        return result;
    }

    protected static Object findExactVersionMatch(String version, Vector versions)
    {
        int size = versions != null ? versions.size() : 0;
        for(int i = 0; i < size; i++)
        {
            VersionInterface checker = (VersionInterface)versions.elementAt(i);
            if(compareVersionNames(version, checker.getVersion()) == 0)
                return ((Object) (checker));
        }

        return ((Object) (null));
    }

    protected static Object findNextVersionMatch(String version, Vector versions)
    {
        Object exact = findExactVersionMatch(version, versions);
        if(exact != null)
            return exact;
        int size = versions != null ? versions.size() : 0;
        for(int i = 0; i < size; i++)
        {
            VersionInterface checker = (VersionInterface)versions.elementAt(i);
            if(compareVersionNames(checker.getVersion(), version) >= 0)
                return ((Object) (checker));
        }

        return ((Object) (null));
    }

    protected static Object findLatestVersionMatch(String version, Vector versions)
    {
        VersionInterface result = null;
        int size = versions != null ? versions.size() : 0;
        int i;
        for(i = 0; i < size; i++)
        {
            VersionInterface checker = (VersionInterface)versions.elementAt(i);
            if(compareVersionNames(checker.getVersion(), version) < 0)
                continue;
            result = checker;
            break;
        }

        if(result == null)
            return ((Object) (null));
        for(; i < size; i++)
        {
            VersionInterface checker = (VersionInterface)versions.elementAt(i);
            if(compareVersionNames(checker.getVersion(), result.getVersion()) >= 0)
                result = checker;
        }

        return ((Object) (result));
    }

    protected static Object findLatestPointVersion(String version, Vector versions)
    {
        VersionInterface result = null;
        int size = versions != null ? versions.size() : 0;
        int i;
        for(i = 0; i < size; i++)
        {
            VersionInterface checker = (VersionInterface)versions.elementAt(i);
            if(!isPointRelease(checker.getVersion(), version) || compareVersionNames(checker.getVersion(), version) < 0)
                continue;
            result = checker;
            break;
        }

        if(result == null)
            return ((Object) (null));
        for(; i < size; i++)
        {
            VersionInterface checker = (VersionInterface)versions.elementAt(i);
            if(isPointRelease(checker.getVersion(), version) && compareVersionNames(checker.getVersion(), result.getVersion()) > 0)
                result = checker;
        }

        return ((Object) (result));
    }
}
