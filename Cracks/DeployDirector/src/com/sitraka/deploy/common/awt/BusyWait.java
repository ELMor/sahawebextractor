// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusyWait.java

package com.sitraka.deploy.common.awt;

import com.sitraka.deploy.common.awt.resources.LocaleInfo;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.PrintStream;
import java.util.ResourceBundle;

// Referenced classes of package com.sitraka.deploy.common.awt:
//            ProgressBar

public class BusyWait extends Thread
{

    public static final String PLEASE_WAIT = "DDCAWait";
    protected static final int MAXIMUM = 100;
    protected static final int MINIMUM = 0;
    protected static final long GUI_DELAY = 250L;
    protected static final long TEXT_DELAY = 500L;
    protected static final long DEFAULT_POPUP_DELAY = 3000L;
    protected static long popupDelay = 3000L;
    protected long total_time;
    protected int delta;
    protected int count;
    protected String text;
    protected Dialog dialog;
    protected ProgressBar bar;
    protected boolean gui;
    protected boolean text_displayed;
    protected boolean startup;
    protected boolean hide;
    protected static BusyWait busyWait = null;
    protected static int callCount = 0;

    protected BusyWait()
    {
        total_time = 0L;
        delta = 1;
        count = 0;
        text = "";
        dialog = null;
        bar = null;
        gui = true;
        text_displayed = false;
        startup = true;
        hide = false;
        dialog = new Dialog(new Frame());
        dialog.setTitle(LocaleInfo.li.getString("DDCAWait"));
        ((Container) (dialog)).setLayout(((java.awt.LayoutManager) (new FlowLayout())));
        dialog.setResizable(false);
        ((Thread)this).setName("BusyWait-Thread");
        ((Thread)this).setPriority(8);
        startup = true;
        ((Thread)this).start();
        while(startup) 
        {
            try
            {
                Thread.sleep(200L);
                continue;
            }
            catch(InterruptedException e) { }
            break;
        }
    }

    public static synchronized void startWaiting(String text, boolean gui)
    {
        callCount++;
        if(busyWait == null)
            busyWait = new BusyWait();
        busyWait.setText(text);
        busyWait.setGui(gui);
        synchronized(busyWait)
        {
            ((Object) (busyWait)).notify();
        }
    }

    public static void stopWaiting()
    {
        callCount--;
        if(callCount < 0)
            callCount = 0;
    }

    public static void hide()
    {
        if(busyWait != null)
            busyWait.hideDialog();
    }

    public static void show()
    {
        if(busyWait != null)
            busyWait.showDialog();
    }

    protected void hideDialog()
    {
        hide = true;
        ((Component) (dialog)).setVisible(false);
    }

    protected void showDialog()
    {
        hide = false;
    }

    protected void setText(String text)
    {
        this.text = text;
        if(bar != null)
            ((Container) (dialog)).remove(((Component) (bar)));
        bar = new ProgressBar();
        bar.setBarColor(Color.blue);
        bar.setAutoLabel(false);
        bar.setLabelWidth(30);
        bar.setLabel(text);
        bar.setLabelPosition(2);
        bar.setMaximum(100);
        bar.setMinimum(0);
        bar.setValue(count);
        bar.setBarCount(100);
        bar.setBarSpacing(0);
        ((Container) (dialog)).add(((Component) (bar)));
        ((Window) (dialog)).pack();
        centerWindow(((Component) (dialog)));
    }

    protected void setGui(boolean gui)
    {
        this.gui = gui;
    }

    public boolean isGui()
    {
        return gui;
    }

    public void run()
    {
        synchronized(this)
        {
            do
            {
                if(callCount <= 0 || startup)
                    try
                    {
                        ((Component) (dialog)).setVisible(false);
                        total_time = 0L;
                        count = 0;
                        text_displayed = false;
                        startup = false;
                        ((Object)this).wait();
                    }
                    catch(InterruptedException e) { }
                if(gui)
                {
                    if(hide)
                    {
                        if(((Component) (dialog)).isVisible())
                            ((Component) (dialog)).setVisible(false);
                    } else
                    if(popupDelay == 0L && !((Component) (dialog)).isVisible())
                        ((Component) (dialog)).setVisible(true);
                    try
                    {
                        ((Object)this).wait(250L);
                    }
                    catch(InterruptedException e) { }
                    total_time += 250L;
                    if(total_time >= popupDelay && !hide)
                    {
                        count += delta;
                        if(!((Component) (dialog)).isVisible())
                            ((Component) (dialog)).setVisible(true);
                        if(count == 100 || count == 0)
                            delta = delta * -1;
                        bar.setValue(count);
                    }
                } else
                {
                    try
                    {
                        ((Object)this).wait(500L);
                    }
                    catch(InterruptedException e) { }
                    if(!text_displayed)
                    {
                        text_displayed = true;
                        System.out.print(text + " ");
                    }
                    if(!hide)
                        System.out.print(".");
                }
            } while(true);
        }
    }

    protected static void centerWindow(Component c)
    {
        Dimension d = c.getSize();
        Dimension size = c.getToolkit().getScreenSize();
        int left = size.width / 2 - d.width / 2;
        int top = size.height / 2 - d.height / 2;
        c.setLocation(left, top);
    }

    public static void setPopupDelay(long time)
    {
        if(time < -1L)
        {
            throw new IllegalArgumentException("setPopupDelay() time arg must be -1 or greater.");
        } else
        {
            popupDelay = time;
            return;
        }
    }

    public static long getPopupDelay()
    {
        return popupDelay;
    }

}
