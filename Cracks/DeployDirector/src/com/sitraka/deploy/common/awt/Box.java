// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Box.java

package com.sitraka.deploy.common.awt;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.Panel;

// Referenced classes of package com.sitraka.deploy.common.awt:
//            ElasticLayout

public class Box extends Panel
{

    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    private int h_instance_count;
    private int v_instance_count;
    protected int orientation;

    public Box()
    {
        this(0);
    }

    public Box(int orientation)
    {
        h_instance_count = 0;
        v_instance_count = 0;
        checkOrientation(orientation);
        if(orientation == 0)
        {
            setLayout(((LayoutManager) (new ElasticLayout(0))));
            ((Component)this).setName("HorizontalBox" + h_instance_count++);
        } else
        if(orientation == 1)
        {
            setLayout(((LayoutManager) (new ElasticLayout(1))));
            ((Component)this).setName("VerticalBox" + v_instance_count++);
        } else
        {
            throw new IllegalArgumentException("orientation must be HORIZONTAL or VERTICAL");
        }
    }

    public static Box createHorizontalBox()
    {
        return new Box(0);
    }

    public static Box createVerticalBox()
    {
        return new Box(1);
    }

    public void setOrientation(int orientation)
    {
        checkOrientation(orientation);
        if(this.orientation == orientation)
            return;
        this.orientation = orientation;
        if(orientation == 0)
            ((Container)this).setLayout(((LayoutManager) (new ElasticLayout(0))));
        else
            ((Container)this).setLayout(((LayoutManager) (new ElasticLayout(1))));
    }

    public int getOrientation()
    {
        return orientation;
    }

    public void setAlignment(int alignment)
    {
        ((ElasticLayout)((Container)this).getLayout()).setAlignment(alignment);
        ((Container)this).invalidate();
    }

    public int getAlignment()
    {
        return ((ElasticLayout)((Container)this).getLayout()).getAlignment();
    }

    public void checkOrientation(int o)
    {
        if(o != 0 && o != 1)
            throw new IllegalArgumentException("Orientation must be one of HORIZONTAL or VERTICAL");
        else
            return;
    }

    public void setLayout(LayoutManager mgr)
    {
        if(mgr instanceof ElasticLayout)
            ((Container)this).setLayout(mgr);
    }
}
