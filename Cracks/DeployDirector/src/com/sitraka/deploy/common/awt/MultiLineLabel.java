// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MultiLineLabel.java

package com.sitraka.deploy.common.awt;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Label;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.StringTokenizer;

public class MultiLineLabel extends Label
{

    protected transient Image dblbuffer_image;
    protected transient Graphics dblbuffer_image_gc;
    protected Insets insets;

    public MultiLineLabel(String text)
    {
        super(text);
        insets = new Insets(0, 0, 0, 0);
    }

    private static int computeHeight(Object value, Component comp, Font font)
    {
        if(value == null)
            return 0;
        int lines = 1;
        String val = value.toString();
        for(int i = 0; i < val.length(); i++)
            if(val.charAt(i) == '\n')
                lines++;

        return comp.getToolkit().getFontMetrics(font).getHeight() * lines;
    }

    private static int stringWidth(FontMetrics fm, Font font, String s)
    {
        return fm.stringWidth(s) + (font.isItalic() ? font.getSize() / 3 + 1 : 0);
    }

    protected int computeWidth(String text)
    {
        if(text == null)
            return 0;
        Font font = ((Component)this).getFont();
        FontMetrics fm = ((Component)this).getToolkit().getFontMetrics(font);
        StringTokenizer toker = new StringTokenizer(text, "\n");
        int longest = 0;
        while(toker.hasMoreTokens()) 
        {
            String sample = toker.nextToken();
            int current_length = stringWidth(fm, font, sample);
            if(current_length > longest)
                longest = current_length;
        }
        return longest;
    }

    public Dimension getPreferredSize()
    {
        Font f = ((Component)this).getFont();
        int width = computeWidth(((Label)this).getText());
        int height = computeHeight(((Object) (((Label)this).getText())), ((Component) (this)), f);
        return new Dimension(width, height);
    }

    public Dimension getMinimumSize()
    {
        return getPreferredSize();
    }

    protected Rectangle getDrawingArea(Rectangle rect)
    {
        rect.setBounds(insets.left, insets.top, Math.max(0, ((Component)this).getSize().width - (insets.left + insets.right)), Math.max(0, ((Component)this).getSize().height - (insets.top + insets.bottom)));
        return rect;
    }

    protected static synchronized Image createImage(Component comp, int width, int height)
    {
        width = Math.max(1, Math.min(width, comp.getToolkit().getScreenSize().width));
        height = Math.max(1, Math.min(height, comp.getToolkit().getScreenSize().height));
        return comp.createImage(width, height);
    }

    public void paint(Graphics gc)
    {
        synchronized(((Component)this).getTreeLock())
        {
            Rectangle rect = new Rectangle();
            Graphics draw_gc = gc;
            Image old_image = dblbuffer_image;
            if(gc == null || ((Component)this).getBackground() == null)
                return;
            Rectangle paint_rect = gc.getClipBounds();
            dblbuffer_image = createImage(((Component) (this)), ((Component)this).getSize().width, ((Component)this).getSize().height);
            if(dblbuffer_image == null)
                dblbuffer_image_gc = null;
            else
            if(dblbuffer_image != old_image)
                dblbuffer_image_gc = dblbuffer_image.getGraphics();
            if(gc == null)
                gc = draw_gc;
            else
                gc = dblbuffer_image_gc;
            if(paint_rect != null)
                gc.setClip(((java.awt.Shape) (paint_rect)));
            if(paint_rect == null)
            {
                paint_rect = new Rectangle(((Component)this).getSize());
                gc.setClip(((java.awt.Shape) (paint_rect)));
            }
            rect.setBounds(0, 0, ((Component)this).getSize().width, ((Component)this).getSize().height);
            gc.setColor(((Component)this).getBackground());
            gc.fillRect(0, 0, ((Component)this).getSize().width, ((Component)this).getSize().height);
            gc.setFont(((Component)this).getFont());
            gc.setColor(((Component)this).getForeground());
            getDrawingArea(rect);
            gc.clipRect(rect.x, rect.y, rect.width, rect.height);
            old_image = dblbuffer_image;
            ((Component)this).doLayout();
            String templabel = ((Label)this).getText();
            if(templabel != null)
            {
                gc.setColor(((Component)this).getForeground());
                drawLabel(gc, templabel);
            }
            if(dblbuffer_image != null)
                draw_gc.drawImage(dblbuffer_image, 0, 0, ((java.awt.image.ImageObserver) (null)));
            dblbuffer_image = old_image;
            draw_gc = null;
            paint_rect = null;
        }
    }

    protected synchronized void drawLabel(Graphics gc, String label)
    {
        Rectangle draw_rect = getDrawingArea(new Rectangle());
        if(((Label)this).getText() == null)
            return;
        int align = ((Label)this).getAlignment();
        String string = label;
        if(string == null || string.length() == 0)
            return;
        FontMetrics fm = gc.getFontMetrics();
        Font font = gc.getFont();
        int height = fm.getHeight();
        int offset = 0;
        int line_space = height - fm.getAscent();
        int y = (draw_rect.y + height) - line_space;
        int str_height = computeHeight(((Object) (string)), ((Component) (this)), font);
        if(string.indexOf('\n') != -1)
        {
            int start = 0;
            int x = draw_rect.x;
            int i;
            String s;
            while((i = string.indexOf('\n', start)) != -1) 
            {
                s = string.substring(start, i);
                if(align == 1)
                    offset = (draw_rect.width - stringWidth(fm, font, s)) / 2;
                else
                if(align == 2)
                    offset = draw_rect.width - stringWidth(fm, font, s);
                gc.drawString(s, draw_rect.x + offset, y);
                start = i + 1;
                y += height;
            }
            s = string.substring(start, string.length());
            if(align == 1)
                offset = (draw_rect.width - stringWidth(fm, font, s)) / 2;
            else
            if(align == 2)
                offset = draw_rect.width - stringWidth(fm, font, s);
            gc.drawString(s, draw_rect.x + offset, y);
        } else
        {
            if(align == 1)
                offset = (draw_rect.width - stringWidth(fm, font, string)) / 2;
            else
            if(align == 2)
                offset = draw_rect.width - stringWidth(fm, font, string);
            gc.drawString(string, draw_rect.x + offset, y);
        }
    }
}
