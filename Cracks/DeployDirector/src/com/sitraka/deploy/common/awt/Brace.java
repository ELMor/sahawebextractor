// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Brace.java

package com.sitraka.deploy.common.awt;

import java.awt.Component;
import java.awt.Dimension;

public class Brace extends Component
{

    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    protected int orientation;
    protected Dimension minimumSize;
    protected Dimension preferredSize;
    protected Dimension maximumSize;

    public Brace()
    {
        minimumSize = new Dimension(10, 0);
        preferredSize = new Dimension(10, 0);
        maximumSize = new Dimension(10, 32767);
    }

    public Brace(Dimension minimum_size, Dimension preferred_size, Dimension maximum_size)
    {
        minimumSize = minimum_size;
        preferredSize = preferred_size;
        maximumSize = maximum_size;
    }

    public Brace(int length, int orientation)
    {
        this.orientation = orientation;
        minimumSize = new Dimension();
        preferredSize = new Dimension();
        maximumSize = new Dimension();
        setLength(length);
    }

    public static Brace createHorizontalBrace(int width)
    {
        return new Brace(width, 0);
    }

    public static Brace createVerticalBrace(int height)
    {
        return new Brace(height, 1);
    }

    public int getOrientation()
    {
        return orientation;
    }

    public void setOrientation(int orientation)
    {
        if(this.orientation == orientation)
            return;
        if(orientation == 0 || orientation == 1)
        {
            int length = getLength();
            this.orientation = orientation;
            setLength(length);
        }
    }

    public int getLength()
    {
        if(orientation == 0)
            return minimumSize.width;
        if(orientation == 1)
            return minimumSize.height;
        else
            return 0;
    }

    public void setLength(int length)
    {
        if(orientation == 0)
        {
            minimumSize.setSize(length, 0);
            preferredSize.setSize(length, 0);
            maximumSize.setSize(length, 32767);
        } else
        if(orientation == 1)
        {
            minimumSize.setSize(0, length);
            preferredSize.setSize(0, length);
            maximumSize.setSize(32767, length);
        }
    }

    public Dimension getMinimumSize()
    {
        return minimumSize;
    }

    public Dimension getPreferredSize()
    {
        return preferredSize;
    }

    public Dimension getMaximumSize()
    {
        return maximumSize;
    }
}
