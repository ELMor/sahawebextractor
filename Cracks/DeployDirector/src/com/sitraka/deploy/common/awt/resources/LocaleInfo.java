// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo.java

package com.sitraka.deploy.common.awt.resources;

import java.util.ListResourceBundle;
import java.util.ResourceBundle;

public class LocaleInfo extends ListResourceBundle
{

    public static ResourceBundle li = ResourceBundle.getBundle("com.sitraka.deploy.common.awt.resources.LocaleInfo");
    public static final String CANCEL = "DDCACancel";
    public static final String OK = "DDCAOk";
    static final Object strings[][] = {
        {
            "DDCACancel", "Cancel"
        }, {
            "DDCAOk", "OK"
        }, {
            "DDTimeoutError", "Timeout"
        }, {
            "DDTimeoutMessage", "Unable to connect to any server, tried: {0}"
        }, {
            "DDCADone", "Done"
        }, {
            "DDCAWait", "Please Wait ..."
        }, {
            "DDCAInstallTo", "Install To:"
        }, {
            "DDCACurrDir", "Current selected directory:"
        }, {
            "DDCAInstallDir", "Install dir: {0}"
        }, {
            "DDCAError", "Error"
        }, {
            "DDCAMessage", "Message"
        }, {
            "DDCANo", "No"
        }, {
            "DDCAQuestion", "Question"
        }, {
            "DDCAWarning", "Warning"
        }, {
            "DDCAYes", "Yes"
        }, {
            "Finish", "Finish"
        }, {
            "Help", "Help"
        }, {
            "Next", "Next"
        }, {
            "Previous", "Previous"
        }
    };

    public LocaleInfo()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
