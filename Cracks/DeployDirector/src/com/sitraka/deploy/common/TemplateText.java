// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TemplateText.java

package com.sitraka.deploy.common;

import java.io.BufferedReader;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.util.Vector;

public class TemplateText
{
    class PlaceHolder
    {

        String name;
        int textIndex;
        String precedingText;
        String textToInsert;

        public boolean equals(Object obj)
        {
            return (obj instanceof PlaceHolder) && name.equals(((Object) (((PlaceHolder)obj).name)));
        }

        PlaceHolder(String name, String precedingText)
        {
            this.name = name;
            this.precedingText = precedingText;
        }
    }


    private Vector placeHolders;
    private String fileName;
    private String endingText;
    private long lastModified;

    public TemplateText(InputStream is)
        throws IOException
    {
        fileName = null;
        lastModified = System.currentTimeMillis();
        if(is == null)
        {
            throw new IOException("InputStream cannot be null");
        } else
        {
            BufferedReader br = new BufferedReader(((java.io.Reader) (new InputStreamReader(is))));
            readData(br);
            return;
        }
    }

    public TemplateText(String fileName)
        throws IOException
    {
        this.fileName = fileName;
        readFile();
    }

    public TemplateText(File file)
        throws IOException
    {
        this(file.getAbsolutePath());
    }

    public void setPlaceHolder(String name, Object value)
    {
        PlaceHolder ph = new PlaceHolder(name, ((String) (null)));
        for(int i = -1; (i = placeHolders.indexOf(((Object) (ph)), i + 1)) != -1;)
        {
            ph = (PlaceHolder)placeHolders.elementAt(i);
            ph.textToInsert = value.toString();
        }

    }

    public void reset()
        throws IOException
    {
        File file = new File(fileName);
        if(lastModified < file.lastModified())
        {
            readFile();
        } else
        {
            for(int i = 0; i < placeHolders.size(); i++)
            {
                PlaceHolder ph = (PlaceHolder)placeHolders.elementAt(i);
                ph.textToInsert = null;
            }

        }
    }

    public void print(PrintWriter out)
    {
        for(int i = 0; i < placeHolders.size(); i++)
        {
            PlaceHolder ph = (PlaceHolder)placeHolders.elementAt(i);
            if(ph.precedingText != null)
                out.print(ph.precedingText);
            if(ph.textToInsert != null)
                out.print(ph.textToInsert);
        }

        out.print(endingText);
        out.flush();
    }

    public String toString()
    {
        CharArrayWriter writer = new CharArrayWriter();
        print(new PrintWriter(((java.io.Writer) (writer))));
        return writer.toString();
    }

    private void readFile()
        throws IOException
    {
        File file = new File(fileName);
        lastModified = file.lastModified();
        BufferedReader br = new BufferedReader(((java.io.Reader) (new FileReader(file))));
        readData(br);
        br.close();
    }

    private void readData(BufferedReader br)
        throws IOException
    {
        placeHolders = new Vector();
        StringBuffer strBuf = new StringBuffer();
        String line = null;
        for(boolean foundPlaceHolder = false; foundPlaceHolder || (line = br.readLine()) != null;)
        {
            foundPlaceHolder = false;
            int index = 0;
            if((index = line.indexOf("<!--PlaceHolder")) != -1)
            {
                foundPlaceHolder = true;
                strBuf.append(line.substring(0, index));
                String s = line.substring(index, line.length());
                int endIndex = s.indexOf("-->");
                line = s.substring(endIndex + 3, s.length());
                String name = s.substring(0, endIndex);
                StringTokenizer toker = new StringTokenizer(name, ":");
                toker.nextToken();
                name = toker.nextToken();
                placeHolders.addElement(((Object) (new PlaceHolder(name, strBuf.toString()))));
                strBuf = new StringBuffer();
            } else
            {
                strBuf.append(line).append("\n");
            }
        }

        endingText = strBuf.toString();
    }
}
