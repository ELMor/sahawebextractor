// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ShortCut.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            AppFile, CommonInterface

public class ShortCut
    implements CommonInterface
{

    public static final String TYPE_JAVA = "java";
    public static final String TYPE_FILE = "file";
    public static final String TYPE_UPDATE = "update";
    public static final String TYPE_UNINSTALL = "uninstall";
    public static final String TYPES[] = {
        "java", "file", "update", "uninstall"
    };
    protected String type;
    protected String name;
    protected String link;
    protected String winicon;
    protected String winiconindex;
    protected String macicon;
    protected String desktop;

    public ShortCut()
    {
        type = null;
        name = null;
        link = null;
        winicon = null;
        winiconindex = "0";
        macicon = null;
        desktop = "false";
        type = "java";
        name = "";
        link = "";
        winicon = "";
        macicon = "";
    }

    public ShortCut(Node shortcut_node)
        throws XmlException
    {
        type = null;
        name = null;
        link = null;
        winicon = null;
        winiconindex = "0";
        macicon = null;
        desktop = "false";
        type = XmlSupport.getAttribute(shortcut_node, "TYPE");
        if(type == null)
            throw new XmlException("XML Error: TYPE is missing in SHORTCUT tag");
        name = XmlSupport.getAttribute(shortcut_node, "NAME");
        setLink(XmlSupport.getAttribute(shortcut_node, "LINK"));
        setWinIcon(XmlSupport.getAttribute(shortcut_node, "WINICON"));
        winiconindex = XmlSupport.getAttribute(shortcut_node, "WINICONINDEX");
        setMacIcon(XmlSupport.getAttribute(shortcut_node, "MACICON"));
        desktop = XmlSupport.getAttribute(shortcut_node, "DESKTOP");
        if(winiconindex == null)
            winiconindex = "0";
        if(desktop == null)
            desktop = "false";
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setLink(String link)
    {
        this.link = FileUtils.makeInternalPath(link);
    }

    public String getLink()
    {
        return link;
    }

    public void setDesktop(String desktop)
    {
        this.desktop = desktop;
    }

    public String getDesktop()
    {
        return desktop;
    }

    public void setWinIcon(String winIcon)
    {
        winicon = FileUtils.makeInternalPath(winIcon);
    }

    public String getwinIcon()
    {
        return winicon;
    }

    public void setWinIconIndex(String winIconIndex)
    {
        if(winIconIndex == null)
            winiconindex = "0";
        else
            winiconindex = winIconIndex;
    }

    public void setWinIconIndex(int index)
    {
        if(index < 0)
            winiconindex = "0";
        else
            winiconindex = Integer.toString(index);
    }

    public String getWinIconIndexString()
    {
        return winiconindex;
    }

    public int getWinIconIndex()
    {
        return Integer.decode(winiconindex).intValue();
    }

    public void setMacIcon(String macIcon)
    {
        macicon = FileUtils.makeInternalPath(macIcon);
    }

    public String getMacIcon()
    {
        return macicon;
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof ShortCut))
            return false;
        ShortCut other = (ShortCut)obj;
        String otherPropFile = other.getPropertiesFile(-1, ((String) (null)));
        String ourPropFile = getPropertiesFile(-1, ((String) (null)));
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public boolean isChanged(Object obj)
    {
        if(!(obj instanceof AppFile))
            return true;
        if(equals(obj))
            return false;
        ShortCut other = (ShortCut)obj;
        return name.equalsIgnoreCase(other.name) && !type.equalsIgnoreCase(other.type);
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        String my_prefix = prefix != null ? prefix + "." : "";
        String my_base = base != -1 ? "" + base : "";
        sb.append(my_prefix + "shortcut" + my_base + ".type=" + type + "\n");
        sb.append(my_prefix + "shortcut" + my_base + ".name=" + name + "\n");
        if(link != null)
            sb.append(my_prefix + "shortcut" + my_base + ".link=" + link + "\n");
        if(winicon != null)
            sb.append(my_prefix + "shortcut" + my_base + ".winicon=" + winicon + "\n");
        if(winiconindex != null)
            sb.append(my_prefix + "shortcut" + my_base + ".winiconindex=" + winiconindex + "\n");
        if(macicon != null)
            sb.append(my_prefix + "shortcut" + my_base + ".macicon=" + macicon + "\n");
        sb.append(my_prefix + "shortcut" + my_base + ".desktop=" + desktop + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<SHORTCUT");
        sb.append(" NAME=\"" + XmlSupport.toCDATA(name) + "\"");
        sb.append(" TYPE=\"" + XmlSupport.toCDATA(type) + "\"");
        if(link != null)
            sb.append(" LINK=\"" + XmlSupport.toCDATA(link) + "\"");
        if(winicon != null)
            sb.append(" WINICON=\"" + XmlSupport.toCDATA(winicon) + "\"");
        if(winiconindex != null)
            sb.append(" WINICONINDEX=\"" + XmlSupport.toCDATA(winiconindex) + "\"");
        if(macicon != null)
            sb.append(" MACICON=\"" + XmlSupport.toCDATA(macicon) + "\"");
        sb.append(" DESKTOP=\"" + XmlSupport.toCDATA(desktop) + "\"");
        sb.append("/>\n");
        return sb.toString();
    }

}
