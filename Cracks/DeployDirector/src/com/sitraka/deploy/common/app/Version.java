// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Version.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.PropertyUtils;
import com.sitraka.deploy.util.SortUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import org.apache.xerces.parsers.AbstractDOMParser;
import org.apache.xerces.parsers.DOMParser;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.common.app:
//            InstallData, ConnectionPolicy, UpdatePolicy, AccessControl, 
//            Platform, AppFile, CommonInterface, VersionDTD, 
//            JRE

public class Version
{

    protected String name;
    protected boolean nameKnown;
    protected String id;
    protected boolean serverVersion;
    protected boolean deleted;
    protected ConnectionPolicy connectionPolicy;
    protected AccessControl accessControl;
    protected UpdatePolicy updatePolicy;
    protected InstallData installData;
    protected Platform platform;
    protected String description;
    protected String platformIdentifier;
    protected Date creationDate;
    protected boolean descriptionLoaded;
    protected File sourceXML;
    protected boolean xmlParsed;

    public Version()
    {
        nameKnown = false;
        serverVersion = false;
        deleted = false;
        accessControl = null;
        installData = null;
        description = null;
        platformIdentifier = "";
        descriptionLoaded = false;
        sourceXML = null;
        xmlParsed = false;
        installData = new InstallData();
        connectionPolicy = new ConnectionPolicy();
        updatePolicy = new UpdatePolicy();
        accessControl = new AccessControl();
        platform = new Platform();
        platform.setName("All");
        creationDate = Calendar.getInstance().getTime();
    }

    public Version(File xmlfile)
        throws IOException, XmlException
    {
        nameKnown = false;
        serverVersion = false;
        deleted = false;
        accessControl = null;
        installData = null;
        description = null;
        platformIdentifier = "";
        descriptionLoaded = false;
        sourceXML = null;
        xmlParsed = false;
        sourceXML = xmlfile;
        if(!sourceXML.exists())
            throw new IOException("File Not Found: " + sourceXML.getAbsolutePath());
        else
            return;
    }

    public Version(File xmlfile, String name)
        throws IOException, XmlException
    {
        this(xmlfile);
        this.name = name;
        nameKnown = true;
    }

    public Version(InputSource xml_source)
        throws IOException, XmlException
    {
        nameKnown = false;
        serverVersion = false;
        deleted = false;
        accessControl = null;
        installData = null;
        description = null;
        platformIdentifier = "";
        descriptionLoaded = false;
        sourceXML = null;
        xmlParsed = false;
        parseXML(xml_source);
    }

    public Version(InputStream stream)
        throws IOException, XmlException
    {
        nameKnown = false;
        serverVersion = false;
        deleted = false;
        accessControl = null;
        installData = null;
        description = null;
        platformIdentifier = "";
        descriptionLoaded = false;
        sourceXML = null;
        xmlParsed = false;
        parseXML(stream);
    }

    public Version(String name)
    {
        nameKnown = false;
        serverVersion = false;
        deleted = false;
        accessControl = null;
        installData = null;
        description = null;
        platformIdentifier = "";
        descriptionLoaded = false;
        sourceXML = null;
        xmlParsed = false;
        this.name = name;
        nameKnown = true;
    }

    public void parseXML(InputStream stream)
        throws IOException, XmlException
    {
        InputSource source = XmlSupport.createInputSource(stream);
        source.setSystemId("file:/" + VersionDTD.getDTDName());
        parseXML(source);
    }

    public void parseXML(InputSource xml_source)
        throws IOException, XmlException
    {
        org.w3c.dom.Document xml = null;
        DOMParser p = null;
        try
        {
            DOMParser parser = new DOMParser();
            parser.setFeature("http://xml.org/sax/features/validation", true);
            parser.setFeature("http://apache.org/xml/features/dom/include-ignorable-whitespace", false);
            parser.setErrorHandler(((org.xml.sax.ErrorHandler) (new com.sitraka.deploy.common.XmlSupport.ErrorPrinter())));
            parser.setEntityResolver(((org.xml.sax.EntityResolver) (new com.sitraka.deploy.common.XmlSupport.DTDStringResolver(VersionDTD.getDTD(), VersionDTD.getDTDName()))));
            parser.parse(xml_source);
            xml = ((AbstractDOMParser) (parser)).getDocument();
        }
        catch(SAXException e)
        {
            throw new XmlException(e.getMessage());
        }
        catch(IOException e)
        {
            throw e;
        }
        if(xml == null)
            throw new XmlException("XML Error: Internal error, unable to create XmlDocument object for version");
        org.w3c.dom.traversal.TreeWalker treeWalker = XmlSupport.createTreeWalker(((org.w3c.dom.Node) (xml)));
        org.w3c.dom.Node version_node = XmlSupport.getNextElementNamed(treeWalker, "VERSION");
        name = XmlSupport.getAttribute(version_node, "NAME");
        if(name == null)
            throw new XmlException("XML Error: Version NAME is missing");
        id = XmlSupport.getAttribute(version_node, "ID");
        if(id == null)
            throw new XmlException("XML Error: Version ID is missing");
        String dateString = XmlSupport.getAttribute(version_node, "CREATIONDATE");
        if(dateString == null)
            throw new XmlException("Xml Error: Version CREATIONDATE is missing");
        long date = PropertyUtils.parseLongDate(dateString);
        if(date == 0L)
            throw new XmlException("Xml Error: Version CREATIONDATE cannot be parsed from '" + dateString + "'");
        creationDate = new Date(date);
        platformIdentifier = XmlSupport.getAttribute(version_node, "PLATFORMIDENTIFIER");
        org.w3c.dom.Node node = null;
        node = XmlSupport.getNextElementNamed(treeWalker, "INSTALLDATA");
        if(node == null)
            throw new XmlException("XML Error: Version '" + name + "' is missing INSTALLDATA tag");
        installData = new InstallData(node);
        node = XmlSupport.getNextElementNamed(treeWalker, "CONNECTION");
        if(node == null)
            throw new XmlException("XML Error: Version '" + name + "' is missing CONNECTION tag");
        connectionPolicy = new ConnectionPolicy(node);
        node = XmlSupport.getNextElementNamed(treeWalker, "UPDATE");
        if(node == null)
            throw new XmlException("XML Error: Version '" + name + "' is missing UPDATE tag");
        updatePolicy = new UpdatePolicy(node);
        node = XmlSupport.getNextElementNamed(treeWalker, "ACCESS");
        if(node == null)
            throw new XmlException("XML Error: Version '" + name + "' is missing ACCESS tag");
        accessControl = new AccessControl(node);
        node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM");
        if(node == null)
            throw new XmlException("XML Error: Version '" + name + "' is missing PLATFORM tag");
        platform = new Platform(node, ((Platform) (null)), sourceXML);
        node = XmlSupport.getNextElementNamed(treeWalker, "DESCRIPTION");
        if(node != null)
            description = XmlSupport.getTagValue(node);
        xmlParsed = true;
        descriptionLoaded = true;
    }

    protected void loadDescription()
    {
        if(!xmlParsed && !descriptionLoaded && sourceXML != null && sourceXML.exists())
            try
            {
                String descriptionOpenTag = "<DESCRIPTION>";
                String descriptionCloseTag = "</DESCRIPTION>";
                FileInputStream fis = new FileInputStream(sourceXML);
                InputStreamReader isr = new InputStreamReader(((InputStream) (fis)), "UTF8");
                BufferedReader br = new BufferedReader(((java.io.Reader) (isr)));
                int startIndex = -1;
                String line;
                for(line = br.readLine(); line != null; line = br.readLine())
                {
                    startIndex = line.indexOf(descriptionOpenTag);
                    if(startIndex != -1)
                        break;
                }

                if(startIndex == -1)
                {
                    description = "";
                    descriptionLoaded = true;
                    return;
                }
                startIndex += descriptionOpenTag.length();
                int endIndex = line.indexOf(descriptionCloseTag, startIndex);
                if(endIndex != -1)
                {
                    description = XmlSupport.fromCDATA(line.substring(startIndex, endIndex));
                    descriptionLoaded = true;
                    return;
                }
                StringBuffer descBuffer = new StringBuffer(1000);
                descBuffer.append(XmlSupport.fromCDATA(line.substring(startIndex, line.length())));
                descBuffer.append("\n");
                for(line = br.readLine(); line != null; line = br.readLine())
                {
                    endIndex = line.indexOf(descriptionCloseTag);
                    if(endIndex != -1)
                        break;
                    descBuffer.append(XmlSupport.fromCDATA(line));
                    descBuffer.append("\n");
                }

                if(endIndex == -1)
                {
                    description = descBuffer.toString();
                    descriptionLoaded = true;
                    return;
                } else
                {
                    descBuffer.append(XmlSupport.fromCDATA(line.substring(0, endIndex)));
                    description = descBuffer.toString();
                    descriptionLoaded = true;
                    return;
                }
            }
            catch(IOException e) { }
    }

    public void ensureXMLIsParsed()
    {
        try
        {
            if(!xmlParsed && sourceXML != null && sourceXML.exists())
                parseXML(XmlSupport.createInputSource(sourceXML));
        }
        catch(IOException ioe)
        {
            ((Throwable) (ioe)).printStackTrace();
        }
        catch(XmlException xmle)
        {
            ((Throwable) (xmle)).printStackTrace();
        }
    }

    public void setXMLFile(File f)
    {
        sourceXML = f;
        xmlParsed = false;
        descriptionLoaded = false;
    }

    public File getXMLFile()
    {
        return sourceXML;
    }

    public void setName(String name)
    {
        this.name = name;
        nameKnown = true;
    }

    public String getName()
    {
        if(!nameKnown)
            ensureXMLIsParsed();
        return name;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    public String getID()
    {
        ensureXMLIsParsed();
        return id;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public Date getCreationDate()
    {
        ensureXMLIsParsed();
        return creationDate;
    }

    public void setPlatformIdentifier(String identifier)
    {
        platformIdentifier = identifier;
    }

    public String getPlatformIdentifier()
    {
        ensureXMLIsParsed();
        return platformIdentifier;
    }

    public void setDescription(String description)
    {
        descriptionLoaded = true;
        this.description = description;
    }

    public String getDescription()
    {
        loadDescription();
        return description;
    }

    public void setServerVersion(boolean serverVersion)
    {
        this.serverVersion = serverVersion;
    }

    public boolean isServerVersion()
    {
        return serverVersion;
    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public Vector getFiles(String platformName)
    {
        ensureXMLIsParsed();
        Vector result = new Vector();
        if(platform != null)
            platform.getFiles(platformName != null ? platformName.toLowerCase() : "all", result);
        return result;
    }

    public AppFile findAppFile(String platform_name, String parent_file, String sub_file)
    {
        ensureXMLIsParsed();
        if(platform != null)
            return platform.findAppFile(platform_name, parent_file, sub_file);
        else
            return null;
    }

    public boolean hasFile(String filename)
    {
        ensureXMLIsParsed();
        if(platform != null)
            return platform.hasFile(filename);
        else
            return false;
    }

    public String getPathToFile(String filename, String platformName)
    {
        ensureXMLIsParsed();
        if(filename == null)
            return null;
        if(platformName == null)
            platformName = "all";
        if(!platform.hasFile(filename))
            return null;
        String path = platform.getPathToFile(filename, platformName);
        if(path == null)
        {
            return null;
        } else
        {
            String filepath = name.toLowerCase() + File.separator + path;
            return filepath;
        }
    }

    public Platform getPlatform()
    {
        ensureXMLIsParsed();
        return platform;
    }

    public void setPlatform(Platform platform)
    {
        this.platform = platform;
    }

    public Vector listPlatforms()
    {
        ensureXMLIsParsed();
        if(platform == null)
        {
            return null;
        } else
        {
            Vector list = platform.listPlatforms();
            list.insertElementAt(((Object) (platform)), 0);
            return list;
        }
    }

    public InstallData getInstallData()
    {
        ensureXMLIsParsed();
        return installData;
    }

    public void setInstallData(InstallData id)
    {
        installData = id;
    }

    public ConnectionPolicy getConnectionPolicy()
    {
        ensureXMLIsParsed();
        return connectionPolicy;
    }

    public UpdatePolicy getUpdatePolicy()
    {
        ensureXMLIsParsed();
        return updatePolicy;
    }

    public AccessControl getAccessPolicy()
    {
        ensureXMLIsParsed();
        return accessControl;
    }

    public String getLicenseFilename()
    {
        ensureXMLIsParsed();
        return installData.getLicenseFilename();
    }

    public String getInstallProperties(String platformName)
    {
        ensureXMLIsParsed();
        StringBuffer content = new StringBuffer();
        content.append("# Application version:\n");
        content.append("application.version=" + name + "\n\n");
        JRE jre = getJRE(platformName);
        if(jre != null)
            content.append(jre.getPropertiesFile());
        AccessControl ac = getAccessPolicy();
        if(ac != null)
        {
            content.append("# Access Policy information:\n");
            content.append(ac.getPropertiesFile() + "\n");
        }
        content.append("# Entry points:\n");
        getPlatform().getEntryPoints(platformName, content, 0);
        content.append(installData.getPropertiesFile() + "\n");
        String installDir = getPlatform().getInstallDir();
        installDir = getPlatform().getAvailableInstallDir(platformName.toLowerCase(), installDir);
        if(installDir != null)
        {
            content.append("#Platform Install Directory:\n");
            content.append("install.dir=" + installDir + "\n");
        }
        return content.toString();
    }

    public String getAppProperties(String platformName)
    {
        ensureXMLIsParsed();
        if(platformName == null)
            platformName = "";
        StringBuffer content = new StringBuffer();
        content.append("# Application version:\n");
        content.append("version=" + name + "\n\n");
        content.append("# Connection policy:\n");
        content.append(getConnectionPolicy() + "\n\n");
        content.append("# Update policy:\n");
        content.append(getUpdatePolicy() + "\n\n");
        content.append("# Entry points:\n");
        getPlatform().getEntryPoints(platformName, content, 0);
        String installDir = getPlatform().getInstallDir();
        installDir = getPlatform().getAvailableInstallDir(platformName.toLowerCase(), installDir);
        if(installDir != null)
        {
            content.append("#Platform Install Directory:\n");
            content.append("install.dir=" + installDir + "\n\n");
        }
        content.append("# Required JAVA versions:\n");
        JRE jre = getPlatform().getJRE(platformName);
        if(jre != null)
            content.append(jre.getPropertiesFile());
        content.append("\n\n");
        content.append("# Classpath additions:\n");
        getPlatform().getClassPaths(platformName, content, 0);
        content.append("\n\n");
        if(installData != null)
            content.append(installData.getPropertiesFile());
        content.append("\n\n");
        content.append("# Authorization and Authentication:\n");
        if(accessControl != null)
            content.append(accessControl.getPropertiesFile());
        content.append("\n\n");
        return content.toString();
    }

    public JRE getJRE(String platformName)
    {
        ensureXMLIsParsed();
        if(platform != null)
            return platform.getJRE(platformName);
        else
            return null;
    }

    public boolean computeFileHashAndSize(File location)
    {
        ensureXMLIsParsed();
        File baseDir = new File(location + File.separator + name.toLowerCase());
        return recurseHashAndSize(platform, baseDir);
    }

    protected boolean recurseHashAndSize(Platform platform, File baseDir)
    {
        File platformBase = new File(baseDir + File.separator + FileUtils.convertNameToFile(platform.getFullName(), '\0'));
        Vector fileList = platform.listFiles();
        int size = fileList.size();
        for(int i = 0; i < size; i++)
        {
            AppFile appFile = (AppFile)fileList.elementAt(i);
            File file = new File(platformBase + File.separator + appFile.getName());
            if(appFile.isDirectory())
            {
                appFile.setDirectory(true);
                appFile.setHash("");
                appFile.setSize(new Long(0L));
            } else
            {
                if(!file.exists())
                    return false;
                appFile.setDirectory(false);
                appFile.setHash(Hashcode.computeHash(file));
                appFile.setSize(new Long(file.length()));
                if(appFile.isArchive())
                    appFile.expandJarFile(file);
            }
        }

        Vector platformList = platform.getPlatforms();
        size = platformList.size();
        for(int i = 0; i < size; i++)
            if(!recurseHashAndSize((Platform)platformList.elementAt(i), baseDir))
                return false;

        return true;
    }

    public String computeInstallPropDifferenceFrom(Version other, String platformName)
    {
        ensureXMLIsParsed();
        InstallData otherData = other.getInstallData();
        StringBuffer sb = new StringBuffer();
        sb.append("#\n# Install Properties that we always include\n#\n\n");
        Platform otherPlatform = other.getPlatform();
        JRE jre = otherPlatform.getJRE(platformName);
        if(jre != null)
            sb.append(jre.getPropertiesFile());
        sb.append("#\n# New Entry points\n#\n\n");
        otherPlatform.getEntryPoints(platformName, sb, 0);
        sb.append("#\n# Old Entry points\n#\n\n");
        getPlatform().getEntryPoints(platformName, sb, 0, "remove.entry");
        sb.append("#\n# Install Properties differences from version " + name);
        sb.append(" to version " + other.getName() + "\n#\n\n");
        String diff = installData.differenceFrom(other.getInstallData());
        sb.append(diff != null ? diff : "");
        return sb.toString();
    }

    public String computeDifferenceFrom(Version other, String platformName)
    {
        ensureXMLIsParsed();
        if(platformName == null)
            platformName = "all";
        Platform otherPlatform = other.getPlatform();
        Vector our_files = new Vector();
        Vector their_files = new Vector();
        platform.getFiles(platformName, our_files);
        otherPlatform.getFiles(platformName, their_files);
        Vector our_expanded_files = new Vector();
        for(int i = 0; i < our_files.size(); i++)
        {
            AppFile f = (AppFile)our_files.elementAt(i);
            int index = SortUtils.searchVector(their_files, ((com.sitraka.deploy.util.SortInterface) (f)), true);
            if(f.isCompoundObject() && index != -1)
                f.expandInto(our_expanded_files);
            else
                SortUtils.insertSorted(our_expanded_files, ((com.sitraka.deploy.util.SortInterface) (f)), true);
        }

        Vector their_expanded_files = new Vector();
        for(int i = 0; i < their_files.size(); i++)
        {
            AppFile f = (AppFile)their_files.elementAt(i);
            int index = SortUtils.searchVector(our_files, ((com.sitraka.deploy.util.SortInterface) (f)), true);
            if(f.isCompoundObject() && index != -1)
                f.expandInto(their_expanded_files);
            else
                SortUtils.insertSorted(their_expanded_files, ((com.sitraka.deploy.util.SortInterface) (f)), true);
        }

        StringBuffer sb = new StringBuffer();
        sb.append(differenceToString(our_expanded_files, their_expanded_files));
        return sb.toString();
    }

    protected static Vector computeDifferences(Vector from, Vector to)
    {
        Vector unchanged = new Vector();
        Vector added = new Vector();
        Vector deleted = new Vector();
        Vector changed = new Vector();
        Vector result = new Vector();
        result.addElement(((Object) (changed)));
        result.addElement(((Object) (added)));
        result.addElement(((Object) (deleted)));
        if(from == null)
        {
            int size = from != null ? from.size() : 0;
            for(int i = 0; i < size; i++)
                added.addElement(to.elementAt(i));

        } else
        if(to == null)
        {
            int size = to != null ? to.size() : 0;
            for(int i = 0; i < size; i++)
                deleted.addElement(from.elementAt(i));

        } else
        {
            Vector from_files = (Vector)from.clone();
            Vector to_files = (Vector)to.clone();
            for(int i = 0; i < from_files.size(); i++)
            {
                Object our_obj = from_files.elementAt(i);
                if(!(our_obj instanceof AppFile))
                {
                    if(to_files.removeElement(our_obj))
                    {
                        from_files.removeElementAt(i);
                        unchanged.addElement(our_obj);
                        i--;
                    }
                } else
                {
                    int index = SortUtils.searchVector(to_files, ((com.sitraka.deploy.util.SortInterface) ((AppFile)our_obj)), true);
                    if(index != -1)
                    {
                        AppFile sample = (AppFile)to_files.elementAt(index);
                        if(sample.isEqualFile(our_obj))
                        {
                            unchanged.addElement(our_obj);
                            from_files.removeElementAt(i);
                            to_files.removeElementAt(index);
                            i--;
                        }
                    }
                }
            }

            for(int i = 0; i < from_files.size(); i++)
            {
                CommonInterface from_obj = (CommonInterface)from_files.elementAt(i);
                for(int j = 0; j < to_files.size(); j++)
                {
                    CommonInterface to_obj = (CommonInterface)to_files.elementAt(j);
                    if(!from_obj.isChanged(((Object) (to_obj))))
                        continue;
                    changed.addElement(((Object) (to_obj)));
                    deleted.addElement(((Object) (from_obj)));
                    from_files.removeElementAt(i);
                    to_files.removeElementAt(j);
                    i--;
                    j--;
                    break;
                }

            }

            for(int i = 0; i < from_files.size(); i++)
            {
                Object obj = from_files.elementAt(i);
                deleted.addElement(obj);
            }

            for(int j = 0; j < to_files.size(); j++)
            {
                Object obj = to_files.elementAt(j);
                added.addElement(obj);
            }

            result.insertElementAt(((Object) (unchanged)), 0);
        }
        return result;
    }

    protected static String differenceToString(Vector mine, Vector yours)
    {
        StringBuffer sb = new StringBuffer();
        int suffix = 0;
        Vector diffs = computeDifferences(mine, yours);
        Vector changed = (Vector)diffs.elementAt(1);
        for(int i = 0; i < changed.size(); i++)
        {
            CommonInterface key = (CommonInterface)changed.elementAt(i);
            sb.append("# CHANGED:\n" + key.getPropertiesFile(suffix, ((String) (null))));
            suffix++;
        }

        Vector added = (Vector)diffs.elementAt(2);
        for(int i = 0; i < added.size(); i++)
        {
            CommonInterface key = (CommonInterface)added.elementAt(i);
            sb.append("# ADDED:\n" + key.getPropertiesFile(suffix, ((String) (null))));
            suffix++;
        }

        Vector deleted = (Vector)diffs.elementAt(3);
        for(int i = 0; i < deleted.size(); i++)
        {
            CommonInterface key = (CommonInterface)deleted.elementAt(i);
            sb.append("# DELETED:\n" + key.getPropertiesFile(i, "remove"));
        }

        return sb.toString();
    }

    protected static Vector prioritizeDiffs(Vector diffs)
    {
        for(int i = 1; i < diffs.size(); i++)
        {
            Vector v = (Vector)diffs.elementAt(i);
            int lastArchive = 0;
            for(int j = 1; j < v.size(); j++)
            {
                AppFile file = (AppFile)v.elementAt(j);
                if(file.isArchive())
                {
                    v.insertElementAt(v.remove(j), lastArchive);
                    lastArchive++;
                } else
                if(file.isSystem())
                    v.insertElementAt(v.remove(j), lastArchive + 1);
            }

        }

        return diffs;
    }

    public String getXML(String indent)
    {
        return getXML(indent, true);
    }

    public String getXML(String indent, boolean includeJarContents)
    {
        ensureXMLIsParsed();
        StringBuffer sb = new StringBuffer();
        String indent_string = indent;
        sb.append("<?xml version=\"1.0\"?>\n");
        sb.append("<!DOCTYPE VERSION SYSTEM \"Version.dtd\">\n");
        sb.append("<VERSION");
        if(id != null)
            sb.append(" ID=\"" + XmlSupport.toCDATA(id) + "\"");
        if(name != null)
            sb.append(" NAME=\"" + XmlSupport.toCDATA(name) + "\"");
        if(creationDate != null)
            sb.append(" CREATIONDATE=\"" + XmlSupport.toCDATA(PropertyUtils.writeLongDate(creationDate)) + "\"");
        if(platformIdentifier != null)
            sb.append(" PLATFORMIDENTIFIER=\"" + XmlSupport.toCDATA(platformIdentifier) + "\"");
        sb.append(">\n");
        sb.append("\t<!-- Data used during the install of an version -->\n");
        if(installData != null)
            sb.append(installData.getXML(indent_string));
        sb.append("\t<!-- Connection policy for this version -->\n");
        if(connectionPolicy != null)
            sb.append(connectionPolicy.getXML(indent_string));
        sb.append("\t<!-- Update policy for this version -->\n");
        if(updatePolicy != null)
            sb.append(updatePolicy.getXML(indent_string));
        sb.append("\t<!-- Classes used to control access to this version -->\n");
        if(accessControl != null)
            sb.append(accessControl.getXML(indent_string));
        sb.append("\t<!-- All the platforms this version is available on -->\n");
        if(platform != null)
            sb.append(platform.getXML(indent_string, includeJarContents));
        sb.append("\t<!-- A descripton for this version (optional) -->\n");
        if(description != null)
        {
            sb.append(indent_string + "<DESCRIPTION>");
            sb.append(XmlSupport.toCDATA(description));
            sb.append("</DESCRIPTION>\n");
        }
        sb.append("</VERSION>\n");
        return sb.toString();
    }

    public void saveXMLToFile(File xmlFile)
        throws IOException
    {
        saveXMLToFile(xmlFile, true);
    }

    public void saveXMLToFile(File xmlFile, boolean includeJarContents)
        throws IOException
    {
        String xml = getXML("\t", includeJarContents);
        Writer writer = ((Writer) (new OutputStreamWriter(((java.io.OutputStream) (new FileOutputStream(xmlFile))), "UTF8")));
        writer.write(xml);
        writer.flush();
        writer.close();
    }
}
