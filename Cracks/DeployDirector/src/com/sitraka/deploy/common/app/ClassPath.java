// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ClassPath.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            CommonInterface

public class ClassPath
    implements CommonInterface
{

    protected String file_name;

    public ClassPath()
    {
        file_name = null;
    }

    public ClassPath(Node class_node)
        throws XmlException
    {
        file_name = null;
        String name = XmlSupport.getAttribute(class_node, "FILE");
        if(name == null)
        {
            throw new XmlException("XML Error: class path file name is missing in classpath");
        } else
        {
            setFileName(name);
            return;
        }
    }

    public void setFileName(String filename)
    {
        file_name = FileUtils.makeInternalPath(filename);
    }

    public String getFileName()
    {
        return file_name;
    }

    public boolean isValid()
    {
        return getFileName() != null && !"".equals(((Object) (getFileName())));
    }

    public boolean isChanged(Object obj)
    {
        return false;
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        String my_prefix = prefix != null ? prefix + "." : "";
        String my_base = base != -1 ? "" + base : "";
        sb.append(my_prefix + "classpath" + my_base + ".name=" + file_name + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<CLASSPATH");
        sb.append(" FILE=\"" + XmlSupport.toCDATA(file_name) + "\"");
        sb.append("/>\n");
        return sb.toString();
    }
}
