// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Platform.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.util.SortInterface;

// Referenced classes of package com.sitraka.deploy.common.app:
//            AppFile

class StringCompare
    implements SortInterface
{

    protected String source;

    StringCompare(String _source)
    {
        source = null;
        source = _source;
    }

    public int compareTo(Object obj, boolean increasing)
    {
        if(obj == null)
            return 0;
        String c = null;
        if(obj instanceof StringCompare)
            c = ((StringCompare)obj).source;
        else
        if(obj instanceof String)
            c = (String)obj;
        else
        if(obj instanceof AppFile)
            c = ((AppFile)obj).getName();
        if(increasing)
            return source.compareTo(c);
        else
            return source.compareTo(c) * -1;
    }
}
