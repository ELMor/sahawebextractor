// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Application.java

package com.sitraka.deploy.common.app;

import com.klg.jclass.util.JCStringTokenizer;
import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.common.app:
//            Version

public class Application
{

    protected String name;
    protected String id;
    protected Date creationDate;
    protected Vector versions;
    protected boolean serverApp;
    protected boolean reorderedVersions;
    protected boolean deleted;

    public Application()
    {
        name = null;
        id = null;
        creationDate = null;
        versions = new Vector();
        serverApp = false;
        reorderedVersions = false;
        deleted = false;
        Version version = new Version();
        version.setName("1.0.0");
        version.setID("0");
        addVersion(version);
        creationDate = Calendar.getInstance().getTime();
    }

    public Application(String appName, File appDirectory)
        throws SAXException, IOException
    {
        name = null;
        id = null;
        creationDate = null;
        versions = new Vector();
        serverApp = false;
        reorderedVersions = false;
        deleted = false;
        name = appName;
        String current_version = null;
        try
        {
            File versionFile = new File(appDirectory + File.separator + "versions.lst");
            if(!versionFile.exists() || !versionFile.canRead())
                throw new FileNotFoundException("Version list file: \"" + versionFile.getAbsolutePath() + "\" does not exist or cannot be read");
            BufferedReader parser;
            for(parser = new BufferedReader(((java.io.Reader) (new FileReader(versionFile)))); parser.ready();)
            {
                current_version = parser.readLine();
                if(current_version != null && !current_version.startsWith("#") && !current_version.equals(""))
                {
                    current_version = current_version.trim();
                    File versionXML = new File(appDirectory.getAbsolutePath() + File.separator + current_version.toLowerCase() + File.separator + "version.xml");
                    if(!versionXML.exists() || !versionXML.canRead())
                    {
                        versionXML = new File(appDirectory.getAbsolutePath() + File.separator + current_version.toLowerCase() + File.separator + "META-INF" + File.separator + "version.xml");
                        if(!versionXML.exists() || !versionXML.canRead())
                        {
                            parser.close();
                            throw new FileNotFoundException("Version XML file \"" + versionXML.getAbsolutePath() + "\" does not exist or cannot be read");
                        }
                    }
                    Version ver = new Version(versionXML, current_version);
                    if(ver != null)
                    {
                        versions.addElement(((Object) (ver)));
                    } else
                    {
                        parser.close();
                        throw new FileNotFoundException("Unable to load application data for " + current_version);
                    }
                }
            }

            parser.close();
            if(versions.size() == 0)
                throw new FileNotFoundException("No versions found for " + name);
        }
        catch(IOException e)
        {
            ((Throwable) (e)).printStackTrace();
            throw new FileNotFoundException("Error accessing file: " + ((Throwable) (e)).getMessage());
        }
        catch(XmlException e)
        {
            throw new FileNotFoundException("Error loading '" + appName + "', version '" + current_version + "': " + ((Throwable) (e)).getMessage());
        }
    }

    public Application(String appDetails)
    {
        name = null;
        id = null;
        creationDate = null;
        versions = new Vector();
        serverApp = false;
        reorderedVersions = false;
        deleted = false;
        Vector details = PropertyUtils.readVector(appDetails, ',');
        if(details.size() > 0)
            name = (String)details.elementAt(0);
        else
            name = null;
        if(details.size() > 1)
            id = (String)details.elementAt(1);
        else
            id = null;
        if(details.size() > 2)
            setCreationDate((String)details.elementAt(2));
        else
            creationDate = null;
    }

    public Application(String name, String id, String creationDate)
    {
        this.name = null;
        this.id = null;
        this.creationDate = null;
        versions = new Vector();
        serverApp = false;
        reorderedVersions = false;
        deleted = false;
        this.name = name;
        this.id = id;
        setCreationDate(creationDate);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String id)
    {
        this.id = id;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    public void setCreationDate(String creationDate)
    {
        if(creationDate == null)
        {
            this.creationDate = null;
        } else
        {
            long time = PropertyUtils.parseLongDate(creationDate);
            if(time == 0L)
                time = PropertyUtils.parseShortDate(creationDate);
            if(time == 0L)
                this.creationDate = null;
            else
                this.creationDate = new Date(time);
        }
    }

    public void setServerApp(boolean serverApp)
    {
        this.serverApp = serverApp;
    }

    public boolean isServerApp()
    {
        return serverApp;
    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public boolean hasDeletedVersions()
    {
        int size = versions.size();
        for(int i = 0; i < size; i++)
        {
            Version ver = (Version)versions.elementAt(i);
            if(ver.isDeleted())
                return true;
        }

        return false;
    }

    public boolean hasReorderedVersions()
    {
        return reorderedVersions;
    }

    public boolean hasVersion(String versionName)
    {
        int size = versions.size();
        for(int i = 0; i < size; i++)
        {
            Version ver = (Version)versions.elementAt(i);
            if(ver.getName().equalsIgnoreCase(versionName))
                return true;
        }

        return false;
    }

    public boolean reorderVersions(int oldIndex, int newIndex)
    {
        if(oldIndex < 0 || oldIndex >= versions.size())
            return false;
        if(newIndex < 0 || newIndex >= versions.size())
            return false;
        if(oldIndex == newIndex)
        {
            return true;
        } else
        {
            Object version = versions.elementAt(oldIndex);
            versions.removeElementAt(oldIndex);
            versions.insertElementAt(version, newIndex);
            reorderedVersions = true;
            return true;
        }
    }

    public boolean areVersionsOutOfOrder(String new_version_order)
    {
        JCStringTokenizer toker = new JCStringTokenizer(new_version_order);
        int index = 0;
        while(toker.hasMoreTokens()) 
        {
            String line = toker.nextToken('\n');
            if(!line.startsWith("#") && !line.equals("") && line.trim().length() != 0)
            {
                Version ver = getVersion(line);
                if(ver == null || versions.indexOf(((Object) (ver))) != index)
                    return true;
                index++;
            }
        }
        return false;
    }

    public boolean resyncVersionOrder(String new_version_order)
    {
        JCStringTokenizer toker = new JCStringTokenizer(new_version_order);
        int index = 0;
        while(toker.hasMoreTokens()) 
        {
            String line = toker.nextToken('\n');
            if(!line.startsWith("#") && !line.equals("") && line.trim().length() != 0)
            {
                Version ver = getVersion(line);
                if(ver == null)
                    return false;
                if(!reorderVersions(versions.indexOf(((Object) (ver))), index))
                    return false;
                index++;
            }
        }
        return true;
    }

    public String getApplicationFileString()
    {
        return getName() + "," + (getID() == null ? "" : getID()) + "," + (getCreationDate() == null ? "" : PropertyUtils.writeLongDate(getCreationDate()));
    }

    public Vector listVersions()
    {
        return versions;
    }

    public Vector listServerVersions()
    {
        Vector list = new Vector();
        int size = versions.size();
        for(int i = 0; i < size; i++)
            if(((Version)versions.elementAt(i)).isServerVersion())
                list.addElement(versions.elementAt(i));

        return list;
    }

    public Vector listLocalVersions()
    {
        Vector list = new Vector();
        int size = versions.size();
        for(int i = 0; i < size; i++)
            if(!((Version)versions.elementAt(i)).isServerVersion())
                list.addElement(versions.elementAt(i));

        return list;
    }

    public boolean hasLocalVersions()
    {
        int size = versions.size();
        for(int i = 0; i < size; i++)
            if(!((Version)versions.elementAt(i)).isServerVersion())
                return true;

        return false;
    }

    public void setVersions(Vector versions)
    {
        this.versions = versions;
    }

    public Version getVersion(String version_name)
    {
        if(version_name == null)
            return null;
        Version result = null;
        if(versions == null)
        {
            result = null;
        } else
        {
            for(int i = 0; i < versions.size(); i++)
            {
                Version candidate = (Version)versions.elementAt(i);
                if(!candidate.getName().equalsIgnoreCase(version_name))
                    continue;
                result = candidate;
                break;
            }

        }
        return result;
    }

    public Version getVersionByID(String version_id)
    {
        Version result = null;
        if(versions == null)
        {
            result = null;
        } else
        {
            for(int i = 0; i < versions.size(); i++)
            {
                Version candidate = (Version)versions.elementAt(i);
                if(!candidate.getID().equalsIgnoreCase(version_id))
                    continue;
                result = candidate;
                break;
            }

        }
        return result;
    }

    public Version newVersion(String name)
    {
        if(name == null)
            return null;
        name = name.trim();
        if(name.equals(""))
            return null;
        if(hasVersion(name))
        {
            return null;
        } else
        {
            Version version = new Version();
            version.setName(name);
            version.setID("" + versions.size());
            addVersionToHead(version);
            return version;
        }
    }

    public Version newVersion(File xmlFile)
    {
        Version version = null;
        if(xmlFile == null)
            return null;
        if(!xmlFile.exists())
            return null;
        try
        {
            version = new Version(xmlFile);
        }
        catch(IOException e)
        {
            return null;
        }
        catch(XmlException e)
        {
            return null;
        }
        version.setCreationDate(Calendar.getInstance().getTime());
        addVersionToHead(version);
        return version;
    }

    public void addVersion(Version ver)
    {
        versions.addElement(((Object) (ver)));
    }

    public void addVersionToHead(Version ver)
    {
        versions.insertElementAt(((Object) (ver)), 0);
    }
}
