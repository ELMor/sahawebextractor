// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   InstallData.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.TreeWalker;

// Referenced classes of package com.sitraka.deploy.common.app:
//            Vendor, QueryUser, ProxyData, RegistryKey, 
//            ShortCut, SystemProperty, Version

public class InstallData
{

    protected QueryUser queryUser;
    protected Vendor vendor;
    protected Vector registryKeys;
    protected Vector shortCuts;
    protected Vector properties;
    protected ProxyData proxyData;
    protected String installEventClass;
    protected String registerWithOS;
    protected String vmParams;
    protected String licenseFile;
    protected String readMe;

    public InstallData()
    {
        queryUser = null;
        vendor = null;
        registryKeys = new Vector();
        shortCuts = new Vector();
        properties = new Vector();
        proxyData = null;
        installEventClass = "";
        registerWithOS = "true";
        vmParams = null;
        licenseFile = null;
        readMe = null;
        vendor = new Vendor();
        queryUser = new QueryUser();
        proxyData = new ProxyData();
    }

    public InstallData(Node install_node)
        throws XmlException
    {
        queryUser = null;
        vendor = null;
        registryKeys = new Vector();
        shortCuts = new Vector();
        properties = new Vector();
        proxyData = null;
        installEventClass = "";
        registerWithOS = "true";
        vmParams = null;
        licenseFile = null;
        readMe = null;
        TreeWalker treeWalker = XmlSupport.createTreeWalker(install_node);
        setLicenseFilename(XmlSupport.getAttribute(install_node, "LICENSEFILE"));
        setReadmeFilename(XmlSupport.getAttribute(install_node, "README"));
        setVMParameters(XmlSupport.getAttribute(install_node, "VMPARAMS"));
        setRegisterWithOS(XmlSupport.getAttribute(install_node, "REGISTERWITHOS"));
        setInstallEventClass(XmlSupport.getAttribute(install_node, "INSTALLEVENTCLASS"));
        Node node = null;
        Node currentNode = treeWalker.getRoot();
        node = XmlSupport.getNextElementNamed(treeWalker, "VENDOR");
        if(node == null)
            throw new XmlException("XML Error: no vendor specified in INSTALLDATA");
        vendor = new Vendor(node);
        currentNode = node;
        node = XmlSupport.getNextElementNamed(treeWalker, "QUERYUSER");
        if(node != null)
            queryUser = new QueryUser(node);
        currentNode = node;
        for(node = XmlSupport.getNextElementNamed(treeWalker, "WINREGENTRY"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "WINREGENTRY"))
        {
            currentNode = node;
            RegistryKey r = new RegistryKey(node);
            if(registryKeys == null)
                registryKeys = new Vector();
            registryKeys.addElement(((Object) (r)));
        }

        treeWalker.setCurrentNode(currentNode);
        for(node = XmlSupport.getNextElementNamed(treeWalker, "SHORTCUT"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "SHORTCUT"))
        {
            currentNode = node;
            ShortCut s = new ShortCut(node);
            if(shortCuts == null)
                shortCuts = new Vector();
            shortCuts.addElement(((Object) (s)));
        }

        treeWalker.setCurrentNode(currentNode);
        for(node = XmlSupport.getNextElementNamed(treeWalker, "SYSTEMPROP"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "SYSTEMPROP"))
        {
            currentNode = node;
            SystemProperty sp = new SystemProperty(node);
            if(properties == null)
                properties = new Vector();
            properties.addElement(((Object) (sp)));
        }

        treeWalker.setCurrentNode(currentNode);
        node = XmlSupport.getNextElementNamed(treeWalker, "PROXY");
        if(node == null)
        {
            proxyData = new ProxyData();
        } else
        {
            proxyData = new ProxyData(node);
            Node node1 = node;
        }
    }

    public String toString()
    {
        String data = "";
        data = data + "licensefile=" + licenseFile + "\n";
        data = data + "readme=" + readMe + "\n";
        data = data + "vmparams=" + vmParams + "\n";
        data = data + "registerwithos=" + registerWithOS + "\n";
        data = data + "installeventclass=" + installEventClass + "\n";
        if(registryKeys != null)
            data = data + registryKeys.toString() + "\n";
        if(shortCuts != null)
            data = data + shortCuts.toString() + "\n";
        if(queryUser != null)
            data = data + ((Object) (queryUser)).toString() + "\n";
        if(vendor != null)
            data = data + ((Object) (vendor)).toString() + "\n";
        if(properties != null)
            data = data + properties.toString() + "\n";
        return data;
    }

    public String getLicenseFilename()
    {
        return licenseFile;
    }

    public void setLicenseFilename(String filename)
    {
        licenseFile = FileUtils.makeInternalPath(filename);
    }

    public String getReadmeFilename()
    {
        return readMe;
    }

    public void setReadmeFilename(String filename)
    {
        readMe = FileUtils.makeInternalPath(filename);
    }

    public String getVMParameters()
    {
        return vmParams;
    }

    public void setVMParameters(String vm_params)
    {
        vmParams = vm_params;
    }

    public void setRegisterWithOS(boolean registerWithOS)
    {
        if(registerWithOS)
            this.registerWithOS = "true";
        else
            this.registerWithOS = "false";
    }

    protected void setRegisterWithOS(String registerWithOS)
    {
        this.registerWithOS = registerWithOS;
    }

    public boolean isRegisterWithOS()
    {
        return registerWithOS.equals("true");
    }

    public String getRegisterWithOS()
    {
        return registerWithOS;
    }

    public void setInstallEventClass(String installEventClass)
    {
        this.installEventClass = installEventClass;
    }

    public String getInstallEventClass()
    {
        return installEventClass;
    }

    public void setRegistryKeys(Vector entries)
    {
        registryKeys = entries;
    }

    public Vector getRegistryKeys()
    {
        return registryKeys;
    }

    public void setQueryUser(QueryUser queryUser)
    {
        this.queryUser = queryUser;
    }

    public QueryUser getQueryUser()
    {
        return queryUser;
    }

    public void setSystemProps(Vector properties)
    {
        this.properties = properties;
    }

    public Vector getSystemProps()
    {
        return properties;
    }

    public void setShortcuts(Vector shortcuts)
    {
        shortCuts = shortcuts;
    }

    public Vector getShortcuts()
    {
        return shortCuts;
    }

    public void setVendor(Vendor vendor)
    {
        this.vendor = vendor;
    }

    public Vendor getVendor()
    {
        return vendor;
    }

    public void setProxyData(ProxyData proxyData)
    {
        this.proxyData = proxyData;
    }

    public ProxyData getProxyData()
    {
        return proxyData;
    }

    public String getPropertiesFile()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("# Readme File\n");
        sb.append("read.me=" + (readMe != null ? readMe : "") + "\n\n");
        sb.append("# License File\n");
        sb.append("license.file=" + (licenseFile != null ? licenseFile : "") + "\n\n");
        sb.append("# VM Parameters\n");
        sb.append("vm.params=" + (vmParams != null ? vmParams : "") + "\n\n");
        sb.append("# Register with OS Flag\n");
        sb.append("register.with.os=" + (registerWithOS != null ? registerWithOS : "true") + "\n\n");
        sb.append("# Event Class\n");
        sb.append("install.event.class=" + (installEventClass != null ? installEventClass : "") + "\n\n");
        if(vendor != null)
        {
            sb.append("# Vendor\n");
            sb.append(vendor.getPropertiesFile(-1, ((String) (null))) + "\n");
        }
        if(queryUser != null)
        {
            sb.append("# QueryUser\n");
            sb.append(queryUser.getPropertiesFile(-1, ((String) (null))) + "\n");
        }
        int size = registryKeys != null ? registryKeys.size() : 0;
        for(int i = 0; i < size; i++)
        {
            sb.append("# Registry entry " + i + "\n");
            RegistryKey reg = (RegistryKey)registryKeys.elementAt(i);
            sb.append(reg.getPropertiesFile(i, ((String) (null))) + "\n");
        }

        size = shortCuts != null ? shortCuts.size() : 0;
        for(int i = 0; i < size; i++)
        {
            sb.append("# ShortCut entry " + i + "\n");
            ShortCut sc = (ShortCut)shortCuts.elementAt(i);
            sb.append(sc.getPropertiesFile(i, ((String) (null))) + "\n");
        }

        size = properties != null ? properties.size() : 0;
        for(int i = 0; i < size; i++)
        {
            sb.append("# SystemProperty entry " + i + "\n");
            SystemProperty sp = (SystemProperty)properties.elementAt(i);
            sb.append(sp.getPropertiesFile(i) + "\n");
        }

        sb.append(proxyData.getPropertiesFile());
        sb.append("\n");
        return sb.toString();
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof InstallData))
            return false;
        InstallData other = (InstallData)obj;
        String otherPropFile = other.getPropertiesFile();
        String ourPropFile = getPropertiesFile();
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public String differenceFrom(InstallData other)
    {
        StringBuffer sb = new StringBuffer();
        String vm_params = other.getVMParameters();
        if(vm_params != null)
        {
            sb.append("#\n# VM Params:\n#\n");
            sb.append("vm.params=" + vm_params + "\n\n");
        }
        String otherRegisterWithOS = other.getRegisterWithOS();
        if(otherRegisterWithOS != null)
        {
            sb.append("#\n# Add Remove:\n#\n");
            sb.append("register.with.os=" + otherRegisterWithOS + "\n\n");
        }
        String otherEventClass = other.getInstallEventClass();
        if(otherEventClass != null)
        {
            sb.append("#\n# Install Event Class:\n#\n");
            sb.append("install.event.class=" + otherEventClass + "\n\n");
        }
        sb.append("#\n# Vendor:\n#\n");
        sb.append(other.getVendor().getPropertiesFile(-1, ((String) (null))) + "\n");
        sb.append("#\n# System properties:\n#\n");
        Vector props = other.getSystemProps();
        int size = props != null ? props.size() : 0;
        for(int i = 0; i < size; i++)
        {
            sb.append("# SystemProperty entry " + i + "\n");
            SystemProperty sp = (SystemProperty)props.elementAt(i);
            sb.append(sp.getPropertiesFile(i) + "\n");
        }

        QueryUser otherUser = other.getQueryUser();
        sb.append("#\n# Query User:\n#\n");
        sb.append(otherUser.getPropertiesFile(-1, ((String) (null))) + "\n\n");
        Vector yours = other.getRegistryKeys();
        sb.append(Version.differenceToString(registryKeys, yours) + "\n");
        yours = other.getShortcuts();
        sb.append(Version.differenceToString(shortCuts, yours) + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<INSTALLDATA");
        if(readMe != null)
            sb.append(" README=\"" + XmlSupport.toCDATA(readMe) + "\"");
        if(licenseFile != null)
            sb.append(" LICENSEFILE=\"" + XmlSupport.toCDATA(licenseFile) + "\"");
        if(vmParams != null)
            sb.append(" VMPARAMS=\"" + XmlSupport.toCDATA(vmParams) + "\"");
        if(registerWithOS != null)
            sb.append(" REGISTERWITHOS=\"" + XmlSupport.toCDATA(registerWithOS) + "\"");
        if(installEventClass != null)
            sb.append(" INSTALLEVENTCLASS=\"" + XmlSupport.toCDATA(installEventClass) + "\"");
        sb.append(">\n");
        sb.append(indent_string + "\t<!-- Name of the vendor -->\n");
        if(vendor != null)
            sb.append(vendor.getXML(indent_string));
        sb.append(indent_string + "\t<!-- What should we query the user about? -->\n");
        if(queryUser != null)
            sb.append(queryUser.getXML(indent_string));
        sb.append(indent_string + "\t<!-- Windows registry entries (optional) -->\n");
        int size = registryKeys != null ? registryKeys.size() : 0;
        for(int i = 0; i < size; i++)
        {
            RegistryKey k = (RegistryKey)registryKeys.elementAt(i);
            sb.append(k.getXML(indent_string));
        }

        sb.append(indent_string + "\t<!-- Shortcuts (optional) -->\n");
        size = shortCuts != null ? shortCuts.size() : 0;
        for(int i = 0; i < size; i++)
        {
            ShortCut s = (ShortCut)shortCuts.elementAt(i);
            sb.append(s.getXML(indent_string));
        }

        sb.append(indent_string + "\t<!-- Properties (runtime) (optional) -->\n");
        size = properties != null ? properties.size() : 0;
        for(int i = 0; i < size; i++)
        {
            SystemProperty p = (SystemProperty)properties.elementAt(i);
            sb.append(p.getXML(indent_string));
        }

        sb.append(indent_string + "\t<!-- Proxy Settings (optional) -->\n");
        if(proxyData != null)
            sb.append(proxyData.getXML(indent_string));
        sb.append(indent_string + "</INSTALLDATA>\n");
        return sb.toString();
    }
}
