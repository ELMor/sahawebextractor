// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Platform.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.SortUtils;
import java.io.File;
import java.io.IOException;
import java.util.Vector;
import org.apache.xerces.parsers.AbstractDOMParser;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.TreeWalker;
import org.xml.sax.SAXException;

// Referenced classes of package com.sitraka.deploy.common.app:
//            JRE, Admin, AppFile, StringCompare, 
//            EntryPoint, VersionDTD

public class Platform
{

    protected JRE jre;
    protected Admin admin;
    protected Vector files;
    protected Vector platforms;
    protected Vector entry_points;
    protected String name;
    protected String installDir;
    protected Platform parent;
    protected String fullName;
    protected File sourceXML;
    protected boolean xmlParsed;

    public Platform()
    {
        jre = null;
        admin = null;
        files = new Vector();
        platforms = new Vector();
        entry_points = new Vector();
        name = null;
        installDir = null;
        parent = null;
        fullName = null;
        sourceXML = null;
        xmlParsed = false;
        jre = new JRE();
        setName("");
    }

    public Platform(Platform _parent, String name)
    {
        jre = null;
        admin = null;
        files = new Vector();
        platforms = new Vector();
        entry_points = new Vector();
        this.name = null;
        installDir = null;
        parent = null;
        fullName = null;
        sourceXML = null;
        xmlParsed = false;
        jre = new JRE();
        parent = _parent;
        setName(name);
    }

    public Platform(Node platform_node, Platform parent, File _sourceXML)
        throws XmlException
    {
        jre = null;
        admin = null;
        files = new Vector();
        platforms = new Vector();
        entry_points = new Vector();
        name = null;
        installDir = null;
        this.parent = null;
        fullName = null;
        sourceXML = null;
        xmlParsed = false;
        sourceXML = _sourceXML;
        parseXML(platform_node, parent);
    }

    public Platform(Node platform_node, Platform parent)
        throws XmlException
    {
        jre = null;
        admin = null;
        files = new Vector();
        platforms = new Vector();
        entry_points = new Vector();
        name = null;
        installDir = null;
        this.parent = null;
        fullName = null;
        sourceXML = null;
        xmlParsed = false;
        parseXML(platform_node, parent);
    }

    protected void parseXML(Node platform_node, Platform parent)
        throws XmlException
    {
        this.parent = parent;
        String _name = XmlSupport.getAttribute(platform_node, "NAME");
        if(_name == null || _name.trim().length() == 0)
            throw new XmlException("XML Error: Platform NAME is missing");
        setName(_name);
        setInstallDir(XmlSupport.getAttribute(platform_node, "INSTALLDIR"));
        TreeWalker treeWalker = XmlSupport.createTreeWalker(platform_node);
        Node node = null;
        Node currentNode = treeWalker.getRoot();
        node = XmlSupport.getNextElementNamed(treeWalker, "ADMIN");
        if(node != null)
        {
            admin = new Admin(node);
            currentNode = node;
        } else
        {
            treeWalker.setCurrentNode(currentNode);
        }
        node = XmlSupport.getNextElementNamed(treeWalker, "JAVA");
        if(node != null)
        {
            jre = new JRE(node);
            currentNode = node;
        } else
        {
            treeWalker.setCurrentNode(currentNode);
        }
        for(node = XmlSupport.getNextElementNamed(treeWalker, "FILE"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "FILE"))
            if(node.getParentNode() == platform_node)
            {
                currentNode = node;
                if(sourceXML == null)
                {
                    xmlParsed = true;
                    AppFile f = new AppFile(node);
                    addFile(f);
                } else
                {
                    AppFile.validateNode(node);
                }
            }

        treeWalker.setCurrentNode(currentNode);
        for(node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"))
            if(node.getParentNode() == platform_node)
            {
                currentNode = node;
                Platform p = new Platform(node, this, sourceXML);
                if(platforms == null)
                    platforms = new Vector();
                platforms.addElement(((Object) (p)));
            }

    }

    public String getFullName()
    {
        return fullName;
    }

    public String getName()
    {
        return name;
    }

    public void freeAppFiles()
    {
        if(getSourceXML() == null)
            return;
        files = null;
        xmlParsed = false;
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            Platform p = (Platform)platforms.elementAt(i);
            p.freeAppFiles();
        }

    }

    protected void setSourceXML(File xml)
    {
        sourceXML = xml;
    }

    protected File getSourceXML()
    {
        return sourceXML;
    }

    public void ensureXMLParsed()
    {
        if(xmlParsed)
            return;
        if(getSourceXML() == null)
            return;
        if(parent != null)
        {
            parent.ensureXMLParsed();
            return;
        }
        org.w3c.dom.Document xml = null;
        try
        {
            DOMParser parser = new DOMParser();
            parser.setFeature("http://xml.org/sax/features/validation", true);
            parser.setFeature("http://apache.org/xml/features/dom/include-ignorable-whitespace", false);
            parser.setErrorHandler(((org.xml.sax.ErrorHandler) (new com.sitraka.deploy.common.XmlSupport.ErrorPrinter())));
            parser.setEntityResolver(((org.xml.sax.EntityResolver) (new com.sitraka.deploy.common.XmlSupport.DTDStringResolver(VersionDTD.getDTD(), VersionDTD.getDTDName()))));
            parser.parse(XmlSupport.createInputSource(getSourceXML()));
            xml = ((AbstractDOMParser) (parser)).getDocument();
        }
        catch(SAXException e)
        {
            throw new IllegalArgumentException("Error creating XML parser for: " + getSourceXML());
        }
        catch(IOException e)
        {
            throw new IllegalArgumentException("Error creating XML parser for: " + getSourceXML());
        }
        if(xml == null)
            throw new IllegalArgumentException("XML Error: Internal error, unable to create XmlDocument object for version");
        TreeWalker treeWalker = XmlSupport.createTreeWalker(((Node) (xml)));
        Node node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM");
        if(node == null)
            throw new IllegalArgumentException("XML Error: version.xml is missing PLATFORM tag");
        try
        {
            createAppFiles(node);
        }
        catch(XmlException e)
        {
            throw new IllegalArgumentException("Error parsing XML for: " + getSourceXML());
        }
    }

    protected void createAppFiles(Node platform_node)
        throws XmlException
    {
        TreeWalker treeWalker = XmlSupport.createTreeWalker(platform_node);
        Node node = null;
        Node currentNode = treeWalker.getRoot();
        for(node = XmlSupport.getNextElementNamed(treeWalker, "FILE"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "FILE"))
            if(node.getParentNode() == platform_node)
            {
                currentNode = node;
                AppFile f = new AppFile(node);
                addFile(f);
            }

        treeWalker.setCurrentNode(currentNode);
        for(node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"))
            if(node.getParentNode() == platform_node)
            {
                currentNode = node;
                String name = getFullName();
                if(name == null || name.trim().length() == 0)
                    name = "";
                else
                    name = name + ".";
                name = name + XmlSupport.getAttribute(node, "NAME");
                Platform p = findPlatform(name);
                p.createAppFiles(node);
            }

        xmlParsed = true;
    }

    public void addFile(AppFile f)
    {
        if(files == null)
            files = new Vector();
        SortUtils.insertSorted(files, ((com.sitraka.deploy.util.SortInterface) (f)), true);
    }

    public void setName(String name)
    {
        this.name = name;
        if(parent != null)
        {
            if(parent.getFullName().trim().equals(""))
                fullName = this.name;
            else
                fullName = parent.getFullName() + "." + this.name;
        } else
        {
            fullName = "";
        }
        fullName = fullName.toLowerCase();
    }

    public void setInstallDir(String dir)
    {
        installDir = FileUtils.makeInternalPath(dir);
    }

    public String getInstallDir()
    {
        return installDir;
    }

    public String getAvailableInstallDir(String platformName, String install_dir)
    {
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                if(platformName.startsWith(child.getFullName().toLowerCase()))
                {
                    String current_dir = child.getInstallDir();
                    if(current_dir != null && current_dir.length() > 0)
                        install_dir = current_dir;
                    return child.getAvailableInstallDir(platformName, install_dir);
                }
            }

        }
        return install_dir;
    }

    public int generateManifest(StringBuffer manifest, String baseDir, int fileCount)
    {
        ensureXMLParsed();
        Vector platformList = getPlatforms();
        int size = platformList.size();
        for(int i = 0; i < size; i++)
        {
            Platform plat = (Platform)platformList.elementAt(i);
            fileCount = plat.generateManifest(manifest, baseDir, fileCount);
        }

        Vector fileList = listFiles();
        size = fileList.size();
        for(int i = 0; i < size; i++)
        {
            AppFile appFile = (AppFile)fileList.elementAt(i);
            String platformDir = FileUtils.convertNameToFile(getFullName(), '\0');
            if(platformDir == null || platformDir.equals(""))
                manifest.append("file" + fileCount + ".name=" + baseDir + File.separator + appFile.getName() + "\n");
            else
                manifest.append("file" + fileCount + ".name=" + baseDir + File.separator + platformDir + File.separator + appFile.getName() + "\n");
            manifest.append("file" + fileCount + ".size=" + appFile.getSize().toString() + "\n");
            manifest.append("file" + fileCount + ".hash=" + appFile.getHash() + "\n");
            fileCount++;
        }

        return fileCount;
    }

    public void getFiles(String platformName, Vector fileList)
    {
        ensureXMLParsed();
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                child.getFiles(platformName, fileList);
            }

        }
        if(!platformName.toLowerCase().startsWith(fullName))
            return;
        if(files != null)
        {
            int size = files.size();
            for(int i = 0; i < size; i++)
            {
                AppFile file = (AppFile)files.elementAt(i);
                if(SortUtils.searchVector(fileList, ((com.sitraka.deploy.util.SortInterface) (file)), true) == -1)
                    SortUtils.insertSorted(fileList, ((com.sitraka.deploy.util.SortInterface) (file)), true);
            }

        }
    }

    public Platform getParentPlatform()
    {
        return parent;
    }

    public Platform getAllPlatform()
    {
        if(parent == null)
            return this;
        else
            return parent.getAllPlatform();
    }

    public Vector getPlatforms()
    {
        return platforms;
    }

    public void setPlatforms(Vector platforms)
    {
        this.platforms = platforms;
    }

    public void addPlatform(Platform platform)
    {
        platforms.add(((Object) (platform)));
    }

    public AppFile findAppFile(String platform_name, String parent_file, String sub_file)
    {
        ensureXMLParsed();
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                AppFile a = child.findAppFile(platform_name, parent_file, sub_file);
                if(a != null)
                    return a;
            }

        }
        if(!platform_name.toLowerCase().startsWith(fullName))
            return null;
        int index = SortUtils.searchVector(files, ((com.sitraka.deploy.util.SortInterface) (new StringCompare(parent_file))), true);
        if(index == -1)
            return null;
        AppFile file = (AppFile)files.elementAt(index);
        if(sub_file == null)
            return file;
        Vector v = file.getArchiveDetails();
        index = SortUtils.searchVector(v, ((com.sitraka.deploy.util.SortInterface) (new StringCompare(sub_file))), true);
        if(index == -1)
            return null;
        else
            return (AppFile)v.elementAt(index);
    }

    public boolean hasFile(String filename)
    {
        ensureXMLParsed();
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                if(child.hasFile(filename))
                    return true;
            }

        }
        return hasLocalFile(filename);
    }

    public Platform getPlatform(String platformName)
    {
        ensureXMLParsed();
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                if(child.getName().equals(((Object) (platformName))))
                    return child;
            }

        }
        return null;
    }

    public boolean hasLocalFile(String filename)
    {
        return getLocalFile(filename) != null;
    }

    public AppFile getLocalFile(String filename)
    {
        ensureXMLParsed();
        if(files != null)
        {
            int index = SortUtils.searchVector(files, ((com.sitraka.deploy.util.SortInterface) (new StringCompare(filename))), true);
            if(index == -1)
                return null;
            else
                return (AppFile)files.elementAt(index);
        } else
        {
            return null;
        }
    }

    public boolean branchHasFile(String filename)
    {
        if(hasFile(filename))
            return true;
        return parent != null && parentHasFile(filename);
    }

    public boolean parentHasFile(String filename)
    {
        if(parent == null)
            return false;
        if(parent.hasLocalFile(filename))
            return true;
        return parent.parentHasFile(filename);
    }

    public String getPathToFile(String filename, String platformName)
    {
        ensureXMLParsed();
        Platform p = findPlatform(platformName);
        if(p == null)
            return null;
        if(!p.hasFile(filename))
        {
            for(p = p.parent; p != null; p = p.parent)
                if(p.hasFile(filename))
                    break;

            if(p == null)
                return null;
        }
        String pathname = p.getFullName().replace('.', File.separatorChar);
        return pathname + File.separator + filename;
    }

    public Platform findPlatform(String platformName)
    {
        if(fullName.equalsIgnoreCase(platformName))
            return this;
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                Platform candidate = child.findPlatform(platformName);
                if(candidate != null)
                    return candidate;
            }

        }
        if(platformName.startsWith(fullName))
            return this;
        else
            return null;
    }

    public Vector listPlatforms()
    {
        Vector v = new Vector();
        listPlatforms(v);
        return v;
    }

    public Vector listFiles()
    {
        ensureXMLParsed();
        return files;
    }

    public void setFileList(Vector files)
    {
        this.files = files;
    }

    protected void listPlatforms(Vector list)
    {
        if(platforms == null)
            return;
        int size = platforms.size();
        for(int i = 0; i < size; i++)
        {
            Platform p = (Platform)platforms.elementAt(i);
            if(p != null)
            {
                list.addElement(((Object) (p)));
                p.listPlatforms(list);
            }
        }

    }

    public Platform copyPlatform()
    {
        Platform newPlatform = new Platform(parent, getName());
        newPlatform.setAdmin(getAdmin());
        newPlatform.setInstallDir(getInstallDir());
        newPlatform.setJRE(getJRE());
        newPlatform.setPlatforms(getPlatforms());
        newPlatform.setFileList(listFiles());
        return newPlatform;
    }

    public Admin getAdmin()
    {
        return admin;
    }

    public void setAdmin(Admin admin)
    {
        this.admin = admin;
    }

    public JRE getJRE()
    {
        return jre;
    }

    public void setJRE(JRE jre)
    {
        this.jre = jre;
    }

    protected boolean isJRESpecified()
    {
        if(jre == null)
        {
            return false;
        } else
        {
            boolean vendor_specified = jre.getVendor() != null && jre.getVendor().trim().length() != 0;
            boolean version_specified = jre.getVersion() != null && jre.getVersion().trim().length() != 0;
            return vendor_specified || version_specified;
        }
    }

    public JRE getJRE(String platformName)
    {
        if(platformName == null)
            platformName = "";
        if(fullName.equalsIgnoreCase(platformName) && isJRESpecified())
            return jre;
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                JRE candidate = child.getJRE(platformName);
                if(candidate != null)
                    return candidate;
            }

        }
        if(platformName.startsWith(fullName) && isJRESpecified())
            return jre;
        else
            return null;
    }

    public int getEntryPoints(String platformName, StringBuffer sb, int base)
    {
        return getEntryPoints(platformName, sb, base, "entry");
    }

    public int getEntryPoints(String platformName, StringBuffer sb, int base, String prefix)
    {
        if(platformName == null)
            platformName = "";
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                base = child.getEntryPoints(platformName, sb, base, prefix);
            }

        }
        if(platformName.startsWith(fullName))
        {
            Vector entry_points = jre == null ? null : jre.listEntryPoints();
            if(entry_points != null)
            {
                int size = entry_points.size();
                for(int i = 0; i < size; i++)
                {
                    EntryPoint p = (EntryPoint)entry_points.elementAt(i);
                    sb.append(p.getPropertiesFile(base, prefix) + "\n");
                    base++;
                }

            }
        }
        return base;
    }

    public int getClassPaths(String platformName, StringBuffer sb, int base)
    {
        if(platformName == null)
            platformName = "";
        if(platforms != null)
        {
            int size = platforms.size();
            for(int i = 0; i < size; i++)
            {
                Platform child = (Platform)platforms.elementAt(i);
                base = child.getClassPaths(platformName, sb, base);
            }

        }
        if(platformName.startsWith(fullName) && jre != null)
            base = jre.getClassPaths(sb, base);
        return base;
    }

    public String getXML(String indent)
    {
        return getXML(indent, true);
    }

    public String getXML(String indent, boolean includeJarContents)
    {
        ensureXMLParsed();
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<PLATFORM NAME=\"" + XmlSupport.toCDATA(name) + "\" ");
        if(installDir != null)
            sb.append("INSTALLDIR=\"" + XmlSupport.toCDATA(installDir) + "\" ");
        sb.append(">\n");
        sb.append(indent_string + "\t<!-- Administrator data (optional) -->\n");
        if(admin != null)
            sb.append(admin.getXML(indent_string));
        sb.append(indent_string + "\t<!-- Required Java version and vendor (optional) -->\n");
        if(jre != null)
            sb.append(jre.getXML(indent_string));
        sb.append(indent_string + "\t<!-- List of files for " + (fullName.length() != 0 ? XmlSupport.toCDATA(fullName) : "all platforms") + " -->\n");
        int size = files != null ? files.size() : 0;
        for(int i = 0; i < size; i++)
        {
            AppFile f = (AppFile)files.elementAt(i);
            sb.append(f.getXML(indent_string, includeJarContents));
        }

        size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            Platform p = (Platform)platforms.elementAt(i);
            sb.append(p.getXML(indent_string, includeJarContents));
        }

        sb.append(indent_string + "</PLATFORM>\n");
        return sb.toString();
    }
}
