// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UpdatePolicy.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.CommonEnums;
import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.PropertyUtils;
import java.util.Date;
import org.w3c.dom.Node;

public class UpdatePolicy
{

    protected int policy;
    protected Date schedule;

    public UpdatePolicy()
    {
        schedule = null;
        policy = 1;
        schedule = null;
    }

    public UpdatePolicy(Node update_node)
        throws XmlException
    {
        schedule = null;
        String policy_string = XmlSupport.getAttribute(update_node, "POLICY");
        policy = PropertyUtils.toEnum(policy_string, CommonEnums.UPDATE_POLICY_NAMES, CommonEnums.UPDATE_POLICY_VALUES);
        if(policy == -1)
            throw new XmlException("XML Error: unable to determine update policy from '" + policy_string + "'");
        String date_string = XmlSupport.getAttribute(update_node, "SCHEDULE");
        if(date_string == null)
        {
            schedule = null;
        } else
        {
            long date = PropertyUtils.parseLongDate(date_string);
            if(date == 0L)
                throw new XmlException("XML Error: unable to determine update schedule from '" + date_string + "'");
            schedule = new Date(date);
        }
        if(policy == 2 && schedule == null)
            throw new XmlException("XML Error: UpdatePolicy set to scheduled mandatory, but no date provided");
        else
            return;
    }

    public int getUpdatePolicy()
    {
        if(policy == 2 && (schedule == null || schedule.getTime() <= (new Date()).getTime()))
            return 1;
        else
            return policy;
    }

    public void setUpdatePolicy(int policy)
    {
        this.policy = policy;
    }

    public Date getSchedule()
    {
        return schedule;
    }

    public void setSchedule(Date schedule)
    {
        this.schedule = schedule;
    }

    public String toString()
    {
        int current_policy = getUpdatePolicy();
        StringBuffer buff = new StringBuffer("update=");
        String value = PropertyUtils.fromEnum(current_policy, CommonEnums.UPDATE_POLICY_NAMES, CommonEnums.UPDATE_POLICY_VALUES);
        buff.append(value + "\n");
        if(schedule != null)
            buff.append("schedule=" + PropertyUtils.writeLongDate(schedule) + "\n");
        return buff.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<UPDATE");
        String value = PropertyUtils.fromEnum(policy, CommonEnums.UPDATE_POLICY_NAMES, CommonEnums.UPDATE_POLICY_VALUES);
        sb.append(" POLICY=\"" + XmlSupport.toCDATA(value) + "\"");
        if(schedule != null)
            sb.append(" SCHEDULE=\"" + XmlSupport.toCDATA(PropertyUtils.writeLongDate(schedule)) + "\"");
        sb.append("/>\n");
        return sb.toString();
    }
}
