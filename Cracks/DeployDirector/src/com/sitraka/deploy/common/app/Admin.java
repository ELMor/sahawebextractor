// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Admin.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlSupport;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            Vendor, CommonInterface

public class Admin
    implements CommonInterface
{

    protected String user;
    protected String password;

    public Admin()
    {
        user = null;
        password = null;
    }

    public Admin(Node admin_node)
    {
        user = null;
        password = null;
        user = XmlSupport.getAttribute(admin_node, "USER");
        password = XmlSupport.getAttribute(admin_node, "PASSWORD");
    }

    public void setUserName(String userName)
    {
        user = userName;
    }

    public String getUserName()
    {
        return user;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword()
    {
        return password;
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof Vendor))
            return false;
        Admin other = (Admin)obj;
        String otherPropFile = other.getPropertiesFile(-1, ((String) (null)));
        String ourPropFile = getPropertiesFile(-1, ((String) (null)));
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public boolean isChanged(Object obj)
    {
        if(!(obj instanceof Admin))
            return true;
        if(equals(obj))
            return false;
        Admin other = (Admin)obj;
        String otherUser = other.user;
        String otherPassword = other.password;
        return user.equals(((Object) (otherUser))) && password.equals(((Object) (otherPassword)));
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        String my_prefix = prefix != null ? prefix + "." : "";
        sb.append(my_prefix + "platform.admin.user=" + user + "\n");
        sb.append(my_prefix + "platform.admin.password=" + password + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<ADMIN USER=\"" + XmlSupport.toCDATA(user) + "\"");
        sb.append(" PASSWORD=\"" + XmlSupport.toCDATA(password) + "\"/>\n");
        return sb.toString();
    }
}
