// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AccessControl.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.AuthGroups;
import com.sitraka.deploy.Authentication;
import com.sitraka.deploy.Authorization;
import com.sitraka.deploy.ClientAuthentication;
import com.sitraka.deploy.GroupAuthorization;
import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import java.io.PrintStream;
import org.w3c.dom.Node;

public class AccessControl
{

    public static final String DEFAULT_CLIENT_AUTHENTICATION_CLASS = "com.sitraka.deploy.authentication.ClientAuthenticateAll";
    public static final String DEFAULT_AUTHENTICATION_CLASS = "com.sitraka.deploy.authentication.AuthenticateAll";
    public static final String DEFAULT_AUTHORIZATION_CLASS = "com.sitraka.deploy.authorization.AuthorizeAll";
    public static final String DEFAULT_AUTHGROUPS_CLASS = "com.sitraka.deploy.authentication.SimpleAuthGroups";
    public static final String DEFAULT_PASSWORD_CACHE = "false";
    protected String allowCache;
    protected String clientAuthentication;
    protected String authentication;
    protected String authorization;
    protected String authGroups;

    public AccessControl()
    {
        allowCache = "false";
        clientAuthentication = "com.sitraka.deploy.authentication.ClientAuthenticateAll";
        authentication = "com.sitraka.deploy.authentication.AuthenticateAll";
        authorization = "com.sitraka.deploy.authorization.AuthorizeAll";
        authGroups = "com.sitraka.deploy.authentication.SimpleAuthGroups";
    }

    public AccessControl(Node access_node)
        throws XmlException
    {
        allowCache = "false";
        clientAuthentication = "com.sitraka.deploy.authentication.ClientAuthenticateAll";
        authentication = "com.sitraka.deploy.authentication.AuthenticateAll";
        authorization = "com.sitraka.deploy.authorization.AuthorizeAll";
        authGroups = "com.sitraka.deploy.authentication.SimpleAuthGroups";
        allowCache = XmlSupport.getAttribute(access_node, "ALLOWCACHE");
        clientAuthentication = XmlSupport.getAttribute(access_node, "CLIENTAUTHENTICATION");
        authentication = XmlSupport.getAttribute(access_node, "AUTHENTICATION");
        authorization = XmlSupport.getAttribute(access_node, "AUTHORIZATION");
        authGroups = XmlSupport.getAttribute(access_node, "AUTHGROUPS");
        validateValues();
    }

    protected void validateValues()
        throws XmlException
    {
        if(allowCache == null)
            throw new XmlException("XML Definition error 'ALLOWCACHE' is missing");
        if(clientAuthentication == null)
            throw new XmlException("XML Definition error 'CLIENTAUTHENTICATION' is missing");
        if(authentication == null)
            throw new XmlException("XML Definition error 'AUTHENTICATION' is missing");
        if(authorization == null)
            throw new XmlException("XML Definition error 'AUTHORIZATION' is missing");
        else
            return;
    }

    public String getClientAuthentication()
    {
        return clientAuthentication;
    }

    public void setClientAuthentication(String className)
    {
        clientAuthentication = className;
    }

    public String getAuthentication()
    {
        return authentication;
    }

    public void setAuthentication(String className)
    {
        authentication = className;
    }

    public String getAuthorization()
    {
        return authorization;
    }

    public void setAuthorization(String className)
    {
        authorization = className;
    }

    public String getAuthGroups()
    {
        return authGroups;
    }

    public void setAuthGroups(String className)
    {
        authGroups = className;
    }

    public String getAllowCache()
    {
        return allowCache;
    }

    public boolean isAllowCache()
    {
        return allowCache.equalsIgnoreCase("true");
    }

    public void setAllowCache(String allow)
    {
        allowCache = allow;
    }

    public void setAllowCache(boolean allow)
    {
        if(allow)
            allowCache = "true";
        else
            allowCache = "false";
    }

    public String classNameToFileName(String classname)
    {
        String filename = new String(classname);
        int index = classname.lastIndexOf(".");
        if(index != -1)
            filename = filename.substring(index + 1);
        filename = filename.concat(".class");
        return filename;
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<ACCESS");
        if(allowCache != null)
            sb.append(" ALLOWCACHE=\"" + XmlSupport.toCDATA(allowCache) + "\"");
        if(clientAuthentication != null && clientAuthentication.trim().length() > 0)
            sb.append(" CLIENTAUTHENTICATION=\"" + XmlSupport.toCDATA(clientAuthentication) + "\"");
        if(authentication != null && authentication.trim().length() > 0)
            sb.append(" AUTHENTICATION=\"" + XmlSupport.toCDATA(authentication) + "\"");
        if(authorization != null && authorization.trim().length() > 0)
            sb.append(" AUTHORIZATION=\"" + XmlSupport.toCDATA(authorization) + "\"");
        if(authGroups != null && authGroups.trim().length() > 0 && authorization != null && authorization.trim().length() > 0)
        {
            Class auth_class = null;
            try
            {
                auth_class = Class.forName(authorization);
                if((com.sitraka.deploy.GroupAuthorization.class).isAssignableFrom(auth_class))
                    sb.append(" AUTHGROUPS=\"" + XmlSupport.toCDATA(authGroups) + "\"");
            }
            catch(ClassNotFoundException cnfe)
            {
                sb.append(" AUTHGROUPS=\"" + XmlSupport.toCDATA(authGroups) + "\"");
            }
        }
        sb.append("/>\n");
        return sb.toString();
    }

    public String getPropertiesFile()
    {
        StringBuffer sb = new StringBuffer();
        if(allowCache != null)
            sb.append("allowcache=" + allowCache + "\n");
        if(clientAuthentication != null)
            sb.append("clientauthentication=" + clientAuthentication + "\n");
        if(authentication != null)
            sb.append("authentication=" + authentication + "\n");
        if(authorization != null)
            sb.append("authorization=" + authorization + "\n");
        if(authGroups != null)
            sb.append("authgroups=" + authGroups + "\n");
        return sb.toString();
    }

    public String getAuthenticationFileName()
    {
        return getAuthenticationFileName(authentication);
    }

    public static String getAuthenticationFileName(String authClass)
    {
        return authClass + "." + "authentication.dat";
    }

    public String getAuthorizationFileName()
    {
        return getAuthorizationFileName(authorization);
    }

    public static String getAuthorizationFileName(String authClass)
    {
        return authClass + "." + "authorization.dat";
    }

    public String getAuthGroupsFileName()
    {
        return getAuthGroupsFileName(authGroups);
    }

    public static String getAuthGroupsFileName(String authGroupsClass)
    {
        return authGroupsClass + "." + "authgroups.dat";
    }

    public Authentication createAuthenticationInstance()
    {
        Object obj = loadCreateInstance(authentication);
        if(obj == null)
            return null;
        if(!(com.sitraka.deploy.Authentication.class).isAssignableFrom(obj.getClass()))
        {
            System.err.println("Class " + authentication + " is not an instance of Authentication");
            return null;
        } else
        {
            return (Authentication)obj;
        }
    }

    public Authorization createAuthorizationInstance()
    {
        Object obj = loadCreateInstance(authorization);
        if(obj == null)
            return null;
        if(!(com.sitraka.deploy.Authorization.class).isAssignableFrom(obj.getClass()))
        {
            System.err.println("Class " + authorization + " is not an instance of Authorization");
            return null;
        }
        if(obj instanceof GroupAuthorization)
            ((GroupAuthorization)obj).setAuthGroups(createAuthGroupsInstance());
        return (Authorization)obj;
    }

    public AuthGroups createAuthGroupsInstance()
    {
        Object obj = loadCreateInstance(authGroups);
        if(obj == null)
            return null;
        if(!(com.sitraka.deploy.AuthGroups.class).isAssignableFrom(obj.getClass()))
        {
            System.err.println("Class " + authGroups + " is not an instance of AuthGroups");
            (new Throwable()).printStackTrace(System.err);
            return null;
        } else
        {
            return (AuthGroups)obj;
        }
    }

    public ClientAuthentication createClientAuthenticationInstance()
    {
        Object obj = loadCreateInstance(clientAuthentication);
        if(obj == null)
            return null;
        if(!(com.sitraka.deploy.ClientAuthentication.class).isAssignableFrom(obj.getClass()))
        {
            System.err.println("Class " + clientAuthentication + " is not an instance of ClientAuthentication");
            return null;
        } else
        {
            return (ClientAuthentication)obj;
        }
    }

    protected static Object loadCreateInstance(String cas)
    {
        if(cas == null || cas.equals(""))
            return ((Object) (null));
        Class clz = null;
        try
        {
            clz = Class.forName(cas);
        }
        catch(ClassNotFoundException cnfe)
        {
            System.err.println("Class " + cas + " not found");
            return ((Object) (null));
        }
        if(clz == null)
        {
            System.err.println("No class loaded for " + cas);
            return ((Object) (null));
        }
        try
        {
            return clz.newInstance();
        }
        catch(InstantiationException ie1)
        {
            System.err.println("Could not instantiate instance of " + cas);
        }
        catch(IllegalAccessException iae)
        {
            System.err.println("Could not access no-argument constructor for " + cas);
        }
        return ((Object) (null));
    }
}
