// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Vendor.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            CommonInterface

public class Vendor
    implements CommonInterface
{

    protected String name;
    protected String dir;

    public Vendor()
    {
        name = "";
        dir = "";
    }

    public Vendor(Node vendor_node)
        throws XmlException
    {
        name = "";
        dir = "";
        setName(XmlSupport.getAttribute(vendor_node, "NAME"));
        String _dir = XmlSupport.getAttribute(vendor_node, "DIR");
        if(_dir == null)
        {
            throw new XmlException("XML Error: No directory specified for VENDOR tag");
        } else
        {
            setDirectory(_dir);
            return;
        }
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDirectory()
    {
        return dir;
    }

    public void setDirectory(String dir)
    {
        this.dir = FileUtils.makeInternalPath(dir);
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof Vendor))
            return false;
        Vendor other = (Vendor)obj;
        String otherPropFile = other.getPropertiesFile(-1, ((String) (null)));
        String ourPropFile = getPropertiesFile(-1, ((String) (null)));
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public boolean isChanged(Object obj)
    {
        if(!(obj instanceof Vendor))
            return true;
        if(equals(obj))
            return false;
        Vendor other = (Vendor)obj;
        String otherName = other.name.toLowerCase();
        String otherDir = other.dir;
        return name.equalsIgnoreCase(otherName) && !dir.equals(((Object) (otherDir)));
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        String my_prefix = prefix != null ? prefix + "." : "";
        sb.append(my_prefix + "vendor.name=" + name + "\n");
        sb.append(my_prefix + "vendor.dir=" + dir + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<VENDOR NAME=\"" + XmlSupport.toCDATA(name) + "\"");
        sb.append(" DIR=\"" + XmlSupport.toCDATA(dir) + "\"/>\n");
        return sb.toString();
    }
}
