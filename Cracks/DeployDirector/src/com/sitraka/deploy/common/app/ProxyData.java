// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ProxyData.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.PropertyUtils;
import java.util.Vector;
import org.w3c.dom.Node;

public class ProxyData
{

    public static final String SOURCE_BROWSER = "browser";
    public static final String SOURCE_NOPROXY = "noproxy";
    public static final String SOURCE_XML = "xml";
    protected String source;
    protected String host;
    protected String port;
    protected Vector nonProxyHosts;

    public ProxyData()
    {
        source = "browser";
        host = "";
        port = "";
        nonProxyHosts = new Vector();
    }

    public ProxyData(Node proxy_node)
        throws XmlException
    {
        source = "browser";
        host = "";
        port = "";
        String data = XmlSupport.getAttribute(proxy_node, "PROXYSOURCE");
        if(data == null)
            setSource("browser");
        else
            setSource(data);
        data = XmlSupport.getAttribute(proxy_node, "HTTPPROXYHOST");
        if(data == null)
            setHost("");
        else
            setHost(data);
        data = XmlSupport.getAttribute(proxy_node, "HTTPPROXYPORT");
        if(data == null)
            setPort("");
        else
            setPort(data);
        String nonProxyHostsString = XmlSupport.getAttribute(proxy_node, "HTTPNONPROXYHOSTS");
        if(nonProxyHostsString == null)
            nonProxyHosts = new Vector();
        else
            nonProxyHosts = PropertyUtils.readVector(nonProxyHostsString, ',');
    }

    public String getPropertiesFile()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("# Proxy Settings\n");
        sb.append("http.proxySource=");
        sb.append(source);
        sb.append("\n");
        sb.append("http.proxyHost=");
        sb.append(host);
        sb.append("\n");
        sb.append("http.proxyPort=");
        sb.append(port);
        sb.append("\n");
        sb.append("http.nonProxyHosts=");
        sb.append(getNonProxyHostsString());
        sb.append("\n");
        return sb.toString();
    }

    public void setSource(String source)
    {
        this.source = source;
    }

    public String getSource()
    {
        return source;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getHost()
    {
        return host;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getPort()
    {
        return port;
    }

    public void setNonProxyHosts(String nonProxyHostsString)
    {
        nonProxyHosts = PropertyUtils.readVector(nonProxyHostsString, ',');
    }

    public void setNonProxyHosts(Vector nonProxyHosts)
    {
        this.nonProxyHosts = nonProxyHosts;
    }

    public Vector getNonProxyHosts()
    {
        return nonProxyHosts;
    }

    public String getNonProxyHostsString()
    {
        StringBuffer sb = new StringBuffer();
        int size = nonProxyHosts.size();
        if(size >= 1)
            sb.append(XmlSupport.toCDATA((String)nonProxyHosts.elementAt(0)));
        for(int i = 1; i < size; i++)
        {
            sb.append(",");
            sb.append(XmlSupport.toCDATA((String)nonProxyHosts.elementAt(i)));
        }

        return sb.toString();
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof ProxyData))
            return false;
        ProxyData other = (ProxyData)obj;
        String otherPropFile = other.getPropertiesFile();
        String ourPropFile = getPropertiesFile();
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<PROXY");
        sb.append(" PROXYSOURCE=\"");
        sb.append(XmlSupport.toCDATA(source));
        sb.append("\"");
        sb.append(" HTTPPROXYHOST=\"");
        sb.append(XmlSupport.toCDATA(host));
        sb.append("\"");
        sb.append(" HTTPPROXYPORT=\"");
        sb.append(XmlSupport.toCDATA(port));
        sb.append("\"");
        sb.append(" HTTPNONPROXYHOSTS=\"");
        sb.append(getNonProxyHostsString());
        sb.append("\"");
        sb.append("/>\n");
        return sb.toString();
    }
}
