// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RegistryKey.java

package com.sitraka.deploy.common.app;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.app:
//            CommonInterface

public class RegistryKey
    implements CommonInterface
{

    protected String value;
    protected String name;
    protected String hkey;
    protected String skey;

    public RegistryKey()
    {
        value = null;
        name = null;
        hkey = null;
        skey = null;
    }

    public RegistryKey(Node install_node)
        throws XmlException
    {
        value = null;
        name = null;
        hkey = null;
        skey = null;
        value = XmlSupport.getAttribute(install_node, "VALUE");
        if(value == null)
            throw new XmlException("XML error: VALUE is missing in WINREGENTRY");
        name = XmlSupport.getAttribute(install_node, "NAME");
        if(name == null)
            throw new XmlException("XML error: NAME is missing in WINREGENTRY");
        hkey = XmlSupport.getAttribute(install_node, "HKEY");
        if(hkey == null)
            throw new XmlException("XML error: HKEY is missing in WINREGENTRY");
        skey = XmlSupport.getAttribute(install_node, "SKEY");
        if(skey == null)
            throw new XmlException("XML error: SKEY is missing in WINREGENTRY");
        else
            return;
    }

    public String getHKey()
    {
        return hkey;
    }

    public void setHKey(String hkey)
    {
        this.hkey = hkey;
    }

    public String getSKey()
    {
        return skey;
    }

    public void setSKey(String skey)
    {
        this.skey = skey;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public boolean equals(Object obj)
    {
        if(!(obj instanceof RegistryKey))
            return false;
        RegistryKey other = (RegistryKey)obj;
        String otherPropFile = other.getPropertiesFile(-1, ((String) (null)));
        String ourPropFile = getPropertiesFile(-1, ((String) (null)));
        return ourPropFile.equalsIgnoreCase(otherPropFile);
    }

    public boolean isChanged(Object obj)
    {
        if(!(obj instanceof RegistryKey))
            return true;
        if(equals(obj))
            return false;
        RegistryKey other = (RegistryKey)obj;
        if(!hkey.equalsIgnoreCase(other.hkey) || !skey.equalsIgnoreCase(other.skey))
            return false;
        return !name.equalsIgnoreCase(other.name) || !value.equalsIgnoreCase(other.value);
    }

    public String getPropertiesFile(int base, String prefix)
    {
        StringBuffer sb = new StringBuffer();
        String my_prefix = prefix != null ? prefix + "." : "";
        String my_base = base != -1 ? "" + base : "";
        sb.append(my_prefix + "winregentry" + my_base + ".hkey=" + hkey + "\n");
        sb.append(my_prefix + "winregentry" + my_base + ".skey=" + skey + "\n");
        sb.append(my_prefix + "winregentry" + my_base + ".name=" + name + "\n");
        sb.append(my_prefix + "winregentry" + my_base + ".value=" + value + "\n");
        return sb.toString();
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<WINREGENTRY");
        sb.append(" NAME=\"" + XmlSupport.toCDATA(name) + "\"");
        sb.append(" HKEY=\"" + XmlSupport.toCDATA(hkey) + "\"");
        sb.append(" SKEY=\"" + XmlSupport.toCDATA(skey) + "\"");
        sb.append(" VALUE=\"" + XmlSupport.toCDATA(value) + "\"");
        sb.append("/>\n");
        return sb.toString();
    }
}
