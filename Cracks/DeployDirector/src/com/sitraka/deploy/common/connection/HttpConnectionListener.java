// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HttpConnectionListener.java

package com.sitraka.deploy.common.connection;


public interface HttpConnectionListener
{

    public abstract void connectionStatusUpdated(String s, boolean flag);

    public abstract void connectionSuccessful(String s);
}
