// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RoleManager.java

package com.sitraka.deploy.common.roles;

import com.sitraka.deploy.AuthGroups;
import com.sitraka.deploy.AuthModelEvent;
import com.sitraka.deploy.AuthModelListener;
import com.sitraka.deploy.AuthorizationModel;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.event.EventListenerList;

// Referenced classes of package com.sitraka.deploy.common.roles:
//            Role, ServerAdminRole

public class RoleManager
    implements AuthorizationModel
{

    private static RoleManager instance;
    protected List roleInstances;
    protected Hashtable emptyRoleInstances;
    protected AuthGroups groupData;
    protected boolean needsCleanUp;
    protected EventListenerList listenerList;

    private RoleManager()
    {
        groupData = null;
        needsCleanUp = false;
        listenerList = new EventListenerList();
        roleInstances = ((List) (new Vector()));
        emptyRoleInstances = new Hashtable();
    }

    public static RoleManager getInstance()
    {
        if(instance == null)
            instance = new RoleManager();
        return instance;
    }

    public void setAuthGroups(AuthGroups groupData)
    {
        this.groupData = groupData;
    }

    public AuthGroups getAuthGroups()
    {
        return groupData;
    }

    public void add(Role role)
    {
        roleInstances.add(((Object) (setCleanUpFlag(role))));
        fireAuthModelChanged();
    }

    public boolean addRoleFromString(String line)
    {
        if(line == null || line.length() == 0)
            return false;
        String className = null;
        String users = null;
        String attributes = null;
        StringTokenizer st = new StringTokenizer(line, ":");
        try
        {
            className = (String)st.nextElement();
            users = (String)st.nextElement();
            attributes = (String)st.nextElement();
        }
        catch(NoSuchElementException nee)
        {
            if(users == null || className == null)
                return false;
        }
        try
        {
            Role role = getRoleInstance(className, getListFromString(users, ","), true);
            if(attributes != null)
                role.setAttributes(getListFromString(attributes, ","));
            setCleanUpFlag(role);
        }
        catch(Exception e)
        {
            return false;
        }
        fireAuthModelChanged();
        return true;
    }

    protected void set(int index, Role role)
    {
        if(index < 0 || index >= roleInstances.size())
        {
            return;
        } else
        {
            roleInstances.set(index, ((Object) (setCleanUpFlag(role))));
            fireAuthModelChanged();
            return;
        }
    }

    public void remove(Role role)
    {
        roleInstances.remove(((Object) (role)));
        fireAuthModelChanged();
    }

    public void removeAllRoles()
    {
        roleInstances = ((List) (new Vector()));
        fireAuthModelChanged();
    }

    public int size()
    {
        cleanUp();
        return roleInstances.size();
    }

    public Role get(int index)
    {
        if(index < 0 || index >= roleInstances.size())
            return null;
        else
            return (Role)roleInstances.get(index);
    }

    public Role getRoleInstance(String className, String user)
    {
        Role role = null;
        try
        {
            ArrayList u = new ArrayList(1);
            u.add(((Object) (user)));
            role = getRoleInstance(className, ((List) (u)), false);
        }
        catch(Exception e) { }
        return role;
    }

    protected Role getRoleInstance(String className, String user, boolean create)
    {
        ArrayList users = new ArrayList(1);
        users.add(((Object) (user)));
        Role role = null;
        try
        {
            role = getRoleInstance(className, ((List) (users)), create);
        }
        catch(Exception e) { }
        return role;
    }

    protected Role getRoleInstance(String className, List users, boolean create)
        throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Role role = null;
        for(Iterator it = roleInstances.iterator(); it.hasNext();)
        {
            role = (Role)it.next();
            if(className.equals(((Object) (((Object) (role)).getClass().getName()))) && role.hasMembers(users))
                return role;
        }

        role = null;
        if(create)
        {
            role = getEmptyRoleInstance(className);
            role.setUsers(users);
            add(role);
        }
        return role;
    }

    protected Role getEmptyRoleInstance(String className)
        throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        Role role = (Role)emptyRoleInstances.get(((Object) (className)));
        if(role == null)
        {
            Object object = Class.forName(className).newInstance();
            if(!(object instanceof Role))
                throw new InstantiationException(className + ": does not implement the Role interface.");
            role = (Role)object;
            emptyRoleInstances.put(((Object) (className)), ((Object) (role)));
        }
        return (Role)role.clone();
    }

    public List getRolesForUser(String user)
    {
        cleanUp();
        ArrayList users = new ArrayList();
        users.add(((Object) (user)));
        users.addAll(((java.util.Collection) (getGroupsForUser(user))));
        ArrayList roles = new ArrayList();
        Role role = null;
        for(int u = 0; u < users.size(); u++)
        {
            for(int r = 0; r < roleInstances.size(); r++)
            {
                role = (Role)roleInstances.get(r);
                if(role.hasMember((String)users.get(u)))
                    roles.add(((Object) (role)));
            }

        }

        return ((List) (roles));
    }

    protected List getGroupsForUser(String user)
    {
        ArrayList results = new ArrayList();
        if(groupData != null)
        {
            List groups = ((List) (groupData.listAllGroups()));
            String group = null;
            for(int i = 0; i < groups.size(); i++)
            {
                group = (String)groups.get(i);
                if(groupData.belongsTo(user, group))
                    results.add(((Object) (group)));
            }

        }
        return ((List) (results));
    }

    public List getRoles()
    {
        cleanUp();
        return roleInstances;
    }

    public boolean isRoleAuthorized(Role role)
    {
        cleanUp();
        if(groupData != null)
        {
            List users = role.getMembers();
            List groups = ((List) (new Vector()));
            for(int i = 0; i < users.size(); i++)
                groups.addAll(((java.util.Collection) (getGroupsForUser((String)users.get(i)))));

            role.addMembers(groups);
        }
        Role other = null;
        for(int i = 0; i < roleInstances.size(); i++)
        {
            other = (Role)roleInstances.get(i);
            if(other.implies(role))
                return true;
        }

        return false;
    }

    public void cleanUp()
    {
        if(!needsCleanUp)
            return;
        int roleCount = roleInstances.size();
        ArrayList deleteRoles = new ArrayList(roleCount);
        Role role = null;
        for(int i = 0; i < roleCount; i++)
        {
            role = (Role)roleInstances.get(i);
            if(role == null || !role.isValid())
                deleteRoles.add(((Object) (role)));
        }

        for(int i = 0; i < deleteRoles.size(); i++)
            roleInstances.remove(deleteRoles.get(i));

        needsCleanUp = false;
        fireAuthModelChanged();
    }

    protected Role setCleanUpFlag(Role role)
    {
        if(role == null || !role.isValid())
            needsCleanUp = true;
        return role;
    }

    public String toString()
    {
        cleanUp();
        StringBuffer sb = new StringBuffer();
        int roleCount = size();
        sb.append(roleCount);
        sb.append('\n');
        for(int i = 0; i < roleCount; i++)
        {
            sb.append(roleInstances.get(i));
            sb.append('\n');
        }

        return sb.toString();
    }

    public List getContexts()
    {
        return ((List) (new ArrayList(0)));
    }

    public List getUsers(Object context)
    {
        ArrayList users = new ArrayList();
        Role role = null;
        String member = null;
        for(Iterator roles = getRoles().iterator(); roles.hasNext();)
        {
            role = (Role)roles.next();
            if(context.equals(((Object) (((Object) (role)).getClass().getName()))))
            {
                for(Iterator members = role.getMembers().iterator(); members.hasNext();)
                {
                    member = (String)members.next();
                    if(!users.contains(((Object) (member))))
                        users.add(((Object) (member)));
                }

            }
        }

        return ((List) (users));
    }

    public List getAccess(Object context, Object user)
    {
        ArrayList access = new ArrayList();
        Role role = getRoleInstance(context.toString(), user.toString());
        if(role != null)
        {
            access.addAll(((java.util.Collection) (role.getAttributes())));
            if(role instanceof ServerAdminRole)
                access.add("Access");
        }
        return ((List) (access));
    }

    public void setAccess(Object context, Object user, List access)
    {
        Role role = getRoleInstance(context.toString(), user.toString(), true);
        if(role == null)
            return;
        if(access == null || access.size() == 0)
        {
            remove(role);
            return;
        }
        if(!(role instanceof ServerAdminRole))
            role.setAttributes(access);
    }

    public void addAuthModelListener(AuthModelListener aml)
    {
        listenerList.add(((Object) (aml)).getClass(), ((java.util.EventListener) (aml)));
    }

    public void removeAuthModelListener(AuthModelListener aml)
    {
        listenerList.remove(((Object) (aml)).getClass(), ((java.util.EventListener) (aml)));
    }

    public void fireAuthModelChanged()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelChanged(e);

    }

    public void fireAuthModelReset()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelReset(e);

    }

    public void fireAuthModelSaved()
    {
        AuthModelEvent e = new AuthModelEvent(((com.sitraka.deploy.AuthModel) (this)));
        Object listeners[] = listenerList.getListenerList();
        for(int i = 0; i < listeners.length; i++)
            if(listeners[i] instanceof AuthModelListener)
                ((AuthModelListener)listeners[i]).modelSaved(e);

    }

    protected static List getListFromString(String string, String delim)
    {
        StringTokenizer st = new StringTokenizer(string, delim);
        int numTokens = st.countTokens();
        ArrayList result = new ArrayList(numTokens);
        String element = null;
        for(int i = 0; i < numTokens; i++)
        {
            element = st.nextElement().toString().trim();
            if(element.length() > 0)
                result.add(((Object) (element)));
        }

        return ((List) (result));
    }
}
