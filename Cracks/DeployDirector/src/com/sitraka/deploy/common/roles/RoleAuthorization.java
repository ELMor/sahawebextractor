// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RoleAuthorization.java

package com.sitraka.deploy.common.roles;

import com.sitraka.deploy.GroupAuthorization;

// Referenced classes of package com.sitraka.deploy.common.roles:
//            Role

public interface RoleAuthorization
    extends GroupAuthorization
{

    public abstract int isRoleAuthorized(Role role);

    public abstract int isRoleAuthorized(String s, Role arole[]);
}
