// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServerAdminRole.java

package com.sitraka.deploy.common.roles;


// Referenced classes of package com.sitraka.deploy.common.roles:
//            Role

public class ServerAdminRole extends Role
{

    public static final String ACCESS_KEYWORD = "Access";

    public ServerAdminRole()
    {
    }

    public ServerAdminRole(String user)
    {
        super(user);
    }

    public boolean implies(Role other)
    {
        return super.implies(other);
    }

    public String toString()
    {
        return super.toString();
    }
}
