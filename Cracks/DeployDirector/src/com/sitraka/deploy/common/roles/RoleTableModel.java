// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RoleTableModel.java

package com.sitraka.deploy.common.roles;

import com.sitraka.deploy.AuthModelEvent;
import com.sitraka.deploy.AuthModelListener;
import java.util.EventObject;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;

// Referenced classes of package com.sitraka.deploy.common.roles:
//            RoleManager, Role

public class RoleTableModel extends AbstractTableModel
    implements AuthModelListener
{

    protected RoleManager manager;

    public RoleTableModel()
    {
    }

    public RoleTableModel(RoleManager manager)
    {
        setRoleManager(manager);
    }

    public void setRoleManager(RoleManager manager)
    {
        this.manager = manager;
        manager.addAuthModelListener(((AuthModelListener) (this)));
    }

    public int getRowCount()
    {
        if(manager == null)
            return 0;
        else
            return manager.roleInstances.size();
    }

    public int getColumnCount()
    {
        return 3;
    }

    public String getColumnName(int columnIndex)
    {
        String name = null;
        if(columnIndex == 0)
            name = "Role";
        else
        if(columnIndex == 1)
            name = "User/Group";
        else
        if(columnIndex == 2)
            name = "Attributes";
        return name;
    }

    public Class getColumnClass(int columnIndex)
    {
        return java.lang.String.class;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex)
    {
        if(manager == null)
            return false;
        if(rowIndex < 0 || rowIndex > getRowCount())
            return false;
        if(columnIndex < 0 || columnIndex > getColumnCount())
            return false;
        Role role = manager.get(rowIndex);
        if(role == null && columnIndex > 0)
            return false;
        return role == null || columnIndex != 0;
    }

    public Object getValueAt(int rowIndex, int columnIndex)
    {
        if(manager == null)
            return ((Object) (null));
        if(rowIndex < 0 || rowIndex > getRowCount())
            return ((Object) (null));
        if(columnIndex < 0 || columnIndex > getColumnCount())
            return ((Object) (null));
        Role role = manager.get(rowIndex);
        if(role == null)
            return ((Object) (""));
        StringTokenizer st = new StringTokenizer(role.toString(), ":");
        int tokenCount = st.countTokens();
        if(columnIndex >= tokenCount)
            return ((Object) (""));
        String fields[] = new String[tokenCount];
        for(int i = 0; i < tokenCount; i++)
            fields[i] = (String)st.nextElement();

        return ((Object) (fields[columnIndex]));
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex)
    {
        if(!isCellEditable(rowIndex, columnIndex))
            return;
        String value = ((String)aValue).trim();
        Role role = manager.get(rowIndex);
        if(columnIndex == 0)
        {
            if(role == null && value != null && value.length() > 0)
                try
                {
                    manager.set(rowIndex, manager.getEmptyRoleInstance(value));
                }
                catch(Exception e) { }
        } else
        if(columnIndex == 1)
            role.setUsers(((List) (getVectorFromString(value, ","))));
        else
        if(columnIndex == 2)
            role.setAttributes(((List) (getVectorFromString(value, ","))));
        ((AbstractTableModel)this).fireTableChanged(new TableModelEvent(((javax.swing.table.TableModel) (this)), rowIndex, rowIndex, columnIndex));
    }

    private static Vector getVectorFromString(String string, String delim)
    {
        StringTokenizer st = new StringTokenizer(string, delim);
        int numTokens = st.countTokens();
        Vector result = new Vector(numTokens);
        String element = null;
        for(int i = 0; i < numTokens; i++)
        {
            element = st.nextElement().toString().trim();
            if(element.length() > 0)
                result.addElement(((Object) (element)));
        }

        return result;
    }

    public void addRow()
    {
        if(manager == null)
        {
            return;
        } else
        {
            manager.add(((Role) (null)));
            int row = getRowCount();
            ((AbstractTableModel)this).fireTableRowsInserted(row, row);
            return;
        }
    }

    public void removeRow(int row)
    {
        if(manager == null)
        {
            return;
        } else
        {
            manager.roleInstances.remove(row);
            ((AbstractTableModel)this).fireTableRowsDeleted(row, row);
            return;
        }
    }

    public void modelChanged(AuthModelEvent e)
    {
        if(((EventObject) (e)).getSource() == manager)
            ((AbstractTableModel)this).fireTableDataChanged();
    }

    public void modelReset(AuthModelEvent e)
    {
        if(((EventObject) (e)).getSource() == manager)
            ((AbstractTableModel)this).fireTableDataChanged();
    }

    public void modelSaved(AuthModelEvent e)
    {
        if(((EventObject) (e)).getSource() == manager)
            ((AbstractTableModel)this).fireTableDataChanged();
    }
}
