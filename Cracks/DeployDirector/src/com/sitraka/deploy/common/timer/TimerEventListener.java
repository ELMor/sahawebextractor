// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TimerEventListener.java

package com.sitraka.deploy.common.timer;


// Referenced classes of package com.sitraka.deploy.common.timer:
//            TimerEvent

public interface TimerEventListener
{

    public abstract void eventOccured(TimerEvent timerevent);

    public abstract void flushOccured(TimerEvent timerevent);
}
