// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   TimerEvent.java

package com.sitraka.deploy.common.timer;

import com.sitraka.deploy.common.jclass.util.JCListenerList;
import java.io.Serializable;
import java.util.Enumeration;

// Referenced classes of package com.sitraka.deploy.common.timer:
//            TimerEventListener

public class TimerEvent
    implements Serializable
{
    class SerializableObject
        implements Serializable
    {

        SerializableObject()
        {
        }
    }


    static final long serialVersionUID = 0x4b137165a1dfae2cL;
    protected transient JCListenerList listeners;
    protected long startTime;
    protected long repeatInterval;
    protected long nextEventTime;
    private final SerializableObject LOCK;

    public TimerEvent(long start_time, long repeat_interval)
    {
        LOCK = new SerializableObject();
        if(start_time == 0L)
            throw new IllegalArgumentException("TimerEvent: start_time may not be 0");
        if(repeat_interval < 0L)
            throw new IllegalArgumentException("TimerEvent: repeat_interval may not be less than 0");
        synchronized(LOCK)
        {
            startTime = start_time;
            nextEventTime = start_time;
            repeatInterval = repeat_interval;
        }
    }

    public TimerEvent(long start_time, long repeat_interval, TimerEventListener l)
    {
        this(start_time, repeat_interval);
        addTimerEventListener(l);
    }

    public long getRepeatInterval()
    {
        return repeatInterval;
    }

    public long getStartTime()
    {
        return startTime;
    }

    public long getNextEventTime()
    {
        long next = 0L;
        synchronized(LOCK)
        {
            next = nextEventTime;
        }
        return next;
    }

    public void addTimerEventListener(TimerEventListener listener)
    {
        listeners = JCListenerList.add(listeners, ((Object) (listener)));
    }

    public void removeTimerEventListener(TimerEventListener listener)
    {
        listeners = JCListenerList.remove(listeners, ((Object) (listener)));
    }

    void flushEvent()
    {
        TimerEventListener listener;
        for(Enumeration e = JCListenerList.elements(listeners); e.hasMoreElements(); listener.flushOccured(this))
            listener = (TimerEventListener)e.nextElement();

    }

    void processEvent()
    {
        TimerEventListener listener;
        for(Enumeration e = JCListenerList.elements(listeners); e.hasMoreElements(); listener.eventOccured(this))
            listener = (TimerEventListener)e.nextElement();

        if(repeatInterval == 0L)
        {
            synchronized(LOCK)
            {
                nextEventTime = 0L;
            }
        } else
        {
            long currentTime = System.currentTimeMillis();
            synchronized(LOCK)
            {
                for(nextEventTime += repeatInterval; nextEventTime <= currentTime; nextEventTime += repeatInterval);
            }
        }
    }

    public String toString()
    {
        String l = "";
        for(Enumeration e = JCListenerList.elements(listeners); e.hasMoreElements();)
        {
            TimerEventListener listener = (TimerEventListener)e.nextElement();
            l = l + listener + ", ";
        }

        String s = "startTime = " + startTime + " repeatInterval = " + repeatInterval + " nextEventTime = " + nextEventTime + " listeners = " + l;
        return s;
    }
}
