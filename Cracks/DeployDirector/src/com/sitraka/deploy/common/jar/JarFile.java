// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JarFile.java

package com.sitraka.deploy.common.jar;

import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.SortUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

// Referenced classes of package com.sitraka.deploy.common.jar:
//            JarEntry

public class JarFile
    implements Serializable
{

    static final long serialVersionUID = 0x6093e9b3acc26402L;
    protected File source;
    protected File dest;
    protected File workingDir;
    protected Hashtable entries;
    protected int compressionMethod;
    protected int compressionLevel;

    public JarFile(File _source)
    {
        this(_source, false);
    }

    public JarFile(File _source, int _level)
    {
        this(_source, false, 8, _level);
    }

    public JarFile(File _source, boolean precompute)
    {
        this(_source, precompute, 8, -1);
    }

    public JarFile(File _source, boolean precompute, int _method, int _level)
    {
        this();
        source = _source;
        setCompressionMethod(_method);
        setCompressionLevel(_level);
        createFromFile(source, precompute);
    }

    public JarFile()
    {
        source = null;
        dest = null;
        workingDir = null;
        entries = null;
        compressionMethod = 8;
        compressionLevel = -1;
        source = null;
        dest = null;
        workingDir = null;
        entries = new Hashtable();
    }

    public JarFile(JarFile _copyFrom)
    {
        source = null;
        dest = null;
        workingDir = null;
        entries = null;
        compressionMethod = 8;
        compressionLevel = -1;
        System.err.println("This method is not yet implemented as I am not usre of the need for it");
    }

    public File getSource()
    {
        return source;
    }

    public void setCompressionLevel(int level)
    {
        compressionLevel = level;
    }

    public int getCompressionLevel()
    {
        return compressionLevel;
    }

    public void setCompressionMethod(int method)
    {
        compressionMethod = method;
    }

    public int getCompressionMethod()
    {
        return compressionMethod;
    }

    public File getDestination()
    {
        return dest;
    }

    public void setDestination(File _dest)
    {
        if(_dest == null)
            return;
        if(workingDir != null)
            dest = new File(workingDir, _dest.getName());
        else
            dest = _dest;
    }

    public void setDestination(String _dest)
    {
        if(_dest == null)
        {
            return;
        } else
        {
            setDestination(new File(_dest));
            return;
        }
    }

    public File getWorkingDir()
    {
        return workingDir;
    }

    public void setWorkingDir(File _workingDir)
    {
        if(_workingDir == null)
        {
            return;
        } else
        {
            workingDir = _workingDir;
            return;
        }
    }

    public void setWorkingDir(String _workingDir)
    {
        if(_workingDir == null)
        {
            return;
        } else
        {
            setWorkingDir(new File(_workingDir));
            return;
        }
    }

    public boolean addEntry(JarEntry entry)
    {
        if(entry == null)
        {
            return false;
        } else
        {
            String name = entry.getName();
            name = FileUtils.makeInternalPath(name);
            entries.put(((Object) (name)), ((Object) (entry)));
            return true;
        }
    }

    public boolean removeEntry(String name)
    {
        if(name == null || !hasEntry(name))
        {
            return true;
        } else
        {
            String new_name = FileUtils.makeInternalPath(name);
            return entries.remove(((Object) (new_name))) != null;
        }
    }

    public JarEntry getEntry(String name)
    {
        if(name == null)
        {
            return null;
        } else
        {
            String new_name = FileUtils.makeInternalPath(name);
            return (JarEntry)entries.get(((Object) (new_name)));
        }
    }

    public Enumeration listClasses()
    {
        if(entries == null)
            return null;
        else
            return entries.keys();
    }

    public boolean hasEntry(String name)
    {
        if(name == null)
        {
            return false;
        } else
        {
            String new_name = FileUtils.makeInternalPath(name);
            return entries.get(((Object) (new_name))) != null;
        }
    }

    protected void createFromFile(File file)
    {
        createFromFile(file, false);
    }

    protected void createFromFile(File file, boolean precompute)
    {
        if(!file.canRead())
            return;
        ZipInputStream zis = null;
        try
        {
            zis = new ZipInputStream(((InputStream) (new FileInputStream(file))));
        }
        catch(FileNotFoundException e)
        {
            return;
        }
        try
        {
            for(ZipEntry e = null; (e = zis.getNextEntry()) != null;)
            {
                JarEntry entry = new JarEntry(e, file);
                if(precompute)
                    entry.computeHashcode(((InputStream) (zis)));
                String name = entry.getName();
                name = FileUtils.makeInternalPath(name);
                entries.put(((Object) (name)), ((Object) (entry)));
            }

            zis.close();
            zis = null;
        }
        catch(ZipException e) { }
        catch(IOException e) { }
    }

    public boolean saveToFile()
    {
        if(dest.exists())
            return false;
        if(dest == null)
            return false;
        try
        {
            ZipOutputStream zos = new ZipOutputStream(((java.io.OutputStream) (new FileOutputStream(dest))));
            int method = getCompressionMethod();
            zos.setMethod(method);
            if(method == 8)
                zos.setLevel(getCompressionLevel());
            Hashtable processed = new Hashtable(entries.size());
            ZipInputStream zis = null;
            if(source != null)
            {
                zis = new ZipInputStream(((InputStream) (new FileInputStream(source))));
                for(ZipEntry entry = null; (entry = zis.getNextEntry()) != null;)
                {
                    JarEntry jarEntry = getEntry(entry.getName());
                    if(jarEntry != null && jarEntry.isFromJar())
                    {
                        if(entry.isDirectory())
                            addToZipStream(zos, (InputStream)null, entry.getName());
                        else
                            addToZipStream(zos, ((InputStream) (zis)), entry.getName());
                        String name = jarEntry.getName();
                        name = FileUtils.makeInternalPath(name);
                        entries.remove(((Object) (name)));
                        processed.put(((Object) (name)), ((Object) (jarEntry)));
                    }
                }

                zis.close();
                zis = null;
            }
            for(Enumeration e = entries.keys(); e.hasMoreElements();)
            {
                JarEntry entry = getEntry((String)e.nextElement());
                InputStream is = entry.getInputStream();
                if(is != null || entry.isDirectory())
                {
                    FileUtils.addToZipStream(zos, is, entry.getName());
                    String name = entry.getName();
                    name = FileUtils.makeInternalPath(name);
                    processed.put(((Object) (name)), ((Object) (entry)));
                }
            }

            entries = processed;
            zos.finish();
            ((FilterOutputStream) (zos)).flush();
            zos.close();
            zos = null;
            return true;
        }
        catch(IOException e)
        {
            return false;
        }
    }

    protected void addToZipStream(ZipOutputStream zipper, InputStream stream, String fileName)
    {
        if(zipper == null)
            return;
        fileName = FileUtils.makeInternalPath(fileName);
        int BUF_SIZE = 8192;
        byte buf[] = new byte[BUF_SIZE];
        try
        {
            if(stream == null && !fileName.endsWith("/"))
                fileName = fileName + "/";
            ZipEntry entry = new ZipEntry(fileName);
            entry.setTime((new Date()).getTime());
            zipper.putNextEntry(entry);
            int i;
            if(stream != null)
                while((i = stream.read(buf, 0, BUF_SIZE)) != -1) 
                    zipper.write(buf, 0, i);
            ((FilterOutputStream) (zipper)).flush();
            zipper.closeEntry();
        }
        catch(IOException e)
        {
            return;
        }
        finally
        {
            buf = null;
        }
    }

    private static void generateXML(String filename)
    {
        System.out.print("\t<FILE NAME=\"" + filename + "\" SOURCE=\"\" DIRECTORY=\"");
        filename = FileUtils.makeSystemPath(filename);
        JarFile file = new JarFile(new File(filename), true);
        System.out.print("false\" SIZE=\"" + file.getSource().length() + "\" HASH=\"");
        System.out.print(Hashcode.computeHash(file.getSource()));
        System.out.println("\">");
        Enumeration e = file.listClasses();
        Vector v = new Vector();
        for(; e.hasMoreElements(); v.addElement(e.nextElement()));
        v = SortUtils.sortStringVector(v);
        JarEntry entry;
        for(e = v.elements(); e.hasMoreElements(); System.out.println("HASH=\"" + entry.getHashcode() + "\"/>"))
        {
            entry = file.getEntry((String)e.nextElement());
            System.out.print("\t\t<FILE NAME=\"" + entry.getName() + "\" SOURCE=\"\" ");
            System.out.print("DIRECTORY=\"" + entry.isDirectory() + "\" SIZE=\"" + entry.getSize() + "\" ");
        }

        System.out.println("\t</FILE>");
    }

    public static void main(String args[])
    {
        if(args.length < 1)
        {
            System.err.println("\nUsage: " + (com.sitraka.deploy.common.jar.JarFile.class).getName() + " jar_file_name\n");
            System.exit(1);
        }
        boolean generateXML = false;
        for(int i = 0; i < args.length; i++)
            if(args[i].toLowerCase().equals("-g") || args[i].toLowerCase().equals("--generate"))
                generateXML = true;

        for(int i = 0; i < args.length; i++)
            if(!args[i].toLowerCase().equals("-g") && !args[i].toLowerCase().equals("--generate") && generateXML)
                generateXML(args[i]);

        System.exit(0);
    }
}
