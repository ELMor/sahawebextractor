// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MimeType.java

package com.sitraka.deploy.common;

import java.util.Hashtable;

public class MimeType
{

    protected static Hashtable map = new Hashtable();

    public MimeType()
    {
    }

    public static String getType(String filename)
    {
        int ind = filename.lastIndexOf('.');
        String ct = null;
        if(ind > 0)
        {
            String type = filename.substring(ind + 1);
            ct = (String)map.get(((Object) (type.toLowerCase())));
        }
        if(ct == null)
            ct = "unknown/unknown";
        return ct;
    }

    private static void setSuffix(String k, String v)
    {
        map.put(((Object) (k)), ((Object) (v)));
    }

    private static void fillMap()
    {
        setSuffix("tmp", "application/zip");
        setSuffix("ai", "application/postscript");
        setSuffix("aif", "audio/x-aiff");
        setSuffix("aifc", "audio/x-aiff");
        setSuffix("aiff", "audio/x-aiff");
        setSuffix("asd", "application/astound");
        setSuffix("asn", "application/astound");
        setSuffix("asp", "application/x-asap");
        setSuffix("au", "audio/basic");
        setSuffix("avi", "video/msvideo");
        setSuffix("avi", "video/x-msvideo");
        setSuffix("bat", "magnus-internal/cgi");
        setSuffix("bin", "application/octet-stream");
        setSuffix("bmp", "image/bmp");
        setSuffix("cacert", "application/x-x509-ca-cert");
        setSuffix("cdf", "application/x-netcdf");
        setSuffix("cgi", "magnus-internal/cgi");
        setSuffix("class", "application/java-vm");
        setSuffix("clp", "application/x-msclip");
        setSuffix("cpio", "application/x-cpio");
        setSuffix("crd", "application/x-mscardfile");
        setSuffix("csh", "application/x-csh");
        setSuffix("css", "application/x-pointplus");
        setSuffix("doc", "application/msword");
        setSuffix("dot", "application/x-dot");
        setSuffix("dvi", "application/x-dvi");
        setSuffix("dwg", "image/vnd");
        setSuffix("ecert", "application/x-x509-email-cert");
        setSuffix("eps", "application/postscript");
        setSuffix("es", "audio/echospeech");
        setSuffix("esl", "audio/echospeech");
        setSuffix("etc", "application/x-earthtime");
        setSuffix("etx", "text/x-setext");
        setSuffix("evy", "application/x-envoy");
        setSuffix("exe", "application/octet-stream");
        setSuffix("fif", "image/fif");
        setSuffix("fm", "application/x-maker");
        setSuffix("fvi", "video/isivideo");
        setSuffix("gif", "image/gif");
        setSuffix("gtar", "application/x-gtar");
        setSuffix("gz", "x-gzip");
        setSuffix("hdf", "application/x-hdf");
        setSuffix("hlp", "application/winhlp");
        setSuffix("hqx", "application/mac-binhex40");
        setSuffix("htm", "text/html");
        setSuffix("html", "text/html");
        setSuffix("ice", "x-conference/x-cooltalk");
        setSuffix("ief", "image/ief");
        setSuffix("ifs", "image/ifs");
        setSuffix("ins", "application/x-NET-Install");
        setSuffix("jar", "application/java-archive");
        setSuffix("jfif", "image/jpeg");
        setSuffix("jpe", "image/jpeg");
        setSuffix("jpeg", "image/jpeg");
        setSuffix("jpg", "image/jpeg");
        setSuffix("js", "application/x-javascript");
        setSuffix("jsc", "application/x-javascript-config");
        setSuffix("lam", "audio/x-liveaudio");
        setSuffix("latex", "application/x-latex");
        setSuffix("lcc", "application/fastman");
        setSuffix("m13", "application/x-msmediaview");
        setSuffix("m14", "application/x-msmediaview");
        setSuffix("man", "application/x-troff-man");
        setSuffix("map", "magnus-internal/imagemap");
        setSuffix("mbd", "application/mbedlet");
        setSuffix("mdb", "application/x-msaccess");
        setSuffix("me", "application/x-troff-me");
        setSuffix("mi", "application/x-mif");
        setSuffix("mid", "audio/midi");
        setSuffix("mid", "audio/x-midi");
        setSuffix("midi", "audio/midi");
        setSuffix("midi", "audio/x-midi");
        setSuffix("mif", ",application/x-mif");
        setSuffix("mny", "application/x-msmoney");
        setSuffix("moc", "application/x-mocha");
        setSuffix("mocha", "application/x-mocha");
        setSuffix("moov", "video/quicktime");
        setSuffix("mov", "video/quicktime");
        setSuffix("movie", "video/x-sgi-movie");
        setSuffix("mp2v", "video/x-mpeg2");
        setSuffix("mpe", "video/mpeg");
        setSuffix("mpeg", "video/mpeg");
        setSuffix("mpegv", "video/mpeg");
        setSuffix("mpg", "video/mpeg");
        setSuffix("mpp", "application/vnd.ms-project");
        setSuffix("mpv", "video/mpeg");
        setSuffix("mpv2", "video/x-mpeg2");
        setSuffix("ms", "application/x-troff-ms");
        setSuffix("nc", "application/x-netcdf");
        setSuffix("oda", "application/oda");
        setSuffix("pac", "audio/x-pac");
        setSuffix("pae", "audio/x-epac");
        setSuffix("pbm", "image/x-portable-bitmap");
        setSuffix("pcd", "image/x-photo-cd");
        setSuffix("pdf", "application/pdf");
        setSuffix("pgm", "image/x-portable-graymap");
        setSuffix("pjp", "image/jpeg");
        setSuffix("pjpeg", "image/jpeg");
        setSuffix("pl", "application/x-perl");
        setSuffix("png", "image/png");
        setSuffix("pnm", "image/x-portable-anymap");
        setSuffix("pot", "application/vnd.ms-powerpoint");
        setSuffix("ppm", "image/x-portable-pixmap");
        setSuffix("pps", "application/vnd.ms-powerpoint");
        setSuffix("ppt", "application/vnd.ms-powerpoint");
        setSuffix("properties", "text/plain");
        setSuffix("proxy", "application/x-ns-proxy-autoconfig");
        setSuffix("ps", "application/postscript");
        setSuffix("pub", "application/x-mspublisher");
        setSuffix("qt", "video/quicktime");
        setSuffix("ra", "audio/x-pn-realaudio");
        setSuffix("ram", "audio/x-pn-realaudio");
        setSuffix("ras", "image/x-cmu-raster");
        setSuffix("rgb", "image/x-rgb");
        setSuffix("roff", "application/x-troff");
        setSuffix("rtf", "application/rtf");
        setSuffix("rtx", "text/richtext");
        setSuffix("scd", "application/x-msschedule");
        setSuffix("scert", "application/x-x509-server-cert");
        setSuffix("ser", "application/java-serialized-object");
        setSuffix("sh", "application/x-sh");
        setSuffix("shar", "application/x-shar");
        setSuffix("shtml", "magnus-internal/parsed-html");
        setSuffix("sit", "application/x-stuffit");
        setSuffix("slc", "application/x-salsa");
        setSuffix("smp", "application/studiom");
        setSuffix("snd", "audio/basic");
        setSuffix("spr", "application/x-sprite");
        setSuffix("sprite", "application/x-sprite");
        setSuffix("src", "application/x-wais-source");
        setSuffix("svf", "image/vnd");
        setSuffix("svr", "x-world/x-svr");
        setSuffix("t", "application/x-troff");
        setSuffix("talk", "text/x-speech");
        setSuffix("tar", "application/x-tar");
        setSuffix("tbp", "application/x-timbuktu");
        setSuffix("tbt", "application/timbuktu");
        setSuffix("tcl", "application/x-tcl");
        setSuffix("tex", "application/x-tex");
        setSuffix("texi", "application/x-texinfo");
        setSuffix("texinfo", "application/x-texinfo");
        setSuffix("tif", "image/tiff");
        setSuffix("tiff", "image/tiff");
        setSuffix("tki", "application/x-tkined");
        setSuffix("tkined", "application/x-tkined");
        setSuffix("tr", "application/x-troff");
        setSuffix("trm", "application/x-msterminal");
        setSuffix("tsv", "text/tab-separated-values");
        setSuffix("txt", "text/plain");
        setSuffix("ucert", "application/x-x509-user-cert");
        setSuffix("uu", "x-uuencode");
        setSuffix("uue", "x-uuencode");
        setSuffix("vbs", "video/mpeg");
        setSuffix("viv", "video/vivo");
        setSuffix("vivo", "video/vivo");
        setSuffix("vrt", "x-world/x-vrt");
        setSuffix("wav", "audio/x-wav");
        setSuffix("wi", "image/wavelet");
        setSuffix("wmf", "application/x-msmetafile");
        setSuffix("wri", "application/x-mswrite");
        setSuffix("wrl", "x-world/x-vrml");
        setSuffix("wv", "video/wavelet");
        setSuffix("xbm", "image/x-xbitmap");
        setSuffix("xla", "application/vnd.ms-excel");
        setSuffix("xlc", "application/vnd.ms-excel");
        setSuffix("xlc", "application/x-excel");
        setSuffix("xll", "application/x-excel");
        setSuffix("xlm", "application/vnd.ms-excel");
        setSuffix("xlm", "application/x-excel");
        setSuffix("xls", "application/vnd.ms-excel");
        setSuffix("xls", "application/x-excel");
        setSuffix("xlt", "application/vnd.ms-excel");
        setSuffix("xlw", "application/vnd.ms-excel");
        setSuffix("xlw", "application/x-excel");
        setSuffix("xpm", "image/x-xpixmap");
        setSuffix("xwd", "image/x-xwindowdump");
        setSuffix("z", "x-compress");
        setSuffix("zip", "application/zip");
    }

    static 
    {
        fillMap();
    }
}
