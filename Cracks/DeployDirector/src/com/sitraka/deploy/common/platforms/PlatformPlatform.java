// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PlatformPlatform.java

package com.sitraka.deploy.common.platforms;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.VersionUtils;
import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.TreeWalker;

// Referenced classes of package com.sitraka.deploy.common.platforms:
//            PlatformJRE

public class PlatformPlatform
{

    protected Vector jres;
    protected Vector platforms;
    protected String name;
    protected PlatformPlatform parent;
    protected String shortName;
    protected boolean deleted;

    public PlatformPlatform()
    {
        jres = new Vector();
        platforms = new Vector();
        name = null;
        parent = null;
        shortName = null;
        deleted = false;
    }

    public PlatformPlatform(Node platform_node, PlatformPlatform parent)
        throws XmlException
    {
        jres = new Vector();
        platforms = new Vector();
        name = null;
        this.parent = null;
        shortName = null;
        deleted = false;
        TreeWalker treeWalker = XmlSupport.createTreeWalker(platform_node);
        this.parent = parent;
        name = XmlSupport.getAttribute(platform_node, "NAME");
        if(name == null)
            throw new XmlException("XML Error: PLATFORM object is missing the NAME tag");
        setShortName(name);
        for(Node node = XmlSupport.getNextElementNamed(treeWalker, "JRE"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "JRE"))
            if(node.getParentNode() == platform_node)
            {
                PlatformJRE j = new PlatformJRE(node, this);
                addJRE(j);
            }

        treeWalker.setCurrentNode(treeWalker.getRoot());
        for(Node node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "PLATFORM"))
            if(node.getParentNode() == platform_node)
            {
                PlatformPlatform p = new PlatformPlatform(node, this);
                addPlatform(p);
            }

    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public Vector getJREList()
    {
        return jres;
    }

    public void setJREList(Vector jreList)
    {
        jres = jreList;
    }

    public boolean hasJRE(String vendor, String version)
    {
        int size = jres.size();
        for(int i = 0; i < size; i++)
        {
            PlatformJRE jre = (PlatformJRE)jres.elementAt(i);
            if(jre.getVendor().equalsIgnoreCase(vendor) && jre.getVersion().equalsIgnoreCase(version))
                return true;
        }

        return false;
    }

    public Vector getPlatformList()
    {
        return platforms;
    }

    public void setPlatformList(Vector platformList)
    {
        platforms = platformList;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getShortName()
    {
        return shortName;
    }

    public void setShortName(String shortName)
    {
        this.shortName = shortName.toLowerCase();
        if(parent != null && parent.getName() != null)
            name = parent.getName() + "." + this.shortName;
        else
            name = this.shortName;
        name = name.toLowerCase();
    }

    public PlatformPlatform getParent()
    {
        return parent;
    }

    public void setParent(PlatformPlatform platform)
    {
        parent = platform;
    }

    public void addPlatform(PlatformPlatform newPlatform)
    {
        if(platforms == null)
            platforms = new Vector();
        platforms.addElement(((Object) (newPlatform)));
        newPlatform.setParent(this);
    }

    public void addJRE(PlatformJRE jre)
    {
        if(jres == null)
            jres = new Vector();
        jres.addElement(((Object) (jre)));
        jre.setPlatform(this);
    }

    public String toString()
    {
        return name;
    }

    public boolean removePlatform(String platform, String vendor, String version)
    {
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
            boolean check = plat.removePlatform(platform, vendor, version);
            if(check)
            {
                if(plat.canRemove() && platforms.elementAt(i).equals(((Object) (plat))))
                    platforms.removeElementAt(i);
                return true;
            }
        }

        if(!platform.equalsIgnoreCase(name))
            return false;
        size = jres != null ? jres.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformJRE j = (PlatformJRE)jres.elementAt(i);
            if(vendor.equalsIgnoreCase(j.getVendor()) && version.equalsIgnoreCase(j.getVersion()))
            {
                jres.removeElementAt(i);
                return true;
            }
        }

        return false;
    }

    protected boolean canRemove()
    {
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
            if(plat.canRemove())
            {
                platforms.removeElementAt(i);
                i--;
                size--;
            }
        }

        size = platforms != null ? platforms.size() : 0;
        if(size > 0)
            return false;
        size = jres != null ? jres.size() : 0;
        return size <= 0;
    }

    public PlatformPlatform findMatchingPlatform(String platform_name)
    {
        if(platform_name == null)
            return null;
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
            PlatformPlatform check = plat.findMatchingPlatform(platform_name);
            if(check != null)
                return check;
        }

        if(name == null)
            return null;
        if(platform_name.startsWith(name))
            return this;
        else
            return null;
    }

    public PlatformPlatform findExactPlatform(String platformName)
    {
        int size = platforms != null ? platforms.size() : 0;
        for(int i = 0; i < size; i++)
        {
            PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
            if(plat.getName().equals(((Object) (platformName))))
                return plat;
        }

        return null;
    }

    public Vector buildJREList(String vendor)
    {
        Vector found = new Vector();
        Vector current = jres;
        PlatformPlatform parent = this.parent;
        while(current != null) 
        {
            int size = current != null ? current.size() : 0;
            for(int i = 0; i < size; i++)
            {
                PlatformJRE jre = (PlatformJRE)current.elementAt(i);
                if((vendor == null || vendor.equalsIgnoreCase(jre.getVendor())) && !found.contains(((Object) (jre))))
                {
                    int insert_pos;
                    for(insert_pos = 0; insert_pos < found.size(); insert_pos++)
                    {
                        PlatformJRE test = (PlatformJRE)found.elementAt(insert_pos);
                        if(VersionUtils.compareVersionNames(test.getVersion(), jre.getVersion()) >= 0)
                            break;
                    }

                    found.insertElementAt(((Object) (jre)), insert_pos);
                }
            }

            if(parent != null)
            {
                current = parent.getJREList();
                parent = parent.getParent();
            } else
            {
                current = null;
            }
        }
        return found;
    }

    public PlatformJRE findMatchingJRE(String version, String vendor)
    {
        PlatformJRE jre = null;
        if(vendor != null && vendor.trim().length() == 0)
            vendor = null;
        Vector matching_jres = buildJREList(vendor);
        jre = (PlatformJRE)VersionUtils.findMatchingVersion(version, ((String) (null)), matching_jres);
        return jre;
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        sb.append(indent_string + "<PLATFORM");
        if(name != null)
            sb.append(" NAME=\"" + XmlSupport.toCDATA(getShortName()) + "\">\n");
        if(jres != null)
        {
            for(int i = 0; i < jres.size(); i++)
            {
                PlatformJRE jre = (PlatformJRE)jres.elementAt(i);
                sb.append(jre.getXML(indent_string));
            }

        }
        if(platforms != null)
        {
            for(int i = 0; i < platforms.size(); i++)
            {
                PlatformPlatform plat = (PlatformPlatform)platforms.elementAt(i);
                sb.append(plat.getXML(indent_string));
            }

        }
        sb.append(indent_string + "</PLATFORM>\n");
        return sb.toString();
    }
}
