// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PlatformsDTD.java

package com.sitraka.deploy.common.platforms;

import java.io.PrintStream;

public class PlatformsDTD
{

    protected static final String DTD_NAME = "Platforms.dtd";
    protected static final String DTD_TEXT = "<!-- DTD for DeployDirector Platforms -->\n<!ELEMENT PLATFORMS (PLATFORM*)>\n\n<!ELEMENT PLATFORM (JRE*,PLATFORM*)>\n<!ATTLIST PLATFORM NAME CDATA #REQUIRED>\n\n<!ELEMENT JRE (CLASSPATH*)>\n<!ATTLIST JRE VENDOR CDATA #REQUIRED>\n<!ATTLIST JRE VERSION CDATA #REQUIRED>\n<!ATTLIST JRE NAME CDATA #REQUIRED>\n<!ATTLIST JRE FORMAT CDATA #REQUIRED>\n\n<!ELEMENT CLASSPATH EMPTY>\n<!ATTLIST CLASSPATH FILE CDATA #REQUIRED>\n";

    public PlatformsDTD()
    {
    }

    public static String getDTD()
    {
        return "<!-- DTD for DeployDirector Platforms -->\n<!ELEMENT PLATFORMS (PLATFORM*)>\n\n<!ELEMENT PLATFORM (JRE*,PLATFORM*)>\n<!ATTLIST PLATFORM NAME CDATA #REQUIRED>\n\n<!ELEMENT JRE (CLASSPATH*)>\n<!ATTLIST JRE VENDOR CDATA #REQUIRED>\n<!ATTLIST JRE VERSION CDATA #REQUIRED>\n<!ATTLIST JRE NAME CDATA #REQUIRED>\n<!ATTLIST JRE FORMAT CDATA #REQUIRED>\n\n<!ELEMENT CLASSPATH EMPTY>\n<!ATTLIST CLASSPATH FILE CDATA #REQUIRED>\n";
    }

    public static String getDTDName()
    {
        return "Platforms.dtd";
    }

    public static void main(String args[])
    {
        System.out.println(getDTD());
    }
}
