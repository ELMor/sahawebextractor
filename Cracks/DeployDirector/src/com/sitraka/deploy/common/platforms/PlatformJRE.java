// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PlatformJRE.java

package com.sitraka.deploy.common.platforms;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.VersionInterface;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Vector;
import org.w3c.dom.Node;

// Referenced classes of package com.sitraka.deploy.common.platforms:
//            PlatformClassPath, PlatformPlatform

public class PlatformJRE
    implements VersionInterface
{

    protected String vendor;
    protected String version;
    protected String name;
    protected String format;
    protected Vector classpath;
    protected boolean serverJRE;
    protected boolean deleted;
    protected PlatformPlatform parent;

    public PlatformJRE()
    {
        vendor = null;
        version = null;
        name = null;
        format = null;
        classpath = new Vector();
        serverJRE = true;
        deleted = false;
        parent = null;
    }

    public PlatformJRE(Node jre_node, PlatformPlatform parent)
        throws XmlException
    {
        vendor = null;
        version = null;
        name = null;
        format = null;
        classpath = new Vector();
        serverJRE = true;
        deleted = false;
        this.parent = null;
        String _value = null;
        _value = XmlSupport.getAttribute(jre_node, "VENDOR");
        if(_value == null)
            throw new XmlException("XML Error: VENDOR tag missing from JRE object");
        vendor = _value.toLowerCase();
        _value = XmlSupport.getAttribute(jre_node, "VERSION").toLowerCase();
        if(_value == null)
            throw new XmlException("XML Error: VERSION tag missing from JRE object");
        version = _value.toLowerCase();
        name = XmlSupport.getAttribute(jre_node, "NAME");
        if(name == null)
            throw new XmlException("XML Error: NAME tag missing from JRE object");
        _value = XmlSupport.getAttribute(jre_node, "FORMAT").toLowerCase();
        if(name == null)
            throw new XmlException("XML Error: FORMAT tag missing from JRE object");
        format = _value.toLowerCase();
        this.parent = parent;
        org.w3c.dom.traversal.TreeWalker treeWalker = XmlSupport.createTreeWalker(jre_node);
        Node node = null;
        for(node = XmlSupport.getNextElementNamed(treeWalker, "CLASSPATH"); node != null; node = XmlSupport.getNextElementNamed(treeWalker, "CLASSPATH"))
            if(node.getParentNode() == jre_node)
            {
                PlatformClassPath p = new PlatformClassPath(node);
                if(classpath == null)
                    classpath = new Vector();
                classpath.addElement(((Object) (p)));
            }

    }

    public String getVendor()
    {
        return vendor;
    }

    public void setVendor(String vendor)
    {
        this.vendor = vendor;
    }

    public void setServerJRE(boolean serverJRE)
    {
        this.serverJRE = serverJRE;
    }

    public boolean isServerJRE()
    {
        return serverJRE;
    }

    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    public boolean isDeleted()
    {
        return deleted;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getSpecificData()
    {
        return vendor;
    }

    public PlatformPlatform getPlatform()
    {
        return parent;
    }

    public void setPlatform(PlatformPlatform platform)
    {
        parent = platform;
    }

    public String getExecutableName()
    {
        return name;
    }

    public void setExecutableName(String name)
    {
        this.name = name;
    }

    public String getFormat()
    {
        return format;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public void setClassPaths(Vector classpaths)
    {
        classpath = classpaths;
    }

    public Vector getClassPaths()
    {
        return classpath;
    }

    public String getClassPathsString()
    {
        StringBuffer sb = new StringBuffer("");
        int size = classpath.size();
        if(size > 0)
        {
            sb.append(((PlatformClassPath)classpath.elementAt(0)).getFileName());
            for(int i = 1; i < size; i++)
                sb.append("," + ((PlatformClassPath)classpath.elementAt(i)).getFileName());

        }
        return sb.toString();
    }

    public String toString()
    {
        StringBuffer b = new StringBuffer();
        b.append(vendor + ", ");
        b.append("version " + version);
        return b.toString();
    }

    public boolean equals(Object o)
    {
        if(o == null || !(o instanceof PlatformJRE))
            return false;
        PlatformJRE jre = (PlatformJRE)o;
        String our_props = getPropertiesFile(0);
        String their_props = jre.getPropertiesFile(0);
        return our_props.equalsIgnoreCase(their_props);
    }

    public void writePropertiesFile(String basedir)
    {
        String prop_file_name = getDirectory() + File.separator + "jre.properties";
        File base = new File(basedir + File.separator + getDirectory());
        if(!base.exists() && !base.mkdirs())
            return;
        File propfile = new File(basedir + File.separator + prop_file_name);
        try
        {
            BufferedWriter writer = new BufferedWriter(((Writer) (new FileWriter(propfile))));
            ((Writer) (writer)).write(getPropertiesFile(-1));
            writer.flush();
            writer.close();
            writer = null;
        }
        catch(IOException e)
        {
            ((Throwable) (e)).printStackTrace();
        }
    }

    public String getPropertiesFile(int base)
    {
        StringBuffer sb = new StringBuffer();
        String basestring = base != -1 ? "" + base : "";
        sb.append("java" + basestring + ".version=" + version + "\n");
        sb.append("java" + basestring + ".vendor=" + vendor + "\n");
        sb.append("java" + basestring + ".name=" + name + "\n");
        sb.append("java" + basestring + ".format=" + format + "\n");
        int size = classpath.size();
        if(size > 0)
        {
            sb.append("java" + basestring + ".classpath=" + ((PlatformClassPath)classpath.elementAt(0)).getFileName());
            for(int i = 1; i < size; i++)
                sb.append("," + ((PlatformClassPath)classpath.elementAt(i)).getFileName());

            sb.append("\n");
        }
        return sb.toString();
    }

    public String getDirectory()
    {
        String parent_dir = FileUtils.convertNameToFile(parent.getName(), '\0');
        String version_string = version;
        return parent_dir + File.separator + vendor + File.separator + version_string;
    }

    public String getXML(String indent)
    {
        StringBuffer sb = new StringBuffer();
        String indent_string = indent + "\t";
        String indent_string2 = indent_string + "\t";
        sb.append(indent_string + "<JRE\n");
        if(vendor != null)
            sb.append(indent_string2 + "VENDOR=\"" + XmlSupport.toCDATA(vendor) + "\"\n");
        if(version != null)
            sb.append(indent_string2 + "VERSION=\"" + XmlSupport.toCDATA(version) + "\"\n");
        if(name != null)
            sb.append(indent_string2 + "NAME=\"" + XmlSupport.toCDATA(name) + "\"\n");
        if(format != null)
            sb.append(indent_string2 + "FORMAT=\"" + XmlSupport.toCDATA(format) + "\"");
        if(classpath != null && classpath.size() > 0)
        {
            sb.append(">\n");
            for(int i = 0; i < classpath.size(); i++)
            {
                PlatformClassPath cp = (PlatformClassPath)classpath.elementAt(i);
                sb.append(cp.getXML(indent_string));
            }

            sb.append(indent_string + "</JRE>\n");
        } else
        {
            sb.append("/>\n");
        }
        return sb.toString();
    }
}
