// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DarFileAccessException.java

package com.sitraka.deploy.common.dar;

import java.io.File;
import java.io.PrintStream;

// Referenced classes of package com.sitraka.deploy.common.dar:
//            DarException

public class DarFileAccessException extends DarException
{

    protected Exception originalException;
    protected File sourceFile;
    protected String sourceFileName;

    public DarFileAccessException(String message)
    {
        super(message);
        originalException = null;
        sourceFile = null;
        sourceFileName = null;
    }

    public DarFileAccessException(String message, Exception ex)
    {
        super(message);
        originalException = null;
        sourceFile = null;
        sourceFileName = null;
        originalException = ex;
    }

    public DarFileAccessException(String message, Exception ex, String file)
    {
        super(message);
        originalException = null;
        sourceFile = null;
        sourceFileName = null;
        originalException = ex;
        sourceFileName = file;
    }

    public DarFileAccessException(String message, Exception ex, File file)
    {
        super(message);
        originalException = null;
        sourceFile = null;
        sourceFileName = null;
        originalException = ex;
        sourceFile = file;
    }

    public Exception getOriginalException()
    {
        return originalException;
    }

    public void setOriginalException(Exception ex)
    {
        originalException = ex;
    }

    public File getSourceFile()
    {
        if(sourceFile == null && sourceFileName != null)
            return new File(sourceFileName);
        else
            return sourceFile;
    }

    public void setSourceFile(File source)
    {
        sourceFile = source;
    }

    public String getSourceFileName()
    {
        if(sourceFileName == null && sourceFile != null)
            return sourceFile.getAbsolutePath();
        else
            return sourceFileName;
    }

    public void setSourceFileName(String name)
    {
        sourceFileName = name;
    }

    public void printDetails()
    {
        printDetails(System.err);
    }

    public void printDetails(PrintStream out)
    {
        out.println(((Throwable)this).getMessage());
        ((Throwable)this).printStackTrace(out);
        if(originalException != null)
        {
            out.println("\nOriginal exception was:");
            out.println(((Throwable) (originalException)).getMessage());
            ((Throwable) (originalException)).printStackTrace(out);
        }
    }
}
