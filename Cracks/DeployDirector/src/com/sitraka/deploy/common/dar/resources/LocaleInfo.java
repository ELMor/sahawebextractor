// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LocaleInfo.java

package com.sitraka.deploy.common.dar.resources;

import java.util.ListResourceBundle;

public class LocaleInfo extends ListResourceBundle
{

    static final Object strings[][] = {
        {
            "dar error app ver not found", "Error, application {0}, version {1} not found in vault."
        }, {
            "dar error creating output", "Unable to create output stream for file {0}."
        }, {
            "dar error creating temp file", "Error creating temporary file."
        }, {
            "dar error creating xml", "Error creating the XML file {0}."
        }, {
            "dar error extracting file", "Error extracting {0} from {1}."
        }, {
            "dar error file not in dar", "Error, file \"{0}\" was not found in the DAR file."
        }, {
            "dar error finding filelist", "Cannot access filelist file \"{0}\"\nPlease make sure the file exists and can be accessed."
        }, {
            "dar error missing argument", "Error, missing argument to \"{0}\"."
        }, {
            "dar error missing command", "Error, you must specify a command action."
        }, {
            "dar error missing command arguments", "Error, command arguments are missing."
        }, {
            "dar error missing dar filename", "Error, the DAR file name is missing."
        }, {
            "dar error no file to retrieve", "Error, no property file was named to retrieve."
        }, {
            "dar error opening dar file", "Error opening DAR file \"{0}\"."
        }, {
            "dar error preparing bundle", "An error occurred preparing DAR file {0}{1}."
        }, {
            "dar error reading filelist", "Cannot read from filelist \"{0}\"."
        }, {
            "dar error reading version xml", "Error reading the stored version.xml."
        }, {
            "dar error updating dar", "Error updating the DAR file."
        }, {
            "dar error writing bundlename", "Error creating the bundlename file {0}."
        }, {
            "dar error writing temp file", "Error writing the temporary DAR file."
        }, {
            "dar error writing zipfile", "Error writing the ZIPped DAR file to {0}."
        }, {
            "dar message bundle", "Bundle: {0}"
        }, {
            "dar message bundle doesnt exist", "Selected bundle does not exist."
        }, {
            "dar message cannot create server command", "Cannot create server command object."
        }, {
            "dar message cannot get http connection", "Cannot get an HTTP connection."
        }, {
            "dar message cannot retrieve server list", "Cannot retrieve proper server list.\nPlease correct cluster and server properties."
        }, {
            "dar message cant connect to any server", "Cannot connect to any server: {0}"
        }, {
            "dar message cant create bundlename file", "Can''t create bundlename.txt in ''{0}''."
        }, {
            "dar message cant create dar file", "Unable to create DAR file ''{0}''."
        }, {
            "dar message cant find bundle name", "Can''t find bundlename.txt in ''{0}''."
        }, {
            "dar message cant find version xml", "Can''t find version.xml in ''{0}''."
        }, {
            "dar message cant read dar file", "Unable to read DAR file ''{0}''."
        }, {
            "dar message cant write to home dir", "Unable to write to DDAdmin home directory."
        }, {
            "dar message dar file", "DAR file: {0}"
        }, {
            "dar message source directory", "Source directory: {0}"
        }, {
            "dar message doesnt exist", " does not exist."
        }, {
            "dar message download dar failed", "Export DAR from server action failed."
        }, {
            "dar message download dar succeeded", "Export DAR from server succeeded."
        }, {
            "dar message error copying bundle", "Error copying the bundle to the http request."
        }, {
            "dar message error loading jnlp", "Error loading JNLP file."
        }, {
            "dar message error parsing cmd exception", "Error parsing command-line:  {0}"
        }, {
            "dar message error parsing command options", "Error parsing command-line options."
        }, {
            "dar message error parsing xml file", "Error parsing XML file ''{0}'':\n{1}"
        }, {
            "dar message error reading xml file", "Error reading XML file ''{0}'':\n{1}"
        }, {
            "dar message error saving bundle", "Error saving exported bundle."
        }, {
            "dar message error saving input stream", "Error saving input-stream data."
        }, {
            "dar message error writing output", "Error copying bundle to standard output."
        }, {
            "dar message failed to init vault", "Error, failed trying to initialize the vault to {0}."
        }, {
            "dar message get dar failed", "Failed to export DAR."
        }, {
            "dar message get dar succeeded", "Export DAR successful."
        }, {
            "dar message missing bundle name", "Missing the bundle name.  Option '-b'"
        }, {
            "dar message missing dar file name", "Missing the DAR file name."
        }, {
            "dar message missing server name", "Missing the server URL.  Option '-u'"
        }, {
            "dar message missing version name", "Missing the version name.  Option '-v'"
        }, {
            "dar message new argument for option", "An argument is required for option ''{0}''."
        }, {
            "dar message no bundle name", "Unable to determine bundle name."
        }, {
            "dar message no version name", "Unable to determine version name."
        }, {
            "dar message only one action allowed", "Only one action command is allowed at a time."
        }, {
            "dar message option not valid", "The option ''{0}'' is not valid."
        }, {
            "dar message password", "Server password: {0}"
        }, {
            "dar message properties not found", "Properties files were not found."
        }, {
            "dar message retrieve file error", "Unable to retrieve: {0}\nError: {1}"
        }, {
            "dar message server", "Server name: {0}"
        }, {
            "dar message source file doesnt exist", "Source file doesn't exist to import."
        }, {
            "dar message template", "Template XML file: {0}"
        }, {
            "dar message unable to access server", "Unable to access server: {0}"
        }, {
            "dar message unable to convert war file", "Unable to convert WAR file to DAR."
        }, {
            "dar message unable to create temp file", "Unable to create temp file."
        }, {
            "dar message unable to extract war file", "Unable to extract WAR file {0}."
        }, {
            "dar message unable to find xml", "Unable to find XML template file {0}."
        }, {
            "dar message unable to load resetting", "Unable to load properties; resetting prop file."
        }, {
            "dar message unable to parse xml", "Unable to parse the provided XML template {0}:\n\t{1}"
        }, {
            "dar message unable to read properties", "Unable to read properties."
        }, {
            "dar message unable to read temp file", "Unable to read temp file."
        }, {
            "dar message unable to read xml", "Unable to access XML template file {0}."
        }, {
            "dar message unable to read zip stream", "Unable to read ZIP stream."
        }, {
            "dar message unable to retrieve properties", "Unable to retrieve properties."
        }, {
            "dar message unable to rtrv prop no prop", "cluster and server properties do not seem to be properly set up on the server."
        }, {
            "dar message unrecognized option", "Unrecognized entry ''{0}'' on command line."
        }, {
            "dar message upload dar failed", "Failed to import DAR."
        }, {
            "dar message upload dar succeeded", "Import DAR successful."
        }, {
            "dar message user", "Server user name: {0}"
        }, {
            "dar message vault dir doesnt exist", "Vault base directory {0} does not exist."
        }, {
            "dar message version", "Version: {0}"
        }, {
            "dar message war conversion error", "Error converting WAR; conversion incomplete"
        }, {
            "dar message war file", "WAR file: {0}"
        }, {
            "dar message war file not found", "Specified WAR file was not found."
        }, {
            "dar message war file not specified", "No WAR file was specified."
        }, {
            "dar message war missing xml tag", "Version ''{0}'' is missing the ''{1}'' tag."
        }, {
            "dar message war xml create error", "Internal error:  Unable to create XmlDocument object for version."
        }, {
            "dar command line help", "Usage: dar [ [ -h | --help ] | [ command [ args ] ] ]\n\t-h | --help    Display this help text\n\t[ command ] is one of \"create,\" \"convert,\" \"import\" or \"export\"\t\"dar [ command ] --help\" will provide command option help\n\tCompatibility mode (tar syntax) is still supported.\n"
        }, {
            "dar convert war command line help", "\ndar convert -w | --war <source war> [ -x | --xml <xml template file> ]\n\t-d | --dar <dar file> [ -b | --bundle <bundle name> ]\n\t[ -v | --version <version name> ]\n\ndar convert -h | --help\n"
        }, {
            "dar create command line help", "\ndar create -b | --bundle <bundle name> [ -v | --version <version name> ]\n\t[ -d | --dar <dar file> ] [ -x | --xml <xml template file> ]\n\t[ -f | --filelist <list file> ] [ -c | --classpath <classpath entry> ]\n\t[ -P | --platform <platform id> ] [ -C | --changedir <source dir> ] <files>\n\ndar create -h | --help\n"
        }, {
            "dar download command line help", "\ndar export -u | --url <server> -U | --user <server username>\n\t-p | --password <server password> -d | --dar <dar file>\n\t[ -b | --bundle <bundle name> ] [ -v | --version <version name> ]\n\ndar export -h | --help\n"
        }, {
            "dar upload command line help", "\ndar import -u | --url <server> -U | --user <server username>\n\t-p | --password <server password> -d | --dar <dar file>\n\t[ -b | --bundle <bundle name> ] [ -v | --version <version name> ]\n\ndar import -u | --url <server> -U | --password <server password>\n\t-p | --password <server password> -w | --war <war file>\n\t[ -x | --xml <xml template file> ] [ -b | --bundle <bundle name> ]\n\t[ -v | --version <version name> ]\n\ndar import -h | --help\n"
        }, {
            "dar create old command line help", "\"dar -c\" creates a DAR file.\nUsage: dar -cf[v0] {file.dar} [ -basedir {dir} ] [ -bundle {app} [ -version {version} ] ]\n\nExamples:\ndar -c test.dar -basedir d:/server/deploydirector -bundle PureJava -version 1.0\ndar -cv0 test.dar -basedir ddinstall/deploydirector -bundle PureJava -version 1.0"
        }, {
            "dar list old command line help", "\"dar -t\" lists the contents of a DAR file.\nUsage: dar -tf {file.dar}\n\nExample: dar -tf test.dar"
        }, {
            "dar extract old command line help", "\"dar -x\" extracts the contents of a DAR file.\nUsage: dar -xf {file.dar} [ file [ file ... ] ]\n\nExamples:\ndar -xf test.dar\ndar -xf test.dar PureJava.java jsse.jar"
        }, {
            "dar update old command line help", "\"dar -u\" updates a DAR file.\nUsage: dar -uf {file.dar} [ file [ file ... ] ]\n\nExample: dar -uf test.dar PureJava.java"
        }, {
            "dar old command line help", "Usage: dar -[ctux][v0]f {file.dar} [ -basedir {dir} ]\n           [ -bundle {app} [ -version {ver} ] ] [ file ... ]\nOptions:\n  -c  create archive\n  -t  list archive contents\n  -u  update archive files\n  -x  extract archive\n  -v  generate verbose output\n  -f  specify DAR file\n  -0  store files in the DAR (no compression)"
        }
    };

    public LocaleInfo()
    {
    }

    public Object[][] getContents()
    {
        return strings;
    }

}
