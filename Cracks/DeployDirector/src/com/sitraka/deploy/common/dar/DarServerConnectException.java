// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DarServerConnectException.java

package com.sitraka.deploy.common.dar;

import java.io.PrintStream;

// Referenced classes of package com.sitraka.deploy.common.dar:
//            DarException

public class DarServerConnectException extends DarException
{

    protected String contextInfo;

    public DarServerConnectException(String message)
    {
        super(message);
        contextInfo = null;
    }

    public void printDetails(PrintStream out)
    {
        out.println(((Throwable)this).getMessage());
    }
}
