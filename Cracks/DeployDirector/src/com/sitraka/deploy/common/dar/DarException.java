// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DarException.java

package com.sitraka.deploy.common.dar;

import java.io.PrintStream;

public abstract class DarException extends Exception
{

    public DarException(String message)
    {
        super(message);
    }

    public void printDetails()
    {
        printDetails(System.err);
    }

    public void printDetails(PrintStream out)
    {
        out.println(((Throwable)this).getMessage());
    }
}
