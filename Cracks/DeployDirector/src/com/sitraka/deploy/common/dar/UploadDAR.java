// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UploadDAR.java

package com.sitraka.deploy.common.dar;

import com.klg.jclass.util.JCLocaleManager;
import com.sitraka.deploy.authentication.AbstractClientHTTPAuthentication;
import com.sitraka.deploy.authentication.ClientUsernamePassword;
import com.sitraka.deploy.common.PlatformIdentifier;
import com.sitraka.deploy.common.connection.AbstractHttpConnection;
import com.sitraka.deploy.common.connection.HttpConnection;
import com.sitraka.deploy.common.connection.HttpConnectionListener;
import com.sitraka.deploy.common.option.OptParser;
import com.sitraka.deploy.common.option.Option;
import com.sitraka.deploy.sam.Configuration;
import com.sitraka.deploy.util.FileUtils;
import com.sitraka.deploy.util.Hashcode;
import com.sitraka.deploy.util.PropertyUtils;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

// Referenced classes of package com.sitraka.deploy.common.dar:
//            DarServerConnectException, DarFileAccessException, DarCommandOptionException, ParseJNLP, 
//            DarXMLException, DarException

public class UploadDAR
    implements HttpConnectionListener
{

    protected static JCLocaleManager li = JCLocaleManager.getDefault();
    protected String commandLineArgs[];
    protected HttpConnection serverCommand;
    protected File homeDir;
    protected Vector servers;
    protected Properties properties;
    protected boolean loggedIn;
    protected boolean darCopy;
    protected String applicationName;
    protected String darName;
    protected String serverUser;
    protected String serverPassword;
    protected String serverName;
    protected String versionName;
    protected String warName;
    protected String xmlTemplate;
    protected static int BUF_SIZE = 8192;

    public UploadDAR()
    {
        JCLocaleManager.getDefault().add("com.sitraka.deploy.common.dar.resources.LocaleInfo");
        String debug = System.getProperty("deploy.debug_on");
        if(debug != null)
            if(!debug.equalsIgnoreCase("true"));
        properties = new Properties();
        loggedIn = false;
        darCopy = false;
        applicationName = null;
        darName = null;
        serverUser = new String("ddadmin");
        serverPassword = null;
        serverName = null;
        versionName = null;
        warName = null;
        xmlTemplate = null;
    }

    public UploadDAR(String args[])
    {
        JCLocaleManager.getDefault().add("com.sitraka.deploy.common.dar.resources.LocaleInfo");
        String debug = System.getProperty("deploy.debug_on");
        if(debug != null)
            if(!debug.equalsIgnoreCase("true"));
        properties = new Properties();
        loggedIn = false;
        darCopy = false;
        applicationName = null;
        darName = null;
        serverUser = new String("ddadmin");
        serverPassword = null;
        serverName = null;
        versionName = null;
        warName = null;
        xmlTemplate = null;
        setArgs(args);
    }

    public void setArgs(String args[])
    {
        commandLineArgs = new String[args.length];
        System.arraycopy(((Object) (args)), 0, ((Object) (commandLineArgs)), 0, args.length);
    }

    public void start()
        throws DarCommandOptionException, DarFileAccessException, DarServerConnectException, DarXMLException
    {
        String adminHome = System.getProperty("admin.home");
        if(adminHome == null)
            adminHome = "";
        homeDir = new File(adminHome);
        if(!homeDir.isDirectory())
            homeDir = new File("");
        homeDir = homeDir.getAbsoluteFile();
        processCommandLine();
    }

    protected void setupServerCommand(String server)
        throws DarFileAccessException, DarServerConnectException
    {
        if(serverCommand != null)
        {
            ((AbstractHttpConnection) (serverCommand)).setServer(server);
            return;
        }
        serverCommand = new HttpConnection();
        if(serverCommand == null)
            throw new DarServerConnectException(li.getString("dar message cannot create server command"));
        ((AbstractHttpConnection) (serverCommand)).resetTags();
        ClientUsernamePassword auth = new ClientUsernamePassword();
        ((AbstractClientHTTPAuthentication) (auth)).setData(serverUser, serverPassword);
        auth.setAllowEditor(false);
        ((AbstractHttpConnection) (serverCommand)).setClientAuthentication(((com.sitraka.deploy.ClientAuthentication) (auth)));
        ((AbstractHttpConnection) (serverCommand)).setUserIDObject(auth.getAuthenticationInfo());
        ((AbstractHttpConnection) (serverCommand)).setApplication(((Object)this).getClass().getName());
        ((AbstractHttpConnection) (serverCommand)).setVersion("2.5.0");
        String machine = null;
        try
        {
            machine = InetAddress.getLocalHost().getHostName();
        }
        catch(UnknownHostException uhe)
        {
            machine = "localhost";
        }
        ((AbstractHttpConnection) (serverCommand)).setClientID(machine);
        ((AbstractHttpConnection) (serverCommand)).setPlatform(PlatformIdentifier.getPlatformString());
        ((AbstractHttpConnection) (serverCommand)).addConnectionListener(((HttpConnectionListener) (this)));
        ((AbstractHttpConnection) (serverCommand)).setIsAdmin(true);
        ((AbstractHttpConnection) (serverCommand)).setUseGUI(false);
        ((AbstractHttpConnection) (serverCommand)).setServerList((Vector)null);
        ((AbstractHttpConnection) (serverCommand)).setServer(server);
    }

    public void loadProperties()
        throws DarFileAccessException
    {
        try
        {
            properties.load(((java.io.InputStream) (new FileInputStream(homeDir.getAbsolutePath() + File.separator + "admin.config"))));
        }
        catch(IOException ioe)
        {
            System.out.println(li.getString("dar message unable to load resetting"));
            try
            {
                properties.store(((OutputStream) (new FileOutputStream(homeDir.getAbsolutePath() + File.separator + "admin.config"))), "Administration Properties");
            }
            catch(IOException ioe2)
            {
                String msg = MessageFormat.format(li.getString("dar message cant write to home dir"), new Object[] {
                    homeDir.getAbsolutePath()
                });
                throw new DarFileAccessException(msg, ((Exception) (ioe2)));
            }
        }
        if(getCurrentServer() == null || getCurrentServer().equals(""))
            serverName = properties.getProperty("admin.lastServer", "");
    }

    public Vector getServers()
    {
        return servers;
    }

    public void setServers(Vector newServers)
    {
        Vector oldServers = servers;
        servers = newServers;
    }

    public void setServersFromProperties(Properties clusterProperties, Properties serverProperties)
        throws DarServerConnectException
    {
        Properties groupedProperties;
        try
        {
            groupedProperties = Configuration.loadConfiguration(clusterProperties, serverProperties);
        }
        catch(IllegalArgumentException e)
        {
            throw new DarServerConnectException(li.getString("dar message unable to rtrv prop no prop"));
        }
        if(groupedProperties == null)
            throw new DarServerConnectException(li.getString("dar message unable to rtrv prop no prop"));
        String serversString = groupedProperties.getProperty("deploy.cluster.processedhosts");
        if(serversString == null)
            throw new DarServerConnectException(li.getString("dar message unable to retrieve properties"));
        Vector serverList = PropertyUtils.readVector(serversString, ',');
        if(serverList == null)
        {
            throw new DarServerConnectException(li.getString("dar message unable to retrieve properties"));
        } else
        {
            setServers(serverList);
            return;
        }
    }

    public String getCurrentServer()
    {
        return serverName;
    }

    protected void uploadDAR(File darFile)
        throws DarCommandOptionException, DarFileAccessException, DarServerConnectException
    {
        uploadDAR(darFile, ((String) (null)), ((String) (null)));
    }

    protected void uploadDAR(File darFile, String bundle, String version)
        throws DarCommandOptionException, DarFileAccessException, DarServerConnectException
    {
        if(!darFile.exists())
            throw new DarFileAccessException(li.getString("dar message source file doesnt exist"));
        ZipFile zip = null;
        if(bundle == null || bundle.length() == 0)
        {
            try
            {
                zip = new ZipFile(darFile);
                ZipEntry ze = zip.getEntry("META-INF/bundlename.txt");
                if(ze == null)
                {
                    String msg = MessageFormat.format(li.getString("dar message cant find bundle name"), new Object[] {
                        darFile.getAbsolutePath()
                    });
                    throw new DarFileAccessException(msg);
                }
                BufferedReader br = new BufferedReader(((java.io.Reader) (new InputStreamReader(zip.getInputStream(ze)))));
                bundle = br.readLine();
                br.close();
            }
            catch(IOException ioe)
            {
                String msg = MessageFormat.format(li.getString("dar message cant find bundle name"), new Object[] {
                    darFile.getAbsolutePath()
                });
                throw new DarFileAccessException(msg, ((Exception) (ioe)));
            }
            if(bundle == null)
                throw new DarCommandOptionException(li.getString("dar message no bundle name"));
            applicationName = bundle;
        }
        if(version == null || version.length() == 0)
        {
            try
            {
                if(zip == null)
                    zip = new ZipFile(darFile);
                ZipEntry ze = zip.getEntry("META-INF/version.xml");
                if(ze == null)
                {
                    String msg = MessageFormat.format(li.getString("dar message cant find version xml"), new Object[] {
                        darFile.getAbsolutePath()
                    });
                    throw new DarFileAccessException(msg);
                }
                BufferedReader br = new BufferedReader(((java.io.Reader) (new InputStreamReader(zip.getInputStream(ze)))));
                String s;
                while((s = br.readLine()) != null) 
                    if(s.startsWith("<VERSION "))
                    {
                        int loc = s.indexOf("NAME=\"");
                        if(loc != -1)
                            version = s.substring(loc + 6, s.indexOf('"', loc + 6));
                        break;
                    }
            }
            catch(IOException ioe)
            {
                String msg = li.getString("dar message no version name");
                throw new DarFileAccessException(msg, ((Exception) (ioe)));
            }
            if(version == null)
                throw new DarCommandOptionException(li.getString("dar message no version name"));
            versionName = version;
        }
        File tempFile = FileUtils.createTempFile();
        if(tempFile == null)
            throw new DarFileAccessException(li.getString("dar message unable to create temp file"));
        FileOutputStream fos;
        try
        {
            fos = new FileOutputStream(tempFile);
        }
        catch(IOException ioe)
        {
            String msg = li.getString("dar message unable to create temp file");
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        String man = "file0.name=" + darFile.getName() + "\nfile0.size=" + darFile.length() + "\nfile0.hash=" + Hashcode.computeHash(darFile) + "\n";
        ZipOutputStream zos = new ZipOutputStream(((OutputStream) (fos)));
        zos.setLevel(0);
        FileUtils.addToZipStream(zos, ((java.io.InputStream) (new ByteArrayInputStream(man.getBytes()))), "deploy.properties");
        FileUtils.addToZipStream(zos, darFile, darFile.getName());
        try
        {
            zos.finish();
            ((FilterOutputStream) (zos)).flush();
            fos.close();
        }
        catch(IOException ioe)
        {
            tempFile.delete();
            String txt1 = bundle == null ? darFile.getName() : "for " + bundle;
            String txt2 = bundle == null || version == null ? "" : ", version " + version;
            String msg = MessageFormat.format(li.getString("dar error preparing bundle"), new Object[] {
                txt1, txt2
            });
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        FileInputStream fis;
        try
        {
            fis = new FileInputStream(tempFile);
        }
        catch(FileNotFoundException fnfe)
        {
            tempFile.delete();
            String msg = li.getString("dar message unable to read temp file");
            throw new DarFileAccessException(msg, ((Exception) (fnfe)));
        }
        ((AbstractHttpConnection) (serverCommand)).resetTags();
        ((AbstractHttpConnection) (serverCommand)).setApplication(bundle);
        ((AbstractHttpConnection) (serverCommand)).setVersion(version);
        ((AbstractHttpConnection) (serverCommand)).setFilename(darFile.getName());
        ((AbstractHttpConnection) (serverCommand)).setServer(getCurrentServer());
        ((AbstractHttpConnection) (serverCommand)).setContentLength((int)tempFile.length());
        OutputStream os = ((AbstractHttpConnection) (serverCommand)).executeUploadCommand("UPLOADDAR");
        if(os == null)
        {
            tempFile.delete();
            String msg = MessageFormat.format(li.getString("dar message unable to access server"), new Object[] {
                ((AbstractHttpConnection) (serverCommand)).getResponseExplanation()
            });
            throw new DarServerConnectException(msg);
        }
        try
        {
            byte buf[] = new byte[BUF_SIZE];
            int i;
            while((i = fis.read(buf)) > 0) 
                os.write(buf, 0, i);
            fis.close();
            tempFile.delete();
        }
        catch(IOException ioe)
        {
            tempFile.delete();
            String msg = li.getString("dar message error copying bundle");
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        int result = serverCommand.finishUploadCommand();
        if(!((AbstractHttpConnection) (serverCommand)).isSuccessful())
        {
            String msg = MessageFormat.format(li.getString("dar message upload dar failed"), new Object[] {
                ((AbstractHttpConnection) (serverCommand)).getResponseExplanation()
            });
            throw new DarServerConnectException(msg);
        } else
        {
            return;
        }
    }

    public Properties downloadPropertyFile(String filename)
        throws DarFileAccessException, DarServerConnectException
    {
        ZipInputStream zis = null;
        if(filename == null || filename.equals(""))
            throw new DarServerConnectException(li.getString("dar error no file to retrieve"));
        try
        {
            ((AbstractHttpConnection) (serverCommand)).resetTags();
            ((AbstractHttpConnection) (serverCommand)).setFilename(filename);
            ((AbstractHttpConnection) (serverCommand)).setPlatform("DeploySam");
            java.io.InputStream is = ((AbstractHttpConnection) (serverCommand)).executeCommand("FILE");
            if(is == null)
            {
                String msg = MessageFormat.format(li.getString("dar message retrieve file error"), new Object[] {
                    filename, ((AbstractHttpConnection) (serverCommand)).getResponseExplanation()
                });
                throw new DarServerConnectException(msg);
            }
            zis = new ZipInputStream(is);
            ZipEntry entry = zis.getNextEntry();
            if(entry != null && entry.getName().equals("deploy.properties"))
                entry = zis.getNextEntry();
            if(entry == null || !entry.getName().equals(((Object) (filename))))
            {
                zis.close();
                zis = null;
                String m = li.getString("dar message unable to read zip stream");
                String msg = MessageFormat.format(li.getString("dar message retrieve file error"), new Object[] {
                    filename, m
                });
                throw new DarServerConnectException(msg);
            }
        }
        catch(IOException ioe)
        {
            String m = li.getString("dar message unable to read properties");
            String msg = MessageFormat.format(li.getString("dar message retrieve file error"), new Object[] {
                filename, m
            });
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        Properties properties = new Properties();
        try
        {
            properties.load(((java.io.InputStream) (zis)));
            zis.close();
        }
        catch(IOException ioe)
        {
            properties = null;
            String m = li.getString("dar message unable to read properties");
            String msg = MessageFormat.format(li.getString("dar message retrieve file error"), new Object[] {
                filename, m
            });
            throw new DarFileAccessException(msg, ((Exception) (ioe)));
        }
        finally
        {
            zis = null;
        }
        return properties;
    }

    protected boolean checkVersion(String application, String version)
    {
        ((AbstractHttpConnection) (serverCommand)).resetTags();
        ((AbstractHttpConnection) (serverCommand)).setApplication(application);
        ((AbstractHttpConnection) (serverCommand)).setVersion(version);
        ((AbstractHttpConnection) (serverCommand)).setServer(getCurrentServer());
        ((AbstractHttpConnection) (serverCommand)).executeTextCommand("VERSIONXML");
        return ((AbstractHttpConnection) (serverCommand)).isSuccessful();
    }

    protected File saveStdin()
        throws DarFileAccessException
    {
        File temp_save = FileUtils.createTempFile((String)null, "dd", ".tmp");
        if(temp_save == null)
            throw new DarFileAccessException(li.getString("dar message unable to create temp file"));
        if(!FileUtils.saveStreamToFile(System.in, temp_save))
        {
            temp_save.delete();
            throw new DarFileAccessException(li.getString("dar message error saving input stream"));
        } else
        {
            return temp_save;
        }
    }

    protected void processCommandLine()
        throws DarCommandOptionException, DarFileAccessException, DarServerConnectException, DarXMLException
    {
        String commandHelp = li.getString("dar upload command line help");
        boolean help = false;
        boolean uploadDAR = false;
        boolean uploadWAR = false;
        if(commandLineArgs.length == 0)
            throw new DarCommandOptionException(commandHelp);
        Option options[] = {
            new Option(false, 1, 'b', "bundle"), new Option(false, 1, 'd', "dar"), new Option(false, -1, 'h', "help"), new Option(false, 1, 'p', "password"), new Option(false, 1, 'u', "url"), new Option(false, 1, 'U', "user"), new Option(false, 1, 'v', "version"), new Option(false, 1, 'w', "war"), new Option(false, 1, 'x', "xml")
        };
        OptParser cl_parse = new OptParser(options, commandLineArgs);
        int i;
        try
        {
            while((i = cl_parse.getOption()) != -1) 
                switch(i)
                {
                case 98: // 'b'
                    applicationName = cl_parse.getOptionArg();
                    break;

                case 100: // 'd'
                    darName = cl_parse.getOptionArg();
                    uploadDAR = true;
                    break;

                case 104: // 'h'
                    help = true;
                    break;

                case 112: // 'p'
                    serverPassword = cl_parse.getOptionArg();
                    break;

                case 117: // 'u'
                    serverName = cl_parse.getOptionArg();
                    break;

                case 85: // 'U'
                    serverUser = cl_parse.getOptionArg();
                    break;

                case 118: // 'v'
                    versionName = cl_parse.getOptionArg();
                    break;

                case 119: // 'w'
                    warName = cl_parse.getOptionArg();
                    uploadWAR = true;
                    break;

                case 120: // 'x'
                    xmlTemplate = cl_parse.getOptionArg();
                    break;

                case 1: // '\001'
                    String msg = MessageFormat.format(li.getString("dar message unrecognized option"), new Object[] {
                        cl_parse.getOptionArg()
                    });
                    throw new DarCommandOptionException(msg);
                }
        }
        catch(IllegalArgumentException iae)
        {
            String message = MessageFormat.format(li.getString("dar message error parsing command options"), new Object[] {
                ((Throwable) (iae)).getMessage()
            });
            throw new DarCommandOptionException(message, commandHelp);
        }
        if(help && uploadDAR || help && uploadWAR || uploadDAR && uploadWAR)
        {
            String msg = li.getString("dar message only one action allowed");
            throw new DarCommandOptionException(msg, commandHelp);
        }
        if(help || !uploadDAR && !uploadWAR)
            throw new DarCommandOptionException(commandHelp);
        if(getCurrentServer() == null)
        {
            String msg = li.getString("dar message missing server name");
            throw new DarCommandOptionException(msg, commandHelp);
        }
        if(serverPassword == null || serverPassword.length() == 0)
            loadCachedPassword();
        setupServerCommand(serverName);
        if(uploadWAR)
        {
            if(warName == null || warName.length() == 0)
            {
                String msg = li.getString("dar message missing war file name");
                throw new DarCommandOptionException(msg, commandHelp);
            }
            if(warName.equals("-"))
            {
                if(applicationName == null || applicationName.length() == 0)
                {
                    String msg = li.getString("dar message missing bundle name");
                    throw new DarCommandOptionException(msg, commandHelp);
                }
                File tempWAR = saveStdin();
                warName = tempWAR.getAbsolutePath();
            }
            if(applicationName == null || applicationName.trim().length() == 0)
            {
                applicationName = warName.substring(warName.lastIndexOf(File.separator) + 1);
                if(warName.endsWith(".war"))
                    applicationName = applicationName.substring(0, applicationName.length() - 4);
            }
            File temp_file = FileUtils.createTempFile((String)null, "dd", ".dar");
            if(temp_file == null)
                throw new DarFileAccessException(li.getString("dar message unable to create temp file"));
            darName = temp_file.getAbsolutePath();
            ParseJNLP parse = new ParseJNLP();
            try
            {
                parse.createDar(darName, warName, applicationName, versionName, xmlTemplate);
            }
            catch(DarFileAccessException dfae)
            {
                temp_file.delete();
                throw dfae;
            }
            catch(DarXMLException dxe)
            {
                temp_file.delete();
                throw dxe;
            }
            darCopy = true;
        }
        File dar_file = null;
        if(darName == null || darName.length() == 0)
        {
            String msg = li.getString("dar message missing dar file name");
            throw new DarCommandOptionException(msg, commandHelp);
        }
        if(darName.equals("-"))
        {
            dar_file = saveStdin();
            darCopy = true;
        } else
        {
            dar_file = new File(darName);
            if(!dar_file.exists() || !dar_file.canRead())
            {
                String msg = MessageFormat.format(li.getString("dar message cant read dar file"), new Object[] {
                    darName
                });
                throw new DarFileAccessException(msg);
            }
        }
        try
        {
            uploadDAR(dar_file, applicationName, versionName);
        }
        finally
        {
            if(darCopy)
                dar_file.delete();
        }
        if(!checkVersion(applicationName, versionName))
            throw new DarServerConnectException(li.getString("dar message upload dar failed"));
        else
            return;
    }

    protected void loadCachedPassword()
    {
        try
        {
            String dir = FileUtils.unifyFileSeparator(homeDir.getAbsolutePath());
            File auth_file = new File(dir, "authentication.dat");
            BufferedReader br = new BufferedReader(((java.io.Reader) (new InputStreamReader(((java.io.InputStream) (new FileInputStream(auth_file)))))));
            serverUser = br.readLine();
            serverPassword = br.readLine();
            br.close();
        }
        catch(IOException ioe) { }
    }

    public void connectionStatusUpdated(String s, boolean flag)
    {
    }

    public void connectionSuccessful(String s)
    {
    }

    public static void main(String args[])
    {
        UploadDAR dartool = new UploadDAR(args);
        try
        {
            dartool.start();
        }
        catch(DarException de)
        {
            de.printDetails();
        }
    }

}
