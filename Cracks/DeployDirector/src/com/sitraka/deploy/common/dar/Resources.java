// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Resources.java

package com.sitraka.deploy.common.dar;

import com.sitraka.deploy.common.XmlException;
import com.sitraka.deploy.common.XmlSupport;
import com.sitraka.deploy.common.app.SystemProperty;
import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Resources
{

    protected Vector files;
    protected Vector properties;
    protected String minVersion;
    protected String os;
    protected String arch;
    protected String locale;

    public Resources()
    {
        properties = new Vector();
        minVersion = null;
        files = new Vector();
    }

    public Resources(Node resources_node)
        throws XmlException
    {
        properties = new Vector();
        minVersion = null;
        files = new Vector();
        os = XmlSupport.getAttribute(resources_node, "os");
        arch = XmlSupport.getAttribute(resources_node, "arch");
        locale = XmlSupport.getAttribute(resources_node, "locale");
        NodeList children = resources_node.getChildNodes();
        for(int i = 0; i < children.getLength(); i++)
        {
            Node child = children.item(i);
            if(child.getNodeType() == 1)
            {
                String name = child.getNodeName();
                if(name.equals("jar"))
                {
                    String filename = XmlSupport.getAttribute(child, "href");
                    if(!filename.startsWith("http://"))
                        files.add(((Object) (filename)));
                } else
                if(name.equals("nativelib"))
                {
                    String filename = XmlSupport.getAttribute(child, "href");
                    if(!filename.startsWith("http://"))
                        files.add(((Object) (filename)));
                } else
                if(name.equals("property"))
                {
                    String prop_name = XmlSupport.getAttribute(child, "name");
                    String prop_value = XmlSupport.getAttribute(child, "value");
                    if(prop_name != null)
                    {
                        SystemProperty new_prop = new SystemProperty();
                        new_prop.setName(prop_name);
                        new_prop.setValue(prop_value);
                        properties.addElement(((Object) (new_prop)));
                    }
                } else
                if(name.equals("j2se"))
                    minVersion = XmlSupport.getAttribute(child, "version");
            }
        }

    }

    public Vector getFiles()
    {
        return files;
    }

    public String getOS()
    {
        return os;
    }

    public String getArch()
    {
        return arch;
    }

    public String getLocale()
    {
        return locale;
    }

    public Vector getProperties()
    {
        return properties;
    }

    public String getMinJREVersion()
    {
        return minVersion;
    }
}
