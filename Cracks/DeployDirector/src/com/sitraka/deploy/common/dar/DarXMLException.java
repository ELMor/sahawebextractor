// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DarXMLException.java

package com.sitraka.deploy.common.dar;

import java.io.PrintStream;

// Referenced classes of package com.sitraka.deploy.common.dar:
//            DarException

public class DarXMLException extends DarException
{

    protected Exception originalException;

    public DarXMLException(String message)
    {
        super(message);
        originalException = null;
    }

    public DarXMLException(String message, Exception ex)
    {
        super(message);
        originalException = null;
        originalException = ex;
    }

    public Exception getOriginalException()
    {
        return originalException;
    }

    public void setOriginalException(Exception ex)
    {
        originalException = ex;
    }

    public void printDetails(PrintStream out)
    {
        out.println(((Throwable)this).getMessage());
        ((Throwable)this).printStackTrace(out);
        out.println("\nOriginal exception was:");
        out.println(((Throwable) (originalException)).getMessage());
        ((Throwable) (originalException)).printStackTrace(out);
    }
}
