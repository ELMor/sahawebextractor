// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SuffixFileFilter.java

package com.sitraka.deploy.common.dar;

import java.io.File;
import java.io.FileFilter;

public class SuffixFileFilter
    implements FileFilter
{

    protected String suffix;

    public SuffixFileFilter()
    {
        suffix = null;
    }

    public SuffixFileFilter(String suffix)
    {
        this.suffix = suffix;
    }

    public boolean accept(File testfile)
    {
        if(testfile == null)
            return false;
        if(suffix == null)
            return true;
        String filename = testfile.getName();
        if(filename == null || filename.length() < suffix.length())
        {
            return false;
        } else
        {
            String terminal = filename.substring(filename.length() - suffix.length());
            return suffix.equalsIgnoreCase(terminal);
        }
    }
}
