// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LoggingEnums.java

package com.sitraka.deploy.common;

import java.util.Hashtable;

public class LoggingEnums
{

    public static final String CLIENT_DATA_RECORD = "ClientDataRecord";
    public static final String SERVER_LOAD_RECORD = "ServerLoadRecord";
    public static final String LOG_CLASS_CLIENT_LOCAL = "client.local";
    public static final String LOG_CLASS_CLIENT_CONNECTION = "client.connection";
    public static final String LOG_CLASS_SERVER_LOCAL = "server.local";
    public static final String LOG_CLASS_SERVER_CONNECTION = "server.connection";
    public static final String LOG_CLASS_ALL_BUNDLES = "bundle.all";
    public static final String LOG_CLASS_ALL = "log.all";
    public static final int LOG_TYPE_CLIENT = 1;
    public static final int LOG_TYPE_SERVER = 2;
    public static final int LOG_TYPE_LOAD = 3;
    public static final int LOG_TYPE_CLIENT_DATA = 4;
    private static Hashtable event_to_error_class = new Hashtable();

    public LoggingEnums()
    {
    }

    private static void setErrorClassCommand(String k, String v)
    {
        event_to_error_class.put(((Object) (k)), ((Object) (v)));
    }

    private static void fillErrorClassMap()
    {
        setErrorClassCommand("ClientAppException", "client.local");
        setErrorClassCommand("ClientFileIOException", "client.local");
        setErrorClassCommand("ClientCannotModifyRegistry", "client.local");
        setErrorClassCommand("ClientCannotCreateShortcut", "client.local");
        setErrorClassCommand("ClientRequestFailed", "client.connection");
        setErrorClassCommand("ServerCannotModifyFile", "server.local");
        setErrorClassCommand("ServerConfigurationError", "server.local");
        setErrorClassCommand("ServerDiskFull", "server.local");
        setErrorClassCommand("ServerExceptionError", "server.local");
        setErrorClassCommand("ServerInternalError", "server.local");
        setErrorClassCommand("ServerLogFailure", "server.local");
        setErrorClassCommand("ServerLicenseError", "server.local");
        setErrorClassCommand("ServerRequestFailed", "server.connection");
        setErrorClassCommand("ServerReplicationDelayed", "server.connection");
        setErrorClassCommand("ServerReplicationRejected", "server.connection");
        setErrorClassCommand("ServerLoginFailed", "server.connection");
        setErrorClassCommand("ClientLoginFailed", "client.connection");
    }

    public static String getEventClass(String event)
    {
        if(!event_to_error_class.containsKey(((Object) (event))))
            return event;
        else
            return (String)event_to_error_class.get(((Object) (event)));
    }

    public static boolean isErrorEvent(String event)
    {
        return event_to_error_class.containsKey(((Object) (event)));
    }

    public static boolean isApplicationEvent(String event)
    {
        return "ClientAppException".equalsIgnoreCase(event);
    }

    static 
    {
        fillErrorClassMap();
    }
}
