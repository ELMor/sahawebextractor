// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PlatformIdentifier.java

package com.sitraka.deploy.common;

import com.sitraka.deploy.PlatformIDInterface;
import java.math.BigDecimal;

public class PlatformIdentifier
    implements PlatformIDInterface
{

    protected static String os_name = null;
    protected static String os_type = null;
    protected static String os_version = null;
    protected static String architecture = null;
    protected static String platform_string = null;
    protected static PlatformIDInterface instance = null;
    protected static String class_name;
    protected static final String UNIX_NAMES[] = {
        "aix", "dg/ux", "dynix", "freebsd", "hp-ux", "hpux", "irix", "linux", "mpe/ix", "netbsd", 
        "solaris", "sunos", "tru64", "unixware"
    };

    private PlatformIdentifier()
    {
    }

    protected static void createInstance()
    {
        if(instance == null || ((Object) (instance)).getClass().getName() != class_name)
        {
            try
            {
                Class c = Class.forName(class_name);
                instance = (PlatformIDInterface)c.newInstance();
            }
            catch(ClassNotFoundException e)
            {
                instance = ((PlatformIDInterface) (new PlatformIdentifier()));
                class_name = (com.sitraka.deploy.common.PlatformIdentifier.class).getName();
            }
            catch(IllegalAccessException e)
            {
                instance = ((PlatformIDInterface) (new PlatformIdentifier()));
                class_name = (com.sitraka.deploy.common.PlatformIdentifier.class).getName();
            }
            catch(InstantiationException e)
            {
                instance = ((PlatformIDInterface) (new PlatformIdentifier()));
                class_name = (com.sitraka.deploy.common.PlatformIdentifier.class).getName();
            }
            os_type = instance.getOSType(os_type);
            os_name = instance.getOSName(os_name);
            os_version = instance.getOSVersion(os_version);
            architecture = instance.getArchitecture(architecture);
        }
    }

    public static String getPlatformString()
    {
        createInstance();
        platform_string = os_type + "." + architecture + "." + os_name + "." + os_version;
        platform_string = platform_string.toLowerCase();
        return platform_string;
    }

    public static void setClassName(String new_class_name)
    {
        class_name = new_class_name;
        createInstance();
    }

    protected boolean isKnownUnixOS(String name)
    {
        for(int i = 0; i < UNIX_NAMES.length; i++)
            if(UNIX_NAMES[i].indexOf(name.toLowerCase()) != -1)
                return true;

        return false;
    }

    public String getOSType(String default_os_type)
    {
        String result = System.getProperty("os.name");
        if(result == null)
            return null;
        result = result.toLowerCase();
        if(result.indexOf("windows") != -1)
            result = "windows";
        else
        if(result.indexOf("mac") != -1)
            result = "apple";
        else
        if(result.indexOf("apple") != -1)
            result = "apple";
        else
        if(result.indexOf("os/2") != -1)
            result = "os2";
        else
        if(isKnownUnixOS(result))
            result = "unix";
        else
            result = "unknown";
        return result;
    }

    public String getOSName(String default_os_name)
    {
        String result = System.getProperty("os.name");
        if(result == null)
            return null;
        result = result.toLowerCase();
        if(result.indexOf("sunos") != -1)
            result = "solaris";
        else
        if(result.indexOf("windows") != -1)
        {
            int index = result.indexOf(' ');
            if(index != -1)
                result = result.substring(index + 1);
        } else
        if(result.indexOf("mac os") != -1)
            result = "macos";
        else
        if(result.indexOf("macos") != -1)
            result = "macos";
        return result;
    }

    public String getOSVersion(String default_os_version)
    {
        String result = System.getProperty("os.version");
        String name = System.getProperty("os.name");
        if(result == null || name == null)
            return null;
        result = result.toLowerCase();
        name = name.toLowerCase();
        if(name.indexOf("sunos") != -1 || name.indexOf("solaris") != -1)
        {
            if(result.equals("2.x"))
                result = "2";
            else
            if(!result.startsWith("2"))
                try
                {
                    String before = result.substring(0, 3);
                    String after = "";
                    if(result.length() > 3)
                        after = result.substring(3);
                    BigDecimal version = new BigDecimal(before);
                    if(version.compareTo(new BigDecimal(5D)) >= 0 && version.compareTo(new BigDecimal(5.7999999999999998D)) <= 0)
                        version = version.subtract(new BigDecimal(3D));
                    result = version.toString() + after;
                }
                catch(NumberFormatException e) { }
        } else
        if(name.indexOf("hp-ux") != -1)
            if(result.startsWith("b."))
                result = result.substring(2);
            else
            if(result.startsWith("b"))
                result = result.substring(1);
        return result;
    }

    public String getArchitecture(String default_architecture)
    {
        String result = System.getProperty("os.arch");
        if(result == null)
            return null;
        result = result.toLowerCase();
        if(result.indexOf("x86") != -1 || result.indexOf("i486") != -1 || result.indexOf("i586") != -1 || result.indexOf("i686") != -1 || result.indexOf("i786") != -1 || result.indexOf("pentium") != -1)
            result = "i386";
        else
        if(result.indexOf("power") != -1)
            result = "powerpc";
        return result;
    }

    static 
    {
        class_name = (com.sitraka.deploy.common.PlatformIdentifier.class).getName();
        createInstance();
    }
}
