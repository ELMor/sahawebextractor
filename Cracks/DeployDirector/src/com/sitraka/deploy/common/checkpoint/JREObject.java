// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JREObject.java

package com.sitraka.deploy.common.checkpoint;

import java.io.Serializable;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            MetaInfo

public class JREObject
    implements Serializable, MetaInfo
{

    protected String remoteServer;
    protected String vendor;
    protected String version;
    protected String platform;
    protected String name;
    protected String format;
    protected String classpaths;

    public JREObject(String remoteServer, String vendor, String version, String platform, String name, String format, String classpaths)
    {
        this.remoteServer = null;
        this.vendor = null;
        this.version = null;
        this.platform = null;
        this.name = null;
        this.format = null;
        this.classpaths = null;
        this.remoteServer = remoteServer;
        this.vendor = vendor;
        this.version = version;
        this.platform = platform;
        this.name = name;
        this.format = format;
        this.classpaths = classpaths;
    }

    public String getServer()
    {
        return remoteServer;
    }

    public String getVendor()
    {
        return vendor;
    }

    public String getVersion()
    {
        return version;
    }

    public String getPlatform()
    {
        return platform;
    }

    public String getName()
    {
        return name;
    }

    public String getFormat()
    {
        return format;
    }

    public String getClasspaths()
    {
        return classpaths;
    }
}
