// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ConfigCheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, ConfigObject

public class ConfigCheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "Config.chk";

    public ConfigCheckpoint()
    {
        this(((Object) (null)));
    }

    public ConfigCheckpoint(Object o)
    {
        super(o);
    }

    public ConfigObject getConfigObject()
    {
        return (ConfigObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
