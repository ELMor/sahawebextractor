// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JRECheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, JREObject

public class JRECheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "JRE.chk";

    public JRECheckpoint()
    {
        this(((Object) (null)));
    }

    public JRECheckpoint(Object o)
    {
        super(o);
    }

    public JREObject getJREObject()
    {
        return (JREObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
