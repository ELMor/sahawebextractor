// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DARCheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, DARObject

public class DARCheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "DAR.chk";

    public DARCheckpoint()
    {
        this(((Object) (null)));
    }

    public DARCheckpoint(Object o)
    {
        super(o);
    }

    public DARObject getDARObject()
    {
        return (DARObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
