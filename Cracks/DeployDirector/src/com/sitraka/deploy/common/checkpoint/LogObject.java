// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LogObject.java

package com.sitraka.deploy.common.checkpoint;

import java.io.Serializable;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            MetaInfo

public class LogObject
    implements Serializable, MetaInfo
{

    protected String remoteServer;
    protected String name;
    protected String from;
    protected String to;

    public LogObject(String remoteServer, String name, String from, String to)
    {
        this.remoteServer = null;
        this.name = null;
        this.from = null;
        this.to = null;
        this.remoteServer = remoteServer;
        this.name = name;
        this.from = from;
        this.to = to;
    }

    public String getServer()
    {
        return remoteServer;
    }

    public String getName()
    {
        return name;
    }

    public String getFrom()
    {
        return from;
    }

    public String getTo()
    {
        return to;
    }
}
