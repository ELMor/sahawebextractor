// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   InstallerCheckpoint.java

package com.sitraka.deploy.common.checkpoint;


// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            BaseCheckpoint, InstallerObject

public class InstallerCheckpoint extends BaseCheckpoint
{

    protected static String SerializeFileName = "Installer.chk";

    public InstallerCheckpoint()
    {
        this(((Object) (null)));
    }

    public InstallerCheckpoint(Object o)
    {
        super(o);
    }

    public InstallerObject getInstallerObject()
    {
        return (InstallerObject)((BaseCheckpoint)this).getUserData();
    }

    public String getDefaultExtension()
    {
        return SerializeFileName;
    }

}
