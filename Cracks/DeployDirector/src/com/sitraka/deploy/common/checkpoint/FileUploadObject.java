// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FileUploadObject.java

package com.sitraka.deploy.common.checkpoint;

import java.io.Serializable;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            MetaInfo

public class FileUploadObject
    implements Serializable, MetaInfo
{

    protected String remoteServer;
    protected String filename;

    public FileUploadObject(String remoteServer, String filename)
    {
        this.remoteServer = null;
        this.filename = null;
        this.remoteServer = remoteServer;
        this.filename = filename;
    }

    public String getServer()
    {
        return remoteServer;
    }

    public String getFilename()
    {
        return filename;
    }
}
