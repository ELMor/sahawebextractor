// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DARObject.java

package com.sitraka.deploy.common.checkpoint;

import java.io.Serializable;

// Referenced classes of package com.sitraka.deploy.common.checkpoint:
//            MetaInfo

public class DARObject
    implements Serializable, MetaInfo
{

    protected String remoteServer;
    protected String darName;
    protected String application;
    protected String version;

    public DARObject(String remoteServer, String darName, String application, String version)
    {
        this.remoteServer = null;
        this.darName = null;
        this.application = null;
        this.version = null;
        this.remoteServer = remoteServer;
        this.darName = darName;
        this.application = application;
        this.version = version;
    }

    public String getServer()
    {
        return remoteServer;
    }

    public String getDARName()
    {
        return darName;
    }

    public String getApplication()
    {
        return application;
    }

    public String getVersion()
    {
        return version;
    }
}
