// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JCStringTokenizer.java

package com.sitraka.deploy.common.jclass.util;


public class JCStringTokenizer
{

    private int index;
    private String string;
    private int length;
    private boolean count;
    public boolean strip_esc;
    protected char escape_char;
    private char token[];

    public JCStringTokenizer(String s)
    {
        count = false;
        strip_esc = true;
        escape_char = '\\';
        if(s != null)
        {
            string = s.trim();
            length = string.length();
        }
    }

    public static String[] parse(String s, char delim)
    {
        return parse(s, delim, '\\');
    }

    public static String[] parse(String s, char delim, char escape_char)
    {
        JCStringTokenizer t = new JCStringTokenizer(s);
        t.escape_char = escape_char;
        String list[] = new String[t.countTokens(delim)];
        for(int i = 0; i < list.length; i++)
        {
            list[i] = t.nextToken(delim);
            if(list[i] == null)
                list[i] = "";
        }

        return list;
    }

    public char getEscapeChar()
    {
        return escape_char;
    }

    public void setEscapeChar(char c)
    {
        escape_char = c;
    }

    public String nextToken()
    {
        if(string == null || index >= length)
            return null;
        int i;
        for(i = index; i < length && Character.isWhitespace(string.charAt(i)); i++);
        index = i;
        if(i >= length)
            return null;
        if(!count)
            token = new char[length + 1];
        i = index;
        int k = 0;
        for(; i < length; i++)
        {
            if(Character.isWhitespace(string.charAt(i)))
                break;
            if(!count)
                token[k++] = string.charAt(i);
        }

        index = i + 1;
        return count ? null : (new String(token)).trim();
    }

    public String nextToken(char delim)
    {
        if(string == null || index >= length)
            return null;
        if(!count)
            token = new char[length + 1];
        int i = index;
        int k;
        for(k = 0; i < length; k++)
        {
            if(escape_char != 0 && i + 1 < length && string.charAt(i) == escape_char)
            {
                if(!strip_esc)
                {
                    if(token != null)
                    {
                        token[k++] = string.charAt(i);
                        token[k] = string.charAt(++i);
                    }
                } else
                {
                    i++;
                    if(!count)
                        if(string.charAt(i) == 'n')
                            token[k] = '\n';
                        else
                            token[k] = string.charAt(i);
                }
            } else
            {
                if(string.charAt(i) == delim)
                    break;
                if(!count)
                    token[k] = string.charAt(i);
            }
            i++;
        }

        index = i + 1;
        return count || k <= 0 ? null : new String(token, 0, k);
    }

    public int countTokens(char delim)
    {
        int old_index = index;
        count = true;
        int num;
        for(num = 0; index < length; num++)
            nextToken(delim);

        index = old_index;
        count = false;
        return num;
    }

    public boolean hasMoreTokens()
    {
        return index < length;
    }

    public int getPosition()
    {
        return index;
    }
}
