// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthGroups.java

package com.sitraka.deploy;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

public interface AuthGroups
{

    public static final String AUTHGROUPS_FILENAME = "authgroups.dat";

    public abstract Vector listAllGroups();

    public abstract boolean isGroup(String s);

    public abstract Vector getGroupMembers(String s);

    public abstract Vector listAllMembers(String s);

    public abstract boolean belongsTo(String s, String s1);

    public abstract boolean usesDataFile();

    public abstract File getDataFile();

    public abstract void setDataFile(File file)
        throws IOException;

    public abstract boolean hasEditor();

    public abstract Component getEditorComponent();

    public abstract boolean isModified();

    public abstract boolean commitChanges();
}
