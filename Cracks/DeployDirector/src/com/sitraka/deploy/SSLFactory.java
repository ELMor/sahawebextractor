// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SSLFactory.java

package com.sitraka.deploy;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public interface SSLFactory
{

    public abstract boolean acceptsOpenSocket();

    public abstract boolean supportsProxy();

    public abstract Socket getSSLSocket(Socket socket, String s, int i)
        throws IOException;

    public abstract Socket getSSLSocket(InetAddress inetaddress, int i, String s)
        throws IOException;

    public abstract boolean needsSSLHandshake();

    public abstract boolean startSSLHandshake(Socket socket)
        throws IOException;
}
