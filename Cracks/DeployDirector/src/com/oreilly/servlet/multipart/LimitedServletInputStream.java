// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LimitedServletInputStream.java

package com.oreilly.servlet.multipart;

import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletInputStream;

public class LimitedServletInputStream extends ServletInputStream
{

    private ServletInputStream in;
    private int totalExpected;
    private int totalRead;

    public LimitedServletInputStream(ServletInputStream in, int totalExpected)
    {
        totalRead = 0;
        this.in = in;
        this.totalExpected = totalExpected;
    }

    public int readLine(byte b[], int off, int len)
        throws IOException
    {
        int left = totalExpected - totalRead;
        if(left <= 0)
            return -1;
        int result = in.readLine(b, off, Math.min(left, len));
        if(result > 0)
            totalRead += result;
        return result;
    }

    public int read()
        throws IOException
    {
        if(totalRead >= totalExpected)
            return -1;
        else
            return ((InputStream) (in)).read();
    }

    public int read(byte b[], int off, int len)
        throws IOException
    {
        int left = totalExpected - totalRead;
        if(left <= 0)
            return -1;
        int result = ((InputStream) (in)).read(b, off, Math.min(left, len));
        if(result > 0)
            totalRead += result;
        return result;
    }
}
