// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   FilePart.java

package com.oreilly.servlet.multipart;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.ServletInputStream;

// Referenced classes of package com.oreilly.servlet.multipart:
//            Part, PartInputStream, MacBinaryDecoderOutputStream

public class FilePart extends Part
{

    private String fileName;
    private String filePath;
    private String contentType;
    private PartInputStream partInput;

    FilePart(String name, ServletInputStream in, String boundary, String contentType, String fileName, String filePath)
        throws IOException
    {
        super(name);
        this.fileName = fileName;
        this.filePath = filePath;
        this.contentType = contentType;
        partInput = new PartInputStream(in, boundary);
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public String getContentType()
    {
        return contentType;
    }

    public InputStream getInputStream()
    {
        return ((InputStream) (partInput));
    }

    public long writeTo(File fileOrDirectory)
        throws IOException
    {
        long written = 0L;
        OutputStream fileOut = null;
        try
        {
            if(fileName != null)
            {
                File file;
                if(fileOrDirectory.isDirectory())
                    file = new File(fileOrDirectory, fileName);
                else
                    file = fileOrDirectory;
                fileOut = ((OutputStream) (new BufferedOutputStream(((OutputStream) (new FileOutputStream(file))))));
                written = write(fileOut);
            }
        }
        finally
        {
            if(fileOut != null)
                fileOut.close();
        }
        return written;
    }

    public long writeTo(OutputStream out)
        throws IOException
    {
        long size = 0L;
        if(fileName != null)
            size = write(out);
        return size;
    }

    long write(OutputStream out)
        throws IOException
    {
        if(contentType.equals("application/x-macbinary"))
            out = ((OutputStream) (new MacBinaryDecoderOutputStream(out)));
        long size = 0L;
        byte buf[] = new byte[8192];
        int i;
        while((i = partInput.read(buf)) != -1) 
        {
            out.write(buf, 0, i);
            size += i;
        }
        return size;
    }

    public boolean isFile()
    {
        return true;
    }
}
