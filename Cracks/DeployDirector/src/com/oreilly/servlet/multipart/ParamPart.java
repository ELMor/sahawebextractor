// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ParamPart.java

package com.oreilly.servlet.multipart;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.servlet.ServletInputStream;

// Referenced classes of package com.oreilly.servlet.multipart:
//            Part, PartInputStream

public class ParamPart extends Part
{

    private byte value[];

    ParamPart(String name, ServletInputStream in, String boundary)
        throws IOException
    {
        super(name);
        PartInputStream pis = new PartInputStream(in, boundary);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
        byte buf[] = new byte[128];
        int i;
        while((i = pis.read(buf)) != -1) 
            baos.write(buf, 0, i);
        pis.close();
        baos.close();
        value = baos.toByteArray();
    }

    public byte[] getValue()
    {
        return value;
    }

    public String getStringValue()
        throws UnsupportedEncodingException
    {
        return getStringValue("ISO-8859-1");
    }

    public String getStringValue(String encoding)
        throws UnsupportedEncodingException
    {
        return new String(value, encoding);
    }

    public boolean isParam()
    {
        return true;
    }
}
