// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package org.apache.xml.serialize;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.xerces.dom.DOMMessageFormatter;
import org.apache.xerces.dom3.DOMErrorHandler;
import org.apache.xerces.util.NamespaceSupport;
import org.apache.xerces.util.SymbolTable;
import org.apache.xerces.util.XMLChar;
import org.apache.xerces.util.XMLSymbols;
import org.apache.xerces.xni.NamespaceContext;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.AttributeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

// Referenced classes of package org.apache.xml.serialize:
//            BaseMarkupSerializer, OutputFormat, ElementState, Printer, 
//            EncodingInfo

public class XMLSerializer extends BaseMarkupSerializer
{

    protected static final boolean DEBUG = false;
    protected NamespaceSupport fNSBinder;
    protected NamespaceSupport fLocalNSBinder;
    protected SymbolTable fSymbolTable;
    protected boolean fDOML1;
    protected int fNamespaceCounter;
    protected static final String PREFIX = "NS";
    protected boolean fNamespaces;
    private boolean fPreserveSpace;

    public XMLSerializer()
    {
        super(new OutputFormat("xml", ((String) (null)), false));
        fDOML1 = false;
        fNamespaceCounter = 1;
        fNamespaces = false;
    }

    public XMLSerializer(OutputFormat outputformat)
    {
        super(outputformat == null ? new OutputFormat("xml", ((String) (null)), false) : outputformat);
        fDOML1 = false;
        fNamespaceCounter = 1;
        fNamespaces = false;
        super._format.setMethod("xml");
    }

    public XMLSerializer(Writer writer, OutputFormat outputformat)
    {
        super(outputformat == null ? new OutputFormat("xml", ((String) (null)), false) : outputformat);
        fDOML1 = false;
        fNamespaceCounter = 1;
        fNamespaces = false;
        super._format.setMethod("xml");
        ((BaseMarkupSerializer)this).setOutputCharStream(writer);
    }

    public XMLSerializer(OutputStream outputstream, OutputFormat outputformat)
    {
        super(outputformat == null ? new OutputFormat("xml", ((String) (null)), false) : outputformat);
        fDOML1 = false;
        fNamespaceCounter = 1;
        fNamespaces = false;
        super._format.setMethod("xml");
        ((BaseMarkupSerializer)this).setOutputByteStream(outputstream);
    }

    public void setOutputFormat(OutputFormat outputformat)
    {
        super.setOutputFormat(outputformat == null ? new OutputFormat("xml", ((String) (null)), false) : outputformat);
    }

    /**
     * @deprecated Method setNamespaces is deprecated
     */

    public void setNamespaces(boolean flag)
    {
        fNamespaces = flag;
        fNSBinder = new NamespaceSupport();
        fLocalNSBinder = new NamespaceSupport();
        fSymbolTable = new SymbolTable();
    }

    public void startElement(String s, String s1, String s2, Attributes attributes)
        throws SAXException
    {
        boolean flag1 = false;
        try
        {
            if(super._printer == null)
            {
                String s8 = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "NoWriterSupplied", ((Object []) (null)));
                throw new IllegalStateException(s8);
            }
            ElementState elementstate = ((BaseMarkupSerializer)this).getElementState();
            if(((BaseMarkupSerializer)this).isDocumentState())
            {
                if(!super._started)
                    startDocument(s1 != null && s1.length() != 0 ? s1 : s2);
            } else
            {
                if(elementstate.empty)
                    super._printer.printText('>');
                if(elementstate.inCData)
                {
                    super._printer.printText("]]>");
                    elementstate.inCData = false;
                }
                if(super._indenting && !elementstate.preserveSpace && (elementstate.empty || elementstate.afterElement || elementstate.afterComment))
                    super._printer.breakLine();
            }
            boolean flag = elementstate.preserveSpace;
            attributes = extractNamespaces(attributes);
            if(s2 == null || s2.length() == 0)
            {
                if(s1 == null)
                {
                    String s9 = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "NoName", ((Object []) (null)));
                    throw new SAXException(s9);
                }
                if(s != null && !s.equals(""))
                {
                    String s10 = ((BaseMarkupSerializer)this).getPrefix(s);
                    if(s10 != null && s10.length() > 0)
                        s2 = s10 + ":" + s1;
                    else
                        s2 = s1;
                } else
                {
                    s2 = s1;
                }
                boolean flag2 = true;
            }
            super._printer.printText('<');
            super._printer.printText(s2);
            super._printer.indent();
            if(attributes != null)
            {
                for(int i = 0; i < attributes.getLength(); i++)
                {
                    super._printer.printSpace();
                    String s3 = attributes.getQName(i);
                    if(s3 != null && s3.length() == 0)
                    {
                        s3 = attributes.getLocalName(i);
                        String s12 = attributes.getURI(i);
                        if(s12 != null && s12.length() != 0 && (s == null || s.length() == 0 || !s12.equals(((Object) (s)))))
                        {
                            String s11 = ((BaseMarkupSerializer)this).getPrefix(s12);
                            if(s11 != null && s11.length() > 0)
                                s3 = s11 + ":" + s3;
                        }
                    }
                    String s6 = attributes.getValue(i);
                    if(s6 == null)
                        s6 = "";
                    super._printer.printText(s3);
                    super._printer.printText("=\"");
                    printEscaped(s6);
                    super._printer.printText('"');
                    if(s3.equals("xml:space"))
                        if(s6.equals("preserve"))
                            flag = true;
                        else
                            flag = super._format.getPreserveSpace();
                }

            }
            if(super._prefixes != null)
            {
                for(Enumeration enumeration = super._prefixes.keys(); enumeration.hasMoreElements();)
                {
                    super._printer.printSpace();
                    String s7 = (String)enumeration.nextElement();
                    String s4 = (String)super._prefixes.get(((Object) (s7)));
                    if(s4.length() == 0)
                    {
                        super._printer.printText("xmlns=\"");
                        printEscaped(s7);
                        super._printer.printText('"');
                    } else
                    {
                        super._printer.printText("xmlns:");
                        super._printer.printText(s4);
                        super._printer.printText("=\"");
                        printEscaped(s7);
                        super._printer.printText('"');
                    }
                }

            }
            elementstate = ((BaseMarkupSerializer)this).enterElementState(s, s1, s2, flag);
            String s5 = s1 != null && s1.length() != 0 ? s + "^" + s1 : s2;
            elementstate.doCData = super._format.isCDataElement(s5);
            elementstate.unescaped = super._format.isNonEscapingElement(s5);
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void endElement(String s, String s1, String s2)
        throws SAXException
    {
        try
        {
            endElementIO(s, s1, s2);
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void endElementIO(String s, String s1, String s2)
        throws IOException
    {
        super._printer.unindent();
        ElementState elementstate = ((BaseMarkupSerializer)this).getElementState();
        if(elementstate.empty)
        {
            super._printer.printText("/>");
        } else
        {
            if(elementstate.inCData)
                super._printer.printText("]]>");
            if(super._indenting && !elementstate.preserveSpace && (elementstate.afterElement || elementstate.afterComment))
                super._printer.breakLine();
            super._printer.printText("</");
            super._printer.printText(elementstate.rawName);
            super._printer.printText('>');
        }
        elementstate = ((BaseMarkupSerializer)this).leaveElementState();
        elementstate.afterElement = true;
        elementstate.afterComment = false;
        elementstate.empty = false;
        if(((BaseMarkupSerializer)this).isDocumentState())
            super._printer.flush();
    }

    public void startElement(String s, AttributeList attributelist)
        throws SAXException
    {
        try
        {
            if(super._printer == null)
            {
                String s3 = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "NoWriterSupplied", ((Object []) (null)));
                throw new IllegalStateException(s3);
            }
            ElementState elementstate = ((BaseMarkupSerializer)this).getElementState();
            if(((BaseMarkupSerializer)this).isDocumentState())
            {
                if(!super._started)
                    startDocument(s);
            } else
            {
                if(elementstate.empty)
                    super._printer.printText('>');
                if(elementstate.inCData)
                {
                    super._printer.printText("]]>");
                    elementstate.inCData = false;
                }
                if(super._indenting && !elementstate.preserveSpace && (elementstate.empty || elementstate.afterElement || elementstate.afterComment))
                    super._printer.breakLine();
            }
            boolean flag = elementstate.preserveSpace;
            super._printer.printText('<');
            super._printer.printText(s);
            super._printer.indent();
            if(attributelist != null)
            {
                for(int i = 0; i < attributelist.getLength(); i++)
                {
                    super._printer.printSpace();
                    String s1 = attributelist.getName(i);
                    String s2 = attributelist.getValue(i);
                    if(s2 != null)
                    {
                        super._printer.printText(s1);
                        super._printer.printText("=\"");
                        printEscaped(s2);
                        super._printer.printText('"');
                    }
                    if(s1.equals("xml:space"))
                        if(s2.equals("preserve"))
                            flag = true;
                        else
                            flag = super._format.getPreserveSpace();
                }

            }
            elementstate = ((BaseMarkupSerializer)this).enterElementState(((String) (null)), ((String) (null)), s, flag);
            elementstate.doCData = super._format.isCDataElement(s);
            elementstate.unescaped = super._format.isNonEscapingElement(s);
        }
        catch(IOException ioexception)
        {
            throw new SAXException(((Exception) (ioexception)));
        }
    }

    public void endElement(String s)
        throws SAXException
    {
        endElement(((String) (null)), ((String) (null)), s);
    }

    protected void startDocument(String s)
        throws IOException
    {
        String s1 = super._printer.leaveDTD();
        if(!super._started)
        {
            if(!super._format.getOmitXMLDeclaration())
            {
                StringBuffer stringbuffer = new StringBuffer("<?xml version=\"");
                if(super._format.getVersion() != null)
                    stringbuffer.append(super._format.getVersion());
                else
                    stringbuffer.append("1.0");
                stringbuffer.append('"');
                String s2 = super._format.getEncoding();
                if(s2 != null)
                {
                    stringbuffer.append(" encoding=\"");
                    stringbuffer.append(s2);
                    stringbuffer.append('"');
                }
                if(super._format.getStandalone() && super._docTypeSystemId == null && super._docTypePublicId == null)
                    stringbuffer.append(" standalone=\"yes\"");
                stringbuffer.append("?>");
                super._printer.printText(stringbuffer);
                super._printer.breakLine();
            }
            if(!super._format.getOmitDocumentType())
                if(super._docTypeSystemId != null)
                {
                    super._printer.printText("<!DOCTYPE ");
                    super._printer.printText(s);
                    if(super._docTypePublicId != null)
                    {
                        super._printer.printText(" PUBLIC ");
                        ((BaseMarkupSerializer)this).printDoctypeURL(super._docTypePublicId);
                        if(super._indenting)
                        {
                            super._printer.breakLine();
                            for(int i = 0; i < 18 + s.length(); i++)
                                super._printer.printText(" ");

                        } else
                        {
                            super._printer.printText(" ");
                        }
                        ((BaseMarkupSerializer)this).printDoctypeURL(super._docTypeSystemId);
                    } else
                    {
                        super._printer.printText(" SYSTEM ");
                        ((BaseMarkupSerializer)this).printDoctypeURL(super._docTypeSystemId);
                    }
                    if(s1 != null && s1.length() > 0)
                    {
                        super._printer.printText(" [");
                        printText(s1, true, true);
                        super._printer.printText(']');
                    }
                    super._printer.printText(">");
                    super._printer.breakLine();
                } else
                if(s1 != null && s1.length() > 0)
                {
                    super._printer.printText("<!DOCTYPE ");
                    super._printer.printText(s);
                    super._printer.printText(" [");
                    printText(s1, true, true);
                    super._printer.printText("]>");
                    super._printer.breakLine();
                }
        }
        super._started = true;
        ((BaseMarkupSerializer)this).serializePreRoot();
    }

    protected void serializeElement(Element element)
        throws IOException
    {
        if(fNamespaces)
        {
            fLocalNSBinder.reset();
            fLocalNSBinder.pushContext();
            fNSBinder.pushContext();
        }
        String s5 = element.getTagName();
        ElementState elementstate = ((BaseMarkupSerializer)this).getElementState();
        if(((BaseMarkupSerializer)this).isDocumentState())
        {
            fDOML1 = ((Node) (element)).getLocalName() == null;
            if(!super._started)
                startDocument(s5);
        } else
        {
            if(elementstate.empty)
                super._printer.printText('>');
            if(elementstate.inCData)
            {
                super._printer.printText("]]>");
                elementstate.inCData = false;
            }
            if(super._indenting && !elementstate.preserveSpace && (elementstate.empty || elementstate.afterElement || elementstate.afterComment))
                super._printer.breakLine();
        }
        fPreserveSpace = elementstate.preserveSpace;
        int l = 0;
        NamedNodeMap namednodemap = null;
        if(((Node) (element)).hasAttributes())
        {
            namednodemap = ((Node) (element)).getAttributes();
            l = namednodemap.getLength();
        }
        if(!fNamespaces)
        {
            super._printer.printText('<');
            super._printer.printText(s5);
            super._printer.indent();
            for(int i = 0; i < l; i++)
            {
                Attr attr = (Attr)namednodemap.item(i);
                String s = attr.getName();
                String s2 = attr.getValue();
                if(s2 == null)
                    s2 = "";
                if(attr.getSpecified())
                {
                    super._printer.printSpace();
                    super._printer.printText(s);
                    super._printer.printText("=\"");
                    printEscaped(s2);
                    super._printer.printText('"');
                }
                if(s.equals("xml:space"))
                    if(s2.equals("preserve"))
                        fPreserveSpace = true;
                    else
                        fPreserveSpace = super._format.getPreserveSpace();
            }

        } else
        {
            for(int j = 0; j < l; j++)
            {
                Attr attr1 = (Attr)namednodemap.item(j);
                String s11 = ((Node) (attr1)).getNamespaceURI();
                if(s11 != null && s11.equals(((Object) (NamespaceContext.XMLNS_URI))))
                {
                    String s3 = ((Node) (attr1)).getNodeValue();
                    if(s3 == null)
                        s3 = XMLSymbols.EMPTY_STRING;
                    if(s3.equals(((Object) (NamespaceContext.XMLNS_URI))))
                    {
                        if(super.fDOMErrorHandler != null)
                        {
                            ((BaseMarkupSerializer)this).modifyDOMError("No prefix other than 'xmlns' can be bound to 'http://www.w3.org/2000/xmlns/' namespace name", (short)1, ((Node) (attr1)));
                            boolean flag = super.fDOMErrorHandler.handleError(((org.apache.xerces.dom3.DOMError) (super.fDOMError)));
                            if(!flag)
                                throw new RuntimeException("Stopped at user request");
                        }
                    } else
                    {
                        String s6 = ((Node) (attr1)).getPrefix();
                        s6 = s6 != null && s6.length() != 0 ? fSymbolTable.addSymbol(s6) : XMLSymbols.EMPTY_STRING;
                        String s15 = fSymbolTable.addSymbol(((Node) (attr1)).getLocalName());
                        if(s6 == XMLSymbols.PREFIX_XMLNS)
                        {
                            s3 = fSymbolTable.addSymbol(s3);
                            if(s3.length() != 0)
                                fNSBinder.declarePrefix(s15, s3);
                        } else
                        {
                            s3 = fSymbolTable.addSymbol(s3);
                            fNSBinder.declarePrefix(XMLSymbols.EMPTY_STRING, s3);
                        }
                    }
                }
            }

            String s12 = ((Node) (element)).getNamespaceURI();
            String s7 = ((Node) (element)).getPrefix();
            if(s12 != null && s7 != null && s12.length() == 0 && s7.length() != 0)
            {
                s7 = null;
                super._printer.printText('<');
                super._printer.printText(((Node) (element)).getLocalName());
                super._printer.indent();
            } else
            {
                super._printer.printText('<');
                super._printer.printText(s5);
                super._printer.indent();
            }
            if(s12 != null)
            {
                s12 = fSymbolTable.addSymbol(s12);
                s7 = s7 != null && s7.length() != 0 ? fSymbolTable.addSymbol(s7) : XMLSymbols.EMPTY_STRING;
                if(fNSBinder.getURI(s7) != s12)
                {
                    printNamespaceAttr(s7, s12);
                    fLocalNSBinder.declarePrefix(s7, s12);
                }
            } else
            {
                int i1 = s5.indexOf(':');
                if(i1 > -1)
                {
                    if(super.fDOMErrorHandler != null)
                    {
                        String s17 = DOMMessageFormatter.formatMessage("http://apache.org/xml/serializer", "ElementQName", new Object[] {
                            s5
                        });
                        ((BaseMarkupSerializer)this).modifyDOMError(s17, (short)1, ((Node) (element)));
                        boolean flag2 = super.fDOMErrorHandler.handleError(((org.apache.xerces.dom3.DOMError) (super.fDOMError)));
                        if(!flag2)
                            throw new RuntimeException("Process stoped at user request");
                    }
                } else
                {
                    String s13 = fNSBinder.getURI(XMLSymbols.EMPTY_STRING);
                    if(s13 != null && s13.length() > 0)
                    {
                        printNamespaceAttr(XMLSymbols.EMPTY_STRING, XMLSymbols.EMPTY_STRING);
                        fLocalNSBinder.declarePrefix(XMLSymbols.EMPTY_STRING, XMLSymbols.EMPTY_STRING);
                    }
                }
            }
            for(int k = 0; k < l; k++)
            {
                Attr attr2 = (Attr)namednodemap.item(k);
                String s4 = attr2.getValue();
                String s1 = ((Node) (attr2)).getNodeName();
                String s14 = ((Node) (attr2)).getNamespaceURI();
                if(s14 != null && s14.length() == 0)
                {
                    s14 = null;
                    s1 = ((Node) (attr2)).getLocalName();
                }
                if(s4 == null)
                    s4 = XMLSymbols.EMPTY_STRING;
                if(s14 != null)
                {
                    String s8 = ((Node) (attr2)).getPrefix();
                    s8 = s8 != null ? fSymbolTable.addSymbol(s8) : XMLSymbols.EMPTY_STRING;
                    String s16 = fSymbolTable.addSymbol(((Node) (attr2)).getLocalName());
                    if(s14 != null && s14.equals(((Object) (NamespaceContext.XMLNS_URI))))
                    {
                        s8 = ((Node) (attr2)).getPrefix();
                        s8 = s8 != null && s8.length() != 0 ? fSymbolTable.addSymbol(s8) : XMLSymbols.EMPTY_STRING;
                        s16 = fSymbolTable.addSymbol(((Node) (attr2)).getLocalName());
                        if(s8 == XMLSymbols.PREFIX_XMLNS)
                        {
                            String s9 = fLocalNSBinder.getURI(s16);
                            s4 = fSymbolTable.addSymbol(s4);
                            if(s4.length() != 0)
                                if(s9 != null)
                                    fNSBinder.declarePrefix(s16, s9);
                                else
                                    printNamespaceAttr(s16, s4);
                        } else
                        {
                            s14 = fNSBinder.getURI(XMLSymbols.EMPTY_STRING);
                            String s10 = fLocalNSBinder.getURI(XMLSymbols.EMPTY_STRING);
                            s4 = fSymbolTable.addSymbol(s4);
                            if(s10 != null)
                                fNSBinder.declarePrefix(XMLSymbols.EMPTY_STRING, s4);
                            else
                                printNamespaceAttr(XMLSymbols.EMPTY_STRING, s4);
                        }
                    } else
                    {
                        s14 = fSymbolTable.addSymbol(s14);
                        String s18 = fNSBinder.getURI(s8);
                        if(s8 == XMLSymbols.EMPTY_STRING || s18 != s14)
                        {
                            s1 = ((Node) (attr2)).getNodeName();
                            String s19 = fLocalNSBinder.getPrefix(s14);
                            if(s19 == null)
                                s19 = fNSBinder.getPrefix(s14);
                            if(s19 != null && s19 != XMLSymbols.EMPTY_STRING)
                            {
                                s8 = s19;
                                s1 = s8 + ":" + s16;
                            } else
                            {
                                if(s8 == XMLSymbols.EMPTY_STRING || fLocalNSBinder.getURI(s8) != null)
                                {
                                    for(s8 = fSymbolTable.addSymbol("NS" + fNamespaceCounter++); fLocalNSBinder.getURI(s8) != null; s8 = fSymbolTable.addSymbol("NS" + fNamespaceCounter++));
                                    s1 = s8 + ":" + s16;
                                }
                                printNamespaceAttr(s8, s14);
                                s4 = fSymbolTable.addSymbol(s4);
                                fLocalNSBinder.declarePrefix(s8, s4);
                                fNSBinder.declarePrefix(s8, s14);
                            }
                        }
                        printAttribute(s1, s4 != null ? s4 : XMLSymbols.EMPTY_STRING, attr2.getSpecified());
                    }
                } else
                {
                    int j1 = s1.indexOf(':');
                    if(j1 > -1)
                    {
                        if(super.fDOMErrorHandler != null)
                        {
                            ((BaseMarkupSerializer)this).modifyDOMError("DOM Level 1 Node: " + s1, (short)1, ((Node) (attr2)));
                            boolean flag1 = super.fDOMErrorHandler.handleError(((org.apache.xerces.dom3.DOMError) (super.fDOMError)));
                            if(!flag1)
                                throw new RuntimeException("Stopped at user request");
                        }
                        printAttribute(s1, s4, attr2.getSpecified());
                    } else
                    {
                        printAttribute(s1, s4, attr2.getSpecified());
                    }
                }
            }

        }
        if(((Node) (element)).hasChildNodes())
        {
            elementstate = ((BaseMarkupSerializer)this).enterElementState(((String) (null)), ((String) (null)), s5, fPreserveSpace);
            elementstate.doCData = super._format.isCDataElement(s5);
            elementstate.unescaped = super._format.isNonEscapingElement(s5);
            for(Node node = ((Node) (element)).getFirstChild(); node != null; node = node.getNextSibling())
                ((BaseMarkupSerializer)this).serializeNode(node);

            if(fNamespaces)
                fNSBinder.popContext();
            endElementIO(((String) (null)), ((String) (null)), s5);
        } else
        {
            if(fNamespaces)
                fNSBinder.popContext();
            super._printer.unindent();
            super._printer.printText("/>");
            elementstate.afterElement = true;
            elementstate.afterComment = false;
            elementstate.empty = false;
            if(((BaseMarkupSerializer)this).isDocumentState())
                super._printer.flush();
        }
    }

    private void printNamespaceAttr(String s, String s1)
        throws IOException
    {
        super._printer.printSpace();
        if(s == XMLSymbols.EMPTY_STRING)
            super._printer.printText(XMLSymbols.PREFIX_XMLNS);
        else
            super._printer.printText("xmlns:" + s);
        super._printer.printText("=\"");
        printEscaped(s1);
        super._printer.printText('"');
    }

    private void printAttribute(String s, String s1, boolean flag)
        throws IOException
    {
        if(flag || super.fFeatures != null && !((Boolean)super.fFeatures.get("discard-default-content")).booleanValue())
        {
            super._printer.printSpace();
            super._printer.printText(s);
            super._printer.printText("=\"");
            printEscaped(s1);
            super._printer.printText('"');
        }
        if(s.equals("xml:space"))
            if(s1.equals("preserve"))
                fPreserveSpace = true;
            else
                fPreserveSpace = super._format.getPreserveSpace();
    }

    protected String getEntityRef(int i)
    {
        switch(i)
        {
        case 60: // '<'
            return "lt";

        case 62: // '>'
            return "gt";

        case 34: // '"'
            return "quot";

        case 39: // '\''
            return "apos";

        case 38: // '&'
            return "amp";
        }
        return null;
    }

    private Attributes extractNamespaces(Attributes attributes)
        throws SAXException
    {
        if(attributes == null)
            return null;
        int j = attributes.getLength();
        AttributesImpl attributesimpl = new AttributesImpl(attributes);
        for(int i = j - 1; i >= 0; i--)
        {
            String s = attributesimpl.getQName(i);
            if(s.startsWith("xmlns"))
                if(s.length() == 5)
                {
                    ((BaseMarkupSerializer)this).startPrefixMapping("", attributes.getValue(i));
                    attributesimpl.removeAttribute(i);
                } else
                if(s.charAt(5) == ':')
                {
                    ((BaseMarkupSerializer)this).startPrefixMapping(s.substring(6), attributes.getValue(i));
                    attributesimpl.removeAttribute(i);
                }
        }

        return ((Attributes) (attributesimpl));
    }

    protected void printEscaped(String s)
        throws IOException
    {
        int i = s.length();
        for(int j = 0; j < i; j++)
        {
            int k = ((int) (s.charAt(j)));
            if(!XMLChar.isValid(k))
            {
                if(++j < i)
                    ((BaseMarkupSerializer)this).surrogates(k, ((int) (s.charAt(j))));
                else
                    ((BaseMarkupSerializer)this).fatalError("The character '" + (char)k + "' is an invalid XML character");
            } else
            {
                printXMLChar(k);
            }
        }

    }

    protected final void printXMLChar(int i)
        throws IOException
    {
        if(i == 60)
            super._printer.printText("&lt;");
        else
        if(i == 38)
            super._printer.printText("&amp;");
        else
        if(i == 34)
            super._printer.printText("&quot;");
        else
        if(i >= 32 && super._encodingInfo.isPrintable(i) || i == 10 || i == 13 || i == 9)
        {
            super._printer.printText((char)i);
        } else
        {
            super._printer.printText("&#x");
            super._printer.printText(Integer.toHexString(i));
            super._printer.printText(';');
        }
    }

    protected void printText(String s, boolean flag, boolean flag1)
        throws IOException
    {
        int k = s.length();
        if(flag)
        {
            for(int i = 0; i < k; i++)
            {
                char c = s.charAt(i);
                if(!XMLChar.isValid(((int) (c))))
                {
                    if(++i < k)
                        ((BaseMarkupSerializer)this).surrogates(((int) (c)), ((int) (s.charAt(i))));
                    else
                        ((BaseMarkupSerializer)this).fatalError("The character '" + c + "' is an invalid XML character");
                } else
                if(flag1)
                    super._printer.printText(c);
                else
                    printXMLChar(((int) (c)));
            }

        } else
        {
            for(int j = 0; j < k; j++)
            {
                char c1 = s.charAt(j);
                if(!XMLChar.isValid(((int) (c1))))
                {
                    if(++j < k)
                        ((BaseMarkupSerializer)this).surrogates(((int) (c1)), ((int) (s.charAt(j))));
                    else
                        ((BaseMarkupSerializer)this).fatalError("The character '" + c1 + "' is an invalid XML character");
                } else
                if(XMLChar.isSpace(((int) (c1))))
                    super._printer.printSpace();
                else
                if(flag1)
                    super._printer.printText(c1);
                else
                    printXMLChar(((int) (c1)));
            }

        }
    }

    protected void printText(char ac[], int i, int j, boolean flag, boolean flag1)
        throws IOException
    {
        if(flag)
            while(j-- > 0) 
            {
                char c = ac[i];
                i++;
                if(!XMLChar.isValid(((int) (c))))
                {
                    if(++i < j)
                        ((BaseMarkupSerializer)this).surrogates(((int) (c)), ((int) (ac[i])));
                    else
                        ((BaseMarkupSerializer)this).fatalError("The character '" + c + "' is an invalid XML character");
                } else
                if(flag1)
                    super._printer.printText(c);
                else
                    printXMLChar(((int) (c)));
            }
        else
            while(j-- > 0) 
            {
                char c1 = ac[i];
                i++;
                if(!XMLChar.isValid(((int) (c1))))
                {
                    if(++i < j)
                        ((BaseMarkupSerializer)this).surrogates(((int) (c1)), ((int) (ac[i])));
                    else
                        ((BaseMarkupSerializer)this).fatalError("The character '" + c1 + "' is an invalid XML character");
                } else
                if(XMLChar.isSpace(((int) (c1))))
                    super._printer.printSpace();
                else
                if(flag1)
                    super._printer.printText(c1);
                else
                    printXMLChar(((int) (c1)));
            }
    }

    public boolean reset()
    {
        super.reset();
        return true;
    }
}
