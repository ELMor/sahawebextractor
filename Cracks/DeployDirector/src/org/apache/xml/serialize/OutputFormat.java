// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package org.apache.xml.serialize;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Node;
import org.w3c.dom.html.HTMLDocument;

// Referenced classes of package org.apache.xml.serialize:
//            EncodingInfo, Encodings

public class OutputFormat
{
    public static class Defaults
    {

        public static final int Indent = 4;
        public static final String Encoding = "UTF-8";
        public static final int LineWidth = 72;

        public Defaults()
        {
        }
    }

    public static class DTD
    {

        public static final String HTMLPublicId = "-//W3C//DTD HTML 4.0//EN";
        public static final String HTMLSystemId = "http://www.w3.org/TR/WD-html-in-xml/DTD/xhtml1-strict.dtd";
        public static final String XHTMLPublicId = "-//W3C//DTD XHTML 1.0 Strict//EN";
        public static final String XHTMLSystemId = "http://www.w3.org/TR/WD-html-in-xml/DTD/xhtml1-strict.dtd";

        public DTD()
        {
        }
    }


    private String _method;
    private String _version;
    private int _indent;
    private String _encoding;
    private EncodingInfo _encodingInfo;
    private String _mediaType;
    private String _doctypeSystem;
    private String _doctypePublic;
    private boolean _omitXmlDeclaration;
    private boolean _omitDoctype;
    private boolean _omitComments;
    private boolean _stripComments;
    private boolean _standalone;
    private String _cdataElements[];
    private String _nonEscapingElements[];
    private String _lineSeparator;
    private int _lineWidth;
    private boolean _preserve;
    private boolean _preserveEmptyAttributes;

    public OutputFormat()
    {
        _indent = 0;
        _encoding = "UTF-8";
        _encodingInfo = null;
        _omitXmlDeclaration = false;
        _omitDoctype = false;
        _omitComments = false;
        _stripComments = false;
        _standalone = false;
        _lineSeparator = "\n";
        _lineWidth = 72;
        _preserve = false;
        _preserveEmptyAttributes = false;
    }

    public OutputFormat(String s, String s1, boolean flag)
    {
        _indent = 0;
        _encoding = "UTF-8";
        _encodingInfo = null;
        _omitXmlDeclaration = false;
        _omitDoctype = false;
        _omitComments = false;
        _stripComments = false;
        _standalone = false;
        _lineSeparator = "\n";
        _lineWidth = 72;
        _preserve = false;
        _preserveEmptyAttributes = false;
        setMethod(s);
        setEncoding(s1);
        setIndenting(flag);
    }

    public OutputFormat(Document document)
    {
        _indent = 0;
        _encoding = "UTF-8";
        _encodingInfo = null;
        _omitXmlDeclaration = false;
        _omitDoctype = false;
        _omitComments = false;
        _stripComments = false;
        _standalone = false;
        _lineSeparator = "\n";
        _lineWidth = 72;
        _preserve = false;
        _preserveEmptyAttributes = false;
        setMethod(whichMethod(document));
        setDoctype(whichDoctypePublic(document), whichDoctypeSystem(document));
        setMediaType(whichMediaType(getMethod()));
    }

    public OutputFormat(Document document, String s, boolean flag)
    {
        this(document);
        setEncoding(s);
        setIndenting(flag);
    }

    public String getMethod()
    {
        return _method;
    }

    public void setMethod(String s)
    {
        _method = s;
    }

    public String getVersion()
    {
        return _version;
    }

    public void setVersion(String s)
    {
        _version = s;
    }

    public int getIndent()
    {
        return _indent;
    }

    public boolean getIndenting()
    {
        return _indent > 0;
    }

    public void setIndent(int i)
    {
        if(i < 0)
            _indent = 0;
        else
            _indent = i;
    }

    public void setIndenting(boolean flag)
    {
        if(flag)
        {
            _indent = 4;
            _lineWidth = 72;
        } else
        {
            _indent = 0;
            _lineWidth = 0;
        }
    }

    public String getEncoding()
    {
        return _encoding;
    }

    public void setEncoding(String s)
    {
        _encoding = s;
        _encodingInfo = null;
    }

    public void setEncoding(EncodingInfo encodinginfo)
    {
        _encoding = encodinginfo.getName();
        _encodingInfo = encodinginfo;
    }

    public EncodingInfo getEncodingInfo()
    {
        if(_encodingInfo == null)
            _encodingInfo = Encodings.getEncodingInfo(_encoding);
        return _encodingInfo;
    }

    public String getMediaType()
    {
        return _mediaType;
    }

    public void setMediaType(String s)
    {
        _mediaType = s;
    }

    public void setDoctype(String s, String s1)
    {
        _doctypePublic = s;
        _doctypeSystem = s1;
    }

    public String getDoctypePublic()
    {
        return _doctypePublic;
    }

    public String getDoctypeSystem()
    {
        return _doctypeSystem;
    }

    public boolean getOmitComments()
    {
        return _omitComments;
    }

    public void setOmitComments(boolean flag)
    {
        _omitComments = flag;
    }

    public boolean getOmitDocumentType()
    {
        return _omitDoctype;
    }

    public void setOmitDocumentType(boolean flag)
    {
        _omitDoctype = flag;
    }

    public boolean getOmitXMLDeclaration()
    {
        return _omitXmlDeclaration;
    }

    public void setOmitXMLDeclaration(boolean flag)
    {
        _omitXmlDeclaration = flag;
    }

    public boolean getStandalone()
    {
        return _standalone;
    }

    public void setStandalone(boolean flag)
    {
        _standalone = flag;
    }

    public String[] getCDataElements()
    {
        return _cdataElements;
    }

    public boolean isCDataElement(String s)
    {
        if(_cdataElements == null)
            return false;
        for(int i = 0; i < _cdataElements.length; i++)
            if(_cdataElements[i].equals(((Object) (s))))
                return true;

        return false;
    }

    public void setCDataElements(String as[])
    {
        _cdataElements = as;
    }

    public String[] getNonEscapingElements()
    {
        return _nonEscapingElements;
    }

    public boolean isNonEscapingElement(String s)
    {
        if(_nonEscapingElements == null)
            return false;
        for(int i = 0; i < _nonEscapingElements.length; i++)
            if(_nonEscapingElements[i].equals(((Object) (s))))
                return true;

        return false;
    }

    public void setNonEscapingElements(String as[])
    {
        _nonEscapingElements = as;
    }

    public String getLineSeparator()
    {
        return _lineSeparator;
    }

    public void setLineSeparator(String s)
    {
        if(s == null)
            _lineSeparator = "\n";
        else
            _lineSeparator = s;
    }

    public boolean getPreserveSpace()
    {
        return _preserve;
    }

    public void setPreserveSpace(boolean flag)
    {
        _preserve = flag;
    }

    public int getLineWidth()
    {
        return _lineWidth;
    }

    public void setLineWidth(int i)
    {
        if(i <= 0)
            _lineWidth = 0;
        else
            _lineWidth = i;
    }

    public boolean getPreserveEmptyAttributes()
    {
        return _preserveEmptyAttributes;
    }

    public void setPreserveEmptyAttributes(boolean flag)
    {
        _preserveEmptyAttributes = flag;
    }

    public char getLastPrintable()
    {
        return getEncoding() == null || !getEncoding().equalsIgnoreCase("ASCII") ? '\uFFFF' : '\377';
    }

    public static String whichMethod(Document document)
    {
        if(document instanceof HTMLDocument)
            return "html";
        for(Node node = ((Node) (document)).getFirstChild(); node != null; node = node.getNextSibling())
        {
            if(node.getNodeType() == 1)
            {
                if(node.getNodeName().equalsIgnoreCase("html"))
                    return "html";
                if(node.getNodeName().equalsIgnoreCase("root"))
                    return "fop";
                else
                    return "xml";
            }
            if(node.getNodeType() == 3)
            {
                String s = node.getNodeValue();
                for(int i = 0; i < s.length(); i++)
                    if(s.charAt(i) != ' ' && s.charAt(i) != '\n' && s.charAt(i) != '\t' && s.charAt(i) != '\r')
                        return "xml";

            }
        }

        return "xml";
    }

    public static String whichDoctypePublic(Document document)
    {
        DocumentType documenttype = document.getDoctype();
        if(documenttype != null)
            try
            {
                return documenttype.getPublicId();
            }
            catch(Error error) { }
        if(document instanceof HTMLDocument)
            return "-//W3C//DTD XHTML 1.0 Strict//EN";
        else
            return null;
    }

    public static String whichDoctypeSystem(Document document)
    {
        DocumentType documenttype = document.getDoctype();
        if(documenttype != null)
            try
            {
                return documenttype.getSystemId();
            }
            catch(Error error) { }
        if(document instanceof HTMLDocument)
            return "http://www.w3.org/TR/WD-html-in-xml/DTD/xhtml1-strict.dtd";
        else
            return null;
    }

    public static String whichMediaType(String s)
    {
        if(s.equalsIgnoreCase("xml"))
            return "text/xml";
        if(s.equalsIgnoreCase("html"))
            return "text/html";
        if(s.equalsIgnoreCase("xhtml"))
            return "text/html";
        if(s.equalsIgnoreCase("text"))
            return "text/plain";
        if(s.equalsIgnoreCase("fop"))
            return "application/pdf";
        else
            return null;
    }
}
