// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 

package org.apache.xml.serialize;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import org.xml.sax.ContentHandler;
import org.xml.sax.DocumentHandler;

// Referenced classes of package org.apache.xml.serialize:
//            OutputFormat, DOMSerializer

public interface Serializer
{

    public abstract void setOutputByteStream(OutputStream outputstream);

    public abstract void setOutputCharStream(Writer writer);

    public abstract void setOutputFormat(OutputFormat outputformat);

    public abstract DocumentHandler asDocumentHandler()
        throws IOException;

    public abstract ContentHandler asContentHandler()
        throws IOException;

    public abstract DOMSerializer asDOMSerializer()
        throws IOException;
}
