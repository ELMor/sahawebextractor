// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StandardInputStream.java

package org.javagroup.util;

import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package org.javagroup.util:
//            StandardIO

public class StandardInputStream extends InputStream
{

    public StandardInputStream()
    {
    }

    public int read()
        throws IOException
    {
        return StandardIO.getInstance().getIn().read();
    }

    public int read(byte data[], int a, int b)
        throws IOException
    {
        return StandardIO.getInstance().getIn().read(data, a, b);
    }
}
