// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   WindowResource.java

package org.javagroup.util;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

// Referenced classes of package org.javagroup.util:
//            Resource

public class WindowResource extends Resource
{
    final class WindowDisposalListener extends WindowAdapter
    {

        protected WindowResource _resource;

        public void windowClosed(WindowEvent event)
        {
            _resource.dispose();
        }

        public WindowDisposalListener(WindowResource window)
        {
            _resource = window;
            _resource.getWindow().addWindowListener(((java.awt.event.WindowListener) (this)));
        }
    }


    protected Window _window;

    public WindowResource(Window window)
    {
        _window = window;
    }

    public Window getWindow()
    {
        return _window;
    }

    public void dispose()
    {
        if(_window == null)
            return;
        try
        {
            if(((Component) (_window)).isVisible())
                _window.dispose();
            _window = null;
        }
        catch(Exception e) { }
        super.dispose();
    }

    public String toString()
    {
        return "WindowResource: " + ((Component) (_window)).toString();
    }
}
