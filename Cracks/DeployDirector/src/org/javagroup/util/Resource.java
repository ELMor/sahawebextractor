// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Resource.java

package org.javagroup.util;

import java.util.Vector;

// Referenced classes of package org.javagroup.util:
//            ResourceDisposalListener, Disposable

public class Resource
    implements Disposable
{

    private static Vector __resources;
    protected int _references;
    protected boolean _static;
    protected Vector _disposalListeners;
    protected Disposable _wrappedResource;
    protected boolean _disposed;

    public Resource()
    {
        _disposed = false;
        _disposalListeners = new Vector();
    }

    public Resource(Disposable disposable)
    {
        this();
        _wrappedResource = disposable;
    }

    public synchronized void addLock()
    {
        _references++;
    }

    public synchronized void releaseLock()
    {
        if(!_static)
        {
            _references--;
            if(_references <= 0)
                dispose();
        }
    }

    public synchronized void setStatic()
    {
        if(!_static)
            __resources.addElement(((Object) (this)));
        _static = true;
    }

    public void unregister()
    {
        if(__resources.contains(((Object) (this))))
            __resources.removeElement(((Object) (this)));
    }

    public void dispose()
    {
        if(_disposed)
            return;
        if(_wrappedResource != null)
            _wrappedResource.dispose();
        fireResourceDisposalEvent();
        _disposalListeners.removeAllElements();
        _disposed = true;
    }

    public boolean isDisposed()
    {
        return _disposed;
    }

    public void addDisposalListener(ResourceDisposalListener listener)
    {
        _disposalListeners.addElement(((Object) (listener)));
    }

    public void removeDisposalListener(ResourceDisposalListener listener)
    {
        _disposalListeners.removeElement(((Object) (listener)));
    }

    public void fireResourceDisposalEvent()
    {
        synchronized(_disposalListeners)
        {
            for(int i = 0; i < _disposalListeners.size(); i++)
            {
                ResourceDisposalListener listener = (ResourceDisposalListener)_disposalListeners.elementAt(i);
                listener.resourceDisposed(this);
            }

        }
    }
}
