/* ProcessSecurityManager - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package org.javagroup.process;
import java.awt.Window;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;

import org.javagroup.util.WindowResource;

public class ProcessSecurityManager extends SecurityManager
{
    protected ProcessManager _manager;
    protected boolean DEBUG_ON = false;
    protected boolean VERBOSE_DEBUG_ON = false;
    private static final String START = "[CAM SecurityManager:";
    protected static final PrintWriter safeStream
        = new PrintWriter(new FileOutputStream(FileDescriptor.out), true);

    public ProcessSecurityManager(ProcessManager manager) {
        _manager = manager;
        String is_debug = System.getProperty("deploy.debug");
        if (is_debug != null && is_debug.indexOf("security") != -1)
            DEBUG_ON = true;
        else
            DEBUG_ON = false;
        if (is_debug != null && is_debug.indexOf("verbosesecurity") != -1)
            VERBOSE_DEBUG_ON = true;
        else
            VERBOSE_DEBUG_ON = false;
    }

    ProcessSecurityManager() {
        /* empty */
    }

    void setProcessManager(ProcessManager manager) {
        _manager = manager;
    }

    public void removeProcessManager() {
        _manager = null;
    }

    public JProcess getCurrentProcess() {
        JProcess match = null;
        ThreadGroup group = this.getThreadGroup();
        if (_manager == null)
            return null;
        match = _manager.getProcessFor(group);
        if (match != null)
            return match;
        Class[] class_context = this.getClassContext();
        for (int i = 0; i < class_context.length; i++) {
            ClassLoader loader = null;
            try {
                loader = class_context[i].getClassLoader();
            } catch (Exception e) {
                continue;
            }
            if (loader != null) {
                match = _manager.getProcessFor(loader);
                if (match != null)
                    return match;
            }
        }
        return null;
    }

    public void checkCreateClassLoader() {
        if (DEBUG_ON)
            safeStream
                .println("[CAM SecurityManager: checkCreateClassLoader()]");
    }

    public void checkAccess(Thread g) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkAccess(Thread '"
                               + g.getName() + "')]");
    }

    public void checkAccess(ThreadGroup g) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkAccess(ThreadGroup '"
                 + g.getName() + "')]");
    }

    public void checkExit(int status) {
        if (DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkExit(int '" + status
                               + "')]");
        if (_manager != null) {
            JProcess process = getCurrentProcess();
            if (process != null) {
                if (process instanceof StandardJProcess)
                    ((StandardJProcess) process).setExitCode(status);
                _manager.kill(process.getPid());
            }
            throw new SecurityException();
        }
    }

    public void checkExec(String cmd) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkExec(String '" + cmd
                               + "')]");
    }

    public void checkLink(String lib) {
        Class[] class_context = this.getClassContext();
        String className = null;
        for (int i = 0; i < class_context.length; i++) {
            className = class_context[i].getName();
            if (!className.equals("java.lang.System")
                && !className.equals("java.lang.Runtime")
                && !(className.equals
                     ("org.javagroup.process.ProcessSecurityManager"))) {
                try {
                    if (class_context[i].getClassLoader() == null)
                        return;
                } catch (Exception exception) {
                    /* empty */
                }
                break;
            }
        }
        StringBuffer text = new StringBuffer();
        text.append("Cannot load library: '");
        text.append(lib);
        text.append
            ("'\nNative Libraries are not supported in 1.1 VM's as a result of the JDK bug 4052610.\n");
        text.append
            ("Details available at: http://developer.java.sun.com/developer/bugParade/bugs/4052610.html");
        if (DEBUG_ON) {
            safeStream.println("[CAM SecurityManager: checkLink(Library '"
                               + lib + "') called by " + className);
            safeStream.println(text.toString());
            safeStream.println("]");
        }
        throw new SecurityException(text.toString());
    }

    public void checkRead(FileDescriptor fd) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkRead(FileDescriptor '" + fd
                 + "')]");
    }

    public void checkRead(String file) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkRead(File '" + file
                               + "')]");
    }

    public void checkRead(String file, Object context) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkRead(File '" + file
                               + "', Object '" + context + "')]");
    }

    public void checkWrite(FileDescriptor fd) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkWrite(FileDescriptor '" + fd
                 + "')]");
    }

    public void checkWrite(String file) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkWrite(File '" + file
                               + "')]");
    }

    public void checkDelete(String file) {
        if (DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkDelete(File '"
                               + file + "')]");
    }

    public void checkConnect(String host, int port) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkConnect(String '"
                               + host + "', int '" + port + "')]");
    }

    public void checkConnect(String host, int port, Object context) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkConnect(String '"
                               + host + "', int '" + port + "', Object '"
                               + context + "')]");
    }

    public void checkListen(int port) {
        if (DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkListen(int '" + port
                               + "')]");
    }

    public void checkAccept(String host, int port) {
        if (DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkAccept(String '"
                               + host + "', int '" + port + "')]");
    }

    public void checkMulticast(InetAddress maddr) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkMulticast(InetAddress '" + maddr
                 + "')]");
    }

    public void checkMulticast(InetAddress maddr, byte ttl) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkMulticast(InetAddress '" + maddr
                 + "', byte '" + ttl + "')]");
    }

    public void checkPropertiesAccess() {
        if (VERBOSE_DEBUG_ON)
            safeStream
                .println("[CAM SecurityManager: checkPropertiesAccess()]");
    }

    public void checkPropertyAccess(String key) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkPropertiesAccess(String '" + key
                 + "')]");
    }

    public void checkPropertyAccess(String key, String def) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkPropertiesAccess(String '" + key
                 + "', String '" + def + "')]");
    }

    public boolean checkTopLevelWindow(Object obj) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkTopLevelWindow(Object '" + obj
                 + "')]");
        Window window = null;
        if (obj instanceof Window)
            window = (Window) obj;
        if (_manager == null)
            return true;
        JProcess process = _manager.getCurrentProcess();
        if (process != null && window != null)
            process.registerAndBindToResource(new WindowResource(window));
        return true;
    }

    public void checkPrintJobAccess() {
        if (DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkPrintJobAccess()]");
    }

    public void checkSystemClipboardAccess() {
        if (DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkSystemClipboardAccess()]");
    }

    public void checkAwtEventQueueAccess() {
        if (DEBUG_ON)
            safeStream
                .println("[CAM SecurityManager: checkAwtEventQueueAccess()]");
    }

    public void checkPackageAccess(String pkg) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkPackageAccess(String '" + pkg
                 + "')]");
    }

    public void checkPackageDefinition(String pkg) {
        if (DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkPackageDefinition(String '" + pkg
                 + "')]");
    }

    public void checkSetFactory() {
        if (DEBUG_ON)
            safeStream.println("[CAM SecurityManager: checkSetFactory()]");
    }

    public void checkMemberAccess(Class clazz, int which) {
        if (VERBOSE_DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkMemberAccess(Class '" + clazz
                 + "', int '" + which + "')]");
    }

    public void checkSecurityAccess(String provider) {
        if (DEBUG_ON)
            safeStream.println
                ("[CAM SecurityManager: checkSecurityAccess(String '"
                 + provider + "')]");
    }
}
