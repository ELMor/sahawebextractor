// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ProcessManager.java

package org.javagroup.process;

import java.net.URL;
import java.util.Enumeration;
import org.javagroup.util.StandardIO;

// Referenced classes of package org.javagroup.process:
//            ProcessCreationException, JProcess, ProcessEventListener

public interface ProcessManager
{

    public abstract JProcess createProcess(String s)
        throws ProcessCreationException;

    public abstract JProcess createProcess(String s, String as[])
        throws ProcessCreationException;

    public abstract JProcess createProcess(String s, String as[], URL aurl[])
        throws ProcessCreationException;

    public abstract JProcess createProcessFromString(String s)
        throws ProcessCreationException;

    public abstract JProcess createProcessFromString(String s, URL aurl[])
        throws ProcessCreationException;

    public abstract JProcess getProcessFor(ThreadGroup threadgroup);

    public abstract JProcess getProcessFor(ClassLoader classloader);

    public abstract JProcess getCurrentProcess();

    public abstract JProcess getProcess(long l);

    public abstract Enumeration getProcesses();

    public abstract void kill(long l);

    public abstract void addProcessEventListener(ProcessEventListener processeventlistener);

    public abstract void removeProcessEventListener(ProcessEventListener processeventlistener);

    public abstract void doGarbageCollect();

    public abstract StandardIO getStandardIOForProcess(JProcess jprocess);

    public abstract void setStandardIOForProcess(JProcess jprocess, StandardIO standardio);
}
