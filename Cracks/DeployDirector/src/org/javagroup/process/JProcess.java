// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JProcess.java

package org.javagroup.process;

import org.javagroup.util.Resource;

public interface JProcess
{

    public static final int UNSTARTED = 0;
    public static final int RUNNING = 1;
    public static final int DEAD = 2;

    public abstract String getName();

    public abstract long getPid();

    public abstract void launch();

    public abstract int getState();

    public abstract ThreadGroup getThreadGroup();

    public abstract ClassLoader getClassLoader();

    public abstract void kill();

    public abstract void registerResource(Resource resource);

    public abstract void registerAndBindToResource(Resource resource);

    public abstract void waitFor();

    public abstract int getExitCode();
}
