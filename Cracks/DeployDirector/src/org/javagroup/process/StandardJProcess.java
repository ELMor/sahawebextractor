// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   StandardJProcess.java

package org.javagroup.process;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Vector;
import org.javagroup.util.Resource;
import org.javagroup.util.ResourceDisposalListener;
import org.javagroup.util.URLClassLoader;

// Referenced classes of package org.javagroup.process:
//            ProcessCreationException, WrappedProcessException, JProcess

public class StandardJProcess
    implements Runnable, JProcess, ResourceDisposalListener
{

    protected long _pid;
    protected String _name;
    protected URLClassLoader _classLoader;
    protected ThreadGroup _processThreadGroup;
    protected Vector _lockedResources;
    protected Vector _resourcesLockedTo;
    protected int _state;
    protected int _exitCode;
    protected Method _targetMethod;
    protected String _args[];
    protected Thread _mainThread;
    protected boolean _isKilled;

    public StandardJProcess(String className, long pid, String args[], ThreadGroup parent, URL classpath[])
        throws ProcessCreationException, ClassNotFoundException
    {
        _name = className;
        _args = args;
        setState(0);
        _lockedResources = new Vector();
        _resourcesLockedTo = new Vector();
        if(_args == null)
            _args = new String[0];
        for(int i = 0; i < _args.length; i++)
            _name += " " + _args[i];

        _pid = pid;
        _classLoader = createClassLoader();
        if(classpath != null)
            _classLoader.addClassPath(classpath);
        _processThreadGroup = createThreadGroup(parent);
        try
        {
            setTargetClass(className);
        }
        catch(Exception e)
        {
            if(e instanceof ClassNotFoundException)
                throw (ClassNotFoundException)e;
            else
                throw new ProcessCreationException(((Throwable) (e)).toString());
        }
    }

    public void launch()
    {
        _mainThread = new Thread(_processThreadGroup, ((Runnable) (this)), "Main thread for " + _name);
        _mainThread.start();
        setState(1);
        Thread.yield();
    }

    public void addClassPath(URL classpath[])
    {
        _classLoader.addClassPath(classpath);
    }

    public void run()
    {
        try
        {
            Object args_array[] = {
                _args
            };
            _targetMethod.invoke(((Object) (null)), args_array);
        }
        catch(InvocationTargetException e)
        {
            throw new WrappedProcessException(((Throwable) (e)));
        }
        catch(Exception e)
        {
            throw new WrappedProcessException(((Throwable) (e)));
        }
    }

    protected URLClassLoader createClassLoader()
    {
        return new URLClassLoader();
    }

    protected ThreadGroup createThreadGroup(ThreadGroup parent)
    {
        ThreadGroup group = new ThreadGroup(parent, toString());
        group.setMaxPriority(9);
        return group;
    }

    private void setTargetClass(String className)
        throws ClassNotFoundException, NoSuchMethodException
    {
        Class target_class = _classLoader.loadMainClass(className);
        Class arg_types[] = {
            java.lang.String[].class
        };
        _targetMethod = target_class.getMethod("main", arg_types);
        if((_targetMethod.getModifiers() & 8) == 0 || (_targetMethod.getModifiers() & 1) == 0)
            throw new NoSuchMethodException("main(String[]) method of " + target_class.getName() + " is not public and static.");
        else
            return;
    }

    public ClassLoader getClassLoader()
    {
        return ((ClassLoader) (_classLoader));
    }

    public ThreadGroup getThreadGroup()
    {
        return _processThreadGroup;
    }

    public synchronized void registerResource(Resource resource)
    {
        if(!_lockedResources.contains(((Object) (resource))))
        {
            _lockedResources.addElement(((Object) (resource)));
            resource.addLock();
        }
    }

    public synchronized void registerAndBindToResource(Resource resource)
    {
        registerResource(resource);
        bindToResource(resource);
    }

    public synchronized void bindToResource(Resource resource)
    {
        if(!_resourcesLockedTo.contains(((Object) (resource))))
        {
            _resourcesLockedTo.addElement(((Object) (resource)));
            resource.addDisposalListener(((ResourceDisposalListener) (this)));
        }
    }

    public synchronized void releaseResources()
    {
        Resource resources[] = new Resource[_lockedResources.size()];
        _lockedResources.copyInto(((Object []) (resources)));
        for(int i = 0; i < resources.length; i++)
            releaseResource(resources[i]);

    }

    protected synchronized void releaseResource(Resource resource)
    {
        if(resource == null)
            return;
        if(_lockedResources.contains(((Object) (resource))))
        {
            try
            {
                _lockedResources.removeElement(((Object) (resource)));
                resource.removeDisposalListener(((ResourceDisposalListener) (this)));
            }
            catch(Exception e) { }
            try
            {
                resource.releaseLock();
            }
            catch(Exception e) { }
        }
        if(_resourcesLockedTo.contains(((Object) (resource))))
            _resourcesLockedTo.removeElement(((Object) (resource)));
    }

    public void resourceDisposed(Resource resource)
    {
        if(resource == null)
        {
            return;
        } else
        {
            releaseResource(resource);
            return;
        }
    }

    public synchronized void kill()
    {
        if(_isKilled)
            return;
        setState(2);
        releaseResources();
        _isKilled = true;
        int numThreads = _processThreadGroup.activeCount();
        Thread threads[] = new Thread[numThreads * 2];
        _processThreadGroup.enumerate(threads, true);
        for(int i = 0; i < numThreads * 2; i++)
        {
            if(threads[i] == null)
                break;
            if(!threads[i].getName().startsWith("AWT-") && !threads[i].getName().startsWith("SunToolkit"))
                threads[i].stop();
        }

        _classLoader.unlockJarFiles();
        _classLoader = null;
    }

    private synchronized void kill_destroyThreadGroup()
    {
        if(_processThreadGroup != null)
        {
            _processThreadGroup.destroy();
            _processThreadGroup = null;
        }
    }

    public synchronized void tryGarbageCollect()
    {
        if(_state != 0 && _state != 2 && _resourcesLockedTo.isEmpty() && _processThreadGroup.activeCount() <= 0)
        {
            kill();
            kill_destroyThreadGroup();
        }
    }

    public long getPid()
    {
        return _pid;
    }

    public String getName()
    {
        return _name;
    }

    public int getState()
    {
        return _state;
    }

    protected synchronized void setState(int state)
    {
        _state = state;
        ((Object)this).notifyAll();
    }

    public synchronized void waitFor()
    {
        while(getState() != 2) 
            try
            {
                ((Object)this).wait();
            }
            catch(InterruptedException e) { }
    }

    public int getExitCode()
    {
        return _exitCode;
    }

    public void setExitCode(int code)
    {
        _exitCode = code;
    }

    public String toString()
    {
        return String.valueOf(_pid) + ": " + _name;
    }
}
