// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Cookie.java

package HTTPClient;

import java.io.Serializable;
import java.net.ProtocolException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

// Referenced classes of package HTTPClient:
//            RoRequest, HTTPConnection, Util

public class Cookie
    implements Serializable
{

    protected String name;
    protected String value;
    protected Date expires;
    protected String domain;
    protected String path;
    protected boolean secure;
    protected static String formats[] = {
        "EE, dd-MMM-yy HH:mm:ss z", "EE, dd MMM yyyy HH:mm:ss z", "EE, dd MMM yyyy HH:mm z", "dd MMM yyyy HH:mm:ss z", "dd MMM yyyy HH:mm z", "EE, dd MMM yy HH:mm:ss z", "EE, dd MMM yy HH:mm z", "dd MMM yy HH:mm:ss z", "dd MMM yy HH:mm z", "EEEE, dd-MMM-yy HH:mm:ss z", 
        "EE MMM dd HH:mm:ss yyyy", "EE, dd-MMM-yyyy HH:mm:ss z", "EE, dd-MMM-yyyy HH:mm:ss", "EE, dd-MMM-yyyy HH:mm z", "EE, dd-MMM-yyyy HH:mm", "EE, dd-MMM-yy HH:mm:ss", "EE, dd-MMM-yy HH:mm z", "EE, dd-MMM-yy HH:mm", "EEEE, dd-MMM-yyyy HH:mm:ss z", "EEEE, dd-MMM-yyyy HH:mm:ss", 
        "EEEE, dd-MMM-yyyy HH:mm z", "EEEE, dd-MMM-yyyy HH:mm", "EEEE, dd-MMM-yy HH:mm:ss", "EEEE, dd-MMM-yy HH:mm z", "EEEE, dd-MMM-yy HH:mm", "dd-MMM-yy HH:mm:ss z", "dd-MMM-yy HH:mm:ss", "dd-MMM-yy HH:mm z", "dd-MMM-yy HH:mm", "dd-MMM-yyyy HH:mm:ss z", 
        "dd-MMM-yyyy HH:mm:ss", "dd-MMM-yyyy HH:mm z", "dd-MMM-yyyy HH:mm", "EE, dd MMM yyyy HH:mm:ss", "EE, dd MMM yyyy HH:mm", "dd MMM yyyy HH:mm:ss", "dd MMM yyyy HH:mm", "EE, dd MMM yy HH:mm:ss", "EE, dd MMM yy HH:mm", "dd MMM yy HH:mm:ss", 
        "dd MMM yy HH:mm"
    };

    public Cookie(String name, String value, String domain, String path, Date expires, boolean secure)
    {
        if(name == null)
            throw new NullPointerException("missing name");
        if(value == null)
            throw new NullPointerException("missing value");
        if(domain == null)
            throw new NullPointerException("missing domain");
        if(path == null)
            throw new NullPointerException("missing path");
        this.name = name;
        this.value = value;
        this.domain = domain.toLowerCase();
        this.path = path;
        this.expires = expires;
        this.secure = secure;
        if(this.domain.indexOf('.') == -1)
            this.domain += ".local";
    }

    protected Cookie(RoRequest req)
    {
        name = null;
        value = null;
        expires = null;
        domain = req.getConnection().getHost();
        if(domain.indexOf('.') == -1)
            domain += ".local";
        path = Util.getPath(req.getRequestURI());
        String prot = req.getConnection().getProtocol();
        if(prot.equals("https") || prot.equals("shttp"))
            secure = true;
        else
            secure = false;
    }

    protected static Cookie[] parse(String set_cookie, RoRequest req)
        throws ProtocolException
    {
        int beg = 0;
        int end = 0;
        int start = 0;
        char buf[] = set_cookie.toCharArray();
        int len = buf.length;
        Cookie cookie_arr[] = new Cookie[0];
        do
        {
            beg = Util.skipSpace(buf, beg);
            if(beg >= len)
                break;
            if(buf[beg] == ',')
            {
                beg++;
            } else
            {
                Cookie curr = new Cookie(req);
                start = beg;
                boolean legal = true;
                do
                {
                    if(beg >= len || buf[beg] == ',')
                        break;
                    if(buf[beg] == ';')
                        beg = Util.skipSpace(buf, beg + 1);
                    else
                    if(beg + 6 <= len && set_cookie.regionMatches(true, beg, "secure", 0, 6))
                    {
                        curr.secure = true;
                        beg += 6;
                        beg = Util.skipSpace(buf, beg);
                        if(beg < len && buf[beg] == ';')
                            beg = Util.skipSpace(buf, beg + 1);
                        else
                        if(beg < len && buf[beg] != ',')
                            throw new ProtocolException("Bad Set-Cookie header: " + set_cookie + "\nExpected " + "';' or ',' at position " + beg);
                    } else
                    {
                        end = set_cookie.indexOf('=', beg);
                        if(end == -1)
                            throw new ProtocolException("Bad Set-Cookie header: " + set_cookie + "\nNo '=' found " + "for token starting at " + "position " + beg);
                        String name = set_cookie.substring(beg, end).trim();
                        beg = Util.skipSpace(buf, end + 1);
                        int comma = set_cookie.indexOf(',', beg);
                        int semic = set_cookie.indexOf(';', beg);
                        if(semic == -1)
                            semic = len;
                        if(comma == -1)
                            end = semic;
                        else
                            end = Math.min(comma, semic);
                        String value = set_cookie.substring(beg, end).trim();
                        if(name.equalsIgnoreCase("expires"))
                        {
                            String weekdays[] = {
                                "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"
                            };
                            if(comma != -1 && comma < semic)
                            {
                                boolean gotWeekday = false;
                                for(int i = 0; i < weekdays.length; i++)
                                {
                                    if(!value.equalsIgnoreCase(weekdays[i].substring(0, value.length())))
                                        continue;
                                    gotWeekday = true;
                                    break;
                                }

                                if(!gotWeekday && (value.length() < 15 || value.indexOf(":") == -1))
                                    gotWeekday = true;
                                if(gotWeekday)
                                {
                                    comma = set_cookie.indexOf(',', comma + 1);
                                    if(comma == -1)
                                        end = semic;
                                    else
                                        end = Math.min(comma, semic);
                                    value = set_cookie.substring(beg, end).trim();
                                }
                            }
                            Date date = null;
                            for(int i = 0; i < formats.length;)
                                try
                                {
                                    DateFormat df = ((DateFormat) (new SimpleDateFormat(formats[i], Locale.US)));
                                    date = df.parse(value);
                                    break;
                                }
                                catch(ParseException pe)
                                {
                                    i++;
                                }

                            if(date == null)
                                throw new ProtocolException("Bad Set-Cookie header: " + set_cookie + "\nInvalid date found at " + "position " + beg);
                            curr.expires = date;
                        } else
                        if(name.equalsIgnoreCase("domain"))
                        {
                            value = value.toLowerCase();
                            if(value.charAt(0) != '.' && !value.equals(((Object) (curr.domain))))
                                value = '.' + value;
                            if(!curr.domain.endsWith(value))
                                legal = false;
                            if(!value.equals(".local") && value.indexOf('.', 1) == -1)
                                legal = false;
                            String top = null;
                            if(value.length() > 3)
                                top = value.substring(value.length() - 4);
                            if(top == null || !top.equalsIgnoreCase(".com") && !top.equalsIgnoreCase(".edu") && !top.equalsIgnoreCase(".net") && !top.equalsIgnoreCase(".org") && !top.equalsIgnoreCase(".gov") && !top.equalsIgnoreCase(".mil") && !top.equalsIgnoreCase(".int"))
                            {
                                int dl = curr.domain.length();
                                int vl = value.length();
                                if(dl > vl && curr.domain.substring(0, dl - vl).indexOf('.') != -1)
                                    legal = false;
                            }
                            curr.domain = value;
                        } else
                        if(name.equalsIgnoreCase("path"))
                        {
                            curr.path = value;
                        } else
                        {
                            curr.name = name;
                            curr.value = value;
                        }
                        beg = end;
                        if(beg < len && buf[beg] == ';')
                            beg = Util.skipSpace(buf, beg + 1);
                    }
                } while(true);
                if(curr.name == null || curr.value == null)
                    throw new ProtocolException("Bad Set-Cookie header: " + set_cookie + "\nNo Name=Value found" + " for cookie starting at " + "posibition " + start);
                if(legal)
                {
                    cookie_arr = Util.resizeArray(cookie_arr, cookie_arr.length + 1);
                    cookie_arr[cookie_arr.length - 1] = curr;
                }
            }
        } while(true);
        return cookie_arr;
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    public Date expires()
    {
        return expires;
    }

    public boolean discard()
    {
        return expires == null;
    }

    public String getDomain()
    {
        return domain;
    }

    public String getPath()
    {
        return path;
    }

    public boolean isSecure()
    {
        return secure;
    }

    public boolean hasExpired()
    {
        return expires != null && expires.getTime() <= System.currentTimeMillis();
    }

    protected boolean sendWith(RoRequest req)
    {
        HTTPConnection con = req.getConnection();
        String eff_host = con.getHost();
        if(eff_host.indexOf('.') == -1)
            eff_host = eff_host + ".local";
        return (domain.charAt(0) == '.' && eff_host.endsWith(domain) || domain.charAt(0) != '.' && eff_host.equals(((Object) (domain)))) && Util.getPath(req.getRequestURI()).startsWith(path) && (!secure || con.getProtocol().equals("https") || con.getProtocol().equals("shttp"));
    }

    public int hashCode()
    {
        return name.hashCode() + path.hashCode() + domain.hashCode();
    }

    public boolean equals(Object obj)
    {
        if(obj != null && (obj instanceof Cookie))
        {
            Cookie other = (Cookie)obj;
            return name.equals(((Object) (other.name))) && path.equals(((Object) (other.path))) && domain.equals(((Object) (other.domain)));
        } else
        {
            return false;
        }
    }

    protected String toExternalForm()
    {
        return name + "=" + value;
    }

    public String toString()
    {
        String string = name + "=" + value;
        if(expires != null)
            string = string + "; expires=" + expires;
        if(path != null)
            string = string + "; path=" + path;
        if(domain != null)
            string = string + "; domain=" + domain;
        if(secure)
            string = string + "; secure";
        return string;
    }

}
