// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CIHashtable.java

package HTTPClient;


final class CIString
{

    private String string;
    private int hash;
    private static final char lc[];

    public CIString(String string)
    {
        this.string = string;
        hash = calcHashCode(string);
    }

    public final String getString()
    {
        return string;
    }

    public int hashCode()
    {
        return hash;
    }

    private static final int calcHashCode(String str)
    {
        int hash = 0;
        char llc[] = lc;
        int len = str.length();
        for(int idx = 0; idx < len; idx++)
            hash = 31 * hash + llc[str.charAt(idx)];

        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj != null)
        {
            if(obj instanceof CIString)
                return string.equalsIgnoreCase(((CIString)obj).string);
            if(obj instanceof String)
                return string.equalsIgnoreCase((String)obj);
        }
        return false;
    }

    public String toString()
    {
        return string;
    }

    static 
    {
        lc = new char[256];
        for(char idx = '\0'; idx < '\u0100'; idx++)
            lc[idx] = Character.toLowerCase(idx);

    }
}
