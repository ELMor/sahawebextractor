/* HttpURLConnection - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package HTTPClient;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

public class HttpURLConnection extends java.net.HttpURLConnection
    implements GlobalConstants
{
    private static Hashtable connections = new Hashtable();
    private HTTPConnection con;
    private String resource;
    private String method;
    private boolean method_set;
    private static NVPair[] default_headers = new NVPair[0];
    private NVPair[] headers;
    private HTTPResponse resp;
    private boolean do_redir;
    private static Class redir_mod;
    private OutputStream output_stream;
    private static String non_proxy_hosts;
    private static String proxy_host;
    private static int proxy_port;
    private String[] hdr_keys;
    private String[] hdr_values;

    public HttpURLConnection(URL url)
        throws ProtocolNotSuppException, IOException {
        super(url);
        try {
            String hosts = System.getProperty("http.nonProxyHosts", "");
            if (!hosts.equalsIgnoreCase(non_proxy_hosts)) {
                connections.clear();
                non_proxy_hosts = hosts;
                String[] list = Util.splitProperty(hosts);
                for (int idx = 0; idx < list.length; idx++)
                    HTTPConnection.dontProxyFor(list[idx]);
            }
        } catch (ParseException pe) {
            throw new IOException(pe.toString());
        } catch (SecurityException securityexception) {
            /* empty */
        }
        try {
            String host = System.getProperty("http.proxyHost", "");
            int port = Integer.getInteger("http.proxyPort", -1).intValue();
            if (!host.equalsIgnoreCase(proxy_host) || port != proxy_port) {
                connections.clear();
                proxy_host = host;
                proxy_port = port;
                HTTPConnection.setProxyServer(host, port);
            }
        } catch (SecurityException securityexception) {
            /* empty */
        }
        con = getConnection(url);
        method = "GET";
        method_set = false;
        resource = url.getFile();
        headers = default_headers;
        do_redir = java.net.HttpURLConnection.getFollowRedirects();
        output_stream = null;
    }

    private HTTPConnection getConnection(URL url)
        throws ProtocolNotSuppException {
        String php = (url.getProtocol() + ":" + url.getHost() + ":"
                      + (url.getPort() != -1 ? url.getPort()
                         : URI.defaultPort(url.getProtocol())));
        php = php.toLowerCase();
        HTTPConnection con = (HTTPConnection) connections.get(php);
        if (con != null)
            return con;
        con = new HTTPConnection(url);
        connections.put(php, con);
        return con;
    }

    public void setRequestMethod(String method) throws ProtocolException {
        if (connected)
            throw new ProtocolException("Already connected!");
        if (GlobalConstants.DebugURLC)
            System.err.println("URLC:  (" + url + ") Setting request method: "
                               + method);
        this.method = method.trim().toUpperCase();
        method_set = true;
    }

    public String getRequestMethod() {
        return method;
    }

    public int getResponseCode() throws IOException {
        if (!connected)
            connect();
        try {
            return resp.getStatusCode();
        } catch (ModuleException me) {
            throw new IOException(me.toString());
        }
    }

    public String getResponseMessage() throws IOException {
        if (!connected)
            connect();
        try {
            return resp.getReasonLine();
        } catch (ModuleException me) {
            throw new IOException(me.toString());
        }
    }

    public String getHeaderField(String name) {
        try {
            if (!connected)
                connect();
            return resp.getHeader(name);
        } catch (Exception e) {
            return null;
        }
    }

    public int getHeaderFieldInt(String name, int def) {
        try {
            if (!connected)
                connect();
            return resp.getHeaderAsInt(name);
        } catch (Exception e) {
            return def;
        }
    }

    public long getHeaderFieldDate(String name, long def) {
        try {
            if (!connected)
                connect();
            return resp.getHeaderAsDate(name).getTime();
        } catch (Exception e) {
            return def;
        }
    }

    public String getHeaderFieldKey(int n) {
        if (hdr_keys == null)
            fill_hdr_arrays();
        if (n >= 0 && n < hdr_keys.length)
            return hdr_keys[n];
        return null;
    }

    public String getHeaderField(int n) {
        if (hdr_values == null)
            fill_hdr_arrays();
        if (n >= 0 && n < hdr_values.length)
            return hdr_values[n];
        return null;
    }

    private void fill_hdr_arrays() {
        try {
            if (!connected)
                connect();
            int num = 1;
            Enumeration enum = resp.listHeaders();
            while (enum.hasMoreElements()) {
                num++;
                enum.nextElement();
            }
            hdr_keys = new String[num];
            hdr_values = new String[num];
            enum = resp.listHeaders();
            for (int idx = 1; idx < num; idx++) {
                hdr_keys[idx] = (String) enum.nextElement();
                hdr_values[idx] = resp.getHeader(hdr_keys[idx]);
            }
            hdr_values[0] = (resp.getVersion() + " " + resp.getStatusCode()
                             + " " + resp.getReasonLine());
        } catch (Exception e) {
            hdr_keys = hdr_values = new String[0];
        }
    }

    public InputStream getInputStream() throws IOException {
        if (!doInput)
            throw new ProtocolException
                      ("Input not enabled! (use setDoInput(true))");
        if (!connected)
            connect();
        InputStream stream;
        try {
            stream = resp.getInputStream();
        } catch (ModuleException e) {
            throw new IOException(e.toString());
        }
        return stream;
    }

    public InputStream getErrorStream() {
        try {
            if (!doInput || !connected || resp.getStatusCode() < 300
                || resp.getHeaderAsInt("Content-length") <= 0)
                return null;
            return resp.getInputStream();
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized OutputStream getOutputStream() throws IOException {
        if (connected)
            throw new ProtocolException("Already connected!");
        if (!doOutput)
            throw new ProtocolException
                      ("Output not enabled! (use setDoOutput(true))");
        if (!method_set)
            method = "POST";
        else if (method.equals("HEAD") || method.equals("GET")
                 || method.equals("TRACE"))
            throw new ProtocolException("Method " + method
                                        + " does not support output!");
        if (getRequestProperty("Content-type") == null)
            setRequestProperty("Content-type",
                               "application/x-www-form-urlencoded");
        if (output_stream == null) {
            if (GlobalConstants.DebugURLC)
                System.err
                    .println("URLC:  (" + url + ") creating output stream");
            String cl = getRequestProperty("Content-Length");
            if (cl != null)
                output_stream = new HttpOutputStream(Integer.parseInt(cl));
            else if (getRequestProperty("Content-type")
                         .equals("application/x-www-form-urlencoded"))
                output_stream = new ByteArrayOutputStream(300);
            else
                output_stream = new HttpOutputStream();
            if (output_stream instanceof HttpOutputStream)
                connect();
        }
        return output_stream;
    }

    public URL getURL() {
        if (connected) {
            try {
                if (resp.getEffectiveURL() != null)
                    return resp.getEffectiveURL();
            } catch (Exception e) {
                return null;
            }
        }
        return url;
    }

    public void setIfModifiedSince(long time) {
        super.setIfModifiedSince(time);
        setRequestProperty("If-Modified-Since", Util.httpDate(new Date(time)));
    }

    public void setRequestProperty(String name, String value) {
        if (GlobalConstants.DebugURLC)
            System.err.println("URLC:  (" + url
                               + ") Setting request property: " + name + " : "
                               + value);
        int idx;
        for (idx = 0; idx < headers.length; idx++) {
            if (headers[idx].getName().equalsIgnoreCase(name))
                break;
        }
        if (idx == headers.length)
            headers = Util.resizeArray(headers, idx + 1);
        headers[idx] = new NVPair(name, value);
    }

    public String getRequestProperty(String name) {
        for (int idx = 0; idx < headers.length; idx++) {
            if (headers[idx].getName().equalsIgnoreCase(name))
                return headers[idx].getValue();
        }
        return null;
    }

    public static void setDefaultRequestProperty(String name, String value) {
        if (GlobalConstants.DebugURLC)
            System.err.println("URLC:  Setting default request property: "
                               + name + " : " + value);
        int idx;
        for (idx = 0; idx < default_headers.length; idx++) {
            if (default_headers[idx].getName().equalsIgnoreCase(name))
                break;
        }
        if (idx == default_headers.length)
            default_headers = Util.resizeArray(default_headers, idx + 1);
        default_headers[idx] = new NVPair(name, value);
    }

    public static String getDefaultRequestProperty(String name) {
        for (int idx = 0; idx < default_headers.length; idx++) {
            if (default_headers[idx].getName().equalsIgnoreCase(name))
                return default_headers[idx].getValue();
        }
        return null;
    }

    public void setInstanceFollowRedirects(boolean set) {
        if (connected)
            throw new IllegalStateException("Already connected!");
        do_redir = set;
    }

    public boolean getInstanceFollowRedirects() {
        return do_redir;
    }

    public synchronized void connect() throws IOException {
        if (!connected) {
            if (GlobalConstants.DebugURLC)
                System.err.println("URLC:  (" + url + ") Connecting ...");
            synchronized (con) {
                con.setAllowUserInteraction(allowUserInteraction);
                if (do_redir)
                    con.addModule(redir_mod, 2);
                else
                    con.removeModule(redir_mod);
                try {
                    if (output_stream instanceof ByteArrayOutputStream)
                        resp = con.ExtensionMethod(method, resource,
                                                   ((ByteArrayOutputStream)
                                                    output_stream)
                                                       .toByteArray(),
                                                   headers);
                    else
                        resp = con.ExtensionMethod(method, resource,
                                                   ((HttpOutputStream)
                                                    output_stream),
                                                   headers);
                } catch (ModuleException e) {
                    throw new IOException(e.toString());
                }
            }
            connected = true;
        }
    }

    public void disconnect() {
        if (GlobalConstants.DebugURLC)
            System.err.println("URLC:  (" + url + ") Disconnecting ...");
        con.stop();
    }

    public boolean usingProxy() {
        return con.getProxyHost() != null;
    }

    public String toString() {
        return this.getClass().getName() + "[" + url + "]";
    }

    static {
        try {
            if (Boolean.getBoolean("HTTPClient.HttpURLConnection.AllowUI"))
                URLConnection.setDefaultAllowUserInteraction(true);
        } catch (SecurityException securityexception) {
            /* empty */
        }
        try {
            redir_mod = Class.forName("HTTPClient.RedirectionModule");
        } catch (ClassNotFoundException cnfe) {
            throw new NoClassDefFoundError(cnfe.getMessage());
        }
        try {
            String agent = System.getProperty("http.agent");
            if (agent != null)
                setDefaultRequestProperty("User-Agent", agent);
        } catch (SecurityException securityexception) {
            /* empty */
        }
        non_proxy_hosts = "";
        proxy_host = "";
        proxy_port = -1;
    }
}
