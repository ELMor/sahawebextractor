// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SocksClient.java

package HTTPClient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

// Referenced classes of package HTTPClient:
//            SocksException, AuthSchemeNotImplException, GlobalConstants, AuthorizationInfo, 
//            NVPair

class SocksClient
    implements GlobalConstants
{

    private String socks_host;
    private int socks_port;
    private int socks_version;
    private static final byte CONNECT = 1;
    private static final byte BIND = 2;
    private static final byte UDP_ASS = 3;
    private static final byte NO_AUTH = 0;
    private static final byte GSSAPI = 1;
    private static final byte USERPWD = 2;
    private static final byte NO_ACC = -1;
    private static final byte IP_V4 = 1;
    private static final byte DMNAME = 3;
    private static final byte IP_V6 = 4;
    private boolean v4A;
    private byte user[];

    SocksClient(String host, int port)
    {
        v4A = false;
        user = null;
        socks_host = host;
        socks_port = port;
        socks_version = -1;
    }

    SocksClient(String host, int port, int version)
        throws SocksException
    {
        v4A = false;
        user = null;
        socks_host = host;
        socks_port = port;
        if(version != 4 && version != 5)
        {
            throw new SocksException("SOCKS Version not supported: " + version);
        } else
        {
            socks_version = version;
            return;
        }
    }

    Socket getSocket(String host, int port)
        throws IOException
    {
        Socket sock = null;
        try
        {
            if(GlobalConstants.DebugSocks)
                System.err.println("Socks: contacting server on " + socks_host + ":" + socks_port);
            sock = connect(socks_host, socks_port);
            InputStream inp = sock.getInputStream();
            OutputStream out = sock.getOutputStream();
            switch(socks_version)
            {
            case 4: // '\004'
                v4ProtExchg(inp, out, host, port);
                break;

            case 5: // '\005'
                v5ProtExchg(inp, out, host, port);
                break;

            case -1: 
                try
                {
                    v4ProtExchg(inp, out, host, port);
                    socks_version = 4;
                }
                catch(SocksException se)
                {
                    if(GlobalConstants.DebugSocks)
                        System.err.println("Socks: V4 request failed: " + ((Throwable) (se)).getMessage());
                    sock.close();
                    sock = connect(socks_host, socks_port);
                    inp = sock.getInputStream();
                    out = sock.getOutputStream();
                    v5ProtExchg(inp, out, host, port);
                    socks_version = 5;
                }
                break;

            default:
                throw new Error("SocksClient internal error: unknown version " + socks_version);
            }
            if(GlobalConstants.DebugSocks)
                System.err.println("Socks: connection established.");
            return sock;
        }
        catch(IOException ioe)
        {
            if(sock != null)
                try
                {
                    sock.close();
                }
                catch(IOException ee) { }
            throw ioe;
        }
    }

    private static final Socket connect(String host, int port)
        throws IOException
    {
        InetAddress addr_list[] = InetAddress.getAllByName(host);
        for(int idx = 0; idx < addr_list.length; idx++)
            try
            {
                return new Socket(addr_list[idx], port);
            }
            catch(SocketException se)
            {
                if(idx >= addr_list.length - 1)
                    throw se;
            }

        return null;
    }

    private void v4ProtExchg(InputStream inp, OutputStream out, String host, int port)
        throws SocksException, IOException
    {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(100);
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Beginning V4 Protocol Exchange for host " + host + ":" + port);
        byte addr[] = {
            0, 0, 0, 42
        };
        if(!v4A)
        {
            try
            {
                addr = InetAddress.getByName(host).getAddress();
            }
            catch(UnknownHostException uhe)
            {
                v4A = true;
            }
            catch(SecurityException se)
            {
                v4A = true;
            }
            if(GlobalConstants.DebugSocks && v4A)
                System.err.println("Socks: Switching to version 4A");
        }
        if(user == null)
        {
            String user_str;
            try
            {
                user_str = System.getProperty("user.name", "");
            }
            catch(SecurityException se)
            {
                user_str = "";
            }
            user = new byte[user_str.length() + 1];
            user_str.getBytes(0, user_str.length(), user, 0);
            user[user_str.length()] = 0;
        }
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Sending connect request for user " + new String(user, 0, 0, user.length - 1));
        buffer.reset();
        buffer.write(4);
        buffer.write(1);
        buffer.write(port >> 8 & 0xff);
        buffer.write(port & 0xff);
        buffer.write(addr, 0, addr.length);
        buffer.write(user, 0, user.length);
        if(v4A)
        {
            byte host_buf[] = new byte[host.length()];
            host.getBytes(0, host.length(), host_buf, 0);
            buffer.write(host_buf, 0, host_buf.length);
            buffer.write(0);
        }
        buffer.writeTo(out);
        int version = inp.read();
        if(version == -1)
            throw new SocksException("Connection refused by server");
        if(version == 4)
            if(GlobalConstants.DebugSocks)
                System.err.println("Socks: Warning: received version 4 instead of 0");
            else
            if(version != 0)
                throw new SocksException("Received invalid version: " + version + "; expected: 0");
        int sts = inp.read();
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Received response; version: " + version + "; status: " + sts);
        byte skip[];
        switch(sts)
        {
        case 91: // '['
            throw new SocksException("Connection request rejected");

        case 92: // '\\'
            throw new SocksException("Connection request rejected: can't connect to identd");

        case 93: // ']'
            throw new SocksException("Connection request rejected: identd reports different user-id from " + new String(user, 0, 0, user.length - 1));

        default:
            throw new SocksException("Connection request rejected: unknown error " + sts);

        case 90: // 'Z'
            skip = new byte[6];
            break;
        }
        int rcvd = 0;
        for(int tot = 0; tot < skip.length && (rcvd = inp.read(skip, 0, skip.length - tot)) != -1; tot += rcvd);
    }

    private void v5ProtExchg(InputStream inp, OutputStream out, String host, int port)
        throws SocksException, IOException
    {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream(100);
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Beginning V5 Protocol Exchange for host " + host + ":" + port);
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Sending authentication request; methods No-Authentication, Username/Password");
        buffer.reset();
        buffer.write(5);
        buffer.write(2);
        buffer.write(0);
        buffer.write(2);
        buffer.writeTo(out);
        int version = inp.read();
        if(version == -1)
            throw new SocksException("Connection refused by server");
        if(version != 5)
            throw new SocksException("Received invalid version: " + version + "; expected: 5");
        int method = inp.read();
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Received response; version: " + version + "; method: " + method);
        switch(method)
        {
        case 1: // '\001'
            negotiate_gssapi(inp, out);
            break;

        case 2: // '\002'
            negotiate_userpwd(inp, out);
            break;

        case -1: 
            throw new SocksException("Server unwilling to accept any standard authentication methods");

        default:
            throw new SocksException("Cannot handle authentication method " + method);

        case 0: // '\0'
            break;
        }
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Sending connect request");
        buffer.reset();
        buffer.write(5);
        buffer.write(1);
        buffer.write(0);
        buffer.write(3);
        buffer.write(host.length() & 0xff);
        byte hname[] = new byte[host.length()];
        host.getBytes(0, host.length(), hname, 0);
        buffer.write(hname, 0, hname.length);
        buffer.write(port >> 8 & 0xff);
        buffer.write(port & 0xff);
        buffer.writeTo(out);
        version = inp.read();
        if(version != 5)
            throw new SocksException("Received invalid version: " + version + "; expected: 5");
        int sts = inp.read();
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Received response; version: " + version + "; status: " + sts);
        switch(sts)
        {
        case 1: // '\001'
            throw new SocksException("General SOCKS server failure");

        case 2: // '\002'
            throw new SocksException("Connection not allowed");

        case 3: // '\003'
            throw new SocksException("Network unreachable");

        case 4: // '\004'
            throw new SocksException("Host unreachable");

        case 5: // '\005'
            throw new SocksException("Connection refused");

        case 6: // '\006'
            throw new SocksException("TTL expired");

        case 7: // '\007'
            throw new SocksException("Command not supported");

        case 8: // '\b'
            throw new SocksException("Address type not supported");

        default:
            throw new SocksException("Unknown reply received from server: " + sts);

        case 0: // '\0'
            inp.read();
            break;
        }
        int atype = inp.read();
        int alen;
        switch(atype)
        {
        case 4: // '\004'
            alen = 16;
            break;

        case 1: // '\001'
            alen = 4;
            break;

        case 3: // '\003'
            alen = inp.read();
            break;

        case 2: // '\002'
        default:
            throw new SocksException("Invalid address type received from server: " + atype);
        }
        byte skip[] = new byte[alen + 2];
        int rcvd = 0;
        for(int tot = 0; tot < skip.length && (rcvd = inp.read(skip, 0, skip.length - tot)) != -1; tot += rcvd);
    }

    private void negotiate_gssapi(InputStream inp, OutputStream out)
        throws SocksException, IOException
    {
        throw new SocksException("GSSAPI authentication protocol not implemented");
    }

    private void negotiate_userpwd(InputStream inp, OutputStream out)
        throws SocksException, IOException
    {
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Entering authorization subnegotiation; method: Username/Password");
        AuthorizationInfo auth_info;
        try
        {
            auth_info = AuthorizationInfo.getAuthorization(socks_host, socks_port, "SOCKS5", "USER/PASS", true);
        }
        catch(AuthSchemeNotImplException atnie)
        {
            auth_info = null;
        }
        if(auth_info == null)
            throw new SocksException("No Authorization info for SOCKS found (server requested username/password).");
        NVPair unpw[] = auth_info.getParams();
        if(unpw == null || unpw.length == 0)
            throw new SocksException("No Username/Password found in authorization info for SOCKS.");
        String user_str = unpw[0].getName();
        String pass_str = unpw[0].getValue();
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Sending authorization request for user " + user_str);
        byte buffer[] = new byte[2 + user_str.length() + 1 + pass_str.length()];
        buffer[0] = 1;
        buffer[1] = (byte)user_str.length();
        user_str.getBytes(0, ((int) (buffer[1])), buffer, 2);
        buffer[2 + buffer[1]] = (byte)pass_str.length();
        pass_str.getBytes(0, ((int) (buffer[2 + buffer[1]])), buffer, 2 + buffer[1] + 1);
        out.write(buffer);
        int version = inp.read();
        if(version != 1)
            throw new SocksException("Wrong version received in username/password subnegotiation response: " + version + "; expected: 1");
        int sts = inp.read();
        if(sts != 0)
            throw new SocksException("Username/Password authentication failed; status: " + sts);
        if(GlobalConstants.DebugSocks)
            System.err.println("Socks: Received response; version: " + version + "; status: " + sts);
    }

    public String toString()
    {
        return ((Object)this).getClass().getName() + "[" + socks_host + ":" + socks_port + "]";
    }
}
