// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HTTPClientModule.java

package HTTPClient;

import java.io.IOException;

// Referenced classes of package HTTPClient:
//            HTTPClientModuleConstants, ModuleException, Request, Response, 
//            RoRequest

public interface HTTPClientModule
    extends HTTPClientModuleConstants
{

    public abstract int requestHandler(Request request, Response aresponse[])
        throws IOException, ModuleException;

    public abstract void responsePhase1Handler(Response response, RoRequest rorequest)
        throws IOException, ModuleException;

    public abstract int responsePhase2Handler(Response response, Request request)
        throws IOException, ModuleException;

    public abstract void responsePhase3Handler(Response response, RoRequest rorequest)
        throws IOException, ModuleException;

    public abstract void trailerHandler(Response response, RoRequest rorequest)
        throws IOException, ModuleException;
}
