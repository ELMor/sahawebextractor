// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   RespInputStream.java

package HTTPClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            ModuleException, GlobalConstants, ResponseHandler, Response,
//            StreamDemultiplexor, HTTPResponse, Util

final class RespInputStream extends InputStream
    implements GlobalConstants
{

    private StreamDemultiplexor demux;
    private ResponseHandler resph;
    boolean closed;
    private boolean dont_truncate;
    private byte buffer[];
    private boolean interrupted;
    private int offset;
    private int end;
    int count;
    private byte ch[];

    RespInputStream(StreamDemultiplexor demux, ResponseHandler resph)
    {
        this.demux = null;
        closed = false;
        dont_truncate = false;
        buffer = null;
        interrupted = false;
        offset = 0;
        end = 0;
        count = 0;
        ch = new byte[1];
        this.demux = demux;
        this.resph = resph;
    }

    public synchronized int read()
        throws IOException
    {
        int rcvd = read(ch, 0, 1);
        if(rcvd == 1)
            return ch[0] & 0xff;
        else
            return -1;
    }

    public synchronized int read(byte b[], int off, int len)
        throws IOException
    {
        if(closed)
            return -1;
        int left = end - offset;
        if(buffer != null && (left != 0 || !interrupted))
            if(left == 0)
            {
                return -1;
            } else
            {
                len = len <= left ? len : left;
                System.arraycopy(((Object) (buffer)), offset, ((Object) (b)), off, len);
                offset += len;
                return len;
            }
        if(GlobalConstants.DebugDemux && resph.resp.cd_type != 1)
            System.err.println("RspIS: Reading stream " + ((Object)this).hashCode() + " (" + Thread.currentThread() + ")");
        int rcvd;
        if(resph.resp.cd_type == 1)
            rcvd = demux.read(b, off, len, resph, resph.resp.timeout);
        else
            rcvd = demux.read(b, off, len, resph, 0);
        if(rcvd != -1 && resph.resp.got_headers)
            count += rcvd;
        return rcvd;
    }

    public synchronized long skip(long num)
        throws IOException
    {
        if(closed)
            return 0L;
        int left = end - offset;
        if(buffer != null && (left != 0 || !interrupted))
        {
            num = num <= (long)left ? num : left;
            offset += ((int) (num));
            return num;
        }
        long skpd = demux.skip(num, resph);
        if(resph.resp.got_headers)
            count += ((int) (skpd));
        return skpd;
    }

    public synchronized int available()
        throws IOException
    {
        if(closed)
            return 0;
        if(buffer != null && (end - offset != 0 || !interrupted))
            return end - offset;
        else
            return demux.available(resph);
    }

    public synchronized void close()
        throws IOException
    {
        if(!closed)
        {
            closed = true;
            if(dont_truncate && (buffer == null || interrupted))
                readAll(resph.resp.timeout);
            if(GlobalConstants.DebugDemux)
                System.err.println("RspIS: User closed stream " + ((Object)this).hashCode() + " (" + Thread.currentThread() + ")");
            demux.closeSocketIfAllStreamsClosed();
            if(dont_truncate)
                try
                {
                    resph.resp.http_resp.invokeTrailerHandlers(false);
                }
                catch(ModuleException me)
                {
                    throw new IOException(((Throwable) (me)).toString());
                }
        }
    }

    protected void finalize()
        throws Throwable
    {
        try
        {
            close();
        }
        finally
        {
            finalize();
        }
    }

    void readAll(int timeout)
        throws IOException
    {
        if(GlobalConstants.DebugDemux)
            System.err.println("RspIS: Read-all on stream " + ((Object)this).hashCode() + " (" + Thread.currentThread() + ")");
        synchronized(resph.resp)
        {
            if(!resph.resp.got_headers)
            {
                int sav_to = resph.resp.timeout;
                resph.resp.timeout = timeout;
                resph.resp.getStatusCode();
                resph.resp.timeout = sav_to;
            }
        }
        synchronized(this)
        {
            if(buffer != null && !interrupted)
                return;
            int rcvd = 0;
            try
            {
                if(closed)
                {
                    buffer = new byte[10000];
                    do
                    {
                        count += rcvd;
                        rcvd = demux.read(buffer, 0, buffer.length, resph, timeout);
                    } while(rcvd != -1);
                    buffer = null;
                } else
                {
                    if(buffer == null)
                    {
                        buffer = new byte[10000];
                        offset = 0;
                        end = 0;
                    }
                    do
                    {
                        rcvd = demux.read(buffer, end, buffer.length - end, resph, timeout);
                        if(rcvd < 0)
                            break;
                        count += rcvd;
                        end += rcvd;
                        buffer = Util.resizeArray(buffer, end + 10000);
                    } while(true);
                }
            }
            catch(InterruptedIOException iioe)
            {
                interrupted = true;
                throw iioe;
            }
            catch(IOException ioe)
            {
                buffer = null;
            }
            interrupted = false;
        }
    }

    synchronized void dontTruncate()
    {
        dont_truncate = true;
    }
}
