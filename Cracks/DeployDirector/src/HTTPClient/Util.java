// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Util.java

package HTTPClient;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.BitSet;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package HTTPClient:
//            NVPair, AuthorizationInfo, Cookie, ParseException, 
//            HttpHeaderElement, URI

public class Util
{

    private static final BitSet Separators;
    private static final BitSet TokenChar;
    private static final BitSet UnsafeChar;
    private static SimpleDateFormat http_format;
    static final char hex_map[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        'A', 'B', 'C', 'D', 'E', 'F'
    };

    private Util()
    {
    }

    static final Object[] resizeArray(Object src[], int new_size)
    {
        Object tmp[] = new Object[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final NVPair[] resizeArray(NVPair src[], int new_size)
    {
        NVPair tmp[] = new NVPair[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final AuthorizationInfo[] resizeArray(AuthorizationInfo src[], int new_size)
    {
        AuthorizationInfo tmp[] = new AuthorizationInfo[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final Cookie[] resizeArray(Cookie src[], int new_size)
    {
        Cookie tmp[] = new Cookie[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final String[] resizeArray(String src[], int new_size)
    {
        String tmp[] = new String[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final boolean[] resizeArray(boolean src[], int new_size)
    {
        boolean tmp[] = new boolean[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final byte[] resizeArray(byte src[], int new_size)
    {
        byte tmp[] = new byte[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final char[] resizeArray(char src[], int new_size)
    {
        char tmp[] = new char[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static final int[] resizeArray(int src[], int new_size)
    {
        int tmp[] = new int[new_size];
        System.arraycopy(((Object) (src)), 0, ((Object) (tmp)), 0, src.length >= new_size ? new_size : src.length);
        return tmp;
    }

    static String[] splitProperty(String prop)
    {
        if(prop == null)
            return new String[0];
        StringTokenizer tok = new StringTokenizer(prop, "|");
        String list[] = new String[tok.countTokens()];
        for(int idx = 0; idx < list.length; idx++)
            list[idx] = tok.nextToken().trim();

        return list;
    }

    static final Hashtable getList(Hashtable cntxt_list, Object cntxt)
    {
        Hashtable list = (Hashtable)cntxt_list.get(cntxt);
        if(list == null)
            synchronized(cntxt_list)
            {
                list = (Hashtable)cntxt_list.get(cntxt);
                if(list == null)
                {
                    list = new Hashtable();
                    cntxt_list.put(cntxt, ((Object) (list)));
                }
            }
        return list;
    }

    static final int[] compile_search(byte search[])
    {
        int cmp[] = {
            0, 1, 0, 1, 0, 1
        };
        for(int idx = 0; idx < search.length; idx++)
        {
            int end;
            for(end = idx + 1; end < search.length; end++)
                if(search[idx] == search[end])
                    break;

            if(end < search.length)
                if(end - idx > cmp[1])
                {
                    cmp[4] = cmp[2];
                    cmp[5] = cmp[3];
                    cmp[2] = cmp[0];
                    cmp[3] = cmp[1];
                    cmp[0] = idx;
                    cmp[1] = end - idx;
                } else
                if(end - idx > cmp[3])
                {
                    cmp[4] = cmp[2];
                    cmp[5] = cmp[3];
                    cmp[2] = idx;
                    cmp[3] = end - idx;
                } else
                if(end - idx > cmp[3])
                {
                    cmp[4] = idx;
                    cmp[5] = end - idx;
                }
        }

        cmp[1] += cmp[0];
        cmp[3] += cmp[2];
        cmp[5] += cmp[4];
        return cmp;
    }

    static final int findStr(byte search[], int cmp[], byte str[], int beg, int end)
    {
        int c1f = cmp[0];
        int c1l = cmp[1];
        int d1 = c1l - c1f;
        int c2f = cmp[2];
        int c2l = cmp[3];
        int d2 = c2l - c2f;
        int c3f = cmp[4];
        int c3l = cmp[5];
        int d3 = c3l - c3f;
        while(beg + search.length <= end) 
            if(search[c1l] == str[beg + c1l])
            {
                if(search[c1f] == str[beg + c1f])
                {
                    boolean same = true;
                    for(int idx = 0; idx < search.length; idx++)
                    {
                        if(search[idx] == str[beg + idx])
                            continue;
                        same = false;
                        break;
                    }

                    if(same)
                        break;
                }
                beg += d1;
            } else
            if(search[c2l] == str[beg + c2l])
                beg += d2;
            else
            if(search[c3l] == str[beg + c3l])
                beg += d3;
            else
                beg++;
        if(beg + search.length > end)
            return -1;
        else
            return beg;
    }

    public static final String dequoteString(String str)
    {
        if(str.indexOf('\\') == -1)
            return str;
        char buf[] = str.toCharArray();
        int pos = 0;
        int num_deq = 0;
        for(; pos < buf.length; pos++)
            if(buf[pos] == '\\' && pos + 1 < buf.length)
            {
                System.arraycopy(((Object) (buf)), pos + 1, ((Object) (buf)), pos, buf.length - pos - 1);
                num_deq++;
            }

        return new String(buf, 0, buf.length - num_deq);
    }

    public static final String quoteString(String str, String qlist)
    {
        char list[] = qlist.toCharArray();
        int idx;
        for(idx = 0; idx < list.length; idx++)
            if(str.indexOf(((int) (list[idx]))) != -1)
                break;

        if(idx == list.length)
            return str;
        int len = str.length();
        char buf[] = new char[len * 2];
        str.getChars(0, len, buf, 0);
        for(int pos = 0; pos < len; pos++)
            if(qlist.indexOf(((int) (buf[pos])), 0) != -1)
            {
                if(len == buf.length)
                    buf = resizeArray(buf, len + str.length());
                System.arraycopy(((Object) (buf)), pos, ((Object) (buf)), pos + 1, len - pos);
                len++;
                buf[pos++] = '\\';
            }

        return new String(buf, 0, len);
    }

    public static final Vector parseHeader(String header)
        throws ParseException
    {
        return parseHeader(header, true);
    }

    public static final Vector parseHeader(String header, boolean dequote)
        throws ParseException
    {
        if(header == null)
            return null;
        char buf[] = header.toCharArray();
        Vector elems = new Vector();
        boolean first = true;
        int beg = -1;
        int end = 0;
        int len = buf.length;
        int abeg[] = new int[1];
        do
        {
            if(!first)
            {
                beg = skipSpace(buf, end);
                if(beg == len)
                    break;
                if(buf[beg] != ',')
                    throw new ParseException("Bad header format: '" + header + "'\nExpected \",\" at position " + beg);
            }
            first = false;
            beg = skipSpace(buf, beg + 1);
            if(beg == len)
                break;
            if(buf[beg] == ',')
            {
                end = beg;
            } else
            {
                if(buf[beg] == '=' || buf[beg] == ';' || buf[beg] == '"')
                    throw new ParseException("Bad header format: '" + header + "'\nEmpty element name at position " + beg);
                for(end = beg + 1; end < len && !Character.isSpace(buf[end]) && buf[end] != '=' && buf[end] != ',' && buf[end] != ';'; end++);
                String elem_name = new String(buf, beg, end - beg);
                beg = skipSpace(buf, end);
                String elem_value;
                if(beg < len && buf[beg] == '=')
                {
                    abeg[0] = beg + 1;
                    elem_value = parseValue(buf, abeg, header, dequote);
                    end = abeg[0];
                } else
                {
                    elem_value = null;
                    end = beg;
                }
                NVPair params[] = new NVPair[0];
                do
                {
                    beg = skipSpace(buf, end);
                    if(beg == len || buf[beg] != ';')
                        break;
                    beg = skipSpace(buf, beg + 1);
                    if(beg == len || buf[beg] == ',')
                    {
                        end = beg;
                        break;
                    }
                    if(buf[beg] == ';')
                    {
                        end = beg;
                    } else
                    {
                        if(buf[beg] == '=' || buf[beg] == '"')
                            throw new ParseException("Bad header format: '" + header + "'\nEmpty parameter name at position " + beg);
                        for(end = beg + 1; end < len && !Character.isSpace(buf[end]) && buf[end] != '=' && buf[end] != ',' && buf[end] != ';'; end++);
                        String param_name = new String(buf, beg, end - beg);
                        beg = skipSpace(buf, end);
                        String param_value;
                        if(beg < len && buf[beg] == '=')
                        {
                            abeg[0] = beg + 1;
                            param_value = parseValue(buf, abeg, header, dequote);
                            end = abeg[0];
                        } else
                        {
                            param_value = null;
                            end = beg;
                        }
                        params = resizeArray(params, params.length + 1);
                        params[params.length - 1] = new NVPair(param_name, param_value);
                    }
                } while(true);
                elems.addElement(((Object) (new HttpHeaderElement(elem_name, elem_value, params))));
            }
        } while(true);
        return elems;
    }

    private static String parseValue(char buf[], int abeg[], String header, boolean dequote)
        throws ParseException
    {
        int beg = abeg[0];
        int end = beg;
        int len = buf.length;
        beg = skipSpace(buf, beg);
        String value;
        if(beg < len && buf[beg] == '"')
        {
            end = ++beg;
            char deq_buf[] = null;
            int deq_pos = 0;
            int lst_pos = beg;
            for(; end < len && buf[end] != '"'; end++)
                if(buf[end] == '\\')
                    if(dequote)
                    {
                        if(deq_buf == null)
                            deq_buf = new char[buf.length];
                        System.arraycopy(((Object) (buf)), lst_pos, ((Object) (deq_buf)), deq_pos, end - lst_pos);
                        deq_pos += end - lst_pos;
                        lst_pos = ++end;
                    } else
                    {
                        end++;
                    }

            if(end == len)
                throw new ParseException("Bad header format: '" + header + "'\nClosing <\"> for quoted-string" + " starting at position " + (beg - 1) + " not found");
            if(deq_buf != null)
            {
                System.arraycopy(((Object) (buf)), lst_pos, ((Object) (deq_buf)), deq_pos, end - lst_pos);
                deq_pos += end - lst_pos;
                value = new String(deq_buf, 0, deq_pos);
            } else
            {
                value = new String(buf, beg, end - beg);
            }
            end++;
        } else
        {
            for(end = beg; end < len && !Character.isSpace(buf[end]) && buf[end] != ',' && buf[end] != ';'; end++);
            value = new String(buf, beg, end - beg);
        }
        abeg[0] = end;
        return value;
    }

    public static final boolean hasToken(String header, String token)
        throws ParseException
    {
        if(header == null)
            return false;
        else
            return parseHeader(header).contains(((Object) (new HttpHeaderElement(token))));
    }

    public static final HttpHeaderElement getElement(Vector header, String name)
    {
        int idx = header.indexOf(((Object) (new HttpHeaderElement(name))));
        if(idx == -1)
            return null;
        else
            return (HttpHeaderElement)header.elementAt(idx);
    }

    public static final String getParameter(String param, String hdr)
        throws ParseException
    {
        NVPair params[] = ((HttpHeaderElement)parseHeader(hdr).firstElement()).getParams();
        for(int idx = 0; idx < params.length; idx++)
            if(params[idx].getName().equalsIgnoreCase(param))
                return params[idx].getValue();

        return null;
    }

    public static final String assembleHeader(Vector pheader)
    {
        StringBuffer hdr = new StringBuffer(200);
        int len = pheader.size();
        for(int idx = 0; idx < len; idx++)
        {
            ((HttpHeaderElement)pheader.elementAt(idx)).appendTo(hdr);
            hdr.append(", ");
        }

        hdr.setLength(hdr.length() - 2);
        return hdr.toString();
    }

    static final int skipSpace(char str[], int pos)
    {
        for(int len = str.length; pos < len && Character.isSpace(str[pos]); pos++);
        return pos;
    }

    static final int findSpace(char str[], int pos)
    {
        for(int len = str.length; pos < len && !Character.isSpace(str[pos]); pos++);
        return pos;
    }

    static final int skipToken(char str[], int pos)
    {
        for(int len = str.length; pos < len && TokenChar.get(((int) (str[pos]))); pos++);
        return pos;
    }

    static final boolean needsQuoting(String str)
    {
        int len = str.length();
        int pos;
        for(pos = 0; pos < len && TokenChar.get(((int) (str.charAt(pos)))); pos++);
        return pos < len;
    }

    public static final boolean sameHttpURL(URL url1, URL url2)
    {
        if(!url1.getProtocol().equalsIgnoreCase(url2.getProtocol()))
            return false;
        if(!url1.getHost().equalsIgnoreCase(url2.getHost()))
            return false;
        int port1 = url1.getPort();
        int port2 = url2.getPort();
        if(port1 == -1)
            port1 = URI.defaultPort(url1.getProtocol());
        if(port2 == -1)
            port2 = URI.defaultPort(url1.getProtocol());
        if(port1 != port2)
            return false;
        try
        {
            return URI.unescape(url1.getFile()).equals(((Object) (URI.unescape(url2.getFile()))));
        }
        catch(ParseException pe)
        {
            return url1.getFile().equals(((Object) (url2.getFile())));
        }
    }

    /**
     * @deprecated Method defaultPort is deprecated
     */

    public static final int defaultPort(String protocol)
    {
        return URI.defaultPort(protocol);
    }

    public static final String httpDate(Date date)
    {
        if(http_format == null)
            synchronized(HTTPClient.Util.class)
            {
                if(http_format == null)
                {
                    http_format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);
                    ((DateFormat) (http_format)).setTimeZone(((java.util.TimeZone) (new SimpleTimeZone(0, "GMT"))));
                }
            }
        return ((DateFormat) (http_format)).format(date);
    }

    static final String escapeUnsafeChars(String path)
    {
        int len = path.length();
        char buf[] = new char[3 * len];
        int dst = 0;
        for(int src = 0; src < len; src++)
        {
            char ch = path.charAt(src);
            if(ch >= '\200' || UnsafeChar.get(((int) (ch))))
            {
                buf[dst++] = '%';
                buf[dst++] = hex_map[(ch & 0xf0) >>> 4];
                buf[dst++] = hex_map[ch & 0xf];
            } else
            {
                buf[dst++] = ch;
            }
        }

        if(dst > len)
            return new String(buf, 0, dst);
        else
            return path;
    }

    public static final String getPath(String resource)
    {
        int end = resource.length();
        int p;
        if((p = resource.indexOf('#')) != -1)
            end = p;
        if((p = resource.indexOf('?')) != -1 && p < end)
            end = p;
        if((p = resource.indexOf(';')) != -1 && p < end)
            end = p;
        return resource.substring(0, end);
    }

    public static final String getParams(String resource)
    {
        int beg;
        if((beg = resource.indexOf(';')) == -1)
            return null;
        int f;
        if((f = resource.indexOf('#')) != -1 && f < beg)
            return null;
        int q;
        if((q = resource.indexOf('?')) != -1 && q < beg)
            return null;
        if(q == -1 && f == -1)
            return resource.substring(beg + 1);
        if(f == -1 || q != -1 && q < f)
            return resource.substring(beg + 1, q);
        else
            return resource.substring(beg + 1, f);
    }

    public static final String getQuery(String resource)
    {
        int beg;
        if((beg = resource.indexOf('?')) == -1)
            return null;
        int f;
        if((f = resource.indexOf('#')) != -1 && f < beg)
            return null;
        if(f == -1)
            return resource.substring(beg + 1);
        else
            return resource.substring(beg + 1, f);
    }

    public static final String getFragment(String resource)
    {
        int beg;
        if((beg = resource.indexOf('#')) == -1)
            return null;
        else
            return resource.substring(beg + 1);
    }

    static 
    {
        Separators = new BitSet(128);
        TokenChar = new BitSet(128);
        UnsafeChar = new BitSet(128);
        Separators.set(40);
        Separators.set(41);
        Separators.set(60);
        Separators.set(62);
        Separators.set(64);
        Separators.set(44);
        Separators.set(59);
        Separators.set(58);
        Separators.set(92);
        Separators.set(34);
        Separators.set(47);
        Separators.set(91);
        Separators.set(93);
        Separators.set(63);
        Separators.set(61);
        Separators.set(123);
        Separators.set(125);
        Separators.set(32);
        Separators.set(9);
        for(int ch = 32; ch < 127; ch++)
            TokenChar.set(ch);

        TokenChar.xor(Separators);
        for(int ch = 0; ch < 32; ch++)
            UnsafeChar.set(ch);

        UnsafeChar.set(32);
        UnsafeChar.set(60);
        UnsafeChar.set(62);
        UnsafeChar.set(34);
        UnsafeChar.set(123);
        UnsafeChar.set(125);
        UnsafeChar.set(124);
        UnsafeChar.set(92);
        UnsafeChar.set(94);
        UnsafeChar.set(126);
        UnsafeChar.set(91);
        UnsafeChar.set(93);
        UnsafeChar.set(96);
        UnsafeChar.set(127);
    }
}
