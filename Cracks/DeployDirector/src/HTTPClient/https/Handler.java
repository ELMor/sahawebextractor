// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Handler.java

package HTTPClient.https;

import HTTPClient.HTTPConnection;
import HTTPClient.HttpURLConnection;
import HTTPClient.ProtocolNotSuppException;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class Handler extends URLStreamHandler
{

    public Handler()
        throws ProtocolNotSuppException
    {
        new HTTPConnection("https", "", -1);
    }

    public URLConnection openConnection(URL url)
        throws IOException, ProtocolNotSuppException
    {
        return ((URLConnection) (new HttpURLConnection(url)));
    }
}
