/* Codecs - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package HTTPClient;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.BitSet;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

public class Codecs
{
    private static BitSet BoundChar = new BitSet(256);
    private static BitSet EBCDICUnsafeChar;
    private static byte[] Base64EncMap;
    private static byte[] Base64DecMap;
    private static char[] UUEncMap;
    private static byte[] UUDecMap;
    private static final String ContDisp
        = "\r\nContent-Disposition: form-data; name=\"";
    private static final String FileName = "\"; filename=\"";
    private static final String Boundary
        = "\r\n-----ieoau._._+2_8_GoodLuck8.3-dskdfJwSJKlrWLr0234324jfLdsjfdAuaoei-----";
    private static NVPair[] dummy;

    private Codecs() {
        /* empty */
    }

    public static final String base64Encode(String str) {
        if (str == null)
            return null;
        byte[] data = new byte[str.length()];
        str.getBytes(0, str.length(), data, 0);
        return new String(base64Encode(data), 0);
    }

    public static final byte[] base64Encode(byte[] data) {
        if (data == null)
            return null;
        byte[] dest = new byte[(data.length + 2) / 3 * 4];
        int sidx = 0;
        int didx = 0;
        for (/**/; sidx < data.length - 2; sidx += 3) {
            dest[didx++] = Base64EncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = Base64EncMap[(data[sidx + 1] >>> 4 & 0xf
                                         | data[sidx] << 4 & 0x3f)];
            dest[didx++] = Base64EncMap[(data[sidx + 2] >>> 6 & 0x3
                                         | data[sidx + 1] << 2 & 0x3f)];
            dest[didx++] = Base64EncMap[data[sidx + 2] & 0x3f];
        }
        if (sidx < data.length) {
            dest[didx++] = Base64EncMap[data[sidx] >>> 2 & 0x3f];
            if (sidx < data.length - 1) {
                dest[didx++] = Base64EncMap[(data[sidx + 1] >>> 4 & 0xf
                                             | data[sidx] << 4 & 0x3f)];
                dest[didx++] = Base64EncMap[data[sidx + 1] << 2 & 0x3f];
            } else
                dest[didx++] = Base64EncMap[data[sidx] << 4 & 0x3f];
        }
        for (/**/; didx < dest.length; didx++)
            dest[didx] = (byte) 61;
        return dest;
    }

    public static final String base64Decode(String str) {
        if (str == null)
            return null;
        byte[] data = new byte[str.length()];
        str.getBytes(0, str.length(), data, 0);
        return new String(base64Decode(data), 0);
    }

    public static final byte[] base64Decode(byte[] data) {
        if (data == null)
            return null;
        int tail;
        for (tail = data.length; data[tail - 1] == 61; tail--) {
            /* empty */
        }
        byte[] dest = new byte[tail - data.length / 4];
        for (int idx = 0; idx < data.length; idx++)
            data[idx] = Base64DecMap[data[idx]];
        int sidx = 0;
        int didx;
        for (didx = 0; didx < dest.length - 2; didx += 3) {
            dest[didx]
                = (byte) (data[sidx] << 2 & 0xff | data[sidx + 1] >>> 4 & 0x3);
            dest[didx + 1] = (byte) (data[sidx + 1] << 4 & 0xff
                                     | data[sidx + 2] >>> 2 & 0xf);
            dest[didx + 2]
                = (byte) (data[sidx + 2] << 6 & 0xff | data[sidx + 3] & 0x3f);
            sidx += 4;
        }
        if (didx < dest.length)
            dest[didx]
                = (byte) (data[sidx] << 2 & 0xff | data[sidx + 1] >>> 4 & 0x3);
        if (++didx < dest.length)
            dest[didx] = (byte) (data[sidx + 1] << 4 & 0xff
                                 | data[sidx + 2] >>> 2 & 0xf);
        return dest;
    }

    public static final char[] uuencode(byte[] data) {
        if (data == null)
            return null;
        if (data.length == 0)
            return new char[0];
        int line_len = 45;
        char[] nl = System.getProperty("line.separator", "\n").toCharArray();
        char[] dest
            = (new char
               [(data.length + 2) / 3 * 4 + ((data.length + line_len - 1)
                                             / line_len * (nl.length + 1))]);
        int sidx = 0;
        int didx = 0;
        while (sidx + line_len < data.length) {
            dest[didx++] = UUEncMap[line_len];
            for (int end = sidx + line_len; sidx < end; sidx += 3) {
                dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
                dest[didx++] = UUEncMap[(data[sidx + 1] >>> 4 & 0xf
                                         | data[sidx] << 4 & 0x3f)];
                dest[didx++] = UUEncMap[(data[sidx + 2] >>> 6 & 0x3
                                         | data[sidx + 1] << 2 & 0x3f)];
                dest[didx++] = UUEncMap[data[sidx + 2] & 0x3f];
            }
            for (int idx = 0; idx < nl.length; idx++)
                dest[didx++] = nl[idx];
        }
        dest[didx++] = UUEncMap[data.length - sidx];
        for (/**/; sidx + 2 < data.length; sidx += 3) {
            dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = UUEncMap[(data[sidx + 1] >>> 4 & 0xf
                                     | data[sidx] << 4 & 0x3f)];
            dest[didx++] = UUEncMap[(data[sidx + 2] >>> 6 & 0x3
                                     | data[sidx + 1] << 2 & 0x3f)];
            dest[didx++] = UUEncMap[data[sidx + 2] & 0x3f];
        }
        if (sidx < data.length - 1) {
            dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = UUEncMap[(data[sidx + 1] >>> 4 & 0xf
                                     | data[sidx] << 4 & 0x3f)];
            dest[didx++] = UUEncMap[data[sidx + 1] << 2 & 0x3f];
            dest[didx++] = UUEncMap[0];
        } else if (sidx < data.length) {
            dest[didx++] = UUEncMap[data[sidx] >>> 2 & 0x3f];
            dest[didx++] = UUEncMap[data[sidx] << 4 & 0x3f];
            dest[didx++] = UUEncMap[0];
            dest[didx++] = UUEncMap[0];
        }
        for (int idx = 0; idx < nl.length; idx++)
            dest[didx++] = nl[idx];
        if (didx != dest.length)
            throw new Error("Calculated " + dest.length + " chars but wrote "
                            + didx + " chars!");
        return dest;
    }

    private static final byte[] uudecode(BufferedReader rdr)
        throws ParseException, IOException {
        String line;
        while ((line = rdr.readLine()) != null && !line.startsWith("begin ")) {
            /* empty */
        }
        if (line == null)
            throw new ParseException("'begin' line not found");
        StringTokenizer tok = new StringTokenizer(line);
        tok.nextToken();
        try {
            int file_mode = Integer.parseInt(tok.nextToken(), 8);
        } catch (Exception e) {
            throw new ParseException("Invalid mode on line: " + line);
        }
        try {
            String file_name = tok.nextToken();
        } catch (NoSuchElementException e) {
            throw new ParseException("No file name found on line: " + line);
        }
        byte[] body = new byte[1000];
        int off = 0;
        while ((line = rdr.readLine()) != null && !line.equals("end")) {
            byte[] tmp = uudecode(line.toCharArray());
            if (off + tmp.length > body.length)
                body = Util.resizeArray(body, off + 1000);
            System.arraycopy(tmp, 0, body, off, tmp.length);
            off += tmp.length;
        }
        if (line == null)
            throw new ParseException("'end' line not found");
        return Util.resizeArray(body, off);
    }

    public static final byte[] uudecode(char[] data) {
        if (data == null)
            return null;
        byte[] dest = new byte[data.length / 4 * 3];
        int sidx = 0;
        int didx = 0;
        while (sidx < data.length) {
            int len = UUDecMap[data[sidx++]];
            int end = didx + len;
            while (didx < end - 2) {
                byte A = UUDecMap[data[sidx]];
                byte B = UUDecMap[data[sidx + 1]];
                byte C = UUDecMap[data[sidx + 2]];
                byte D = UUDecMap[data[sidx + 3]];
                dest[didx++] = (byte) (A << 2 & 0xff | B >>> 4 & 0x3);
                dest[didx++] = (byte) (B << 4 & 0xff | C >>> 2 & 0xf);
                dest[didx++] = (byte) (C << 6 & 0xff | D & 0x3f);
                sidx += 4;
            }
            if (didx < end) {
                byte A = UUDecMap[data[sidx]];
                byte B = UUDecMap[data[sidx + 1]];
                dest[didx++] = (byte) (A << 2 & 0xff | B >>> 4 & 0x3);
            }
            if (didx < end) {
                byte B = UUDecMap[data[sidx + 1]];
                byte C = UUDecMap[data[sidx + 2]];
                dest[didx++] = (byte) (B << 4 & 0xff | C >>> 2 & 0xf);
            }
            for (/**/; sidx < data.length && data[sidx] != '\n'; sidx++) {
                if (data[sidx] == '\r')
                    break;
            }
            for (/**/; sidx < data.length && (data[sidx] == '\n'
                                              || data[sidx] == '\r'); sidx++) {
                /* empty */
            }
        }
        return Util.resizeArray(dest, didx);
    }

    public static final String quotedPrintableEncode(String str) {
        if (str == null)
            return null;
        char[] map = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A',
                       'B', 'C', 'D', 'E', 'F' };
        char[] nl = System.getProperty("line.separator", "\n").toCharArray();
        char[] res = new char[(int) ((double) str.length() * 1.5)];
        char[] src = str.toCharArray();
        int cnt = 0;
        int didx = 1;
        int last = 0;
        int slen = str.length();
        for (int sidx = 0; sidx < slen; sidx++) {
            char ch = src[sidx];
            if (ch == nl[0] && match(src, sidx, nl)) {
                if (res[didx - 1] == 32) {
                    res[didx - 1] = '=';
                    res[didx++] = '2';
                    res[didx++] = '0';
                } else if (res[didx - 1] == 9) {
                    res[didx - 1] = '=';
                    res[didx++] = '0';
                    res[didx++] = '9';
                }
                res[didx++] = '\r';
                res[didx++] = '\n';
                sidx += nl.length - 1;
                cnt = didx;
            } else if (ch > 126 || ch < 32 && ch != 9 || ch == 61
                       || EBCDICUnsafeChar.get(ch)) {
                res[didx++] = '=';
                res[didx++] = map[(ch & 0xf0) >>> 4];
                res[didx++] = map[ch & 0xf];
            } else
                res[didx++] = ch;
            if (didx > cnt + 70) {
                res[didx++] = '=';
                res[didx++] = '\r';
                res[didx++] = '\n';
                cnt = didx;
            }
            if (didx > res.length - 5)
                res = Util.resizeArray(res, res.length + 500);
        }
        return String.valueOf(res, 1, didx - 1);
    }

    private static final boolean match(char[] str, int start, char[] arr) {
        if (str.length < start + arr.length)
            return false;
        for (int idx = 1; idx < arr.length; idx++) {
            if (str[start + idx] != arr[idx])
                return false;
        }
        return true;
    }

    public static final String quotedPrintableDecode(String str)
        throws ParseException {
        if (str == null)
            return null;
        char[] res = new char[(int) ((double) str.length() * 1.1)];
        char[] src = str.toCharArray();
        char[] nl = System.getProperty("line.separator", "\n").toCharArray();
        int last = 0;
        int didx = 0;
        int slen = str.length();
        int sidx = 0;
        while (sidx < slen) {
            char ch = src[sidx++];
            if (ch == '=') {
                if (sidx >= slen - 1)
                    throw new ParseException
                              ("Premature end of input detected");
                if (src[sidx] == '\n' || src[sidx] == '\r') {
                    sidx++;
                    if (src[sidx - 1] == '\r' && src[sidx] == '\n')
                        sidx++;
                } else {
                    int hi = Character.digit(src[sidx], 16);
                    int lo = Character.digit(src[sidx + 1], 16);
                    if ((hi | lo) < 0)
                        throw new ParseException(new String(src, sidx - 1, 3)
                                                 + " is an invalid code");
                    char repl = (char) (hi << 4 | lo);
                    sidx += 2;
                    res[didx++] = repl;
                }
                last = didx;
            } else if (ch == '\n' || ch == '\r') {
                if (ch == '\r' && sidx < slen && src[sidx] == '\n')
                    sidx++;
                for (int idx = 0; idx < nl.length; idx++)
                    res[last++] = nl[idx];
                didx = last;
            } else {
                res[didx++] = ch;
                if (ch != ' ' && ch != '\t')
                    last = didx;
            }
            if (didx > res.length - nl.length - 2)
                res = Util.resizeArray(res, res.length + 500);
        }
        return new String(res, 0, didx);
    }

    public static final String URLEncode(String str) {
        if (str == null)
            return null;
        return URLEncoder.encode(str);
    }

    public static final String URLDecode(String str) throws ParseException {
        if (str == null)
            return null;
        char[] res = new char[str.length()];
        int didx = 0;
        for (int sidx = 0; sidx < str.length(); sidx++) {
            char ch = str.charAt(sidx);
            if (ch == '+')
                res[didx++] = ' ';
            else if (ch == '%') {
                try {
                    res[didx++]
                        = (char) Integer.parseInt(str.substring(sidx + 1,
                                                                sidx + 3),
                                                  16);
                    sidx += 2;
                } catch (NumberFormatException e) {
                    throw new ParseException(str.substring(sidx, sidx + 3)
                                             + " is an invalid code");
                }
            } else
                res[didx++] = ch;
        }
        return String.valueOf(res, 0, didx);
    }

    public static final NVPair[] mpFormDataDecode
        (byte[] data, String cont_type, String dir)
        throws IOException, ParseException {
        return mpFormDataDecode(data, cont_type, dir, null);
    }

    public static final NVPair[] mpFormDataDecode
        (byte[] data, String cont_type, String dir, FilenameMangler mangler)
        throws IOException, ParseException {
        String bndstr = Util.getParameter("boundary", cont_type);
        if (bndstr == null)
            throw new ParseException
                      ("'boundary' parameter not found in Content-type: "
                       + cont_type);
        byte[] srtbndry = new byte[bndstr.length() + 4];
        byte[] boundary = new byte[bndstr.length() + 6];
        byte[] endbndry = new byte[bndstr.length() + 6];
        ("--" + bndstr + "\r\n").getBytes(0, srtbndry.length, srtbndry, 0);
        ("\r\n--" + bndstr + "\r\n").getBytes(0, boundary.length, boundary, 0);
        ("\r\n--" + bndstr + "--").getBytes(0, endbndry.length, endbndry, 0);
        int[] bs = Util.compile_search(srtbndry);
        int[] bc = Util.compile_search(boundary);
        int[] be = Util.compile_search(endbndry);
        int start = Util.findStr(srtbndry, bs, data, 0, data.length);
        if (start == -1)
            throw new ParseException("Starting boundary not found: "
                                     + new String(srtbndry, 0));
        start += srtbndry.length;
        NVPair[] res = new NVPair[10];
        boolean done = false;
        int idx = 0;
        while (!done) {
            int end = Util.findStr(boundary, bc, data, start, data.length);
            if (end == -1) {
                end = Util.findStr(endbndry, be, data, start, data.length);
                if (end == -1)
                    throw new ParseException("Ending boundary not found: "
                                             + new String(endbndry, 0));
                done = true;
            }
            String name = null;
            String filename = null;
            String cont_disp = null;
            for (;;) {
                int next = findEOL(data, start) + 2;
                if (next - 2 <= start)
                    break;
                String hdr = new String(data, 0, start, next - 2 - start);
                start = next;
                byte ch;
                while (next < data.length - 1
                       && ((ch = data[next]) == 32 || ch == 9)) {
                    next = findEOL(data, start) + 2;
                    hdr += new String(data, 0, start, next - 2 - start);
                    start = next;
                }
                if (hdr.regionMatches(true, 0, "Content-Disposition", 0, 19)) {
                    Vector pcd
                        = Util.parseHeader(hdr.substring(hdr.indexOf(':')
                                                         + 1));
                    HttpHeaderElement elem = Util.getElement(pcd, "form-data");
                    if (elem == null)
                        throw new ParseException
                                  ("Expected 'Content-Disposition: form-data' in line: "
                                   + hdr);
                    NVPair[] params = elem.getParams();
                    name = filename = null;
                    for (int pidx = 0; pidx < params.length; pidx++) {
                        if (params[pidx].getName().equalsIgnoreCase("name"))
                            name = params[pidx].getValue();
                        if (params[pidx].getName()
                                .equalsIgnoreCase("filename"))
                            filename = params[pidx].getValue();
                    }
                    if (name == null)
                        throw new ParseException
                                  ("'name' parameter not found in header: "
                                   + hdr);
                    cont_disp = hdr;
                }
            }
            start += 2;
            if (start > end)
                throw new ParseException("End of header not found at offset "
                                         + end);
            if (cont_disp == null)
                throw new ParseException
                          ("Missing 'Content-Disposition' header at offset "
                           + start);
            String value;
            if (filename != null) {
                if (mangler != null)
                    filename = mangler.mangleFilename(filename, name);
                if (filename != null) {
                    File file = new File(dir, filename);
                    FileOutputStream out = new FileOutputStream(file);
                    out.write(data, start, end - start);
                    out.close();
                }
                value = filename;
            } else
                value = new String(data, 0, start, end - start);
            if (idx >= res.length)
                res = Util.resizeArray(res, idx + 10);
            res[idx] = new NVPair(name, value);
            start = end + boundary.length;
            idx++;
        }
        return Util.resizeArray(res, idx);
    }

    private static final int findEOL(byte[] arr, int off) {
        while (off < arr.length - 1 && (arr[off++] != 13 || arr[off] != 10)) {
            /* empty */
        }
        return off - 1;
    }

    public static final byte[] mpFormDataEncode
        (NVPair[] opts, NVPair[] files, NVPair[] cont_type)
        throws IOException {
        return mpFormDataEncode(opts, files, cont_type, null);
    }

    public static final byte[] mpFormDataEncode
        (NVPair[] opts, NVPair[] files, NVPair[] cont_type,
         FilenameMangler mangler)
        throws IOException {
        int len = 0;
        int hdr_len = 119;
        byte[] boundary = new byte[74];
        byte[] cont_disp = new byte[40];
        byte[] filename = new byte[13];
        "\r\nContent-Disposition: form-data; name=\"".getBytes
            (0, "\r\nContent-Disposition: form-data; name=\"".length(),
             cont_disp, 0);
        "\"; filename=\"".getBytes(0, "\"; filename=\"".length(), filename, 0);
        "\r\n-----ieoau._._+2_8_GoodLuck8.3-dskdfJwSJKlrWLr0234324jfLdsjfdAuaoei-----"
            .getBytes
            (0,
             "\r\n-----ieoau._._+2_8_GoodLuck8.3-dskdfJwSJKlrWLr0234324jfLdsjfdAuaoei-----"
                 .length(),
             boundary, 0);
        if (opts == null)
            opts = dummy;
        if (files == null)
            files = dummy;
        for (int idx = 0; idx < opts.length; idx++)
            len += (hdr_len + opts[idx].getName().length()
                    + opts[idx].getValue().length());
        for (int idx = 0; idx < files.length; idx++) {
            File file = new File(files[idx].getValue());
            String fname = file.getName();
            if (mangler != null)
                fname = mangler.mangleFilename(fname, files[idx].getName());
            if (fname != null) {
                len += hdr_len + files[idx].getName().length() + 13;
                len += (long) fname.length() + file.length();
            }
        }
        len -= 2;
        len += 78;
        byte[] res = new byte[len];
        int pos = 0;
    while_8_:
        for (int new_c = 808464432; new_c != 2054847098; new_c++) {
            pos = 0;
            for (/**/; !BoundChar.get(new_c & 0xff); new_c++) {
                /* empty */
            }
            for (/**/; !BoundChar.get(new_c >> 8 & 0xff); new_c += 256) {
                /* empty */
            }
            for (/**/; !BoundChar.get(new_c >> 16 & 0xff); new_c += 65536) {
                /* empty */
            }
            for (/**/; !BoundChar.get(new_c >> 24 & 0xff); new_c += 16777216) {
                /* empty */
            }
            boundary[40] = (byte) (new_c & 0xff);
            boundary[42] = (byte) (new_c >> 8 & 0xff);
            boundary[44] = (byte) (new_c >> 16 & 0xff);
            boundary[46] = (byte) (new_c >> 24 & 0xff);
            int off = 2;
            int[] bnd_cmp = Util.compile_search(boundary);
            for (int idx = 0; idx < opts.length; idx++) {
                System.arraycopy(boundary, off, res, pos,
                                 boundary.length - off);
                pos += boundary.length - off;
                off = 0;
                System.arraycopy(cont_disp, 0, res, pos, cont_disp.length);
                pos += cont_disp.length;
                int nlen = opts[idx].getName().length();
                opts[idx].getName().getBytes(0, nlen, res, pos);
                if (nlen >= boundary.length
                    && (Util.findStr(boundary, bnd_cmp, res, pos, pos + nlen)
                        != -1))
                    continue while_8_;
                pos += nlen;
                res[pos++] = (byte) 34;
                res[pos++] = (byte) 13;
                res[pos++] = (byte) 10;
                res[pos++] = (byte) 13;
                res[pos++] = (byte) 10;
                int vlen = opts[idx].getValue().length();
                opts[idx].getValue().getBytes(0, vlen, res, pos);
                if (vlen >= boundary.length
                    && (Util.findStr(boundary, bnd_cmp, res, pos, pos + vlen)
                        != -1))
                    continue while_8_;
                pos += vlen;
            }
            for (int idx = 0; idx < files.length; idx++) {
                File file = new File(files[idx].getValue());
                String fname = file.getName();
                if (mangler != null)
                    fname
                        = mangler.mangleFilename(fname, files[idx].getName());
                if (fname != null) {
                    System.arraycopy(boundary, off, res, pos,
                                     boundary.length - off);
                    pos += boundary.length - off;
                    off = 0;
                    System.arraycopy(cont_disp, 0, res, pos, cont_disp.length);
                    pos += cont_disp.length;
                    int nlen = files[idx].getName().length();
                    files[idx].getName().getBytes(0, nlen, res, pos);
                    if (nlen >= boundary.length
                        && Util.findStr(boundary, bnd_cmp, res, pos,
                                        pos + nlen) != -1)
                        continue while_8_;
                    pos += nlen;
                    System.arraycopy(filename, 0, res, pos, filename.length);
                    pos += filename.length;
                    nlen = fname.length();
                    fname.getBytes(0, nlen, res, pos);
                    if (nlen >= boundary.length
                        && Util.findStr(boundary, bnd_cmp, res, pos,
                                        pos + nlen) != -1)
                        continue while_8_;
                    pos += nlen;
                    res[pos++] = (byte) 34;
                    res[pos++] = (byte) 13;
                    res[pos++] = (byte) 10;
                    res[pos++] = (byte) 13;
                    res[pos++] = (byte) 10;
                    nlen = (int) file.length();
                    int opos = pos;
                    FileInputStream fin = new FileInputStream(file);
                    while (nlen > 0) {
                        int got = fin.read(res, pos, nlen);
                        nlen -= got;
                        pos += got;
                    }
                    if (Util.findStr(boundary, bnd_cmp, res, opos, pos) != -1)
                        continue while_8_;
                }
            }
            break;
        }
        System.arraycopy(boundary, 0, res, pos, boundary.length);
        pos += boundary.length;
        res[pos++] = (byte) 45;
        res[pos++] = (byte) 45;
        res[pos++] = (byte) 13;
        res[pos++] = (byte) 10;
        if (pos != len)
            throw new Error("Calculated " + len + " bytes but wrote " + pos
                            + " bytes!");
        cont_type[0]
            = new NVPair("Content-Type", ("multipart/form-data; boundary="
                                          + new String(boundary, 0, 4, 70)));
        return res;
    }

    public static final String nv2query(NVPair[] pairs) {
        if (pairs == null)
            return null;
        StringBuffer qbuf = new StringBuffer();
        int idx;
        for (idx = 0; idx < pairs.length; idx++)
            qbuf.append(URLEncode(pairs[idx].getName()) + "="
                        + URLEncode(pairs[idx].getValue()) + "&");
        if (idx > 0)
            qbuf.setLength(qbuf.length() - 1);
        return qbuf.toString();
    }

    public static final NVPair[] query2nv(String query) throws ParseException {
        if (query == null)
            return null;
        int idx = -1;
        int cnt = 1;
        while ((idx = query.indexOf('&', idx + 1)) != -1)
            cnt++;
        NVPair[] pairs = new NVPair[cnt];
        idx = 0;
        for (cnt = 0; cnt < pairs.length; cnt++) {
            int eq = query.indexOf('=', idx);
            int end = query.indexOf('&', idx);
            if (end == -1)
                end = query.length();
            if (eq == -1 || eq >= end)
                throw new ParseException("'=' missing in "
                                         + query.substring(idx, end));
            pairs[cnt] = new NVPair(URLDecode(query.substring(idx, eq)),
                                    URLDecode(query.substring(eq + 1, end)));
            idx = end + 1;
        }
        return pairs;
    }

    public static final byte[] chunkedEncode(byte[] data, NVPair[] ftrs,
                                             boolean last) {
        return chunkedEncode(data, 0, data == null ? 0 : data.length, ftrs,
                             last);
    }

    public static final byte[] chunkedEncode(byte[] data, int off, int len,
                                             NVPair[] ftrs, boolean last) {
        if (data == null) {
            data = new byte[0];
            len = 0;
        }
        if (last && ftrs == null)
            ftrs = new NVPair[0];
        String hex_len = Integer.toString(len, 16);
        int res_len = 0;
        if (len > 0)
            res_len += hex_len.length() + 2 + len + 2;
        if (last) {
            res_len += 3;
            for (int idx = 0; idx < ftrs.length; idx++)
                res_len += (ftrs[idx].getName().length() + 2
                            + ftrs[idx].getValue().length() + 2);
            res_len += 2;
        }
        byte[] res = new byte[res_len];
        int r_off = 0;
        if (len > 0) {
            hex_len.getBytes(0, hex_len.length(), res, r_off);
            r_off += hex_len.length();
            res[r_off++] = (byte) 13;
            res[r_off++] = (byte) 10;
            System.arraycopy(data, off, res, r_off, len);
            r_off += len;
            res[r_off++] = (byte) 13;
            res[r_off++] = (byte) 10;
        }
        if (last) {
            res[r_off++] = (byte) 48;
            res[r_off++] = (byte) 13;
            res[r_off++] = (byte) 10;
            for (int idx = 0; idx < ftrs.length; idx++) {
                ftrs[idx].getName().getBytes(0, ftrs[idx].getName().length(),
                                             res, r_off);
                r_off += ftrs[idx].getName().length();
                res[r_off++] = (byte) 58;
                res[r_off++] = (byte) 32;
                ftrs[idx].getValue().getBytes(0, ftrs[idx].getValue().length(),
                                              res, r_off);
                r_off += ftrs[idx].getValue().length();
                res[r_off++] = (byte) 13;
                res[r_off++] = (byte) 10;
            }
            res[r_off++] = (byte) 13;
            res[r_off++] = (byte) 10;
        }
        if (r_off != res.length)
            throw new Error("Calculated " + res.length + " bytes but wrote "
                            + r_off + " bytes!");
        return res;
    }

    public static final Object chunkedDecode(InputStream input)
        throws ParseException, IOException {
        int len = getChunkLength(input);
        if (len > 0) {
            byte[] res = new byte[len];
            int off;
            for (off = 0; len != -1 && off < res.length; off += len)
                len = input.read(res, off, res.length - off);
            if (len == -1)
                throw new ParseException
                          ("Premature EOF while reading chunk;Expected: "
                           + res.length + " Bytes, " + "Received: " + (off + 1)
                           + " Bytes");
            input.read();
            input.read();
            return res;
        }
        NVPair[] res = new NVPair[0];
        DataInputStream datain = new DataInputStream(input);
        String line;
        while ((line = datain.readLine()) != null && line.length() > 0) {
            int colon = line.indexOf(':');
            if (colon == -1)
                throw new ParseException
                          ("Error in Footer format: no ':' found in '" + line
                           + "'");
            res = Util.resizeArray(res, res.length + 1);
            res[res.length - 1] = new NVPair(line.substring(0, colon).trim(),
                                             line.substring(colon + 1).trim());
        }
        return res;
    }

    static final int getChunkLength(InputStream input)
        throws ParseException, IOException {
        byte[] hex_len = new byte[8];
        int off = 0;
        int ch;
        while ((ch = input.read()) != 13 && ch != 10 && ch != 59
               && off < hex_len.length)
            hex_len[off++] = (byte) ch;
        if (ch == 59) {
            while ((ch = input.read()) != 13 && ch != 10) {
                /* empty */
            }
        }
        if (ch != 10 && (ch != 13 || input.read() != 10))
            throw new ParseException("Didn't find valid chunk length: "
                                     + new String(hex_len, 0, 0, off));
        int len;
        try {
            len = Integer.parseInt(new String(hex_len, 0, 0, off).trim(), 16);
        } catch (NumberFormatException nfe) {
            throw new ParseException("Didn't find valid chunk length: "
                                     + new String(hex_len, 0, 0, off));
        }
        return len;
    }

    static {
        for (int ch = 48; ch <= 57; ch++)
            BoundChar.set(ch);
        for (int ch = 65; ch <= 90; ch++)
            BoundChar.set(ch);
        for (int ch = 97; ch <= 122; ch++)
            BoundChar.set(ch);
        BoundChar.set(43);
        BoundChar.set(95);
        BoundChar.set(45);
        BoundChar.set(46);
        EBCDICUnsafeChar = new BitSet(256);
        EBCDICUnsafeChar.set(33);
        EBCDICUnsafeChar.set(34);
        EBCDICUnsafeChar.set(35);
        EBCDICUnsafeChar.set(36);
        EBCDICUnsafeChar.set(64);
        EBCDICUnsafeChar.set(91);
        EBCDICUnsafeChar.set(92);
        EBCDICUnsafeChar.set(93);
        EBCDICUnsafeChar.set(94);
        EBCDICUnsafeChar.set(96);
        EBCDICUnsafeChar.set(123);
        EBCDICUnsafeChar.set(124);
        EBCDICUnsafeChar.set(125);
        EBCDICUnsafeChar.set(126);
        byte[] map
            = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101,
                102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113,
                114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51,
                52, 53, 54, 55, 56, 57, 43, 47 };
        Base64EncMap = map;
        Base64DecMap = new byte[128];
        for (int idx = 0; idx < Base64EncMap.length; idx++)
            Base64DecMap[Base64EncMap[idx]] = (byte) idx;
        UUEncMap = new char[64];
        for (int idx = 0; idx < UUEncMap.length; idx++)
            UUEncMap[idx] = (char) (idx + 32);
        UUDecMap = new byte[128];
        for (int idx = 0; idx < UUEncMap.length; idx++)
            UUDecMap[UUEncMap[idx]] = (byte) idx;
        dummy = new NVPair[0];
    }
}
