// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CIHashtable.java

package HTTPClient;

import java.util.Enumeration;

// Referenced classes of package HTTPClient:
//            CIString

final class CIHashtableEnumeration
    implements Enumeration
{

    Enumeration HTEnum;

    public CIHashtableEnumeration(Enumeration enum)
    {
        HTEnum = enum;
    }

    public boolean hasMoreElements()
    {
        return HTEnum.hasMoreElements();
    }

    public Object nextElement()
    {
        Object tmp = HTEnum.nextElement();
        if(tmp instanceof CIString)
            return ((Object) (((CIString)tmp).getString()));
        else
            return tmp;
    }
}
