// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RoRequest.java

package HTTPClient;


// Referenced classes of package HTTPClient:
//            HTTPConnection, NVPair, HttpOutputStream

public interface RoRequest
{

    public abstract HTTPConnection getConnection();

    public abstract String getMethod();

    public abstract String getRequestURI();

    public abstract NVPair[] getHeaders();

    public abstract byte[] getData();

    public abstract HttpOutputStream getStream();

    public abstract boolean allowUI();
}
