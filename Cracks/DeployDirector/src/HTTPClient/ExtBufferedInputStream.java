// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ExtBufferedInputStream.java

package HTTPClient;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

// Referenced classes of package HTTPClient:
//            Util

final class ExtBufferedInputStream extends FilterInputStream
{

    protected byte buf[];
    protected int count;
    protected int pos;
    protected int markpos;
    protected int marklimit;

    public ExtBufferedInputStream(InputStream in)
    {
        this(in, 2048);
    }

    public ExtBufferedInputStream(InputStream in, int size)
    {
        super(in);
        markpos = -1;
        buf = new byte[size];
    }

    private void fill()
        throws IOException
    {
        if(markpos < 0)
            pos = 0;
        else
        if(pos >= buf.length)
            if(markpos > 0)
            {
                int sz = pos - markpos;
                System.arraycopy(((Object) (buf)), markpos, ((Object) (buf)), 0, sz);
                pos = sz;
                markpos = 0;
            } else
            if(buf.length >= marklimit)
            {
                markpos = -1;
                pos = 0;
            } else
            {
                int nsz = pos * 2;
                if(nsz > marklimit)
                    nsz = marklimit;
                byte nbuf[] = new byte[nsz];
                System.arraycopy(((Object) (buf)), 0, ((Object) (nbuf)), 0, pos);
                buf = nbuf;
            }
        count = pos;
        int n = super.in.read(buf, pos, buf.length - pos);
        count = n > 0 ? n + pos : pos;
    }

    public synchronized int read()
        throws IOException
    {
        if(pos >= count)
        {
            fill();
            if(pos >= count)
                return -1;
        }
        return buf[pos++] & 0xff;
    }

    public synchronized int read(byte b[], int off, int len)
        throws IOException
    {
        int avail = count - pos;
        if(avail <= 0)
        {
            if(len >= buf.length && markpos < 0)
                return super.in.read(b, off, len);
            fill();
            avail = count - pos;
            if(avail <= 0)
                return -1;
        }
        int cnt = avail >= len ? len : avail;
        System.arraycopy(((Object) (buf)), pos, ((Object) (b)), off, cnt);
        pos += cnt;
        return cnt;
    }

    public synchronized long skip(long n)
        throws IOException
    {
        if(n < 0L)
            return 0L;
        long avail = count - pos;
        if(avail >= n)
        {
            pos += ((int) (n));
            return n;
        } else
        {
            pos += ((int) (avail));
            return avail + super.in.skip(n - avail);
        }
    }

    public synchronized int available()
        throws IOException
    {
        return (count - pos) + super.in.available();
    }

    public synchronized void mark(int readlimit)
    {
        marklimit = readlimit;
        markpos = pos;
    }

    public synchronized void reset()
        throws IOException
    {
        if(markpos < 0)
        {
            throw new IOException("Resetting to invalid mark");
        } else
        {
            pos = markpos;
            return;
        }
    }

    public boolean markSupported()
    {
        return true;
    }

    int pastEnd(byte search[], int search_cmp[])
    {
        int idx = Util.findStr(search, search_cmp, buf, markpos, pos);
        if(idx == -1)
        {
            markpos = pos - search.length;
        } else
        {
            markpos = idx + search.length;
            idx = pos - markpos;
        }
        return idx;
    }

    void initMark()
    {
        mark(buf.length);
    }
}
