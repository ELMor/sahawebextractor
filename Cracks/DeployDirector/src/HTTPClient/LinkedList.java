// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   LinkedList.java

package HTTPClient;

import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            LinkElement

class LinkedList
{

    private LinkElement head;
    private LinkElement tail;
    private LinkElement next_enum;

    LinkedList()
    {
        head = null;
        tail = null;
        next_enum = null;
    }

    public synchronized void addToHead(Object elem)
    {
        head = new LinkElement(elem, head);
        if(head.next == null)
            tail = head;
    }

    public synchronized void addToEnd(Object elem)
    {
        if(head == null)
            head = tail = new LinkElement(elem, ((LinkElement) (null)));
        else
            tail = tail.next = new LinkElement(elem, ((LinkElement) (null)));
    }

    public synchronized void remove(Object elem)
    {
        if(head == null)
            return;
        if(head.element == elem)
        {
            head = head.next;
            return;
        }
        for(LinkElement curr = head; curr.next != null; curr = curr.next)
            if(curr.next.element == elem)
            {
                if(curr.next == tail)
                    tail = curr;
                curr.next = curr.next.next;
                return;
            }

    }

    public synchronized Object getFirst()
    {
        if(head == null)
            return ((Object) (null));
        else
            return head.element;
    }

    public synchronized Object enumerate()
    {
        if(head == null)
        {
            return ((Object) (null));
        } else
        {
            next_enum = head.next;
            return head.element;
        }
    }

    public synchronized Object next()
    {
        if(next_enum == null)
        {
            return ((Object) (null));
        } else
        {
            Object elem = next_enum.element;
            next_enum = next_enum.next;
            return elem;
        }
    }

    public static void main(String args[])
        throws Exception
    {
        System.err.println("\n*** Linked List Tests ...");
        LinkedList list = new LinkedList();
        list.addToHead("One");
        list.addToEnd("Last");
        if(!list.getFirst().equals("One"))
            throw new Exception("First element wrong");
        if(!list.enumerate().equals("One"))
            throw new Exception("First element wrong");
        if(!list.next().equals("Last"))
            throw new Exception("Last element wrong");
        if(list.next() != null)
            throw new Exception("End of list wrong");
        list.remove("One");
        if(!list.getFirst().equals("Last"))
            throw new Exception("First element wrong");
        list.remove("Last");
        if(list.getFirst() != null)
            throw new Exception("End of list wrong");
        list = new LinkedList();
        list.addToEnd("Last");
        list.addToHead("One");
        if(!list.getFirst().equals("One"))
            throw new Exception("First element wrong");
        if(!list.enumerate().equals("One"))
            throw new Exception("First element wrong");
        if(!list.next().equals("Last"))
            throw new Exception("Last element wrong");
        if(list.next() != null)
            throw new Exception("End of list wrong");
        if(!list.enumerate().equals("One"))
            throw new Exception("First element wrong");
        list.remove("One");
        if(!list.next().equals("Last"))
            throw new Exception("Last element wrong");
        list.remove("Last");
        if(list.next() != null)
            throw new Exception("End of list wrong");
        list = new LinkedList();
        list.addToEnd("Last");
        list.addToHead("Two");
        list.addToHead("One");
        if(!list.getFirst().equals("One"))
            throw new Exception("First element wrong");
        if(!list.enumerate().equals("One"))
            throw new Exception("First element wrong");
        if(!list.next().equals("Two"))
            throw new Exception("Second element wrong");
        if(!list.next().equals("Last"))
            throw new Exception("Last element wrong");
        if(list.next() != null)
            throw new Exception("End of list wrong");
        list.remove("Last");
        list.remove("Two");
        list.remove("One");
        if(list.getFirst() != null)
        {
            throw new Exception("Empty list wrong");
        } else
        {
            System.err.println("\n*** Tests finished successfuly");
            return;
        }
    }
}
