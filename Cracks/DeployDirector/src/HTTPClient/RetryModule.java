// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   RetryModule.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            RetryException, IdempotentSequence, ParseException, NVPair, 
//            HTTPClientModule, GlobalConstants, ModuleException, Response, 
//            Request, HTTPConnection, HTTPResponse, Util, 
//            RoRequest

class RetryModule
    implements HTTPClientModule, GlobalConstants
{

    RetryModule()
    {
    }

    public int requestHandler(Request req, Response resp[])
    {
        return 0;
    }

    public void responsePhase1Handler(Response resp, RoRequest roreq)
        throws IOException, ModuleException
    {
        try
        {
            resp.getStatusCode();
        }
        catch(RetryException re)
        {
            if(GlobalConstants.DebugMods)
                System.err.println("RtryM: Caught RetryException");
            boolean got_lock = false;
            try
            {
                synchronized(re.first)
                {
                    got_lock = true;
                    IdempotentSequence seq = new IdempotentSequence();
                    for(RetryException e = re.first; e != null; e = e.next)
                        seq.add(e.request);

                    for(RetryException e = re.first; e != null; e = e.next)
                    {
                        Request req = e.request;
                        HTTPConnection con = req.getConnection();
                        if(!seq.isIdempotent(req) || con.ServProtVersKnown && con.ServerProtocolVersion >= 0x10001 && req.num_retries > 0 || (!con.ServProtVersKnown || con.ServerProtocolVersion <= 0x10000) && req.num_retries > 4 || e.response.got_headers || req.getStream() != null)
                        {
                            e.first = null;
                        } else
                        {
                            if(req.getData() != null && e.conn_reset)
                                if(con.ServProtVersKnown && con.ServerProtocolVersion >= 0x10001)
                                    addToken(req, "Expect", "100-continue");
                                else
                                    req.delay_entity = 5000L << req.num_retries;
                            if(e.next != null && e.next.request.getData() != null && (!con.ServProtVersKnown || con.ServerProtocolVersion < 0x10001) && e.conn_reset)
                                addToken(req, "Connection", "close");
                            if(con.ServProtVersKnown && con.ServerProtocolVersion >= 0x10001 && e.conn_reset)
                                req.dont_pipeline = true;
                            req.dont_pipeline = true;
                            if(GlobalConstants.DebugDemux)
                                System.err.println("RtryM: Retrying request '" + req.getMethod() + " " + req.getRequestURI() + "'");
                            if(e.conn_reset)
                                req.num_retries++;
                            e.response.http_resp.set(req, con.sendRequest(req, e.response.timeout));
                            e.exception = null;
                            e.first = null;
                        }
                    }

                }
            }
            catch(NullPointerException npe)
            {
                if(got_lock)
                    throw npe;
            }
            catch(ParseException pe)
            {
                throw new IOException(((Throwable) (pe)).getMessage());
            }
            if(re.exception != null)
            {
                throw re.exception;
            } else
            {
                re.restart = true;
                throw re;
            }
        }
    }

    public int responsePhase2Handler(Response resp, Request req)
    {
        req.delay_entity = 0L;
        req.dont_pipeline = false;
        req.num_retries = 0;
        return 10;
    }

    public void responsePhase3Handler(Response response, RoRequest rorequest)
    {
    }

    public void trailerHandler(Response response, RoRequest rorequest)
    {
    }

    private void addToken(Request req, String hdr, String tok)
        throws ParseException
    {
        NVPair hdrs[] = req.getHeaders();
        int idx;
        for(idx = 0; idx < hdrs.length; idx++)
            if(hdrs[idx].getName().equalsIgnoreCase(hdr))
                break;

        if(idx == hdrs.length)
        {
            hdrs = Util.resizeArray(hdrs, idx + 1);
            hdrs[idx] = new NVPair(hdr, tok);
            req.setHeaders(hdrs);
        } else
        if(!Util.hasToken(hdrs[idx].getValue(), tok))
            hdrs[idx] = new NVPair(hdr, hdrs[idx].getValue() + ", " + tok);
    }
}
