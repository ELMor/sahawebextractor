/* HTTPConnection - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package HTTPClient;
import java.applet.Applet;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Vector;

import com.sitraka.deploy.SSLFactory;

public class HTTPConnection
    implements GlobalConstants, HTTPClientModuleConstants
{
    public static final String version = "RPT-HTTPClient/0.3-2-SITRAKA";
    private static final Object dflt_context = new Object();
    private Object Context = null;
    private int Protocol;
    int ServerProtocolVersion;
    boolean ServProtVersKnown;
    private String RequestProtocolVersion;
    private static boolean no_chunked = false;
    private static boolean force_1_0 = false;
    private String Host;
    private int Port;
    private String Proxy_Host = null;
    private int Proxy_Port;
    private static String Default_Proxy_Host = null;
    private static int Default_Proxy_Port;
    private static CIHashtable non_proxy_host_list = new CIHashtable();
    private static Vector non_proxy_dom_list = new Vector();
    private static Vector non_proxy_addr_list = new Vector();
    private static Vector non_proxy_mask_list = new Vector();
    private SocksClient Socks_client = null;
    private static SocksClient Default_Socks_client = null;
    private StreamDemultiplexor input_demux = null;
    LinkedList DemuxList;
    private LinkedList RequestList;
    private boolean DoesKeepAlive;
    private boolean KeepAliveUnknown;
    private int KeepAliveReqMax;
    private int KeepAliveReqLeft;
    private static boolean NeverPipeline = false;
    private static boolean NoKeepAlives = false;
    private static boolean haveMSLargeWritesBug = false;
    private static int DefaultTimeout = 0;
    private int Timeout;
    private NVPair[] DefaultHeaders;
    private static Vector DefaultModuleList;
    private Vector ModuleList;
    private static boolean DefaultAllowUI = true;
    private boolean AllowUI;
    private Hashtable ssl_socket_factory;
    private Response early_stall;
    private Response late_stall;
    private Response prev_resp;
    private boolean output_finished;

    private class MSLargeWritesBugStream extends FilterOutputStream
    {
        private final int CHUNK_SIZE = 20000;

        MSLargeWritesBugStream(OutputStream os) {
            super(os);
        }

        public void write(byte[] b, int off, int len) throws IOException {
            for (/**/; len > 20000; len -= 20000) {
                out.write(b, off, 20000);
                off += 20000;
            }
            out.write(b, off, len);
        }
    }

    private class EstablishConnection extends Thread
    {
        String actual_host;
        int actual_port;
        IOException exception;
        Socket sock;
        SocksClient Socks_client;
        boolean close;

        EstablishConnection(String host, int port, SocksClient socks) {
            super("EstablishConnection (" + host + ":" + port + ")");
            try {
                this.setDaemon(true);
            } catch (SecurityException securityexception) {
                /* empty */
            }
            actual_host = host;
            actual_port = port;
            Socks_client = socks;
            exception = null;
            sock = null;
            close = false;
        }

        public void run() {
            try {
                if (Socks_client != null)
                    sock = Socks_client.getSocket(actual_host, actual_port);
                else {
                    InetAddress[] addr_list
                        = InetAddress.getAllByName(actual_host);
                    int idx = 0;
                    while (idx < addr_list.length) {
                        try {
                            SSLFactory factory
                                = HTTPConnection.this.getCurrentSSLFactory();
                            if (factory != null
                                && !factory.acceptsOpenSocket()) {
                                sock = factory.getSSLSocket(addr_list[idx],
                                                            actual_port,
                                                            actual_host);
                                if (sock == null)
                                    throw new SocketException
                                              ("Could not create SSL socket connection to: "
                                               + actual_host);
                            } else
                                sock = new Socket(addr_list[idx], actual_port);
                            break;
                        } catch (SocketException se) {
                            if (idx == addr_list.length - 1 || close)
                                throw se;
                            idx++;
                        }
                    }
                }
            } catch (IOException ioe) {
                exception = ioe;
            }
            if (close && sock != null) {
                try {
                    sock.close();
                } catch (IOException ioexception) {
                    /* empty */
                }
                sock = null;
            }
        }

        IOException getException() {
            return exception;
        }

        Socket getSocket() {
            return sock;
        }

        void forget() {
            close = true;
        }
    }

    public HTTPConnection(Applet applet) throws ProtocolNotSuppException {
        this(applet.getCodeBase().getProtocol(),
             applet.getCodeBase().getHost(), applet.getCodeBase().getPort());
    }

    public HTTPConnection(String host) {
        DemuxList = new LinkedList();
        RequestList = new LinkedList();
        DoesKeepAlive = false;
        KeepAliveUnknown = true;
        KeepAliveReqMax = -1;
        DefaultHeaders = new NVPair[0];
        ssl_socket_factory = new Hashtable();
        early_stall = null;
        late_stall = null;
        prev_resp = null;
        output_finished = true;
        Setup(0, host, 80);
    }

    public HTTPConnection(String host, int port) {
        DemuxList = new LinkedList();
        RequestList = new LinkedList();
        DoesKeepAlive = false;
        KeepAliveUnknown = true;
        KeepAliveReqMax = -1;
        DefaultHeaders = new NVPair[0];
        ssl_socket_factory = new Hashtable();
        early_stall = null;
        late_stall = null;
        prev_resp = null;
        output_finished = true;
        Setup(0, host, port);
    }

    public HTTPConnection(String prot, String host, int port)
        throws ProtocolNotSuppException {
        DemuxList = new LinkedList();
        RequestList = new LinkedList();
        DoesKeepAlive = false;
        KeepAliveUnknown = true;
        KeepAliveReqMax = -1;
        DefaultHeaders = new NVPair[0];
        ssl_socket_factory = new Hashtable();
        early_stall = null;
        late_stall = null;
        prev_resp = null;
        output_finished = true;
        prot = prot.trim().toLowerCase();
        if (!prot.equals("http") && !prot.equals("https"))
            throw new ProtocolNotSuppException("Unsupported protocol '" + prot
                                               + "'");
        if (prot.equals("http"))
            Setup(0, host, port);
        else if (prot.equals("https"))
            Setup(1, host, port);
        else if (prot.equals("shttp"))
            Setup(2, host, port);
        else if (prot.equals("http-ng"))
            Setup(3, host, port);
    }

    public HTTPConnection(URL url) throws ProtocolNotSuppException {
        this(url.getProtocol(), url.getHost(), url.getPort());
    }

    private void Setup(int prot, String host, int port) {
        Protocol = prot;
        Host = host.trim().toLowerCase();
        Port = port;
        if (Port == -1)
            Port = URI.defaultPort(getProtocol());
        if (Default_Proxy_Host != null && !matchNonProxy(Host))
            setCurrentProxy(Default_Proxy_Host, Default_Proxy_Port);
        else
            setCurrentProxy(null, 0);
        Socks_client = Default_Socks_client;
        Timeout = DefaultTimeout;
        ModuleList = (Vector) DefaultModuleList.clone();
        AllowUI = DefaultAllowUI;
        if (NoKeepAlives)
            setDefaultHeaders(new NVPair[] { new NVPair("Connection",
                                                        "close") });
    }

    private boolean matchNonProxy(String host) {
        if (non_proxy_host_list.get(host) != null)
            return true;
        for (int idx = 0; idx < non_proxy_dom_list.size(); idx++) {
            if (host.endsWith((String) non_proxy_dom_list.elementAt(idx)))
                return true;
        }
        if (non_proxy_addr_list.size() == 0)
            return false;
        InetAddress[] host_addr;
        try {
            host_addr = InetAddress.getAllByName(host);
        } catch (UnknownHostException uhe) {
            return false;
        }
        for (int idx = 0; idx < non_proxy_addr_list.size(); idx++) {
            byte[] addr = (byte[]) non_proxy_addr_list.elementAt(idx);
            byte[] mask = (byte[]) non_proxy_mask_list.elementAt(idx);
        while_1_:
            for (int idx2 = 0; idx2 < host_addr.length; idx2++) {
                byte[] raw_addr = host_addr[idx2].getAddress();
                if (raw_addr.length == addr.length) {
                    for (int idx3 = 0; idx3 < raw_addr.length; idx3++) {
                        if ((raw_addr[idx3] & mask[idx3])
                            != (addr[idx3] & mask[idx3]))
                            continue while_1_;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public HTTPResponse Head(String file) throws IOException, ModuleException {
        return Head(file, (String) null, null);
    }

    public HTTPResponse Head(String file, NVPair[] form_data)
        throws IOException, ModuleException {
        return Head(file, form_data, null);
    }

    public HTTPResponse Head
        (String file, NVPair[] form_data, NVPair[] headers)
        throws IOException, ModuleException {
        String File = stripRef(file);
        String query = Codecs.nv2query(form_data);
        if (query != null && query.length() > 0)
            File += "?" + (String) query;
        return setupRequest("HEAD", File, headers, null, null);
    }

    public HTTPResponse Head(String file, String query)
        throws IOException, ModuleException {
        return Head(file, query, null);
    }

    public HTTPResponse Head(String file, String query, NVPair[] headers)
        throws IOException, ModuleException {
        String File = stripRef(file);
        if (query != null && query.length() > 0)
            File += "?" + Codecs.URLEncode(query);
        return setupRequest("HEAD", File, headers, null, null);
    }

    public HTTPResponse Get(String file) throws IOException, ModuleException {
        return Get(file, (String) null, null);
    }

    public HTTPResponse Get(String file, NVPair[] form_data)
        throws IOException, ModuleException {
        return Get(file, form_data, null);
    }

    public HTTPResponse Get
        (String file, NVPair[] form_data, NVPair[] headers)
        throws IOException, ModuleException {
        String File = stripRef(file);
        String query = Codecs.nv2query(form_data);
        if (query != null && query.length() > 0)
            File += "?" + (String) query;
        return setupRequest("GET", File, headers, null, null);
    }

    public HTTPResponse Get(String file, String query)
        throws IOException, ModuleException {
        return Get(file, query, null);
    }

    public HTTPResponse Get(String file, String query, NVPair[] headers)
        throws IOException, ModuleException {
        String File = stripRef(file);
        if (query != null && query.length() > 0)
            File += "?" + Codecs.URLEncode(query);
        return setupRequest("GET", File, headers, null, null);
    }

    public HTTPResponse Post(String file) throws IOException, ModuleException {
        return Post(file, (byte[]) null, null);
    }

    public HTTPResponse Post(String file, NVPair[] form_data)
        throws IOException, ModuleException {
        NVPair[] headers = { new NVPair("Content-type",
                                        "application/x-www-form-urlencoded") };
        return Post(file, Codecs.nv2query(form_data), headers);
    }

    public HTTPResponse Post
        (String file, NVPair[] form_data, NVPair[] headers)
        throws IOException, ModuleException {
        int idx;
        for (idx = 0; idx < headers.length; idx++) {
            if (headers[idx].getName().equalsIgnoreCase("Content-type"))
                break;
        }
        if (idx == headers.length) {
            headers = Util.resizeArray(headers, idx + 1);
            headers[idx] = new NVPair("Content-type",
                                      "application/x-www-form-urlencoded");
        }
        return Post(file, Codecs.nv2query(form_data), headers);
    }

    public HTTPResponse Post(String file, String data)
        throws IOException, ModuleException {
        return Post(file, data, null);
    }

    public HTTPResponse Post(String file, String data, NVPair[] headers)
        throws IOException, ModuleException {
        byte[] tmp = null;
        if (data != null && data.length() > 0)
            tmp = data.getBytes();
        return Post(file, tmp, headers);
    }

    public HTTPResponse Post(String file, byte[] data)
        throws IOException, ModuleException {
        return Post(file, data, null);
    }

    public HTTPResponse Post(String file, byte[] data, NVPair[] headers)
        throws IOException, ModuleException {
        if (data == null)
            data = new byte[0];
        return setupRequest("POST", stripRef(file), headers, data, null);
    }

    public HTTPResponse Post(String file, HttpOutputStream stream)
        throws IOException, ModuleException {
        return Post(file, stream, null);
    }

    public HTTPResponse Post
        (String file, HttpOutputStream stream, NVPair[] headers)
        throws IOException, ModuleException {
        return setupRequest("POST", stripRef(file), headers, null, stream);
    }

    public HTTPResponse Put(String file, String data)
        throws IOException, ModuleException {
        return Put(file, data, null);
    }

    public HTTPResponse Put(String file, String data, NVPair[] headers)
        throws IOException, ModuleException {
        byte[] tmp = null;
        if (data != null)
            tmp = data.getBytes();
        return Put(file, tmp, headers);
    }

    public HTTPResponse Put(String file, byte[] data)
        throws IOException, ModuleException {
        return Put(file, data, null);
    }

    public HTTPResponse Put(String file, byte[] data, NVPair[] headers)
        throws IOException, ModuleException {
        if (data == null)
            data = new byte[0];
        return setupRequest("PUT", stripRef(file), headers, data, null);
    }

    public HTTPResponse Put(String file, HttpOutputStream stream)
        throws IOException, ModuleException {
        return Put(file, stream, null);
    }

    public HTTPResponse Put
        (String file, HttpOutputStream stream, NVPair[] headers)
        throws IOException, ModuleException {
        return setupRequest("PUT", stripRef(file), headers, null, stream);
    }

    public HTTPResponse Options(String file)
        throws IOException, ModuleException {
        return Options(file, null, (byte[]) null);
    }

    public HTTPResponse Options(String file, NVPair[] headers)
        throws IOException, ModuleException {
        return Options(file, headers, (byte[]) null);
    }

    public HTTPResponse Options(String file, NVPair[] headers, byte[] data)
        throws IOException, ModuleException {
        return setupRequest("OPTIONS", stripRef(file), headers, data, null);
    }

    public HTTPResponse Options
        (String file, NVPair[] headers, HttpOutputStream stream)
        throws IOException, ModuleException {
        return setupRequest("OPTIONS", stripRef(file), headers, null, stream);
    }

    public HTTPResponse Delete(String file)
        throws IOException, ModuleException {
        return Delete(file, null);
    }

    public HTTPResponse Delete(String file, NVPair[] headers)
        throws IOException, ModuleException {
        return setupRequest("DELETE", stripRef(file), headers, null, null);
    }

    public HTTPResponse Trace(String file, NVPair[] headers)
        throws IOException, ModuleException {
        return setupRequest("TRACE", stripRef(file), headers, null, null);
    }

    public HTTPResponse Trace(String file)
        throws IOException, ModuleException {
        return Trace(file, null);
    }

    public HTTPResponse ExtensionMethod
        (String method, String file, byte[] data, NVPair[] headers)
        throws IOException, ModuleException {
        return setupRequest(method.trim(), stripRef(file), headers, data,
                            null);
    }

    public HTTPResponse ExtensionMethod
        (String method, String file, HttpOutputStream os, NVPair[] headers)
        throws IOException, ModuleException {
        return setupRequest(method.trim(), stripRef(file), headers, null, os);
    }

    public void stop() {
        for (Request req = (Request) RequestList.enumerate(); req != null;
             req = (Request) RequestList.next())
            req.aborted = true;
        for (StreamDemultiplexor demux
                 = (StreamDemultiplexor) DemuxList.enumerate();
             demux != null; demux = (StreamDemultiplexor) DemuxList.next())
            demux.abort();
    }

    public void setDefaultHeaders(NVPair[] headers) {
        int length = headers == null ? 0 : headers.length;
        NVPair[] def_hdrs = new NVPair[length];
        int sidx = 0;
        int didx = 0;
        for (/**/; sidx < length; sidx++) {
            String name = headers[sidx].getName().trim();
            if (!name.equalsIgnoreCase("Content-length")
                && !name.equalsIgnoreCase("Host"))
                def_hdrs[didx++] = headers[sidx];
        }
        if (didx < length)
            def_hdrs = Util.resizeArray(DefaultHeaders, didx);
        synchronized (DefaultHeaders) {
            DefaultHeaders = def_hdrs;
        }
    }

    public NVPair[] getDefaultHeaders() {
        synchronized (DefaultHeaders) {
            NVPair[] headers = new NVPair[DefaultHeaders.length];
            System.arraycopy(DefaultHeaders, 0, headers, 0, headers.length);
            return headers;
        }
    }

    public String getProtocol() {
        switch (Protocol) {
        case 0:
            return "http";
        case 1:
            return "https";
        case 2:
            return "shttp";
        case 3:
            return "http-ng";
        default:
            throw new Error("HTTPClient Internal Error: invalid protocol "
                            + Protocol);
        }
    }

    public String getHost() {
        return Host;
    }

    public int getPort() {
        return Port;
    }

    public String getProxyHost() {
        return Proxy_Host;
    }

    public int getProxyPort() {
        return Proxy_Port;
    }

    public boolean isCompatibleWith(URI uri) {
        if (!uri.getScheme().equals(getProtocol())
            || !uri.getHost().equalsIgnoreCase(Host))
            return false;
        int port = uri.getPort();
        if (port == -1)
            port = URI.defaultPort(uri.getScheme());
        return port == Port;
    }

    /**
     * @deprecated
     */
    public void setRawMode(boolean raw) {
        String[] modules
            = { "HTTPClient.CookieModule", "HTTPClient.RedirectionModule",
                "HTTPClient.AuthorizationModule", "HTTPClient.DefaultModule",
                "HTTPClient.TransferEncodingModule",
                "HTTPClient.ContentMD5Module",
                "HTTPClient.ContentEncodingModule" };
        for (int idx = 0; idx < modules.length; idx++) {
            try {
                if (raw)
                    removeModule(Class.forName(modules[idx]));
                else
                    addModule(Class.forName(modules[idx]), -1);
            } catch (ClassNotFoundException classnotfoundexception) {
                /* empty */
            }
        }
    }

    public static void setDefaultTimeout(int time) {
        DefaultTimeout = time;
    }

    public static int getDefaultTimeout() {
        return DefaultTimeout;
    }

    public void setTimeout(int time) {
        Timeout = time;
    }

    public int getTimeout() {
        return Timeout;
    }

    public void setAllowUserInteraction(boolean allow) {
        AllowUI = allow;
    }

    public boolean getAllowUserInteraction() {
        return AllowUI;
    }

    public static void setDefaultAllowUserInteraction(boolean allow) {
        DefaultAllowUI = allow;
    }

    public static boolean getDefaultAllowUserInteraction() {
        return DefaultAllowUI;
    }

    public static Class[] getDefaultModules() {
        synchronized (DefaultModuleList) {
            Class[] modules = new Class[DefaultModuleList.size()];
            DefaultModuleList.copyInto(modules);
            return modules;
        }
    }

    public static boolean addDefaultModule(Class module, int pos) {
        try {
            HTTPClientModule httpclientmodule
                = (HTTPClientModule) module.newInstance();
        } catch (RuntimeException re) {
            throw re;
        } catch (Exception e) {
            throw new RuntimeException(e.toString());
        }
        synchronized (DefaultModuleList) {
            if (DefaultModuleList.contains(module))
                return false;
            if (pos < 0)
                DefaultModuleList.insertElementAt(module,
                                                  (DefaultModuleList.size()
                                                   + pos + 1));
            else
                DefaultModuleList.insertElementAt(module, pos);
        }
        if (GlobalConstants.DebugConn)
            System.err.println("Conn:  Added module " + module.getName()
                               + " to default list");
        return true;
    }

    public static boolean removeDefaultModule(Class module) {
        boolean removed = DefaultModuleList.removeElement(module);
        if (GlobalConstants.DebugConn && removed)
            System.err.println("Conn:  Removed module " + module.getName()
                               + " from default list");
        return removed;
    }

    public Class[] getModules() {
        synchronized (ModuleList) {
            Class[] modules = new Class[ModuleList.size()];
            ModuleList.copyInto(modules);
            return modules;
        }
    }

    public boolean addModule(Class module, int pos) {
        try {
            HTTPClientModule httpclientmodule
                = (HTTPClientModule) module.newInstance();
        } catch (RuntimeException re) {
            throw re;
        } catch (Exception e) {
            throw new RuntimeException(e.toString());
        }
        synchronized (ModuleList) {
            if (ModuleList.contains(module))
                return false;
            if (pos < 0)
                ModuleList.insertElementAt(module,
                                           ModuleList.size() + pos + 1);
            else
                ModuleList.insertElementAt(module, pos);
        }
        return true;
    }

    public boolean removeModule(Class module) {
        if (module == null)
            return false;
        return ModuleList.removeElement(module);
    }

    public void setContext(Object context) {
        if (context == null)
            throw new IllegalArgumentException("Context must be non-null");
        if (Context != null)
            throw new RuntimeException("Context already set");
        Context = context;
    }

    public Object getContext() {
        if (Context != null)
            return Context;
        return dflt_context;
    }

    static Object getDefaultContext() {
        return dflt_context;
    }

    public void addDigestAuthorization(String realm, String user,
                                       String passwd) {
        AuthorizationInfo.addDigestAuthorization(Host, Port, realm, user,
                                                 passwd, getContext());
    }

    public void addBasicAuthorization(String realm, String user,
                                      String passwd) {
        AuthorizationInfo.addBasicAuthorization(Host, Port, realm, user,
                                                passwd, getContext());
    }

    public static void setProxyServer(String host, int port) {
        if (host == null || host.trim().length() == 0)
            Default_Proxy_Host = null;
        else {
            Default_Proxy_Host = host.trim().toLowerCase();
            Default_Proxy_Port = port;
        }
    }

    public synchronized void setCurrentProxy(String host, int port) {
        if (host == null || host.trim().length() == 0)
            Proxy_Host = null;
        else {
            Proxy_Host = host.trim().toLowerCase();
            if (port <= 0)
                Proxy_Port = 80;
            else
                Proxy_Port = port;
        }
        switch (Protocol) {
        case 0:
        case 1:
            if (force_1_0) {
                ServerProtocolVersion = 65536;
                ServProtVersKnown = true;
                RequestProtocolVersion = "HTTP/1.0";
            } else {
                ServerProtocolVersion = 65537;
                ServProtVersKnown = false;
                RequestProtocolVersion = "HTTP/1.1";
            }
            break;
        case 3:
            ServerProtocolVersion = -1;
            ServProtVersKnown = false;
            RequestProtocolVersion = "";
            break;
        case 2:
            ServerProtocolVersion = -1;
            ServProtVersKnown = false;
            RequestProtocolVersion = "Secure-HTTP/1.3";
            break;
        default:
            throw new Error("HTTPClient Internal Error: invalid protocol "
                            + Protocol);
        }
        KeepAliveUnknown = true;
        DoesKeepAlive = false;
        input_demux = null;
        early_stall = null;
        late_stall = null;
        prev_resp = null;
    }

    public static void dontProxyFor(String host) throws ParseException {
        host = host.trim().toLowerCase();
        if (host.charAt(0) == '.') {
            if (!non_proxy_dom_list.contains(host))
                non_proxy_dom_list.addElement(host);
        } else {
            for (int idx = 0; idx < host.length(); idx++) {
                if (!Character.isDigit(host.charAt(idx))
                    && host.charAt(idx) != '.' && host.charAt(idx) != '/') {
                    non_proxy_host_list.put(host, "");
                    return;
                }
            }
            int slash;
            byte[] ip_addr;
            byte[] ip_mask;
            if ((slash = host.indexOf('/')) != -1) {
                ip_addr = string2arr(host.substring(0, slash));
                ip_mask = string2arr(host.substring(slash + 1));
                if (ip_addr.length != ip_mask.length)
                    throw new ParseException("length of IP-address ("
                                             + ip_addr.length
                                             + ") != length of netmask ("
                                             + ip_mask.length + ")");
            } else {
                ip_addr = string2arr(host);
                ip_mask = new byte[ip_addr.length];
                for (int idx = 0; idx < ip_mask.length; idx++)
                    ip_mask[idx] = (byte) -1;
            }
        while_3_:
            for (int idx = 0; idx < non_proxy_addr_list.size(); idx++) {
                byte[] addr = (byte[]) non_proxy_addr_list.elementAt(idx);
                byte[] mask = (byte[]) non_proxy_mask_list.elementAt(idx);
                if (addr.length == ip_addr.length) {
                    for (int idx2 = 0; idx2 < addr.length; idx2++) {
                        if ((ip_addr[idx2] & mask[idx2]) != (addr[idx2]
                                                             & mask[idx2])
                            || mask[idx2] != ip_mask[idx2])
                            continue while_3_;
                    }
                    return;
                }
            }
            non_proxy_addr_list.addElement(ip_addr);
            non_proxy_mask_list.addElement(ip_mask);
        }
    }

    public static void dontProxyFor(String[] hosts) {
        if (hosts != null && hosts.length != 0) {
            for (int idx = 0; idx < hosts.length; idx++) {
                try {
                    if (hosts[idx] != null)
                        dontProxyFor(hosts[idx]);
                } catch (ParseException parseexception) {
                    /* empty */
                }
            }
        }
    }

    public static boolean doProxyFor(String host) throws ParseException {
        host = host.trim().toLowerCase();
        if (host.charAt(0) == '.')
            return non_proxy_dom_list.removeElement(host);
        for (int idx = 0; idx < host.length(); idx++) {
            if (!Character.isDigit(host.charAt(idx)) && host.charAt(idx) != '.'
                && host.charAt(idx) != '/')
                return non_proxy_host_list.remove(host) != null;
        }
        int slash;
        byte[] ip_addr;
        byte[] ip_mask;
        if ((slash = host.indexOf('/')) != -1) {
            ip_addr = string2arr(host.substring(0, slash));
            ip_mask = string2arr(host.substring(slash + 1));
            if (ip_addr.length != ip_mask.length)
                throw new ParseException("length of IP-address ("
                                         + ip_addr.length
                                         + ") != length of netmask ("
                                         + ip_mask.length + ")");
        } else {
            ip_addr = string2arr(host);
            ip_mask = new byte[ip_addr.length];
            for (int idx = 0; idx < ip_mask.length; idx++)
                ip_mask[idx] = (byte) -1;
        }
    while_5_:
        for (int idx = 0; idx < non_proxy_addr_list.size(); idx++) {
            byte[] addr = (byte[]) non_proxy_addr_list.elementAt(idx);
            byte[] mask = (byte[]) non_proxy_mask_list.elementAt(idx);
            if (addr.length == ip_addr.length) {
                for (int idx2 = 0; idx2 < addr.length; idx2++) {
                    if ((ip_addr[idx2] & mask[idx2]) != (addr[idx2]
                                                         & mask[idx2])
                        || mask[idx2] != ip_mask[idx2])
                        continue while_5_;
                }
                non_proxy_addr_list.removeElementAt(idx);
                non_proxy_mask_list.removeElementAt(idx);
                return true;
            }
        }
        return false;
    }

    private static byte[] string2arr(String ip) {
        char[] ip_char = new char[ip.length()];
        ip.getChars(0, ip_char.length, ip_char, 0);
        int cnt = 0;
        for (int idx = 0; idx < ip_char.length; idx++) {
            if (ip_char[idx] == '.')
                cnt++;
        }
        byte[] arr = new byte[cnt + 1];
        cnt = 0;
        int pos = 0;
        for (int idx = 0; idx < ip_char.length; idx++) {
            if (ip_char[idx] == '.') {
                arr[cnt] = (byte) Integer.parseInt(ip.substring(pos, idx));
                cnt++;
                pos = idx + 1;
            }
        }
        arr[cnt] = (byte) Integer.parseInt(ip.substring(pos));
        return arr;
    }

    public static void setSocksServer(String host) {
        setSocksServer(host, 1080);
    }

    public static void setSocksServer(String host, int port) {
        if (port <= 0)
            port = 1080;
        if (host == null || host.length() == 0)
            Default_Socks_client = null;
        else
            Default_Socks_client = new SocksClient(host, port);
    }

    public static void setSocksServer(String host, int port, int version)
        throws SocksException {
        if (port <= 0)
            port = 1080;
        if (host == null || host.length() == 0)
            Default_Socks_client = null;
        else
            Default_Socks_client = new SocksClient(host, port, version);
    }

    private final String stripRef(String file) {
        if (file == null)
            return "";
        int hash = file.indexOf('#');
        if (hash != -1)
            file = file.substring(0, hash);
        return file.trim();
    }

    private HTTPResponse setupRequest
        (String method, String resource, NVPair[] headers, byte[] entity,
         HttpOutputStream stream)
        throws IOException, ModuleException {
        Request req
            = new Request(this, method, resource, mergedHeaders(headers),
                          entity, stream, AllowUI);
        RequestList.addToEnd(req);
        try {
            HTTPResponse resp
                = new HTTPResponse(gen_mod_insts(), Timeout, req);
            handleRequest(req, resp, null, true);
            return resp;
        } finally {
            RequestList.remove(req);
        }
    }

    private NVPair[] mergedHeaders(NVPair[] spec) {
        int spec_len = spec != null ? spec.length : 0;
        int defs_len;
        NVPair[] merged;
        synchronized (DefaultHeaders) {
            defs_len = DefaultHeaders != null ? DefaultHeaders.length : 0;
            merged = new NVPair[spec_len + defs_len];
            System.arraycopy(DefaultHeaders, 0, merged, 0, defs_len);
        }
        int didx = defs_len;
        for (int sidx = 0; sidx < spec_len; sidx++) {
            String s_name = spec[sidx].getName().trim();
            if (!s_name.equalsIgnoreCase("Content-length")
                && !s_name.equalsIgnoreCase("Host")) {
                int search;
                for (search = 0; search < didx; search++) {
                    if (merged[search].getName().trim()
                            .equalsIgnoreCase(s_name))
                        break;
                }
                merged[search] = spec[sidx];
                if (search == didx)
                    didx++;
            }
        }
        if (didx < merged.length)
            merged = Util.resizeArray(merged, didx);
        return merged;
    }

    private HTTPClientModule[] gen_mod_insts() {
        synchronized (ModuleList) {
            HTTPClientModule[] mod_insts
                = new HTTPClientModule[ModuleList.size()];
            for (int idx = 0; idx < ModuleList.size(); idx++) {
                Class mod = (Class) ModuleList.elementAt(idx);
                try {
                    mod_insts[idx] = (HTTPClientModule) mod.newInstance();
                } catch (Exception e) {
                    throw new Error
                              ("HTTPClient Internal Error: could not create instance of "
                               + mod.getName() + " -\n" + e);
                }
            }
            return mod_insts;
        }
    }

    void handleRequest
        (Request req, HTTPResponse http_resp, Response resp,
         boolean usemodules)
        throws IOException, ModuleException {
        Response[] rsp_arr = { resp };
        HTTPClientModule[] modules = http_resp.getModules();
        if (usemodules) {
        while_6_:
            for (int idx = 0; idx < modules.length; idx++) {
                int sts = modules[idx].requestHandler(req, rsp_arr);
                switch (sts) {
                case 0:
                    break;
                case 1:
                    idx = -1;
                    break;
                case 2:
                    break while_6_;
                case 3:
                case 4:
                    if (rsp_arr[0] == null)
                        throw new Error
                                  ("HTTPClient Internal Error: no response returned by module "
                                   + modules[idx].getClass().getName());
                    http_resp.set(req, rsp_arr[0]);
                    if (req.getStream() != null)
                        req.getStream().ignoreData(req);
                    if (!req.internal_subrequest) {
                        if (sts == 3)
                            http_resp.handleResponse();
                        else
                            http_resp.init(rsp_arr[0]);
                        return;
                    }
                    return;
                case 5:
                    if (!req.internal_subrequest) {
                        req.getConnection().handleRequest(req, http_resp,
                                                          rsp_arr[0], true);
                        return;
                    }
                    return;
                case 6:
                    if (!req.internal_subrequest) {
                        req.getConnection().handleRequest(req, http_resp,
                                                          rsp_arr[0], false);
                        return;
                    }
                    return;
                default:
                    throw new Error
                              ("HTTPClient Internal Error: invalid status "
                               + sts + " returned by module "
                               + modules[idx].getClass().getName());
                }
            }
        }
        if (!req.internal_subrequest) {
            if (req.getStream() != null && req.getStream().getLength() == -1) {
                if (!ServProtVersKnown || ServerProtocolVersion < 65537
                    || no_chunked) {
                    req.getStream().goAhead(req, null, http_resp.getTimeout());
                    http_resp.set(req, req.getStream());
                } else {
                    NVPair[] hdrs = req.getHeaders();
                    int idx;
                    for (idx = 0; idx < hdrs.length; idx++) {
                        if (hdrs[idx].getName()
                                .equalsIgnoreCase("Transfer-Encoding"))
                            break;
                    }
                    if (idx == hdrs.length) {
                        hdrs = Util.resizeArray(hdrs, idx + 1);
                        hdrs[idx] = new NVPair("Transfer-Encoding", "chunked");
                        req.setHeaders(hdrs);
                    } else {
                        String v = hdrs[idx].getValue();
                        try {
                            if (!Util.hasToken(v, "chunked"))
                                hdrs[idx] = new NVPair("Transfer-Encoding",
                                                       v + ", chunked");
                        } catch (ParseException pe) {
                            throw new IOException(pe.toString());
                        }
                    }
                    http_resp.set(req,
                                  sendRequest(req, http_resp.getTimeout()));
                }
            } else
                http_resp.set(req, sendRequest(req, http_resp.getTimeout()));
            if (req.aborted)
                throw new IOException("Request aborted by user");
        }
    }

    Response sendRequest(Request req, int con_timeout)
        throws IOException, ModuleException {
        ByteArrayOutputStream hdr_buf = new ByteArrayOutputStream(600);
        Response resp = null;
        if (early_stall != null) {
            try {
                if (GlobalConstants.DebugConn)
                    System.err.println("Conn:  Early-stalling Request: "
                                       + req.getMethod() + " "
                                       + req.getRequestURI());
                synchronized (early_stall) {
                    try {
                        early_stall.getVersion();
                    } catch (IOException ioexception) {
                        /* empty */
                    }
                    early_stall = null;
                }
            } catch (NullPointerException nullpointerexception) {
                /* empty */
            }
        }
        String[] con_hdrs = assembleHeaders(req, hdr_buf);
        boolean keep_alive;
        try {
            if (ServerProtocolVersion >= 65537 && !Util.hasToken(con_hdrs[0],
                                                                 "close")
                || (ServerProtocolVersion == 65536
                    && Util.hasToken(con_hdrs[0], "keep-alive")))
                keep_alive = true;
            else
                keep_alive = false;
        } catch (ParseException pe) {
            throw new IOException(pe.toString());
        }
        synchronized (this) {
            if (late_stall != null) {
                if (input_demux != null || KeepAliveUnknown) {
                    if (GlobalConstants.DebugConn)
                        System.err.println("Conn:  Stalling Request: "
                                           + req.getMethod() + " "
                                           + req.getRequestURI());
                    try {
                        late_stall.getVersion();
                        if (KeepAliveUnknown)
                            determineKeepAlive(late_stall);
                    } catch (IOException ioexception) {
                        /* empty */
                    }
                }
                late_stall = null;
            }
            if ((req.getMethod().equals("POST") || req.dont_pipeline)
                && prev_resp != null && input_demux != null) {
                if (GlobalConstants.DebugConn)
                    System.err.println("Conn:  Stalling Request: "
                                       + req.getMethod() + " "
                                       + req.getRequestURI());
                try {
                    prev_resp.getVersion();
                } catch (IOException ioexception) {
                    /* empty */
                }
            }
            if (!output_finished) {
                try {
                    this.wait();
                } catch (InterruptedException ie) {
                    throw new IOException(ie.toString());
                }
            }
            if (req.aborted)
                throw new IOException("Request aborted by user");
            int try_count = 3;
            while (try_count-- > 0) {
                try {
                    Socket sock;
                    if (input_demux == null
                        || (sock = input_demux.getSocket()) == null) {
                        sock = getSocket(con_timeout, Protocol);
                        if (Protocol == 1) {
                            SSLFactory fac = getCurrentSSLFactory();
                            if (fac == null)
                                throw new IOException
                                          ("SSL factory could not be loaded");
                            if (Proxy_Host != null) {
                                if (!fac.supportsProxy())
                                    throw new IOException
                                              ("SSL connections through proxies are not supported");
                                Socket[] sarr = { sock };
                                resp = enableSSLTunneling(sarr, req,
                                                          con_timeout);
                                if (resp != null) {
                                    resp.final_resp = true;
                                    return resp;
                                }
                                sock = sarr[0];
                            }
                            if (fac.acceptsOpenSocket()) {
                                sock = fac.getSSLSocket(sock, Host, Port);
                                if (fac.needsSSLHandshake() == true
                                    && !fac.startSSLHandshake(sock))
                                    throw new IOException
                                              ("SSL handshake could not be accomplished");
                            } else if (!fac.startSSLHandshake(sock))
                                throw new IOException
                                          ("SSL handshake could not be accomplished");
                        }
                        input_demux
                            = new StreamDemultiplexor(Protocol, sock, this);
                        DemuxList.addToEnd(input_demux);
                        KeepAliveReqLeft = KeepAliveReqMax;
                    }
                    if (req.aborted)
                        throw new IOException("Request aborted by user");
                    if (GlobalConstants.DebugConn) {
                        System.err.println("Conn:  Sending Request: ");
                        System.err.println();
                        hdr_buf.writeTo(System.err);
                    }
                    OutputStream sock_out = sock.getOutputStream();
                    if (haveMSLargeWritesBug)
                        sock_out = new MSLargeWritesBugStream(sock_out);
                    hdr_buf.writeTo(sock_out);
                    try {
                        if (ServProtVersKnown && ServerProtocolVersion >= 65537
                            && Util.hasToken(con_hdrs[1], "100-continue")) {
                            resp = new Response(req,
                                                (Proxy_Host != null
                                                 && Protocol != 1),
                                                input_demux);
                            resp.timeout = 60;
                            if (resp.getContinue() != 100)
                                break;
                        }
                    } catch (ParseException pe) {
                        throw new IOException(pe.toString());
                    } catch (InterruptedIOException iioe) {
                        /* empty */
                    } finally {
                        if (resp != null)
                            resp.timeout = 0;
                    }
                    if (req.getData() != null && req.getData().length > 0) {
                        if (req.delay_entity > 0L) {
                            long num_units = req.delay_entity / 100L;
                            long one_unit = req.delay_entity / num_units;
                            for (int idx = 0; (long) idx < num_units; idx++) {
                                if (input_demux.available(null) != 0)
                                    break;
                                try {
                                    Thread.sleep(one_unit);
                                } catch (InterruptedException interruptedexception) {
                                    /* empty */
                                }
                            }
                            if (input_demux.available(null) == 0)
                                sock_out.write(req.getData());
                            else
                                keep_alive = false;
                        } else
                            sock_out.write(req.getData());
                    }
                    if (req.getStream() != null)
                        req.getStream().goAhead(req, sock_out, 0);
                    else
                        sock_out.flush();
                    if (resp == null)
                        resp
                            = new Response(req,
                                           Proxy_Host != null && Protocol != 1,
                                           input_demux);
                } catch (IOException ioe) {
                    if (GlobalConstants.DebugConn) {
                        System.err.print("Conn:  ");
                        ioe.printStackTrace();
                    }
                    closeDemux(ioe, true);
                    if (try_count == 0 || ioe instanceof UnknownHostException
                        || ioe instanceof InterruptedIOException
                        || req.aborted)
                        throw ioe;
                    if (GlobalConstants.DebugConn)
                        System.err.println("Conn:  Retrying request");
                    continue;
                }
                break;
            }
            prev_resp = resp;
            if (!KeepAliveUnknown && !DoesKeepAlive || !keep_alive
                || KeepAliveReqMax != -1 && KeepAliveReqLeft-- == 0) {
                input_demux.markForClose(resp);
                input_demux = null;
            } else
                input_demux.restartTimer();
            if (GlobalConstants.DebugConn && KeepAliveReqMax != -1)
                System.err.println("Conn:  Number of requests left: "
                                   + KeepAliveReqLeft);
            if (!ServProtVersKnown) {
                early_stall = resp;
                resp.markAsFirstResponse(req);
            }
            if (KeepAliveUnknown
                || !IdempotentSequence.methodIsIdempotent(req.getMethod())
                || req.dont_pipeline || NeverPipeline)
                late_stall = resp;
            if (req.getStream() != null)
                output_finished = false;
            else {
                output_finished = true;
                this.notify();
            }
            if (GlobalConstants.DebugConn)
                System.err.println("Conn:  Request sent");
        }
        return resp;
    }

    private Socket getSocket(int con_timeout, int protocol)
        throws IOException {
        Socket sock = null;
        if (protocol == 1 && (Proxy_Host != null || Socks_client != null)) {
            SSLFactory fac = getCurrentSSLFactory();
            if (fac == null || !fac.supportsProxy())
                throw new IOException
                          ("SSL connections through proxies are not supported by this factory.");
        }
        String actual_host;
        int actual_port;
        if (Proxy_Host != null) {
            actual_host = Proxy_Host;
            actual_port = Proxy_Port;
        } else {
            actual_host = Host;
            actual_port = Port;
        }
        if (GlobalConstants.DebugConn)
            System.err.println("Conn:  Creating Socket: " + actual_host + ":"
                               + actual_port);
        if (con_timeout == 0) {
            if (Socks_client != null)
                sock = Socks_client.getSocket(actual_host, actual_port);
            else {
                InetAddress[] addr_list
                    = InetAddress.getAllByName(actual_host);
                int idx = 0;
                while (idx < addr_list.length) {
                    try {
                        sock = new Socket(addr_list[idx], actual_port);
                        break;
                    } catch (SocketException se) {
                        if (idx == addr_list.length - 1)
                            throw se;
                        idx++;
                    }
                }
            }
        } else {
            EstablishConnection con
                = new EstablishConnection(actual_host, actual_port,
                                          Socks_client);
            con.start();
            try {
                con.join((long) con_timeout);
            } catch (InterruptedException interruptedexception) {
                /* empty */
            }
            if (con.getException() != null)
                throw con.getException();
            if ((sock = con.getSocket()) == null) {
                con.forget();
                if ((sock = con.getSocket()) == null)
                    throw new InterruptedIOException
                              ("Connection establishment timed out");
            }
        }
        return sock;
    }

    private Response enableSSLTunneling
        (Socket[] sock, Request req, int timeout)
        throws IOException, ModuleException {
        Vector hdrs = new Vector();
        for (int idx = 0; idx < req.getHeaders().length; idx++) {
            String name = req.getHeaders()[idx].getName();
            if (name.equalsIgnoreCase("User-Agent")
                || name.equalsIgnoreCase("Proxy-Authorization"))
                hdrs.addElement(req.getHeaders()[idx]);
        }
        NVPair[] h = new NVPair[hdrs.size()];
        hdrs.copyInto(h);
        Request connect = new Request(this, "CONNECT", Host + ":" + Port, h,
                                      null, null, req.allowUI());
        connect.internal_subrequest = true;
        ByteArrayOutputStream hdr_buf = new ByteArrayOutputStream(600);
        HTTPResponse r = new HTTPResponse(gen_mod_insts(), timeout, connect);
        Response resp = null;
        for (;;) {
            handleRequest(connect, r, resp, true);
            hdr_buf.reset();
            assembleHeaders(connect, hdr_buf);
            if (GlobalConstants.DebugConn) {
                System.err
                    .println("Conn:  Sending SSL-Tunneling Subrequest: ");
                System.err.println();
                hdr_buf.writeTo(System.err);
            }
            hdr_buf.writeTo(sock[0].getOutputStream());
            resp = new Response(connect, sock[0].getInputStream());
            if (resp.getStatusCode() == 200)
                return null;
            try {
                resp.getData();
            } catch (IOException ioexception) {
                /* empty */
            }
            try {
                sock[0].close();
            } catch (IOException ioexception) {
                /* empty */
            }
            r.set(connect, resp);
            if (!r.handleResponse())
                return resp;
            sock[0] = getSocket(timeout, Protocol);
        }
    }

    private SSLFactory getCurrentSSLFactory() throws IOException {
        String classname = null;
        try {
            classname = (String) System.getProperties()
                                     .get("deploy.http.sslsocketfactory");
        } catch (SecurityException e) {
            throw new IOException
                      ("Could not get SSLFactory; SecurityException: "
                       + e.getMessage());
        }
        if (classname == null || classname.trim().length() == 0)
            return null;
        SSLFactory fac = (SSLFactory) ssl_socket_factory.get(classname);
        if (fac != null)
            return fac;
        try {
            Class c = Class.forName(classname);
            fac = (SSLFactory) c.newInstance();
        } catch (ClassNotFoundException e) {
            throw new IOException
                      ("Could not get SSLFactory; ClassNotFoundException: "
                       + e.getMessage());
        } catch (IllegalAccessException e) {
            throw new IOException
                      ("Could not get SSLFactory; IllegalAccessException: "
                       + e.getMessage());
        } catch (InstantiationException e) {
            throw new IOException
                      ("Could not get SSLFactory; InstantiationException: "
                       + e.getMessage());
        } catch (NoClassDefFoundError e) {
            throw new IOException
                      ("Could not get SSLFactory; NoClassDefFoundException: "
                       + e.getMessage());
        }
        return fac;
    }

    private String[] assembleHeaders
        (Request req, ByteArrayOutputStream hdr_buf) throws IOException {
        DataOutputStream dataout = new DataOutputStream(hdr_buf);
        String[] con_hdrs = { "", "" };
        NVPair[] hdrs = req.getHeaders();
        String file = Util.escapeUnsafeChars(req.getRequestURI());
        if (Proxy_Host != null && Protocol != 1 && !file.equals("*"))
            dataout.writeBytes(req.getMethod() + " http://" + Host + ":" + Port
                               + file + " " + RequestProtocolVersion + "\r\n");
        else
            dataout.writeBytes(req.getMethod() + " " + file + " "
                               + RequestProtocolVersion + "\r\n");
        if (Port != 80)
            dataout.writeBytes("Host: " + Host + ":" + Port + "\r\n");
        else
            dataout.writeBytes("Host: " + Host + "\r\n");
        int ct_idx = -1;
        int ua_idx = -1;
        int co_idx = -1;
        int pc_idx = -1;
        int ka_idx = -1;
        int ex_idx = -1;
        int te_idx = -1;
        int tc_idx = -1;
        int ug_idx = -1;
        for (int idx = 0; idx < hdrs.length; idx++) {
            String name = hdrs[idx].getName().trim();
            if (name.equalsIgnoreCase("Content-Type"))
                ct_idx = idx;
            else if (name.equalsIgnoreCase("User-Agent"))
                ua_idx = idx;
            else if (name.equalsIgnoreCase("Connection"))
                co_idx = idx;
            else if (name.equalsIgnoreCase("Proxy-Connection"))
                pc_idx = idx;
            else if (name.equalsIgnoreCase("Keep-Alive"))
                ka_idx = idx;
            else if (name.equalsIgnoreCase("Expect"))
                ex_idx = idx;
            else if (name.equalsIgnoreCase("TE"))
                te_idx = idx;
            else if (name.equalsIgnoreCase("Transfer-Encoding"))
                tc_idx = idx;
            else if (name.equalsIgnoreCase("Upgrade"))
                ug_idx = idx;
        }
        String co_hdr = null;
        if (!ServProtVersKnown || ServerProtocolVersion < 65537
            || co_idx != -1) {
            if (co_idx == -1) {
                co_hdr = "Keep-Alive";
                con_hdrs[0] = "Keep-Alive";
            } else {
                con_hdrs[0] = hdrs[co_idx].getValue().trim();
                co_hdr = con_hdrs[0];
            }
            try {
                if (ka_idx != -1 && Util.hasToken(con_hdrs[0], "keep-alive"))
                    dataout.writeBytes("Keep-Alive: "
                                       + hdrs[ka_idx].getValue().trim()
                                       + "\r\n");
            } catch (ParseException pe) {
                throw new IOException(pe.toString());
            }
        }
        if (Proxy_Host != null && Protocol != 1
            && (!ServProtVersKnown || ServerProtocolVersion < 65537)
            && co_hdr != null) {
            dataout.writeBytes("Proxy-Connection: ");
            dataout.writeBytes(co_hdr);
            dataout.writeBytes("\r\n");
            co_hdr = null;
        }
        if (co_hdr != null) {
            try {
                if (!Util.hasToken(co_hdr, "TE"))
                    co_hdr += ", TE";
            } catch (ParseException pe) {
                throw new IOException(pe.toString());
            }
        } else
            co_hdr = "TE";
        if (ug_idx != -1)
            co_hdr += ", Upgrade";
        if (co_hdr != null) {
            dataout.writeBytes("Connection: ");
            dataout.writeBytes(co_hdr);
            dataout.writeBytes("\r\n");
        }
        if (te_idx != -1) {
            dataout.writeBytes("TE: ");
            Vector pte;
            try {
                pte = Util.parseHeader(hdrs[te_idx].getValue());
            } catch (ParseException pe) {
                throw new IOException(pe.toString());
            }
            if (!pte.contains(new HttpHeaderElement("trailers")))
                dataout.writeBytes("trailers, ");
            dataout.writeBytes(hdrs[te_idx].getValue().trim() + "\r\n");
        } else
            dataout.writeBytes("TE: trailers\r\n");
        if (ua_idx != -1)
            dataout.writeBytes("User-Agent: " + hdrs[ua_idx].getValue().trim()
                               + "\r\n");
        else
            dataout.writeBytes("User-Agent: RPT-HTTPClient/0.3-2-SITRAKA\r\n");
        for (int idx = 0; idx < hdrs.length; idx++) {
            if (idx != ct_idx && idx != ua_idx && idx != co_idx
                && idx != pc_idx && idx != ka_idx && idx != ex_idx
                && idx != te_idx)
                dataout.writeBytes(hdrs[idx].getName().trim() + ": "
                                   + hdrs[idx].getValue().trim() + "\r\n");
        }
        if (req.getData() != null || req.getStream() != null) {
            dataout.writeBytes("Content-Type: ");
            if (ct_idx != -1)
                dataout.writeBytes(hdrs[ct_idx].getValue().trim());
            else
                dataout.writeBytes("application/octet-stream");
            dataout.writeBytes("\r\n");
            if (req.getData() != null)
                dataout.writeBytes("Content-Length: " + req.getData().length
                                   + "\r\n");
            else if (req.getStream().getLength() != -1 && tc_idx == -1)
                dataout.writeBytes("Content-Length: "
                                   + req.getStream().getLength() + "\r\n");
            if (ex_idx != -1) {
                con_hdrs[1] = hdrs[ex_idx].getValue().trim();
                dataout.writeBytes("Expect: " + con_hdrs[1] + "\r\n");
            }
        } else if (ex_idx != -1) {
            Vector expect_tokens;
            try {
                expect_tokens = Util.parseHeader(hdrs[ex_idx].getValue());
            } catch (ParseException pe) {
                throw new IOException(pe.toString());
            }
            HttpHeaderElement cont = new HttpHeaderElement("100-continue");
            while (expect_tokens.removeElement(cont)) {
                /* empty */
            }
            if (!expect_tokens.isEmpty()) {
                con_hdrs[1] = Util.assembleHeader(expect_tokens);
                dataout.writeBytes("Expect: " + con_hdrs[1] + "\r\n");
            }
        }
        dataout.writeBytes("\r\n");
        return con_hdrs;
    }

    boolean handleFirstRequest(Request req, Response resp) throws IOException {
        ServerProtocolVersion = String2ProtVers(resp.getVersion());
        ServProtVersKnown = true;
        if (Proxy_Host != null && Protocol != 1
            && resp.getHeader("Via") == null)
            ServerProtocolVersion = 65536;
        if (GlobalConstants.DebugConn)
            System.err.println("Conn:  Protocol Version established: "
                               + ProtVers2String(ServerProtocolVersion));
        if (ServerProtocolVersion == 65536
            && (resp.getStatusCode() == 400 || resp.getStatusCode() == 500)) {
            input_demux.markForClose(resp);
            input_demux = null;
            RequestProtocolVersion = "HTTP/1.0";
            return false;
        }
        return true;
    }

    private void determineKeepAlive(Response resp) throws IOException {
        try {
            String con;
            if (ServerProtocolVersion >= 65537
                || (((Proxy_Host == null || Protocol == 1)
                     && (con = resp.getHeader("Connection")) != null)
                    || (Proxy_Host != null && Protocol != 1
                        && ((con = resp.getHeader("Proxy-Connection"))
                            != null))) && Util.hasToken(con, "keep-alive")) {
                DoesKeepAlive = true;
                if (GlobalConstants.DebugConn)
                    System.err.println("Conn:  Keep-Alive enabled");
                KeepAliveUnknown = false;
            } else if (resp.getStatusCode() < 400)
                KeepAliveUnknown = false;
            if (DoesKeepAlive && ServerProtocolVersion == 65536
                && (con = resp.getHeader("Keep-Alive")) != null) {
                HttpHeaderElement max
                    = Util.getElement(Util.parseHeader(con), "max");
                if (max != null && max.getValue() != null) {
                    KeepAliveReqMax = Integer.parseInt(max.getValue());
                    KeepAliveReqLeft = KeepAliveReqMax;
                    if (GlobalConstants.DebugConn)
                        System.err.println("Conn:  Max Keep-Alive requests: "
                                           + KeepAliveReqMax);
                }
            }
        } catch (ParseException pe) {
            /* empty */
        } catch (NumberFormatException nfe) {
            /* empty */
        } catch (ClassCastException classcastexception) {
            /* empty */
        }
    }

    synchronized void outputFinished() {
        output_finished = true;
        this.notify();
    }

    synchronized void closeDemux(IOException ioe, boolean was_reset) {
        if (input_demux != null)
            input_demux.close(ioe, was_reset);
        early_stall = null;
        late_stall = null;
        prev_resp = null;
    }

    static final String ProtVers2String(int prot_vers) {
        return "HTTP/" + (prot_vers >>> 16) + "." + (prot_vers & 0xffff);
    }

    static final int String2ProtVers(String prot_vers) {
        String vers = prot_vers.substring(5);
        int dot = vers.indexOf('.');
        return (Integer.parseInt(vers.substring(0, dot)) << 16
                | Integer.parseInt(vers.substring(dot + 1)));
    }

    public String toString() {
        return (getProtocol() + "://" + getHost()
                + (getPort() != URI.defaultPort(getProtocol())
                   ? ":" + getPort() : ""));
    }

    static {
        try {
            String host = System.getProperty("http.proxyHost");
            if (host == null)
                throw new Exception();
            int port = Integer.getInteger("http.proxyPort", -1).intValue();
            if (GlobalConstants.DebugConn)
                System.err.println("Conn:  using proxy " + host + ":" + port);
            setProxyServer(host, port);
        } catch (Exception e) {
            try {
                if (Boolean.getBoolean("proxySet")) {
                    String host = System.getProperty("proxyHost");
                    int port = Integer.getInteger("proxyPort", -1).intValue();
                    if (GlobalConstants.DebugConn)
                        System.err.println("Conn:  using proxy " + host + ":"
                                           + port);
                    setProxyServer(host, port);
                }
            } catch (Exception ee) {
                Default_Proxy_Host = null;
            }
        }
        try {
            String hosts = System.getProperty("HTTPClient.nonProxyHosts");
            if (hosts == null)
                hosts = System.getProperty("http.nonProxyHosts");
            String[] list = Util.splitProperty(hosts);
            dontProxyFor(list);
        } catch (Exception exception) {
            /* empty */
        }
        try {
            String host = System.getProperty("HTTPClient.socksHost");
            if (host != null && host.length() > 0) {
                int port = Integer.getInteger("HTTPClient.socksPort", -1)
                               .intValue();
                int version = Integer.getInteger
                                  ("HTTPClient.socksVersion", -1).intValue();
                if (GlobalConstants.DebugConn)
                    System.err
                        .println("Conn:  using SOCKS " + host + ":" + port);
                if (version == -1)
                    setSocksServer(host, port);
                else
                    setSocksServer(host, port, version);
            }
        } catch (Exception e) {
            Default_Socks_client = null;
        }
        String modules
            = "HTTPClient.RetryModule|HTTPClient.CookieModule|HTTPClient.RedirectionModule|HTTPClient.AuthorizationModule|HTTPClient.DefaultModule|HTTPClient.TransferEncodingModule|HTTPClient.ContentMD5Module|HTTPClient.ContentEncodingModule";
        boolean in_applet = false;
        try {
            modules = System.getProperty("HTTPClient.Modules", modules);
        } catch (SecurityException se) {
            in_applet = true;
        }
        DefaultModuleList = new Vector();
        String[] list = Util.splitProperty(modules);
        for (int idx = 0; idx < list.length; idx++) {
            try {
                DefaultModuleList.addElement(Class.forName(list[idx]));
                if (GlobalConstants.DebugConn)
                    System.err.println("Conn:  added module " + list[idx]);
            } catch (ClassNotFoundException cnfe) {
                if (!in_applet)
                    throw new NoClassDefFoundError(cnfe.getMessage());
            }
        }
        try {
            NeverPipeline
                = Boolean.getBoolean("HTTPClient.disable_pipelining");
            if (GlobalConstants.DebugConn && NeverPipeline)
                System.err.println("Conn:  disabling pipelining");
        } catch (Exception exception) {
            /* empty */
        }
        try {
            NoKeepAlives = Boolean.getBoolean("HTTPClient.disableKeepAlives");
            if (GlobalConstants.DebugConn && NoKeepAlives)
                System.err.println("Conn:  disabling keep-alives");
        } catch (Exception exception) {
            /* empty */
        }
        try {
            force_1_0 = Boolean.getBoolean("HTTPClient.forceHTTP_1.0");
            if (GlobalConstants.DebugConn && force_1_0)
                System.err.println("Conn:  forcing HTTP/1.0 requests");
        } catch (Exception exception) {
            /* empty */
        }
        try {
            no_chunked = Boolean.getBoolean("HTTPClient.dontChunkRequests");
            if (GlobalConstants.DebugConn && no_chunked)
                System.err.println("Conn:  never chunking requests");
        } catch (Exception exception) {
            /* empty */
        }
        try {
            if (System.getProperty("os.name").indexOf("Windows") >= 0
                && System.getProperty("java.version").startsWith("1.1"))
                haveMSLargeWritesBug = true;
            if (GlobalConstants.DebugConn && haveMSLargeWritesBug)
                System.err.println
                    ("Conn:  splitting large writes into 20K chunks (M$ bug)");
        } catch (Exception exception) {
            /* empty */
        }
    }
}
