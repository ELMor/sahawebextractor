// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Cookie2.java

package HTTPClient;

import java.net.ProtocolException;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

// Referenced classes of package HTTPClient:
//            Cookie, ParseException, HttpHeaderElement, URI, 
//            Util, NVPair, RoRequest, HTTPConnection

public class Cookie2 extends Cookie
{

    protected int version;
    protected boolean discard;
    protected String comment;
    protected URI comment_url;
    protected int port_list[];
    protected String port_list_str;
    protected boolean path_set;
    protected boolean port_set;
    protected boolean domain_set;

    public Cookie2(String name, String value, String domain, int port_list[], String path, Date expires, boolean discard, 
            boolean secure, String comment, URI comment_url)
    {
        super(name, value, domain, path, expires, secure);
        this.discard = discard;
        this.port_list = port_list;
        this.comment = comment;
        this.comment_url = comment_url;
        path_set = true;
        domain_set = true;
        if(port_list != null && port_list.length > 0)
        {
            StringBuffer tmp = new StringBuffer();
            tmp.append(port_list[0]);
            for(int idx = 1; idx < port_list.length; idx++)
            {
                tmp.append(',');
                tmp.append(port_list[idx]);
            }

            port_list_str = tmp.toString();
            port_set = true;
        }
        version = 1;
    }

    protected Cookie2(RoRequest req)
    {
        super(req);
        int slash = super.path.lastIndexOf('/');
        if(slash != -1)
            super.path = super.path.substring(0, slash + 1);
        if(super.domain.indexOf('.') == -1)
            super.domain += ".local";
        version = -1;
        discard = false;
        comment = null;
        comment_url = null;
        port_list = null;
        port_list_str = null;
        path_set = false;
        port_set = false;
        domain_set = false;
    }

    protected static Cookie[] parse(String set_cookie, RoRequest req)
        throws ProtocolException
    {
        Vector cookies;
        try
        {
            cookies = Util.parseHeader(set_cookie);
        }
        catch(ParseException pe)
        {
            throw new ProtocolException(((Throwable) (pe)).getMessage());
        }
        Cookie cookie_arr[] = new Cookie[cookies.size()];
        int cidx = 0;
        for(int idx = 0; idx < cookie_arr.length; idx++)
        {
            HttpHeaderElement c_elem = (HttpHeaderElement)cookies.elementAt(idx);
            if(c_elem.getValue() == null)
                throw new ProtocolException("Bad Set-Cookie2 header: " + set_cookie + "\nMissing value " + "for cookie '" + c_elem.getName() + "'");
            Cookie2 curr = new Cookie2(req);
            curr.name = c_elem.getName();
            curr.value = c_elem.getValue();
            NVPair params[] = c_elem.getParams();
            boolean discard_set = false;
            boolean secure_set = false;
            for(int idx2 = 0; idx2 < params.length; idx2++)
            {
                String name = params[idx2].getName().toLowerCase();
                if((name.equals("version") || name.equals("max-age") || name.equals("domain") || name.equals("path") || name.equals("comment") || name.equals("commenturl")) && params[idx2].getValue() == null)
                    throw new ProtocolException("Bad Set-Cookie2 header: " + set_cookie + "\nMissing value " + "for " + params[idx2].getName() + " attribute in cookie '" + c_elem.getName() + "'");
                if(name.equals("version"))
                {
                    if(curr.version == -1)
                        try
                        {
                            curr.version = Integer.parseInt(params[idx2].getValue());
                        }
                        catch(NumberFormatException nfe)
                        {
                            throw new ProtocolException("Bad Set-Cookie2 header: " + set_cookie + "\nVersion '" + params[idx2].getValue() + "' not a number");
                        }
                } else
                if(name.equals("path"))
                {
                    if(!curr.path_set)
                    {
                        curr.path = params[idx2].getValue();
                        curr.path_set = true;
                    }
                } else
                if(name.equals("domain"))
                {
                    if(!curr.domain_set)
                    {
                        String d = params[idx2].getValue().toLowerCase();
                        if(d.charAt(0) != '.' && !d.equals(((Object) (((Cookie) (curr)).domain))))
                            curr.domain = "." + d;
                        else
                            curr.domain = d;
                        curr.domain_set = true;
                    }
                } else
                if(name.equals("max-age"))
                {
                    if(((Cookie) (curr)).expires == null)
                    {
                        int age;
                        try
                        {
                            age = Integer.parseInt(params[idx2].getValue());
                        }
                        catch(NumberFormatException nfe)
                        {
                            throw new ProtocolException("Bad Set-Cookie2 header: " + set_cookie + "\nMax-Age '" + params[idx2].getValue() + "' not a number");
                        }
                        curr.expires = new Date(System.currentTimeMillis() + (long)age * 1000L);
                    }
                } else
                if(name.equals("port"))
                {
                    if(!curr.port_set)
                        if(params[idx2].getValue() == null)
                        {
                            curr.port_list = new int[1];
                            curr.port_list[0] = req.getConnection().getPort();
                            curr.port_set = true;
                        } else
                        {
                            curr.port_list_str = params[idx2].getValue();
                            StringTokenizer tok = new StringTokenizer(params[idx2].getValue(), ",");
                            curr.port_list = new int[tok.countTokens()];
                            for(int idx3 = 0; idx3 < curr.port_list.length; idx3++)
                            {
                                String port = tok.nextToken().trim();
                                try
                                {
                                    curr.port_list[idx3] = Integer.parseInt(port);
                                }
                                catch(NumberFormatException nfe)
                                {
                                    throw new ProtocolException("Bad Set-Cookie2 header: " + set_cookie + "\nPort '" + port + "' not a number");
                                }
                            }

                            curr.port_set = true;
                        }
                } else
                if(name.equals("discard"))
                {
                    if(!discard_set)
                    {
                        curr.discard = true;
                        discard_set = true;
                    }
                } else
                if(name.equals("secure"))
                {
                    if(!secure_set)
                    {
                        curr.secure = true;
                        secure_set = true;
                    }
                } else
                if(name.equals("comment"))
                {
                    if(curr.comment == null)
                        curr.comment = params[idx2].getValue();
                } else
                if(name.equals("commenturl") && curr.comment_url == null)
                    try
                    {
                        curr.comment_url = new URI(params[idx2].getValue());
                    }
                    catch(ParseException pe)
                    {
                        throw new ProtocolException("Bad Set-Cookie2 header: " + set_cookie + "\nCommentURL '" + params[idx2].getValue() + "' not a valid URL");
                    }
            }

            if(curr.version == -1)
                throw new ProtocolException("Bad Set-Cookie2 header: " + set_cookie + "\nMissing Version " + "attribute");
            if(curr.version != 1)
                continue;
            if(((Cookie) (curr)).expires == null)
                curr.discard = true;
            if(!Util.getPath(req.getRequestURI()).startsWith(((Cookie) (curr)).path))
                continue;
            String eff_host = req.getConnection().getHost();
            if(eff_host.indexOf('.') == -1)
                eff_host = eff_host + ".local";
            if(!((Cookie) (curr)).domain.equals(".local") && ((Cookie) (curr)).domain.indexOf('.', 1) == -1 || !eff_host.endsWith(((Cookie) (curr)).domain) || eff_host.substring(0, eff_host.length() - ((Cookie) (curr)).domain.length()).indexOf('.') != -1)
                continue;
            if(curr.port_set)
            {
                int idx2 = 0;
                for(idx2 = 0; idx2 < curr.port_list.length; idx2++)
                    if(curr.port_list[idx2] == req.getConnection().getPort())
                        break;

                if(idx2 == curr.port_list.length)
                    continue;
            }
            cookie_arr[cidx++] = ((Cookie) (curr));
        }

        if(cidx < cookie_arr.length)
            cookie_arr = Util.resizeArray(cookie_arr, cidx);
        return cookie_arr;
    }

    public int getVersion()
    {
        return version;
    }

    public String getComment()
    {
        return comment;
    }

    public URI getCommentURL()
    {
        return comment_url;
    }

    public int[] getPorts()
    {
        return port_list;
    }

    public boolean discard()
    {
        return discard;
    }

    protected boolean sendWith(RoRequest req)
    {
        HTTPConnection con = req.getConnection();
        boolean port_match = !port_set;
        if(port_set)
        {
            for(int idx = 0; idx < port_list.length; idx++)
            {
                if(port_list[idx] != con.getPort())
                    continue;
                port_match = true;
                break;
            }

        }
        String eff_host = con.getHost();
        if(eff_host.indexOf('.') == -1)
            eff_host = eff_host + ".local";
        return (super.domain.charAt(0) == '.' && eff_host.endsWith(super.domain) || super.domain.charAt(0) != '.' && eff_host.equals(((Object) (super.domain)))) && port_match && Util.getPath(req.getRequestURI()).startsWith(super.path) && (!super.secure || con.getProtocol().equals("https") || con.getProtocol().equals("shttp"));
    }

    protected String toExternalForm()
    {
        StringBuffer cookie = new StringBuffer();
        if(version == 1)
        {
            cookie.append(super.name);
            cookie.append("=");
            cookie.append(super.value);
            if(path_set)
            {
                cookie.append("; ");
                cookie.append("$Path=");
                cookie.append(super.path);
            }
            if(domain_set)
            {
                cookie.append("; ");
                cookie.append("$Domain=");
                cookie.append(super.domain);
            }
            if(port_set)
            {
                cookie.append("; ");
                cookie.append("$Port");
                if(port_list_str != null)
                {
                    cookie.append("=\"");
                    cookie.append(port_list_str);
                    cookie.append('"');
                }
            }
        } else
        {
            throw new Error("Internal Error: unknown version " + version);
        }
        return cookie.toString();
    }

    public String toString()
    {
        String string = super.name + "=" + super.value;
        if(version == 1)
        {
            string = string + "; Version=" + version;
            string = string + "; Path=" + super.path;
            string = string + "; Domain=" + super.domain;
            if(port_set)
            {
                string = string + "; Port=\"" + port_list[0];
                for(int idx = 1; idx < port_list.length; idx++)
                    string = string + "," + port_list[idx];

                string = string + "\"";
            }
            if(super.expires != null)
                string = string + "; Max-Age=" + (super.expires.getTime() - (new Date()).getTime()) / 1000L;
            if(discard)
                string = string + "; Discard";
            if(super.secure)
                string = string + "; Secure";
            if(comment != null)
                string = string + "; Comment=\"" + comment + "\"";
            if(comment_url != null)
                string = string + "; CommentURL=\"" + comment_url + "\"";
        } else
        {
            throw new Error("Internal Error: unknown version " + version);
        }
        return string;
    }
}
