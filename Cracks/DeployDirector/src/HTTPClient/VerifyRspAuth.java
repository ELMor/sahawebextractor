// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DefaultAuthHandler.java

package HTTPClient;

import java.io.IOException;
import java.io.PrintStream;

// Referenced classes of package HTTPClient:
//            ParseException, MD5, HashVerifier, GlobalConstants, 
//            RoResponse, Util, HttpHeaderElement, DefaultAuthHandler

class VerifyRspAuth
    implements HashVerifier, GlobalConstants
{

    private String uri;
    private String HA1;
    private String alg;
    private String nonce;
    private String cnonce;
    private String nc;
    private String hdr;
    private RoResponse resp;

    public VerifyRspAuth(String uri, String HA1, String alg, String nonce, String cnonce, String nc, String hdr, 
            RoResponse resp)
    {
        this.uri = uri;
        this.HA1 = HA1;
        this.alg = alg;
        this.nonce = nonce;
        this.cnonce = cnonce;
        this.nc = nc;
        this.hdr = hdr;
        this.resp = resp;
    }

    public void verifyHash(byte hash[], long len)
        throws IOException
    {
        String auth_info = resp.getHeader(hdr);
        if(auth_info == null)
            auth_info = resp.getTrailer(hdr);
        if(auth_info == null)
            return;
        java.util.Vector pai;
        try
        {
            pai = Util.parseHeader(auth_info);
        }
        catch(ParseException pe)
        {
            throw new IOException(((Throwable) (pe)).toString());
        }
        HttpHeaderElement elem = Util.getElement(pai, "qop");
        String qop;
        if(elem == null || (qop = elem.getValue()) == null || !qop.equalsIgnoreCase("auth") && !qop.equalsIgnoreCase("auth-int"))
            return;
        elem = Util.getElement(pai, "rspauth");
        if(elem == null || elem.getValue() == null)
            return;
        byte digest[] = DefaultAuthHandler.unHex(elem.getValue());
        elem = Util.getElement(pai, "cnonce");
        if(elem != null && elem.getValue() != null && !elem.getValue().equals(((Object) (cnonce))))
            throw new IOException("Digest auth scheme: received wrong client-nonce '" + elem.getValue() + "' - expected '" + cnonce + "'");
        elem = Util.getElement(pai, "nc");
        if(elem != null && elem.getValue() != null && !elem.getValue().equals(((Object) (nc))))
            throw new IOException("Digest auth scheme: received wrong nonce-count '" + elem.getValue() + "' - expected '" + nc + "'");
        String A1;
        if(alg != null && alg.equalsIgnoreCase("MD5-sess"))
            A1 = (new MD5(((Object) (HA1 + ":" + nonce + ":" + cnonce)))).asHex();
        else
            A1 = HA1;
        String A2 = ":" + uri;
        if(qop.equalsIgnoreCase("auth-int"))
            A2 = A2 + ":" + MD5.asHex(hash);
        A2 = (new MD5(((Object) (A2)))).asHex();
        hash = (new MD5(((Object) (A1 + ":" + nonce + ":" + nc + ":" + cnonce + ":" + qop + ":" + A2)))).Final();
        for(int idx = 0; idx < hash.length; idx++)
            if(hash[idx] != digest[idx])
                throw new IOException("MD5-Digest mismatch: expected " + DefaultAuthHandler.hex(digest) + " but calculated " + DefaultAuthHandler.hex(hash));

        if(GlobalConstants.DebugAuth)
            System.err.println("Auth:  rspauth from " + hdr + " successfully verified");
    }
}
