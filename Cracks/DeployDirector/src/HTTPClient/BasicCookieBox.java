// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CookieModule.java

package HTTPClient;

import java.awt.Button;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;

// Referenced classes of package HTTPClient:
//            Separator, Cookie2, Cookie, DefaultCookiePolicyHandler

class BasicCookieBox extends Frame
{
    private class Close extends WindowAdapter
    {

        public void windowClosing(WindowEvent we)
        {
            (new Reject()).actionPerformed(((ActionEvent) (null)));
        }

        private Close()
        {
        }

    }

    private class RejectDomain
        implements ActionListener
    {

        public void actionPerformed(ActionEvent ae)
        {
            accept = false;
            accept_domain = true;
            synchronized(BasicCookieBox.this)
            {
                notifyAll();
            }
        }

        private RejectDomain()
        {
        }

    }

    private class AcceptDomain
        implements ActionListener
    {

        public void actionPerformed(ActionEvent ae)
        {
            accept = true;
            accept_domain = true;
            synchronized(BasicCookieBox.this)
            {
                notifyAll();
            }
        }

        private AcceptDomain()
        {
        }

    }

    private class Reject
        implements ActionListener
    {

        public void actionPerformed(ActionEvent ae)
        {
            accept = false;
            accept_domain = false;
            synchronized(BasicCookieBox.this)
            {
                notifyAll();
            }
        }

        private Reject()
        {
        }

    }

    private class Accept
        implements ActionListener
    {

        public void actionPerformed(ActionEvent ae)
        {
            accept = true;
            accept_domain = false;
            synchronized(BasicCookieBox.this)
            {
                notifyAll();
            }
        }

        private Accept()
        {
        }

    }


    private static final String title = "Set Cookie Request";
    private Dimension screen;
    private GridBagConstraints constr;
    private Label name_value_label;
    private Label domain_value;
    private Label ports_label;
    private Label ports_value;
    private Label path_value;
    private Label expires_value;
    private Label discard_note;
    private Label secure_note;
    private Label c_url_note;
    private Panel left_panel;
    private Panel right_panel;
    private Label comment_label;
    private TextArea comment_value;
    private TextField domain;
    private Button default_focus;
    private boolean accept;
    private boolean accept_domain;

    BasicCookieBox()
    {
        super("Set Cookie Request");
        screen = ((Window)this).getToolkit().getScreenSize();
        ((Frame)this).addNotify();
        ((Window)this).addWindowListener(((java.awt.event.WindowListener) (new Close())));
        GridBagLayout layout;
        ((Container)this).setLayout(((java.awt.LayoutManager) (layout = new GridBagLayout())));
        constr = new GridBagConstraints();
        constr.gridwidth = 0;
        constr.anchor = 17;
        ((Container)this).add(((Component) (new Label("The server would like to set the following cookie:"))), ((Object) (constr)));
        Panel p = new Panel();
        left_panel = new Panel();
        ((Container) (left_panel)).setLayout(((java.awt.LayoutManager) (new GridLayout(4, 1))));
        ((Container) (left_panel)).add(((Component) (new Label("Name=Value:"))));
        ((Container) (left_panel)).add(((Component) (new Label("Domain:"))));
        ((Container) (left_panel)).add(((Component) (new Label("Path:"))));
        ((Container) (left_panel)).add(((Component) (new Label("Expires:"))));
        ports_label = new Label("Ports:");
        ((Container) (p)).add(((Component) (left_panel)));
        right_panel = new Panel();
        ((Container) (right_panel)).setLayout(((java.awt.LayoutManager) (new GridLayout(4, 1))));
        ((Container) (right_panel)).add(((Component) (name_value_label = new Label())));
        ((Container) (right_panel)).add(((Component) (domain_value = new Label())));
        ((Container) (right_panel)).add(((Component) (path_value = new Label())));
        ((Container) (right_panel)).add(((Component) (expires_value = new Label())));
        ports_value = new Label();
        ((Container) (p)).add(((Component) (right_panel)));
        ((Container)this).add(((Component) (p)), ((Object) (constr)));
        secure_note = new Label("This cookie will only be sent over secure connections");
        discard_note = new Label("This cookie will be discarded at the end of the session");
        c_url_note = new Label("");
        comment_label = new Label("Comment:");
        comment_value = new TextArea("", 3, 45, 1);
        ((TextComponent) (comment_value)).setEditable(false);
        ((Container)this).add(((Component) (new Panel())), ((Object) (constr)));
        constr.gridwidth = 1;
        constr.anchor = 10;
        constr.weightx = 1.0D;
        ((Container)this).add(((Component) (default_focus = new Button("Accept"))), ((Object) (constr)));
        default_focus.addActionListener(((ActionListener) (new Accept())));
        constr.gridwidth = 0;
        Button b;
        ((Container)this).add(((Component) (b = new Button("Reject"))), ((Object) (constr)));
        b.addActionListener(((ActionListener) (new Reject())));
        constr.weightx = 0.0D;
        p = ((Panel) (new Separator()));
        constr.fill = 2;
        ((Container)this).add(((Component) (p)), ((Object) (constr)));
        constr.fill = 0;
        constr.anchor = 17;
        ((Container)this).add(((Component) (new Label("Accept/Reject all cookies from a host or domain:"))), ((Object) (constr)));
        p = new Panel();
        ((Container) (p)).add(((Component) (new Label("Host/Domain:"))));
        ((Container) (p)).add(((Component) (domain = new TextField(30))));
        ((Container)this).add(((Component) (p)), ((Object) (constr)));
        ((Container)this).add(((Component) (new Label("domains are characterized by a leading dot (`.');"))), ((Object) (constr)));
        ((Container)this).add(((Component) (new Label("an empty string matches all hosts"))), ((Object) (constr)));
        constr.anchor = 10;
        constr.gridwidth = 1;
        constr.weightx = 1.0D;
        ((Container)this).add(((Component) (b = new Button("Accept All"))), ((Object) (constr)));
        b.addActionListener(((ActionListener) (new AcceptDomain())));
        constr.gridwidth = 0;
        ((Container)this).add(((Component) (b = new Button("Reject All"))), ((Object) (constr)));
        b.addActionListener(((ActionListener) (new RejectDomain())));
        ((Window)this).pack();
        constr.anchor = 17;
        constr.gridwidth = 0;
    }

    public Dimension getMaximumSize()
    {
        return new Dimension((screen.width * 3) / 4, (screen.height * 3) / 4);
    }

    public synchronized boolean accept(Cookie cookie, DefaultCookiePolicyHandler h, String server)
    {
        name_value_label.setText(cookie.getName() + "=" + cookie.getValue());
        domain_value.setText(cookie.getDomain());
        path_value.setText(cookie.getPath());
        if(cookie.expires() == null)
            expires_value.setText("never");
        else
            expires_value.setText(cookie.expires().toString());
        int pos = 2;
        if(cookie.isSecure())
            ((Container)this).add(((Component) (secure_note)), ((Object) (constr)), pos++);
        if(cookie.discard())
            ((Container)this).add(((Component) (discard_note)), ((Object) (constr)), pos++);
        if(cookie instanceof Cookie2)
        {
            Cookie2 cookie2 = (Cookie2)cookie;
            if(cookie2.getPorts() != null)
            {
                ((GridLayout)((Container) (left_panel)).getLayout()).setRows(5);
                ((Container) (left_panel)).add(((Component) (ports_label)), 2);
                ((GridLayout)((Container) (right_panel)).getLayout()).setRows(5);
                int ports[] = cookie2.getPorts();
                StringBuffer plist = new StringBuffer();
                plist.append(ports[0]);
                for(int idx = 1; idx < ports.length; idx++)
                {
                    plist.append(", ");
                    plist.append(ports[idx]);
                }

                ports_value.setText(plist.toString());
                ((Container) (right_panel)).add(((Component) (ports_value)), 2);
            }
            if(cookie2.getCommentURL() != null)
            {
                c_url_note.setText("For more info on this cookie see: " + cookie2.getCommentURL());
                ((Container)this).add(((Component) (c_url_note)), ((Object) (constr)), pos++);
            }
            if(cookie2.getComment() != null)
            {
                ((TextComponent) (comment_value)).setText(cookie2.getComment());
                ((Container)this).add(((Component) (comment_label)), ((Object) (constr)), pos++);
                ((Container)this).add(((Component) (comment_value)), ((Object) (constr)), pos++);
            }
        }
        ((Component) (name_value_label)).invalidate();
        ((Component) (domain_value)).invalidate();
        ((Component) (ports_value)).invalidate();
        ((Component) (path_value)).invalidate();
        ((Component) (expires_value)).invalidate();
        ((Container) (left_panel)).invalidate();
        ((Container) (right_panel)).invalidate();
        ((Component) (secure_note)).invalidate();
        ((Component) (discard_note)).invalidate();
        ((Component) (c_url_note)).invalidate();
        ((Component) (comment_value)).invalidate();
        ((Container)this).invalidate();
        domain.setText(cookie.getDomain());
        ((Frame)this).setResizable(true);
        ((Window)this).pack();
        ((Frame)this).setResizable(false);
        ((Component)this).setLocation((screen.width - ((Container)this).getPreferredSize().width) / 2, (int)((double)((screen.height - ((Container)this).getPreferredSize().height) / 2) * 0.69999999999999996D));
        ((Component)this).setVisible(true);
        ((Component) (default_focus)).requestFocus();
        try
        {
            ((Object)this).wait();
        }
        catch(InterruptedException e) { }
        ((Component)this).setVisible(false);
        ((Container)this).remove(((Component) (secure_note)));
        ((Container)this).remove(((Component) (discard_note)));
        ((Container) (left_panel)).remove(((Component) (ports_label)));
        ((GridLayout)((Container) (left_panel)).getLayout()).setRows(4);
        ((Container) (right_panel)).remove(((Component) (ports_value)));
        ((GridLayout)((Container) (right_panel)).getLayout()).setRows(4);
        ((Container)this).remove(((Component) (c_url_note)));
        ((Container)this).remove(((Component) (comment_label)));
        ((Container)this).remove(((Component) (comment_value)));
        if(accept_domain)
        {
            String dom = ((TextComponent) (domain)).getText().trim().toLowerCase();
            if(accept)
                h.addAcceptDomain(dom);
            else
                h.addRejectDomain(dom);
        }
        return accept;
    }


}
