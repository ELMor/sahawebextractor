// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   NVPair.java

package HTTPClient;


public final class NVPair
{

    private String name;
    private String value;

    NVPair()
    {
        this("", "");
    }

    public NVPair(NVPair p)
    {
        this(p.name, p.value);
    }

    public NVPair(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    public final String getName()
    {
        return name;
    }

    public final String getValue()
    {
        return value;
    }

    public String toString()
    {
        return ((Object)this).getClass().getName() + "[name=" + name + ",value=" + value + "]";
    }
}
