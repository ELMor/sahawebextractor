// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   AuthorizationHandler.java

package HTTPClient;

import java.io.IOException;

// Referenced classes of package HTTPClient:
//            AuthSchemeNotImplException, AuthorizationInfo, RoRequest, RoResponse, 
//            Response

public interface AuthorizationHandler
{

    public abstract AuthorizationInfo getAuthorization(AuthorizationInfo authorizationinfo, RoRequest rorequest, RoResponse roresponse)
        throws AuthSchemeNotImplException;

    public abstract AuthorizationInfo fixupAuthInfo(AuthorizationInfo authorizationinfo, RoRequest rorequest, AuthorizationInfo authorizationinfo1, RoResponse roresponse)
        throws AuthSchemeNotImplException;

    public abstract void handleAuthHeaders(Response response, RoRequest rorequest, AuthorizationInfo authorizationinfo, AuthorizationInfo authorizationinfo1)
        throws IOException;

    public abstract void handleAuthTrailers(Response response, RoRequest rorequest, AuthorizationInfo authorizationinfo, AuthorizationInfo authorizationinfo1)
        throws IOException;
}
