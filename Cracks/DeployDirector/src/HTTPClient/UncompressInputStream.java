// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   UncompressInputStream.java

package HTTPClient;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

class UncompressInputStream extends FilterInputStream
{

    byte one[];
    private static final int TBL_CLEAR = 256;
    private static final int TBL_FIRST = 257;
    private int tab_prefix[];
    private byte tab_suffix[];
    private int zeros[];
    private byte stack[];
    private boolean block_mode;
    private int n_bits;
    private int maxbits;
    private int maxmaxcode;
    private int maxcode;
    private int bitmask;
    private int oldcode;
    private byte finchar;
    private int stackp;
    private int free_ent;
    private byte data[];
    private int bit_pos;
    private int end;
    private int got;
    private boolean eof;
    private static final int EXTRA = 64;
    private static final int LZW_MAGIC = 8093;
    private static final int MAX_BITS = 16;
    private static final int INIT_BITS = 9;
    private static final int HDR_MAXBITS = 31;
    private static final int HDR_EXTENDED = 32;
    private static final int HDR_FREE = 64;
    private static final int HDR_BLOCK_MODE = 128;
    private static final boolean debug = false;

    public UncompressInputStream(InputStream is)
        throws IOException
    {
        super(is);
        one = new byte[1];
        zeros = new int[256];
        data = new byte[10000];
        bit_pos = 0;
        end = 0;
        got = 0;
        eof = false;
        parse_header();
    }

    public synchronized int read()
        throws IOException
    {
        int b = super.in.read(one, 0, 1);
        if(b == 1)
            return one[0] & 0xff;
        else
            return -1;
    }

    public synchronized int read(byte buf[], int off, int len)
        throws IOException
    {
        if(eof)
            return -1;
        int start = off;
        int l_tab_prefix[] = tab_prefix;
        byte l_tab_suffix[] = tab_suffix;
        byte l_stack[] = stack;
        int l_n_bits = n_bits;
        int l_maxcode = maxcode;
        int l_maxmaxcode = maxmaxcode;
        int l_bitmask = bitmask;
        int l_oldcode = oldcode;
        byte l_finchar = finchar;
        int l_stackp = stackp;
        int l_free_ent = free_ent;
        byte l_data[] = data;
        int l_bit_pos = bit_pos;
        int s_size = l_stack.length - l_stackp;
        if(s_size > 0)
        {
            int num = s_size < len ? s_size : len;
            System.arraycopy(((Object) (l_stack)), l_stackp, ((Object) (buf)), off, num);
            off += num;
            len -= num;
            l_stackp += num;
        }
        if(len == 0)
        {
            stackp = l_stackp;
            return off - start;
        }
label0:
        do
        {
            if(end < 64)
                fill();
            int bit_in = got <= 0 ? (end << 3) - (l_n_bits - 1) : end - end % l_n_bits << 3;
            while(l_bit_pos < bit_in) 
            {
                if(l_free_ent > l_maxcode)
                {
                    int n_bytes = l_n_bits << 3;
                    l_bit_pos = ((l_bit_pos - 1) + n_bytes) - ((l_bit_pos - 1) + n_bytes) % n_bytes;
                    l_maxcode = ++l_n_bits != maxbits ? (1 << l_n_bits) - 1 : l_maxmaxcode;
                    l_bitmask = (1 << l_n_bits) - 1;
                    l_bit_pos = resetbuf(l_bit_pos);
                    continue label0;
                }
                int pos = l_bit_pos >> 3;
                int code = (l_data[pos] & 0xff | (l_data[pos + 1] & 0xff) << 8 | (l_data[pos + 2] & 0xff) << 16) >> (l_bit_pos & 7) & l_bitmask;
                l_bit_pos += l_n_bits;
                if(l_oldcode == -1)
                {
                    if(code >= 256)
                        throw new IOException("corrupt input: " + code + " > 255");
                    l_finchar = (byte)(l_oldcode = code);
                    buf[off++] = l_finchar;
                    len--;
                    continue;
                }
                if(code == 256 && block_mode)
                {
                    System.arraycopy(((Object) (zeros)), 0, ((Object) (l_tab_prefix)), 0, zeros.length);
                    l_free_ent = 256;
                    int n_bytes = l_n_bits << 3;
                    l_bit_pos = ((l_bit_pos - 1) + n_bytes) - ((l_bit_pos - 1) + n_bytes) % n_bytes;
                    l_n_bits = 9;
                    l_maxcode = (1 << l_n_bits) - 1;
                    l_bitmask = l_maxcode;
                    l_bit_pos = resetbuf(l_bit_pos);
                    continue label0;
                }
                int incode = code;
                l_stackp = l_stack.length;
                if(code >= l_free_ent)
                {
                    if(code > l_free_ent)
                        throw new IOException("corrupt input: code=" + code + ", free_ent=" + l_free_ent);
                    l_stack[--l_stackp] = l_finchar;
                    code = l_oldcode;
                }
                for(; code >= 256; code = l_tab_prefix[code])
                    l_stack[--l_stackp] = l_tab_suffix[code];

                l_finchar = l_tab_suffix[code];
                buf[off++] = l_finchar;
                len--;
                s_size = l_stack.length - l_stackp;
                int num = s_size < len ? s_size : len;
                System.arraycopy(((Object) (l_stack)), l_stackp, ((Object) (buf)), off, num);
                off += num;
                len -= num;
                l_stackp += num;
                if(l_free_ent < l_maxmaxcode)
                {
                    l_tab_prefix[l_free_ent] = l_oldcode;
                    l_tab_suffix[l_free_ent] = l_finchar;
                    l_free_ent++;
                }
                l_oldcode = incode;
                if(len == 0)
                {
                    n_bits = l_n_bits;
                    maxcode = l_maxcode;
                    bitmask = l_bitmask;
                    oldcode = l_oldcode;
                    finchar = l_finchar;
                    stackp = l_stackp;
                    free_ent = l_free_ent;
                    bit_pos = l_bit_pos;
                    return off - start;
                }
            }
            l_bit_pos = resetbuf(l_bit_pos);
        } while(got > 0);
        n_bits = l_n_bits;
        maxcode = l_maxcode;
        bitmask = l_bitmask;
        oldcode = l_oldcode;
        finchar = l_finchar;
        stackp = l_stackp;
        free_ent = l_free_ent;
        bit_pos = l_bit_pos;
        eof = true;
        return off - start;
    }

    private final int resetbuf(int bit_pos)
    {
        int pos = bit_pos >> 3;
        System.arraycopy(((Object) (data)), pos, ((Object) (data)), 0, end - pos);
        end -= pos;
        return 0;
    }

    private final void fill()
        throws IOException
    {
        got = super.in.read(data, end, data.length - 1 - end);
        if(got > 0)
            end += got;
    }

    public synchronized long skip(long num)
        throws IOException
    {
        byte tmp[] = new byte[(int)num];
        int got = read(tmp, 0, (int)num);
        if(got > 0)
            return (long)got;
        else
            return 0L;
    }

    public synchronized int available()
        throws IOException
    {
        if(eof)
            return 0;
        else
            return super.in.available();
    }

    private void parse_header()
        throws IOException
    {
        int t = super.in.read();
        if(t < 0)
            throw new EOFException("Failed to read magic number");
        int magic = (t & 0xff) << 8;
        t = super.in.read();
        if(t < 0)
            throw new EOFException("Failed to read magic number");
        magic += t & 0xff;
        if(magic != 8093)
            throw new IOException("Input not in compress format (read magic number 0x" + Integer.toHexString(magic) + ")");
        int header = super.in.read();
        if(header < 0)
            throw new EOFException("Failed to read header");
        block_mode = (header & 0x80) > 0;
        maxbits = header & 0x1f;
        if(maxbits > 16)
            throw new IOException("Stream compressed with " + maxbits + " bits, but can only handle " + 16 + " bits");
        if((header & 0x20) > 0)
            throw new IOException("Header extension bit set");
        if((header & 0x40) > 0)
            throw new IOException("Header bit 6 set");
        maxmaxcode = 1 << maxbits;
        n_bits = 9;
        maxcode = (1 << n_bits) - 1;
        bitmask = maxcode;
        oldcode = -1;
        finchar = 0;
        free_ent = block_mode ? 257 : 256;
        tab_prefix = new int[1 << maxbits];
        tab_suffix = new byte[1 << maxbits];
        stack = new byte[1 << maxbits];
        stackp = stack.length;
        for(int idx = 255; idx >= 0; idx--)
            tab_suffix[idx] = (byte)idx;

    }

    public static void main(String args[])
        throws Exception
    {
        if(args.length != 1)
        {
            System.err.println("Usage: UncompressInputStream <file>");
            System.exit(1);
        }
        InputStream in = ((InputStream) (new UncompressInputStream(((InputStream) (new FileInputStream(args[0]))))));
        byte buf[] = new byte[0x186a0];
        int tot = 0;
        long beg = System.currentTimeMillis();
        do
        {
            int got = in.read(buf);
            if(got >= 0)
            {
                System.out.write(buf, 0, got);
                tot += got;
            } else
            {
                long end = System.currentTimeMillis();
                System.err.println("Decompressed " + tot + " bytes");
                System.err.println("Time: " + (double)(end - beg) / 1000D + " seconds");
                return;
            }
        } while(true);
    }
}
