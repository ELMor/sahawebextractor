// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   URI.java

package HTTPClient;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.BitSet;

// Referenced classes of package HTTPClient:
//            ParseException

public class URI
{

    protected static BitSet alphanumChar;
    protected static BitSet markChar;
    protected static BitSet reservedChar;
    protected static BitSet unreservedChar;
    protected static BitSet uricChar;
    protected static BitSet pcharChar;
    protected static BitSet userinfoChar;
    protected static BitSet schemeChar;
    protected static BitSet reg_nameChar;
    protected boolean is_generic;
    protected String scheme;
    protected String opaque;
    protected String userinfo;
    protected String host;
    protected int port;
    protected String path;
    protected String query;
    protected String fragment;
    protected URL url;
    private static final char hex[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        'A', 'B', 'C', 'D', 'E', 'F'
    };

    public URI(String uri)
        throws ParseException
    {
        this((URI)null, uri);
    }

    public URI(URI base, String rel_uri)
        throws ParseException
    {
        port = -1;
        url = null;
        char uri[] = rel_uri.toCharArray();
        int pos = 0;
        int len;
        for(len = uri.length; pos < len && Character.isSpace(uri[pos]); pos++);
        for(; len > 0 && Character.isSpace(uri[len - 1]); len--);
        if(pos < len - 3 && uri[pos + 3] == ':' && (uri[pos + 0] == 'u' || uri[pos + 0] == 'U') && (uri[pos + 1] == 'r' || uri[pos + 1] == 'R') && (uri[pos + 2] == 'i' || uri[pos + 2] == 'I' || uri[pos + 2] == 'l' || uri[pos + 2] == 'L'))
            pos += 4;
        int idx;
        for(idx = pos; idx < len && uri[idx] != ':' && uri[idx] != '/' && uri[idx] != '?' && uri[idx] != '#'; idx++);
        if(idx < len && uri[idx] == ':')
        {
            scheme = rel_uri.substring(pos, idx).trim().toLowerCase();
            pos = idx + 1;
        }
        String final_scheme = scheme;
        if(scheme == null)
        {
            if(base == null)
                throw new ParseException("No scheme found");
            final_scheme = base.scheme;
        }
        is_generic = usesGenericSyntax(final_scheme);
        if(!is_generic)
            if(base != null && scheme == null)
            {
                throw new ParseException("Can't resolve relative URI for scheme " + final_scheme);
            } else
            {
                opaque = rel_uri.substring(pos);
                return;
            }
        if(pos < len - 1 && uri[pos] == '/' && uri[pos + 1] == '/')
        {
            for(idx = pos += 2; idx < len && uri[idx] != '/' && uri[idx] != '?' && uri[idx] != '#'; idx++);
            parse_authority(rel_uri.substring(pos, idx), final_scheme);
            pos = idx;
        }
        for(idx = pos; idx < len && uri[idx] != '?' && uri[idx] != '#'; idx++);
        path = rel_uri.substring(pos, idx);
        pos = idx;
        if(pos < len && uri[pos] == '?')
        {
            for(idx = ++pos; idx < len && uri[idx] != '#'; idx++);
            query = unescape(rel_uri.substring(pos, idx));
            pos = idx;
        }
        if(pos < len && uri[pos] == '#')
            fragment = unescape(rel_uri.substring(pos + 1, len));
        if(base != null)
        {
            if(scheme != null)
                return;
            scheme = base.scheme;
            if(host != null)
                return;
            userinfo = base.userinfo;
            host = base.host;
            port = base.port;
            if(path.length() == 0 && query == null)
            {
                path = base.path;
                query = base.query;
                return;
            }
            if(path.length() == 0 || path.charAt(0) != '/')
            {
                idx = base.path.lastIndexOf('/');
                if(idx == -1)
                    return;
                path = base.path.substring(0, idx + 1) + path;
                len = path.length();
                if((idx = path.indexOf("/.")) == -1 || idx != len - 2 && path.charAt(idx + 2) != '/' && (path.charAt(idx + 2) != '.' || idx != len - 3 && path.charAt(idx + 3) != '/'))
                    return;
                char p[] = new char[path.length()];
                path.getChars(0, p.length, p, 0);
                int beg = 0;
                for(idx = 1; idx < len; idx++)
                {
                    if(p[idx] != '.' || p[idx - 1] != '/')
                        continue;
                    int end;
                    if(idx == len - 1)
                    {
                        end = idx;
                        idx++;
                    } else
                    if(p[idx + 1] == '/')
                    {
                        end = idx - 1;
                        idx++;
                    } else
                    {
                        if(p[idx + 1] != '.' || idx != len - 2 && p[idx + 2] != '/')
                            continue;
                        if(idx < beg + 2)
                        {
                            beg = idx + 2;
                            continue;
                        }
                        for(end = idx - 2; end > beg && p[end] != '/'; end--);
                        if(p[end] != '/')
                            continue;
                        if(idx == len - 2)
                            end++;
                        idx += 2;
                    }
                    System.arraycopy(((Object) (p)), idx, ((Object) (p)), end, len - idx);
                    len -= idx - end;
                    idx = end;
                }

                path = new String(p, 0, len);
            }
        }
    }

    private void parse_authority(String authority, String scheme)
        throws ParseException
    {
        char uri[] = authority.toCharArray();
        int pos = 0;
        int len = uri.length;
        int idx;
        for(idx = pos; idx < len && uri[idx] != '@'; idx++);
        if(idx < len && uri[idx] == '@')
        {
            userinfo = unescape(authority.substring(pos, idx));
            pos = idx + 1;
        }
        for(idx = pos; idx < len && uri[idx] != ':'; idx++);
        host = authority.substring(pos, idx);
        pos = idx;
        if(pos < len - 1 && uri[pos] == ':')
        {
            int p;
            try
            {
                p = Integer.parseInt(authority.substring(pos + 1, len));
                if(p < 0)
                    throw new NumberFormatException();
            }
            catch(NumberFormatException e)
            {
                throw new ParseException(authority.substring(pos + 1, len) + " is an invalid port number");
            }
            if(p == defaultPort(scheme))
                port = -1;
            else
                port = p;
        }
    }

    public URI(URL url)
        throws ParseException
    {
        this((URI)null, url.toExternalForm());
    }

    public URI(String scheme, String host, String path)
        throws ParseException
    {
        this(scheme, ((String) (null)), host, -1, path, ((String) (null)), ((String) (null)));
    }

    public URI(String scheme, String host, int port, String path)
        throws ParseException
    {
        this(scheme, ((String) (null)), host, port, path, ((String) (null)), ((String) (null)));
    }

    public URI(String scheme, String userinfo, String host, int port, String path, String query, String fragment)
        throws ParseException
    {
        this.port = -1;
        url = null;
        if(scheme == null)
            throw new ParseException("missing scheme");
        this.scheme = scheme.trim().toLowerCase();
        if(userinfo != null)
            this.userinfo = unescape(userinfo.trim());
        if(host != null)
            this.host = host.trim();
        if(port != defaultPort(scheme))
            this.port = port;
        if(path != null)
            this.path = path.trim();
        if(query != null)
            this.query = query.trim();
        if(fragment != null)
            this.fragment = fragment.trim();
        is_generic = true;
    }

    public URI(String scheme, String opaque)
        throws ParseException
    {
        port = -1;
        url = null;
        if(scheme == null)
        {
            throw new ParseException("missing scheme");
        } else
        {
            this.scheme = scheme.trim().toLowerCase();
            this.opaque = opaque;
            is_generic = false;
            return;
        }
    }

    public static boolean usesGenericSyntax(String scheme)
    {
        scheme = scheme.trim();
        return scheme.equalsIgnoreCase("http") || scheme.equalsIgnoreCase("https") || scheme.equalsIgnoreCase("shttp") || scheme.equalsIgnoreCase("coffee") || scheme.equalsIgnoreCase("ftp") || scheme.equalsIgnoreCase("file") || scheme.equalsIgnoreCase("gopher") || scheme.equalsIgnoreCase("nntp") || scheme.equalsIgnoreCase("smtp") || scheme.equalsIgnoreCase("telnet") || scheme.equalsIgnoreCase("news") || scheme.equalsIgnoreCase("snews") || scheme.equalsIgnoreCase("hnews") || scheme.equalsIgnoreCase("rwhois") || scheme.equalsIgnoreCase("whois++") || scheme.equalsIgnoreCase("imap") || scheme.equalsIgnoreCase("pop") || scheme.equalsIgnoreCase("wais") || scheme.equalsIgnoreCase("irc") || scheme.equalsIgnoreCase("nfs") || scheme.equalsIgnoreCase("ldap") || scheme.equalsIgnoreCase("prospero") || scheme.equalsIgnoreCase("z39.50r") || scheme.equalsIgnoreCase("z39.50s") || scheme.equalsIgnoreCase("sip") || scheme.equalsIgnoreCase("sips") || scheme.equalsIgnoreCase("sipt") || scheme.equalsIgnoreCase("sipu") || scheme.equalsIgnoreCase("vemmi") || scheme.equalsIgnoreCase("videotex");
    }

    public static final int defaultPort(String protocol)
    {
        String prot = protocol.trim();
        if(prot.equalsIgnoreCase("http") || prot.equalsIgnoreCase("shttp") || prot.equalsIgnoreCase("http-ng") || prot.equalsIgnoreCase("coffee"))
            return 80;
        if(prot.equalsIgnoreCase("https"))
            return 443;
        if(prot.equalsIgnoreCase("ftp"))
            return 21;
        if(prot.equalsIgnoreCase("telnet"))
            return 23;
        if(prot.equalsIgnoreCase("nntp") || prot.equalsIgnoreCase("news"))
            return 119;
        if(prot.equalsIgnoreCase("snews"))
            return 563;
        if(prot.equalsIgnoreCase("hnews"))
            return 80;
        if(prot.equalsIgnoreCase("smtp"))
            return 25;
        if(prot.equalsIgnoreCase("gopher"))
            return 70;
        if(prot.equalsIgnoreCase("wais"))
            return 210;
        if(prot.equalsIgnoreCase("whois"))
            return 43;
        if(prot.equalsIgnoreCase("whois++"))
            return 63;
        if(prot.equalsIgnoreCase("rwhois"))
            return 4321;
        if(prot.equalsIgnoreCase("imap"))
            return 143;
        if(prot.equalsIgnoreCase("pop"))
            return 110;
        if(prot.equalsIgnoreCase("prospero"))
            return 1525;
        if(prot.equalsIgnoreCase("irc"))
            return 194;
        if(prot.equalsIgnoreCase("ldap"))
            return 389;
        if(prot.equalsIgnoreCase("nfs"))
            return 2049;
        if(prot.equalsIgnoreCase("z39.50r") || prot.equalsIgnoreCase("z39.50s"))
            return 210;
        if(prot.equalsIgnoreCase("vemmi"))
            return 575;
        return !prot.equalsIgnoreCase("videotex") ? 0 : 516;
    }

    public String getScheme()
    {
        return scheme;
    }

    public String getOpaque()
    {
        return opaque;
    }

    public String getHost()
    {
        return host;
    }

    public int getPort()
    {
        return port;
    }

    public String getUserinfo()
    {
        return userinfo;
    }

    public String getPath()
    {
        if(query != null)
        {
            if(path != null)
                return path + "?" + query;
            else
                return "?" + query;
        } else
        {
            return path;
        }
    }

    public String getQueryString()
    {
        return query;
    }

    public String getFragment()
    {
        return fragment;
    }

    public boolean isGenericURI()
    {
        return is_generic;
    }

    public URL toURL()
        throws MalformedURLException
    {
        if(url != null)
            return url;
        if(opaque != null)
            return url = new URL(scheme + ":" + opaque);
        String hostinfo;
        if(userinfo != null && host != null)
            hostinfo = userinfo + "@" + host;
        else
        if(userinfo != null)
            hostinfo = userinfo + "@";
        else
            hostinfo = host;
        StringBuffer file = new StringBuffer(100);
        if(path != null)
            file.append(escape(path.toCharArray(), uricChar));
        if(query != null)
        {
            file.append('?');
            file.append(escape(query.toCharArray(), uricChar));
        }
        if(fragment != null)
        {
            file.append('#');
            file.append(escape(fragment.toCharArray(), uricChar));
        }
        url = new URL(scheme, hostinfo, port, file.toString());
        return url;
    }

    public String toExternalForm()
    {
        StringBuffer uri = new StringBuffer(100);
        if(scheme != null)
        {
            uri.append(escape(scheme.toCharArray(), schemeChar));
            uri.append(':');
        }
        if(opaque != null)
        {
            uri.append(escape(opaque.toCharArray(), uricChar));
            return uri.toString();
        }
        if(userinfo != null || host != null || port != -1)
            uri.append("//");
        if(userinfo != null)
        {
            uri.append(escape(userinfo.toCharArray(), userinfoChar));
            uri.append('@');
        }
        if(host != null)
            uri.append(host.toCharArray());
        if(port != -1)
        {
            uri.append(':');
            uri.append(port);
        }
        if(path != null)
            uri.append(path.toCharArray());
        if(query != null)
        {
            uri.append('?');
            uri.append(escape(query.toCharArray(), uricChar));
        }
        if(fragment != null)
        {
            uri.append('#');
            uri.append(escape(fragment.toCharArray(), uricChar));
        }
        return uri.toString();
    }

    public String toString()
    {
        return toExternalForm();
    }

    public boolean equals(Object other)
    {
        if(other instanceof URI)
        {
            URI o = (URI)other;
            return scheme.equals(((Object) (o.scheme))) && (!is_generic && (opaque == null && o.opaque == null || opaque != null && o.opaque != null && opaque.equals(((Object) (o.opaque)))) || is_generic && (userinfo == null && o.userinfo == null || userinfo != null && o.userinfo != null && userinfo.equals(((Object) (o.userinfo)))) && (host == null && o.host == null || host != null && o.host != null && host.equalsIgnoreCase(o.host)) && port == o.port && (path == null && o.path == null || path != null && o.path != null && unescapeNoPE(path).equals(((Object) (unescapeNoPE(o.path))))) && (query == null && o.query == null || query != null && o.query != null && unescapeNoPE(query).equals(((Object) (unescapeNoPE(o.query))))) && (fragment == null && o.fragment == null || fragment != null && o.fragment != null && unescapeNoPE(fragment).equals(((Object) (unescapeNoPE(o.fragment))))));
        }
        if(other instanceof URL)
        {
            URL o = (URL)other;
            String h;
            if(userinfo != null)
                h = userinfo + "@" + host;
            else
                h = host;
            String f;
            if(query != null)
                f = path + "?" + query;
            else
                f = path;
            return scheme.equalsIgnoreCase(o.getProtocol()) && (!is_generic && opaque.equals(((Object) (o.getFile()))) || is_generic && (h == null && o.getHost() == null || h != null && o.getHost() != null && h.equalsIgnoreCase(o.getHost())) && (port == o.getPort() || o.getPort() == defaultPort(scheme)) && (f == null && o.getFile() == null || f != null && o.getFile() != null && unescapeNoPE(f).equals(((Object) (unescapeNoPE(o.getFile()))))) && (fragment == null && o.getRef() == null || fragment != null && o.getRef() != null && unescapeNoPE(fragment).equals(((Object) (unescapeNoPE(o.getRef()))))));
        } else
        {
            return false;
        }
    }

    private static char[] escape(char elem[], BitSet allowed_char)
    {
        int cnt = 0;
        for(int idx = 0; idx < elem.length; idx++)
            if(!allowed_char.get(((int) (elem[idx]))))
                cnt++;

        if(cnt == 0)
            return elem;
        char tmp[] = new char[elem.length + 2 * cnt];
        int idx = 0;
        for(int pos = 0; idx < elem.length; pos++)
        {
            if(allowed_char.get(((int) (elem[idx]))))
            {
                tmp[pos] = elem[idx];
            } else
            {
                if(elem[idx] > '\377')
                    throw new RuntimeException("Can't handle non 8-bt chars");
                tmp[pos++] = '%';
                tmp[pos++] = hex[elem[idx] >> 4 & 0xf];
                tmp[pos] = hex[elem[idx] & 0xf];
            }
            idx++;
        }

        return tmp;
    }

    static final String unescape(String str)
        throws ParseException
    {
        if(str == null || str.indexOf('%') == -1)
            return str;
        char buf[] = str.toCharArray();
        char res[] = new char[buf.length];
        int didx = 0;
        for(int sidx = 0; sidx < buf.length;)
        {
            if(buf[sidx] == '%')
            {
                int ch;
                try
                {
                    ch = Integer.parseInt(str.substring(sidx + 1, sidx + 3), 16);
                    if(ch < 0)
                        throw new NumberFormatException();
                }
                catch(NumberFormatException e)
                {
                    throw new ParseException(str.substring(sidx, sidx + 3) + " is an invalid code");
                }
                res[didx] = (char)ch;
                sidx += 2;
            } else
            {
                res[didx] = buf[sidx];
            }
            sidx++;
            didx++;
        }

        return new String(res, 0, didx);
    }

    private static final String unescapeNoPE(String str)
    {
        try
        {
            return unescape(str);
        }
        catch(ParseException pe)
        {
            return str;
        }
    }

    public static void main(String args[])
        throws Exception
    {
        System.err.println();
        System.err.println("*** URI Tests ...");
        URI base = new URI("http://a/b/c/d;p?q");
        testParser(base, "g:h", "g:h");
        testParser(base, "g", "http://a/b/c/g");
        testParser(base, "./g", "http://a/b/c/g");
        testParser(base, "g/", "http://a/b/c/g/");
        testParser(base, "/g", "http://a/g");
        testParser(base, "//g", "http://g");
        testParser(base, "?y", "http://a/b/c/?y");
        testParser(base, "g?y", "http://a/b/c/g?y");
        testParser(base, "#s", "http://a/b/c/d;p?q#s");
        testParser(base, "g#s", "http://a/b/c/g#s");
        testParser(base, "g?y#s", "http://a/b/c/g?y#s");
        testParser(base, ";x", "http://a/b/c/;x");
        testParser(base, "g;x", "http://a/b/c/g;x");
        testParser(base, "g;x?y#s", "http://a/b/c/g;x?y#s");
        testParser(base, ".", "http://a/b/c/");
        testParser(base, "./", "http://a/b/c/");
        testParser(base, "..", "http://a/b/");
        testParser(base, "../", "http://a/b/");
        testParser(base, "../g", "http://a/b/g");
        testParser(base, "../..", "http://a/");
        testParser(base, "../../", "http://a/");
        testParser(base, "../../g", "http://a/g");
        testParser(base, "", "http://a/b/c/d;p?q");
        testParser(base, "/./g", "http://a/./g");
        testParser(base, "/../g", "http://a/../g");
        testParser(base, "../../../g", "http://a/../g");
        testParser(base, "../../../../g", "http://a/../../g");
        testParser(base, "g.", "http://a/b/c/g.");
        testParser(base, ".g", "http://a/b/c/.g");
        testParser(base, "g..", "http://a/b/c/g..");
        testParser(base, "..g", "http://a/b/c/..g");
        testParser(base, "./../g", "http://a/b/g");
        testParser(base, "./g/.", "http://a/b/c/g/");
        testParser(base, "g/./h", "http://a/b/c/g/h");
        testParser(base, "g/../h", "http://a/b/c/h");
        testParser(base, "g;x=1/./y", "http://a/b/c/g;x=1/y");
        testParser(base, "g;x=1/../y", "http://a/b/c/y");
        testParser(base, "g?y/./x", "http://a/b/c/g?y/./x");
        testParser(base, "g?y/../x", "http://a/b/c/g?y/../x");
        testParser(base, "g#s/./x", "http://a/b/c/g#s/./x");
        testParser(base, "g#s/../x", "http://a/b/c/g#s/../x");
        testParser(base, "http:g", "http:g");
        testParser(base, "http:", "http:");
        testParser(base, "./g:h", "http://a/b/c/g:h");
        base = new URI("http://a/b/c/d;p?q=1/2");
        testParser(base, "g", "http://a/b/c/g");
        testParser(base, "./g", "http://a/b/c/g");
        testParser(base, "g/", "http://a/b/c/g/");
        testParser(base, "/g", "http://a/g");
        testParser(base, "//g", "http://g");
        testParser(base, "?y", "http://a/b/c/?y");
        testParser(base, "g?y", "http://a/b/c/g?y");
        testParser(base, "g?y/./x", "http://a/b/c/g?y/./x");
        testParser(base, "g?y/../x", "http://a/b/c/g?y/../x");
        testParser(base, "g#s", "http://a/b/c/g#s");
        testParser(base, "g#s/./x", "http://a/b/c/g#s/./x");
        testParser(base, "g#s/../x", "http://a/b/c/g#s/../x");
        testParser(base, "./", "http://a/b/c/");
        testParser(base, "../", "http://a/b/");
        testParser(base, "../g", "http://a/b/g");
        testParser(base, "../../", "http://a/");
        testParser(base, "../../g", "http://a/g");
        base = new URI("http://a/b/c/d;p=1/2?q");
        testParser(base, "g", "http://a/b/c/d;p=1/g");
        testParser(base, "./g", "http://a/b/c/d;p=1/g");
        testParser(base, "g/", "http://a/b/c/d;p=1/g/");
        testParser(base, "g?y", "http://a/b/c/d;p=1/g?y");
        testParser(base, ";x", "http://a/b/c/d;p=1/;x");
        testParser(base, "g;x", "http://a/b/c/d;p=1/g;x");
        testParser(base, "g;x=1/./y", "http://a/b/c/d;p=1/g;x=1/y");
        testParser(base, "g;x=1/../y", "http://a/b/c/d;p=1/y");
        testParser(base, "./", "http://a/b/c/d;p=1/");
        testParser(base, "../", "http://a/b/c/");
        testParser(base, "../g", "http://a/b/c/g");
        testParser(base, "../../", "http://a/b/");
        testParser(base, "../../g", "http://a/b/g");
        base = new URI("fred:///s//a/b/c");
        testParser(base, "g:h", "g:h");
        testPE(base, "g");
        base = new URI("http:///s//a/b/c");
        testParser(base, "g:h", "g:h");
        testParser(base, "g", "http:///s//a/b/g");
        testParser(base, "./g", "http:///s//a/b/g");
        testParser(base, "g/", "http:///s//a/b/g/");
        testParser(base, "/g", "http:///g");
        testParser(base, "//g", "http://g");
        testParser(base, "//g/x", "http://g/x");
        testParser(base, "///g", "http:///g");
        testParser(base, "./", "http:///s//a/b/");
        testParser(base, "../", "http:///s//a/");
        testParser(base, "../g", "http:///s//a/g");
        testParser(base, "../../", "http:///s//");
        testParser(base, "../../g", "http:///s//g");
        testParser(base, "../../../g", "http:///s/g");
        testParser(base, "../../../../g", "http:///g");
        testNotEqual("http://a/", "nntp://a/");
        testNotEqual("http://a/", "https://a/");
        testNotEqual("http://a/", "shttp://a/");
        testEqual("http://a/", "Http://a/");
        testEqual("http://a/", "hTTP://a/");
        testEqual("url:http://a/", "hTTP://a/");
        testEqual("urI:http://a/", "hTTP://a/");
        testEqual("http://a/", "Http://A/");
        testEqual("http://a.b.c/", "Http://A.b.C/");
        testEqual("http:///", "Http:///");
        testNotEqual("http:///", "Http://a/");
        testEqual("http://a.b.c/", "Http://A.b.C:80/");
        testEqual("http://a.b.c:/", "Http://A.b.C:80/");
        testEqual("nntp://a", "nntp://a:119");
        testEqual("nntp://a:", "nntp://a:119");
        testEqual("nntp://a/", "nntp://a:119/");
        testNotEqual("nntp://a", "nntp://a:118");
        testNotEqual("nntp://a", "nntp://a:0");
        testNotEqual("nntp://a:", "nntp://a:0");
        testEqual("telnet://:23/", "telnet:///");
        testPE(((URI) (null)), "ftp://:a/");
        testPE(((URI) (null)), "ftp://:-1/");
        testPE(((URI) (null)), "ftp://::1/");
        testNotEqual("ftp://me@a", "ftp://a");
        testNotEqual("ftp://me@a", "ftp://Me@a");
        testEqual("ftp://Me@a", "ftp://Me@a");
        testEqual("ftp://Me:My@a:21", "ftp://Me:My@a");
        testEqual("ftp://Me:My@a:", "ftp://Me:My@a");
        testNotEqual("ftp://Me:My@a:21", "ftp://Me:my@a");
        testNotEqual("ftp://Me:My@a:", "ftp://Me:my@a");
        testEqual("ftp://a/b%2b/", "ftp://a/b+/");
        testEqual("ftp://a/b%2b/", "ftp://a/b+/");
        testEqual("ftp://a/b%5E/", "ftp://a/b^/");
        testNotEqual("ftp://a/b%3f/", "ftp://a/b?/");
        System.err.println("*** Tests finished successfuly");
    }

    private static void testParser(URI base, String relURI, String result)
        throws Exception
    {
        if(!(new URI(base, relURI)).toString().equals(((Object) (result))))
        {
            String nl = System.getProperty("line.separator");
            throw new Exception("Test failed: " + nl + "  base-URI = <" + base + ">" + nl + "  rel-URI  = <" + relURI + ">" + nl + "  expected   <" + result + ">" + nl + "  but got    <" + new URI(base, relURI) + ">");
        } else
        {
            return;
        }
    }

    private static void testEqual(String one, String two)
        throws Exception
    {
        if(!(new URI(one)).equals(((Object) (new URI(two)))))
        {
            String nl = System.getProperty("line.separator");
            throw new Exception("Test failed: " + nl + "  <" + one + "> != <" + two + ">");
        } else
        {
            return;
        }
    }

    private static void testNotEqual(String one, String two)
        throws Exception
    {
        if((new URI(one)).equals(((Object) (new URI(two)))))
        {
            String nl = System.getProperty("line.separator");
            throw new Exception("Test failed: " + nl + "  <" + one + "> == <" + two + ">");
        } else
        {
            return;
        }
    }

    private static void testPE(URI base, String uri)
        throws Exception
    {
        boolean got_pe = false;
        try
        {
            new URI(base, uri);
        }
        catch(ParseException pe)
        {
            got_pe = true;
        }
        if(!got_pe)
        {
            String nl = System.getProperty("line.separator");
            throw new Exception("Test failed: " + nl + "  <" + uri + "> should be invalid");
        } else
        {
            return;
        }
    }

    static 
    {
        alphanumChar = new BitSet(128);
        for(int ch = 48; ch <= 57; ch++)
            alphanumChar.set(ch);

        for(int ch = 65; ch <= 90; ch++)
            alphanumChar.set(ch);

        for(int ch = 97; ch <= 122; ch++)
            alphanumChar.set(ch);

        markChar = new BitSet(128);
        markChar.set(45);
        markChar.set(95);
        markChar.set(46);
        markChar.set(33);
        markChar.set(126);
        markChar.set(42);
        markChar.set(39);
        markChar.set(40);
        markChar.set(41);
        reservedChar = new BitSet(128);
        reservedChar.set(59);
        reservedChar.set(47);
        reservedChar.set(63);
        reservedChar.set(58);
        reservedChar.set(64);
        reservedChar.set(38);
        reservedChar.set(61);
        reservedChar.set(43);
        reservedChar.set(36);
        reservedChar.set(44);
        unreservedChar = new BitSet(128);
        unreservedChar.or(alphanumChar);
        unreservedChar.or(markChar);
        uricChar = new BitSet(128);
        uricChar.or(unreservedChar);
        uricChar.or(reservedChar);
        pcharChar = new BitSet(128);
        pcharChar.or(unreservedChar);
        pcharChar.set(58);
        pcharChar.set(64);
        pcharChar.set(38);
        pcharChar.set(61);
        pcharChar.set(43);
        pcharChar.set(36);
        pcharChar.set(44);
        userinfoChar = new BitSet(128);
        userinfoChar.or(unreservedChar);
        userinfoChar.set(59);
        userinfoChar.set(58);
        userinfoChar.set(38);
        userinfoChar.set(61);
        userinfoChar.set(43);
        userinfoChar.set(36);
        userinfoChar.set(44);
        schemeChar = new BitSet(128);
        schemeChar.or(alphanumChar);
        schemeChar.set(43);
        schemeChar.set(45);
        schemeChar.set(46);
        reg_nameChar = new BitSet(128);
        reg_nameChar.or(unreservedChar);
        reg_nameChar.set(36);
        reg_nameChar.set(44);
        reg_nameChar.set(59);
        reg_nameChar.set(58);
        reg_nameChar.set(64);
        reg_nameChar.set(38);
        reg_nameChar.set(61);
        reg_nameChar.set(43);
    }
}
