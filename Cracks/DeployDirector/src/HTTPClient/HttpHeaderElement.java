// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   HttpHeaderElement.java

package HTTPClient;


// Referenced classes of package HTTPClient:
//            NVPair, Util

public class HttpHeaderElement
{

    private String name;
    private String value;
    private NVPair parameters[];

    public HttpHeaderElement(String name)
    {
        this.name = name;
        value = null;
        parameters = new NVPair[0];
    }

    public HttpHeaderElement(String name, String value, NVPair params[])
    {
        this.name = name;
        this.value = value;
        if(params != null)
        {
            parameters = new NVPair[params.length];
            System.arraycopy(((Object) (params)), 0, ((Object) (parameters)), 0, params.length);
        } else
        {
            parameters = new NVPair[0];
        }
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    public NVPair[] getParams()
    {
        return parameters;
    }

    public boolean equals(Object obj)
    {
        if(obj != null && (obj instanceof HttpHeaderElement))
        {
            String other = ((HttpHeaderElement)obj).name;
            return name.equalsIgnoreCase(other);
        } else
        {
            return false;
        }
    }

    public String toString()
    {
        StringBuffer buf = new StringBuffer();
        appendTo(buf);
        return buf.toString();
    }

    public void appendTo(StringBuffer buf)
    {
        buf.append(name);
        if(value != null)
            if(Util.needsQuoting(value))
            {
                buf.append("=\"");
                buf.append(Util.quoteString(value, "\\\""));
                buf.append('"');
            } else
            {
                buf.append('=');
                buf.append(value);
            }
        for(int idx = 0; idx < parameters.length; idx++)
        {
            buf.append(";");
            buf.append(parameters[idx].getName());
            String pval = parameters[idx].getValue();
            if(pval != null)
                if(Util.needsQuoting(pval))
                {
                    buf.append("=\"");
                    buf.append(Util.quoteString(pval, "\\\""));
                    buf.append('"');
                } else
                {
                    buf.append('=');
                    buf.append(pval);
                }
        }

    }
}
