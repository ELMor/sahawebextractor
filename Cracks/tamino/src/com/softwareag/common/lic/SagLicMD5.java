// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagLicMD5.java

package com.softwareag.common.lic;

import java.security.MessageDigest;

// Referenced classes of package com.softwareag.common.lic:
//            SagLicLog

public class SagLicMD5 extends MessageDigest
    implements Cloneable
{

    public static final String VERSION = Messages.getString("SagLicMD5.1.4.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagLicMD5.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$
    private int countMD5[];
    private int stateMD5[];
    private byte bufferMD5[];
    static final int S11 = 7;
    static final int S12 = 12;
    static final int S13 = 17;
    static final int S14 = 22;
    static final int S21 = 5;
    static final int S22 = 9;
    static final int S23 = 14;
    static final int S24 = 20;
    static final int S31 = 4;
    static final int S32 = 11;
    static final int S33 = 16;
    static final int S34 = 23;
    static final int S41 = 6;
    static final int S42 = 10;
    static final int S43 = 15;
    static final int S44 = 21;
    static final byte PADDING[] = {
        -128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0
    };

    public SagLicMD5()
    {
        super(Messages.getString("SagLicMD5.MD5_3")); //$NON-NLS-1$
        countMD5 = new int[2];
        stateMD5 = new int[4];
        bufferMD5 = new byte[64];
        engineReset();
    }

    static int F(int i, int j, int k)
    {
        return i & j | ~i & k;
    }

    static int G(int i, int j, int k)
    {
        return i & k | j & ~k;
    }

    static int H(int i, int j, int k)
    {
        return i ^ j ^ k;
    }

    static int I(int i, int j, int k)
    {
        return j ^ (i | ~k);
    }

    static int ROTATE_LEFT(int i, int j)
    {
        return i << j | i >>> 32 - j;
    }

    static int FF(int i, int j, int k, int l, int i1, int j1, int k1)
    {
        i += F(j, k, l) + i1 + k1;
        i = ROTATE_LEFT(i, j1);
        i += j;
        return i;
    }

    static int GG(int i, int j, int k, int l, int i1, int j1, int k1)
    {
        i += G(j, k, l) + i1 + k1;
        i = ROTATE_LEFT(i, j1);
        i += j;
        return i;
    }

    static int HH(int i, int j, int k, int l, int i1, int j1, int k1)
    {
        i += H(j, k, l) + i1 + k1;
        i = ROTATE_LEFT(i, j1);
        i += j;
        return i;
    }

    static int II(int i, int j, int k, int l, int i1, int j1, int k1)
    {
        i += I(j, k, l) + i1 + k1;
        i = ROTATE_LEFT(i, j1);
        i += j;
        return i;
    }

    static void MD5_memcpy(byte abyte0[], int i, byte abyte1[], int j, int k)
    {
        String s = Messages.getString("SagLicMD5.MD5_memcpy()_4"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        int l = j;
        for(int i1 = i; l < j + k; i1++)
        {
            abyte0[i1] = abyte1[l];
            l++;
        }

        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._5")); //$NON-NLS-1$
    }

    static void MD5_memset(byte abyte0[], int i, int j, int k)
    {
        String s = Messages.getString("SagLicMD5.MD5_memset()_6"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        for(int l = i; l < k; l++)
            abyte0[l] = (byte)j;

        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._7")); //$NON-NLS-1$
    }

    static void Encode(byte abyte0[], int ai[], int i)
    {
        String s = Messages.getString("SagLicMD5.Encode()_8"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___output____n_9")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, abyte0);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___input____n_10")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, ai);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___outputLength____n_11")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, i);
        if(i % 4 == 0)
        {
            int j = 0;
            for(int k = 0; k < i; k += 4)
            {
                abyte0[k] = (byte)(ai[j] & 0xff);
                abyte0[k + 1] = (byte)(ai[j] >> 8 & 0xff);
                abyte0[k + 2] = (byte)(ai[j] >> 16 & 0xff);
                abyte0[k + 3] = (byte)(ai[j] >> 24 & 0xff);
                j++;
            }

        }
        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._12")); //$NON-NLS-1$
    }

    static void Decode(int ai[], byte abyte0[], int i)
    {
        String s = Messages.getString("SagLicMD5.Decode()_13"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___output____n_14")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, ai);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___input____n_15")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, abyte0);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___inputLength____n_16")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, i);
        if(i % 4 == 0)
        {
            int j = 0;
            for(int k = 0; k < i; k += 4)
            {
                ai[j] = abyte0[k] & 0xff | (abyte0[k + 1] & 0xff) << 8 | (abyte0[k + 2] & 0xff) << 16 | (abyte0[k + 3] & 0xff) << 24;
                j++;
            }

        }
        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._17")); //$NON-NLS-1$
    }

    static void MD5Transform(int ai[], byte abyte0[])
    {
        String s = Messages.getString("SagLicMD5.MD5Transform()_18"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___state____n_19")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, ai);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___buffer____n_20")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, abyte0);
        int i = ai[0];
        int j = ai[1];
        int k = ai[2];
        int l = ai[3];
        int ai1[] = new int[abyte0.length / 4];
        Decode(ai1, abyte0, abyte0.length);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___x____n_21")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, ai1);
        i = FF(i, j, k, l, ai1[0], 7, 0xd76aa478);
        l = FF(l, i, j, k, ai1[1], 12, 0xe8c7b756);
        k = FF(k, l, i, j, ai1[2], 17, 0x242070db);
        j = FF(j, k, l, i, ai1[3], 22, 0xc1bdceee);
        i = FF(i, j, k, l, ai1[4], 7, 0xf57c0faf);
        l = FF(l, i, j, k, ai1[5], 12, 0x4787c62a);
        k = FF(k, l, i, j, ai1[6], 17, 0xa8304613);
        j = FF(j, k, l, i, ai1[7], 22, 0xfd469501);
        i = FF(i, j, k, l, ai1[8], 7, 0x698098d8);
        l = FF(l, i, j, k, ai1[9], 12, 0x8b44f7af);
        k = FF(k, l, i, j, ai1[10], 17, -42063);
        j = FF(j, k, l, i, ai1[11], 22, 0x895cd7be);
        i = FF(i, j, k, l, ai1[12], 7, 0x6b901122);
        l = FF(l, i, j, k, ai1[13], 12, 0xfd987193);
        k = FF(k, l, i, j, ai1[14], 17, 0xa679438e);
        j = FF(j, k, l, i, ai1[15], 22, 0x49b40821);
        i = GG(i, j, k, l, ai1[1], 5, 0xf61e2562);
        l = GG(l, i, j, k, ai1[6], 9, 0xc040b340);
        k = GG(k, l, i, j, ai1[11], 14, 0x265e5a51);
        j = GG(j, k, l, i, ai1[0], 20, 0xe9b6c7aa);
        i = GG(i, j, k, l, ai1[5], 5, 0xd62f105d);
        l = GG(l, i, j, k, ai1[10], 9, 0x2441453);
        k = GG(k, l, i, j, ai1[15], 14, 0xd8a1e681);
        j = GG(j, k, l, i, ai1[4], 20, 0xe7d3fbc8);
        i = GG(i, j, k, l, ai1[9], 5, 0x21e1cde6);
        l = GG(l, i, j, k, ai1[14], 9, 0xc33707d6);
        k = GG(k, l, i, j, ai1[3], 14, 0xf4d50d87);
        j = GG(j, k, l, i, ai1[8], 20, 0x455a14ed);
        i = GG(i, j, k, l, ai1[13], 5, 0xa9e3e905);
        l = GG(l, i, j, k, ai1[2], 9, 0xfcefa3f8);
        k = GG(k, l, i, j, ai1[7], 14, 0x676f02d9);
        j = GG(j, k, l, i, ai1[12], 20, 0x8d2a4c8a);
        i = HH(i, j, k, l, ai1[5], 4, 0xfffa3942);
        l = HH(l, i, j, k, ai1[8], 11, 0x8771f681);
        k = HH(k, l, i, j, ai1[11], 16, 0x6d9d6122);
        j = HH(j, k, l, i, ai1[14], 23, 0xfde5380c);
        i = HH(i, j, k, l, ai1[1], 4, 0xa4beea44);
        l = HH(l, i, j, k, ai1[4], 11, 0x4bdecfa9);
        k = HH(k, l, i, j, ai1[7], 16, 0xf6bb4b60);
        j = HH(j, k, l, i, ai1[10], 23, 0xbebfbc70);
        i = HH(i, j, k, l, ai1[13], 4, 0x289b7ec6);
        l = HH(l, i, j, k, ai1[0], 11, 0xeaa127fa);
        k = HH(k, l, i, j, ai1[3], 16, 0xd4ef3085);
        j = HH(j, k, l, i, ai1[6], 23, 0x4881d05);
        i = HH(i, j, k, l, ai1[9], 4, 0xd9d4d039);
        l = HH(l, i, j, k, ai1[12], 11, 0xe6db99e5);
        k = HH(k, l, i, j, ai1[15], 16, 0x1fa27cf8);
        j = HH(j, k, l, i, ai1[2], 23, 0xc4ac5665);
        i = II(i, j, k, l, ai1[0], 6, 0xf4292244);
        l = II(l, i, j, k, ai1[7], 10, 0x432aff97);
        k = II(k, l, i, j, ai1[14], 15, 0xab9423a7);
        j = II(j, k, l, i, ai1[5], 21, 0xfc93a039);
        i = II(i, j, k, l, ai1[12], 6, 0x655b59c3);
        l = II(l, i, j, k, ai1[3], 10, 0x8f0ccc92);
        k = II(k, l, i, j, ai1[10], 15, 0xffeff47d);
        j = II(j, k, l, i, ai1[1], 21, 0x85845dd1);
        i = II(i, j, k, l, ai1[8], 6, 0x6fa87e4f);
        l = II(l, i, j, k, ai1[15], 10, 0xfe2ce6e0);
        k = II(k, l, i, j, ai1[6], 15, 0xa3014314);
        j = II(j, k, l, i, ai1[13], 21, 0x4e0811a1);
        i = II(i, j, k, l, ai1[4], 6, 0xf7537e82);
        l = II(l, i, j, k, ai1[11], 10, 0xbd3af235);
        k = II(k, l, i, j, ai1[2], 15, 0x2ad7d2bb);
        j = II(j, k, l, i, ai1[9], 21, 0xeb86d391);
        ai[0] += i;
        ai[1] += j;
        ai[2] += k;
        ai[3] += l;
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___state____n_22")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, ai);
        for(int i1 = 0; i1 < 16; i1++)
            ai1[i1] = 0;

        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._23")); //$NON-NLS-1$
    }

    public void engineReset()
    {
        String s = Messages.getString("SagLicMD5.engineReset()_24"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        countMD5[0] = countMD5[1] = 0;
        bufferMD5[0] = bufferMD5[1] = bufferMD5[2] = bufferMD5[3] = 0;
        stateMD5[0] = 0x67452301;
        stateMD5[1] = 0xefcdab89;
        stateMD5[2] = 0x98badcfe;
        stateMD5[3] = 0x10325476;
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___stateMD5____n_25")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, stateMD5);
        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._26")); //$NON-NLS-1$
    }

    public void engineUpdate(byte byte0)
    {
        String s = Messages.getString("SagLicMD5.engineUpdate(byte)_27"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._28")); //$NON-NLS-1$
    }

    public void engineUpdate(byte abyte0[], int i, int j)
    {
        String s = Messages.getString("SagLicMD5.engineUpdate(byte[])_29"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        int l = countMD5[0] >>> 3 & 0x3f;
        if((countMD5[0] += j << 3) < j << 3)
            countMD5[1]++;
        countMD5[1] += j >>> 29;
        int i1 = 64 - l;
        int k;
        if(j >= i1)
        {
            MD5_memcpy(bufferMD5, l, abyte0, i, i1);
            MD5Transform(stateMD5, bufferMD5);
            for(k = i1; k + 63 < j; k += 64)
            {
                MD5_memcpy(bufferMD5, 0, abyte0, k, 64);
                MD5Transform(stateMD5, bufferMD5);
            }

            l = 0;
        } else
        {
            k = 0;
        }
        MD5_memcpy(bufferMD5, l, abyte0, i + k, j - k);
        SagLicLog.logExit(4100, s, Messages.getString("SagLicMD5._30")); //$NON-NLS-1$
    }

    public byte[] engineDigest()
    {
        String s = Messages.getString("SagLicMD5.engineDigest()_31"); //$NON-NLS-1$
        SagLicLog.logEntry(4100, s);
        byte abyte0[] = new byte[8];
        byte abyte1[] = new byte[16];
        Encode(abyte0, countMD5, abyte0.length);
        int i = countMD5[0] >>> 3 & 0x3f;
        int k = i >= 56 ? 120 - i : 56 - i;
        engineUpdate(PADDING, 0, k);
        engineUpdate(abyte0, 0, abyte0.length);
        Encode(abyte1, stateMD5, abyte1.length);
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___digest____n_32")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, abyte1);
        for(int j = 0; j < abyte1.length - 1; j++)
            abyte1[j + 1] ^= abyte1[j];

        abyte1[0] ^= abyte1[abyte1.length - 1];
        SagLicLog.logTrace(4104, s, Messages.getString("SagLicMD5.___digest____n_33")); //$NON-NLS-1$
        SagLicLog.logTrace(4104, s, abyte1);
        engineReset();
        SagLicLog.logExit(4100, s, abyte1);
        return abyte1;
    }

}
