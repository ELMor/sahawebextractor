// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagVersion.java

package com.softwareag.common.lic;


public final class SagVersion
{

    public static final String VERSION = Messages.getString("SagVersion.1.4.0.1_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagVersion.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$
    private static final String LICDATE = Messages.getString("SagVersion.10_December_2001_3"); //$NON-NLS-1$
    private static final String LICVERS = Messages.getString("SagVersion.1.4.0.1_4"); //$NON-NLS-1$
    private static final String LICFILE = Messages.getString("SagVersion._5"); //$NON-NLS-1$
    private static String internalVersion = null;
    private static String componentName = null;
    private static final String VERSION_COMPOSITE = Messages.getString("SagVersion.SagLic_Java_Package,_Version__1.4.0.1,_Date__10_December_2001_6"); //$NON-NLS-1$

    public SagVersion(String s)
    {
        componentName = s;
    }

    public static String getDate()
    {
        return Messages.getString("SagVersion.10_December_2001_7"); //$NON-NLS-1$
    }

    public static String getVersion()
    {
        return Messages.getString("SagVersion.1.4.0.1_8"); //$NON-NLS-1$
    }

    public static String getComponentName()
    {
        return componentName;
    }

    public static String getLicenseFileName()
    {
        return Messages.getString("SagVersion._9"); //$NON-NLS-1$
    }

    public static String getInternalVersion()
    {
        if(internalVersion == null)
        {
            StringBuffer stringbuffer = new StringBuffer();
            for(int i = 0; i < Messages.getString("SagVersion.1.4.0.1_10").length(); i++) //$NON-NLS-1$
                if(Character.isDigit(Messages.getString("SagVersion.1.4.0.1_11").charAt(i))) //$NON-NLS-1$
                    stringbuffer.append(Messages.getString("SagVersion.1.4.0.1_12").charAt(i)); //$NON-NLS-1$

            if(stringbuffer.length() == 4)
                stringbuffer.append('0');
            if(stringbuffer.length() != 5)
                internalVersion = Messages.getString("SagVersion.00000_13"); //$NON-NLS-1$
            else
                internalVersion = stringbuffer.toString();
        }
        return internalVersion;
    }

    public static String getVersionString()
    {
        return Messages.getString("SagVersion.SagLic__14") + componentName + Messages.getString("SagVersion._nVersion____15") + Messages.getString("SagVersion.1.4.0.1_16") + Messages.getString("SagVersion._nDate_______17") + Messages.getString("SagVersion.10_December_2001_18"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
    }

}
