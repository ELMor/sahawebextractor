// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagLicLog.java

package com.softwareag.common.lic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class SagLicLog
{

    public static final String VERSION = Messages.getString("SagLicLog.1.4.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagLicLog.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$
    public static final int LIC_LOG_ERR = 1;
    public static final int LIC_LOG_EXT = 2;
    public static final int LIC_LOG_INT = 4;
    public static final int LIC_LOG_DBG = 8;
    public static final int LIC_LOG_CHECK_OS = 16;
    public static final int LIC_LOG_CHECK_EXP = 32;
    public static final int LIC_LOG_READ_PARM = 64;
    public static final int LIC_LOG_CHECK_SIG = 128;
    public static final int LIC_LOG_GEN_SIG = 256;
    public static final int LIC_LOG_FREE_PARM = 512;
    public static final int LIC_LOG_SOCKETS = 1024;
    public static final int LIC_LOG_UTILS = 2048;
    public static final int LIC_LOG_MD5 = 4096;
    public static final int LIC_LOG_UNUSED = 8192;
    public static final int LIC_LOG_LOG = 16384;
    public static final int LIC_LOG_FILE = 32768;
    public static final int LIC_LOG_HIGH_LEVEL = 1008;
    public static final int LIC_LOG_LOW_LEVEL = 7168;
    public static final int LIC_LOG_ALL = 65535;
    public static final String traceEnvVar = Messages.getString("SagLicLog.LICLOG_3"); //$NON-NLS-1$
    private static final String traceTmpVar = Messages.getString("SagLicLog.java.io.tmpdir_4"); //$NON-NLS-1$
    private static final String traceFileName = Messages.getString("SagLicLog.LIC_trace_java.txt_5"); //$NON-NLS-1$
    private static int traceSet = 0;
    private static int traceModeSet = 0;
    private static int traceComponentSet = 0;
    private static final byte PADDING[] = {
        -128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
        0, 0, 0, 0
    };

    public SagLicLog()
    {
    }

    public static String getTraceFilePath()
    {
        String s = System.getProperty(Messages.getString("SagLicLog.java.io.tmpdir_6")) + File.separator + Messages.getString("SagLicLog.LIC_trace_java.txt_7"); //$NON-NLS-1$ //$NON-NLS-2$
        return s;
    }

    private static void setLogTraceModes(int i)
    {
        try
        {
            Integer integer = Integer.getInteger(Messages.getString("SagLicLog.LICLOG_8"), 0); //$NON-NLS-1$
            traceSet = integer.intValue();
            traceModeSet = traceSet & i;
            traceComponentSet = traceSet & ~i;
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__9") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__10") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

    public static void logEntry(int i, String s)
    {
        try
        {
            setLogTraceModes(6);
            if((traceSet & 0x4000) != 0)
                System.out.println(Messages.getString("SagLicLog.(traceLevel_&_traceModeSet)_________11") + (i & traceModeSet) + Messages.getString("SagLicLog._n(traceLevel_&_traceComponentSet)____12") + (i & traceComponentSet)); //$NON-NLS-1$ //$NON-NLS-2$
            if((i & traceModeSet) != 0 && (i & traceComponentSet) != 0)
            {
                String s1 = s + Messages.getString("SagLicLog._{_13"); //$NON-NLS-1$
                System.out.println(s1);
                if((traceSet & 0x8000) != 0)
                {
                    String s2 = getTraceFilePath();
                    PrintStream printstream = new PrintStream(new FileOutputStream(s2, true));
                    printstream.println(s1);
                    printstream.close();
                }
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicLog.File_Not_Found_Error_-__14") + filenotfoundexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicLog.IO_Error_-__15") + ioexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__16") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__17") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

    public static void logExit(int i, String s, Object obj)
    {
        try
        {
            setLogTraceModes(6);
            if((traceSet & 0x4000) != 0)
                System.out.println(Messages.getString("SagLicLog.(traceLevel_&_traceModeSet)_________18") + (i & traceModeSet) + Messages.getString("SagLicLog._n(traceLevel_&_traceComponentSet)____19") + (i & traceComponentSet)); //$NON-NLS-1$ //$NON-NLS-2$
            if((i & traceModeSet) != 0 && (i & traceComponentSet) != 0)
            {
                String s1 = s + Messages.getString("SagLicLog._}_returned____20") + obj; //$NON-NLS-1$
                System.out.println(s1);
                if((traceSet & 0x8000) != 0)
                {
                    String s2 = getTraceFilePath();
                    PrintStream printstream = new PrintStream(new FileOutputStream(s2, true));
                    printstream.println(s1);
                    printstream.close();
                }
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicLog.File_Not_Found_Error_-__21") + filenotfoundexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicLog.IO_Error_-__22") + ioexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__23") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__24") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

    public static void logTrace(int i, String s, String s1)
    {
        try
        {
            setLogTraceModes(15);
            if((traceSet & 0x4000) != 0)
            {
                System.out.println(Messages.getString("SagLicLog.traceSet____________________________25") + traceSet); //$NON-NLS-1$
                System.out.println(Messages.getString("SagLicLog.traceLevel__________________________26") + i); //$NON-NLS-1$
                System.out.println(Messages.getString("SagLicLog.(traceLevel_&_traceModeSet)_________27") + (i & traceModeSet) + Messages.getString("SagLicLog._n(traceLevel_&_traceComponentSet)____28") + (i & traceComponentSet)); //$NON-NLS-1$ //$NON-NLS-2$
            }
            if((i & traceModeSet) != 0 && (i & traceComponentSet) != 0 || i == 65535)
            {
                String s2;
                if(s.length() > 0)
                    s2 = s + Messages.getString("SagLicLog.____29") + s1; //$NON-NLS-1$
                else
                    s2 = Messages.getString("SagLicLog._30") + s1; //$NON-NLS-1$
                System.out.println(s2);
                if((traceSet & 0x8000) != 0)
                {
                    String s3 = getTraceFilePath();
                    PrintStream printstream = new PrintStream(new FileOutputStream(s3, true));
                    printstream.println(s2);
                    printstream.close();
                }
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicLog.File_Not_Found_Error_-__31") + filenotfoundexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicLog.IO_Error_-__32") + ioexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__33") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__34") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

    public static void logTrace(int i, String s, byte byte0)
    {
        try
        {
            setLogTraceModes(15);
            if((traceSet & 0x4000) != 0)
                System.out.println(Messages.getString("SagLicLog.(traceLevel_&_traceModeSet)_________35") + (i & traceModeSet) + Messages.getString("SagLicLog._n(traceLevel_&_traceComponentSet)____36") + (i & traceComponentSet)); //$NON-NLS-1$ //$NON-NLS-2$
            if((i & traceModeSet) != 0 && (i & traceComponentSet) != 0 || i == 65535)
            {
                String s1;
                if(s.length() > 0)
                    s1 = s + Messages.getString("SagLicLog.____37") + byte0; //$NON-NLS-1$
                else
                    s1 = Messages.getString("SagLicLog._38") + byte0; //$NON-NLS-1$
                System.out.println(s1);
                byte byte1 = 16;
                byte abyte0[] = new byte[1];
                abyte0[0] = byte0;
                String s2 = getTraceFilePath();
                PrintStream printstream = new PrintStream(new FileOutputStream(s2, true));
                printstream.write(s.getBytes(Messages.getString("SagLicLog.US-ASCII_39")), 0, s.length()); //$NON-NLS-1$
                printstream.write(0);
                printstream.write(abyte0.length);
                int j = (s.length() + 2) % byte1;
                int k = j <= 0 ? j : byte1 - j;
                printstream.write(PADDING, 1, k);
                printstream.write(abyte0, 0, abyte0.length);
                j = abyte0.length % byte1;
                k = j <= 0 ? j : byte1 - j;
                printstream.write(PADDING, 1, k);
                printstream.close();
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicLog.File_Not_Found_Error_-__40") + filenotfoundexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicLog.IO_Error_-__41") + ioexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__42") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__43") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

    public static void logTrace(int i, String s, byte abyte0[])
    {
        try
        {
            setLogTraceModes(15);
            if((traceSet & 0x4000) != 0)
                System.out.println(Messages.getString("SagLicLog.(traceLevel_&_traceModeSet)_________44") + (i & traceModeSet) + Messages.getString("SagLicLog._n(traceLevel_&_traceComponentSet)____45") + (i & traceComponentSet)); //$NON-NLS-1$ //$NON-NLS-2$
            if((i & traceModeSet) != 0 && (i & traceComponentSet) != 0 || i == 65535)
            {
                String s1;
                if(s.length() > 0)
                    s1 = s + Messages.getString("SagLicLog.____46") + abyte0; //$NON-NLS-1$
                else
                    s1 = Messages.getString("SagLicLog._47") + abyte0; //$NON-NLS-1$
                System.out.println(s1);
                byte byte0 = 16;
                String s2 = getTraceFilePath();
                PrintStream printstream = new PrintStream(new FileOutputStream(s2, true));
                printstream.write(s.getBytes(Messages.getString("SagLicLog.US-ASCII_48")), 0, s.length()); //$NON-NLS-1$
                printstream.write(0);
                printstream.write(abyte0.length);
                int j = (s.length() + 2) % byte0;
                int k = j <= 0 ? j : byte0 - j;
                printstream.write(PADDING, 1, k);
                printstream.write(abyte0, 0, abyte0.length);
                j = abyte0.length % byte0;
                k = j <= 0 ? j : byte0 - j;
                printstream.write(PADDING, 1, k);
                printstream.close();
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicLog.File_Not_Found_Error_-__49") + filenotfoundexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicLog.IO_Error_-__50") + ioexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__51") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__52") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

    public static void logTrace(int i, String s, int j)
    {
        try
        {
            setLogTraceModes(15);
            if((traceSet & 0x4000) != 0)
                System.out.println(Messages.getString("SagLicLog.(traceLevel_&_traceModeSet)_________53") + (i & traceModeSet) + Messages.getString("SagLicLog._n(traceLevel_&_traceComponentSet)____54") + (i & traceComponentSet)); //$NON-NLS-1$ //$NON-NLS-2$
            if((i & traceModeSet) != 0 && (i & traceComponentSet) != 0 || i == 65535)
            {
                String s1;
                if(s.length() > 0)
                    s1 = s + Messages.getString("SagLicLog.____55") + j; //$NON-NLS-1$
                else
                    s1 = Messages.getString("SagLicLog._56") + j; //$NON-NLS-1$
                System.out.println(s1);
                byte byte0 = 16;
                String s2 = getTraceFilePath();
                PrintStream printstream = new PrintStream(new FileOutputStream(s2, true));
                printstream.write(s.getBytes(Messages.getString("SagLicLog.US-ASCII_57")), 0, s.length()); //$NON-NLS-1$
                printstream.write(0);
                printstream.write(1);
                int k = (s.length() + 2) % byte0;
                int l = k <= 0 ? k : byte0 - k;
                printstream.write(PADDING, 1, l);
                byte abyte0[] = new byte[4];
                abyte0[0] = (byte)(j >>> 24 & 0xff);
                abyte0[1] = (byte)(j >>> 16 & 0xff);
                abyte0[2] = (byte)(j >>> 8 & 0xff);
                abyte0[3] = (byte)(j & 0xff);
                printstream.write(abyte0, 0, abyte0.length);
                k = abyte0.length % byte0;
                l = k <= 0 ? k : byte0 - k;
                printstream.write(PADDING, 1, l);
                printstream.close();
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicLog.File_Not_Found_Error_-__58") + filenotfoundexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicLog.IO_Error_-__59") + ioexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__60") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__61") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

    public static void logTrace(int i, String s, int ai[])
    {
        try
        {
            setLogTraceModes(15);
            if((traceSet & 0x4000) != 0)
                System.out.println(Messages.getString("SagLicLog.(traceLevel_&_traceModeSet)_________62") + (i & traceModeSet) + Messages.getString("SagLicLog._n(traceLevel_&_traceComponentSet)____63") + (i & traceComponentSet)); //$NON-NLS-1$ //$NON-NLS-2$
            if((i & traceModeSet) != 0 && (i & traceComponentSet) != 0 || i == 65535)
            {
                String s1;
                if(s.length() > 0)
                    s1 = s + Messages.getString("SagLicLog.____64") + ai; //$NON-NLS-1$
                else
                    s1 = Messages.getString("SagLicLog._65") + ai; //$NON-NLS-1$
                System.out.println(s1);
                byte byte0 = 16;
                String s2 = getTraceFilePath();
                PrintStream printstream = new PrintStream(new FileOutputStream(s2, true));
                printstream.write(s.getBytes(Messages.getString("SagLicLog.US-ASCII_66")), 0, s.length()); //$NON-NLS-1$
                printstream.write(0);
                printstream.write(ai.length);
                int j = (s.length() + 2) % byte0;
                int k = j <= 0 ? j : byte0 - j;
                printstream.write(PADDING, 1, k);
                byte abyte0[] = new byte[4 * ai.length];
                for(int l = 0; l < ai.length; l++)
                {
                    abyte0[l * 4] = (byte)(ai[l] >>> 24 & 0xff);
                    abyte0[l * 4 + 1] = (byte)(ai[l] >>> 16 & 0xff);
                    abyte0[l * 4 + 2] = (byte)(ai[l] >>> 8 & 0xff);
                    abyte0[l * 4 + 3] = (byte)(ai[l] & 0xff);
                }

                printstream.write(abyte0, 0, abyte0.length);
                j = abyte0.length % byte0;
                k = j <= 0 ? j : byte0 - j;
                printstream.write(PADDING, 1, k);
                printstream.close();
            }
        }
        catch(FileNotFoundException filenotfoundexception)
        {
            System.out.println(Messages.getString("SagLicLog.File_Not_Found_Error_-__67") + filenotfoundexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(IOException ioexception)
        {
            System.out.println(Messages.getString("SagLicLog.IO_Error_-__68") + ioexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(SecurityException securityexception)
        {
            System.out.println(Messages.getString("SagLicLog.Security_Error_-__69") + securityexception.getLocalizedMessage()); //$NON-NLS-1$
        }
        catch(Exception exception)
        {
            System.out.println(Messages.getString("SagLicLog.Undefined_Error_-__70") + exception.getLocalizedMessage()); //$NON-NLS-1$
        }
    }

}
