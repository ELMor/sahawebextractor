// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   SagLic.java

package com.softwareag.common.lic;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.TimeZone;

import com.softwareag.common.lic.utilities.SagUtilitiesDates;
import com.softwareag.common.lic.utilities.SagUtilitiesStrings;

// Referenced classes of package com.softwareag.common.lic:
//            SagLicProvider, SagLicException, SagLicLog

public class SagLic {

  public static final String VERSION = Messages.getString("SagLic.1.4.0.1_1"); //$NON-NLS-1$
  public static final String COPYRIGHT =
      Messages.getString("SagLic.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$
  private static boolean traceTypeSet = false;
  private static final String LIC_CLASSES[] = {
      Messages.getString("SagLic.SagLic_3"), Messages.getString("SagLic.SagLicException_4"), Messages.getString("SagLic.SagLicLog_5"), Messages.getString("SagLic.SagLicMD5_6"), Messages.getString("SagLic.SagLicProvider_7"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
      Messages.getString("SagLic.SagVersion_8"), Messages.getString("SagLic.utilities.SagUtilitiesDates_9"), //$NON-NLS-1$ //$NON-NLS-2$
      Messages.getString("SagLic.utilities.SagUtilitiesStrings_10") //$NON-NLS-1$
  };
  public static final int LIC_OK = 0;
  public static final int LIC_FILENOTFOUND = 1;
  public static final int LIC_NOKEY = 2;
  public static final int LIC_KEYINVALID = 3;
  public static final int LIC_PARAMNOTFOUND = 4;
  public static final int LIC_COMPONENTNOTFOUND = 5;
  public static final int LIC_MISSINGPARAMS = 6;
  public static final int LIC_NOSOCKETFOUND = 7;
  public static final int LIC_SOCKETWRONGVER = 8;
  public static final int LIC_SOCKETERROR = 9;
  public static final int LIC_HOSTNOTFOUND = 10;
  public static final int LIC_ERRORHOSTOPEN = 11;
  public static final int LIC_HTTPERROR = 12;
  public static final int LIC_EXTERNALBUILD = 13;
  public static final int LIC_ALLOCERROR = 14;
  public static final int LIC_EXPIRED = 15;
  public static final int LIC_INVALIDOS = 16;
  public static final int LIC_REGCERROR = 17;
  public static final int LIC_EXTENDED = 18;
  public static final int LIC_FILEREADERROR = 19;
  public static final int LIC_90DAYSCHECKNOTVALID = 20;
  public static final int LIC_CONVERSIONERROR = 21;
  public static final int LIC_SYSTEMERROR = 22;
  public static final int LIC_SECURITYERROR = 23;
  public static final int LIC_FILEWRITEERROR = 24;
  private static final String LIC_MESSAGES[] = {
      Messages.getString("SagLic.LICENSE_OK_11"), Messages.getString("SagLic.ERROR_-_LICENSE_KEY_FILE_NOT_FOUND_12"), //$NON-NLS-1$ //$NON-NLS-2$
      Messages.getString("SagLic.ERROR_-_INVALID_(TAMPERED)_LICENSE_KEY_13"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_INVALID_(TAMPERED)_LICENSE_KEY_14"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_PARAMETER_NOT_FOUND_IN_LICENSE_KEY_15"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_COMPONENT_NOT_FOUND_IN_LICENSE_KEY_16"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_MISSING_PARAMETER(S)_-_SEE_FUNCTION_USAGE_17"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_MISSING_WINSOCK_DLL_ON_WIN32_18"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_WRONG_WINSOCK_DLL_(BELOW_1.0___)_ON_WIN32_19"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_SOCKET_CANNOT_BE_OPENED_20"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_HOST_CANNOT_BE_FOUND_IN_HTTP_ADDRESS_21"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_HOST_CANNOT_BE_OPENED_IN_HTTP_ADDRESS_22"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_HOST_CANNOT_BE_OPENED_IN_HTTP_ADDRESS_23"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_CANNOT_CREATE_LICENSE_USING_EXTERNAL_LIC_VERSION_24"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_CANNOT_ALLOCATE_MEMORY_25"), Messages.getString("SagLic.ERROR_-_LICENSE_EXPIRED_26"), //$NON-NLS-1$ //$NON-NLS-2$
      Messages.getString("SagLic.ERROR_-_INVALID_LICENSE_OS_27"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_CANNOT_CREATE_FIRST_WINDOWS_REGISTRY_KEY___software_ag___28"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_CANNOT_ACCESS_WINDOWS_REGISTRY_29"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_READING_LICENSE_KEY_FILE_30"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_TRIAL_LICENSE_KEY_FILES_must_NOT_be_WRITE-PROTECTED_n________and_MUST_be_passed_as_a_PATHNAME_(not_CONTENTS)._31"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_CONVERTING_ASCII-TO-EBCDIC_(OR_VICE-VERSA)_32"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_SYSTEM_COMMAND_(I.E.,_UNAME)_33"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_CANNOT_READ_LOCAL_SAG_SECURITY_PROFILE_34"), //$NON-NLS-1$
      Messages.getString("SagLic.ERROR_-_WRITING_to_LICENSE_KEY_FILE_35") //$NON-NLS-1$
  };
  private static final String resourceBundleClass =
      Messages.getString("SagLic.com.softwareag.common.messages.SagLicErrors_36"); //$NON-NLS-1$
  private static final String algorithmName = Messages.getString("SagLic.MD5_37"); //$NON-NLS-1$
  private static final String encodingName = Messages.getString("SagLic.US-ASCII_38"); //$NON-NLS-1$
  private static String saglicScope;
  private static final String tagLicenseKey = Messages.getString("SagLic.<LicenseKey>_39"); //$NON-NLS-1$
  static final byte sHex[] = {
      48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
      65, 66, 67, 68, 69, 70, 0
  };

  public SagLic() {
    String s = Messages.getString("SagLic.SagLic()_40"); //$NON-NLS-1$
    SagLicLog.logEntry(14, s);
    saglicScope = Messages.getString("SagLic.PUBLIC_41"); //$NON-NLS-1$
    SagLicLog.logTrace(14, s, Messages.getString("SagLic.Add_SagLicProvider()_at_1_42")); //$NON-NLS-1$
    Security.insertProviderAt(new SagLicProvider(), 1);
    Integer integer = Integer.getInteger(Messages.getString("SagLic.LICLOG_43"), 0); //$NON-NLS-1$
    int i = integer.intValue();
    if (i != 0) {/*
      for (int j = 0; j < LIC_CLASSES.length; j++) {
        try {
          Class class1 = Class.forName("com.softwareag.common.lic." +
                                       LIC_CLASSES[j]);
          System.out.println("version = " +
                             (String) class1.getField("VERSION").get(class1) +
                             "  " + class1.getName());

        }
        catch (NoClassDefFoundError noclassdeffounderror) {
          System.out.println("ERROR - " + LIC_CLASSES[j] + " NOT FOUND");
        }
        catch (ClassNotFoundException classnotfoundexception) {
          System.out.println("ERROR - " + LIC_CLASSES[j] + " NOT FOUND");
        }
        catch (UnsatisfiedLinkError unsatisfiedlinkerror) {}
        catch (NoSuchFieldException nosuchfieldexception) {
          System.out.println("ERROR - " + LIC_CLASSES[j] +
                             " VERSION field does not exist");
        }
        catch (IllegalAccessException illegalaccessexception) {
          System.out.println("ERROR - " + LIC_CLASSES[j] +
                             " Illegal Access to VERSION field");
        }
      }

    */}
    if ( (i & 0x1c00) != 0) {
      try {
        java.security.Provider aprovider[] = Security.getProviders();
        System.out.println(Messages.getString("SagLic._nSecurity_Providers__n_44")); //$NON-NLS-1$
        for (int k = 0; k < aprovider.length; k++) {
          System.out.println(aprovider[k]);
          for (Enumeration enumeration = aprovider[k].keys();
               enumeration.hasMoreElements();
               System.out.println(Messages.getString("SagLic._t_45") + enumeration.nextElement())) { //$NON-NLS-1$
            ;
          }
        }

        System.out.println(Messages.getString("SagLic._nSystem_Properties__n_46")); //$NON-NLS-1$
        Properties properties = System.getProperties();
        String s1;
        for (Enumeration enumeration1 = System.getProperties().propertyNames();
             enumeration1.hasMoreElements();
             System.out.println(s1 + Messages.getString("SagLic.____47") + //$NON-NLS-1$
                                System.getProperty(s1, Messages.getString("SagLic.UNASSIGNED_48")))) { //$NON-NLS-1$
          s1 = enumeration1.nextElement().toString();

        }
      }
      catch (SecurityException securityexception) {
        System.out.println(Messages.getString("SagLic._nPUBLIC___49") + securityexception.getMessage()); //$NON-NLS-1$
      }
      catch (Exception exception) {
        System.out.println(Messages.getString("SagLic._nPUBLIC___50") + exception.getMessage()); //$NON-NLS-1$
      }
    }
  }

  public static void printLICStatus(int i) {
    String s = Messages.getString("SagLic.printLICStatus()_51"); //$NON-NLS-1$
    SagLicLog.logEntry(16386, s);
    SagLicLog.logTrace(16386, s, Messages.getString("SagLic.saglicStatusCode____52") + i); //$NON-NLS-1$
    String s1 = Messages.getString("SagLic._53"); //$NON-NLS-1$
    try {
      ResourceBundle resourcebundle = ResourceBundle.getBundle(
          Messages.getString("SagLic.com.softwareag.common.messages.SagLicErrors_54")); //$NON-NLS-1$
      s1 = resourcebundle.getString(Integer.toString(i));
    }
    catch (MissingResourceException missingresourceexception) {
      SagLicLog.logExit(16385, s,
                        Messages.getString("SagLic.ERROR__n____55") + missingresourceexception.getMessage()); //$NON-NLS-1$
      if (i >= 0 && i < LIC_MESSAGES.length) {
        s1 = LIC_MESSAGES[i];
      }
      else {
        s1 = Messages.getString("SagLic.ERROR___LIC_ERROR_CODE_RETURNED____56") + i; //$NON-NLS-1$
      }
    }
    s1 = Messages.getString("SagLic._n____57") + i + Messages.getString("SagLic.____58") + s1; //$NON-NLS-1$ //$NON-NLS-2$
    System.out.println(s1);
    SagLicLog.logExit(16386, s, s1);
  }

  public int convertTraceLevel(String s) {
    String s1 = Messages.getString("SagLic.convertTraceLevel()_59"); //$NON-NLS-1$
    SagLicLog.logEntry(16386, s1);
    SagLicLog.logTrace(16386, s1, Messages.getString("SagLic.Trace_Definitions____60") + s); //$NON-NLS-1$
    int i = 0;
    boolean flag = false;
    int k = 0;
    do {
      int j = s.indexOf(Messages.getString("SagLic.,_61"), i); //$NON-NLS-1$
      String s2;
      if (j > i) {
        s2 = s.substring(i, j);
      }
      else {
        s2 = s.substring(i);
        j = s.length();
      }
      SagLicLog.logTrace(16386, s1, Messages.getString("SagLic.traceDef____62") + s2); //$NON-NLS-1$
      if (s2.equals(Messages.getString("SagLic.ERR_63"))) { //$NON-NLS-1$
        traceTypeSet = true;
        k |= 1;
      }
      else
      if (s2.equals(Messages.getString("SagLic.EXT_64"))) { //$NON-NLS-1$
        traceTypeSet = true;
        k |= 2;
      }
      else
      if (s2.equals(Messages.getString("SagLic.INT_65"))) { //$NON-NLS-1$
        traceTypeSet = true;
        k |= 4;
      }
      else
      if (s2.equals(Messages.getString("SagLic.DBG_66"))) { //$NON-NLS-1$
        traceTypeSet = true;
        k |= 8;
      }
      else
      if (s2.equals(Messages.getString("SagLic.CHECK_OS_67"))) { //$NON-NLS-1$
        k |= 0x10;
      }
      else
      if (s2.equals(Messages.getString("SagLic.CHECK_EXP_68"))) { //$NON-NLS-1$
        k |= 0x20;
      }
      else
      if (s2.equals(Messages.getString("SagLic.READ_PARM_69"))) { //$NON-NLS-1$
        k |= 0x40;
      }
      else
      if (s2.equals(Messages.getString("SagLic.CHECK_SIG_70"))) { //$NON-NLS-1$
        k |= 0x80;
      }
      else
      if (s2.equals(Messages.getString("SagLic.GEN_SIG_71"))) { //$NON-NLS-1$
        k |= 0x100;
      }
      else
      if (s2.equals(Messages.getString("SagLic.FREE_PARM_72"))) { //$NON-NLS-1$
        k |= 0x200;
      }
      else
      if (s2.equals(Messages.getString("SagLic.SOCKETS_73"))) { //$NON-NLS-1$
        k |= 0x400;
      }
      else
      if (s2.equals(Messages.getString("SagLic.UTILS_74"))) { //$NON-NLS-1$
        k |= 0x800;
      }
      else
      if (s2.equals(Messages.getString("SagLic.MD5_75"))) { //$NON-NLS-1$
        k |= 0x1000;
      }
      else
      if (s2.equals(Messages.getString("SagLic.LOG_76"))) { //$NON-NLS-1$
        k |= 0x4000;
      }
      else
      if (s2.equals(Messages.getString("SagLic.LOG_FILE_77"))) { //$NON-NLS-1$
        k |= 0x8000;
      }
      else
      if (s2.equals(Messages.getString("SagLic.HIGH_LEVEL_78"))) { //$NON-NLS-1$
        k |= 0x3f0;
      }
      else
      if (s2.equals(Messages.getString("SagLic.LOW_LEVEL_79"))) { //$NON-NLS-1$
        k |= 0x1c00;
      }
      else
      if (s2.equals(Messages.getString("SagLic.ALL_80"))) { //$NON-NLS-1$
        k |= 0xffff;
      }
      i = j + 1;
    }
    while (i < s.length());
    if (!traceTypeSet) {
      k |= 8;
    }
    SagLicLog.logExit(16386, s1, Messages.getString("SagLic.traceLevel____81") + k); //$NON-NLS-1$
    return k;
  }

  public void setTraceLevel(int i) {
    String s = Messages.getString("SagLic.setTraceLevel()_82"); //$NON-NLS-1$
    SagLicLog.logEntry(16386, s);
    SagLicLog.logTrace(16386, s, Messages.getString("SagLic.Trace_Level____83") + i); //$NON-NLS-1$
    try {
      String s1 = Messages.getString("SagLic.0x_84") + Integer.toString(i, 16); //$NON-NLS-1$
      SagLicLog.logTrace(16386, s, Messages.getString("SagLic.Trace_Level_(conv.)____85") + s1); //$NON-NLS-1$
      System.setProperty(Messages.getString("SagLic.LICLOG_86"), s1); //$NON-NLS-1$
      Integer integer = Integer.getInteger(Messages.getString("SagLic.LICLOG_87"), 0); //$NON-NLS-1$
      int j = integer.intValue();
      SagLicLog.logTrace(15, s, Messages.getString("SagLic.Trace_Level_set_to____88") + j); //$NON-NLS-1$
      if ( (j & 0x8000) != 0) {
        System.out.println(Messages.getString("SagLic._nTrace_LOG_Output_File__n____89") + //$NON-NLS-1$
                           SagLicLog.getTraceFilePath() + Messages.getString("SagLic._n_90")); //$NON-NLS-1$
      }
      if (j != 0) {
        System.out.println(Messages.getString("SagLic._ntraceSet_______91") + j); //$NON-NLS-1$
        System.out.println(Messages.getString("SagLic.LIC_Trace_Level____92") + //$NON-NLS-1$
                           System.getProperty(Messages.getString("SagLic.LICLOG_93"), Messages.getString("SagLic.UNASSIGNED_94"))); //$NON-NLS-1$ //$NON-NLS-2$
      }
    }
    catch (SecurityException securityexception) {
      String s2 = Messages.getString("SagLic.Security_Error_n____95") + securityexception.getLocalizedMessage(); //$NON-NLS-1$
      SagLicLog.logExit(16387, s, s2);
    }
    catch (Exception exception) {
      String s3 = Messages.getString("SagLic.Undefined_Error_n____96") + exception.getLocalizedMessage(); //$NON-NLS-1$
      SagLicLog.logExit(16387, s, s3);
    }
  }

  private void updateLicenseFile(String s, String s1, String s2, int i,
                                 boolean flag, boolean flag1) throws
      SagLicException, FileNotFoundException, IOException, NullPointerException,
      UnsupportedEncodingException, NoSuchAlgorithmException {
    String s3 = Messages.getString("SagLic.updateLicenseFile()_97"); //$NON-NLS-1$
    SagLicLog.logEntry(36, s3);
    SagLicLog.logTrace(36, s3,
                       Messages.getString("SagLic._n___licenseFileName____98") + s + //$NON-NLS-1$
                       Messages.getString("SagLic._n___searchStr__________99") + //$NON-NLS-1$
                       s1 + Messages.getString("SagLic._n___replaceStr_________100") + s2 + //$NON-NLS-1$
                       Messages.getString("SagLic._n___fromIndex__________101") + i); //$NON-NLS-1$
    try {
      FileReader filereader = new FileReader(s);
      BufferedReader bufferedreader = new BufferedReader(filereader);
      String s5 = Messages.getString("SagLic._102"); //$NON-NLS-1$
      for (boolean flag2 = false; !flag2; ) {
        String s6 = bufferedreader.readLine();
        if (s6 == null) {
          flag2 = true;
        }
        else {
          s5 = s5 + s6 + Messages.getString("SagLic._n_103"); //$NON-NLS-1$
        }
      }

      bufferedreader.close();
      String s7 = SagUtilitiesStrings.replaceStrInStr(s5, s1, s2, i, flag,
          flag1);
      String s8 = getSignedCertificate(s7);
      SagLicLog.logTrace(36, s3, Messages.getString("SagLic.certificateUpdate.length()____104") + s8.length()); //$NON-NLS-1$
      if (s8.length() > 0) {
        FileWriter filewriter = new FileWriter(s);
        BufferedWriter bufferedwriter = new BufferedWriter(filewriter);
        bufferedwriter.write(s8, 0, s8.length());
        bufferedwriter.close();
        SagLicLog.logExit(36, s3, Messages.getString("SagLic.with_Certificate_written_105")); //$NON-NLS-1$
      }
      else {
        SagLicLog.logExit(36, s3, Messages.getString("SagLic.with_Certificate_NOT_written_106")); //$NON-NLS-1$
      }
    }
    catch (IOException ioexception) {
      String s4 = Messages.getString("SagLic.IO_Error_for____107") + s + Messages.getString("SagLic._n____108") + ioexception.getMessage(); //$NON-NLS-1$ //$NON-NLS-2$
      SagLicLog.logExit(35, s3, s4);
      throw new SagLicException(20,
                                Messages.getString("SagLic._n_________109") + s4 + Messages.getString("SagLic._-__110") + //$NON-NLS-1$ //$NON-NLS-2$
                                ioexception.getMessage());
    }
  }

  private String getSignedCertificate(String s) throws SagLicException,
      NullPointerException, UnsupportedEncodingException,
      NoSuchAlgorithmException {
    String s1 = Messages.getString("SagLic.getSignedCertificate()_111"); //$NON-NLS-1$
    SagLicLog.logEntry(292, s1);
    SagLicLog.logTrace(296, s1, Messages.getString("SagLic.certificateString.length()____112") + s.length()); //$NON-NLS-1$
    try {
      MessageDigest messagedigest = MessageDigest.getInstance(Messages.getString("SagLic.MD5_113")); //$NON-NLS-1$
      SagLicLog.logTrace(296, s1, Messages.getString("SagLic.New_MessageDigest_for_MD5_114")); //$NON-NLS-1$
      int i;
      if ( (i = SagUtilitiesStrings.indexStrInStr(s, Messages.getString("SagLic.<LicenseKey>_115"), 0, true)) < //$NON-NLS-1$
          0) {
        throw new SagLicException(2, s1);
      }
      byte abyte0[] = s.getBytes(Messages.getString("SagLic.US-ASCII_116")); //$NON-NLS-1$
      byte abyte1[] = s.getBytes(Messages.getString("SagLic.US-ASCII_117")); //$NON-NLS-1$
      for (int j = i + Messages.getString("SagLic.<LicenseKey>_118").length(); //$NON-NLS-1$
           j < 32 + i + Messages.getString("SagLic.<LicenseKey>_119").length(); j++) { //$NON-NLS-1$
        abyte1[j] = 32;

      }
      for (int k = 0; k < abyte1.length; k++) {
        if (SagUtilitiesStrings.isHashableChar(abyte1[k])) {
          messagedigest.update(abyte1, k, 1);

        }
      }
      SagLicLog.logTrace(296, s1, Messages.getString("SagLic.Calling_myMessageDigest.digest()..._120")); //$NON-NLS-1$
      byte abyte2[] = messagedigest.digest();
      SagLicLog.logTrace(296, s1,
                         Messages.getString("SagLic.Calling_myMessageDigest.getDigestLength()..._121")); //$NON-NLS-1$
      int k1 = abyte2.length;
      String s2 = messagedigest.toString();
      SagLicLog.logTrace(296, s1,
                         Messages.getString("SagLic._n___Digest_length____122") + k1 + Messages.getString("SagLic._n___Digest___________123") + //$NON-NLS-1$ //$NON-NLS-2$
                         abyte2 + Messages.getString("SagLic._n___Digest___________124") + s2); //$NON-NLS-1$
      for (int l = 0; l < k1; l++) {
        SagLicLog.logTrace(296, s1, Messages.getString("SagLic.digestKey[_125") + l + Messages.getString("SagLic.]____126") + abyte2[l]); //$NON-NLS-1$ //$NON-NLS-2$

      }
      int i1 = i + Messages.getString("SagLic.<LicenseKey>_127").length(); //$NON-NLS-1$
      for (int j1 = 0; i1 < 32 + i + Messages.getString("SagLic.<LicenseKey>_128").length(); j1++) { //$NON-NLS-1$
        abyte1[i1] = sHex[abyte2[j1] >> 4 & 0xf];
        abyte1[i1 + 1] = sHex[abyte2[j1] & 0xf];
        i1 += 2;
      }

      String s3 = new String(abyte1);
      SagLicLog.logExit(292, s1, Messages.getString("SagLic._n___newCertificateString____129") + s3); //$NON-NLS-1$
      return s3;
    }
    catch (NullPointerException nullpointerexception) {
      SagLicLog.logExit(289, s1,
                        Messages.getString("SagLic.ERROR__n____130") + nullpointerexception.getMessage()); //$NON-NLS-1$
      throw new NullPointerException();
    }
    catch (UnsupportedEncodingException unsupportedencodingexception) {
      SagLicLog.logExit(289, s1,
                        Messages.getString("SagLic.ERROR_-_UNSUPPORTED_ENCODING__n____131") + //$NON-NLS-1$
                        unsupportedencodingexception.getMessage());
      throw new UnsupportedEncodingException();
    }
    catch (NoSuchAlgorithmException nosuchalgorithmexception) {
      SagLicLog.logExit(289, s1,
                        Messages.getString("SagLic.ERROR_-_NO_SUCH_ALGORITHM__n____132") + //$NON-NLS-1$
                        nosuchalgorithmexception.getMessage());
    }
    throw new NoSuchAlgorithmException();
  }

  public String LICgenSignature(String s) throws SagLicException {
    String s1 = null;
    try {
      s1 = Messages.getString("SagLic.LICgenSignature()_133"); //$NON-NLS-1$
    }
    catch (Exception ex1) {
    }
    SagLicLog.logEntry(258, s1);
    SagLicLog.logTrace(264, s1, Messages.getString("SagLic.certificateString.length()____134") + s.length()); //$NON-NLS-1$
    try {
      if (s == null || s.length() <= 0) {
        throw new SagLicException(6, s1);
      }
      else {
        try {
          return getSignedCertificate(s);
        }
        catch (SagLicException ex) {
          return null;
        }
        catch (NullPointerException ex) {
          return null;
        }
        catch (UnsupportedEncodingException ex) {
          return null;
        }
        catch (NoSuchAlgorithmException ex) {
          return null;
        }
      }
    }
    catch (NullPointerException nullpointerexception) {
      String s2 = Messages.getString("SagLic.NULL_POINTER_n____135") + nullpointerexception.getMessage(); //$NON-NLS-1$
      SagLicLog.logExit(259, s1, s2);
      throw new SagLicException(6,
                                Messages.getString("SagLic._n_________136") + s2 + Messages.getString("SagLic._-__137") + //$NON-NLS-1$ //$NON-NLS-2$
                                nullpointerexception.getMessage() + Messages.getString("SagLic._-__138") + s1); //$NON-NLS-1$
    }
  }

  public void LICcheckSignature(String s) throws SagLicException {
    String s1 = Messages.getString("SagLic.LICcheckSignature()_139"); //$NON-NLS-1$
    SagLicLog.logEntry(130, s1);
    SagLicLog.logTrace(136, s1, Messages.getString("SagLic.certificateString.length()____140") + s.length()); //$NON-NLS-1$
    try {
      if (s == null || s.length() <= 0) {
        throw new SagLicException(6, s1);
      }
      String s2 = SagUtilitiesStrings.restoreMarkupChars(s);
      if (!s.equals(getSignedCertificate(s)) &&
          !s2.equals(getSignedCertificate(s2))) {
        String s3 = Messages.getString("SagLic.LIC_KEYINVALID_141"); //$NON-NLS-1$
        SagLicLog.logExit(131, s1, s3);
        throw new SagLicException(3, s1);
      }
      SagLicLog.logExit(130, s1, Messages.getString("SagLic.LIC_OK_142")); //$NON-NLS-1$
    }
    catch (NullPointerException nullpointerexception) {
      String s4 = Messages.getString("SagLic.NULL_POINTER_n____143") + nullpointerexception.getMessage(); //$NON-NLS-1$
      SagLicLog.logExit(131, s1, s4);
      throw new SagLicException(6,
                                Messages.getString("SagLic._n_________144") + s4 + Messages.getString("SagLic._-__145") + //$NON-NLS-1$ //$NON-NLS-2$
                                nullpointerexception.getMessage() + Messages.getString("SagLic._-__146") + s1); //$NON-NLS-1$
    }
    catch (UnsupportedEncodingException unsupportedencodingexception) {
      String s5 = Messages.getString("SagLic.UNSUPPORTED_ENCODING__US-ASCII_n____147") + //$NON-NLS-1$
          unsupportedencodingexception.getMessage();
      SagLicLog.logExit(131, s1, s5);
      throw new SagLicException(22,
                                Messages.getString("SagLic._n_________148") + s5 + Messages.getString("SagLic._-__149") + //$NON-NLS-1$ //$NON-NLS-2$
                                unsupportedencodingexception.getMessage() +
                                Messages.getString("SagLic._-__150") + s1); //$NON-NLS-1$
    }
    catch (NoSuchAlgorithmException nosuchalgorithmexception) {
      String s6 = Messages.getString("SagLic.NO_SUCH_ALGORITHM___MD5_n____151") + //$NON-NLS-1$
          nosuchalgorithmexception.getMessage();
      SagLicLog.logExit(131, s1, s6);
      throw new SagLicException(22,
                                Messages.getString("SagLic._n_________152") + s6 + Messages.getString("SagLic._-__153") + //$NON-NLS-1$ //$NON-NLS-2$
                                nosuchalgorithmexception.getMessage() + Messages.getString("SagLic._-__154") + //$NON-NLS-1$
                                s1);
    }
  }

  public String LICreadParameter(String s, String s1, String s2) throws
      SagLicException {
    String s3 = Messages.getString("SagLic.LICreadParameter()_155"); //$NON-NLS-1$
    SagLicLog.logEntry(66, s3);
    SagLicLog.logTrace(72, s3,
                       Messages.getString("SagLic._n___licenseFileName____156") + s + //$NON-NLS-1$
                       Messages.getString("SagLic._n___componentName______157") + //$NON-NLS-1$
                       s1 + Messages.getString("SagLic._n___parameterName______158") + s2); //$NON-NLS-1$
    try {
      if (s == null || s.length() <= 0 || s1 == null || s1.length() <= 0 || s2 == null ||
          s2.length() <= 0) {
        throw new SagLicException(6, s3);
      }
      String s4 = Messages.getString("SagLic.<LicenseKey>_159"); //$NON-NLS-1$
      String s5 = Messages.getString("SagLic._160"); //$NON-NLS-1$
      int i = SagUtilitiesStrings.indexStrInStr(s, s4, 0, true);
      if (i >= 0) {
        s5 = s;
      }
      else {
        FileReader filereader = new FileReader(s);
        BufferedReader bufferedreader = new BufferedReader(filereader);
        s5 = Messages.getString("SagLic._161"); //$NON-NLS-1$
        for (boolean flag = false; !flag; ) {
          String s11 = bufferedreader.readLine();
          if (s11 == null) {
            flag = true;
          }
          else {
            s5 = s5 + s11 + Messages.getString("SagLic._n_162"); //$NON-NLS-1$
          }
        }

        bufferedreader.close();
      }
      LICcheckSignature(s5);
      String s8 = Messages.getString("SagLic.<Component_Id___163") + s1 + Messages.getString("SagLic._>_164"); //$NON-NLS-1$ //$NON-NLS-2$
      String s10 = Messages.getString("SagLic.</Component>_165"); //$NON-NLS-1$
      int j = -1;
      int k = -1;
      j = SagUtilitiesStrings.indexStrInStr(s5, s8, 0, true);
      if (j >= 0) {
        k = SagUtilitiesStrings.indexStrInStr(s5, s10, j, true);
      }
      else {
        throw new SagLicException(5, s1);
      }
      String s12 = Messages.getString("SagLic.<_166") + s2 + Messages.getString("SagLic.>_167"); //$NON-NLS-1$ //$NON-NLS-2$
      String s13 = Messages.getString("SagLic.</_168") + s2 + Messages.getString("SagLic.>_169"); //$NON-NLS-1$ //$NON-NLS-2$
      int l = -1;
      int i1 = -1;
      SagLicLog.logTrace(72, s3, Messages.getString("SagLic.parameterStartTag____170") + s12); //$NON-NLS-1$
      l = SagUtilitiesStrings.indexStrInStr(s5, s12, j, true);
      if (l < 0 || l >= k) {
        throw new SagLicException(4, s2);
      }
      SagLicLog.logTrace(72, s3, Messages.getString("SagLic.parameterStopTag_____171") + s13); //$NON-NLS-1$
      i1 = SagUtilitiesStrings.indexStrInStr(s5, s13, l, true);
      if (i1 >= k) {
        throw new SagLicException(4, s2);
      }
      else {
        String s14 = s5.substring(l + s12.length(), i1);
        SagLicLog.logTrace(72, s3, Messages.getString("SagLic._n___parameterValue_(unrestored)____172") + s14); //$NON-NLS-1$
        s14 = SagUtilitiesStrings.restoreMarkupChars(s14);
        SagLicLog.logExit(66, s3, Messages.getString("SagLic._n___parameterValue____173") + s14); //$NON-NLS-1$
        return s14;
      }
    }
    catch (NullPointerException nullpointerexception) {
      String s6 = Messages.getString("SagLic.NULL_POINTER_n____174") + nullpointerexception.getMessage(); //$NON-NLS-1$
      SagLicLog.logExit(67, s3, s6);
      throw new SagLicException(6,
                                Messages.getString("SagLic._n_________175") + s6 + Messages.getString("SagLic._-__176") + //$NON-NLS-1$ //$NON-NLS-2$
                                nullpointerexception.getMessage() + Messages.getString("SagLic._-__177") + s3); //$NON-NLS-1$
    }
    catch (FileNotFoundException filenotfoundexception) {
      String s7 = Messages.getString("SagLic.File_NOT_Found____178") + s + Messages.getString("SagLic._n____179") + //$NON-NLS-1$ //$NON-NLS-2$
          filenotfoundexception.getMessage();
      SagLicLog.logExit(67, s3, s7);
      throw new SagLicException(1,
                                Messages.getString("SagLic._n_________180") + s7 + Messages.getString("SagLic._-__181") + //$NON-NLS-1$ //$NON-NLS-2$
                                filenotfoundexception.getMessage() + Messages.getString("SagLic._-__182") + s3); //$NON-NLS-1$
    }
    catch (IOException ioexception) {
      String s9 = Messages.getString("SagLic.IO_Error_for__n____183") + s + Messages.getString("SagLic._n____184") + ioexception.getMessage(); //$NON-NLS-1$ //$NON-NLS-2$
      SagLicLog.logExit(67, s3, s9);
      throw new SagLicException(19,
                                Messages.getString("SagLic._n_________185") + s9 + Messages.getString("SagLic._-__186") + //$NON-NLS-1$ //$NON-NLS-2$
                                ioexception.getMessage() +
                                Messages.getString("SagLic._-__187") + s3); //$NON-NLS-1$
    }
  }

  public void LICcheckExpiration(String s, String s1) throws SagLicException {
    String s2 = Messages.getString("SagLic.LICcheckExpiration()_188"); //$NON-NLS-1$
    SagLicLog.logEntry(34, s2);
    SagLicLog.logTrace(40, s2,
                       Messages.getString("SagLic._n___licenseFileName____189") + s + //$NON-NLS-1$
                       Messages.getString("SagLic._n___componentName______190") + //$NON-NLS-1$
                       s1);
    String s3 = Messages.getString("SagLic.ExpirationDate_191"); //$NON-NLS-1$
    String s4 = Messages.getString("SagLic.RegistryDate_192"); //$NON-NLS-1$
    Object obj = null;
    String s6 = null;
    try {
      if (s == null || s.length() <= 0 || s1 == null || s1.length() <= 0) {
        SagLicLog.logExit(35, s2, Messages.getString("SagLic.LIC_MISSINGPARAMS_193")); //$NON-NLS-1$
        throw new SagLicException(6, s2);
      }
      String s5 = LICreadParameter(s, s1, s3);
      SagLicLog.logTrace(40, s2, Messages.getString("SagLic.expirationDateValue_____194") + s5 + Messages.getString("SagLic.__195")); //$NON-NLS-1$ //$NON-NLS-2$
      if (s5.length() <= 0) {
        SagLicLog.logExit(35, s2, Messages.getString("SagLic.LIC_PARAMNOTFOUND___196") + s3); //$NON-NLS-1$
        throw new SagLicException(4, s3);
      }
      Calendar calendar = Calendar.getInstance(TimeZone.getDefault(),
                                               Locale.GERMANY);
      if (SagUtilitiesStrings.indexStrInStr(s5, Messages.getString("SagLic.unlimited_197"), 0, true) < 0) { //$NON-NLS-1$
        int i = SagUtilitiesStrings.indexStrInStr(s5, Messages.getString("SagLic.days_198"), 0, true); //$NON-NLS-1$
        if (i < 1) {
          Calendar calendar1 = SagUtilitiesDates.setCalendarDate(s5);
          if (SagUtilitiesDates.compareDates(calendar, calendar1) > 0) {
            SagLicLog.logExit(35, s2,
                              Messages.getString("SagLic.LIC_EXPIRED__DATE___199") + //$NON-NLS-1$
                              SagUtilitiesDates.getCalendarDate(calendar1));
            throw new SagLicException(15,
                                      Messages.getString("SagLic._n________DATE___200") + //$NON-NLS-1$
                                      SagUtilitiesDates.
                                      getCalendarDate(calendar1));
          }
        }
        else {
          String s8 = Messages.getString("SagLic.<LicenseKey>_201"); //$NON-NLS-1$
          int j = SagUtilitiesStrings.indexStrInStr(s, s8, 0, true);
          if (j >= 0) {
            SagLicLog.logExit(35, s2,
                              Messages.getString("SagLic.LIC_90DAYSCHECKNOTVALID__COMPONENT___202") + s1); //$NON-NLS-1$
            throw new SagLicException(20, Messages.getString("SagLic._203")); //$NON-NLS-1$
          }
          if (s5.charAt(i - 1) == ' ') {
            i--;
          }
          Integer integer = new Integer(s5.substring(0, i));
          int k = integer.intValue();
          SagLicLog.logTrace(40, s2, Messages.getString("SagLic.Number_of_days____204") + k); //$NON-NLS-1$
          try {
            s6 = LICreadParameter(s, s1, s4);
            SagLicLog.logTrace(40, s2, Messages.getString("SagLic.registryDateValue_____205") + s6 + Messages.getString("SagLic.__206")); //$NON-NLS-1$ //$NON-NLS-2$
          }
          catch (SagLicException saglicexception) {}
          if (s6 != null) {
            try {
              Calendar calendar2 = SagUtilitiesDates.setCalendarDate(s6);
              Calendar calendar4 = calendar2;
              if (SagUtilitiesDates.compareDates(calendar, calendar4) > 0) {
                SagLicLog.logExit(35, s2,
                                  Messages.getString("SagLic.LIC_90DAYSCHECKNOTVALID__DATE___207") + //$NON-NLS-1$
                                  SagUtilitiesDates.getCalendarDate(calendar4));
                throw new SagLicException(15,
                                          Messages.getString("SagLic._n________DATE___208") + //$NON-NLS-1$
                                          SagUtilitiesDates.
                                          getCalendarDate(calendar4));
              }
            }
            catch (IllegalArgumentException illegalargumentexception1) {
              String s13 = Messages.getString("SagLic.ILLEGAL_<RegistryDate>_TAG_VALUE_209"); //$NON-NLS-1$
              SagLicLog.logExit(35, s2,
                                Messages.getString("SagLic.LIC_MISSINGPARAMS___210") + s13 + Messages.getString("SagLic._-__211") + //$NON-NLS-1$ //$NON-NLS-2$
                                illegalargumentexception1.getMessage());
              throw new SagLicException(6,
                                        Messages.getString("SagLic._n_________212") + s13 + Messages.getString("SagLic._-__213") + //$NON-NLS-1$ //$NON-NLS-2$
                                        illegalargumentexception1.getMessage() +
                                        Messages.getString("SagLic._-__214") + s2); //$NON-NLS-1$
            }
          }
          Calendar calendar3 = SagUtilitiesDates.addDaysToDate(calendar, k);
          updateLicenseFile(s, s5, SagUtilitiesDates.getCalendarDate(calendar3),
                            0, true, false);
          if (SagUtilitiesDates.compareDates(calendar, calendar3) > 0) {
            SagLicLog.logExit(35, s2,
                              Messages.getString("SagLic.LIC_EXPIRED__DATE___215") + //$NON-NLS-1$
                              SagUtilitiesDates.getCalendarDate(calendar3));
            throw new SagLicException(15,
                                      Messages.getString("SagLic._n________DATE___216") + //$NON-NLS-1$
                                      SagUtilitiesDates.
                                      getCalendarDate(calendar3));
          }
        }
      }
      SagLicLog.logExit(34, s2, Messages.getString("SagLic.LIC_OK_217")); //$NON-NLS-1$
    }
    catch (NullPointerException nullpointerexception) {
      String s7 = Messages.getString("SagLic.NULL_POINTER_n____218") + nullpointerexception.getMessage(); //$NON-NLS-1$
      SagLicLog.logExit(35, s2, s7);
      throw new SagLicException(6, Messages.getString("SagLic._n_________219") + s7 + Messages.getString("SagLic._-__220") + s2); //$NON-NLS-1$ //$NON-NLS-2$
    }
    catch (IllegalArgumentException illegalargumentexception) {
      String s9 = Messages.getString("SagLic.ILLEGAL_<ExpirationDate>_TAG_VALUE_n____221") + //$NON-NLS-1$
          illegalargumentexception.getMessage();
      SagLicLog.logExit(35, s2, s9);
      throw new SagLicException(6,
                                Messages.getString("SagLic._n_________222") + s9 + Messages.getString("SagLic._-__223") + //$NON-NLS-1$ //$NON-NLS-2$
                                illegalargumentexception.getMessage() + Messages.getString("SagLic._-__224") + //$NON-NLS-1$
                                s2);
    }
    catch (FileNotFoundException filenotfoundexception) {
      String s10 = Messages.getString("SagLic.File_NOT_Found____225") + s + Messages.getString("SagLic._n____226") + //$NON-NLS-1$ //$NON-NLS-2$
          filenotfoundexception.getMessage();
      SagLicLog.logExit(35, s2, s10);
      throw new SagLicException(1,
                                Messages.getString("SagLic._n_________227") + s10 + Messages.getString("SagLic._-__228") + //$NON-NLS-1$ //$NON-NLS-2$
                                filenotfoundexception.getMessage() + Messages.getString("SagLic._-__229") + s2); //$NON-NLS-1$
    }
    catch (IOException ioexception) {
      String s11 = Messages.getString("SagLic.IO_Error_for____230") + s + Messages.getString("SagLic._n____231") + ioexception.getMessage(); //$NON-NLS-1$ //$NON-NLS-2$
      SagLicLog.logExit(35, s2, s11);
      throw new SagLicException(19,
                                Messages.getString("SagLic._n_________232") + s11 + Messages.getString("SagLic._-__233") + //$NON-NLS-1$ //$NON-NLS-2$
                                ioexception.getMessage() +
                                Messages.getString("SagLic._-__234") + s2); //$NON-NLS-1$
    }
    catch (NoSuchAlgorithmException nosuchalgorithmexception) {
      String s12 = Messages.getString("SagLic.NO_SUCH_ALGORITHM___MD5_n____235") + //$NON-NLS-1$
          nosuchalgorithmexception.getMessage();
      SagLicLog.logExit(35, s2, s12);
      throw new SagLicException(22,
                                Messages.getString("SagLic._n_________236") + s12 + Messages.getString("SagLic._-__237") + //$NON-NLS-1$ //$NON-NLS-2$
                                nosuchalgorithmexception.getMessage() + Messages.getString("SagLic._-__238") + //$NON-NLS-1$
                                s2);
    }
  }

  public void LICcheckOS(String s, String s1) throws SagLicException {
    String s2 = Messages.getString("SagLic.LICcheckOS()_239"); //$NON-NLS-1$
    SagLicLog.logEntry(18, s2);
    SagLicLog.logTrace(24, s2,
                       Messages.getString("SagLic._n___licenseFileName____240") + s + //$NON-NLS-1$
                       Messages.getString("SagLic._n___componentName______241") + //$NON-NLS-1$
                       s1);
    try {
      if (s == null || s.length() <= 0 || s1 == null || s1.length() <= 0) {
        throw new SagLicException(6, s2);
      }
      String s3 = Messages.getString("SagLic.OS_242"); //$NON-NLS-1$
      String s4 = LICreadParameter(s, s1, s3);
      if (s4.length() <= 0) {
        throw new SagLicException(4, s3);
      }
      s4 = s4.toLowerCase(Locale.ENGLISH);
      String s6 = System.getProperty(Messages.getString("SagLic.os.name_243"), Messages.getString("SagLic.UNASSIGNED_244")); //$NON-NLS-1$ //$NON-NLS-2$
      String s7 = System.getProperty(Messages.getString("SagLic.os.version_245"), Messages.getString("SagLic.UNASSIGNED_246")); //$NON-NLS-1$ //$NON-NLS-2$
      String s8 = System.getProperty(Messages.getString("SagLic.os.arch_247"), Messages.getString("SagLic.UNASSIGNED_248")); //$NON-NLS-1$ //$NON-NLS-2$
      s6 = s6.toLowerCase(Locale.ENGLISH);
      s7 = s7.toLowerCase(Locale.ENGLISH);
      s8 = s8.toLowerCase(Locale.ENGLISH);
      SagLicLog.logTrace(24, s2,
                         Messages.getString("SagLic._n___os.name_______249") + s6 + Messages.getString("SagLic._n___os.version____250") + s7 + //$NON-NLS-1$ //$NON-NLS-2$
                         Messages.getString("SagLic._n___os.arch_______251") + s8); //$NON-NLS-1$
      String as[] = new String[20];
      as[0] = Messages.getString("SagLic.any_252"); //$NON-NLS-1$
      if (s6.equals(Messages.getString("SagLic.linux_253")) && s8.equals(Messages.getString("SagLic.s390_254"))) { //$NON-NLS-1$ //$NON-NLS-2$
        as[1] = s6 + Messages.getString("SagLic.__255") + s8; //$NON-NLS-1$
      }
      else {
        as[1] = s6;
      }
      SagLicLog.logTrace(24, s2, Messages.getString("SagLic._nosNames[1]____256") + as[1]); //$NON-NLS-1$
      String s9 = Messages.getString("SagLic.Windows_257"); //$NON-NLS-1$
      if (as[1].startsWith(s9.toLowerCase())) {
        if (as[1].equalsIgnoreCase(Messages.getString("SagLic.Windows_XP_258")) || s7.equals(Messages.getString("SagLic.5.1_259"))) { //$NON-NLS-1$ //$NON-NLS-2$
          SagLicLog.logTrace(24, s2, Messages.getString("SagLic._nWindows_XP_260")); //$NON-NLS-1$
          as[1] = Messages.getString("SagLic.win_261"); //$NON-NLS-1$
          as[2] = Messages.getString("SagLic.winxp_262"); //$NON-NLS-1$
          as[3] = Messages.getString("SagLic.winxp_pro_263"); //$NON-NLS-1$
          as[4] = Messages.getString("SagLic.winxp_per_264"); //$NON-NLS-1$
          as[5] = Messages.getString("SagLic.winxp_p_265"); //$NON-NLS-1$
          as[6] = Messages.getString("SagLic.winxp_s_266"); //$NON-NLS-1$
          as[7] = Messages.getString("SagLic.winxp_a_267"); //$NON-NLS-1$
          as[8] = Messages.getString("SagLic.winxp_dc_268"); //$NON-NLS-1$
          as[9] = Messages.getString("SagLic.winxp_d_269"); //$NON-NLS-1$
        }
        else
        if (as[1].equalsIgnoreCase(Messages.getString("SagLic.Windows_2000_270"))) { //$NON-NLS-1$
          SagLicLog.logTrace(24, s2, Messages.getString("SagLic._nWindows_2000_271")); //$NON-NLS-1$
          as[1] = Messages.getString("SagLic.win_272"); //$NON-NLS-1$
          as[2] = Messages.getString("SagLic.w2000_273"); //$NON-NLS-1$
          as[3] = Messages.getString("SagLic.w2000p_274"); //$NON-NLS-1$
          as[4] = Messages.getString("SagLic.w2000s_275"); //$NON-NLS-1$
          as[5] = Messages.getString("SagLic.w2000a_276"); //$NON-NLS-1$
          as[6] = Messages.getString("SagLic.w2000d_277"); //$NON-NLS-1$
          as[7] = Messages.getString("SagLic.w2000dc_278"); //$NON-NLS-1$
          as[8] = Messages.getString("SagLic.winnt_5.0_279"); //$NON-NLS-1$
          as[9] = Messages.getString("SagLic.winnt_5.0p_280"); //$NON-NLS-1$
          as[10] = Messages.getString("SagLic.winnt_5.0s_281"); //$NON-NLS-1$
          as[11] = Messages.getString("SagLic.winnt_5.0a_282"); //$NON-NLS-1$
          as[12] = Messages.getString("SagLic.winnt_5.0dc_283"); //$NON-NLS-1$
          as[13] = Messages.getString("SagLic.winnt_5.0d_284"); //$NON-NLS-1$
          as[14] = Messages.getString("SagLic.win2000_285"); //$NON-NLS-1$
          as[15] = Messages.getString("SagLic.win2000p_286"); //$NON-NLS-1$
          as[16] = Messages.getString("SagLic.win2000s_287"); //$NON-NLS-1$
          as[17] = Messages.getString("SagLic.win2000a_288"); //$NON-NLS-1$
          as[18] = Messages.getString("SagLic.win2000dc_289"); //$NON-NLS-1$
          as[19] = Messages.getString("SagLic.win2000d_290"); //$NON-NLS-1$
        }
        else
        if (as[1].equalsIgnoreCase(Messages.getString("SagLic.Windows_NT_291"))) { //$NON-NLS-1$
          SagLicLog.logTrace(24, s2, Messages.getString("SagLic._nWindows_NT_292")); //$NON-NLS-1$
          as[1] = Messages.getString("SagLic.win_293"); //$NON-NLS-1$
          as[2] = Messages.getString("SagLic.winnti_4.0_294"); //$NON-NLS-1$
          as[3] = Messages.getString("SagLic.winnti_4.0p_295"); //$NON-NLS-1$
          as[4] = Messages.getString("SagLic.winnti_4.0s_296"); //$NON-NLS-1$
        }
        else
        if (as[1].equalsIgnoreCase(Messages.getString("SagLic.Windows_ME_297"))) { //$NON-NLS-1$
          SagLicLog.logTrace(24, s2, Messages.getString("SagLic._nWindows_ME_298")); //$NON-NLS-1$
          as[1] = Messages.getString("SagLic.win_299"); //$NON-NLS-1$
          as[2] = Messages.getString("SagLic.win_me_300"); //$NON-NLS-1$
          as[3] = Messages.getString("SagLic.winme_301"); //$NON-NLS-1$
        }
        else
        if (as[1].equalsIgnoreCase(Messages.getString("SagLic.Windows_98_302"))) { //$NON-NLS-1$
          SagLicLog.logTrace(24, s2, Messages.getString("SagLic._nWindows_98_303")); //$NON-NLS-1$
          as[1] = Messages.getString("SagLic.win_304"); //$NON-NLS-1$
          as[2] = Messages.getString("SagLic.win_98_305"); //$NON-NLS-1$
          as[3] = Messages.getString("SagLic.win98_306"); //$NON-NLS-1$
        }
        else
        if (as[1].equalsIgnoreCase(Messages.getString("SagLic.Windows_95_307"))) { //$NON-NLS-1$
          SagLicLog.logTrace(24, s2, Messages.getString("SagLic._nWindows_95_308")); //$NON-NLS-1$
          as[1] = Messages.getString("SagLic.win_309"); //$NON-NLS-1$
          as[2] = Messages.getString("SagLic.win_95_310"); //$NON-NLS-1$
          as[3] = Messages.getString("SagLic.win95_311"); //$NON-NLS-1$
        }
        else {
          as[1] = Messages.getString("SagLic.win_312"); //$NON-NLS-1$
        }
      }
      SagLicLog.logTrace(24, s2, Messages.getString("SagLic._n___osValue_______313") + s4); //$NON-NLS-1$
      boolean flag = false;
      for (int i = 0; !flag && i < as.length && as[i] != null; i++) {
        SagLicLog.logTrace(24, s2, Messages.getString("SagLic._n___osNames[_314") + i + Messages.getString("SagLic.]____315") + as[i]); //$NON-NLS-1$ //$NON-NLS-2$
        if (s4.equals(as[i]) || s4.startsWith(as[i] + Messages.getString("SagLic.,_316")) || //$NON-NLS-1$
            s4.indexOf(Messages.getString("SagLic.__317") + as[i] + Messages.getString("SagLic.,_318")) > 0 || //$NON-NLS-1$ //$NON-NLS-2$
            s4.indexOf(Messages.getString("SagLic.,_319") + as[i] + Messages.getString("SagLic.,_320")) > 0 || s4.endsWith(Messages.getString("SagLic.__321") + as[i]) || //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            s4.endsWith(Messages.getString("SagLic.,__322") + as[i]) || s4.endsWith(Messages.getString("SagLic.,_323") + as[i])) { //$NON-NLS-1$ //$NON-NLS-2$
          flag = true;
        }
      }

      if (!flag) {
        throw new SagLicException(16, s1);
      }
      SagLicLog.logExit(18, s2, Messages.getString("SagLic.LIC_OK_324")); //$NON-NLS-1$
    }
    catch (NullPointerException nullpointerexception) {
      String s5 = Messages.getString("SagLic.NULL_POINTER_n____325") + nullpointerexception.getMessage(); //$NON-NLS-1$
      SagLicLog.logExit(19, s2, s5);
      throw new SagLicException(6, Messages.getString("SagLic._n_________326") + s5 + Messages.getString("SagLic._-__327") + s2); //$NON-NLS-1$ //$NON-NLS-2$
    }
  }

  public String WrapLICgenSignature(String s) {
    String s1 = Messages.getString("SagLic.WrapLICgenSignature()_328"); //$NON-NLS-1$
    SagLicLog.logEntry(258, s1);
    SagLicLog.logTrace(264, s1, Messages.getString("SagLic.certificateString.length()____329") + s.length()); //$NON-NLS-1$
    try {
      SagLicLog.logExit(258, s1,
                        Messages.getString("SagLic._n___certificateString.length()____330") + s.length()); //$NON-NLS-1$
      return LICgenSignature(s);
    }
    catch (SagLicException saglicexception) {
      String s2 = Messages.getString("SagLic.____331") + saglicexception.getErrorCode() + Messages.getString("SagLic.____332") + //$NON-NLS-1$ //$NON-NLS-2$
          saglicexception.getMessage();
      SagLicLog.logExit(259, s1, Messages.getString("SagLic._n____333") + s2); //$NON-NLS-1$
      return Messages.getString("SagLic._334"); //$NON-NLS-1$
    }
  }

  public int WrapLICcheckSignature(String s) {
    String s1 = Messages.getString("SagLic.WrapLICcheckSignature()_335"); //$NON-NLS-1$
    SagLicLog.logEntry(130, s1);
    SagLicLog.logTrace(136, s1, Messages.getString("SagLic.certificateString.length()____336") + s.length()); //$NON-NLS-1$
    try {
      LICcheckSignature(s);
      SagLicLog.logExit(130, s1, Messages.getString("SagLic.LIC_OK_337")); //$NON-NLS-1$
      return 0;
    }
    catch (SagLicException saglicexception) {
      String s2 = Messages.getString("SagLic.____338") + saglicexception.getErrorCode() + Messages.getString("SagLic.____339") + //$NON-NLS-1$ //$NON-NLS-2$
          saglicexception.getMessage();
      SagLicLog.logExit(131, s1, Messages.getString("SagLic._n____340") + s2); //$NON-NLS-1$
      return saglicexception.getErrorCode();
    }
  }

  public String WrapLICreadParameter(String s, String s1, String s2) {
    String s3 = Messages.getString("SagLic.WrapLICreadParameter()_341"); //$NON-NLS-1$
    SagLicLog.logEntry(66, s3);
    SagLicLog.logTrace(72, s3,
                       Messages.getString("SagLic._n___licenseFileName____342") + s + //$NON-NLS-1$
                       Messages.getString("SagLic._n___componentName______343") + //$NON-NLS-1$
                       s1 + Messages.getString("SagLic._n___parameterName______344") + s2); //$NON-NLS-1$
    try {
      String s4 = LICreadParameter(s, s1, s2);
      SagLicLog.logExit(66, s3, Messages.getString("SagLic._n___parameterValue____345") + s4); //$NON-NLS-1$
      return s4;
    }
    catch (SagLicException saglicexception) {
      String s5 = Messages.getString("SagLic.____346") + saglicexception.getErrorCode() + Messages.getString("SagLic.____347") + //$NON-NLS-1$ //$NON-NLS-2$
          saglicexception.getMessage();
      SagLicLog.logExit(67, s3, Messages.getString("SagLic._n____348") + s5); //$NON-NLS-1$
      return s5;
    }
  }

  public int WrapLICcheckExpiration(String s, String s1) {
    String s2 = Messages.getString("SagLic.WrapLICcheckExpiration()_349"); //$NON-NLS-1$
    SagLicLog.logEntry(34, s2);
    SagLicLog.logTrace(40, s2,
                       Messages.getString("SagLic._n___licenseFileName____350") + s + //$NON-NLS-1$
                       Messages.getString("SagLic._n___componentName______351") + //$NON-NLS-1$
                       s1);
    try {
      LICcheckExpiration(s, s1);
      SagLicLog.logExit(34, s2, Messages.getString("SagLic.LIC_OK_352")); //$NON-NLS-1$
      return 0;
    }
    catch (SagLicException saglicexception) {
      String s3 = Messages.getString("SagLic.____353") + saglicexception.getErrorCode() + Messages.getString("SagLic.____354") + //$NON-NLS-1$ //$NON-NLS-2$
          saglicexception.getMessage();
      SagLicLog.logExit(35, s2, Messages.getString("SagLic._n____355") + s3); //$NON-NLS-1$
      return saglicexception.getErrorCode();
    }
  }

  public int WrapLICcheckOS(String s, String s1) {
    String s2 = Messages.getString("SagLic.WrapLICcheckOS()_356"); //$NON-NLS-1$
    SagLicLog.logEntry(18, s2);
    SagLicLog.logTrace(24, s2,
                       Messages.getString("SagLic._n___licenseFileName____357") + s + //$NON-NLS-1$
                       Messages.getString("SagLic._n___componentName______358") + //$NON-NLS-1$
                       s1);
    try {
      LICcheckOS(s, s1);
      SagLicLog.logExit(18, s2, Messages.getString("SagLic.LIC_OK_359")); //$NON-NLS-1$
      return 0;
    }
    catch (SagLicException saglicexception) {
      String s3 = Messages.getString("SagLic.____360") + saglicexception.getErrorCode() + Messages.getString("SagLic.____361") + //$NON-NLS-1$ //$NON-NLS-2$
          saglicexception.getMessage();
      SagLicLog.logExit(19, s2, Messages.getString("SagLic._n____362") + s3); //$NON-NLS-1$
      return saglicexception.getErrorCode();
    }
  }

}
