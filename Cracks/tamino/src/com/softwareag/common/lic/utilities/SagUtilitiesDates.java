// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   SagUtilitiesDates.java

package com.softwareag.common.lic.utilities;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class SagUtilitiesDates
{

    public static final String VERSION = Messages.getString("SagUtilitiesDates.1.4.0.0_1"); //$NON-NLS-1$
    public static final String COPYRIGHT = Messages.getString("SagUtilitiesDates.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$

    public SagUtilitiesDates()
    {
    }

    public static void validateDateFormat(String s)
        throws IllegalArgumentException
    {
        String s1 = Messages.getString("SagUtilitiesDates.validateDateFormat_3"); //$NON-NLS-1$
        if(s.length() != 10 || s.charAt(4) != '/' || s.charAt(7) != '/')
            throw new IllegalArgumentException(Messages.getString("SagUtilitiesDates.Date_format_must_be___YYYY/MM/DD___4")); //$NON-NLS-1$
        for(int i = 0; i < s.length(); i++)
            if(i != 4 && i != 7 && !Character.isDigit(s.charAt(i)))
                throw new IllegalArgumentException(Messages.getString("SagUtilitiesDates.Date_format_must_be___YYYY/MM/DD___5")); //$NON-NLS-1$

    }

    public static String getCalendarDate(Calendar calendar)
    {
        String s = Messages.getString("SagUtilitiesDates.getCalendarDate_6"); //$NON-NLS-1$
        String s1 = Messages.getString("SagUtilitiesDates._7") + calendar.get(1); //$NON-NLS-1$
        String s2 = Messages.getString("SagUtilitiesDates._8") + (calendar.get(2) + 1); //$NON-NLS-1$
        String s3 = Messages.getString("SagUtilitiesDates._9") + calendar.get(5); //$NON-NLS-1$
        if(s2.length() == 1)
            s2 = Messages.getString("SagUtilitiesDates.0_10") + s2; //$NON-NLS-1$
        if(s3.length() == 1)
            s3 = Messages.getString("SagUtilitiesDates.0_11") + s3; //$NON-NLS-1$
        return s1 + Messages.getString("SagUtilitiesDates./_12") + s2 + Messages.getString("SagUtilitiesDates./_13") + s3; //$NON-NLS-1$ //$NON-NLS-2$
    }

    public static Calendar setCalendarDate(String s)
        throws IllegalArgumentException
    {
        String s1 = Messages.getString("SagUtilitiesDates.setCalendarDate_14"); //$NON-NLS-1$
        validateDateFormat(s);
        Integer integer = new Integer(s.substring(0, 4));
        Integer integer1 = new Integer(s.substring(5, 7));
        Integer integer2 = new Integer(s.substring(8, 10));
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.GERMANY);
        calendar.set(1, integer.intValue());
        calendar.set(2, integer1.intValue() - 1);
        calendar.set(5, integer2.intValue());
        return calendar;
    }

    public static Calendar addDaysToDate(Calendar calendar, int i)
    {
        String s = Messages.getString("SagUtilitiesDates.addDaysToDate_15"); //$NON-NLS-1$
        calendar.add(5, i);
        return calendar;
    }

    public static int compareDates(Calendar calendar, Calendar calendar1)
        throws IllegalArgumentException
    {
        String s = Messages.getString("SagUtilitiesDates.compareDates_16"); //$NON-NLS-1$
        if(calendar.before(calendar1))
            return -1;
        return !calendar.after(calendar1) ? 0 : 1;
    }
}
