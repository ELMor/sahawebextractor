// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst
// Source File Name:   SagLicTest.java

package com;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.softwareag.common.lic.SagLic;
import com.softwareag.common.lic.SagLicException;
import com.softwareag.common.lic.SagLicLog;
import com.softwareag.common.lic.SagVersion;

public class SagLicTest {

  public static final String VERSION = Messages.getString("SagLicTest.1.4.0.0_1"); //$NON-NLS-1$
  public static final String COPYRIGHT =
      Messages.getString("SagLicTest.Copyright_Software_AG_1995-2001._All_rights_reserved._2"); //$NON-NLS-1$

  public SagLicTest() {
  }

  public static void main(String args[]) {
    String s = Messages.getString("SagLicTest.main()_3"); //$NON-NLS-1$
    SagLicLog.logEntry(10, s);
    for (int i = 0; i < args.length; i++) {
      SagLicLog.logTrace(10, s, Messages.getString("SagLicTest.args[_4") + i + Messages.getString("SagLicTest.]____5") + args[i]); //$NON-NLS-1$ //$NON-NLS-2$

    }
    String s1 = Messages.getString("SagLicTest._nUSAGE__n____java_com.SagLicTest_0_n___________________List_LICLOG_<trace_level>_options_n____java_com.SagLicTest_1_<xmlfile_in>_<component>_<parameter>_n___________Call_LIC_read_parameter()_with_the_last_3_parameters_n____java_com.SagLicTest_2_<xmlfile_in>_<xmlfile_out>_n___________Call_LIC_genSignature()_with_<xmlfile_in>_and_<xmlfile_out>_n____java_com.SagLicTest_3_<xmlfile_in>_n___________Call_LIC_checkSignature()_with_<xmlfile_in>_n____java_com.SagLicTest_4_<xmlfile_in>_<component>_n___________Call_LIC_checkExpiration()_with_<xmlfile_in>_and_<component>_n____java_com.SagLicTest_5_<xmlfile_in>_<component>_n___________Call_LIC_checkOS()_with_<xmlfile_in>_and_<component>_nFor_example__n____java_com.SagLicTest_5_ino223.xml_TaminoProduct_6"); //$NON-NLS-1$
    String s2 = Messages.getString("SagLicTest._nTo_execute_this_command_with_TRACING_turned_on,_you_may_either__n_n1._Specify_<trace_level>[,<trace_level>]..._as_the_LAST_argument;_n___i.e.,_java_com.SagLicTest_..._DBG,CHECK_OS_n2._Specify_-DLICLOG_<LICLOG>_on_the_command_line;_n___i.e.,_java_-DLICLOG_0x0018_com.SagLicTest_..._n_nLICLOG__<trace_level>_DESCRIPTION_n---------------------------------------------------------------_n0x0001__ERR___________Trace_error_conditions_n0x0002__EXT___________Trace_external_LIC_API_functions_n0x0004__INT___________Trace_internal_LIC_functions_n0x0008__DBG___________Trace_debugging_info_n0x0010__CHECK_OS______LIC_checkOS_function_only_n0x0020__CHECK_EXP_____LIC_checkExpiration_function_only_n0x0040__READ_PARM_____LIC_read_parameter_function_only_n0x0080__CHECK_SIG_____LIC_checkSignature_function_only_n0x0100__GEN_SIG_______LIC_genSignature_function_only_n0x0200__FREE_PARM_____LIC_freeParameter_function_only_n0x0400__SOCKETS_______Socket-related_operations_only_n0x0800__UTILS_________Utility_functions_only_n0x1000__MD5___________MD5_(Message_Digest)_functions_only_n0x2000__UNUSED________UNUSED_n0x4000__LOG___________Trace_LOG_functions_only_n0x8000__LIC_FILE______LOG_to_a_local_file_in_TMP_directory_n0x03f0__HIGH_LEVEL____High_Level_(LIC)_functions_only_n0x1c00__LOW_LEVEL_____Low_Level_operations_only_n0xffff__ALL___________All_Trace_Levels_above_combined_7") //$NON-NLS-1$
        ;
    Object obj = null;
    try {
      SagVersion sagversion = new SagVersion(Messages.getString("SagLicTest.Java_Package_8")); //$NON-NLS-1$
      SagLic saglic = new SagLic();
      Integer integer = Integer.getInteger(Messages.getString("SagLicTest.LICLOG_9"), 0); //$NON-NLS-1$
      int j = integer.intValue();
      SagLicLog.logTrace(15, s, Messages.getString("SagLicTest.Trace_Level_set_to____10") + j); //$NON-NLS-1$
      if (args.length < 1) {
        SagLicLog.logTrace(65535, Messages.getString("SagLicTest._11"), Messages.getString("SagLicTest._n_12") + SagVersion.getVersionString()); //$NON-NLS-1$ //$NON-NLS-2$
        if ( (j & 0x1c00) != 0) {
          Package package1 = Package.getPackage(Messages.getString("SagLicTest.com.softwareag.common.lic_13")); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest._n___Package_Information_from_JAR__n_14")); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest.____15") + package1.getName()); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest.____16") + package1.getSpecificationTitle()); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest.____17") + package1.getImplementationVersion()); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest.____18") + package1.getSpecificationVendor()); //$NON-NLS-1$
          Package package2 = Package.getPackage(
              Messages.getString("SagLicTest.com.softwareag.common.lic.utilities_19")); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest._n____20") + package2.getName()); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest.____21") + package2.getSpecificationTitle()); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest.____22") + package2.getImplementationVersion()); //$NON-NLS-1$
          System.out.println(Messages.getString("SagLicTest.____23") + package2.getSpecificationVendor()); //$NON-NLS-1$
          Package apackage[] = Package.getPackages();
          System.out.println(Messages.getString("SagLicTest._nPackages__n_24")); //$NON-NLS-1$
          for (int k = 0; k < apackage.length; k++) {
            System.out.println(Messages.getString("SagLicTest.Package_Name____25") + apackage[k].getName()); //$NON-NLS-1$
            if (apackage[k].isSealed()) {
              System.out.println(Messages.getString("SagLicTest.This_Package_is_SEALED_!_26")); //$NON-NLS-1$
            }
            System.out.println(Messages.getString("SagLicTest.___Package_Specification_Title____27") + //$NON-NLS-1$
                               apackage[k].getSpecificationTitle());
            System.out.println(Messages.getString("SagLicTest.___Package_Specification_Version____28") + //$NON-NLS-1$
                               apackage[k].getSpecificationVersion());
            System.out.println(Messages.getString("SagLicTest.___Package_Specification_Vendor____29") + //$NON-NLS-1$
                               apackage[k].getSpecificationVendor());
            System.out.println(Messages.getString("SagLicTest.___Package_Implementation_Title____30") + //$NON-NLS-1$
                               apackage[k].getImplementationTitle());
            System.out.println(Messages.getString("SagLicTest.___Package_Implementation_Version____31") + //$NON-NLS-1$
                               apackage[k].getImplementationVersion());
            System.out.println(Messages.getString("SagLicTest.___Package_Implementation_Vendor____32") + //$NON-NLS-1$
                               apackage[k].getImplementationVendor());
          }

        }
        System.out.println(s1);
        System.exit(0);
      }
      String s3 = Messages.getString("SagLicTest._33"); //$NON-NLS-1$
      String s4 = Messages.getString("SagLicTest._34"); //$NON-NLS-1$
      String s5 = Messages.getString("SagLicTest._35"); //$NON-NLS-1$
      String s6 = Messages.getString("SagLicTest._36"); //$NON-NLS-1$
      String s7 = Messages.getString("SagLicTest._37"); //$NON-NLS-1$
      String s8 = Messages.getString("SagLicTest._38"); //$NON-NLS-1$
      if ( (j & 0x8000) != 0) {
        System.out.println(Messages.getString("SagLicTest._nTrace_LOG_Output_File__n____39") + //$NON-NLS-1$
                           SagLicLog.getTraceFilePath() + Messages.getString("SagLicTest._n_40")); //$NON-NLS-1$
      }
      switch (args.length) {
        case 5: // '\005'
          s3 = args[4];
          // fall through

        case 4: // '\004'
          s7 = args[3];
          if (s3.length() == 0) {
            s3 = args[3];
            // fall through

          }
        case 3: // '\003'
          s6 = args[2];
          s5 = args[2];
          if (s3.length() == 0) {
            s3 = args[2];
            // fall through

          }
        case 2: // '\002'
          s4 = args[1];
          if (s3.length() == 0) {
            s3 = args[1];
            // fall through

          }
        case 1: // '\001'
          s8 = args[0];
          break;

        default:
          System.out.println(s1);
          System.out.println(s2);
          break;
      }
      File file = new File(s4);
      if (s8.equals(Messages.getString("SagLicTest.0_41"))) { //$NON-NLS-1$
        if (args.length > 1) {
          saglic.setTraceLevel(saglic.convertTraceLevel(s3));
          SagLicLog.logTrace(16392, s, Messages.getString("SagLicTest.traceLevel____42") + s3); //$NON-NLS-1$
        }
        System.out.println(s2);
      }
      else
      if (s8.equals(Messages.getString("SagLicTest.1_43"))) { //$NON-NLS-1$
        SagLicLog.logTrace(65535, Messages.getString("SagLicTest._44"), //$NON-NLS-1$
                           Messages.getString("SagLicTest._n___LICreadParameter()_n_n___License_File_(In)____45") + //$NON-NLS-1$
                           file.getName() + Messages.getString("SagLicTest._n___Component_Name_______46") + s6 + //$NON-NLS-1$
                           Messages.getString("SagLicTest._n___Parameter_Name_______47") + s7); //$NON-NLS-1$
        if (args.length > 4) {
          saglic.setTraceLevel(saglic.convertTraceLevel(s3));
          SagLicLog.logTrace(16392, s, Messages.getString("SagLicTest.traceLevel____48") + s3); //$NON-NLS-1$
        }
        String s9 = saglic.LICreadParameter(s4, s6, s7);
        if (s9.length() > 0) {
          SagLicLog.logTrace(65535, Messages.getString("SagLicTest._49"), Messages.getString("SagLicTest._n___Return____50") + s9); //$NON-NLS-1$ //$NON-NLS-2$
        }
      }
      else
      if (s8.equals(Messages.getString("SagLicTest.2_51")) || s8.equals(Messages.getString("SagLicTest.3_52"))) { //$NON-NLS-1$ //$NON-NLS-2$
        FileReader filereader = new FileReader(s4);
        char ac[] = new char[ (int) file.length()];
        int l = filereader.read(ac);
        filereader.close();
        SagLicLog.logTrace(392, s, Messages.getString("SagLicTest.Chars_read____53") + l); //$NON-NLS-1$
        String s10 = new String(ac);
        SagLicLog.logTrace(392, s, Messages.getString("SagLicTest._n___certificate.length()____54") + s10.length()); //$NON-NLS-1$
        if (s8.equals(Messages.getString("SagLicTest.2_55"))) { //$NON-NLS-1$
          File file1 = new File(s5);
          SagLicLog.logTrace(65535, Messages.getString("SagLicTest._56"), //$NON-NLS-1$
              Messages.getString("SagLicTest._n___LICgenSignature()_n_n___License_File_(In)_____57") + //$NON-NLS-1$
                             file.getName() + Messages.getString("SagLicTest._n___License_File_(Out)____58") + //$NON-NLS-1$
                             file1.getName());
          if (args.length > 3) {
            saglic.setTraceLevel(saglic.convertTraceLevel(s3));
            SagLicLog.logTrace(16392, s, Messages.getString("SagLicTest.traceLevel____59") + s3); //$NON-NLS-1$
          }
          String s11 = saglic.LICgenSignature(s10);
          SagLicLog.logTrace(264, s,
                             Messages.getString("SagLicTest._n___certificateUpdate.length()____60") + s11.length()); //$NON-NLS-1$
          if (s11.length() > 0) {
            FileWriter filewriter = new FileWriter(s5);
            BufferedWriter bufferedwriter = new BufferedWriter(filewriter);
            filewriter.write(s11, 0, s11.length());
            filewriter.close();
            SagLicLog.logTrace(65535, Messages.getString("SagLicTest._61"), //$NON-NLS-1$
                               Messages.getString("SagLicTest._n____62") + file1.getName() + Messages.getString("SagLicTest._n___has_been_SIGNED_and_WRITTEN._63")); //$NON-NLS-1$ //$NON-NLS-2$
          }
        }
        else
        if (s8.equals(Messages.getString("SagLicTest.3_64"))) { //$NON-NLS-1$
          SagLicLog.logTrace(65535, Messages.getString("SagLicTest._65"), //$NON-NLS-1$
              Messages.getString("SagLicTest._n___LICcheckSignature()_n_n___License_File_(In)_____66") + //$NON-NLS-1$
                             file.getName());
          if (args.length > 2) {
            saglic.setTraceLevel(saglic.convertTraceLevel(s3));
            SagLicLog.logTrace(16392, s, Messages.getString("SagLicTest.traceLevel____67") + s3); //$NON-NLS-1$
          }
          saglic.LICcheckSignature(s10);
          SagLicException saglicexception3 = new SagLicException(0);
          SagLicLog.logTrace(65535, Messages.getString("SagLicTest._68"), //$NON-NLS-1$
                             Messages.getString("SagLicTest._n____69") + saglicexception3.getErrorCode() + Messages.getString("SagLicTest.____70") + //$NON-NLS-1$ //$NON-NLS-2$
                             saglicexception3.getMessage());
        }
      }
      else
      if (s8.equals(Messages.getString("SagLicTest.4_71"))) { //$NON-NLS-1$
        SagLicLog.logTrace(65535, Messages.getString("SagLicTest._72"), //$NON-NLS-1$
            Messages.getString("SagLicTest._n___LICcheckExpiration()_n_n___License_File_(In)____73") + //$NON-NLS-1$
                           file.getName() + Messages.getString("SagLicTest._n___Component_Name_______74") + s6); //$NON-NLS-1$
        if (args.length > 3) {
          saglic.setTraceLevel(saglic.convertTraceLevel(s3));
          SagLicLog.logTrace(16392, s, Messages.getString("SagLicTest.traceLevel____75") + s3); //$NON-NLS-1$
        }
        saglic.LICcheckExpiration(s4, s6);
        SagLicException saglicexception1 = new SagLicException(0);
        SagLicLog.logTrace(65535, Messages.getString("SagLicTest._76"), //$NON-NLS-1$
                           Messages.getString("SagLicTest._n____77") + saglicexception1.getErrorCode() + Messages.getString("SagLicTest.____78") + //$NON-NLS-1$ //$NON-NLS-2$
                           saglicexception1.getMessage());
      }
      else
      if (s8.equals(Messages.getString("SagLicTest.5_79"))) { //$NON-NLS-1$
        SagLicLog.logTrace(65535, Messages.getString("SagLicTest._80"), //$NON-NLS-1$
                           Messages.getString("SagLicTest._n___LICcheckOS()_n_n___License_File_(In)____81") + //$NON-NLS-1$
                           file.getName() + Messages.getString("SagLicTest._n___Component_Name_______82") + s6); //$NON-NLS-1$
        if (args.length > 3) {
          saglic.setTraceLevel(saglic.convertTraceLevel(s3));
          SagLicLog.logTrace(16392, s, Messages.getString("SagLicTest.traceLevel____83") + s3); //$NON-NLS-1$
        }
        saglic.LICcheckOS(s4, s6);
        SagLicException saglicexception2 = new SagLicException(0);
        SagLicLog.logTrace(65535, Messages.getString("SagLicTest._84"), //$NON-NLS-1$
                           Messages.getString("SagLicTest._n____85") + saglicexception2.getErrorCode() + Messages.getString("SagLicTest.____86") + //$NON-NLS-1$ //$NON-NLS-2$
                           saglicexception2.getMessage());
      }
      else {
        System.out.println(s1);
      }
    }
    catch (SagLicException saglicexception) {
      SagLicLog.logTrace(65535, Messages.getString("SagLicTest._87"), //$NON-NLS-1$
                         Messages.getString("SagLicTest._n____88") + saglicexception.getErrorCode() + Messages.getString("SagLicTest.____89") + //$NON-NLS-1$ //$NON-NLS-2$
                         saglicexception.getMessage());
      if (saglicexception.getErrorCode() == 6) {
        System.out.println(s1);
      }
    }
    catch (FileNotFoundException filenotfoundexception) {
      SagLicLog.logTrace(65535, Messages.getString("SagLicTest._90"), //$NON-NLS-1$
                         Messages.getString("SagLicTest._njava_com.SagLicTest___Error_-_File_NOT_Found_-__91") + //$NON-NLS-1$
                         filenotfoundexception.getMessage());
      System.out.println(s1);
    }
    catch (IOException ioexception) {
      SagLicLog.logTrace(65535, Messages.getString("SagLicTest._92"), //$NON-NLS-1$
                         Messages.getString("SagLicTest._njava_com.SagLicTest___Error_-_IO_-__93") + //$NON-NLS-1$
                         ioexception.getMessage());
      System.out.println(s1);
    }
    catch (Exception exception) {
      SagLicLog.logTrace(65535, Messages.getString("SagLicTest._94"), //$NON-NLS-1$
                         Messages.getString("SagLicTest._njava_com.SagLicTest___Error_-__95") + //$NON-NLS-1$
                         exception.getMessage());
      System.out.println(s1);
    }
  }
}
