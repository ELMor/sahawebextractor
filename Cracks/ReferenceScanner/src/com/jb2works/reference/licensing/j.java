//Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: fieldsfirst 
//Source File Name:   source

package com.jb2works.reference.licensing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.jb2works.reference.HttpScanner;
import com.jb2works.reference.profile.a;
import com.jb2works.reference.util.m;

//Referenced classes of package com.jb2works.reference.licensing:
//         e, i, a, f, 
//         d, g

public class j
{

 public static final class i extends RuntimeException
 {

     public final Collection a;

     private i(Collection collection)
     {
         a = com.jb2works.reference.licensing.j.a(collection);
     }

     i(Collection collection, a a1)
     {
         this(collection);
     }
 }


 private com.jb2works.reference.licensing.e l;
 private boolean u;
 private String t[];
 private final String o = com.jb2works.reference.util.q.a("d01ff0953bdb312d8e1a2d082ed6", "xyz");
 private final String b = com.jb2works.reference.util.q.a("0faee10117eabdd410e5ba", "xyz");
 private final String m = com.jb2works.reference.util.q.a("0b07544bcf918c65113c6011cc5a29", "xyz");
 private final String v = com.jb2works.reference.util.q.a("4eaef005ff8af0ce5efaaf800ee121fef408f5", "xyz");
 private final String d = com.jb2works.reference.util.q.a("3cc8653bc2344c4c34fbe6b43bec204b51d44c22741c5480fc91645b49c3", "xyz");
 private final String n = com.jb2works.reference.util.q.a("99a3aaf9d636fd392bda3eaaa83693b2599449b9a3", "xyz");
 private final String g = com.jb2works.reference.util.q.a("614897015c893808253049111a48161149131d249c5c", "xyz");
 private final String q = com.jb2works.reference.util.q.a("19d96e727aa28765e19663e49672a796682797a1777e", "xyz");
 private final String i = com.jb2works.reference.util.q.a("b99731c9c646f9c9a509c3c6b9c7d59ade6aa9d36f", "xyz");
 private final String k = com.jb2works.reference.util.q.a("96470a87484d80199c5b18", "xyz");
 private final com.jb2works.reference.licensing.e f;
 private final com.jb2works.reference.licensing.e w;
 private final com.jb2works.reference.licensing.e c;
 private final com.jb2works.reference.licensing.e h;
 private final com.jb2works.reference.licensing.e a;
 private final com.jb2works.reference.licensing.e p;
 private final com.jb2works.reference.licensing.e s;
 private final com.jb2works.reference.licensing.e j;
 private final com.jb2works.reference.licensing.e r;
 public final com.jb2works.reference.licensing.e e[];

 public j()
 {
     l = null;
     u = false;
     f = b(o + b);
     w = b(o + m);
     c = b(o + v);
     h = b(o + d);
     a = b(o + n);
     p = a(o + g);
     s = a(o + q);
     j = a(o + i);
     r = a(o + k);
     e = (new com.jb2works.reference.licensing.e[] {
         f, w, c, h, a, p, s, j, r
     });
 }

 public boolean g()
 {
     if(l == null)
     {
         com.jb2works.reference.licensing.e e1 = k();
         if(e1 == null)
             return true;
         com.jb2works.reference.licensing.e e2 = e(e1);
         com.jb2works.reference.licensing.e e3 = d(e2);
         a(e3);
     }
     return l.a() ? true : true ; // DEBE DEVOLVER TRUE SIEMPRE
 }

 private com.jb2works.reference.licensing.e d(com.jb2works.reference.licensing.e e1)
 {
     if((e1 == s || e1 == j) && a(e1.f()))
         e1 = c;
     return e1;
 }

 private boolean a(long l1)
 {
     return b(l1) < 0L;
 }

 public int j()
 {
     if(l != s && l != j)
         return -1;
     else
         return d(l.f());
 }

 private int d(long l1)
 {
     return (int)(b(l1) / 0x36ee80L);
 }

 private long b(long l1)
 {
     long l2 = l1 + com.jb2works.reference.e.D().I();
     l2 -= System.currentTimeMillis();
     return l2;
 }

 private com.jb2works.reference.licensing.e e(com.jb2works.reference.licensing.e e1)
 {
     if(e1 == p && f(e1.b()))
         e1 = w;
     return e1;
 }

 public static boolean f(long l1)
 {
     return c(l1) < 0L;
 }

 private com.jb2works.reference.licensing.e k()
 {
     com.jb2works.reference.licensing.e e1 = null;
     try
     {
         boolean flag = false;
         e.f f1 = com.jb2works.reference.licensing.e.a(false);
         e1 = f1 != null && !c(f1.b) ? f1.b : null;
     }
     catch(IOException ioexception) { }
     catch(RuntimeException runtimeexception) { }
     return e1;
 }

 private com.jb2works.reference.licensing.e a(String s1)
 {
     return com.jb2works.reference.licensing.e.a("registerAfter.xml", s1, true);
 }

 private com.jb2works.reference.licensing.e b(String s1)
 {
     return com.jb2works.reference.licensing.e.a("registerBefore.xml", s1, false);
 }

 public boolean b(com.jb2works.reference.licensing.e e1)
 {
     return e1 == l;
 }

 public void a(HttpScanner httpscanner, com.jb2works.reference.page.e e1)
 {
     a(e1.b.b, e1.a.b);
     c(e1.c.b);
     u = !f();
     if(u)
         return;
     m m1 = com.jb2works.reference.e.D().az;
     String s1 = m1.g();
     String s2 = m1.b();
     if(com.jb2works.reference.e.D().Q() == null)
     {
         d d1 = com.jb2works.reference.licensing.d.b(s1);
         if(d1 != null && d1.a())
         {
             a(w);
         } else
         {
             if(d1 != null)
                 p.b(d1.b());
             a(p);
         }
     } else
     {
         com.jb2works.reference.licensing.g g1 = com.jb2works.reference.licensing.g.c(com.jb2works.reference.e.D().Q());
         if(g1 == null)
             a(a);
         else
         if(g1.a(s1, s2))
             a(r);
         else
         if(!g1.a())
             a(h);
         else
         if(l == c)
         {
             j.c();
             a(j);
         } else
         {
             s.c();
             a(s);
         }
     }
 }

 private void a(com.jb2works.reference.licensing.e e1)
 {
     if(l != e1)
     {
         l = e1;
         try
         {
             e1.h();
         }
         catch(IOException ioexception)
         {
             ioexception.printStackTrace();
         }
     }
 }

 static URL d()
     throws MalformedURLException
 {
     String s1 = com.jb2works.reference.e.D().o();
     int i1 = s1.indexOf("http://") + "http://".length();
     i1 = s1.indexOf('/', i1 + 1);
     return new URL(s1.substring(0, i1));
 }

 public boolean f()
 {
     try
     {
         return com.jb2works.reference.util.o.a(d()) != null;
     }
     catch(MalformedURLException malformedurlexception)
     {
         malformedurlexception.printStackTrace();
     }
     return false;
 }

 public String h()
 {
     String s1 = com.jb2works.reference.e.D().d();
     return s1 != null && s1.indexOf("<") == -1 ? s1 : "&lt;no proxy>";
 }

 private static void a(String s1, String s2)
 {
     s1 = !"<no proxy>".equals(s1) && !"".equals(s1) ? s1 : null;
     s2 = !"<no port>".equals(s2) && !"".equals(s2) ? s2 : null;
     com.jb2works.reference.e.D().a(s1, s2);
 }

 public String c()
 {
     String s1 = com.jb2works.reference.e.D().M();
     return s1 != null && s1.indexOf("<") == -1 ? s1 : "&lt;no port>";
 }

 public String i()
 {
     String s1 = com.jb2works.reference.e.D().Q();
     return s1 != null && s1.indexOf("<") == -1 ? s1 : "&lt;no activation key>";
 }

 private void c(String s1)
 {
     String s2 = !"<no activation key>".equals(s1) && !"".equals(s1) ? s1 : null;
     com.jb2works.reference.e.D().f(s2);
 }

 public String b()
 {
     if(p == l)
         return com.jb2works.reference.util.q.a("791c8225436f7013cd681a6a1486", "ldsf");
     if(s == l || j == l)
         return com.jb2works.reference.util.q.a("e1d36e6f22c40e6162f03c626de9e17d61fc387172323f156060", "sedf");
     if(r == l || j == l)
         return com.jb2works.reference.util.q.a("0f231006dafd333f03eef9524409d0298244024023d0ba", "sedf1");
     else
         return "n/a";
 }

 public int e()
 {
     if(p == l)
         return e(l.b());
     else
         return -1;
 }

 static int e(long l1)
 {
     return (int)(c(l1) / 0x36ee80L);
 }

 private static long c(long l1)
 {
     long l2 = l1 + com.jb2works.reference.e.D().v();
     l2 -= System.currentTimeMillis();
     return l2;
 }

 public boolean a()
 {
     return u;
 }

 com.jb2works.reference.licensing.e d(String s1)
 {
     for(int i1 = 0; i1 < e.length; i1++)
     {
         com.jb2works.reference.licensing.e e1 = e[i1];
         if(e1.d().equals(s1))
             return e1;
     }

     return null;
 }

 public boolean c(com.jb2works.reference.licensing.e e1)
 {
     return e1 == f;
 }

 public void b(Collection collection)
 {
     if(l == null)
     {
         ArrayList arraylist = new ArrayList();
         for(Iterator iterator = collection.iterator(); iterator.hasNext();)
         {
             a a1 = (a)iterator.next();
             if(!a(a1.a))
                 arraylist.add(a1.a.getName());
         }

         if(!arraylist.isEmpty())
         {
             a(f);
             throw new i(arraylist, null);
         }
     }
 }

 static Collection a(Collection collection)
 {
     HashSet hashset = new HashSet();
     for(Iterator iterator = collection.iterator(); iterator.hasNext();)
     {
         String s1 = (String)iterator.next();
         int i1 = s1.indexOf(".");
         if(i1 != -1)
         {
             int j1 = s1.indexOf(".", i1 + 1);
             if(j1 != -1)
                 i1 = j1;
         }
         if(i1 != -1)
         {
             String s2 = s1.substring(0, i1) + ".*";
             if(!hashset.contains(s2))
                 hashset.add(s2);
         }
     }

     return hashset;
 }

 private boolean a(Class class1)
 {
     String s1 = class1.getName();
     if(s1 == null)
         return true;
     String as[] = l();
     for(int i1 = 0; i1 < as.length; i1++)
         if(s1.startsWith(as[i1]))
             return true;

     return false;
 }

 private String[] l()
 {
     if(t == null)
     {
         ArrayList arraylist = new ArrayList();
         arraylist.add("[");
         arraylist.add("org.");
         arraylist.add("java.");
         arraylist.add("sun.");
         arraylist.add("filters.");
         arraylist.add("compressionFilters.");
         arraylist.add("com.sun.");
         arraylist.add("javax.");
         arraylist.add("listeners.");
         arraylist.add("edu.");
         arraylist.add("oracle.");
         arraylist.add("com.jb2works.");
         arraylist.add("com.intellij.");
         File file = new File(com.jb2works.reference.e.D().f());
         if(file.exists() && file.canRead())
             try
             {
                 BufferedReader bufferedreader = new BufferedReader(new FileReader(file));
                 String s1 = bufferedreader.readLine();
                 String s2 = com.jb2works.reference.util.q.a(s1, "90e3e");
                 for(StringTokenizer stringtokenizer = new StringTokenizer(s2, ","); stringtokenizer.hasMoreTokens(); arraylist.add(stringtokenizer.nextToken()));
                 bufferedreader.close();
             }
             catch(IOException ioexception)
             {
                 ioexception.printStackTrace();
             }
         t = (String[])arraylist.toArray(new String[arraylist.size()]);
     }
     return t;
 }
}
