// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseInProcessWizardPageX.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseActivationKey;
import com.instantiations.common.core.LicenseSerialNumber;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardPageX

public class LicenseInProcessWizardPageX extends LicenseWizardPageX
{

    private Label _responseLabel;
    private Label _serialNumberLabel;
    private Text _serialNumberField;
    private Text _activationKeyField;
    private Text _textField;

    public LicenseInProcessWizardPageX()
    {
        super("wizardPage");
        setTitle("Registration In Process");
        setDescription("Enter activation key from email...");
    }

    public void createControl(Composite parent)
    {
        Composite container = new Composite(parent, 0);
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 2;
        container.setLayout(gridLayout);
        setControl(container);
        _responseLabel = new Label(container, 0);
        GridData gridData_2 = new GridData();
        gridData_2.horizontalSpan = 2;
        _responseLabel.setLayoutData(gridData_2);
        _responseLabel.setText("Responce from the server:");
        _textField = new Text(container, 2826);
        GridData gridData = new GridData(1296);
        gridData.horizontalIndent = 20;
        gridData.horizontalSpan = 2;
        _textField.setLayoutData(gridData);
        Label label_2 = new Label(container, 0);
        GridData gridData_1 = new GridData();
        gridData_1.horizontalSpan = 2;
        label_2.setLayoutData(gridData_1);
        label_2.setText("Once you receive the email from our server,\ncopy the activation key out of the email and paste it into the field below:");
        Label label = new Label(container, 0);
        GridData gridData_3 = new GridData();
        gridData_3.horizontalIndent = 20;
        label.setLayoutData(gridData_3);
        label.setText("Activation Key:");
        _activationKeyField = new Text(container, 2048);
        _activationKeyField.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e)
            {
                updatePageComplete();
            }

        });
        _activationKeyField.setLayoutData(new GridData(768));
        _serialNumberLabel = new Label(container, 0);
        GridData gridData_4 = new GridData();
        gridData_4.horizontalIndent = 20;
        _serialNumberLabel.setLayoutData(gridData_4);
        _serialNumberLabel.setText("Serial Number:");
        _serialNumberField = new Text(container, 2048);
        _serialNumberField.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e)
            {
                updatePageComplete();
            }

        });
        _serialNumberField.setLayoutData(new GridData(768));
        Label label_1 = new Label(container, 0);
        GridData gridData_5 = new GridData();
        gridData_5.horizontalSpan = 2;
        label_1.setLayoutData(gridData_5);
        label_1.setText("Alternately, you may close this wizard, \nand then later when you receive the email from our server, reopen this wizard \nand copy the activation key out of the email and paste it into the field on the prior page.");
        updatePageComplete();
    }

    private void updatePageComplete()
    {
        String errMsg = validateContent();
        setMessage(null);
        setErrorMessage(errMsg);
        setPageComplete(errMsg == null);
    }

    protected String validateContent()
    {
        LicenseActivationKey activationKey = getActivationKey();
        if(activationKey == null)
            return "Please enter the activation key";
        else
            return null;
    }

    public void setTransactionMessage(String message)
    {
        _textField.setText(message);
    }

    public void setTransactionDescription(String message)
    {
        _responseLabel.setText(message);
    }

    public void showSerialNumber(boolean show)
    {
        _serialNumberLabel.setVisible(show);
        _serialNumberField.setVisible(show);
    }

    public LicenseActivationKey getActivationKey()
    {
        String text = LicenseWizardPageX.trimActivationKeyOrSerialNumberText(_activationKeyField);
        if(text == null)
            return null;
        else
            return new LicenseActivationKey(text);
    }

    public LicenseSerialNumber getSerialNumber()
    {
        String text = LicenseWizardPageX.trimActivationKeyOrSerialNumberText(_serialNumberField);
        if(text == null)
            return null;
        else
            return new LicenseSerialNumber(text);
    }

}
