// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseQuestionWizardPageX.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseProduct;
import com.instantiations.common.core.LicenseUtil;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class LicenseQuestionWizardPageX extends WizardPage
{

    private static final String NOT_COMPLETE = "NOT_COMPLETE";
    private Label _howUsefulLabel1;
    private Button _howUsefulFields[];
    private static int _howUseful = -1;
    private Label _moreUsefulLabel;
    private Text _moreUsefulField;
    private static String _moreUseful = "";
    private Label _purchaseLabel;
    private Button _purchaseNoField;
    private Button _purchaseYesField;
    private static boolean _purchaseYes = false;
    private static boolean _purchaseNo = true;
    private Label _whyNotLabel;
    private Text _whyNotField;
    private static String _whyNot = "";
    private Label _purchasePriceLabel;
    private Text _purchasePriceField;
    private static String _purchasePrice = "";
    private Map _textMap;

    public LicenseQuestionWizardPageX()
    {
        super("wizardPage");
        setTitle("Evaluation Questionaire");
        setDescription("Please tell about your evaluation experience.\nThis information will be kept confidential and not used outside of Instantiations.");
    }

    public void createControl(Composite parent)
    {
        Composite container = new Composite(parent, 0);
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 1;
        container.setLayout(gridLayout);
        setControl(container);
        _howUsefulLabel1 = new Label(container, 0);
        GridData gridData_1 = new GridData();
        _howUsefulLabel1.setLayoutData(gridData_1);
        _howUsefulLabel1.setText("How useful is ${product} to you (1-5) ?");
        Composite howUsefulComposite = new Composite(container, 0);
        GridData gridData_3 = new GridData();
        gridData_3.horizontalIndent = 20;
        howUsefulComposite.setLayoutData(gridData_3);
        GridLayout gridLayout_1 = new GridLayout();
        gridLayout_1.marginHeight = 0;
        gridLayout_1.numColumns = 7;
        howUsefulComposite.setLayout(gridLayout_1);
        Label howUsefulLabel2 = new Label(howUsefulComposite, 0);
        howUsefulLabel2.setText("[1 = not useful]     ");
        Button howUsefulField1 = new Button(howUsefulComposite, 16);
        howUsefulField1.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        howUsefulField1.setText("1");
        Button howUsefulField2 = new Button(howUsefulComposite, 16);
        howUsefulField2.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        howUsefulField2.setText("2");
        Button howUsefulField3 = new Button(howUsefulComposite, 16);
        howUsefulField3.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        howUsefulField3.setText("3");
        Button howUsefulField4 = new Button(howUsefulComposite, 16);
        howUsefulField4.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        howUsefulField4.setText("4");
        Button howUsefulField5 = new Button(howUsefulComposite, 16);
        howUsefulField5.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        howUsefulField5.setText("5");
        Label howUsefulLabel3 = new Label(howUsefulComposite, 0);
        howUsefulLabel3.setText("     [5 = very useful]");
        Label spacer1 = new Label(container, 0);
        spacer1.setText("");
        _moreUsefulLabel = new Label(container, 0);
        _moreUsefulLabel.setText("How can we make ${product} more useful to you ?");
        _moreUsefulField = new Text(container, 2050);
        _moreUsefulField.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e)
            {
                updatePageComplete();
            }

        });
        _moreUsefulField.addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent e)
            {
                if(e.character == '\t')
                {
                    if(_purchaseYesField.getSelection())
                        _purchaseYesField.setFocus();
                    else
                        _purchaseNoField.setFocus();
                    e.doit = false;
                }
            }

        });
        _moreUsefulField.setTextLimit(250);
        GridData gridData_4 = new GridData(768);
        gridData_4.heightHint = 50;
        gridData_4.horizontalIndent = 20;
        _moreUsefulField.setLayoutData(gridData_4);
        Label spacer2 = new Label(container, 0);
        spacer2.setText("");
        Composite purchaseComposite = new Composite(container, 0);
        GridLayout gridLayout_3 = new GridLayout();
        gridLayout_3.marginWidth = 0;
        gridLayout_3.marginHeight = 0;
        gridLayout_3.numColumns = 3;
        purchaseComposite.setLayout(gridLayout_3);
        _purchaseLabel = new Label(purchaseComposite, 0);
        _purchaseLabel.setText("Are you planning to purchase ${product} ?");
        _purchaseYesField = new Button(purchaseComposite, 16);
        _purchaseYesField.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                showWhyNotAndHowMuch(false);
                updatePageComplete();
            }

        });
        _purchaseYesField.setText("Yes");
        _purchaseNoField = new Button(purchaseComposite, 16);
        _purchaseNoField.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                showWhyNotAndHowMuch(true);
                updatePageComplete();
            }

        });
        _purchaseNoField.setText("No");
        Composite whyNotComposite = new Composite(container, 0);
        GridData gridData_5 = new GridData(256);
        gridData_5.horizontalIndent = 20;
        whyNotComposite.setLayoutData(gridData_5);
        GridLayout gridLayout_2 = new GridLayout();
        gridLayout_2.marginWidth = 0;
        gridLayout_2.marginHeight = 0;
        gridLayout_2.numColumns = 2;
        whyNotComposite.setLayout(gridLayout_2);
        _whyNotLabel = new Label(whyNotComposite, 0);
        GridData gridData_6 = new GridData();
        gridData_6.horizontalSpan = 2;
        _whyNotLabel.setLayoutData(gridData_6);
        _whyNotLabel.setText("Why not ?");
        _whyNotField = new Text(whyNotComposite, 2050);
        _whyNotField.addKeyListener(new KeyAdapter() {

            public void keyPressed(KeyEvent e)
            {
                if(e.character == '\t')
                {
                    _purchasePriceField.setFocus();
                    e.doit = false;
                }
            }

        });
        _whyNotField.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e)
            {
                updatePageComplete();
            }

        });
        GridData gridData_7 = new GridData(768);
        gridData_7.heightHint = 50;
        gridData_7.horizontalSpan = 2;
        _whyNotField.setLayoutData(gridData_7);
        _purchasePriceLabel = new Label(whyNotComposite, 0);
        _purchasePriceLabel.setText("How much would you be willing to pay for ${product} ($0 to $${listPrice})?");
        _purchasePriceField = new Text(whyNotComposite, 2048);
        _purchasePriceField.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        _purchasePriceField.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e)
            {
                updatePageComplete();
            }

        });
        GridData gridData = new GridData(256);
        gridData.widthHint = 50;
        _purchasePriceField.setLayoutData(gridData);
        _howUsefulFields = (new Button[] {
            howUsefulField1, howUsefulField2, howUsefulField3, howUsefulField4, howUsefulField5
        });
    }

    private void showWhyNotAndHowMuch(boolean visible)
    {
        _whyNotLabel.setVisible(visible);
        _whyNotField.setVisible(visible);
        _purchasePriceLabel.setVisible(visible);
        _purchasePriceField.setVisible(visible);
    }

    public void setProduct(LicenseProduct product)
    {
        setProductText(product, _howUsefulLabel1);
        setProductText(product, _moreUsefulLabel);
        setProductText(product, _purchaseLabel);
        setProductText(product, _purchasePriceLabel);
        _purchasePriceLabel.setText(LicenseUtil.replace(_purchasePriceLabel.getText(), "${listPrice}", Integer.toString(product.getListPrice())));
        ((Composite)getControl()).layout();
    }

    private void setProductText(LicenseProduct product, Label label)
    {
        if(_textMap == null)
            _textMap = new HashMap(10);
        String text = (String)_textMap.get(label);
        if(text == null)
        {
            text = label.getText();
            _textMap.put(label, text);
        }
        label.setText(LicenseUtil.replace(text, "${product}", product.getShortName()));
    }

    public void setVisible(boolean visible)
    {
        if(!visible)
            saveContent();
        super.setVisible(visible);
        if(visible)
            getShell().getDisplay().asyncExec(new Runnable() {

                public void run()
                {
                    initContent();
                    updatePageComplete();
                }

            });
    }

    private void initContent()
    {
        _howUsefulFields[_howUseful < 0 ? 2 : _howUseful].setFocus();
        for(int i = 0; i < _howUsefulFields.length; i++)
            _howUsefulFields[i].setSelection(i == _howUseful);

        _moreUsefulField.setText(_moreUseful);
        _purchaseYesField.setSelection(_purchaseYes);
        _purchaseNoField.setSelection(_purchaseNo);
        showWhyNotAndHowMuch(_purchaseNo);
        _whyNotField.setText(_whyNot);
        _purchasePriceField.setText(_purchasePrice);
    }

    private void saveContent()
    {
        _howUseful = getHowUseful();
        _moreUseful = getMoreUseful();
        _purchaseYes = _purchaseYesField.getSelection();
        _purchaseNo = _purchaseNoField.getSelection();
        _whyNot = getWhyNoPurchase();
        _purchasePrice = getPurchasePrice();
    }

    protected void updatePageComplete()
    {
        String errMsg = validateContent();
        setErrorMessage(errMsg == "NOT_COMPLETE" ? null : errMsg);
        setPageComplete(errMsg == null);
    }

    protected String validateContent()
    {
        if(getHowUseful() < 0)
            return "NOT_COMPLETE";
        if(getMoreUseful().length() == 0)
        {
            for(int i = 0; i < _howUsefulFields.length; i++)
                if(_howUsefulFields[i].isFocusControl())
                    return "NOT_COMPLETE";

            return "Please indicate how we can make this product more valuable to you.";
        }
        if(!getWillPurchase())
        {
            if(!_purchaseNoField.getSelection())
                return "NOT_COMPLETE";
            if(getWhyNoPurchase().length() == 0)
                if(_purchaseNoField.isFocusControl())
                    return "NOT_COMPLETE";
                else
                    return "Please indicate why you are not planning to purchase.";
            if(getPurchasePrice().length() == 0)
                if(_whyNotField.isFocusControl())
                    return "NOT_COMPLETE";
                else
                    return "Please indicate how much you would spend to purchase this product.";
        }
        return null;
    }

    public int getHowUseful()
    {
        for(int i = 0; i < _howUsefulFields.length; i++)
            if(_howUsefulFields[i].getSelection())
                return i;

        return -1;
    }

    public String getMoreUseful()
    {
        return _moreUsefulField.getText().trim();
    }

    public boolean getWillPurchase()
    {
        return _purchaseYesField.getSelection();
    }

    public String getWhyNoPurchase()
    {
        return _whyNotField.getText().trim();
    }

    public String getPurchasePrice()
    {
        return _purchasePriceField.getText().trim();
    }






}
