// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   PluginCompatibilityUI.java

package com.instantiations.common.ui;

import com.instantiations.common.core.PluginCompatibility;
import com.swtdesigner.DesignerPlugin;
import com.swtdesigner.ResourceManager;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class PluginCompatibilityUI
{

    private PluginCompatibilityUI()
    {
    }

    public static void warnUserIfNecessary()
    {
        if(PluginCompatibility.isCorrectIDEVersion() || DesignerPlugin.suppressVersionWarnings())
            return;
        if(Display.getCurrent() != null)
            warnUser(PluginCompatibility.getVersionWarningTitle(), PluginCompatibility.getVersionWarningMessage());
        else
            Display.getDefault().asyncExec(new Runnable() {

                public void run()
                {
                    PluginCompatibilityUI.warnUser(PluginCompatibility.getVersionWarningTitle(), PluginCompatibility.getVersionWarningMessage());
                }

            });
    }

    private static void warnUser(String title, String msg)
    {
        IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        org.eclipse.swt.widgets.Shell shell = window == null ? null : window.getShell();
        MessageBox dialog = new MessageBox(shell, 33);
        dialog.setText(title);
        dialog.setMessage(msg);
        dialog.open();
    }

    public static void createWarningLabelIfNecessary(Composite parent, int horzSpan)
    {
        if(PluginCompatibility.isCorrectIDEVersion() || DesignerPlugin.suppressVersionWarnings())
        {
            return;
        } else
        {
            Label wrongVersionLabel = new Label(parent, 0);
            GridData gridData = new GridData(768);
            gridData.horizontalSpan = horzSpan;
            wrongVersionLabel.setLayoutData(gridData);
            wrongVersionLabel.setText(PluginCompatibility.getVersionWarningMessage());
            wrongVersionLabel.setForeground(ResourceManager.getColor(3));
            wrongVersionLabel.setFont(ResourceManager.getFont("", 12, 1));
            return;
        }
    }

}
