// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseWizardPageX.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseUtil;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Text;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardX

public abstract class LicenseWizardPageX extends WizardPage
{

    protected LicenseWizardPageX(String pageName)
    {
        super(pageName);
    }

    protected LicenseWizardX getLicenseWizard()
    {
        return (LicenseWizardX)getWizard();
    }

    protected static String trimActivationKeyOrSerialNumberText(Text field)
    {
        if(field == null || !field.isVisible())
            return null;
        String text = LicenseUtil.trim(field.getText());
        if(text.length() == 0)
            return null;
        String prefix = "Activation Key:";
        if(text.startsWith(prefix))
            text = text.substring(prefix.length()).trim();
        prefix = "Serial Number:";
        if(text.startsWith(prefix))
            text = text.substring(prefix.length()).trim();
        return LicenseUtil.trim(text);
    }
}
