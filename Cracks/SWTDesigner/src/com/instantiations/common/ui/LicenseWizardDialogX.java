// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseWizardDialogX.java

package com.instantiations.common.ui;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardX

public class LicenseWizardDialogX extends WizardDialog
{

    private LicenseWizardX _licenseWizard;

    public LicenseWizardDialogX(Shell parentShell, LicenseWizardX licenseWizard)
    {
        super(parentShell, licenseWizard);
        _licenseWizard = licenseWizard;
    }

    protected void nextPressed()
    {
        IWizardPage currentPage = getCurrentPage();
        if(!_licenseWizard.processNext(currentPage))
            return;
        IWizardPage page = currentPage.getNextPage();
        if(page == null)
        {
            return;
        } else
        {
            showPage(page);
            return;
        }
    }
}
