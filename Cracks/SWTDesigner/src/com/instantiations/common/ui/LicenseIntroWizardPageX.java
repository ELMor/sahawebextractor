// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseIntroWizardPageX.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseManager;
import com.instantiations.common.core.LicenseProduct;
import com.instantiations.common.core.LicenseUtil;
import com.swtdesigner.ResourceManager;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardPageX

public class LicenseIntroWizardPageX extends LicenseWizardPageX
{

    private final Map _productRadioButtonMap = new HashMap();
    private Composite _modeComposite;
    private Label _modeChoiceLabel;
    private Button _proModeRadioButton;
    private Button _freeModeRadioButton;
    private String _modeChoiceText;
    private LicenseProduct _selectedProduct;
    private int _selectedMode;

    public LicenseIntroWizardPageX()
    {
        super("wizardPage");
        _selectedProduct = null;
        setTitle("Product Modes");
    }

    public void createControl(Composite parent)
    {
        boolean canBeFree = false;
        for(int i = 0; i < LicenseManager.PRODUCTS_IN_WIZARD_DISPLAY_ORDER.length; i++)
        {
            if(!LicenseManager.PRODUCTS_IN_WIZARD_DISPLAY_ORDER[i].canBeFree())
                continue;
            canBeFree = true;
            break;
        }

        setDescription("This product has " + (canBeFree ? "three" : "two") + " different modes of operation");
        Composite container = new Composite(parent, 0);
        container.setLayout(new FormLayout());
        CLabel proModeNameLabel = new CLabel(container, 0);
        FormData proModeNameFormData = new FormData();
        proModeNameFormData.top = new FormAttachment(0, 10);
        proModeNameFormData.left = new FormAttachment(0, 15);
        proModeNameLabel.setLayoutData(proModeNameFormData);
        proModeNameLabel.setForeground(Display.getCurrent().getSystemColor(4));
        proModeNameLabel.setFont(ResourceManager.getFont("", 12, 1));
        proModeNameLabel.setText("Professional Paid Mode");
        Label proModeDescriptionLabel = new Label(container, 0);
        FormData proModeDescriptionFormData = new FormData();
        proModeDescriptionFormData.top = new FormAttachment(proModeNameLabel, 5, 1024);
        proModeDescriptionFormData.left = new FormAttachment(0, 30);
        proModeDescriptionLabel.setLayoutData(proModeDescriptionFormData);
        proModeDescriptionLabel.setText("When you purchase the product for use on this machine,\nall of the product's features and functionality become available to you.\nSelect this mode, if you already own a valid license to the product.");
        CLabel evalModeNameLabel = new CLabel(container, 0);
        evalModeNameLabel.setFont(ResourceManager.getFont("", 12, 1));
        evalModeNameLabel.setForeground(Display.getCurrent().getSystemColor(4));
        FormData evalModeNameFormData = new FormData();
        evalModeNameFormData.top = new FormAttachment(proModeDescriptionLabel, 20, 1024);
        evalModeNameFormData.left = new FormAttachment(proModeNameLabel, 0, 16384);
        evalModeNameLabel.setLayoutData(evalModeNameFormData);
        evalModeNameLabel.setText("Professional Evaluation Mode");
        Label evalModeDescriptionLabel = new Label(container, 0);
        FormData evalModeDescriptionFormData = new FormData();
        evalModeDescriptionFormData.top = new FormAttachment(evalModeNameLabel, 5, 1024);
        evalModeDescriptionFormData.left = new FormAttachment(proModeDescriptionLabel, 0, 16384);
        evalModeDescriptionLabel.setLayoutData(evalModeDescriptionFormData);
        evalModeDescriptionLabel.setText("You can evaluate the product for a short period of time during\nwhich all of the product's features and functionality are available to you.");
        CLabel freeModeNameLabel = new CLabel(container, 0);
        freeModeNameLabel.setFont(ResourceManager.getFont("", 12, 1));
        freeModeNameLabel.setForeground(Display.getCurrent().getSystemColor(4));
        FormData freeModeNameFormData = new FormData();
        freeModeNameFormData.top = new FormAttachment(evalModeDescriptionLabel, 20, 1024);
        freeModeNameFormData.left = new FormAttachment(proModeNameLabel, 0, 16384);
        freeModeNameLabel.setLayoutData(freeModeNameFormData);
        freeModeNameLabel.setText("Free Mode");
        Label freeModeDescriptionLabel = new Label(container, 0);
        FormData freeModeDescriptionFormData = new FormData();
        freeModeDescriptionFormData.top = new FormAttachment(freeModeNameLabel, 5, 1024);
        freeModeDescriptionFormData.left = new FormAttachment(proModeDescriptionLabel, 0, 16384);
        freeModeDescriptionLabel.setLayoutData(freeModeDescriptionFormData);
        freeModeDescriptionLabel.setText("If your evaluation period has expired and you choose not to purchase,\nyou can continue to use many but not all of the product's features and functionality.");
        Label spacerLabel = new Label(container, 258);
        FormData spacerFormData = new FormData();
        spacerFormData.right = new FormAttachment(freeModeDescriptionLabel, 0, 0x20000);
        spacerFormData.top = new FormAttachment(freeModeDescriptionLabel, 20, 1024);
        spacerFormData.left = new FormAttachment(freeModeNameLabel, 0, 16384);
        spacerLabel.setLayoutData(spacerFormData);
        Composite topRadioComposite = new Composite(container, 0);
        GridLayout topRadioLayout = new GridLayout();
        topRadioLayout.marginWidth = 0;
        topRadioLayout.marginHeight = 0;
        topRadioLayout.numColumns = 2;
        topRadioComposite.setLayout(topRadioLayout);
        FormData topRadioFormData = new FormData();
        topRadioFormData.bottom = new FormAttachment(100, -5);
        topRadioFormData.right = new FormAttachment(freeModeDescriptionLabel, 0, 0x20000);
        topRadioFormData.top = new FormAttachment(freeModeDescriptionLabel, 40, 1024);
        topRadioFormData.left = new FormAttachment(freeModeNameLabel, 0, 16384);
        topRadioComposite.setLayoutData(topRadioFormData);
        Composite productRadioComposite = new Composite(topRadioComposite, 0);
        GridLayout gridLayout_1 = new GridLayout();
        gridLayout_1.marginWidth = 0;
        gridLayout_1.marginHeight = 0;
        productRadioComposite.setLayout(gridLayout_1);
        productRadioComposite.setLayoutData(new GridData(1026));
        Label productChoiceLabel = new Label(productRadioComposite, 0);
        productChoiceLabel.setLayoutData(new GridData(2));
        productChoiceLabel.setBounds(0, 0, 64, 13);
        productChoiceLabel.setText("I want to use:");
        for(int i = 0; i < LicenseManager.PRODUCTS_IN_WIZARD_DISPLAY_ORDER.length; i++)
            createRadioButton(productRadioComposite, LicenseManager.PRODUCTS_IN_WIZARD_DISPLAY_ORDER[i]);

        _selectedProduct = LicenseManager.getProduct();
        if(_selectedProduct != null)
            ((Button)_productRadioButtonMap.get(_selectedProduct)).setSelection(true);
        _modeComposite = new Composite(topRadioComposite, 0);
        _modeComposite.setLayoutData(new GridData(2));
        GridLayout modeLayout = new GridLayout();
        modeLayout.marginWidth = 0;
        modeLayout.marginHeight = 0;
        _modeComposite.setLayout(modeLayout);
        _modeChoiceLabel = new Label(_modeComposite, 0);
        _modeChoiceLabel.setText("I want to use %product% in this mode:                             ");
        _proModeRadioButton = new Button(_modeComposite, 16);
        GridData proModeGridData = new GridData();
        proModeGridData.horizontalIndent = 20;
        _proModeRadioButton.setLayoutData(proModeGridData);
        _proModeRadioButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        _proModeRadioButton.setText("Professional (paid or evaluation)");
        _proModeRadioButton.setToolTipText("Use all of the features of the product");
        _proModeRadioButton.setSelection(true);
        _freeModeRadioButton = new Button(_modeComposite, 16);
        GridData freeModeGridData = new GridData(2);
        freeModeGridData.horizontalIndent = 20;
        _freeModeRadioButton.setLayoutData(freeModeGridData);
        _freeModeRadioButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                updatePageComplete();
            }

        });
        _freeModeRadioButton.setText("Free (limited functionality)");
        _freeModeRadioButton.setToolTipText("Use the product with limited functionality");
        _freeModeRadioButton.setSelection(false);
        _modeChoiceText = _modeChoiceLabel.getText();
        if(!canBeFree)
        {
            freeModeNameLabel.setVisible(false);
            freeModeDescriptionLabel.setVisible(false);
            _modeComposite.setVisible(false);
        }
        setControl(container);
        updatePageComplete();
    }

    private Button createRadioButton(Composite productRadioComposite, LicenseProduct product)
    {
        return createRadioButton(productRadioComposite, product, product.getFullName(), product.getDescription());
    }

    private Button createRadioButton(Composite productRadioComposite, final LicenseProduct product, String productName, String productDescription)
    {
        Button radioButton = new Button(productRadioComposite, 16);
        GridData gridData = new GridData();
        gridData.horizontalIndent = 20;
        radioButton.setLayoutData(gridData);
        radioButton.addSelectionListener(new SelectionAdapter() {

            public void widgetSelected(SelectionEvent e)
            {
                _selectedProduct = product;
                updatePageComplete();
            }

        });
        radioButton.setText(productName);
        radioButton.setToolTipText(LicenseUtil.replace(productDescription, "\\n", LicenseUtil.LINE_END));
        _productRadioButtonMap.put(product, radioButton);
        _productRadioButtonMap.put(radioButton, product);
        return radioButton;
    }

    private void updatePageComplete()
    {
        if(_selectedProduct != null && !_selectedProduct.canBeFree())
        {
            _modeComposite.setVisible(false);
            _selectedMode = 3;
        } else
        {
            _modeComposite.setVisible(true);
            if(_proModeRadioButton.getSelection())
                _selectedMode = 3;
            else
            if(_freeModeRadioButton.getSelection())
                _selectedMode = 1;
            else
                _selectedMode = 0;
        }
        _modeChoiceLabel.setText(LicenseUtil.replace(_modeChoiceText, "%product%", _selectedProduct == null ? "it" : _selectedProduct.getName()));
        if(_selectedProduct == null)
        {
            setErrorMessage("Please select a product to be registered and activated");
            setPageComplete(false);
        } else
        if(_selectedMode == 0)
        {
            setErrorMessage("Please select a mode in which the product will be used");
            setPageComplete(false);
        } else
        {
            setErrorMessage(null);
            setPageComplete(true);
        }
    }

    public LicenseProduct getProduct()
    {
        return _selectedProduct;
    }

    public int getMode()
    {
        return _selectedMode;
    }


}
