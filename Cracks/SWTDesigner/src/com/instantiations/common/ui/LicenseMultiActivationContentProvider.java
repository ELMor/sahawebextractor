// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseMultiActivationContentProvider.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseRemoteMachine;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;

class LicenseMultiActivationContentProvider
    implements IStructuredContentProvider
{

    private LicenseRemoteMachine _machines[];
    private TableViewer _viewer;

    LicenseMultiActivationContentProvider()
    {
    }

    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {
        _viewer = (TableViewer)viewer;
        _machines = (LicenseRemoteMachine[])newInput;
    }

    public Object[] getElements(Object inputElement)
    {
        return _machines;
    }

    public void dispose()
    {
    }

    public void add(LicenseRemoteMachine addedMachines[])
    {
        int len1 = _machines.length;
        int len2 = addedMachines.length;
        LicenseRemoteMachine newMachines[] = new LicenseRemoteMachine[len1 + len2];
        System.arraycopy(_machines, 0, newMachines, 0, len1);
        System.arraycopy(addedMachines, 0, newMachines, len1, len2);
        _machines = newMachines;
        _viewer.refresh();
    }
}
