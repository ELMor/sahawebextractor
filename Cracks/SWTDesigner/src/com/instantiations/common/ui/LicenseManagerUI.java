// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseManagerUI.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseManager;
import com.swtdesigner.gef.palette.swing.SwingDesignerEditorPalette;
import com.swtdesigner.gef.palette.swt.FieldEditorPreferencePageDesignerEditorPalette;
import com.swtdesigner.gef.palette.swt.FormsSWTDesignerEditorPalette;
import com.swtdesigner.gef.palette.swt.SWTDesignerEditorPalette;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

// Referenced classes of package com.instantiations.common.ui:
//            LicensedPart

public class LicenseManagerUI
{

    private LicenseManagerUI()
    {
    }

    public static void checkLicenseState()
    {
        SWTDesignerEditorPalette.resetCategories();
        FormsSWTDesignerEditorPalette.resetCategories();
        FieldEditorPreferencePageDesignerEditorPalette.resetCategories();
        SwingDesignerEditorPalette.resetCategories();
        checkLicenseState(PlatformUI.getWorkbench().getWorkbenchWindows());
    }

    private static void checkLicenseState(IWorkbenchWindow windows[])
    {
        for(int i = 0; i < windows.length; i++)
            checkLicenseState(windows[i].getPages());

    }

    private static void checkLicenseState(IWorkbenchPage pages[])
    {
        for(int i = 0; i < pages.length; i++)
        {
            IWorkbenchPage page = pages[i];
            checkLicenseState(page, ((IWorkbenchPartReference []) (page.getEditorReferences())));
            checkLicenseState(page, ((IWorkbenchPartReference []) (page.getViewReferences())));
        }

    }

    private static void checkLicenseState(IWorkbenchPage page, IWorkbenchPartReference partRefs[])
    {
        for(int i = 0; i < partRefs.length; i++)
        {
            LicensedPart part = getLicensedPart(partRefs[i]);
            if(part != null)
                part.checkLicenseState();
        }

    }

    public static LicensedPart getLicensedPart(IWorkbenchPartReference partRef)
    {
        String partID = partRef.getId();
        for(int i = 0; i < LicenseManager.LICENSED_PART_IDS.length; i++)
            if(partID.equals(LicenseManager.LICENSED_PART_IDS[i]))
                return getLicensedPart(partRef.getPart(true));

        return null;
    }

    public static LicensedPart getLicensedPart(IWorkbenchPart part)
    {
        if(part instanceof LicensedPart)
            return (LicensedPart)part;
        else
            return null;
    }
}
