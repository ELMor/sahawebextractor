// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   AbstractDetailsDialog.java

package com.instantiations.common.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

// Referenced classes of package com.instantiations.common.ui:
//            DialogImage

public abstract class AbstractDetailsDialog extends Dialog
{

    private final String _title;
    private final String _message;
    private final Image _image;
    private Button _detailsButton;
    private Control _detailsArea;
    private Point _cachedWindowSize;

    public AbstractDetailsDialog(Shell parentShell, String title, DialogImage image, String message)
    {
        this(parentShell, title, image.getImage(), message);
    }

    public AbstractDetailsDialog(Shell parentShell, String title, Image image, String message)
    {
        super(parentShell);
        _title = title;
        _image = image;
        _message = message;
        setShellStyle(0x10870);
    }

    protected void buttonPressed(int id)
    {
        if(id == 13)
            toggleDetailsArea();
        else
            super.buttonPressed(id);
    }

    protected void configureShell(Shell shell)
    {
        super.configureShell(shell);
        if(_title != null)
            shell.setText(_title);
    }

    protected void createButtonsForButtonBar(Composite parent)
    {
        createButton(parent, 0, IDialogConstants.OK_LABEL, false);
        _detailsButton = createButton(parent, 13, IDialogConstants.SHOW_DETAILS_LABEL, false);
    }

    protected Control createDialogArea(Composite parent)
    {
        Composite composite = (Composite)super.createDialogArea(parent);
        composite.setLayoutData(new GridData(768));
        Label label;
        if(_image != null)
        {
            ((GridLayout)composite.getLayout()).numColumns = 2;
            label = new Label(composite, 0);
            _image.setBackground(label.getBackground());
            label.setImage(_image);
            label.setLayoutData(new GridData(66));
        }
        label = new Label(composite, 64);
        if(_message != null)
            label.setText(_message);
        GridData data = new GridData(772);
        data.widthHint = convertHorizontalDLUsToPixels(300);
        label.setLayoutData(data);
        label.setFont(parent.getFont());
        return composite;
    }

    protected void toggleDetailsArea()
    {
        Point oldWindowSize = getShell().getSize();
        Point newWindowSize = _cachedWindowSize;
        _cachedWindowSize = oldWindowSize;
        if(_detailsArea == null)
        {
            _detailsArea = createDetailsArea((Composite)getContents());
            _detailsButton.setText(IDialogConstants.HIDE_DETAILS_LABEL);
        } else
        {
            _detailsArea.dispose();
            _detailsArea = null;
            _detailsButton.setText(IDialogConstants.SHOW_DETAILS_LABEL);
        }
        Point oldSize = getContents().getSize();
        Point newSize = getContents().computeSize(-1, -1);
        if(newWindowSize == null)
            newWindowSize = new Point(oldWindowSize.x, oldWindowSize.y + (newSize.y - oldSize.y));
        Point windowLoc = getShell().getLocation();
        Rectangle screenArea = getContents().getDisplay().getClientArea();
        if(newWindowSize.y > screenArea.height - (windowLoc.y - screenArea.y))
            newWindowSize.y = screenArea.height - (windowLoc.y - screenArea.y);
        getShell().setSize(newWindowSize);
        ((Composite)getContents()).layout();
    }

    protected abstract Control createDetailsArea(Composite composite);
}
