// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseEmbeddedPanel.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseManager;
import com.instantiations.common.core.LicenseProduct;
import com.instantiations.common.core.LicenseUtil;
import com.swtdesigner.ResourceManager;
import org.eclipse.jface.action.IAction;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardActionX, OpenBrowserAction

public class LicenseEmbeddedPanel extends Composite
{

    private static final String PURCHASE_TEXT_LONG = "Buy It Now!";
    private static final String ACTIVATE_TEXT_LONG = "Activate License...";
    private static final Color DARK_RED = Display.getCurrent().getSystemColor(4);
    private static final Color DARK_BLUE = Display.getCurrent().getSystemColor(10);
    private Label _licenseLabel;
    private Button _activateButton;
    private Button _purchaseButton;
    private IAction _activateAction;
    private IAction _purchaseAction;
    private String _activateText;
    private String _purchaseText;

    public LicenseEmbeddedPanel(Composite parent, int style)
    {
        super(parent, style);
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 3;
        gridLayout.marginHeight = 0;
        gridLayout.marginWidth = 0;
        gridLayout.horizontalSpacing = 0;
        setLayout(gridLayout);
        _licenseLabel = new Label(this, 0);
        _licenseLabel.setLayoutData(new GridData(768));
        _licenseLabel.setText(" ");
        _licenseLabel.setFont(ResourceManager.getFont("", 12, 1));
        _licenseLabel.setForeground(DARK_RED);
        _activateButton = new Button(this, 0);
        _activateButton.setText("Activate License...");
        _activateAction = new LicenseWizardActionX(_activateButton.getShell());
        _activateButton.addSelectionListener(new SelectionListener() {

            public void widgetSelected(SelectionEvent e)
            {
                _activateAction.run();
            }

            public void widgetDefaultSelected(SelectionEvent e)
            {
                widgetSelected(e);
            }

        });
        _purchaseButton = new Button(this, 0);
        _purchaseButton.setText("Buy It Now!");
        _purchaseAction = new OpenBrowserAction("BuyItNow", LicenseManager.getProduct().getBuyItNowURL());
        _purchaseButton.addSelectionListener(new SelectionListener() {

            public void widgetSelected(SelectionEvent e)
            {
                _purchaseAction.run();
            }

            public void widgetDefaultSelected(SelectionEvent e)
            {
                widgetSelected(e);
            }

        });
        hookResize();
    }

    private void hookResize()
    {
        _activateText = _activateButton.getText();
        _purchaseText = _purchaseButton.getText();
        addControlListener(new ControlListener() {

            public void controlMoved(ControlEvent controlevent)
            {
            }

            public void controlResized(ControlEvent e)
            {
                boolean changed = false;
                if(updateButtonText(_activateButton, _activateText))
                    changed = true;
                if(updateButtonText(_purchaseButton, _purchaseText))
                    changed = true;
                if(!changed)
                {
                    return;
                } else
                {
                    _activateButton.pack();
                    _purchaseButton.pack();
                    layout();
                    return;
                }
            }

        });
    }

    public void setMessageForegroundColor(Color color)
    {
        _licenseLabel.setForeground(color);
    }

    public void setMessage(String message)
    {
        if(_licenseLabel != null && !_licenseLabel.isDisposed())
            _licenseLabel.setText(message);
    }

    public void setActivateButtonText(String text)
    {
        _activateText = text;
        if(!updateButtonText(_activateButton, text))
        {
            return;
        } else
        {
            _activateButton.pack();
            layout();
            return;
        }
    }

    public void setPurchaseButtonText(String text)
    {
        _purchaseText = text;
        if(!updateButtonText(_purchaseButton, text))
        {
            return;
        } else
        {
            _purchaseButton.pack();
            layout();
            return;
        }
    }

    private String getTextToDisplay(String text)
    {
        if(getBounds().width > 450)
            return text;
        int len = text.indexOf(' ');
        if(len == -1)
            len = text.length();
        return text.substring(0, len);
    }

    private boolean updateButtonText(Button button, String text)
    {
        String textToDisplay = getTextToDisplay(text);
        if(button.isDisposed() || textToDisplay.equals(button.getText()))
            return false;
        button.setText(textToDisplay);
        if(text.equals(textToDisplay))
            button.setToolTipText(null);
        else
            button.setToolTipText(text);
        return true;
    }

    public void updateMessage(LicenseProduct products[])
    {
        updateMessage(LicenseManager.getProduct(products));
    }

    public void updateMessage(LicenseProduct product)
    {
        if(isDisposed())
            return;
        if(product == null)
        {
            setMessage(" ");
            return;
        }
        if(product.isProMode())
        {
            Composite parent = getParent();
            dispose();
            parent.layout();
            return;
        }
        int days = Math.max(0, product.getDaysUntilExpiration());
        if(days > 0)
            setMessageForegroundColor(DARK_BLUE);
        else
            setMessageForegroundColor(DARK_RED);
        if(product.isEvalMode())
        {
            setMessage(product.getShortProName() + " Evaluation - " + days + " Days Remaining");
            setActivateButtonText("Activate License...");
            return;
        }
        String label = product.getShortName();
        if(product.isFreeMode())
            label = label + " Free Edition";
        if(days == 0)
            label = label + " - Activation Required";
        else
        if(days < 15)
            label = label + " - " + days + " Days Until Activation Required";
        setMessage(label);
        if(days >= 15)
            setActivateButtonText("Evaluate " + product.getShortProName() + "...");
        else
            setActivateButtonText("Activate License...");
    }

    public static Composite createParent(Composite parent)
    {
        Composite subParent = new Composite(parent, 0);
        GridLayout layout = new GridLayout();
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.numColumns = 1;
        subParent.setLayout(layout);
        return subParent;
    }

    public static boolean createIfNecessaryX(Composite parent, int style, LicenseProduct products[])
    {
        createIfNecessary(parent, style, products);
        return LicenseUtil.isActivated(products);
    }

    public static LicenseEmbeddedPanel createIfNecessary(Composite parent, int style, LicenseProduct products[])
    {
        if(LicenseUtil.isProMode(products))
            return null;
        else
            return create(parent, style, products);
    }

    public static LicenseEmbeddedPanel create(Composite parent, int style, LicenseProduct products[])
    {
        LicenseEmbeddedPanel panel = new LicenseEmbeddedPanel(parent, style);
        panel.setLayoutData(new GridData(768));
        panel.updateMessage(products);
        return panel;
    }

    public static void disableAllIfNecessary(Composite parent, LicenseProduct products[])
    {
        if(!LicenseUtil.isActivated(products))
            disableAll(parent.getChildren());
    }

    private static void disableAll(Control controls[])
    {
        for(int i = 0; i < controls.length; i++)
        {
            controls[i].setEnabled(false);
            if(controls[i] instanceof Composite)
                disableAll(((Composite)controls[i]).getChildren());
        }

    }

    public void disposeButtons()
    {
        _activateButton.dispose();
        _purchaseButton.dispose();
    }








}
