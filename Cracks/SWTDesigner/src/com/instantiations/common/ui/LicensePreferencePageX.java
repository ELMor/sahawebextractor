// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicensePreferencePageX.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseFile;
import com.instantiations.common.core.LicenseHardwareAddress;
import com.instantiations.common.core.LicenseManager;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;

// Referenced classes of package com.instantiations.common.ui:
//            LicenseWizardActionX

public abstract class LicensePreferencePageX extends PreferencePage
{

    private static final int UPDATE_DAYS[] = {
        7, 14, 21, 30, 60, 90
    };
    private static final String UPDATE_TEXT[] = {
        "week", "2 weeks", "3 weeks", "month", "2 months", "3 months"
    };
    private Text _licenseText;
    private Button _autoActivateCheckbox;
    private Button _updateCheckbox;
    private Combo _updateCombo;

    public LicensePreferencePageX()
    {
    }

    protected Control createContents(Composite parent)
    {
        LicenseFile licenseFile = LicenseFile.getInstance();
        String localAddress = ":";
        String localAddresses[] = LicenseHardwareAddress.getLocalAddresses();
        if(localAddresses.length > 0)
            localAddress = " [" + localAddresses[0] + "]:";
        Composite container = new Composite(parent, 0);
        container.setLayout(new FormLayout());
        Label label = new Label(container, 0);
        FormData formData_2 = new FormData();
        formData_2.top = new FormAttachment(0, 5);
        formData_2.left = new FormAttachment(0, 5);
        label.setLayoutData(formData_2);
        label.setText("Current License" + localAddress);
        _licenseText = new Text(container, 10);
        _licenseText.setText("None");
        FormData formData_3 = new FormData();
        formData_3.bottom = new FormAttachment(0, 180);
        formData_3.right = new FormAttachment(100, -5);
        formData_3.top = new FormAttachment(label, 5, 1024);
        formData_3.left = new FormAttachment(0, 25);
        _licenseText.setLayoutData(formData_3);
        _licenseText.setText(LicenseManager.getLicenseDescription());
        Button wizardButton = new Button(container, 0);
        wizardButton.setToolTipText("Open the Registration and Activation Wizard");
        FormData formData = new FormData();
        formData.left = new FormAttachment(label, 0, 16384);
        formData.top = new FormAttachment(_licenseText, 5, 1024);
        wizardButton.setLayoutData(formData);
        wizardButton.setText("Registration and Activation");
        final IAction wizardAction = new LicenseWizardActionX(getShell());
        wizardButton.addSelectionListener(new SelectionListener() {

            public void widgetSelected(SelectionEvent e)
            {
                wizardAction.run();
                _licenseText.setText(LicenseManager.getLicenseDescription());
            }

            public void widgetDefaultSelected(SelectionEvent e)
            {
                widgetSelected(e);
            }

        });
        Label separator = new Label(container, 258);
        formData = new FormData();
        formData.right = new FormAttachment(100, -5);
        formData.top = new FormAttachment(wizardButton, 15, -1);
        formData.left = new FormAttachment(0, 5);
        separator.setLayoutData(formData);
        Composite updateComposite = new Composite(container, 0);
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 2;
        updateComposite.setLayout(gridLayout);
        formData = new FormData();
        formData.top = new FormAttachment(separator, 5, 1024);
        formData.left = new FormAttachment(separator, 0, 16384);
        updateComposite.setLayoutData(formData);
        _autoActivateCheckbox = new Button(updateComposite, 32);
        GridData gridData = new GridData();
        gridData.horizontalSpan = 2;
        _autoActivateCheckbox.setLayoutData(gridData);
        _autoActivateCheckbox.setText("Automatically activate when possible");
        _autoActivateCheckbox.setSelection(licenseFile.getBooleanValue(LicenseFile.AUTO_ACTIVATE_KEY, true));
        _updateCheckbox = new Button(updateComposite, 32);
        _updateCheckbox.setText("Automatically check for updates every");
        _updateCheckbox.setSelection(licenseFile.getBooleanValue(LicenseFile.UPDATE_CHECK_KEY, true));
        _updateCombo = new Combo(updateComposite, 0);
        int updateDays = licenseFile.getIntValue(LicenseFile.UPDATE_DAYS_KEY, 14);
        for(int i = 0; i < UPDATE_TEXT.length; i++)
            _updateCombo.add(UPDATE_TEXT[i]);

        setUpdateComboSelection(updateDays);
        _updateCombo.addSelectionListener(new SelectionListener() {

            public void widgetSelected(SelectionEvent e)
            {
                _updateCheckbox.setSelection(true);
            }

            public void widgetDefaultSelected(SelectionEvent e)
            {
                widgetSelected(e);
            }

        });
        return container;
    }

    public void init(IWorkbench iworkbench)
    {
    }

    protected void performDefaults()
    {
        _autoActivateCheckbox.setSelection(true);
        _updateCheckbox.setSelection(true);
        setUpdateComboSelection(14);
        super.performDefaults();
    }

    public boolean performOk()
    {
        boolean changed = false;
        LicenseFile licenseFile = LicenseFile.getInstance();
        if(licenseFile.putValue(LicenseFile.AUTO_ACTIVATE_KEY, _autoActivateCheckbox.getSelection()))
            changed = true;
        if(licenseFile.putValue(LicenseFile.UPDATE_CHECK_KEY, _updateCheckbox.getSelection()))
            changed = true;
        int index = _updateCombo.getSelectionIndex();
        if(index >= 0 && index < UPDATE_DAYS.length && licenseFile.putValue(LicenseFile.UPDATE_DAYS_KEY, UPDATE_DAYS[index]))
            changed = true;
        if(changed)
            licenseFile.saveContent();
        return super.performOk();
    }

    private void setUpdateComboSelection(int updateDays)
    {
        for(int i = 0; i < UPDATE_TEXT.length; i++)
            if(updateDays == UPDATE_DAYS[i])
                _updateCombo.select(i);

    }



}
