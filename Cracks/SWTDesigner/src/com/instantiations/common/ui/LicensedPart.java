// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicensedPart.java

package com.instantiations.common.ui;

import com.instantiations.common.core.LicenseProduct;
import org.eclipse.ui.IWorkbenchPart;

public interface LicensedPart
    extends IWorkbenchPart
{

    public abstract LicenseProduct getLicenseProduct();

    public abstract void checkLicenseState();

    public abstract void showLicenseActivationRequired();
}
