// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseUtil.java

package com.instantiations.common.core;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;

// Referenced classes of package com.instantiations.common.core:
//            ProductLog, LicenseProduct

public class LicenseUtil
{

    private static final String ENCODED_CHARS = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ";
    private static final int ENCODED_OFFSET = 7;
    private static final int ENCODED_BASE;
    private static final int ENCODED_MAX;
    private static final int ENCODED_MAX_ASCII = 127;
    private static final int ENCODED_INDEX_OF_ZERO = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ".indexOf('0');
    private static final int ENCODED_INDEX_OF_ONE = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ".indexOf('1');
    private static final int ENCODED_INT_LEN = 7;
    private static final int ENCODED_CHK_LEN = 8;
    private static final int ENCODED_CHK_RANDOM_CHAR_INDEX = 5;
    private static Random random = null;
    private static final int CRCTable[];
    private static boolean crc32ExceptionLogged = false;
    public static final String UTF_8_ENCODING = decode("CU9STQAM");
    public static final String LINE_END = System.getProperty("line.separator");

    private LicenseUtil()
    {
    }

    public static String encode(String original)
    {
        return encode0(original, ENCODED_MAX, true);
    }

    public static String encodeAscii(String original)
    {
        return encode0(original, 127, false);
    }

    private static String encode0(String original, int max, boolean allChar)
    {
        if(original == null)
            return null;
        StringBuffer buf = new StringBuffer();
        for(int i = 0; i < original.length(); i++)
        {
            char ch = original.charAt(i);
            if(ch < max)
            {
                buf.append(encode(ch % ENCODED_BASE, buf.length()));
                buf.append(encode(ch / ENCODED_BASE, buf.length()));
            } else
            if(allChar)
                buf.append(ch);
        }

        return buf.toString();
    }

    private static char encode(int value, int offset)
    {
        int index;
        for(index = value - (7 + offset); index < 0; index += ENCODED_BASE);
        return "0123456789ABCDEFGHJKMNPQRSTUVWXYZ".charAt(index);
    }

    private static int decode(char ch, int offset)
    {
        int index = decode(ch);
        if(index == -1)
            return -1;
        else
            return (index + (7 + offset)) % ENCODED_BASE;
    }

    private static int decode(char ch)
    {
        switch(ch)
        {
        case 79: // 'O'
        case 111: // 'o'
            return ENCODED_INDEX_OF_ZERO;

        case 73: // 'I'
        case 105: // 'i'
        case 108: // 'l'
        case 304: 
        case 305: 
            return ENCODED_INDEX_OF_ONE;
        }
        return "0123456789ABCDEFGHJKMNPQRSTUVWXYZ".indexOf(Character.toUpperCase(ch));
    }

    public static String decode(String original)
    {
        return decode0(original, ENCODED_MAX, true);
    }

    public static String decodeAscii(String original)
    {
        return decode0(original, 127, false);
    }

    private static String decode0(String original, int max, boolean allChar)
    {
        if(original == null)
            return null;
        int offset = 0;
        StringBuffer buf = new StringBuffer();
        for(int i = 0; i < original.length(); i++)
        {
            char ch = original.charAt(i);
            if(ch < max)
            {
                int i0 = decode(ch, offset);
                if(i0 == -1)
                    continue;
                offset++;
                while(++i < original.length()) 
                {
                    ch = original.charAt(i);
                    int i1 = decode(ch, offset);
                    if(i1 != -1)
                    {
                        offset++;
                        ch = (char)(i1 * ENCODED_BASE + i0);
                        break;
                    }
                }
            } else
            {
                offset++;
            }
            if(allChar || ch < max)
                buf.append(ch);
        }

        return buf.toString();
    }

    public static String stripNonEncoded(String original)
    {
        StringBuffer buf = new StringBuffer(original.length());
        for(int i = 0; i < original.length(); i++)
        {
            char ch = original.charAt(i);
            if(ch < ENCODED_MAX)
            {
                if(decode(ch) != -1)
                    buf.append(ch);
            } else
            {
                buf.append(ch);
            }
        }

        return buf.toString();
    }

    public static String encodeInt(int original)
    {
        return encodeInt(original, 7);
    }

    public static String encodeInt(int original, int len)
    {
        long value = original;
        if(value < 0L)
            value += 0x100000000L;
        char chars[] = new char[len];
        for(int i = len; --i >= 0;)
        {
            chars[i] = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ".charAt((int)(value % (long)ENCODED_BASE));
            value /= ENCODED_BASE;
        }

        return new String(chars);
    }

    public static int decodeInt(String original)
    {
        long value = 0L;
        for(int i = 0; i < original.length(); i++)
        {
            int digit = decode(original.charAt(i));
            if(digit != -1)
                value = value * (long)ENCODED_BASE + (long)digit;
        }

        if(value > 0x7fffffffL)
            value -= 0x100000000L;
        return (int)value;
    }

    public static String encodedCrc32(String buffer)
    {
        return encodedChk0(crc32(buffer));
    }

    public static String encodedChksumAscii(String buffer)
    {
        return encodedChk0(chksumAscii(buffer));
    }

    private static String encodedChk0(int chkValue)
    {
        String result = encodeInt(chkValue);
        StringBuffer buf = new StringBuffer(8);
        buf.append(result.substring(0, 5));
        if(random == null)
            random = new Random();
        buf.append("0123456789ABCDEFGHJKMNPQRSTUVWXYZ".charAt(random.nextInt(20) + 11));
        buf.append(result.substring(5));
        return buf.toString();
    }

    public static String stripChk(String original)
    {
        if(original.length() < 8)
            return null;
        else
            return original.substring(original.length() - 8);
    }

    public static int decodeChk(String encodedChk)
    {
        return decodeInt(encodedChk.substring(0, 5) + encodedChk.substring(6));
    }

    public static boolean validateCrc32(String buffer, String encodedCrc32)
    {
        return crc32(buffer) == decodeChk(encodedCrc32);
    }

    public static boolean validateChksumAscii(String buffer, String encodedChksum)
    {
        return chksumAscii(buffer) == decodeChk(encodedChksum);
    }

    public static int crc32(String buffer)
    {
        return crc32(buffer, -1);
    }

    public static int crc32(String buffer, int crc)
    {
        byte bytes[];
        try
        {
            bytes = buffer.getBytes(UTF_8_ENCODING);
        }
        catch(UnsupportedEncodingException e)
        {
            if(!crc32ExceptionLogged)
            {
                crc32ExceptionLogged = true;
                ProductLog.logError(e);
            }
            bytes = buffer.getBytes();
        }
        return crc32(bytes, 0, bytes.length, crc);
    }

    public static int crc32(byte buffer[], int start, int count, int lastcrc)
    {
        int i = start;
        int temp1;
        int temp2;
        int crc;
        for(crc = lastcrc; count-- != 0; crc = temp1 ^ temp2)
        {
            temp1 = crc >>> 8;
            temp2 = CRCTable[(crc ^ buffer[i++]) & 0xff];
        }

        return crc;
    }

    public static String toUpperCase(String str)
    {
        return str.toUpperCase().replace('\u0130', 'I');
    }

    public static String toLowerCase(String str)
    {
        return str.toLowerCase().replace('\u0131', 'i');
    }

    public static boolean readKeyValuePairs(LineNumberReader reader, Map map)
    {
        try
        {
            while(true) 
            {
                String line = reader.readLine();
                if(line == null)
                    return true;
                line = line.trim();
                if(line.length() == 0)
                    return true;
                int index = line.indexOf('=');
                if(index == -1)
                    return false;
                String key = line.substring(0, index).trim();
                String value = line.substring(index + 1).trim();
                map.put(key, value);
            }
        }
        catch(IOException e)
        {
            ProductLog.logError(e);
        }
        return false;
    }

    public static void writeKeyValuePairs(PrintWriter writer, Map map)
    {
        for(Iterator iter = map.entrySet().iterator(); iter.hasNext(); writer.println())
        {
            java.util.Map.Entry entry = (java.util.Map.Entry)iter.next();
            writer.print(entry.getKey());
            writer.print('=');
            writer.print(entry.getValue());
        }

    }

    public static String urlEncode(String string)
    {
        try
        {
            return URLEncoder.encode(normalizeNewline(string), UTF_8_ENCODING);
        }
        catch(NoSuchMethodError err)
        {
            return URLEncoder.encode(normalizeNewline(string));
        }
        catch(Exception e)
        {
            ProductLog.logError("UnsupportedEncodingException: " + string, e);
        }
        return string;
    }

    public static String normalizeNewline(String string)
    {
        String normalized = string;
        int index = 0;
        do
        {
            index = string.indexOf('\n', index);
            if(index != -1)
            {
                if(index == 0 || string.charAt(index - 1) != '\r')
                    normalized = normalized.substring(0, index) + "\r" + normalized.substring(index);
                index++;
            } else
            {
                return normalized;
            }
        } while(true);
    }

    public static String urlDecode(String string)
    {
        try
        {
            return URLDecoder.decode(string, UTF_8_ENCODING);
        }
        catch(NoSuchMethodError err)
        {
            return URLDecoder.decode(string);
        }
        catch(Exception e)
        {
            ProductLog.logError("UnsupportedEncodingException: " + string, e);
        }
        return string;
    }

    public static int chksumAscii(String data)
    {
        int result = 0;
        for(int i = 0; i < data.length(); i++)
        {
            char ch = data.charAt(i);
            if(ch < '\177')
            {
                if(result > 0x3fffffff)
                    result = (result - 0x40000000) * 2 + 1;
                else
                    result *= 2;
                if(result > 0x7fffffff - ch)
                    result -= 0x40000000;
                result += ch;
            }
        }

        return result;
    }

    public static GregorianCalendar newCalendar(int dateOffset)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(5, dateOffset);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar;
    }

    public static String replace(String text, String tag, String value)
    {
        if(text == null)
            return null;
        StringBuffer buf = new StringBuffer(text.length());
        int start = 0;
        for(int end = 0; (end = text.indexOf(tag, start)) != -1;)
        {
            buf.append(text.substring(start, end)).append(value);
            start = end + tag.length();
        }

        buf.append(text.substring(start));
        return buf.toString();
    }

    public static int extractInt(String text)
    {
        int start;
        for(start = 0; start < text.length() && !Character.isDigit(text.charAt(start)); start++);
        int end;
        for(end = start; end < text.length() && Character.isDigit(text.charAt(end)); end++);
        if(start >= end)
            return -1;
        try
        {
            return Integer.parseInt(text.substring(start, end));
        }
        catch(NumberFormatException e)
        {
            return -1;
        }
    }

    public static String trim(String text)
    {
        if(text == null)
            return null;
        int start = 0;
        int end;
        for(end = text.length() - 1; start <= end; start++)
        {
            char ch = text.charAt(start);
            if(!Character.isWhitespace(ch) && ch != '_')
                break;
        }

        for(; start <= end; end--)
        {
            char ch = text.charAt(end);
            if(!Character.isWhitespace(ch) && ch != '_')
                break;
        }

        return text.substring(start, end + 1);
    }

    public static boolean isProMode(LicenseProduct products[])
    {
        for(int i = 0; i < products.length; i++)
            if(products[i].isProMode())
                return true;

        return false;
    }

    public static boolean isProOrEvalMode(LicenseProduct products[])
    {
        for(int i = 0; i < products.length; i++)
            if(products[i].isProOrEvalMode())
                return true;

        return false;
    }

    public static boolean isActivated(LicenseProduct products[])
    {
        for(int i = 0; i < products.length; i++)
            if(products[i].isActivated())
                return true;

        return false;
    }

    static 
    {
        ENCODED_BASE = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ".length();
        ENCODED_MAX = ENCODED_BASE * ENCODED_BASE - 1;
        int CRC32_POLYNOMIAL = 0xedb88320;
        CRCTable = new int[256];
        for(int i = 0; i <= 255; i++)
        {
            int crc = i;
            for(int j = 8; j > 0; j--)
                if((crc & 1) == 1)
                    crc = crc >>> 1 ^ 0xedb88320;
                else
                    crc >>>= 1;

            CRCTable[i] = crc;
        }

    }
}
