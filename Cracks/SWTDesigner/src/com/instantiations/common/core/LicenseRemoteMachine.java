// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseRemoteMachine.java

package com.instantiations.common.core;


// Referenced classes of package com.instantiations.common.core:
//            LicenseHardwareAddress

public class LicenseRemoteMachine
{

    private final String name;
    private final String remark;
    private String address;

    public LicenseRemoteMachine(String name, String remark)
    {
        this(name, remark, null);
    }

    public LicenseRemoteMachine(String name, String remark, String address)
    {
        this.name = name;
        this.remark = remark;
        this.address = address;
    }

    public String getName()
    {
        return name;
    }

    public String getRemark()
    {
        return remark;
    }

    public String getAddress()
    {
        loadAddress();
        return address;
    }

    public void loadAddress()
    {
        if(address == null)
            address = LicenseHardwareAddress.getRemoteAddress(name);
    }
}
