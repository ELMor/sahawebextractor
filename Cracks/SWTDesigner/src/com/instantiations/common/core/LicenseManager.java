// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseManager.java

package com.instantiations.common.core;

import com.swtdesigner.DesignerPlugin;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;

// Referenced classes of package com.instantiations.common.core:
//            LicenseUtil, LicenseInfo, LicenseProduct, LicenseFile, 
//            LicenseActivationKey, LicenseSerialNumber, LicenseActivationTransaction, ProductLog

public class LicenseManager
{

    public static final String DESIGNER_NAME = LicenseUtil.decode("VUTT5RTPPMUJGGUE");
    public static final String DESIGNER_PLUGIN_ID = LicenseUtil.decode("TV3TZR0M1M3JYGDECCRAC886D422D0");
    public static final String LICENSED_PART_IDS[] = {
        "com.swtdesigner.editors.MultiPageEditor"
    };
    public static final String REGISTRATION_EMAIL_ADDR = "registration@swt-designer.com";
    public static final String DEFAULT_SUPPORT_EMAIL_ADDRESS = LicenseUtil.decode("DVQS1P3P3MWJUGRESCSA46A6D4G2F0UX5W9UWSKP3NQKUHRFUDP979H7D5");
    public static final String DEFAULT_INFO_EMAIL_ADDRESS = LicenseUtil.decode("ZV2TSRZPGJPJSGVEUC69H8N684Y1F02Y6W3U6S1NKNWKSH");
    public static final String DEFAULT_SALES_EMAIL_ADDRESS = LicenseUtil.decode("9VPSYRPP1MEGMGQETCSA47F6K462WZDY0W4U1S4QZKHKUHQF");
    public static final String WB_FREE_PRODUCT_ID = LicenseUtil.decode("EUXT0RNPXM3JEFXEGCHA7866H441D0YYWW");
    public static final String WB_EVAL_PRODUCT_ID = LicenseUtil.decode("EUXT0RNPXM3JEFXEGCHA7866H431H0UX3W");
    public static final String WB_PRO_PRODUCT_ID = LicenseUtil.decode("EUXT0RNPXM3JEFXEGCHA7866H4E1D08Y");
    public static final LicenseProduct WB_PRODUCT;
    public static final String SWT_FREE_PRODUCT_ID = LicenseUtil.decode("AUCS7QPNMMZJMGGENCAAN885H42200");
    public static final String SWT_EVAL_PRODUCT_ID = LicenseUtil.decode("AUCS7QPNMMZJMGGENCAAN875N4Y170");
    public static final String SWT_PRO_PRODUCT_ID = LicenseUtil.decode("AUCS7QPNMMZJMGGENCAAN8J5H4C2");
    public static final LicenseProduct SWT_PRODUCT;
    public static final String SWING_FREE_PRODUCT_ID = LicenseUtil.decode("AUBTVRYPPMJHGGVEGCCAH866H441D0YYWW");
    public static final String SWING_EVAL_PRODUCT_ID = LicenseUtil.decode("AUBTVRYPPMJHGGVEGCCAH866H431H0UX3W");
    public static final String SWING_PRO_PRODUCT_ID = LicenseUtil.decode("AUBTVRYPPMJHGGVEGCCAH866H4E1D08Y");
    public static final LicenseProduct SWING_PRODUCT;
    public static final LicenseProduct DEFAULT_PRODUCT;
    public static final LicenseProduct PRODUCTS_IN_WIZARD_DISPLAY_ORDER[];
    public static final LicenseProduct PRODUCTS_IN_VALIDATE_ORDER[];
    public static final Collection PRODUCT_IDS = Arrays.asList(new String[] {
        SWT_FREE_PRODUCT_ID, 
        SWT_EVAL_PRODUCT_ID, 
        SWT_PRO_PRODUCT_ID, 
        SWING_FREE_PRODUCT_ID, 
        SWING_EVAL_PRODUCT_ID, 
        SWING_PRO_PRODUCT_ID, 
        WB_FREE_PRODUCT_ID, 
        WB_EVAL_PRODUCT_ID, 
        WB_PRO_PRODUCT_ID
    });
    public static String _productBuildDateKey = null;
    public static boolean _autoActivated = false;

    public LicenseManager()
    {
    }

    public static void initialize()
    {
    }

    public static boolean isValid(LicenseActivationKey activationKey)
    {
        return validate(activationKey) == null;
    }

    public static boolean wasValid(LicenseActivationKey activationKey)
    {
        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
            if(PRODUCTS_IN_VALIDATE_ORDER[i].wasValid(activationKey))
                return true;

        return false;
    }

    public static String validate(LicenseActivationKey activationKey)
    {
        if(activationKey == null)
            return "missing";
        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
        {
            String result = PRODUCTS_IN_VALIDATE_ORDER[i].validate(activationKey);
            if(!"invalid".equals(result))
                return result;
        }

        return "invalid";
    }

    public static boolean isValid(LicenseSerialNumber serialNumber)
    {
        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
            if(PRODUCTS_IN_VALIDATE_ORDER[i].isValid(serialNumber))
                return true;

        return false;
    }

    public static LicenseActivationKey getPriorActivationKey()
    {
        LicenseFile licenseFile = LicenseFile.getInstance();
        LicenseActivationKey activationKeys[] = licenseFile.getActivationKeys();
        for(int i = activationKeys.length; --i >= 0;)
        {
            LicenseActivationKey eachKey = activationKeys[i];
            if(PRODUCT_IDS.contains(eachKey.getProductId()))
                return eachKey;
        }

        return null;
    }

    public static LicenseActivationKey getLastActivationKey()
    {
        LicenseFile licenseFile = LicenseFile.getInstance();
        LicenseActivationKey activationKeys[] = licenseFile.getActivationKeys();
        if(activationKeys.length > 0)
            return activationKeys[activationKeys.length - 1];
        else
            return null;
    }

    public static String getLicenseDescription()
    {
        StringBuffer buf = new StringBuffer(400);
        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
            PRODUCTS_IN_VALIDATE_ORDER[i].appendLicenseDescription(buf);

        String description = buf.toString().trim();
        if(description.length() > 0)
            return description;
        else
            return "none";
    }

    public static boolean isUnactivatedOrExpired()
    {
        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
            if(PRODUCTS_IN_VALIDATE_ORDER[i].getMode() != 0)
                return false;

        return true;
    }

    public static String getProductNameAndMode()
    {
        StringBuffer buf = new StringBuffer(60);
        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
        {
            LicenseProduct each = PRODUCTS_IN_VALIDATE_ORDER[i];
            LicenseProduct parent = each.getParent();
            if(parent == null || !parent.isProOrEvalMode())
            {
                String shortName = null;
                if(each.isProOrEvalMode())
                {
                    shortName = each.getShortName();
                    if(shortName.indexOf("Pro") == -1)
                        shortName = shortName + " Pro";
                } else
                if(each.isFreeMode())
                    shortName = each.getShortName() + " Free";
                if(shortName != null)
                {
                    if(buf.length() > 0)
                        buf.append(" and ");
                    buf.append(shortName);
                }
            }
        }

        if(buf.length() > 0)
            return buf.toString();
        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
        {
            LicenseProduct each = PRODUCTS_IN_VALIDATE_ORDER[i];
            LicenseActivationKey key = each.getPriorActivationKey();
            if(key != null && each.isProProductId(key.getProductId()))
            {
                String shortName = each.getShortName();
                if(shortName.indexOf("Pro") == -1)
                    shortName = shortName + " Pro";
                return shortName + " (support expired)";
            }
        }

        for(int i = 0; i < PRODUCTS_IN_VALIDATE_ORDER.length; i++)
        {
            LicenseProduct each = PRODUCTS_IN_VALIDATE_ORDER[i];
            LicenseActivationKey key = each.getPriorActivationKey();
            if(key != null)
                return each.getShortName() + " (activation expired)";
        }

        return DEFAULT_PRODUCT.getShortName() + " (not activated)";
    }

    public static LicenseProduct getProduct()
    {
        return getProduct(PRODUCTS_IN_VALIDATE_ORDER);
    }

    public static LicenseProduct getProduct(LicenseProduct products[])
    {
        for(int i = 0; i < products.length; i++)
        {
            LicenseProduct each = products[i];
            if(each.isProOrEvalMode())
                return each;
        }

        for(int i = 0; i < products.length; i++)
        {
            LicenseProduct each = products[i];
            if(each.isFreeMode())
                return each;
        }

        for(int i = 0; i < products.length; i++)
        {
            LicenseProduct each = products[i];
            LicenseActivationKey key = each.getPriorActivationKey();
            if(key != null && each.isProProductId(key.getProductId()))
                return each;
        }

        for(int i = 0; i < products.length; i++)
        {
            LicenseProduct each = products[i];
            if(each.wasProMode())
                return each;
        }

        for(int i = 0; i < products.length; i++)
        {
            LicenseProduct each = products[i];
            LicenseActivationKey key = each.getPriorActivationKey();
            if(key != null)
                return each;
        }

        return products[0];
    }

    public static void autoActivateAsNecessaryAsync()
    {
        if(_autoActivated)
        {
            return;
        } else
        {
            _autoActivated = true;
            Thread thread = new Thread("Checking Activation") {

                public void run()
                {
                    LicenseManager.autoActivateAsNecessary();
                }

            };
            thread.setPriority(Thread.currentThread().getPriority() - 1);
            thread.start();
            return;
        }
    }

    public static void autoActivateAsNecessary()
    {
        LicenseFile licenseFile = LicenseFile.getInstance();
        if(!licenseFile.getBooleanValue(LicenseFile.AUTO_ACTIVATE_KEY, true))
            return;
        String buildDateKey = getProductBuildDateKey();
        String actualBuildDate = LicenseInfo.getProductBuildDateString();
        String expectedBuildDate = licenseFile.getValue(buildDateKey);
        if(actualBuildDate.equals(expectedBuildDate))
            return;
        if(actualBuildDate.equals("unknown date"))
            return;
        LicenseProduct prod = getProduct();
        String prodId = null;
        LicenseSerialNumber serialNum = null;
        if(prod.isProMode())
        {
            serialNum = prod.getSerialNumber();
            prodId = serialNum == null ? prod.getProProductIds()[0] : serialNum.getProductId();
        } else
        if(prod.isEvalMode())
        {
            prodId = prod.getEvalProductId();
        } else
        {
            prodId = prod.getFreeProductId();
            if(prodId == null)
                prodId = prod.getEvalProductId();
        }
        org.eclipse.core.runtime.PluginVersionIdentifier prodVer = LicenseInfo.getProductVersion();
        LicenseActivationTransaction transaction = new LicenseActivationTransaction(prodId, prodVer, actualBuildDate, null);
        transaction.setSerialNumber(serialNum);
        transaction.setLastActivationKey(getLastActivationKey());
        transaction.setUpdateOnly(true);
        if(!transaction.process(new NullProgressMonitor()))
            return;
        licenseFile.putValue(buildDateKey, actualBuildDate);
        String disabledKey = transaction.getDisabledKey();
        if(disabledKey != null)
            licenseFile.addDisabledKey(disabledKey);
        licenseFile.saveContent();
    }

    public static String getProductBuildDateKey()
    {
        if(_productBuildDateKey == null)
        {
            int crc;
            try
            {
                crc = LicenseUtil.crc32(Platform.resolve(LicenseInfo.getInstallURL()).getPath());
            }
            catch(Throwable e)
            {
                ProductLog.logInfo("Failed to determine product build date key: " + e);
                crc = 0;
            }
            _productBuildDateKey = "BuildDate" + crc;
        }
        return _productBuildDateKey;
    }

    static 
    {
        LicenseInfo.initialize(DesignerPlugin.getDefault(), DESIGNER_NAME, DESIGNER_PLUGIN_ID);
        WB_PRODUCT = new LicenseProduct(null, DESIGNER_PLUGIN_ID, LicenseUtil.decode("EUXT0RNPXM3JEFXEGCHA7866H4"), LicenseUtil.decode("EUXT0RNPXM3JEFXEGCHA7866H4ZZCZBY6W"), LicenseUtil.decode("EUXT0RNPXM3JEFXEGCHA7866H4\u2122YY4XCWEU9SNNJNWMHJDDWDXCEAH886A2"), LicenseUtil.decode("EUXT0RNPXM3JEFXEGCHA7866H4\u2122YY3ZBXSSPSNN0PXM3JGGUEDCTAF83303B2Z0VVWWQT6SAQYK1KUHSDWDSB994484M260WWWXVV7TVRRPWMJJWG5DNCBAJ8K61102D0YYSV9UWSZQQNGGNHRFVDBBP987141311DZUUBV7TVRYPPMFFHFCEKCEAK8M644ZZFZHXCVRR2S3QJJSJDGZF9C88R8S795C331WWCXVVRTURYPXMSJTGGEZCN8Z7F693H2XX2YAWRRNRMM2N2KVHFFTDSB99P722D321WWUW5V8TURKK2K4HZFBBCBAAP8A664B200BYTTQT1SPQJJ1J2HKFPDDB667655H3511Z6XVV6T"), DEFAULT_SUPPORT_EMAIL_ADDRESS, DEFAULT_INFO_EMAIL_ADDRESS, DEFAULT_SALES_EMAIL_ADDRESS, LicenseUtil.decode("YV8T6R0PAJXGVEZEXCVAK656844240VXCWEU3QNQYNUKWFDFFDFBK74695C3DZ9ZAXUVTT4R0MQM0JRGNE7AF9H8M6K4Y190DY0WQT7SUQYNUHYFVD1BV9X7Q5R3M1NZKXFVCT"), LicenseUtil.decode("DVQS2R2PXMVGDFADNCKA88K6F24240ZY"), new String[] {
            WB_PRO_PRODUCT_ID
        }, new String[0], WB_EVAL_PRODUCT_ID, WB_FREE_PRODUCT_ID, 7, -1, 299, null);
        SWT_PRODUCT = new LicenseProduct(WB_PRODUCT, DESIGNER_PLUGIN_ID, LicenseUtil.decode("AUCS7QKKMKJJXGJEECKA88K6"), LicenseUtil.decode("AUCS7QKKMKJJXGJEECKA88K6"), LicenseUtil.decode("AUCS7QKKMKJJXGJEECKA88K6\u2122"), LicenseUtil.decode("AUCS7QKKMKJJXGJEECKA88K6\u21220051DZUURUQQ2RZP5MJJWGFEVCHA5525D412XXYYSV8UCS0N3NWKUFYFUDBB66A6P482YYYZXX9VXTTRYPMMYJ7FQEDCMAN83324F200UXBWYU1SSQJJQKTHXFDDRBA9363533F1WWDX9VXT0RRPHHKHEGNEGCNAP86611H1KZEXTT9USSNQRNVKUHPFQDDBW9J5"), LicenseUtil.decode("9V9T2R0PXMYJYGACTCVAQ8G43422E02YYW3USS3QZKHKUHQF"), LicenseUtil.decode("ZV2TSRZPGJZJ1GWEPA9A88M6844290YY9W5SQS0QWN"), LicenseUtil.decode("9VPSYRPP1MEGXGZEUCM87866J462207YWW7U3QNQYNUK"), LicenseUtil.decode("YV8T6R0PAJXGVEZEXCVAK656844240VXCWEU3QNQYNUKWFDFFDFBK74695C3DZ9ZAXUVTT4R0MQM0JRGNE7AF9H8M6K4Y190DY0WQT7SUQYNUHYFVDZBU9X7N5R3M1QZEXBVET"), LicenseUtil.decode("9VBT6RZMHKEHSGQECCQAK6868432"), new String[] {
            SWT_PRO_PRODUCT_ID
        }, new String[0], SWT_EVAL_PRODUCT_ID, SWT_FREE_PRODUCT_ID, 4, -1, 199, null);
        SWING_PRODUCT = new LicenseProduct(WB_PRODUCT, DESIGNER_PLUGIN_ID, LicenseUtil.decode("AUBTVRYPPMFFGFEETCEAA8F644F2"), LicenseUtil.decode("AUBTVRYPPMFFGFEETCEAA8F644F2"), LicenseUtil.decode("AUBTVRYPPMFFGFEETCEAA8F644F2\u2122"), LicenseUtil.decode("AUBTVRYPPMFFGFEETCEAA8F644F2\u2122WW1X9VQQMQKKYMVJ1GEESCBAR8D611Y190XYTTUUNR4Q8NWHZHSFQBUBQ9772262J04YUUUVTT5RTPPMUJGGUE3BKA98G6H4ZZY0BYWWQT7SUQXNNKEEKFPDTB99M765Z2Z1ZZBXSS9T5RTPWMMJDDMD8BUA4733K3M2407YYWRR7SQQKNPKTHSFMDNBB9U7G3"), DEFAULT_SUPPORT_EMAIL_ADDRESS, DEFAULT_INFO_EMAIL_ADDRESS, DEFAULT_SALES_EMAIL_ADDRESS, LicenseUtil.decode("YV8T6R0PAJXGVEZEXCVAK656844240VXCWEU3QNQYNUKWFDFFDFBK74695C3DZ9ZAXUVTT4R0MQM0JRGNE7AF9H8M6K4Y190DY0WQT7SUQYNUHYFVD1BV9X7Q5Q3Q1NZDXHV9T"), LicenseUtil.decode("9VBTVRYPPMVGDFADNCKA88K6F24240ZY"), new String[] {
            SWING_PRO_PRODUCT_ID
        }, new String[0], SWING_EVAL_PRODUCT_ID, SWING_FREE_PRODUCT_ID, 3, -1, 199, null);
        DEFAULT_PRODUCT = WB_PRODUCT;
        PRODUCTS_IN_WIZARD_DISPLAY_ORDER = (new LicenseProduct[] {
            WB_PRODUCT, SWT_PRODUCT, SWING_PRODUCT
        });
        PRODUCTS_IN_VALIDATE_ORDER = (new LicenseProduct[] {
            WB_PRODUCT, SWT_PRODUCT, SWING_PRODUCT
        });
    }

}
