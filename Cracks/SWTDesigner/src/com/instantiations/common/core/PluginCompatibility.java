// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   PluginCompatibility.java

package com.instantiations.common.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Dictionary;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.PluginVersionIdentifier;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.Bundle;

// Referenced classes of package com.instantiations.common.core:
//            EclipseVersion, EclipseInstallation, ProductLog

public class PluginCompatibility
{

    private static final String UNINITIALIZED = "PluginCompatibility.check(...) has not been called.";
    private static final String NEWLINE = System.getProperty("line.separator");
    private static EclipseVersion expectedIDEVersion = null;
    private static EclipseVersion actualIDEVersion = null;
    private static String versionWarningMessage = "PluginCompatibility.check(...) has not been called.";
    private static String versionWarningTitle = "PluginCompatibility.check(...) has not been called.";
    private static EclipseInstallation ideInstallation;

    private PluginCompatibility()
    {
    }

    public static boolean check(String productName, Plugin plugin)
    {
        String version = (String)ResourcesPlugin.getPlugin().getBundle().getHeaders().get("Bundle-Version");
        PluginVersionIdentifier actualVersion = new PluginVersionIdentifier(version);
        actualIDEVersion = new EclipseVersion(actualVersion.getMajorComponent(), actualVersion.getMinorComponent(), actualVersion.getServiceComponent(), actualVersion.getMajorComponent() < 3 ? null : readBuildId("org.eclipse.jdt", "about.mappings"));
        expectedIDEVersion = new EclipseVersion(3, 1, 0, readBuildId(plugin.getBundle(), "org.eclipse.sdk_about.mappings"));
        File ideInstallDir;
        try
        {
            URL url = ResourcesPlugin.getPlugin().getBundle().getEntry("/");
            url = Platform.asLocalURL(url);
            ideInstallDir = (new Path(url.getFile())).toFile().getParentFile().getParentFile();
        }
        catch(Exception e)
        {
            ideInstallDir = null;
            log(plugin, "Failed to determine IDE install directory", e);
        }
        ideInstallation = new EclipseInstallation(ideInstallDir);
        if(expectedIDEVersion.isCompatible(actualIDEVersion))
        {
            versionWarningMessage = null;
            versionWarningTitle = null;
            return true;
        }
        String pluginName = (String)plugin.getBundle().getHeaders().get("Bundle-Name");
        if(pluginName == null || pluginName.trim().length() == 0)
            pluginName = getPluginID(plugin);
        versionWarningMessage = "This is the incorrect version of " + productName + " for " + getIDEDescription() + "." + NEWLINE + "This version was compiled for " + getIDEVersionTextExpected() + " and is running on " + getIDEVersionTextActual() + ".";
        versionWarningTitle = "Incorrect Version of " + productName;
        log(plugin, NEWLINE + NEWLINE + versionWarningMessage + NEWLINE, null);
        return false;
    }

    private static void checkState()
    {
        if(expectedIDEVersion == null)
            throw new IllegalStateException("PluginCompatibility.check(...) has not been called.");
        else
            return;
    }

    public static boolean isCorrectIDEVersion()
    {
        checkState();
        return versionWarningMessage == null;
    }

    public static String getVersionWarningTitle()
    {
        checkState();
        return versionWarningTitle;
    }

    public static String getVersionWarningMessage()
    {
        checkState();
        return versionWarningMessage;
    }

    public static String getIDEDescription()
    {
        checkState();
        return ideInstallation.getDescription();
    }

    public static String getIDEVersionTextExpected()
    {
        checkState();
        return "Eclipse " + expectedIDEVersion.toString();
    }

    public static String getIDEVersionTextActual()
    {
        checkState();
        return "Eclipse " + actualIDEVersion.toString();
    }

    public static String getIDEVersionActualBuildID()
    {
        checkState();
        return actualIDEVersion.getActualBuild();
    }

    public static String readBuildId(String pluginId, String fileName)
    {
        return readBuildId(Platform.getBundle(pluginId), fileName);
    }

    public static String readBuildId(Bundle bundle, String fileName)
    {
        String buildId;
        InputStream stream = null;
        buildId = "???";
        try
        {
            URL url = bundle.getEntry("/");
            url = Platform.resolve(url);
            url = new URL(url, fileName);
            stream = url.openStream();
            buildId = EclipseVersion.readBuildId(stream);
        }
        catch(IOException e)
        {
            ProductLog.logError(e);
        }
        finally
        {
            try
            {
                if(stream != null)
                    stream.close();
            }
            catch(IOException e)
            {
                ProductLog.logError(e);
            }
        }
        return buildId;
    }

    private static void log(Plugin plugin, String errMsg, Throwable e)
    {
        String pluginID = getPluginID(plugin);
        Status status = new Status(4, pluginID, 4, errMsg, e);
        plugin.getLog().log(status);
    }

    private static String getPluginID(Plugin plugin)
    {
        String pluginID = plugin.getBundle().getSymbolicName();
        return pluginID;
    }

}
