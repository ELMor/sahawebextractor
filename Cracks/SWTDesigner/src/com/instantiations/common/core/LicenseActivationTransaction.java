// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseActivationTransaction.java

package com.instantiations.common.core;

import java.io.PrintWriter;
import org.eclipse.core.runtime.PluginVersionIdentifier;

// Referenced classes of package com.instantiations.common.core:
//            LicenseTransaction, LicenseUtil, LicenseSerialNumber, LicenseActivationKey, 
//            LicenseRemoteMachine

public class LicenseActivationTransaction extends LicenseTransaction
{

    private static final String ACTIVATION_SPEC = LicenseUtil.decode("YV8T6R0PAJXGVEZEXCVAK6A6D4G2F0UX5W9UWSKP3NQKUHRFUDP979H7D5F1V0AZAXZVATMQ4PMMXGCFCEUCEAS825K462A07YQV9UVSKPXNSKYHTD9CSBM9");
    private static final String PRODUCT_ID = LicenseUtil.decode("6V6T1RNP3MGJYGKDBC");
    private static final String PRODUCT_VERSION_MAJOR = LicenseUtil.decode("CVTT4R3PRMVJSGQD8BFAJ8K6");
    private static final String PRODUCT_VERSION_MINOR = LicenseUtil.decode("CVTT4R3PRMVJSGQDGCKAJ8K6");
    private static final String PRODUCT_VERSION_SERVICE = LicenseUtil.decode("CVTT4R3PRMVJSGWDCCQAS8A62422");
    private static final String PRODUCT_BUILD = LicenseUtil.decode("6V6T1RNP3MGJYGCDVCEAF856");
    public static final String PRODUCT_SERIALNUM = LicenseUtil.decode("9VTT4RTPGKSJTFXEMC7988K6");
    public static final String USERNAME = LicenseUtil.decode("BV7TRR2PWMEHRGEE");
    public static final String PASSWORD = LicenseUtil.decode("6VPS5R3P5MVJWGDE");
    private static final String LAST_ACTIVATION_KEY = LicenseUtil.decode("2VPS5R4PHJGJYGJEWC69Q8A6E4B27ZYYGW");
    private static final String UPDATE_ONLY_KEY = LicenseUtil.decode("BV4TQRJN2MJJUFQEKCXA");
    public static final String TRUE_VALUE = LicenseUtil.decode("AV6T7RPP");
    public static final String FALSE_VALUE = LicenseUtil.decode("WVPSYR3PMM");
    private static final String MESSAGE_TAG = LicenseUtil.decode("3VTT5R3PGKMJGG");
    private static final String ERRORMESSAGE_TAG = LicenseUtil.decode("VV6T4RZP0MUHGGVETC69A866");
    private static final String ACTIVATIONKEY_TAG = LicenseUtil.decode("RURT6RTP4MEHYGJEPCKAF766R4");
    private static final String SERIALNUMBER_TAG = LicenseUtil.decode("9VTT4RTPGKSJTFXEMC7988K6");
    private static final String DISABLEDKEY_TAG = LicenseUtil.decode("UVXT5RJNHKSJGGDEKBAAV8");
    public static final String HOW_USEFUL_KEY = LicenseUtil.decode("YV3T9R6N1MJJHGXEKC");
    public static final String MORE_USEFUL_KEY = LicenseUtil.decode("3V3T4RPP4KZJGGFEVCHA");
    public static final String WILL_PURCHASE_KEY = LicenseUtil.decode("DVXTYRWPZK1JWGCEFC69P866");
    public static final String WHY_NO_PURCHASE_KEY = LicenseUtil.decode("DVWTBRZNXMXHZGUEACDA47M644");
    public static final String PURCHASE_PRICE_KEY = LicenseUtil.decode("6V9T4RMPQMEHXGEERBQAC84644");
    private static final String HANDLES_REDIRECT_TAG = LicenseUtil.decode("YVPS0RNPUMJJXGVDCC9AC8K64402F0");
    private String _message;
    private String _errorMessage;
    private LicenseActivationKey _activationKey;
    private LicenseSerialNumber _serialNum;
    private String _disabledKey;

    public LicenseActivationTransaction(String productId, PluginVersionIdentifier version, String prodBuild, LicenseRemoteMachine machines[])
    {
        super(ACTIVATION_SPEC);
        addFormData(PRODUCT_ID, productId);
        addFormData(PRODUCT_VERSION_MAJOR, Integer.toString(version.getMajorComponent()));
        addFormData(PRODUCT_VERSION_MINOR, Integer.toString(version.getMinorComponent()));
        addFormData(PRODUCT_VERSION_SERVICE, Integer.toString(version.getServiceComponent()));
        addFormData(PRODUCT_BUILD, prodBuild);
        addFormData(HANDLES_REDIRECT_TAG, TRUE_VALUE);
        addUserInfo();
        addHardwareInfo(machines);
        addSoftwareInfo();
    }

    public void setSerialNumber(LicenseSerialNumber serialNumber)
    {
        if(serialNumber != null)
            addFormData(PRODUCT_SERIALNUM, serialNumber.getText());
    }

    public void setUsername(String username)
    {
        if(username != null && username.length() > 0)
            addFormData(USERNAME, username);
    }

    public void setPassword(String password)
    {
        if(password != null && password.length() > 0)
            addFormData(PASSWORD, password);
    }

    public void setLastActivationKey(LicenseActivationKey key)
    {
        if(key != null)
            addFormData(LAST_ACTIVATION_KEY, key.getText());
    }

    public void setUpdateOnly(boolean updateOnly)
    {
        addFormData(UPDATE_ONLY_KEY, updateOnly ? TRUE_VALUE : FALSE_VALUE);
    }

    public void setHowUseful(int howUseful)
    {
        if(howUseful >= 0)
            addFormData(HOW_USEFUL_KEY, Integer.toString(howUseful + 1));
    }

    public void setMoreUseful(String moreUseful)
    {
        addFormData(MORE_USEFUL_KEY, moreUseful);
    }

    public void setWillPurchase(boolean willPurchase)
    {
        addFormData(WILL_PURCHASE_KEY, willPurchase ? TRUE_VALUE : FALSE_VALUE);
    }

    public void setWhyNoPurchase(String whyNoPurchase)
    {
        addFormData(WHY_NO_PURCHASE_KEY, whyNoPurchase);
    }

    public void setPurchasePrice(String purchasePrice)
    {
        int value = LicenseUtil.extractInt(purchasePrice);
        if(value > 0)
            addFormData(PURCHASE_PRICE_KEY, Integer.toString(value));
    }

    boolean processResponse()
    {
        String response = getResponse();
        _message = LicenseTransaction.extractValue(response, MESSAGE_TAG);
        if(_message == null)
        {
            setErrorMessage(LicenseUtil.decode("0U2T8RJNUMPJFGBBTCAAN8Q644F2XXBYWW8U3S0QXN0KHH5DAAKBD9N7K573A11ZUU3VTT5R3PGKMJGG"));
            return false;
        }
        _message = LicenseUtil.replace(_message, "\r\n", LicenseUtil.LINE_END);
        _errorMessage = LicenseTransaction.extractValue(response, ERRORMESSAGE_TAG);
        if(_errorMessage != null)
        {
            _errorMessage = LicenseUtil.replace(_errorMessage, "\r\n", LicenseUtil.LINE_END).trim();
            if(_errorMessage.length() == 0)
                _errorMessage = null;
        }
        String key = LicenseTransaction.extractValue(response, ACTIVATIONKEY_TAG);
        if(key != null && key.length() > 0)
            _activationKey = new LicenseActivationKey(key);
        key = LicenseTransaction.extractValue(response, SERIALNUMBER_TAG);
        if(key != null && key.length() > 0)
            _serialNum = new LicenseSerialNumber(key);
        key = LicenseTransaction.extractValue(response, DISABLEDKEY_TAG);
        if(key != null && key.length() > 0)
            _disabledKey = key;
        return true;
    }

    public String getMessage()
    {
        return _message;
    }

    public String getErrorMessage()
    {
        return _errorMessage;
    }

    public LicenseActivationKey getActivationKey()
    {
        return _activationKey;
    }

    public LicenseSerialNumber getSerialNumber()
    {
        return _serialNum;
    }

    public String getDisabledKey()
    {
        return _disabledKey;
    }

    public static void writeEmail(PrintWriter writer, String productId, PluginVersionIdentifier version, String productBuild, LicenseRemoteMachine machines[])
    {
        LicenseTransaction.writeEmailFormData(writer, PRODUCT_ID, productId);
        LicenseTransaction.writeEmailFormData(writer, PRODUCT_VERSION_MAJOR, Integer.toString(version.getMajorComponent()));
        LicenseTransaction.writeEmailFormData(writer, PRODUCT_VERSION_MINOR, Integer.toString(version.getMinorComponent()));
        LicenseTransaction.writeEmailFormData(writer, PRODUCT_VERSION_SERVICE, Integer.toString(version.getServiceComponent()));
        LicenseTransaction.writeEmailFormData(writer, PRODUCT_BUILD, productBuild);
        LicenseTransaction.writeEmailUserInfo(writer);
        LicenseTransaction.writeEmailHardwareInfo(writer, machines);
        LicenseTransaction.writeEmailSoftwareInfo(writer);
    }

}
