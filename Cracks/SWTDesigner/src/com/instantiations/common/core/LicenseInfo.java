// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   LicenseInfo.java

package com.instantiations.common.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Dictionary;
import java.util.GregorianCalendar;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProduct;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.PluginVersionIdentifier;
import org.eclipse.osgi.util.ManifestElement;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;

// Referenced classes of package com.instantiations.common.core:
//            ProductLog, PluginCompatibility

public class LicenseInfo
{

    private static final String UNKNOWN_IDE_NAME = "Unknown IDE";
    private static final String UNKNOWN_IDE_VERSION = "???";
    private static final String UNKNOWN_NL = "??";
    private static String _productIdentifier;
    private static Plugin _plugin;
    private static String _genericName;
    private static String _ideFullName;
    private static String _ideVersionString;
    private static String _ideNL;
    private static File _installDir;
    private static PluginVersionIdentifier _buildVersion;
    private static String _buildVersionString;
    private static GregorianCalendar _buildDate;
    private static String _buildDateString;

    private LicenseInfo()
    {
    }

    public static void initialize(Plugin plugin, String genericName, String productIdentifier)
    {
        if(plugin == null)
            throw new IllegalArgumentException();
        if(_plugin != null)
        {
            throw new IllegalStateException();
        } else
        {
            _plugin = plugin;
            _genericName = genericName;
            _productIdentifier = productIdentifier;
            return;
        }
    }

    public static String getIDEFullName()
    {
        if(_ideFullName == null)
            try
            {
                IProduct ideProduct = Platform.getProduct();
                if(ideProduct != null)
                {
                    _ideFullName = ideProduct.getName();
                    if(_ideFullName != null)
                    {
                        _ideFullName = _ideFullName.trim();
                        if(_ideFullName.startsWith("Eclipse Pl"))
                            _ideFullName = "Eclipse";
                        else
                        if(_ideFullName.startsWith("Common OS-independent base of the Eclipse platform"))
                            _ideFullName = "Eclipse";
                    } else
                    {
                        _ideFullName = "Unknown IDE";
                    }
                } else
                {
                    _ideFullName = "Unknown IDE";
                }
            }
            catch(Exception ex)
            {
                _ideFullName = "Unknown IDE";
            }
        return _ideFullName;
    }

    public static String getIDEVersionString()
    {
        if(_ideVersionString == null)
            try
            {
                IProduct ideProduct = Platform.getProduct();
                if(ideProduct != null)
                {
                    Bundle ideBundle = ideProduct.getDefiningBundle();
                    if(ideBundle != null)
                        _ideVersionString = (String)ideBundle.getHeaders().get("Bundle-Version");
                }
                if(_ideVersionString == null || _ideVersionString.length() == 0)
                    _ideVersionString = "???";
            }
            catch(Exception ex)
            {
                _ideVersionString = "???";
            }
        return _ideVersionString;
    }

    public static String getIDENL()
    {
        if(_ideNL == null)
        {
            try
            {
                _ideNL = Platform.getNL();
            }
            catch(Exception exception) { }
            if(_ideNL == null)
                _ideNL = "??";
        }
        return _ideNL;
    }

    public static String getProductGenericName()
    {
        return _genericName;
    }

    public static String getProductIdentifier()
    {
        return _productIdentifier;
    }

    public static File getInstallDir()
        throws Exception
    {
        if(_installDir == null)
        {
            URL url = _plugin.getBundle().getEntry("/");
            url = Platform.asLocalURL(url);
            _installDir = new File(url.getPath());
        }
        return _installDir;
    }

    private static String getFirstJarFileName()
    {
        try
        {
            String requires = (String)_plugin.getBundle().getHeaders().get("Bundle-ClassPath");
            ManifestElement elements[] = ManifestElement.parseHeader("Bundle-ClassPath", requires);
            return elements[0].getValue();
        }
        catch(BundleException e)
        {
            ProductLog.logError(e);
            return "Exception:" + e.getMessage();
        }
    }

    public static String getProductVersionBuildString()
    {
        return getProductVersionString() + " compiled on " + getProductBuildDateString() + " for " + PluginCompatibility.getIDEVersionTextExpected();
    }

    public static String getProductVersionString()
    {
        if(_buildVersionString == null)
            _buildVersionString = (String)_plugin.getBundle().getHeaders().get("Bundle-Version");
        return _buildVersionString;
    }

    public static PluginVersionIdentifier getProductVersion()
    {
        if(_buildVersion == null)
            _buildVersion = new PluginVersionIdentifier(getProductVersionString());
        return _buildVersion;
    }

    public static GregorianCalendar getProductBuildDate()
    {
        if(_buildDate == null)
            initBuildDate();
        return _buildDate;
    }

    public static String getProductBuildDateString()
    {
        if(_buildDateString == null)
            initBuildDate();
        return _buildDateString;
    }

    private static void initBuildDate()
    {
        try
        {
            File jarFile = new File(getInstallDir(), getFirstJarFileName());
            if(jarFile.exists())
            {
                Date jarDate = new Date(jarFile.lastModified());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(jarDate);
                _buildDate = new GregorianCalendar(calendar.get(1), calendar.get(2), calendar.get(5));
                _buildDateString = (new SimpleDateFormat("yyyy.MM.dd")).format(jarDate);
            } else
            {
                _buildDate = new GregorianCalendar();
                _buildDateString = "unknown date";
            }
        }
        catch(Exception ex)
        {
            _buildDate = new GregorianCalendar();
            _buildDateString = "failed to determine date";
        }
    }

    public static InputStream openProductStream(IPath relPath)
        throws IOException
    {
        return _plugin.openStream(relPath);
    }

    public static URL getInstallURL()
    {
        return _plugin.getBundle().getEntry("/");
    }

    public static IPath getStateLocation()
    {
        return _plugin.getStateLocation();
    }

    public static URL findProductURL(IPath relPath)
    {
        return _plugin.find(relPath);
    }
}
