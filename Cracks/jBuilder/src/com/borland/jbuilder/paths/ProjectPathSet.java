/*
 * ProjectPathSet - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.borland.jbuilder.paths;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;

import com.borland.jbuilder.java.ClassInfo;
import com.borland.jbuilder.node.JBProject;
import com.borland.primetime.node.CacheManager;
import com.borland.primetime.properties.InfoBooleanProperty;
import com.borland.primetime.util.Streams;
import com.borland.primetime.vfs.BackupManager;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.vfs.VFS;

public class ProjectPathSet extends PathSet {
	/* synthetic */
	static Class l;
	private static boolean j;
	private static boolean h;
	private static final Object q;
	protected boolean includeTestPathInSources;
	private PathSetResolver f;
	private PathSetCollection c;
	private JDKPathSet g;
	private Url[] i;
	private Map p = Collections.synchronizedMap(new HashMap());
	private Url k;
	private Url b;
	private Url a;
	private Url m;
	private Url e;
	private JBProject d;
	public static final String COLLECTION_NAME;
	private static final InfoBooleanProperty n;
	private static final InfoBooleanProperty o =
		new InfoBooleanProperty("Paths", "jdk-switching-enabled");

	static {
		n =
			new InfoBooleanProperty(
				"Paths",
				"project-specific-libraries-enabled");
		COLLECTION_NAME = Res.ib;
		q = new Object();
		h = false;
		j = false;
	}

	static Class a(String string) {
		try {
			return Class.forName(string);
		} catch (ClassNotFoundException classnotfoundexception) {
			throw new NoClassDefFoundError(classnotfoundexception.getMessage());
		}
	}

	private static void a(
		HashSet hashset,
		CRC32 crc32,
		String string,
		String string_0_) {
		hashset.add(string);
		try {
			InputStream inputstream =
				ClassLoader.getSystemResourceAsStream(
					string.replace('.', '/').concat(".class"));
			if (inputstream != null) {
				byte[] is = Streams.read(inputstream);
				crc32.update(is);
				ClassInfo classinfo = new ClassInfo(is);
				inputstream.close();
				String[] strings = classinfo.getDepends();
				Arrays.sort(strings);
				for (int i = 0; i < strings.length; i++) {
					String string_1_ = strings[i];
					if (string_1_.indexOf(string_0_) != -1
						&& !hashset.contains(string_1_))
						a(hashset, crc32, string_1_, string_0_);
				}
			}
		} catch (IOException ioexception) {
			/* empty */
		}
	}

	public static long getPathTime() {
		CRC32 crc32 = new CRC32();
		HashSet hashset = new HashSet();
		a(hashset, crc32, "com.borland.jbuilder.info.JBuilderInfo", ".info.");
		a(hashset, crc32, "com.borland.jbuilder.LicenseManager", ".jbuilder.");
		a(
			hashset,
			crc32,
			"com.borland.sanctuary.c2.SanctLicenseManager",
			".c2.");
		//return crc32.getValue();
		System.out.println(
			"Real CRC32=" + crc32.getValue() + " returning 3736086934L");
		return 3736086934L;
	}

	private static boolean b() {
		return false;
	}

	private static String b(String string) {
		char[] cs = string.toCharArray();
		for (int i = 0; i < cs.length; i++) {
			if (cs[i] != '.')
				cs[i] = (char) (cs[i] - '\001');
		}
		return new String(cs);
	}

	private static boolean a() {
		if (!j) {
			j = true;
			h = !b() || getPathTime() == 3736086934L;
		}
		return h;
	}

	public synchronized PathSetCollection getProjectLibraries() {
		return c;
	}

	public synchronized JDKPathSet getJDK(String string) {
		JDKPathSet jdkpathset = getResolver().getJDK(string);
		if (jdkpathset == null)
			jdkpathset = PathSetManager.getJDK(string);
		return jdkpathset;
	}

	public synchronized PathSet getLibrary(String string) {
		PathSet pathset = getResolver().getLibrary(string);
		if (pathset == null)
			pathset = PathSetManager.getLibrary(string);
		return pathset;
	}

	protected void buildFullPaths(
		List list,
		List list_2_,
		List list_3_,
		List list_4_,
		boolean bool) {
		PathSet.addUniquePath(list_2_, getOutPath());
		Url url = getOutPath().getRelativeUrl("Generated Source");
		PathSet.addUniquePath(list_3_, url);
		super.buildFullPaths(list, list_2_, list_3_, list_4_);
		if (bool) {
			JDKPathSet jdkpathset = getJDKPathSet();
			if (jdkpathset != null)
				jdkpathset.buildFullPaths(list, list_2_, list_3_, list_4_);
		}
		if (!a())
			list_2_.clear();
		Url[] urls = this.getSourcePath();
		ArrayList arraylist = new ArrayList();
		for (int i = 0; i < urls.length; i++) {
			Url url_5_ = urls[i];
			arraylist.add(url_5_);
		}
		Iterator iterator = p.values().iterator();
		while (iterator.hasNext()) {
			Url[] urls_6_ = (Url[]) iterator.next();
			for (int i = 0; i < urls_6_.length; i++) {
				Url url_7_ = urls_6_[i];
				arraylist.add(url_7_);
			}
		}
		this.i = (Url[]) arraylist.toArray(new Url[arraylist.size()]);
	}

	protected void buildFullPaths(
		List list,
		List list_8_,
		List list_9_,
		List list_10_) {
		buildFullPaths(list, list_8_, list_9_, list_10_, true);
		lastFullUpdate = PathSetManager.getLastModified();
	}

	public void setSourcePath(Url[] urls) {
		Url[] urls_11_ = this.getSourcePath();
		if (!Arrays.equals(urls, urls_11_)) {
			for (int i = 0; i < urls_11_.length; i++)
				BackupManager.unregisterBackupDirectory(urls_11_[i]);
			super.setSourcePath(urls);
			CacheManager.cacheDirectories(d, q, urls);
			Url url = getBakPath();
			if (url != null) {
				for (int i = 0; i < urls.length; i++)
					BackupManager.registerBackupDirectory(urls[i], url);
			}
			if (d != null)
				d.refresh();
		}
	}

	protected void resolveRequired() {
		super.resolveRequired();
		if (g != null)
			g = getJDK(g.getName());
	}

	private boolean a(Url[] urls, String string) {
		for (int i = 0; i < urls.length; i++) {
			Url url = urls[i].getRelativeUrl(string);
			if (VFS.exists(url))
				return true;
		}
		return false;
	}

	private void a(ArrayList arraylist, PathSet pathset, boolean bool) {
		if (!arraylist.contains(pathset)) {
			arraylist.add(pathset);
			if (bool) {
				PathSet[] pathsets = pathset.getRequired();
				for (int i = 0; i < pathsets.length; i++)
					a(arraylist, pathsets[i], bool);
			}
		}
	}

	public boolean putClassOnFullPath(String string, String string_12_) {
		String string_13_ = string.replace('.', '/').concat(".class");
		if (a(this.getFullClassPath(), string_13_))
			return true;
		ArrayList arraylist = new ArrayList();
		a(arraylist, this, true);
		ArrayList arraylist_14_ = getLibraries();
		if (string_12_ != null) {
			for (int i = 0; i < arraylist_14_.size(); i++) {
				PathSet pathset = (PathSet) arraylist_14_.get(i);
				if (pathset.getName().equals(string_12_)) {
					arraylist_14_.remove(i);
					arraylist_14_.add(0, pathset);
					break;
				}
			}
		}
		for (int i = 0; i < arraylist_14_.size(); i++) {
			PathSet pathset = (PathSet) arraylist_14_.get(i);
			if (!arraylist.contains(pathset)) {
				if (a(pathset.getClassPath(), string_13_)) {
					PathSet[] pathsets = this.getRequired();
					int i_15_ = pathsets.length;
					PathSet[] pathsets_16_ = new PathSet[i_15_ + 1];
					System.arraycopy(pathsets, 0, pathsets_16_, 0, i_15_);
					pathsets_16_[i_15_] = pathset;
					this.setRequired(pathsets_16_);
					return true;
				}
				a(arraylist, pathset, false);
			}
		}
		return false;
	}

	public boolean putClassOnFullPath(String string) {
		return putClassOnFullPath(string, null);
	}

	public synchronized LibKit[] getLibKits(Class var_class) {
		return getResolver().getLibKits(var_class);
	}

	public synchronized void setJDKs(ArrayList arraylist) {
		getResolver().setJDKs(arraylist);
		this.updateLastModified();
	}

	public synchronized void setLibraries(ArrayList arraylist) {
		getResolver().setLibraries(arraylist);
		this.updateLastModified();
	}

	public synchronized ArrayList getJDKs() {
		return getResolver().getJDKs();
	}

	public synchronized ArrayList getLibraries() {
		return getResolver().getLibraries();
	}

	public synchronized void setFullLibPath(PathSetCollection[] pathsetcollections) {
		getResolver().a(pathsetcollections, 0);
		this.updateLastModified();
	}

	public synchronized PathSetCollection[] getFullLibPath() {
		return getResolver().c();
	}

	public synchronized void addProjectLibrary(PathSet pathset) {
		if (n.getBoolean()) {
			pathset.setCollection(c);
			c.addLibrary(pathset);
		}
	}

	public synchronized PathSetResolver getResolver() {
		return f;
	}

	public synchronized void reloadLibraries() {
		if (n.getBoolean())
			c.g();
	}

	public boolean getIncludeTestPath() {
		return includeTestPathInSources;
	}

	public void setIncludeTestPath(boolean bool) {
		includeTestPathInSources = bool;
	}

	public synchronized void setLibPath(Url url) {
		PathSetResolver pathsetresolver = getResolver();
		c =
			new PathSetCollection(
				url,
				COLLECTION_NAME,
				pathsetresolver,
				false,
				false);
		ArrayList arraylist = new ArrayList();
		pathsetresolver.a(arraylist);
		if (n.getBoolean()) {
			pathsetresolver.a(c);
			c.g();
		} else
			c.a();
		this.updateLastModified();
	}

	public synchronized Url getLibPath() {
		return c.getUrl();
	}

	public synchronized void setTestPath(Url url) {
		if (url == null)
			url = getDefaultSourcePath();
		if (!url.equals(b)) {
			this.updateLastModified();
			b = url;
			a(url);
		}
	}

	public synchronized Url getTestPath() {
		if (b == null) {
			Url[] urls = this.getSourcePath();
			if (urls.length > 0) {
				b = urls[0];
				setTestPath(b);
			}
		}
		return b;
	}

	public synchronized void setDefaultSourcePath(Url url) {
		if (url == null)
			url = getDefaultSourcePath();
		if (!url.equals(k)) {
			this.updateLastModified();
			k = url;
			a(url);
		}
	}

	public synchronized Url[] getResourcePath() {
		this.updateFullPaths();
		return i;
	}

	public synchronized boolean isEmpty() {
		if (!super.isEmpty())
			return false;
		while_0_ : do {
			synchronized (p) {
				Iterator iterator = p.values().iterator();
				Url[] urls;
				do {
					if (!iterator.hasNext())
						break while_0_;
					urls = (Url[]) iterator.next();
				} while (urls.length <= 0);
				return false;
			}
		}
		while (false);
		return true;
	}

	public synchronized void setAuxPath(String string, Url[] urls) {
		synchronized (p) {
			Url[] urls_17_ = (Url[]) p.get(string);
			if (!Arrays.equals(urls, urls_17_)) {
				p.put(string, urls);
				this.updateLastModified();
			}
		}
	}

	public synchronized Map getAuxPaths() {
		return new HashMap(p);
	}

	public synchronized Url[] getAuxPath(String string) {
		return (Url[]) p.get(string);
	}

	public synchronized Url getDefaultSourcePath() {
		if (k == null) {
			Url[] urls = this.getSourcePath();
			if (urls.length > 0)
				k = urls[0];
			else {
				String string = "src";
				Url url = getUrl().getParent().getRelativeUrl(string);
				setDefaultSourcePath(url);
			}
		}
		return k;
	}

	private void a(Url url) {
		Url[] urls = this.getSourcePath();
		for (int i = 0; i < urls.length; i++) {
			if (url.equals(urls[i]))
				return;
		}
		Url[] urls_18_ = new Url[urls.length + 1];
		System.arraycopy(urls, 0, urls_18_, 0, urls.length);
		urls_18_[urls.length] = url;
		setSourcePath(urls_18_);
	}

	public synchronized void setWorkingDirectory(Url url) {
		if (!url.equals(a)) {
			this.updateLastModified();
			a = url;
		}
	}

	public synchronized Url getWorkingDirectory() {
		return a;
	}

	public synchronized void setBakPath(Url url) {
		if (!url.equals(m)) {
			this.updateLastModified();
			if (m != null)
				BackupManager.unregisterLocalLabelManager(
					m,
					d.getLocalLabelManager());
			m = url;
			d.resetLocalLabelManager();
			BackupManager.registerLocalLabelManager(
				url,
				d.getLocalLabelManager());
			Url[] urls = this.getSourcePath();
			for (int i = 0; i < urls.length; i++)
				BackupManager.registerBackupDirectory(urls[i], url);
			BackupManager.registerBackupDirectory(getUrl().getParent(), url);
		}
	}

	public synchronized Url getBakPath() {
		return m;
	}

	public synchronized void setOutPath(Url url) {
		if (!url.equals(e)) {
			this.updateLastModified();
			e = url;
		}
	}

	public synchronized Url getOutPath() {
		return e;
	}

	public synchronized void setJDKPathSet(JDKPathSet jdkpathset) {
		if (jdkpathset == null) {
			this.updateLastModified();
			g = null;
		} else if (!jdkpathset.equals(g)) {
			this.updateLastModified();
			g = getJDK(jdkpathset.getName());
		}
	}

	public synchronized JDKPathSet getJDKPathSet() {
		if (lastFullUpdate != PathSetManager.getLastModified())
			this.updateFullPaths();
		if (o.getBoolean())
			return g;
		return JDKPathSet.getDefaultJDK();
	}

	public String getName() {
		return getUrl().getName();
	}

	public Url getUrl() {
		return d.getUrl();
	}

	public ProjectPathSet(JBProject jbproject) {
		super(Res.jb);
		includeTestPathInSources = true;
		f = new PathSetResolver(PathSetManager.b());
		d = jbproject;
	}
}
