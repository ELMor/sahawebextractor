/*
 * JBuilderInfo - Decompiled by JODE Visit http://jode.sourceforge.net/
 */
package com.borland.jbuilder.info;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import com.borland.jbuilder.JBuilder;
import com.borland.jbuilder.LicenseManager;
import com.borland.jbuilder.ide.JBuilderIcons;
import com.borland.primetime.PrimeTime;
import com.borland.primetime.ide.Browser;
import com.borland.primetime.ide.BrowserAction;
import com.borland.primetime.ide.ShutdownListener;
import com.borland.primetime.ide.about.UserInfoPageFactory;
import com.borland.primetime.ide.about.UserInformation;
import com.borland.primetime.properties.InfoBooleanProperty;
import com.borland.primetime.properties.Property;
import com.borland.primetime.properties.PropertyManager;
import com.borland.primetime.util.Strings;
import com.borland.primetime.vfs.Url;
import com.borland.primetime.vfs.VFS;

public class JBuilderInfo
	extends InfoBooleanProperty
	implements ShutdownListener {
	public static final long serialVersionUID = -3275758211L;
	private static final HashMap arrays;
	private static final String[][] eb;
	private static final String[][] v;
	private static final String[][] w;
	private static final String[][] kb;
	private static final String[][] fb;
	private static final String[] jb;
	private static final String[] ib;
	public static final BrowserAction ACTION_LicenseManager;
	private static int s;
	private static String r;
	private static final String[] a;
	private static final int cb = 5;
	private static final int x = 4;
	private static final int z = 3;
	private static final int bb = 2;
	private static final int ab = 1;
	public static final int SKU_STD = 0;
	private static final String gb;
	private static final String db = "JBuilder X";
	private static final String c = "010.000.000.000";
	private static final int hb = 1010;
	private static HashMap p;
	private static Boolean d;
	private static boolean o;
	private static boolean m;
	private static int e;
	private static int f = 2;
	private static int b = -99;

	static {
		e = -1;
		p = new HashMap();
		gb = Res.jb;
		a = new String[] { Res.db, Res.cb, Res.eb, Res.fb, Res.bb, Res.ab };
		r = System.getProperties().getProperty("com.borland.jbuilder.sku");
		s = -1;
		try {
			if (r != null) {
				if (r.toLowerCase().startsWith("per"))
					s = 0;
				else if (r.toLowerCase().startsWith("fou"))
					s = 0;
				else if (r.toLowerCase().startsWith("pro"))
					s = 3;
				else if (r.toLowerCase().startsWith("se"))
					s = 3;
				else if (r.toLowerCase().startsWith("dev"))
					s = 3;
				else if (r.toLowerCase().startsWith("bea"))
					s = 5;
			}
		} catch (Exception exception) {
			/* empty */
		}
		LicenseManager.initialize(
			"JBuilder X",
			1010,
			gb,
			JBuilderIcons.JBUILDER_IMAGE,
			getSKU(),
			s);
		ACTION_LicenseManager = new BrowserAction(Res.wb, '\0', Res.xb) {
			public void actionPerformed(Browser browser) {
				LicenseManager.showInfoDialogFromUI();
			}
		};
		ib = new String[] { "ValueFeature", "enabled" };
		jb = new String[] { "PremiumFeature", "enabled" };
		fb = new String[0][];
		kb = (new String[][] { { "Action", "action-goto-class-member" }, {
				"CodeFormatting", "import-export" }, {
				"CodeTemplate", "enabled" }, {
				"Editor", "keymap-edit-enabled" }, {
				"Editor", "split-panes-enabled" }, {
				"LocalLabel", "mini" }, {
				"Node", "zip-file-type-enabled" }, {
				"Paths", "add-paths-enabled" }, {
				"Paths", "additional-search-paths-enabled" }, {
				"Paths", "automatic-packages-enabled" }, {
				"Paths", "delete-paths-enabled" }, {
				"Paths", "jdk-switching-enabled" }, {
				"Paths", "project-specific-libraries-enabled" }, {
				"ProjectView", "file-browser-tab-enabled" }, {
				"TeamDev", "cvs-enabled" }, {
				"TeamDev", "select-only-cvs" }, {
				"TeamDev", "smart-diff" }, {
				"TeamDev", "team-menu-enabled" }, {
				"Wizard", "data-module" }, {
				"Refactoring", "action-generic-refactoring" }
		});
		w = (new String[][] { ib, { "Archiver", "executable-jar" }, {
				"Archiver", "native-executable" }, {
				"Archiver", "web-start" }, {
				"DirectoryView", "action-create-directory" }, {
				"DirectoryView", "enabled" }, {
				"Debugger", "actions-breakpoint-tree" }, {
				"Debugger", "actions-breakpoint" }, {
				"Debugger", "actions-debug-tree" }, {
				"Debugger", "action-edit-no-tracing-class" }, {
				"Debugger", "actions-no-tracing-tree" }, {
				"Debugger", "actions-primitive-tree" }, {
				"Debugger", "actions-reference-tree" }, {
				"Debugger", "action-smart_swap" }, {
				"Debugger", "action-custom_views" }, {
				"Debugger", "actions-thread-tree" }, {
				"Debugger", "actions-custom-view-tree" }, {
				"Debugger", "action-cross-process-breakpoint" }, {
				"Debugger", "action-set-execution-point" }, {
				"Debugger", "action-keep-thread-suspended" }, {
				"EditFieldsProperties", "enabled" }, {
				"Editor", "action-replace-in-path" }, {
				"ExportToAnt", "enabled" }, {
				"Help", "action-beans-express-help" }, {
				"JavaStructure", "fix-all-javadoc-conflicts" }, {
				"JavaStructure", "fix-single-javadoc-conflict" }, {
				"LocalLabel", "ui" }, {
				"LocalLabel", "full" }, {
				"Module", "mobile-development" }, {
				"Module", "web-enabled" }, {
				"ProjectGroup", "enabled" }, {
				"ProjectPane", "open-in-new-browser" }, {
				"RunConfiguration", "multiple" }, {
				"RunConfiguration", "copy" }, {
				"Servers", "actions-configure" }, {
				"Servers", "action-server-setup" }, {
				"Servers", "enabled" }, {
				"Service", "deploy-enabled" }, {
				"Service", "jdatastore-enabled" }, {
				"Service", "jspservlet-enabled" }, {
				"Service", "naming-enabled" }, {
				"Service", "session-enabled" }, {
				"Service", "transaction-enabled" }, {
				"TagInsight", "enabled" }, {
				"TeamDev", "clearcase-enabled" }, {
				"TeamDev", "select-any-vcs" }, {
				"TeamDev", "starteam-enabled" }, {
				"TeamDev", "vss-enabled" }, {
				"Tools", "database-pilot" }, {
				"Tools", "jdbc-monitor" }, {
				"Web", "action-convert-to-struts" }, {
				"Web", "action-create-jsp" }, {
				"Web", "action-exclude-directory-from-webapp" }, {
				"Web", "actions-struts-structure-view" }, {
				"Web", "actions-struts-tile-definitions" }, {
				"Wizard", "action" }, {
				"Wizard", "actionform" }, {
				"Wizard", "action-bean-insight" }, {
				"Wizard", "dtd-from-xml-generation" }, {
				"Wizard", "dtd-to-xml-generation" }, {
				"Wizard", "databinding-generation" }, {
				"Wizard", "export-to-ant" }, {
				"Wizard", "external-build-task" }, {
				"Wizard", "import-project" }, {
				"Wizard", "import-source" }, {
				"Wizard", "javadoc" }, {
				"Wizard", "jsp" }, {
				"Wizard", "jsp-from-actionform" }, {
				"Wizard", "native-executable-builder" }, {
				"Wizard", "project-group" }, {
				"Wizard", "resource-strings" }, {
				"Wizard", "servlet" }, {
				"Wizard", "struts-converter" }, {
				"Wizard", "use-data-module" }, {
				"Wizard", "web-application" }, {
				"Wizard", "web-start-launcher" }, {
				"XML", "action-generate-dtd-from-xml" }, {
				"XML", "action-generate-java" }, {
				"XML", "action-generate-xml-from-dtd" }, {
				"XML", "action-validate-xml" }, {
				"XML", "catalog-property-pages" }
		});
		v = new String[][] { { "PrimeTime", "hide-if-disabled" }
		};
		eb = (new String[][] { jb, { "Action", "create-ejb-client-jar" }, {
				"Action", "server-deployment" }, {
				"Archiver", "ear" }, {
				"Cocoon", "enabled" }, {
				"EJB", "actions-ejb-1x-bean-designer-methods-page" }, {
				"EJB", "ejb-modeler-viewer" }, {
				"Module", "ejb-enabled" }, {
				"Module", "application-client-enabled" }, {
				"Module", "application-module-enabled" }, {
				"Module", "connector-enabled" }, {
				"Paths", "project-required-library-enabled" }, {
				"Service", "client-jar-enabled" }, {
				"Service", "connector-enabled" }, {
				"Service", "ejb-enabled" }, {
				"Service", "message-enabled" }, {
				"Service", "sqlj-enabled" }, {
				"Tool", "soap-tcp-monitor" }, {
				"Tool", "web-services-console" }, {
				"Tool", "web-services-explorer" }, {
				"UML", "actions-refactorings" }, {
				"UML", "actions-diagram" }, {
				"UML", "enabled" }, {
				"Web", "actions-toolbar" }, {
				"Wizard", "cactus" }, {
				"Wizard", "cocoon-web-app-generation" }, {
				"Wizard", "comparison-fixture" }, {
				"Wizard", "custom-fixture" }, {
				"Wizard", "dtd-to-xml-schema-generation" }, {
				"Wizard", "ear" }, {
				"Wizard", "ejb-1.x-bean-generation" }, {
				"Wizard", "new-ejb-1.x-bean-generation" }, {
				"Wizard", "ejb-1.x-modeler" }, {
				"Wizard", "new-ejb-1.x-modeler" }, {
				"Wizard", "ejb-2.0-designer" }, {
				"Wizard", "new-ejb-2.0-designer" }, {
				"Wizard", "ejb-group" }, {
				"Wizard", "ejb-test-client" }, {
				"Wizard", "new-ejb-test-client" }, {
				"Wizard", "export-as-web-service" }, {
				"Wizard", "export-as-async-web-service" }, {
				"Wizard", "export-to-sql-ddl" }, {
				"Wizard", "idl-enabled" }, {
				"Wizard", "import-visual-cafe" }, {
				"Wizard", "import-web-service" }, {
				"Wizard", "import-web-service-to-generate-java" }, {
				"Wizard", "jdbc-fixture" }, {
				"Wizard", "jdbc-import-schema" }, {
				"Wizard", "jndi-fixture" }, {
				"Wizard", "jms" }, {
				"Wizard", "sax-handler-generation" }, {
				"Wizard", "use-ejb-test-client" }, {
				"Wizard", "new-use-ejb-test-client" }, {
				"Wizard", "web-services-configuration" }, {
				"Wizard", "web-services-designer" }, {
				"Wizard", "web-services-export-async" }, {
				"Wizard", "web-services-export-async-weblogic" }, {
				"Wizard", "web-services-export-class" }, {
				"Wizard", "web-services-export-class-axis" }, {
				"Wizard", "web-services-export-class-weblogic" }, {
				"Wizard", "web-services-import" }, {
				"Wizard", "web-services-import-axis" }, {
				"Wizard", "web-services-import-weblogic" }, {
				"Wizard", "xml-dbms" }, {
				"Wizard", "xml-dbms-sql-script-generator" }, {
				"Wizard", "ejb-menu-wizards" }
		});
		arrays = new HashMap();
	}

	public boolean equals(Object object) {
		if (object == Boolean.FALSE)
			return true;
		if (object == Boolean.TRUE)
			throw new ArrayIndexOutOfBoundsException();
		if (object instanceof String)
			throw new NullPointerException();
		return false;
	}

	public synchronized String toString() {
		throw new RuntimeException();
	}

	protected final String findState(Property property) {
		if (findBoolean(property))
			return Res.s;
		if (this.getBoolean())
			return f();
		return b(property.getPropertyCategory(), property.getPropertyName());
	}

	protected final boolean findBoolean(Property property) {
		if (!(property instanceof JBuilderInfo))
			return searchStringInArrayName(
				property.getPropertyCategory(),
				property.getPropertyName());
		return false;
	}

	private static final String b(String string, String string_1_) {
		if (m())
			return f();
		for (int i = 0; i < eb.length; i++) {
			String[] strings = eb[i];
			if (strings[0].equals(string) && strings[1].equals(string_1_))
				return f();
		}
		return e();
	}

	private static final boolean searchStringInArrayName(String arrayName, String searchString) {
		ArrayList arraylist = (ArrayList) arrays.get(arrayName);
		if (arraylist == null)
			return false;
		return arraylist.contains(searchString);
	}

	private static final void o() throws IOException {
		/* empty */
	}

	public static final void loadSkuMappings() {
		b = 2; //LicenseManager.getSKU();
		arrays.clear();
		ArrayList arraylist = new ArrayList();
		arraylist.addAll(Arrays.asList(fb));
		if (isFoundationOnlyEnabled())
			arraylist.addAll(Arrays.asList(kb));
		else if (isBeq2or5()) {
			arraylist.addAll(Arrays.asList(kb));
			arraylist.addAll(Arrays.asList(w));
			arraylist.addAll(Arrays.asList(eb));
		} else if (m()) {
			arraylist.addAll(Arrays.asList(kb));
			arraylist.addAll(Arrays.asList(w));
			if (!isBeq2or5())
				arraylist.addAll(Arrays.asList(v));
		}
		Iterator iterator = arraylist.iterator();
		while (iterator.hasNext()) {
			String[] strings = (String[]) iterator.next();
			String string = strings[0];
			String string_3_ = strings[1];
			ArrayList arraylist_4_ = (ArrayList) arrays.get(string);
			if (arraylist_4_ == null) {
				arraylist_4_ = new ArrayList();
				arrays.put(string, arraylist_4_);
			}
			arraylist_4_.add(string_3_);
		}
	}

	public void browserExit(int i) {
		LicenseManager.checkin();
	}

	public static void showInfoDialog() {
		LicenseManager.showInfoDialog(1010, getSKU());
	}

	public static final boolean isFeatureVerified(String[] strings) {
		if (strings != null && strings.length == 2)
			return searchStringInArrayName(strings[0], strings[1]);
		return true;
	}

	public static final String getFeatureVerifyReason(String[] strings) {
		if (strings != null && strings.length == 2)
			return b(strings[0], strings[1]);
		return f();
	}

	private static String e() {
		return Strings.format(Res.l, a[3], a[2]);
	}

	private static String f() {
		return Strings.format(Res.q, a[2]);
	}

	public static String getCompanyName() {
		return LicenseManager.getCompanyName();
	}

	public static String getUserName() {
		return LicenseManager.getUserName();
	}

	public static String[] getExtraDescriptions() {
		theVault();
		ArrayList arraylist = new ArrayList();
		if (o)
			arraylist.add(Res.hb);
		return (String[]) arraylist.toArray(new String[arraylist.size()]);
	}

	public static String getSKUDescription() {
		theVault();
		return LicenseManager.getSKUDescription();
	}

	public static String getDescription() {
		return Strings.format(Res.gb, "JBuilder X", getSKUDescription());
	}

	public static String getSKUName(int i) {
		return a[i];
	}

	private static String h() {
		String[] strings =
			{
				"com.borland.jbuilder.server.bes.BES50xServer",
				"com.borland.jbuilder.server.sybase.EAServer",
				"com.borland.jbuilder.enterprise.ejb.weblogic.WebLogic70AppServer",
				"com.borland.jbuilder.enterprise.ejb.websphere.WebSphere40AppServer",
				"com.borland.jbuilder.enterprise.ejb.iplanet.IAS60AppServer" };
		String string = null;
		for (int i = 0; i < strings.length; i++) {
			try {
				com.fourthpass.runtimejb.a.a(strings[i]);
				if (string == null)
					string = strings[i];
				else {
					String string_5_ = "";
					return string_5_;
				}
			} catch (ClassNotFoundException classnotfoundexception) {
				/* empty */
			}
		}
		return string;
	}

	public static int getSKU() {
		if (f != -99)
			return f;
		try {
			Class.forName("com.borland.internetbeans.Binder");
		} catch (ClassNotFoundException classnotfoundexception) {
			f = 0;
			int i = f;
			return i;
		}
		try {
			Class.forName("com.borland.cx.OrbConnect");
		} catch (ClassNotFoundException classnotfoundexception) {
			f = 3;
			int i = f;
			return i;
		}
		String string = h();
		if (string == null) {
			try {
				Class.forName(
					"com.borland.jbuilder.server.tomcat.Tomcat40Server");
				f = 2;
			} catch (ClassNotFoundException classnotfoundexception) {
				f = 0;
			}
		} else if (string.indexOf(".weblogic.") >= 0)
			f = 5;
		else
			f = 2;
		return f;
	}

	private static String firstCharsFromURL(Url url) {
		char[] cs = url.getName().toCharArray();
		int i;
		for (i = 0; i < cs.length; i++) {
			char c = cs[i];
			if (!Character.isLetter(c))
				break;
		}
		return new String(cs, 0, i);
	}

	public static boolean isLibraryEnabled(Url url) {
		theVault();
		if (isFeatureVerifiedJB_if_B_eq_245())
			return true;
		String string = firstCharsFromURL(url).toLowerCase();
		if (string.equals("cx"))
			return false;
		if (string.equals("dxejb"))
			return false;
		if (b == 3 && string.equals("internetbeans"))
			return true;
		if (string.equals("internetbeans"))
			return false;
		return true;
	}

	public static boolean isComponentEnabled(String string) {
		theVault();
		if (isFeatureVerifiedJB_if_B_eq_245())
			return true;
		if (string.startsWith("com.borland.cx."))
			return false;
		if (string.startsWith("com.borland.dx.ejb."))
			return false;
		if (string.startsWith("com.borland.jbuilder.xml.database."))
			return false;
		if (b == 3 && string.startsWith("com.borland.internetbeans."))
			return true;
		if (string.startsWith("com.borland.internetbeans."))
			return false;
		return true;
	}

	public static boolean isAddinEnabled(int i, int i_6_) {
		theVault();
		return LicenseManager.isAddinEnabled(i, i_6_);
	}

	public static String[] getExpansionPackNames() {
		PrimeTime.initializeOpenTools("Info");
		ArrayList arraylist = new ArrayList();
		Iterator iterator = p.keySet().iterator();
		while (iterator.hasNext()) {
			Integer integer = (Integer) iterator.next();
			Integer integer_7_ = (Integer) p.get(integer);
			String string =
				LicenseManager.getAddinIdLabel(
					integer.intValue(),
					integer_7_.intValue());
			if (string != null)
				arraylist.add(string);
		}
		return (String[]) arraylist.toArray(new String[arraylist.size()]);
	}

	public static void registerExpansionPack(int i, int i_8_) {
		p.put(new Integer(i), new Integer(i_8_));
	}

	public static boolean validatePlugin(Object object) {
		if (isBeaEnabled()) {
			String string = object.getClass().getName();
			return (
				(string
					.startsWith("com.borland.jbuilder.enterprise.ejb.weblogic."))
					|| (string
						.startsWith("com.borland.jbuilder.server.weblogic.")));
		}
		return m();
	}

	public static boolean isTermLicense() {
		theVault();
		//return LicenseManager.isTermLicense();
		return false;
	}

	private static boolean isFeatureVerifiedJB_if_B_eq_245() {
		switch (b) {
			case 2 :
			case 4 :
			case 5 :
				return isFeatureVerified(jb);
			default :
				return false;
		}
	}

	public static boolean isGenericPremiumEnabled() {
		theVault();
		return isFeatureVerifiedJB_if_B_eq_245();
	}

	public static boolean isBeaEnabled() {
		theVault();
		return b == 5;
	}

	public static boolean isGenericValueEnabled() {
		theVault();
		return isFeatureVerified(ib) || isGenericPremiumEnabled();
	}

	public static boolean isStudio() {
		theVault();
		if (b == 2 && LicenseManager.isAddinEnabled(10070, 5)) {
			if (d == null) {
				d = Boolean.FALSE;
				try {
					Class.forName(
						"com.togethersoft.platform.progress.ProgressAdapter");
					d = Boolean.TRUE;
				} catch (Exception exception) {
					/* empty */
				}
			}
			return d == Boolean.TRUE;
		}
		return false;
	}

	private static boolean isBeq2or5() {
		theVault();
		switch (b) {
			case 2 :
			case 5 :
				return true;
			default :
				return false;
		}
	}

	private static boolean m() {
		theVault();
		return b == 3 || isBeq2or5();
	}

	public static boolean isFoundationOnlyEnabled() {
		theVault();
		return b == 0;
	}

	public static boolean isTrial() {
		theVault();
		//return LicenseManager.isTrial();
		return false;
	}

	public static boolean isLicensed() {
		theVault();
		return b >= 0;
	}

	private static void theVault() {
		b=2;
		/*
		if (b == -99) {
			LicenseManager.checkKey();
			b = LicenseManager.getSKU();
		}
		*/
	}

	public static void antiAmnesia() {
		/* empty */
	}

	public static void setStatusMsg(String string, int i, int i_9_) {
		Browser browser = Browser.getActiveBrowser();
		if (browser != null)
			browser.getStatusView().setText(string, i, i_9_);
	}

	public static String getRawBuildNumber() {
		Url url =
			PropertyManager.getInstallRootUrl().getRelativeUrl("buildnum.txt");
		InputStream inputstream = null;
		StringBuffer stringbuffer = new StringBuffer();
		try {
			inputstream = VFS.getInputStream(url);
			BufferedReader bufferedreader =
				new BufferedReader(new InputStreamReader(inputstream, "ASCII"));
			for (int i = 0; i < 4; i++) {
				String string = bufferedreader.readLine();
				if (i != 0)
					stringbuffer.append('.');
				stringbuffer.append(string);
			}
		} catch (Exception exception) {
			/* empty */
		} finally {
			if (inputstream != null) {
				try {
					inputstream.close();
				} catch (Exception exception) {
					/* empty */
				}
			}
		}
		if (stringbuffer.length() == 0)
			return "010.000.000.000";
		return stringbuffer.toString();
	}

	public static String getBuildNumber() {
		Url url =
			PropertyManager.getInstallRootUrl().getRelativeUrl("buildnum.txt");
		InputStream inputstream = null;
		StringBuffer stringbuffer = new StringBuffer();
		try {
			inputstream = VFS.getInputStream(url);
			BufferedReader bufferedreader =
				new BufferedReader(new InputStreamReader(inputstream, "ASCII"));
			for (int i = 0; i < 4; i++) {
				int i_10_ = Integer.parseInt(bufferedreader.readLine());
				if (i != 0)
					stringbuffer.append('.');
				stringbuffer.append(i_10_);
			}
		} catch (Exception exception) {
			/* empty */
		} finally {
			if (inputstream != null) {
				try {
					inputstream.close();
				} catch (Exception exception) {
					/* empty */
				}
			}
		}
		if (stringbuffer.length() == 0)
			return "010.000.000.000";
		return stringbuffer.toString();
	}

	public static void initOpenTool(byte i, byte i_11_) {
		JBuilderInfo jbuilderinfo = new JBuilderInfo();
		Browser.addBrowserShutdownListener(jbuilderinfo);
		Browser.setProperty(jbuilderinfo);
		Browser.setBrowserTitle("JBuilder X");
		Browser.setApplicationName("JBuilder");
		Browser.setBrowserIcon(JBuilderIcons.JBUILDER_ICON);
		Browser.setSmallBrowserIcon(JBuilderIcons.JBUILDER_ICON_NO_ALPHA);
		Browser.setApplicationUserHome(
			new Url(new File(JBuilder.getRootDir())));
		UserInformation userinformation =
			new UserInformation(
				getDescription(),
				getUserName(),
				getCompanyName());
		UserInfoPageFactory.registerUserInformation(userinformation);
	}

	public JBuilderInfo() {
		super("ProjectGroup", "enabled");
	}

	public static int applyDegrade() {
		b = 2 ;
		if (s < 0)
			return b;
		switch (b) {
			case 2 :
				return s;
			case 0 :
			case 5 :
				return b;
			default :
				return s == 0 ? 0 : b;
		}
	}
}
