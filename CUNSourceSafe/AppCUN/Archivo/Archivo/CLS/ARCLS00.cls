VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SYSECA Bilbao
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco

' Ahora las ventanas. Continuan la numeraci�n a partir del AR1001
Const SGWinFrmPuntosArchivo         As String = "AR1001"
Const SGWinFrmTipologias            As String = "AR1002"
Const SGWinFrmProcesosHHCC          As String = "AR1003"
Const SGWinFrmBajaHHCC              As String = "AR1004"
Const SGWinFrmUbicacionHHCC         As String = "AR1005"
Const SGWinDlgFiltroProceso         As String = "AR1006"
Const SGWinDlgFiltroUbicacion       As String = "AR1007"
Const SGWinFrmCtaHistorial          As String = "AR1008"
Const SGWinDlgTraslado              As String = "AR1009"
Const SGWinDlgUbicacion             As String = "AR1010"
Const SGWinDlgReubicacion           As String = "AR1011"
Const SGWinDlgDevolucion            As String = "AR1012"
Const SGWinFrmCtaProcesos           As String = "AR1013"
Const SGWinDlgSelCtaProcesos        As String = "AR1014"
Const SGWinFrmCtaPtosUbicacion      As String = "AR1015"
Const SGWinDlgSelCtaPtosUbicacion   As String = "AR1016"
Const SGWinFrmCtaTraslados          As String = "AR1017"
Const SGWinDlgSelCtaTraslados       As String = "AR1018"
Const SGWinFrmCtaBajas              As String = "AR1019"
Const SGWinFrmMotivosSolicitud      As String = "AR1020"
Const SGWinFrmSolicitud             As String = "AR1021"
Const SGWinFrmSolicitudInvestiga    As String = "AR1022"
Const SGWinFrmPrestamos             As String = "AR1023"
Const SGWinFrmEtiqSecuen            As String = "AR1040"
Const SGWinDlgRutaLocalizacion      As String = "AR1024"
Const SGWinFrmCtaPendientesDevol    As String = "AR1025"
Const SGWinDlgSelCtaPendientesDevol As String = "AR1026"
Const SGWinDlgSelEtiquetadoPtosUbi  As String = "AR1027"
Const SGWinDlgIntervalo             As String = "AR1028"
Const SGWinDlgSelEtiquetadoHHCC     As String = "AR1029"
Const SGWinFrmTiposEntrega          As String = "AR1030"
Const SGWinFrmCitas                 As String = "AR1031"
Const SGWinDlgCitas                 As String = "AR1032"

' C�digos de ayuda
'Const CWHelpIDApps          As Integer = 25

' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
'   Case SGWinQueryDelega
'      Load frmCWQueryDelega
'      Call objSecurity.AddHelpContext(CWHelpIDDelega)
'      Call frmCWQueryDelega.Show(vbModal)
'      Call objSecurity.RemoveHelpContext
'      Unload frmCWQueryDelega
'      Set frmCWQueryDelega = Nothing
    Case SGWinFrmPuntosArchivo
      Load frmPuntosArchivo
      Call objSecurity.AddHelpContext(1)
      Call frmPuntosArchivo.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmPuntosArchivo
      Set frmPuntosArchivo = Nothing
    Case SGWinFrmTipologias
      Load frmTipologias
      Call objSecurity.AddHelpContext(3)
      Call frmTipologias.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmTipologias
      Set frmTipologias = Nothing
    Case SGWinFrmProcesosHHCC
      Load frmProcesosHHCC
      Call objSecurity.AddHelpContext(8)
      Call frmProcesosHHCC.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmProcesosHHCC
      Set frmProcesosHHCC = Nothing
    Case SGWinFrmBajaHHCC
      Load frmBajaHHCC
      Call objSecurity.AddHelpContext(11)
      Call frmBajaHHCC.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmBajaHHCC
      Set frmBajaHHCC = Nothing
    Case SGWinFrmUbicacionHHCC
      Load frmUbicacionHHCC
      Call objSecurity.AddHelpContext(12)
      Call frmUbicacionHHCC.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmUbicacionHHCC
      Set frmUbicacionHHCC = Nothing
    Case SGWinDlgFiltroProceso
      Load dlgFiltroProceso
      'Call objSecurity.AddHelpContext
      Call dlgFiltroProceso.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgFiltroProceso
      Set dlgFiltroProceso = Nothing
    Case SGWinDlgFiltroUbicacion
      Load dlgFiltroUbicacion
      'Call objSecurity.AddHelpContext
      Call dlgFiltroUbicacion.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgFiltroUbicacion
      Set dlgFiltroUbicacion = Nothing
    Case SGWinFrmCtaHistorial
      Load frmCtaHistorial
      'Call objSecurity.AddHelpContext
      Call frmCtaHistorial.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmCtaHistorial
      Set frmCtaHistorial = Nothing
    Case SGWinDlgTraslado
      Load dlgTraslado
      'Call objSecurity.AddHelpContext
      Call dlgTraslado.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgTraslado
      Set dlgTraslado = Nothing
    Case SGWinDlgUbicacion
      Load dlgUbicacion
      'Call objSecurity.AddHelpContext
      Call dlgUbicacion.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgUbicacion
      Set dlgUbicacion = Nothing
    Case SGWinDlgReubicacion
      Load dlgReubicacion
      'Call objSecurity.AddHelpContext
      Call dlgReubicacion.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgReubicacion
      Set dlgReubicacion = Nothing
    Case SGWinDlgDevolucion
      Load dlgDevolucion
      'Call objSecurity.AddHelpContext
      Call dlgDevolucion.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgDevolucion
      Set dlgDevolucion = Nothing
    Case SGWinFrmCtaProcesos
      Load frmCtaProcesos
      Call objSecurity.AddHelpContext(28)
      Call frmCtaProcesos.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCtaProcesos
      Set frmCtaProcesos = Nothing
    Case SGWinDlgSelCtaProcesos
      Load dlgSelCtaProcesos
      Call objSecurity.AddHelpContext(27)
      Call dlgSelCtaProcesos.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload dlgSelCtaProcesos
      Set dlgSelCtaProcesos = Nothing
    Case SGWinFrmCtaPtosUbicacion
      Load frmCtaPtosUbicacion
      Call objSecurity.AddHelpContext(15)
      Call frmCtaPtosUbicacion.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCtaPtosUbicacion
      Set frmCtaPtosUbicacion = Nothing
    Case SGWinDlgSelCtaPtosUbicacion
      Load dlgSelCtaPtosUbicacion
      Call objSecurity.AddHelpContext(24)
      Call dlgSelCtaPtosUbicacion.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload dlgSelCtaPtosUbicacion
      Set dlgSelCtaPtosUbicacion = Nothing
    Case SGWinFrmCtaTraslados
      Load frmCtaTraslados
      Call objSecurity.AddHelpContext(16)
      Call frmCtaTraslados.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCtaTraslados
      Set frmCtaTraslados = Nothing
    Case SGWinDlgSelCtaTraslados
      Load dlgSelCtaTraslados
      Call objSecurity.AddHelpContext(23)
      Call dlgSelCtaTraslados.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload dlgSelCtaTraslados
      Set dlgSelCtaTraslados = Nothing
   Case SGWinFrmCtaBajas
      Load frmCtaBajas
      Call objSecurity.AddHelpContext(14)
      Call frmCtaBajas.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCtaBajas
      Set frmCtaBajas = Nothing
   Case SGWinFrmMotivosSolicitud
      Load frmMotivosSolicitud
      Call objSecurity.AddHelpContext(25)
      Call frmMotivosSolicitud.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmMotivosSolicitud
      Set frmMotivosSolicitud = Nothing
   Case SGWinFrmSolicitud
      Dim a
      On Error Resume Next
      a = vntData
      If Err = 0 Then
          glblngNumHC = vntData(1)
      End If
      Load frmSolicitud
      Call objSecurity.AddHelpContext(21)
      Call frmSolicitud.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmSolicitud
      Set frmSolicitud = Nothing
  Case SGWinFrmSolicitudInvestiga
      Load frmSolicitudInvestiga
      Call objSecurity.AddHelpContext(18)
      Call frmSolicitudInvestiga.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmSolicitudInvestiga
      Set frmSolicitudInvestiga = Nothing
  Case SGWinFrmPrestamos
      Load frmPrestamos
      Call objSecurity.AddHelpContext(19)
      Call frmPrestamos.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmPrestamos
      Set frmPrestamos = Nothing
  Case SGWinDlgRutaLocalizacion
      Load dlgRutaLocalizacion
      Call objSecurity.AddHelpContext(13)
      Call dlgRutaLocalizacion.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload dlgRutaLocalizacion
      Set dlgRutaLocalizacion = Nothing
  Case SGWinFrmCtaPendientesDevol
      Load frmCtaPendientesDevol
      Call objSecurity.AddHelpContext(20)
      Call frmCtaPendientesDevol.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCtaPendientesDevol
      Set frmCtaPendientesDevol = Nothing
  Case SGWinDlgSelCtaPendientesDevol
      Load dlgSelCtaPendientesDevol
      Call objSecurity.AddHelpContext(22)
      Call dlgSelCtaPendientesDevol.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload dlgSelCtaPendientesDevol
      Set dlgSelCtaPendientesDevol = Nothing
  Case SGWinDlgSelEtiquetadoPtosUbi
      Load dlgSelEtiquetadoPtosUbi
      Call objSecurity.AddHelpContext(7)
      Call dlgSelEtiquetadoPtosUbi.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload dlgSelEtiquetadoPtosUbi
      Set dlgSelEtiquetadoPtosUbi = Nothing
  Case SGWinDlgIntervalo
      Load dlgIntervalo
      'Call objSecurity.AddHelpContext
      Call dlgIntervalo.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgIntervalo
      Set dlgIntervalo = Nothing
  Case SGWinDlgSelEtiquetadoHHCC
      Load dlgSelEtiquetadoHHCC
      Call objSecurity.AddHelpContext(6)
      Call dlgSelEtiquetadoHHCC.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload dlgSelEtiquetadoHHCC
      Set dlgSelEtiquetadoHHCC = Nothing
  Case SGWinFrmTiposEntrega
      Load frmTiposEntrega
      Call objSecurity.AddHelpContext(2)
      Call frmTiposEntrega.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmTiposEntrega
      Set frmTiposEntrega = Nothing
  Case SGWinFrmCitas
      Load frmCitas
      Call objSecurity.AddHelpContext(17)
      Call frmCitas.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCitas
      Set frmCitas = Nothing
  Case SGWinDlgCitas
      Load dlgCitas
      'Call objSecurity.AddHelpContext
      Call dlgCitas.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload dlgCitas
      Set dlgCitas = Nothing
  Case SGWinFrmEtiqSecuen
      Load frmEtiqueSecuen
      Call frmEtiqueSecuen.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmEtiqueSecuen
      Set frmEtiqueSecuen = Nothing
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  Call Err.Clear
End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 32 procesos
  ReDim aProcess(1 To 33, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = SGWinFrmPuntosArchivo
  aProcess(1, 2) = "Mantenimiento de Puntos de Archivo"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = SGWinFrmTipologias
  aProcess(2, 2) = "Mantenimiento de Tipolog�as"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = SGWinFrmProcesosHHCC
  aProcess(3, 2) = "Procesos de Archivo"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = SGWinFrmBajaHHCC
  aProcess(4, 2) = "Baja de HHCC"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = SGWinFrmUbicacionHHCC
  aProcess(5, 2) = "Ubicacion HHCC"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = SGWinDlgFiltroProceso
  aProcess(6, 2) = "Filtro de Procesos y Bajas de HHCC"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = SGWinDlgFiltroUbicacion
  aProcess(7, 2) = "Filtro de Ubicacion de HHCC"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = SGWinFrmCtaHistorial
  aProcess(8, 2) = "Consulta de Procesos de una HC"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
            
  aProcess(9, 1) = SGWinDlgTraslado
  aProcess(9, 2) = "Traslado de HHCC"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = SGWinDlgUbicacion
  aProcess(10, 2) = "Ubicaci�n de HHCC"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
      
  aProcess(11, 1) = SGWinDlgReubicacion
  aProcess(11, 2) = "Reubicaci�n de HHCC"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
      
  aProcess(12, 1) = SGWinDlgDevolucion
  aProcess(12, 2) = "Devoluci�n de HHCC"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
      
  aProcess(13, 1) = SGWinFrmCtaProcesos
  aProcess(13, 2) = "Consulta de Procesos de Archivo"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow
      
  aProcess(14, 1) = SGWinDlgSelCtaProcesos
  aProcess(14, 2) = "Consulta de Procesos de Archivo"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
      
  aProcess(15, 1) = SGWinFrmCtaPtosUbicacion
  aProcess(15, 2) = "Consulta de Puntos de Ubicaci�n"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
      
  aProcess(16, 1) = SGWinDlgSelCtaPtosUbicacion
  aProcess(16, 2) = "Consulta de E/S en Puntos de Ubicaci�n"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
      
  aProcess(17, 1) = SGWinFrmCtaTraslados
  aProcess(17, 2) = "Consulta de Traslados"
  aProcess(17, 3) = False
  aProcess(17, 4) = cwTypeWindow
      
  aProcess(18, 1) = SGWinDlgSelCtaTraslados
  aProcess(18, 2) = "Consulta de Traslados"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
      
  aProcess(19, 1) = SGWinFrmCtaBajas
  aProcess(19, 2) = "Consulta de Bajas de HHCC"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
      
  aProcess(20, 1) = SGWinFrmMotivosSolicitud
  aProcess(20, 2) = "Mantenimiento de Motivos de Solicitud"
  aProcess(20, 3) = True
  aProcess(20, 4) = cwTypeWindow
      
  aProcess(21, 1) = SGWinFrmSolicitud
  aProcess(21, 2) = "Solicitud de Pr�stamo"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
      
  aProcess(22, 1) = SGWinFrmSolicitudInvestiga
  aProcess(22, 2) = "Solicitud de Pr�stamo para Investigaci�n"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = SGWinFrmPrestamos
  aProcess(23, 2) = "Pr�stamos de HHCC"
  aProcess(23, 3) = True
  aProcess(23, 4) = cwTypeWindow
  
  aProcess(24, 1) = SGWinDlgRutaLocalizacion
  aProcess(24, 2) = "Ruta de Localizaci�n"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeWindow

  aProcess(25, 1) = SGWinFrmCtaPendientesDevol
  aProcess(25, 2) = "Consulta HHCC Pendientes de Devoluci�n"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = SGWinDlgSelCtaPendientesDevol
  aProcess(26, 2) = "Consulta HHCC Pendientes de Devoluci�n"
  aProcess(26, 3) = True
  aProcess(26, 4) = cwTypeWindow

  aProcess(27, 1) = SGWinDlgSelEtiquetadoPtosUbi
  aProcess(27, 2) = "Etiquetado de Puntos de Ubicaci�n"
  aProcess(27, 3) = True
  aProcess(27, 4) = cwTypeWindow

  aProcess(28, 1) = SGWinDlgIntervalo
  aProcess(28, 2) = "Intervalo de Tiempo de Refresco"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow

  aProcess(29, 1) = SGWinDlgSelEtiquetadoHHCC
  aProcess(29, 2) = "Etiquetado de Historias Cl�nicas"
  aProcess(29, 3) = True
  aProcess(29, 4) = cwTypeWindow

  aProcess(30, 1) = SGWinFrmTiposEntrega
  aProcess(30, 2) = "Preferencias para el Pr�stamo de Citas"
  aProcess(30, 3) = True
  aProcess(30, 4) = cwTypeWindow

  aProcess(31, 1) = SGWinFrmCitas
  aProcess(31, 2) = "Mantenimiento de Citas"
  aProcess(31, 3) = True
  aProcess(31, 4) = cwTypeWindow

  aProcess(32, 1) = SGWinDlgCitas
  aProcess(32, 2) = "Listado de Citas"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeWindow
  
  aProcess(33, 1) = SGWinFrmEtiqSecuen
  aProcess(33, 2) = "Etiquetas Secuenciales"
  aProcess(33, 3) = True
  aProcess(33, 4) = cwTypeWindow
  
End Sub
