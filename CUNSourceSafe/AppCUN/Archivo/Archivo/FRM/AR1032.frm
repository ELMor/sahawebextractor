VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form dlgCitas 
   Caption         =   "ARCHIVO. Listado de Citas"
   ClientHeight    =   1410
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7125
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   ScaleHeight     =   1410
   ScaleWidth      =   7125
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Ordenaci�n"
      Height          =   975
      Left            =   120
      TabIndex        =   5
      Top             =   180
      Width           =   5235
      Begin VB.OptionButton optOrdenar 
         Caption         =   "N�mero H.C."
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Width           =   1275
      End
      Begin VB.OptionButton optOrdenar 
         Caption         =   "Departamento"
         Height          =   315
         Index           =   1
         Left            =   1620
         TabIndex        =   1
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton optOrdenar 
         Caption         =   "Ruta de Localizaci�n"
         Height          =   315
         Index           =   2
         Left            =   3180
         TabIndex        =   2
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   5520
      TabIndex        =   3
      Top             =   240
      Width           =   1455
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5520
      TabIndex        =   4
      Top             =   780
      Width           =   1455
   End
   Begin Crystal.CrystalReport crtCitas 
      Left            =   4860
      Top             =   900
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      Destination     =   1
      PrintFileLinesPerPage=   60
   End
End
Attribute VB_Name = "dlgCitas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Declaracion de variables

Public dlgCitasstrPuntoArchivo As String
Public dlgCitasstrCodDepartamento As String
Public dlgCitasstrFechaDesde As String
Public dlgCitasstrFechaHasta As String

Private Sub cmdCancelar_Click()
Unload Me
End Sub

Private Sub cmdImprimir_Click()
Dim strCamino As String
Dim SQLReport As String
camino = Trim(App.Path)
If Right(camino, 1) = "\" Then
    camino = Left(camino, Len(camino) - 1)
End If
      

'Por Numero HC y Tipolog�a
If optOrdenar(0).Value = True Then
    
    SQLReport = " SELECT "
    SQLReport = SQLReport & " AR0500.CI22NUMHISTORIA, AR0500.AR00CODTIPOLOGIA, AR0500.AR01CODMOTIVOSOL, AR0500.AR05FECFINSOLICITUD, "
    SQLReport = SQLReport & " AD0200.AD02DESDPTO, "
    SQLReport = SQLReport & " CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, "
    SQLReport = SQLReport & " SG0200.SG02NOM, SG0200.SG02APE1, SG0200.SG02APE2 "
    SQLReport = SQLReport & " FROM "
    SQLReport = SQLReport & " AR0500, AD0200, CI2200, SG0200 "
    SQLReport = SQLReport & " WHERE "
    SQLReport = SQLReport & " AR0500.AR02CODPTOARCHIVO= '" & dlgCitasstrPuntoArchivo & "' "
    SQLReport = SQLReport & " AND AR0500.AR01CODMOTIVOSOL='C' "
    SQLReport = SQLReport & " AND AR0500.AR05FECFINSOLICITUD IS NULL "
    SQLReport = SQLReport & " AND TRUNC(AR0500.AR05FECSOLICITUD) >= TO_DATE('" & dlgCitasstrFechaDesde & "','DD/MM/YYYY') "
    SQLReport = SQLReport & " AND TRUNC(AR0500.AR05FECSOLICITUD) <= TO_DATE('" & dlgCitasstrFechaHasta & "','DD/MM/YYYY') "
    'SQLReport = SQLReport & " AND TRUNC(AR0500.AR05FECSOLICITUD) >= '" & dlgCitasstrFechaDesde & "' "
    'SQLReport = SQLReport & " AND to_char(AR0500.AR05FECSOLICITUD,'DD/MM/YYYY') <= '" & dlgCitasstrFechaHasta & "' "
    If dlgCitasstrCodDepartamento <> "" Then
        SQLReport = SQLReport & " AND AR0500.AD02CODDPTO= '" & dlgCitasstrCodDepartamento & "' "
    End If
    SQLReport = SQLReport & " AND CI2200.CI22NUMHISTORIA=AR0500.CI22NUMHISTORIA "
    SQLReport = SQLReport & " AND AD0200.AD02CODDPTO=AR0500.AD02CODDPTO "
    SQLReport = SQLReport & " AND SG0200.SG02COD=AR0500.SG02COD "
    SQLReport = SQLReport & " ORDER BY AR0500.CI22NUMHISTORIA,AR0500.AR00CODTIPOLOGIA"
    'ENVIAR LA CONSULTA AL REPORT
   crtCitas.ReportFileName = camino & PATH_REPORTS & "\CITASHC.RPT"
   crtCitas.Connect = objApp.rdoConnect.Connect
   crtCitas.SQLQuery = SQLReport
   crtCitas.Action = 1
    If crtCitas.LastErrorNumber > 0 Then
        Call MsgBox("Se han producido Errores durante la Impresi�n", vbOKOnly + vbInformation, "Aviso")
        crtCitas.LastErrorNumber = 0
        Exit Sub
    End If
'Por Departamento, Numero HC y Tipolog�a
ElseIf optOrdenar(1).Value = True Then
    SQLReport = " SELECT "
    SQLReport = SQLReport & " AR0500.CI22NUMHISTORIA, AR0500.AR00CODTIPOLOGIA, AR0500.AR01CODMOTIVOSOL, AR0500.AR05FECFINSOLICITUD, "
    SQLReport = SQLReport & " AD0200.AD02DESDPTO, "
    SQLReport = SQLReport & " CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, "
    SQLReport = SQLReport & " SG0200.SG02NOM, SG0200.SG02APE1, SG0200.SG02APE2 "
    SQLReport = SQLReport & " FROM "
    SQLReport = SQLReport & " AR0500, AD0200, CI2200, SG0200 "
    SQLReport = SQLReport & " WHERE "
    SQLReport = SQLReport & " AR0500.AR02CODPTOARCHIVO= '" & dlgCitasstrPuntoArchivo & "' "
    SQLReport = SQLReport & " AND AR0500.AR01CODMOTIVOSOL='C' "
    SQLReport = SQLReport & " AND AR0500.AR05FECFINSOLICITUD IS NULL "
    SQLReport = SQLReport & " AND TRUNC(AR0500.AR05FECSOLICITUD) >= TO_DATE('" & dlgCitasstrFechaDesde & "','DD/MM/YYYY') "
    SQLReport = SQLReport & " AND TRUNC(AR0500.AR05FECSOLICITUD) <= TO_DATE('" & dlgCitasstrFechaHasta & "','DD/MM/YYYY') "
    'SQLReport = SQLReport & " AND to_char(AR0500.AR05FECSOLICITUD,'DD/MM/YYYY') >= '" & dlgCitasstrFechaDesde & "' "
    'SQLReport = SQLReport & " AND to_char(AR0500.AR05FECSOLICITUD,'DD/MM/YYYY') <= '" & dlgCitasstrFechaHasta & "' "
    If dlgCitasstrCodDepartamento <> "" Then
        SQLReport = SQLReport & " AND AR0500.AD02CODDPTO= '" & dlgCitasstrCodDepartamento & "' "
    End If
    SQLReport = SQLReport & " AND CI2200.CI22NUMHISTORIA=AR0500.CI22NUMHISTORIA "
    SQLReport = SQLReport & " AND AD0200.AD02CODDPTO=AR0500.AD02CODDPTO "
    SQLReport = SQLReport & " AND SG0200.SG02COD=AR0500.SG02COD "
    SQLReport = SQLReport & " ORDER BY AD0200.AD02DESDPTO,SG0200.SG02APE1,SG0200.SG02APE2,AR0500.CI22NUMHISTORIA,AR0500.AR00CODTIPOLOGIA"
    'ENVIAR LA CONSULTA AL REPORT
   crtCitas.ReportFileName = camino & PATH_REPORTS & "\CITASD.RPT"
   crtCitas.Connect = objApp.rdoConnect.Connect
   crtCitas.SQLQuery = SQLReport
   crtCitas.Action = 1
    If crtCitas.LastErrorNumber > 0 Then
        Call MsgBox("Se han producido Errores durante la Impresi�n", vbOKOnly + vbInformation, "Aviso")
        crtCitas.LastErrorNumber = 0
        Exit Sub
    End If
'Por L�nea y Pto. Ubicaci�n
Else
    SQLReport = " SELECT "
    SQLReport = SQLReport & " AR0500.CI22NUMHISTORIA, AR0500.AR00CODTIPOLOGIA, AR0500.AR01CODMOTIVOSOL, AR0500.AR05FECFINSOLICITUD, "
    SQLReport = SQLReport & " AD0200.AD02DESDPTO, "
    SQLReport = SQLReport & " CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, "
    SQLReport = SQLReport & " SG0200.SG02NOM, SG0200.SG02APE1, SG0200.SG02APE2, "
    SQLReport = SQLReport & " AR0400.AR03CODLINEA, AR0400.AR04NUMHUECO "
    SQLReport = SQLReport & " FROM "
    SQLReport = SQLReport & " AR0500, AD0200, CI2200, SG0200, AR0400 "
    SQLReport = SQLReport & " WHERE "
    SQLReport = SQLReport & " AR0500.AR02CODPTOARCHIVO= '" & dlgCitasstrPuntoArchivo & "' "
    SQLReport = SQLReport & " AND AR0500.AR01CODMOTIVOSOL='C' "
    SQLReport = SQLReport & " AND AR0500.AR05FECFINSOLICITUD IS NULL "
    'SQLReport = SQLReport & " AND to_char(AR0500.AR05FECSOLICITUD,'DD/MM/YYYY') >= '" & dlgCitasstrFechaDesde & "' "
    'SQLReport = SQLReport & " AND to_char(AR0500.AR05FECSOLICITUD,'DD/MM/YYYY') <= '" & dlgCitasstrFechaHasta & "' "
    SQLReport = SQLReport & " AND TRUNC(AR0500.AR05FECSOLICITUD) >= TO_DATE('" & dlgCitasstrFechaDesde & "','DD/MM/YYYY') "
    SQLReport = SQLReport & " AND TRUNC(AR0500.AR05FECSOLICITUD) <= TO_DATE('" & dlgCitasstrFechaHasta & "','DD/MM/YYYY') "
    If dlgCitasstrCodDepartamento <> "" Then
        SQLReport = SQLReport & " AND AR0500.AD02CODDPTO= '" & dlgCitasstrCodDepartamento & "' "
    End If
    SQLReport = SQLReport & " AND CI2200.CI22NUMHISTORIA=AR0500.CI22NUMHISTORIA "
    SQLReport = SQLReport & " AND AD0200.AD02CODDPTO=AR0500.AD02CODDPTO "
    SQLReport = SQLReport & " AND SG0200.SG02COD=AR0500.SG02COD "
    SQLReport = SQLReport & " AND AR0400.CI22NUMHISTORIA=AR0500.CI22NUMHISTORIA "
    SQLReport = SQLReport & " AND AR0400.AR00CODTIPOLOGIA=AR0500.AR00CODTIPOLOGIA "
    SQLReport = SQLReport & " AND AR0400.AR04FECFIN IS NULL "
    SQLReport = SQLReport & " ORDER BY AR0400.AR03CODLINEA, AR0400.AR04NUMHUECO, AR0400.CI22NUMHISTORIA "
    
    'ENVIAR LA CONSULTA AL REPORT
   crtCitas.ReportFileName = camino & PATH_REPORTS & "\CITASL.RPT"
   crtCitas.Connect = objApp.rdoConnect.Connect
   crtCitas.SQLQuery = SQLReport
   crtCitas.Action = 1
    If crtCitas.LastErrorNumber > 0 Then
        Call MsgBox("Se han producido Errores durante la Impresi�n", vbOKOnly + vbInformation, "Aviso")
        crtCitas.LastErrorNumber = 0
        Exit Sub
    End If
End If
End Sub

Private Sub Form_Load()
optOrdenar(0).Value = True
End Sub

