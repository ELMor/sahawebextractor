VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form dlgSelCtaPendientesDevol 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Seleccionar Consulta de Historias Cl�nicas Pendientes de Devoluci�n"
   ClientHeight    =   6960
   ClientLeft      =   2085
   ClientTop       =   1335
   ClientWidth     =   6990
   ControlBox      =   0   'False
   HelpContextID   =   22
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6960
   ScaleWidth      =   6990
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   22
      Left            =   5220
      TabIndex        =   25
      Top             =   3480
      Width           =   1575
   End
   Begin VB.Frame Frame2 
      Caption         =   "Tipo de Pr�stamo"
      Height          =   2355
      Left            =   180
      TabIndex        =   23
      Top             =   1560
      Width           =   4815
      Begin VB.ListBox lstMotivo 
         Height          =   1230
         ItemData        =   "AR1026.frx":0000
         Left            =   600
         List            =   "AR1026.frx":0002
         MultiSelect     =   1  'Simple
         TabIndex        =   5
         Top             =   960
         Width           =   3615
      End
      Begin VB.CheckBox chkInvestigacion 
         Caption         =   "Investigaci�n"
         Height          =   375
         Left            =   3120
         TabIndex        =   4
         Top             =   240
         Width           =   1335
      End
      Begin VB.CheckBox chkCita 
         Caption         =   "Cita"
         Height          =   375
         Left            =   1920
         TabIndex        =   3
         Top             =   240
         Width           =   855
      End
      Begin VB.CheckBox chkUrgente 
         Caption         =   "Urgente"
         Height          =   375
         Left            =   480
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Motivos de Solicitud"
         Height          =   255
         Left            =   600
         TabIndex        =   24
         Top             =   720
         Width           =   1575
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Ordenaci�n"
      Height          =   1815
      Left            =   180
      TabIndex        =   22
      Top             =   4950
      Width           =   6615
      Begin VB.ListBox lstColumnaA 
         Height          =   1230
         ItemData        =   "AR1026.frx":0004
         Left            =   240
         List            =   "AR1026.frx":0006
         TabIndex        =   8
         Top             =   360
         Width           =   1935
      End
      Begin VB.CommandButton cmdAsc 
         Caption         =   "Ascendente"
         Height          =   375
         Left            =   2400
         TabIndex        =   9
         Top             =   300
         Width           =   1215
      End
      Begin VB.CommandButton cmdDes 
         Caption         =   "Descendente"
         Height          =   375
         Left            =   2400
         TabIndex        =   10
         Top             =   780
         Width           =   1215
      End
      Begin VB.CommandButton cmdEliminar 
         Caption         =   "Eliminar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   2400
         TabIndex        =   11
         Top             =   1260
         Width           =   1215
      End
      Begin VB.CommandButton cmdUp 
         Enabled         =   0   'False
         Height          =   375
         Left            =   5940
         Picture         =   "AR1026.frx":0008
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   540
         Width           =   435
      End
      Begin VB.CommandButton cmdDown 
         Enabled         =   0   'False
         Height          =   375
         Left            =   5940
         Picture         =   "AR1026.frx":0312
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   1020
         Width           =   435
      End
      Begin VB.ListBox lstColumnaB 
         Height          =   1230
         ItemData        =   "AR1026.frx":061C
         Left            =   3840
         List            =   "AR1026.frx":061E
         TabIndex        =   12
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.ComboBox cboDpto 
      Height          =   315
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   1080
      Width           =   4815
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de Pr�stamo"
      Height          =   855
      Left            =   180
      TabIndex        =   17
      Top             =   4000
      Width           =   4815
      Begin SSCalendarWidgets_A.SSDateCombo datDesde 
         Height          =   375
         Left            =   840
         TabIndex        =   6
         Top             =   300
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin SSCalendarWidgets_A.SSDateCombo datHasta 
         Height          =   375
         Left            =   3120
         TabIndex        =   7
         Top             =   300
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin VB.Label Label6 
         Caption         =   "Desde"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   2520
         TabIndex        =   18
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.ComboBox cboArchivo 
      BackColor       =   &H00FFFF00&
      Height          =   315
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   4815
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5220
      TabIndex        =   16
      Top             =   2640
      Width           =   1575
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   5220
      TabIndex        =   15
      Top             =   1920
      Width           =   1575
   End
   Begin VB.Label Label9 
      Caption         =   "Departamento"
      Height          =   255
      Left            =   180
      TabIndex        =   21
      Top             =   840
      Width           =   1335
   End
   Begin VB.Label Label8 
      Caption         =   "Punto de Archivo"
      Height          =   255
      Left            =   180
      TabIndex        =   20
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "dlgSelCtaPendientesDevol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim columnas_lst As New Collection
Dim columnas_orderby As New Collection
Dim columnasA As New Collection
Dim columnasB As New Collection
Dim columnasB_orden As New Collection
Dim ordenando As Boolean

Dim c As rdoConnection
Dim q As rdoQuery
Dim r As rdoResultset
Dim puntos As New Collection
Dim dptos As New Collection
Dim motivos As New Collection
Sub rellenar_orden()
  Dim colA As Integer
  Dim colB As Integer
  Dim i As Integer
  Dim j As Integer
  Dim encontrado As Boolean
  ordenando = True
  'guardar posiciones
  colA = lstColumnaA.ListIndex
  colB = lstColumnaB.ListIndex
  'vaciar listas
  lstColumnaA.Clear
  lstColumnaB.Clear
  'vaciar columnas A
  Set columnasA = Nothing
  'cargar lista A
  For i = 1 To columnas_lst.Count
    encontrado = False
    For j = 1 To columnasB.Count
      If columnasB(j) = i Then
        encontrado = True
        Exit For
      End If
    Next j
    If Not encontrado Then
      columnasA.Add (i)
      lstColumnaA.AddItem (columnas_lst(i))
    End If
  Next i
  'cargar lista B
  For i = 1 To columnasB.Count
    lstColumnaB.AddItem (columnas_lst(columnasB(i)) & " (" & columnasB_orden(i) & ")")
  Next i
  'volver a posicionar en las listas
  If colA >= lstColumnaA.ListCount Then
    colA = lstColumnaA.ListCount - 1
  End If
  lstColumnaA.ListIndex = colA
  If colB >= lstColumnaB.ListCount Then
    colB = lstColumnaB.ListCount - 1
  End If
  lstColumnaB.ListIndex = colB
  'des/habilitar botones
  If lstColumnaA.ListCount = 0 Or lstColumnaA.ListIndex = -1 Then
    cmdAsc.Enabled = False
    cmdDes.Enabled = False
  Else
    cmdAsc.Enabled = True
    cmdDes.Enabled = True
  End If
  If lstColumnaB.ListCount = 0 Or lstColumnaB.ListIndex = -1 Then
    cmdEliminar.Enabled = False
  Else
    cmdEliminar.Enabled = True
  End If
  If lstColumnaB.ListCount < 2 Or lstColumnaB.ListIndex = -1 Then
    cmdUp.Enabled = False
    cmdDown.Enabled = False
  Else
    cmdUp.Enabled = True
    cmdDown.Enabled = True
  End If
  ordenando = False
End Sub

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 22)
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdConsultar_Click()
  'declaraciones
  Dim correcto As Boolean
  Dim sql As String
  Dim haymotivos As Boolean
  Dim i As Integer
  Dim primero As Boolean
  'comprobaciones
  correcto = True
  If cboArchivo.ListIndex = -1 Then
    MsgBox "Debe especificar el Punto de Archivo", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    correcto = False
    cboArchivo.SetFocus
  End If
  If datDesde.FormattedText = "" Or datHasta.FormattedText = "" Then
    MsgBox "Debe especificar el rango de Fechas del Pr�stamo", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    correcto = False
  End If
  If correcto And datDesde.FormattedText <> "" And Not IsDate(datDesde.FormattedText) Then
    MsgBox "El contenido del campo Fecha Desde no es una fecha correcta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    correcto = False
    datDesde.SetFocus
  End If
  If correcto And datHasta.FormattedText <> "" And Not IsDate(datHasta.FormattedText) Then
    MsgBox "El contenido del campo Fecha Hasta no es una fecha correcta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
    correcto = False
    datHasta.SetFocus
  End If
  If correcto And datDesde.FormattedText <> "" And datHasta.FormattedText <> "" Then
    If DateValue(datDesde.Date) > DateValue(datHasta.Date) Then
      MsgBox "Fecha Desde debe ser menor que Fecha Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Consulta cancelada"
      correcto = False
      datDesde.SetFocus
    End If
  End If
  If correcto Then  'comprobaciones superadas
    sql = "SELECT "
    sql = sql & "D.AD02DESDPTO AS ""Departamento Solicitante"", "
    sql = sql & "M.AR01DESMOTIVOSOL AS ""Tipo Pr�stamo"", "
    sql = sql & "V.AR04FECINICIO AS ""Fecha Pr�stamo"", "
    sql = sql & "V.AR04HORAINICIO AS ""Hora Pr�stamo"", "
    sql = sql & "V.SG02DOCTORSOL AS ""Destinatario"", "
    sql = sql & "V.CI22NUMHISTORIA AS ""N�mero H.C."", "
    sql = sql & "T.AR00DESTIPOLOGIA AS ""Tipolog�a"", "
    sql = sql & "V.AR05OBSERVACIO AS ""Observaciones"", "
    sql = sql & "V.AR07FECMVTO AS ""Fecha Movimiento"", "
    sql = sql & "D2.AD02DESDPTO AS ""Departamento Movimiento"", "
    sql = sql & "V.SG02DOCTORMVTO AS ""Destinatario Movimiento"", "
    sql = sql & "A.AR02DESPTOARCHIVO AS ""Punto Archivo"" "
    sql = sql & "FROM "
    sql = sql & "AR0200 A, "
    sql = sql & "AR0100 M, "
    sql = sql & "AD0200 D2, "
    sql = sql & "AD0200 D, "
    sql = sql & "AR0000 T, "
    sql = sql & "AR0501J V "
    sql = sql & "WHERE "
    sql = sql & "V.AR02CODPTOARCHIVO = '" & puntos(cboArchivo.ListIndex + 1) & "' AND "
    If datDesde.FormattedText <> "" Or datHasta.FormattedText <> "" Then
      sql = sql & "(("
    End If
    If datDesde.FormattedText <> "" Then
      sql = sql & "TRUNC(V.AR04FECINICIO) >= TO_DATE ('" & datDesde.Date & "', 'DD/MM/YYYY') "
    End If
    If datDesde.FormattedText <> "" And datHasta.FormattedText <> "" Then
      sql = sql & "AND "
    End If
    If datHasta.FormattedText <> "" Then
      sql = sql & "TRUNC(V.AR04FECINICIO) <= TO_DATE ('" & datHasta.Date & "', 'DD/MM/YYYY') "
    End If
    If datDesde.FormattedText <> "" Or datHasta.FormattedText <> "" Then
      sql = sql & ") OR ("
    End If
    If datDesde.FormattedText <> "" Then
      sql = sql & "TRUNC(V.AR07FECMVTO) >= TO_DATE ('" & datDesde.Date & "', 'DD/MM/YYYY') "
    End If
    If datDesde.FormattedText <> "" And datHasta.FormattedText <> "" Then
      sql = sql & "AND "
    End If
    If datHasta.FormattedText <> "" Then
      sql = sql & "TRUNC(V.AR07FECMVTO) <= TO_DATE ('" & datHasta.Date & "', 'DD/MM/YYYY') "
    End If
    If datDesde.FormattedText <> "" Or datHasta.FormattedText <> "" Then
      sql = sql & ")) AND "
    End If
    If cboDpto.ListIndex <> -1 And cboDpto.ListIndex < cboDpto.ListCount - 1 Then
      sql = sql & "(V.AD02CODDPTOSOL = '" & dptos(cboDpto.ListIndex + 1) & "' OR "
      sql = sql & "V.AD02CODDPTOMVTO = '" & dptos(cboDpto.ListIndex + 1) & "') AND "
    End If
    haymotivos = False
    For i = 0 To lstMotivo.ListCount - 1
      If lstMotivo.Selected(i) Then
        haymotivos = True
      End If
    Next i
    If haymotivos Or chkUrgente.Value = Checked Or chkCita.Value = Checked Or chkInvestigacion.Value = Checked Then
      sql = sql & "V.AR01CODMOTIVOSOL IN ("
      primero = True
      If chkUrgente.Value = Checked Then
        sql = sql & "'U'"
        primero = False
      End If
      If chkCita.Value = Checked Then
        If Not primero Then
          sql = sql & ", "
        End If
        sql = sql & "'C'"
        primero = False
      End If
      If chkInvestigacion.Value = Checked Then
        If Not primero Then
          sql = sql & ", "
        End If
        sql = sql & "'I'"
        primero = False
      End If
      If haymotivos Then
        For i = 0 To lstMotivo.ListCount - 1
          If lstMotivo.Selected(i) Then
            If Not primero Then
              sql = sql & ", "
            End If
            primero = False
            sql = sql & "'" & motivos(i + 1) & "'"
          End If
        Next i
      End If
      sql = sql & ") AND "
    End If
    sql = sql & "T.AR00CODTIPOLOGIA = V.AR00CODTIPOLOGIA AND "
    sql = sql & "M.AR01CODMOTIVOSOL = V.AR01CODMOTIVOSOL AND "
    sql = sql & "D.AD02CODDPTO = V.AD02CODDPTOSOL AND "
    sql = sql & "D2.AD02CODDPTO(+) = V.AD02CODDPTOMVTO AND "
    sql = sql & "A.AR02CODPTOARCHIVO = V.AR02CODPTOARCHIVO "
    If columnasB.Count = 0 Then
      sql = sql & " ORDER BY 1, 2, 3, 5"
    Else
      sql = sql & " ORDER BY " & columnas_orderby(columnasB(1)) & " " & columnasB_orden(1)
      For i = 2 To columnasB.Count
        sql = sql & ", " & columnas_orderby(columnasB(i)) & " " & columnasB_orden(i)
      Next i
    End If
    'mostrar resultados consulta
    Screen.MousePointer = vbHourglass
    frmCtaPendientesDevol.sql = sql
    frmCtaPendientesDevol.Show vbModal
    Set frmCtaPendientesDevol = Nothing
  End If
End Sub

Private Sub Form_Load()
  'declaraciones
  Dim i As Integer
  'inicializaciones
  Set c = objApp.rdoConnect
  ordenando = False
  datDesde.Date = Date
  datHasta.Date = Date
  'cargar puntos de archivo
  Set q = c.CreateQuery("", "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO")
  Set r = q.OpenResultset
  cboArchivo.Clear
  Set puntos = Nothing
  Do Until r.EOF
    cboArchivo.AddItem CStr(r(1))
    puntos.Add CStr(r(0))
    r.MoveNext
  Loop
  r.Close
  q.Close
  'cargar departamentos
  Set q = c.CreateQuery("", "SELECT AD02CODDPTO, AD02DESDPTO FROM AD0200 ORDER BY AD02DESDPTO")
  Set r = q.OpenResultset
  cboDpto.Clear
  Set dptos = Nothing
  Do Until r.EOF
    cboDpto.AddItem CStr(r(1))
    dptos.Add CStr(r(0))
    r.MoveNext
  Loop
  cboDpto.AddItem "<ninguno>"
  cboDpto.ListIndex = cboDpto.ListCount - 1
  r.Close
  q.Close
  'cargar motivos
  Set q = c.CreateQuery("", "SELECT AR01CODMOTIVOSOL, AR01DESMOTIVOSOL FROM AR0100 WHERE AR01CODMOTIVOSOL NOT IN ('I', 'C', 'U') ORDER BY AR01DESMOTIVOSOL")
  Set r = q.OpenResultset
  lstMotivo.Clear
  Set motivos = Nothing
  While Not r.EOF
    lstMotivo.AddItem r(1)
    motivos.Add CStr(r(0))
    r.MoveNext
  Wend
  r.Close
  q.Close
  'cargar columnas
  Set columnas_lst = Nothing
  Set columnas_orderby = Nothing
  columnas_lst.Add "N�mero de H.C."
  columnas_orderby.Add "6"
  columnas_lst.Add "Tipolog�a"
  columnas_orderby.Add "7"
  columnas_lst.Add "Fecha Pr�stamo"
  columnas_orderby.Add "3"
  columnas_lst.Add "Departamento"
  columnas_orderby.Add "1"
  columnas_lst.Add "Destinatario"
  columnas_orderby.Add "5"
  columnas_lst.Add "Tipo de Pr�stamo"
  columnas_orderby.Add "2"
  Set columnasB = Nothing
  Set columnasB_orden = Nothing
  Call rellenar_orden
End Sub
Private Sub cmdUp_Click()
  Dim i As Integer
  Dim col As Integer
  Dim cols As New Collection
  Dim cols_orden As New Collection
  If lstColumnaB.ListIndex > 0 Then
    'columnasB -> cols y columnasB_orden -> cols_orden
    For i = 1 To columnasB.Count
      cols.Add (columnasB(i))
      cols_orden.Add (columnasB_orden(i))
    Next i
    'calcular col
    col = lstColumnaB.ListIndex + 1
    'vaciar columnasB, columnasB_orden
    For i = columnasB.Count To 1 Step -1
      columnasB.Remove (i)
      columnasB_orden.Remove (i)
    Next i
    'cols -> columnasB y cols_orden -> columnasB_orden (intercambiando col por col - 1)
    For i = 1 To cols.Count
      If i = col - 1 Then
        columnasB.Add (cols(col))
        columnasB_orden.Add (cols_orden(col))
      ElseIf i = col Then
        columnasB.Add (cols(col - 1))
        columnasB_orden.Add (cols_orden(col - 1))
      Else
        columnasB.Add (cols(i))
        columnasB_orden.Add (cols_orden(i))
      End If
    Next i
    lstColumnaB.ListIndex = lstColumnaB.ListIndex - 1
    Call rellenar_orden
  End If
End Sub
Private Sub cmdDown_Click()
  Dim i As Integer
  Dim col As Integer
  Dim cols As New Collection
  Dim cols_orden As New Collection
  If lstColumnaB.ListIndex < lstColumnaB.ListCount - 1 Then
    'columnasB -> cols y columnasB_orden -> cols_orden
    For i = 1 To columnasB.Count
      cols.Add (columnasB(i))
      cols_orden.Add (columnasB_orden(i))
    Next i
    'calcular col
    col = lstColumnaB.ListIndex + 1
    'vaciar columnasB, columnasB_orden
    For i = columnasB.Count To 1 Step -1
      columnasB.Remove (i)
      columnasB_orden.Remove (i)
    Next i
    'cols -> columnasB y cols_orden -> columnasB_orden (intercambiando col por col - 1)
    For i = 1 To cols.Count
      If i = col Then
        columnasB.Add (cols(col + 1))
        columnasB_orden.Add (cols_orden(col + 1))
      ElseIf i = col + 1 Then
        columnasB.Add (cols(col))
        columnasB_orden.Add (cols_orden(col))
      Else
        columnasB.Add (cols(i))
        columnasB_orden.Add (cols_orden(i))
      End If
    Next i
    lstColumnaB.ListIndex = lstColumnaB.ListIndex + 1
    Call rellenar_orden
  End If
End Sub
Private Sub lstColumnaB_Click()
  If lstColumnaB.ListIndex <> -1 Then
    cmdAsc.Enabled = False
    cmdDes.Enabled = False
    cmdEliminar.Enabled = True
    If lstColumnaB.ListCount >= 2 Then
      cmdUp.Enabled = True
      cmdDown.Enabled = True
    End If
  End If
End Sub
Private Sub lstColumnaB_DblClick()
  Call cmdEliminar_Click
End Sub
Private Sub lstColumnaB_GotFocus()
  lstColumnaA.ListIndex = -1
End Sub
Private Sub lstColumnaA_Click()
  If lstColumnaA.ListIndex <> -1 Then
    cmdAsc.Enabled = True
    cmdDes.Enabled = True
    cmdEliminar.Enabled = False
    cmdUp.Enabled = False
    cmdDown.Enabled = False
  End If
End Sub
Private Sub lstColumnaA_DblClick()
  Call cmdAsc_Click
End Sub
Private Sub lstColumnaA_GotFocus()
  lstColumnaB.ListIndex = -1
End Sub
Private Sub cmdAsc_Click()
  If lstColumnaA.ListIndex <> -1 Then
    columnasB.Add columnasA(lstColumnaA.ListIndex + 1)
    columnasB_orden.Add "ASC"
    Call rellenar_orden
  End If
End Sub
Private Sub cmdDes_Click()
  If lstColumnaA.ListIndex <> -1 Then
    columnasB.Add columnasA(lstColumnaA.ListIndex + 1)
    columnasB_orden.Add "DESC"
    Call rellenar_orden
  End If
End Sub
Private Sub cmdEliminar_Click()
  If lstColumnaB.ListIndex <> -1 Then
    columnasB.Remove lstColumnaB.ListIndex + 1
    columnasB_orden.Remove lstColumnaB.ListIndex + 1
    Call rellenar_orden
  End If
End Sub
