VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form dlgRutaLocalizacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Listado de Ruta de Localización de Historias Clínicas"
   ClientHeight    =   2535
   ClientLeft      =   2340
   ClientTop       =   2325
   ClientWidth     =   6930
   ControlBox      =   0   'False
   HelpContextID   =   13
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2535
   ScaleWidth      =   6930
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   13
      Left            =   5280
      TabIndex        =   9
      Top             =   1920
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5280
      TabIndex        =   6
      Top             =   1320
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   5280
      TabIndex        =   5
      Top             =   360
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Ordenación"
      Height          =   975
      Left            =   240
      TabIndex        =   8
      Top             =   1320
      Width           =   4815
      Begin VB.CheckBox Check1 
         Caption         =   "Departamento"
         Height          =   255
         Index           =   5
         Left            =   2760
         TabIndex        =   4
         Top             =   480
         Width           =   1575
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Ruta de Localización"
         Height          =   255
         Index           =   4
         Left            =   600
         TabIndex        =   3
         Top             =   480
         Width           =   1815
      End
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Otros"
      Enabled         =   0   'False
      Height          =   255
      Index           =   3
      Left            =   4020
      TabIndex        =   2
      Top             =   600
      Width           =   735
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Investigación"
      Enabled         =   0   'False
      Height          =   255
      Index           =   2
      Left            =   2160
      TabIndex        =   1
      Top             =   600
      Width           =   1335
   End
   Begin VB.Frame PSELRUTA 
      Caption         =   "Tipo de Pedido"
      Height          =   855
      Left            =   240
      TabIndex        =   7
      Top             =   240
      Width           =   4815
      Begin VB.CheckBox Check1 
         Caption         =   "Urgentes"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   0
         Top             =   360
         Width           =   1095
      End
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Bindings        =   "AR1024.frx":0000
      Left            =   4560
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      Destination     =   1
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
End
Attribute VB_Name = "dlgRutaLocalizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public cllHCTip As New Collection
Public dlgRLstrPuntoArchivo As String
Dim sql As String
Dim SQL2 As String

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 13)
End Sub

Private Sub Command1_Click()
Dim camino As String
Dim inti As Integer
Dim strNumHC As String
Dim strCodTipologia As String
Dim strMotivoSol As String
Dim sql As String

camino = Trim(App.Path)
      If Right(camino, 1) = "\" Then
        camino = Left(camino, Len(camino) - 1)
      End If

Dim HCTIP As String

On Error Resume Next

If Check1(0).Value = Unchecked And _
  Check1(2).Value = Unchecked And _
  Check1(3).Value = Unchecked Then
  MsgBox "Debe especificar un Tipo de Pedido", vbOKOnly + vbInformation, "Aviso"
Else
  If Check1(4).Value = Unchecked And _
    Check1(5).Value = Unchecked Then
    MsgBox "Debe especificar una Ordenación", vbOKOnly + vbInformation, "Aviso"
  Else
   'LEER LA COLECCION
    HCTIP = ""
    For inti = 1 To cllHCTip.Count
        strNumHC = Trim(Left(cllHCTip(inti), InStr(1, cllHCTip(inti), "-") - 1))
        strCodTipologia = Trim(Right(cllHCTip(inti), (Len(cllHCTip(inti)) - (Len(strNumHC) + 1))))
        If cllHCTip.Count = 1 Then
            HCTIP = " AND  P.CI22NUMHISTORIA='" & strNumHC & "' AND P.AR00CODTIPOLOGIA='" & strCodTipologia & "' "
        Else
            If inti = 1 Then
                HCTIP = " AND (( P.CI22NUMHISTORIA='" & strNumHC & "' AND P.AR00CODTIPOLOGIA='" & strCodTipologia & "')"
            Else
                HCTIP = HCTIP & " OR ( P.CI22NUMHISTORIA='" & strNumHC & "' AND P.AR00CODTIPOLOGIA='" & strCodTipologia & "')"
            End If
        End If
    Next
    If cllHCTip.Count > 1 Then
        HCTIP = HCTIP & ") "
    End If
    
    'Obtener el motivo de la solicitud
    strMotivoSol = ""
    If Check1(0).Value = 1 Then
        strMotivoSol = "'U'"
    End If
    If Check1(2).Value = 1 Then
        If strMotivoSol <> "" Then
            strMotivoSol = strMotivoSol & ",'I'"
        Else
            strMotivoSol = "'I'"
        End If
    End If
    If Check1(3).Value = 1 Then
        If strMotivoSol <> "" Then
            strMotivoSol = strMotivoSol & ",'O'"
        Else
            strMotivoSol = "'O'"
        End If
    End If
    sql = " SELECT H.AR03CODLINEA, H.AR04NUMHUECO, H.CI22NUMHISTORIA, "
    sql = sql & " H.AR00CODTIPOLOGIA, D.AD02DESDPTO, "
    sql = sql & " TO_CHAR(P.AR05FECSOLICITUD,'DD/MM/YYYY HH24:MI:SS'), "
    sql = sql & " M.AR01DESMOTIVOSOL "
    sql = sql & " FROM AR0100 M, SG0200 S, AD0200 D, CI2200 C, AR0400 H, AR0500 P "
    sql = sql & " WHERE P.AR02CODPTOARCHIVO=" & dlgRLstrPuntoArchivo & " "
    sql = sql & " AND P.AR01CODMOTIVOSOL !='C' "
    sql = sql & " AND P.AR05FECFINSOLICITUD IS NULL "
    sql = sql & HCTIP
    sql = sql & "AND H.CI22NUMHISTORIA=P.CI22NUMHISTORIA "
    sql = sql & " AND H.AR00CODTIPOLOGIA  = P.AR00CODTIPOLOGIA "
    sql = sql & " AND H.AR04FECFIN IS NULL "
    sql = sql & " AND C.CI22NUMHISTORIA= H.CI22NUMHISTORIA "
    sql = sql & " AND D.AD02CODDPTO   = P.AD02CODDPTO "
    sql = sql & " AND S.SG02COD(+)=P.SG02COD "
    sql = sql & " AND M.AR01CODMOTIVOSOL= P.AR01CODMOTIVOSOL"
    

    CrystalReport1.Connect = objApp.rdoConnect.Connect
    CrystalReport1.ReportFileName = camino & PATH_REPORTS & "\Ruta.rpt"
    'CAMBIAMOS LAS PROPIEDADES PARA QUE VAYA A IMPRESORA
    CrystalReport1.Destination = crptToPrinter
    CrystalReport1.PrintFileName = ""
    
    
    If Check1(4).Value = 1 And Check1(5).Value = 1 Then
    
        SQL2 = sql & "  ORDER BY H.AR03CODLINEA,H.AR04NUMHUECO, H.CI22NUMHISTORIA,H.AR00CODTIPOLOGIA"
        sql = sql & "  ORDER BY D.AD02DESDPTO, H.AR03CODLINEA, H.AR04NUMHUECO, H.CI22NUMHISTORIA "
    
        CrystalReport1.SQLQuery = sql
        CrystalReport1.Action = 1
        
    
        If CrystalReport1.LastErrorNumber > 0 Then
            Call MsgBox("Se han producido Errores durante la Impresión", vbOKOnly + vbInformation, "Aviso")
            CrystalReport1.LastErrorNumber = 0
            Exit Sub

        Else

            If CrystalReport1.Status = 3 Then
                CrystalReport1.SQLQuery = SQL2
                CrystalReport1.Action = 1
                If CrystalReport1.LastErrorNumber > 0 Then
                    Call MsgBox("Se han producido Errores durante la Impresión", vbOKOnly + vbInformation, "Aviso")
                    CrystalReport1.LastErrorNumber = 0
                    Exit Sub
                End If
            End If
    End If

    ElseIf Check1(4).Value = 1 Then
        sql = sql & "  ORDER BY H.AR03CODLINEA,H.AR04NUMHUECO, H.CI22NUMHISTORIA,H.AR00CODTIPOLOGIA "
        CrystalReport1.SQLQuery = sql
        CrystalReport1.Action = 1

    ElseIf Check1(5).Value = 1 Then
        sql = sql & "  ORDER BY D.AD02DESDPTO, H.AR03CODLINEA, H.AR04NUMHUECO , H.CI22NUMHISTORIA "
        CrystalReport1.SQLQuery = sql
        CrystalReport1.Action = 1
    Else
        sql = sql & "  ORDER BY H.AR03CODLINEA,H.AR04NUMHUECO , H.CI22NUMHISTORIA,H.AR00CODTIPOLOGIA "
        CrystalReport1.SQLQuery = sql
        CrystalReport1.Action = 1
    

    End If

    If CrystalReport1.LastErrorNumber > 0 Then
        MsgBox CrystalReport1.LastErrorNumber & " " & CrystalReport1.LastErrorString
        Call MsgBox("Se han producido Errores durante la Impresión" & Chr(13) & CrystalReport1.LastErrorString, vbOKOnly + vbInformation, "Aviso")
    Else
      If Check1(0).Value = 1 Then
          frmPrestamos.bolHCTipUrge = False
      End If
      If Check1(2).Value = 1 Then
          frmPrestamos.bolHCTipInve = False
      End If
      If Check1(3).Value = 1 Then
          frmPrestamos.bolHCTipOtro = False
      End If
    End If

  End If
End If
End Sub

Private Sub Command2_Click()
Unload Me
End Sub


Private Sub Form_Load()

End Sub
