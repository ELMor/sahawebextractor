VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form dlgSelEtiquetadoHHCC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Etiquetado de Historias Cl�nicas"
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6990
   ControlBox      =   0   'False
   HelpContextID   =   6
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   6990
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAyuda 
      Caption         =   "&Ayuda"
      Height          =   375
      HelpContextID   =   6
      Left            =   5340
      TabIndex        =   17
      Top             =   3480
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Historias Cl�nicas"
      Height          =   1455
      Left            =   180
      TabIndex        =   9
      Top             =   840
      Width           =   4935
      Begin VB.TextBox txtHCD 
         BackColor       =   &H00FFFF00&
         Height          =   315
         Left            =   120
         MaxLength       =   7
         TabIndex        =   1
         Top             =   480
         Width           =   735
      End
      Begin VB.TextBox txtHCH 
         BackColor       =   &H00FFFF00&
         Height          =   315
         Left            =   1080
         MaxLength       =   7
         TabIndex        =   2
         Top             =   480
         Width           =   735
      End
      Begin VB.ListBox lstTipologia 
         Height          =   840
         ItemData        =   "Ar1029.frx":0000
         Left            =   2040
         List            =   "Ar1029.frx":0002
         MultiSelect     =   1  'Simple
         TabIndex        =   3
         Top             =   480
         Width           =   2775
      End
      Begin VB.Label Label3 
         Caption         =   "Desde"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   1080
         TabIndex        =   13
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Tipolog�a"
         Height          =   255
         Left            =   2040
         TabIndex        =   14
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.ComboBox cboArchivo 
      BackColor       =   &H00FFFF00&
      Height          =   315
      Left            =   180
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   4935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fecha de Revisi�n"
      Height          =   2355
      Left            =   180
      TabIndex        =   10
      Top             =   2520
      Width           =   4935
      Begin VB.TextBox txtHoraHasta 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   3120
         TabIndex        =   19
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox txtHoraDesde 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   1200
         TabIndex        =   18
         Top             =   1800
         Width           =   615
      End
      Begin VB.CheckBox chkRevision 
         Caption         =   "En Revisi�n"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   360
         Width           =   1335
      End
      Begin SSCalendarWidgets_A.SSDateCombo datDesde 
         Height          =   375
         Left            =   1200
         TabIndex        =   5
         Top             =   1020
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483644
         Enabled         =   0   'False
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin SSCalendarWidgets_A.SSDateCombo datHasta 
         Height          =   375
         Left            =   3120
         TabIndex        =   6
         Top             =   1020
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   93
         BackColor       =   -2147483644
         Enabled         =   0   'False
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
      End
      Begin VB.Label lblHora 
         Caption         =   "Hora:"
         Height          =   255
         Left            =   360
         TabIndex        =   23
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label lblFecha 
         Caption         =   "Fecha:"
         Height          =   255
         Left            =   360
         TabIndex        =   22
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label lblHoraHasta 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   3120
         TabIndex        =   21
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label lblHoraDesde 
         Caption         =   "Desde"
         Height          =   255
         Left            =   1200
         TabIndex        =   20
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label lblHasta 
         Caption         =   "Hasta"
         Height          =   255
         Left            =   3120
         TabIndex        =   16
         Top             =   780
         Width           =   495
      End
      Begin VB.Label lblDesde 
         Caption         =   "Desde"
         Height          =   255
         Left            =   1200
         TabIndex        =   15
         Top             =   780
         Width           =   495
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5340
      TabIndex        =   8
      Top             =   2340
      Width           =   1455
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   5340
      TabIndex        =   7
      Top             =   1740
      Width           =   1455
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Bindings        =   "Ar1029.frx":0004
      Left            =   5880
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      Destination     =   1
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      PrintFileLinesPerPage=   60
   End
   Begin VB.Label Label8 
      Caption         =   "Punto de Archivo"
      Height          =   255
      Left            =   180
      TabIndex        =   11
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "dlgSelEtiquetadoHHCC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim puntos As New Collection
Dim tipologias As New Collection

Private Sub cmdAyuda_Click()
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 6)
End Sub

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub cmdImprimir_Click()
  'declaraciones
  Dim correcto As Boolean
  Dim sql As String
  Dim haytipologias As Boolean
  Dim i As Integer
  Dim camino As String
  'comprobaciones
  correcto = True
  'pto archivo
  If correcto And cboArchivo.ListIndex = -1 Then
    MsgBox "Debe especificar un Punto de Archivo", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
    cboArchivo.SetFocus
    correcto = False
  End If
  If correcto Then
    If chkRevision.Value = Unchecked Then
      If txtHCD.Text = "" Or txtHCH.Text = "" Then
        MsgBox "Debe especificar un rango de Historias Cl�nicas o de Fechas de Revisi�n", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
        txtHCD.SetFocus
        correcto = False
      End If
    Else
      If txtHCD.Text = "" And txtHCH.Text = "" Then
        If datDesde.FormattedText = "" Or datHasta.FormattedText = "" Then
          MsgBox "Debe especificar un rango de Historias Cl�nicas o de Fechas de Revisi�n", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
          txtHCD.SetFocus
          correcto = False
        End If
      Else
        If (txtHCD.Text = "" Or txtHCH.Text = "") And (datDesde.FormattedText = "" Or datHasta.FormattedText = "") Then
          MsgBox "Debe especificar un rango de Historias Cl�nicas o de Fechas de Revisi�n", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
          txtHCD.SetFocus
          correcto = False
        End If
      End If
    End If
  End If
  If correcto And txtHCD.Text <> "" And txtHCH.Text <> "" And Val(txtHCD.Text) > Val(txtHCH.Text) Then
    MsgBox "La Historia Cl�nica Desde debe ser menor que la Historia Cl�nica Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
    txtHCD.SetFocus
    correcto = False
  End If
  If chkRevision = Checked And datDesde.FormattedText <> "" And datHasta.FormattedText <> "" Then
    If DateValue(datDesde.Date) > DateValue(datHasta.Date) Then
      MsgBox "La Fecha Desde debe ser menor que la Fecha Hasta", vbOKOnly + vbExclamation + vbApplicationModal, "Impresi�n cancelada"
      datDesde.SetFocus
      correcto = False
    End If
  End If
  If correcto Then
    sql = "SELECT H.CI22NUMHISTORIA, H.AR00CODTIPOLOGIA, "
    sql = sql & "T.AR00LETRAETIQUETA, C.CI22PRIAPEL, "
    sql = sql & "C.CI22SEGAPEL , C.CI22NOMBRE, C.CI22FECNACIM "
    sql = sql & "FROM AR0000 T, CI2200 C, AR0400 H "
    sql = sql & "WHERE "
    If txtHCD.Text <> "" Then
      sql = sql & "H.CI22NUMHISTORIA >= " & txtHCD.Text & " AND "
    End If
    If txtHCH.Text <> "" Then
      sql = sql & "H.CI22NUMHISTORIA <= " & txtHCH.Text & " AND "
    End If
    haytipologias = False
    For i = 0 To lstTipologia.ListCount - 1
      If lstTipologia.Selected(i) Then
        haytipologias = True
      End If
    Next i
    If haytipologias Then
      sql = sql & "H.AR00CODTIPOLOGIA IN ("
      For i = 0 To lstTipologia.ListCount - 1
        If lstTipologia.Selected(i) Then
          If Not haytipologias Then
            sql = sql & ", "
          End If
          haytipologias = False
          sql = sql & "'" & tipologias(i + 1) & "'"
        End If
      Next i
      sql = sql & ") AND "
    End If
    sql = sql & "H.AR06CODESTADO = 'R' AND "
    If chkRevision.Value = Checked Then
      If datDesde.FormattedText <> "" Then
        sql = sql & "TRUNC (H.AR04FECINICIO) >= TO_DATE ('" & datDesde.Date & "', 'DD/MM/YYYY') AND "
      End If
      If datHasta.FormattedText <> "" Then
        sql = sql & "TRUNC (H.AR04FECINICIO) <= TO_DATE ('" & datHasta.Date & "', 'DD/MM/YYYY') AND "
      End If
    End If
    If txtHoraDesde.Text <> "" Then
       sql = sql & "to_char(H.ar04fecinicio,'HH24:MI') >= '" & txtHoraDesde.Text & "' AND "
    End If
    If txtHoraHasta.Text <> "" Then
       sql = sql & "to_char(H.ar04fecinicio,'HH24:MI')  <= '" & txtHoraHasta.Text & "' AND "
    End If
    sql = sql & "H.AR02CODPTOARCHIVO = '" & puntos(cboArchivo.ListIndex + 1) & "' AND "
    sql = sql & "H.AR04FECFIN IS NULL AND "
    sql = sql & "C.CI22NUMHISTORIA(+) = H.CI22NUMHISTORIA AND "
    sql = sql & "T.AR00CODTIPOLOGIA = H.AR00CODTIPOLOGIA "
    sql = sql & " ORDER BY H.CI22NUMHISTORIA, H.AR00CODTIPOLOGIA"
    camino = Trim(App.Path)
    If Right(camino, 1) = "\" Then
      camino = Left(camino, Len(camino) - 1)
    End If
    CrystalReport1.ReportFileName = camino & PATH_REPORTS & "\ETIQ_HHCC.RPT"
    CrystalReport1.Connect = objApp.rdoConnect.Connect
    CrystalReport1.SQLQuery = sql
    CrystalReport1.Action = 1
  End If
End Sub

Private Sub chkRevision_Click()
  If chkRevision.Value = Checked Then
    datDesde.Enabled = True
    datDesde.BackColor = &HFFFF00
    datHasta.Enabled = True
    datHasta.BackColor = &HFFFF00
    txtHoraDesde.Enabled = True
    txtHoraDesde.BackColor = &H80000005
    txtHoraHasta.Enabled = True
    txtHoraHasta.BackColor = &H80000005
  Else
    datDesde.Enabled = False
    datDesde.BackColor = &H80000004
    datHasta.Enabled = False
    datHasta.BackColor = &H80000004
    txtHoraDesde.Enabled = True
    txtHoraDesde.BackColor = &H80000004
    txtHoraHasta.Enabled = True
    txtHoraHasta.BackColor = &H80000004
  End If
End Sub

Private Sub Form_Load()
  'declaraciones
  Dim c As rdoConnection
  Dim q As rdoQuery
  Dim r As rdoResultset
  'inicializaciones
  datDesde.Date = Date
  datHasta.Date = Date
  'cargar ptos archivo
  Set c = objApp.rdoConnect
  Set q = c.CreateQuery("", "SELECT AR02CODPTOARCHIVO, AR02DESPTOARCHIVO FROM AR0200 ORDER BY AR02DESPTOARCHIVO")
  Set r = q.OpenResultset
  Set puntos = Nothing
  Do Until r.EOF
    cboArchivo.AddItem CStr(r(1))
    puntos.Add CStr(r(0))
    r.MoveNext
  Loop
  r.Close
  q.Close
  'cargar tipologias
  Set q = c.CreateQuery("", "SELECT AR00CODTIPOLOGIA, AR00DESTIPOLOGIA FROM AR0000 ORDER BY AR00DESTIPOLOGIA")
  Set r = q.OpenResultset
  While Not r.EOF
    lstTipologia.AddItem r(1)
    tipologias.Add CStr(r(0))
    r.MoveNext
  Wend
  r.Close
  q.Close
End Sub

Private Sub txtHCD_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

Private Sub txtHCH_KeyPress(KeyAscii As Integer)
  If (KeyAscii < 48 Or KeyAscii > 57) Then
    If KeyAscii <> 8 Then
      KeyAscii = 13
    End If
  End If
End Sub

Private Sub txtHoraDesde_LostFocus()
  If txtHoraDesde.Text <> "" Then
    If Not Horavalida(txtHoraDesde.Text) Then
      MsgBox "Hora no v�lida", vbOKOnly + vbExclamation + vbApplicationModal, "Hora incorrecta"
      txtHoraDesde.SetFocus
    End If
  End If
End Sub

Private Sub txtHoraHasta_LostFocus()
  If txtHoraHasta.Text <> "" Then
    If Not Horavalida(txtHoraHasta.Text) Then
      MsgBox "Hora no v�lida", vbOKOnly + vbExclamation + vbApplicationModal, "Hora incorrecta"
      txtHoraHasta.SetFocus
    End If
  End If
End Sub
