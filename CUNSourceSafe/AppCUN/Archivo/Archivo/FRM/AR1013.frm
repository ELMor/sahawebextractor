VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "MSRDC20.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmCtaProcesos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ARCHIVO. Consulta de Procesos"
   ClientHeight    =   7215
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11610
   ClipControls    =   0   'False
   HelpContextID   =   28
   Icon            =   "AR1013.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7215
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   11
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "espacio"
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "Imprimir"
            Object.ToolTipText     =   "Imprimir"
            Object.Tag             =   ""
            ImageKey        =   "print"
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "espacio"
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "Primer Registro"
            Object.ToolTipText     =   "Primero"
            Object.Tag             =   ""
            ImageKey        =   "first"
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "Registro Anterior"
            Object.ToolTipText     =   "Anterior"
            Object.Tag             =   ""
            ImageKey        =   "previous"
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "Siguiente Registro"
            Object.ToolTipText     =   "Siguiente"
            Object.Tag             =   ""
            ImageKey        =   "next"
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "Ultimo Registro"
            Object.ToolTipText     =   "Ultimo"
            Object.Tag             =   ""
            ImageKey        =   "last"
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "espacio"
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Actualizar"
            Object.ToolTipText     =   "Actualizar"
            Object.Tag             =   ""
            ImageKey        =   "refresh"
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
            Style           =   3
            MixedState      =   -1  'True
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Description     =   "Salir"
            Object.ToolTipText     =   "Salir"
            Object.Tag             =   ""
            ImageKey        =   "exit"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   330
      Left            =   4800
      Top             =   360
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   582
      _Version        =   393216
      Options         =   0
      CursorDriver    =   0
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   1
      LockType        =   3
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid Grid 
      Bindings        =   "AR1013.frx":0442
      Height          =   6495
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   11370
      ScrollBars      =   3
      _Version        =   131078
      BevelColorFace  =   -2147483638
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      SelectByCell    =   -1  'True
      MaxSelectedRows =   0
      ForeColorEven   =   -2147483630
      ForeColorOdd    =   -2147483630
      BackColorEven   =   -2147483633
      BackColorOdd    =   -2147483633
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20055
      _ExtentY        =   11456
      _StockProps     =   79
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Bindings        =   "AR1013.frx":0453
      Left            =   3960
      Top             =   240
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      Destination     =   1
      PrintFileLinesPerPage=   60
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   6480
      Top             =   240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":0463
            Key             =   "new"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":077D
            Key             =   "open"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":0A97
            Key             =   "save"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":0DB1
            Key             =   "print"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":10CB
            Key             =   "delete"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":13E5
            Key             =   "cut"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":16FF
            Key             =   "copy"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":1A19
            Key             =   "paste"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":1D33
            Key             =   "refresh"
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":204D
            Key             =   "find"
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":2367
            Key             =   "filon"
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":2681
            Key             =   "filof"
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":299B
            Key             =   "first"
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":2CB5
            Key             =   "previous"
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":2FCF
            Key             =   "next"
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":32E9
            Key             =   "last"
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":3603
            Key             =   "undo"
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":391D
            Key             =   "maint"
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "AR1013.frx":3C37
            Key             =   "exit"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Enabled         =   0   'False
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Enabled         =   0   'False
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "frmCtaProcesos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public sql As String
Dim primero As Boolean
Dim rsConsulta As rdoResultset
Dim QyConsulta As rdoQuery
Private Sub Form_Activate()
  Grid.Columns(0).Alignment = ssCaptionAlignmentRight
  Grid.Columns(0).Width = 1100  'hc
  Grid.Columns(1).Width = 1300  'tipologia
  Grid.Columns(2).Width = 1000  'FECHA
  Grid.Columns(3).Alignment = ssCaptionAlignmentCenter
  Grid.Columns(3).Width = 1200  'hora proc
  Grid.Columns(4).Width = 1400  'tipo proc
  Grid.Columns(5).Width = 1225  'linea
  Grid.Columns(6).Width = 1225  'hueco
  Grid.Columns(7).Width = 2300  'dev dpto
  Grid.Columns(8).Width = 2300  'prestado pto
  Grid.Columns(9).Width = 2800  'prestado destinatario
  Grid.Columns(10).Width = 1400  'tipo prestamo
  Grid.Columns(11).Width = 2800  'pto archivo
  Grid.Columns(12).Width = 2800  'observ
  Grid.Columns(13).Visible = False  'pto archivo
  Screen.MousePointer = vbDefault
End Sub
Private Sub Form_Load()
  'declaraciones
  'inicializaciones
  primero = True
  MSRDC1.Connect = objApp.rdoConnect.Connect
  'cargar datos
  Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
  Set rsConsulta = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
  Set MSRDC1.Resultset = rsConsulta
 'MSRDC1.sql = sql
 'MSRDC1.Refresh
End Sub

Private Sub mnuAyudaOpcion_Click(Index As Integer)
Dim h As Integer
    h = WinHelp(hwnd, (App.HelpFile), &H1, 28)
End Sub

Private Sub mnuDatosOpcion_Click(Index As Integer)
  Dim camino As String
  Select Case Index
    Case 80   'imprimir
      camino = Trim(App.Path)
      If Right(camino, 1) = "\" Then
        camino = Left(camino, Len(camino) - 1)
      End If
     CrystalReport1.ReportFileName = camino & PATH_REPORTS & "\CTAPROHC.RPT"
      CrystalReport1.Connect = objApp.rdoConnect.Connect
      CrystalReport1.SQLQuery = sql
      CrystalReport1.Action = 1
    Case 100  'salir
      Unload Me
  End Select
End Sub
Private Sub mnuOpcionesOpcion_Click(Index As Integer)
  MSRDC1.Refresh
  primero = True
End Sub
Private Sub mnuRegistroOpcion_Click(Index As Integer)
  Select Case Index
    Case 40
      MSRDC1.Resultset.MoveFirst
    Case 50
      MSRDC1.Resultset.MovePrevious
      If MSRDC1.Resultset.BOF Then
        MSRDC1.Resultset.MoveFirst
      End If
    Case 60
      MSRDC1.Resultset.MoveNext
      If MSRDC1.Resultset.EOF Then
        MSRDC1.Resultset.MoveLast
      End If
    Case 70
      MSRDC1.Resultset.MoveLast
  End Select
  If Index = 40 Or Index = 50 Or Index = 60 Or Index = 70 Then
    If primero Then
      primero = False
      mnuRegistroOpcion_Click (Index)
    End If
  End If
End Sub
Private Sub tlbToolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Select Case Button.Index
  Case 2  'imprimir
    mnuDatosOpcion_Click (80)
  Case 4  'primero
    mnuRegistroOpcion_Click (40)
  Case 5  'anterior
    mnuRegistroOpcion_Click (50)
  Case 6  'siguiente
    mnuRegistroOpcion_Click (60)
  Case 7  'ultimo
    mnuRegistroOpcion_Click (70)
  Case 9  'actualizar
    mnuOpcionesOpcion_Click (10)
  Case 11 'salir
    mnuDatosOpcion_Click (100)
End Select
End Sub

