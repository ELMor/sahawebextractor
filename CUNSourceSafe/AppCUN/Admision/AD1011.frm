VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPacientesIngrNW 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pacientes Ingresados"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11610
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6360
   ScaleWidth      =   11610
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraConsulta 
      Caption         =   "Pacientes Ingresados"
      ForeColor       =   &H8000000D&
      Height          =   4740
      Index           =   2
      Left            =   0
      TabIndex        =   8
      Top             =   1500
      Width           =   11595
      Begin VsOcxLib.VideoSoftAwk Awk1 
         Left            =   10680
         Top             =   960
         _Version        =   327680
         _ExtentX        =   847
         _ExtentY        =   847
         _StockProps     =   0
      End
      Begin SSDataWidgets_B.SSDBGrid grdIngresos 
         Height          =   4425
         Left            =   60
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   240
         Width           =   11415
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   8
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   8
         Columns(0).Width=   1826
         Columns(0).Caption=   "F.Ingreso"
         Columns(0).Name =   "F.Ingreso"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   6535
         Columns(1).Caption=   "Paciente"
         Columns(1).Name =   "Paciente"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1508
         Columns(2).Caption=   "C. Planta"
         Columns(2).Name =   "Habitacion"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   1111
         Columns(3).Caption=   "C. Quir"
         Columns(3).Name =   "Quirofano"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2064
         Columns(4).Caption=   "Dpto."
         Columns(4).Name =   "Dpto."
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   1429
         Columns(5).Caption=   "N� H�"
         Columns(5).Name =   "N� Historia"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3493
         Columns(6).Caption=   "Doctor"
         Columns(6).Name =   "Doctor"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   1746
         Columns(7).Caption=   "T.Eco./Entid"
         Columns(7).Name =   "T.Eco."
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   20135
         _ExtentY        =   7805
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraConsulta 
      Caption         =   "Filtros"
      ForeColor       =   &H8000000D&
      Height          =   1395
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8715
      Begin VB.TextBox txtApe2 
         Height          =   285
         Left            =   3180
         TabIndex        =   25
         Top             =   960
         Width           =   1755
      End
      Begin VB.TextBox txtApe1 
         Height          =   285
         Left            =   720
         TabIndex        =   13
         Top             =   960
         Width           =   1695
      End
      Begin VB.TextBox txtNombre 
         Height          =   285
         Left            =   3180
         TabIndex        =   12
         Top             =   600
         Width           =   1755
      End
      Begin VB.TextBox txtHistoria 
         Height          =   285
         Left            =   720
         TabIndex        =   11
         Top             =   600
         Width           =   975
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "Consultar"
         Height          =   375
         Left            =   7560
         TabIndex        =   10
         Top             =   900
         Width           =   1035
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
         Height          =   330
         Left            =   5760
         TabIndex        =   1
         Tag             =   "Fecha superior de las citas"
         Top             =   900
         Width           =   1650
         _Version        =   65537
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboDpto 
         DataField       =   "AD10CODTIPPACIEN"
         Height          =   330
         Left            =   720
         TabIndex        =   2
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   180
         Width           =   1665
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2937
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
         Height          =   330
         Left            =   5760
         TabIndex        =   18
         Tag             =   "Fecha superior de las citas"
         Top             =   540
         Width           =   1650
         _Version        =   65537
         _ExtentX        =   2910
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboTipEcon 
         DataField       =   "AD10CODTIPPACIEN"
         Height          =   330
         Left            =   5760
         TabIndex        =   23
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   180
         Width           =   720
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3201
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1270
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 0"
      End
      Begin SSDataWidgets_B.SSDBCombo cboDoctor 
         DataField       =   "AD10CODTIPPACIEN"
         Height          =   330
         Left            =   3180
         TabIndex        =   24
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   180
         Width           =   1800
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4471
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3175
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   1
         Left            =   5160
         TabIndex        =   19
         Top             =   600
         Width           =   510
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Ape:"
         Height          =   195
         Index           =   3
         Left            =   2520
         TabIndex        =   17
         Top             =   1020
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Ape:"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   16
         Top             =   1020
         Width           =   525
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   2460
         TabIndex        =   15
         Top             =   660
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   660
         Width           =   525
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Index           =   0
         Left            =   5220
         TabIndex        =   6
         Top             =   1020
         Width           =   420
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Dpto"
         Height          =   195
         Index           =   2
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   345
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Eco."
         Height          =   195
         Index           =   3
         Left            =   5040
         TabIndex        =   4
         Top             =   240
         Width           =   690
      End
      Begin VB.Label lblConsulta 
         AutoSize        =   -1  'True
         Caption         =   "Doctor"
         Height          =   195
         Index           =   4
         Left            =   2520
         TabIndex        =   3
         Top             =   180
         Width           =   480
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Listados"
      ForeColor       =   &H8000000D&
      Height          =   1395
      Left            =   8760
      TabIndex        =   20
      Top             =   0
      Width           =   1575
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "&Imprimir"
         Height          =   375
         Left            =   300
         TabIndex        =   26
         Top             =   900
         Width           =   915
      End
      Begin VB.CheckBox chkPlantas 
         Caption         =   "Plantas"
         Height          =   315
         Left            =   120
         TabIndex        =   22
         Top             =   240
         Width           =   975
      End
      Begin VB.CheckBox chkListado 
         Caption         =   "Departamentos"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   600
         Width           =   1395
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10440
      TabIndex        =   7
      Top             =   120
      Width           =   1155
   End
End
Attribute VB_Name = "frmPacientesIngrNW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Function fSeleccionar(strEstCama As String, strDpto As String, strOrder As String) As rdoResultset
Dim cllBuscar As New Collection
Dim qry     As rdoQuery
Dim rs As rdoResultset
Dim SQL As String
Dim i As Integer
SQL = "SELECT CAMA, CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL,AD14CODESTCAMA, "
SQL = SQL & "CI22NUMHISTORIA, DOCTOR, AD25FECINICIO,"
SQL = SQL & "TO_CHAR(AD25FECINICIO, 'DD/MM/YYYY HH24:MI:SS') FECHA, "
SQL = SQL & "CI32CODTIPECON, AD02DESDPTO, CI13CODENTIDAD "
SQL = SQL & "FROM AD1504J WHERE "
If txtHistoria.Text <> "" Then
    SQL = SQL & "   CI22NUMHISTORIA= ? AND "
    cllBuscar.Add txtHistoria.Text
End If

If txtNombre.Text <> "" Then
    SQL = SQL & " CI22NOMBRE Like ? AND "
    cllBuscar.Add "%" & UCase(Trim(txtNombre.Text)) & "%"
End If
If txtApe1.Text <> "" Then
    SQL = SQL & " CI22PRIAPEL Like ? AND "
    cllBuscar.Add "%" & UCase(Trim(txtApe1.Text)) & "%"
End If
If txtApe2.Text <> "" Then
    SQL = SQL & " CI22SEGAPEL Like ? AND "
    cllBuscar.Add "%" & UCase(Trim(txtApe2.Text)) & "%"
End If
If dcboDesde.Text <> "" Then
    SQL = SQL & "TRUNC(AD25FECINICIO) >= TO_DATE(?, 'DD/MM/YYYY') AND "
    cllBuscar.Add Format(dcboDesde.Text, "dd/mm/yyyy")
End If
If dcboHasta.Text <> "" Then
    SQL = SQL & "TRUNC(AD25FECINICIO) < TO_DATE(?, 'DD/MM/YYYY') AND "
    cllBuscar.Add Format(DateAdd("d", 1, CDate(dcboHasta.Text)), "dd/mm/yyyy")
End If
If cboTipEcon.Text <> "" Then
    SQL = SQL & " CI32CODTIPECON= ? AND "
    cllBuscar.Add cboTipEcon.Columns(0).Value
End If
If cboDpto.Columns(0).Value <> "0" Then
    SQL = SQL & " AD02CODDPTO= ? AND "
    cllBuscar.Add cboDpto.Columns(0).Value
End If
If cboDoctor.Text <> "" Then
    SQL = SQL & " SG02COD= ? AND "
    cllBuscar.Add cboDoctor.Columns(0).Value
End If
SQL = SQL & " AD02CODDPTO_CAMA " & strDpto
If strEstCama <> "" Then
    SQL = SQL & " AND AD14CODESTCAMA" & strEstCama
End If
SQL = SQL & strOrder
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
For i = 0 To cllBuscar.Count - 1
    qry(i) = cllBuscar(i + 1)
Next
Set fSeleccionar = qry.OpenResultset
End Function
Private Sub pConsultar(intCol As Integer)
Dim rsQuir As rdoResultset
Dim rsTodos As rdoResultset
Dim cllBuscar As New Collection
Static sqlOrder As String
Dim strEstCama As String
Dim strDpto As String
Dim arrIngresos() As typeIngresos
Dim i As Integer
Dim sRow As String
Screen.MousePointer = vbHourglass

grdIngresos.RemoveAll

If intCol = -1 Then
    If sqlOrder = "" Then sqlOrder = " ORDER BY AD25FECINICIO"
    Else
        Select Case UCase(grdIngresos.Columns(intCol).Caption)
        Case UCase("F.Ingreso"): sqlOrder = " ORDER BY AD25FECINICIO"
        Case UCase("N� H�"): sqlOrder = " ORDER BY CI22NUMHISTORIA"
        Case UCase("C. Planta"): sqlOrder = " ORDER BY CAMA"
        Case UCase("Paciente"): sqlOrder = " ORDER BY CI22PRIAPEL, CI22SEGAPEL,CI22NOMBRE"
        Case UCase("T.Eco./Entid"): sqlOrder = " ORDER BY CI32CODTIPECON "
        Case UCase("Doctor"): sqlOrder = " ORDER BY DOCTOR"
        Case UCase("Dpto."): sqlOrder = " ORDER BY AD02DESDPTO"
        Case Else: Exit Sub
        End Select
    End If
strEstCama = ""
strDpto = "<>" & constDPTO_QUIROFANO
Set rsTodos = fSeleccionar(strEstCama, strDpto, sqlOrder)

If Not rsTodos.EOF Then
    Do While Not rsTodos.EOF
        On Error GoTo dimVector
        ReDim Preserve arrIngresos(1 To UBound(arrIngresos) + 1)
dimVector: If Err <> 0 Then
            ReDim arrIngresos(1 To 1)
            Err = 0
        End If
        i = UBound(arrIngresos)
        With arrIngresos(i)
            .strFecha = rsTodos!FECHA
            .strPaciente = rsTodos!CI22PRIAPEL & " " & rsTodos!CI22SEGAPEL & ", " & rsTodos!CI22NOMBRE
            .strCPlanta = rsTodos!Cama
            .strDpto = rsTodos!AD02DESDPTO
            .lngHistoria = rsTodos!CI22NUMHISTORIA
            .strDoctor = rsTodos!DOCTOR
            .strTipEcon = rsTodos!CI32CODTIPECON & "/" & rsTodos!CI13CODENTIDAD
        End With
        rsTodos.MoveNext
    Loop
    strEstCama = "=" & constCAMA_OCUPADA
    strDpto = "=" & constDPTO_QUIROFANO
    Set rsQuir = fSeleccionar(strEstCama, strDpto, sqlOrder)
    i = 1
    If Not rsQuir.EOF Then
        Do While Not rsQuir.EOF
            Do While (rsQuir!CI22NUMHISTORIA <> arrIngresos(i).lngHistoria And i < UBound(arrIngresos))
                i = i + 1
            Loop
            If rsQuir!CI22NUMHISTORIA = arrIngresos(i).lngHistoria Then
                arrIngresos(i).strCQuirofano = rsQuir!Cama
            End If
        rsQuir.MoveNext
        Loop
    End If
    For i = 1 To UBound(arrIngresos)
        With arrIngresos(i)
            sRow = .strFecha & Chr$(9)
            sRow = sRow & .strPaciente & Chr$(9)
            sRow = sRow & .strCPlanta & Chr$(9)
            sRow = sRow & .strCQuirofano & Chr$(9)
            sRow = sRow & .strDpto & Chr$(9)
            sRow = sRow & .lngHistoria & Chr$(9)
            sRow = sRow & .strDoctor & Chr$(9)
            sRow = sRow & .strTipEcon
            grdIngresos.AddItem sRow
        End With
    Next
Else
    MsgBox "No se ha encontrado ning�n registro con las condiciones de b�squeda especificadas", vbOKOnly + vbInformation, Me.Caption
End If
Screen.MousePointer = vbDefault
End Sub
Private Sub pCargarDpto()
Dim SQL As String
Dim rs As rdoResultset
Dim qry As rdoQuery

cboDpto.RemoveAll
'Cargar comboDpto
SQL = "SELECT COUNT(*) FROM AD0300 WHERE SG02COD = ? "
SQL = SQL & " AND AD02CODDPTO IN (" & constDPTO_INFORMACION
SQL = SQL & "," & constDPTO_ADMINISTRACION & "," & constDPTO_ARCHIVO
SQL = SQL & "," & constDPTO_ADMISION & ")"
Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = objSecurity.strUser
Set rs = qry.OpenResultset()
If rs(0) > 0 Then
    cboDpto.AddItem "0" & Chr(9) & "TODOS"
    SQL = "SELECT AD02CODDPTO, AD02DESDPTO "
    SQL = SQL & "FROM AD0200 "
    SQL = SQL & "WHERE AD0200.AD32CODTIPODPTO = 1 "      'Servicios clinicos
    SQL = SQL & "ORDER BY  AD02DESDPTO "
Else
    SQL = "SELECT AD0200.AD02CODDPTO, AD02DESDPTO "
    SQL = SQL & "FROM AD0200,AD0300 "
    SQL = SQL & "WHERE AD0200.AD32CODTIPODPTO = 1 "      'Servicios clinicos
    SQL = SQL & " AND AD0200.AD02CODDPTO = AD0300.AD02CODDPTO"
    SQL = SQL & " AND AD0300.SG02COD = '" & objSecurity.strUser
    SQL = SQL & "' ORDER BY  AD02DESDPTO "
End If
rs.Close
qry.Close
Set rs = objApp.rdoConnect.OpenResultset(SQL)
Do While Not rs.EOF
    cboDpto.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
Loop
rs.Close
cboDpto.Text = cboDpto.Columns(1).Value

End Sub
Private Sub pCargarDoctor()
Dim SQL As String
Dim rs As rdoResultset
Dim qry As rdoQuery
cboDoctor.RemoveAll
cboDoctor.Text = ""
           
''Cargar comboDoctor
    SQL = "SELECT SG0200.SG02COD,AG11DESRECURSO"
    SQL = SQL & " FROM AG1100,SG0200"
    SQL = SQL & " WHERE SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    SQL = SQL & " AND SG0200.SG02COD=AG1100.SG02COD"
    SQL = SQL & " AND SG0200.AD30CODCATEGORIA=1"
    If cboDpto.Columns(0).Value <> "0" Then
        SQL = SQL & " AND AD02CODDPTO = ?"
    End If
    SQL = SQL & " ORDER BY AG11ORDEN, AG11DESRECURSO"
    If cboDpto.Columns(0).Value <> "0" Then
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = cboDpto.Columns(0).Value
        Set rs = qry.OpenResultset()
    Else
        Set rs = objApp.rdoConnect.OpenResultset(SQL)
    End If
    Do While Not rs.EOF
        cboDoctor.AddItem rs(0) & Chr(9) & rs(1)
        rs.MoveNext
    Loop
End Sub
Private Sub pCargartipecon()
    Dim SQL As String
    Dim rs As rdoResultset
    cboTipEcon.RemoveAll
    SQL = "SELECT  CI32CODTIPECON,CI32DESTIPECON"
    SQL = SQL & " FROM CI3200 "
    SQL = SQL & " ORDER BY CI32DESTIPECON "
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboTipEcon.AddItem rs(0) & Chr(9) & rs(1)
        rs.MoveNext
    Loop
End Sub


Public Function fAhora() As String
'Devuelve la fecha y hora actual obtenida del servidor
    Dim SQL$, rs As rdoResultset
    
    SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    fAhora = rs(0)
    rs.Close
End Function

Private Sub pLimpiar()
    Dim i As Integer
    txtHistoria.Text = ""
    txtNombre.Text = ""
    txtApe1.Text = ""
    txtApe2.Text = ""
    'Fecha iniciar a d�a de hoy
'    dcboDesde.Date = Format(fAhora, "dd/mm/yyyy")
'    dcboDesde.Text = Format(fAhora, "dd/mm/yyyy")
'    dcboHasta.Date = Format(fAhora, "dd/mm/yyyy")
'    dcboHasta.Text = Format(fAhora, "dd/mm/yyyy")
    'Textos
    Call pCargarDpto
    Call pCargartipecon
    grdIngresos.RemoveAll
End Sub

Private Sub cboDoctor_Change()
grdIngresos.RemoveAll
End Sub

Private Sub cboDoctor_Click()
grdIngresos.RemoveAll
End Sub

Private Sub cboDpto_Change()
grdIngresos.RemoveAll
Call pCargarDoctor
End Sub

Private Sub cboDpto_Click()
grdIngresos.RemoveAll
Call pCargarDoctor
End Sub

Private Sub cboTipEcon_Change()
grdIngresos.RemoveAll

End Sub

Private Sub cboTipEcon_Click()
grdIngresos.RemoveAll

End Sub

Private Sub cmdConsultar_Click()
Screen.MousePointer = vbHourglass
If dcboDesde.Text <> "" Then
    If Not IsDate(dcboDesde.Text) Then
        MsgBox "La fecha Inicial no es una fecha valida", vbExclamation, Me.Caption
        Exit Sub
    End If
End If
If dcboHasta.Text <> "" Then
    If Not IsDate(dcboHasta.Text) Then
        MsgBox "La fecha final no es una fecha valida", vbExclamation, Me.Caption
        Exit Sub
    End If
End If
    Call pConsultar(-1)
    Screen.MousePointer = vbDefault
End Sub

Private Sub CmdImprimir_Click()
         Dim strWhereTotal   As String
         Dim strwhereand As String
         strWhereTotal = ""
         strwhereand = ""
         If chkPlantas.Value = 1 Then
             If cboDpto.Columns(0).Value = "0" Then
                strWhereTotal = "1=1"
             Else
                strWhereTotal = "ad1504J.ad02coddpto= " & cboDpto.Columns(0).Value
             End If
             If cboDoctor.Text <> "" Then
                strWhereTotal = strWhereTotal & " and ad1504J.sg02cod= '" & cboDoctor.Columns(0).Value & "'"
             End If
            Call Imprimir_API(strWhereTotal, "AD2012.rpt", , , , False)
        End If
        If chkListado.Value = 1 Then
            strWhereTotal = "ad1504J.ad02coddpto= " & cboDpto.Columns(0).Value
            Call Imprimir_API(strWhereTotal, "AD10100.RPT")
        End If

End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()

End Sub

Private Sub dcboDesde_Change()
grdIngresos.RemoveAll
If dcboDesde.Text <> "" And dcboHasta.Text <> "" Then
    If CDate(dcboDesde.Text) > CDate(dcboHasta.Text) Then
            dcboHasta.Text = dcboDesde.Text
            dcboHasta.Date = dcboDesde.Date
    End If
End If
End Sub

Private Sub dcboDesde_CloseUp()
grdIngresos.RemoveAll
If dcboDesde.Text <> "" And dcboHasta.Text <> "" Then
    If CDate(dcboDesde.Text) > CDate(dcboHasta.Text) Then
            dcboHasta.Text = dcboDesde.Text
            dcboHasta.Date = dcboDesde.Date
    End If
End If
End Sub

Private Sub dcboHasta_Change()
grdIngresos.RemoveAll
If dcboDesde.Text <> "" And dcboHasta.Text <> "" Then
    If CDate(dcboHasta.Text) < CDate(dcboDesde.Text) Then
        dcboDesde.Text = dcboHasta.Text
        dcboDesde.Date = dcboHasta.Date
    End If
End If
End Sub

Private Sub dcboHasta_CloseUp()
grdIngresos.RemoveAll
If dcboDesde.Text <> "" And dcboHasta.Text <> "" Then
    If CDate(dcboHasta.Text) < CDate(dcboDesde.Text) Then
            dcboDesde.Text = dcboHasta.Text
            dcboDesde.Date = dcboHasta.Date
    End If
End If
End Sub

Private Sub Form_Activate()
cboDpto.SetFocus
End Sub

Private Sub Form_Load()
Call pLimpiar
End Sub

Private Sub grdIngresos_HeadClick(ByVal ColIndex As Integer)
Call pConsultar(ColIndex)
End Sub

Private Sub txtApe1_Change()
grdIngresos.RemoveAll

End Sub

Private Sub txtApe1_GotFocus()
txtApe1.SelStart = 0
txtApe1.SelLength = Len(Trim(txtApe1.Text))
End Sub

Private Sub txtApe2_Change()
grdIngresos.RemoveAll

End Sub

Private Sub txtApe2_GotFocus()
 txtApe2.SelStart = 0
 txtApe2.SelLength = Len(Trim(txtApe2.Text))

End Sub

Private Sub txtHistoria_Change()
grdIngresos.RemoveAll

End Sub

Private Sub txtHistoria_GotFocus()
txtHistoria.SelStart = 0
 txtHistoria.SelLength = Len(Trim(txtHistoria.Text))

End Sub

Private Sub txtNombre_Change()
grdIngresos.RemoveAll

End Sub

Private Sub txtNombre_GotFocus()
txtNombre.SelStart = 0
 txtNombre.SelLength = Len(Trim(txtNombre.Text))

End Sub
