VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmAbrirAsistencias 
   Caption         =   "Re-abrir Proceso-asistencia"
   ClientHeight    =   8115
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   8115
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Abrir Asitencia"
      Height          =   375
      Left            =   5040
      TabIndex        =   7
      Top             =   7560
      Width           =   1455
   End
   Begin VB.Frame frmframe1 
      Caption         =   "Procesos/Asistencia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6135
      Index           =   1
      Left            =   0
      TabIndex        =   5
      Top             =   1200
      Width           =   11895
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5805
         HelpContextID   =   2
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   11655
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   8
         stylesets.count =   1
         stylesets(0).Name=   "Urgente"
         stylesets(0).BackColor=   255
         stylesets(0).Picture=   "AD0550.frx":0000
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Caption=   "Proceso"
         Columns(0).Name =   "Proceso"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Asistencia"
         Columns(1).Name =   "Asistencia"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "Fec. Inici Asistencia"
         Columns(2).Name =   "Fec. Inici Asistencia"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "Fec. Fin. Asis"
         Columns(3).Name =   "Fec. Fin. Asis"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3200
         Columns(4).Caption=   "Fecha inicio P/A"
         Columns(4).Name =   "Fecha inicio"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Caption=   "Fecha fin P/A"
         Columns(5).Name =   "Fecha fin"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Caption=   "Dpto Responsable"
         Columns(6).Name =   "Dpto Responsable"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Caption=   "Dr/Dra Responsable"
         Columns(7).Name =   "Dr/Dra Responsable"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   20558
         _ExtentY        =   10239
         _StockProps     =   79
      End
   End
   Begin VB.Frame frmframe1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   9015
      Begin VB.TextBox txtText1 
         Height          =   375
         Index           =   1
         Left            =   3360
         TabIndex        =   2
         Top             =   360
         Width           =   5535
      End
      Begin VB.TextBox txtText1 
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   1
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre:"
         Height          =   255
         Index           =   1
         Left            =   2640
         TabIndex        =   4
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Historia:"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmAbrirAsistencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub pInicializar_grid()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

sql = "SELECT AD0100.AD01CODASISTENCI, AD0700.AD07CODPROCESO, AD01FECINICIO,AD01FECFIN, AD08FECINICIO, AD08FECFIN,"
sql = sql & " AD0200.AD02DESDPTO,SG02NOM||' '||SG02APE1||' '||SG02APE2 DOCTOR"
sql = sql & " FROM AD0100,AD0700, AD0800,AD0500,AD0200,SG0200 WHERE"
sql = sql & " AD0100.CI22NUMHISTORIA = ?"
sql = sql & " AND AD0700.CI22NUMHISTORIA = ?"
sql = sql & " AND AD0700.AD07CODPROCESO = AD0800.AD07CODPROCESO"
sql = sql & " AND AD0100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI"
sql = sql & " AND AD0700.AD07CODPROCESO = AD0500.AD07CODPROCESO"
sql = sql & " AND AD0100.AD01CODASISTENCI = AD0500.AD01CODASISTENCI"
sql = sql & " AND AD0500.AD05FECFINRESPON IS NULL"
sql = sql & " AND AD0500.AD02CODDPTO = AD0200.AD02CODDPTO"
sql = sql & " AND AD0500.SG02COD = SG0200.SG02COD"
sql = sql & " ORDER BY AD01FECINICIO DESC"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(txtText1(0).Text)
    qry(1) = qry(0)
Set rs = qry.OpenResultset()

Do While Not rs.EOF
    grdDBGrid1(0).AddItem rs!AD07CODPROCESO & Chr$(9) _
        & rs!AD01CODASISTENCI & Chr$(9) _
        & rs!AD01FECINICIO & Chr$(9) _
        & rs!AD01FECFIN & Chr$(9) _
        & rs!AD08FECINICIO & Chr$(9) _
        & rs!AD08FECFIN & Chr$(9) _
        & rs!AD02DESDPTO & Chr$(9) _
        & rs!DOCTOR
    rs.MoveNext
Loop

End Sub

Private Sub cmdCommand1_Click()
Dim i As Integer
Dim qryPA As rdoQuery
Dim qryA As rdoQuery
Dim sql1 As String
Dim sql2 As String
Dim vntBookMark As Variant
err = 0
'On Error Resume Next

If grdDBGrid1(0).SelBookmarks.Count = 0 Then
    MsgBox "selecciones los procesos/asistencia para abrir", vbCritical
Else
   objApp.BeginTrans
   sql1 = "UPDATE AD0800 SET AD08FECFIN = NULL WHERE AD01CODASISTENCI = ? AND AD07CODPROCESO = ?"
   sql2 = "UPDATE AD0100 SET AD01FECFIN = NULL WHERE AD01CODASISTENCI = ?"
   Set qryPA = objApp.rdoConnect.CreateQuery("", sql1)
   Set qryA = objApp.rdoConnect.CreateQuery("", sql2)
   For i = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
     vntBookMark = grdDBGrid1(0).SelBookmarks(i)
     If Trim(grdDBGrid1(0).Columns("Fecha fin P/A").CellValue(vntBookMark)) <> "" Then
        qryPA(1) = Trim(grdDBGrid1(0).Columns("Proceso").CellValue(vntBookMark))
        qryPA(0) = Trim(grdDBGrid1(0).Columns("Asistencia").CellValue(vntBookMark))
        qryPA.Execute
     End If
     If Trim(grdDBGrid1(0).Columns("Fec. Fin. Asis").CellValue(vntBookMark)) <> "" Then
        qryA(0) = Trim(grdDBGrid1(0).Columns("Asistencia").CellValue(vntBookMark))
        qryA.Execute
     End If
     
     If err > 0 Then
        MsgBox "Imposible abrir el proceso/asistencia", vbOKOnly
        objApp.RollbackTrans
        Exit Sub
     End If
   Next i
   qryPA.Close
   qryA.Close
   objApp.CommitTrans
   grdDBGrid1(0).RemoveAll
   Call pInicializar_grid
End If
End Sub


Private Sub txtText1_LostFocus(Index As Integer)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
If Index = 0 Then
    grdDBGrid1(0).RemoveAll
    sql = "SELECT CI22PRIAPEL||', '||CI22SEGAPEL||', '||CI22NOMBRE NOMBRE "
    sql = sql & "FROM CI2200 WHERE CI22NUMHISTORIA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Trim(txtText1(0).Text)
    Set rs = qry.OpenResultset()
    If rs.EOF Then
        MsgBox "Historia Inexistente"
        Exit Sub
    End If
    txtText1(1).Text = rs!Nombre
    rs.Close
    qry.Close
    grdDBGrid1(0).RemoveAll
    Call pInicializar_grid
End If
End Sub
