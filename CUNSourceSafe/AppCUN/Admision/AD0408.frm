VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Begin VB.Form frmSituacionCamas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Situaci�n Camas"
   ClientHeight    =   7890
   ClientLeft      =   1125
   ClientTop       =   735
   ClientWidth     =   9600
   ClipControls    =   0   'False
   Icon            =   "AD0408.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7890
   ScaleWidth      =   9600
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9600
      _ExtentX        =   16933
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Nueva Situaci�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1275
      Index           =   2
      Left            =   60
      TabIndex        =   22
      Top             =   6270
      Width           =   9465
      Begin VB.TextBox txtsegundo 
         Height          =   285
         Left            =   5880
         TabIndex        =   35
         Top             =   360
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox txtHora 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   2220
         MaxLength       =   2
         TabIndex        =   30
         TabStop         =   0   'False
         Tag             =   "Hora de Fin"
         Text            =   "0"
         Top             =   660
         Width           =   390
      End
      Begin VB.TextBox txtMinuto 
         Alignment       =   1  'Right Justify
         Height          =   330
         HelpContextID   =   3
         Index           =   0
         Left            =   2910
         MaxLength       =   2
         TabIndex        =   29
         TabStop         =   0   'False
         Tag             =   "Minutos de Fin"
         Text            =   "0"
         Top             =   660
         Width           =   390
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Asignar"
         Height          =   405
         Index           =   0
         Left            =   7050
         TabIndex        =   28
         Top             =   600
         Width           =   1215
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   2
         Left            =   300
         TabIndex        =   23
         Tag             =   "Fecha Inicio|Fecha Inicio"
         Top             =   660
         Width           =   1845
         _Version        =   65537
         _ExtentX        =   3254
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1000/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY HH:NN:SS"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   3690
         TabIndex        =   24
         Tag             =   "Descripci�n Estado Nuevo|Descripci�n Estado Nuevo"
         Top             =   660
         Width           =   2910
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5133
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin ComCtl2.UpDown UpDownMinuto 
         Height          =   330
         Index           =   0
         Left            =   3300
         TabIndex        =   31
         Top             =   660
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         BuddyControl    =   "txtMinuto(0)"
         BuddyDispid     =   196612
         BuddyIndex      =   0
         OrigLeft        =   2880
         OrigTop         =   1110
         OrigRight       =   3120
         OrigBottom      =   1440
         Increment       =   5
         Max             =   55
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDownHora 
         Height          =   330
         Index           =   0
         Left            =   2610
         TabIndex        =   32
         Top             =   660
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         BuddyControl    =   "txtHora(0)"
         BuddyDispid     =   196611
         BuddyIndex      =   0
         OrigLeft        =   5760
         OrigTop         =   360
         OrigRight       =   6000
         OrigBottom      =   690
         Max             =   23
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hora Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   2220
         TabIndex        =   33
         Top             =   420
         Width           =   945
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Estado Nuevo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   3690
         TabIndex        =   26
         Top             =   420
         Width           =   1215
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Inicio"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   300
         TabIndex        =   25
         Top             =   420
         Width           =   1065
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Cambios de Situaci�n"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3555
      Index           =   1
      Left            =   60
      TabIndex        =   9
      Top             =   2670
      Width           =   9465
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3090
         Index           =   1
         Left            =   150
         TabIndex        =   11
         Top             =   360
         Width           =   9105
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         AllowColumnShrinking=   0   'False
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   16060
         _ExtentY        =   5450
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Camas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   0
      Left            =   60
      TabIndex        =   8
      Top             =   450
      Width           =   9465
      Begin TabDlg.SSTab tabTab1 
         Height          =   1695
         HelpContextID   =   90001
         Index           =   0
         Left            =   150
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   360
         Width           =   9000
         _ExtentX        =   15875
         _ExtentY        =   2990
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AD0408.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtcDateCombo1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtTEXT1(6)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtTEXT1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtTEXT1(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtTEXT1(3)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtTEXT1(2)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtTEXT1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtTEXT1(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "chkCheck1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtTEXT1(7)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).ControlCount=   17
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AD0408.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD15CODCAMA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   7
            Left            =   120
            TabIndex        =   34
            Tag             =   "C�digo Cama|C�digo Cama"
            Top             =   300
            Width           =   930
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Permitir cambio de situaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   2310
            TabIndex        =   27
            Tag             =   "Responsable de Procesos|Responsable de Procesos"
            Top             =   1410
            Visible         =   0   'False
            Width           =   3015
         End
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD15CODCAMA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   1320
            TabIndex        =   1
            Tag             =   "C�digo Cama|C�digo Cama"
            Top             =   600
            Visible         =   0   'False
            Width           =   930
         End
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   1440
            TabIndex        =   2
            Tag             =   "Descripci�n del Departamento|Descripci�n del Departamento"
            Top             =   300
            Width           =   3600
         End
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   2730
            TabIndex        =   3
            Tag             =   "Tipo Cama|Tipo Cama"
            Top             =   0
            Visible         =   0   'False
            Width           =   570
         End
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   3
            Left            =   5490
            TabIndex        =   4
            Tag             =   "Descripci�n Tipo de Cama|Descripci�n Tipo de Cama"
            Top             =   300
            Width           =   2370
         End
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD13CODTIPOCAMA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   4
            Left            =   6450
            TabIndex        =   5
            Tag             =   "Departamento|Departamento"
            Top             =   0
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   5
            Left            =   2220
            TabIndex        =   6
            Tag             =   "Estado Actual|Estado Actual"
            Top             =   1020
            Width           =   2820
         End
         Begin VB.TextBox txtTEXT1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD14CODESTCAMA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   3570
            TabIndex        =   7
            Tag             =   "Estado Actual|Estado Actual"
            Top             =   810
            Visible         =   0   'False
            Width           =   570
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1500
            Index           =   0
            Left            =   -74910
            TabIndex        =   13
            Top             =   90
            Width           =   8640
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15240
            _ExtentY        =   2646
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD15FECINICIO"
            Height          =   330
            Index           =   0
            Left            =   150
            TabIndex        =   18
            Tag             =   "Fecha Inicio|Fecha Inicio"
            Top             =   1020
            Width           =   1845
            _Version        =   65537
            _ExtentX        =   3254
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD15FECFIN"
            Height          =   330
            Index           =   1
            Left            =   5250
            TabIndex        =   20
            Tag             =   "Fecha Fin|Fecha Fin"
            Top             =   1020
            Width           =   1845
            _Version        =   65537
            _ExtentX        =   3254
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   5
            Left            =   5250
            TabIndex        =   21
            Top             =   780
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   150
            TabIndex        =   19
            Top             =   780
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado Actual"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   2220
            TabIndex        =   17
            Top             =   810
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   5460
            TabIndex        =   16
            Top             =   90
            Width           =   915
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   1440
            TabIndex        =   15
            Top             =   90
            Width           =   1230
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Cama"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   150
            TabIndex        =   14
            Top             =   90
            Width           =   750
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   7590
      Width           =   9600
      _ExtentX        =   16933
      _ExtentY        =   529
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSituacionCamas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
'Dim lngX As Long
'Dim lngY As Long
'Dim strEstado As String --> JRC 15/10/1998
Const iFecNuevaSit = 2



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Command Button
' -----------------------------------------------

Private Sub cmdCommand1_Click(Index As Integer)
  Dim vntA As Variant
  Dim strSQL As String
  Dim strFecha As String
  Dim strSelect As String '--> JRC
  Dim rstSelect As rdoResultset
  
  Dim sCodAsistencia    As String
  Dim sCodProceso       As String
  Dim sCodCama          As String

  Dim sStmSql   As String
  Dim qrySelect As rdoQuery
  Dim rdoCaso   As rdoResultset
  Dim strCaso   As String
  Dim sHistoria As String
  Dim qryInsert As rdoQuery
  Dim sNumRegistro  As String
  Dim rdoConsul As rdoResultset
  Dim strFecha_AD1600 As String
  Dim strHora_AD1600 As String
  err = 0
  If dtcDateCombo1(iFecNuevaSit).Date = "" Then
    Call objError.SetError(cwCodeMsg, "Debe seleccionar una fecha", vntA)
    vntA = objError.Raise
    Exit Sub
  End If
  If cboDBCombo1(0).Text = "" Then
    Call objError.SetError(cwCodeMsg, "Debe seleccionar un estado", vntA)
    vntA = objError.Raise
    Exit Sub
  End If
  grdDBGrid1(1).Row = 1
  If cboDBCombo1(0).Columns(0).Text = txtTEXT1(6).Text Then
    Call objError.SetError(cwCodeMsg, "Est� asignando un estado id�ntico al estado actual", vntA)
    vntA = objError.Raise
    Exit Sub
  End If
  
  strFecha = dtcDateCombo1(iFecNuevaSit).Date & " " & txtHora(0) & ":" & txtMinuto(0) & ":" & txtsegundo.Text
  
  
  strSelect = "select * from AD1600 where AD16FECFIN=TO_DATE('" & strFecha & "','DD\MM\YYYY HH24:MI:SS')"
  Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
  If Not rstSelect.EOF Then
    Call objError.SetError(cwCodeMsg, "Ya existe un registro en la Base de Datos con la Fecha y Hora de inicio introducidas.", vntA)
    vntA = objError.Raise
    rstSelect.Close
    Set rstSelect = Nothing
    Exit Sub
  End If
  rstSelect.Close
  Set rstSelect = Nothing
  
  strSQL = "UPDATE AD1600 SET AD16FECFIN=TO_DATE('" & strFecha & "','DD\MM\YYYY HH24:MI:SS')"
  strSQL = strSQL & "WHERE AD15CODCAMA='" & txtTEXT1(0).Text & "' AND AD16FECFIN IS NULL"
  Call objApp.rdoConnect.Execute(strSQL, 64)
  'Call objApp.rdoConnect.Execute("COMMIT", 64)
  
  strSQL = "INSERT INTO AD1600 (AD15CODCAMA,AD16FECCAMBIO,AD14CODESTCAMA)"
  strSQL = strSQL & " VALUES ('" & txtTEXT1(0) & "',"
  strSQL = strSQL & "TO_DATE('" & strFecha & "','DD/MM/YYYY HH24:MI:SS')"
  strSQL = strSQL & ",'" & cboDBCombo1(0).Columns(0).Text & "')"
  Call objApp.rdoConnect.Execute(strSQL, 64)
  'Call objApp.rdoConnect.Execute("COMMIT", 64)

  strSQL = "UPDATE AD1500 SET AD14CODESTCAMA='" & cboDBCombo1(0).Columns(0).Text & "' "
  strSQL = strSQL & "WHERE AD15CODCAMA='" & txtTEXT1(0).Text & "'"
  Call objApp.rdoConnect.Execute(strSQL, 64)
    
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  objWinInfo.DataRefresh
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataRefresh
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String

  
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "AD1500"
    .intAllowance = cwAllowReadOnly
    .strInitialWhere = "AD15CODCAMA='" & frmGestionCamas.txtTEXT1(0) & "'"
    .strName = "Camas"
     
          
    Call .FormCreateFilterWhere("AD1500", "Situaci�n Camas")
    Call .FormAddFilterWhere("AD1500", "AD02CODDPTO", "Departamento", cwString, "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objSecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL" & _
             " AND AD32CODTIPODPTO = 3" & _
             " ORDER BY AD0200.AD02DESDPTO")
    Call .FormAddFilterWhere("AD1500", "AD13CODTIPOCAMA", "Tipo de Cama", cwString, "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA FROM AD1300 ORDER BY AD13CODTIPOCAMA")
    Call .FormAddFilterWhere("AD1500", "AD14CODESTCAMA", "Estado de Cama", cwString, "SELECT AD14CODESTCAMA, AD14DESESTCAMA FROM AD1400 ORDER BY AD14CODESTCAMA")
    
    Call .FormAddFilterOrder("AD1500", "AD15CODCAMA", "N�Cama")
    Call .FormAddOrderField("AD15CODCAMA", cwAscending)
  End With
  
  With objMultiInfo
    .strName = "Cambio Situacion"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
    .intAllowance = cwAllowReadOnly
    .strTable = "AD1600"
        
    Call .FormAddOrderField("AD16FECCAMBIO", cwDescending)
    
    Call .FormAddRelation("AD15CODCAMA", txtTEXT1(0))
    
    

  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
                
    Call .GridAddColumn(objMultiInfo, "Cama", "AD15CODCAMA", cwString)
    Call .GridAddColumn(objMultiInfo, "Fecha", "AD16FECCAMBIO", cwDate)
    Call .GridAddColumn(objMultiInfo, "C�digo Estado Cama", "AD14CODESTCAMA", cwString)
    Call .GridAddColumn(objMultiInfo, "Estado Cama       ", "", cwString)
    Call .GridAddColumn(objMultiInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Proceso", "AD07CODPROCESO", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Cod. Paciente", "", cwString)
    Call .GridAddColumn(objMultiInfo, "Nombre", "", cwString)
    Call .GridAddColumn(objMultiInfo, "Primer Apellido", "", cwString)
    Call .GridAddColumn(objMultiInfo, "Segundo Apellido", "", cwString)
    
    Call .FormCreateInfo(objMasterInfo)
        
   ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
   ' .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
   
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnMandatory = True
            
    Call .FormChangeColor(objMultiInfo)
  
    .CtrlGetInfo(txtTEXT1(2)).blnInFind = True
    .CtrlGetInfo(txtTEXT1(4)).blnInFind = True
    .CtrlGetInfo(txtTEXT1(6)).blnInFind = True
            
    Call .CtrlCreateLinked(.CtrlGetInfo(txtTEXT1(2)), "AD02CODDPTO", "SELECT AD02CODDPTO, AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtTEXT1(2)), txtTEXT1(1), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtTEXT1(4)), "AD13CODTIPOCAMA", "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA FROM AD1300 WHERE AD13CODTIPOCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtTEXT1(4)), txtTEXT1(3), "AD13DESTIPOCAMA")
   
    Call .CtrlCreateLinked(.CtrlGetInfo(txtTEXT1(6)), "AD14CODESTCAMA", "SELECT AD14CODESTCAMA, AD14DESESTCAMA, AD14INDSITUAC FROM AD1400 WHERE AD14CODESTCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtTEXT1(6)), txtTEXT1(5), "AD14DESESTCAMA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtTEXT1(6)), chkCheck1(0), "AD14INDSITUAC")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), "AD01CODASISTENCI", "SELECT * FROM AD0100 WHERE AD0100.AD34CODESTADO = 1 AND AD01CODASISTENCI = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(7)), grdDBGrid1(1).Columns(9), "CI21CODPERSONA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtTEXT1(0)), "AD15CODCAMA", "SELECT GCFN06(AD15CODCAMA)CAMA FROM AD1500 WHERE AD15CODCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtTEXT1(0)), txtTEXT1(7), "CAMA")
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), "CI21CODPERSONA", "SELECT * FROM CI2200 WHERE CI21CODPERSONA = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(10), "CI22NOMBRE")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(11), "CI22PRIAPEL")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(9)), grdDBGrid1(1).Columns(12), "CI22SEGAPEL")
        
        
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).strSQL = "SELECT AD14CODESTCAMA,AD14DESESTCAMA FROM " & "AD1400 ORDER BY AD14DESESTCAMA"
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "AD14CODESTCAMA", "SELECT AD14CODESTCAMA,AD14DESESTCAMA FROM AD1400 WHERE AD14CODESTCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "AD14DESESTCAMA")
  
    Dim Rs As rdoResultset
   
    Set Rs = objApp.rdoConnect.OpenResultset("SELECT AD14CODESTCAMA, AD14DESESTCAMA " & _
                                             "FROM AD1400 WHERE AD14FECFIN is NULL " & _
                                             "AND AD14INDSITUAC=-1", rdOpenDynamic)
    Do While Not Rs.EOF
      cboDBCombo1(0).AddItem Rs.rdoColumns(0) & Chr$(9) & Rs.rdoColumns(1)
      Rs.MoveNext
    Loop
    Rs.Close
    Set Rs = Nothing
    
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(5).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    If Index = 1 Then
        grdDBGrid1(Index).Columns(10).Text = GetTableColumn("SELECT CI22NOMBRE FROM CI2200 WHERE CI21CODPERSONA=" & Val(grdDBGrid1(1).Columns(9).Text))("CI22NOMBRE")
        grdDBGrid1(Index).Columns(11).Text = GetTableColumn("SELECT CI22PRIAPEL FROM CI2200 WHERE CI21CODPERSONA=" & Val(grdDBGrid1(1).Columns(9).Text))("CI22PRIAPEL")
        grdDBGrid1(Index).Columns(12).Text = GetTableColumn("SELECT CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA=" & Val(grdDBGrid1(1).Columns(9).Text))("CI22SEGAPEL")
    End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'  If strFormName = "Cambios Situaci�n" Then
'    If grdDBGrid1(1).Columns(0).Value <> "Le�do" Then
'      Select Case strCtrl
'        Case "grdDBGrid1(1).Estado Cama"
'          Call frmEstadosCama.Show(vbModal)
'          Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(grdDBGrid1(1).Columns(5)))
'      End Select
'    End If
'  End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
Dim vntA As Variant
  If dtcDateCombo1(1).Date <> "" Then
    If CDate(Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(1).Date, "DD/MM/YYYY")) Then
      Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
      vntA = objError.Raise
      blnCancel = True
    End If
  End If
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  Call objWinInfo.CtrlSet(txtTEXT1(7), txtTEXT1(0).Text)
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Dim strFecha As String '--> JRC
Dim rstFecha As rdoResultset
  
  If strFormName = "Camas" Then
    If dtcDateCombo1(1).Text = "" Then
      fraFrame1(2).Enabled = True
    Else
      fraFrame1(2).Enabled = False
      Exit Sub
    End If
    
    If chkCheck1(0) = 0 Then
      fraFrame1(2).Enabled = False
    Else
      fraFrame1(2).Enabled = True
    End If
  End If
  
  strFecha = "select to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') from dual"
  Set rstFecha = objApp.rdoConnect.OpenResultset(strFecha)
  dtcDateCombo1(iFecNuevaSit).Date = Left(rstFecha(0).Value, InStr(rstFecha(0).Value, " ") - 1)
  txtHora(0).Text = Mid(rstFecha(0).Value, 12, 2)
  txtMinuto(0).Text = Mid(rstFecha(0).Value, 15, 2)
  txtsegundo.Text = Mid(rstFecha(0).Value, 18, 2)
  
  rstFecha.Close
  Set rstFecha = Nothing

End Sub

' --> JRC 15/10/1998 �valor de strEstado?
'Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'  Dim strSQL As String
'
'  If Not blnError Then
'    strSQL = "UPDATE AD1500 SET AD14CODESTCAMA = '" & strEstado & "'"
'    strSQL = strSQL & " WHERE AD15CODCAMA = '" & txtText1(0).Text & "'"
'    objApp.rdoConnect.Execute strSQL, 64
'    'objApp.rdoConnect.Execute "Commit", 64
'  End If
'End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
     
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  If intIndex <> 2 Then Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  If intIndex <> 2 Then Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)

Dim dateNueva       As Date     'D�a Nueva situacion
Dim strHoraNueva    As String   'Hora Nueva Situacion
Dim dateUltima      As Date     'Dia ultima Situacion
Dim strHoraUltima   As String   'Hora ultima situacion

Select Case intIndex
    Case iFecNuevaSit
        grdDBGrid1(1).MoveFirst
        strHoraNueva = txtHora(0) & ":" & txtMinuto(0)
        dateNueva = dtcDateCombo1(iFecNuevaSit).Date
        dateUltima = Left(grdDBGrid1(1).Columns(4).Value, 10)
        strHoraUltima = Left(Right(grdDBGrid1(1).Columns(4).Value, 8), 5)
        'Chequeamos la fecha, si es menor a la actual error.
        If dateUltima > dateNueva Then
            MsgBox "No es posible modificar la situaci�n de una cama con fecha anterior al ultimo movimiento", vbCritical
            dtcDateCombo1(iFecNuevaSit).Date = strFecha_Sistema
            txtHora(0) = Left(strHora_Sistema, 2)
            txtMinuto(0) = Mid(strHora_Sistema, 4, 2)
        ElseIf dateUltima = dateNueva Then
            'Si la fecha es igual, chequeamos la hora
            If strHoraUltima > strHoraNueva Then
                MsgBox "No es posible modificar la situaci�n de una cama con fecha anterior al ultimo movimiento", vbCritical
                dtcDateCombo1(iFecNuevaSit).Date = strFecha_Sistema
                txtHora(0) = Left(strHora_Sistema, 2)
                txtMinuto(0) = Mid(strHora_Sistema, 4, 2)
            End If
        End If
    Case Else
        Call objWinInfo.CtrlDataChange
End Select
  
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  If intIndex <> 2 Then Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  If intIndex <> 0 Then Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  If intIndex <> 0 Then Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  If intIndex <> 0 Then Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtHora_LostFocus(Index As Integer)

    Dim dateNueva       As Date     'D�a Nueva situacion
    Dim strHoraNueva    As String   'Hora Nueva Situacion
    Dim dateUltima      As Date     'Dia ultima Situacion
    Dim strHoraUltima   As String   'Hora ultima situacion
    
    txtHora(0) = llenar(txtHora(0), 2, "0", True)
    grdDBGrid1(1).MoveFirst
    strHoraNueva = txtHora(0) & ":" & txtMinuto(0)
    dateNueva = dtcDateCombo1(iFecNuevaSit).Date
    dateUltima = Left(grdDBGrid1(1).Columns(4).Value, 10)
    strHoraUltima = Left(Right(grdDBGrid1(1).Columns(4).Value, 8), 5)
    'Chequeamos la fecha, si es menor a la actual error.
    If dateUltima > dateNueva Then
        MsgBox "No es posible modificar la situaci�n de una cama con fecha anterior al ultimo movimiento", vbCritical
        dtcDateCombo1(iFecNuevaSit).Date = strFecha_Sistema
        txtHora(0) = Left(strHora_Sistema, 2)
        txtMinuto(0) = Mid(strHora_Sistema, 4, 2)
    ElseIf dateUltima = dateNueva Then
        'Si la fecha es igual, chequeamos la hora
        If strHoraUltima > strHoraNueva Then
            MsgBox "No es posible modificar la situaci�n de una cama con fecha anterior al ultimo movimiento", vbCritical
            dtcDateCombo1(iFecNuevaSit).Date = strFecha_Sistema
            txtHora(0) = Left(strHora_Sistema, 2)
            txtMinuto(0) = Mid(strHora_Sistema, 4, 2)
        End If
    End If
End Sub

Private Sub txtMinuto_LostFocus(Index As Integer)
Dim dateNueva       As Date     'D�a Nueva situacion
Dim strHoraNueva    As String   'Hora Nueva Situacion
Dim dateUltima      As Date     'Dia ultima Situacion
Dim strHoraUltima   As String   'Hora ultima situacion

    grdDBGrid1(1).MoveFirst
    txtMinuto(0) = llenar(txtMinuto(0), 2, "0", True)
    strHoraNueva = txtHora(0) & ":" & txtMinuto(0)
    dateNueva = dtcDateCombo1(iFecNuevaSit).Date
    dateUltima = Left(grdDBGrid1(1).Columns(4).Value, 10)
    strHoraUltima = Left(Right(grdDBGrid1(1).Columns(4).Value, 8), 5)
    'Chequeamos la fecha, si es menor a la actual error.
    If dateUltima > dateNueva Then
        MsgBox "No es posible modificar la situaci�n de una cama con fecha anterior al ultimo movimiento", vbCritical
        dtcDateCombo1(iFecNuevaSit).Date = strFecha_Sistema
        txtHora(0) = Left(strHora_Sistema, 2)
        txtMinuto(0) = Mid(strHora_Sistema, 4, 2)
    ElseIf dateUltima = dateNueva Then
        'Si la fecha es igual, chequeamos la hora
        If strHoraUltima > strHoraNueva Then
            MsgBox "No es posible modificar la situaci�n de una cama con fecha anterior al ultimo movimiento", vbCritical
            dtcDateCombo1(iFecNuevaSit).Date = strFecha_Sistema
            txtHora(0) = Left(strHora_Sistema, 2)
            txtMinuto(0) = Mid(strHora_Sistema, 4, 2)
        End If
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de UpDown
' -----------------------------------------------
Private Sub UpDownHora_DownClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownHora_UpClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownMinuto_DownClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub

Private Sub UpDownMinuto_UpClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub

