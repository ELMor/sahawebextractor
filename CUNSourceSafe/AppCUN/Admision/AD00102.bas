Attribute VB_Name = "modConstantes"
Option Explicit
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long 'para cargar los TreeViews a gran velocidad

'Constantes de los tipos de departamentos
Public Const constTIPODPTO_SERVCLINICOS = 1
Public Const constTIPODPTO_SERVBASICOS = 2

'Constantes de los puestos de los usuarios
Public Const constPUESTO_CONSULTOR = 1
Public Const constPUESTO_ENFERMERA = 2
Public Const constPUESTO_COLABORADOR = 5
Public Const constPUESTO_RESIDENTE = 6
Public Const constPUESTO_JEFE = 9

'Constantes de los Tipos de Asistencia
Public Const constASIST_HOSP = 1
Public Const constASIST_AMBUL = 2

'Constantes de los Estados de las Camas
Public Const constCAMA_LIBRE = 1
Public Const constCAMA_OCUPADA = 2
Public Const constCAMA_RESERVADA = 6
Public Const constCAMA_QUIR = 8

'Constantes de los Tipos de Relaci�n con la UN
Public Const constREL_UN_DRREMITENTE = 24
Public Const constREL_UN_DRASOCIADO = 25

'Constantes de los Tipos Econ�micos
Public Const constTE_PRIVADO = "P"
Public Const constTE_ACUNSA = "J"
Public Const constTE_ENTCOLAB = "I"

'Constantes de los estados de las actuaciones
Public Const constESTACT_PLANIFICADA = 1
Public Const constESTACT_CITADA = 2
Public Const constESTACT_REALIZANDOSE = 3
Public Const constESTACT_REALIZADA = 4
Public Const constESTACT_INFORMADA = 5
Public Const constESTACT_CANCELADA = 6

'tipos de documentos
Public Const const_VOLANTE = 1

Public Type typeIngresos
    strFecha As String
    strPaciente As String
    strDpto As String
    lngHistoria As Long
    strTipEcon As String
    strDoctor As String
    strCPlanta As String
    strCQuirofano As String
End Type


