VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmConsultaPacientesdia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Pacientes del d�a"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin Crystal.CrystalReport crReport1 
      Left            =   10800
      Top             =   60
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   10920
      TabIndex        =   20
      Top             =   600
      Width           =   975
   End
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   10320
      Top             =   60
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   "|"
   End
   Begin VB.Frame Frame5 
      Caption         =   "Consulta"
      ForeColor       =   &H00C00000&
      Height          =   1755
      Left            =   180
      TabIndex        =   16
      Top             =   0
      Width           =   6375
      Begin VB.Frame Frame2 
         Caption         =   "Fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   795
         Left            =   3480
         TabIndex        =   17
         Top             =   120
         Width           =   2715
         Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
            Height          =   315
            Left            =   960
            TabIndex        =   18
            Top             =   240
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Fecha:"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   19
            Top             =   240
            Width           =   675
         End
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   5040
         TabIndex        =   0
         Top             =   1200
         Width           =   975
      End
      Begin SSDataWidgets_B.SSDBCombo cboTipoRec 
         Height          =   315
         Left            =   1080
         TabIndex        =   21
         Top             =   600
         Width           =   2355
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cod"
         Columns(0).Name =   "cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "nom"
         Columns(1).Name =   "nom"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Caption=   "design"
         Columns(2).Name =   "design"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   4154
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 2"
      End
      Begin SSDataWidgets_B.SSDBCombo cboDptos 
         Height          =   315
         Left            =   1080
         TabIndex        =   23
         Top             =   240
         Width           =   2355
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4154
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Dpto.:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   675
      End
      Begin VB.Label Label3 
         Caption         =   "Doctor:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   22
         Top             =   600
         Width           =   615
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1755
      Left            =   6600
      TabIndex        =   9
      Top             =   0
      Width           =   4275
      Begin VB.CommandButton cmdB 
         Caption         =   "&Anterior"
         Height          =   375
         Index           =   1
         Left            =   3060
         TabIndex        =   15
         Top             =   1140
         Width           =   975
      End
      Begin VB.CommandButton cmdB 
         Caption         =   "Si&guiente"
         Height          =   375
         Index           =   0
         Left            =   3060
         TabIndex        =   10
         Top             =   660
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   900
         TabIndex        =   4
         Top             =   1320
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   900
         TabIndex        =   3
         Top             =   960
         Width           =   1995
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   3060
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   900
         TabIndex        =   2
         Top             =   600
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   900
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   1320
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   960
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   12
         Top             =   600
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   11
         Top             =   240
         Width           =   525
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10920
      TabIndex        =   8
      Top             =   1080
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pacientes D�a"
      ForeColor       =   &H00C00000&
      Height          =   6675
      Left            =   180
      TabIndex        =   6
      Top             =   1800
      Width           =   11625
      Begin VB.CommandButton cmdVer 
         Caption         =   "Visi�n Global"
         Height          =   375
         Left            =   9840
         TabIndex        =   25
         Top             =   6120
         Width           =   1335
      End
      Begin SSDataWidgets_B.SSDBGrid grdconsultas 
         Height          =   5745
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   11385
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20082
         _ExtentY        =   10134
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmConsultaPacientesdia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strFecDesde$, strFecHasta$
Dim lngTipoAsist&
Dim strDptos As String
Private Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long 'para cargar los TreeViews a gran velocidad
Dim lngDptoSel&, lngTipoRec&
'variable utilizadas para b�squedas
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection 'historia|nombre|apel1|apel2|numActPlan|n� persona

Public Function Hay_Persona() As Boolean
If grdconsultas.Columns("Persona").Text <> "" Then
    Hay_Persona = True
Else
    MsgBox "No hay ninguna persona seleccionada", vbOKOnly
    Hay_Persona = False
End If
End Function
Private Sub cboDptos_Click()
lngDptoSel = cboDptos.Columns(0).Text
Call pCargarTiposRec
End Sub

Private Sub cboTipoRec_Click()
    lngTipoRec = cboTipoRec.Columns(0).Text
    
End Sub



Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub

Private Sub cmdBuscar_Click()
    Dim intN%, I%, blnE As Boolean
    Dim msg$, n%
    
    If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If

label:
        For I = 1 To cllBuscar.Count
            blnE = False
            awk1 = cllBuscar(I)
            If UCase(Left$(awk1.F(2), Len(txtBuscar(1).Text))) = UCase(txtBuscar(1).Text) _
            And UCase(Left$(awk1.F(3), Len(txtBuscar(2).Text))) = UCase(txtBuscar(2).Text) _
            And UCase(Left$(awk1.F(4), Len(txtBuscar(3).Text))) = UCase(txtBuscar(3).Text) Then
                If txtBuscar(0).Text = "" Then
                    blnE = True
                Else
                    If awk1.F(1) = txtBuscar(0).Text Then blnE = True
                End If
            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdconsultas.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hWnd
                    grdconsultas.MoveFirst
                    grdconsultas.MoveRecords I - 1
                    LockWindowUpdate 0&
                    grdconsultas.SelBookmarks.Add (grdconsultas.RowBookmark(grdconsultas.Row))
                    cmdB(0).SetFocus
                    Exit Sub
                End If
            End If
        Next I
        
        If intBuscar = 1 Then
            If n > 0 Then
                msg = "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda."
                MsgBox msg, vbInformation, Me.Caption
                Exit Sub
            Else
                msg = "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda."
                msg = msg & Chr$(13) & Chr$(13)
                msg = msg & "Se va a proceder a actualizar los datos de la pantalla y volver a intentar la b�squeda."
                If MsgBox(msg, vbInformation + vbYesNo, Me.Caption) = vbYes Then
                    n = 1
                    Call pActualizar(-1)
                    GoTo label
                End If
            End If
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
    Else
        blnBuscarOtro = False
    End If
End Sub


Private Sub cmdConsultar_Click()
    Call pActualizar(-1)
End Sub



Private Sub CmdImprimir_Click()
    Dim msg$, strW$, strFD$, strFH$
Dim strd As String
Dim strm As String

    If grdconsultas.Rows = 0 Then Exit Sub
     Screen.MousePointer = vbHourglass
    crReport1.ReportFileName = objApp.strReportsPath & "Pacientesdia.rpt"
     If Day(strFecDesde) < 10 Then
      strd = "0" & Day(strFecDesde)
    Else
      strd = Day(strFecDesde)
    End If
    If Month(strFecDesde) < 10 Then
      strm = "0" & Month(strFecDesde)
    Else
     strm = Month(strFecDesde)
    End If
    If lngDptoSel > 0 Then
    strW = "{PR0464J.AD02CODDPTO} =" & lngDptoSel
    strFD = strFecDesde
    strFH = strFecDesde
   
    
    strW = strW & " AND {PR0464J.CI01FECCONCERT} >= '" & Year(strFD) & "/" & strm & "/" & strd & " 00:00:00.0'"
    strW = strW & " AND {PR0464J.CI01FECCONCERT} <= '" & Year(strFH) & "/" & strm & "/" & strd & " 23:59:59'"

    Else
    strFD = strFecDesde
    strFH = strFecDesde
    
    strW = "{PR0464J.CI01FECCONCERT} >= '" & Year(strFD) & "/" & strm & "/" & strd & " 00:00:00.0'"
    strW = strW & " AND {PR0464J.CI01FECCONCERT} <= '" & Year(strFH) & "/" & strm & "/" & strd & " 23:59:59'"
    End If
    
        If lngTipoRec > 0 Then
    strW = strW & " AND {PR0464J.AG11CODRECURSO} =" & lngTipoRec
    End If
    
    With crReport1
        .PrinterCopies = 1
        .Destination = crptToPrinter
        .SelectionFormula = objGen.ReplaceStr(strW, "#", Chr(34), 0)
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
       .Action = 1
        Screen.MousePointer = vbDefault
    End With
End Sub
Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdVer_Click()
Dim arData() As Variant
Me.MousePointer = vbHourglass
If Not Hay_Persona Then Exit Sub
ReDim vntData(1) As Variant
cmdVer.Enabled = False
ReDim arData(1 To 2) As Variant
arData(1) = grdconsultas.Columns("Persona").Text
arData(2) = 2
Call objSecurity.LaunchProcess("HC02", arData())
Me.MousePointer = vbDefault
cmdVer.Enabled = True
End Sub

Private Sub Form_Load()
    Dim strHoy$
  
    Call Cargar_Dpto
    Call pCargarTiposRec
    cboTipoRec.Text = "TODOS"
    
    Call pFormatearGrids
    
    
    strHoy = strFecha_Sistema
    dcboDesde.Date = strHoy


    dcboDesde.BackColor = objApp.objUserColor.lngMandatory


End Sub
Private Sub Cargar_Dpto()
 Dim sql$, rs As rdoResultset, qry As rdoQuery
 
    sql = "SELECT AD02CODDPTO, AD02DESDPTO"
        sql = sql & " FROM AD0200"
        sql = sql & " WHERE AD02INDRESPONPROC=-1 AND (AD02FECFIN > SYSDATE OR AD02FECFIN IS NULL)"
        sql = sql & " ORDER BY AD02DESDPTO"
        cboDptos.AddItem "0" & Chr$(9) & "TODOS"
        cboDptos.Text = "TODOS"
        lngDptoSel = 0
        
       
    
    Set rs = objApp.rdoConnect.OpenResultset(sql)
     strDptos = "x"
    Do While Not rs.EOF
    
        cboDptos.AddItem rs(0) & Chr$(9) & rs(1)
        If strDptos = "x" Then
          strDptos = rs(0)
        Else
          strDptos = strDptos & "," & rs(0)
        End If
        
        rs.MoveNext
    Loop
    rs.Close
End Sub
Private Sub pActualizar(intCol%)
    Dim sql$, qry As rdoQuery, rs As rdoResultset
    Dim strFecPlan$, strHoraPlan$, strCama$
    Dim strApel1$, strApel2$
    Dim str1$, str2$, str3$
    Dim n%
    Static sqlOrder$
        
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
      If sqlOrder = "" Then sqlOrder = " ORDER BY AD02DESDPTO,CI01FECCONCERT, PACIENTE" 'Opci�n por defecto: DPTO Y PACIENTE
    Else
        Select Case UCase(grdconsultas.Columns(intCol).Caption)
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE"
        Case UCase("Historia"): sqlOrder = " ORDER BY CI22NUMHISTORIA, PACIENTE"
        Case UCase("Consulta"): sqlOrder = " ORDER BY PR01DESCORTA, PACIENTE"
        Case UCase("Dpto"): sqlOrder = " ORDER BY AD02DESDPTO, PACIENTE"
        Case UCase("Doctor"): sqlOrder = " ORDER BY AG11DESRECURSO"
        Case UCase("Fecha"), UCase("Hora"): sqlOrder = " ORDER BY CI01FECCONCERT, PACIENTE"
        Case UCase("Tipo economico"): sqlOrder = " ORDER BY CI32CODTIPECON, PACIENTE"
        Case UCase("Entidad"): sqlOrder = " ORDER BY CI13CODENTIDAD, PACIENTE"
        Case UCase("Estado"): sqlOrder = " ORDER BY PR37DESESTADO, PACIENTE"
        Case Else: Exit Sub
        End Select
    End If
    
    'Fechas
    strFecDesde = dcboDesde.Date


    
    'vaciado previo del grid y de la colecci�n de b�squeda
    grdconsultas.RemoveAll
    Do While cllBuscar.Count > 0: cllBuscar.Remove 1: Loop
    
    'se cambia el scrollbar para evitar el efecto de llenado del grid dejando la �ltima _
    fila vacia durante un cierto rato
    grdconsultas.ScrollBars = ssScrollBarsNone

    sql = "SELECT"
    sql = sql & " PR0464J.CI22NUMHISTORIA,"
    sql = sql & " PR0464J.CI22PRIAPEL, PR0464J.CI22SEGAPEL, PR0464J.CI22NOMBRE,"
    sql = sql & " PR0464J.CI22PRIAPEL||' '||PR0464J.CI22SEGAPEL||', '||PR0464J.CI22NOMBRE PACIENTE,PR0464J.CI01FECCONCERT,"
    sql = sql & " PR0464J.PR01DESCORTA,PR0464J.PR37DESESTADO, PR0464J.AD02DESDPTO, PR0464J.AG11DESRECURSO,PR0464J.CI32CODTIPECON,PR0464J.CI13CODENTIDAD,PR0464J.CI21CODPERSONA"
    sql = sql & " FROM  PR0464J"
    sql = sql & " WHERE "
    
       If lngDptoSel <> 0 Then
    sql = sql & "PR0464J.AD02CODDPTO = ?"
    sql = sql & " AND (PR0464J.CI01FECCONCERT) >= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    sql = sql & " AND (PR0464J.CI01FECCONCERT)<= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    Else
    'sql = sql & "PR0400.AD02CODDPTO IN(" & strDptos & ")"
    sql = sql & "(PR0464J.CI01FECCONCERT) >= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    sql = sql & " AND (PR0464J.CI01FECCONCERT)<= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    End If
    If lngTipoRec > 0 Then
    sql = sql & " AND PR0464J.AG11CODRECURSO = ?"
    End If
    sql = sql & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        If lngDptoSel <> 0 Then
        qry(0) = lngDptoSel
        strFecDesde = Format(strFecDesde, "DD/MM/YYYY")
        strFecHasta = strFecDesde & " 23:59:59"
        strFecDesde = strFecDesde & " 00:00:00"
        qry(1) = strFecDesde
        qry(2) = strFecHasta

    
        n = 3
       Else
       strFecDesde = Format(strFecDesde, "DD/MM/YYYY")
        strFecHasta = strFecDesde & " 23:59:59"
        strFecDesde = strFecDesde & " 00:00:00"

        qry(0) = strFecDesde
        qry(1) = strFecHasta
        n = 2
       End If
       
      
  
    If lngTipoRec > 0 Then qry(n) = lngTipoRec 'n: n� de par�metro del query
    Set rs = qry.OpenResultset()
    If rs.EOF Then
      MsgBox "No hay ning�n paciente citado", vbOKOnly + vbInformation
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    
    Do While Not rs.EOF
         strFecPlan = Format(rs!CI01FECCONCERT, "dd/mm/yyyy")
         strHoraPlan = Format(rs!CI01FECCONCERT, "hh:mm")
       grdconsultas.AddItem rs!CI22NUMHISTORIA & Chr$(9) _
                            & rs!Paciente & Chr$(9) _
                            & strFecPlan & Chr$(9) _
                            & strHoraPlan & Chr$(9) _
                            & rs!PR01DESCORTA & Chr$(9) _
                            & rs!PR37DESESTADO & Chr$(9) _
                            & rs!AD02DESDPTO & Chr$(9) _
                            & rs!AG11DESRECURSO & Chr$(9) _
                            & rs!CI32CODTIPECON & Chr$(9) _
                            & rs!CI13CODENTIDAD & Chr$(9) _
                            & rs!CI21CODPERSONA
        If IsNull(rs!CI22PRIAPEL) Then strApel1 = "" Else strApel1 = rs!CI22PRIAPEL
        If IsNull(rs!CI22SEGAPEL) Then strApel2 = "" Else strApel2 = rs!CI22SEGAPEL
        cllBuscar.Add rs!CI22NUMHISTORIA & "|" & rs!CI22NOMBRE & "|" & strApel1 & "|" & strApel2
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'se pone el scrollbar en su estado normal
    grdconsultas.ScrollBars = ssScrollBarsAutomatic

    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatearGrids()
    Dim I%
    With grdconsultas
        .Columns(0).Caption = "Historia"
        .Columns(0).Width = 700
        .Columns(1).Caption = "Paciente"
        .Columns(1).Width = 2800
        .Columns(2).Caption = "Fecha"
        .Columns(2).Width = 1000
        .Columns(3).Caption = "Hora"
        .Columns(3).Width = 600
        .Columns(4).Caption = "Consulta"
        .Columns(4).Width = 2300
        .Columns(5).Caption = "Estado"
        .Columns(5).Width = 1000
        .Columns(6).Caption = "Dpto"
        .Columns(6).Width = 1500
        .Columns(7).Caption = "Doctor"
        .Columns(7).Width = 1700
        .Columns(8).Caption = "Tipo economico"
        .Columns(8).Width = 1500
        .Columns(9).Caption = "Entidad"
        .Columns(9).Width = 700
        .Columns(10).Caption = "Persona"
        .Columns(10).Width = 500
        .Columns(10).Visible = False
        
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Function fBlnBuscar() As Boolean
    Dim I%
    For I = 0 To txtBuscar.Count - 1
        If txtBuscar(I).Text <> "" Then fBlnBuscar = True: Exit Function
    Next I
End Function

Private Sub Frame3_DragDrop(Source As Control, X As Single, Y As Single)

End Sub




Private Sub grdconsultas_Click()
    If grdconsultas.SelBookmarks.Count > 1 Then
        If grdconsultas.Columns("Historia").CellText(grdconsultas.SelBookmarks(0)) <> grdconsultas.Columns("Historia").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, Me.Caption
            grdconsultas.SelBookmarks.Remove grdconsultas.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub grdconsultas_HeadClick(ByVal ColIndex As Integer)
    If grdconsultas.Rows > 0 Then Call pActualizar(ColIndex)
End Sub

Private Sub txtBuscar_Change(Index As Integer)
    cmdB(0).Enabled = False: cmdB(1).Enabled = False
End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    txtBuscar(Index).SelStart = 0: txtBuscar(Index).SelLength = Len(txtBuscar(Index).Text)
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub pCargarTiposRec()
     Dim sql$, qry As rdoQuery, rs As rdoResultset
    cboTipoRec.RemoveAll
    sql = "SELECT AG11CODRECURSO, SG02COD,AG11DESRECURSO"
    sql = sql & " FROM AG1100"
    sql = sql & " WHERE AG11FECFINVREC > SYSDATE OR AG1100.AG11FECFINVREC IS NULL AND SG02COD IS NOT NULL"
    If lngDptoSel <> 0 Then
    sql = sql & " AND AD02CODDPTO = ?"
    
    
    
    End If
    sql = sql & " ORDER BY AG11DESRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    If lngDptoSel <> 0 Then
    qry(0) = lngDptoSel
    End If
    
    Set rs = qry.OpenResultset()
    
    
    cboTipoRec.AddItem "0" & Chr$(9) & "TODOS" & Chr$(9) & "TODOS"
    cboTipoRec.Text = "TODOS"
    lngTipoRec = 0

    Do While Not rs.EOF
        cboTipoRec.AddItem rs(0) & Chr$(9) & rs(1) & Chr$(9) & rs(2)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
End Sub

