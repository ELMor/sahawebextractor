Attribute VB_Name = "AD00101"
Option Explicit

Public Sub Main()

End Sub
Public Function strFechaHora_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select " & _
"to_char(sysdate,'DD/MM/YYYY HH24:MI:SS') from dual")
  strFechaHora_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function

Public Function strHora_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'hh24:mi:ss') from dual")
  strHora_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function

Public Function strFecha_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY') from dual")
  strFecha_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function


Public Function BuscaResponsable(lngCodPersona As Double) As String
    Dim strSQL As String
    Dim rscod As rdoResultset
    
        strSQL = " SELECT CI21CODPERSONA_REC FROM CI2200 WHERE  CI21CODPERSONA=" & lngCodPersona
        Set rscod = objApp.rdoConnect.OpenResultset(strSQL)
        If Not IsNull(rscod.rdoColumns(0)) Then
            BuscaResponsable = rscod.rdoColumns(0)
        Else
            BuscaResponsable = lngCodPersona
        End If
End Function
Public Function bDBPacienteIngresado(sCodPaciente As String, sMensaje As String) As Boolean

    Dim sStmSql     As String
    Dim qrySelect   As rdoQuery
    Dim rdoSelect    As rdoResultset

    bDBPacienteIngresado = False
    sMensaje = ""
    sStmSql = "SELECT AD0100.CI21CODPERSONA, "
    sStmSql = sStmSql & "CI2200.CI22NOMBRE, "
    sStmSql = sStmSql & "CI2200.CI22PRIAPEL, "
    sStmSql = sStmSql & "CI2200.CI22SEGAPEL, "
    sStmSql = sStmSql & "GCFN06(AD1500.AD15CODCAMA) "
    sStmSql = sStmSql & "FROM AD0100, CI2200, AD1500 "
    sStmSql = sStmSql & "WHERE AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA "
    sStmSql = sStmSql & "AND  AD0100.AD34CODESTADO = 1 "
    sStmSql = sStmSql & "AND  AD0100.ad01CODASISTENCI= AD1500.ad01CODASISTENCI "
    sStmSql = sStmSql & "AND  AD0100.AD01FECFIN IS NULL "
    sStmSql = sStmSql & "AND  AD0100.CI21CODPERSONA = ? "
    sStmSql = sStmSql & "AND  AD1500.AD02CODDPTO <> " & constDPTO_QUIROFANO
    sStmSql = sStmSql & " AND AD1500.AD14CODESTCAMA = 2  "
    Set qrySelect = objApp.rdoConnect.CreateQuery("", sStmSql)
        qrySelect(0) = Trim(sCodPaciente)
        qrySelect.OpenResultset
        Set rdoSelect = qrySelect.OpenResultset(sStmSql)
        If Not rdoSelect.EOF Then
            If Trim(sCodPaciente) = Trim(rdoSelect.rdoColumns(0)) Then
                bDBPacienteIngresado = True
                sMensaje = "El paciente:  " & Trim(rdoSelect.rdoColumns(1)) & " " & Trim(rdoSelect.rdoColumns(2)) & " " & Trim(rdoSelect.rdoColumns(3)) & Chr(13)
                sMensaje = sMensaje & "esta ya ingresado en la cama: " & Trim(rdoSelect.rdoColumns(4))
            Else
                bDBPacienteIngresado = False
                sMensaje = ""
            End If
        End If
End Function

