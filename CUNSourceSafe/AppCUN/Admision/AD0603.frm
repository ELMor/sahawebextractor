VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmSalidasArchivo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION.Salidas de Archivo"
   ClientHeight    =   8625
   ClientLeft      =   -45
   ClientTop       =   285
   ClientWidth     =   11910
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   "Copiar Dpto y Motivo"
      Height          =   375
      Index           =   4
      Left            =   5520
      TabIndex        =   6
      Top             =   7845
      Width           =   1935
   End
   Begin VsOcxLib.VideoSoftAwk vs 
      Left            =   4320
      Top             =   7920
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   " "
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Salir"
      Height          =   375
      Index           =   3
      Left            =   10080
      TabIndex        =   4
      Top             =   7845
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Confirmar"
      Height          =   375
      Index           =   2
      Left            =   7920
      TabIndex        =   3
      Top             =   7845
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Borrar Todo"
      Height          =   375
      Index           =   1
      Left            =   2010
      TabIndex        =   2
      Top             =   7845
      Width           =   1560
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Borrar Seleccionada"
      Height          =   375
      Index           =   0
      Left            =   135
      TabIndex        =   1
      Top             =   7845
      Width           =   1560
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   8325
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   529
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   7725
      HelpContextID   =   2
      Index           =   0
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11775
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   12
      stylesets.count =   1
      stylesets(0).Name=   "Urgente"
      stylesets(0).BackColor=   255
      stylesets(0).Picture=   "AD0603.frx":0000
      SelectTypeRow   =   3
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   16776960
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   12
      Columns(0).Width=   1482
      Columns(0).Caption=   "Historia"
      Columns(0).Name =   "Historia"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4022
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Parte del Sobre"
      Columns(2).Name =   "Parte del Sobre"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   979
      Columns(3).Caption=   "Cod Dpto Destino"
      Columns(3).Name =   "Cod Dpto Destino"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   2037
      Columns(4).Caption=   "Dpto Destino"
      Columns(4).Name =   "Dpto Destino"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "Dr. Solicitante"
      Columns(5).Name =   "Dr. Solicitante"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      Columns(5).Style=   3
      Columns(6).Width=   4419
      Columns(6).Caption=   "Motivo"
      Columns(6).Name =   "Motivo"
      Columns(6).DataField=   "Column 6"
      Columns(6).DataType=   8
      Columns(6).FieldLen=   256
      Columns(7).Width=   2328
      Columns(7).Caption=   "Fecha"
      Columns(7).Name =   "Fecha"
      Columns(7).DataField=   "Column 7"
      Columns(7).DataType=   8
      Columns(7).FieldLen=   256
      Columns(8).Width=   1984
      Columns(8).Caption=   "Hora"
      Columns(8).Name =   "Hora"
      Columns(8).DataField=   "Column 8"
      Columns(8).DataType=   8
      Columns(8).FieldLen=   256
      Columns(9).Width=   1402
      Columns(9).Caption=   "Usuario"
      Columns(9).Name =   "Usuario"
      Columns(9).DataField=   "Column 9"
      Columns(9).DataType=   8
      Columns(9).FieldLen=   256
      Columns(10).Width=   1032
      Columns(10).Caption=   "Cod Dpto OR"
      Columns(10).Name=   "Cod Dpto OR"
      Columns(10).DataField=   "Column 10"
      Columns(10).DataType=   8
      Columns(10).FieldLen=   256
      Columns(11).Width=   2408
      Columns(11).Caption=   "Dpto 0rigen"
      Columns(11).Name=   "Dpto 0rigen"
      Columns(11).DataField=   "Column 11"
      Columns(11).DataType=   8
      Columns(11).FieldLen=   256
      _ExtentX        =   20770
      _ExtentY        =   13626
      _StockProps     =   79
   End
End
Attribute VB_Name = "frmSalidasArchivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strHistoria As String
Dim intTipo As Integer

Private Sub Command1_Click(Index As Integer)
Select Case Index
    Case 0 'BORRAR SELECCI�N
        pBorrarSeleccion
    Case 1 'BORRAR TODO
        grdDBGrid1(0).RemoveAll
        If grdDBGrid1(0).Rows = 0 Then
            grdDBGrid1(0).AddNew
        End If
        SendKeys "{TAB}"
    Case 2 'CONFIRMAR
        Call pConfirEntradas
    Case 3 'SALIR
        Unload Me
    Case 4
        pRepetirDpto
End Select
End Sub

Private Sub Form_Load()
grdDBGrid1(0).AddNew
End Sub






Private Sub grdDBGrid1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If grdDBGrid1(0).Col = 0 Then
    If KeyCode = 13 Then
'     KeyCode = 9
     strHistoria = Left(grdDBGrid1(0).Columns(0).Value, Len(grdDBGrid1(0).Columns(0).Value) - 2)
     intTipo = Int(Right(grdDBGrid1(0).Columns(0).Value, 2))
     Call LlenarLinea
    End If
Else
    If KeyCode = 13 Then KeyCode = 9
End If
End Sub
Private Sub grdDBGrid1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
  If grdDBGrid1(0).Col = 4 Then
     If KeyCode = 9 Or KeyCode = 13 Then
        pDptoDoctores
    End If
  End If
End Sub
Private Sub LlenarLinea()
Dim sql As String
Dim qry As rdoQuery
Dim rs  As rdoResultset
'NOMBRE
sql = "SELECT CI22NOMBRE||' '||CI22PRIAPEL||' '||CI22SEGAPEL NOMBRE FROM CI2200 WHERE"
sql = sql & " CI22NUMHISTORIA = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(strHistoria)
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    grdDBGrid1(0).Columns("Nombre").Value = rs(0).Value
Else
    MsgBox "No existe esta historia", vbCritical
    Exit Sub
End If
rs.Close
qry.Close
sql = "SELECT AD20DESPARTSOBRE FROM AD2000 WHERE AD20CODPARTSOBRE = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = intTipo
Set rs = qry.OpenResultset()
'DEPARTAMENTO DESTINO Y ORIGEN
grdDBGrid1(0).Columns("Cod Dpto OR").Value = "8"
grdDBGrid1(0).Columns("Dpto 0rigen").Value = "Archivo"
grdDBGrid1(0).Columns("Usuario").Value = objSecurity.strUser
grdDBGrid1(0).Columns("Parte del Sobre").Value = intTipo & " " & rs!AD20DESPARTSOBRE

grdDBGrid1(0).Columns("Fecha").Value = strFecha_Sistema
grdDBGrid1(0).Columns("Hora").Value = strHora_Sistema
grdDBGrid1(0).Columns(0).Value = strHistoria
grdDBGrid1(0).AddNew
SendKeys "{TAB}"
End Sub

Private Sub pBorrarSeleccion()
Dim i As Integer
If grdDBGrid1(0).SelBookmarks.Count = 0 Then
    MsgBox "Por favor, Seleccione la historia para borrar", vbCritical
    Exit Sub
End If

'grdDBGrid1(0).SelBookmarks.item
grdDBGrid1(0).RemoveItem grdDBGrid1(0).Row
grdDBGrid1(0).Refresh

grdDBGrid1(0).AddNew
SendKeys "{TAB}"
End Sub
Private Sub pConfirEntradas()
Dim i As Integer
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
'On Error Resume Next
 grdDBGrid1(0).MoveFirst
 sql = "INSERT INTO AD2100 (CI22NUMHISTORIA,AD21FECMOVIMIENT,SG02COD_USU,"
 sql = sql & " AD20CODPARTSOBRE,AD02CODDPTO_ORI,AD02CODDPTO_DES,SG02COD_DOC,AD21DESMOTIMOVIM)"
 sql = sql & " VALUES (?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?,?,?,?,?)"
 objApp.BeginTrans
 Set qry = objApp.rdoConnect.CreateQuery("", sql)
 
 Do While grdDBGrid1(0).Columns("Historia").Value <> ""
    strHistoria = grdDBGrid1(0).Columns("Historia").Value
    intTipo = Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1)
    
    qry(0) = Trim(grdDBGrid1(0).Columns("Historia").Value)
    qry(1) = Format(grdDBGrid1(0).Columns("Fecha").Value & " " & grdDBGrid1(0).Columns("Hora").Value, _
    "dd/mm/yyyy hh:mm")
    qry(2) = grdDBGrid1(0).Columns("Usuario").Value
    qry(3) = Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1)
    qry(4) = grdDBGrid1(0).Columns("Cod dpto OR").Value
    qry(5) = grdDBGrid1(0).Columns("Cod Dpto Destino").Value
    vs = grdDBGrid1(0).Columns("Dr. Solicitante").Value
    qry(6) = vs.F(1)
    If Trim(grdDBGrid1(0).Columns("Motivo").Value) <> "" Then
        qry(7) = grdDBGrid1(0).Columns("Motivo").Value
    Else
        qry(7) = Null
    End If
    qry.Execute
    If err > 0 Then
        MsgBox "SQL: " & sql & Chr$(13) & qry(0) & Chr$(13) & qry(0) & Chr$(13) & qry(0) & Chr$(13) & qry(0) & Chr$(13) & qry(0), vbOKOnly
        MsgBox "Error confirmando la fila " & grdDBGrid1(0).Row & Chr$(13) & _
        "Confirme los datos", vbCritical
        objApp.RollbackTrans
        Exit Sub
    End If
    grdDBGrid1(0).MoveNext
    If strHistoria = grdDBGrid1(0).Columns("Historia").Value And intTipo = Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1) Then
        Exit Do
    End If
Loop
objApp.CommitTrans
MsgBox "SALIDAS CONFIRMADAS", vbOKOnly
grdDBGrid1(0).RemoveAll
If grdDBGrid1(0).Rows = 0 Then
    grdDBGrid1(0).AddNew
End If
'grdDBGrid1(0).RemoveItem 1
SendKeys "{TAB}"
End Sub

Private Sub pDptoDoctores()
Dim sql As String
Dim qry As rdoQuery
Dim rs  As rdoResultset


On Error Resume Next
grdDBGrid1(0).Columns("Dpto Destino").Value = ""
grdDBGrid1(0).Columns("Dr. Solicitante").Value = ""

sql = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(grdDBGrid1(0).Columns("Cod Dpto Destino").Value)
Set rs = qry.OpenResultset()

If rs.EOF Then
    MsgBox "Departamento Inexistente", vbCritical
    Exit Sub
End If
grdDBGrid1(0).Columns("Dpto Destino").Value = rs!AD02DESDPTO
rs.Close
qry.Close

sql = "SELECT SG02COD||' '||SG02NOM||' '||SG02APE1||' '||SG02APE2 DOCTOR FROM SG0200  where SG02COD IN (SELECT SG02COD FROM AD0300 WHERE AD02CODDPTO= ?" & _
  " AND (AD03FECFIN > SYSDATE OR AD03FECFIN IS NULL)" & _
  " AND AD31CODPUESTO IN (1,5,6,9))"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Trim(grdDBGrid1(0).Columns("Cod Dpto Destino").Value)
Set rs = qry.OpenResultset()

grdDBGrid1(0).Columns("Dr. Solicitante").RemoveAll
Do While Not rs.EOF
    grdDBGrid1(0).Columns("Dr. Solicitante").AddItem rs!DOCTOR
    rs.MoveNext
Loop

End Sub

Private Sub pRepetirDpto()
Dim strDpto As String
Dim strMotivo As String
strDpto = grdDBGrid1(0).Columns("Cod Dpto Destino").Value
strMotivo = grdDBGrid1(0).Columns("Motivo").Value
grdDBGrid1(0).MoveFirst
Do While grdDBGrid1(0).Columns("Historia").Value <> ""
    strHistoria = grdDBGrid1(0).Columns("Historia").Value
    intTipo = Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1)
    grdDBGrid1(0).Columns("Motivo").Value = strMotivo
    grdDBGrid1(0).Columns("Cod Dpto Destino").Value = strDpto
    Call pDptoDoctores
    grdDBGrid1(0).MoveNext
    If strHistoria = grdDBGrid1(0).Columns("Historia").Value And intTipo = Left(grdDBGrid1(0).Columns("Parte del Sobre").Value, 1) Then
        Exit Do
    End If
Loop
End Sub

