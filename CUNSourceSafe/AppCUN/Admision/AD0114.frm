VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "idperson.ocx"
Begin VB.Form frmReasignarAsistencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Reasignar Asistencia a Proceso"
   ClientHeight    =   7635
   ClientLeft      =   345
   ClientTop       =   720
   ClientWidth     =   11010
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7635
   ScaleWidth      =   11010
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11010
      _ExtentX        =   19420
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Procesos del Paciente"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   3
      Left            =   5580
      TabIndex        =   16
      Top             =   4650
      Width           =   5310
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1680
         Index           =   3
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   5025
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   8864
         _ExtentY        =   2963
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Procesos de la Asistencia"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   2
      Left            =   60
      TabIndex        =   14
      Top             =   4650
      Width           =   5130
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1680
         Index           =   2
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   4905
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   8652
         _ExtentY        =   2963
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Asistencia"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Index           =   1
      Left            =   60
      TabIndex        =   9
      Top             =   2670
      Width           =   10845
      Begin TabDlg.SSTab tabTab1 
         Height          =   1455
         HelpContextID   =   90001
         Index           =   1
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   10545
         _ExtentX        =   18600
         _ExtentY        =   2566
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Det&alle"
         TabPicture(0)   =   "AD0114.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(74)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(6)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(9)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtMinuto(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtHora(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "&Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   0
            Left            =   3510
            MaxLength       =   2
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   570
            Width           =   390
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            HelpContextID   =   3
            Index           =   0
            Left            =   4020
            MaxLength       =   2
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   570
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD01CODASISTENCI"
            Height          =   315
            HelpContextID   =   40101
            Index           =   9
            Left            =   240
            MaxLength       =   10
            TabIndex        =   3
            Tag             =   "C�digo de Asistencia|C�digo de Asistencia"
            Top             =   570
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   315
            HelpContextID   =   40101
            Index           =   6
            Left            =   8520
            MaxLength       =   7
            TabIndex        =   5
            Top             =   300
            Visible         =   0   'False
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1275
            Index           =   1
            Left            =   -74940
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   90
            Width           =   10035
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17701
            _ExtentY        =   2249
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD01FECINICIO"
            Height          =   330
            Index           =   1
            Left            =   1590
            TabIndex        =   4
            Tag             =   "Fecha de Inicio|Fecha de Inicio "
            Top             =   570
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   3930
            TabIndex        =   22
            Top             =   630
            Width           =   75
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   3510
            TabIndex        =   21
            Top             =   330
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   1590
            TabIndex        =   13
            Top             =   300
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Asistencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   74
            Left            =   270
            TabIndex        =   12
            Top             =   300
            Width           =   885
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2145
      Index           =   0
      Left            =   60
      TabIndex        =   6
      Top             =   480
      Width           =   10845
      Begin TabDlg.SSTab tabTab1 
         Height          =   1665
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   10515
         _ExtentX        =   18547
         _ExtentY        =   2937
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "&Detalle"
         TabPicture(0)   =   "AD0114.frx":001C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "IdPersona1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "&Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1425
            Index           =   0
            Left            =   -74820
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   120
            Width           =   9915
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17489
            _ExtentY        =   2514
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1335
            Left            =   120
            TabIndex        =   18
            Top             =   120
            Width           =   9975
            _ExtentX        =   17595
            _ExtentY        =   2355
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
      End
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Reasignar"
      Height          =   375
      Index           =   0
      Left            =   4770
      TabIndex        =   2
      Top             =   6900
      Width           =   1335
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   7350
      Width           =   11010
      _ExtentX        =   19420
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmReasignarAsistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim objMultiInfoPAS As New clsCWForm
  Dim objMultiInfoPAC As New clsCWForm
  Dim strKey As String
  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    .strName = "Pacientes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "CI2200"
    .strWhere = " CI2200.AD34CODESTADO = 1 "
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
  End With
  
  With objDetailInfo
    .strName = "Asistencias"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "AD0100"
    .strWhere = " AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 "
    Call .FormAddOrderField("AD01CODASISTENCI", cwAscending)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)              'nuevo
    .intAllowance = cwAllowReadOnly
    
    Call .FormCreateFilterWhere("AD0100", "Tabla de Asistencia del Paciente")
    Call .FormAddFilterWhere("AD0100", "AD01CODASISTENCI", "C�digo Asistencia", cwNumeric)
    Call .FormAddFilterOrder("AD0100", "AD01CODASISTENCI", "C�digo Situaci�n Asistencia")
  End With
   
  With objMultiInfoPAS
    .strName = "Procesos de la Asistencia"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intAllowance = cwAllowReadOnly
    .strTable = "AD0801J"
    
    Call .FormAddOrderField("AD07DESNOMBPROCE", cwAscending)
    Call .FormAddRelation("AD01CODASISTENCI", txtText1(9))
  End With
  
  With objMultiInfoPAC
    .strName = "Procesos del Paciente"
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intAllowance = cwAllowReadOnly
    .strTable = "AD0700"
    .strWhere = " AD07FECHORAFIN IS NULL" & " AND AD0700.AD34CODESTADO = 1 "
    
    Call .FormAddOrderField("AD07DESNOMBPROCE", cwAscending)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)             'nuevo
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfoPAS, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfoPAC, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfoPAS, "Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfoPAS, "C�digo", "AD07CODPROCESO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfoPAS, "Proceso", "AD07DESNOMBPROCE", cwString, 30)
                          
    Call .GridAddColumn(objMultiInfoPAC, "Persona", "CI21CODPERSONA", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfoPAC, "C�digo", "AD07CODPROCESO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfoPAC, "Proceso", "AD07DESNOMBPROCE", cwString, 30)
    
    Call .FormCreateInfo(objMasterInfo)
        
    .CtrlGetInfo(txtText1(6)).blnInGrid = False
    
    .CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnInGrid = False
    .CtrlGetInfo(grdDBGrid1(2).Columns(4)).blnInGrid = False
    
    .CtrlGetInfo(grdDBGrid1(3).Columns(3)).blnInGrid = False
    .CtrlGetInfo(grdDBGrid1(3).Columns(4)).blnInGrid = False
    
    grdDBGrid1(2).Columns(3).Visible = False
   ' grdDBGrid1(2).Columns(4).Visible = False
    
    grdDBGrid1(3).Columns(3).Visible = False
   ' grdDBGrid1(3).Columns(4).Visible = False
    
    .CtrlGetInfo(IdPersona1).blnForeign = True 'nuevo
    .CtrlGetInfo(IdPersona1).blnInFind = True  'nuevo
    IdPersona1.ReadPersona                     'nuevo
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  Call objWinInfo.CtrlSet(txtText1(6), IdPersona1)               'nuevo
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  Select Case strFormName
    Case "Asistencias"
      On Error Resume Next
      Call objWinInfo.CtrlSet(txtHora(0), Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AD01FECINICIO, "hh:mm"), 2))
      Call objWinInfo.CtrlSet(txtMinuto(0), Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AD01FECINICIO, "hh:mm"), 2))
  End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  If btnButton.Index = 16 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  If intIndex < 2 Then
    Call objWinInfo.CtrlGotFocus
  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
  If intIndex < 2 Then
    Call objWinInfo.GridDblClick
  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  If intIndex < 2 Then
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  If intIndex < 2 Then
    Call objWinInfo.CtrlDataChange
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim vntA As Variant
  Dim rs08 As rdoResultset
  Dim rs05 As rdoResultset
  Dim intI As Integer
  Dim intFila As Variant
  Dim strSQL As String
  Dim rstfec As rdoResultset
  Dim strfec As String
  
  ' Comprobar que se ha seleccionado un proceso de la Asistencia
  If grdDBGrid1(2).Rows > 0 Then
    If grdDBGrid1(2).SelBookmarks.Count = 0 Then
      Call objError.SetError(cwCodeMsg, "Debe selecionar alg�n Proceso de la Asistencia", vntA)
      vntA = objError.Raise
      Exit Sub
    End If
  End If
  
  ' Comprobar que se ha seleccionado un proceso del Paciente
  If grdDBGrid1(3).Rows > 0 Then
    If grdDBGrid1(3).SelBookmarks.Count = 0 Then
      Call objError.SetError(cwCodeMsg, "Debe selecionar alg�n Proceso del Paciente", vntA)
      vntA = objError.Raise
      Exit Sub
    End If
  End If
  
  intFila = grdDBGrid1(2).Bookmark
  
  ' Comprobar que el proceso del paciente seleccionado, no se encuentre ya
  ' en los Procesos Asistencias del Paciente
  For intI = 0 To grdDBGrid1(2).Rows
    grdDBGrid1(2).Bookmark = intI
    If grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row)) = _
      grdDBGrid1(2).Columns(4).Text Then
      Call objError.SetError(cwCodeMsg, "El Proceso del Paciente seleccionado ya est� asociado a la Asistecia", vntA)
      vntA = objError.Raise
      Exit Sub
    End If
  Next intI
  
  ' Actualizar tabla Proceso-Asistencia AD0800
 grdDBGrid1(2).Bookmark = intFila
  Set rs08 = _
    objApp.rdoConnect.OpenResultset("SELECT AD07CODPROCESO, AD01CODASISTENCI, AD10CODTIPPACIEN, CI21CODPERSONA_ENV, CI21CODPERSONA_MED " & _
                                    "FROM AD0800 WHERE " & _
                                    "AD07CODPROCESO = " & grdDBGrid1(2).Columns(4).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row)) & " AND AD0800.AD34CODESTADO = 1 " & _
                                    " AND AD01CODASISTENCI = " & grdDBGrid1(2).Columns(3).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row)) _
                                    , rdOpenKeyset, rdConcurRowVer, rdExecDirect)
  strfec = "SELECT count(*) FROM AD0800 WHERE " & _
         "AD07CODPROCESO=" & grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row)) & _
         " AND AD0800.AD34CODESTADO = 1 " & " AND " & _
         "AD01CODASISTENCI=" & rs08.rdoColumns!AD01CODASISTENCI & _
         " AND " & _
         "AD08INDREQIM=0 AND AD08FECINICIO=SYSDATE"
  Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
  If rstfec.rdoColumns(0).Value = 0 Then
        strSQL = "INSERT INTO AD0800 (AD07CODPROCESO,AD01CODASISTENCI,AD10CODTIPPACIEN,CI21CODPERSONA_ENV,CI21CODPERSONA_MED,AD08INDREQIM,AD08FECINICIO) "
        strSQL = strSQL & "VALUES (" & grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row)) & ","
        strSQL = strSQL & rs08.rdoColumns!AD01CODASISTENCI & ","
        If Not IsNull(rs08.rdoColumns!AD10CODTIPPACIEN) Then
          strSQL = strSQL & "'" & rs08.rdoColumns!AD10CODTIPPACIEN & "',"
        Else
          strSQL = strSQL & "NULL,"
        End If
        If Not IsNull(rs08.rdoColumns!CI21CODPERSONA_ENV) Then
          strSQL = strSQL & rs08.rdoColumns!CI21CODPERSONA_ENV & ","
        Else
          strSQL = strSQL & "NULL,"
        End If
        If Not IsNull(rs08.rdoColumns!CI21CODPERSONA_MED) Then
          strSQL = strSQL & rs08.rdoColumns!CI21CODPERSONA_MED & ","
        Else
          strSQL = strSQL & "NULL,"
        End If
        strSQL = strSQL & "0,SYSDATE)"
        
        objApp.rdoConnect.Execute strSQL, 64
  End If
  rstfec.Close
  Set rstfec = Nothing
  
  ' Actualizar tabla Responsable de Proceso-Asistencia AD0500
  Set rs05 = _
    objApp.rdoConnect.OpenResultset("SELECT AD07CODPROCESO,AD01CODASISTENCI," & _
                                    "TO_CHAR(AD05FECINIRESPON,'DD/MM/YYYY HH24:MI:SS')," & _
                                    "AD02CODDPTO,SG02COD," & _
                                    "TO_CHAR(AD05FECFINRESPON,'DD/MM/YYYY HH24:MI:SS') " & _
                                    "FROM AD0500 WHERE " & _
                                    "AD07CODPROCESO = " & grdDBGrid1(2).Columns(4).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row)) & _
                                    " AND AD01CODASISTENCI = " & grdDBGrid1(2).Columns(3).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row)) _
                                    , rdOpenKeyset, rdConcurRowVer, rdExecDirect)
                                    
     
  For intI = 1 To rs05.RowCount
     strfec = "SELECT COUNT(*) FROM AD0500 WHERE " & _
            "AD07CODPROCESO=" & grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row)) & _
            " AND " & _
            "AD01CODASISTENCI=" & rs05.rdoColumns(1) & _
            " AND " & _
            "AD05FECINIRESPON=TO_DATE('" & rs05.rdoColumns(2) & "','DD/MM/YYYY HH24:MI:SS')" & _
            " AND " & _
            "AD02CODDPTO=" & "'" & rs05.rdoColumns(3) & "'" & _
            " AND " & _
            "SG02COD=" & "'" & rs05.rdoColumns(4) & "'"
    Set rstfec = objApp.rdoConnect.OpenResultset(strfec)
    If rstfec.rdoColumns(0).Value = 0 Then
            'rs05.MoveFirst
            strSQL = "INSERT INTO AD0500 (AD07CODPROCESO,AD01CODASISTENCI,AD05FECINIRESPON,"
            strSQL = strSQL & "AD02CODDPTO,SG02COD,AD05FECFINRESPON) "
            strSQL = strSQL & "VALUES (" & grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row)) & ","
            strSQL = strSQL & rs05.rdoColumns(1) & ",TO_DATE('"
            strSQL = strSQL & rs05.rdoColumns(2) & "','DD/MM/YYYY HH24:MI:SS'),'"
            strSQL = strSQL & rs05.rdoColumns(3) & "','"
            strSQL = strSQL & rs05.rdoColumns(4) & "',"
            If Not IsNull(rs05.rdoColumns(5)) Then
              strSQL = strSQL & "TO_DATE('"
              strSQL = strSQL & rs05.rdoColumns(5) & "','DD/MM/YYYY HH24:MI:SS'))"
            Else
              strSQL = strSQL & "NULL)"
            End If
            objApp.rdoConnect.Execute strSQL, 64
    End If
    rstfec.Close
    Set rstfec = Nothing
    rs05.MoveNext
  Next intI
  rs05.Close
  Set rs05 = Nothing
    
  'objApp.rdoConnect.Execute strSQL, 64
  strSQL = "DELETE AD0500 WHERE AD07CODPROCESO = "
  strSQL = strSQL & grdDBGrid1(2).Columns(4).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row))
  strSQL = strSQL & " AND AD01CODASISTENCI = " & grdDBGrid1(2).Columns(3).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row))
  objApp.rdoConnect.Execute strSQL, 64
  'objApp.rdoConnect.Execute "Commit", 64
    
 
  ' Actualizar Proceso en Petici�n Actuaci�n Pedida (PR0800)
  ' Se partir� de la tabla PR0400 Actuaci�n Planificada con la Asistencia seleccionada
  ' y se seleccionar�n todas las actuaciones pedidas, con ellas accederemos a la tabla
  ' PR0800
  'Dim rs As rdoResultset
  'Set rs = _
    objApp.rdoConnect.OpenResultset("SELECT PR03NUMACTPEDI " & _
                                    "FROM PR0400 WHERE " & _
                                    "AD01CODASISTENCI = " & grdDBGrid1(2).Columns(3).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row)) _
                                    , rdOpenDynamic)
  'Do While Not rs.EOF
    'strSQL = "UPDATE PR0800 SET AD07CODPROCESO = "
    'strSQL = strSQL & grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row))
    'strSQL = strSQL & " WHERE AD07CODPROCESO = "
    'strSQL = strSQL & grdDBGrid1(2).Columns(4).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row))
    'strSQL = strSQL & " AND PR03NUMACTPEDI = "
    'strSQL = strSQL & rs!PR03NUMACTPEDI
    'objApp.rdoConnect.Execute strSQL, 64
    'objApp.rdoConnect.Execute "Commit", 64
    'rs.MoveNext
  'Loop
  'rs.Close
  'Set rs = Nothing
  strSQL = "UPDATE PR0800 SET AD07CODPROCESO = "
  strSQL = strSQL & grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row))
  strSQL = strSQL & " WHERE AD07CODPROCESO = "
  strSQL = strSQL & grdDBGrid1(2).Columns(4).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row))
  strSQL = strSQL & " AND AD01CODASISTENCI = "
  strSQL = strSQL & grdDBGrid1(2).Columns(3).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row))
  objApp.rdoConnect.Execute strSQL, 64

  strSQL = "UPDATE AD2100 SET AD07CODPROCESO = "
  strSQL = strSQL & grdDBGrid1(3).Columns(4).CellValue(grdDBGrid1(3).SelBookmarks(grdDBGrid1(3).Row))
  strSQL = strSQL & " WHERE AD07CODPROCESO = "
  strSQL = strSQL & grdDBGrid1(2).Columns(4).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row))
  strSQL = strSQL & " AND AD01CODASISTENCI = " & grdDBGrid1(2).Columns(3).CellValue(grdDBGrid1(2).SelBookmarks(grdDBGrid1(2).Row))
  objApp.rdoConnect.Execute strSQL, 64
  
  rs08.Delete
  rs08.Close
  Set rs08 = Nothing
  
  Call objWinInfo.FormChangeActive(fraFrame1(2), True, True)
  objWinInfo.DataRefresh
 
 End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

