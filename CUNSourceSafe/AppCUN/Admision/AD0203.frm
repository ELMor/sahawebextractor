VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmDepartamentos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Departamentos"
   ClientHeight    =   8340
   ClientLeft      =   45
   ClientTop       =   645
   ClientWidth     =   11880
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "AD0203.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11880
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin Crystal.CrystalReport crReport1 
      Left            =   5280
      Top             =   7920
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdListado 
      Caption         =   "Listado"
      Height          =   375
      Left            =   3480
      TabIndex        =   57
      Top             =   7920
      Width           =   1455
   End
   Begin VB.CommandButton cmdAsociar 
      Caption         =   "Asociar a mas departamentos"
      Height          =   375
      Left            =   480
      TabIndex        =   56
      Top             =   7920
      Width           =   2535
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Departamentos"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3705
      Index           =   0
      Left            =   30
      TabIndex        =   0
      Top             =   480
      Width           =   11760
      Begin TabDlg.SSTab tabTab1 
         Height          =   3285
         Index           =   0
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   5794
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AD0203.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(10)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(8)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(13)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(15)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(16)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "cboDBCombo1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "chkCheck1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "chkCheck1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(6)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(9)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(13)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(14)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).ControlCount=   22
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AD0203.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   14
            Left            =   -69480
            TabIndex        =   54
            TabStop         =   0   'False
            Tag             =   "Descripci�n Almac�n|Descripci�n "
            Top             =   2760
            Width           =   4725
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR04CODALMACEN"
            Height          =   330
            Index           =   13
            Left            =   -70320
            TabIndex        =   10
            Tag             =   "C�digo del Almac�n"
            Top             =   2760
            Width           =   780
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02FAXDIRECTO"
            Height          =   330
            Index           =   9
            Left            =   -68040
            TabIndex        =   50
            Tag             =   "Fax Directo"
            Top             =   1680
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02TFNODIRECTO"
            Height          =   330
            Index           =   7
            Left            =   -68040
            TabIndex        =   48
            Tag             =   "Tel�fono Directo"
            Top             =   1080
            Width           =   2700
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02DESCOMPLETA"
            Height          =   690
            Index           =   6
            Left            =   -74640
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   46
            Tag             =   "Descripci�n Larga|Descripci�n"
            Top             =   1080
            Width           =   5700
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   2
            Left            =   -73080
            TabIndex        =   5
            Tag             =   "Descripci�n Tipo Departamento|Descripci�n"
            Top             =   2160
            Width           =   4725
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -73620
            TabIndex        =   3
            Tag             =   "Descripci�n Departamento|Descripci�n"
            Top             =   420
            Width           =   4725
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   -74640
            TabIndex        =   2
            Tag             =   "C�digo Departamento|C�digo Departamento"
            Top             =   420
            Width           =   930
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Responsable de Procesos"
            DataField       =   "AD02INDRESPONPROC"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Index           =   0
            Left            =   -67680
            TabIndex        =   6
            Tag             =   "Responsable de Procesos|Responsable Procesos"
            Top             =   120
            Width           =   2655
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Responsable de Camas"
            DataField       =   "AD02INDCAMA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -67680
            TabIndex        =   7
            Tag             =   "Responsable de Camas|Responsable Camas"
            Top             =   480
            Width           =   2595
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3045
            Index           =   0
            Left            =   90
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   90
            Width           =   10875
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19182
            _ExtentY        =   5371
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD02FECINICIO"
            Height          =   330
            Index           =   0
            Left            =   -74640
            TabIndex        =   8
            Tag             =   "Fecha Inicio |Fecha Inicio"
            Top             =   2760
            Width           =   1995
            _Version        =   65537
            _ExtentX        =   3519
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD02FECFIN"
            Height          =   330
            Index           =   1
            Left            =   -72510
            TabIndex        =   9
            Tag             =   "Fecha Fin |Fecha Fin"
            Top             =   2760
            Width           =   1995
            _Version        =   65537
            _ExtentX        =   3519
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD32CODTIPODPTO"
            Height          =   330
            Index           =   0
            Left            =   -74670
            TabIndex        =   4
            Tag             =   "Tipo Departamento|Tipo Departamento"
            Top             =   2160
            Width           =   1350
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).BackColor=   16777215
            stylesets(0).Picture=   "AD0203.frx":0044
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD0203.frx":0060
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   2381
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 0"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Almac�n (del Servicio)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   -70320
            TabIndex        =   55
            Top             =   2520
            Width           =   1920
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fax Directo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   -68040
            TabIndex        =   51
            Top             =   1440
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tel�fono Directo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   -68040
            TabIndex        =   49
            Top             =   840
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Larga"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   -74640
            TabIndex        =   47
            Top             =   840
            Width           =   1560
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   -72510
            TabIndex        =   32
            Top             =   2520
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   165
            Index           =   5
            Left            =   -74640
            TabIndex        =   31
            Top             =   2520
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   -74640
            TabIndex        =   30
            Top             =   180
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   -73620
            TabIndex        =   29
            Top             =   180
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   -74640
            TabIndex        =   28
            Top             =   1920
            Width           =   1635
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   25
      Top             =   8055
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabTab1 
      Height          =   3495
      HelpContextID   =   90001
      Index           =   3
      Left            =   120
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   4320
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   6165
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Personal"
      TabPicture(0)   =   "AD0203.frx":007C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(1)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Puestos"
      TabPicture(1)   =   "AD0203.frx":0098
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(2)"
      Tab(1).ControlCount=   1
      Begin VB.Frame fraFrame1 
         Caption         =   "Departamentos / Puestos"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2955
         Index           =   2
         Left            =   -74880
         TabIndex        =   19
         Top             =   360
         Width           =   11040
         Begin TabDlg.SSTab tabTab1 
            Height          =   2415
            Index           =   2
            Left            =   120
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   360
            Width           =   10710
            _ExtentX        =   18891
            _ExtentY        =   4260
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "AD0203.frx":00B4
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(11)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(12)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(14)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "dtcDateCombo1(5)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "dtcDateCombo1(4)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtText1(10)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(11)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(12)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "chkCheck1(3)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "chkCheck1(2)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).ControlCount=   10
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "AD0203.frx":00D0
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(2)"
            Tab(1).ControlCount=   1
            Begin VB.CheckBox chkCheck1 
               Caption         =   "�Pedir Actuaciones?"
               DataField       =   "AD33INDPEDACT"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   225
               Index           =   2
               Left            =   7080
               TabIndex        =   53
               Tag             =   "Responsable de Procesos|Responsable Procesos"
               Top             =   1200
               Width           =   2655
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "�Firmar Informes?"
               DataField       =   "AD33INDFIRMA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   3
               Left            =   7080
               TabIndex        =   52
               Tag             =   "Responsable de Camas|Responsable Camas"
               Top             =   1560
               Width           =   2595
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   12
               Left            =   1380
               TabIndex        =   22
               Tag             =   "Descripci�n Puesto|Descripci�n Puesto"
               Top             =   600
               Width           =   6015
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD31CODPUESTO"
               Height          =   330
               Index           =   11
               Left            =   300
               TabIndex        =   21
               Tag             =   "C�digo Puesto|Puesto"
               Top             =   600
               Width           =   915
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   10
               Left            =   8070
               TabIndex        =   41
               Tag             =   "C�digo Departamento|C�digo"
               Top             =   600
               Visible         =   0   'False
               Width           =   915
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   1815
               Index           =   2
               Left            =   -74850
               TabIndex        =   42
               TabStop         =   0   'False
               Top             =   150
               Width           =   10065
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17754
               _ExtentY        =   3201
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "AD33FECINICIO"
               Height          =   330
               Index           =   4
               Left            =   300
               TabIndex        =   23
               Tag             =   "Fecha Inicio |Fecha Inicio"
               Top             =   1440
               Width           =   1995
               _Version        =   65537
               _ExtentX        =   3519
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1000/1/1"
               MaxDate         =   "3000/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "AD33FECFIN"
               Height          =   330
               Index           =   5
               Left            =   3600
               TabIndex        =   24
               Tag             =   "Fecha Fin |Fecha Fin"
               Top             =   1440
               Width           =   1995
               _Version        =   65537
               _ExtentX        =   3519
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1000/1/1"
               MaxDate         =   "3000/12/31"
               Format          =   "MM/DD/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Puesto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   14
               Left            =   270
               TabIndex        =   45
               Tag             =   "Puesto|C�digo Puesto"
               Top             =   390
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Inicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   270
               TabIndex        =   44
               Top             =   1200
               Width           =   1065
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   3600
               TabIndex        =   43
               Top             =   1200
               Width           =   855
            End
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Departamentos / Personal"
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3015
         Index           =   1
         Left            =   120
         TabIndex        =   12
         Top             =   360
         Width           =   11160
         Begin TabDlg.SSTab tabTab1 
            Height          =   2505
            Index           =   1
            Left            =   120
            TabIndex        =   33
            TabStop         =   0   'False
            Top             =   360
            Width           =   10770
            _ExtentX        =   18997
            _ExtentY        =   4419
            _Version        =   327681
            TabOrientation  =   3
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   529
            WordWrap        =   0   'False
            ShowFocusRect   =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Detalle"
            TabPicture(0)   =   "AD0203.frx":00EC
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(9)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(2)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(4)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(3)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(7)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "cboDBCombo1(1)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "dtcDateCombo1(3)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "dtcDateCombo1(2)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(4)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(3)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(8)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(5)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).ControlCount=   12
            TabCaption(1)   =   "Tabla"
            TabPicture(1)   =   "AD0203.frx":0108
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "grdDBGrid1(1)"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   5
               Left            =   1410
               TabIndex        =   14
               Tag             =   "Nombre Usuario|Nombre Usuario"
               Top             =   390
               Width           =   6705
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   8
               Left            =   1380
               TabIndex        =   16
               Tag             =   "Descripci�n Puesto|Descripci�n Puesto"
               Top             =   1140
               Width           =   6015
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   3
               Left            =   8400
               TabIndex        =   34
               Tag             =   "C�digo Departamento|C�digo"
               Top             =   390
               Visible         =   0   'False
               Width           =   915
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               Index           =   4
               Left            =   270
               TabIndex        =   13
               Tag             =   "C�digo Usuario|Usuario"
               Top             =   390
               Width           =   915
            End
            Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
               Height          =   2265
               Index           =   1
               Left            =   -74880
               TabIndex        =   35
               TabStop         =   0   'False
               Top             =   120
               Width           =   10185
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               BevelColorFrame =   0
               BevelColorHighlight=   16777215
               AllowUpdate     =   0   'False
               MultiLine       =   0   'False
               AllowRowSizing  =   0   'False
               AllowGroupSizing=   0   'False
               AllowGroupMoving=   0   'False
               AllowColumnMoving=   2
               AllowGroupSwapping=   0   'False
               AllowGroupShrinking=   0   'False
               AllowDragDrop   =   0   'False
               SelectTypeCol   =   0
               SelectTypeRow   =   1
               MaxSelectedRows =   0
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               UseDefaults     =   0   'False
               _ExtentX        =   17965
               _ExtentY        =   3995
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "AD03FECINICIO"
               Height          =   330
               Index           =   2
               Left            =   210
               TabIndex        =   17
               Tag             =   "Fecha Inicio |Fecha Inicio"
               Top             =   1860
               Width           =   1995
               _Version        =   65537
               _ExtentX        =   3519
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1000/1/1"
               MaxDate         =   "3000/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "AD03FECFIN"
               Height          =   330
               Index           =   3
               Left            =   4440
               TabIndex        =   18
               Tag             =   "Fecha Fin |Fecha Fin"
               Top             =   1860
               Width           =   1995
               _Version        =   65537
               _ExtentX        =   3519
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1000/1/1"
               MaxDate         =   "3000/12/31"
               Format          =   "MM/DD/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
               DataField       =   "AD31CODPUESTO"
               Height          =   330
               Index           =   1
               Left            =   240
               TabIndex        =   15
               Tag             =   "C�digo Puesto|C�digo Puesto"
               Top             =   1140
               Width           =   1005
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               stylesets.count =   2
               stylesets(0).Name=   "Activo"
               stylesets(0).BackColor=   16777215
               stylesets(0).Picture=   "AD0203.frx":0124
               stylesets(1).Name=   "Inactivo"
               stylesets(1).BackColor=   255
               stylesets(1).Picture=   "AD0203.frx":0140
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Nombre"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1773
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 0"
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Fin"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   4410
               TabIndex        =   40
               Top             =   1620
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Inicio"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   210
               TabIndex        =   39
               Top             =   1620
               Width           =   1065
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Puesto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   210
               TabIndex        =   38
               Tag             =   "Puesto|C�digo Puesto"
               Top             =   900
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Personal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   270
               TabIndex        =   37
               Top             =   180
               Width           =   750
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre y Apellidos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   1410
               TabIndex        =   36
               Top             =   150
               Width           =   1635
            End
         End
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDepartamentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdAsociar_Click()
Dim intI As Integer
Dim strSql1 As Variant
Dim strSQL2 As String
Dim strMsg As String
Dim rsPuesto As rdoResultset
Dim rdoConsulta As rdoQuery
Err = 0

If txtText1(4).Text = "" Then
  MsgBox "El campo Personal es obligatorio", vbExclamation, "Departamentos / Personal"
  txtText1(4).SetFocus
  Exit Sub
End If
If cboDBCombo1(1).Text = "" Then
  MsgBox "El campo Puesto es obligatorio", vbExclamation, "Departamentos / Personal"
  cboDBCombo1(1).SetFocus
  Exit Sub
End If
If Not IsDate(dtcDateCombo1(2).Date) Then
  MsgBox "El campo Fecha Inicio es obligatorio", vbExclamation, "Departamentos / Personal"
  dtcDateCombo1(2).SetFocus
  Exit Sub
End If

objApp.rdoConnect.BeginTrans
For intI = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'strSQL2 = "SELECT COUNT(*) FROM AD3300 WHERE AD02CODDPTO=" & _
'grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI)) & _
'"AND AD31CODPUESTO=" & cboDBCombo1(1).Text
'Set rsPuesto = objApp.rdoConnect.OpenResultset(strSQL2)
strSQL2 = "SELECT COUNT(*) FROM AD3300 WHERE AD02CODDPTO=?" & _
" AND AD31CODPUESTO=?"

    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL2)
    rdoConsulta(0) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI))
    rdoConsulta(1) = cboDBCombo1(1).Text
    Set rsPuesto = rdoConsulta.OpenResultset(strSQL2)

If rsPuesto.rdoColumns(0) = 0 Then
    strMsg = "El departamento " & grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI)) & _
    " no tiene asociado el puesto" & cboDBCombo1(1).Text
    MsgBox strMsg
Else
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'    strSQL2 = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO=" & _
'    grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI)) & _
'    " AND SG02COD=" & _
'    "'" & txtText1(4).Text & "'"
'    Set rsPuesto = objApp.rdoConnect.OpenResultset(strSQL2)
    strSQL2 = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO=?" & _
    " AND SG02COD=?"
    
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL2)
    rdoConsulta(0) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI))
    rdoConsulta(1) = txtText1(4).Text
    Set rsPuesto = rdoConsulta.OpenResultset(strSQL2)
    
    If rsPuesto.rdoColumns(0) <> 0 Then
        strMsg = "El departamento " & grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI)) & _
        "ya tiene asociado el usuario" & txtText1(4).Text
        MsgBox strMsg
    Else
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'        strSql1 = "INSERT INTO AD0300 (AD02CODDPTO,SG02COD,AD31CODPUESTO,AD03FECINICIO) VALUES (" & _
'        grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI)) & _
'        "," & "'" & txtText1(4).Text & "'" & "," & cboDBCombo1(1).Text & "," & _
'        "TO_DATE('" & dtcDateCombo1(2).Date & "','DD/MM/YY')" & ")"
'        objApp.rdoConnect.Execute strSql1, 64
        strSql1 = "INSERT INTO AD0300 (AD02CODDPTO,SG02COD,AD31CODPUESTO,AD03FECINICIO) VALUES (?,?,?," & _
        "TO_DATE(?,'DD/MM/YYYY')" & ")"
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSql1)
    rdoConsulta(0) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(intI))
    rdoConsulta(1) = txtText1(4).Text
    rdoConsulta(2) = cboDBCombo1(1).Text
    rdoConsulta(3) = Format(dtcDateCombo1(2).Date, "DD/MM/YYYY")
    rdoConsulta.Execute
    End If
End If
Next intI
If Err > 0 Then
    objApp.rdoConnect.RollbackTrans
    MsgBox "No se han asociado, Ha habido almenos un departamento con error"
Else: objApp.rdoConnect.CommitTrans
End If
End Sub

Private Sub cmdListado_Click()
Dim SQL$
crReport1.ReportFileName = objApp.strReportsPath & "SG0024.rpt"
SQL = "{AD0200.AD02CODDPTO} = " & txtText1(0).Text
 With crReport1
        .PrinterCopies = 1
        .Destination = crptToWindow
        .SelectionFormula = "(" & SQL & ")"
'        .Destination = crptToPrinter
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Me.MousePointer = vbHourglass
        .Action = 1
        Me.MousePointer = vbDefault
    End With
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo1 As New clsCWForm
  Dim objDetailInfo2 As New clsCWForm
  Dim strKey As String
  
'  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  With objWinInfo.objDoc
    .cwPRJ = "ADMISION"
    .cwMOD = "M�dulo Maestro Detalle"
    .cwAUT = "A.R."
    .cwDAT = "13-03-97"
    .cwDES = "Mantenimiento Departamentos-Personal Departamentos-Puestos"
    .cwUPD = "13-03-97  - A.R. - xxxxxxxx"
    .cwEVT = "Descripci�n del evento xxxxx"
  End With

  With objMasterInfo
    .strName = "Departamentos"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "AD0200"
    .intCursorSize = -1
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    .blnHasMaint = True
 '    .blnMasiveOn = True
    If objPipe.PipeExist("Departamento") Then
      If objPipe.PipeGet("Departamento") <> "" Then
        .strInitialWhere = "AD02CODDPTO=" & objPipe.PipeGet("Departamento")
      End If
    End If
'    Call .objPrinter.Add("AD002021", "Listado de Departamentos")
    
    .intAllowance = cwAllowAdd + cwAllowModify
    
    Call .FormCreateFilterWhere("AD0200", "Departamentos")
    Call .FormAddFilterWhere("AD0200", "AD02DESDPTO", "Departamento", cwString)
    Call .FormAddFilterWhere("AD0200", "AD32DESTIPODPTO", "Tipo Departamento", cwString)
    Call .FormAddFilterWhere("AD0200", "AD02FECINICIO", "Fecha Inicio", cwDate)
    Call .FormAddFilterWhere("AD0200", "AD02FECFIN", "Fecha Fin", cwDate)
    Call .FormAddFilterWhere("AD0200", "AD02DESCOMPLETA", "Descripci�n Completa", cwString)
    Call .FormAddFilterWhere("AD0200", "AD02TFNODIRECTO", "Tel�fono Directo", cwNumeric)
    Call .FormAddFilterWhere("AD0200", "AD02FAXDIRECTO", "Fax Directo", cwNumeric)
    Call .FormAddFilterWhere("AD0200", "FR04CODALMACEN", "Almac�n", cwNumeric)
    
    Call .FormAddFilterOrder("AD0200", "AD02DESDPTO", "Descripci�n")
  End With
  
  With objDetailInfo1
    .strName = "Personal"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "AD0300"
    .intAllowance = cwAllowAdd + cwAllowModify
    
    Call .FormAddOrderField("SG02COD", cwAscending)
    
    Call .FormAddFilterOrder("AD0300", "SG02COD", "Usuario")
    Call .FormAddRelation("AD02CODDPTO", txtText1(0))
    
    Call .FormCreateFilterWhere("AD0300", "Departamentos Personal")
    Call .FormAddFilterWhere("AD0300", "SG02COD", "Usuario", cwString)
  End With
  
  With objDetailInfo2
    .strName = "Puesto"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(2)
    Set .grdGrid = grdDBGrid1(2)
    .strTable = "AD3300"
    .intAllowance = cwAllowAdd + cwAllowModify
    .blnHasMaint = True
    
    Call .FormAddOrderField("AD31CODPUESTO", cwAscending)
    
    Call .FormAddFilterOrder("AD3300", "AD31CODPUESTO", "Puesto")
    Call .FormAddRelation("AD02CODDPTO", txtText1(0))
    
    Call .FormCreateFilterWhere("AD3300", "Departamentos Puestos")
    Call .FormAddFilterWhere("AD3300", "AD31CODPUESTO", "Puesto", cwString)
   End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo1, cwFormDetail)
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
  
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(cboDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    
    .CtrlGetInfo(cboDBCombo1(1)).blnInFind = True
    
    .CtrlGetInfo(txtText1(11)).blnForeign = True
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    .CtrlGetInfo(txtText1(13)).blnForeign = True
    
    .CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT AD32CODTIPODPTO,AD32DESTIPODPTO,AD32FECFIN FROM AD3200 ORDER BY AD32CODTIPODPTO"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(0)), "AD32CODTIPODPTO", "SELECT AD32CODTIPODPTO,AD32DESTIPODPTO FROM AD3200 WHERE AD32CODTIPODPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(0)), txtText1(2), "AD32DESTIPODPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "FR04CODALMACEN", "SELECT FR04CODALMACEN,FR04DESALMACEN FROM FR0400 WHERE FR04CODALMACEN = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(14), "FR04DESALMACEN")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(1)), "SG02COD", "SELECT SG02COD,SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 AS DOCTOR FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(1)), txtText1(1), "DOCTOR")
    
    If objPipe.PipeExist("Categoria") Then
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "SG02COD", _
        "SELECT SG02COD,SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 AS PERSONAL FROM SG0200 " & _
        "WHERE AD30CODCATEGORIA=1 AND SG02COD = ?")
    Else
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "SG02COD", _
        "SELECT SG02COD,SG02NOM || ' ' || SG02APE1 || ' ' || SG02APE2 AS PERSONAL FROM SG0200 " & _
        "WHERE SG02COD = ?")
    End If
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "PERSONAL")

    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(1)), "AD31CODPUESTO", "SELECT AD31CODPUESTO,AD31DESPUESTO FROM AD3100 WHERE AD31CODPUESTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(1)), txtText1(8), "AD31DESPUESTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "AD31CODPUESTO", "SELECT AD31CODPUESTO,AD31DESPUESTO FROM AD3100 WHERE AD31CODPUESTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(12), "AD31DESPUESTO")
    
    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
'Call grdDBGrid1_DblClick(0)
'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If strFormName = "Departamentos" Then
    objWinInfo.CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD3300.AD31CODPUESTO,AD3100.AD31DESPUESTO FROM AD3100,AD3300 WHERE AD3100.AD31FECFIN IS NULL AND AD3300.AD31CODPUESTO=AD3100.AD31CODPUESTO AND AD3300.AD02CODDPTO = " & txtText1(0)
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
    If dtcDateCombo1(1) <> "" Then
      objWinInfo.cllWinForms(2).intAllowance = cwAllowReadOnly
      objWinInfo.cllWinForms(3).intAllowance = cwAllowReadOnly
    Else
      objWinInfo.cllWinForms(2).intAllowance = cwAllowAdd + cwAllowModify
      objWinInfo.cllWinForms(3).intAllowance = cwAllowAdd + cwAllowModify
    End If
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Empleados" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim strNombre As String
  Dim objField As clsCWFieldSearch
  Set objSearch = New clsCWSearch
  
  Select Case strCtrl
    Case "txtText1(13)"
      With objSearch
        .strTable = "FR0400"
        .strOrder = "ORDER BY FR04CODALMACEN ASC"
        .strWhere = "WHERE ((FR04FECFINVIG IS NULL) OR (FR04FECFINVIG > (SELECT SYSDATE FROM DUAL)))"
        
        Set objField = .AddField("FR04CODALMACEN")
        objField.strSmallDesc = "C�digo del Almac�n"
        
        Set objField = .AddField("FR04DESALMACEN")
        objField.strSmallDesc = "Almac�n"
        
        If .Search Then
          Call objWinInfo.CtrlSet(txtText1(13), .cllValues("FR04CODALMACEN"))
        End If
      End With
      Set objSearch = Nothing
    Case "txtText1(4)"
      With objSearch
        .strTable = "SG0200"
        .strOrder = "ORDER BY SG02COD ASC"
        .strWhere = "WHERE AD30CODCATEGORIA=1 AND SG02FECDES IS NULL"
        
        Set objField = .AddField("SG02COD")
        objField.strSmallDesc = "C�digo del Doctor"
        
        Set objField = .AddField("SG02NOM")
        objField.strSmallDesc = "Nombre Doctor"
        
        Set objField = .AddField("SG02APE1")
        objField.strSmallDesc = "Primer Apellido Doctor"
        
        Set objField = .AddField("SG02APE2")
        objField.strSmallDesc = "Segundo Apellido Doctor"
        
        If .Search Then
          Call objWinInfo.CtrlSet(txtText1(4), .cllValues("SG02COD"))
        End If
      End With
      Set objSearch = Nothing
    Case "txtText1(11)"
      With objSearch
        .strTable = "AD3100"
        .strOrder = "ORDER BY AD31CODPUESTO ASC"
        .strWhere = "WHERE AD31FECFIN IS NULL"
        
        Set objField = .AddField("AD31CODPUESTO")
        objField.strSmallDesc = "C�digo del Puesto"
        
        Set objField = .AddField("AD31DESPUESTO")
        objField.strSmallDesc = "Desripci�n del Puesto"
        
        If .Search Then
          Call objWinInfo.CtrlSet(txtText1(11), .cllValues("AD31CODPUESTO"))
        End If
      End With
      Set objSearch = Nothing
  End Select
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Select Case strFormName
    Case "Departamentos"
      Call objSecurity.LaunchProcess("AD0202")
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(0)))
    Case "Puesto"
      Call objSecurity.LaunchProcess("AD0207")
  End Select
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  If strFormName = "Personal" Then
    Call objWinInfo.CtrlSet(txtText1(3), txtText1(0))
  End If
  If strFormName = "Puesto" Then
    Call objWinInfo.CtrlSet(txtText1(10), txtText1(0))
  End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim vntA As Variant
  
  If strFormName = "Departamentos" Then
    If cboDBCombo1(0).Columns(2).Value <> "" Then
      Call objError.SetError(cwCodeMsg, "Este Tipo de Departamento est� dado de baja. Elija otro", vntA)
      vntA = objError.Raise
      blnCancel = True
    End If
    If dtcDateCombo1(1).Date <> "" Then
      If CDate(Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(1).Date, "DD/MM/YYYY")) Then
        Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
        vntA = objError.Raise
        blnCancel = True
      End If
    End If
  End If
  
  If strFormName = "Personal" Then
    If cboDBCombo1(1).Columns(2).Value <> "" Then
      Call objError.SetError(cwCodeMsg, "Este Puesto est� dado de baja. Elija otro", vntA)
      vntA = objError.Raise
      blnCancel = True
    End If
    If dtcDateCombo1(3).Date <> "" Then
      If CDate(Format(dtcDateCombo1(2).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(3).Date, "DD/MM/YYYY")) Then
        Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
        vntA = objError.Raise
        blnCancel = True
      End If
    End If
    If dtcDateCombo1(2).Date <> "" Then
        If CDate(Format(dtcDateCombo1(2).Date, "DD/MM/YYYY")) < CDate(Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")) Then
          Call objError.SetError(cwCodeMsg, "Fecha Inicio no puede ser inferior a Fecha Inicio de Departamento.", vntA)
          vntA = objError.Raise
          blnCancel = True
        End If
    Else
        Call objError.SetError(cwCodeMsg, "Fecha Inicio es obligatoria", vntA)
          vntA = objError.Raise
          blnCancel = True
    End If
  End If
  
  If strFormName = "Puesto" Then
    If dtcDateCombo1(5).Date <> "" Then
      If CDate(Format(dtcDateCombo1(4).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(5).Date, "DD/MM/YYYY")) Then
        Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
        vntA = objError.Raise
        blnCancel = True
      End If
    End If
    If CDate(Format(dtcDateCombo1(4).Date, "DD/MM/YYYY")) < CDate(Format(dtcDateCombo1(0).Date, "DD/MM/YYYY")) Then
      Call objError.SetError(cwCodeMsg, "Fecha Inicio no puede ser inferior a Fecha Inicio de Departamento.", vntA)
      vntA = objError.Raise
      blnCancel = True
    End If
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 3 Then
    If objWinInfo.cllWinForms(2).blnEnabled Then
      Call objWinInfo_cwPostDefault("Personal")
    End If
    If objWinInfo.cllWinForms(3).blnEnabled Then
      Call objWinInfo_cwPostDefault("Puesto")
    End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
'   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  If intIndex = 3 Then
    Select Case tabTab1(3).Tab
      Case 0
        intIndex = 1
        objWinInfo.CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD3300.AD31CODPUESTO,AD3100.AD31DESPUESTO FROM AD3100,AD3300 WHERE AD3100.AD31FECFIN IS NULL AND AD3300.AD31CODPUESTO=AD3100.AD31CODPUESTO AND AD3300.AD02CODDPTO = " & txtText1(0)
        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
      Case 1
        intIndex = 2
    End Select
  End If
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Dim vntA As Variant
  
  Call objWinInfo.CtrlLostFocus
  Select Case intIndex
    Case 4
      If txtText1(5) = "" And txtText1(intIndex) <> "" Then
        Call objError.SetError(cwCodeMsg, "Usuario no existente.", vntA)
        vntA = objError.Raise
        Call objWinInfo.CtrlSet(txtText1(intIndex), "")
      End If
    Case 11
      If txtText1(12) = "" And txtText1(intIndex) <> "" Then
        Call objError.SetError(cwCodeMsg, "Puesto no existente.", vntA)
        vntA = objError.Raise
        Call objWinInfo.CtrlSet(txtText1(intIndex), "")
      End If
  End Select
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
