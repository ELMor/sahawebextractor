VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmSalidasMasivas 
   Caption         =   "Salidas Masivas de Archivo para las citas previstas"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9435
   LinkTopic       =   "Form1"
   ScaleHeight     =   12609.02
   ScaleMode       =   0  'Usuario
   ScaleWidth      =   10857.09
   StartUpPosition =   3  'Windows Default
   Begin Crystal.CrystalReport crtrpt 
      Left            =   6720
      Top             =   8040
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton Command3 
      Caption         =   "LISTADOS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   4680
      TabIndex        =   4
      Top             =   7920
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "SALIR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7560
      TabIndex        =   3
      Top             =   7920
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "PRESTAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2400
      TabIndex        =   2
      Top             =   7920
      Width           =   1695
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "ANULAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   7920
      Width           =   1695
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Height          =   7575
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9375
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   5
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   1561
      Columns(0).Caption=   "ANULAR"
      Columns(0).Name =   "ANULAR"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).Style=   2
      Columns(1).Width=   3200
      Columns(1).Caption=   "HISTORIA"
      Columns(1).Name =   "HISTORIA"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1164
      Columns(2).Caption=   "TIPOL"
      Columns(2).Name =   "TIPOL"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   5530
      Columns(3).Caption=   "PACIENTE"
      Columns(3).Name =   "PACIENTE"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   4471
      Columns(4).Caption=   "DEPARTAMENTO"
      Columns(4).Name =   "DEPARTAMENTO"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   16536
      _ExtentY        =   13361
      _StockProps     =   79
   End
End
Attribute VB_Name = "frmSalidasMasivas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intNumeroLineas As Integer

Private Sub cmdAnular_Click()
Dim sql As String
Dim qry As rdoQuery
Dim I As Integer

sql = "DELETE FROM AR0500 WHERE CI22NUMHISTORIA = ?"
sql = sql & " AND  AR00CODTIPOLOGIA = ? AND AR01CODMOTIVOSOL ='C' "
sql = sql & " AND  AR05FECFINSOLICITUD IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
On Error Resume Next
SSDBGrid1.MoveFirst
For I = 1 To intNumeroLineas
    If SSDBGrid1.Columns("ANULAR").Value = -1 Then
        qry(0) = SSDBGrid1.Columns("HISTORIA").Value
        qry(1) = SSDBGrid1.Columns("TIPOL").Value
        qry.Execute
        If err > 0 Then
            MsgBox "LA HISTORIA " & SSDBGrid1.Columns("HISTORIA").Value & "NO SE HA PODIDO BORRAR", vbOKOnly
            err = 0
        End If
    End If
    SSDBGrid1.MoveNext
Next I
pInicializarGrid
End Sub

Private Sub Command1_Click()
Dim intI As Integer
Dim sql As String
Dim qry As rdoQuery
Dim strFecha As String
intI = MsgBox("ESTA SEGURO DE QUE HA ANULADO TODAS LAS HISTORIAS NECESARIAS ", vbYesNo)
If intI = vbNo Then
    Exit Sub
End If
intI = MsgBox("DESEA SEGUIR EL PROCESO DE PRESTAMO?", vbYesNo)
If intI = vbNo Then
    Exit Sub
End If
strFecha = strFechaHora_Sistema
objApp.BeginTrans
On Error Resume Next

sql = "UPDATE AR0400 H"
sql = sql & " Set h.AR04FECFIN = to_date(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " WHERE H.AR04FECFIN IS NULL AND"
sql = sql & " EXISTS(SELECT 1 FROM AR0500 P"
sql = sql & " WHERE P.CI22NUMHISTORIA=H.CI22NUMHISTORIA AND"
sql = sql & " P.AR00CODTIPOLOGIA=H.AR00CODTIPOLOGIA AND"
sql = sql & " P.AR01CODMOTIVOSOL='C' AND"
sql = sql & " P.AR05FECFINSOLICITUD IS NULL)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(strFecha, "dd/mm/yyyy hh:mm")
qry.Execute
If err > 0 Then
    MsgBox "Error en al cerrar el registro actual de todos los que se van a prestar", vbCritical
    objApp.RollbackTrans
    qry.Close
    Exit Sub
End If
qry.Close
sql = "INSERT INTO AR0400"
sql = sql & " (CI22NUMHISTORIA,AR00CODTIPOLOGIA,AR04FECINICIO, "
sql = sql & " AR06CODESTADO,AR02CODPTOARCHIVO)"
sql = sql & " SELECT CI22NUMHISTORIA,AR00CODTIPOLOGIA, to_date(?,'DD/MM/YYYY HH24:MI'),'P','01'"
sql = sql & " From AR0500"
sql = sql & " WHERE AR02CODPTOARCHIVO='01' AND AR01CODMOTIVOSOL='C' "
sql = sql & " AND AR05FECFINSOLICITUD IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(strFecha, "dd/mm/yyyy hh:mm")
qry.Execute
If err > 0 Then
    MsgBox "Error creando el registro actual", vbCritical
    objApp.RollbackTrans
    qry.Close
    Exit Sub
End If
qry.Close
sql = "UPDATE AR0500 SET AR05FECFINSOLICITUD=to_date(?,'DD/MM/YYYY HH24:MI'), "
sql = sql & " AR06CODESTADO='L'"
sql = sql & " WHERE AR02CODPTOARCHIVO='01' "
sql = sql & " AND AR01CODMOTIVOSOL='C' AND AR05FECFINSOLICITUD IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Format(strFecha, "dd/mm/yyyy hh:mm")
qry.Execute
If err > 0 Then
    MsgBox "Error cerrando los pedidos de citas", vbCritical
    objApp.RollbackTrans
    qry.Close
    Exit Sub
End If
qry.Close
objApp.CommitTrans
End Sub

Private Sub Command2_Click()
Unload Me
End Sub

Private Sub Command3_Click()

crtrpt.ReportFileName = objApp.strReportsPath & "citas1.rpt"
    With crtrpt
        .PrinterCopies = 1
        .Destination = crptToWindow
        .Connect = objApp.rdoConnect.Connect
'        .DiscardSavedData = True
        Me.MousePointer = vbHourglass
        .Action = 1
        Me.MousePointer = vbDefault
    End With
crtrpt.ReportFileName = objApp.strReportsPath & "citas2.rpt"
    With crtrpt
        .PrinterCopies = 1
        .Destination = crptToWindow
        .Connect = objApp.rdoConnect.Connect
'        .DiscardSavedData = True
        Me.MousePointer = vbHourglass
        .Action = 1
        Me.MousePointer = vbDefault
    End With
End Sub

Private Sub Form_Load()
pInicializarGrid
End Sub
Private Sub pInicializarGrid()
Dim str As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Screen.MousePointer = vbHourglass
SSDBGrid1.RemoveAll

intNumeroLineas = 0
str = "SELECT P.CI22NUMHISTORIA,P.AR00CODTIPOLOGIA,C.CI22SEGAPEL"
str = str & "||' '||C.CI22PRIAPEL||', '||C.CI22NOMBRE PACIENTE, D.AD02DESDPTO "
str = str & " FROM AD0200 D, CI2200 C, AR0500 P WHERE"
str = str & " P.AR02CODPTOARCHIVO = '01'"
str = str & " AND P.AR01CODMOTIVOSOL='C' "
str = str & " AND P.AR05FECFINSOLICITUD IS NULL"
str = str & " AND C.CI22NUMHISTORIA = P.CI22NUMHISTORIA"
str = str & " AND D.AD02CODDPTO = P.AD02CODDPTO"
str = str & " ORDER BY CI22NUMHISTORIA"

Set rs = objApp.rdoConnect.OpenResultset(str)
Do While Not rs.EOF
       SSDBGrid1.AddItem 0 & Chr$(9) _
                    & rs!CI22NUMHISTORIA & Chr$(9) _
                    & rs!AR00CODTIPOLOGIA & Chr$(9) _
                    & rs!Paciente & Chr$(9) _
                    & rs!AD02DESDPTO
      
       intNumeroLineas = intNumeroLineas + 1
                    
       rs.MoveNext
Loop
Screen.MousePointer = vbDefault
End Sub
