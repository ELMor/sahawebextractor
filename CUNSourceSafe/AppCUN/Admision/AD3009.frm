VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmMantRespEcon 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Responsables - Tipos Econ�micos"
   ClientHeight    =   6060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   8025
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab tabRE 
      Height          =   3255
      Left            =   60
      TabIndex        =   16
      Top             =   2760
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   5741
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Tipos Econ�micos"
      TabPicture(0)   =   "AD3009.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "grdRespEcon"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Tipos Econ�micos Resp. Superior"
      TabPicture(1)   =   "AD3009.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdRespEconSup"
      Tab(1).ControlCount=   1
      Begin SSDataWidgets_B.SSDBGrid grdRespEcon 
         Height          =   2775
         Left            =   60
         TabIndex        =   17
         Top             =   360
         Width           =   7785
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   13732
         _ExtentY        =   4895
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdRespEconSup 
         Height          =   2775
         Left            =   -74940
         TabIndex        =   18
         Top             =   360
         Width           =   7785
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   13732
         _ExtentY        =   4895
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Responsable"
      ForeColor       =   &H00FF0000&
      Height          =   2595
      Left            =   60
      TabIndex        =   8
      Top             =   60
      Width           =   6795
      Begin VB.TextBox txtRESup 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   2820
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   960
         Width           =   3855
      End
      Begin VB.TextBox txtRE 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   2820
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   360
         Width           =   3855
      End
      Begin VB.CommandButton cmdMantTE 
         Caption         =   "..."
         Height          =   375
         Left            =   6060
         TabIndex        =   13
         Top             =   1980
         Width           =   435
      End
      Begin VB.TextBox txtCodRESup 
         BackColor       =   &H00FFFF00&
         Height          =   315
         Left            =   1920
         TabIndex        =   1
         Top             =   960
         Width           =   915
      End
      Begin VB.TextBox txtCodRE 
         BackColor       =   &H00FFFF00&
         Height          =   315
         Left            =   1920
         TabIndex        =   0
         Top             =   360
         Width           =   915
      End
      Begin SSDataWidgets_B.SSDBCombo cboTipoEcon 
         Height          =   315
         Left            =   1920
         TabIndex        =   2
         Top             =   1500
         Width           =   3315
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5292
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5847
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboEntidad 
         Height          =   315
         Left            =   1920
         TabIndex        =   3
         Top             =   2040
         Width           =   4095
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7408
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   7223
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Entidad:"
         Height          =   195
         Index           =   1
         Left            =   900
         TabIndex        =   12
         Top             =   2100
         Width           =   975
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo Econ�mico:"
         Height          =   195
         Index           =   0
         Left            =   600
         TabIndex        =   11
         Top             =   1560
         Width           =   1275
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Resp. Econ�mico Sup.:"
         Height          =   195
         Index           =   2
         Left            =   180
         TabIndex        =   10
         Top             =   1020
         Width           =   1695
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Resp. Econ�mico:"
         Height          =   195
         Index           =   3
         Left            =   420
         TabIndex        =   9
         Top             =   420
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmdNuevo 
      Caption         =   "&Nuevo"
      Height          =   375
      Left            =   6960
      TabIndex        =   5
      Top             =   180
      Width           =   915
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   6960
      TabIndex        =   7
      Top             =   2280
      Width           =   915
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "&Guardar"
      Height          =   375
      Left            =   6960
      TabIndex        =   4
      Top             =   720
      Width           =   915
   End
   Begin VB.CommandButton cmdEliminar 
      Caption         =   "&Eliminar"
      Height          =   375
      Left            =   6960
      TabIndex        =   6
      Top             =   1260
      Width           =   915
   End
End
Attribute VB_Name = "frmMantRespEcon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCodRESel$, strCodREAux$

Private Sub cboEntidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cboTipoEcon_Click()
    cboEntidad.RemoveAll: cboEntidad.Text = ""
    Call pCargarEntidades
End Sub

Private Sub cboTipoEcon_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub cmdMantTE_Click()
    frmMantTiposEcon.Show vbModal
    Set frmMantTiposEcon = Nothing
    If cboTipoEcon.Text <> "" Then
        If cboEntidad.Text <> "" Then
            Call pReposicionarEntidad(cboEntidad.Columns(0).Text)
        Else
            Call pCargarEntidades
        End If
    End If
End Sub

Private Sub cmdEliminar_Click()
    Call pEliminar
End Sub

Private Sub cmdGuardar_Click()
    Call pGuardar
End Sub

Private Sub cmdNuevo_Click()
    Call pLimpiar
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call pFormatearControles
    Call pCargarTiposEcon
    
    If objPipe.PipeExist("AD_CodRespEco") Then
        strCodRESel = objPipe.PipeGet("AD_CodRespEco")
        Call objPipe.PipeRemove("AD_CodRespEco")
        txtCodRE.Text = strCodRESel
        txtRE.Text = fNombreRE(strCodRESel)
        Call pCargarRE(strCodRESel)
    End If
End Sub

Private Sub grdRespEcon_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub grdRespEcon_Click()
    grdRespEcon.SelBookmarks.RemoveAll
    grdRespEcon.SelBookmarks.Add (grdRespEcon.RowBookmark(grdRespEcon.Row))
    Call pCargarDatos
End Sub

Private Sub grdRespEcon_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    If grdRespEcon.SelBookmarks.Count = 0 Then grdRespEcon_Click
End Sub

Private Sub txtCodRE_GotFocus()
    txtCodRE.SelStart = 0
    txtCodRE.SelLength = Len(txtCodRE.Text)
End Sub

Private Sub txtCodRE_LostFocus()
    If txtCodRE.Text <> "" Then
        txtRE.Text = fNombreRE(txtCodRE.Text)
    Else
        txtRE.Text = ""
    End If
    If txtRE.Text <> "" Then
        If strCodREAux <> txtCodRE.Text Then
            strCodREAux = txtCodRESup.Text
            Call pCargarRE(txtCodRE.Text)
        End If
    End If
End Sub

Private Sub txtCodRE_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub txtCodRESup_GotFocus()
    txtCodRESup.SelStart = 0
    txtCodRESup.SelLength = Len(txtCodRESup.Text)
End Sub

Private Sub txtCodRESup_LostFocus()
    If txtCodRESup.Text <> "" Then
        txtRESup.Text = fNombreRE(txtCodRESup.Text)
    Else
        txtRESup.Text = ""
    End If
End Sub

Private Sub txtCodRESup_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub pCargarDatos()
    Dim i%
    txtCodRE.Text = Val(grdRespEcon.Columns("Resp. Econ�mico").CellText(grdRespEcon.SelBookmarks(0)))
    txtCodRE.BackColor = objApp.objUserColor.lngReadOnly
    txtCodRE.Locked = True
    strCodREAux = txtCodRE.Text
    txtRE.Text = grdRespEcon.Columns("RE").CellText(grdRespEcon.SelBookmarks(0))
    txtCodRESup.Text = Val(grdRespEcon.Columns("Resp. Econ�mico Sup.").CellText(grdRespEcon.SelBookmarks(0)))
    txtRESup.Text = grdRespEcon.Columns("RES").CellText(grdRespEcon.SelBookmarks(0))
    For i = 1 To cboTipoEcon.Rows
        If i = 1 Then cboTipoEcon.MoveFirst Else cboTipoEcon.MoveNext
        If cboTipoEcon.Columns(0).Text = grdRespEcon.Columns("Tipo E.").CellText(grdRespEcon.SelBookmarks(0)) Then
            cboTipoEcon.Text = cboTipoEcon.Columns(1).Text
            Call pCargarEntidades
            Exit For
        End If
    Next i
    For i = 1 To cboEntidad.Rows
        If i = 1 Then cboEntidad.MoveFirst Else cboEntidad.MoveNext
        If cboEntidad.Columns(0).Text = grdRespEcon.Columns("Entidad").CellText(grdRespEcon.SelBookmarks(0)) Then
            cboEntidad.Text = cboEntidad.Columns(1).Text
            Exit For
        End If
    Next i
    On Error Resume Next
    cboTipoEcon.SetFocus
    On Error GoTo 0
End Sub

Private Sub pCargarRE(strCodRE$)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    LockWindowUpdate Me.hWnd
    grdRespEcon.RemoveAll
    SQL = "SELECT CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA_REC) RE_REC,"
    SQL = SQL & " CI32CODTIPECON, CI13CODENTIDAD,"
    SQL = SQL & " CI21CODPERSONA, ADFN01(CI21CODPERSONA) RE"
    SQL = SQL & " FROM CI2900"
    SQL = SQL & " WHERE CI21CODPERSONA_REC = ?"
    SQL = SQL & " ORDER BY CI21CODPERSONA, CI32CODTIPECON, CI13CODENTIDAD"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodRE
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        grdRespEcon.AddItem rs!CI21CODPERSONA_REC & " - " & rs!RE_REC & Chr$(9) _
                        & rs!RE_REC & Chr$(9) _
                        & rs!CI32CODTIPECON & Chr$(9) _
                        & rs!CI13CODENTIDAD & Chr$(9) _
                        & rs!CI21CODPERSONA & " - " & rs!RE & Chr$(9) _
                        & rs!RE
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    grdRespEconSup.RemoveAll
    SQL = "SELECT CI21CODPERSONA_REC, ADFN01(CI21CODPERSONA_REC) RE_REC,"
    SQL = SQL & " CI32CODTIPECON, CI13CODENTIDAD,"
    SQL = SQL & " CI21CODPERSONA, ADFN01(CI21CODPERSONA) RE"
    SQL = SQL & " FROM CI2900"
    SQL = SQL & " WHERE CI21CODPERSONA IN "
    SQL = SQL & " (SELECT DISTINCT CI21CODPERSONA"
    SQL = SQL & " FROM CI2900"
    SQL = SQL & " WHERE CI21CODPERSONA_REC = ?)"
    SQL = SQL & " ORDER BY CI21CODPERSONA_REC"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodRE
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        grdRespEconSup.AddItem rs!CI21CODPERSONA_REC & " - " & rs!RE_REC & Chr$(9) _
                        & rs!RE_REC & Chr$(9) _
                        & rs!CI32CODTIPECON & Chr$(9) _
                        & rs!CI13CODENTIDAD & Chr$(9) _
                        & rs!CI21CODPERSONA & " - " & rs!RE & Chr$(9) _
                        & rs!RE
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    If strCodRESel <> "" Then
        strCodRESel = ""
        grdRespEcon.MoveFirst
        grdRespEcon.SelBookmarks.Add (grdRespEcon.RowBookmark(grdRespEcon.Row))
        Call pCargarDatos
    End If
    
    LockWindowUpdate 0&
End Sub

Private Function fNombreRE(strCodRE$) As String
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT ADFN01(CI21CODPERSONA) FROM CI2100 WHERE CI21CODPERSONA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodRE
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then fNombreRE = rs(0)
    rs.Close
    qry.Close
End Function

Private Sub pFormatearControles()
    
    cboTipoEcon.BackColor = objApp.objUserColor.lngMandatory
    cboEntidad.BackColor = objApp.objUserColor.lngMandatory
    txtCodRE.BackColor = objApp.objUserColor.lngMandatory
    txtCodRESup.BackColor = objApp.objUserColor.lngMandatory
    txtRE.BackColor = objApp.objUserColor.lngReadOnly
    txtRESup.BackColor = objApp.objUserColor.lngReadOnly

    With grdRespEcon
        .Columns(0).Caption = "Resp. Econ�mico"
        .Columns(0).Width = 5000
        .Columns(1).Caption = "RE"
        .Columns(1).Visible = False
        .Columns(2).Caption = "Tipo E."
        .Columns(2).Width = 700
        .Columns(3).Caption = "Entidad"
        .Columns(3).Width = 700
        .Columns(4).Caption = "Resp. Econ�mico Sup."
        .Columns(4).Width = 5000
        .Columns(5).Caption = "RES"
        .Columns(5).Visible = False

        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    
    With grdRespEconSup
        .Columns(0).Caption = "Resp. Econ�mico"
        .Columns(0).Width = 5000
        .Columns(1).Caption = "RE"
        .Columns(1).Visible = False
        .Columns(2).Caption = "Tipo E."
        .Columns(2).Width = 700
        .Columns(3).Caption = "Entidad"
        .Columns(3).Width = 700
        .Columns(4).Caption = "Resp. Econ�mico Sup."
        .Columns(4).Width = 5000
        .Columns(5).Caption = "RES"
        .Columns(5).Visible = False

        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub pCargarTiposEcon()
    Dim SQL$, rs As rdoResultset
    
    SQL = "SELECT CI32CODTIPECON, CI32DESTIPECON"
    SQL = SQL & " FROM CI3200"
    SQL = SQL & " WHERE CI32FECFIVGTEC IS NULL"
    SQL = SQL & " ORDER BY CI32CODTIPECON"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboTipoEcon.AddItem rs!CI32CODTIPECON & Chr$(9) & rs!CI32CODTIPECON & " - " & rs!CI32DESTIPECON
        rs.MoveNext
    Loop
    rs.Close
End Sub

Private Sub pCargarEntidades()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    cboEntidad.RemoveAll
    SQL = "SELECT CI13CODENTIDAD, CI13DESENTIDAD"
    SQL = SQL & " FROM CI1300"
    SQL = SQL & " WHERE CI13FECFIVGENT IS NULL"
    SQL = SQL & " AND CI32CODTIPECON = ?"
    SQL = SQL & " ORDER BY CI13CODENTIDAD"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = cboTipoEcon.Columns(0).Text
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboEntidad.AddItem rs!CI13CODENTIDAD & Chr$(9) & rs!CI13CODENTIDAD & " - " & rs!CI13DESENTIDAD
        rs.MoveNext
    Loop
    rs.Close
    If cboEntidad.Rows = 1 Then cboEntidad.Text = cboEntidad.Columns(1).Text
End Sub

Private Sub pLimpiar()
    grdRespEcon.SelBookmarks.RemoveAll
    txtCodRE.Text = "": strCodREAux = "": txtRE.Text = ""
    txtCodRE.BackColor = objApp.objUserColor.lngMandatory
    txtCodRE.Locked = False
    txtCodRESup.Text = "": txtRESup.Text = ""
    cboTipoEcon.Text = ""
    cboEntidad.RemoveAll: cboEntidad.Text = ""
    txtCodRE.SetFocus
End Sub

Private Sub pEliminar()
    Dim SQL$, qry As rdoQuery
    Dim strRESup$, strRE$, strTE$, strE$

    If grdRespEcon.SelBookmarks.Count = 0 Then Exit Sub

    If MsgBox("�Desea UD. elimiar el registro seleccionado?", vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Sub
    
    strRESup = Val(grdRespEcon.Columns("Resp. Econ�mico Sup.").CellText(grdRespEcon.SelBookmarks(0)))
    strRE = Val(grdRespEcon.Columns("Resp. Econ�mico").CellText(grdRespEcon.SelBookmarks(0)))
    strTE = grdRespEcon.Columns("Tipo E.").CellText(grdRespEcon.SelBookmarks(0))
    strE = grdRespEcon.Columns("Entidad").CellText(grdRespEcon.SelBookmarks(0))
    
    SQL = "DELETE FROM CI2900"
    SQL = SQL & " WHERE CI21CODPERSONA = ?"
    SQL = SQL & " AND CI21CODPERSONA_REC = ?"
    SQL = SQL & " AND CI32CODTIPECON = ?"
    SQL = SQL & " AND CI13CODENTIDAD = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strRESup
    qry(1) = strRE
    qry(2) = strTE
    qry(3) = strE
    qry.Execute
    If qry.RowsAffected > 0 Then
        grdRespEcon.DeleteSelected
    Else
        MsgBox "No se ha podido eliminar el registro.", vbExclamation, Me.Caption
        qry.Close
        Exit Sub
    End If
    qry.Close
    
    'Se intenta borrar tambi�n de la tabla CI0900 por si el Resp. Econ. Sup. ya no tiene
    'm�s registros en la CI2900
    SQL = "DELETE FROM CI0900"
    SQL = SQL & " WHERE CI21CODPERSONA = ?"
    SQL = SQL & " AND CI32CODTIPECON = ?"
    SQL = SQL & " AND CI13CODENTIDAD = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strRESup
    qry(1) = strTE
    qry(2) = strE
    On Error Resume Next
    qry.Execute
    On Error GoTo 0
    qry.Close
    
    Call pLimpiar
End Sub

Private Sub pGuardar()
    Dim SQL$, qry As rdoQuery

    If Not fComprobarAlGuardar Then Exit Sub
    
    'Se intenta a�adir a la tabla CI0900 por si no existe
    SQL = "INSERT INTO CI0900 (CI21CODPERSONA, CI32CODTIPECON, CI13CODENTIDAD)"
    SQL = SQL & " VALUES (?,?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = txtCodRESup.Text
    qry(1) = cboTipoEcon.Columns(0).Text
    qry(2) = cboEntidad.Columns(0).Text
    On Error Resume Next
    qry.Execute
    On Error GoTo 0

    If grdRespEcon.SelBookmarks.Count = 0 Then 'NUEVO
        SQL = "INSERT INTO CI2900 (CI21CODPERSONA, CI32CODTIPECON, CI13CODENTIDAD, CI21CODPERSONA_REC)"
        SQL = SQL & " VALUES (?,?,?,?)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = txtCodRESup.Text
        qry(1) = cboTipoEcon.Columns(0).Text
        qry(2) = cboEntidad.Columns(0).Text
        qry(3) = txtCodRE.Text
        qry.Execute
    Else 'MODIFICACI�N
        SQL = "UPDATE CI2900"
        SQL = SQL & " SET CI21CODPERSONA = ?,"
        SQL = SQL & " CI32CODTIPECON = ?,"
        SQL = SQL & " CI13CODENTIDAD = ?,"
        SQL = SQL & " CI21CODPERSONA_REC = ?"
        SQL = SQL & " WHERE CI21CODPERSONA = ?"
        SQL = SQL & " AND CI21CODPERSONA_REC = ?"
        SQL = SQL & " AND CI32CODTIPECON = ?"
        SQL = SQL & " AND CI13CODENTIDAD = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = txtCodRESup.Text
        qry(1) = cboTipoEcon.Columns(0).Text
        qry(2) = cboEntidad.Columns(0).Text
        qry(3) = txtCodRE.Text
        qry(4) = Val(grdRespEcon.Columns("Resp. Econ�mico Sup.").CellText(grdRespEcon.SelBookmarks(0)))
        qry(5) = Val(grdRespEcon.Columns("Resp. Econ�mico").CellText(grdRespEcon.SelBookmarks(0)))
        qry(6) = grdRespEcon.Columns("Tipo E.").CellText(grdRespEcon.SelBookmarks(0))
        qry(7) = grdRespEcon.Columns("Entidad").CellText(grdRespEcon.SelBookmarks(0))
        qry.Execute
    End If
    Call pCargarRE(txtCodRE.Text)
    Call pLimpiar
End Sub

Private Function fComprobarAlGuardar() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    On Error Resume Next
    For Each c In Me
        strColor = c.BackColor
        If strColor = objApp.objUserColor.lngMandatory Then
            If c.Text = "" Or c.Text = "__:__" Then
                msg = "Existen datos obligatorios sin rellenar."
                MsgBox msg, vbExclamation, Me.Caption
                On Error GoTo 0
                Exit Function
            End If
        End If
    Next
    On Error GoTo 0
    
    'existencia de los responsables econ�micos como personas jur�dicas
    If txtRE.Text = "" Then
        msg = "El responsable econ�mico no existe."
        MsgBox msg, vbExclamation, Me.Caption
        Exit Function
    End If
    If txtRESup.Text = "" Then
        msg = "El responsable econ�mico superior no existe."
        MsgBox msg, vbExclamation, Me.Caption
        Exit Function
    End If
        
    fComprobarAlGuardar = True
End Function

Private Sub pReposicionarEntidad(strEntidad$)
    Dim i%
    Call pCargarEntidades
    For i = 1 To cboEntidad.Rows
        If i = 1 Then cboEntidad.MoveFirst Else cboEntidad.MoveNext
        If cboEntidad.Columns(0).Text = strEntidad Then
            cboEntidad.Text = cboEntidad.Columns(1).Text
            Exit For
        End If
    Next i
End Sub
