VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmResponsables 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISI�N. Personas F�sicas"
   ClientHeight    =   8340
   ClientLeft      =   45
   ClientTop       =   615
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Height          =   420
      Left            =   0
      TabIndex        =   50
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin idperson.IdPersona IdPersona2 
      Height          =   255
      Left            =   2640
      TabIndex        =   137
      Top             =   7800
      Visible         =   0   'False
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   450
      BackColor       =   12648384
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Datafield       =   "CI21CodPersona"
      MaxLength       =   7
      blnAvisos       =   0   'False
   End
   Begin idperson.IdPersona IdPersona1 
      Height          =   375
      Left            =   4800
      TabIndex        =   136
      Top             =   7680
      Visible         =   0   'False
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      BackColor       =   12648384
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Datafield       =   "CI21CodPersona"
      MaxLength       =   7
      blnAvisos       =   0   'False
   End
   Begin VB.CommandButton cmdUniversidad 
      Caption         =   "Consultar U.N."
      Height          =   375
      Left            =   6720
      TabIndex        =   135
      Top             =   7680
      Width           =   1455
   End
   Begin VB.CommandButton cmdAcunsa 
      Caption         =   "Consultar Acunsa"
      Height          =   375
      Left            =   8400
      TabIndex        =   134
      Top             =   7680
      Width           =   1455
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Direcciones"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3810
      Index           =   1
      Left            =   120
      TabIndex        =   48
      Tag             =   "Mantenimiento de Direcciones de  Personas F�sicas"
      Top             =   3840
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   3420
         Index           =   1
         Left            =   120
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   360
         Width           =   11520
         _ExtentX        =   20320
         _ExtentY        =   6033
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AD0128.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tabTab1(26)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin TabDlg.SSTab tabTab1 
            Height          =   3180
            Index           =   26
            Left            =   120
            TabIndex        =   76
            Top             =   120
            Width           =   10980
            _ExtentX        =   19368
            _ExtentY        =   5609
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabHeight       =   520
            TabCaption(0)   =   "Do&micilio"
            TabPicture(0)   =   "AD0128.frx":001C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(0)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(7)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(33)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(35)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(36)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(37)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(4)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(38)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(39)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(40)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(3)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(2)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(34)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "dtcDateCombo1(3)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "dtcDateCombo1(2)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "cboSSDBCombo1(11)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "cboSSDBCombo1(10)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(18)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(23)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(24)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(37)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(27)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "chkCheck1(0)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(5)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(28)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(21)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(20)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(19)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "txtText1(33)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "txtText1(10)"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).ControlCount=   30
            TabCaption(1)   =   "Observa&ciones"
            TabPicture(1)   =   "AD0128.frx":0038
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(25)"
            Tab(1).Control(1)=   "lblLabel1(41)"
            Tab(1).ControlCount=   2
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10FAX"
               Height          =   330
               HelpContextID   =   30104
               Index           =   10
               Left            =   2400
               TabIndex        =   139
               Tag             =   "Tel�fono"
               Top             =   2760
               Width           =   1890
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI16CODLOCALID"
               Height          =   330
               HelpContextID   =   30101
               Index           =   33
               Left            =   10245
               TabIndex        =   94
               TabStop         =   0   'False
               Tag             =   "N�mero Direcci�n"
               Top             =   2325
               Visible         =   0   'False
               Width           =   525
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10CALLE"
               Height          =   330
               HelpContextID   =   30104
               Index           =   19
               Left            =   240
               TabIndex        =   36
               Tag             =   "Calle"
               Top             =   2040
               Width           =   4530
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10PORTAL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   20
               Left            =   5010
               TabIndex        =   37
               Tag             =   "Portal"
               Top             =   2040
               Width           =   1050
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10RESTODIREC"
               Height          =   330
               HelpContextID   =   30104
               Index           =   21
               Left            =   6240
               TabIndex        =   38
               Tag             =   "Resto Direcci�n"
               Top             =   2040
               Width           =   2730
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI17CODMUNICIP"
               Height          =   330
               HelpContextID   =   30101
               Index           =   28
               Left            =   10200
               TabIndex        =   34
               TabStop         =   0   'False
               Tag             =   "N�mero Direcci�n"
               Top             =   1800
               Visible         =   0   'False
               Width           =   525
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI07CODPOSTAL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   5
               Left            =   240
               TabIndex        =   33
               Tag             =   "C�digo Postal"
               Top             =   1320
               Width           =   1350
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Direcci�n Principal"
               DataField       =   "CI10INDDIRPRINC"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   8640
               TabIndex        =   91
               Tag             =   "Indicador Direcci�n Principal"
               Top             =   2760
               Width           =   2100
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               HelpContextID   =   30101
               Index           =   27
               Left            =   9255
               TabIndex        =   89
               TabStop         =   0   'False
               Tag             =   "C�digo Persona"
               Top             =   2250
               Visible         =   0   'False
               Width           =   780
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10OBSERVACIO"
               Height          =   1485
               HelpContextID   =   30104
               Index           =   25
               Left            =   -74700
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   42
               Tag             =   "Observaciones"
               Top             =   810
               Width           =   10365
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10TELEFONO"
               Height          =   330
               HelpContextID   =   30104
               Index           =   37
               Left            =   240
               TabIndex        =   39
               Tag             =   "Tel�fono"
               Top             =   2760
               Width           =   1890
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10DESLOCALID"
               Height          =   330
               HelpContextID   =   30104
               Index           =   24
               Left            =   1800
               TabIndex        =   35
               Tag             =   "Localidad"
               Top             =   1320
               Width           =   5550
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10DESPROVI"
               Height          =   330
               HelpContextID   =   30104
               Index           =   23
               Left            =   7320
               TabIndex        =   32
               Tag             =   "Provincia"
               Top             =   600
               Width           =   2820
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI10NUMDIRECCI"
               Height          =   330
               HelpContextID   =   30101
               Index           =   18
               Left            =   240
               TabIndex        =   29
               Tag             =   "N�mero Direcci�n"
               Top             =   600
               Width           =   885
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI26CODPROVI"
               Height          =   330
               Index           =   10
               Left            =   4440
               TabIndex        =   31
               Tag             =   "C�digo Provincia"
               Top             =   600
               Width           =   2685
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4736
               Columns(1).Caption=   "Provincia"
               Columns(1).Name =   "Provincia"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4736
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI19CODPAIS"
               Height          =   330
               Index           =   11
               Left            =   1440
               TabIndex        =   30
               Tag             =   "C�digo Pa�s"
               Top             =   600
               Width           =   2685
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3889
               Columns(1).Caption=   "Pa�s"
               Columns(1).Name =   "Pa�s"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4736
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI10FECINIVALID"
               Height          =   330
               Index           =   2
               Left            =   4560
               TabIndex        =   40
               Tag             =   "Fecha Inicio Vigencia"
               Top             =   2760
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               BevelColorFace  =   12632256
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI10FECFINVALID"
               Height          =   330
               Index           =   3
               Left            =   6600
               TabIndex        =   41
               Tag             =   "Fecha Fin Vigencia"
               Top             =   2760
               Width           =   1860
               _Version        =   65537
               _ExtentX        =   3281
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               BevelColorFace  =   12632256
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fax"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   34
               Left            =   2400
               TabIndex        =   140
               Top             =   2520
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Calle"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   240
               TabIndex        =   93
               Top             =   1770
               Width           =   435
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Portal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   5010
               TabIndex        =   92
               Top             =   1770
               Width           =   510
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   41
               Left            =   -74700
               TabIndex        =   87
               Top             =   570
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Fin Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   40
               Left            =   6600
               TabIndex        =   86
               Top             =   2520
               Width           =   1650
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Inicio Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   39
               Left            =   4560
               TabIndex        =   85
               Top             =   2520
               Width           =   1860
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   38
               Left            =   1560
               TabIndex        =   84
               Top             =   360
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   7800
               TabIndex        =   83
               Top             =   480
               Width           =   75
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   37
               Left            =   270
               TabIndex        =   82
               Top             =   2520
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   36
               Left            =   7320
               TabIndex        =   81
               Top             =   360
               Width           =   1875
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   4440
               TabIndex        =   80
               Top             =   360
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   33
               Left            =   1800
               TabIndex        =   79
               Top             =   1080
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Postal"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   360
               TabIndex        =   78
               Top             =   1080
               Width           =   1185
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   240
               TabIndex        =   77
               Top             =   360
               Width           =   600
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3240
            Index           =   1
            Left            =   -74910
            TabIndex        =   47
            TabStop         =   0   'False
            Top             =   90
            Width           =   11055
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19500
            _ExtentY        =   5715
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Personas F�sicas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3435
      Index           =   0
      Left            =   120
      TabIndex        =   45
      Tag             =   "Mantenimiento de Personas F�sicas"
      Top             =   360
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   3015
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   360
         Width           =   11520
         _ExtentX        =   20320
         _ExtentY        =   5318
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "AD0128.frx":0054
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tabTab1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "AD0128.frx":0070
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2730
            Index           =   0
            Left            =   -74910
            TabIndex        =   44
            Top             =   90
            Width           =   10965
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19341
            _ExtentY        =   4815
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   2805
            Index           =   2
            Left            =   120
            TabIndex        =   28
            Tag             =   "Fecha Nacimiento"
            Top             =   120
            Width           =   10875
            _ExtentX        =   19182
            _ExtentY        =   4948
            _Version        =   327681
            Style           =   1
            Tabs            =   6
            TabsPerRow      =   6
            TabHeight       =   520
            TabCaption(0)   =   "Datos &Personales"
            TabPicture(0)   =   "AD0128.frx":008C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(18)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(17)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(15)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(14)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(13)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(12)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(11)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(10)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(8)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(6)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(5)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(1)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(43)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "cboSSDBCombo1(8)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "cboSSDBCombo1(12)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "cboSSDBCombo1(2)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "cboSSDBCombo1(1)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "cboSSDBCombo1(0)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(26)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(8)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(9)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(7)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(6)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(4)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(3)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(2)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(1)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(0)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "chkCheck1(1)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "txtText1(39)"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).Control(30)=   "txtText1(46)"
            Tab(0).Control(30).Enabled=   0   'False
            Tab(0).Control(31)=   "txtText1(47)"
            Tab(0).Control(31).Enabled=   0   'False
            Tab(0).Control(32)=   "txtText1(48)"
            Tab(0).Control(32).Enabled=   0   'False
            Tab(0).Control(33)=   "chkCheck1(2)"
            Tab(0).Control(33).Enabled=   0   'False
            Tab(0).Control(34)=   "txtText1(49)"
            Tab(0).Control(34).Enabled=   0   'False
            Tab(0).Control(35)=   "chkCheck1(3)"
            Tab(0).Control(35).Enabled=   0   'False
            Tab(0).ControlCount=   36
            TabCaption(1)   =   "Datos &Nacimiento"
            TabPicture(1)   =   "AD0128.frx":00A8
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(34)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "txtText1(32)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "txtText1(11)"
            Tab(1).Control(3)=   "txtText1(12)"
            Tab(1).Control(4)=   "dtcDateCombo1(0)"
            Tab(1).Control(5)=   "dtcDateCombo1(1)"
            Tab(1).Control(6)=   "cboSSDBCombo1(3)"
            Tab(1).Control(7)=   "cboSSDBCombo1(4)"
            Tab(1).Control(8)=   "lblLabel1(42)"
            Tab(1).Control(9)=   "lblLabel1(19)"
            Tab(1).Control(10)=   "lblLabel1(20)"
            Tab(1).Control(11)=   "lblLabel1(21)"
            Tab(1).Control(12)=   "lblLabel1(22)"
            Tab(1).Control(13)=   "lblLabel1(23)"
            Tab(1).Control(14)=   "lblLabel1(25)"
            Tab(1).ControlCount=   15
            TabCaption(2)   =   "Datos Profe&sionales"
            TabPicture(2)   =   "AD0128.frx":00C4
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(38)"
            Tab(2).Control(1)=   "txtText1(13)"
            Tab(2).Control(2)=   "txtText1(14)"
            Tab(2).Control(3)=   "txtText1(15)"
            Tab(2).Control(4)=   "cboSSDBCombo1(6)"
            Tab(2).Control(5)=   "cboSSDBCombo1(5)"
            Tab(2).Control(6)=   "lblLabel1(26)"
            Tab(2).Control(7)=   "lblLabel1(27)"
            Tab(2).Control(8)=   "lblLabel1(28)"
            Tab(2).Control(9)=   "lblLabel1(29)"
            Tab(2).ControlCount=   10
            TabCaption(3)   =   "Persona Responsa&ble"
            TabPicture(3)   =   "AD0128.frx":00E0
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "Frame2"
            Tab(3).Control(1)=   "Frame1"
            Tab(3).ControlCount=   2
            TabCaption(4)   =   "Obser&vaciones"
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "lblLabel1(32)"
            Tab(4).Control(1)=   "txtText1(17)"
            Tab(4).ControlCount=   2
            TabCaption(5)   =   "Responsable &Familiar"
            TabPicture(5)   =   "AD0128.frx":00FC
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "lblLabel1(46)"
            Tab(5).Control(1)=   "lblLabel1(47)"
            Tab(5).Control(2)=   "lblLabel1(48)"
            Tab(5).Control(3)=   "lblLabel1(49)"
            Tab(5).Control(4)=   "lblLabel1(50)"
            Tab(5).Control(5)=   "lblLabel1(51)"
            Tab(5).Control(6)=   "lblLabel1(52)"
            Tab(5).Control(7)=   "lblLabel1(53)"
            Tab(5).Control(8)=   "cboSSDBCombo1(13)"
            Tab(5).Control(9)=   "cboSSDBCombo1(9)"
            Tab(5).Control(10)=   "txtText1(40)"
            Tab(5).Control(11)=   "txtText1(41)"
            Tab(5).Control(12)=   "txtText1(42)"
            Tab(5).Control(13)=   "txtText1(43)"
            Tab(5).Control(14)=   "txtText1(44)"
            Tab(5).Control(15)=   "txtText1(45)"
            Tab(5).ControlCount=   16
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Paciente"
               DataField       =   "CI22INDHISTORIA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   3
               Left            =   9300
               TabIndex        =   138
               Tag             =   "Estado Persona (Activo/Borrado)"
               Top             =   2280
               Width           =   1290
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD34CODESTADO"
               Height          =   330
               HelpContextID   =   30101
               Index           =   49
               Left            =   8760
               MaxLength       =   3
               TabIndex        =   133
               Tag             =   "Estado Persona (1-Activo, 2-Borrado)"
               Top             =   2280
               Visible         =   0   'False
               Width           =   330
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Borrado"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   2
               Left            =   9000
               TabIndex        =   16
               Tag             =   "Estado Persona (Activo/Borrado)"
               Top             =   1800
               Visible         =   0   'False
               Width           =   1290
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NUMSEGSOC"
               Height          =   285
               HelpContextID   =   30104
               Index           =   48
               Left            =   8400
               TabIndex        =   132
               Tag             =   "N� Seguridad Social"
               Top             =   1080
               Visible         =   0   'False
               Width           =   450
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   47
               Left            =   6360
               MaxLength       =   8
               TabIndex        =   8
               Tag             =   "N� Seguridad Social"
               Top             =   1440
               Width           =   1530
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   46
               Left            =   7920
               MaxLength       =   2
               TabIndex        =   9
               Tag             =   "Dig. Control N� SS"
               Top             =   1440
               Width           =   450
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMRFTELEFO"
               Height          =   330
               HelpContextID   =   30104
               Index           =   45
               Left            =   -68205
               TabIndex        =   130
               Tag             =   "Tel�fono IBM"
               Top             =   2160
               Width           =   1170
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMRFPOBLAC"
               Height          =   330
               HelpContextID   =   30104
               Index           =   44
               Left            =   -68205
               TabIndex        =   123
               Tag             =   "Localidad IBM|Localidad IBM"
               Top             =   1440
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMRFDIRECC"
               Height          =   330
               HelpContextID   =   30104
               Index           =   43
               Left            =   -74760
               TabIndex        =   122
               Tag             =   "Direcci�n IBM|Direcci�n IBM"
               Top             =   2160
               Width           =   6210
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMRFSEGAPEL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   42
               Left            =   -68205
               TabIndex        =   118
               Tag             =   "Segundo Apellido IBM|Segundo Apellido IBM"
               Top             =   720
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMRFPRIAPEL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   41
               Left            =   -71445
               TabIndex        =   117
               Tag             =   "Primer Apellido IBM|Primer Apellido IBM"
               Top             =   720
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMRFNOMBRE"
               Height          =   330
               HelpContextID   =   30104
               Index           =   40
               Left            =   -74745
               TabIndex        =   116
               Tag             =   "Nombre Persona IBM|Nombre Persona IBM"
               Top             =   720
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMRELUDN"
               Height          =   285
               HelpContextID   =   30104
               Index           =   39
               Left            =   9960
               TabIndex        =   115
               Tag             =   "N�mero Historia"
               Top             =   360
               Visible         =   0   'False
               Width           =   360
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22IBMPROFESION"
               Height          =   330
               HelpContextID   =   30104
               Index           =   38
               Left            =   -67200
               TabIndex        =   113
               Tag             =   "Profesi�n IBM"
               Top             =   1560
               Visible         =   0   'False
               Width           =   2610
            End
            Begin VB.Frame Frame2 
               Caption         =   "Responsable Econ�mico"
               Height          =   870
               Left            =   -74910
               TabIndex        =   107
               Top             =   1860
               Width           =   10710
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   29
                  Left            =   1755
                  TabIndex        =   109
                  Tag             =   "Responsable Econ�mico"
                  Top             =   435
                  Width           =   8610
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "CI21CODPERSONA_REC"
                  Height          =   330
                  HelpContextID   =   30101
                  Index           =   31
                  Left            =   375
                  TabIndex        =   108
                  Tag             =   "C�digo Persona|C�digo"
                  Top             =   450
                  Width           =   1140
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   " Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   16
                  Left            =   1695
                  TabIndex        =   111
                  Top             =   225
                  Width           =   720
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   45
                  Left            =   375
                  TabIndex        =   110
                  Top             =   225
                  Width           =   600
               End
            End
            Begin VB.Frame Frame1 
               Caption         =   "Responsable Familiar"
               Height          =   1455
               Left            =   -74910
               TabIndex        =   96
               Top             =   360
               Width           =   10710
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   36
                  Left            =   7545
                  TabIndex        =   100
                  Tag             =   "PersonaResponsable"
                  Top             =   450
                  Width           =   2820
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   35
                  Left            =   4560
                  TabIndex        =   99
                  Tag             =   "PersonaResponsable"
                  Top             =   450
                  Width           =   2820
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  Height          =   330
                  HelpContextID   =   30104
                  Index           =   16
                  Left            =   1635
                  TabIndex        =   98
                  Tag             =   "PersonaResponsable"
                  Top             =   450
                  Width           =   2730
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "CI21CODPERSONA_RFA"
                  Height          =   330
                  HelpContextID   =   30101
                  Index           =   30
                  Left            =   375
                  TabIndex        =   97
                  TabStop         =   0   'False
                  Tag             =   "C�digo Persona|C�digo"
                  Top             =   450
                  Width           =   1035
               End
               Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
                  DataField       =   "CI35CODTIPVINC"
                  Height          =   330
                  Index           =   7
                  Left            =   360
                  TabIndex        =   101
                  Tag             =   "Tipo de Vinculaci�n"
                  Top             =   1080
                  Width           =   3045
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColorEven   =   0
                  BackColorOdd    =   16777215
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   1508
                  Columns(0).Caption=   "C�digo"
                  Columns(0).Name =   "C�digo"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   3916
                  Columns(1).Caption=   "Descripci�n"
                  Columns(1).Name =   "Descripci�n"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5371
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   16777215
                  DataFieldToDisplay=   "Column 1"
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Segundo Apellido"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   24
                  Left            =   7545
                  TabIndex        =   106
                  Top             =   225
                  Width           =   1500
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Primer Apellido"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   9
                  Left            =   4560
                  TabIndex        =   105
                  Top             =   225
                  Width           =   1275
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tipo de Vinculaci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   31
                  Left            =   375
                  TabIndex        =   104
                  Top             =   840
                  Width           =   1710
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   " Nombre"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   30
                  Left            =   1575
                  TabIndex        =   103
                  Top             =   225
                  Width           =   720
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�digo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   44
                  Left            =   375
                  TabIndex        =   102
                  Top             =   225
                  Width           =   600
               End
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI16CODLOCALID"
               Height          =   330
               HelpContextID   =   30104
               Index           =   34
               Left            =   -66975
               TabIndex        =   95
               TabStop         =   0   'False
               Tag             =   "C�d.Localidad"
               Top             =   2250
               Visible         =   0   'False
               Width           =   720
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI17CODMUNICIP"
               Height          =   330
               HelpContextID   =   30104
               Index           =   32
               Left            =   -68040
               TabIndex        =   23
               TabStop         =   0   'False
               Tag             =   "C�d.Municipio"
               Top             =   2265
               Visible         =   0   'False
               Width           =   705
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Provisional"
               DataField       =   "CI22INDPROVISI"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   9300
               TabIndex        =   15
               Tag             =   "Persona Provisional"
               Top             =   2040
               Width           =   1290
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               HelpContextID   =   30101
               Index           =   0
               Left            =   240
               TabIndex        =   0
               TabStop         =   0   'False
               Tag             =   "N�meroPersona|N�mero"
               Top             =   720
               Width           =   1275
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NOMBRE"
               Height          =   330
               HelpContextID   =   30104
               Index           =   1
               Left            =   1800
               TabIndex        =   1
               Tag             =   "Nombre Persona|Nombre Persona"
               Top             =   720
               Width           =   2610
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22PRIAPEL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   2
               Left            =   4680
               TabIndex        =   2
               Tag             =   "Primer Apellido|Primer Apellido"
               Top             =   720
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22SEGAPEL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   3
               Left            =   7800
               TabIndex        =   3
               Tag             =   "Segundo Apellido|Segundo Apellido"
               Top             =   720
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22DNI"
               Height          =   330
               HelpContextID   =   30104
               Index           =   4
               Left            =   240
               TabIndex        =   4
               Tag             =   "D.N.I|D.N.I "
               Top             =   1440
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   6
               Left            =   5895
               MaxLength       =   2
               TabIndex        =   7
               Tag             =   "C.Provincia N� SS"
               Top             =   1440
               Width           =   450
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22TFNOMOVIL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   7
               Left            =   240
               TabIndex        =   11
               Tag             =   "Tel�fono Movil"
               Top             =   2160
               Width           =   1785
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA_REA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   9
               Left            =   8940
               TabIndex        =   10
               Tag             =   "C�d. Persona Real"
               Top             =   1440
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NUMHISTORIA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   8
               Left            =   7020
               Locked          =   -1  'True
               TabIndex        =   14
               Tag             =   "N�mero Historia"
               Top             =   2175
               Width           =   1680
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NUMDIRPRINC"
               Height          =   330
               HelpContextID   =   30101
               Index           =   26
               Left            =   10305
               MaxLength       =   4
               TabIndex        =   52
               Tag             =   "C�digo Empleado|C�digo"
               Top             =   345
               Visible         =   0   'False
               Width           =   330
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22DESPROVI"
               Height          =   330
               HelpContextID   =   30104
               Index           =   11
               Left            =   -68280
               TabIndex        =   19
               Tag             =   "Provincia"
               Top             =   840
               Width           =   3210
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22DESLOCALID"
               Height          =   330
               HelpContextID   =   30104
               Index           =   12
               Left            =   -74655
               TabIndex        =   20
               Tag             =   "Localidad"
               Top             =   1560
               Width           =   5130
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22PUESTO"
               Height          =   330
               HelpContextID   =   30104
               Index           =   13
               Left            =   -74610
               TabIndex        =   26
               Tag             =   "Puesto"
               Top             =   1695
               Width           =   4050
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22EMPRESA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   14
               Left            =   -70230
               TabIndex        =   25
               Tag             =   "Empresa"
               Top             =   960
               Width           =   3330
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22TFNOEMPRESA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   15
               Left            =   -70230
               TabIndex        =   27
               Tag             =   "Tel�fono Empresa"
               Top             =   1695
               Width           =   2610
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22OBSERVAC"
               Height          =   1455
               Index           =   17
               Left            =   -74730
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   51
               Tag             =   "Observaciones"
               Top             =   750
               Width           =   10350
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI30CODSEXO"
               Height          =   330
               Index           =   0
               Left            =   2205
               TabIndex        =   5
               Tag             =   "Sexo"
               Top             =   1440
               Width           =   1290
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2355
               Columns(1).Caption=   "Sexo"
               Columns(1).Name =   "Sexo"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2275
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI14CODESTCIVI"
               Height          =   330
               Index           =   1
               Left            =   3800
               TabIndex        =   6
               Tag             =   "Estado Civil"
               Top             =   1440
               Width           =   1725
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3043
               Columns(1).Caption=   "Estado Civil"
               Columns(1).Name =   "Estado Civil"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3043
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               Height          =   330
               Index           =   2
               Left            =   2205
               TabIndex        =   12
               Tag             =   "Relaci�n UDN"
               Top             =   2160
               Width           =   2880
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4551
               Columns(1).Caption=   "Relaci�n UDN"
               Columns(1).Name =   "Relaci�n UDN"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5080
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI22FECNACIM"
               Height          =   330
               Index           =   0
               Left            =   -74640
               TabIndex        =   21
               Tag             =   "Fecha de Nacimiento"
               Top             =   2280
               Width           =   1980
               _Version        =   65537
               _ExtentX        =   3492
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1860/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI22FECFALLE"
               Height          =   330
               Index           =   1
               Left            =   -72120
               TabIndex        =   22
               Tag             =   "Fecha de Fallecimiento"
               Top             =   2280
               Width           =   1980
               _Version        =   65537
               _ExtentX        =   3492
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI19CODPAIS"
               Height          =   330
               Index           =   3
               Left            =   -74640
               TabIndex        =   17
               Tag             =   "C�digo Pa�s"
               Top             =   840
               Width           =   3045
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4022
               Columns(1).Caption=   "Pa�s"
               Columns(1).Name =   "Pa�s"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5371
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI26CODPROVI"
               Height          =   330
               Index           =   4
               Left            =   -71280
               TabIndex        =   18
               Tag             =   "C�digo Provincia"
               Top             =   840
               Width           =   2715
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4736
               Columns(1).Caption=   "Provincia"
               Columns(1).Name =   "Provincia"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4789
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               Height          =   330
               Index           =   6
               Left            =   -74640
               TabIndex        =   24
               Tag             =   "Profesi�n"
               Top             =   960
               Width           =   3330
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5874
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI34CODTRATAMI"
               Height          =   330
               Index           =   12
               Left            =   5250
               TabIndex        =   13
               Tag             =   "Tratamiento"
               Top             =   2175
               Width           =   1590
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3043
               Columns(1).Caption=   "Estado Civil"
               Columns(1).Name =   "Estado Civil"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2805
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI25CODPROFESI"
               Height          =   330
               Index           =   5
               Left            =   -67680
               TabIndex        =   112
               Tag             =   "Profesi�n"
               Top             =   480
               Visible         =   0   'False
               Width           =   3330
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5874
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI28CODRELUDN"
               Height          =   330
               Index           =   8
               Left            =   9360
               TabIndex        =   114
               Tag             =   "Relaci�n UDN"
               Top             =   360
               Visible         =   0   'False
               Width           =   600
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4551
               Columns(1).Caption=   "Relaci�n UDN"
               Columns(1).Name =   "Relaci�n UDN"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1058
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI19CODPAIS_IRF"
               Height          =   330
               Index           =   9
               Left            =   -74760
               TabIndex        =   126
               Tag             =   "C�digo Pa�s IBM"
               Top             =   1440
               Width           =   2805
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4948
               Columns(1).Caption=   "Pa�s"
               Columns(1).Name =   "Pa�s"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4948
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI26CODPROVI_IRF"
               Height          =   330
               Index           =   13
               Left            =   -71445
               TabIndex        =   128
               Tag             =   "C�digo Provincia IBM"
               Top             =   1440
               Width           =   2805
               DataFieldList   =   "Column 0"
               AllowInput      =   0   'False
               AutoRestore     =   0   'False
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4948
               Columns(1).Caption=   "Provincia"
               Columns(1).Name =   "Provincia"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   4948
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   53
               Left            =   -68205
               TabIndex        =   131
               Top             =   1920
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   52
               Left            =   -71445
               TabIndex        =   129
               Top             =   1200
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   51
               Left            =   -74760
               TabIndex        =   127
               Top             =   1200
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   50
               Left            =   -68205
               TabIndex        =   125
               Top             =   1200
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   49
               Left            =   -74760
               TabIndex        =   124
               Top             =   1920
               Width           =   825
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Segundo Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   48
               Left            =   -68205
               TabIndex        =   121
               Top             =   480
               Width           =   1500
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Primer Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   47
               Left            =   -71445
               TabIndex        =   120
               Top             =   480
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   46
               Left            =   -74745
               TabIndex        =   119
               Top             =   480
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tratamiento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   43
               Left            =   5235
               TabIndex        =   90
               Top             =   1935
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   42
               Left            =   -73680
               TabIndex        =   88
               Top             =   600
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�mero"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   255
               TabIndex        =   75
               Top             =   480
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   1845
               TabIndex        =   74
               Top             =   480
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Primer Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   4680
               TabIndex        =   73
               Top             =   480
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Segundo Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   7800
               TabIndex        =   72
               Top             =   480
               Width           =   1500
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "D.N.I"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   255
               TabIndex        =   71
               Top             =   1200
               Width           =   465
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   2205
               TabIndex        =   70
               Top             =   1200
               Width           =   435
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�mero Seguridad Social"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   5895
               TabIndex        =   69
               Top             =   1200
               Width           =   2160
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono Movil"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   240
               TabIndex        =   68
               Top             =   1920
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Estado Civil"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   14
               Left            =   3800
               TabIndex        =   67
               Top             =   1200
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Relaci�n UDN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   15
               Left            =   2205
               TabIndex        =   66
               Top             =   1920
               Width           =   1230
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   " C�d. Persona Real"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   17
               Left            =   8880
               TabIndex        =   65
               Top             =   1215
               Width           =   1665
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�mero de Historia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   18
               Left            =   7050
               TabIndex        =   64
               Top             =   1950
               Width           =   1635
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha de Naciminento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   -74640
               TabIndex        =   63
               Top             =   2040
               Width           =   1920
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha de Fallecimiento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   20
               Left            =   -72120
               TabIndex        =   62
               Top             =   2040
               Width           =   1980
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d. Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   -74640
               TabIndex        =   61
               Top             =   600
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   -70320
               TabIndex        =   60
               Top             =   600
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   23
               Left            =   -67440
               TabIndex        =   59
               Top             =   600
               Width           =   1875
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   25
               Left            =   -74655
               TabIndex        =   58
               Top             =   1320
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Profesi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   26
               Left            =   -74640
               TabIndex        =   57
               Top             =   720
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Puesto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   27
               Left            =   -74610
               TabIndex        =   56
               Top             =   1455
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Empresa"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   28
               Left            =   -70230
               TabIndex        =   55
               Top             =   720
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   29
               Left            =   -70230
               TabIndex        =   54
               Top             =   1455
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Observaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   32
               Left            =   -74730
               TabIndex        =   53
               Top             =   510
               Width           =   1275
            End
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   49
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmResponsables"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim WithEvents objSearchCalles As clsCWSearch
Attribute objSearchCalles.VB_VarHelpID = -1
Dim blnCalles As Boolean
Dim strTipoPoblacion As String
Dim lngPortalImpCom As Long
Dim lngPortalImpFin As Long
Dim lngPortalParCom As Long
Dim lngPortalParFin As Long
Dim lngNumPersona   As Long
Dim blnControlPortal As Boolean
Dim blnActDirPrinc As Boolean
Dim blnActivado As Boolean

Dim blnBorrando As Boolean
Dim blnUpdate As Boolean



Public Function Hay_Persona() As Boolean
If txtText1(0).Text <> "" Then
    Hay_Persona = True
Else
    MsgBox "No hay ninguna persona seleccionada", vbOKOnly
    Hay_Persona = False
End If
End Function
Public Sub DeshabilitarPersona()
    txtText1(0).Enabled = False
    txtText1(1).Enabled = False
    txtText1(2).Enabled = False
    txtText1(3).Enabled = False
    txtText1(4).Enabled = False
    txtText1(6).Enabled = False
    txtText1(47).Enabled = False
    txtText1(46).Enabled = False
    txtText1(9).Enabled = False
    txtText1(7).Enabled = False
    txtText1(8).Enabled = False
'    txtText1(10).Enabled = False
    txtText1(11).Enabled = False
    txtText1(12).Enabled = False
    txtText1(13).Enabled = False
    txtText1(14).Enabled = False
    txtText1(15).Enabled = False
    txtText1(16).Enabled = False
    txtText1(30).Enabled = False
    txtText1(35).Enabled = False
    txtText1(36).Enabled = False
    txtText1(29).Enabled = False
    txtText1(31).Enabled = False
    txtText1(17).Enabled = False
    objWinInfo.CtrlGetInfo(chkCheck1(1)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(2)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(12)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(4)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(6)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(7)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
    objWinInfo.CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    fraFrame1(1).Enabled = False
    objWinInfo.WinPrepareScr
End Sub

Public Sub HabilitarPersona()
    txtText1(0).Enabled = True
    txtText1(1).Enabled = True
    txtText1(2).Enabled = True
    txtText1(3).Enabled = True
    txtText1(4).Enabled = True
    txtText1(6).Enabled = True
    txtText1(47).Enabled = True
    txtText1(46).Enabled = True
    txtText1(9).Enabled = True
    txtText1(7).Enabled = True
    txtText1(8).Enabled = True
'    txtText1(10).Enabled = True
    txtText1(11).Enabled = True
    txtText1(12).Enabled = True
    txtText1(13).Enabled = True
    txtText1(14).Enabled = True
    txtText1(15).Enabled = True
    txtText1(16).Enabled = True
    txtText1(30).Enabled = True
    txtText1(35).Enabled = True
    txtText1(36).Enabled = True
    txtText1(29).Enabled = True
    txtText1(31).Enabled = True
    txtText1(17).Enabled = True
    objWinInfo.CtrlGetInfo(chkCheck1(1)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(2)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(12)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(3)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(4)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(6)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(cboSSDBCombo1(7)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = False
    objWinInfo.CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = False
    fraFrame1(1).Enabled = True
    objWinInfo.WinPrepareScr
End Sub
Public Sub LocalizarCalles(intCodProvi As Integer, strTipoPob As String)
  Dim objFieldCalle As clsCWFieldSearch

  Set objSearchCalles = New clsCWSearch
  
  With objSearchCalles
    .strTable = "CI0700"
    .strWhere = "WHERE CI26CODPROVI=" & cboSSDBCombo1(10).Value & " AND CI07TIPPOBLA='" & strTipoPob & "'"
    .strOrder = "ORDER BY CI07CALLE"
    
    Set objFieldCalle = .AddField("CI07CODPOSTAL")
    objFieldCalle.strSmallDesc = "C�DIGO_POSTAL"
    Set objFieldCalle = .AddField("CI07DESPOBLA")
    objFieldCalle.blnInGrid = False
    
    Set objFieldCalle = .AddField("CI07CALLE")
    objFieldCalle.strSmallDesc = "CALLE                               "
    
      Set objFieldCalle = .AddField("CI07NUMIMPCOM")
      objFieldCalle.strSmallDesc = "COMIENZO IMPAR"
      
      Set objFieldCalle = .AddField("CI07NUMIMPFIN")
      objFieldCalle.strSmallDesc = "FIN IMPAR"
      
      Set objFieldCalle = .AddField("CI07NUMPARCOM")
      objFieldCalle.strSmallDesc = "COMIENZO PAR"
      
      Set objFieldCalle = .AddField("CI07NUMPARFIN")
      objFieldCalle.strSmallDesc = "FIN PAR"
        
    If .ViewSelect Then
      Call objWinInfo.CtrlSet(txtText1(19), .cllValues("CI07CALLE"))
      Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
      
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
      If .cllValues("CI07CALLE") <> "" Then
        lngPortalImpCom = .cllValues("CI07NUMIMPCOM")
        lngPortalImpFin = .cllValues("CI07NUMIMPFIN")
        lngPortalParCom = .cllValues("CI07NUMPARCOM")
        lngPortalParFin = .cllValues("CI07NUMPARFIN")
        blnControlPortal = True
      End If

    End If
  End With
  Set objSearchCalles = Nothing
 

End Sub
'Public Function ProcesoDuplicados(strWhereDuplicados As String) As Variant
'   strWherePersDup = strWhereDuplicados
'   vntCodPersDup = 0
'   Load frmDetallePersona
'   Call frmDetallePersona.Show(vbModal)
'   Unload frmDetallePersona
'   Set frmDetallePersona = Nothing
'   ProcesoDuplicados = vntCodPersDup
'End Function

Private Sub cboSSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
'  If (intIndex = 1 Or intIndex = 2 Or intIndex = 12 Or intIndex = 4 Or intIndex = 10 Or intIndex = 11) And KeyCode = 46 Then
'    cboSSDBCombo1(intIndex).Text = ""
'    If (intIndex = 1 Or intIndex = 2 Or intIndex = 12) Then
'      cboSSDBCombo1(intIndex).Value = Null
'    End If
'  End If

End Sub

'Private Sub cmdAcunsa_Click()
'Dim rdoAcunsa As rdoResultset
'Dim strSQL As String
'Dim intI As Integer
'Dim intJ As Integer
'Dim blnAcunsa As Boolean
'Dim blnVerAsegu As Boolean
'Dim blnNoEncontrado As Boolean
'Dim blnBaja As Boolean
'
'If Not Hay_Persona Then Exit Sub
''Buscamos por historia si hay
'Me.MousePointer = vbHourglass
'If Not txtText1(8).Text = "" Then
'    strSQL = "SELECT * FROM CI0300 WHERE CI22NUMHISTORIA=" & txtText1(8)
'    strSQL = strSQL & " AND (CI03FECFINPOLI IS NULL OR CI03FECFINPOLI >= SYSDATE)"
'    Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
'    If Not rdoAcunsa.RowCount = 0 Then
'    blnAcunsa = True
'    Else
'      strSQL = "SELECT * FROM CI0300 WHERE CI22NUMHISTORIA=" & txtText1(8)
'        Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
'        If Not rdoAcunsa.EOF Then blnBaja = True
'    End If
'End If
'
'If blnAcunsa Then
''encontramos el numero de historia 1 vez
'    If objGen.GetRowCount(rdoAcunsa) = 1 Then
'        If rdoAcunsa("CI03CORRIPAGO") = 1 Then
'            intI = MsgBox("El paciente no esta al corriente de pago" & Chr$(13) & "�Desea ver su ficha?" _
'            , vbYesNo)
'            If intI = vbYes Then
'               ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
'               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
'                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
'                    rdoAcunsa.MoveNext
'               Next intJ
'               blnVerAsegu = True
'           End If
'        ElseIf rdoAcunsa("CI03FECFINPOLI") < strFecha_Sistema Then
'        intI = MsgBox("Se ha encontrado una poliza finalizada" & Chr$(13) & "�Desea verla en la pantalla de mantenimiento?" _
'            , vbYesNo)
'            If intI = vbYes Then
'               ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
'               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
'                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
'                    rdoAcunsa.MoveNext
'               Next intJ
'               blnVerAsegu = True
'            End If
'        ElseIf IsNull(rdoAcunsa("CI03EXCLUSION")) And IsNull(rdoAcunsa("CI03OBSERVACIO")) Then
'         intI = MsgBox("El paciente pertenece a Acunsa, sin observaciones ni restrinciones." & Chr$(13) & _
'                "�Desea verlo?", vbYesNo)
'
'             If intI = vbYes Then
'              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
'               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
'                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
'                    rdoAcunsa.MoveNext
'               Next intJ
'               blnVerAsegu = True
'            End If
'        Else
'            intI = MsgBox("El paciente pertenece a Acunsa" & Chr$(13) & "�Desea ver sus restrinciones u observaciones?" _
'            , vbYesNo)
'            If intI = vbYes Then
'              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
'               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
'                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
'                    rdoAcunsa.MoveNext
'               Next intJ
'               blnVerAsegu = True
'            End If
'        End If
''Encontramos varias polizas para ese numero de historia
'    Else
'
'      intI = MsgBox("Se han encontrado " & objGen.GetRowCount(rdoAcunsa) & " polizas para ese n�mero de historia" & Chr$(13) & "�Desea verlas?" _
'            , vbYesNo)
'            If intI = vbYes Then
'              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
'               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
'                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
'                    rdoAcunsa.MoveNext
'               Next intJ
'               blnVerAsegu = True
'             End If
'
'    End If
'    If intI = vbYes Then
'    Call objPipe.PipeSet("PrimerNumPoliza", lngNumPoliza(0))
'    End If
'Else
'If blnBaja Then
'       intI = MsgBox("Este paciente est� dado de baja en Acunsa" & Chr$(13) & _
'                            "�Desea verlo en la pantalla de mantenimiento?", vbYesNo)
'       If intI = vbYes Then
'              ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
'               For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
'                    lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
'                    rdoAcunsa.MoveNext
'               Next intJ
'               blnVerAsegu = True
'               Call objPipe.PipeSet("PrimerNumPoliza", lngNumPoliza(0))
'      End If
'Else
''*******************************************************************
''Caso de que no este el numero de historia miramos si esta el nombre y los apellidos y el DNI.
''*******************************************************************
'    strSQL = "SELECT * FROM CI0300 WHERE CI03NOMBRASEGU='" & txtText1(1) & "'"
'    strSQL = strSQL & " AND CI03APELLIASEGU='" & txtText1(2) & " " & txtText1(3) & "'"
''    If Not IsNull(txtText1(4)) Then
''      strSQL = strSQL & "AND CI03DNI =" & txtText1(4)
''    End If
'    Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
'    If objGen.GetRowCount(rdoAcunsa) > 0 Then
'    'Se han encontrado registros con datos similares a los del paciente pero sin numero de historia
'        intI = MsgBox("No hay ningun registro asociado a este n� de Historia " & Chr$(13) & "pero se han encontrado " & objGen.GetRowCount(rdoAcunsa) & " registros similares" _
'        & Chr$(13) & "�Desea buscarlos y asociar a uno el n� de Historia?", vbYesNo)
'        If intI = vbYes Then
'           ReDim lngNumPoliza(0 To objGen.GetRowCount(rdoAcunsa) - 1)
'           For intJ = 0 To objGen.GetRowCount(rdoAcunsa) - 1
'                lngNumPoliza(intJ) = Val(rdoAcunsa("CI03NUMPOLIZA"))
'                rdoAcunsa.MoveNext
'           Next intJ
'           blnAsocHistoria = True
'           lngNumHistoria = Val(txtText1(8).Text)
'           blnVerAsegu = True
'        End If
'    Else
'        intI = MsgBox("No se ha encontrado en ACUNSA" _
'        & Chr$(13) & "�Desea ver la pantalla de mantenimiento?", vbYesNo)
'        If intI = vbYes Then
'           blnAsocHistoria = True
'           lngNumHistoria = Val(txtText1(8).Text)
'           blnNoEncontrado = True
'           Call objPipe.PipeSet("numeroHistoria", txtText1(8).Text)
'           Call objSecurity.LaunchProcess("CI1010")
'           If objPipe.PipeExist("numeroHistoria") Then objPipe.PipeRemove ("numeroHistoria")
'        End If
'    End If
'
'
'''    If intI = vbYes And blnNoEncontrado = False Then
'''''    Call objPipe.PipeSet("Nombreac", txtText1(1).Text)
'''''    Call objPipe.PipeSet("Primerapac", txtText1(2).Text)
'''''    Call objPipe.PipeSet("Segapac", txtText1(3).Text)
'''''    Call objPipe.PipeSet("PrimerNumPoliza", lngNumPoliza(0))
'''    End If
'End If
'End If
'rdoAcunsa.Close
''Si tenemos que ir a la otra pantalla
'If blnVerAsegu Then
'    blnPerFisica = True
'    Call objPipe.PipeSet("numeroHistoria", txtText1(8).Text)
'    Call objSecurity.LaunchProcess("CI1010")
'    If objPipe.PipeExist("numeroHistoria") Then objPipe.PipeRemove ("numeroHistoria")
'''    If objPipe.PipeExist("nombreac") Then objPipe.PipeRemove ("Nombreac")
'''    If objPipe.PipeExist("Primerapac") Then objPipe.PipeRemove ("Primerapac")
'''    If objPipe.PipeExist("Segapac") Then objPipe.PipeRemove ("Segapac")
'''    If objPipe.PipeExist("Historia") Then objPipe.PipeRemove ("Historia")
'    If objPipe.PipeExist("PrimerNumPoliza") Then objPipe.PipeRemove ("PrimerNumPoliza")
'
'
'End If
'Me.MousePointer = vbDefault
'lngNumHistoria = 0
'blnPerFisica = False
'blnVerAsegu = False
'blnAsocHistoria = False
''lngNumPoliza = ""
'
'End Sub

'Private Sub cmdUniversidad_Click()
'Dim rdoUniver As rdoResultset
'Dim strSQL As String
'Dim intI As Integer
'Dim intJ As Integer
'Dim blnUniver As Boolean
'Dim blnVerPersonal As Boolean
'Dim blnBaja As Boolean
'If Not Hay_Persona Then Exit Sub
''Buscamos por historia si hay
'If Not txtText1(8) = "" Then
'    strSQL = "SELECT * FROM CI2400 WHERE CI22NUMHISTORIA=" & txtText1(8)
'    strSQL = strSQL & " AND (CI24FECFIN IS NULL OR CI24FECFIN >= SYSDATE)"
'    Set rdoUniver = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
'    If Not rdoUniver.RowCount = 0 Then
'    blnUniver = True
'    Else
'        strSQL = "SELECT * FROM CI2400 WHERE CI22NUMHISTORIA=" & txtText1(8)
'        Set rdoUniver = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
'        If Not rdoUniver.EOF Then blnBaja = True
'    End If
'End If
'If blnUniver Then
'    intI = MsgBox("EL paciente pertenece al personal Universitario" & Chr$(13) & _
'    "�Desea verlo en la pantalla de Personal Universitario?", vbYesNo)
'    If intI = vbYes Then
'        blnPerFisica = True
'        ReDim lngNumCodEmpl(0 To objGen.GetRowCount(rdoUniver) - 1)
'        For intJ = 0 To objGen.GetRowCount(rdoUniver) - 1
'             lngNumCodEmpl(intJ) = Val(rdoUniver("CI24CODEMPLEAD"))
'             rdoUniver.MoveNext
'        Next intJ
'        blnPerFisica = True
'        lngNumHistoria = Val(txtText1(8).Text)
'        Call objPipe.PipeSet("Histuni", lngNumHistoria)
'        Call objPipe.PipeSet("PrimerNumEmplead", lngNumCodEmpl(0))
'        Call objSecurity.LaunchProcess("CI1009")
'        objPipe.PipeRemove ("Histuni")
'        objPipe.PipeRemove ("PrimerNumEmplead")
'    End If
'    rdoUniver.Close
'Else
'    If blnBaja Then
'       intI = MsgBox("Este paciente est� dado de baja en Personal Universitario" & Chr$(13) & _
'                            "�Desea verlo en la pantalla de mantenimiento?", vbYesNo)
'       If intI = vbYes Then
'        blnPerFisica = True
'        ReDim lngNumCodEmpl(0 To objGen.GetRowCount(rdoUniver) - 1)
'        For intJ = 0 To objGen.GetRowCount(rdoUniver) - 1
'             lngNumCodEmpl(intJ) = Val(rdoUniver("CI24CODEMPLEAD"))
'             rdoUniver.MoveNext
'        Next intJ
'        blnPerFisica = True
'        lngNumHistoria = Val(txtText1(8).Text)
'        Call objPipe.PipeSet("Histuni", lngNumHistoria)
'        Call objPipe.PipeSet("PrimerNumEmplead", lngNumCodEmpl(0))
'        Call objSecurity.LaunchProcess("CI1009")
'        objPipe.PipeRemove ("Histuni")
'        objPipe.PipeRemove ("PrimerNumEmplead")
'       End If
'    Else
'    strSQL = "SELECT CI24CODEMPLEAD FROM CI2400 WHERE CI24NOMBRBENEF LIKE '%" & txtText1(1) & "%'"
'  strSQL = strSQL & " AND CI24APELLIBENEF LIKE '%" & txtText1(2) & " " & txtText1(3) & "%'"
''  If txtText1(3) <> "" Then
''    strSql = strSql & " AND CI24SEGAPBENEF='" & txtText1(3) & "'"
''  End If
'  Set rdoUniver = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
'  If objGen.GetRowCount(rdoUniver) > 0 Then
'    intI = MsgBox("Se han encontrado " & objGen.GetRowCount(rdoUniver) & " similares, pero no estan " & Chr$(13) & "asociados al n� de Historia " _
'    & "�Desea buscarlos para asociarlos?", vbYesNo)
'    If intI = vbYes Then
'        blnPerFisica = True
'        ReDim lngNumCodEmpl(0 To objGen.GetRowCount(rdoUniver) - 1)
'        For intJ = 0 To objGen.GetRowCount(rdoUniver) - 1
'             lngNumCodEmpl(intJ) = Val(rdoUniver("CI24CODEMPLEAD"))
'             rdoUniver.MoveNext
'        Next intJ
'        blnPerFisica = True
'        lngNumHistoria = Val(txtText1(8).Text)
'        Call objPipe.PipeSet("Histuni", lngNumHistoria)
''''        Call objPipe.PipeSet("NomBenef", txtText1(1).Text)
''''        Call objPipe.PipeSet("ApelBenef", txtText1(2).Text)
''''        Call objPipe.PipeSet("PrimerNumEmplead", lngNumCodEmpl(0))
'        Call objSecurity.LaunchProcess("CI1009")
'        objPipe.PipeRemove ("Histuni")
''''        objPipe.PipeRemove ("NomBenef")
''''        objPipe.PipeRemove ("ApelBenef")
''''        objPipe.PipeRemove ("PrimerNumEmplead")
'    End If
'  Else
'   intI = MsgBox("No se ha encontrado el paciente." & Chr$(13) & "�Desea ver el mantenimiento " & _
'     "de personal universitario?", vbYesNo)
'    If intI = vbYes Then
'    lngNumHistoria = Val(txtText1(8).Text)
'        Call objPipe.PipeSet("Histuni", lngNumHistoria)
'        Call objSecurity.LaunchProcess("CI1009")
'        objPipe.PipeRemove ("Histuni")
'    End If
' End If
' End If
'End If
' blnPerFisica = False
'End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form padre
  Dim objMasterInfo As New clsCWForm
' Form hijo
  Dim objDetailInfo As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  Dim intMode As Integer
' **********************************
' Fin declaraci�n de variables
' **********************************
  
' Se visualiza el formulario de splash
  Call objApp.SplashOn
  Call IdPersona1.BeginControl(objApp, objGen)
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  If blnAddMode Then
    intMode = cwModeSingleAddRest
  Else
    intMode = cwModeSingleEmpty
  End If
  If lngUserCode > 0 Then
    intMode = cwModeSingleEdit
  End If
  Call objWinInfo.WinCreateInfo(intMode, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Agenda"
    .cwMOD = "Mantenimiento de Personas F�sicas"
    .cwDAT = "18-07-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener los empleados de la CUN"
    
    .cwUPD = "18-07-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = ""
  End With
  
' Declaraci�n de las caracter�sticas del form padre
  With objMasterInfo
    .strName = "PersF�sica"

' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    .blnAskPrimary = False
    If lngUserCode > 0 Then
      .strInitialWhere = "CI21CODPERSONA=" & lngUserCode
    End If
    .blnHasMaint = True
' Reports generados por el form
      Call .objPrinter.Add("CI0009", "Listado 1 Relaci�n de Personas F�sicas")
    
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable

' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Tabla de Personas F�sicas")
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22DNI", "D.N.I", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NUMHISTORIA", "N� Historia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI22FECNACIM", "Fecha de Nacimiento", cwDate)
    Call .FormAddFilterWhere(strKey, "CI22CODPERSONA_REA", "C�digo persona Real", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI22INDPROVISI", "Indicador Provisional", cwBoolean)
    Call .FormAddFilterWhere(strKey, "CI30CODSEXO", "Sexo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI14CODESTCIVI", "Estado Civil", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI28CODRELUDN", "Relaci�n UDN", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI19CODPAIS", "Pa�s", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI22DESPROVI", "Provincia", cwString)
    Call .FormAddFilterWhere(strKey, "CI22DESLOCALID", "Localidad", cwString)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA_RFA", "Persona Responsable Familiar", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA_REC", "Persona Responsable Econ�mico", cwNumeric)
    
  End With
  
' Declaraci�n de las caracter�sticas del form hijo
  With objDetailInfo
    .strName = "Direcci�n"
 ' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
 ' Asignaci�n del contenedor(frame)padre del form
    Set .objFatherContainer = fraFrame1(0)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(1)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI1000"
    .blnAskPrimary = False
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI10INDDIRPRINC", cwAscending)
    Call .FormAddOrderField("CI10NUMDIRECCI", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI21CODPERSONA", txtText1(0))
  
  End With
   
  
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
    
' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
' Se obtiene informaci�n de las caracter�sticas de
' los controles del formulario
    Call .FormCreateInfo(objMasterInfo)
        
    
' Valores con los que se cargara la DbCombo
    .CtrlGetInfo(cboSSDBCombo1(0)).strSQL = "SELECT CI30CODSEXO, CI30DESSEXO  FROM " & objEnv.GetValue("Database") & "CI3000 ORDER BY CI30DESSEXO"
    .CtrlGetInfo(cboSSDBCombo1(1)).strSQL = "SELECT CI14CODESTCIVI, CI14DESESTCIVI FROM " & objEnv.GetValue("Database") & "CI1400 ORDER BY CI14DESESTCIVI"
    .CtrlGetInfo(cboSSDBCombo1(2)).strSQL = "SELECT CI28CODRELUDN, CI28DESRELUDN FROM " & objEnv.GetValue("Database") & "CI2800 ORDER BY CI28DESRELUDN"
    .CtrlGetInfo(cboSSDBCombo1(3)).strSQL = "SELECT CI19CODPAIS, CI19DESPAIS FROM " & objEnv.GetValue("Database") & "CI1900 ORDER BY CI19DESPAIS"
    .CtrlGetInfo(cboSSDBCombo1(4)).strSQL = "SELECT CI26CODPROVI,CI26DESPROVI  FROM " & objEnv.GetValue("Database") & "CI2600 ORDER BY CI26DESPROVI"
    .CtrlGetInfo(cboSSDBCombo1(6)).strSQL = "SELECT CI25CODPROFESI, CI25DESPROFESI FROM " & objEnv.GetValue("Database") & "CI2500 ORDER BY CI25DESPROFESI"
    .CtrlGetInfo(cboSSDBCombo1(7)).strSQL = "SELECT CI35CODTIPVINC, CI35DESTIPVINC FROM " & objEnv.GetValue("Database") & "CI3500 ORDER BY CI35DESTIPVINC"
    .CtrlGetInfo(cboSSDBCombo1(11)).strSQL = "SELECT CI19CODPAIS, CI19DESPAIS FROM " & objEnv.GetValue("Database") & "CI1900 ORDER BY CI19DESPAIS"
    .CtrlGetInfo(cboSSDBCombo1(10)).strSQL = "SELECT CI26CODPROVI, CI26DESPROVI FROM " & objEnv.GetValue("Database") & "CI2600 ORDER BY CI26DESPROVI"
    .CtrlGetInfo(cboSSDBCombo1(12)).strSQL = "SELECT CI34CODTRATAMI, CI34DESTRATAMI FROM " & objEnv.GetValue("Database") & "CI3400 ORDER BY CI34DESTRATAMI"
    .CtrlGetInfo(cboSSDBCombo1(13)).strSQL = "SELECT CI26CODPROVI,CI26DESPROVI  FROM " & objEnv.GetValue("Database") & "CI2600 ORDER BY CI26DESPROVI"
    .CtrlGetInfo(cboSSDBCombo1(9)).strSQL = "SELECT CI19CODPAIS, CI19DESPAIS FROM " & objEnv.GetValue("Database") & "CI1900 ORDER BY CI19DESPAIS"
  
' Eliminamos campo del grid
    .CtrlGetInfo(txtText1(27)).blnInGrid = False
    .CtrlGetInfo(txtText1(26)).blnInGrid = False
    .CtrlGetInfo(txtText1(28)).blnInGrid = False
    .CtrlGetInfo(txtText1(33)).blnInGrid = False
    .CtrlGetInfo(txtText1(18)).blnInGrid = False
    .CtrlGetInfo(txtText1(34)).blnInGrid = False
'    .CtrlGetInfo(dtcDateCombo1(11)).blnInGrid = False
'    .CtrlGetInfo(dtcDateCombo1(10)).blnInGrid = False
    .CtrlGetInfo(dtcDateCombo1(3)).blnInGrid = False
'    .CtrlGetInfo(dtcDateCombo1(4)).blnInGrid = False
    
  ' Campos con lista de valores
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    .CtrlGetInfo(txtText1(28)).blnForeign = True
    .CtrlGetInfo(txtText1(9)).blnForeign = True
    .CtrlGetInfo(txtText1(30)).blnForeign = True
    .CtrlGetInfo(txtText1(31)).blnForeign = True
    .CtrlGetInfo(txtText1(19)).blnForeign = True
    .CtrlGetInfo(txtText1(12)).blnForeign = True
    .CtrlGetInfo(txtText1(24)).blnForeign = True
    
'   .CtrlGetInfo(IdPersona1).blnNegotiated = False

' Propiedades del campo clave para asignaci�n autom�tica
    .CtrlGetInfo(txtText1(0)).blnValidate = False
    .CtrlGetInfo(txtText1(0)).blnInGrid = False

    .CtrlGetInfo(txtText1(18)).blnValidate = False
    .CtrlGetInfo(txtText1(18)).blnInGrid = False

'Campos del form Persona que son obligatorios
    .CtrlGetInfo(txtText1(2)).blnMandatory = True
    '.CtrlGetInfo(txtText1(3)).blnMandatory = True
'    .CtrlGetInfo(cboSSDBCombo1(1)).blnMandatory = True
    .CtrlGetInfo(cboSSDBCombo1(3)).blnMandatory = True
    '.CtrlGetInfo(dtcDateCombo1(0)).blnMandatory = True

'campos de b�squeda
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
'Campos de solo lectura ( N�mero de Historia )
    .CtrlGetInfo(txtText1(8)).blnReadOnly = True

' Mascara para C�digos Postales
    .CtrlGetInfo(txtText1(5)).intMask = cwMaskInteger

' Definici�n de controles relacionados entre s�
    'Pais Nacimiento
''    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(3)), "CI19CODPAIS", "SELECT CI19CODPAIS, CI19DESPAIS FROM CI1900 WHERE CI19CODPAIS = ?")
''    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(3)), txtText1(10), "CI19DESPAIS")
    'Responsable Familiar
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(30)), "CI21CODPERSONA", "SELECT CI21CODPERSONA, CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(30)), txtText1(16), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(30)), txtText1(35), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(30)), txtText1(36), "CI22SEGAPEL")
    'Responsable Econ�mico
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(31)), "CI21CODPERSONA", "SELECT CI21CODPERSONA, CI21NOMPERSONA FROM CI2101J WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(31)), txtText1(29), "CI21NOMPERSONA")
           
    'Provincia Nacimiento
    'Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(4)), "CI10DESLOCALID", "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 WHERE CI26CODPROVI = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(4)), txtText1(11), "CI26DESPROVI")
    .CtrlGetInfo(txtText1(11)).blnReadOnly = True
    
   'Localidad Direcci�n
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "CI10DESLOCALID", "SELECT CI17CODMUNICIP, CI16CODLOCALID, CI16DESLOCALID FROM CI1600 WHERE CI16DESLOCALID = UPPER(?)")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(28), "CI17CODMUNICIP")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(33), "CI16CODLOCALID")
    
    .CtrlGetInfo(txtText1(12)).blnReadOnly = False

    'Pais (Direcci�n)
'    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(11)), "CI19CODPAIS", "SELECT CI19CODPAIS, CI19DESPAIS FROM CI1900 WHERE CI19CODPAIS = ?")
'    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(11)), txtText1(22), "CI19DESPAIS")
           
    'Provincia (Direcci�n)
    'Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(10)), "CI26CODPROVI", "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 WHERE CI26CODPROVI = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(10)), txtText1(23), "CI26DESPROVI")
    .CtrlGetInfo(txtText1(23)).blnReadOnly = True
    .CtrlGetInfo(txtText1(24)).blnReadOnly = False

    .CtrlGetInfo(txtText1(16)).blnReadOnly = True
    .CtrlGetInfo(txtText1(29)).blnReadOnly = True

    'Campos Responsable Familiar IBM, s�lo lectura.
    .CtrlGetInfo(txtText1(40)).blnReadOnly = True
    .CtrlGetInfo(txtText1(41)).blnReadOnly = True
    .CtrlGetInfo(txtText1(42)).blnReadOnly = True
    .CtrlGetInfo(txtText1(43)).blnReadOnly = True
    .CtrlGetInfo(txtText1(44)).blnReadOnly = True
    .CtrlGetInfo(txtText1(45)).blnReadOnly = True
    .CtrlGetInfo(cboSSDBCombo1(9)).blnReadOnly = True
    .CtrlGetInfo(cboSSDBCombo1(13)).blnReadOnly = True

   'Fecha actual por defecto para Fecha Inicio Vigencia Direcci�n
    .CtrlGetInfo(dtcDateCombo1(2)).vntDefaultValue = Format(objGen.GetDBDateTime, "DD/MM/YYYY")

    'Mascara num�rica para C�digo Provincia del N� S.S.
    .CtrlGetInfo(txtText1(6)).intMask = cwMaskInteger

    'Mascara num�rica para N� S.S.
    .CtrlGetInfo(txtText1(47)).intMask = cwMaskInteger

    'Mascara num�rica para D�gito Control del N� S.S.
    .CtrlGetInfo(txtText1(46)).intMask = cwMaskInteger

' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    Call .WinStabilize
    
  End With
  
' Se oculta el formulario de splash
  Call objApp.SplashOff
  
  'Invisible bot�n de B�squeda
  IdPersona1.blnSearchButton = False
  Screen.MousePointer = vbDefault    'Cuando se viene de Citar por Huecos

End Sub




Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  Dim strTabla As String
  Dim strWhereCond As String
  Dim strOrderCond As String
  Dim blnObtenerCalles As Boolean
  
  If strFormName = "Direcci�n" Then
    
    If cboSSDBCombo1(10).Value = "" Then
       Call objError.SetError(cwCodeMsg, "No se ha seleccionado ninguna Provincia")
       Call objError.Raise
       Exit Sub
    End If
     
  If strCtrl = "txtText1(5)" Then
    Set objSearch = New clsCWSearch
    If Len(Trim(txtText1(5))) > 2 Then
      strTabla = "CI0700"
      strWhereCond = "WHERE CI07CODPOSTAL LIKE '" & Trim(txtText1(5)) & "%'"
      strOrderCond = "ORDER BY CI07CALLE"
    Else
      strTabla = "CI0701"
      strWhereCond = "WHERE CI26CODPROVI =" & cboSSDBCombo1(10).Value
      strOrderCond = "ORDER BY CI07DESPOBLA"
    End If
    With objSearch
      .strTable = strTabla
      .strWhere = strWhereCond
      .strOrder = strOrderCond
      
      Set objField = .AddField("CI07CODPOSTAL")
      objField.strSmallDesc = "C�DIGO POSTAL"
      
      Set objField = .AddField("CI07TIPPOBLA")
      objField.blnInGrid = False
      
      
      Set objField = .AddField("CI26CODPROVI")
      objField.blnInGrid = False
      
      Set objField = .AddField("CI07DESPOBLA")
      objField.strSmallDesc = "POBLACI�N                     "
     
      If strTabla = "CI0700" Then
        
        Set objField = .AddField("CI07CALLE")
        objField.strSmallDesc = "CALLE                       "
     
        Set objField = .AddField("CI07NUMIMPCOM")
        objField.blnInGrid = False
      
        Set objField = .AddField("CI07NUMIMPFIN")
        objField.blnInGrid = False
      
        Set objField = .AddField("CI07NUMPARCOM")
        objField.blnInGrid = False
      
        Set objField = .AddField("CI07NUMPARFIN")
        objField.blnInGrid = False
        
      End If
      If .ViewSelect Then
        If .cllValues("CI07CODPOSTAL") = " " Or Val(.cllValues("CI07CODPOSTAL")) = 0 Then
          If Val(.cllValues("CI07CODPOSTAL")) = 0 Then
            strTipoPoblacion = .cllValues("CI07TIPPOBLA")
          End If
          blnObtenerCalles = True
        Else
          Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
          Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
          blnObtenerCalles = False
        End If
        If strTabla = "CI0700" Then
          Call objWinInfo.CtrlSet(txtText1(19), .cllValues("CI07CALLE"))
          If .cllValues("CI07CALLE") <> "" Then
            lngPortalImpCom = .cllValues("CI07NUMIMPCOM")
            lngPortalImpFin = .cllValues("CI07NUMIMPFIN")
            lngPortalParCom = .cllValues("CI07NUMPARCOM")
            lngPortalParFin = .cllValues("CI07NUMPARFIN")
            blnControlPortal = True
          End If
        End If
      End If
    End With
    
    Set objSearch = Nothing
    If blnObtenerCalles Then
      Call LocalizarCalles(cboSSDBCombo1(10).Value, strTipoPoblacion)
    End If
  End If
    
    
 'Lista de Valores por Localidades
   If strCtrl = "txtText1(24)" Then
    Set objSearch = New clsCWSearch
      strTabla = "CI0701"
      strWhereCond = "WHERE CI26CODPROVI =" & cboSSDBCombo1(10).Value & " AND CI07DESPOBLA LIKE '%" & UCase(Trim(txtText1(24))) & "%'"
      strOrderCond = "ORDER BY CI07DESPOBLA"
    With objSearch
      .strTable = strTabla
      .strWhere = strWhereCond
      .strOrder = strOrderCond
      
      Set objField = .AddField("CI07CODPOSTAL")
      objField.strSmallDesc = "C�DIGO POSTAL"
      
      Set objField = .AddField("CI07TIPPOBLA")
      objField.blnInGrid = False
      
      
      Set objField = .AddField("CI26CODPROVI")
      objField.blnInGrid = False
      
      Set objField = .AddField("CI07DESPOBLA")
      objField.strSmallDesc = "POBLACI�N                     "
     
      If .ViewSelect Then
        If .cllValues("CI07CODPOSTAL") = " " Or Val(.cllValues("CI07CODPOSTAL")) = 0 Then
          If Val(.cllValues("CI07CODPOSTAL")) = 0 Then
            strTipoPoblacion = "0"
          Else
            strTipoPoblacion = .cllValues("CI07TIPPOBLA")
          End If
          blnObtenerCalles = True
        Else
          Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
          Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
          blnObtenerCalles = False
        End If
      End If
      
    End With
    
    Set objSearch = Nothing
    If blnObtenerCalles Then
      Call LocalizarCalles(cboSSDBCombo1(10).Value, strTipoPoblacion)
    End If
  End If

    

 'Lista de Valores de Calles
  If strCtrl = "txtText1(19)" Then
    Set objSearch = New clsCWSearch
     With objSearch
      .strTable = "CI0700"
      .strWhere = "WHERE CI26CODPROVI=" & cboSSDBCombo1(10).Value & " AND CI07TIPPOBLA != '9' AND CI07CALLE LIKE '%" & UCase(Trim(txtText1(19))) & "%'"
      .strOrder = "ORDER BY CI07CALLE"
      
      Set objField = .AddField("CI07CODPOSTAL")
      objField.strSmallDesc = "C�DIGO POSTAL"
      
      Set objField = .AddField("CI07TIPPOBLA")
      objField.blnInGrid = False
      
      
      Set objField = .AddField("CI26CODPROVI")
      objField.blnInGrid = False
      
      Set objField = .AddField("CI07DESPOBLA")
      objField.strSmallDesc = "POBLACI�N           "
      
      Set objField = .AddField("CI07CALLE")
      objField.strSmallDesc = "CALLE                           "
           
      Set objField = .AddField("CI07NUMIMPCOM")
      objField.strSmallDesc = "COMIENZO IMPAR"
      
      Set objField = .AddField("CI07NUMIMPFIN")
      objField.strSmallDesc = "FIN IMPAR"
      
      Set objField = .AddField("CI07NUMPARCOM")
      objField.strSmallDesc = "COMIENZO PAR"
      
      Set objField = .AddField("CI07NUMPARFIN")
      objField.strSmallDesc = "FIN PAR"
        
                                  
      
      If .ViewSelect Then
        Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
        strTipoPoblacion = .cllValues("CI07TIPPOBLA")
        Call objWinInfo.CtrlSet(txtText1(5), .cllValues("CI07CODPOSTAL"))
        Call objWinInfo.CtrlSet(txtText1(24), .cllValues("CI07DESPOBLA"))
        Call objWinInfo.CtrlSet(txtText1(19), .cllValues("CI07CALLE"))
        lngPortalImpCom = .cllValues("CI07NUMIMPCOM")
        lngPortalImpFin = .cllValues("CI07NUMIMPFIN")
        lngPortalParCom = .cllValues("CI07NUMPARCOM")
        lngPortalParFin = .cllValues("CI07NUMPARFIN")
        blnControlPortal = True
      End If
        
    End With
    
    Set objSearch = Nothing
    If blnObtenerCalles Then
      Call LocalizarCalles(cboSSDBCombo1(10).Value, strTipoPoblacion)
    End If
  End If

 End If
    
  
  
  
  '*******pERSONA fISICA***********
  
  
  If strFormName = "PersF�sica" Then
    
     
     If strCtrl = "txtText1(9)" Then
      Set objSearch = New clsCWSearch
      With objSearch
       .strTable = "CI2200"
       .strWhere = "WHERE CI21CODPERSONA_REA=0 OR CI21CODPERSONA_REA IS NULL"
       .strOrder = ""
      
       Set objField = .AddField("CI21CODPERSONA")
       objField.strSmallDesc = "C�DIGO PERSONA"
       Set objField = .AddField("CI22NOMBRE")
       objField.strSmallDesc = "NOMBRE"
       Set objField = .AddField("CI22PRIAPEL")
       objField.strSmallDesc = "PRIMER APELLIDO"
       Set objField = .AddField("CI22SEGAPEL")
       objField.strSmallDesc = "SEGUNDO APELLIDO"
       Set objField = .AddField("CI22DNI")
       objField.strSmallDesc = "DNI"
       Set objField = .AddField("CI22FECNACIM")
       objField.strSmallDesc = "FECHA NACIMIENTO"

       If .Search Then
         Call objWinInfo.CtrlSet(txtText1(9), .cllValues("CI21CODPERSONA"))
       End If
      End With
    End If
     
  'maria1
    If strCtrl = "txtText1(30)" Then
    IdPersona2.blnBus = True
   Call IdPersona2.Buscar
   If Val(IdPersona2.Text) <> 0 Then
       'Call objWinInfo.FormChangeActive(tabTab1(2), False, True)
        'Call objApp.objActiveWin.DataOpen
        txtText1_GotFocus (30)
        txtText1(30).Text = IdPersona2.Text
        txtText1(16).Text = IdPersona2.Nombre
        txtText1(35).Text = IdPersona2.Apellido1
        txtText1(36).Text = IdPersona2.Apellido2
    IdPersona2.blnBus = False
    End If
    'maria2
'SE ELIMINA ESE BUSCADOR Y SE PONE EL GENERAL
'      objWinInfo.CtrlGetInfo(txtText1(16)).blnReadOnly = False
'      objWinInfo.CtrlGetInfo(txtText1(35)).blnReadOnly = False
'      objWinInfo.CtrlGetInfo(txtText1(36)).blnReadOnly = False
'      Call objWinInfo.WinPrepareScr
'      Set objSearch = New clsCWSearch
'      With objSearch
'        .strTable = "CI2200"
'       .strWhere = ""
'       .strOrder = ""
'
'       Set objField = .AddField("CI21CODPERSONA")
'       objField.strSmallDesc = "C�DIGO PERSONA"
'       Set objField = .AddField("CI22NOMBRE")
'       objField.strSmallDesc = "NOMBRE        "
'       Set objField = .AddField("CI22PRIAPEL")
'       objField.strSmallDesc = "PRIMER APELLIDO   "
'       Set objField = .AddField("CI22SEGAPEL")
'       objField.strSmallDesc = "SEGUNDO APELLIDO  "
'       Set objField = .AddField("CI22DNI")
'       objField.strSmallDesc = "DNI        "
'       Set objField = .AddField("CI22FECNACIM")
'       objField.strSmallDesc = "FECHA NACIMIENTO"
'
'       If .Search Then
'         Call objWinInfo.CtrlSet(txtText1(30), .cllValues("CI21CODPERSONA"))
'         Call objWinInfo.CtrlSet(txtText1(16), .cllValues("CI22NOMBRE"))
'         Call objWinInfo.CtrlSet(txtText1(35), .cllValues("CI22PRIAPEL"))
'         Call objWinInfo.CtrlSet(txtText1(36), .cllValues("CI22SEGAPEL"))
'       End If
'      End With
'      objWinInfo.CtrlGetInfo(txtText1(16)).blnReadOnly = True
'      objWinInfo.CtrlGetInfo(txtText1(35)).blnReadOnly = True
'      objWinInfo.CtrlGetInfo(txtText1(36)).blnReadOnly = True
'      Call objWinInfo.WinPrepareScr
    
    End If
'maria1
    If strCtrl = "txtText1(31)" Then
    IdPersona2.blnBus = True
    Call IdPersona2.Buscar
    If Val(IdPersona2.Text) <> 0 Then
        'Call objWinInfo.FormChangeActive(tabTab1(2), False, True)
        'Call objApp.objActiveWin.DataOpen
        txtText1_GotFocus (31)
        txtText1(31).Text = IdPersona2.Text
        txtText1(29).Text = IdPersona2.Nombre & " " & IdPersona2.Apellido1 & " " & IdPersona2.Apellido2
        
        IdPersona2.blnBus = True
    End If
'maria2
'SE ELIMINA EL BUSCARDOR
'      objWinInfo.CtrlGetInfo(txtText1(29)).blnReadOnly = False
'      Call objWinInfo.WinPrepareScr
'
'      Set objSearch = New clsCWSearch
'
'      With objSearch
'       .strTable = "CI2101J"
'       .strWhere = ""
'       .strOrder = ""
'
'       Set objField = .AddField("CI21CODPERSONA")
'       objField.strSmallDesc = "C�DIGO PERSONA                "
'       Set objField = .AddField("CI21NOMPERSONA")
'       objField.strSmallDesc = "NOMBRE                        "
'
'       If .Search Then
'         Call objWinInfo.CtrlSet(txtText1(31), .cllValues("CI21CODPERSONA"))
'         Call objWinInfo.CtrlSet(txtText1(29), .cllValues("CI21NOMPERSONA"))
'       End If
'      End With
'      objWinInfo.CtrlGetInfo(txtText1(29)).blnReadOnly = True
'      Call objWinInfo.WinPrepareScr
    End If
   
    If strCtrl = "txtText1(12)" Then
      If cboSSDBCombo1(4).Value = "" Then
        Call objError.SetError(cwCodeMsg, "No se ha seleccionado ninguna Provincia")
        Call objError.Raise
        Exit Sub
      End If
      Set objSearch = New clsCWSearch
      
      With objSearch
       .strTable = "CI1601J"
       .strWhere = "WHERE CI26CODPROVI=" & cboSSDBCombo1(4).Value & " AND ( CI16DESLOCALID LIKE '%" & UCase(Trim(txtText1(12).Text)) & "%' OR CI17DESMUNICIP LIKE '%" & UCase(Trim(txtText1(12).Text)) & "%' ) "
       .strOrder = "ORDER BY CI16DESLOCALID"
       
       Set objField = .AddField("CI17CODMUNICIP")
       objField.blnInGrid = False
       Set objField = .AddField("CI16CODLOCALID")
       objField.blnInGrid = False
       Set objField = .AddField("CI17DESMUNICIP")
       objField.strSmallDesc = "MUNICIPIO               "
       Set objField = .AddField("CI16DESLOCALID")
       objField.strSmallDesc = "LOCALIDAD               "
      

       If .ViewSelect Then
         Call objWinInfo.CtrlSet(txtText1(32), .cllValues("CI17CODMUNICIP"))
         Call objWinInfo.CtrlSet(txtText1(34), .cllValues("CI16CODLOCALID"))
         Call objWinInfo.CtrlSet(txtText1(12), .cllValues("CI16DESLOCALID"))
       End If
      
      End With

     End If
   End If
 
      
   
 Set objSearch = Nothing
  
 

End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
   If strFormName = "PersF�sica" Then
     If Me.ActiveControl.DataField = "CI25CODPROFESI" Then
      Call objSecurity.LaunchProcess("CI1012")
      Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(6)))
     End If
   
    If Me.ActiveControl.DataField = "CI35CODTIPVINC" Then
     Call objSecurity.LaunchProcess("CI1014")
     Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(7)))
    End If
    If Me.ActiveControl.DataField = "CI28CODRELUDN" Then
     Call objSecurity.LaunchProcess("CI1037")
     Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(2)))
    
    End If
   
   End If
   
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  If strFormName = "Direcci�n" Then
    If intNewStatus = cwModeSingleAddRest Then
'      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = False
      objWinInfo.WinPrepareScr
      DoEvents
    End If
  End If
  If strFormName = "PersF�sica" And intNewStatus <> cwModeSingleOpen Then
    With objWinInfo
      'Check Provisional
      If objWinInfo.CtrlGet(chkCheck1(1)) = 1 Then
        .CtrlGetInfo(txtText1(2)).blnMandatory = False
        '.CtrlGetInfo(txtText1(3)).blnMandatory = False
        .CtrlGetInfo(cboSSDBCombo1(1)).blnMandatory = False
        .CtrlGetInfo(cboSSDBCombo1(3)).blnMandatory = False
        '.CtrlGetInfo(dtcDateCombo1(0)).blnMandatory = False
      Else
        .CtrlGetInfo(txtText1(2)).blnMandatory = True
        '.CtrlGetInfo(txtText1(3)).blnMandatory = True
'        .CtrlGetInfo(cboSSDBCombo1(1)).blnMandatory = True
        .CtrlGetInfo(cboSSDBCombo1(3)).blnMandatory = True
        '.CtrlGetInfo(dtcDateCombo1(0)).blnMandatory = True
      End If
      Call .FormChangeColor(.objWinActiveForm)
      Call .WinPrepareScr
    DoEvents
    End With
  End If
  
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'******************************************************************
' Control del form Direcciones
'******************************************************************
  If strFormName = "Direcci�n" Then
    If objWinInfo.CtrlGet(chkCheck1(0)) = 1 Then
'      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = True
    Else
      objWinInfo.CtrlGetInfo(chkCheck1(0)).blnReadOnly = False
    
    End If
''    If cboSSDBCombo1(11).Text = "45" Then
''      cboSSDBCombo1(10).Enabled = True
''    Else
''      cboSSDBCombo1(10).Enabled = False
''      cboSSDBCombo1(10).Text = ""
''    End If
    
    DoEvents
    objWinInfo.WinPrepareScr
'    cmdVer.Enabled = True
  End If
  If strFormName = "PersF�sica" Then
    
    'Campo C�d. Profesi�n con valor, visualizarlo
    If cboSSDBCombo1(5).Value <> "" Then
        cboSSDBCombo1(6).Value = cboSSDBCombo1(5).Value
    'Campo Profesi�n IBM con valor, visualizarlo
    ElseIf txtText1(38).Text <> "" Then
       cboSSDBCombo1(6).Text = txtText1(38).Text
    Else
       cboSSDBCombo1(6).Text = ""
    End If
    
    'Campo C�d.Relaci�n Universidad con valor, visualizarlo
    If cboSSDBCombo1(8).Value <> "" Then
        cboSSDBCombo1(2).Value = cboSSDBCombo1(8).Value
    'Campo Relaci�n Universidad IBM con valor, visualizarlo
    ElseIf txtText1(39).Text <> "" Then
       cboSSDBCombo1(2).Text = txtText1(39).Text
    Else
       cboSSDBCombo1(2).Text = ""
    End If
    
    'Responsable familiar
    If txtText1(30).Text <> "" Then
       txtText1(40).Text = ""
       txtText1(41).Text = ""
       txtText1(42).Text = ""
       txtText1(43).Text = ""
       txtText1(44).Text = ""
       txtText1(45).Text = ""
       cboSSDBCombo1(9).Text = ""
       cboSSDBCombo1(13).Text = ""
    End If
    
    'C�digo Provincia del N� S.S.
    txtText1(6).Text = Left(txtText1(48).Text, 2)

    'N� S.S.
    txtText1(47).Text = Mid(txtText1(48).Text, 3, 8)

    'D�gito Control del N� S.S.
    If Len(txtText1(48).Text) > 10 Then
       txtText1(46).Text = Right(txtText1(48).Text, 2)
    Else
       txtText1(46).Text = ""
    End If
        
    'Estado Persona
    If Val(txtText1(49).Text) = 2 Then
        'Marcado
        chkCheck1(2).Value = 1
    Else
        'No marcado
        chkCheck1(2).Value = 0
    End If
 
    'LAS***************
 'Si tiene procesos/asistencias colgando, deshabilitar el check box 3
 'MsgBox blnPrAsisColgando
 If blnPrAsisColgando Then
 objWinInfo.CtrlGetInfo(chkCheck1(3)).blnReadOnly = True
 Else
 objWinInfo.CtrlGetInfo(chkCheck1(1)).blnReadOnly = False
 objWinInfo.CtrlGetInfo(chkCheck1(3)).blnReadOnly = False
 End If
  
  Call objWinInfo.FormChangeActive(tabTab1(0), False, True)
 'LAS***************
  
  End If

End Sub

Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim strWhereDup As String
  Dim rdoDuplicados As rdoResultset
  Dim intRespuesta As Integer
  Dim vntCodPersona As Variant
  Dim strSQL As String
  Dim strLetra As String
  Dim vntDigControl
  Dim strNumSS As String
  
  Dim Nombre
  Dim priapel
  Dim segapel
  
  If strFormName = "PersF�sica" Then
  

'********************************************LAS 4/2/99
   'Control de Personas Duplicadas
'    If txtText1(4) <> "" Then
'    'DNI con valor
'        If IsDate(dtcDateCombo1(0).Date) Then
'        'Fecha Nacimiento con valor
'              strWhereDup = "((( CI22NOMBRE=" & objGen.ValueToSQL(UCase(Trim(txtText1(1).Text)), cwString) _
'               & " OR CI22NOMBRE=" & objGen.ValueToSQL(LCase(Trim(txtText1(1).Text)), cwString) & ") AND " _
'               & "( CI22PRIAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(2).Text)), cwString) _
'               & " OR CI22PRIAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(2).Text)), cwString) & ") AND " _
'               & "( CI22SEGAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(3).Text)), cwString) _
'               & " OR CI22SEGAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(3).Text)), cwString) & ") AND " _
'               & " CI22FECNACIM =" & objGen.ValueToSQL(dtcDateCombo1(0).Date, cwDate) & " ) " _
'               & " OR CI22DNI='" & Trim(txtText1(4).Text) & "')"
'        Else
'        'Fecha Nacimiento sin valor
'           strWhereDup = "(( CI22NOMBRE=" & objGen.ValueToSQL(UCase(Trim(txtText1(1).Text)), cwString) _
'           & " OR CI22NOMBRE=" & objGen.ValueToSQL(LCase(Trim(txtText1(1).Text)), cwString) & ") AND " _
'           & "( CI22PRIAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(2).Text)), cwString) _
'           & " OR CI22PRIAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(2).Text)), cwString) & ") AND " _
'           & "( CI22SEGAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(3).Text)), cwString) _
'           & " OR CI22SEGAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(3).Text)), cwString) & " ) " _
'           & " OR CI22DNI='" & Trim(txtText1(4).Text) & "')"
'        End If
'    Else
'    'DNI sin valor
'       If IsDate(dtcDateCombo1(0).Date) Then
'       'Fecha Nacimiento con valor
'              strWhereDup = "(( CI22NOMBRE=" & objGen.ValueToSQL(UCase(Trim(txtText1(1).Text)), cwString) _
'               & " OR CI22NOMBRE=" & objGen.ValueToSQL(LCase(Trim(txtText1(1).Text)), cwString) & ") AND " _
'               & "( CI22PRIAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(2).Text)), cwString) _
'               & " OR CI22PRIAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(2).Text)), cwString) & ") AND " _
'               & "( CI22SEGAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(3).Text)), cwString) _
'               & " OR CI22SEGAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(3).Text)), cwString) & ") AND " _
'               & " CI22FECNACIM =" & objGen.ValueToSQL(dtcDateCombo1(0).Date, cwDate) & ")"
'       Else
'       'Fecha Nacimiento sin valor
'              strWhereDup = "(( CI22NOMBRE=" & objGen.ValueToSQL(UCase(Trim(txtText1(1).Text)), cwString) _
'               & " OR CI22NOMBRE=" & objGen.ValueToSQL(LCase(Trim(txtText1(1).Text)), cwString) & ") AND " _
'               & "( CI22PRIAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(2).Text)), cwString) _
'               & " OR CI22PRIAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(2).Text)), cwString) & ") AND " _
'               & "( CI22SEGAPEL =" & objGen.ValueToSQL(UCase(Trim(txtText1(3).Text)), cwString) _
'               & " OR CI22SEGAPEL =" & objGen.ValueToSQL(LCase(Trim(txtText1(3).Text)), cwString) & " ))"
'       End If
'    End If


    Nombre = Alf(Trim(txtText1(1).Text))
    priapel = Alf(Trim(txtText1(2).Text))
    segapel = Alf(Trim(txtText1(3).Text))


       If txtText1(4) <> "" Then
    'DNI con valor
        If IsDate(dtcDateCombo1(0).Date) Then
        'Fecha Nacimiento con valor
              strWhereDup = "((( CI22NOMBREALF=" & objGen.ValueToSQL(Nombre, cwString) & ") AND " _
               & "( CI22PRIAPELALF =" & objGen.ValueToSQL(priapel, cwString) & ") AND " _
               & "( CI22SEGAPELALF =" & objGen.ValueToSQL(segapel, cwString) & ") AND " _
               & " CI22FECNACIM =" & objGen.ValueToSQL(dtcDateCombo1(0).Date, cwDate) & " ) " _
               & " OR CI22DNI='" & Trim(txtText1(4).Text) & "')"
        Else
        'Fecha Nacimiento sin valor
           strWhereDup = "(( CI22NOMBREALF=" & objGen.ValueToSQL(Nombre, cwString) & ") AND " _
           & "( CI22PRIAPELALF =" & objGen.ValueToSQL(priapel, cwString) & ") AND " _
           & "( CI22SEGAPELALF =" & objGen.ValueToSQL(segapel, cwString) & " ) " _
           & " OR CI22DNI='" & Trim(txtText1(4).Text) & "')"
        End If
    Else
    'DNI sin valor
       If IsDate(dtcDateCombo1(0).Date) Then
       'Fecha Nacimiento con valor
              strWhereDup = "(( CI22NOMBREALF=" & objGen.ValueToSQL(Nombre, cwString) & ") AND " _
               & "( CI22PRIAPELALF =" & objGen.ValueToSQL(priapel, cwString) & ") AND " _
               & "( CI22SEGAPELALF =" & objGen.ValueToSQL(segapel, cwString) & ") AND " _
               & " CI22FECNACIM =" & objGen.ValueToSQL(dtcDateCombo1(0).Date, cwDate) & ")"
       Else
       'Fecha Nacimiento sin valor
              strWhereDup = "(( CI22NOMBREALF=" & objGen.ValueToSQL(Nombre, cwString) & ") AND " _
               & "( CI22PRIAPELALF =" & objGen.ValueToSQL(priapel, cwString) & ") AND " _
               & "( CI22SEGAPELALF =" & objGen.ValueToSQL(segapel, cwString) & " ))"



       End If
    End If
'********************************************LAS 4/2/99
   
      
   'Comprobamos que al a�adir  este nuevo registro no haya
   'duplicados
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      
      strSQL = "SELECT CI21CODPERSONA FROM CI2200 WHERE  " & strWhereDup
      Set rdoDuplicados = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
      If objGen.GetRowCount(rdoDuplicados) > 0 Then
        Call objError.SetError(cwCodeQuery, "Esta Persona puede estar dado de alta." & vbCrLf & "�Desea visualizar los registros coincidentes?")
        intRespuesta = objError.Raise

        If intRespuesta = vbYes Then
          'vntCodPersona = ProcesoDuplicados(strWhereDup)
          'If vntCodPersona <> 0 Then
          '  objWinInfo.objWinActiveForm.blnChanged = False
          '  blnCancel = True
          '  objWinInfo.objWinActiveForm.strInitialWhere = "CI21CODPERSONA=" & vntCodPersona
          '  objWinInfo.DataMoveFirst
          'Else
            Call objError.SetError(cwCodeQuery, "�Desea crear la nueva Persona F�sica?")
            intRespuesta = objError.Raise

            If intRespuesta = vbNo Then
              blnCancel = True
            End If
          'End If
        End If
      End If
       
      'Guardar valor del N.I.F sin puntos ni guiones.
      If txtText1(4).Text <> "" Then
        txtText1(4).Text = ValidarDNI(txtText1(4).Text)
        'Validar d�gito control del N.I.F.
        If Right(txtText1(4).Text, 1) Like "[a-zA-Z]" And _
           Left(Right(txtText1(4).Text, 2), 1) Like "#" Then
        
          If Right(txtText1(4).Text, 1) Like "[a-z]" Then
            'D�gito en min�sculas, lo pasamos a may�sculas.
            vntDigControl = UCase(Right(txtText1(4).Text, 1))
            txtText1(4).Text = Left(txtText1(4).Text, Len(txtText1(4).Text) - 1) & _
                  CStr(vntDigControl)
           End If
        
           strLetra = ValidaDigControlDNI(txtText1(4).Text)
           If strLetra <> Right(txtText1(4).Text, 1) Then
             Call objError.SetError(cwCodeMsg, _
               "El d�gito de control del D.N.I. es err�neo.")
             Call objError.Raise
             blnCancel = True
             txtText1(4).SetFocus
             Exit Sub
           End If
        End If
      End If
      
      'N� S.S.
      'Validar que metan C�d.Provincia y N�.
      If (txtText1(6).Text <> "" And txtText1(47) = "") Or _
        (txtText1(6).Text = "" And txtText1(47) <> "") Then
        Call objError.SetError(cwCodeMsg, _
          "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero")
        Call objError.Raise
        blnCancel = True
        txtText1(6).SetFocus
        Exit Sub
      End If
      
      'Validar que no metan solo el d�gito de control.
      If txtText1(6).Text = "" And txtText1(47) = "" And _
         txtText1(46) <> "" Then
        Call objError.SetError(cwCodeMsg, _
          "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero")
        Call objError.Raise
        blnCancel = True
        txtText1(6).SetFocus
        Exit Sub
      End If
      
      'Validar C�d.Provincia del N� S.S.
      If txtText1(6).Text <> "" Then
         If Val(txtText1(6).Text) = 0 Or Val(txtText1(6).Text) > 52 Then
            Call objError.SetError(cwCodeMsg, _
              "El C�digo de Provincia del N� Seguridad Social es incorrecto.")
            Call objError.Raise
            blnCancel = True
            txtText1(6).SetFocus
            Exit Sub
         End If
      End If
      
      'Validar d�gito de control del N� S.S.
      If (txtText1(6).Text <> "" And txtText1(47) <> "") Then
        If txtText1(46).Text <> "" Then
            If Not ValidaDigControlNSS(Val(txtText1(6).Text), _
                Val(txtText1(47).Text), Val(txtText1(46).Text)) Then
               Call objError.SetError(cwCodeMsg, _
                  "El d�gito de control del N�mero de la Seguridad Social es err�neo.")
               Call objError.Raise
               blnCancel = True
               txtText1(46).SetFocus
               Exit Sub
            End If
        End If
      End If
      
      
      
    End If
   
   'Si estamos modificando un registro compruebo que no este
   'duplicado
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
    
      If (objWinInfo.objWinActiveForm.rdoCursor("CI22PRIAPEL") <> objWinInfo.CtrlGet(txtText1(2)) _
        Or (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI22SEGAPEL")) And objWinInfo.CtrlGet(txtText1(3)) <> "") _
        Or objWinInfo.objWinActiveForm.rdoCursor("CI22SEGAPEL") <> objWinInfo.CtrlGet(txtText1(3)) _
        Or objWinInfo.objWinActiveForm.rdoCursor("CI22DNI") <> objWinInfo.CtrlGet(txtText1(4)) _
        Or (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI22DNI")) And objWinInfo.CtrlGet(txtText1(4)) <> "") _
        Or (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI22FECNACIM")) And objWinInfo.CtrlGet(dtcDateCombo1(0)) <> "") _
        Or Format(objWinInfo.objWinActiveForm.rdoCursor("CI22FECNACIM"), "DD/MM/YYYY") <> Format(objWinInfo.CtrlGet(dtcDateCombo1(0)), "DD/MM/YYYY")) Then
      
        strWhereDup = strWhereDup & " AND CI21CODPERSONA <> " & Val(txtText1(0))
        strSQL = "SELECT CI21CODPERSONA FROM CI2200 WHERE  " & strWhereDup

        Set rdoDuplicados = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
        If objGen.GetRowCount(rdoDuplicados) >= 1 Then
          Call objError.SetError(cwCodeQuery, "Esta Persona puede estar dado de alta." & vbCrLf & "�Desea visualizar los registros coincidentes?")
          intRespuesta = objError.Raise

          If intRespuesta = vbYes Then
          'vntCodPersona = ProcesoDuplicados(strWhereDup)
          'If vntCodPersona <> 0 Then
          '  blnCancel = True
          '  objWinInfo.objWinActiveForm.strInitialWhere = "CI21CODPERSONA=" & vntCodPersona
          '  objWinInfo.objWinActiveForm.blnChanged = False
          '  objWinInfo.DataMoveFirst
          ' Else
            Call objError.SetError(cwCodeQuery, "�Desea actualizar los cambios en la Persona F�sica?")
            intRespuesta = objError.Raise

            If intRespuesta = vbNo Then
              blnCancel = True
            End If
          'End If
        End If
      End If
    End If
    
    If (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI22DNI")) And objWinInfo.CtrlGet(txtText1(4)) <> "") _
      Or objWinInfo.objWinActiveForm.rdoCursor("CI22DNI") <> objWinInfo.CtrlGet(txtText1(4)) Then
        
        'Guardar valor del N.I.F sin puntos ni guiones.
        txtText1(4).Text = ValidarDNI(txtText1(4).Text)
        
        'Validar d�gito control del N.I.F.
        If Right(txtText1(4).Text, 1) Like "[a-zA-Z]" And _
           Left(Right(txtText1(4).Text, 2), 1) Like "#" Then
        
           If Right(txtText1(4).Text, 1) Like "[a-z]" Then
             'D�gito en min�sculas, lo pasamos a may�sculas.
             vntDigControl = UCase(Right(txtText1(4).Text, 1))
             txtText1(4).Text = Left(txtText1(4).Text, Len(txtText1(4).Text) - 1) & _
                    CStr(vntDigControl)
           End If
        
           strLetra = ValidaDigControlDNI(txtText1(4).Text)
           If strLetra <> Right(txtText1(4).Text, 1) Then
              Call objError.SetError(cwCodeMsg, _
                "El d�gito de control del D.N.I. es err�neo.")
              Call objError.Raise
              blnCancel = True
              txtText1(4).SetFocus
              Exit Sub
            End If
        End If
    End If
      
    'N� S.S.
    'Validar que metan C�d.Provincia y N�.
    If (txtText1(6).Text <> "" And txtText1(47) = "") Or _
        (txtText1(6).Text = "" And txtText1(47) <> "") Then
      Call objError.SetError(cwCodeMsg, _
        "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero")
      Call objError.Raise
      blnCancel = True
      txtText1(6).SetFocus
      Exit Sub
    End If
      
    'Validar que no metan solo el d�gito de control.
    If txtText1(6).Text = "" And txtText1(47) = "" And _
       txtText1(46) <> "" Then
      Call objError.SetError(cwCodeMsg, _
        "N� Seguridad Social incorrecto. Es obligatorio introducir C�digo Provincia y N�mero")
      Call objError.Raise
      blnCancel = True
      txtText1(6).SetFocus
      Exit Sub
    End If
      
    'Concatenar los tres campos
    If txtText1(6).Text <> "" And txtText1(47).Text <> "" Then
       strNumSS = Format(txtText1(6).Text, "00") & _
                    Format(txtText1(47).Text, "00000000")
    End If
    If txtText1(46).Text <> "" Then
       strNumSS = strNumSS & Format(txtText1(46).Text, "00")
    End If
    If (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI22NUMSEGSOC")) And strNumSS <> "") _
      Or objWinInfo.objWinActiveForm.rdoCursor("CI22NUMSEGSOC") <> strNumSS Then
    
      'Validar C�d.Provincia del N� S.S.
      If txtText1(6).Text <> "" Then
         If Val(txtText1(6).Text) = 0 Or Val(txtText1(6).Text) > 52 Then
            Call objError.SetError(cwCodeMsg, _
              "El C�digo de Provincia del N� Seguridad Social es incorrecto.")
            Call objError.Raise
            blnCancel = True
            txtText1(6).SetFocus
            Exit Sub
         End If
      End If
      
      'Validar d�gito de control del N� S.S.
      If (txtText1(6).Text <> "" And txtText1(47) <> "") Then
        If txtText1(46).Text <> "" Then
            If Not ValidaDigControlNSS(Val(txtText1(6).Text), _
                Val(txtText1(47).Text), Val(txtText1(46).Text)) Then
               Call objError.SetError(cwCodeMsg, _
                  "El d�gito de control del N�mero de la Seguridad Social es err�neo.")
               Call objError.Raise
               blnCancel = True
               txtText1(46).SetFocus
               Exit Sub
            End If
        End If
      End If
   End If
      
 End If
 'Comprobaci�n de que si se mete un c�digo de persona real
 'existe en la tabla de Personas F�sicas
 If objWinInfo.CtrlGet(txtText1(9)) <> "" Then
   strSQL = "SELECT CI21CODPERSONA,CI21CODPERSONA_REA " _
        & " FROM CI2200 WHERE CI21CODPERSONA=" _
        & Val(objWinInfo.CtrlGet(txtText1(9)))
   Set rdoDuplicados = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
   If Not rdoDuplicados.EOF Then
     If Not IsNull(rdoDuplicados("CI21CODPERSONA_REA")) Then
        Call objError.SetError(cwCodeMsg, "La Persona Real indicada no es una Persona Real.")
        Call objError.Raise
        blnCancel = True
        txtText1(9).SetFocus
        Call rdoDuplicados.Close
        Exit Sub
     End If
     'Comprobaci�n de que esa persona no es ya una persona real asociada
     Call rdoDuplicados.Close
     strSQL = "SELECT CI21CODPERSONA,CI21CODPERSONA_REA " _
        & " FROM CI2200 WHERE CI21CODPERSONA_REA=" _
        & Val(objWinInfo.CtrlGet(txtText1(0)))
     Set rdoDuplicados = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
     If Not rdoDuplicados.EOF Then
       Call objError.SetError(cwCodeMsg, "Esta Persona ya esta asociada como Persona Real en la Persona con N�mero " & rdoDuplicados(0))
       Call objError.Raise
       blnCancel = True
       txtText1(9).SetFocus
     End If
   Else
     Call objError.SetError(cwCodeMsg, "La Persona Real indicada no existe.")
     Call objError.Raise
     blnCancel = True
     txtText1(9).SetFocus
   End If
 End If
 Set rdoDuplicados = Nothing

'Comprobaci�n Fecha Nacimiento inferior fecha actual
 If IsDate(dtcDateCombo1(0).Date) Then
  If dtcDateCombo1(0).Date > CDate(Format(objGen.GetDBDateTime, "DD/MM/YYYY")) Then
    Call objError.SetError(cwCodeMsg, "La Fecha de Nacimiento es mayor que la Fecha Actual.")
    Call objError.Raise
    blnCancel = True
    dtcDateCombo1(0).SetFocus
    End If
  End If
  
  
  
  
  
  
  
End If
 
 If strFormName = "Direcci�n" Then
   'si es la primera Direcci�n la hago principal
   'EFS: lo comento para ver sie esto es un error
'   If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
'      Call objWinInfo.CtrlSet(chkCheck1(0), 1)
'   End If
   'Control de N�mero de Portal
 '  If IsNumeric(txtText1(20)) And blnControlPortal Then
     'Si el n�mero de portal es par
 '    If Val(objWinInfo.CtrlGet(txtText1(20))) Mod 2 = 0 Then
       
 '      If Val(objWinInfo.CtrlGet(txtText1(20))) < lngPortalParCom Or _
 '         Val(objWinInfo.CtrlGet(txtText1(20))) > lngPortalParFin Then
       
 '        blnControlPortal = False
 '        Call objError.SetError(cwCodeMsg, "El n�mero de portal no coincide con los rangos del C�digo Postal")
 '        Call objError.Raise
 '        blnCancel = True
 '        Exit Sub
 '      End If
 '    Else
     'Si el n�mero de portal es impar
 '      If Val(objWinInfo.CtrlGet(txtText1(20))) < lngPortalImpCom Or _
 '             Val(objWinInfo.CtrlGet(txtText1(20))) > lngPortalImpFin Then
         
'         blnControlPortal = False
'         Call objError.SetError(cwCodeMsg, "El n�mero de portal no coincide con los rangos del C�digo Postal")
'         Call objError.Raise
'         blnCancel = True
'         Exit Sub
'       End If
'     End If
'   End If
 End If

End Sub


Private Sub objWinInfo_cwPreChangeStatus(ByVal strFormName As String, ByVal intOldStatus As CodeWizard.cwFormStatus, ByVal intNewStatus As CodeWizard.cwFormStatus)
 
  If strFormName = "PersF�sica" Then
    If intNewStatus = cwModeSingleAddRest Then
      With objWinInfo
      .CtrlGetInfo(chkCheck1(1)).blnMandatory = False
      .CtrlGetInfo(chkCheck1(2)).blnMandatory = False
        .CtrlGetInfo(txtText1(2)).blnMandatory = True
        '.CtrlGetInfo(txtText1(3)).blnMandatory = True
'        .CtrlGetInfo(cboSSDBCombo1(1)).blnMandatory = True
        .CtrlGetInfo(cboSSDBCombo1(3)).blnMandatory = True
        '.CtrlGetInfo(dtcDateCombo1(0)).blnMandatory = True
         Call .FormChangeColor(.objWinActiveForm)
      End With
'LAS 31.3.1999 Modificado por ajo  (puesto en el toolbar)
'    ElseIf intNewStatus = cwModeSingleEdit Then
'        If blnBorrando = True And txtText1(8).Text <> "" Then
'
'            If comprobPersFis(CLng(txtText1(8).Text)) = True Then
'                 borrarPersFis (CLng(txtText1(8).Text))
'            End If
'        End If
'        blnBorrando = False
    End If
'LAS 31.3.1999
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
       If chkCheck1(2).Value = 0 Then
          txtText1(49).Text = "1"
       Else
          txtText1(49).Text = "2"
       End If
    End If
  
  End If
     objWinInfo.CtrlGetInfo(chkCheck1(1)).blnMandatory = False
    objWinInfo.CtrlGetInfo(chkCheck1(2)).blnMandatory = False
End Sub
Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
 Dim strSQL As String
 Dim rdoDir As rdoResultset
 If strFormName = "Direcci�n" And blnActivado Then
    strSQL = "SELECT * FROM CI1000 WHERE CI21CODPERSONA=" & lngNumPersona
    Set rdoDir = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset)
    If objGen.GetRowCount(rdoDir) = 1 Then
        strSQL = "SELECT * FROM CI1000 WHERE CI21CODPERSONA=" & lngNumPersona
        Set rdoDir = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurValues)
        rdoDir.Edit
        rdoDir("CI10INDDIRPRINC") = -1
        rdoDir.Update
        objWinInfo.DataRefresh
    ElseIf objGen.GetRowCount(rdoDir) > 1 Then
        MsgBox "Debe elegir una direcci�n como principal.", vbOKOnly, "Aviso"
    End If
    blnActivado = False
End If

End Sub


Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
   If chkCheck1(0).Value = 1 Then
    blnActivado = True
   End If
   lngNumPersona = CLng(txtText1(0).Text) 'modificaci�n LAS 25/11/98
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  If strFormName = "Direcci�n" And blnRefrescar = True Then
    blnRefrescar = False
    objWinInfo.DataRefresh
  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "PersF�sica" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
      DoEvents
    End With
  End If
 
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  lngCodPers = Val(txtText1(0).Text)
  Call objPipe.PipeSet("PERSONA", txtText1(0).Text)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
'******************************************************************
' Si se a�ade una nueva direcci�n le paso
' el c�digo de la persona
'******************************************************************
 If strFormName = "Direcci�n" Then
 ' Le paso el c�digo de persona al nuevo registro de direcci�n
   Call objWinInfo.CtrlSet(txtText1(27), objWinInfo.CtrlGet(txtText1(0)))
 End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)

Dim sql As String
Dim qryHistoria As rdoQuery
Dim rstHistoria As rdoResultset

 If blnUpdate = True Then
        sql = "update CI1000 set CI10INDDIRPRINC=0 " _
             & "where CI21CODPERSONA=" & Val(objWinInfo.CtrlGet(txtText1(0))) _
             & " and CI10NUMDIRECCI<>" & Trim(objWinInfo.CtrlGet(txtText1(18)))
        objApp.rdoConnect.Execute (sql)
        blnUpdate = False
'objWinInfo.DataRefresh
End If
'******************************************************************
' Control del form Personas F�sicas
'******************************************************************
'  If strFormName = "PersF�sica" Then
    If blnError = True Then
     'Cancelo los cambios
      Call objApp.rdoEnv.RollbackTrans
    Else
     'Actualizo los cambios
      Call objApp.rdoEnv.CommitTrans
    End If
'    cmdVer.Enabled = True
'  End If
  
'******************************************************************
' Control del form Direcciones
'******************************************************************
  If strFormName = "Direcci�n" Then
    If objWinInfo.CtrlGet(chkCheck1(0)) = 1 Then
      'chkCheck1(0).Enabled = False
    Else
      chkCheck1(0).Enabled = True
    End If
  End If
   sql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA =?"
   Set qryHistoria = objApp.rdoConnect.CreateQuery("", sql)
   'EFS: Cambio como "creo" que debe ser 27/2/1999
'      qryHistoria(0) = txtText1(0).Text
   Set rstHistoria = qryHistoria.OpenResultset()
      If Not IsNull(rstHistoria(0)) And Not rstHistoria.EOF Then txtText1(8).Text = rstHistoria(0)
   rstHistoria.Close
   qryHistoria.Close
End Sub


Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
' Variable que guarda la sentencia Sql
  Dim strSQL As String
' Variable para el resultset para dar de alta en la tabla personas
  Dim rdoPersonas As rdoResultset
  Dim rdoQueryPersonas As rdoQuery
  Dim lngNuevoCod As Long
  Dim rdoDireccion As rdoResultset
  Dim rdoDireccion2 As rdoResultset
'  Dim rdoQueryDir As rdoQuery
  Dim strNumSS As String
  
  blnUpdate = False
    
  If strFormName = "PersF�sica" Then
'******************************************************************
  ' Si estamos a�adiendo un registro nuevo creamos un nuevo registro en
  ' la tabla persona (CI2100)
'******************************************************************
    
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then
      
      strSQL = "select CI21CODPERSONA from CI2100 order by  CI21CODPERSONA desc"
    ' Empieza transacci�n
      Call objApp.rdoEnv.BeginTrans
     'Obtengo el nuevo c�digo
     
      lngNuevoCod = GetNewCode(strSQL)
    ' Creamos el cursor
     
      strSQL = "select CI21CODPERSONA, CI33CODTIPPERS from " _
             & " CI2100"
      Set rdoQueryPersonas = objApp.rdoConnect.CreateQuery("", strSQL)
     'Establecemos el n� de registros a devolver a 1
      rdoQueryPersonas.MaxRows = 1
      Set rdoPersonas = rdoQueryPersonas.OpenResultset(rdOpenKeyset, rdConcurRowVer)

     'A�ado un nuevo registro en la tabla Personas (CI2100)
      rdoPersonas.AddNew
      rdoPersonas("CI21CODPERSONA") = lngNuevoCod
      rdoPersonas("CI33CODTIPPERS") = 1 ' NO SE LOS CODIGOS DE TIPO PERSONA
     'Actualizo y cierro el cursor
      rdoPersonas.Update
      rdoPersonas.Close
     'Le paso el nuevo c�digo a la nueva persona f�sica
      Call objWinInfo.CtrlStabilize(txtText1(0), lngNuevoCod)
    
      'Volcar la informaci�n C�d. Profesi�n
      If cboSSDBCombo1(6).Value <> "" Then
         objWinInfo.objWinActiveForm.rdoCursor("CI25CODPROFESI") = Val(cboSSDBCombo1(6).Value)
      End If
      
      'Volcar la informaci�n C�d. Relaci�n Universidad
      If cboSSDBCombo1(2).Value <> "" Then
         objWinInfo.objWinActiveForm.rdoCursor("CI28CODRELUDN") = Val(cboSSDBCombo1(2).Value)
      End If
      
      'Concatenar los tres campos del N� S.S.
      If txtText1(6).Text <> "" And txtText1(47).Text <> "" Then
         strNumSS = Format(txtText1(6).Text, "00") & _
                    Format(txtText1(47).Text, "00000000")
      End If
      If txtText1(46).Text <> "" Then
         strNumSS = strNumSS & Format(txtText1(46).Text, "00")
      End If
      If strNumSS <> "" Then
        'Volcar la informaci�n N� S.S.
        objWinInfo.objWinActiveForm.rdoCursor("CI22NUMSEGSOC") = strNumSS
      End If
      
      'Check Borrado
      If chkCheck1(2).Value = 0 Then
        objWinInfo.objWinActiveForm.rdoCursor("AD34CODESTADO") = 1
      Else
        objWinInfo.objWinActiveForm.rdoCursor("AD34CODESTADO") = 2
      End If
    End If
    
    Call objWinInfo.CtrlSearchLinked(objWinInfo.CtrlGetInfo(txtText1(12)))
 
'******************************************************************
  ' Si estamos modificando un registro existente
'******************************************************************
    If objWinInfo.intWinStatus = cwModeSingleEdit Then
  
       'Volcar la informaci�n C�d. Profesi�n
       If objWinInfo.objWinActiveForm.rdoCursor("CI25CODPROFESI") <> Val(cboSSDBCombo1(6).Value) _
        Or (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI25CODPROFESI")) And _
        cboSSDBCombo1(6).Value <> "") Then
         
         If cboSSDBCombo1(6).Value <> "" Then
           objWinInfo.objWinActiveForm.rdoCursor("CI25CODPROFESI") = Val(cboSSDBCombo1(6).Value)
         Else
           objWinInfo.objWinActiveForm.rdoCursor("CI25CODPROFESI") = Null
         End If
       End If

       'Volcar la informaci�n C�d. Relaci�n Universidad
       If objWinInfo.objWinActiveForm.rdoCursor("CI28CODRELUDN") <> Val(cboSSDBCombo1(2).Value) _
        Or (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI28CODRELUDN")) And _
        cboSSDBCombo1(2).Value <> "") Then
         
         If cboSSDBCombo1(2).Value <> "" Then
           objWinInfo.objWinActiveForm.rdoCursor("CI28CODRELUDN") = Val(cboSSDBCombo1(2).Value)
         Else
           objWinInfo.objWinActiveForm.rdoCursor("CI28CODRELUDN") = Null
         End If
       End If

       'Volcar la informaci�n N� Seguridad Social
       If txtText1(6).Text <> "" And txtText1(47).Text <> "" Then
         strNumSS = Format(txtText1(6).Text, "00") & _
                    Format(txtText1(47).Text, "00000000")
       End If
       If txtText1(46).Text <> "" Then
         strNumSS = strNumSS & Format(txtText1(46).Text, "00")
       End If
       
       If objWinInfo.objWinActiveForm.rdoCursor("CI22NUMSEGSOC") <> strNumSS _
        Or (IsNull(objWinInfo.objWinActiveForm.rdoCursor("CI22NUMSEGSOC")) And _
        strNumSS <> "") Then
         
         If strNumSS <> "" Then
           objWinInfo.objWinActiveForm.rdoCursor("CI22NUMSEGSOC") = strNumSS
         Else
           objWinInfo.objWinActiveForm.rdoCursor("CI22NUMSEGSOC") = Null
         End If
       End If
       
       'Check Borrado
       If chkCheck1(2).Value = 0 And objWinInfo.objWinActiveForm.rdoCursor("AD34CODESTADO") <> 1 Then
         objWinInfo.objWinActiveForm.rdoCursor("AD34CODESTADO") = 1
       ElseIf chkCheck1(2).Value = 1 And objWinInfo.objWinActiveForm.rdoCursor("AD34CODESTADO") <> 2 Then
         objWinInfo.objWinActiveForm.rdoCursor("AD34CODESTADO") = 2
       End If

    End If

  End If
  
  If strFormName = "Direcci�n" Then
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then

      strSQL = "select CI10NUMDIRECCI from CI1000 " _
               & "where CI21CODPERSONA=" & Val(objWinInfo.CtrlGet(txtText1(0))) _
               & " order by  CI10NUMDIRECCI desc"
'    ' Empieza transacci�n
'      Call objApp.rdoEnv.BeginTrans
'     'Obtengo el nuevo c�digo
'
      lngNuevoCod = GetNewCode(strSQL)
     'Le paso el nuevo c�digo a la nueva direcci�n
      Call objWinInfo.CtrlStabilize(txtText1(18), lngNuevoCod)
'      Call objApp.rdoEnv.CommitTrans
    End If
'calculamos el numero de direcciones principales existentes(0 � 1)
    strSQL = "select count(*) from CI1000 " _
             & "where CI21CODPERSONA=" & Val(objWinInfo.CtrlGet(txtText1(0))) _
             & " and CI10INDDIRPRINC=-1"
             
    Set rdoDireccion = objApp.rdoConnect.OpenResultset(strSQL)
'si es cero, activamos el indicador de doreccion principal
    If rdoDireccion(0) = 0 Then
        objWinInfo.objWinActiveForm.rdoCursor("CI10INDDIRPRINC") = -1
'        objWinInfo.DataRefresh
'si es uno, hallamos en numero de la direccion que es la principal
    ElseIf rdoDireccion(0) = 1 Then
        strSQL = "select CI10NUMDIRECCI from CI1000 " _
             & "where CI21CODPERSONA=" & Val(objWinInfo.CtrlGet(txtText1(0))) _
             & " and CI10INDDIRPRINC=-1"
        Set rdoDireccion2 = objApp.rdoConnect.OpenResultset(strSQL)
'si la direccion pricipal es la actual, y el check no esta activo,
'lo activamos
        If CStr(rdoDireccion2(0)) = Trim(objWinInfo.CtrlGet(txtText1(18))) And _
         objWinInfo.CtrlGet(chkCheck1(0)) = 0 Then
            objWinInfo.objWinActiveForm.rdoCursor("CI10INDDIRPRINC") = -1
'si la direccion pricipal no es la actual, y el check esta activado,
'desactivamos todas menos la actual
        ElseIf CStr(rdoDireccion2(0)) <> Trim(objWinInfo.CtrlGet(txtText1(18))) And _
        objWinInfo.CtrlGet(chkCheck1(0)) = 1 Then
             blnUpdate = True
             blnRefrescar = True
'        strSql = "update CI1000 set CI10INDDIRPRINC=0 " _
'             & "where CI21CODPERSONA=" & Val(objWinInfo.CtrlGet(txtText1(0))) _
'             & " and CI10NUMDIRECCI<>" & Trim(objWinInfo.CtrlGet(txtText1(18)))
'        objApp.rdoConnect.Execute (strSql)
        End If
        rdoDireccion2.Close
    End If
    rdoDireccion.Close
    
    
      'EFS: LO COMENTO PORQUE CREO QUE ES AQUI DONDE DA LOS ERRORES.
      'REVISAR MAS ADELANTE. 27/02/99
'''''''
'''''''
'''''''
'''''''
'''''''   'Actualizamos la direcci�n principal
'''''''    If blnActDirPrinc Then
'''''''      blnActDirPrinc = False
'''''''      strSql = "select * from CI1000 where CI21CODPERSONA=" & Val(objWinInfo.CtrlGet(txtText1(0))) & " AND CI10INDDIRPRINC=-1"
'''''''      Set rdoDireccion = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
'''''''      Call objApp.rdoEnv.BeginTrans
'''''''
'''''''      If objGen.GetRowCount(rdoDireccion) > 0 Then
'''''''        While Not rdoDireccion.EOF
'''''''          rdoDireccion.Edit
'''''''          rdoDireccion("CI10INDDIRPRINC") = 0
'''''''          rdoDireccion.Update
'''''''          rdoDireccion.MoveNext
'''''''        Wend
'''''''        blnRefrescar = True
'''''''        rdoDireccion.Close
'''''''      End If
'''''''      Set rdoDireccion = Nothing
'''''''
'''''''      With objWinInfo.objWinMainForm
'''''''        .rdoCursor.Edit
'''''''        .rdoCursor("CI22NUMDIRPRINC") = Val(objWinInfo.CtrlGet(txtText1(18)))
'''''''        .rdoCursor.Update
'''''''      End With
'''''''      Call objApp.rdoEnv.CommitTrans 'EFS
'''''''  End If
'''''''
'''''''    Call objWinInfo.CtrlSearchLinked(objWinInfo.CtrlGetInfo(txtText1(24)))
       
  End If
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim strSQL As String
  Dim rdoDir As rdoResultset
  Dim bResp  As Boolean

  'si pulsan a borrar, se chequea primero si es responsable
  'no se puede borrar y nos salimos de la rutina.
  If btnButton.Index = 8 Then
    'AJO-EFS
    If objWinInfo.objWinActiveForm.strName = "PersF�sica" Then
        'blnBorrando = True
        lngNumPersona = txtText1(0).Text
        bResp = comprobPersFis(lngNumPersona)
        If Not bResp Then
            Exit Sub
        Else
            bResp = borrarPersFis(CLng(txtText1(0).Text))
            'AJO-EFS: Vemos si hemos realizado todos los borrados
            If bResp Then
             objApp.rdoConnect.CommitTrans
            Else
             objApp.rdoConnect.RollbackTrans
             MsgBox "No se ha podido borrar esta persona, intentelo m�s tarde", vbCritical
             Exit Sub
            End If
        End If
    Else
        'blnBorrando = False
    End If
    
  End If
  '--------------- LUIS
'  ElseIf intNewStatus = cwModeSingleEdit Then
'        If blnBorrando = True And txtText1(8).Text <> "" Then
'
'            If comprobPersFis(CLng(txtText1(8).Text)) = True Then
'                 borrarPersFis (CLng(txtText1(8).Text))
'            End If
'        End If
'        blnBorrando = False
  '--------------
  
  
  
  
  
  
  If btnButton.Index = 16 Then
    IdPersona1.Text = 0
    Call IdPersona1.Buscar
    If Val(IdPersona1.Text) <> 0 Then
        Call objWinInfo.FormChangeActive(tabTab1(2), False, True)
        Call objApp.objActiveWin.DataOpen
        txtText1_GotFocus (0)
        txtText1(0).Text = IdPersona1.Text
        txtText1_LostFocus (0)
    Else
        txtText1(1).SetFocus
    End If
  ElseIf btnButton.Index = 30 And (txtText1(0).Text <> "" Or txtText1(0).Text <> "0") And txtText1(18).Text <> "" Then
    strSQL = "SELECT * FROM CI1000 WHERE CI21CODPERSONA=" & txtText1(0).Text & " AND CI10INDDIRPRINC=-1"
    Set rdoDir = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset)
    If objGen.GetRowCount(rdoDir) = 0 Then
        MsgBox "Seleccione una Direcci�n como Principal antes de salir", vbOKOnly, "Direcci�n principal"
        Exit Sub
    ElseIf objGen.GetRowCount(rdoDir) > 1 Then
        MsgBox "Hay m�s de una Direcci�n Principal, seleccione s�lo una antes de salir", vbOKOnly, "Direcci�n principal"
        Exit Sub
    End If
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  ElseIf btnButton.Index = 30 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Else
    If btnButton.Index = 2 Then
'        cmdVer.Enabled = False
        txtText1(1).SetFocus
    End If
    Me.MousePointer = vbHourglass
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    
 
' 'LAS***************
' 'Si tiene procesos/asistencias colgando, deshabilitar el check box 3
' If blnPrAsisColgando Then
' chkCheck1(3).Enabled = False
'  End If
' 'LAS***************
    
    Me.MousePointer = vbDefault
If objWinInfo.objWinActiveForm.strName = "PersF�sica" And btnButton.Index = 2 Then
    cboSSDBCombo1(3).Columns(0).Text = "45"
    cboSSDBCombo1(3).Text = "Espa�a"
    cboSSDBCombo1(4).Enabled = True
  ElseIf objWinInfo.objWinActiveForm.strName = "Direcci�n" And btnButton.Index = 2 Then
'    cboSSDBCombo1(11).Columns(0).Text = "45"
'    cboSSDBCombo1(11).Text = "Espa�a"
    Do While cboSSDBCombo1(11).Columns(0).Text <> intCodEsp
        cboSSDBCombo1(11).MoveNext
    Loop
   'cboSSDBCombo1(11).MovePrevious
   cboSSDBCombo1(11).Text = cboSSDBCombo1(11).Columns(1).Text
   cboSSDBCombo1(10).Enabled = True
  End If
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Me.MousePointer = vbHourglass
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  Me.MousePointer = vbDefault
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Dim iResp As Integer
    
    'Si esta pantalla es invocada desde personas f�sicas
    'y tiene datos para duplicar es decir c�digo de personas f�sicas
    'y c�digo de direcci�n, damos la opci�n de insertar la misma
    If bIgualDireccion Then
        iResp = MsgBox("�Quiere que sea la misma direcci�n que el paciente?", vbYesNo)
        If iResp = 6 Then       'Si
            'Grabamos direcci�n
            bDuplicarDireccion
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(26))
        End If
        bIgualDireccion = False
    End If
    'Si no ha salido de la funcion es que no quiere que la direccion sea la
    'misma o que esta pantalla no ha sido invocada desde p.f.
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  If intIndex = 11 Then
    If cboSSDBCombo1(11).Columns(0).Text = "45" Then
      cboSSDBCombo1(10).Enabled = True
    Else
      cboSSDBCombo1(10).Enabled = False
      cboSSDBCombo1(10).Text = ""
    End If
  End If
  If intIndex = 4 Then
    'txtText1(11) = cboSSDBCombo1(4).Columns("Provincia").Value
     
  End If

End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 10 Then
    With objWinInfo
      .CtrlGetInfo(txtText1(5)).blnForeign = True
      'If Len(.CtrlGet(cboSSDBCombo1(10))) = 1 Then
      '  Call objWinInfo.CtrlSet(txtText1(5), "0" & .CtrlGet(cboSSDBCombo1(10)))
      'Else
      '  Call objWinInfo.CtrlSet(txtText1(5), .CtrlGet(cboSSDBCombo1(10)))
      'End If
      Call objWinInfo.CtrlSet(txtText1(24), "")
      Call objWinInfo.CtrlSet(txtText1(28), "")
      Call objWinInfo.CtrlSet(txtText1(33), "")
    End With

  End If

  If intIndex = 4 Then
    Call objWinInfo.CtrlSet(txtText1(12), "")
    Call objWinInfo.CtrlSet(txtText1(32), "")
    Call objWinInfo.CtrlSet(txtText1(34), "")
  End If
  
  If intIndex = 11 Then
    If cboSSDBCombo1(11).Columns(0).Text = "45" Then
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = False
      txtText1(23) = ""
      objWinInfo.CtrlGetInfo(txtText1(23)).blnReadOnly = True
      Call objWinInfo.WinPrepareScr
'      Call cboSSDBCombo1(10).SetFocus
     
    Else
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = True
      objWinInfo.CtrlGetInfo(txtText1(23)).blnReadOnly = False
      cboSSDBCombo1(10).Text = ""
      Call objWinInfo.WinPrepareScr
    
    End If
 
  End If
  
  If intIndex = 3 Then
    If cboSSDBCombo1(3).Columns(0).Text = "45" Then
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(4)).blnReadOnly = False
      txtText1(11) = ""
      objWinInfo.CtrlGetInfo(txtText1(11)).blnReadOnly = True
    Else
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(4)).blnReadOnly = True
      objWinInfo.CtrlGetInfo(txtText1(11)).blnReadOnly = False
      cboSSDBCombo1(4).Text = ""
    End If
    Call objWinInfo.WinPrepareScr
    Call cboSSDBCombo1(3).SetFocus
  End If
      
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 10 And objWinInfo.intWinStatus <> cwModeSingleEmpty Then
    If Len(cboSSDBCombo1(10).Text) = 1 Then
      Call objWinInfo.CtrlSet(txtText1(5), "0" & cboSSDBCombo1(10).Text)
    Else
      Call objWinInfo.CtrlSet(txtText1(5), cboSSDBCombo1(10).Text)
    End If
  End If
  
  If intIndex = 11 Then
    If cboSSDBCombo1(11).Columns(0).Text = "45" Then
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = False
     
    Else
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(10)).blnReadOnly = True
      cboSSDBCombo1(10).Text = ""
    End If
  End If
  If intIndex = 3 And objWinInfo.intWinStatus <> cwModeSingleOpen Then
    If cboSSDBCombo1(3).Columns(0).Text = "45" Then
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(4)).blnReadOnly = False
    Else
      objWinInfo.CtrlGetInfo(cboSSDBCombo1(4)).blnReadOnly = True
      cboSSDBCombo1(4).Text = ""
    End If
  
  End If
    
End Sub
Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 10 Then
    'Call objWinInfo.CtrlSet(txtText1(23), cboSSDBCombo1(10).Columns(1).Text)
  End If
  If intIndex = 4 Then
    'Call objWinInfo.CtrlSet(txtText1(11), cboSSDBCombo1(4).Columns(1).Text)
  End If


End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  On Error Resume Next
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  On Error Resume Next
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  On Error Resume Next
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  On Error Resume Next
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  'Si es una persona provisional pongo una serie de campos
  'como opcionales
  If intIndex = 0 And chkCheck1(0).Value = 1 Then
     blnActDirPrinc = True
  Else
     blnActDirPrinc = False
  End If

  
  With objWinInfo
    If intIndex = 1 Then
      If objWinInfo.CtrlGet(chkCheck1(1)) = 1 Then
        .CtrlGetInfo(txtText1(2)).blnMandatory = False
        '.CtrlGetInfo(txtText1(3)).blnMandatory = False
        .CtrlGetInfo(cboSSDBCombo1(1)).blnMandatory = False
        .CtrlGetInfo(cboSSDBCombo1(3)).blnMandatory = False
        '.CtrlGetInfo(dtcDateCombo1(0)).blnMandatory = False
  
      Else
        .CtrlGetInfo(txtText1(2)).blnMandatory = True
        '.CtrlGetInfo(txtText1(3)).blnMandatory = True
'        .CtrlGetInfo(cboSSDBCombo1(1)).blnMandatory = True
        .CtrlGetInfo(cboSSDBCombo1(3)).blnMandatory = True
        '.CtrlGetInfo(dtcDateCombo1(0)).blnMandatory = True
      End If
      Call .FormChangeColor(.objWinActiveForm)
      Call .WinPrepareScr
      If objWinInfo.intWinStatus <> cwModeSingleOpen Then
        On Error Resume Next
        chkCheck1(1).SetFocus
      End If
    End If
  End With
    
    'Check Borrado
    If intIndex = 2 Then
        'Marcado
        If chkCheck1(2).Value = 1 Then
           Call DeshabilitarPersona
        'No marcado
        Else
           Call HabilitarPersona
        End If
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub


Private Sub txtText1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'If Index = 0 Then
'If KeyCode = "13" Then
'        Call txtText1_LostFocus(0)
'    End If
'End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  
  Call objWinInfo.CtrlLostFocus

End Sub

Private Sub txtText1_Change(intIndex As Integer)
  If intIndex = 0 Then
    If blnAddMode Then
      lngUserCode = Val(txtText1(0))
    End If
  '  gvntCodProceso = objPipe.PipeGet("AD_PROCESO")
'    frmGenerarAsistencias.txtText1(18).Text = txtText1(0).Text
 '   frmGenerarAsistencias.txtText1(21).Text = txtText1(1).Text & " " & txtText1(2).Text & " " & txtText1(3).Text
  End If
  Call objWinInfo.CtrlDataChange
End Sub


'********************************************LAS 4/2/99
Private Function Alf(Nombre As String) As String

    Dim rdoSons     As rdoQuery
    Dim rdoCursor   As rdoResultset
    Dim strSQL      As String
    
    If Nombre = "" Then
    Alf = ""
    Exit Function
    End If
    
    strSQL = "SELECT GCFN08(?) FROM DUAL"
    Set rdoSons = objApp.rdoConnect.CreateQuery("", strSQL)
    rdoSons.rdoParameters(0) = Nombre
    
    Set rdoCursor = rdoSons.OpenResultset(rdOpenStatic, rdConcurReadOnly)
    Alf = rdoCursor.rdoColumns(0)
    
    Call rdoCursor.Close
    Call rdoSons.Close
    
    Set rdoCursor = Nothing
    Set rdoSons = Nothing
    
End Function
'********************************************LAS 4/2/99

Private Function blnPrAsisColgando() As Boolean
Dim sql As String
Dim QyConsulta As rdoQuery
Dim RsRespuesta As rdoResultset


err = 0

    
    sql = "SELECT COUNT(AD0800.AD07CODPROCESO)"
    sql = sql & " FROM AD0800,AD0700"
    sql = sql & " WHERE "
    sql = sql & " AD0700.AD07CODPROCESO=AD0800.AD07CODPROCESO AND"
    sql = sql & " AD0700.CI21CODPERSONA=?"
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = Val(objWinInfo.CtrlGet(txtText1(0)))
    
    Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    If err > 0 Then
        MsgBox "error en blnPrAsisColg"
        blnPrAsisColgando = False
    Else
        If RsRespuesta(0) > 0 And chkCheck1(3) = 1 Then
            blnPrAsisColgando = True
        Else
            blnPrAsisColgando = False
        End If
    End If

RsRespuesta.Close
QyConsulta.Close


End Function


Private Function comprobPersFis(lngNumPersona As Long) As Boolean

Dim sql As String
Dim lngNumHistoria  As Long
Dim QyConsulta As rdoQuery
Dim RsRespuesta As rdoResultset
Dim strMsg As String
Dim I As Byte

err = 0
comprobPersFis = True
'Si no ha tenido una cita, no tiene historia y no se chequea nada referente a las pruebas.
If txtText1(8).Text <> "" Then
        'Si tienen una cita que no esta cancelada.
        sql = "SELECT "
        sql = sql & " 1 "
        'SQL = SQL & " COUNT(CI0100.CI01SITCITA) "
        sql = sql & "FROM"
        sql = sql & " AD0800,AD0700,PR0400,CI0100 "
        sql = sql & "WHERE"
        sql = sql & " AD0700.CI22NUMHISTORIA=? AND"
        sql = sql & " AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO AND"
        sql = sql & " PR0400.AD07CODPROCESO=AD0800.AD07CODPROCESO AND"
        sql = sql & " PR0400.AD01CODASISTENCI=AD0800.AD01CODASISTENCI AND"
        sql = sql & " CI0100.PR04NUMACTPLAN=PR0400.PR04NUMACTPLAN AND"
        sql = sql & " PR0400.PR37CODESTADO IN(2,3,4,5) AND"
        sql = sql & " CI0100.CI01SITCITA <> 2;" ' IN(1,3,4);"
        
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        lngNumHistoria = txtText1(8).Text
        QyConsulta(0) = lngNumHistoria
        
        Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            
        If RsRespuesta.EOF = False Then
            comprobPersFis = False
            MsgBox "Este paciente no se puede eliminar, porque tiene citas", vbCritical
        End If
        
        RsRespuesta.Close
        QyConsulta.Close
        
        'Si ya no se puede dar de baja, nos saltamos el resto de los chequeos.
        If comprobPersFis = False Then Exit Function
        
        'Comprobamos que tengan una cita realizada, de ser as� no se podr�
        'borrar dicha persona.
        
        sql = "SELECT "
        sql = sql & " 1 "
        sql = sql & " FROM "
        sql = sql & " PR0400 "
        sql = sql & " WHERE "
        sql = sql & " CI21CODPERSONA =? AND"
        sql = sql & " PR37CODESTADO = 4 "
        
        Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
        QyConsulta(0) = lngNumPersona
        
        Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            
        If RsRespuesta.EOF = False Then
            comprobPersFis = False
            MsgBox "Este paciente no se puede eliminar, porque ya tiene actuaciones realizadas", vbCritical
        End If
        
        RsRespuesta.Close
        QyConsulta.Close
        
        'Si ya no se puede dar de baja, nos saltamos el resto de los chequeos.
        If comprobPersFis = False Then Exit Function
    End If
    
    'Comprobacion de que la persona no es responsable familiar de otra
    
    sql = "SELECT "
    sql = sql & " CI21CODPERSONA "
    sql = sql & "FROM"
    sql = sql & " CI2200 "
    sql = sql & "WHERE"
    sql = sql & " CI21CODPERSONA_RFA=?;"
    
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = lngNumPersona
    
    Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
    If RsRespuesta.EOF = False Then
    comprobPersFis = False
    I = 0
    strMsg = ""
        Do While RsRespuesta.EOF = False
            strMsg = strMsg + CStr(RsRespuesta(0)) + Chr$(13)
            RsRespuesta.MoveNext
            I = I + 1
        Loop
    If I = 1 Then
        strMsg = "la persona con el siguiente numero de persona: " + Chr$(13) + strMsg
    ElseIf I > 1 Then
        strMsg = "las personas con los siguientes numeros de persona: " + Chr$(13) + strMsg
    End If
    strMsg = "La persona " + CStr(lngNumPersona) + " no se puede eliminar por ser reponsable familiar de " + Chr$(13) + strMsg
    
    MsgBox strMsg
    End If
    
    RsRespuesta.Close
    QyConsulta.Close
    
    'Si ya no se puede dar de baja, nos saltamos el resto de los chequeos.
    If comprobPersFis = False Then Exit Function
    
    Rem comprobacion de que la persona no es persona real de otra
    
    sql = "SELECT "
    sql = sql & " CI21CODPERSONA "
    sql = sql & "FROM"
    sql = sql & " CI2200 "
    sql = sql & "WHERE"
    sql = sql & " CI21CODPERSONA_REA=?;"
    
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = lngNumPersona
    
    
    Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
    If RsRespuesta.EOF = False Then
    comprobPersFis = False
    I = 0
    strMsg = ""
        Do While RsRespuesta.EOF = False
            strMsg = strMsg + CStr(RsRespuesta(0)) + Chr$(13)
            RsRespuesta.MoveNext
            I = I + 1
        Loop
    If I = 1 Then
        strMsg = "la persona con el siguiente numero de persona: " + Chr$(13) + strMsg
    ElseIf I > 1 Then
        strMsg = "las personas con los siguientes numeros de persona: " + Chr$(13) + strMsg
    End If
    strMsg = "La persona " + CStr(lngNumPersona) + " no se puede eliminar por ser la identidad real de " + Chr$(13) + strMsg
    
    MsgBox strMsg
    End If
    
    RsRespuesta.Close
    QyConsulta.Close
    
    'Si ya no se puede dar de baja, nos saltamos el resto de los chequeos.
    If comprobPersFis = False Then Exit Function
    
    
    Rem comprobacion de que la persona no es responsable econ�mico de otra
    
    sql = "SELECT "
    sql = sql & " CI21CODPERSONA "
    sql = sql & "FROM"
    sql = sql & " CI2200 "
    sql = sql & "WHERE"
    sql = sql & " CI21CODPERSONA_REC=?;"
    
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = lngNumPersona
    
    
    Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
    If RsRespuesta.EOF = False Then
    comprobPersFis = False
    I = 0
    strMsg = ""
        Do While RsRespuesta.EOF = False
            strMsg = strMsg + CStr(RsRespuesta(0)) + Chr$(13)
            RsRespuesta.MoveNext
            I = I + 1
        Loop
    If I = 1 Then
        strMsg = "la persona con el siguiente numero de persona: " + Chr$(13) + strMsg
    ElseIf I > 1 Then
        strMsg = "las personas con los siguientes numeros de persona: " + Chr$(13) + strMsg
    End If
    strMsg = "La persona " + CStr(lngNumPersona) + " es el responsable econ�mico de " + Chr$(13) + strMsg
    MsgBox strMsg
End If

RsRespuesta.Close
QyConsulta.Close

End Function


Private Function borrarPersFis(lngNumPersona As Long) As Boolean
Dim sql As String
Dim QyNumSolicit As rdoQuery
Dim QyConsulta As rdoQuery
Dim RsNumSolicit As rdoResultset
Dim RsRespuesta As rdoResultset

Dim lngCodPersona As Long
borrarPersFis = True
err = 0
On Error GoTo Canceltrans
Rem SECUENCIA DE BORRADO DE UNA PERSONA F�SICA

'SELECTS//////////////////////////////////////////////////////

'SE HALLAN LOS N�MEROS DE SOLICITUD
sql = "SELECT DISTINCT"
sql = sql & " CI0100.CI31NUMSOLICIT"
sql = sql & " FROM"
sql = sql & " CI0100,"
sql = sql & " PR0400,"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND"
sql = sql & " PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND"
sql = sql & " CI0100.PR04NUMACTPLAN=PR0400.PR04NUMACTPLAN;"

Set QyNumSolicit = objApp.rdoConnect.CreateQuery("", sql)
QyNumSolicit(0) = lngNumPersona
Set RsNumSolicit = QyNumSolicit.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)



'SE HALLA EL C�DIGO DE LA PERSONA
'''SQL = "SELECT "
'''SQL = SQL & " CI2200.CI21CODPERSONA"
'''SQL = SQL & " FROM"
'''SQL = SQL & " CI2200"
'''SQL = SQL & " WHERE"
'''SQL = SQL & " CI2200.CI22NUMHISTORIA=?;"
'''
'''Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQL)
'''QyConsulta(0) = lngNumHistoria
'''Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'''
'''If RsRespuesta.EOF Then
'''    MsgBox "ERROR: N�mero de historia err�neo"
'''    RsRespuesta.Close
'''    QyConsulta.Close
'''    Exit Sub
'''End If
'''
'''lngCodPersona = RsRespuesta(0)
'''
'''RsRespuesta.Close
'''QyConsulta.Close

'\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


'DELETES//////////////////////////////////////////////////////
'EFS:COMENZAMOS LA TRANSACION PARA BORRAR TODO LO QUE PUEDE COLGAR DE LA PERSONA
'FISICA, DE FORMA QUE LUEGO LA TABLA CI2200 SE BORRE POR LAS FUNCIONES DE CW Y NO
'HAYA ERRORES.
objApp.rdoConnect.BeginTrans
'objSecurity.RegSession


'PR0800 PET-ACT PEDIDA******************************
sql = "DELETE FROM PR0800"
sql = sql & " WHERE "
sql = sql & " (PR0800.PR09NUMPETICION,"
sql = sql & " PR0800.PR03NUMACTPEDI,"
sql = sql & " PR0800.PR08NUMSECUENCIA)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " PR0800.PR09NUMPETICION,"
sql = sql & " PR0800.PR03NUMACTPEDI,"
sql = sql & " PR0800.PR08NUMSECUENCIA"
sql = sql & " FROM"
sql = sql & " PR0800,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0800.PR09NUMPETICION=PR0900.PR09NUMPETICION);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'PR1400 TIPO RECURSO PEDIDO**************************
sql = "DELETE FROM PR1400"
sql = sql & " WHERE "
sql = sql & " (PR1400.PR03NUMACTPEDI,"
sql = sql & " PR1400.PR06NUMFASE,"
sql = sql & " PR1400.PR14NUMNECESID)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " PR1400.PR03NUMACTPEDI,"
sql = sql & " PR1400.PR06NUMFASE,"
sql = sql & " PR1400.PR14NUMNECESID"
sql = sql & " FROM"
sql = sql & " PR1400,"
sql = sql & " PR0600,"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND"
sql = sql & " PR0600.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND"
sql = sql & " PR1400.PR03NUMACTPEDI=PR0600.PR03NUMACTPEDI);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'PR0600 FASE PEDIDA********************************************
sql = "DELETE FROM PR0600"
sql = sql & " WHERE "
sql = sql & " (PR0600.PR03NUMACTPEDI,"
sql = sql & " PR0600.PR06NUMFASE)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " PR0600.PR03NUMACTPEDI,"
sql = sql & " PR0600.PR06NUMFASE"
sql = sql & " FROM"
sql = sql & " PR0600,"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND"
sql = sql & " PR0600.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'CI2700 RECURSO CITADO********************************
sql = "DELETE FROM CI2700"
sql = sql & " WHERE "
sql = sql & " (CI2700.CI31NUMSOLICIT,"
sql = sql & " CI2700.CI01NUMCITA,"
sql = sql & " CI2700.CI15NUMFASECITA,"
sql = sql & " CI2700.AG11CODRECURSO)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " CI2700.CI31NUMSOLICIT,"
sql = sql & " CI2700.CI01NUMCITA,"
sql = sql & " CI2700.CI15NUMFASECITA,"
sql = sql & " CI2700.AG11CODRECURSO"
sql = sql & " FROM"
sql = sql & " CI2700,"
sql = sql & " CI1500,"
sql = sql & " CI0100,"
sql = sql & " PR0400,"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND"
sql = sql & " PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND"
sql = sql & " CI0100.PR04NUMACTPLAN=PR0400.PR04NUMACTPLAN AND"
sql = sql & " CI1500.CI01NUMCITA=CI0100.CI01NUMCITA AND"
sql = sql & " CI1500.CI31NUMSOLICIT=CI0100.CI31NUMSOLICIT AND"
sql = sql & " CI2700.CI15NUMFASECITA=CI1500.CI15NUMFASECITA AND"
sql = sql & " CI2700.CI31NUMSOLICIT=CI1500.CI31NUMSOLICIT AND"
sql = sql & " CI2700.CI01NUMCITA=CI1500.CI01NUMCITA);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'CI1500 FASE CITADA********************************
sql = "DELETE FROM CI1500"
sql = sql & " WHERE "
sql = sql & " (CI1500.CI31NUMSOLICIT,"
sql = sql & " CI1500.CI01NUMCITA,"
sql = sql & " CI1500.CI15NUMFASECITA)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " CI1500.CI31NUMSOLICIT,"
sql = sql & " CI1500.CI01NUMCITA,"
sql = sql & " CI1500.CI15NUMFASECITA"
sql = sql & " FROM"
sql = sql & " CI1500,"
sql = sql & " CI0100,"
sql = sql & " PR0400,"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND"
sql = sql & " PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND"
sql = sql & " CI0100.PR04NUMACTPLAN=PR0400.PR04NUMACTPLAN AND"
sql = sql & " CI1500.CI01NUMCITA=CI0100.CI01NUMCITA AND"
sql = sql & " CI1500.CI31NUMSOLICIT=CI0100.CI31NUMSOLICIT);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'CI0100 ACTUACION CITADA****************************
sql = "DELETE FROM CI0100"
sql = sql & " WHERE "
sql = sql & " (CI0100.CI31NUMSOLICIT,"
sql = sql & " CI0100.CI01NUMCITA)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " CI0100.CI31NUMSOLICIT,"
sql = sql & " CI0100.CI01NUMCITA"
sql = sql & " FROM"
sql = sql & " CI0100,"
sql = sql & " PR0400,"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND"
sql = sql & " PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND"
sql = sql & " CI0100.PR04NUMACTPLAN=PR0400.PR04NUMACTPLAN);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'CI3100 SOLICITUD************************************
sql = "DELETE FROM CI3100"
sql = sql & " WHERE"
sql = sql & " CI3100.CI31NUMSOLICIT=?;"

Do While RsNumSolicit.EOF = False
    Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = RsNumSolicit(0)
    QyConsulta.Execute
    QyConsulta.Close
    RsNumSolicit.MoveNext
Loop

RsNumSolicit.Close
QyNumSolicit.Close

'PR0400 ACTUACION PLANIFICADA*************************
sql = "DELETE FROM PR0400"
sql = sql & " WHERE "
sql = sql & " (PR0400.PR04NUMACTPLAN)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " PR0400.PR04NUMACTPLAN"
sql = sql & " FROM"
sql = sql & " PR0400,"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND"
sql = sql & " PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI);"
 
Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'PR0300 ACTUACION PEDIDA*************************
sql = "DELETE FROM PR0300"
sql = sql & " WHERE "
sql = sql & " (PR0300.PR03NUMACTPEDI)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " PR0300.PR03NUMACTPEDI"
sql = sql & " FROM"
sql = sql & " PR0300,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'PR4700 VALORES RESTRICCIONES PETICION*************************
sql = "DELETE FROM PR4700"
sql = sql & " WHERE "
sql = sql & " (PR4700.PR09NUMPETICION,"
sql = sql & " PR4700.AG16CODTIPREST)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " PR4700.PR09NUMPETICION,"
sql = sql & " PR4700.AG16CODTIPREST"
sql = sql & " FROM"
sql = sql & " PR4700,"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA AND"
sql = sql & " PR4700.PR09NUMPETICION=PR0900.PR09NUMPETICION);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'PR0900 PETICION*************************
sql = "DELETE FROM PR0900"
sql = sql & " WHERE "
sql = sql & " (PR0900.PR09NUMPETICION)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " PR0900.PR09NUMPETICION"
sql = sql & " FROM"
sql = sql & " PR0900,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " PR0900.CI21CODPERSONA=CI2200.CI21CODPERSONA);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'AD0400 DEP-RESP-PROC*************************
sql = "DELETE FROM AD0400"
sql = sql & " WHERE "
sql = sql & " (AD0400.AD07CODPROCESO,"
sql = sql & " AD0400.AD04FECINIRESPON)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD0400.AD07CODPROCESO,"
sql = sql & " AD0400.AD04FECINIRESPON"
sql = sql & " FROM"
sql = sql & " AD0400,"
sql = sql & " AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=? AND"
sql = sql & " AD0400.AD07CODPROCESO=AD0700.AD07CODPROCESO);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'AD0500 DEP-RESP-PROC-ASIST*************************
sql = "DELETE FROM AD0500"
sql = sql & " WHERE "
sql = sql & " (AD0500.AD07CODPROCESO,"
sql = sql & " AD0500.AD01CODASISTENCI,"
sql = sql & " AD0500.AD05FECINIRESPON)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD0500.AD07CODPROCESO,"
sql = sql & " AD0500.AD01CODASISTENCI,"
sql = sql & " AD0500.AD05FECINIRESPON"
sql = sql & " FROM"
sql = sql & " AD0500,"
sql = sql & " AD0800,"
sql = sql & " AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=? AND"
sql = sql & " AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO AND"
sql = sql & " AD0500.AD07CODPROCESO=AD0800.AD07CODPROCESO AND"
sql = sql & " AD0500.AD01CODASISTENCI=AD0800.AD01CODASISTENCI);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'AD1100 RESPONSABLE ECON. PROC-ASIST*************************
sql = "DELETE FROM AD1100"
sql = sql & " WHERE "
sql = sql & " (AD1100.AD07CODPROCESO,"
sql = sql & " AD1100.AD01CODASISTENCI,"
sql = sql & " AD1100.AD11FECINICIO)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD1100.AD07CODPROCESO,"
sql = sql & " AD1100.AD01CODASISTENCI,"
sql = sql & " AD1100.AD11FECINICIO"
sql = sql & " FROM"
sql = sql & " AD1100,"
sql = sql & " AD0800,"
sql = sql & " AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=? AND"
sql = sql & " AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO AND"
sql = sql & " AD1100.AD07CODPROCESO=AD0800.AD07CODPROCESO AND"
sql = sql & " AD1100.AD01CODASISTENCI=AD0800.AD01CODASISTENCI);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'AD2500 SITUACION ASISTENCIAS*************************
sql = "DELETE FROM AD2500"
sql = sql & " WHERE "
sql = sql & " (AD2500.AD01CODASISTENCI,"
sql = sql & " AD2500.AD25FECINICIO)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD2500.AD01CODASISTENCI,"
sql = sql & " AD2500.AD25FECINICIO"
sql = sql & " FROM"
sql = sql & " AD2500,"
sql = sql & " AD0100"
sql = sql & " WHERE"
sql = sql & " AD0100.CI21CODPERSONA=? AND"
sql = sql & " AD2500.AD01CODASISTENCI=AD0100.AD01CODASISTENCI);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'AD2300 SITUACION CAMA PROCESO*************************
sql = "DELETE FROM AD2300"
sql = sql & " WHERE "
sql = sql & " (AD2300.AD15CODCAMA,"
sql = sql & " AD2300.AD16FECCAMBIO,"
sql = sql & " AD2300.AD07CODPROCESO)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD2300.AD15CODCAMA,"
sql = sql & " AD2300.AD16FECCAMBIO,"
sql = sql & " AD2300.AD07CODPROCESO"
sql = sql & " FROM"
sql = sql & " AD2300,"
sql = sql & " AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=? AND"
sql = sql & " AD2300.AD07CODPROCESO=AD0700.AD07CODPROCESO);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

'AD1600 SITUACION CAMA*************************
sql = "DELETE FROM AD1600"
sql = sql & " WHERE "
sql = sql & " (AD1600.AD15CODCAMA,"
sql = sql & " AD1600.AD16FECCAMBIO)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD1600.AD15CODCAMA,"
sql = sql & " AD1600.AD16FECCAMBIO"
sql = sql & " FROM"
sql = sql & " AD1600,"
sql = sql & " AD0800,"
sql = sql & " AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=? AND"
sql = sql & " AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO AND"
sql = sql & " AD1600.AD07CODPROCESO=AD0800.AD07CODPROCESO AND"
sql = sql & " AD1600.AD01CODASISTENCI=AD0800.AD01CODASISTENCI);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumHistoria
QyConsulta.Execute
QyConsulta.Close

'AD1500 CAMA*************************
sql = "DELETE FROM AD1500"
sql = sql & " WHERE "
sql = sql & " (AD1500.AD15CODCAMA)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD1500.AD15CODCAMA"
sql = sql & " FROM"
sql = sql & " AD1500,"
sql = sql & " AD0800,"
sql = sql & " AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=? AND"
sql = sql & " AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO AND"
sql = sql & " AD1500.AD07CODPROCESO=AD0800.AD07CODPROCESO AND"
sql = sql & " AD1500.AD01CODASISTENCI=AD0800.AD01CODASISTENCI);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumHistoria
QyConsulta.Execute
QyConsulta.Close

'AD0800 PROCESO-ASISTENCIA*************************
sql = "DELETE FROM AD0800"
sql = sql & " WHERE "
sql = sql & " (AD0800.AD07CODPROCESO,"
sql = sql & " AD0800.AD01CODASISTENCI)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " AD0800.AD07CODPROCESO,"
sql = sql & " AD0800.AD01CODASISTENCI"
sql = sql & " FROM"
sql = sql & " AD0800,"
sql = sql & " AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=? AND"
sql = sql & " AD0800.AD07CODPROCESO=AD0700.AD07CODPROCESO);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumHistoria
QyConsulta.Execute
QyConsulta.Close

'AD0100 ASISTENCIA*************************
sql = "DELETE FROM AD0100"
sql = sql & " WHERE"
sql = sql & " AD0100.CI21CODPERSONA=?;"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumHistoria
QyConsulta.Execute
QyConsulta.Close

'AD0700 PROCESO*************************
sql = "DELETE FROM AD0700"
sql = sql & " WHERE"
sql = sql & " AD0700.CI21CODPERSONA=?;"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumHistoria
QyConsulta.Execute
QyConsulta.Close

'CI1000 DIRECCION PERSONA*************************
sql = "DELETE FROM CI1000"
sql = sql & " WHERE "
sql = sql & " (CI1000.CI21CODPERSONA,"
sql = sql & " CI1000.CI10NUMDIRECCI)"
sql = sql & " IN"
sql = sql & " (SELECT "
sql = sql & " CI1000.CI21CODPERSONA,"
sql = sql & " CI1000.CI10NUMDIRECCI"
sql = sql & " FROM"
sql = sql & " CI1000,"
sql = sql & " CI2200"
sql = sql & " WHERE"
sql = sql & " CI2200.CI21CODPERSONA=? AND"
sql = sql & " CI1000.CI21CODPERSONA=CI2200.CI21CODPERSONA);"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumHistoria
QyConsulta.Execute
QyConsulta.Close

'Se mira si es Persona Relacionada con la Universidad
sql = "SELECT CI2400.CI08CODCOLECTI, CI2400.CI24CODEMPLEAD, CI2400.CI24NUMBENEFIC FROM CI2400,CI2200 "
sql = sql & "WHERE CI2400.CI22NUMHISTORIA = CI2200.CI22NUMHISTORIA AND "
sql = sql & "CI2200.CI21CODPERSONA = ? "

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = lngNumPersona
Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
If Not RsRespuesta.EOF Then
    sql = "UPDATE CI2400 SET CI22NUMHISTORIA = NULL WHERE CI08CODCOLECTI = ? AND "
    sql = sql & "CI24CODEMPLEAD = ? AND CI24NUMBENEFIC = ?"
    Set QyNumSolicit = objApp.rdoConnect.CreateQuery("", sql)
        QyNumSolicit(0) = RsRespuesta(0)
        QyNumSolicit(1) = RsRespuesta(1)
        QyNumSolicit(2) = RsRespuesta(2)
    QyNumSolicit.Execute
    QyNumSolicit.Close
End If
RsRespuesta.Close
QyConsulta.Close

'Se mira si es Asegurado ACUNSA
sql = "SELECT CI0300.CI03NUMPOLIZA, CI0300.CI03NUMASEGURA FROM CI0300,CI2200 WHERE CI0300.CI22NUMHISTORIA = CI2200.CI22NUMHISTORIA AND "
sql = sql & "CI2200.CI21CODPERSONA = ?"
Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
    QyConsulta(0) = lngNumPersona
Set RsRespuesta = QyConsulta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
If Not RsRespuesta.EOF Then
    sql = "UPDATE CI0300 SET CI22NUMHISTORIA = NULL WHERE CI03NUMPOLIZA = ? AND "
    sql = sql & "CI03NUMASEGURA = ?"
    Set QyNumSolicit = objApp.rdoConnect.CreateQuery("", sql)
        QyNumSolicit(0) = RsRespuesta(0)
        QyNumSolicit(1) = RsRespuesta(1)
    QyNumSolicit.Execute
    QyNumSolicit.Close
End If
RsRespuesta.Close
QyConsulta.Close

'CI2100 AVISO*************************
sql = "DELETE FROM CI0400"
sql = sql & " WHERE"
sql = sql & " CI0400.CI21CODPERSONA=?;"

Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
QyConsulta(0) = lngNumPersona
QyConsulta.Execute
QyConsulta.Close

''CI2200 PERSONA FISICA*************************
'SQL = "DELETE FROM CI2200"
'SQL = SQL & " WHERE"
'SQL = SQL & " CI2200.CI22NUMHISTORIA=?;"
'
'Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQL)
'QyConsulta(0) = lngNumHistoria
'QyConsulta.Execute
'QyConsulta.Close
'
''CI2100 PERSONA*************************
'SQL = "DELETE FROM CI2100"
'SQL = SQL & " WHERE"
'SQL = SQL & " CI2100.CI21CODPERSONA=?;"
'
'Set QyConsulta = objApp.rdoConnect.CreateQuery("", SQL)
'QyConsulta(0) = lngCodPersona
'QyConsulta.Execute
'QyConsulta.Close

'\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

Canceltrans:
If err > 0 Then
    borrarPersFis = False
    Exit Function
'    MsgBox "ROLLBACK"
'    objApp.rdoConnect.RollbackTrans
Else
'    objApp.rdoConnect.CommitTrans
End If


End Function
Function bDuplicarDireccion() As Boolean
'***************************************************
'Funcion que nos duplica la direcci�n de la persona f�sica
'para el responsable familiar y/o economico en base a
'la direccion de la persona f�sica que estamos tratando
'insertamos en
'***************************************************

  Dim sMsg          As String
  Dim bResp         As String
  Dim sStmSql       As String
  Dim qryUPD        As rdoQuery
    
    bDuplicarDireccion = False
        
    On Error Resume Next
    objApp.rdoConnect.BeginTrans
    'Realizamos el insert de la direcci�n
    'para datos responsable
    sStmSql = "INSERT INTO CI1000 "
    sStmSql = sStmSql + "(CI21CODPERSONA, "
    sStmSql = sStmSql + "CI10NUMDIRECCI, "
    sStmSql = sStmSql + "CI16CODLOCALID, "
    sStmSql = sStmSql + "CI17CODMUNICIP, "
    sStmSql = sStmSql + "CI26CODPROVI, "
    sStmSql = sStmSql + "CI19CODPAIS, "
    sStmSql = sStmSql + "CI10CALLE, "
    sStmSql = sStmSql + "CI10PORTAL, "
    sStmSql = sStmSql + "CI10RESTODIREC, "
    sStmSql = sStmSql + "CI07CODPOSTAL, "
    sStmSql = sStmSql + "CI10DESLOCALID, "
    sStmSql = sStmSql + "CI10DESPROVI, "
    sStmSql = sStmSql + "CI10TELEFONO, "
    sStmSql = sStmSql + "CI10OBSERVACIO, "
    sStmSql = sStmSql + "CI10FECINIVALID, "
    sStmSql = sStmSql + "CI10FECFINVALID, "
    sStmSql = sStmSql + "CI10INDDIRPRINC, "
    sStmSql = sStmSql + "SG02COD_ADD, "
    sStmSql = sStmSql + "CI10FECADD, "
    sStmSql = sStmSql + "SG02COD_UPD, "
    sStmSql = sStmSql + "CI10FECUPD, "
    sStmSql = sStmSql + "CI10FAX) "
    sStmSql = sStmSql + "SELECT "
    sStmSql = sStmSql + txtText1(0) & ", "
    sStmSql = sStmSql + "1 , "
    sStmSql = sStmSql + "CI16CODLOCALID, "
    sStmSql = sStmSql + "CI17CODMUNICIP, "
    sStmSql = sStmSql + "CI26CODPROVI, "
    sStmSql = sStmSql + "CI19CODPAIS, "
    sStmSql = sStmSql + "CI10CALLE, "
    sStmSql = sStmSql + "CI10PORTAL, "
    sStmSql = sStmSql + "CI10RESTODIREC, "
    sStmSql = sStmSql + "CI07CODPOSTAL, "
    sStmSql = sStmSql + "CI10DESLOCALID, "
    sStmSql = sStmSql + "CI10DESPROVI, "
    sStmSql = sStmSql + "CI10TELEFONO, "
    sStmSql = sStmSql + "CI10OBSERVACIO, "
    sStmSql = sStmSql + "CI10FECINIVALID, "
    sStmSql = sStmSql + "CI10FECFINVALID, "
    sStmSql = sStmSql + "CI10INDDIRPRINC, "
    sStmSql = sStmSql + "SG02COD_ADD, "
    sStmSql = sStmSql + "CI10FECADD, "
    sStmSql = sStmSql + "SG02COD_UPD, "
    sStmSql = sStmSql + "CI10FECUPD, "
    sStmSql = sStmSql + "CI10FAX "
    sStmSql = sStmSql + "FROM CI1000 "
    sStmSql = sStmSql + "WHERE CI21CODPERSONA = " & lCodigoIgual & " "
    sStmSql = sStmSql + "AND CI10INDDIRPRINC = -1 "
    
    Set qryUPD = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUPD.Execute

    If err <> 0 Then
        MsgBox err.Description, vbOKOnly
    Else
        bDuplicarDireccion = True
    End If
    qryUPD.Close
    
End Function

Public Function ValidarDNI(ByVal strDni As String) As String

  Dim strAux As String
  Dim intPos As Long
  Dim flag As Boolean

  flag = True
  
  While flag = True
      intPos = InStr(1, strDni, ".")
      If intPos > 0 Then
        strAux = Left(strDni, intPos - 1)
        strDni = strAux & Right(strDni, Len(strDni) - intPos)
      Else
         flag = False
      End If
  Wend
  
  flag = True
  
  While flag = True
      intPos = InStr(1, strDni, ",")
      If intPos > 0 Then
        strAux = Left(strDni, intPos - 1)
        strDni = strAux & Right(strDni, Len(strDni) - intPos)
      Else
         flag = False
      End If
  Wend

  flag = True
  
  While flag = True
      intPos = InStr(1, strDni, "-")
      If intPos > 0 Then
        strAux = Left(strDni, intPos - 1)
        strDni = strAux & Right(strDni, Len(strDni) - intPos)
      Else
         flag = False
      End If
  Wend

  ValidarDNI = strDni

End Function
Public Function ValidaDigControlDNI(ByVal strDni As String) As String

  Dim strAux As String
  Dim lngDni As Long
  Dim vntArrLetra, vntLetra
  Dim intIndex As Integer
  
  vntArrLetra = Array("T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", _
                      "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", _
                      "C", "K", "E")
  
    strAux = Left(strDni, Len(strDni) - 1)
    lngDni = Val(strAux)
    intIndex = (lngDni Mod 23)
    vntLetra = vntArrLetra(intIndex)
    ValidaDigControlDNI = CStr(vntLetra)
  
End Function
Public Function ValidaDigControlNSS(ByVal dblNp As Double, _
                                    ByVal dblNs As Double, _
                                    ByVal dblDs As Double) As Boolean

Dim dblAux As Double
Dim dblDec As Double
Dim intPos As Long
Dim strAux As String
Dim lngResto As Long

   'If dblNs < 100000 Then
   '   dblAux = dblNp * 100000 + dblNs
   'If dblNs < 1000000 Then
   '   dblAux = dblNp * 1000000 + dblNs
   If dblNs < 10000000 Then
      dblAux = dblNp * 10000000 + dblNs
   Else
      dblAux = dblNp * 100000000 + dblNs
   End If
                
   
   dblAux = dblAux / 97
   strAux = CStr(dblAux)
   intPos = InStr(1, strAux, ",")
   If intPos > 0 Then
     strAux = "0," & Right(strAux, Len(strAux) - intPos)
   End If
   dblDec = CDbl(strAux)
   lngResto = CLng(dblDec * 97)

   'dblAux = dblDig - CDbl(lngResto)
   
   'If dblDig - CDbl(dblAux) <> dblDs Then
   If CDbl(lngResto) <> dblDs Then
    ValidaDigControlNSS = False
   Else
    ValidaDigControlNSS = True
   End If
   
End Function

Public Function GetNewCode(strSQL As String) As Long
 'Creaci�n de variables recordset
  Dim rdoCodigo As rdoResultset
  Dim rdoQueryCod As rdoQuery
  
 'Apertura del cursor
  Set rdoQueryCod = objApp.rdoConnect.CreateQuery("", strSQL)

 'Establezco el n� de registros a recuperar a 1
  rdoQueryCod.MaxRows = 1
  Set rdoCodigo = rdoQueryCod.OpenResultset(rdOpenKeyset)
  
 'Devuelvo el nuevo n� de c�digo
  If rdoCodigo.RowCount = 0 Then
    GetNewCode = 1
  Else
    If IsNull(rdoCodigo(0)) Then
      GetNewCode = 1
    Else
      GetNewCode = rdoCodigo(0) + 1
    End If
  End If
 
 'Cierro cursores
  rdoCodigo.Close
  rdoQueryCod.Close

End Function

