VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "comct232.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmAsoProcAsis 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Admisi�n. Asociar Proceso/Asistencia"
   ClientHeight    =   8490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10620
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8490
   ScaleWidth      =   10620
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Asociar Proceso/Asistencia"
      Height          =   375
      Index           =   4
      Left            =   8160
      TabIndex        =   72
      Top             =   4920
      Width           =   2175
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Fin Asistencia"
      Height          =   285
      Index           =   6
      Left            =   9000
      TabIndex        =   71
      Top             =   2760
      Width           =   1125
   End
   Begin VB.Frame Frame1 
      Caption         =   "DOCUMENTACION"
      ForeColor       =   &H00800000&
      Height          =   2895
      Left            =   0
      TabIndex        =   62
      Top             =   5520
      Width           =   10455
      Begin VB.CommandButton cmdEliminarDoc 
         Caption         =   "Eliminar"
         Height          =   285
         Left            =   120
         TabIndex        =   75
         Top             =   2520
         Width           =   1125
      End
      Begin VB.CommandButton cmdAsoDoc 
         Caption         =   "Asociar"
         Enabled         =   0   'False
         Height          =   285
         Left            =   9120
         TabIndex        =   74
         Top             =   2520
         Width           =   1125
      End
      Begin VB.CommandButton cmdGuardarDoc 
         Caption         =   "Guardar"
         Enabled         =   0   'False
         Height          =   285
         Left            =   7920
         TabIndex        =   66
         Top             =   2520
         Width           =   1125
      End
      Begin VB.CommandButton cmdNuevaDoc 
         Caption         =   "Nuevo"
         Enabled         =   0   'False
         Height          =   285
         Left            =   6720
         TabIndex        =   65
         Top             =   2520
         Width           =   1125
      End
      Begin VB.TextBox txtMensaje 
         BackColor       =   &H80000000&
         Height          =   375
         Left            =   1800
         TabIndex        =   64
         Top             =   240
         Width           =   8535
      End
      Begin SSDataWidgets_B.SSDBGrid grdDoc 
         Height          =   1725
         Left            =   120
         TabIndex        =   69
         Top             =   720
         Width           =   10290
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         stylesets.count =   3
         stylesets(0).Name=   "Obligatorio"
         stylesets(0).ForeColor=   0
         stylesets(0).BackColor=   16776960
         stylesets(0).Picture=   "AD1020.frx":0000
         stylesets(1).Name=   "Editable"
         stylesets(1).BackColor=   16777215
         stylesets(1).Picture=   "AD1020.frx":001C
         stylesets(2).Name=   "Locked"
         stylesets(2).BackColor=   12632256
         stylesets(2).HasFont=   -1  'True
         BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         stylesets(2).Picture=   "AD1020.frx":0038
         AllowUpdate     =   0   'False
         SelectTypeRow   =   1
         SelectByCell    =   -1  'True
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   12
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cDoc"
         Columns(0).Name =   "cDoc"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "cTiDoc"
         Columns(1).Name =   "cTiDoc"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2275
         Columns(2).Caption=   "Tipo documento"
         Columns(2).Name =   "TiDoc"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).HasBackColor=   -1  'True
         Columns(2).BackColor=   12632256
         Columns(3).Width=   1852
         Columns(3).Caption=   "Tipo Volante"
         Columns(3).Name =   "TiVolante"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   1
         Columns(3).HasBackColor=   -1  'True
         Columns(3).BackColor=   12632256
         Columns(4).Width=   3704
         Columns(4).Caption=   "Descripci�n"
         Columns(4).Name =   "Descripci�n"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).HasBackColor=   -1  'True
         Columns(4).BackColor=   12632256
         Columns(5).Width=   3200
         Columns(5).Caption=   "N� de Volante"
         Columns(5).Name =   "N� de Volante"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).HasBackColor=   -1  'True
         Columns(5).BackColor=   12632256
         Columns(6).Width=   2355
         Columns(6).Caption=   "F. Ini. Vol/Doc"
         Columns(6).Name =   "Fecha"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(6).Locked=   -1  'True
         Columns(6).HasBackColor=   -1  'True
         Columns(6).BackColor=   12632256
         Columns(7).Width=   2619
         Columns(7).Caption=   "F. Fin Validez"
         Columns(7).Name =   "FVolante"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(7).Locked=   -1  'True
         Columns(7).HasBackColor=   -1  'True
         Columns(7).BackColor=   12632256
         Columns(8).Width=   3200
         Columns(8).Caption=   "Departamento"
         Columns(8).Name =   "Departamento"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(8).HasBackColor=   -1  'True
         Columns(8).BackColor=   12632256
         Columns(9).Width=   3200
         Columns(9).Caption=   "Doctor"
         Columns(9).Name =   "Doctor"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(9).HasBackColor=   -1  'True
         Columns(9).BackColor=   12632256
         Columns(10).Width=   3200
         Columns(10).Visible=   0   'False
         Columns(10).Caption=   "cDpto"
         Columns(10).Name=   "cDpto"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   8
         Columns(10).FieldLen=   256
         Columns(11).Width=   3200
         Columns(11).Visible=   0   'False
         Columns(11).Caption=   "cDoctor"
         Columns(11).Name=   "cDoctor"
         Columns(11).DataField=   "Column 11"
         Columns(11).DataType=   8
         Columns(11).FieldLen=   256
         _ExtentX        =   18150
         _ExtentY        =   3043
         _StockProps     =   79
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateFecIni 
         Height          =   285
         Left            =   5160
         TabIndex        =   70
         Tag             =   "Fecha de Inicio|Fecha de Inicio "
         Top             =   960
         Width           =   1710
         _Version        =   65537
         _ExtentX        =   3016
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBDropDown ddrTiVolante 
         Height          =   1215
         Left            =   720
         TabIndex        =   67
         Top             =   840
         Width           =   2265
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cTiVolante"
         Columns(0).Name =   "cTiVolante"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1958
         Columns(1).Caption=   "TiVolante"
         Columns(1).Name =   "TiVolante"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3995
         _ExtentY        =   2143
         _StockProps     =   77
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBDropDown ddrTipDocumento 
         Height          =   1215
         Left            =   3000
         TabIndex        =   68
         Top             =   840
         Width           =   2265
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "cInspeccion"
         Columns(0).Name =   "cInspeccion"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1693
         Columns(1).Caption=   "Inspeccion"
         Columns(1).Name =   "Inspeccion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3995
         _ExtentY        =   2143
         _StockProps     =   77
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label1 
         Caption         =   "DOC. NECESARIA:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   63
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "PROCESOS"
      ForeColor       =   &H00800000&
      Height          =   2055
      Index           =   1
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9255
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Nuevo"
         Height          =   375
         Index           =   0
         Left            =   8280
         TabIndex        =   2
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Asociar"
         Height          =   375
         Index           =   2
         Left            =   8280
         TabIndex        =   1
         Top             =   720
         Width           =   855
      End
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   1695
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   8115
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   7
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         MaxSelectedRows =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   7
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "N� Proceso"
         Columns(0).Name =   "N� Proceso"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3995
         Columns(1).Caption=   "Descripcion"
         Columns(1).Name =   "Descripcion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   2884
         Columns(2).Caption=   "Fecha Inicio"
         Columns(2).Name =   "Dpto Responsable"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3122
         Columns(3).Caption=   "Dpto. Responsable"
         Columns(3).Name =   "Dr. Responsable"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Style=   3
         Columns(4).Width=   3200
         Columns(4).Caption=   "Dr. Responsable"
         Columns(4).Name =   "Fecha de Incio"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Style=   3
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "CODDPTO"
         Columns(5).Name =   "CODDPTO"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "SG02COD"
         Columns(6).Name =   "SG02COD"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         _ExtentX        =   14314
         _ExtentY        =   2990
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "PROCESO/ASISTENCIA"
      ForeColor       =   &H00800000&
      Height          =   2175
      Index           =   0
      Left            =   0
      TabIndex        =   21
      Top             =   3240
      Width           =   10455
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   6
         Left            =   7200
         Locked          =   -1  'True
         TabIndex        =   36
         TabStop         =   0   'False
         Tag             =   "Nombre M�dico Remitente|Nombre del M�dico Remitente"
         Top             =   1305
         Width           =   2535
      End
      Begin VB.CommandButton cmdResponsable 
         Caption         =   "Buscar "
         Height          =   285
         Index           =   0
         Left            =   6360
         TabIndex        =   29
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton cmdResponsable 
         Caption         =   "Consultar"
         Height          =   285
         Index           =   1
         Left            =   7320
         TabIndex        =   28
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton cmdResponsable 
         Caption         =   "Crear "
         Enabled         =   0   'False
         Height          =   285
         Index           =   2
         Left            =   8280
         TabIndex        =   22
         Top             =   600
         Width           =   855
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "Contestar en un Informe M�dico"
         Height          =   435
         Index           =   0
         Left            =   6360
         TabIndex        =   37
         Tag             =   "Pendiente de Volante|Pendiente de Volante"
         Top             =   840
         Value           =   1  'Checked
         Width           =   2625
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   5
         Left            =   6240
         MaxLength       =   7
         TabIndex        =   35
         Tag             =   "M�dico Remitente|M�dico Remitente"
         Top             =   1305
         Width           =   915
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   4
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   34
         TabStop         =   0   'False
         Tag             =   "Nombre Persona Env�o Informe|Nombre de la Persona Env�o Informe"
         Top             =   1305
         Width           =   2415
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   3
         Left            =   960
         MaxLength       =   7
         TabIndex        =   33
         Tag             =   "Persona Env�o Informe|C�digo de Persona Env�o Informe"
         Top             =   1305
         Width           =   915
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   20
         Left            =   1680
         MaxLength       =   30
         TabIndex        =   32
         Tag             =   "Observaciones|Observaciones"
         Top             =   5400
         Width           =   7815
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   285
         Index           =   7
         Left            =   8400
         MaxLength       =   4
         TabIndex        =   31
         Tag             =   "N�mero de Caso|N�mero de Caso en el Host"
         Top             =   240
         Width           =   765
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "Pendiente de Volante"
         Height          =   435
         Index           =   1
         Left            =   5040
         TabIndex        =   30
         Tag             =   "Pendiente de Volante|Pendiente de Volante"
         Top             =   1560
         Width           =   1935
      End
      Begin VB.CommandButton cmdBuscarMedico 
         Caption         =   "..."
         Height          =   285
         Index           =   0
         Left            =   9840
         TabIndex        =   27
         Top             =   1305
         Width           =   375
      End
      Begin VB.CommandButton cmdBuscarMedico 
         Caption         =   "..."
         Height          =   285
         Index           =   1
         Left            =   4365
         TabIndex        =   26
         Top             =   1305
         Width           =   375
      End
      Begin VB.TextBox txtHora 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   285
         Index           =   1
         Left            =   2760
         MaxLength       =   2
         TabIndex        =   25
         Tag             =   "Hora"
         Top             =   225
         Width           =   390
      End
      Begin VB.TextBox txtMinuto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFF00&
         Height          =   285
         HelpContextID   =   3
         Index           =   1
         Left            =   3390
         MaxLength       =   2
         TabIndex        =   24
         Tag             =   "Minutos"
         Top             =   225
         Width           =   390
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   960
         MaxLength       =   100
         TabIndex        =   23
         Tag             =   "Persona Env�o Informe|C�digo de Persona Env�o Informe"
         Top             =   1680
         Width           =   4035
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   285
         Index           =   1
         Left            =   4800
         TabIndex        =   38
         Tag             =   "Tipo de Paciente|Tipo de Paciente"
         Top             =   240
         Width           =   1500
         DataFieldList   =   "Column 1"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2646
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   285
         Index           =   2
         Left            =   960
         TabIndex        =   39
         Tag             =   "Fecha de Inicio|Fecha de Inicio "
         Top             =   240
         Width           =   1710
         _Version        =   65537
         _ExtentX        =   3016
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   285
         Index           =   4
         Left            =   960
         TabIndex        =   40
         Tag             =   "C�digo Tipo Econ�mico|C�digo Tipo Econ�mico"
         Top             =   600
         Width           =   660
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5609
         Columns(1).Caption=   "Descripci�n "
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1164
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   285
         Index           =   5
         Left            =   2400
         TabIndex        =   41
         Tag             =   "C�digo Entidad|C�digo Entidad"
         Top             =   600
         Width           =   660
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1164
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   285
         Index           =   2
         Left            =   960
         TabIndex        =   42
         Tag             =   "Departamento|C�digo Departamento"
         Top             =   960
         Width           =   2085
         DataFieldList   =   "Column 1"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3678
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   285
         Index           =   3
         Left            =   4080
         TabIndex        =   43
         Tag             =   "Departamento|C�digo Departamento"
         Top             =   960
         Width           =   2205
         DataFieldList   =   "Column 1"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3889
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   285
         Index           =   6
         Left            =   4080
         TabIndex        =   44
         Tag             =   "C�digo Entidad|C�digo Entidad"
         Top             =   600
         Width           =   2220
         DataFieldList   =   "Column 1"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3916
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin ComCtl2.UpDown UpDownMinuto 
         Height          =   285
         Index           =   1
         Left            =   3780
         TabIndex        =   45
         Top             =   225
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   327681
         OrigLeft        =   6360
         OrigTop         =   360
         OrigRight       =   6600
         OrigBottom      =   690
         Increment       =   5
         Max             =   55
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDownHora 
         Height          =   285
         Index           =   1
         Left            =   3150
         TabIndex        =   46
         Top             =   225
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   327681
         OrigLeft        =   5760
         OrigTop         =   360
         OrigRight       =   6000
         OrigBottom      =   690
         Max             =   23
         Enabled         =   -1  'True
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fec. Inicio"
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   58
         Top             =   240
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "M�d. Remitente"
         Height          =   195
         Index           =   17
         Left            =   5040
         TabIndex        =   57
         Top             =   1320
         Width           =   1125
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Env�o Infor."
         Height          =   195
         Index           =   19
         Left            =   120
         TabIndex        =   56
         Top             =   1320
         Width           =   840
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Pac."
         Height          =   195
         Index           =   18
         Left            =   4080
         TabIndex        =   55
         Top             =   240
         Width           =   690
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   20
         Left            =   240
         TabIndex        =   54
         Top             =   5400
         Width           =   1335
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Num. Caso"
         Height          =   195
         Index           =   12
         Left            =   7560
         TabIndex        =   53
         Top             =   240
         Width           =   780
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Entidad"
         Height          =   195
         Index           =   14
         Left            =   1800
         TabIndex        =   52
         Top             =   600
         Width           =   540
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "T. Eco."
         Height          =   195
         Index           =   1
         Left            =   360
         TabIndex        =   51
         Top             =   600
         Width           =   525
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Responsable"
         Height          =   195
         Index           =   3
         Left            =   3120
         TabIndex        =   50
         Top             =   600
         Width           =   930
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Dr .Resp."
         Height          =   195
         Index           =   13
         Left            =   3360
         TabIndex        =   49
         Top             =   960
         Width           =   675
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Dpto Resp."
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   48
         Top             =   960
         Width           =   810
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Obser.:"
         Height          =   195
         Index           =   10
         Left            =   360
         TabIndex        =   47
         Top             =   1680
         Width           =   510
      End
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Salir"
      Height          =   375
      Index           =   5
      Left            =   9480
      TabIndex        =   20
      Top             =   120
      Width           =   975
   End
   Begin VsOcxLib.VideoSoftAwk vsw 
      Left            =   -60
      Top             =   120
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   " "
   End
   Begin VB.CommandButton cmdDocumentacion 
      Caption         =   "Aportar Documentaci�n"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   9360
      TabIndex        =   19
      Top             =   1680
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "ASISTENCIA"
      ForeColor       =   &H00800000&
      Height          =   975
      Index           =   2
      Left            =   0
      TabIndex        =   4
      Top             =   2160
      Width           =   10455
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H0000FFFF&
         Height          =   285
         HelpContextID   =   40101
         Index           =   1
         Left            =   960
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   60
         Tag             =   "Asistencia|C�digo de Asistencia"
         Top             =   240
         Width           =   1275
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   285
         HelpContextID   =   30101
         Index           =   2
         Left            =   5640
         Locked          =   -1  'True
         TabIndex        =   59
         Tag             =   "N� de  Cama|N� de  Cama"
         Top             =   600
         Width           =   1020
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Nueva"
         Height          =   285
         Index           =   3
         Left            =   9000
         TabIndex        =   18
         Top             =   240
         Width           =   1125
      End
      Begin VB.CommandButton cmdCama 
         Caption         =   "Asignar Cama"
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   6840
         TabIndex        =   14
         Top             =   600
         Width           =   1125
      End
      Begin VB.TextBox txtHora 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   6720
         MaxLength       =   2
         TabIndex        =   6
         Tag             =   "Hora"
         Top             =   240
         Width           =   390
      End
      Begin VB.TextBox txtMinuto 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   285
         HelpContextID   =   3
         Index           =   0
         Left            =   7320
         MaxLength       =   2
         TabIndex        =   5
         Tag             =   "Minutos"
         Top             =   255
         Width           =   390
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   285
         Index           =   0
         Left            =   3240
         TabIndex        =   7
         Tag             =   "Fecha de Inicio|Fecha de Inicio "
         Top             =   240
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   12632256
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         Mask            =   2
         StartofWeek     =   2
      End
      Begin ComCtl2.UpDown UpDownMinuto 
         Height          =   330
         Index           =   0
         Left            =   7710
         TabIndex        =   8
         Top             =   210
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   6360
         OrigTop         =   360
         OrigRight       =   6600
         OrigBottom      =   690
         Increment       =   5
         Max             =   55
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDownHora 
         Height          =   330
         Index           =   0
         Left            =   7080
         TabIndex        =   9
         Top             =   195
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   5760
         OrigTop         =   360
         OrigRight       =   6000
         OrigBottom      =   690
         Max             =   23
         Enabled         =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   285
         Index           =   1
         Left            =   960
         TabIndex        =   15
         Tag             =   "Fecha Previsi�n Alta|Fecha Previsi�n Alta"
         Top             =   600
         Width           =   1500
         _Version        =   65537
         _ExtentX        =   2646
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   12632256
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
         Height          =   285
         Index           =   0
         Left            =   3240
         TabIndex        =   61
         Tag             =   "C�digo Tipo Asistencia|C�digo Tipo Asistencia"
         Top             =   600
         Width           =   1620
         DataFieldList   =   "Column 1"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   1508
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5609
         Columns(1).Caption=   "Descripci�n "
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   2857
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N� Cama"
         Height          =   195
         Index           =   6
         Left            =   4920
         TabIndex        =   17
         Top             =   600
         Width           =   630
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Prev. Alta"
         Height          =   195
         Index           =   4
         Left            =   120
         TabIndex        =   16
         Top             =   720
         Width           =   690
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tip.Asis"
         Height          =   195
         Index           =   0
         Left            =   2640
         TabIndex        =   13
         Top             =   600
         Width           =   555
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Inicio"
         Height          =   195
         Index           =   8
         Left            =   2280
         TabIndex        =   12
         Top             =   360
         Width           =   870
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Asistencia"
         Height          =   195
         Index           =   74
         Left            =   150
         TabIndex        =   11
         Top             =   360
         Width           =   720
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hora de Inicio"
         Height          =   195
         Index           =   5
         Left            =   5640
         TabIndex        =   10
         Top             =   360
         Width           =   990
      End
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateFecFin 
      Height          =   285
      Left            =   6360
      TabIndex        =   73
      Tag             =   "Fecha de Inicio|Fecha de Inicio "
      Top             =   6600
      Width           =   1710
      _Version        =   65537
      _ExtentX        =   3016
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      Mask            =   2
      StartofWeek     =   2
   End
End
Attribute VB_Name = "frmAsoProcAsis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
''Dim lngActPlan As Long
''Dim lngActPedi As Long
Dim strActPedidas As String
Dim strActPlan As String
Dim lngPersona As Long
Dim lngProceso As Long
Dim lngAsistencia As Long
Dim strHistoria As String
Dim intContcaso As Integer
Dim strDptoSol As String
Dim strDrSol As String
Const strDptosLab As String = "201,202,203,219,221,222,224,225,208"
Dim blnCreandoNuevo As Boolean
Dim blnCreandoAsistencia As Boolean
Dim blnHayPA As Boolean





Private Sub cboDBCombo1_CloseUp(Index As Integer)
Dim rs As rdoResultset
Dim sql As String
Dim qry As rdoQuery

Select Case Index
Case 0 'Tipo de asistencia determina el tipo de paciente
 pCombo1
Case 2
  cboDBCombo1(3).RemoveAll
  cboDBCombo1(3).Text = ""
  If cboDBCombo1(2).Columns(0).Value <> "" Then
    If cboDBCombo1(2).Columns(0).Value = constDPTO_URGENCIAS Then
        cboDBCombo1(3).AddItem constUSUARIO_PASTRANA & ";" & constDESUSUARIO_PASTRANA
        cboDBCombo1(3).AddItem constUSUARIO_CCU & ";" & constDESUSUARIO_CCU
    Else
        sql = "SELECT AD0300.SG02COD,'Dr. '||SG02APE1||' '||SG02APE2||' '||SG02NOM DOCTOR " & _
             "FROM SG0200,AD0300 " & _
             "WHERE AD0300.AD02CODDPTO = " & cboDBCombo1(2).Columns(0).Value & " " & _
             "AND AD0300.SG02COD=SG0200.SG02COD " & _
             "AND SYSDATE < NVL(SG0200.SG02FECDES,'01/01/2100') " & _
             "AND SYSDATE < NVL(AD0300.AD03FECFIN,'01/01/2100') " & _
             "AND SG0200.AD30CODCATEGORIA = 1 "

             
         Set rs = objApp.rdoConnect.OpenResultset(sql)
         Do While Not rs.EOF
             cboDBCombo1(3).AddItem rs!SG02COD & ";" & rs!DOCTOR
             rs.MoveNext
         Loop
         rs.Close
    End If
End If
Case 4
    cboDBCombo1(5).Text = ""
    cboDBCombo1(6).Text = ""
    txtMensaje = ""
    Call pCombo4

Case 5
    Call pCombo5
Case 6
    Call pCombo6
End Select
End Sub
Private Sub cmdAsoDoc_Click()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset


If grdDoc.SelBookmarks.Count = 0 Then
    MsgBox "No hay ningun Documento seleccionado", vbExclamation, Me.Caption
Else
    If grdDoc.Columns("cTiDoc").CellValue(grdDoc.SelBookmarks(0)) = const_VOLANTE Then
        sql = "SELECT COUNT(*) FROM AD1800, AD4300 WHERE "
        sql = sql & " AD4300.CI21CODPERSONA = ? "
        sql = sql & " AND AD4300.AD01CODASISTENCI = ? "
        sql = sql & " AND AD4300.AD07CODPROCESO = ? "
        sql = sql & " AND AD4300.CI21CODPERSONA = AD1800.CI21CODPERSONA"
        sql = sql & " AND AD4300.AD18CODDOCUMENTO = AD1800.AD18CODDOCUMENTO"
        sql = sql & " AND AD1800.AD19CODTIPODOCUM = " & const_VOLANTE
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngPersona
            qry(1) = txtText1(1).Text
            qry(2) = SSDBGrid1.Columns(0).Text
        Set rs = qry.OpenResultset()
        If rs(0) > 1 Then
            MsgBox "Este Asistencia ya tiene un volante para el responsable seleccionado", vbExclamation, Me.Caption
            Exit Sub
        End If
    End If
        pAsocDoc
    End If
End Sub

Private Sub cmdBuscarMedico_Click(Index As Integer)
Call objSecurity.LaunchProcess("AD2127")
Select Case Index
    Case 0
        If objPipe.PipeExist("PERSONA") Then
            txtText1(5).Text = objPipe.PipeGet("PERSONA")
            Call pLlenarTxt(6)
        End If
    Case 1
        If objPipe.PipeExist("PERSONA") Then
            txtText1(3).Text = objPipe.PipeGet("PERSONA")
            Call pLlenarTxt(4)
        End If
End Select
            
End Sub

Private Sub cmdCommand1_Click(Index As Integer)
Dim vntData(3) As Variant
'cmdCommand1(Index).Enabled = False
Dim sql$, rs As rdoResultset

Select Case Index

    Case 0 'nuevo proceso
        blnCreandoNuevo = True
        Call pCrearNewProc
        Call pVaciarProcAsis
        cmdCommand1(0).Enabled = False
    Case 2

        If fblnAsociarProceso = True Then
            lngProceso = SSDBGrid1.Columns(0).Text
            SSDBGrid1.AllowUpdate = False
            SSDBGrid1.RemoveAll
            Screen.MousePointer = vbHourglass
            Call pInicialiarProc
            Screen.MousePointer = vbDefault
            blnCreandoNuevo = False
        Else
            cmdCommand1(Index).Enabled = True
        End If


    Case 3 'Asistencia Nueva
         blnCreandoAsistencia = True
         Call pAbriAsitencia
         Call pLlenarCombo(1)
         fraFrame1(0).Enabled = True
    Case 4 'ASOCIAR PROCESO ASISTENCIA
       ' cmdCommand1(5).Enabled = False
       Err = 0
        
        If fblnAsoPA = True Then
            cmdNuevaDoc.Enabled = True
            cmdAsoDoc.Enabled = True
            cmdCommand1(4).Enabled = False
            cmdCommand1(5).Enabled = True
            cmdCommand1(6).Enabled = False
            fraFrame1(0).Enabled = False
            fraFrame1(1).Enabled = False
            fraFrame1(2).Enabled = False
        Else
          cmdCommand1(Index).Enabled = True
          cmdCommand1(5).Enabled = True
          cmdCommand1(6).Enabled = True
        End If
        
        
    Case 5
        Unload Me
    Case 6
      'Abre la form de fin de asistencia pasando codigo persona
        Call objSecurity.LaunchProcess("AD0211", lngPersona)
       Screen.MousePointer = vbHourglass
        Call pInicialiarProc
        cmdCommand1(5).Enabled = True
        'POINTER
        Screen.MousePointer = vbDefault
        blnHayPA = False

End Select
End Sub

Private Sub cmdDocumentacion_Click(Index As Integer)
Dim vntData(0 To 2)
vntData(0) = lngPersona
vntData(1) = txtText1(1).Text
vntData(2) = SSDBGrid1.Columns(0).Value
'Call objPipe.PipeSet("Persona", lngPersona)
'Call objPipe.PipeSet("Asistencia", txtText1(1).Text)
'Call objPipe.PipeSet("Proceso", SSDBGrid1.Columns(0).Value)
'Call objPipe.PipeSet("DeDondeViene", 1)
Call objSecurity.LaunchProcess("AD2501", vntData)
'Call objPipe.PipeRemove("DeDondeViene")
'Call objPipe.PipeRemove("Persona")
'Call objPipe.PipeRemove("Asistencia")
'Call objPipe.PipeRemove("Proceso")
End Sub

Private Sub cmdEliminarDoc_Click()
If grdDoc.SelBookmarks.Count = 0 Then
    MsgBox "No hay ningun Documento seleccionado", vbExclamation, Me.Caption
Else
    peliminar
End If
End Sub

Private Sub cmdGuardarDoc_Click()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

    If Trim(grdDoc.Columns("Tipo Documento").Text) = "" Then
        MsgBox "Introduzca el tipo de Documentacion", vbExclamation, Me.Caption
        Exit Sub
    End If
    If Trim(grdDoc.Columns("F. Ini. Vol/Doc").Text) = "" Then
        MsgBox "Introduzca la fecha de inicio del volante", vbExclamation, Me.Caption
        Exit Sub
    End If
    If Trim(grdDoc.Columns("N� de Volante").Text) = "" And _
     ddrTipDocumento.Columns(0).Value = const_VOLANTE Then
        MsgBox "Introduzca el n�mero de volante", vbExclamation, Me.Caption
        Exit Sub
    End If
    If ddrTipDocumento.Columns(0).Value = const_VOLANTE Then
        sql = "SELECT COUNT(*) FROM AD1800, AD4300 WHERE "
        sql = sql & " AD4300.CI21CODPERSONA = ? "
        sql = sql & " AND AD4300.AD01CODASISTENCI = ? "
        sql = sql & " AND AD4300.AD07CODPROCESO = ? "
        sql = sql & " AND AD4300.CI21CODPERSONA = AD1800.CI21CODPERSONA"
        sql = sql & " AND AD4300.AD18CODDOCUMENTO = AD1800.AD18CODDOCUMENTO"
        sql = sql & " AND AD1800.AD19CODTIPODOCUM = " & const_VOLANTE
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngPersona
            qry(1) = txtText1(1).Text
            qry(2) = SSDBGrid1.Columns(0).Text
        Set rs = qry.OpenResultset()
        If rs(0) > 0 Then
            MsgBox "Este Asistencia ya tiene un volante el Responsable seleccionado", vbExclamation, Me.Caption
            Exit Sub
        End If
    End If
    pGuardarDocumentacion
    grdDoc.AllowUpdate = False
End Sub

Private Sub cmdNuevaDoc_Click()
grdDoc.AddNew
grdDoc.AllowUpdate = True

grdDoc.Columns("Departamento").Locked = True
grdDoc.Columns("Doctor").Locked = True
Call grdDoc.Columns("F. Ini. Vol/Doc").CellStyleSet("Obligatorio", grdDoc.Row)
Call grdDoc.Columns("F. Fin Validez").CellStyleSet("Editable", grdDoc.Row)
Call grdDoc.Columns("Tipo documento").CellStyleSet("Obligatorio", grdDoc.Row)
Call grdDoc.Columns("Tipo Volante").CellStyleSet("Editable", grdDoc.Row)
Call grdDoc.Columns("Descripci�n").CellStyleSet("Editable", grdDoc.Row)
grdDoc.Columns("N� de Volante").Locked = True
cmdNuevaDoc.Enabled = False
cmdAsoDoc.Enabled = False
cmdGuardarDoc.Enabled = True
End Sub

Private Sub cmdResponsable_Click(Index As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

Select Case Index
Case 0
    frmBuscarTipoEco.Show vbModal
Case 1
    Call objPipe.PipeSet("PERSONA", lngPersona)
    frmHistoTipEco.Show vbModal
Case 2 'Crear responsable economico
    Call objSecurity.LaunchProcess("CI1042")
End Select

If objPipe.PipeExist("DESRECO") Then
     cboDBCombo1(6).Text = ""
    cboDBCombo1(6).AddItem objPipe.PipeGet("CODRECO") & ";" & objPipe.PipeGet("DESRECO")
    cboDBCombo1(4).Text = objPipe.PipeGet("TIPOECO")
        Call pCombo4
    cboDBCombo1(5).Text = objPipe.PipeGet("ENTIDAD")
        Call pCombo5
        Call pCombo6
    Call objPipe.PipeRemove("TIPOECO")
    Call objPipe.PipeRemove("ENTIDAD")
    Call objPipe.PipeRemove("CODRECO")
    Call objPipe.PipeRemove("DESRECO")
End If
If objPipe.PipeExist("CODRESP") Then
    sql = "SELECT CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE FROM CI2200 "
    sql = sql & " WHERE CI21CODPERSONA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
       qry(0) = objPipe.PipeGet("CODRESP")
    Set rs = qry.OpenResultset()
    cboDBCombo1(6).AddItem objPipe.PipeGet("CODRESP") & ";" & rs!Paciente
    objPipe.PipeRemove ("CODRESP")
    pCombo6
End If

End Sub





Private Sub ddrTipDocumento_CloseUp()
ddrTiVolante.RemoveAll
grdDoc.Columns("Tipo Volante").Value = ""
If ddrTipDocumento.Columns(0).Value = 1 Then
    LlenarTiVolante
    grdDoc.Columns("N� de Volante").Locked = False
    Call grdDoc.Columns("N� de Volante").CellStyleSet("Obligatorio", grdDoc.Row)
Else
    Call grdDoc.Columns("N� de Volante").CellStyleSet("Editable", grdDoc.Row)
    grdDoc.Columns("N� de Volante").Text = ""
    grdDoc.Columns("N� de Volante").Locked = True
End If
End Sub


Private Sub dtcDateFecFin_CloseUp()
If Trim(grdDoc.Columns("F. Ini. Vol/Doc").Value) <> "" Then
    If CDate(dtcDateFecFin) < CDate(dtcDateFecIni) Then
        MsgBox "La fecha fin es menor que fecha inicio", vbExclamation, Me.Caption
        Exit Sub
    End If
End If
    grdDoc.Columns("F. Fin Validez").Value = dtcDateFecFin.Text
End Sub

Private Sub dtcDateFecIni_CloseUp()
If Trim(grdDoc.Columns("F. Fin Validez").Value) <> "" Then
    If dtcDateFecFin.Date < dtcDateFecIni.Date Then
        MsgBox "La fecha fin es menor que fecha inicio", vbExclamation, Me.Caption
        Exit Sub
    End If
End If
    grdDoc.Columns("F. Ini. Vol/Doc").Value = dtcDateFecIni.Text
'    grdDoc.Columns("F. Fin Validez").Value = dtcDateFecIni.Text
End Sub

Private Sub Form_Load()

lngPersona = objPipe.PipeGet("CI_CODPER")
'lngActPlan = objPipe.PipeGet("PR_ACTPLAN")
strActPedidas = objPipe.PipeGet("PR_ACTPEDI")
lngProceso = objPipe.PipeGet("AD_PROCESO")
strHistoria = objPipe.PipeGet("CI_HISTORIA")
strDptoSol = objPipe.PipeGet("DPTOSOL")
strDrSol = objPipe.PipeGet("DRSOL")
strActPlan = objPipe.PipeGet("PR_ACTPLAN")
'documentacion
grdDoc.Columns("TiVolante").DropDownHwnd = ddrTiVolante.hWnd
grdDoc.Columns("Tipo Documento").DropDownHwnd = ddrTipDocumento.hWnd
grdDoc.Columns("F. Ini. Vol/Doc").DropDownHwnd = dtcDateFecIni.hWnd
grdDoc.Columns("F. Fin Validez").DropDownHwnd = dtcDateFecFin.hWnd
llenarTiDocumento
LlenarTiVolante
'Llenar procesos
Call pInicialiarProc
cmdCommand1(5).Enabled = True

'POINTER
Screen.MousePointer = vbDefault
End Sub
Sub LlenarTiVolante()
Dim sql As String
Dim rdo As rdoResultset
  ddrTiVolante.RemoveAll
  sql = "SELECT AD35CodTipVol, AD35DesTipVol FROM AD3500 WHERE AD35FecFinVig IS NULL ORDER BY AD35DesTipVol"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    ddrTiVolante.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend
End Sub
Sub llenarTiDocumento()
Dim sql As String
Dim rdo As rdoResultset
  
  sql = "SELECT AD19CODTIPODOCUM, AD19DESTIPODOCUM FROM AD1900 WHERE AD19FECFIN IS NULL ORDER BY AD19DESTIPODOCUM"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    ddrTipDocumento.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend
End Sub
Private Sub pInicialiarProc()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim i As Integer

If lngProceso = 0 Then 'SI NO HAY PROCESO ASOCIADO
    sql = "SELECT AD0700.AD07CODPROCESO, AD0700.AD07DESNOMBPROCE, AD0200.AD02DESDPTO, "
    sql = sql & " nvl(SG02TXTFIRMA,SG02APE1||' '||SG02APE2||', '||SG02NOM) DOCTOR,"
    sql = sql & " AD0700.AD07FECHORAINICI, AD0200.AD02CODDPTO,SG0200.SG02COD"
    sql = sql & " FROM AD0700, AD0400, AD0200, SG0200"
    sql = sql & " WHERE AD0700.CI21CODPERSONA = ?"
    sql = sql & " AND AD0700.AD07FECHORAFIN IS NULL"
    sql = sql & " AND AD0700.AD07CODPROCESO = AD0400.AD07CODPROCESO"
    sql = sql & " AND AD0400.AD04FECFINRESPON IS NULL"
    sql = sql & " AND AD0400.AD02CODDPTO = AD0200.AD02CODDPTO"
    sql = sql & " AND AD0400.SG02COD = SG0200.SG02COD"
    sql = sql & " ORDER BY AD0700.AD07FECHORAINICI DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngPersona
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        SSDBGrid1.AddItem rs!AD07CODPROCESO & Chr$(9) _
                        & rs!AD07DESNOMBPROCE & Chr$(9) _
                        & rs!AD07FECHORAINICI & Chr$(9) _
                        & rs!AD02DESDPTO & Chr$(9) _
                        & rs!DOCTOR & Chr$(9) _
                        & rs!AD02CODDPTO & Chr$(9) _
                        & rs!SG02COD
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    SSDBGrid1.AddItemBookmark (1)
    lngAsistencia = lngHayAsistencia
    If lngAsistencia > 0 Then Call pCargarProcAsis(lngAsistencia, 1)
    Call pCargarDocumentacion
Else
    sql = "SELECT AD0700.AD07CODPROCESO, AD0700.AD07DESNOMBPROCE, AD0200.AD02DESDPTO, "
    sql = sql & " nvl(SG02TXTFIRMA,SG02APE1||' '||SG02APE2||' '||SG02NOM) DOCTOR,"
    sql = sql & " AD0700.AD07FECHORAINICI,"
    sql = sql & " AD0200.AD02CODDPTO,SG0200.SG02COD"
    sql = sql & " FROM AD0700, AD0400, AD0200, SG0200"
    sql = sql & " WHERE AD0700.CI21CODPERSONA = ?"
    sql = sql & " AND AD0700.AD07CODPROCESO  = ?"
    sql = sql & " AND AD0700.AD07FECHORAFIN IS NULL"
    sql = sql & " AND AD0700.AD07CODPROCESO = AD0400.AD07CODPROCESO"
    sql = sql & " AND AD0400.AD04FECFINRESPON IS NULL"
    sql = sql & " AND AD0400.AD02CODDPTO = AD0200.AD02CODDPTO"
    sql = sql & " AND AD0400.SG02COD = SG0200.SG02COD"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngPersona
        qry(1) = lngProceso
    Set rs = qry.OpenResultset()
    SSDBGrid1.RemoveAll
    SSDBGrid1.Refresh
    
    If Not rs.EOF Then
        SSDBGrid1.AddItem rs!AD07CODPROCESO & Chr$(9) _
                        & rs!AD07DESNOMBPROCE & Chr$(9) _
                        & rs!AD07FECHORAINICI & Chr$(9) _
                        & rs!AD02DESDPTO & Chr$(9) _
                        & rs!DOCTOR & Chr$(9) _
                        & rs!AD02CODDPTO & Chr$(9) _
                        & rs!SG02COD
        rs.MoveNext
    End If
    rs.Close
    qry.Close
    cmdCommand1(0).Enabled = False
    cmdCommand1(2).Enabled = False

    'miramos si hay asistencia abierta y si es asi buscamos el proceso/asistencia
    lngAsistencia = lngHayAsistencia
    If lngAsistencia <> 0 Then Call pCargarProcAsis(lngAsistencia, 1)

End If
pCargarDocumentacion
End Sub
Private Function lngHayAsistencia() As Long
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql As String

sql = "SELECT AD0100.AD01CODASISTENCI,AD0100.AD01FECINICIO, AD2500.AD12CODTIPOASIST, "
sql = sql & " AD1200.AD12DESTIPOASIST,AD25FECPREVALTA, GCFN06(AD1500.AD15CODCAMA) CAMA"
sql = sql & " FROM AD0100,AD2500,AD1200,AD1500"
sql = sql & " WHERE AD0100.CI21CODPERSONA = ?"
sql = sql & " AND AD0100.AD01FECINICIO <= TRUNC(SYSDATE+1)"
sql = sql & " AND AD0100.AD01FECINICIO >= TO_DATE('01/01/2000','DD/MM/YYYY')"
sql = sql & " AND AD0100.AD01FECFIN IS NULL"
sql = sql & " AND AD0100.AD01CODASISTENCI = AD2500.AD01CODASISTENCI"
sql = sql & " AND AD2500.AD25FECFIN IS NULL"
sql = sql & " AND AD2500.AD12CODTIPOASIST = AD1200.AD12CODTIPOASIST"
sql = sql & " AND AD0100.AD01CODASISTENCI = AD1500.AD01CODASISTENCI(+)"
sql = sql & " AND AD1500.AD14CODESTCAMA(+)= 2"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
Set rs = qry.OpenResultset()
If Not rs.EOF Then 'HAY ASISTENCIA ABIERTA
    txtText1(1).Text = rs!AD01CODASISTENCI
    dtcDateCombo1(0).Text = Format(rs!AD01FECINICIO, "dd/mm/yyyy")
    txtHora(0).Text = Format(rs!AD01FECINICIO, "hh")
    txtMinuto(0).Text = Right(Format(rs!AD01FECINICIO, "HH:MM"), 2)
    dtcDateCombo1(1).Text = Format(rs!AD25FECPREVALTA, "dd/mm/yyyy")
    cboDBCombo1(0).AddItem rs!AD12CODTIPOASIST & ";" & rs!AD12DESTIPOASIST
    cboDBCombo1(0).Text = rs!AD12DESTIPOASIST
    If Not IsNull(rs!cama) Then txtText1(2).Text = rs!cama
    lngHayAsistencia = rs!AD01CODASISTENCI
    rs.Close
    qry.Close
    cmdCommand1(3).Enabled = False
    fraFrame1(2).Enabled = False
    fraFrame1(0).Enabled = True
    cmdCommand1(6).Enabled = True
    pLlenarCombo (0)
    
Else
    lngHayAsistencia = 0
    fraFrame1(0).Enabled = False
    cboDBCombo1(0).Enabled = False
    cboDBCombo1(0).Text = ""
    cboDBCombo1(1).Text = ""
    cboDBCombo1(4).Text = ""
    cboDBCombo1(5).Text = ""
    cboDBCombo1(6).Text = ""
    fraFrame1(2).Enabled = True
    cmdCommand1(3).Enabled = True
    cmdCommand1(6).Enabled = False
    rs.Close
    qry.Close
End If
    
   
End Function
Private Sub pCargarProcAsis(lngA As Long, intLLenar As Integer)
Dim sql As String
Dim strFechaHora As String
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "SELECT AD0800.AD08FECINICIO, AD1000.AD10DESTIPPACIEN,"
sql = sql & " AD0800.AD08INDREQIM,AD1100.AD11INDVOLANTE, AD0800.AD10CODTIPPACIEN,"
sql = sql & " AD0800.CI21CODPERSONA_ENV, CI22ENV.CI22NOMBRE||' '||CI22ENV.CI22PRIAPEL||' '||CI22ENV.CI22SEGAPEL PENVINFOR,"
sql = sql & " AD0800.CI21CODPERSONA_MED, CI22MED.CI22NOMBRE||' '||CI22MED.CI22PRIAPEL||' '||CI22MED.CI22SEGAPEL MEDREMITE,"
sql = sql & " AD0800.AD08NUMCASO, AD1100.CI32CODTIPECON, AD1100.CI13CODENTIDAD,"
sql = sql & " CI3200.CI32DESTIPECON, CI1300.CI13DESENTIDAD,"
'sql = sql & " AD1100.CI21CODPERSONA, CI22REC.CI22NOMBRE||' '||CI22REC.CI22PRIAPEL||' '||CI22REC.CI22SEGAPEL RESECONOM,"
sql = sql & "  AD1100.CI21CODPERSONA, ADFN01(AD1100.CI21CODPERSONA) RESECONOM,"
sql = sql & " AD0500.AD02CODDPTO, AD0200.AD02DESDPTO, AD0500.SG02COD,"
sql = sql & " 'Dr. '||SG02APE1||' '||SG02APE2||' '||SG02NOM DOCTOR, AD08OBSERVAC"
sql = sql & " FROM AD0800, AD1000, AD1100,"
sql = sql & " CI2200 CI22ENV, CI2200 CI22MED, CI3200, CI1300, AD0500,"
sql = sql & " AD0200, SG0200"
sql = sql & " WHERE AD0800.AD07CODPROCESO = ?"
sql = sql & " AND AD0800.AD01CODASISTENCI = ?"
sql = sql & " AND AD0800.AD08FECFIN IS NULL "
sql = sql & " AND AD0800.AD10CODTIPPACIEN = AD1000.AD10CODTIPPACIEN"
sql = sql & " AND AD0800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI"
sql = sql & " AND AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO"
sql = sql & " AND AD1100.AD11FECFIN IS NULL"
'sql = sql & " AND AD1100.CI21CODPERSONA = CI22REC.CI21CODPERSONA(+)"
sql = sql & " AND AD1100.CI32CODTIPECON = CI3200.CI32CODTIPECON"
sql = sql & " AND AD1100.CI13CODENTIDAD = CI1300.CI13CODENTIDAD"
sql = sql & " AND AD0800.CI21CODPERSONA_ENV = CI22ENV.CI21CODPERSONA(+)"
sql = sql & " AND AD0800.CI21CODPERSONA_MED = CI22MED.CI21CODPERSONA(+)"
sql = sql & " AND AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI"
sql = sql & " AND AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO"
sql = sql & " AND AD0500.AD05FECFINRESPON IS NULL"
sql = sql & " AND AD0500.AD02CODDPTO = AD0200.AD02CODDPTO"
sql = sql & " AND AD0500.SG02COD = SG0200.SG02COD"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = SSDBGrid1.Columns(0).Value
    qry(1) = lngA
Set rs = qry.OpenResultset()
If Not rs.EOF Then 'si hay proceso asitencia rellenamos la pantalla y la bloqueamos
    dtcDateCombo1(2) = Format(rs!AD08FECINICIO, "dd/mm/yyyy")
    txtHora(1).Text = Format(rs!AD08FECINICIO, "hh")
    txtMinuto(1).Text = Right(Format(rs!AD08FECINICIO, "hh:mm"), 2)
    cboDBCombo1(1).AddItem rs!AD10CODTIPPACIEN & ";" & rs!AD10DESTIPPACIEN
    cboDBCombo1(1).Text = rs!AD10DESTIPPACIEN
    If rs!AD08INDREQIM = 0 Then
        chkCheck1(0).Value = 0
    Else
        chkCheck1(0).Value = 1
    End If
    If rs!AD11INDVOLANTE = 0 Then
        chkCheck1(1).Value = 0
    Else
        chkCheck1(1).Value = 1
    End If
    If Not IsNull(rs!CI21CODPERSONA_ENV) Then
        txtText1(3).Text = rs!CI21CODPERSONA_ENV
        txtText1(4).Text = rs!PENVINFOR
    End If
    If Not IsNull(rs!CI21CODPERSONA_MED) Then
        txtText1(5).Text = rs!CI21CODPERSONA_MED
        txtText1(6).Text = rs!MEDREMITE
    End If
    cboDBCombo1(4).AddItem rs!CI32CODTIPECON & ";" & rs!CI32DESTIPECON
    cboDBCombo1(4).Text = rs!CI32CODTIPECON
    cboDBCombo1(5).AddItem rs!CI13CODENTIDAD & ";" & rs!CI13DESENTIDAD
    cboDBCombo1(5).Text = rs!CI13CODENTIDAD
    cboDBCombo1(6).AddItem rs!CI21CODPERSONA & ";" & rs!RESECONOM
    cboDBCombo1(6).Text = rs!RESECONOM
''    txtText1(8).Text = rs!CI21CODPERSONA
''    txtText1(0).Text = rs!RESECONOM
    cboDBCombo1(2).AddItem rs!AD02CODDPTO & ";" & rs!AD02DESDPTO
    cboDBCombo1(2).Text = rs!AD02DESDPTO
    cboDBCombo1(3).AddItem rs!SG02COD & ";" & rs!DOCTOR
    cboDBCombo1(3).Text = rs!DOCTOR
    If Not IsNull(rs!AD08OBSERVAC) Then
        txtText1(0).Text = rs!AD08OBSERVAC
    Else
        txtText1(0).Text = ""
    End If
    txtText1(7).Text = Right(str(rs!AD08NumCaso), 4)
    rs.Close
    qry.Close
    cmdCommand1(3).Enabled = False
    fraFrame1(2).Enabled = False
    fraFrame1(0).Enabled = False
    cmdCommand1(5).Enabled = False
    blnHayPA = True
  
Else 'SI NO HAY PROCESO ASISTENCIA
    strFechaHora = strFechaHora_Sistema
    cmdCommand1(3).Enabled = False
    fraFrame1(2).Enabled = False
    fraFrame1(0).Enabled = True
    dtcDateCombo1(2).Text = Format(strFechaHora, "dd/mm/yyyy")
    txtHora(1).Text = Mid(strFechaHora, 12, 2)
    txtMinuto(1).Text = Mid(strFechaHora, 15, 2)
    If intLLenar = 0 Then Call pLlenarCombo(0)
    blnHayPA = False
End If
End Sub
Private Sub pCargarDocumentacion()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strDpto$
Dim strDr$
Dim strCDpto$
Dim strCDr$
grdDoc.RemoveAll
sql = "SELECT AD18CODDOCUMENTO,AD18DESDOCUMENTO,"
sql = sql & " AD1800.AD19CODTIPODOCUM,AD19DESTIPODOCUM,AD18FECDOCUMENTO, AD35CODTIPVOL,"
sql = sql & " AD18FECFINVOLANTE,AD02DESDPTO, SG02TXTFIRMA, AD0500.AD02CODDPTO, AD0500.SG02COD,"
sql = sql & " AD1800.AD18NUMVOLANTE FROM AD1800, AD1900, AD0500, AD0200, SG0200"
sql = sql & " WHERE AD1800.CI21CODPERSONA = ? "
sql = sql & " AND AD1800.AD19CODTIPODOCUM = AD1900.AD19CODTIPODOCUM(+)"
sql = sql & " AND AD1800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI(+)"
sql = sql & " AND AD1800.AD07CODPROCESO = AD0500.AD07CODPROCESO(+)"
sql = sql & " AND (AD0500.AD02CODDPTO = ? OR AD1800.AD07CODPROCESO IS NULL)"
sql = sql & " AND AD05FECFINRESPON IS NULL "
sql = sql & " AND AD0500.AD02CODDPTO  = AD0200.AD02CODDPTO(+)"
sql = sql & " AND AD0500.SG02COD  = SG0200.SG02COD(+)"
sql = sql & " ORDER BY AD18CODDOCUMENTO DESC"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
    qry(1) = SSDBGrid1.Columns("CODDPTO").Value
Set rs = qry.OpenResultset()
While Not rs.EOF
    If Not IsNull(rs!AD02DESDPTO) Then strDpto = rs!AD02DESDPTO Else strDpto = ""
    If Not IsNull(rs!SG02TXTFIRMA) Then strDr = rs!SG02TXTFIRMA Else strDr = ""
    If Not IsNull(rs!AD02CODDPTO) Then strCDpto = rs!AD02CODDPTO Else strCDpto = ""
    If Not IsNull(rs!SG02COD) Then strCDr = rs!SG02COD Else strCDr = ""
    grdDoc.AddItem rs!AD18CODDOCUMENTO & Chr$(9) _
                & rs!AD19CODTIPODOCUM & Chr$(9) _
                & rs!AD19CODTIPODOCUM & Chr$(9) _
                & rs!AD35CODTIPVOL & Chr$(9) _
                & rs!AD18DESDOCUMENTO & Chr$(9) _
                & rs!AD18NUMVOLANTE & Chr$(9) _
                & Format(rs!AD18FECDOCUMENTO, "dd/mm/yyyy") & Chr$(9) _
                & Format(rs!AD18FECFINVOLANTE, "dd/mm/yyyy") & Chr$(9) _
                & strDpto & Chr$(9) _
                & strDr & Chr$(9) _
                & strCDpto & Chr$(9) _
                & strCDr
    rs.MoveNext
                
Wend

End Sub
Private Sub pLlenarCombo(intIndex As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

If intIndex = 1 Then
'TIPO DE ASISTENCIA
    sql = "SELECT AD12CODTIPOASIST, AD12DESTIPOASIST FROM AD1200 WHERE AD12FECFIN IS NULL"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rs.EOF
        cboDBCombo1(0).AddItem rs!AD12CODTIPOASIST & ";" & rs!AD12DESTIPOASIST
        rs.MoveNext
    Loop
    rs.Close
End If
'TIPO DE PACIENTE
pCombo1
'sql = "SELECT AD10CODTIPPACIEN,AD10DESTIPPACIEN FROM AD1000 WHERE AD10FECFIN IS NULL"
'Set rs = objApp.rdoConnect.OpenResultset(sql)
'    Do While Not rs.EOF
'        cboDBCombo1(1).AddItem rs!AD10CODTIPPACIEN & ";" & rs!AD10DESTIPPACIEN
'        rs.MoveNext
'    Loop
'    rs.Close
'TIPO ECONOMICO
sql = "SELECT CI32CODTIPECON, CI32DESTIPECON FROM CI3200 WHERE CI32FECFIVGTEC IS NULL"
sql = sql & " ORDER BY CI32CODTIPECON"
Set rs = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rs.EOF
        cboDBCombo1(4).AddItem rs!CI32CODTIPECON & ";" & rs!CI32DESTIPECON
        rs.MoveNext
    Loop
    rs.Close
'ENTIDAD
sql = "SELECT CI13CODENTIDAD,CI13DESENTIDAD FROM CI1300 WHERE CI13FECFIVGENT IS NULL"
sql = sql & " ORDER BY CI13CODENTIDAD"
Set rs = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rs.EOF
        cboDBCombo1(5).AddItem rs!CI13CODENTIDAD & ";" & rs!CI13DESENTIDAD
        rs.MoveNext
    Loop
    rs.Close
'DEPARTAMENTO RESPONSABLE
sql = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD02INDRESPONPROC = -1 AND AD02FECFIN IS NULL"
Set rs = objApp.rdoConnect.OpenResultset(sql)
    cboDBCombo1(2).AddItem SSDBGrid1.Columns("CODDPTO").Value & ";" & SSDBGrid1.Columns("Dpto. Responsable").Value
    Do While Not rs.EOF
        If rs!AD02CODDPTO <> SSDBGrid1.Columns("CODDPTO").Value Then
            cboDBCombo1(2).AddItem rs!AD02CODDPTO & ";" & rs!AD02DESDPTO
        End If
        rs.MoveNext
    Loop
    rs.Close
cboDBCombo1(2).Text = SSDBGrid1.Columns("Dpto. Responsable").Value
'DOCTOR RESPONSABLE
 cboDBCombo1(3).RemoveAll
If cboDBCombo1(2).Columns(0).Text <> "" Then
    If cboDBCombo1(2).Columns(0).Value = constDPTO_URGENCIAS Then
        cboDBCombo1(3).AddItem constUSUARIO_PASTRANA & ";" & constDESUSUARIO_PASTRANA
        cboDBCombo1(3).AddItem constUSUARIO_CCU & ";" & constDESUSUARIO_CCU
        
    Else
        sql = "SELECT AD0300.SG02COD,'Dr. '||SG02APE1||' '||SG02APE2||' '||SG02NOM DOCTOR " & _
             "FROM SG0200,AD0300 " & _
             "WHERE AD0300.AD02CODDPTO = " & cboDBCombo1(2).Columns(0).Text & " " & _
             "AND AD0300.SG02COD=SG0200.SG02COD " & _
             "AND SYSDATE < NVL(SG0200.SG02FECDES,'01/01/2100') " & _
             "AND SYSDATE < NVL(AD0300.AD03FECFIN,'01/01/2100') " & _
             "AND SG0200.AD30CODCATEGORIA = 1 "
         Set rs = objApp.rdoConnect.OpenResultset(sql)
         cboDBCombo1(3).AddItem SSDBGrid1.Columns("SG02COD").Value & ";" & SSDBGrid1.Columns(4).Value
         Do While Not rs.EOF
             If rs!SG02COD <> SSDBGrid1.Columns("SG02COD").Value Then
                cboDBCombo1(3).AddItem rs!SG02COD & ";" & rs!DOCTOR
             End If
             rs.MoveNext
         Loop
         rs.Close
         
     End If
    cboDBCombo1(3).Text = SSDBGrid1.Columns(4).Value
End If
End Sub















Private Sub SSDBGrid1_Click()
If lngAsistencia > 0 Then
'BORRAMOS TODO
pVaciarProcAsis
If Not blnCreandoNuevo Then
    'Llenamos la asistencia
    Call pCargarProcAsis(lngAsistencia, 0)
    'PONEMOS LA DOCUMENTACION
    pCargarDocumentacion
End If
End If

End Sub

Private Sub pVaciarProcAsis()
Dim i As Integer

For i = 3 To 7
    txtText1(i).Text = ""
Next i
For i = 1 To 6
    cboDBCombo1(i).RemoveAll
    cboDBCombo1(i).Text = ""
Next i
End Sub
Public Function BuscaResponsable(lngCodPersona As Long) As String
Dim strSQL As String
Dim rscod As rdoResultset
Dim qry As rdoQuery
Dim qry2 As rdoQuery
Dim rs As rdoResultset


strSQL = " SELECT CI21CODPERSONA_REC FROM CI2200 WHERE  CI21CODPERSONA= ?"
Set qry = objApp.rdoConnect.CreateQuery("", strSQL)
    qry(0) = lngPersona
Set rscod = qry.OpenResultset()

strSQL = "SELECT ADFN01(CI21CODPERSONA) NOMBRE FROM CI2100 WHERE CI21CODPERSONA = ?"
    Set qry2 = objApp.rdoConnect.CreateQuery("", strSQL)
    If Not IsNull(rscod.rdoColumns(0)) Then
        qry2(0) = rscod.rdoColumns(0)
    Else
        qry2(0) = lngCodPersona
    End If
    Set rs = qry2.OpenResultset()
    BuscaResponsable = qry2(0) & " ; " & rs(0).Value
End Function


Private Sub pAcunsa()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
 sql = "SELECT CI22NUMHISTORIA FROM CI0300 WHERE CI22NUMHISTORIA = ? " & _
       " AND (CI03FECFINPOLI IS NULL OR CI03FECFINPOLI > SYSDATE)"
       
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strHistoria
    Set rs = qry.OpenResultset()
    If rs.EOF Then
        MsgBox "Este Paciente No pertence a Acunsa", vbCritical
        cboDBCombo1(4).Text = ""
        cboDBCombo1(5).Text = ""
    End If
End Sub
Private Sub pUniversidad()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
 sql = "SELECT CI22NUMHISTORIA FROM CI2400 WHERE CI22NUMHISTORIA =?" & _
       " AND (CI24FECFIN IS NULL OR CI24FECFIN > SYSDATE)"
      Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = strHistoria
    Set rs = qry.OpenResultset()
    If rs.EOF Then
        MsgBox "Este Paciente No pertence a Universidad", vbCritical
        cboDBCombo1(4).Text = ""
        cboDBCombo1(5).Text = ""
    End If
End Sub
Private Sub pCombo4()
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql As String



  'Cargamos el la entidad
  cboDBCombo1(5).RemoveAll
  'Comprueba si puede tener esos tipos economicos
  If cboDBCombo1(4).Text = "J" Then pAcunsa
  If cboDBCombo1(4).Text = "I" Then pUniversidad
  If cboDBCombo1(4).Text <> "" Then
        sql = "SELECT CI13CODENTIDAD,CI13DESENTIDAD " & _
              "FROM CI1300 " & _
              "WHERE CI32CODTIPECON = '" & cboDBCombo1(4).Text & "' " & _
              "AND (CI13FECFIVGENT IS NULL OR CI13FECFIVGENT > SYSDATE) " & _
              "ORDER BY CI13CODENTIDAD"
        Set rs = objApp.rdoConnect.OpenResultset(sql)
          Do While Not rs.EOF
              cboDBCombo1(5).AddItem rs!CI13CODENTIDAD & ";" & rs!CI13DESENTIDAD
              rs.MoveNext
          Loop
          rs.Close
            'si es privado ponemos el responsable
            If cboDBCombo1(4).Text = "P" Then
                cboDBCombo1(6).RemoveAll
                cboDBCombo1(6).AddItem BuscaResponsable(lngPersona)
                cboDBCombo1(6).Text = cboDBCombo1(6).Columns(1).Value
                cmdResponsable(2).Enabled = True
            Else 'NO ES PRIVADO
                cboDBCombo1(6).RemoveAll
                sql = "SELECT  CI21CODPERSONA_REC,RECO_DES FROM CI2901J WHERE CI32CODTIPECON = ?"
                sql = sql & " ORDER BY RECO_DES"
                Set qry = objApp.rdoConnect.CreateQuery("", sql)
                    qry(0) = cboDBCombo1(4).Text
                Set rs = qry.OpenResultset()
                Do While Not rs.EOF
                    cboDBCombo1(6).AddItem rs!CI21CODPERSONA_REC & ";" & rs!RECO_DES
                    rs.MoveNext
                Loop
                rs.Close
                qry.Close
                cmdResponsable(2).Enabled = False
            End If
        End If
End Sub
Private Sub pCombo5()
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim sql As String
Dim lngCodRes As Long
  txtMensaje = ""
  If cboDBCombo1(4).Text = "P" And _
  (cboDBCombo1(5).Text = "JC" Or cboDBCombo1(5).Text = "J") Then pAcunsa
  If cboDBCombo1(4).Text = "P" And _
  (cboDBCombo1(5).Text = "UB" Or cboDBCombo1(5).Text = "UD") Then pUniversidad
  If cboDBCombo1(4).Text <> "P" Then
    cboDBCombo1(6).RemoveAll
    cboDBCombo1(6).Text = ""
    sql = "SELECT  CI21CODPERSONA_REC,RECO_DES FROM CI2901J WHERE CI32CODTIPECON = ?"
    sql = sql & " AND CI13CODENTIDAD = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = cboDBCombo1(4).Text
        qry(1) = cboDBCombo1(5).Text
    Set rs = qry.OpenResultset()
    If objPipe.PipeExist("CODRECO") Then
        cboDBCombo1(6).AddItem objPipe.PipeGet("CODRECO") & ";" & objPipe.PipeGet("DESRECO")
        cboDBCombo1(6).Text = objPipe.PipeGet("DESRECO")
        lngCodRes = objPipe.PipeGet("CODRECO")
    Else
        lngCodRes = 0
    End If
    Do While Not rs.EOF
        If rs!CI21CODPERSONA_REC <> lngCodRes Then
            cboDBCombo1(6).AddItem rs!CI21CODPERSONA_REC & ";" & rs!RECO_DES
        End If
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
  Else
   If objPipe.PipeExist("CODRECO") Then
     cboDBCombo1(6).RemoveAll
     cboDBCombo1(6).AddItem objPipe.PipeGet("CODRECO") & ";" & objPipe.PipeGet("DESRECO")
     cboDBCombo1(6).Text = objPipe.PipeGet("DESRECO")
   End If
  End If

End Sub

Private Sub pLlenarTxt(Index As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "SELECT CI22PRIAPEL||', '||CI22SEGAPEL||', '||CI22NOMBRE NOMBRE FROM CI2200 WHERE CI21CODPERSONA = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objPipe.PipeGet("PERSONA")
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    txtText1(Index).Text = rs!Nombre
Else
    txtText1(Index).Text = ""
End If
End Sub
Private Sub pCrearNewProc()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

SSDBGrid1.AddNew
SSDBGrid1.AllowUpdate = True

Set rs = objApp.rdoConnect.OpenResultset("SELECT AD07CODPROCESO_S.NEXTVAL FROM DUAL", rdOpenKeyset)
SSDBGrid1.Columns(0).Value = rs(0).Value
SSDBGrid1.Columns(0).Locked = True
rs.Close
SSDBGrid1.Columns(2).Value = Format(strFechaHora_Sistema, "dd/mm/yyyy hh:mm")
SSDBGrid1.Columns(3).Locked = True

sql = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD02INDRESPONPROC = -1 AND AD02FECFIN IS NULL"
sql = sql & " ORDER BY AD02DESDPTO"
Set rs = objApp.rdoConnect.OpenResultset(sql)
Do While Not rs.EOF
    SSDBGrid1.Columns(3).AddItem rs!AD02CODDPTO & " " & rs!AD02DESDPTO
    rs.MoveNext
Loop
rs.Close
SSDBGrid1.Columns(3).Text = strDptoSol
SSDBGrid1.Columns(4).Text = strDrSol

End Sub

Private Sub SSDBGrid1_ComboCloseUp()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strUrgencias As String

If SSDBGrid1.col = 3 Then
    'Llenamos los doctores
   If SSDBGrid1.Columns(3).Value <> "" Then
    SSDBGrid1.Columns(4).Text = ""
    SSDBGrid1.Columns(4).RemoveAll
    strUrgencias = str(constDPTO_URGENCIAS) & " Urgencias"
    If Trim(SSDBGrid1.Columns(3).Value) = Trim(strUrgencias) Then
      SSDBGrid1.Columns(4).AddItem constUSUARIO_PASTRANA & " " & constDESUSUARIO_PASTRANA
      SSDBGrid1.Columns(4).AddItem constUSUARIO_CCU & " " & constDESUSUARIO_CCU
    Else
      sql = "SELECT AD0300.SG02COD,'Dr. '||SG02APE1||' '||SG02APE2||' '||SG02NOM DOCTOR " & _
          "FROM SG0200,AD0300 " & _
          "WHERE AD0300.AD02CODDPTO = " & Trim(Left(SSDBGrid1.Columns(3).Value, 3)) & " " & _
          "AND AD0300.SG02COD=SG0200.SG02COD " & _
          "AND SYSDATE < NVL(SG0200.SG02FECDES,'01/01/2100') " & _
          "AND SYSDATE < NVL(AD0300.AD03FECFIN,'01/01/2100') " & _
          "AND SG0200.AD30CODCATEGORIA = 1 ORDER BY SG02APE1"
      Set rs = objApp.rdoConnect.OpenResultset(sql)
      Do While Not rs.EOF
          SSDBGrid1.Columns(4).AddItem rs!SG02COD & " " & rs!DOCTOR
          rs.MoveNext
      Loop
      rs.Close
    End If
   End If
End If
End Sub
Function fblnAsociarProceso() As Integer
Dim sql As String
Dim rs As rdoResultset
Dim rsa As rdoResultset
Dim qry As rdoQuery
Dim sqlA As String
Dim lngCodDpto As Long
Dim strCodUsu As String

 On Error GoTo CancelTrans

'Comprobamos los datos

If Trim(SSDBGrid1.Columns(4).Text) = "" Or Trim(SSDBGrid1.Columns(3).Text) = "" Then
    MsgBox "Rellene los Responsables del proceso asistencia", vbCritical
    Exit Function
End If
'COMPROBAMOS QUE SI HAY UNA PRUEBA DE HOSPITALIZACION EL PROCESO
'TENGA LOS MISMOS RESPONSABLES
sql = "SELECT PR04NUMACTPLAN FROM PR0400,PR0100 WHERE PR03NUMACTPEDI IN (" & strActPedidas
sql = sql & ") AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & gCodActHospitalizacion
Set rs = objApp.rdoConnect.OpenResultset(sql)
If Not rs.EOF Then
    sql = "SELECT PR0400.AD02CODDPTO, AG1100.SG02COD FROM CI0100, PR0400, AG1100 "
    sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ? "
    sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
    sql = sql & " AND CI0100.CI01SITCITA = 1"
    sql = sql & " AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = rs(0)
    Set rsa = qry.OpenResultset()
    If Not rsa.EOF Then
        If Not IsNull(rsa(0)) And Not IsNull(rsa(1)) Then
            If SSDBGrid1.Columns("CODDPTO").Value <> "" Then
                If Trim(rsa(0)) <> Trim(SSDBGrid1.Columns("CODDPTO").Value) Or Trim(rsa(1)) <> Trim(SSDBGrid1.Columns("SG02COD").Value) Then
                    MsgBox "la prueba de hospitalizacion y los responsables no son iguales", vbOKOnly + vbInformation
                    fblnAsociarProceso = False
                    Exit Function
                End If
            Else
                vsw = SSDBGrid1.Columns(3).Value
                lngCodDpto = vsw.F(1)
                vsw = SSDBGrid1.Columns(4).Value
                strCodUsu = vsw.F(1)
                If rsa(0) <> lngCodDpto Or rsa(1) <> strCodUsu Then
                    MsgBox "la prueba de hospitalizacion y los responsables no son iguales", vbOKOnly + vbInformation
                    fblnAsociarProceso = False
                    Exit Function
                End If
            End If
        
        End If
    End If 'if not rsa.eof
    sql = "SELECT AD02CODDPTO FROM PR0400 WHERE PR04NUMACTPLAN = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = rs(0)
    Set rsa = qry.OpenResultset()
    If Not IsNull(rsa(0)) Then
        If SSDBGrid1.Columns("CODDPTO").Value <> "" Then
            If Trim(rsa(0)) <> Trim(SSDBGrid1.Columns("CODDPTO").Value) Then
                MsgBox "El Departamento de la de hospitalizacion y el Departamento Responsable no son iguales", vbOKOnly + vbInformation
                fblnAsociarProceso = False
                Exit Function
            End If
        Else
            vsw = SSDBGrid1.Columns(3).Value
            lngCodDpto = vsw.F(1)
            vsw = SSDBGrid1.Columns(4).Value
            strCodUsu = vsw.F(1)
            If rsa(0) <> lngCodDpto Then
                    MsgBox "El Departamento de la de hospitalizacion y el Departamento Responsable no son iguales", vbOKOnly + vbInformation
                    fblnAsociarProceso = False
                Exit Function
            End If
        End If
    End If
    
End If
If blnCreandoNuevo = True Then
    'COMPROBAMOS QUE NO HAY NINGUN PROCESO CON EL MISMO DPTO Y DR.
    'RESPONSABLE ASOCIADO Y A LA ASISTENCIA
    sql = "SELECT COUNT(*) FROM AD0800, AD0700, AD0400 WHERE "
    sql = sql & "AD0400.AD02CODDPTO = ? "
    sql = sql & " AND AD0400.SG02COD = ? "
    sql = sql & " AND AD0400.AD07CODPROCESO = AD0700.AD07CODPROCESO"
    sql = sql & " AND AD0700.AD07CODPROCESO = AD0800.AD07CODPROCESO"
    sql = sql & " AND AD0800.AD01CODASISTENCI = ?"
    sql = sql & " AND AD0700.AD07CODPROCESO <> ?"
    sql = sql & " AND AD0400.AD02CODDPTO <> " & gCodDptoPET
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
            vsw = SSDBGrid1.Columns(3).Value
            qry(0) = vsw.F(1)
            vsw = SSDBGrid1.Columns(4).Value
            qry(1) = vsw.F(1)
            If Trim(txtText1(1).Text) = "" Then
                qry(2) = 0
            Else
                qry(2) = txtText1(1).Text
            End If
                qry(3) = SSDBGrid1.Columns(0).Value
        Set rs = qry.OpenResultset()
        If rs(0) > 0 Then
            MsgBox "Ya existe un proceso con ese responsable"
            fblnAsociarProceso = False
            
            Screen.MousePointer = vbHourglass
            SSDBGrid1.AllowUpdate = False
            SSDBGrid1.RemoveAll
            Call pInicialiarProc
            Screen.MousePointer = vbDefault
            blnCreandoNuevo = False
            Exit Function
        End If

objApp.BeginTrans

    'Guardamos el proceso
    sql = "INSERT INTO AD0700 (CI21CODPERSONA,AD07CODPROCESO,AD07DESNOMBPROCE,"
    sql = sql & " AD07FECHORAINICI, CI22NUMHISTORIA) VALUES (?,?,?,TO_DATE(?,'DD\MM\YYYY HH24:MI'),?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngPersona
        qry(1) = SSDBGrid1.Columns(0).Value
        If Trim(SSDBGrid1.Columns(1).Value) <> "" Then
            qry(2) = SSDBGrid1.Columns(1).Value
        Else
            qry(2) = Null
        End If
        qry(3) = Format(SSDBGrid1.Columns(2).Value, "dd/mm/yyyy hh:mm")
        qry(4) = strHistoria
    qry.Execute
    qry.Close
    
    'Guardamos el reponsable del proceso
    sql = "INSERT INTO AD0400 (AD07CODPROCESO,AD04FECINIRESPON,AD02CODDPTO,SG02COD) "
    sql = sql & "VALUES (?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?)"
    
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = SSDBGrid1.Columns(0).Value
        qry(1) = Format(SSDBGrid1.Columns(2).Value, "dd/mm/yyyy hh:mm")
        vsw = SSDBGrid1.Columns(3).Value
        qry(2) = vsw.F(1)
        vsw = SSDBGrid1.Columns(4).Value
        qry(3) = vsw.F(1)
        
    qry.Execute
End If

'Asociamos el proceso
'Si ya hay un proceso/asistencia asociado no lo tocamoS
'tabla PR0800
sql = "UPDATE PR0800 SET AD07CODPROCESO = ? WHERE PR03NUMACTPEDI IN (" & _
strActPedidas & ") AND AD07CODPROCESO IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = SSDBGrid1.Columns(0).Text
qry.Execute
qry.Close


'TABLA PR0400
sql = "UPDATE PR0400 SET AD07CODPROCESO = ? WHERE PR04NUMACTPLAN  IN(" & _
strActPlan & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = SSDBGrid1.Columns(0).Text
qry.Execute
qry.Close


objApp.CommitTrans
fblnAsociarProceso = True

Exit Function
 
CancelTrans:
    MsgBox "Error creando y asociando proceso, Intentelo de Nuevo", vbCritical
    fblnAsociarProceso = False
    objApp.RollbackTrans
End Function

Function fblnAsoPA() As Integer
Dim sql As String
Dim sqlA As String
Dim rs As rdoResultset
Dim rsa As rdoResultset
Dim qry As rdoQuery
Dim blnObligatorios As Boolean
Dim i As Integer
Dim strNumHistoria As String
Dim strNumHistoriaCaso As String
Dim strNumcaso As String
Dim strFechaIniAsis As String
Dim strFechaIniProcAsis As String
Dim strFechaHoy As String
Dim strFechaMa�ana As String

On Error Resume Next

fblnAsoPA = True
If lngProceso = 0 Then 'si no hay procesO
    If fblnAsociarProceso = False Then 'si no lo podemos crear o asociar
        fblnAsoPA = False
        Exit Function
    End If
    lngProceso = SSDBGrid1.Columns(0).Text
End If 'tenemos asociado el proceso

'Comprobamos que tenemos todos los datos
blnObligatorios = True
If dtcDateCombo1(0).Text = "" Then blnObligatorios = False
If txtHora(1).Text = "" Then blnObligatorios = False
If txtMinuto(1).Text = "" Then blnObligatorios = False
For i = 0 To 6
  If cboDBCombo1(i).Text = "" Then blnObligatorios = False
Next i

If Not blnObligatorios Then
    MsgBox "Por favor Rellene los campos obligatorios"
    fblnAsoPA = False
    Exit Function
End If
'si la asistencias es ambulatoria comprobamos que no tengamos
'pruebas de hospitalizacion
If cboDBCombo1(0).Columns(0).Value = constASIS_AMBU Then
    sql = "SELECT COUNT(*) FROM PR0400,PR0100 WHERE PR03NUMACTPEDI IN (" & strActPedidas
    sql = sql & ") AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & gCodActHospitalizacion
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    If rs(0).Value > 0 Then
        MsgBox "No puede asociar una asistencia ambulatoria a una prueba de Hospitalizacion", vbCritical
        fblnAsoPA = False
        Exit Function
    End If
    rs.Close
End If
'PGM: SACAMOS UN MENSAJE AL INTENTAR ASOCIAR UNA EXPLORACION
'A UNA CONSULTA O INFORME
 If cboDBCombo1(0).Columns(0).Value = constASIS_EXPL Then
 
     sql = "SELECT COUNT(*) FROM PR0400,PR0100 "
     sql = sql & "WHERE PR0400.PR04NUMACTPLAN IN (" & strActPlan & ")"
     sql = sql & " AND PR0400.CI21CODPERSONA= ?"
     sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION "
     sql = sql & " AND PR0100.PR12CODACTIVIDAD IN (201,215)"
     Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngPersona
     Set rs = qry.OpenResultset(sql)
     If rs(0) > 0 Then
        MsgBox "No se puede asociar una Consulta o Informe cuando el tipo de aistencia es de Exploracion", vbOKOnly + vbInformation
        fblnAsoPA = False
        Exit Function
     End If
    rs.Close
    qry.Close
 End If
 'Si la asistencia es hospitalizada comprobamos que no haya otra prueba de
 'hospitalizacion realizandose o pedida para esa asistencia
  If cboDBCombo1(0).Columns(0).Value = constASIS_HOSP And Trim(txtText1(1).Text) <> "" Then
    sql = "SELECT PR04NUMACTPLAN FROM PR0400,PR0100 WHERE PR03NUMACTPEDI IN (" & strActPedidas
    sql = sql & ") AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & gCodActHospitalizacion
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    If Not rs.EOF Then  'en caso de que haya una hospitalizacion
'        rs.Close
        sql = "SELECT COUNT(*) FROM PR0400, PR0100  WHERE "
        sql = sql & " PR0400.CI21CODPERSONA = ?"
        sql = sql & " AND PR0400.AD01CODASISTENCI = ? "
        sql = sql & " AND PR0400.PR37CODESTADO < = " & consESTADO_REALIZANDOSE
        sql = sql & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
        sql = sql & " AND PR0100.PR12CODACTIVIDAD = " & gCodActHospitalizacion
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngPersona
            qry(1) = txtText1(1).Text
        Set rsa = qry.OpenResultset()
        If rsa(0) > 0 Then
            MsgBox "Existe ya una prueba de hospitalizacion realizandose en esta Asistencia", vbOKOnly + vbInformation
            fblnAsoPA = False
            Exit Function
        End If
        rsa.Close
        'comprobamos tambien que la hospitalizacion es para el dpto/doctor responsable
        sql = "SELECT PR0400.AD02CODDPTO, AG1100.SG02COD FROM CI0100, PR0400, AG1100 "
        sql = sql & " WHERE PR0400.PR04NUMACTPLAN = ? "
        sql = sql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN "
        sql = sql & " AND CI0100.CI01SITCITA = '1'"
        sql = sql & " AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = rs(0)
        Set rsa = qry.OpenResultset()
        If Not rsa.EOF Then
            If Not IsNull(rsa(0)) And Not IsNull(rsa(1)) Then
                If Trim(rsa(0)) <> Trim(cboDBCombo1(2).Columns(0).Value) Or Trim(rsa(1)) <> Trim(cboDBCombo1(3).Columns(0).Value) Then
                    MsgBox "la prueba de hospitalizacion y los responsables no son iguales", vbOKOnly + vbInformation
                    fblnAsoPA = False
                    Exit Function
                End If
            End If
            rsa.Close
            qry.Close
        Else
            rsa.Close
            qry.Close
            sql = "SELECT AD02CODDPTO FROM PR0400 WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = rs(0)
            Set rsa = qry.OpenResultset()
            If Not IsNull(rsa(0)) Then
                If Trim(rsa(0)) <> Trim(cboDBCombo1(2).Columns(0).Value) Then
                    MsgBox "El Departamento de la Hospitalizaci�n y el Departamento Responsable no son iguales", vbOKOnly + vbInformation
                    fblnAsoPA = False
                    Exit Function
                End If
            End If
        End If
    End If
  End If
'fecha de incio
strFechaIniProcAsis = Format(dtcDateCombo1(2).Text & " " & txtHora(1) & ":" & txtMinuto(1).Text, _
    "dd/mm/yyyy hh:mm")
If cboDBCombo1(0).Columns(0).Value = constASIS_HOSP Then
    strFechaIniAsis = Format(dtcDateCombo1(0).Text & " " & txtHora(0) & ":" & txtMinuto(0).Text, _
        "dd/mm/yyyy hh:mm")
Else
    strFechaIniAsis = Format$(strFechaHora_Sistema, "dd/mm/yyyy hh:mm")
End If
'creamos le proceso asistencia
objApp.BeginTrans
If blnCreandoAsistencia Then
'INSERTAMOS ASISTENCIA
    'COMPROBAMOS QUE LA FECHA SEA LA DE HOY O LA DE MA�ANA (ADMSION)
    strFechaHoy = strFecha_Sistema
    strFechaMa�ana = DateAdd("d", 1, strFechaHoy)
    If dtcDateCombo1(0).Text <> strFechaHoy And dtcDateCombo1(0).Text <> strFechaMa�ana Then
        MsgBox "Revise las Fehas", vbCritical
        objApp.RollbackTrans
        fblnAsoPA = False
        Exit Function
    End If
    If dtcDateCombo1(2).Text <> strFechaHoy And dtcDateCombo1(2).Text <> strFechaMa�ana Then
        MsgBox "Revise las Fehas", vbCritical
        objApp.RollbackTrans
        fblnAsoPA = False
        Exit Function
    End If
    sql = "INSERT INTO AD0100 (CI21CODPERSONA,AD01CODASISTENCI,AD01FECINICIO,CI22NUMHISTORIA)"
    sql = sql & " VALUES(?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngPersona
        qry(1) = txtText1(1).Text
        qry(2) = strFechaIniAsis
        qry(3) = strHistoria
    qry.Execute
    If qry.RowsAffected = 0 Or Err > 0 Then
        MsgBox "Fallo en la insercion AD0100", vbCritical
        objApp.RollbackTrans
        fblnAsoPA = False
        Exit Function
    End If
    qry.Close
'INSERTAMOS EN TIPO DE ASISTENCIA
    sql = "INSERT INTO AD2500 (AD01CODASISTENCI,AD25FECINICIO,AD12CODTIPOASIST,AD25FECPREVALTA)"
    sql = sql & " VALUES(?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,TO_DATE(?,'DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtText1(1).Text
        qry(1) = strFechaIniAsis
        qry(2) = cboDBCombo1(0).Columns(0).Value
        qry(3) = Format(dtcDateCombo1(1).Text, "dd/mm/yyyy")
    qry.Execute
    If qry.RowsAffected = 0 Or Err > 0 Then
        MsgBox "Fallo en la insercion AD2500", vbCritical
        objApp.RollbackTrans
        fblnAsoPA = False
        Exit Function
    End If
    qry.Close
End If
If Not blnHayPA Then 'si no hay proceso asistencia lo creamos
    'miramos que no haya uno cerrado
    Call pProcAsisCerrado(CLng(txtText1(1).Text))
    sql = "INSERT INTO AD0800 (AD07CODPROCESO,AD01CODASISTENCI,AD10CODTIPPACIEN,"
    sql = sql & " CI21CODPERSONA_ENV, CI21CODPERSONA_MED,AD08NUMCASO,AD08FECINICIO,AD08INDREQIM,AD08OBSERVAC)"
    sql = sql & " VALUES(?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = SSDBGrid1.Columns(0).Text
        qry(1) = txtText1(1).Text
        qry(2) = cboDBCombo1(1).Columns(0).Text
        
        If txtText1(3).Text = "" Then
            qry(3) = Null
        Else
            qry(3) = txtText1(3).Text
        End If
        If txtText1(5).Text = "" Then
            qry(4) = txtText1(5).Text
        Else
            qry(4) = txtText1(5).Text
        End If

        'GENERAMOS EL CASO******************************************************
        strNumHistoria = Format(strHistoria, "000000")
        intContcaso = Int(Mid$(txtText1(1), 7, 4))
        Do While strNumcaso = ""
            strNumHistoriaCaso = strNumHistoria & Format$(intContcaso, "0000")
            sqlA = "select ad08numcaso from ad0800 where ad08numcaso = '" & strNumHistoriaCaso & "'"
            Set rs = objApp.rdoConnect.OpenResultset(sqlA, rdOpenForwardOnly)
            If Not rs.EOF Then ' encontrado una historia/caso igual
              If intContcaso = 9999 Then intContcaso = 0
              intContcaso = intContcaso + 1
            Else
              If intContcaso = 0 Then intContcaso = intContcaso + 1
              strNumcaso = Format$(intContcaso, "0000")
              Exit Do
            End If
        Loop
        qry(5) = strNumHistoria & strNumcaso
        txtText1(7).Text = strNumcaso
        '************************************************************************
        qry(6) = strFechaIniProcAsis
        'necesita informe medico
        If chkCheck1(0) = 1 Then
            qry(7) = -1
        Else
            qry(7) = 0
        End If
        If txtText1(0).Text = "" Then
            qry(8) = Null
        Else
            qry(8) = txtText1(0).Text
        End If
    qry.Execute
        If qry.RowsAffected = 0 Or Err > 0 Then
            MsgBox "Fallo en la insercion AD0800" & Error, vbCritical
            objApp.RollbackTrans
            fblnAsoPA = False
            Exit Function
        End If
        qry.Close
    'AD1100 RESPONSABLE ECONOMICO PROCESO ASISTENCIA
    sql = "INSERT INTO AD1100 (AD01CODASISTENCI,AD11FECINICIO,CI32CODTIPECON,"
    sql = sql & " CI21CODPERSONA,CI13CODENTIDAD,AD11INDVOLANTE,AD07CODPROCESO) VALUES"
    sql = sql & " (?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?,?,?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtText1(1).Text
        qry(1) = strFechaIniProcAsis
        qry(2) = Trim(cboDBCombo1(4).Text)
        If Trim(cboDBCombo1(6).Columns(0).Text) <> "" Then
            qry(3) = Trim(cboDBCombo1(6).Columns(0).Text)
        Else
            MsgBox "No tengo codigo de Responsable Economico"
            objApp.RollbackTrans
            fblnAsoPA = False
            Exit Function
        End If
        qry(4) = Trim(cboDBCombo1(5).Text)
        If chkCheck1(1) = 1 Then
            qry(5) = -1
        Else
            qry(5) = 0
        End If
        qry(6) = SSDBGrid1.Columns(0).Text
    qry.Execute
        If qry.RowsAffected = 0 Or Err > 0 Then
            MsgBox "Fallo en la insercion AD1100", vbCritical
            objApp.RollbackTrans
            fblnAsoPA = False
            Exit Function
        End If
        qry.Close
    sql = "INSERT INTO AD0500 (AD07CODPROCESO, AD01CODASISTENCI,AD05FECINIRESPON,"
    sql = sql & " AD02CODDPTO,SG02COD) VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI'),?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = SSDBGrid1.Columns(0).Text
        qry(1) = txtText1(1).Text
        qry(2) = strFechaIniProcAsis
        qry(3) = cboDBCombo1(2).Columns(0).Value
        qry(4) = cboDBCombo1(3).Columns(0).Value
    qry.Execute
        If qry.RowsAffected = 0 Or Err > 0 Then
            MsgBox "Fallo en la insercion AD0500", vbCritical
            objApp.RollbackTrans
            fblnAsoPA = False
            Exit Function
        End If
        Call PasoSIHC(strNumHistoria & strNumcaso)
        If Err > 0 Then
            MsgBox "Fallo en PasoSIHC", vbCritical
            objApp.RollbackTrans
            fblnAsoPA = False
            Exit Function
        End If
End If
'Comprobamos la documentacion
If Trim(txtMensaje.Text) <> "" Then pComDoc
'Asociamos la asistencia a las pruebas

sql = "UPDATE PR0800 SET AD01CODASISTENCI  = ? WHERE PR03NUMACTPEDI IN (" & _
strActPedidas & ") AND AD01CODASISTENCI IS NULL "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtText1(1).Text
qry.Execute
    If Err > 0 Then
        MsgBox "Fallo en UPDATE PR0800", vbCritical
        objApp.RollbackTrans
        fblnAsoPA = False
        Exit Function
    End If
qry.Close
'PGM: BORRAMOS LAS FECHAS DE PLANIFICACION AL ASOCIAR UNA ASISTENCIA
'HOSPITALIZADA DE LOS DPTOS REALIZADORES QUE PERTENECEN A LABORATORIO
If cboDBCombo1(0).Columns(0).Value = 1 Then
    sql = "UPDATE PR0400 SET PR04FECPLANIFIC = NULL"
    sql = sql & " WHERE PR04NUMACTPLAN IN("
    sql = sql & strActPlan & ")"
    sql = sql & " AND AD02CODDPTO IN (" & strDptosLab & ")"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry.Execute
    If Err > 0 Then
      MsgBox "Fallo en UPDATE PR0400", vbCritical
      objApp.RollbackTrans
      fblnAsoPA = False
      Exit Function
    End If
    qry.Close
End If
sql = "UPDATE PR0400 SET AD01CODASISTENCI = ? WHERE PR04NUMACTPLAN IN(" & _
strActPlan & ")"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtText1(1).Text
qry.Execute
    If qry.RowsAffected = 0 Or Err > 0 Then
        MsgBox "Fallo en UPDATE PR0400", vbCritical
        objApp.RollbackTrans
        fblnAsoPA = False
        Exit Function
    End If
qry.Close
objApp.CommitTrans


End Function

Private Sub pAbriAsitencia()
Dim strFecha As String
Dim rs As rdoResultset

Set rs = objApp.rdoConnect.OpenResultset("SELECT AD01CODASISTENCI_S.NEXTVAL FROM DUAL", rdOpenKeyset)
txtText1(1).Text = rs(0).Value
strFecha = strFechaHora_Sistema
cboDBCombo1(0).Enabled = True
dtcDateCombo1(0).Text = Format(strFecha, "dd/mm/yyyy")
txtHora(0).Text = Left(Format(strFecha, "hh:mm"), 2)
txtMinuto(0).Text = Right(Format(strFecha, "hh:mm"), 2)
'PONEMOS TAMBIEN LA HORA DEL PROCESO ASISTENCIA
dtcDateCombo1(2).Text = Format(strFecha, "dd/mm/yyyy")
txtHora(1).Text = Left(Format(strFecha, "hh:mm"), 2)
txtMinuto(1).Text = Right(Format(strFecha, "hh:mm"), 2)

End Sub
Private Sub pCombo1()
Dim sql As String
Dim rs As rdoResultset

    cboDBCombo1(1).RemoveAll
    If cboDBCombo1(0).Columns(0).Value <> "" Then
        sql = "SELECT AD10CODTIPPACIEN,AD10DESTIPPACIEN FROM AD1000 WHERE AD10FECFIN IS NULL AND "
        Select Case cboDBCombo1(0).Columns(0).Value
            Case 1, 2 'HOSPITALIZADO-AMBULATORIA
                sql = sql & " AD10CODTIPPACIEN IN (1,2,3)"
            Case 3 'EXPLORACION
                sql = sql & "  AD10CODTIPPACIEN = 4"
            Case 4 'SEGUNDA CONSULTA
                sql = sql & "  AD10CODTIPPACIEN = 5"
            Case 5 'Donante
                sql = sql & "  AD10CODTIPPACIEN = 6"
            Case 6 'productos
                sql = sql & "  AD10CODTIPPACIEN = 7"
        End Select
        Set rs = objApp.rdoConnect.OpenResultset(sql)
        While Not rs.EOF
            cboDBCombo1(1).AddItem rs!AD10CODTIPPACIEN & ";" & rs!AD10DESTIPPACIEN
            rs.MoveNext
        Wend
    End If
    cboDBCombo1(1).Text = ""
    
End Sub

Private Sub pProcAsisCerrado(nAsis As Long)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim lngNewProc As Long
sql = "SELECT COUNT(*) FROM AD0800 WHERE AD07CODPROCESO = ?"
sql = sql & " AND AD01CODASISTENCI = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = SSDBGrid1.Columns(0).Text
    qry(1) = nAsis
Set rs = qry.OpenResultset()
If rs(0) > 0 Then
    rs.Close
    qry.Close
   Set rs = objApp.rdoConnect.OpenResultset("SELECT AD07CODPROCESO_S.NEXTVAL FROM DUAL", rdOpenKeyset)
    lngNewProc = rs(0).Value
    rs.Close
    sql = "INSERT INTO AD0700 (CI21CODPERSONA,AD07CODPROCESO,"
    sql = sql & " AD07DESNOMBPROCE,AD07FECHORAINICI,AD07FECHORAFIN,"
    sql = sql & " CI22NUMHISTORIA,AD34CODESTADO) SELECT CI21CODPERSONA,"
    sql = sql & " ?,AD07DESNOMBPROCE,AD07FECHORAINICI,AD07FECHORAFIN,"
    sql = sql & " CI22NUMHISTORIA,AD34CODESTADO FROM AD0700 WHERE"
    sql = sql & " AD07CODPROCESO = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNewProc
        qry(1) = SSDBGrid1.Columns(0).Text
    qry.Execute
    qry.Close
  
    sql = "INSERT INTO AD0400 (AD07CODPROCESO,AD04FECINIRESPON,"
    sql = sql & " AD02CODDPTO,SG02COD) SELECT ?,"
    sql = sql & " AD04FECINIRESPON, AD02CODDPTO,"
    sql = sql & " SG02COD FROM AD0400 WHERE AD07CODPROCESO = ?"
    sql = sql & " AND AD04FECFINRESPON IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNewProc
        qry(1) = SSDBGrid1.Columns(0).Text
    qry.Execute
    qry.Close
    
    SSDBGrid1.Columns(0).Text = lngNewProc
    
    sql = "UPDATE PR0400 SET AD07CODPROCESO = ? WHERE PR04NUMACTPLAN IN ("
    sql = sql & strActPlan & ")"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNewProc
    qry.Execute
    qry.Close
    sql = "UPDATE PR0800 SET AD07CODPROCESO = ? WHERE PR03NUMACTPEDI IN ("
    sql = sql & strActPedidas & ")"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = lngNewProc
    qry.Execute
    qry.Close
    End If
End Sub
Private Sub pGuardarDocumentacion()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strCodDoc As String
Dim intR As Integer


sql = "SELECT AD18CODDOCUMENTO_S.NEXTVAL FROM DUAL"
Set rs = objApp.rdoConnect.OpenResultset(sql)
    strCodDoc = rs(0)
rs.Close
sql = "INSERT INTO AD1800 (CI21CODPERSONA,AD18CODDOCUMENTO,"
sql = sql & " AD19CODTIPODOCUM,AD18DESDOCUMENTO,AD18FECDOCUMENTO,"
sql = sql & " AD01CODASISTENCI,AD07CODPROCESO,AD35CODTIPVOL,"
sql = sql & " AD18FECFINVOLANTE, AD18NUMVOLANTE) VALUES (?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),"
sql = sql & " ?,?,?,TO_DATE(?,'DD/MM/YYYY'),?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
    qry(1) = strCodDoc
    qry(2) = grdDoc.Columns("Tipo documento").Value
    If Trim(grdDoc.Columns("Descripci�n").Value <> "") Then qry(3) = grdDoc.Columns("Descripci�n").Value Else qry(3) = Null
    qry(4) = Format(grdDoc.Columns("F. Ini. Vol/Doc").Value, "dd/mm/yyyy")
    qry(5) = txtText1(1).Text
    qry(6) = SSDBGrid1.Columns(0).Text
    If Trim(grdDoc.Columns("Tipo Volante").Value <> "") Then qry(7) = ddrTiVolante.Columns(0).Value Else qry(7) = Null
    If Trim(grdDoc.Columns("F. Fin Validez").Value <> "") Then qry(8) = grdDoc.Columns("F. Fin Validez").Value Else qry(8) = Null
    If Trim(grdDoc.Columns("N� de Volante").Value <> "") Then qry(9) = grdDoc.Columns("N� de Volante").Value Else qry(9) = Null
qry.Execute
qry.Close
sql = "INSERT INTO AD4300 (CI21CODPERSONA, AD18CODDOCUMENTO, "
sql = sql & " AD07CODPROCESO, AD01CODASISTENCI) VALUES (?,?,?,?) "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
    qry(1) = strCodDoc
    qry(3) = txtText1(1).Text
    qry(2) = SSDBGrid1.Columns(0).Text
qry.Execute
qry.Close
 If chkCheck1(1).Value = 1 Then
        intR = MsgBox("Continua Pendiente de volante?", vbYesNo, Me.Caption)
        If intR = vbNo Then
            sql = "UPDATE AD1100 SET AD11INDVOLANTE = 0 WHERE AD01CODASISTENCI = ?"
            sql = sql & " AND AD07CODPROCESO = ? "
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = txtText1(1).Text
                qry(1) = SSDBGrid1.Columns(0).Text
            qry.Execute
            qry.Close
            chkCheck1(1).Value = 0
        End If
    End If
Call pCargarDocumentacion
cmdNuevaDoc.Enabled = True
cmdAsoDoc.Enabled = True
cmdGuardarDoc.Enabled = False
End Sub

Private Sub pCombo6()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

If cboDBCombo1(4).Text <> "" And cboDBCombo1(5).Text <> "" _
And cboDBCombo1(6).Text <> "" Then
    sql = "SELECT CI09DOCREQ FROM CI0900 WHERE CI21CODPERSONA IN (SELECT CI21CODPERSONA "
    sql = sql & " FROM CI2900 WHERE CI21CODPERSONA_REC = ? )"
    sql = sql & " AND CI13CODENTIDAD = ? AND CI32CODTIPECON = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = cboDBCombo1(6).Columns(0).Value
        qry(1) = cboDBCombo1(5).Text
        qry(2) = cboDBCombo1(4).Text
        
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        If IsNull(rs!CI09DOCREQ) Then txtMensaje = "" Else txtMensaje = rs!CI09DOCREQ
    Else
        txtMensaje = ""
    End If
    rs.Close
    qry.Close
End If
End Sub

Private Sub pComDoc()
Dim sql As String
Dim qry As rdoQuery
Dim qry1 As rdoQuery
Dim rs As rdoResultset
Dim rs1 As rdoResultset
Dim intR As Integer
Dim strCodDoc As String


sql = "SELECT AD1800.AD18CODDOCUMENTO FROM AD1800, AD0500 WHERE "
sql = sql & " AD1800.CI21CODPERSONA = ? "
sql = sql & " AND AD1800.AD18FECFINVOLANTE >= TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND AD1800.AD18FECDOCUMENTO <= TO_DATE(?,'DD/MM/YYYY HH24:MI')"
sql = sql & " AND AD1800.AD18CODDOCUMENTO NOT IN (SELECT AD18CODDOCUMENTO "
sql = sql & " FROM AD4300 WHERE CI21CODPERSONA = ? "
sql = sql & " AND AD4300.AD01CODASISTENCI = ? "
sql = sql & " AND AD4300.AD07CODPROCESO = ? )"
sql = sql & " AND AD1800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI "
sql = sql & " AND AD1800.AD07CODPROCESO = AD0500.AD07CODPROCESO"
sql = sql & " AND AD0500.SG02COD = ?"
sql = sql & " AND AD0500.AD02CODDPTO = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
    qry(1) = dtcDateCombo1(2).Text & " " & txtHora(1) & ":" & txtMinuto(1)
    qry(2) = dtcDateCombo1(2).Text & " " & txtHora(1) & ":" & txtMinuto(1)
    qry(3) = lngPersona
    qry(4) = txtText1(1).Text
    qry(5) = SSDBGrid1.Columns(0).Text
    qry(6) = cboDBCombo1(3).Columns(0).Value
    qry(7) = cboDBCombo1(2).Columns(0).Value
Set rs = qry.OpenResultset()
If Not rs.EOF Then
    intR = MsgBox("Existe un documento en vigencia para el mismo Departamento Doctor" _
    & Chr$(13) & "�Desea asociarlo? ", vbYesNo + vbQuestion, Me.Caption)
    If intR = vbYes Then
        'INSERT EN TABLA NUEVA DE FELIPE
        While Not rs.EOF
            sql = "INSERT INTO AD4300 (CI21CODPERSONA,AD18CODDOCUMENTO,"
            sql = sql & " AD01CODASISTENCI, AD07CODPROCESO) VALUES(?,?,?,?)"
            Set qry1 = objApp.rdoConnect.CreateQuery("", sql)
                qry1(0) = lngPersona
                qry1(1) = rs!AD18CODDOCUMENTO
                qry1(2) = txtText1(1).Text
                qry1(3) = SSDBGrid1.Columns(0).Text
            qry1.Execute
            qry1.Close
            rs.MoveNext
        Wend
    Else
        If Trim(txtMensaje) <> "" And chkCheck1(1).Value = 0 Then
            MsgBox "Es necesario aportar Documentacion" & Chr$(13) & _
            "El paciente quedara pendiente de volante ", vbInformation, Me.Caption
            sql = "UPDATE AD1100 SET AD11INDVOLANTE = -1 WHERE AD01CODASISTENCI = ? "
            sql = sql & " AND AD07CODPROCESO = ? "
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = txtText1(1).Text
                qry(1) = SSDBGrid1.Columns(0).Text
            qry.Execute
            qry.Close
            chkCheck1(1).Value = 1
        End If
    End If
'    rs.Close
'    qry.Close
Else 'SI NO HAY NINGUN DOCUMENTO  PARA ASOCIAR MIRAMOS SI TIENE QUE TENER Y SI YA EXISTE
   If Trim(txtMensaje) <> "" And chkCheck1(1).Value = 0 Then
        sql = "SELECT COUNT(*) FROM AD4300 WHERE CI21CODPERSONA = ? "
        sql = sql & " AND  AD01CODASISTENCI = ? AND AD07CODPROCESO = ? "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = lngPersona
            qry(1) = txtText1(1).Text
            qry(2) = SSDBGrid1.Columns(0).Text
        Set rs = qry.OpenResultset()
        If rs(0) = 0 Then
            rs.Close
            qry.Close
            MsgBox "Es necesario aportar Documentacion" & Chr$(13) & _
            "El paciente quedara pendiente de volante ", vbInformation, Me.Caption
            sql = "UPDATE AD1100 SET AD11INDVOLANTE = -1 WHERE AD01CODASISTENCI = ? "
            sql = sql & " AND AD07CODPROCESO = ? "
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = txtText1(1).Text
                qry(1) = SSDBGrid1.Columns(0).Text
            qry.Execute
            qry.Close
            chkCheck1(1).Value = 1
        End If
   End If
End If
End Sub
Private Sub pAsocDoc()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim intR As Integer

sql = "SELECT COUNT(*) FROM AD4300 WHERE  "
sql = sql & " AD18CODDOCUMENTO = ?  AND CI21CODPERSONA = ? "
sql = sql & " AND AD01CODASISTENCI = ? AND AD07CODPROCESO = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = grdDoc.Columns("cDoc").CellValue(grdDoc.SelBookmarks(0))
    qry(1) = lngPersona
    qry(2) = txtText1(1).Text
    qry(3) = SSDBGrid1.Columns(0).Text
Set rs = qry.OpenResultset()
If rs(0) <> 0 Then
    MsgBox "Esta Documentacion ya esta asociada", vbInformation, Me.Caption
Else
    If grdDoc.Columns("cDpto").Text <> cboDBCombo1(2).Columns(0).Value And Trim(grdDoc.Columns("cDpto").Text) <> "" Then
        MsgBox "Este Documento no se trajo para el departamento" & _
        " responsble de esta asistencia", vbExclamation, Me.Caption
        Exit Sub
    End If
    If grdDoc.Columns("cdoctor").Value <> cboDBCombo1(3).Columns(0).Value And Trim(grdDoc.Columns("cdoctor").Text) <> "" Then
        intR = MsgBox("El doctor Responsable para el que se trajo este volante es diferente" & _
        "�Desea Asociarlo?", vbYesNo + vbQuestion, Me.Caption)
        If intR = vbNo Then Exit Sub
    End If
    rs.Close
    qry.Close
    sql = "INSERT INTO AD4300 (AD18CODDOCUMENTO, CI21CODPERSONA, AD01CODASISTENCI, AD07CODPROCESO) "
    sql = sql & " VALUES (?,?,?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdDoc.Columns("cDoc").CellValue(grdDoc.SelBookmarks(0))
        qry(1) = lngPersona
        qry(2) = txtText1(1).Text
        qry(3) = SSDBGrid1.Columns(0).Text
    qry.Execute
    qry.Close
    sql = "SELECT AD07CODPROCESO FROM AD1800 WHERE CI21CODPERSONA = ? "
    sql = sql & " AND AD18CODDOCUMENTO = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(1) = grdDoc.Columns("cDoc").CellValue(grdDoc.SelBookmarks(0))
            qry(0) = lngPersona
    Set rs = qry.OpenResultset()
    If IsNull(rs!AD07CODPROCESO) Then
        rs.Close
        qry.Close
        sql = "UPDATE AD1800 SET AD01CODASISTENCI = ?, AD07CODPROCESO = ? "
        sql = sql & " WHERE CI21CODPERSONA = ? AND AD18CODDOCUMENTO = ? "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = txtText1(1).Text
            qry(1) = SSDBGrid1.Columns(0).Text
            qry(2) = lngPersona
            qry(3) = grdDoc.Columns("cDoc").CellValue(grdDoc.SelBookmarks(0))
        qry.Execute
        qry.Close
        pCargarDocumentacion
    End If
    
    If chkCheck1(1).Value = 1 Then
        intR = MsgBox("Continua Pendiente de volante?", vbYesNo, Me.Caption)
        If intR = vbNo Then
            sql = "UPDATE AD1100 SET AD11INDVOLANTE = 0 WHERE AD01CODASISTENCI = ?"
            sql = sql & " AND AD07CODPROCESO = ? "
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = txtText1(1).Text
                qry(1) = SSDBGrid1.Columns(0).Text
            qry.Execute
            qry.Close
            chkCheck1(1).Value = 0
        End If
    End If
End If
End Sub


Private Sub peliminar()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim intR As Integer

On Error GoTo CancelTrans

objApp.BeginTrans

sql = "SELECT COUNT(*) FROM AD4300 WHERE CI21CODPERSONA = ? AND AD18CODDOCUMENTO = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
    qry(1) = grdDoc.Columns("cDoc").CellValue(grdDoc.SelBookmarks(0))
Set rs = qry.OpenResultset
If rs(0) > 1 Then
    MsgBox "Este Documento pertenece a mas de una asistencia, no se puede eliminar", vbExclamation, Me.Caption
    Exit Sub
End If
rs.Close
qry.Close
sql = "DELETE FROM AD4300 WHERE CI21CODPERSONA = ? AND AD18CODDOCUMENTO = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
    qry(1) = grdDoc.Columns("cDoc").CellValue(grdDoc.SelBookmarks(0))
qry.Execute
qry.Close
sql = "DELETE FROM AD1800 WHERE CI21CODPERSONA = ? AND AD18CODDOCUMENTO = ? "
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngPersona
    qry(1) = grdDoc.Columns("cDoc").CellValue(grdDoc.SelBookmarks(0))
qry.Execute
qry.Close
MsgBox "Documento Eliminado", vbOKOnly, Me.Caption
If chkCheck1(1).Value = 0 Then
     intR = MsgBox("Queda el responsable/asistencia pendiente de volante", vbYesNo + vbQuestion, Me.Caption)
    If intR = vbYes Then
        sql = "UPDATE AD1100 SET AD11INDVOLANTE = -1 WHERE AD01CODASISTENCI = ? AND AD07CODPROCESO = ? "
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = txtText1(1).Text
                qry(1) = SSDBGrid1.Columns(0).Text
        qry.Execute
        qry.Close
        chkCheck1(1).Value = 1
    End If
End If
pCargarDocumentacion
objApp.CommitTrans
Exit Sub
CancelTrans:
    MsgBox "Error al eliminar el documento", vbExclamation, Me.Caption
    objApp.RollbackTrans
End Sub
