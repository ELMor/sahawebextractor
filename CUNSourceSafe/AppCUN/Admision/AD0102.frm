VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "idperson.ocx"
Begin VB.Form frmProcesos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Procesos"
   ClientHeight    =   8280
   ClientLeft      =   60
   ClientTop       =   645
   ClientWidth     =   11895
   HelpContextID   =   3
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8280
   ScaleWidth      =   11895
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   22
      Top             =   0
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin idperson.IdPersona IdPersona1 
      Height          =   2295
      Left            =   11520
      TabIndex        =   63
      Top             =   600
      Visible         =   0   'False
      Width           =   375
      _ExtentX        =   661
      _ExtentY        =   4048
      BackColor       =   12648384
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Datafield       =   "CI21CodPersona"
      MaxLength       =   7
      blnAvisos       =   0   'False
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Responsables Proceso"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3045
      Index           =   2
      Left            =   450
      TabIndex        =   28
      Top             =   4830
      Width           =   10995
      Begin TabDlg.SSTab tabTab1 
         Height          =   2565
         Index           =   2
         Left            =   180
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   360
         Width           =   10635
         _ExtentX        =   18759
         _ExtentY        =   4524
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Det&alle"
         TabPicture(0)   =   "AD0102.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(12)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(11)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(13)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(8)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "UpDownHora(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "UpDownMinuto(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboDBCombo1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "cboDBCombo1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(8)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(9)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(14)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtMinuto(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtHora(2)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtMinuto(3)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtHora(3)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "Frame2"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD0102.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   615
            Left            =   8040
            TabIndex        =   62
            Top             =   840
            Width           =   1215
         End
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   3
            Left            =   1920
            MaxLength       =   2
            TabIndex        =   50
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   2070
            Width           =   390
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            HelpContextID   =   3
            Index           =   3
            Left            =   2430
            MaxLength       =   2
            TabIndex        =   49
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   2070
            Width           =   390
         End
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   2
            Left            =   1920
            MaxLength       =   2
            TabIndex        =   45
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   300
            Width           =   390
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            HelpContextID   =   3
            Index           =   2
            Left            =   2610
            MaxLength       =   2
            TabIndex        =   44
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   300
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   14
            Left            =   1380
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Descripci�n Departamento|Descrici�n departamento"
            Top             =   900
            Width           =   3315
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD07CODPROCESO"
            Height          =   315
            HelpContextID   =   40101
            Index           =   9
            Left            =   8250
            MaxLength       =   7
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Proceso|C�digo de Proceso"
            Top             =   1020
            Width           =   765
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   8
            Left            =   1350
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Doctor|Doctor"
            Top             =   1440
            Width           =   6525
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "AD02CODDPTO"
            Height          =   300
            Index           =   1
            Left            =   180
            TabIndex        =   15
            Tag             =   "C�digo Departamento|C�digo Departamento"
            Top             =   900
            Width           =   1050
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).Picture=   "AD0102.frx":0038
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD0102.frx":0054
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "FechaFin"
            Columns(2).Name =   "FechaFin"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   1852
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "SG02COD"
            Height          =   330
            Index           =   2
            Left            =   180
            TabIndex        =   17
            Tag             =   "C�digo Doctor|C�digo Doctor"
            Top             =   1440
            Width           =   1050
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            stylesets.count =   2
            stylesets(0).Name=   "Activo"
            stylesets(0).Picture=   "AD0102.frx":0070
            stylesets(1).Name=   "Inactivo"
            stylesets(1).BackColor=   255
            stylesets(1).Picture=   "AD0102.frx":008C
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   5
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   2963
            Columns(2).Caption=   "Apellido 1"
            Columns(2).Name =   "Apellido1"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Caption=   "Apellido 2"
            Columns(3).Name =   "Apellido2"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   8
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "FechaFin"
            Columns(4).Name =   "FechaFin"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   8
            Columns(4).FieldLen=   256
            _ExtentX        =   1852
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 0"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD04FECINIRESPON"
            Height          =   330
            Index           =   3
            Left            =   210
            TabIndex        =   14
            Tag             =   "Fecha Inicio|Fecha Inicio"
            Top             =   300
            Width           =   1635
            _Version        =   65537
            _ExtentX        =   2884
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2295
            Index           =   2
            Left            =   -74880
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   150
            Width           =   10065
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17754
            _ExtentY        =   4048
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD04FECFINRESPON"
            Height          =   330
            Index           =   4
            Left            =   210
            TabIndex        =   35
            Tag             =   "Fecha Fin|Fecha Fin"
            Top             =   2070
            Width           =   1635
            _Version        =   65537
            _ExtentX        =   2884
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin ComCtl2.UpDown UpDownMinuto 
            Height          =   330
            Index           =   2
            Left            =   3000
            TabIndex        =   46
            Top             =   300
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   582
            _Version        =   327681
            BuddyControl    =   "txtMinuto(2)"
            BuddyDispid     =   196612
            BuddyIndex      =   2
            OrigLeft        =   2880
            OrigTop         =   1110
            OrigRight       =   3120
            OrigBottom      =   1440
            Increment       =   5
            Max             =   55
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   0   'False
         End
         Begin ComCtl2.UpDown UpDownHora 
            Height          =   330
            Index           =   2
            Left            =   2330
            TabIndex        =   47
            Top             =   300
            Width           =   240
            _ExtentX        =   423
            _ExtentY        =   582
            _Version        =   327681
            BuddyControl    =   "txtHora(2)"
            BuddyDispid     =   196611
            BuddyIndex      =   2
            OrigLeft        =   5760
            OrigTop         =   360
            OrigRight       =   6000
            OrigBottom      =   690
            Max             =   23
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   0   'False
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   2340
            TabIndex        =   52
            Top             =   2130
            Width           =   75
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1920
            TabIndex        =   51
            Top             =   1830
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1920
            TabIndex        =   48
            Top             =   60
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   210
            TabIndex        =   36
            Top             =   1830
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   180
            TabIndex        =   33
            Top             =   660
            Width           =   1200
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   210
            TabIndex        =   32
            Top             =   60
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Doctor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   180
            TabIndex        =   31
            Top             =   1200
            Width           =   585
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Procesos del Paciente"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2205
      Index           =   1
      Left            =   450
      TabIndex        =   9
      Top             =   2610
      Width           =   10995
      Begin TabDlg.SSTab tabTab1 
         Height          =   1755
         Index           =   1
         Left            =   180
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   330
         Width           =   10635
         _ExtentX        =   18759
         _ExtentY        =   3096
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "&Detalle"
         TabPicture(0)   =   "AD0102.frx":00A8
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(9)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(5)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(14)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(7)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(12)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "cmdCommand1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtMinuto(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtHora(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtMinuto(1)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtHora(1)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(0)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).ControlCount=   19
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD0102.frx":00C4
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            Index           =   0
            Left            =   6240
            TabIndex        =   54
            Tag             =   "Persona|C�digo de Persona"
            Top             =   960
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   1
            Left            =   5130
            MaxLength       =   2
            TabIndex        =   41
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   1110
            Width           =   390
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            HelpContextID   =   3
            Index           =   1
            Left            =   5640
            MaxLength       =   2
            TabIndex        =   40
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   1110
            Width           =   390
         End
         Begin VB.TextBox txtHora 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            Index           =   0
            Left            =   1800
            MaxLength       =   2
            TabIndex        =   38
            TabStop         =   0   'False
            Tag             =   "Hora de Fin"
            Top             =   1110
            Width           =   390
         End
         Begin VB.TextBox txtMinuto 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   330
            HelpContextID   =   3
            Index           =   0
            Left            =   2340
            MaxLength       =   2
            TabIndex        =   37
            TabStop         =   0   'False
            Tag             =   "Minutos de Fin"
            Top             =   1110
            Width           =   390
         End
         Begin VB.CommandButton cmdCommand1 
            Caption         =   "Asistencias"
            Enabled         =   0   'False
            Height          =   405
            Index           =   0
            Left            =   7770
            TabIndex        =   34
            Top             =   1020
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   12
            Left            =   8760
            TabIndex        =   20
            Tag             =   "Persona|C�digo de Persona"
            Top             =   690
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD07CODPROCESO"
            Height          =   300
            Index           =   6
            Left            =   90
            TabIndex        =   11
            Tag             =   "C�digo de Proceso|C�digo de Proceso"
            Top             =   390
            Width           =   1155
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD07DESNOMBPROCE"
            Height          =   300
            Index           =   7
            Left            =   1470
            TabIndex        =   12
            Tag             =   "Nombre del Proceso|Nombre del Proceso"
            Top             =   390
            Width           =   7500
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1515
            Index           =   1
            Left            =   -74880
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   120
            Width           =   10065
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17754
            _ExtentY        =   2672
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD07FECHORAFIN"
            Height          =   330
            Index           =   2
            Left            =   3420
            TabIndex        =   13
            Tag             =   "Fecha Fin|Fecha Fin"
            Top             =   1110
            Width           =   1635
            _Version        =   65537
            _ExtentX        =   2884
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "AD07FECHORAINICI"
            Height          =   330
            Index           =   1
            Left            =   120
            TabIndex        =   61
            Tag             =   "Fecha Inicio|Fecha Inicio"
            Top             =   1110
            Width           =   1620
            _Version        =   65537
            _ExtentX        =   2857
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            CaptionBevelType=   0
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   2220
            TabIndex        =   53
            Top             =   1170
            Width           =   75
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   ":"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   5550
            TabIndex        =   43
            Top             =   1170
            Width           =   75
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   5130
            TabIndex        =   42
            Top             =   870
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   1800
            TabIndex        =   39
            Top             =   870
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   1440
            TabIndex        =   26
            Top             =   150
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proceso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   90
            TabIndex        =   25
            Top             =   150
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   90
            TabIndex        =   24
            Top             =   870
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   3420
            TabIndex        =   23
            Top             =   870
            Width           =   855
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Index           =   0
      Left            =   420
      TabIndex        =   6
      Top             =   540
      Width           =   11025
      Begin TabDlg.SSTab tabTab1 
         Height          =   1575
         Index           =   0
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   10725
         _ExtentX        =   18918
         _ExtentY        =   2778
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "&Detalle"
         TabPicture(0)   =   "AD0102.frx":00E0
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(15)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(16)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(17)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(18)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(19)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(20)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(10)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD0102.frx":00FC
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            Index           =   10
            Left            =   6240
            TabIndex        =   5
            Tag             =   "Apellido 2�"
            Top             =   1080
            Width           =   2500
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            Index           =   5
            Left            =   3240
            TabIndex        =   4
            Tag             =   "Apellido 1�"
            Top             =   1080
            Width           =   2500
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            Index           =   4
            Left            =   360
            TabIndex        =   3
            Tag             =   "Nombre"
            Top             =   1080
            Width           =   2500
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "CI22DNI"
            Height          =   330
            Index           =   3
            Left            =   4680
            TabIndex        =   2
            Tag             =   "DNI"
            Top             =   360
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            Index           =   2
            Left            =   2520
            TabIndex        =   1
            Tag             =   "Historia"
            Top             =   360
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   1
            Left            =   360
            TabIndex        =   0
            Tag             =   "C�digo de Proceso|C�digo de Proceso"
            Top             =   360
            Width           =   1800
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1335
            HelpContextID   =   3
            Index           =   0
            Left            =   -74820
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   120
            Width           =   10065
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17754
            _ExtentY        =   2355
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   6240
            TabIndex        =   60
            Top             =   840
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   3240
            TabIndex        =   59
            Top             =   840
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   360
            TabIndex        =   58
            Top             =   840
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "D.N.I"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   4680
            TabIndex        =   57
            Top             =   120
            Width           =   465
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   2520
            TabIndex        =   56
            Top             =   120
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   360
            TabIndex        =   55
            Top             =   120
            Width           =   705
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   21
      Top             =   7995
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmProcesos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim strFecha As String

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfoP As New clsCWForm
  Dim objDetailInfoR As New clsCWForm
  Dim strKey As String
  
  'Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    .strName = "Personas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "CI2200"
    .strWhere = " CI2200.AD34CODESTADO = 1 "
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
    
    Call .FormCreateFilterWhere("CI2200", "Pacientes")
    Call .FormAddFilterWhere("CI2200", "CI21CODPERSONA", "Persona", cwNumeric)
    Call .FormAddFilterWhere("CI2200", "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22PRIAPEL", "Apellido 1�", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22SEGAPEL", "Apellido 2�", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22DNI", "DNI", cwString)
    Call .FormAddFilterWhere("CI2200", "CI22FECNACIM", "Fecha de nacimiento", cwDate)
    Call .FormAddFilterWhere("CI2200", "CI22NUMHISTORIA", "Historia", cwNumeric)
    Call .FormAddFilterWhere("CI2200", "CI22NUMSEGSOC", "N� Seguridad Social", cwString)
    
  End With
  
  With objDetailInfoP
    .strName = "Procesos"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "AD0700"
    .strWhere = "AD0700.AD34CODESTADO = 1 "
    Call .FormAddOrderField("AD07FECHORAINICI", cwDescending)
    'Call .FormAddRelation("CI21CODPERSONA", IdPersona1)             'nuevo
    Call .FormAddRelation("CI21CODPERSONA", txtText1(1))
    .intAllowance = cwAllowAdd + cwAllowModify
    
    Call .FormCreateFilterWhere("AD0700", "Procesos del Paciente")
    Call .FormAddFilterWhere("AD0700", "AD07DESNOMBPROCE", "Nombre del Proceso", cwString)
    Call .FormAddFilterWhere("AD0700", "AD07FECHORAINICI", "Fecha de Inicio", cwDate)
    Call .FormAddFilterOrder("AD0700", "AD07DESNOMBPROCE", "Nombre")
  End With
  
  With objDetailInfoR
    .strName = "Responsable Proceso"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = tabTab1(2)
    Set .grdGrid = grdDBGrid1(2)
    .strTable = "AD0400"
    Call .FormAddOrderField("AD04FECINIRESPON", cwDescending)
    Call .FormAddRelation("AD07CODPROCESO", txtText1(6))
    .intAllowance = cwAllowAdd
    
    Call .FormCreateFilterWhere("AD0400", "Responsables del Proceso")
    Call .FormAddFilterWhere("AD0400", "AD04FECINIRESPON", "Fecha de Inicio", cwDate)
    Call .FormAddFilterOrder("AD0400", "AD04FECINIRESPON", "Fecha de Inicio")
  End With
    
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfoP, cwFormDetail)
    Call .FormAddInfo(objDetailInfoR, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInGrid = False
    .CtrlGetInfo(txtText1(9)).blnInGrid = False
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(4)).blnReadOnly = True
    
    '.CtrlGetInfo(cboDBCombo1(1)).blnForeign = True
    '.CtrlGetInfo(cboDBCombo1(2)).blnForeign = True
    
    .CtrlGetInfo(txtHora(0)).blnNegotiated = False
    .CtrlGetInfo(txtHora(1)).blnNegotiated = False
    .CtrlGetInfo(txtHora(2)).blnNegotiated = False
    .CtrlGetInfo(txtMinuto(0)).blnNegotiated = False
    .CtrlGetInfo(txtMinuto(1)).blnNegotiated = False
    .CtrlGetInfo(txtMinuto(2)).blnNegotiated = False
    
' PARA LOS COMBOS CUANDO FUNCIONEN
    .CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD02CODDPTO,AD02DESDPTO FROM " & "AD0200 WHERE (AD02INDRESPONPROC = -1) AND ((AD02FECFIN IS NULL) " & _
                                          " OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL))) ORDER BY AD02CODDPTO"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(1)), "AD02CODDPTO", "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(1)), txtText1(14), "AD02DESDPTO")
    '
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(2)), "SG02COD", "SELECT SG02COD,SG02NOM || ' ' || SG02APE1|| ' ' || SG02APE2 AS DOCTOR FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(2)), txtText1(8), "DOCTOR")
    
    
    
    '.CtrlGetInfo(IdPersona1).blnForeign = True 'nuevo
    '.CtrlGetInfo(IdPersona1).blnInFind = True  'nuevo
    'IdPersona1.ReadPersona                     'nuevo
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  'Invisible bot�n de B�squeda
  IdPersona1.blnSearchButton = False
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  Dim rs As rdoResultset
  Dim vntA As Variant
  
'Comprobar que al salir, el proceso seleccionado tenga asignado un responsable
  If txtText1(6) <> "" Then
    If cboDBCombo1(1).Text = "" Or cboDBCombo1(2).Text = "" Then
      Call objError.SetError(cwCodeMsg, "Este Proceso no tiene Asignado un Responsable", vntA)
      vntA = objError.Raise
    End If
  End If
  intCancel = objWinInfo.WinExit
  
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Select Case strCtrl
    'Case "IdPersona1"                  'nuevo
    '  IdPersona1.SearchPersona         'nuevo
    Case "cboDBCombo1(1)"
      Call objSecurity.LaunchProcess("AD0206")
      'Call frmDepartamentosPersonal.Show(vbModal)
    Case "cboDBCombo1(2)"
      If cboDBCombo1(1).Text <> "" Then
        Call objPipe.PipeSet("Departamento", cboDBCombo1(1))
        Call objSecurity.LaunchProcess("AD0206")
        'Call frmDepartamentosPersonal.Show(vbModal)
        Call objPipe.PipeRemove("Departamento")
      Else
      End If
  End Select
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  If strFormName <> "Responsable Proceso" And dtcDateCombo1(3).Text = "" Then
'    objWinInfo.DataRefresh
  End If
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
  Dim vntA As Variant
  If strFormName = "Procesos" Then
    If intNewStatus = cwModeSingleEdit Then
      cmdCommand1(0).Enabled = True
    Else
      cmdCommand1(0).Enabled = False
    End If
  ElseIf strFormName = "Responsable Proceso" Then
    If intNewStatus = cwModeSingleAddRest Then
      If strFecha <> "" Then
        If CDate(strFecha) > dtcDateCombo1(3).Date & " " & Format(txtHora(2) & ":" & txtMinuto(2), "hh:mm") & ":00" Then
          Call objError.SetError(cwCodeMsg, "La fecha indicada es menor que la del responsable actual. C�mbiela para poder almacenar la informaci�n", vntA)
          vntA = objError.Raise
        End If
      End If
    End If
    cmdCommand1(0).Enabled = False
  End If

End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  Dim strHora As String
  
  If strFormName = "Procesos" Then
    Dim rs As rdoResultset
    Dim strSQL As String
    If objWinInfo.intWinStatus = cwModeSingleAddKey Then
      Set rs = objApp.rdoConnect.OpenResultset("SELECT AD07CODPROCESO_S.NEXTVAL FROM DUAL", rdOpenKeyset)
      Call objWinInfo.CtrlSet(txtText1(6), rs.rdoColumns(0).Value)
      rs.Close
      Set rs = Nothing
      'SendKeys "{TAB}", True
      'Call objWinInfo.CtrlSet(txtText1(12), IdPersona1.Text) 'nuevo C�D PERSONA
      Call objWinInfo.CtrlSet(txtText1(12), txtText1(0))  'nuevo C�D PERSONA
      'IVM se mete la historia del paciente
      'Call objWinInfo.CtrlSet(txtText1(0), IdPersona1.Historia)  'nuevo  HISTORIA
      Call objWinInfo.CtrlSet(txtText1(0), txtText1(2))  'nuevo  HISTORIA
      Call objWinInfo.CtrlSet(dtcDateCombo1(1), strFecha_Sistema)
      strHora = strHora_Sistema
      txtHora(0).Text = Left(Format(strHora, "hh:mm"), 2)
      txtMinuto(0).Text = Right(Format(strHora, "hh:mm"), 2)
    End If
  End If

  If strFormName = "Responsable Proceso" Then
    Call objWinInfo.CtrlSet(txtText1(9), txtText1(6).Text)
    If objWinInfo.intWinStatus = cwModeSingleAddKey Then
      Call objWinInfo.CtrlSet(dtcDateCombo1(3), strFecha_Sistema)
      strHora = strHora_Sistema
      txtHora(2).Text = Left(Format(strHora, "hh:mm"), 2)
      txtMinuto(2).Text = Right(Format(strHora, "hh:mm"), 2)
      UpDownHora(2).Value = txtHora(2).Text
      If txtMinuto(2).Text > 55 Then
        txtMinuto(2).Text = 55
      End If
      UpDownMinuto(2).Value = txtMinuto(2).Text
      txtHora(2).Enabled = True
      txtMinuto(2).Enabled = True
      UpDownHora(2).Enabled = True
      UpDownMinuto(2).Enabled = True
    End If
    objWinInfo.CtrlGetInfo(cboDBCombo1(1)).strSQL = "SELECT AD02CODDPTO,AD02DESDPTO FROM " & "AD0200 WHERE (AD02INDRESPONPROC = -1) AND ((AD02FECFIN IS NULL) " & _
                                          " OR (AD02FECFIN > (SELECT SYSDATE FROM DUAL))) ORDER BY AD02CODDPTO"
    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(1)))
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  Select Case strFormName
    Case "Procesos"
      If objWinInfo.intWinStatus <> cwModeSingleEmpty Then
        If grdDBGrid1(1).Rows > 0 Then
        'dtcDateCombo1(1).Date = objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI
        'Call objWinInfo.CtrlSet(dtcDateCombo1(1), Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "DD/MM/YY"))
        Call objWinInfo.CtrlSet(txtHora(0), Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "HH:MM"), 2))
        Call objWinInfo.CtrlSet(txtMinuto(0), Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "HH:MM"), 2))
      End If
      End If
      If dtcDateCombo1(2).Date <> "" Then
        Call objWinInfo.CtrlSet(dtcDateCombo1(2), Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAFIN, "DD/MM/YY"))
        Call objWinInfo.CtrlSet(txtHora(1), Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAFIN, "HH:MM"), 2))
        Call objWinInfo.CtrlSet(txtMinuto(1), Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAFIN, "HH:MM"), 2))
      End If
      
      If dtcDateCombo1(2) = "" Then
        objWinInfo.cllWinForms(3).intAllowance = cwAllowAdd
      Else
        objWinInfo.cllWinForms(3).intAllowance = cwAllowReadOnly
      End If
    Case "Responsable Proceso"
      If objWinInfo.intWinStatus <> cwModeSingleEmpty Then
        If dtcDateCombo1(3).Date <> "" Then
          Call objWinInfo.CtrlSet(txtHora(2), Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AD04FECINIRESPON, "HH:MM"), 2))
          Call objWinInfo.CtrlSet(txtMinuto(2), Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AD04FECINIRESPON, "HH:MM"), 2))
          txtHora(2).Enabled = False
          txtMinuto(2).Enabled = False
        End If
      End If
      If dtcDateCombo1(4).Date <> "" Then
        Call objWinInfo.CtrlSet(dtcDateCombo1(4), Format(objWinInfo.objWinActiveForm.rdoCursor!AD04FECFINRESPON, "DD/MM/YY"))
        Call objWinInfo.CtrlSet(txtHora(3), Left(Format(objWinInfo.objWinActiveForm.rdoCursor!AD04FECFINRESPON, "HH:MM"), 2))
        Call objWinInfo.CtrlSet(txtMinuto(3), Right(Format(objWinInfo.objWinActiveForm.rdoCursor!AD04FECFINRESPON, "HH:MM"), 2))
      End If
  End Select
End Sub

Private Sub objWinInfo_cwPreDefault(ByVal strFormName As String)
  Dim rs As rdoResultset
  Select Case strFormName
    Case "Responsable Proceso"
      If txtText1(6).Text <> "" Then
        Set rs = objApp.rdoConnect.OpenResultset("SELECT MAX(AD04FECINIRESPON) FROM AD0400 " & _
                                                 "WHERE AD07CODPROCESO=" & txtText1(6), rdOpenKeyset)
        If Not IsNull(rs.rdoColumns(0)) Then strFecha = rs.rdoColumns(0)
            
        rs.Close
        Set rs = Nothing
      End If
  End Select
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim vntA As Variant
  Select Case strFormName
    Case "Responsable Proceso"
      If strFecha <> "" Then
        If CDate(strFecha) > dtcDateCombo1(3).Date & " " & txtHora(2).Text & "." & txtMinuto(2).Text & ".00" Then
          Call objError.SetError(cwCodeMsg, "La fecha indicada es menor que la del responsable actual.", vntA)
          vntA = objError.Raise
          blnCancel = True
          Exit Sub
        End If
      End If
    If dtcDateCombo1(2).Date <> "" Then
      If CDate(Format(dtcDateCombo1(1).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(2).Date, "DD/MM/YYYY")) Then
        Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
        vntA = objError.Raise
        blnCancel = True
      End If
    End If
    
    'If cboDBCombo1(1).Text = vntTipo Then
    '  Call objError.SetError(cwCodeMsg, "Tipo de Asistencia igual a Tipo de Asistencia actual. Deber� elegir otro distinto.", vntA)
    '  vntA = objError.Raise
    '  blnCancel = True
    '  Exit Sub
    'End If
    'vntTipo = ""
    
    Case "Procesos"
      If dtcDateCombo1(4).Date <> "" Then
        If CDate(Format(dtcDateCombo1(3).Date, "DD/MM/YYYY")) > CDate(Format(dtcDateCombo1(4).Date, "DD/MM/YYYY")) Then
          Call objError.SetError(cwCodeMsg, "Fecha Fin no puede ser inferior a Fecha Inicio.", vntA)
          vntA = objError.Raise
          blnCancel = True
        End If
      End If
  End Select
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim strSQL As String
  
  If strFormName = "Responsable Proceso" Then
    If dtcDateCombo1(3).Text <> "" Then
      objWinInfo.objWinActiveForm.rdoCursor.rdoColumns(0) = _
      objWinInfo.objWinActiveForm.rdoCursor.rdoColumns(0) & " " & txtHora(2).Text & ":" & txtMinuto(2).Text & ":00"
    End If
  End If
  If strFormName = "Procesos" Then
    objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("AD07FECHORAINICI") = objWinInfo.objWinActiveForm.rdoCursor.rdoColumns("AD07FECHORAINICI") & " " & txtHora(0).Text & ":" & txtMinuto(0).Text & ":00"
  End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim strSQL As String
  'Dim strRespProc As String
  'Dim rstRespProc As rdoResultset
  
  If strFormName = "Responsable Proceso" Then
   ' strRespProc = "SELECT TO_CHAR(AD04FECINIRESPON ,'DD/MM/YYYY, HH24:MI:SS')" & _
   '               "  FROM AD0400 " & _
   '               " WHERE AD04FECFINRESPON IS NULL " & _
   '               "   AND AD07CODPROCESO = " & txtText1(9) & _
   '               " ORDER BY AD04FECINIRESPON DESC"
   '
   ' Set rstRespProc = objApp.rdoConnect.OpenResultset(strRespProc)
    
      strSQL = "UPDATE AD0400 SET AD04FECFINRESPON = "
      strSQL = strSQL & "TO_DATE('" & dtcDateCombo1(3).Date & " " & txtHora(2).Text & ":" & txtMinuto(2).Text & ":00" & "','DD/MM/YYYY HH24:MI:SS')"
      strSQL = strSQL & " WHERE AD04FECFINRESPON IS NULL "
      strSQL = strSQL & " AND AD07CODPROCESO = " & txtText1(9)
      strSQL = strSQL & " AND AD04FECINIRESPON = TO_DATE('" & Format(strFecha, "DD/MM/YYYY HH:MM:SS") & "','DD/MM/YYYY HH24:MI:SS')"
      objApp.rdoConnect.Execute strSQL, 64
    
    'rstRespProc.Close
    'Set rstRespProc = Nothing
  End If
  
  
End Sub

Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
  If strFormName = "Responsable Proceso" Then
    If objWinInfo.intWinStatus <> cwModeSingleEmpty Then
      If Not objWinInfo.objWinActiveForm.rdoCursor.EOF Then
        Call objWinInfo.CtrlSet(dtcDateCombo1(3), Format(objWinInfo.objWinActiveForm.rdoCursor.rdoColumns(0), "DD/MM/YY"))
        objWinInfo.CtrlGetInfo(cboDBCombo1(2)).strSQL = _
        "SELECT DISTINCT AD0300.SG02COD,SG02NOM,SG02APE1,SG02APE2,AD03FECFIN " & _
        "FROM AD0300,SG0200 " & _
        "WHERE AD0300.AD02CODDPTO = " & objWinInfo.objWinActiveForm.rdoCursor.rdoColumns(1) & " " & _
        "AND AD0300.SG02COD=SG0200.SG02COD " & _
        "AND SG0200.AD30CODCATEGORIA = 1 " & _
        "ORDER BY SG02APE1,SG02APE2,SG02NOM"
        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(2)))
      End If
    End If
  End If
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  If btnButton.Index = 16 Then
    IdPersona1.Text = 0
    Call IdPersona1.Buscar
    If Val(IdPersona1.Text) <> 0 Then
        Call objWinInfo.FormChangeActive(tabTab1(0), False, True)
        Call objApp.objActiveWin.DataOpen
        txtText1_GotFocus (1)
        txtText1(1).Text = IdPersona1.Text
        txtText1_LostFocus (1)
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  If btnButton.Index = 4 Then
    objWinInfo.DataRefresh
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   If intIndex = 1 Then
    If objWinInfo.objWinActiveForm.strName = "Procesos" And txtText1(0).Text <> "" Then
      Call objWinInfo.CtrlSet(dtcDateCombo1(1), Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "DD/MM/YYYY"))
      dtcDateCombo1(1).Text = Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "DD/MM/YYYY")
      If tabTab1(intIndex).Tab = 0 Then
        If dtcDateCombo1(1).Enabled Then
          dtcDateCombo1(1).SetFocus
          txtText1(7).SetFocus
        End If
      End If
    End If
   End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  If objWinInfo.objWinActiveForm.strName = "Procesos" And txtText1(0).Text <> "" Then
    Call objWinInfo.CtrlSet(dtcDateCombo1(1), Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "DD/MM/YY"))
  End If
End Sub
Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
                                       
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  
  If objWinInfo.objWinActiveForm.strName = "Procesos" And txtText1(0).Text <> "" Then
    If objWinInfo.intWinStatus <> cwModeSingleAddRest And objWinInfo.intWinStatus <> cwModeSingleAddKey Then
      Call objWinInfo.CtrlSet(dtcDateCombo1(1), Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "DD/MM/YYYY"))
      dtcDateCombo1(1).Text = Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "DD/MM/YYYY")
      If tabTab1(intIndex).Tab = 0 Then
          If dtcDateCombo1(1).Enabled Then
            dtcDateCombo1(1).SetFocus
            txtText1(7).SetFocus
          End If
      End If
    End If
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_Click(intIndex As Integer)
  Select Case intIndex
    Case 1
      If cboDBCombo1(1).Text <> "" Then
        objWinInfo.CtrlGetInfo(cboDBCombo1(2)).strSQL = _
        "SELECT DISTINCT AD0300.SG02COD,SG02NOM,SG02APE1,SG02APE2,AD03FECFIN " & _
        "FROM AD0300,SG0200 " & _
        "WHERE AD0300.AD02CODDPTO = " & cboDBCombo1(1).Text & " " & _
        "AND AD0300.SG02COD=SG0200.SG02COD " & _
        "AND AD03FECFIN IS NULL AND SG0200.AD30CODCATEGORIA = 1 " & _
        "ORDER BY SG02APE1,SG02APE2,SG02NOM"
        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboDBCombo1(2)))
      Else
        Call objWinInfo.CtrlSet(cboDBCombo1(2), "")
      End If
    Case 2
      'Call objWinInfo.CtrlSet(txtText1(8), cboDBCombo1(2).Columns(2).Value)
      'Call objWinInfo.CtrlSet(txtText1(10), cboDBCombo1(2).Columns(3).Value)
      'Call objWinInfo.CtrlSet(txtText1(11), cboDBCombo1(2).Columns(4).Value)
  End Select
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboDBCombo1_RowLoaded(intIndex As Integer, ByVal Bookmark As Variant)
  Select Case intIndex
    Case 1
      If cboDBCombo1(intIndex).Columns(2).Value = "" Then
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
      Else
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
      End If
    Case 2
      If cboDBCombo1(intIndex).Columns(4).Value = "" Then
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(2).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(3).CellStyleSet("Activo", cboDBCombo1(intIndex).Row)
      Else
        Call cboDBCombo1(intIndex).Columns(0).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(1).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(2).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
        Call cboDBCombo1(intIndex).Columns(3).CellStyleSet("Inactivo", cboDBCombo1(intIndex).Row)
      End If
  End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  If intIndex = 1 And objWinInfo.objWinActiveForm.strName <> "Responsable Proceso" Then
   If txtText1(1).Text <> "" Then
    'Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    'objWinInfo.objWinActiveForm.strWhere = "CI21CODPERSONA=" & txtText1(1).Text
    'objWinInfo.DataRefresh
    'objWinInfo.DataMoveFirst
    'dtcDateCombo1(1).Text = Format(objWinInfo.objWinActiveForm.rdoCursor!AD07FECHORAINICI, "DD/MM/YY")
    'objWinInfo.objWinActiveForm.blnChanged = False
   End If
  End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command Buttons
' -----------------------------------------------


Private Sub cmdCommand1_Click(intIndex As Integer)
  Select Case intIndex
    Case 0
      Call objSecurity.LaunchProcess("AD0123")
      'Call frmAsistencias.Show(vbModal)
      objWinInfo.DataRefresh
  End Select
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

'Private Sub IdPersona1_Change()
'  Call objWinInfo.CtrlDataChange
'End Sub

'Private Sub IdPersona1_GotFocus()
'  Call objWinInfo.CtrlGotFocus
'End Sub

'Private Sub IdPersona1_LostFocus()
'  Call objWinInfo.CtrlLostFocus
'End Sub

Private Sub UpDownHora_DownClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownHora_UpClick(intIndex As Integer)
  txtHora(intIndex).SetFocus
  txtHora(intIndex) = UpDownHora(intIndex).Value
End Sub

Private Sub UpDownMinuto_DownClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub

Private Sub UpDownMinuto_UpClick(intIndex As Integer)
  txtMinuto(intIndex).SetFocus
  txtMinuto(intIndex) = UpDownMinuto(intIndex).Value
End Sub

Private Sub Frame2_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  'Se activa el frame que contiene los datos del Proceso/Asistencia
  Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
End Sub
