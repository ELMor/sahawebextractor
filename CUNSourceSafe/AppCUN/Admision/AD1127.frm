VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmpruebaspendientesdpto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pruebas Pendientes por Dpto"
   ClientHeight    =   8430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8430
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdOverbooking 
      Caption         =   "Overbooking"
      Height          =   375
      Left            =   1920
      TabIndex        =   25
      Top             =   7920
      Width           =   1575
   End
   Begin VB.CommandButton cmdObservaciones 
      Caption         =   "&Obser./Cuestinario"
      Height          =   375
      Left            =   7320
      TabIndex        =   24
      Top             =   7920
      Width           =   1575
   End
   Begin VB.CommandButton cmdVG 
      Caption         =   "&Visi�n Global"
      Height          =   375
      Left            =   5520
      TabIndex        =   22
      Top             =   7920
      Width           =   1575
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular"
      Height          =   375
      Left            =   3720
      TabIndex        =   21
      Top             =   7920
      Width           =   1575
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   7920
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10320
      TabIndex        =   17
      Top             =   7920
      Width           =   1575
   End
   Begin VB.Frame Frame4 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1395
      Left            =   4440
      TabIndex        =   5
      Top             =   0
      Width           =   6795
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   660
         TabIndex        =   12
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   660
         TabIndex        =   11
         Top             =   600
         Width           =   1995
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   5760
         TabIndex        =   10
         Top             =   120
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   3540
         TabIndex        =   9
         Top             =   240
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   3540
         TabIndex        =   8
         Top             =   600
         Width           =   1995
      End
      Begin VB.CommandButton cmdB 
         Caption         =   "Si&guiente"
         Height          =   375
         Index           =   0
         Left            =   5760
         TabIndex        =   7
         Top             =   540
         Width           =   975
      End
      Begin VB.CommandButton cmdB 
         Caption         =   "&Anterior"
         Height          =   375
         Index           =   1
         Left            =   5760
         TabIndex        =   6
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   60
         TabIndex        =   16
         Top             =   240
         Width           =   525
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   60
         TabIndex        =   15
         Top             =   600
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   2760
         TabIndex        =   14
         Top             =   240
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   2760
         TabIndex        =   13
         Top             =   600
         Width           =   750
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Pruebas Pendientes"
      ForeColor       =   &H00C00000&
      Height          =   6315
      Left            =   0
      TabIndex        =   3
      Top             =   1440
      Width           =   11865
      Begin SSDataWidgets_B.SSDBGrid grdConsultas 
         Height          =   5955
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   11625
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20505
         _ExtentY        =   10504
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Departamento"
      ForeColor       =   &H00C00000&
      Height          =   1395
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4395
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   3360
         TabIndex        =   1
         Top             =   600
         Width           =   915
      End
      Begin SSDataWidgets_B.SSDBCombo cboDptosRea 
         Height          =   315
         Left            =   960
         TabIndex        =   19
         Top             =   720
         Width           =   2235
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3942
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboDptosSol 
         Height          =   315
         Left            =   960
         TabIndex        =   23
         Top             =   240
         Width           =   2235
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3942
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Realizador"
         Height          =   195
         Index           =   1
         Left            =   165
         TabIndex        =   18
         Top             =   840
         Width           =   750
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Solicitante"
         Height          =   195
         Index           =   0
         Left            =   180
         TabIndex        =   2
         Top             =   360
         Width           =   735
      End
   End
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   11040
      Top             =   2040
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   "|"
   End
End
Attribute VB_Name = "frmpruebaspendientesdpto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strDptoSol As String
Dim strdptoRea As String
Dim strRecursosSel As String
Private Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long 'para cargar los TreeViews a gran velocidad
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection
Dim blnpaso As Boolean
Private Sub cmdCitar_Click()
Dim strNAPlan$, i%
'se mira si hay alguna actuaci�n seleccionada
If grdConsultas.SelBookmarks.Count = 0 Then
     MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
    Exit Sub
End If
'se pasa a citar las actuaciones
For i = 0 To grdConsultas.SelBookmarks.Count - 1
    strNAPlan = strNAPlan & grdConsultas.Columns("Act. Planificada").CellValue(grdConsultas.SelBookmarks(i)) & ","
Next i
Call pCitarActuaciones(strNAPlan)
End Sub
Private Sub cmdAnular_Click()
Dim i As Integer
'se mira si hay alguna actuaci�n seleccionada
If grdConsultas.SelBookmarks.Count = 0 Then
     MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
    Exit Sub
End If

For i = 1 To grdConsultas.SelBookmarks.Count
    Call pAnular(grdConsultas.Columns("Act. Planificada").CellValue(grdConsultas.SelBookmarks(i - 1)), "", "")
Next i
MsgBox "La Actuaci�n ha sido anulada", vbInformation, Me.Caption
Call pActualizar(-1)
End Sub

Private Sub pFormatearGrids()
    Dim i%
    With grdConsultas
        .Columns(0).Caption = "Historia"
        .Columns(0).Width = 700
        .Columns(1).Caption = "Paciente"
        .Columns(1).Width = 2800
        .Columns(2).Caption = "Prueba"
        .Columns(2).Width = 1700
        .Columns(3).Caption = "Fec. Plan."
        .Columns(3).Width = 1000
        .Columns(4).Caption = "Recurso Pedido"
        .Columns(4).Width = 1370
        .Columns(5).Caption = "Estado"
        .Columns(5).Width = 1200
        .Columns(5).Visible = False
        .Columns(6).Caption = "Dpto"
        .Columns(6).Width = 1500
        .Columns(7).Caption = "Doctor"
        .Columns(7).Width = 1700
        .Columns(8).Caption = "Cod. Persona"
        .Columns(8).Width = 500
        .Columns(8).Visible = False
        .Columns(9).Caption = "Act. Planificada"
        .Columns(9).Width = 500
        .Columns(9).Visible = False
        .Columns(10).Caption = "Cita"
        .Columns(10).Width = 500
        .Columns(10).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With

End Sub
Private Sub pCargarDptos()
    Dim SQL1$, rs1 As rdoResultset, qry6 As rdoQuery
    
    'se cargan los Dptos.
        SQL1 = "SELECT AD0200.AD02CODDPTO, AD0200.AD02DESDPTO"
        SQL1 = SQL1 & " FROM AD0200,AD0300"
        SQL1 = SQL1 & " WHERE AD0200.AD02CODDPTO = AD0300.AD02CODDPTO "
        SQL1 = SQL1 & " AND AD0300.SG02COD =?"
        SQL1 = SQL1 & " AND AD0200.AD02FECFIN IS NULL"
        SQL1 = SQL1 & " ORDER BY AD0200.AD02DESDPTO"
    Set qry6 = objApp.rdoConnect.CreateQuery("", SQL1)
    qry6(0) = objSecurity.strUser
    Set rs1 = qry6.OpenResultset()
    If Not rs1.EOF Then
      cboDptosSol.Text = rs1(1)
      Do While Not rs1.EOF
        cboDptosSol.AddItem rs1(0) & Chr$(9) & rs1(1)
        rs1.MoveNext
      Loop
      rs1.Close
      qry6.Close
    End If
    
    
        SQL1 = "SELECT AD02CODDPTO, AD02DESDPTO"
        SQL1 = SQL1 & " FROM AD0200"
        SQL1 = SQL1 & " WHERE AD02FECFIN IS NULL"
        SQL1 = SQL1 & " AND AD0200.AD32CODTIPODPTO IN (1,2)"
        SQL1 = SQL1 & " ORDER BY AD02DESDPTO"
    Set rs1 = objApp.rdoConnect.OpenResultset(SQL1)
    If Not rs1.EOF Then
      cboDptosRea.Text = rs1(1)
'        cboDptosRea.AddItem "" & Chr$(9) & "TODOS"
'        cboDptosRea.Text = "TODOS"
        Do While Not rs1.EOF
            cboDptosRea.AddItem rs1(0) & Chr$(9) & rs1(1)
            rs1.MoveNext
        Loop
        cboDptosRea.AddItem "" & Chr$(9) & "TODOS(Inf. Interdep.)"
        rs1.Close
    End If
End Sub
'Private Sub cboDptosRea_Click()
'    strdptoRea = cboDptosRea.Columns(0).Value
'End Sub

Private Sub cmdConsultar_Click()
Call pActualizar(-1)
End Sub
Private Sub pActualizar(intCol%)
Dim SQL As String
Dim qry As rdoQuery
Dim i As Integer
Dim se As rdoResultset
Dim strApel1 As String
Dim strApel2 As String
Static sqlOrder$

Screen.MousePointer = vbHourglass
If intCol = -1 Then
      If sqlOrder = "" Then sqlOrder = " ORDER BY AD02DESDPTO,PACIENTE" 'Opci�n por defecto: DPTO Y PACIENTE
Else
      Select Case UCase(grdConsultas.Columns(intCol).Caption)
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE"
        Case UCase("Historia"): sqlOrder = " ORDER BY CI22NUMHISTORIA, PACIENTE"
        Case UCase("Prueba"): sqlOrder = " ORDER BY PR01DESCORTA, PACIENTE"
        Case Else: Exit Sub
      End Select
End If
grdConsultas.RemoveAll
Do While cllBuscar.Count > 0: cllBuscar.Remove 1: Loop
 grdConsultas.ScrollBars = ssScrollBarsNone
'Call pConsultar
strDptoSol = cboDptosSol.Columns(0).Value
strdptoRea = cboDptosRea.Columns(0).Value
If strdptoRea = "" Then
    SQL = "SELECT /*+ ORDERED FIRST_ROWS INDEX(PR0400 PR0406) INDEX(PR1400 PR1401) */ "
    SQL = SQL & " CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE, "
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||NVL(CI2200.CI22SEGAPEL,' ')||', '||CI2200.CI22NOMBRE PACIENTE,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " PR0100.PR01DESCORTA,"
    SQL = SQL & " PR0400.PR04FECPLANIFIC,PR3700.PR37DESESTADO,"
    SQL = SQL & " AD0200.AD02DESDPTO,"
    SQL = SQL & " PR0400.CI21CODPERSONA,PR0400.PR04NUMACTPLAN,"
    SQL = SQL & " DECODE(PR03INDCITABLE,-1,'SI',0,'NO','NO')"
    SQL = SQL & " PR03INDCITABLE,"
    SQL = SQL & " SG0200.SG02TXTFIRMA, AG1100.AG11DESRECURSO"
    SQL = SQL & " From PR0100, PR0400, PR0300, PR0900, PR1400, AG1100,"
    SQL = SQL & " CI2200, AD0200, PR3700, SG0200"
    SQL = SQL & " Where"
    SQL = SQL & " PR0400.PR37CODESTADO=1"
    SQL = SQL & " AND PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION"
    SQL = SQL & " AND PR0900.AD02CODDPTO=AD0200.AD02CODDPTO"
    SQL = SQL & " AND PR0900.SG02COD = SG0200.SG02COD"
    SQL = SQL & " AND PR0400.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR1400.PR14INDRECPREFE(+) = -1"
    SQL = SQL & " AND PR1400.AG11CODRECURSO = AG1100.AG11CODRECURSO(+)"
    SQL = SQL & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    SQL = SQL & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO"
    SQL = SQL & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    SQL = SQL & " AND PR0100.PR12CODACTIVIDAD=215"
Else
    i = 1
    SQL = "SELECT /*+ ORDERED INDEX(PR0900 PR0901) */ "
    SQL = SQL & " CI2200.CI22PRIAPEL, CI2200.CI22SEGAPEL, CI2200.CI22NOMBRE, "
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||NVL(CI2200.CI22SEGAPEL,' ')||', '||CI2200.CI22NOMBRE PACIENTE,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " PR0100.PR01DESCORTA,"
    SQL = SQL & " PR0400.PR04FECPLANIFIC,PR3700.PR37DESESTADO,"
    SQL = SQL & " AD0200.AD02DESDPTO,"
    SQL = SQL & " PR0400.CI21CODPERSONA,PR0400.PR04NUMACTPLAN,"
    SQL = SQL & " DECODE(PR03INDCITABLE,-1,'SI',0,'NO','NO')"
    SQL = SQL & " PR03INDCITABLE,"
    SQL = SQL & " SG0200.SG02TXTFIRMA, AG1100.AG11DESRECURSO"
    SQL = SQL & " From PR0400, PR0300, PR0900, PR1400, AG1100,"
    SQL = SQL & " CI2200, PR0100, AD0200, PR3700, SG0200"
    SQL = SQL & " Where"
    SQL = SQL & " PR0400.PR37CODESTADO=1"
    SQL = SQL & " AND PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION"
    SQL = SQL & " AND PR0900.AD02CODDPTO=AD0200.AD02CODDPTO"
    SQL = SQL & " AND PR0900.SG02COD = SG0200.SG02COD"
    SQL = SQL & " AND PR0400.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR1400.PR14INDRECPREFE(+) = -1"
    SQL = SQL & " AND PR1400.AG11CODRECURSO = AG1100.AG11CODRECURSO(+)"
    SQL = SQL & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    SQL = SQL & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO"
    SQL = SQL & " AND PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    SQL = SQL & " AND PR0400.AD02CODDPTO= ? "
End If

If strDptoSol <> "0" Then
i = i + 1
SQL = SQL & " AND PR0900.AD02CODDPTO = ?"
End If
SQL = SQL & sqlOrder

Set qry = objApp.rdoConnect.CreateQuery("", SQL)
Select Case i
    Case 1
        If strdptoRea <> "" Then
            qry(0) = strdptoRea
        Else
            qry(0) = strDptoSol
        End If
    Case 2
        qry(0) = strdptoRea
        qry(1) = strDptoSol
End Select
Set se = qry.OpenResultset()
Do While Not se.EOF
  grdConsultas.AddItem se!CI22NUMHISTORIA & Chr$(9) _
                            & se!Paciente & Chr$(9) _
                            & se!PR01DESCORTA & Chr$(9) _
                            & se!PR04FECPLANIFIC & Chr$(9) _
                            & se!AG11DESRECURSO & Chr$(9) _
                            & se!PR37DESESTADO & Chr$(9) _
                            & se!AD02DESDPTO & Chr$(9) _
                            & se!SG02TXTFIRMA & Chr$(9) _
                            & se!CI21CODPERSONA & Chr$(9) _
                            & se!PR04NUMACTPLAN & Chr$(9) _
                            & se!PR03INDCITABLE
                            
 
        If IsNull(se!CI22PRIAPEL) Then strApel1 = "" Else strApel1 = se!CI22PRIAPEL
        If IsNull(se!CI22SEGAPEL) Then strApel2 = "" Else strApel2 = se!CI22SEGAPEL
        cllBuscar.Add se!CI22NUMHISTORIA & "|" & se!CI22NOMBRE & "|" & strApel1 & "|" & strApel2 & "|" & se!CI21CODPERSONA & "|" & se!PR04NUMACTPLAN
        se.MoveNext
Loop
se.Close
qry.Close

grdConsultas.ScrollBars = ssScrollBarsAutomatic
 Screen.MousePointer = vbDefault
If grdConsultas.Rows = 0 Then
  MsgBox "No hay ninguna actuacion planificada", vbOKOnly + vbInformation
End If

End Sub
Private Sub cmdObservaciones_Click()
Dim vntdata As Variant
 'se mira si hay alguna actuaci�n seleccionada
If grdConsultas.SelBookmarks.Count = 0 Then
     MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
    Exit Sub
End If

 If grdConsultas.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple"
    Exit Sub
  End If
If grdConsultas.Columns("Act. Planificada").Value = "" Then Exit Sub
' Call objPipe.PipeSet("PR_NActPlan", grdConsultas.Columns("Act. Planificada").Value)
 vntdata = grdConsultas.Columns("Act. Planificada").Value
 Call objSecurity.LaunchProcess("PR0510", vntdata)
End Sub


Private Sub CmdOverbooking_Click()
   Dim strNAPlan$, i%
   'se mira si hay alguna actuaci�n seleccionada
   If grdConsultas.SelBookmarks.Count = 0 Then
         MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
        Exit Sub
    End If
    'se pasa a citar o recitar las actuaciones
    For i = 0 To grdConsultas.SelBookmarks.Count - 1
        strNAPlan = strNAPlan & grdConsultas.Columns("Act. Planificada").CellValue(grdConsultas.SelBookmarks(i)) & ","
    Next i
    Call pCitarOverbooking(strNAPlan)
    Call pActualizar(-1)
End Sub
Private Sub cmdSalir_Click()
Unload Me
End Sub
Private Sub cmdVG_Click()
'se mira si hay alguna actuaci�n seleccionada
If grdConsultas.SelBookmarks.Count = 0 Then
     MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
    Exit Sub
End If
ReDim vntdata(1 To 2) As Variant
vntdata(1) = grdConsultas.Columns("Cod. Persona").Text
vntdata(2) = 2
Call objSecurity.LaunchProcess("HC02", vntdata())
Me.MousePointer = vbDefault
cmdVG.Enabled = True
End Sub
Private Sub Form_Load()
Dim sql4$
Dim qry3 As rdoQuery
Dim rw As rdoResultset
sql4 = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD=?"
Set qry3 = objApp.rdoConnect.CreateQuery("", sql4)
qry3(0) = objSecurity.strUser
Set rw = qry3.OpenResultset()
Do While Not rw.EOF
  If rw(0) = 4 Then
    cmdCitar.Visible = True
    cmdAnular.Visible = True
    Exit Do
  Else
    cmdCitar.Visible = False
    cmdAnular.Visible = False
  End If
  rw.MoveNext
Loop
rw.Close
qry3.Close


Call pFormatearGrids

Call pCargarDptos

End Sub
Private Sub grdconsultas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
 DispPromptMsg = False
End Sub

Private Sub grdconsultas_Click()
    If grdConsultas.SelBookmarks.Count > 1 Then
        If grdConsultas.Columns("Historia").CellText(grdConsultas.SelBookmarks(0)) <> grdConsultas.Columns("Historia").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, Me.Caption
            grdConsultas.SelBookmarks.RemoveAll 'grdConsultas.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub
Private Sub grdconsultas_HeadClick(ByVal ColIndex As Integer)
    If grdConsultas.Rows > 0 Then Call pActualizar(ColIndex)
End Sub
Private Function fBlnBuscar() As Boolean
    Dim i%
    For i = 0 To txtBuscar.Count - 1
        If txtBuscar(i).Text <> "" Then fBlnBuscar = True: Exit Function
    Next i
End Function
Private Sub cmdBuscar_Click()
    Dim intN%, i%, blnE As Boolean
    Dim msg$, n%
    
    If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If

label:
        For i = 1 To cllBuscar.Count
            blnE = False
            awk1 = cllBuscar(i)
            If UCase(Left$(awk1.F(2), Len(txtBuscar(1).Text))) = UCase(txtBuscar(1).Text) _
            And UCase(Left$(awk1.F(3), Len(txtBuscar(2).Text))) = UCase(txtBuscar(2).Text) _
            And UCase(Left$(awk1.F(4), Len(txtBuscar(3).Text))) = UCase(txtBuscar(3).Text) Then
                If txtBuscar(0).Text = "" Then
                    blnE = True
                Else
                    If awk1.F(1) = txtBuscar(0).Text Then blnE = True
                End If
            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdConsultas.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hWnd
                    grdConsultas.MoveFirst
                    grdConsultas.MoveRecords i - 1
                    LockWindowUpdate 0&
                    grdConsultas.SelBookmarks.Add (grdConsultas.RowBookmark(grdConsultas.Row))
                    cmdB(0).SetFocus
                    Exit Sub
                End If
            End If
        Next i
        
        If intBuscar = 1 Then
            If n > 0 Then
                msg = "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda."
                MsgBox msg, vbInformation, Me.Caption
                Exit Sub
            Else
                msg = "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda."
                msg = msg & Chr$(13) & Chr$(13)
                msg = msg & "Se va a proceder a actualizar los datos de la pantalla y volver a intentar la b�squeda."
                If MsgBox(msg, vbInformation + vbYesNo, Me.Caption) = vbYes Then
                    n = 1
                    Call pActualizar(-1)
                    GoTo label
                End If
            End If
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
    Else
        blnBuscarOtro = False
    End If
End Sub
Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub
