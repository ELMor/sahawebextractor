VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form frmPendVolante 
   Caption         =   "ADMISION. Pendientes de Volante"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "frmPendVolante"
   ScaleHeight     =   8595
   ScaleWidth      =   11880
   WindowState     =   2  'Maximized
   Begin Threed.SSFrame SSFrame1 
      Height          =   6795
      Left            =   0
      TabIndex        =   8
      Top             =   1740
      Width           =   11835
      _Version        =   65536
      _ExtentX        =   20876
      _ExtentY        =   11986
      _StockProps     =   14
      Caption         =   "Pendientes de Volante"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CommandButton cmdDocumenta 
         Caption         =   "Documentacion"
         Height          =   495
         Left            =   3960
         TabIndex        =   17
         Top             =   6120
         Width           =   1695
      End
      Begin SSDataWidgets_B.SSDBGrid grdpendientes 
         Height          =   5775
         Left            =   60
         TabIndex        =   9
         Top             =   240
         Width           =   11715
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         RowHeight       =   423
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20664
         _ExtentY        =   10186
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dpto. / Fecha Inicio"
      ForeColor       =   &H00800000&
      Height          =   1695
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   11775
      Begin ComctlLib.ListView lvwentidad2 
         Height          =   735
         Left            =   6180
         TabIndex        =   16
         Top             =   660
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   1296
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.CommandButton cmdimprimir 
         Caption         =   "&Imprimir"
         Height          =   435
         Left            =   10320
         TabIndex        =   15
         Top             =   660
         Width           =   1155
      End
      Begin VB.CommandButton cmdsalir 
         Caption         =   "&Salir"
         Height          =   435
         Left            =   10320
         TabIndex        =   14
         Top             =   120
         Width           =   1155
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   435
         Left            =   10320
         TabIndex        =   1
         Top             =   1200
         Width           =   1155
      End
      Begin ComctlLib.ListView lvwDpto 
         Height          =   735
         Left            =   300
         TabIndex        =   2
         Top             =   660
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   1296
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecInicio 
         Height          =   330
         Left            =   8400
         TabIndex        =   3
         Tag             =   "Fecha Desde"
         ToolTipText     =   "Fecha Inicio Desde"
         Top             =   480
         Width           =   1710
         _Version        =   65537
         _ExtentX        =   3016
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         Mask            =   2
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcFecFin 
         Height          =   330
         Left            =   8400
         TabIndex        =   4
         Tag             =   "Fecha Hasta"
         ToolTipText     =   "Fecha Inicio Hasta"
         Top             =   1200
         Width           =   1710
         _Version        =   65537
         _ExtentX        =   3016
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1900/1/1"
         MaxDate         =   "2100/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         Mask            =   2
         StartofWeek     =   2
      End
      Begin ComctlLib.ListView lvwtipecon 
         Height          =   735
         Left            =   3960
         TabIndex        =   10
         Top             =   660
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   1296
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin ComctlLib.ListView lvwentidad 
         Height          =   735
         Left            =   6180
         TabIndex        =   12
         Top             =   660
         Width           =   2115
         _ExtentX        =   3731
         _ExtentY        =   1296
         View            =   3
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tipo Econ�mico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   3960
         TabIndex        =   13
         Top             =   420
         Width           =   1380
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Entidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   6180
         TabIndex        =   11
         Top             =   420
         Width           =   660
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   375
         TabIndex        =   7
         Top             =   420
         Width           =   1200
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   8400
         TabIndex        =   6
         Top             =   180
         Width           =   1140
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Hasta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   8400
         TabIndex        =   5
         Top             =   960
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmPendVolante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim strWhere As String
Private strentidad As String
Private strtipecon As String
Dim tipsel As Integer
Private Sub ptipecon()
Dim tipsel As Integer
    If lvwtipecon.ListItems(1).Selected Then
         lvwentidad.Enabled = False
         Label1(1).Enabled = False
         lvwentidad2.Visible = True
          For i = 2 To lvwtipecon.ListItems.Count
           
              lvwtipecon.ListItems(i).Selected = False
              strtipecon = strtipecon & "'" & lvwtipecon.ListItems(i).Tag & "',"
            
                
          Next i
          strtipecon = Left$(strtipecon, Len(strtipecon) - 1)
        Else
            tipsel = 0
            For i = 2 To lvwtipecon.ListItems.Count
                If lvwtipecon.ListItems(i).Selected Then
                    strtipecon = strtipecon & "'" & lvwtipecon.ListItems(i).Tag & "',"
                    tipsel = tipsel + 1
                End If
                Next i
                 If tipsel = 0 Then
                    lvwtipecon.ListItems(1).Selected = True
                    For i = 2 To lvwtipecon.ListItems.Count
                    'lvwtipecon.ListItems(i).Selected = False
                    strtipecon = strtipecon & "'" & lvwtipecon.ListItems(i).Tag & "',"
                    Next i
'                    lvwentidad.Enabled = False
'                    Label1(1).Enabled = False
'                    lvwentidad2.Visible = True
                End If
                strtipecon = Left$(strtipecon, Len(strtipecon) - 1)
            If tipsel > 1 Or tipsel = 0 Then
                lvwentidad.Enabled = False
                Label1(1).Enabled = False
                lvwentidad2.Visible = True
                'lvwentidad2.Enabled = False
            Else
                lvwentidad.Enabled = True
                Label1(1).Enabled = True
                lvwentidad2.Visible = False
                'lvwentidad2.Enabled = False
            End If
           
            Call pcargarentidad(strtipecon, tipsel)
        End If
End Sub

Private Sub pentidad()
Dim entidad As Integer
    entidad = 0
    For i = 1 To lvwentidad.ListItems.Count
    If lvwentidad.ListItems(i).Selected Then
        entidad = entidad + 1
    
    End If
    Next i
    If lvwentidad.ListItems(1).Selected Or tipsel > 1 Then
          For i = 2 To lvwentidad.ListItems.Count
            If tipsel <= 1 Then
              lvwentidad.ListItems(i).Selected = False
            End If
              strentidad = strentidad & "'" & lvwentidad.ListItems(i).Tag & "',"
          Next i
        Else
            If entidad = 0 Then
                lvwentidad.ListItems(1).Selected = True
                For i = 2 To lvwentidad.ListItems.Count
                    strentidad = strentidad & "'" & lvwentidad.ListItems(i).Tag & "',"
                Next i
            Else
            For i = 2 To lvwentidad.ListItems.Count
                If lvwentidad.ListItems(i).Selected Then
                    strentidad = strentidad & "'" & lvwentidad.ListItems(i).Tag & "',"
                End If
                Next i
            End If
      End If
        If strentidad <> "" Then
                    strentidad = Left$(strentidad, Len(strentidad) - 1)
        End If
End Sub
Private Sub pcargargrid(strdptoSel As String, strfecini As String, strfecfin As String) ', strtipecon As String, strentidad As String)
Dim SQL As String
Dim rs As rdoResultset
Dim strOrder As String
SQL = ""
strOrder = ""
SQL = "SELECT /*+ ORDERED INDEX(AD1100 AD1105) INDEX(AD0500 AD0501) */"
SQL = SQL & " AD1100.AD01CODASISTENCI,"
SQL = SQL & " AD1100.AD11FECINICIO,"
SQL = SQL & " AD1100.CI32CODTIPECON,"
SQL = SQL & " AD1100.CI21CODPERSONA CI21CODPERSONA_REC,"
SQL = SQL & " AD1100.CI13CODENTIDAD,"
SQL = SQL & " AD1100.AD11FECFIN,"
SQL = SQL & " AD1100.AD11INDVOLANTE,"
SQL = SQL & " AD1100.AD07CODPROCESO,"
SQL = SQL & " CI2200.CI21CODPERSONA,"
SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE NOMBRE,"
SQL = SQL & " CI2200.CI22NUMHISTORIA,"
SQL = SQL & " AD0200.AD02CODDPTO,"
SQL = SQL & " AD0200.AD02DESDPTO,"
SQL = SQL & " CI3200.CI32DESTIPECON,"
SQL = SQL & " CI1300.CI13DESENTIDAD"
SQL = SQL & " From AD1100, AD0500, AD0700, CI2200, AD0200, CI3200, CI1300"
SQL = SQL & " Where "
SQL = SQL & " AD0500.AD07CODPROCESO=AD1100.AD07CODPROCESO"
SQL = SQL & " AND AD0500.AD01CODASISTENCI=AD1100.AD01CODASISTENCI"
SQL = SQL & " AND CI3200.CI32CODTIPECON =AD1100.CI32CODTIPECON"
SQL = SQL & " AND CI1300.CI13CODENTIDAD=AD1100.CI13CODENTIDAD"
SQL = SQL & " AND CI2200.CI21CODPERSONA=AD0700.CI21CODPERSONA"
SQL = SQL & " AND AD0200.AD02CODDPTO=AD0500.AD02CODDPTO"
SQL = SQL & " AND CI1300.CI32CODTIPECON=AD1100.CI32CODTIPECON"
SQL = SQL & " AND AD0700.AD07CODPROCESO=AD1100.AD07CODPROCESO AND "
strWhere = "AD1100.AD11INDVOLANTE = -1"
strWhere = strWhere & " AND SYSDATE BETWEEN AD1100.AD11FECINICIO AND NVL(AD1100.AD11FECFIN, TO_DATE('31/12/9999','DD/MM/YYYY'))"
strWhere = strWhere & " AND AD0500.AD02CODDPTO IN (" & strdptoSel & ")"
strWhere = strWhere & " AND AD11FECINICIO >= TO_DATE('" & strfecini & "','DD/MM/YYYY')"
strWhere = strWhere & " AND AD11FECINICIO < TO_DATE('" & DateAdd("d", 1, strfecfin) & "','DD/MM/YYYY') "
strWhere = strWhere & " AND CI3200.CI32CODTIPECON IN (" & strtipecon & ")"
strWhere = strWhere & " AND AD0500.AD05FECFINRESPON IS NULL"
If lvwtipecon.ListItems(1).Selected = False Then
    strWhere = strWhere & " AND CI1300.CI13CODENTIDAD IN (" & strentidad & ")"
End If
    strOrder = " ORDER BY AD0200.AD02DESDPTO, NOMBRE"
    SQL = SQL & strWhere & strOrder
Set rs = objApp.rdoConnect.OpenResultset(SQL) ' & strwhere & strorder)
If rs.EOF Then
    MsgBox "No hay ning�na actuaci�n pendiente de volante con esas caracter�sticas", vbOKOnly + vbExclamation, Me.Caption
    cmdimprimir.Enabled = False
    Exit Sub
Else
    cmdimprimir.Enabled = True
    Do While Not rs.EOF
        grdpendientes.AddItem rs!CI22NUMHISTORIA & Chr$(9) _
                            & rs!Nombre & Chr$(9) _
                            & rs!AD02DESDPTO & Chr$(9) _
                            & rs!CI32CODTIPECON & Chr$(9) _
                            & rs!CI32DESTIPECON & Chr$(9) _
                            & rs!CI13CODENTIDAD & Chr$(9) _
                            & rs!AD11FECINICIO & Chr$(9) _
                            & rs!AD11FECFIN & Chr$(9) _
                            & rs!CI13DESENTIDAD & Chr$(9) _
                            & rs!AD07CODPROCESO & Chr$(9) _
                            & rs!AD01CODASISTENCI & Chr$(9) _
                            & rs!CI21CODPERSONA & Chr$(9)
   rs.MoveNext
    Loop
    rs.Close
    End If
End Sub
Private Sub pFormatearGrid()
    Dim i%
    With grdpendientes
        .Columns(0).Caption = "Historia"
        .Columns(0).Width = 800
        .Columns(1).Caption = "Nombre"
        .Columns(1).Width = 3500
        '.Columns(1).Visible = False
        .Columns(2).Caption = "Departamento"
        .Columns(2).Width = 2000
         .Columns(3).Caption = "T.Econ."
        .Columns(3).Width = 1200
        .Columns(3).Alignment = ssCaptionAlignmentCenter
        '.Columns(5).Visible = True
        .Columns(4).Caption = "Destipeco"
        .Columns(4).Width = 800
        .Columns(4).Visible = False
        .Columns(5).Caption = "Entidad"
        .Columns(5).Width = 800
        .Columns(5).Alignment = ssCaptionAlignmentCenter
        .Columns(6).Caption = "Fecha Inicio"
        .Columns(6).Width = 1000
        .Columns(7).Caption = "Fecha Final"
        .Columns(7).Width = 1000
        .Columns(7).Visible = False
       
        '.Columns(6).Visible = True
        .Columns(8).Caption = "DesEntidad"
        .Columns(8).Width = 300
        .Columns(8).Visible = False
        .Columns(9).Caption = "Proceso"
        .Columns(9).Visible = False
        .Columns(9).Width = 1000
        .Columns(10).Caption = "Asistencia"
        .Columns(10).Width = 1200
        .Columns(10).Visible = False
        .Columns(11).Caption = "persona"
        .Columns(11).Visible = False
'        .Columns(12).Caption = "Dr. Sol."
'        .Columns(12).Width = 1500
'        .Columns(13).Caption = "Fecha Pet."
'        .Columns(13).Width = 1000
'        .Columns(14).Caption = "Cama"
'        .Columns(14).Width = 600
'        .Columns(15).Caption = "Num. Act."
'        .Columns(15).Width = 0
'        .Columns(15).Visible = False
'        .Columns(16).Caption = "Cod. Persona"
'        .Columns(16).Width = 0
'        .Columns(16).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub
Private Sub pCargartipecon()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    
    'se cargan los tipos econ�micos
    SQL = "SELECT CI32CODTIPECON, CI32DESTIPECON"
    SQL = SQL & " FROM CI3200"
    SQL = SQL & " WHERE CI32FECFIVGTEC IS NULL"
'    sql = sql & " SELECT AD02CODDPTO FROM AD0300"
'    sql = sql & " WHERE SG02COD = ?"
'    sql = sql & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
'    sql = sql & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
'    sql = sql & " ORDER BY AD02DESDPTO"
'    Set qry = objApp.rdoConnect.CreateQuery("", sql)
'    qry(0) = objSecurity.strUser
'    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    If Not rs.EOF Then
      lvwtipecon.ListItems.Clear
      Set item = lvwtipecon.ListItems.Add(, , "TODOS")
      item.Tag = 0
      item.Selected = True
    End If
    Do While Not rs.EOF
        Set item = lvwtipecon.ListItems.Add(, , rs!CI32CODTIPECON & " - " & rs!CI32DESTIPECON)
        item.Tag = rs!CI32CODTIPECON
        rs.MoveNext
    Loop
    rs.Close
    'qry.Close

End Sub
Private Sub pcargarentidad(strtipecon As String, tipecon As Integer)
    Dim rs As rdoResultset
    Dim SQL As String
    SQL = "SELECT DISTINCT CI13CODENTIDAD,CI13DESENTIDAD FROM CI1300"
    If lvwtipecon.ListItems(1).Selected = False Then
        SQL = SQL & " WHERE CI32CODTIPECON IN (" & strtipecon & ")"
    End If
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    If Not rs.EOF Then
       lvwentidad.ListItems.Clear
       
      Set item = lvwentidad.ListItems.Add(, , "TODOS")
      
      item.Tag = 0
      item.Selected = True
      
    End If
    
    Do While Not rs.EOF
        Set item = lvwentidad.ListItems.Add(, , rs!CI13CODENTIDAD & " - " & rs!CI13DESENTIDAD)
        item.Tag = rs!CI13CODENTIDAD
        rs.MoveNext
    Loop
  
    Call pentidad
End Sub
Private Sub pcargardpto()

Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    
    'se cargan los departamentos a los que tiene acceso el usuario
    SQL = "SELECT AD02CODDPTO, AD02DESDPTO"
    SQL = SQL & " FROM AD0200"
    SQL = SQL & " WHERE AD02CODDPTO IN ("
    SQL = SQL & " SELECT AD02CODDPTO FROM AD0300"
    SQL = SQL & " WHERE SG02COD = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
    SQL = SQL & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    SQL = SQL & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = objSecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
      lvwDpto.ListItems.Clear
      Set item = lvwDpto.ListItems.Add(, , "TODOS")
      item.Tag = 0
      item.Selected = True
    End If
    Do While Not rs.EOF
        Set item = lvwDpto.ListItems.Add(, , rs!AD02DESDPTO)
        item.Tag = rs!AD02CODDPTO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub


Private Sub cmdConsultar_Click()
 Dim i%, j%, strdptoSel$, strfecini$, strfecfin$
Screen.MousePointer = vbHourglass
cmdConsultar.Enabled = False
    grdpendientes.RemoveAll
    For i = 1 To lvwDpto.ListItems.Count
      If lvwDpto.ListItems(i).Selected Then
        If i = 1 Then
          For j = 2 To lvwDpto.ListItems.Count
            strdptoSel = strdptoSel & lvwDpto.ListItems(j).Tag & ","
          Next j
          Exit For
        Else
          strdptoSel = strdptoSel & lvwDpto.ListItems(i).Tag & ","
        End If
      End If
    Next i
    If strdptoSel <> "" Then
      strdptoSel = Left$(strdptoSel, Len(strdptoSel) - 1)
    Else
      MsgBox "No se ha seleccionado ning�n Departamento.", vbExclamation, Me.Caption
      Screen.MousePointer = vbDefault
      cmdConsultar.Enabled = True
      Exit Sub
    End If
    If Not IsDate(dtcFecInicio.Date) Then
      MsgBox "No ha seleccionado ninguna Fecha Desde.", vbExclamation, Me.Caption
      cmdConsultar.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    Else
      strfecini = Format(dtcFecInicio.Date, "DD/MM/YYYY")
    End If
    If Not IsDate(dtcFecFin.Date) Then
      MsgBox "No ha seleccionado ninguna Fecha Hasta.", vbExclamation, Me.Caption
      cmdConsultar.Enabled = True
      Screen.MousePointer = vbDefault
      
      Exit Sub
    Else
      strfecfin = Format(dtcFecFin.Date, "DD/MM/YYYY")
    End If
    
    If DateDiff("d", dtcFecInicio.Date, dtcFecFin.Date) < 0 Then
      MsgBox "La Fecha Desde no puede ser menor que la Fecha Hasta.", vbExclamation, Me.Caption
      cmdConsultar.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
    
    If strtipecon = "" Then Call ptipecon
    
    
    'objWinInfo.objWinActiveForm.strWhere = " AD02CODDPTO IN (" & strdptoSel & ")  AND AD11FECINICIO>=TO_DATE('" & strFecIni & "','DD/MM/YYYY') AND AD11FECINICIO<=TO_DATE('" & strFecFin & ",23:59','DD/MM/YYYY hh24:mi') "
   ' grdDBGrid1(1).Enabled = False
    'Call objWinInfo.DataRefresh
    'grdDBGrid1(1).Enabled = True
    'grdDBGrid1(1).Refresh
    Call pcargargrid(strdptoSel, strfecini, strfecfin) ', strtipecon, strentidad)
    cmdConsultar.Enabled = True
    Screen.MousePointer = vbDefault
End Sub


Private Sub cmdDocumenta_Click()
Dim intResp As Integer
Dim vntData(0 To 2) As Variant
cmdDocumenta.Enabled = False
      If (grdpendientes.Columns(11).Value <> "") Then
        vntData(0) = grdpendientes.Columns(11).Value
        vntData(1) = grdpendientes.Columns(10).Value
        vntData(2) = grdpendientes.Columns(9).Value
      
        Call objSecurity.LaunchProcess("AD2501", vntData)
        'Call objPipe.PipeSet("Persona", grdDBGrid1(1).Columns(3).Value)
'        Call objPipe.PipeSet("Persona", grdpendientes.Columns(11).Value) 'C�D.PACIENTE
'        Call objPipe.PipeSet("Asistencia", grdpendientes.Columns(10).Value) 'ASISTENCIA
'        Call objPipe.PipeSet("Proceso", grdpendientes.Columns(9).Value) 'PROCESO
'        Call objPipe.PipeSet("DeDondeViene", 3)
'        Call objSecurity.LaunchProcess("AD2501")
        'Call frmDocumentacion.Show(vbModal)
'        Call objPipe.PipeRemove("DeDondeViene")
'        Call objPipe.PipeRemove("Persona")
'        Call objPipe.PipeRemove("Asistencia")
'        Call objPipe.PipeRemove("Proceso")
        grdpendientes.Enabled = False
        'Call objWinInfo.DataRefresh
        grdpendientes.Enabled = True
        grdpendientes.Refresh
      Else
        intResp = MsgBox("Eliga un paciente", vbInformation, "Documentaci�n")
      End If
cmdDocumenta.Enabled = True
End Sub

Private Sub CmdImprimir_Click()
'Dim formula(1, 2)
'If strwhere = "" Then
'    MsgBox "Debe Seleccionar primero alguna actuaci�n", vbOKOnly + vbExclamation, "Imprimir. Pendientes de volante"
'    Exit Sub
'Else
 'formula(1, 1) = "FECHA"
 'formula(1, 2) = "date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
 Call Imprimir_API(strWhere, "AD0101.rpt", , , , False)
 cmdimprimir.Enabled = False
'End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
End Sub

Private Sub dtcFecFin_Change()
 If CDate(dtcFecFin.Date) < CDate(dtcFecInicio.Date) Then
        dtcFecInicio.Date = dtcFecFin.Date
    End If
End Sub

Private Sub dtcFecInicio_Change()
If CDate(dtcFecFin.Date) < CDate(dtcFecInicio.Date) Then
        dtcFecFin.Date = dtcFecInicio.Date
    End If
End Sub

Private Sub Form_Load()
    lvwentidad.Enabled = False
    Label1(1).Enabled = False
    lvwentidad2.Enabled = False
    strtipecon = ""
    strentidad = ""
    cmdimprimir.Enabled = False
    lvwDpto.ColumnHeaders.Add , , , 2000
    
    lvwtipecon.ColumnHeaders.Add , , , 1400
    lvwentidad.ColumnHeaders.Add , , , 1400
    Call pFormatearGrid
    Call pcargardpto
    Call pCargartipecon
    'Call pcargarentidad
End Sub



Private Sub grdpendientes_DblClick()
    If grdpendientes.SelBookmarks.Count = 0 Then
        MsgBox "No hay ninguna Actuaci�n seleccionada", vbOKOnly + vbCritical, Me.Caption
        Exit Sub
    Else
        Screen.MousePointer = vbHourglass
        Call objSecurity.LaunchProcess("CI4017", grdpendientes.Columns(11).Value)
      End If
End Sub

Private Sub lvwDpto_Click()
  Dim i%
    
    'Si se ha seleccionado TODOS, se deselecciona el resto de dptos.
    If lvwDpto.ListItems.Count > 0 Then
      If lvwDpto.ListItems(1).Selected Then
          For i = 2 To lvwDpto.ListItems.Count
              lvwDpto.ListItems(i).Selected = False
          Next i
      End If
    End If
End Sub

Private Sub lvwentidad_Click()
    strentidad = ""
     Call pentidad
End Sub

Private Sub lvwtipecon_Click()

    strtipecon = ""
    Call ptipecon
    
    
End Sub
