Attribute VB_Name = "modFunciones"
Option Explicit
'Constantes de los estados de las actuaciones
Public Const constESTACT_PLANIFICADA = 1
Public Const constESTACT_CITADA = 2
Public Const constESTACT_REALIZANDOSE = 3
Public Const constESTACT_REALIZADA = 4
Public Const constESTACT_INFORMADA = 5
Public Const constESTACT_CANCELADA = 6
Public Const constESTCITA_ANULADA = 2
Public Const constESTCITA_CITADA = 1
Public Const constESTCITA_RECITADA = 3
Public Const constESTCITA_PENDIENTERECITAR = 4



Public Function fTEAcunsa(strNH$) As Boolean
'*****************************************************************************************
'*  Comprueba si el Tipo Econ�mico se�alado para el paciente puede ser ACUNSA
'*****************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset

    SQL = "SELECT CI22NUMHISTORIA"
    SQL = SQL & " FROM CI0300"
    SQL = SQL & " WHERE CI22NUMHISTORIA = ? "
    SQL = SQL & " AND (CI03FECFINPOLI IS NULL OR CI03FECFINPOLI > SYSDATE)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNH
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then fTEAcunsa = True
    rs.Close
    qry.Close
End Function

Public Function fTEEntColab(strNH$) As Boolean
'*****************************************************************************************
'*  Comprueba si el Tipo Econ�mico se�alado para el paciente puede ser Entidad Colaboradora
'*****************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset

    SQL = "SELECT CI22NUMHISTORIA"
    SQL = SQL & " FROM CI2400"
    SQL = SQL & " WHERE CI22NUMHISTORIA = ? "
    SQL = SQL & " AND (CI24FECFIN IS NULL OR CI24FECFIN > SYSDATE)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNH
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then fTEEntColab = True
    rs.Close
    qry.Close
End Function
Public Sub pCitarActuaciones(strNAPlan$)
'***************************************************************************************

'*  strNAPLan: NAPlan, NAPlan,...
'*  Accede a las distintas pantallas que se utilizan para citar consultas, informes y pruebas
'*  seg�n cada una de las actuaciones que se pasan como par�metro
'*  Identifica tambi�n aquellas actuaciones que s�lo son citables por el Dpto.realizador
'***************************************************************************************

    Dim SQL$, rs As rdoResultset
    Dim strNAP1$, cllNAP2 As New Collection
    Dim msgDptoRea$, msgNoCitables$

    If Right$(strNAPlan, 1) = "," Then strNAPlan = Left$(strNAPlan, Len(strNAPlan) - 1)

    'se distribuyen las pruebas, informes y consultas a citar por cada uno de los dos m�todos
    While cllNAP2.Count > 0: cllNAP2.Remove 1: Wend
    SQL = "SELECT PR0400.PR04NUMACTPLAN, PR0100.PR12CODACTIVIDAD,"
    SQL = SQL & " PR0400.AD02CODDPTO, PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA,"
    SQL = SQL & " PR6600.PR66INDCITAEXTERNA"
    SQL = SQL & " FROM PR0400, PR0100, PR6600"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN IN (" & strNAPlan & ")"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR6600.AD02CODDPTO (+)= PR0400.AD02CODDPTO"
    SQL = SQL & " AND PR6600.PR01CODACTUACION (+)= PR0400.PR01CODACTUACION"
    SQL = SQL & " ORDER BY PR66INDCITAEXTERNA, PR0100.PR01DESCORTA"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        Select Case rs!PR12CODACTIVIDAD
        Case constACTIV_CONSULTA 'citables por huecos
            strNAP1 = strNAP1 & rs!PR04NUMACTPLAN & ","
        Case constACTIV_PRUEBA
            If IsNull(rs!PR66INDCITAEXTERNA) Then 'citables sin huecos
                If fblnCitarModiAnular(rs!AD02CODDPTO, rs!PR12CODACTIVIDAD, rs!PR01CODACTUACION) Then
                    cllNAP2.Add CStr(rs!PR04NUMACTPLAN)
                Else 'citables s�lo desde el Dpto realizador
                    msgDptoRea = msgDptoRea & "--> " & rs!PR01DESCORTA & Chr$(13)
                End If
            Else
                Select Case rs!PR66INDCITAEXTERNA
                Case -1 'citables por huecos
                    strNAP1 = strNAP1 & rs!PR04NUMACTPLAN & ","
                Case 0 'depende del Dpto del usuario
                    If rs!AD02CODDPTO = objSecurity.strUser Then 'citables por huecos
                        strNAP1 = strNAP1 & rs!PR04NUMACTPLAN & ","
                    Else 'citables s�lo desde el Dpto realizador
                        msgDptoRea = msgDptoRea & "--> " & rs!PR01DESCORTA & Chr$(13)
                    End If
                End Select
            End If
        Case constACTIV_INFORME, constACTIV_CONTCONSULTA 'citable sin huecos
            cllNAP2.Add CStr(rs!PR04NUMACTPLAN)
        Case Else
            msgNoCitables = msgNoCitables & "--> " & rs!PR01DESCORTA & Chr$(13)
        End Select
        rs.MoveNext
    Loop
    rs.Close

    If msgDptoRea <> "" Then
        msgDptoRea = "Las siguientes actuaciones son s�lo citables por el Dpto. realizador:" & Chr$(13) & msgDptoRea & Chr$(13)
    End If
    If msgNoCitables <> "" Then
        msgNoCitables = "Las siguientes actuaciones no pueden ser citadas:" & Chr$(13) & msgNoCitables
    End If
    If msgDptoRea <> "" Or msgNoCitables <> "" Then
        MsgBox msgDptoRea & msgNoCitables, vbInformation, "Citaci�n"
    End If

    If strNAP1 <> "" Then
        strNAP1 = Left$(strNAP1, Len(strNAP1) - 1)
        Call objSecurity.LaunchProcess("AG0211", strNAP1)
    End If
    If cllNAP2.Count > 0 Then
        Dim item
        For Each item In cllNAP2
            Call objSecurity.LaunchProcess("CI1150", item)
        Next
    End If
End Sub
Public Sub pCitarOverbooking(strNAPlan$)
    Dim SQL As String, rs As rdoResultset
    Dim cllNAP As New Collection
    Dim msgDptoRea$, msgNoCitables$

    If Right$(strNAPlan, 1) = "," Then strNAPlan = Left$(strNAPlan, Len(strNAPlan) - 1)

    'Sacamos los datos necesarios para ver si las actuacione son citables por ese usuario
    SQL = "SELECT PR0400.PR04NUMACTPLAN, PR0100.PR12CODACTIVIDAD,"
    SQL = SQL & " PR0400.AD02CODDPTO, PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA "
    SQL = SQL & " FROM PR0400, PR0100 WHERE"
    SQL = SQL & " PR04NUMACTPLAN IN (" & strNAPlan & ")"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " ORDER BY PR0100.PR01DESCORTA"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    While Not rs.EOF
        Select Case rs!PR12CODACTIVIDAD
        Case constACTIV_PRUEBA, constACTIV_CONSULTA, constACTIV_INFORME, constACTIV_CONTCONSULTA
            If fblnCitarModiAnular(rs!AD02CODDPTO, rs!PR12CODACTIVIDAD, rs!PR01CODACTUACION) Then
                cllNAP.Add CStr(rs!PR04NUMACTPLAN)
            Else 'citables s�lo desde el Dpto realizador
                msgDptoRea = msgDptoRea & "--> " & rs!PR01DESCORTA & Chr$(13)
            End If
        Case Else
            msgNoCitables = msgNoCitables & "--> " & rs!PR01DESCORTA & Chr$(13)
        End Select
        rs.MoveNext
    Wend

    If msgDptoRea <> "" Then
        msgDptoRea = "Las siguientes actuaciones son s�lo citables por el Dpto. realizador:" & Chr$(13) & msgDptoRea & Chr$(13)
    End If
    If msgNoCitables <> "" Then
        msgNoCitables = "Las siguientes actuaciones no pueden ser citadas:" & Chr$(13) & msgNoCitables
    End If
    If msgDptoRea <> "" Or msgNoCitables <> "" Then
        MsgBox msgDptoRea & msgNoCitables, vbInformation, "Citaci�n"
    End If

    If cllNAP.Count > 0 Then
        Dim item
        For Each item In cllNAP
            Call objSecurity.LaunchProcess("CI1150", item)
        Next
    End If
End Sub
'Funcion para citar o no una prueba
Public Function fblnCitarModiAnular(strDptoPrueba As String, strTipoPrueba As String, strCodAct As String) As Boolean
Dim SQL As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim vntDeptNoCitables
Dim vntPruebasCitables
Dim i As Integer
Dim K As Integer

  vntDeptNoCitables = Array("lun", "209", "170", "208", "250", "210", "11", "121", "206", "104", "106", "207", "120", "123", "113", "155", "172")
  vntPruebasCitables = Array("2248", "2473", "2474", "2466", "2477", "3196", "2788", "2793", "2802", "2803", _
  "2804", "3198", "2795", "2796", "2806", "3479")

  fblnCitarModiAnular = True

  If strTipoPrueba = constACTIV_PRUEBA Then  'en caso de ser una prueba
    For i = 1 To 16
      'miramos si pertenece a alguno de los dptos que se citan elllos mismos
      If strDptoPrueba = vntDeptNoCitables(i) Then
      'si es asi miramos si el usuario pertenece o no a ese departamento

        SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? AND AD03FECFIN IS NULL"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = vntDeptNoCitables(i)
        qry(1) = objSecurity.strUser
        Set rs = qry.OpenResultset()
        If rs(0) = 0 Then fblnCitarModiAnular = False
        rs.Close
        qry.Close
        If strDptoPrueba = constDPTO_RAYOS Then
        For K = 0 To 15
          If strCodAct = vntPruebasCitables(K) Then fblnCitarModiAnular = True
        Next K
        End If
      End If
    Next i
  End If

  If strTipoPrueba = constACTIV_INTERVENCION Then
    'SI ES UNA INTERVENCION COMPROBAMOS QUE EL USUARIO PERTENECE A QUIROFANO
    SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = constDPTO_QUIROFANO
    qry(1) = objSecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly)
    If rs(0) = 0 Then fblnCitarModiAnular = False
    rs.Close
    qry.Close
  End If
End Function
Public Sub bDBUpdateCamaAnuReservada(Cama As String, NumAct As Long)
    Dim sStmSql      As String
    Dim qryUpdate    As rdoQuery
 'On Error GoTo activarerror
    'Updatemos como Null la cama en la tabla AD1500 la cama reservada
    sStmSql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null "
    sStmSql = sStmSql & "WHERE AD15CODCAMA = ? "

    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = Cama
    qryUpdate.Execute
    qryUpdate.Close

    'Updatemos como Null la cama en la tabla CI0100 la cama reservada
    sStmSql = "UPDATE CI0100 SET AD15CODCAMA = Null "
    sStmSql = sStmSql & "WHERE  PR04NUMACTPLAN = ?  "

    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = NumAct  'Num_Actuaci�n
    qryUpdate.Execute
    qryUpdate.Close
End Sub
Public Sub pAnular(NumActPlan As Long, Cama As String, strMotivo As String)
Dim SQL As String
Dim qry As rdoQuery

   'lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
   'If lngincidencia <> -1 And lngincidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
    On Error GoTo Canceltrans
    objApp.BeginTrans
      SQL = "UPDATE PR0400 SET PR37CODESTADO=6,PR04FECCANCEL=SYSDATE,"
      SQL = SQL & " PR04DESMOTCAN = ? WHERE PR04NUMACTPLAN=?"
      Set qry = objApp.rdoConnect.CreateQuery(" ", SQL)
      qry(0) = strMotivo
      qry(1) = NumActPlan
      qry.Execute
      qry.Close

      SQL = "UPDATE CI0100 SET CI01SITCITA=2 WHERE PR04NUMACTPLAN=?"
      Set qry = objApp.rdoConnect.CreateQuery(" ", SQL)
      qry(0) = NumActPlan
      qry.Execute
      qry.Close

      If Cama <> "" Then 'hay que ir a anular la cama
            Call bDBUpdateCamaAnuReservada(Cama, NumActPlan)
      End If
      objApp.CommitTrans
      Exit Sub
Canceltrans:
     objApp.RollbackTrans
     MsgBox "Error en la anulacion", vbCritical, "Error"
End Sub
Public Sub pAnularAsociadas(lngActPlan&)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim lngActPedi&
    
    SQL = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPlan
    Set rs = qry.OpenResultset()
    lngActPedi = rs(0)
    rs.Close
    qry.Close
    
    SQL = "UPDATE PR0400 SET PR37CODESTADO = " & constESTACT_CANCELADA
    SQL = SQL & " WHERE PR04NUMACTPLAN IN ("
    SQL = SQL & " SELECT PR04NUMACTPLAN "
    SQL = SQL & " FROM PR6100, PR0400"
    SQL = SQL & " WHERE PR6100.PR03NUMACTPEDI_ASO = ?"
    SQL = SQL & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0400.PR37CODESTADO IN (" & constESTACT_PLANIFICADA & "," & constESTACT_CITADA & "))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPedi
    qry.Execute
    qry.Close
    
    SQL = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_ANULADA & "'"
    SQL = SQL & " WHERE CI01SITCITA = '" & constESTCITA_CITADA & "'"
    SQL = SQL & " AND PR04NUMACTPLAN IN ("
    SQL = SQL & " SELECT PR04NUMACTPLAN"
    SQL = SQL & " FROM PR6100, PR0400"
    SQL = SQL & " WHERE PR6100.PR03NUMACTPEDI_ASO = ?"
    SQL = SQL & " AND PR6100.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0400.PR37CODESTADO IN (" & constESTACT_PLANIFICADA & "," & constESTACT_CITADA & "))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActPedi
    qry.Execute
    qry.Close
End Sub

