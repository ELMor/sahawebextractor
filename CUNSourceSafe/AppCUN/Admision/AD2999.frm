VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmBuscaPersonas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Bucador de Personas"
   ClientHeight    =   6150
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11475
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6150
   ScaleWidth      =   11475
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdPFisicas 
      Caption         =   "&Hoja de Filiaci�n"
      Height          =   375
      Left            =   5640
      TabIndex        =   17
      Top             =   1620
      Width           =   1515
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10380
      TabIndex        =   16
      Top             =   1620
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Personas localizadas"
      ForeColor       =   &H00C00000&
      Height          =   4035
      Left            =   120
      TabIndex        =   10
      Top             =   2040
      Width           =   11265
      Begin SSDataWidgets_B.SSDBGrid grdPersonas 
         Height          =   3705
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   11025
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   19447
         _ExtentY        =   6535
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraBusqueda 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   1935
      Left            =   120
      TabIndex        =   5
      Top             =   60
      Width           =   5415
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   4260
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.Frame Frame2 
         Height          =   1095
         Left            =   3240
         TabIndex        =   12
         Top             =   720
         Width           =   1995
         Begin VB.OptionButton optSexo 
            Caption         =   "Mujer"
            Height          =   195
            Index           =   1
            Left            =   540
            TabIndex        =   15
            Top             =   780
            Width           =   1095
         End
         Begin VB.OptionButton optSexo 
            Caption         =   "Hombre"
            Height          =   195
            Index           =   0
            Left            =   540
            TabIndex        =   14
            Top             =   480
            Value           =   -1  'True
            Width           =   1095
         End
         Begin VB.CheckBox chkFon 
            Caption         =   "B�squeda fon�tica"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   180
            Width           =   1695
         End
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   900
         TabIndex        =   0
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   900
         TabIndex        =   1
         Top             =   660
         Width           =   2175
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   900
         TabIndex        =   2
         Top             =   1080
         Width           =   2175
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   900
         TabIndex        =   3
         Top             =   1500
         Width           =   2175
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   9
         Top             =   300
         Width           =   525
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   8
         Top             =   720
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   1140
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   6
         Top             =   1560
         Width           =   750
      End
   End
End
Attribute VB_Name = "frmBuscaPersonas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strCodPersFisica$

Private Sub cmdPFisicas_Click()
    Screen.MousePointer = vbHourglass
    Call objSecurity.LaunchProcess("CI4017")
    Screen.MousePointer = vbDefault
    'Si se ha seleccionado una persona...
    If objPipe.PipeExist("CodPersona") Then
        strCodPersFisica = objPipe.PipeGet("CodPersona")
        Call objPipe.PipeRemove("CodPersona")
        If Val(strCodPersFisica) > 0 Then Call pBuscar
        strCodPersFisica = ""
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call pFormatearGrid
End Sub

Private Sub grdPersonas_DblClick()
    If grdPersonas.Rows > 0 Then
        Call objPipe.PipeSet("AD2999_CI21CODPERSONA", grdPersonas.Columns("Cod. Persona").Text)
        Call objPipe.PipeSet("AD2999_CI22NUMHISTORIA", grdPersonas.Columns("N� Hist.").Text)
        Unload Me
    End If
End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    txtBuscar(Index).SelStart = 0: txtBuscar(Index).SelLength = Len(txtBuscar(Index).Text)
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Index
    Case 0
        Select Case KeyAscii
        Case 8
        Case 13: SendKeys "{TAB}"
        Case Is < Asc("0"), Is > Asc("9"): KeyAscii = 0
        End Select
    Case Else
        If KeyAscii = 13 Then SendKeys "{TAB}"
    End Select
End Sub

Private Sub cmdBuscar_Click()
    Call pBuscar
End Sub

Private Sub pBuscar()
    Dim SQL$, SQL1$, strHint$, qry As rdoQuery, rs As rdoResultset
    Dim cllBind As New Collection, i%, intSexo%
   
    Screen.MousePointer = vbHourglass
    
    If strCodPersFisica <> "" Then
        strHint = "/*+ ORDERED INDEX(CI2200 CI2201) */"
        SQL1 = "CI2200.CI21CODPERSONA = ? AND "
        cllBind.Add strCodPersFisica
    Else
        If txtBuscar(0).Text <> "" Then
            strHint = "/*+ ORDERED INDEX(CI2200 CI2202) */"
            SQL1 = "CI22NUMHISTORIA = ? AND "
            cllBind.Add txtBuscar(0).Text
        Else
            If chkFon.Value = False Then
                If txtBuscar(1).Text <> "" Then
                    SQL1 = SQL1 & "CI22NOMBREALF = GCFN05(?) AND "
                    cllBind.Add txtBuscar(1).Text
                End If
                If txtBuscar(2).Text <> "" Then
                    SQL1 = SQL1 & "CI22PRIAPELALF = GCFN05(?) AND "
                    cllBind.Add txtBuscar(2).Text
                End If
                If txtBuscar(3).Text <> "" Then
                    SQL1 = SQL1 & "CI22SEGAPELALF = GCFN05(?) AND "
                    cllBind.Add txtBuscar(3).Text
                End If
            Else
                If optSexo(0).Value = True Then intSexo = 1 Else intSexo = 2
                If txtBuscar(1).Text <> "" Then
                    SQL1 = SQL1 & "CI22CODFONNOM = GCFN04(?,1," & intSexo & ") AND "
                    cllBind.Add txtBuscar(1).Text
                End If
                If txtBuscar(2).Text <> "" Then
                    SQL1 = SQL1 & "CI22CODFONAP1 = GCFN04(?,0," & intSexo & ") AND "
                    cllBind.Add txtBuscar(2).Text
                End If
                If txtBuscar(3).Text <> "" Then
                    SQL1 = SQL1 & "CI22CODFONAP2 = GCFN04(?,0," & intSexo & ") AND "
                    cllBind.Add txtBuscar(3).Text
                End If
            End If
            If SQL1 <> "" Then strHint = "/*+ ORDERED INDEX(CI2200 " & fIndexHint & ") */"
        End If
    End If
    If SQL1 = "" Then Exit Sub
    
    grdPersonas.RemoveAll
    
    SQL = "SELECT " & strHint & " COUNT(*)"
    SQL = SQL & " FROM CI2200"
    SQL = SQL & " WHERE " & Left$(SQL1, Len(SQL1) - 4)
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    For i = 0 To cllBind.Count - 1
        qry(i) = UCase(cllBind(i + 1))
    Next i
    Set rs = qry.OpenResultset()
    If rs(0) > 500 Then
        SQL = "Se han encontrado " & rs(0) & " registros que cumplen las condiciones de b�squeda."
        SQL = SQL & Chr$(13)
        SQL = SQL & "La carga de los datos puede resultar excesivamente lenta."
        SQL = SQL & Chr$(13) & Chr$(13)
        SQL = SQL & "�Desea Ud. se�alar unas condicones de b�squeda m�s restrictivas?"
        If MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption) = vbYes Then
            Screen.MousePointer = vbDefault
            rs.Close: qry.Close: Exit Sub
        End If
    End If
    
    SQL = "SELECT " & strHint & " CI2200.CI21CODPERSONA, CI22NUMHISTORIA,"
    SQL = SQL & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PAC,"
    SQL = SQL & " CI22DNI, CI22FECNACIM, CI22DESLOCALID,"
    SQL = SQL & " CI10DESLOCALID, CI10CALLE||' '||CI10PORTAL||' '||CI10RESTODIREC DIRECC"
    SQL = SQL & " FROM CI2200, CI1000"
    SQL = SQL & " WHERE " & SQL1 & "CI1000.CI21CODPERSONA (+)= CI2200.CI21CODPERSONA"
    SQL = SQL & " AND CI10INDDIRPRINC (+)= -1"
    SQL = SQL & " ORDER BY PAC, CI10DESLOCALID, DIRECC"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    For i = 0 To cllBind.Count - 1
        qry(i) = UCase(cllBind(i + 1))
    Next i
    qry.QueryTimeout = 20
    On Error GoTo label 'para controlar el QueryTimeout
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        Do While Not rs.EOF
            grdPersonas.AddItem rs!CI21CODPERSONA & Chr$(9) _
                            & rs!CI22NUMHISTORIA & Chr$(9) _
                            & rs!PAC & Chr$(9) _
                            & rs!CI10DESLOCALID & Chr$(9) _
                            & rs!DIRECC & Chr$(9) _
                            & rs!CI22DNI & Chr$(9) _
                            & Format(rs!CI22FECNACIM, "dd/mm/yyyy") & Chr$(9) _
                            & rs!CI22DESLOCALID & Chr$(9)
            rs.MoveNext
        Loop
    Else
        Screen.MousePointer = vbDefault
        MsgBox "No se ha encontrado ning�n registro coincidente.", vbInformation, Me.Caption
    End If
    rs.Close
    qry.Close
    Screen.MousePointer = vbDefault
    
label:
    If Err > 0 Then
        SQL = "Se ha excedido el tiempo de b�squeda de datos."
        MsgBox SQL, vbExclamation, Me.Caption
    End If
End Sub

Private Sub pFormatearGrid()
    Dim i%
    With grdPersonas
        .Columns(0).Caption = "Cod. Persona"
        .Columns(0).Visible = False
        .Columns(1).Caption = "N� Hist."
        .Columns(1).Width = 800
        .Columns(1).Alignment = ssCaptionAlignmentRight
        .Columns(2).Caption = "Paciente"
        .Columns(2).Width = 3000
        .Columns(3).Caption = "Localidad"
        .Columns(3).Width = 1500
        .Columns(4).Caption = "Direcci�n"
        .Columns(4).Width = 2000
        .Columns(5).Caption = "DNI"
        .Columns(5).Width = 900
        .Columns(5).Alignment = ssCaptionAlignmentRight
        .Columns(6).Caption = "Fec. Nacim."
        .Columns(6).Width = 1000
        .Columns(7).Caption = "Lugar Nacim."
        .Columns(7).Width = 1500

        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Function fIndexHint() As String
    If chkFon.Value = False Then
        If txtBuscar(2).Text <> "" Then
            If txtBuscar(1).Text <> "" Then
                fIndexHint = "CI2218": Exit Function
            Else
                fIndexHint = "CI2216": Exit Function
            End If
        End If
        If txtBuscar(3).Text <> "" Then
            fIndexHint = "CI2217": Exit Function
        End If
        If txtBuscar(1).Text <> "" Then
            fIndexHint = "CI2218": Exit Function
        End If
    Else
        If txtBuscar(2).Text <> "" Then
            If txtBuscar(1).Text <> "" Then
                fIndexHint = "CI2215": Exit Function
            Else
                fIndexHint = "CI2213": Exit Function
            End If
        End If
        If txtBuscar(3).Text <> "" Then
            fIndexHint = "CI2214": Exit Function
        End If
        If txtBuscar(1).Text <> "" Then
            fIndexHint = "CI2215": Exit Function
        End If
    End If
End Function


