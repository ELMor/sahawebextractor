VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frmIngresoPend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Ingresos Pendientes"
   ClientHeight    =   8325
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11655
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8325
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin vsViewLib.vsPrinter vs 
      Height          =   375
      Left            =   0
      TabIndex        =   28
      Top             =   1200
      Visible         =   0   'False
      Width           =   375
      _Version        =   196608
      _ExtentX        =   661
      _ExtentY        =   661
      _StockProps     =   229
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
      MarginLeft      =   0
      MarginRight     =   0
      MarginTop       =   0
      MarginBottom    =   0
   End
   Begin TabDlg.SSTab tabTab1 
      Height          =   5175
      HelpContextID   =   90001
      Index           =   0
      Left            =   240
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   2880
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   9128
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Citas Ingresos "
      TabPicture(0)   =   "AD1000.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdAnularReserva"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdReservarCama"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdAsignarCama"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmddesasignarCama"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdActPlanif"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdcamasis"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).ControlCount=   7
      TabCaption(1)   =   "Camas"
      TabPicture(1)   =   "AD1000.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label1(1)"
      Tab(1).Control(1)=   "SSDBCombo1(1)"
      Tab(1).Control(2)=   "grdDBGrid1(1)"
      Tab(1).Control(3)=   "CmdImprimir"
      Tab(1).ControlCount=   4
      Begin VB.CommandButton CmdImprimir 
         Caption         =   "Listado de Ocupaci�n"
         Height          =   375
         Left            =   -74760
         TabIndex        =   25
         Top             =   600
         Width           =   1815
      End
      Begin VB.CommandButton cmdcamasis 
         Caption         =   "Cambios Asistencias"
         Height          =   375
         Left            =   9360
         TabIndex        =   11
         Top             =   4560
         Width           =   1815
      End
      Begin VB.CommandButton cmdActPlanif 
         Caption         =   "Actuaciones Planificadas"
         Height          =   375
         Left            =   7320
         TabIndex        =   10
         Top             =   4560
         Width           =   1935
      End
      Begin VB.CommandButton cmddesasignarCama 
         Caption         =   "Desasignar Cama"
         Height          =   375
         Left            =   5520
         TabIndex        =   9
         Top             =   4560
         Width           =   1695
      End
      Begin VB.CommandButton cmdAsignarCama 
         Caption         =   "Asignar Cama"
         Height          =   375
         Left            =   3720
         TabIndex        =   8
         Top             =   4560
         Width           =   1695
      End
      Begin VB.CommandButton cmdReservarCama 
         Caption         =   "Reservar Cama"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   4560
         Width           =   1695
      End
      Begin VB.CommandButton cmdAnularReserva 
         Caption         =   "Anular Reserva Cama"
         Height          =   375
         Left            =   1920
         TabIndex        =   7
         Top             =   4560
         Width           =   1695
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Ingresos Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3960
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Top             =   480
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3345
            Index           =   0
            Left            =   120
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   360
            Width           =   10800
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19050
            _ExtentY        =   5900
            _StockProps     =   79
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2130
         Index           =   2
         Left            =   -74880
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   90
         Width           =   10005
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17648
         _ExtentY        =   3757
         _StockProps     =   79
         Caption         =   "PACIENTES"
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3705
         Index           =   1
         Left            =   -74760
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   1200
         Width           =   10800
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   8
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   8
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Departamento"
         Columns(0).Name =   "Departamento"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2752
         Columns(1).Caption=   "Tipo Cama"
         Columns(1).Name =   "Tipo Cama"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "CamaReal"
         Columns(2).Name =   "camaReal"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   2
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Caption=   "Cama"
         Columns(3).Name =   "Cama"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3863
         Columns(4).Caption=   "Estado Cama"
         Columns(4).Name =   "Estado Cama"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3493
         Columns(5).Caption=   "Estado Reserva Cama"
         Columns(5).Name =   "Estado Reserva Cama"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   4022
         Columns(6).Caption=   "Paciente"
         Columns(6).Name =   "Paciente"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   2434
         Columns(7).Caption=   "Sexo"
         Columns(7).Name =   "Sexo"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         _ExtentX        =   19050
         _ExtentY        =   6535
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
         Height          =   330
         Index           =   1
         Left            =   -67440
         TabIndex        =   22
         Top             =   600
         Width           =   3495
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         DefColWidth     =   2
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "COD. DPTO"
         Columns(0).Name =   "COD. DPTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5927
         Columns(1).Caption=   "DESCRIPCI�N"
         Columns(1).Name =   "DESCRIPCI�N"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6165
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Selecci�n de Planta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   -69360
         TabIndex        =   23
         Top             =   600
         Width           =   1815
      End
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   5040
      Top             =   3840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Consultar"
      Height          =   375
      Left            =   4800
      TabIndex        =   0
      Top             =   690
      Width           =   1575
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   120
      Top             =   600
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   1
      Left            =   240
      TabIndex        =   13
      Top             =   1080
      Width           =   11340
      Begin VB.CommandButton cmdCitaManualRapida 
         Caption         =   "Cita Manual "
         Height          =   375
         Left            =   9960
         TabIndex        =   24
         Top             =   480
         Width           =   1335
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Imp. Etiquetas Quir."
         Height          =   375
         Left            =   8160
         TabIndex        =   27
         Top             =   480
         Width           =   1695
      End
      Begin VB.CommandButton cmdImprimirHojas 
         Caption         =   "Imprimir Hojas"
         Height          =   375
         Left            =   6840
         TabIndex        =   26
         Top             =   480
         Width           =   1215
      End
      Begin idperson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   2355
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   8040
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
      Height          =   330
      Index           =   0
      Left            =   8160
      TabIndex        =   4
      Top             =   690
      Width           =   3135
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      DefColWidth     =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2540
      Columns(0).Caption=   "COD. DPTO"
      Columns(0).Name =   "COD. DPTO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5927
      Columns(1).Caption=   "DESCRIPCI�N"
      Columns(1).Name =   "DESCRIPCI�N"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   0
      Left            =   720
      TabIndex        =   1
      Tag             =   "Fecha Desde"
      ToolTipText     =   "Fecha Desde"
      Top             =   690
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   2760
      TabIndex        =   3
      Tag             =   "Fecha Hasta"
      ToolTipText     =   "Fecha Hasta"
      Top             =   690
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   720
      TabIndex        =   20
      Top             =   480
      Width           =   1140
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   2685
      TabIndex        =   19
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Selecci�n del   Departamento:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   6720
      TabIndex        =   18
      Top             =   690
      Width           =   1335
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmIngresoPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Constantes para listas
Const iLSTCitas = 0
Const iLSTCamas = 1

'Constante que define el tipo planta dentro de los departamentos
Const iDpto_Tipo_Planta = 3
'Reservada parcial
Const iEstado2_Reservada = 1
'Variable booleana para saber de que forma se carga la lista de camas
Dim bReserva As Boolean
Dim bAsignar As Boolean

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
'Dim objMasterInfo   As New clsCWForm
Dim objMultiInfo1   As New clsCWForm
Dim objMultiInfo2   As New clsCWForm
'Constante de columnas
Const iCOLFecha = 3         'Fecha
Const iCOLHora = 4          'Hora
Const iCOLCodDpto = 5       'C�digo de Departamento
Const iCOLMedico = 6        'M�dico
Const iCOLTipEco = 7        'Tipo Economico
Const iCOLEntidad = 8       'Entidad
Const iCOLTipCama = 9       'Tipo cama
Const iCOLNombre = 10       'Nombre
Const iCOLPriApel = 11      'Primer Apellido
Const iCOLSegApel = 12       'Segundo Apellido
Const iCOLCamaReser = 13    'cama RESERVADA
Const iCOLCamaAsig = 14     'Cama Asignada
Const iCOLCamaReserVisor = 15    'cama RESERVADA Visor
Const iCOLCamaAsigVisor = 16     'Cama Asignada Visor
Const iCOLEdad = 17         'Edad
Const iCOLObserv = 18       'Observaciones
Const iCOLActuacion = 19    'Actuaci�n
Const iCOLCodActPedi = 20   'C�digo Actuaci�n Pedida
Const iCOLCodPaciente = 21  'C�digo Paciente
Const iCOLEstado = 22       'Estado
Const iCOLCodEstado = 23    'C�digo de Estado
Const iCOLCodEstMues = 24   'C�d.Estado Muestra
Const iCOLProceso = 25      'Proceso
Const iCOLAsistencia = 26   'asistencia
Const iCOLSexo = 27         'Sexo
Const iCOLNumPeticion = 28  'Num.Peticion
Const iCOLNumSolicitud = 29 'Num.Solicitud
Const iCOLNumCita = 30      'Num.Cita
    Private Sub cmdActPlanif_Click()
    Dim lrow As Long
    lrow = grdDBGrid1(0).Row
    ReDim vntData(1) As Variant
    'Pasa el c�digo de persona para mostrarla despu�s
    vntData(1) = IdPersona1.Text
    Call objSecurity.LaunchProcess("AD2105", vntData)
    'refrescar llamando de nuevo al bot�n de consulta
    Screen.MousePointer = vbHourglass  'cursor
    lrow = grdDBGrid1(0).Columns(2).Text
    objWinInfo.DataRefresh
    grdDBGrid1(0).MoveFirst
    While grdDBGrid1(0).Columns(2).Text <> lrow
    grdDBGrid1(0).MoveNext
    Wend
    lrow = grdDBGrid1(0).Row
    grdDBGrid1(0).SelBookmarks.Add (grdDBGrid1(0).RowBookmark(grdDBGrid1(0).Row))
    Screen.MousePointer = vbDefault 'cursor
    Call pControlBotones
End Sub
Private Sub cmdAnularReserva_Click()

    Dim sStmSql As String
    Dim lrow    As Long
    
    lrow = grdDBGrid1(0).Row
    Call bDBUpdateCamaAnuReservada
    'refrescar llamando de nuevo al bot�n de consulta
    Screen.MousePointer = vbHourglass  'cursor
    lrow = grdDBGrid1(0).Columns(2).Text
    objWinInfo.DataRefresh
    grdDBGrid1(0).MoveFirst
    While grdDBGrid1(0).Columns(2).Text <> lrow
    grdDBGrid1(0).MoveNext
    Wend
    lrow = grdDBGrid1(0).Row
    grdDBGrid1(0).SelBookmarks.Add (grdDBGrid1(0).RowBookmark(grdDBGrid1(0).Row))
    Screen.MousePointer = vbDefault
    Call pControlBotones
End Sub
Private Sub cmdcamasis_Click()
    Dim lrow As Long

''  'Cambios de asistencia antiguo
''    ReDim vntData(3) As Variant
''    'Pasa el c�digo de persona para mostrarla despu�s
''    vntData(1) = IdPersona1.Text
''    vntData(2) = grdDBGrid1(0).Columns(iCOLAsistencia).Value
''    Call objSecurity.LaunchProcess("AD0115", vntData)
    
    'Cambios de asistencia nuevo
    Dim vntData(1) As Variant
    'Pasa el c�digo de persona para mostrarla despu�s
    vntData(1) = IdPersona1.Text
    Call objSecurity.LaunchProcess("AD3000", vntData)
    
    
    lrow = grdDBGrid1(0).Row
    'refrescar llamando de nuevo al bot�n de consulta
    Screen.MousePointer = vbHourglass  'cursor
    lrow = grdDBGrid1(0).Columns(2).Text
    objWinInfo.DataRefresh
    grdDBGrid1(0).MoveFirst
    While grdDBGrid1(0).Columns(2).Text <> lrow
    grdDBGrid1(0).MoveNext
    Wend
    lrow = grdDBGrid1(0).Row
    grdDBGrid1(0).SelBookmarks.Add (grdDBGrid1(0).RowBookmark(grdDBGrid1(0).Row))
    Screen.MousePointer = vbDefault
    Call pControlBotones
End Sub
Private Sub cmdAsignarCama_Click()
    
    Dim lrow            As Long
    Dim strSQL          As String
    Dim Rs              As rdoResultset
    Dim STRA            As String
    Dim iResp           As Integer
    Dim vntA            As Variant
    Dim rsta            As rdoResultset
    Dim rstwhereTA      As rdoResultset
    Dim sCodActPte      As String
    Dim bResp           As Boolean
    Dim sCodPaciente    As String
    Dim sMensaje        As String
    Dim dFecha          As Date
    
            
    sCodPaciente = Trim(grdDBGrid1(0).Columns(iCOLCodPaciente).Value)
    'Comprobar si esta el paciente ya ingresado
    If bDBPacienteIngresado(sCodPaciente, sMensaje) = True Then
        dFecha = strFecha_Sistema
        If CDate(grdDBGrid1(0).Columns(iCOLFecha).Value) = dFecha Then
            MsgBox sMensaje, vbCritical
            Exit Sub
        Else
            sMensaje = sMensaje & Chr(13) & "�Desea Continuar? "
            iResp = MsgBox(sMensaje, vbYesNo)
            If iResp = 7 Then
                'si es seguir nada sino salimos de la funci�n
                Exit Sub
            End If
        End If
    End If
    'Guardamos el valor del registro de la lista.
    lrow = grdDBGrid1(0).Row
    
    ' Para asignar Cama, debe estar dada de alta la asistencia
    If grdDBGrid1(0).Columns(iCOLAsistencia).Text = 0 Or grdDBGrid1(0).Columns(iCOLProceso).Text = 0 Then
        Call MsgBox("Debe asociar antes el Proceso-Asistencia", vbInformation)
        Exit Sub
    End If
''    strSQL = "SELECT AD1500.AD01CODASISTENCI, AD15CODCAMA "
''    strSQL = strSQL & "FROM AD1500,AD0100 "
''    strSQL = strSQL & "WHERE AD1500.AD01CODASISTENCI=" & grdDBGrid1(0).Columns(iCOLAsistencia).Value & " "
''    strSQL = strSQL & "AND AD1500.AD01CODASISTENCI=AD0100.AD01CODASISTENCI "
''    strSQL = strSQL & "AND AD0100.CI21CODPERSONA=" & IdPersona1.Text
''    strSQL = strSQL & "AND AD0100.AD01FECFIN IS NULL" & " AND AD0100.AD34CODESTADO = 1 AND AD07CODPROCESO=" & grdDBGrid1(0).Columns(iCOLProceso).Text & " "
''    Set Rs = objApp.rdoConnect.OpenResultset(strSQL, rdOpenDynamic)
''    If Not Rs.EOF Then
''      Call objError.SetError(cwCodeMsg, "El paciente se encuentra en la cama:" & Rs!AD15CODCAMA & " y tiene la asistencia:" & Rs!AD01CODASISTENCI)
''      vntA = objError.Raise
''      Exit Sub
''    End If
''    Rs.Close
    Set Rs = Nothing
    If Trim(grdDBGrid1(0).Columns(iCOLCamaReser).Value) <> "" Then
        iResp = MsgBox("�Se desea asignar la cama provisional como real?", vbYesNo)
    Else
         iResp = 0
    End If
    If iResp = 6 Then
        'Yes, pasamos la cama provisional ---> cama
        'Estado Real a Reservada
        'Estado2 a null
        Call bDBUpdateCamaAnuReservada
        sCodActPte = grdDBGrid1(0).Columns(iCOLCodActPedi).Value
        Call bDBUpdateCamaAsignada(sCodActPte, Trim(grdDBGrid1(0).Columns(iCOLCamaReser).Value))
        'refrescar llamando de nuevo al bot�n de consulta
        Screen.MousePointer = vbHourglass  'cursor
        lrow = grdDBGrid1(0).Columns(2).Text
        objWinInfo.DataRefresh
        grdDBGrid1(0).MoveFirst
        While grdDBGrid1(0).Columns(2).Text <> lrow
        grdDBGrid1(0).MoveNext
        Wend
        lrow = grdDBGrid1(0).Row
        grdDBGrid1(0).SelBookmarks.Add (grdDBGrid1(0).RowBookmark(grdDBGrid1(0).Row))
        Screen.MousePointer = vbDefault
    Else
        'Liberamos la cama reservada
        Call bDBUpdateCamaAnuReservada
        'Llamamos a cargar camas pero
        'solo estado real  libre, para que eliga una.
        bReserva = False
        bAsignar = True
        Call pCargaCombo(1)
        tabTab1(0).Tab = 1
    End If
    Call pControlBotones
End Sub
Private Sub bDBUpdateCamaAnuReservada()
    Dim sStmSql      As String
    Dim qryUpdate    As rdoQuery
   
    'Updatemos como Null la cama en la tabla AD1500 la cama reservada
    sStmSql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null "
    sStmSql = sStmSql & "WHERE AD15CODCAMA = ? "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = Trim(grdDBGrid1(0).Columns(iCOLCamaReser).Value)
    qryUpdate.Execute
    qryUpdate.Close
   
    'Updatemos como Null la cama en la tabla CI0100 la cama reservada
    sStmSql = "UPDATE CI0100 SET AD15CODCAMA = Null "
    sStmSql = sStmSql & "WHERE  PR04NUMACTPLAN = ?  "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = Trim(grdDBGrid1(0).Columns(iCOLCodActPedi).Value)   'Num_Actuaci�n
    qryUpdate.Execute
    qryUpdate.Close
   
End Sub
Private Sub bDBUpdateCamaReservada(sCodActPte As String)
  
    Dim sStmSql      As String
    Dim qryUpdate    As rdoQuery
   
    'Updatemos como reservada la cama en la tabla AD1500 la cama reservada
    sStmSql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = ? "
    sStmSql = sStmSql & "WHERE AD15CODCAMA = ?  "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = iEstado2_Reservada
    qryUpdate(1) = Trim(grdDBGrid1(1).Columns(2).Text)
    qryUpdate.Execute
    qryUpdate.Close
   
   
    'Updatemos como reservada la cama en la tabla CI0100 la cama reservada
    sStmSql = "UPDATE CI0100 SET AD15CODCAMA = ? "
    sStmSql = sStmSql & "WHERE  PR04NUMACTPLAN = ?  "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryUpdate(0) = Trim(grdDBGrid1(1).Columns(2).Text)    'Cama
    qryUpdate(1) = sCodActPte  'Num_Actuaci�n
    qryUpdate.Execute
    qryUpdate.Close
   
End Sub
Private Sub bDBUpdateCamaAsignada(sCodActPte As String, sCodCama As String)
    Dim sStmSql      As String
    Dim qryUpdate    As rdoQuery
    Dim qryEstCama   As rdoQuery
    Dim rsEstCama    As rdoResultset
    Dim qrySelect    As rdoQuery
    Dim rdoCaso      As rdoResultset
    Dim strCaso      As String
    Dim sHistoria    As String
    Dim qryInsert    As rdoQuery
    Dim sNumRegistro As String
    
    On Error Resume Next
    Err = 0
    
    
    'EFS:compruebo que la cama no este todavia ocupada
    sStmSql = "SELECT AD14CODESTCAMA FROM AD1500 WHERE AD15CODCAMA = ?"
    Set qryEstCama = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryEstCama(0) = sCodCama
    Set rsEstCama = qryEstCama.OpenResultset(sStmSql)
    If rsEstCama.EOF = False Then
        If rsEstCama(0).Value <> iEstado_Libre Then
            MsgBox "Esta cama no se encuentra libre. No se puede Asignar", vbCritical
            Exit Sub
        End If
    End If
    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Updatemaos del registro en la tabla AD1500 la cama asignada
    sStmSql = "UPDATE AD1500 SET AD37CODESTADO2CAMA = Null, "
    sStmSql = sStmSql & "AD14CODESTCAMA  = ?, "
    sStmSql = sStmSql & "AD01CODASISTENCI = ?, "
    sStmSql = sStmSql & "AD07CODPROCESO = ? "
    sStmSql = sStmSql & "WHERE AD15CODCAMA = ?  "
    
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryUpdate(0) = iEstado_Reservada
        qryUpdate(1) = Trim(grdDBGrid1(0).Columns(iCOLAsistencia).Value)
        qryUpdate(2) = Trim(grdDBGrid1(0).Columns(iCOLProceso).Value)
        qryUpdate(3) = sCodCama
    qryUpdate.Execute
    qryUpdate.Close
        
    'EFS Actualizar Tabla Situaci�n Camas (AD1600)
    'PONEMOS FIN AL ESTADO ACTUAL (QUE DEBE SER LIBRE)
    sStmSql = "UPDATE AD1600 SET AD16FECFIN=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    sStmSql = sStmSql & " WHERE AD15CODCAMA = ?"
    sStmSql = sStmSql & " AND AD16FECFIN IS NULL"
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryUpdate(0) = strFecha_Sistema & " " & strHora_Sistema
        qryUpdate(1) = sCodCama
    qryUpdate.Execute
    qryUpdate.Close
    
    'INSERTAMOS EL NUEVO ESTADO QUE SERA EL DE RESERVADO.
    sStmSql = "INSERT INTO AD1600 (AD15CODCAMA,AD16FECCAMBIO,AD14CODESTCAMA,"
    sStmSql = sStmSql & "AD01CODASISTENCI,AD07CODPROCESO) VALUES (?,"
    sStmSql = sStmSql & "TO_DATE(?,'DD/MM/YYYY HH24:MI:ss'),"
    sStmSql = sStmSql & "?,?,?)"
    Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryUpdate(0) = sCodCama
        qryUpdate(1) = strFecha_Sistema & " " & strHora_Sistema()
        qryUpdate(2) = iEstado_Reservada
        qryUpdate(3) = Trim(grdDBGrid1(0).Columns(iCOLAsistencia).Value)
        qryUpdate(4) = Trim(grdDBGrid1(0).Columns(iCOLProceso).Value)
    qryUpdate.Execute
    qryUpdate.Close
End Sub
Private Sub bDBUpdateCamaDesAsignada()
    Dim sStmSql      As String
    Dim qryUpdate    As rdoQuery
    Dim qryCama      As rdoQuery
    Dim rdoCama      As rdoResultset
    Dim strCama      As String
    
    Dim rdoCaso      As rdoResultset
    Dim strCaso      As String
    Dim sHistoria    As String
    Dim qryInsert    As rdoQuery
    Dim sNumRegistro As String
    Dim qrySelect    As rdoQuery
    Dim sCodCama     As String
    Dim strFecha_AD1600 As String
    Dim strHora_AD1600  As String
    Dim rdoConsul    As rdoResultset
    
    
    On Error Resume Next
    Err = 0
    
    'Obtengo el codigo de cama
    sStmSql = "SELECT AD15CODCAMA FROM AD1500 WHERE "
    sStmSql = sStmSql & "AD01CODASISTENCI = ? "
    sStmSql = sStmSql & "AND AD07CODPROCESO = ? "
    sStmSql = sStmSql & "AND AD14ESTADOCAMA = " & iEstado_Reservada
    Set qryCama = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryCama(0) = Trim(grdDBGrid1(0).Columns(iCOLAsistencia).Value)
        qryCama(1) = Trim(grdDBGrid1(0).Columns(iCOLProceso).Value)
        qryCama.OpenResultset
    Set rdoCama = qryCama.OpenResultset(sStmSql)
    If Not rdoCama.EOF Then
        strCama = rdoCama!AD15CODCAMA
         
        'Updateo del registro en la tabla AD1500 la cama reservada
        'modificado por EFS
        sStmSql = "UPDATE AD1500 SET AD14CODESTCAMA = ?, "
        sStmSql = sStmSql & "AD37CODESTADO2CAMA = Null, "
        sStmSql = sStmSql & "AD01CODASISTENCI = Null,"
        sStmSql = sStmSql & "AD07CODPROCESO = Null "
        sStmSql = sStmSql & "WHERE AD01CODASISTENCI = ? "
        sStmSql = sStmSql & "AND AD07CODPROCESO = ? "
    '    sStmSql = sStmSql & "AND AD15CODCAMA = ? "
        Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryUpdate(0) = iEstado_Libre
    '    qryUpdate(1) = Trim(grdDBGrid1(0).Columns(iCOLTipCama).Value)
        qryUpdate(1) = Trim(grdDBGrid1(0).Columns(iCOLAsistencia).Value)
        qryUpdate(2) = Trim(grdDBGrid1(0).Columns(iCOLProceso).Value)
        qryUpdate.Execute
        qryUpdate.Close
        
                
        'EFS:Borrado del registro de la AD1600 y updateo el anterior para que se quede
        'como estaba
        sStmSql = "DELETE AD1600"
        sStmSql = sStmSql & " WHERE AD15CODCAMA = ?"
        sStmSql = sStmSql & " AND AD16FECCAMBIO = "
        sStmSql = sStmSql & "(SELECT MAX(AD16FECCAMBIO) FROM AD1600 WHERE AD15CODCAMA = ?)"
        Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryUpdate(0) = strCama
        qryUpdate(1) = strCama
        qryUpdate.Execute
        qryUpdate.Close
        sStmSql = "UPDATE AD1600 "
        sStmSql = sStmSql & "SET AD16FECFIN = Null"
        sStmSql = sStmSql & " WHERE AD15CODCAMA = ? "
        sStmSql = sStmSql & " AND AD16FECCAMBIO = "
        sStmSql = sStmSql & "(SELECT MAX(AD16FECCAMBIO) FROM AD1600 WHERE AD15CODCAMA = ?)"
        Set qryUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryUpdate(0) = strCama
        qryUpdate(1) = strCama
        qryUpdate.Execute
        qryUpdate.Close
    Else
        MsgBox "Este paciente no tiene cama asociada para el P/A", vbCritical
    End If
    
End Sub

Private Sub cmdCitaManualRapida_Click()
  
  Dim lrow As Long
    
    ReDim vntData(1) As Variant
    
    lrow = grdDBGrid1(0).Row
    'Pasa el c�digo de persona para mostrarla despu�s
    vntData(1) = IdPersona1.Text
    Call objSecurity.LaunchProcess("CI1147", vntData)
    'refrescar llamando de nuevo al bot�n de consulta
    Screen.MousePointer = vbHourglass  'cursor
    If grdDBGrid1(0).Rows > 0 Then
        lrow = grdDBGrid1(0).Columns(2).Text
        objWinInfo.DataRefresh
        grdDBGrid1(0).MoveFirst
        While grdDBGrid1(0).Columns(2).Text <> lrow
        grdDBGrid1(0).MoveNext
        Wend
        lrow = grdDBGrid1(0).Row
        grdDBGrid1(0).SelBookmarks.Add (grdDBGrid1(0).RowBookmark(grdDBGrid1(0).Row))
    End If
    Screen.MousePointer = vbDefault 'cursor
    Call pControlBotones
End Sub

Private Sub cmddesasignarCama_Click()
Dim sMensaje As String

Dim lrow As Long
    'Borramos la cama que se asigno
    If bDBPacienteIngresado(IdPersona1.Text, sMensaje) Then
        MsgBox sMensaje, vbExclamation, Me.Caption
        objWinInfo.DataRefresh
    Else
        'no updateamos la cita, dejamos la cama como provisional
        Call bDBUpdateCamaDesAsignada
        'refrescar llamando de nuevo al bot�n de consulta
        Screen.MousePointer = vbHourglass  'cursor
        lrow = grdDBGrid1(0).Columns(2).Text
        objWinInfo.DataRefresh
        grdDBGrid1(0).MoveFirst
        While grdDBGrid1(0).Columns(2).Text <> lrow
        grdDBGrid1(0).MoveNext
        Wend
        lrow = grdDBGrid1(0).Row
        grdDBGrid1(0).SelBookmarks.Add (grdDBGrid1(0).RowBookmark(grdDBGrid1(0).Row))
        Screen.MousePointer = vbDefault  'cursor
        Call pControlBotones
    End If
End Sub



Private Sub CmdImprimir_Click()
    
    Dim sDpto           As String
    Dim strWhereTotal   As String
    If Trim(SSDBCombo1(1).Text) <> "" Then
        strWhereTotal = "({AD1500.AD02CODDPTO})= " & SSDBCombo1(1).Columns(0).Text
    Else
        strWhereTotal = ""
    End If
    crtCrystalReport1.ReportFileName = objApp.strReportsPath & "AD1002.RPT"
    With crtCrystalReport1
        .PrinterCopies = 1
        .Destination = crptToWindow
        .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Me.MousePointer = vbHourglass
        .Action = 1
        Me.MousePointer = vbDefault
    End With
End Sub

Private Sub cmdImprimirHojas_Click()
    Dim strWhereTotal As String
    Dim strTipoHoja As String
    Dim intTipoImpresora As Integer
    Dim intCont As Integer
    Dim strSQL As String
    Dim rdprocAsis As rdoResultset
    Dim Rs As rdoResultset
    Dim Qy As rdoQuery
    Dim Qyn As rdoQuery
    Dim NumActPlan As Long
    Dim I As Integer
    Dim vntBookMark As Variant
    Dim lngFilas As Long
    
    
    'Imprimir hojas de los ingresos previstos.    Jesus 25-01-2000

    If (InStr(Printer.DeviceName, "HP LaserJet 4Si") <> 0) Then
        intTipoImpresora = 1
    ElseIf (InStr(Printer.DeviceName, "ADMISION") <> 0) Then
        intTipoImpresora = 2
    Else
        intTipoImpresora = 2
    End If
    

        If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
                If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
                  Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
                  Call objError.Raise
                  Exit Sub
                End If
        End If
        Me.MousePointer = vbHourglass
' Si no se selecciona ninguna, se listan todas las que est�n el grid.

    If grdDBGrid1(0).SelBookmarks.Count = 0 Then
        'Total de hojas a imprimir.
    
        strSQL = "SELECT TRUNC(FECHA),HORA,AD02CODDPTO,RECURSO," & _
                 "CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL," & _
                 "PR04numactplan,CI21CODPERSONA,PR37DESESTADO,PR37CODESTADO," & _
                 "PR56CODESTMUES,AD07CODPROCESO,AD01CODASISTENCI,SEXO," & _
                 "PR09NUMPETICION,CI31NUMSOLICIT,CI01NUMCITA " & _
                 "From PR0451J " & _
                 "Where PR0451J.AD07CODPROCESO Is Not Null " & _
                 "And PR0451J.AD01CODASISTENCI Is Not Null " & _
                 "AND (FECHA < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(1).Date) & "','DD/MM/YYYY') OR FECHA IS NULL)" & _
                 "AND FECHA >=  TO_DATE('" & dtcDateCombo1(0).Date & "','DD/MM/YYYY')" & _
                 "AND (AD14CODESTCAMA <> 2 OR AD14CODESTCAMA IS NULL) " & _
                 "ORDER BY TRUNC(FECHA) ASC,AD02CODDPTO ASC,CI22PRIAPEL ASC,CI22SEGAPEL ASC,CI22NOMBRE ASC"
        
        Me.MousePointer = vbDefault
        Set Qy = objApp.rdoConnect.CreateQuery("", strSQL)
        Set Rs = Qy.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        
            While Rs.EOF = False
                strWhereTotal = "({Ad0823j.AD01CODASISTENCI}=" & Rs.rdoColumns(13) & _
                "AND {Ad0823j.AD07CODPROCESO}=" & Rs.rdoColumns(12) & ")"
        
                For intCont = 0 To 1
                    Select Case intCont
                        Case 0:
                            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "filiacion" & _
                            intTipoImpresora & ".rpt"
                         Case 1:
                            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "hojaverde" & _
                            intTipoImpresora & ".rpt"
'                         Case 2:
'                            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "hojablanca" & _
'                            intTipoImpresora & ".rpt"
                    End Select
                    With crtCrystalReport1
                        .PrinterCopies = 1
                        .Destination = crptToPrinter
                        .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
                        .Destination = crptToPrinter
                    '        .Destination = crptToWindow
                        .Connect = objApp.rdoConnect.Connect
                        .DiscardSavedData = True
                        Me.MousePointer = vbHourglass
                        .Action = 1
                    '        .PrintReport
                        Me.MousePointer = vbDefault
                    End With
                Next intCont
               Rs.MoveNext
            Wend
        Rs.Close
        Qy.Close


    Else
        'Se ha seleccionado una.
            vntBookMark = grdDBGrid1(0).SelBookmarks(0)
            
            strWhereTotal = "({Ad0823j.AD01CODASISTENCI}=" & grdDBGrid1(0).Columns(iCOLAsistencia).CellValue(vntBookMark) & _
            "AND {Ad0823j.AD07CODPROCESO}=" & grdDBGrid1(0).Columns(iCOLProceso).CellValue(vntBookMark) & ")"
        
                For intCont = 0 To 1
                    Select Case intCont
                        Case 0:
                            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "filiacion" & _
                            intTipoImpresora & ".rpt"
                         Case 1:
                            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "hojaverde" & _
                            intTipoImpresora & ".rpt"
'                         Case 2:
'                            crtCrystalReport1.ReportFileName = objApp.strReportsPath & "hojablanca" & _
'                            intTipoImpresora & ".rpt"
                    End Select
                    With crtCrystalReport1
                        .PrinterCopies = 1
                        .Destination = crptToPrinter
                        .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
                        .Destination = crptToPrinter
                    '        .Destination = crptToWindow
                        .Connect = objApp.rdoConnect.Connect
                        .DiscardSavedData = True
                        Me.MousePointer = vbHourglass
                        .Action = 1
                    '        .PrintReport
                        Me.MousePointer = vbDefault
                    End With
                Next intCont
        Me.MousePointer = vbDefault
    End If
Me.MousePointer = vbDefault
End Sub

Private Sub Command1_Click()
If IdPersona1.Historia <> "" Then
    pImprimirEtiquetas (IdPersona1.Text)
Else
    MsgBox "Seleccione un Paciente", vbOKOnly
End If
End Sub

Private Sub Command5_Click()

 If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
        If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
          Call objError.Raise
          Exit Sub
        End If
End If
Me.MousePointer = vbHourglass
If SSDBCombo1(0).Text = "" Then
    objMultiInfo1.strWhere = "(FECHA < TO_DATE('" & _
                 DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') OR FECHA IS NULL) " & _
                 "AND FECHA > =  TO_DATE('" & dtcDateCombo1(0).Text & _
                 "','DD/MM/YYYY') " '& _
                 "AND (AD14CODESTCAMA <> " & iEstado_Ocupado & "OR AD14CODESTCAMA IS NULL)"
Else
    objMultiInfo1.strWhere = "(FECHA < TO_DATE('" & _
                 DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') OR FECHA IS NULL) " & _
                 "AND FECHA > =  TO_DATE('" & dtcDateCombo1(0).Text & _
                 "','DD/MM/YYYY') AND PR0451J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value '& _
                 " AND " & "(AD14CODESTCAMA <> " & iEstado_Ocupado & "OR AD14CODESTCAMA IS NULL)"
End If
objWinInfo.DataRefresh
Me.MousePointer = vbDefault
Call pControlBotones
End Sub


Private Sub Form_Activate()
    'control botones dependiendo de los datos existentes
    Call pControlBotones
          
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  If primerallamada = True Then
    primerallamada = False
  End If
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
   
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMultiInfo1
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
      
    .strTable = "PR0451J"

    .strWhere = "(FECHA < TO_DATE('" & _
                 DateAdd("d", 1, strFecha_Sistema) & "','DD/MM/YYYY') OR FECHA IS NULL) " & _
                 " AND FECHA > = (SELECT TO_date(SYSDATE) FROM DUAL) AND " & _
                 "AD14CODESTCAMA <> " & iEstado_Ocupado
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    'El grid estar� ordenado por la fecha de la consulta
    Call .FormAddOrderField("TRUNC(FECHA)", cwAscending)
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Pendientes")
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado", cwString)
    Call .FormAddFilterWhere(strKey, "CI32CODTIPECON", "Eco.", cwString)
    Call .FormAddFilterWhere(strKey, "CI13CODENTIDAD", "Ent.", cwString)
    
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR37DESESTADO", "Estado")
    
    Call .objPrinter.Add("AD1000", "Listado de Ingresos Pendientes")
    Call .objPrinter.Add("AD1001", "Listado de Ingresos Pendientes de Acunsa")
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo1, "Fecha", "TRUNC(FECHA)", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Hora", "HORA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Dpto", "AD02CODDPTO", cwString, 3)
    Call .GridAddColumn(objMultiInfo1, "M�dico", "RECURSO", cwString, 20)
    'Tipo Economico, Entidad, Tipo cama
    Call .GridAddColumn(objMultiInfo1, "Eco.", "CI32CODTIPECON", cwString, 1)
    Call .GridAddColumn(objMultiInfo1, "Ent.", "CI13CODENTIDAD", cwString, 2)
    Call .GridAddColumn(objMultiInfo1, "T.Cama", "AD13CODTIPOCAMA", cwString, 2)
    
    Call .GridAddColumn(objMultiInfo1, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    'cama RESERVADA
    Call .GridAddColumn(objMultiInfo1, "Cama Reser.", "CAMARESERV", cwString, 7)
    Call .GridAddColumn(objMultiInfo1, "Cama Asigna.", "CAMAASIG", cwString, 7)
    Call .GridAddColumn(objMultiInfo1, "C.Reser", "CAMAREVISOR", cwString, 7)
    Call .GridAddColumn(objMultiInfo1, "C.Asig", "CAMAASVISOR", cwString, 7)
    
    'Edad, Observaciones
    Call .GridAddColumn(objMultiInfo1, "Edad", "EDAD", cwString, 2)
    Call .GridAddColumn(objMultiInfo1, "Observaciones", "PR08DESOBSERV", cwString, 250)
    Call .GridAddColumn(objMultiInfo1, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo1, "Cod act pedi", "PR04numactplan", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Cod paciente", "CI21CODPERSONA", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Estado", "PR37DESESTADO", cwString, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado", "PR37CODESTADO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado Muestra", "PR56CODESTMUES", cwNumeric, 10)
    'Proceso y asistencia
    Call .GridAddColumn(objMultiInfo1, "C�d.Proceso", "AD07CODPROCESO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
    'Sexo
    Call .GridAddColumn(objMultiInfo1, "Sexo", "SEXO", cwString, 8)
    
    Call .GridAddColumn(objMultiInfo1, "Num.Peticion", "PR09NUMPETICION", cwString, 9)
    Call .GridAddColumn(objMultiInfo1, "Num.Solicitud", "CI31NUMSOLICIT", cwString, 9)
    Call .GridAddColumn(objMultiInfo1, "Num.Cita", "CI01NUMCITA", cwString, 5)
    Call .FormCreateInfo(objMultiInfo1)
    'La primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(iCOLFecha)).intKeyNo = 1
    Call .FormChangeColor(objMultiInfo1)
    .CtrlGetInfo(grdDBGrid1(0).Columns(iCOLNombre)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(iCOLPriApel)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(iCOLSegApel)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(iCOLActuacion)).blnInFind = True
    
     
    'Los campos no estar�n visibles en el grid
    grdDBGrid1(0).Columns(iCOLCodActPedi).Visible = False
    grdDBGrid1(0).Columns(iCOLCodPaciente).Visible = False
    grdDBGrid1(0).Columns(iCOLEstado).Visible = False
    grdDBGrid1(0).Columns(iCOLCodEstado).Visible = False
    grdDBGrid1(0).Columns(iCOLCodEstMues).Visible = False
    grdDBGrid1(0).Columns(iCOLProceso).Visible = False
    grdDBGrid1(0).Columns(iCOLAsistencia).Visible = False
    grdDBGrid1(0).Columns(iCOLCamaReser).Visible = False
    grdDBGrid1(0).Columns(iCOLCamaAsig).Visible = False
    grdDBGrid1(0).Columns(iCOLNumPeticion).Visible = False
    grdDBGrid1(0).Columns(iCOLNumSolicitud).Visible = False
    grdDBGrid1(0).Columns(iCOLNumCita).Visible = False
        
    IdPersona1.blnSearchButton = False
    Dim sqlstr As String
    Dim rsta As rdoResultset
    Dim sqlstrdpto As String
    Dim rstadpto As rdoResultset

    sqlstr = "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objSecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL ORDER BY AD0200.AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    While rsta.EOF = False
        SSDBCombo1(0).AddItem rsta("AD02CODDPTO").Value & ";" & rsta("AD02DESDPTO").Value
        SSDBCombo1(0).Text = rsta("AD02DESDPTO").Value
        SSDBCombo1(0).MoveNext
        rsta.MoveNext
    Wend

    rsta.Close
    Set rsta = Nothing
    SSDBCombo1(0).Enabled = True
    SSDBCombo1(0).Text = ""
    'Unir con tipo de cama
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(iCOLTipCama)), "AD13CODTIPOCAMA", "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA FROM AD1300 WHERE AD13CODTIPOCAMA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(iCOLTipCama)), grdDBGrid1(0).Columns(iCOLTipCama), "AD13DESTIPOCAMA")
    Call .WinRegister
    Call .WinStabilize
  End With
    
    'Damos el ancho de las columnas del grid
    grdDBGrid1(0).Columns(iCOLFecha).Width = 1000
    grdDBGrid1(0).Columns(iCOLHora).Width = 600
    grdDBGrid1(0).Columns(iCOLNombre).Width = 900
    grdDBGrid1(0).Columns(iCOLPriApel).Width = 900
    grdDBGrid1(0).Columns(iCOLSegApel).Width = 900
    grdDBGrid1(0).Columns(iCOLActuacion).Width = 2800
    grdDBGrid1(0).Columns(iCOLMedico).Width = 1200
    grdDBGrid1(0).Columns(iCOLEstado).Width = 1400
    grdDBGrid1(0).Columns(iCOLTipEco).Width = 350
    grdDBGrid1(0).Columns(iCOLEntidad).Width = 350
    grdDBGrid1(0).Columns(iCOLCamaReserVisor).Width = 600
    grdDBGrid1(0).Columns(iCOLCamaAsigVisor).Width = 600
    grdDBGrid1(0).Columns(iCOLCodDpto).Width = 470
    grdDBGrid1(0).Columns(iCOLEdad).Width = 400
        
    'Activamos y desactivamos los botones
    If fblnVerbotones Then
        Call pControlBotones
    End If
    'Cargo la combo de dptos.
    Call pCargaCombo(1)
    IdPersona1.blnSearchButton = False
    SSDBCombo1(0).AddItem ";" 'Se introduce un elemento vac�o
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
    
  Dim sStmSql As String
  Dim qryConsulta As rdoQuery
  Dim rstConsulta As rdoResultset
    
    Select Case Index
        Case 0  'Grilla de Ingresos Pendientes
            Select Case grdDBGrid1(0).col
                Case iCOLTipCama
                    grdDBGrid1(0).Columns(iCOLTipCama).style = ssStyleComboBox
                    grdDBGrid1(0).Columns(iCOLTipCama).Locked = False
                    grdDBGrid1(0).Columns(iCOLTipCama).RemoveAll
            
                    'Tipos de Cama
                    grdDBGrid1(0).Columns(iCOLTipCama).RemoveAll
                    sStmSql = "SELECT AD13CODTIPOCAMA, AD13DESTIPOCAMA FROM AD1300 "
                    sStmSql = sStmSql & "ORDER BY TO_NUMBER(AD13CODTIPOCAMA) "
                    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sStmSql)
                    Set rstConsulta = qryConsulta.OpenResultset()
                    Do While Not rstConsulta.EOF
                        grdDBGrid1(0).Columns(iCOLTipCama).AddItem rstConsulta(1)
                        rstConsulta.MoveNext
                    Loop
                    rstConsulta.Close
                    qryConsulta.Close
                Case iCOLObserv
                    grdDBGrid1(0).Columns(iCOLObserv).Locked = False
            End Select
    End Select
End Sub

Private Sub grdDBGrid1_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case Index
        Case 0
            If KeyCode = 46 And grdDBGrid1(0).col = 19 Then
                grdDBGrid1(0).Columns(iCOLTipCama).Value = ""
            End If
    End Select
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
If Index = 0 Then
    If Val(grdDBGrid1(Index).Columns("C.Asig").Value) < 200 Then
      grdDBGrid1(Index).Columns("C.Asig").Value = ""
      grdDBGrid1(Index).Columns("Cama Asigna.").Value = ""
    End If
End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
 

End Sub

Private Sub SSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
    Select Case intIndex
        Case 0
            If KeyCode = 46 Or KeyCode = 8 Then
                SSDBCombo1(intIndex).Text = ""
                objMultiInfo1.strWhere = "(FECHA < TO_DATE('" & _
                             DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') OR FECHA IS NULL) " & _
                             "AND FECHA > =  TO_DATE('" & dtcDateCombo1(0).Text & _
                             "','DD/MM/YYYY') " '& _
                             "AND (AD14CODESTCAMA <> " & iEstado_Ocupado & "OR AD14CODESTCAMA IS NULL)"
                objWinInfo.DataRefresh
            End If
    End Select
End Sub
Private Sub SSDBCombo1_CloseUp(Index As Integer)
    
    Select Case Index
        Case 0
            SSDBCombo1(0).Text = SSDBCombo1(0).Columns(1).Value
            Me.MousePointer = vbHourglass
            If SSDBCombo1(0).Text = "" Then
                objMultiInfo1.strWhere = "(FECHA < TO_DATE('" & _
                             DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') OR FECHA IS NULL) " & _
                             "AND FECHA > =  TO_DATE('" & dtcDateCombo1(0).Text & _
                             "','DD/MM/YYYY') " '& _
                             "AND (AD14CODESTCAMA <> " & iEstado_Ocupado & "OR AD14CODESTCAMA IS NULL)"
           Else
                objMultiInfo1.strWhere = "(FECHA < TO_DATE('" & _
                             DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') OR FECHA IS NULL) " & _
                             "AND FECHA > =  TO_DATE('" & dtcDateCombo1(0).Text & _
                             "','DD/MM/YYYY') AND PR0451J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value '& _
                             " AND " & "(AD14CODESTCAMA <> " & iEstado_Ocupado & "OR AD14CODESTCAMA IS NULL)"
           End If
           objWinInfo.DataRefresh
           Me.MousePointer = vbDefault
             
            IdPersona1.Text = grdDBGrid1(0).Columns(iCOLCodPaciente).Value
        Case 1
            SSDBCombo1(1).Text = SSDBCombo1(1).Columns(1).Value
            Call pCargarCamas(bReserva, SSDBCombo1(1).Columns(0).Value)
    End Select
End Sub
Private Sub SSDBCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = 0
End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub
Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub
Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
If Not gblnCancelar Then
  intCancel = objWinInfo.WinExit
End If
End Sub
Private Sub Form_Unload(intCancel As Integer)
If Not gblnCancelar Then
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End If
End Sub
Private Sub cmdReservarCama_Click()
    ReDim vntData(4) As Variant
    Dim lrow As Long
    Dim sCodPaciente    As String
    Dim sMensaje        As String
    Dim iResp           As Integer
    Dim dFecha          As Date
            
    sCodPaciente = Trim(grdDBGrid1(0).Columns(iCOLCodPaciente).Value)
    'Comprobar si esta el paciente ya ingresado
    If bDBPacienteIngresado(sCodPaciente, sMensaje) = True Then
        dFecha = strFecha_Sistema
        If CDate(grdDBGrid1(0).Columns(iCOLFecha).Value) = dFecha Then
            MsgBox sMensaje, vbCritical
            Exit Sub
        Else
            sMensaje = sMensaje & Chr(13) & "�Desea Continuar? "
            iResp = MsgBox(sMensaje, vbYesNo)
            If iResp = 7 Then
                'si es seguir nada sino salimos de la funci�n
                Exit Sub
            End If
        End If
    End If
    
    'Conservamos en el registro de la grid en la que estan
    lrow = grdDBGrid1(0).Row
    cmdReservarCama.Enabled = False
    If grdDBGrid1(0).Row >= 0 Then
        Call pCargaCombo(1)
        bReserva = True
        bAsignar = False
        If SSDBCombo1(1).Text <> "" Then
            Call pCargarCamas(bReserva, SSDBCombo1(1).Columns(0).Value)
        End If
        tabTab1(0).Tab = 1
    Else
        MsgBox "Selecciona una cita para reservar la cama", vbInformation
        Exit Sub
    End If
End Sub

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub
Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    Call pCargaCombo(1)
    grdDBGrid1(1).RemoveAll
  End If
End Sub
Private Sub tabTab1_DblClick(Index As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    grdDBGrid1(1).RemoveAll
  End If
    objWinInfo.DataRefresh
End Sub
Private Sub Timer1_Timer()

    Dim cont As Integer
    cont = cont + 1
    If cont = 5 Then
        cont = 0
        objWinInfo.DataRefresh
        pControlBotones
    End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Dim numPetic    As Integer
    Dim sStmSql     As String
    Dim QyUpdate    As rdoQuery
    
    
    Err = 0
    On Error Resume Next
    Select Case btnButton.Index
        Case 4 'Grabar
            'Grabaremos los datos modificados:
            
            'Tipo de cama, con los valores que tenemos en las columnas 28 y 29
            If grdDBGrid1(0).Columns(iCOLNumSolicitud).Value <> "" And grdDBGrid1(0).Columns(iCOLNumCita).Value <> "" Then
                sStmSql = "UPDATE CI0100 SET AD13CODTIPOCAMA = (SELECT AD13CODTIPOCAMA FROM AD1300 WHERE AD13DESTIPOCAMA = ? )"
                sStmSql = sStmSql & "WHERE CI31NUMSOLICIT = ? "
                sStmSql = sStmSql & "AND CI01NUMCITA = ? "
                Set QyUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
                
                QyUpdate(0) = Trim(grdDBGrid1(0).Columns(iCOLTipCama).Value)
                QyUpdate(1) = grdDBGrid1(0).Columns(iCOLNumSolicitud).Value
                QyUpdate(2) = grdDBGrid1(0).Columns(iCOLNumCita).Value
                QyUpdate.Execute
                QyUpdate.Close
            End If
            If grdDBGrid1(0).Columns(iCOLNumPeticion).Value <> "" Then
                'Si tenemos numero de peticion, modificamos las observaciones
                'tablas PR0800 y PR0900 chequeando el dato de la columna 27
                sStmSql = "UPDATE PR0800 SET PR08DESOBSERV = ? "
                sStmSql = sStmSql & "WHERE PR09NUMPETICION = ? "
                Set QyUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
                QyUpdate(0) = grdDBGrid1(0).Columns(iCOLObserv).Value
                QyUpdate(1) = grdDBGrid1(0).Columns(iCOLNumPeticion).Value
                QyUpdate.Execute
                QyUpdate.Close
                            
                sStmSql = "UPDATE PR0900 SET PR09DESOBSERVAC = ? "
                sStmSql = sStmSql & "WHERE PR09NUMPETICION = ? "
                Set QyUpdate = objApp.rdoConnect.CreateQuery("", sStmSql)
                QyUpdate(0) = grdDBGrid1(0).Columns(iCOLObserv).Value
                QyUpdate(1) = grdDBGrid1(0).Columns(iCOLNumPeticion).Value
                QyUpdate.Execute
                QyUpdate.Close
            End If
            If Err > 0 Then
                objApp.rdoConnect.RollbackTrans
                MsgBox "Error en grabaci�n de datos", vbCritical
            Else
                objApp.rdoConnect.CommitTrans
            End If
            objWinInfo.DataRefresh
        Case 30
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            Exit Sub
        Case Else
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            'Activamos y desactivamos los botones
            pControlBotones
    End Select
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub
Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub
Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub
Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub
Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub
Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Select Case intIndex
        Case 0
            pControlBotones
    End Select
End Sub
Private Sub grdDBGrid1_DblClick(intIndex As Integer)
            
    Dim sStmSql     As String
    Dim qryUpdate   As rdoQuery
    Dim sCodActPte  As String
    Dim lrow As Long
    
    Select Case intIndex
        Case iLSTCitas
            Call objWinInfo.GridDblClick
        Case iLSTCamas
            If grdDBGrid1(1).Rows > 0 Then
                'Han seleccionado alguna cama que insertamos
                'en el registro de citas. Modificando la cita  (CI0100)
                'para incluirle la cama como reservada provisional
                If Trim(grdDBGrid1(1).Columns(5).Value) = "" Then
                    sCodActPte = Trim(grdDBGrid1(0).Columns(iCOLCodActPedi).Value)
                    If bReserva = True Then
                        Call bDBUpdateCamaReservada(sCodActPte)
                    ElseIf bAsignar = True Then
                        Call bDBUpdateCamaAsignada(sCodActPte, Trim(grdDBGrid1(1).Columns(2).Text))
                    End If
                Else
                  MsgBox "Esta cama esta Reservada", vbCritical
                  Exit Sub
                End If
                'refrescar llamando de nuevo al bot�n de consulta
                Screen.MousePointer = vbHourglass 'cursor
                lrow = grdDBGrid1(0).Columns(2).Text
                objWinInfo.DataRefresh
                grdDBGrid1(0).MoveFirst
                While grdDBGrid1(0).Columns(2).Text <> lrow
                grdDBGrid1(0).MoveNext
                Wend
                lrow = grdDBGrid1(0).Row
                grdDBGrid1(0).SelBookmarks.Add (grdDBGrid1(0).RowBookmark(grdDBGrid1(0).Row))
                tabTab1(0).Tab = 0
                Screen.MousePointer = vbDefault  'cursor
            End If
            Call pControlBotones
    End Select
End Sub
Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Select Case intIndex
        Case iLSTCitas
            Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
            Call pControlBotones
            tabTab1(0).Tab = 0
    End Select
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Select Case intIndex
        Case iLSTCitas
            'grdDBGrid1(0).Columns(iCOLTipCama).Value = Trim(Mid(grdDBGrid1(0).Columns(iCOLTipCama).Text, 3, (Len(grdDBGrid1(0).Columns(iCOLTipCama).CellText))))
            'como lo hemos modificado, se activar� el boton de grabar
            tlbToolbar1.Buttons(4).Enabled = True
    End Select
End Sub
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If intIndex = 3 Then
    SSDBCombo1(0).Enabled = True
  End If
End Sub
Private Sub pCargarCamas(bReserva As Boolean, sDepartamento As String)

    Dim sStmSql As String
    Dim sRow    As String
    Dim Rs      As rdoResultset
    
    sStmSql = "SELECT AD1500.AD02CODDPTO, AD1500.AD13CODTIPOCAMA, AD1300.AD13DESTIPOCAMA, AD1500.AD15CODCAMA, "
    sStmSql = sStmSql & "AD1500.AD14CODESTCAMA, AD1400.AD14DESESTCAMA, AD1500.AD37CODESTADO2CAMA, "
    sStmSql = sStmSql & "AD3700.AD37DESESTADO2CAMA, "
    sStmSql = sStmSql & "CI2200.CI22NOMBRE || ' ' ||CI2200.CI22PRIAPEL || ' ' ||CI2200.CI22SEGAPEL PACIENTE, "
    sStmSql = sStmSql & "CI3000.CI30DESSEXO, GCFN06(AD1500.AD15CODCAMA) CAMA "
    sStmSql = sStmSql & "FROM AD1500, AD1400, AD1300, AD3700, CI2200, AD0700, CI3000 "
    sStmSql = sStmSql & "WHERE AD1500.AD13CODTIPOCAMA = AD1300.AD13CODTIPOCAMA "
    sStmSql = sStmSql & "  AND AD1500.AD14CODESTCAMA = AD1400.AD14CODESTCAMA"
    sStmSql = sStmSql & "  AND AD1500.AD37CODESTADO2CAMA = AD3700.AD37CODESTADO2CAMA (+)"
    sStmSql = sStmSql & "  AND AD1500.AD02CODDPTO = " & sDepartamento
    sStmSql = sStmSql & "  AND AD1500.AD07CODPROCESO = AD0700.AD07CODPROCESO (+) "
    sStmSql = sStmSql & "  AND AD0700.CI21CODPERSONA = CI2200.CI21CODPERSONA (+) "
    sStmSql = sStmSql & "  AND CI2200.CI30CODSEXO = CI3000.CI30CODSEXO (+) "
    If bReserva Then
        sStmSql = sStmSql & "  AND AD1500.AD14CODESTCAMA <> " & iEstado_DeBaja & " "
        sStmSql = sStmSql & "  AND AD1500.AD37CODESTADO2CAMA IS NULL "
    ElseIf bAsignar Then
        sStmSql = sStmSql & "  AND (AD1500.AD14CODESTCAMA = " & iEstado_Libre & ")"
    End If
        sStmSql = sStmSql & "ORDER BY CAMA"
    grdDBGrid1(iLSTCamas).RemoveAll
    Set Rs = objApp.rdoConnect.OpenResultset(sStmSql)
    While Rs.EOF = False
        sRow = Rs("AD02CODDPTO") & Chr(9)
        sRow = sRow & Rs("AD13CODTIPOCAMA") & "   " & Rs("AD13DESTIPOCAMA") & Chr(9)
        sRow = sRow & Rs("AD15CODCAMA") & Chr(9)
        sRow = sRow & Rs("CAMA") & Chr(9)
        sRow = sRow & Rs("AD14CODESTCAMA") & "   " & Rs("AD14DESESTCAMA") & Chr(9)
        sRow = sRow & Rs("AD37CODESTADO2CAMA") & "   " & Rs("AD37DESESTADO2CAMA") & Chr(9)
        sRow = sRow & Rs("PACIENTE") & Chr(9)
        sRow = sRow & Rs("CI30DESSEXO")
        grdDBGrid1(iLSTCamas).AddItem sRow
        Rs.MoveNext
    Wend
    Rs.Close
    Set Rs = Nothing
End Sub
Private Sub pCargaCombo(Index As Integer)
   
    Dim sql As String
    Dim Rs  As rdoResultset
    Dim qry As rdoQuery
    
    Select Case Index
        Case 1  'Dptos.
            sql = "SELECT AD02CODDPTO, "
            sql = sql & "AD02DESDPTO "
            sql = sql & "FROM AD0200 "
            sql = sql & "WHERE AD32CODTIPODPTO =  " & iDpto_Tipo_Planta
            SSDBCombo1(1).RemoveAll
            Set Rs = objApp.rdoConnect.OpenResultset(sql)
            While Rs.EOF = False
                SSDBCombo1(1).AddItem Rs("AD02CODDPTO").Value & ";" & Rs("AD02DESDPTO").Value
                SSDBCombo1(1).Text = Rs("AD02DESDPTO").Value
                SSDBCombo1(1).MoveNext
                Rs.MoveNext
            Wend
            Rs.Close
            Set Rs = Nothing
    End Select
    SSDBCombo1(1).Text = ""
End Sub
Private Sub pControlBotones()
'CONTROLA LA ACTIVACION O DESACTIVACION DE BOTONES
    If grdDBGrid1(0).Rows = 0 Then 'SI no hay ingresos pendientes
        cmdReservarCama.Enabled = False
        cmdAnularReserva.Enabled = False
        cmdAsignarCama.Enabled = False
        cmddesasignarCama.Enabled = False
        cmdActPlanif.Enabled = False
        cmdcamasis.Enabled = False
    Else 'si hay ingresos pendientes
        cmdActPlanif.Enabled = True
        cmdcamasis.Enabled = True
        'si no hay cama reservada
        If grdDBGrid1(0).Columns(iCOLCamaReser).Value = "" Then
            'si no hay cama asisgnada
            If grdDBGrid1(0).Columns(iCOLCamaAsig).Value = "" Then
                cmdReservarCama.Enabled = True
                cmdAnularReserva.Enabled = False
                'si hay proceso/asistencia
                If grdDBGrid1(0).Columns(iCOLProceso).Value <> "" And grdDBGrid1(0).Columns(iCOLAsistencia).Value <> "" Then
                    cmdAsignarCama.Enabled = True
                Else
                     cmdAsignarCama.Enabled = False
                End If
                cmddesasignarCama.Enabled = False
            Else 'hay cama reservada pero no asignada
                cmdReservarCama.Enabled = False
                cmdAnularReserva.Enabled = False
                cmdAsignarCama.Enabled = False
                cmddesasignarCama.Enabled = True
            End If
        Else 'hay cama reservada
            If grdDBGrid1(0).Columns(iCOLCamaAsig).Value = "" Then 'no hay cama asignada
                cmdReservarCama.Enabled = False
                cmdAnularReserva.Enabled = True
                If grdDBGrid1(0).Columns(iCOLProceso).Value <> "" And grdDBGrid1(0).Columns(iCOLAsistencia).Value <> "" Then
                    cmdAsignarCama.Enabled = True
                Else
                     cmdAsignarCama.Enabled = False
                End If
                cmddesasignarCama.Enabled = False
            Else 'hay cama rservada y cama asignada
                cmdReservarCama.Enabled = False
                cmdAnularReserva.Enabled = False
                cmdAsignarCama.Enabled = False
                cmddesasignarCama.Enabled = True
            End If
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(iCOLCodPaciente).Value
        IdPersona1.ReadPersona
    End If
End Sub
Private Function fblnVerbotones() As Boolean
Dim sql As String
Dim qry As rdoQuery
Dim Rs As rdoResultset

    sql = "SELECT COUNT(*) FROM AD0300 WHERE SG02COD = ? AND "
    sql = sql & "AD02CODDPTO IN (3,216,311,319)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = objSecurity.strUser
    Set Rs = qry.OpenResultset()
    If Rs(0).Value = 0 Then
       cmdcamasis.Visible = False
       cmdActPlanif.Visible = False
       cmdAnularReserva.Visible = False
       cmdAsignarCama.Visible = False
       cmdCitaManualRapida.Visible = False
       cmddesasignarCama.Visible = False
       cmdReservarCama.Visible = False
       fblnVerbotones = False
    Else
       fblnVerbotones = True
    End If
    Rs.Close
    qry.Close
End Function
Private Sub pImprimirEtiquetas(CodPersona As String)
Dim fila As Integer
Dim col As Integer
Dim intPosX As Integer
Dim intPosY As Integer
Dim intIncX As Integer
Dim intIncY As Integer
Dim X As Integer
Dim strNombre As String
Dim strFecha As String
Dim sql As String
Dim Rs As rdoResultset
Dim qry As rdoQuery

intPosX = 75
intPosY = 335
intIncX = 3900
intIncY = 1433
vs.StartDoc
vs.CurrentX = intPosX
vs.CurrentY = intPosY

'datos previos
sql = "SELECT CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI22FECNACIM FROM CI2200 WHERE "
sql = sql & "CI21CODPERSONA = ? "
'
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = CodPersona
Set Rs = qry.OpenResultset()
If Rs.EOF Then
    MsgBox "Persona Fisica inexistente", vbOKOnly
    Exit Sub
End If
If Not IsNull(Rs!CI22SEGAPEL) Then
    strNombre = Rs!CI22PRIAPEL & " " & Rs!CI22SEGAPEL & ", " & Rs!CI22NOMBRE
Else
  strNombre = Rs!CI22PRIAPEL & ", " & Rs!CI22NOMBRE
End If
If Len(strNombre) > 40 Then _
strNombre = Rs!CI22PRIAPEL & " " & Rs!CI22SEGAPEL & Chr$(13) & Rs!CI22NOMBRE

'fecha de nacimiento

If Not IsNull(Rs!CI22FECNACIM) Then
    strFecha = Format(Rs!CI22FECNACIM, "dd/mm/yyyy")
Else
    strFecha = ""
End If
Rs.Close
qry.Close
For fila = 1 To 11
    For col = 1 To 3
      X = vs.CurrentX
      vs.FontSize = 12
      vs.Text = "Hist: "
      vs.FontBold = True
      vs.Text = IdPersona1.Historia
      vs.FontBold = False
      vs.Text = Chr$(13)
      vs.CurrentX = X
      vs.FontSize = 8
      vs.Text = strNombre
      vs.Text = Chr$(13)
      vs.CurrentX = X
      vs.Text = strFecha
      X = X + intIncX
      vs.CurrentX = X
      vs.CurrentY = intPosY
    Next col
    vs.CurrentX = intPosX
    intPosY = intPosY + intIncY
    vs.CurrentY = intPosY
Next fila
vs.EndDoc
vs.PrintDoc
End Sub
