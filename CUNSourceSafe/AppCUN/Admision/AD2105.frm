VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Begin VB.Form frmActuacionesPlanificadasNW 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION. Admitir Paciente"
   ClientHeight    =   8340
   ClientLeft      =   -45
   ClientTop       =   525
   ClientWidth     =   11955
   HelpContextID   =   2
   Icon            =   "AD2105.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11955
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11955
      _ExtentX        =   21087
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdCambiosAsis 
      Caption         =   "Camb. Asistencia"
      Height          =   375
      Index           =   0
      Left            =   6249
      TabIndex        =   19
      Top             =   7560
      Width           =   1350
   End
   Begin VB.CommandButton cmdCitasPacientes 
      Caption         =   "Citas Pacientes"
      Height          =   360
      Left            =   8976
      TabIndex        =   18
      Top             =   7560
      Width           =   1545
   End
   Begin Crystal.CrystalReport crtReport1 
      Left            =   15
      Top             =   7950
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdHojas 
      Caption         =   "Prog. Paciente"
      Height          =   375
      Left            =   7680
      TabIndex        =   17
      Top             =   7560
      Width           =   1215
   End
   Begin Threed.SSCommand cmdPedir 
      Height          =   375
      Left            =   10605
      TabIndex        =   16
      Top             =   7545
      Width           =   1260
      _Version        =   65536
      _ExtentX        =   2222
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "Pedir Pruebas"
   End
   Begin VB.CommandButton cmdvisionGlobal 
      Caption         =   "Vision Global"
      Height          =   375
      Index           =   0
      Left            =   5013
      TabIndex        =   15
      Top             =   7560
      Width           =   1155
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Asociar Proceso/Asistencia"
      Height          =   375
      Index           =   2
      Left            =   135
      TabIndex        =   14
      Top             =   7560
      Width           =   2100
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Quitar Proceso"
      Height          =   375
      Index           =   3
      Left            =   2316
      TabIndex        =   13
      Top             =   7560
      Width           =   1200
   End
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "Quitar Asistencia"
      Height          =   375
      Index           =   4
      Left            =   3597
      TabIndex        =   12
      Top             =   7560
      Width           =   1335
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pacientes"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   11715
      Begin TabDlg.SSTab tabTab1 
         Height          =   1665
         Index           =   0
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   2937
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "&Detalle"
         TabPicture(0)   =   "AD2105.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(8)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "dtcDateCombo1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "cboDBCombo1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "IdPersona1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cmdFacturar"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "cmdHojaFiliacion"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cmdCitaManual"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "&Tabla"
         TabPicture(1)   =   "AD2105.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.CommandButton cmdCitaManual 
            Caption         =   "Citas Manuales"
            Height          =   495
            Left            =   7080
            TabIndex        =   22
            Top             =   240
            Width           =   1080
         End
         Begin VB.CommandButton cmdHojaFiliacion 
            Caption         =   "Hoja Filiacion"
            Height          =   495
            Left            =   9600
            TabIndex        =   21
            Top             =   240
            Width           =   1080
         End
         Begin VB.CommandButton cmdFacturar 
            Caption         =   "Facturacion"
            Height          =   495
            Left            =   8340
            TabIndex        =   20
            Top             =   240
            Width           =   1080
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1455
            Left            =   120
            TabIndex        =   1
            Top             =   120
            Width           =   10665
            _ExtentX        =   18812
            _ExtentY        =   2566
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   5
            Left            =   10410
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Sexo|Sexo de la Persona"
            Top             =   900
            Visible         =   0   'False
            Width           =   150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1425
            Index           =   0
            Left            =   -74820
            TabIndex        =   2
            TabStop         =   0   'False
            Top             =   120
            Width           =   10965
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19341
            _ExtentY        =   2514
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboDBCombo1 
            DataField       =   "CI30CODSEXO"
            Height          =   330
            HelpContextID   =   30110
            Index           =   0
            Left            =   10590
            TabIndex        =   8
            Tag             =   "Sexo|Sexo"
            Top             =   900
            Visible         =   0   'False
            Width           =   495
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldDelimiter  =   """"
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1296
            Columns(0).Caption=   "C�digo"
            Columns(0).Alignment=   1
            Columns(0).CaptionAlignment=   1
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   6482
            Columns(1).Caption=   "Provincia"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   873
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI22FECNACIM"
            Height          =   330
            Index           =   0
            Left            =   10380
            TabIndex        =   10
            Tag             =   "Fecha Nacimiento|Fecha Nacimiento"
            Top             =   240
            Visible         =   0   'False
            Width           =   735
            _Version        =   65537
            _ExtentX        =   1296
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1000/1/1"
            MaxDate         =   "3000/12/31"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Sexo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   10410
            TabIndex        =   11
            Top             =   690
            Visible         =   0   'False
            Width           =   435
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Planificadas"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4635
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   2760
      Width           =   11715
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4125
         HelpContextID   =   2
         Index           =   1
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   11415
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20135
         _ExtentY        =   7276
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   8055
      Width           =   11955
      _ExtentX        =   21087
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
   Begin VB.Menu MnuPedir 
      Caption         =   "Pedir"
      Visible         =   0   'False
      Begin VB.Menu mnuPedirAsP 
         Caption         =   "Asociar Proceso"
      End
      Begin VB.Menu mnuPedirAsPA 
         Caption         =   "Asociar Proceso/Asistencia"
      End
   End
End
Attribute VB_Name = "frmActuacionesPlanificadasNW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdCitaManual_Click()
  'Llama frmCitaManualFI pasando el paciente activo
  Dim vntdata(2) As Variant
  
  'Comprueba si hay un paciente activo
  vntdata(1) = IdPersona1.Text
  If vntdata(1) = "" Then
    MsgBox "No hay ningun paciente selecionado.", vbExclamation, Me.Caption
    Exit Sub
  End If
  
  'Llama frmCitaManualFI pasando el paciente activo y el nombre del form llamador
  vntdata(2) = "AD2105"
  Call objSecurity.LaunchProcess("CI1147", vntdata)
  
  'Refresca los datos
  objWinInfo.DataRefresh
End Sub

Private Sub cmdCitasPacientes_Click()
  'Llama frmCitasPacientes pasando el paciente activo
  Dim vntdata(1) As Variant

  'Comprueba si hay un paciente activo
  vntdata(1) = IdPersona1.Text
  If vntdata(1) = "" Then
    MsgBox "No hay ningun paciente selecionado.", vbExclamation, Me.Caption
    Exit Sub
  End If
  
  'Llama frmCitasPacientes pasando el paciente activo
  'La clase CWLauncher ya est� preparada para recibir el c�digo persona como
  'primer elemento de vntData
  Call objSecurity.LaunchProcess("CI0202", vntdata(1))
  
  'Refresca los datos
  objWinInfo.DataRefresh
End Sub

Private Sub cmdFacturar_Click()
Dim vntdata
vntdata = IdPersona1.Text
Call objSecurity.LaunchProcess("FA0101", vntdata)
End Sub

Private Sub cmdHojaFiliacion_Click()
Dim vntdata
If Trim(IdPersona1.Text) <> "" Then
    vntdata = Val(IdPersona1.Text)
    'Call objPipe.PipeSet("CODPERS", vntdata)
    Call objSecurity.LaunchProcess("CI4017", vntdata)

Else
    Call objSecurity.LaunchProcess("CI4017")
End If
If objPipe.PipeExist("CODPER") Then
    Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)
    IdPersona1.Text = objPipe.PipeGet("CODPER")
    IdPersona1_LostFocus
    objPipe.PipeRemove ("CODPER")
End If
End Sub

Private Sub cmdHojas_Click()
    Call pImprimir(3)
End Sub

Private Sub cmdPedir_Click()
  Dim vntA As Variant
  Dim vntdata(6) As Variant
  Dim intR As Integer

  If IdPersona1.Text = "" Then
    Call objError.SetError(cwCodeMsg, "Debe seleccionar un paciente", vntA)
    vntA = objError.Raise
    Exit Sub
  End If
  If grdDBGrid1(1).SelBookmarks.Count <> 0 Then
    If grdDBGrid1(1).SelBookmarks.Count > 1 Then
        MsgBox "Seleccione una sola Actuaci�n", vbInformation
    Else
        'Si la Actuacion no tiene proceso/asistencia
        If grdDBGrid1(1).Columns(10).Value = "" And grdDBGrid1(1).Columns(11).Value = "" Then
        ' No asociar ni proceso ni asistencia
          ReDim arData(1 To 1) As Variant
          arData(1) = IdPersona1.Text
          Call objSecurity.LaunchProcess("PRA103", arData())
        End If
        'Si la Actuacion tiene proceso y no Asistencia
        If grdDBGrid1(1).Columns(10).Value <> "" And grdDBGrid1(1).Columns(11).Value = "" Then
         '   Asociar proceso
          ReDim arData(1 To 1) As Variant
          arData(1) = grdDBGrid1(1).Columns(10).Value
          Call objSecurity.LaunchProcess("PRA102", arData())
        End If
         If grdDBGrid1(1).Columns(10).Value <> "" And grdDBGrid1(1).Columns(11).Value <> "" Then
            Me.PopupMenu MnuPedir, vbPopupMenuLeftAlign, cmdPedir.Left, 6510
         End If
    End If
  Else
    ' No asociar ni proceso ni asistencia
      ReDim arData(1 To 1) As Variant
      arData(1) = IdPersona1.Text
      Call objSecurity.LaunchProcess("PRA103", arData())
  End If

  objWinInfo.DataRefresh
 pFormatearGrid

End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Call objApp.AddCtrl(TypeName(IdPersona1))    'nuevo
  Call IdPersona1.BeginControl(objApp, objGen) 'nuevo
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  With objWinInfo.objDoc
    .cwPRJ = "ADMISION"
    .cwMOD = "M�dulo Maestro-detalle"
    .cwAUT = "A.R."
    .cwDAT = "25-09-97"
    .cwDES = "Mantenimiento Actuaciones Planificadas"
    .cwUPD = "25-09-97  - A.R. - xxxxxxxx"
    .cwEVT = "Descripci�n del evento xxxxx"
  End With
  
  With objMasterInfo
    .strName = "Pacientes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .strTable = "CI2200"
    .strWhere = " CI2200.AD34CODESTADO = 1 "
    .intAllowance = cwAllowReadOnly
    .blnHasMaint = True
    
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
    
    Call .FormCreateFilterWhere("CI2200", "Tabla De Pacientes")
    Call .FormAddFilterWhere("CI2200", "CI21PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere("CI2200", "CI21SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere("CI2200", "CI21NOMBRE", "Nombre", cwNumeric)
    Call .FormAddFilterWhere("CI2200", "CI22NUMHISTORIA", "N�mero de Historia", cwNumeric)
   
    Call .FormAddFilterOrder("CI2200", "CI21PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder("CI2200", "CI21SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder("CI2200", "CI21NOMBRE", "Nombre")
    Call .FormAddFilterOrder("CI2200", "CI22NUMHISTORIA", "N�mero de Historia")
    Call .objPrinter.Add("filiacion2", "Hoja de Filiacion")
    Call .objPrinter.Add("PROPAC1", "Programa del Paciente")
    Call .objPrinter.Add("ConfirmarFiliacion", "Hoja de Datos")
  End With
  
  With objMultiInfo
    .strName = "Actuaciones Planificadas"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intAllowance = cwAllowReadOnly
    .strTable = "CI0121J"
    .intCursorSize = -1
    Call .FormAddOrderField("CI01FECCONCERT", cwAscending)
    Call .FormAddRelation("CI21CODPERSONA", IdPersona1)              'nuevo
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)

    Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Cita Concertada", "CI01FECCONCERT", cwString)
    Call .GridAddColumn(objMultiInfo, "Fecha Plan.", "PR04FECPLANIFIC", cwString)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n", "PR01DESCORTA", cwString)
    Call .GridAddColumn(objMultiInfo, "Dpto. Realizador", "AD02DESDPAG", cwString)
    'JRC 23-11-1998
    Call .GridAddColumn(objMultiInfo, "Recurso", "AG11DESRECURSO", cwString)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n Planificada", "PR04NUMACTPLAN", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Proceso", "AD07CODPROCESO", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Asistencia", "AD01CODASISTENCI", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "N�Actuaci�n Pedida", "PR03NUMACTPEDI", cwNumeric)
    'EFS: DEPARTAMENTO Y DOCTOR SOLICITANTE
    Call .GridAddColumn(objMultiInfo, "Dpto Solicit", "DPTOSOLICIT", cwString)
    Call .GridAddColumn(objMultiInfo, "Dr. Solicit", "DOCTORSOLICT", cwString)
    Call .GridAddColumn(objMultiInfo, "Observaciones Peticion", "PR09DESOBSERVAC", cwString)
    Call .FormCreateInfo(objMasterInfo)
        
   ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
   ' .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
 
    grdDBGrid1(1).Columns(0).Visible = False
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(9).Visible = False
    grdDBGrid1(1).Columns(12).Visible = False

    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(5)).blnInGrid = False
    .CtrlGetInfo(cboDBCombo1(0)).blnInGrid = False
    .CtrlGetInfo(dtcDateCombo1(0)).blnInGrid = False

    .CtrlGetInfo(cboDBCombo1(0)).strSQL = "SELECT CI30CODSEXO,CI30DESSEXO FROM CI3000 ORDER BY CI30CODSEXO"
    Call .CtrlCreateLinked(.CtrlGetInfo(cboDBCombo1(0)), "CI30CODSEXO", "SELECT CI30CODSEXO,CI30DESSEXO FROM CI3000 WHERE CI30CODSEXO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboDBCombo1(0)), txtText1(5), "CI30DESSEXO")
      
    .CtrlGetInfo(IdPersona1).blnForeign = True 'nuevo
    .CtrlGetInfo(IdPersona1).blnInFind = True  'nuevo
    IdPersona1.ReadPersona                     'nuevo

    Call .WinRegister
    Call .WinStabilize
  End With
    If lngPerCodVi <> 0 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)
        IdPersona1.Text = lngPerCodVi
        IdPersona1_LostFocus
    End If
IdPersona1.blnSearchButton = False
pFormatearGrid

 
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  gintProcedencia = 0
  Call IdPersona1.EndControl        'nuevo
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
  
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de Id. Paciente
' -----------------------------------------------

Private Sub IdPersona1_Change()

  Call objWinInfo.CtrlDataChange
  If IdPersona1.Text = "" Then
    cmdPedir.Enabled = False
    grdDBGrid1(1).Refresh
    
  Else
      cmdPedir.Enabled = True
  End If
 
'
End Sub

Private Sub IdPersona1_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
  If IdPersona1.Text <> "" And IdPersona1.Apellido1 <> "" Then
    cmdPedir.Enabled = True
  Else
    cmdPedir.Enabled = False
  End If
End Sub

Private Sub mnuPedirAsP_Click()
'   Asociar proceso
 Dim arData(1 To 1) As Variant
 arData(1) = grdDBGrid1(1).Columns(10).Value
 Call objSecurity.LaunchProcess("PRA102", arData())
End Sub

Private Sub mnuPedirAsPA_Click()
'Asociar Proceso y Asistencia
  Dim arData(1 To 2) As Variant
  arData(1) = grdDBGrid1(1).Columns(10).Value
  arData(2) = grdDBGrid1(1).Columns(11).Value
  Call objSecurity.LaunchProcess("PRA101", arData())
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Select Case strCtrl
    Case "IdPersona1"                  'nuevo
      IdPersona1.SearchPersona         'nuevo
  End Select

End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
Dim vntdatos() As Variant
  'Mantenimiento de personas
  If strFormName = "Pacientes" Then
    ReDim vntdatos(1)
    vntdatos(1) = IdPersona1.Text
    'personas f�sicas (desde 14/6/2000)
'    Call objSecurity.LaunchProcess("CI3017", vntdatos)
    'personas f�sicas (hasta 13/6/2000)
    Call objSecurity.LaunchProcess("CI2017", vntdatos)
    'PGM: Si el codigo de persona a sido pasado lo asigno a idpersona y actualizo el grid
     If objPipe.PipeExist("CODPER") Then
      Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)
      IdPersona1.Text = objPipe.PipeGet("CODPER")
      IdPersona1_LostFocus
      objPipe.PipeRemove ("CODPER")
     End If
    pFormatearGrid
  End If
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
  Dim vntA As Variant
  If strFormName = "Actuaciones Planificadas" Then
    ' Comprobar si el paciente tiene generado el N� Historia
    'If IdPersona1.Historia = "" And IdPersona1.Text <> "" Then 'nuevo
    '  Call objError.SetError(cwCodeQuery, "Este Paciente no tiene asignado un N�Historia." & "�Desea generarle Historia?", vntA)
    '  vntA = objError.Raise
    '  If vntA = 6 Then
    '    Dim rs As rdoResultset
    '    Dim strSQL As String
    '    Set rs = objApp.rdoConnect.OpenResultset("SELECT CI22NUMHISTORIA_S.NEXTVAL FROM DUAL", rdOpenKeyset)
    '    strSQL = "UPDATE CI2200 SET CI22NUMHISTORIA = "
    '    strSQL = strSQL & rs.rdoColumns(0).Value
    '    strSQL = strSQL & " WHERE CI22NUMHISTORIA IS NULL"
    '    strSQL = strSQL & " AND CI21CODPERSONA="
    '    strSQL = strSQL & IdPersona1.Text 'nuevo
    '    objApp.rdoConnect.Execute strSQL, 64
    '    'objApp.rdoConnect.Execute "Commit", 64
    '    rs.Close
    '    Set rs = Nothing
    '    Call objWinInfo.FormChangeActive(fraFrame1(0), True, True)
    '    objWinInfo.DataRefresh
    '    Call objWinInfo.FormChangeActive(fraFrame1(1), True, True)
    '  End If
    'End If
  End If
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(3), IdPersona1.Text) 'nuevo
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
 
 grdDBGrid1(1).Columns(4).Width = 1650
  grdDBGrid1(1).Columns(6).Width = 1800
  grdDBGrid1(1).Columns(7).Width = 2070
  grdDBGrid1(1).Columns(8).Width = 3300
  grdDBGrid1(1).Columns(10).Width = 1100
  grdDBGrid1(1).Columns(11).Width = 1000
  pFormatearGrid

End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
Dim intReport As Integer

With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        Select Case intReport
            Case 1
                Call pImprimir(2)
            Case 2
                Call pImprimir(3)
            Case 3
                Call pImprimir(4)
        End Select
      End If
End With
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim blnPA As Boolean 'Indica si todos tienen proceso/asist asociados
Dim intI As Integer
'  objWinInfo.DataRefresh
   If btnButton.Index = 16 Then
    Call objWinInfo.FormChangeActive(tabTab1(0), False, True)
    Call IdPersona1.Buscar
  ElseIf btnButton.Index = 30 And gintProcedencia = 2 Then
  grdDBGrid1(1).MoveFirst
  For intI = 1 To grdDBGrid1(1).Rows
'  While grdDBGrid1(1).Columns(4).Value <> ""
   If grdDBGrid1(1).Columns(10).Value = "" Or grdDBGrid1(1).Columns(11).Value = "" Then
     blnPA = True
   End If
  grdDBGrid1(1).MoveNext
'  Wend
  Next
  If blnPA Then
    MsgBox "Debe Asociar un proceso/asistencia a todas las Actuaciones", vbCritical
  Else
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
    
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command_Click
' -----------------------------------------------
Private Sub cmdCommand1_Click(intIndex As Integer)
  Dim vntA As Variant
  Dim vntdata(7) As Variant
  Dim intI As Integer
  Dim strSQL As String
  Dim SQL As String
  Dim qry As rdoQuery
  Dim rdoConsulta As rdoQuery
  Dim rs As rdoResultset
  Dim strActuaciones As String
  Dim strNumPlanific As String
  
  ' Comprobar que se ha seleccionado una actuaci�n si se desea asociar un proceso o una asistencia
  If grdDBGrid1(1).SelBookmarks.Count = 0 Then
    Call objError.SetError(cwCodeMsg, "Debe Seleccionar Alguna Actuaci�n Planificada", vntA)
    vntA = objError.Raise
    Exit Sub
  End If
 'COMPROBAMOS QUE LA ACTUACION PERTENECE A LA PERSONA
  strSQL = "SELECT COUNT(*) FROM PR0400 WHERE CI21CODPERSONA = ? AND "
  strSQL = strSQL & " PR04NUMACTPLAN = ?"
  Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
      rdoConsulta(0) = IdPersona1.Text
      rdoConsulta(1) = grdDBGrid1(1).Columns("Actuaci�n Planificada").Value
  Set rs = rdoConsulta.OpenResultset()
  If rs(0) = 0 Then
    MsgBox "AVISE A INFORMATICA", vbCritical
    Exit Sub
  End If
  
    Select Case intIndex

    Case 2 'asociar P/A
      If fblnHistoria And fblnDatosCorrectos(strActuaciones, strNumPlanific) Then
        vntdata(1) = IdPersona1.Text 'CODIGO DE PERSONA
        vntdata(2) = strActuaciones 'Numeros de Actuaci�n pedidas
        If Trim(grdDBGrid1(1).Columns("Proceso").Value) <> "" Then
            vntdata(3) = grdDBGrid1(1).Columns("Proceso").Value
        Else
            vntdata(3) = 0
        End If
        vntdata(4) = IdPersona1.Historia
        strSQL = "SELECT AD02CODDPTO, SG02COD FROM PR0800,PR0900"
        strSQL = strSQL & " WHERE PR0800.PR03NUMACTPEDI = ? AND "
        strSQL = strSQL & " PR0800.PR09NUMPETICION = PR0900.PR09NUMPETICION"
        Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = grdDBGrid1(1).Columns("N�Actuaci�n Pedida").Value
        Set rs = rdoConsulta.OpenResultset()
        If Not rs.EOF Then
            vntdata(5) = rs(0) & " " & grdDBGrid1(1).Columns("Dpto Solicit").Value
            If rs(0) <> STR(constDPTO_URGENCIAS) Then
                vntdata(6) = rs(1) & " " & grdDBGrid1(1).Columns("Dr. Solicit").Value
            Else
                vntdata(6) = constUSUARIO_PASTRANA & " " & constDESUSUARIO_PASTRANA
            End If
        Else
            vntdata(5) = 0
            vntdata(6) = 0
        End If
        vntdata(7) = strNumPlanific 'Numeros de Actuacion planificada
        Screen.MousePointer = vbHourglass
        Call objSecurity.LaunchProcess("AD1020", vntdata)
        Screen.MousePointer = vbHourglass
        objWinInfo.DataRefresh
        Screen.MousePointer = vbDefault
      End If
      Case 3 'Quitar proceso
        objApp.BeginTrans
        For intI = 0 To grdDBGrid1(1).SelBookmarks.Count - 1
          grdDBGrid1(1).Bookmark = grdDBGrid1(1).SelBookmarks.item(intI)
          If grdDBGrid1(1).Columns(11).CellValue(grdDBGrid1(1).SelBookmarks(intI)) <> "" Then
            Call objError.SetError(cwCodeMsg, "No puede quitar Proceso : Existe Alguna Actuaci�n de las Seleccionadas que tiene Asignada una Asistencia", vntA)
            vntA = objError.Raise
            objApp.RollbackTrans
            Exit Sub
          End If
          SQL = "SELECT PR04FECENTRCOLA FROM PR0400 WHERE PR04NUMACTPLAN = ?"
          Set qry = objApp.rdoConnect.CreateQuery("", SQL)
              qry(0) = grdDBGrid1(1).Columns(9).Value
          Set rs = qry.OpenResultset()
          If Not IsNull(rs(0)) Then
            Call objError.SetError(cwCodeMsg, "No puede quitar Proceso : Exite Alguna Actuacion con Fecha de Entrada en Cola", vntA)
            vntA = objError.Raise
            Exit Sub
            objApp.RollbackTrans
          End If
          rs.Close
          qry.Close
          SQL = "SELECT COUNT (*) FROM PR0400 WHERE PR03NUMACTPEDI = ? AND PR37CODESTADO IN (3,4)"
          Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = grdDBGrid1(1).Columns(12).Value
          Set rs = qry.OpenResultset()
          If rs(0) = 0 Then
            strSQL = "UPDATE PR0800 SET AD07CODPROCESO = NULL"
            strSQL = strSQL & " WHERE PR0800.PR03NUMACTPEDI =? "
            strSQL = strSQL & " AND PR0800.PR08NUMSECUENCIA = 1"
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = grdDBGrid1(1).Columns(12).Value
            rdoConsulta.Execute
          End If
          rs.Close
          qry.Close
          strSQL = "UPDATE PR0400 SET AD07CODPROCESO = NULL"
          strSQL = strSQL & " WHERE PR0400.PR04NUMACTPLAN =? "
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = grdDBGrid1(1).Columns(9).Value
            rdoConsulta.Execute
        Next intI
        objApp.CommitTrans
        objWinInfo.DataRefresh
        pFormatearGrid
      Case 4 'QUITAR ASISTENCIA
        For intI = 0 To grdDBGrid1(1).SelBookmarks.Count - 1
            grdDBGrid1(1).Bookmark = grdDBGrid1(1).SelBookmarks.item(intI)
            SQL = "SELECT PR04FECENTRCOLA FROM PR0400 WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
                qry(0) = grdDBGrid1(1).Columns(9).Value
            Set rs = qry.OpenResultset()
            If Not IsNull(rs(0)) Then
              Call objError.SetError(cwCodeMsg, "No puede quitar Proceso : Exite Alguna Actuacion con Fecha de Entrada en Cola", vntA)
              vntA = objError.Raise
              Exit Sub
              objApp.RollbackTrans
            End If
          rs.Close
          qry.Close
            strSQL = "UPDATE PR0400 SET AD01CODASISTENCI = NULL"
            strSQL = strSQL & " WHERE PR0400.PR04NUMACTPLAN =? "
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = grdDBGrid1(1).Columns(9).Value
            rdoConsulta.Execute
            SQL = "SELECT COUNT (*) FROM PR0400 WHERE PR03NUMACTPEDI = ? AND PR37CODESTADO IN (3,4)"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
              qry(0) = grdDBGrid1(1).Columns(12).Value
            Set rs = qry.OpenResultset()
            If rs(0) = 0 Then
                strSQL = "UPDATE PR0800 SET AD01CODASISTENCI = NULL"
                strSQL = strSQL & " WHERE PR0800.PR03NUMACTPEDI =? "
                strSQL = strSQL & " AND PR0800.PR08NUMSECUENCIA= 1"
                Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
                rdoConsulta(0) = grdDBGrid1(1).Columns(12).Value
                rdoConsulta.Execute
            End If
            rs.Close
            qry.Close
        Next intI
        objWinInfo.DataRefresh
        pFormatearGrid
    End Select

End Sub

'''''Private Sub cmdCommand2_Click(Index As Integer)
'''''  Dim vntA As Variant
'''''  Dim vntData(4) As Variant
'''''  Dim intR As Integer
'''''
'''''  If IdPersona1.Text = "" Then
'''''    Call objError.SetError(cwCodeMsg, "Debe seleccionar un paciente", vntA)
'''''    vntA = objError.Raise
'''''  End If
'''''''  If grdDBGrid1(1).SelBookmarks.Count <> 0 Then
'''''''    If grdDBGrid1(1).SelBookmarks.Count > 1 Then
'''''''        MsgBox "Seleccione una sola Actuaci�n", vbInformation
'''''''    Else
'''''''        If grdDBGrid1(1).Columns(9) <> "" Then
'''''''
'''''''        End If
'''''''    End If
'''''''  End If
'''''  'Petici�n de pruebas
'''''  'vntdata(1): Numero de grupo
'''''  'vntdata(2): Numero de peticion
'''''  'vntdata(3): Codigo de persona
'''''  'vntdata(4): Aplicacion desde donde se llama
'''''  vntData(1) = 0
'''''  vntData(2) = 0
'''''  vntData(3) = IdPersona1.Text
'''''  vntData(4) = "ADMISIONES"
'''''  Call objSecurity.LaunchProcess("PR0152", vntData)
'''''  objWinInfo.DataRefresh
'''''End Sub
Private Sub cmdVisionGlobal_Click(Index As Integer)
Dim vntdata() As Variant

If IdPersona1.Text = "" Then
    MsgBox "Seleccione una persona", vbCritical
    Exit Sub
Else
    cmdVisionGlobal(0).Enabled = False
    ReDim vntdata(1 To 2) As Variant
    vntdata(1) = IdPersona1.Text
    vntdata(2) = 2
    Call objSecurity.LaunchProcess("HC02", vntdata())
    Me.MousePointer = vbDefault
    cmdVisionGlobal(0).Enabled = True
End If
End Sub
Private Sub cmdCambiosAsis_Click(Index As Integer)
''    'Cambios de asistencia antiguo
''    Dim vntData(2) As Variant
''    'Pasa el c�digo de persona para mostrarla despu�s
''    vntData(1) = IdPersona1.Text
''    vntData(2) = grdDBGrid1(1).Columns(11).Value
''    Call objSecurity.LaunchProcess("AD0115", vntData)
    
    'Cambios de asistencia nuevo
    Dim vntdata(1) As Variant
    'Pasa el c�digo de persona para mostrarla despu�s
    vntdata(1) = IdPersona1.Text
    Call objSecurity.LaunchProcess("AD3000", vntdata)
End Sub
Private Function fblnHistoria()
Dim vntA As Variant
Dim strSQL As String
Dim strInsert As String
Dim rs As rdoResultset
Dim rdoConsulta As rdoQuery
Dim QyI As rdoQuery
Dim qry As rdoQuery
Dim strFecha As String


fblnHistoria = True
If IdPersona1.Historia = "" Then 'And IdPersona1.Text <> "" Then 'nuevo
          Call objError.SetError(cwCodeQuery, "Este Paciente no tiene asignado un N�Historia." & "�Desea generarle Historia?", vntA)
          vntA = objError.Raise
          If vntA = 6 Then
            Err = 0
            objApp.BeginTrans
            Set rs = objApp.rdoConnect.OpenResultset("SELECT CI22NUMHISTORIA_S.NEXTVAL FROM DUAL", rdOpenKeyset)
            strSQL = "UPDATE CI2200 SET CI22NUMHISTORIA =? "
            strSQL = strSQL & " WHERE CI22NUMHISTORIA IS NULL"
            strSQL = strSQL & " AND CI21CODPERSONA=?"
            
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = rs.rdoColumns(0).Value
            rdoConsulta(1) = IdPersona1.Text
            rdoConsulta.Execute
            rdoConsulta.Close
            
            IdPersona1.Historia = rs.rdoColumns(0).Value
             'updateamos la tabla del proceso
            strSQL = "UPDATE AD0700 SET CI22NUMHISTORIA = ? WHERE CI21CODPERSONA = ? "
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
            rdoConsulta(0) = rs.rdoColumns(0).Value
            rdoConsulta(1) = IdPersona1.Text
            rdoConsulta.Execute
            rdoConsulta.Close
            
            
            rs.Close
            Set rs = Nothing
           
            
            
            strFecha = strFechaHora_Sistema
            
            'Insertar en tabla de Archivo una nueva historia (24/02/2000)
            strInsert = "INSERT INTO AR0400 (CI22NUMHISTORIA, AR00CODTIPOLOGIA, " & _
                        "AR04FECINICIO, AR06CODESTADO) " & _
                        "VALUES (?, ?, to_date(?,'DD/MM/YYYY HH24:MI'), ?)"
            Set QyI = objApp.rdoConnect.CreateQuery("", strInsert)
                QyI(0) = IdPersona1.Historia
                QyI(1) = "01"
                QyI(2) = Format(strFecha, "dd/mm/yyyy hh:mm")
                QyI(3) = "A"
            QyI.Execute
            QyI.Close
            'BUSCAMOS EL DPTO SOLICITANTE'
            strSQL = "SELECT AD02CODDPTO FROM PR0800,PR0900"
            strSQL = strSQL & " WHERE PR0800.PR03NUMACTPEDI = ? AND "
            strSQL = strSQL & " PR0800.PR09NUMPETICION = PR0900.PR09NUMPETICION"
            Set qry = objApp.rdoConnect.CreateQuery("", strSQL)
                qry(0) = grdDBGrid1(1).Columns("N�Actuaci�n Pedida").Value
            Set rs = qry.OpenResultset()
            
            strInsert = "INSERT INTO AR0700 (AR04FECINICIO,CI22NUMHISTORIA,AD02CODDPTO, " & _
            " AR07FECMVTO,AR00CODTIPOLOGIA,SG02COD) VALUES (to_date(?,'DD/MM/YYYY HH24:MI'),?,?,to_date(?,'DD/MM/YYYY HH24:MI'),'01',?)"
                Set QyI = objApp.rdoConnect.CreateQuery("", strInsert)
                QyI(0) = Format(strFecha, "dd/mm/yyyy hh:mm")
                QyI(1) = IdPersona1.Historia
                If Not rs.EOF Then
                    QyI(2) = rs(0)
                Else
                    MsgBox "No se encuentra el departemento solicitante", vbCritical
                    objApp.RollbackTrans
                    fblnHistoria = False
                    Exit Function
                End If
                QyI(3) = Format(strFecha, "dd/mm/yyyy hh:mm")
                QyI(4) = objSecurity.strUser
            QyI.Execute
            QyI.Close
            rs.Close
            qry.Close
            If Err > 0 Then
                objApp.RollbackTrans
                MsgBox "ERROR: No se ha creado la historia"
                fblnHistoria = False
                Exit Function
            Else
                objApp.CommitTrans
            End If

    
          Else
            Call objError.SetError(cwCodeMsg, "El n�mero de Historia es obligatorio para poder asignar proceso.", vntA)
            vntA = objError.Raise
            fblnHistoria = False
            Exit Function
          End If
          
        End If
End Function
Private Function fblnDatosCorrectos(strActuaciones As String, strNumPlanific As String) As Boolean
Dim intI As Integer
Dim vntA As Variant
Dim strProceso As String
Dim strAsistencia As String


strActuaciones = ""
strNumPlanific = ""
strProceso = grdDBGrid1(1).Columns(10).Value
strAsistencia = grdDBGrid1(1).Columns(11).Value
fblnDatosCorrectos = True
For intI = 0 To grdDBGrid1(1).SelBookmarks.Count - 1
    strActuaciones = strActuaciones & grdDBGrid1(1).Columns("N�Actuaci�n Pedida").CellValue(grdDBGrid1(1).SelBookmarks(intI)) & ","
    strNumPlanific = strNumPlanific & grdDBGrid1(1).Columns("Actuaci�n Planificada").CellValue(grdDBGrid1(1).SelBookmarks(intI)) & ","
    If grdDBGrid1(1).Columns(10).CellValue(grdDBGrid1(1).SelBookmarks(intI)) <> strProceso Or _
    grdDBGrid1(1).Columns(11).CellValue(grdDBGrid1(1).SelBookmarks(intI)) <> strAsistencia Then
        MsgBox "Todas las Actuaciones deben tener los mismos Procesos/Asistencia"
        fblnDatosCorrectos = False
        Exit Function
    End If
    If grdDBGrid1(1).Columns(11).CellValue(grdDBGrid1(1).SelBookmarks(intI)) <> "" Then
        MsgBox "Alguna Actuaci�n seleccionada ya tiene proceso/asistencia"
        fblnDatosCorrectos = False
        Exit Function
    End If
Next intI
strActuaciones = Left(strActuaciones, Len(strActuaciones) - 1)
strNumPlanific = Left(strNumPlanific, Len(strNumPlanific) - 1)
End Function
Private Sub pImprimir(intR As Integer)
Dim strWhere As String
'*************************************************
'*** IMPRIME LAS HOJA DE FILIACION, EL PROGRAMA DEL PACIENTE,
'*** Y LA HOJA DE DATOS DEPENDIENDO DEL VALOR DE intR
'*** 1.- Hoja de filiacion y hoja del paciente
'*** 2.- Hoja de filiacion
'*** 3.- Programa del paciente
'*** 4.- hoja de datos
'*****************
If grdDBGrid1(1).Columns("Asistencia").Value = "" Or grdDBGrid1(1).Columns("Proceso").Value = "" Then
    MsgBox "Seleccione una Actuaci�n con Proceso/Asistencia", vbInformation
    Exit Sub
End If
'hoja de filiacion
Select Case intR
    Case 1, 2, 3 'hoja de filiacion
        If intR = 1 Or intR = 2 Then
            strWhere = "({Ad0823j.AD01CODASISTENCI}=" & grdDBGrid1(1).Columns("Asistencia").Value & _
            "AND {Ad0823j.AD07CODPROCESO}=" & grdDBGrid1(1).Columns("Proceso").Value & ")"
    
            crtReport1.ReportFileName = objApp.strReportsPath & "filiacion2.rpt"
            
            With crtReport1
                .PrinterCopies = 1
                .Destination = crptToPrinter
                .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
                .Destination = crptToPrinter
                .Connect = objApp.rdoConnect.Connect
                .DiscardSavedData = True
                Me.MousePointer = vbHourglass
                .Action = 1
                Me.MousePointer = vbDefault
            End With
        End If
        If intR = 1 Or intR = 3 Then
            'Programa del paciente
            strWhere = "{PR0460J.CI21CODPERSONA} = " & IdPersona1.Text
            crtReport1.ReportFileName = objApp.strReportsPath & "PROPAC1.rpt"
            With crtReport1
                .PrinterCopies = 1
                .Destination = crptToPrinter
                .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
                .Destination = crptToPrinter
                .Connect = objApp.rdoConnect.Connect
                .DiscardSavedData = True
                Me.MousePointer = vbHourglass
                .Action = 1
                Me.MousePointer = vbDefault
            End With
        End If
    Case 4
        strWhere = "({Ad0823j.AD01CODASISTENCI}=" & grdDBGrid1(1).Columns("Asistencia").Value & _
               " AND {Ad0823j.AD07CODPROCESO}=" & grdDBGrid1(1).Columns("Proceso").Value & ")"
        crtReport1.ReportFileName = objApp.strReportsPath & "ConfirmarFiliacion.rpt"
        With crtReport1
          .PrinterCopies = 1
          .Destination = crptToPrinter
          .SelectionFormula = objGen.ReplaceStr(strWhere, "#", Chr(34), 0)
          .Destination = crptToPrinter
          .Connect = objApp.rdoConnect.Connect
          .DiscardSavedData = True
           Me.MousePointer = vbHourglass
          .Action = 1
           Me.MousePointer = vbDefault
    End With
  End Select
  End Sub
Private Sub pFormatearGrid()
grdDBGrid1(1).Columns("Fecha Plan.").Width = 1006
grdDBGrid1(1).Columns("Recurso").Width = 1036
grdDBGrid1(1).Columns("Dr. Solicit").Width = 1036
grdDBGrid1(1).Columns("Dpto Solicit").Width = 1485
grdDBGrid1(1).Columns("Dpto. Realizador").Width = 1350
grdDBGrid1(1).Columns(7).Width = 2065
End Sub
