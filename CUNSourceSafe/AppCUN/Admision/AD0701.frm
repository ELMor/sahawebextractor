VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmEscogerProceso 
   Caption         =   "Procesos"
   ClientHeight    =   2310
   ClientLeft      =   4110
   ClientTop       =   2955
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2310
   ScaleWidth      =   4680
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   1800
      Width           =   1815
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Seleccionar Proceso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1365
      Index           =   1
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   4335
      Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   3
         Top             =   720
         Width           =   3735
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         DefColWidth     =   2
         RowHeight       =   423
         Columns(0).Width=   6324
         Columns(0).Caption=   "PROCESO"
         Columns(0).Name =   "PROCESO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblaCT 
         Caption         =   "Proceso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   1
         Top             =   480
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frmEscogerProceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdaceptar_Click()
Dim mensaje As String
    If SSDBCombo1(0).Text <> "" Then
        Call objPipe.PipeSet("Proceso", SSDBCombo1(0).Text)
        Unload Me
    Else
        mensaje = MsgBox("No ha seleccionado ning�n proceso", vbInformation, "Aviso")
    End If
End Sub

Private Sub Form_Load()
    Dim sqlstr As String
    Dim rsta As rdoResultset
    Dim rdoConsulta As rdoQuery
  Rem BIND VARIABLES * * * * * * * * * * * * * * * * * * * * * * * * * * *
  Rem =====================================================================
'    sqlstr = "SELECT AD07CODPROCESO FROM AD0800 WHERE AD01CODASISTENCI =" & _
'             objPipe.PipeGet("CodigoAsistencia") & " AND AD0800.AD34CODESTADO = 1 "
'    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    sqlstr = "SELECT AD07CODPROCESO FROM AD0800 WHERE AD01CODASISTENCI =?" & _
             " AND AD0800.AD34CODESTADO = 1 "
    Set rdoConsulta = objApp.rdoConnect.CreateQuery("", sqlstr)
    rdoConsulta(0) = objPipe.PipeGet("CodigoAsistencia")
    Set rsta = rdoConsulta.OpenResultset(sqlstr)
    While Not rsta.EOF
        SSDBCombo1(0).AddItem rsta("AD07CODPROCESO").Value
        SSDBCombo1(0).MoveNext
    rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
    
End Sub

Private Sub SSDBCombo1_CloseUp(Index As Integer)
    SSDBCombo1(0).Text = SSDBCombo1(0).Columns(0).Value
End Sub

