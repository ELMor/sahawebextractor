VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Begin VB.Form frmGestionCamas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gestion de Camas"
   ClientHeight    =   7650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8865
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7650
   ScaleWidth      =   8865
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmNuevoEstado 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Nuevo Estado"
      ForeColor       =   &H00800000&
      Height          =   795
      Left            =   60
      TabIndex        =   16
      Top             =   6780
      Width           =   8775
      Begin MSMask.MaskEdBox medHora 
         Height          =   315
         Left            =   3420
         TabIndex        =   20
         Top             =   300
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   556
         _Version        =   327681
         MaxLength       =   5
         Format          =   "hh:mm"
         Mask            =   "##:##"
         PromptChar      =   "_"
      End
      Begin VB.CommandButton cmdAceptar 
         Caption         =   "&Aceptar"
         Height          =   435
         Left            =   7620
         TabIndex        =   19
         Top             =   240
         Width           =   855
      End
      Begin SSDataWidgets_B.SSDBCombo cboNuevoEstado 
         Height          =   315
         Left            =   5100
         TabIndex        =   18
         Top             =   300
         Width           =   2175
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "COD"
         Columns(0).Name =   "COD"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "DES"
         Columns(1).Name =   "DES"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3836
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecNuevo 
         Height          =   315
         Left            =   660
         TabIndex        =   17
         Top             =   300
         Width           =   1815
         _Version        =   65537
         _ExtentX        =   3201
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
      End
      Begin VB.Label Label4 
         Caption         =   "Estado:"
         Height          =   255
         Left            =   4500
         TabIndex        =   23
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label3 
         Caption         =   "Hora:"
         Height          =   315
         Left            =   2940
         TabIndex        =   22
         Top             =   360
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha:"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   675
      End
   End
   Begin SSDataWidgets_B.SSDBDropDown ddEstados 
      Height          =   1095
      Left            =   2820
      TabIndex        =   15
      Top             =   2460
      Width           =   1875
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "COD"
      Columns(0).Name =   "COD"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Caption=   "DESCRIPCION"
      Columns(1).Name =   "DESCRIPCION"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3307
      _ExtentY        =   1931
      _StockProps     =   77
      BackColor       =   12632256
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   7980
      TabIndex        =   14
      Top             =   180
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cama"
      ForeColor       =   &H00C00000&
      Height          =   1215
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   7815
      Begin VB.CommandButton cmdGuardarCama 
         Caption         =   "Guadar"
         Height          =   375
         Left            =   6840
         TabIndex        =   12
         Top             =   720
         Width           =   855
      End
      Begin VB.CommandButton cmdNuevaCama 
         Caption         =   "Nueva"
         Height          =   375
         Left            =   6840
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox txtCama 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   840
         TabIndex        =   2
         Top             =   300
         Width           =   675
      End
      Begin SSDataWidgets_B.SSDBCombo cboTipoCama 
         DataField       =   "AD13CODTIPOCAMA"
         Height          =   285
         Left            =   2040
         TabIndex        =   3
         Tag             =   "C�digo Tipo de Cama|Tipo de Cama"
         Top             =   300
         Width           =   1890
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "IN0103.frx":0000
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "IN0103.frx":001C
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripcion"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3334
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSDataWidgets_B.SSDBCombo cboDpto 
         DataField       =   "AD02CODDPTO"
         Height          =   285
         Left            =   4440
         TabIndex        =   5
         Tag             =   "C�digo Departamento|Departamento"
         Top             =   300
         Width           =   2250
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         stylesets.count =   2
         stylesets(0).Name=   "Activo"
         stylesets(0).BackColor=   16777215
         stylesets(0).Picture=   "IN0103.frx":0038
         stylesets(1).Name=   "Inactivo"
         stylesets(1).BackColor=   255
         stylesets(1).Picture=   "IN0103.frx":0054
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   3387
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "des"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3969
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "Column 1"
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecIni 
         DataField       =   "AD15FECINICIO"
         Height          =   330
         Left            =   840
         TabIndex        =   7
         Tag             =   "Fecha Inicio|Fecha Inicio"
         Top             =   720
         Width           =   1845
         _Version        =   65537
         _ExtentX        =   3254
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1000/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecfin 
         DataField       =   "AD15FECFIN"
         Height          =   330
         Left            =   4440
         TabIndex        =   8
         Tag             =   "Fecha Fin|Fecha Fin"
         Top             =   720
         Width           =   1845
         _Version        =   65537
         _ExtentX        =   3254
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         MinDate         =   "1000/1/1"
         MaxDate         =   "3000/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Inicio:"
         Height          =   195
         Index           =   5
         Left            =   180
         TabIndex        =   10
         Top             =   840
         Width           =   420
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Fin:"
         Height          =   195
         Index           =   4
         Left            =   4080
         TabIndex        =   9
         Top             =   840
         Width           =   255
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Dpto:"
         Height          =   195
         Index           =   1
         Left            =   4020
         TabIndex        =   6
         Top             =   360
         Width           =   390
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Tipo:"
         Height          =   195
         Index           =   2
         Left            =   1680
         TabIndex        =   4
         Top             =   360
         Width           =   360
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "N� cama:"
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   300
         Width           =   660
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdEstadosCamas 
      Height          =   5325
      Left            =   0
      TabIndex        =   13
      Top             =   1320
      Width           =   8835
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   15584
      _ExtentY        =   9393
      _StockProps     =   79
      Caption         =   "Estados"
      ForeColor       =   8388608
      BackColor       =   12632256
   End
End
Attribute VB_Name = "frmGestionCamas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const Const_Dpto_Tipo_Planta = 3
Dim strCamaAnt As String
Dim arEstadoAnt() As typeEstado
Dim arEstadoNue() As typeEstado
Dim strCama As String
Dim blnNuevaCama As Boolean
Dim blnCambios As Boolean
Dim blnDatosCargados As Boolean
Private Function fNombre(strnombre As String) As String
Select Case strnombre
Case "txtCama"
    fNombre = "N�mero de cama, "
Case "cboTipoCama"
    fNombre = "Tipo de cama, "
Case "cboDpto"
    fNombre = "Departamento, "
Case "dcboFecIni"
fNombre = "Fecha de inicio, "
End Select

End Function
Private Function fComprobaciones() As String
Dim c As Control, strColor$, msg$
Dim strMensaje As String
    On Error Resume Next
        For Each c In Me
            strColor = c.BackColor
            If strColor = objApp.objUserColor.lngMandatory Then
              If TypeOf c Is TextBox Or TypeOf c Is SSDBCombo Then
                If Trim(c.Text) = "" Or c.Text = "__:__" Then
                strMensaje = strMensaje & fNombre(c.Name)
                End If
               End If
              If TypeOf c Is SSDateCombo Then
               If c.Date = "" Then
                strMensaje = strMensaje & fNombre(c.Name)
                End If
              End If
            End If
        Next
        fComprobaciones = Left(strMensaje, Len(strMensaje) - 2)
End Function

Private Sub pNuevoEstado()
Dim sql As String
Dim rs As rdoResultset
medHora.Text = Format$(fAhora, "hh:mm")
sql = "SELECT AD14CODESTCAMA,AD14DESESTCAMA FROM AD1400 WHERE AD14CODESTCAMA NOT IN(2,6)"
Set rs = objApp.rdoConnect.OpenResultset(sql)
Do While Not rs.EOF
    cboNuevoEstado.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
Loop
End Sub
Private Sub pFormatearGrid()
With grdEstadosCamas
    .Columns(0).Caption = "Fecha Cambio"
    .Columns(0).Width = 1400
    .Columns(1).Caption = "Estado Cama"
    .Columns(1).style = ssStyleComboBox
    .Columns(1).Width = 2000
    .Columns(2).Caption = "Paciente"
    .Columns(2).Width = 5000
End With
End Sub

Private Sub pCargarCombos()
Dim rs As rdoResultset
Dim sql As String
Dim qry As rdoQuery
cboTipoCama.RemoveAll
cboDpto.RemoveAll
sql = "SELECT AD02CODDPTO,AD02DESDPTO FROM AD0200 WHERE AD32CODTIPODPTO=? ORDER BY AD02DESDPTO"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = Const_Dpto_Tipo_Planta
Set rs = qry.OpenResultset()
Do While Not rs.EOF
    cboDpto.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
Loop
    sql = "SELECT AD13CODTIPOCAMA,AD13DESTIPOCAMA FROM AD1300 ORDER BY AD13DESTIPOCAMA"
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    Do While Not rs.EOF
        cboTipoCama.AddItem rs(0) & Chr(9) & rs(1)
        rs.MoveNext
    Loop

End Sub

Private Sub cmdGuardarEst_Click()

End Sub

Private Sub cboDpto_Change()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If
End Sub

Private Sub cboDpto_Click()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If
End Sub



Private Sub cboNuevoEstado_Change()
If cboNuevoEstado.Text <> "" And txtCama.Text <> "" Then
    cmdAceptar.Enabled = True
Else
    cmdAceptar.Enabled = False
End If

End Sub

Private Sub cboNuevoEstado_Click()
If cboNuevoEstado.Text <> "" And txtCama.Text <> "" Then
    cmdAceptar.Enabled = True
Else
    cmdAceptar.Enabled = False
End If
End Sub

Private Sub cboTipoCama_Change()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If

End Sub

Private Sub cboTipoCama_Click()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If

End Sub

Private Sub cmdAceptar_Click()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim strError As String
Dim i As Integer
If cboNuevoEstado.Text <> "" And txtCama.Text <> "" Then
    sql = "SELECT COUNT(*) FROM AD1500 WHERE AD15CODCAMA=GCFN07(?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtCama.Text
    Set rs = qry.OpenResultset
    If rs(0) <> 1 Then
        MsgBox "No existe ninguna cama con ese n�mero", vbInformation, Me.Caption
        cmdAceptar.Enabled = False
        Exit Sub
    End If
End If
'esta soluci�n no me convence pero....
sql = "SELECT COUNT(*) FROM AD1600 WHERE AD16FECCAMBIO=TO_DATE(?,'DD/MM/YYYY HH24:MI') AND AD15CODCAMA=GCFN07(?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = dcboFecNuevo.Text & " " & medHora.Text
qry(1) = txtCama.Text
Set rs = qry.OpenResultset
If rs(0) > 0 Then
    MsgBox "Ya se ha realizado un cambio de estado en este minuto", vbExclamation, Me.Caption
    Exit Sub
End If
On Error GoTo canceltrans
Err = 0
objApp.BeginTrans
sql = "SELECT COUNT(*) FROM AD1600 WHERE AD15CODCAMA=GCFN07(?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = txtCama.Text
Set rs = qry.OpenResultset
If rs(0) <> 0 Then
    sql = "UPDATE AD1600 SET AD16FECFIN=SYSDATE WHERE AD15CODCAMA=GCFN07(?) AND AD16FECFIN is NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtCama.Text
    qry.Execute
     If qry.RowsAffected <> 1 Then
            strError = "No ha sido posible finalizar el estado anterior"
            GoTo canceltrans
        End If
    qry.Close
End If
sql = "INSERT INTO AD1600 (AD16FECCAMBIO,AD15CODCAMA,AD14CODESTCAMA) VALUES (TO_DATE(?,'DD/MM/YYYY HH24:MI'),GCFN07(?),?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = dcboFecNuevo.Text & " " & medHora.Text
qry(1) = txtCama.Text
qry(2) = cboNuevoEstado.Columns(0).Value
qry.Execute
 If qry.RowsAffected <> 1 Then
        strError = "No ha sido posible crear un nuevo estado de la cama"
        GoTo canceltrans
    End If
qry.Close
sql = "UPDATE AD1500 SET AD14CODESTCAMA=? WHERE AD15CODCAMA=GCFN07(?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = cboNuevoEstado.Columns(0).Value
qry(1) = txtCama.Text
qry.Execute
 If qry.RowsAffected <> 1 Then
        strError = "No ha sido posible actualizar el estado de la cama"
        GoTo canceltrans
    End If
qry.Close

objApp.CommitTrans
grdEstadosCamas.RemoveAll
Err = 0
On Error Resume Next
ReDim Preserve arEstadoNue(1 To UBound(arEstadoNue) + 1)
If Err <> 0 Then
    ReDim arEstadoNue(1 To 1)
    Err = 0
End If
With arEstadoNue(UBound(arEstadoNue))
            .strEstado = cboNuevoEstado.Columns(0).Value
            .strFecha = dcboFecNuevo.Text & " " & medHora.Text
            .strPaciente = ""
End With
For i = 1 To UBound(arEstadoNue)
    With arEstadoNue(i)
    grdEstadosCamas.AddItem (.strFecha & Chr(9) & .strEstado & Chr(9) & .strPaciente)
    End With
Next

For i = 1 To UBound(arEstadoAnt)
    With arEstadoAnt(i)
    grdEstadosCamas.AddItem (.strFecha & Chr(9) & .strEstado & Chr(9) & .strPaciente)
    End With
Next

Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError <> "" Then
        MsgBox strError, vbCritical, Me.Caption
    Else
        MsgBox Error, vbCritical, Me.Caption
    End If
End Sub

Private Sub cmdGuardarCama_Click()
Dim cancel As Integer
Call pGuardarcama(cancel)
If cancel <> 1 Then
Call pConsultar
End If
End Sub
Private Sub pGuardarcama(Optional cancel As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim strError As String
If blnNuevaCama Then
    sql = "SELECT COUNT(*) FROM AD1500 WHERE AD15CODCAMA=GCFN07(?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtCama.Text
    Set rs = qry.OpenResultset()
    If rs(0) > 0 Then
        MsgBox "Existe una cama con ese mismo n�mero", vbExclamation, Me.Caption
        cancel = 1
        Exit Sub
    End If
End If
If Trim(fComprobaciones) = "" Then
On Error GoTo canceltrans
Err = 0
objApp.BeginTrans

    If blnNuevaCama Then
        sql = "INSERT INTO AD1500 (AD15CODCAMA,AD02CODDPTO,AD13CODTIPOCAMA,"
        sql = sql & " AD14CODESTCAMA,AD15FECINICIO) "
        sql = sql & " VALUES (GCFN07(?),?,?,?,TO_DATE(?,'DD/MM/YYYY'))"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtCama.Text
        qry(1) = cboDpto.Columns(0).Value
        qry(2) = cboTipoCama.Columns(0).Value
        qry(3) = const_Cama_Libre
        If dcboFecfin.Text = "" Then
            qry(4) = dcboFecIni.Text
        Else
            qry(4) = Null
        End If
        qry.Execute
        If qry.RowsAffected <> 1 Then
            strError = "No ha sido posible crear una nueva cama"
            GoTo canceltrans
        End If
        qry.Close
        sql = "INSERT INTO AD1600 (AD16FECCAMBIO,AD15CODCAMA,AD14CODESTCAMA) VALUES (TO_DATE(?,'DD/MM/YYYY HH24:MI'),GCFN07(?),?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = Format$(fAhora, "dd/mm/yyyy hh:mm")
        qry(1) = txtCama.Text
        qry(2) = const_Cama_Libre
        qry.Execute
        If qry.RowsAffected <> 1 Then
            strError = "No ha sido posible crear una nuevo estado de la cama"
            GoTo canceltrans
        End If
        qry.Close
    Else 'modificaciones en una cama ya existente
        sql = "UPDATE AD1500 SET  AD02CODDPTO=?,AD13CODTIPOCAMA=?,"
        sql = sql & " AD15FECINICIO=TO_DATE(?,'DD/MM/YYYY'),AD15FECFIN=TO_DATE(?,'DD/MM/YYYY')"
        sql = sql & " WHERE AD15CODCAMA=GCFN07(?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = cboDpto.Columns(0).Value
        qry(1) = cboTipoCama.Columns(0).Value
        qry(2) = dcboFecIni.Text
        If dcboFecfin.Text <> "" Then
            qry(3) = dcboFecfin.Text
        Else
            qry(3) = Null
        End If
        qry(4) = txtCama.Text
        qry.Execute
        If qry.RowsAffected <> 1 Then
            strError = "No ha sido posible guardar las modificaciones de la cama"
            GoTo canceltrans
        End If
        qry.Close
    End If
    blnNuevaCama = False
    objApp.CommitTrans
    cmdGuardarCama.Enabled = False
Else 'no se han rellenado todos los campos obligatorios
    MsgBox "Los siguientes campos son obligatorios y est�n en blanco:" & Chr(13) & fComprobaciones, vbExclamation, Me.Caption
    cancel = 1
    Exit Sub
End If


Exit Sub
canceltrans:
    objApp.RollbackTrans
    If strError <> "" Then
        MsgBox strError, vbCritical, Me.Caption
    Else
        MsgBox Error, vbCritical, Me.Caption
    End If
End Sub

Private Sub cmdNuevaCama_Click()
txtCama.Text = ""
Call pLimpiarControles
cmdNuevaCama.Enabled = False
cmdGuardarCama.Enabled = False
blnNuevaCama = True
txtCama.SetFocus
End Sub
Private Sub pLimpiarControles()
blnDatosCargados = False
blnCambios = False
cboTipoCama.Text = ""
cboDpto.Text = ""
dcboFecfin.Text = ""
dcboFecIni.Text = Format$(fAhora, "dd/mm/yyyy")
grdEstadosCamas.RemoveAll
cboNuevoEstado.Text = ""
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub



Private Sub dcboFecfin_Change()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If

End Sub

Private Sub dcboFecfin_Click()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If

End Sub

Private Sub dcboFecIni_Change()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If

End Sub

Private Sub dcboFecIni_Click()
If blnDatosCargados Then
    blnCambios = True
    cmdGuardarCama.Enabled = True
End If

End Sub

Private Sub Form_Load()
If objPipe.PipeExist("IN0102_AD15CODCAMA") Then
    cmdGuardarCama.Enabled = False
    txtCama.Text = objPipe.PipeGet("IN0102_AD15CODCAMA")
    cmdGuardarCama.Enabled = True
    Call objPipe.PipeRemove("IN0102_AD15CODCAMA")
End If
txtCama.BackColor = objApp.objUserColor.lngMandatory
cboTipoCama.BackColor = objApp.objUserColor.lngMandatory
cboNuevoEstado.BackColor = objApp.objUserColor.lngMandatory
cboDpto.BackColor = objApp.objUserColor.lngMandatory
dcboFecIni.BackColor = objApp.objUserColor.lngMandatory
dcboFecNuevo.BackColor = objApp.objUserColor.lngMandatory
Call pFormatearGrid
Call pCargarDD
Call pCargarCombos
Call txtCama_LostFocus
Call pNuevoEstado
cmdGuardarCama.Enabled = False
cmdAceptar.Enabled = False
End Sub
Private Sub pCargarDD()
Dim sql As String
Dim rs As rdoResultset
sql = "SELECT AD14CODESTCAMA,AD14DESESTCAMA FROM AD1400 ORDER BY AD14DESESTCAMA"
Set rs = objApp.rdoConnect.OpenResultset(sql)
Do While Not rs.EOF
    ddEstados.AddItem rs(0) & Chr(9) & rs(1)
    rs.MoveNext
Loop
grdEstadosCamas.Columns("Estado Cama").DropDownHwnd = ddEstados.hWnd
End Sub
''"Fecha", "AD16FECCAMBIO
''"Estado Cama       "
''"Paciente"

Private Sub pConsultar()
Dim sql As String
Dim qry As rdoQuery
Dim rsCama As rdoResultset
Dim rsGrid As rdoResultset
Dim i As Integer
grdEstadosCamas.RemoveAll
Screen.MousePointer = vbHourglass
sql = "SELECT"
sql = sql & " AD1500.AD02CODDPTO,"
sql = sql & "AD1500.AD13CODTIPOCAMA,"
sql = sql & "AD15FECINICIO,"
sql = sql & "AD1500.AD15FECFIN"
sql = sql & " FROM AD1500"
sql = sql & " WHERE"
sql = sql & " AD1500.AD15CODCAMA=GCFN07(?)"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = txtCama.Text
Set rsCama = qry.OpenResultset
If Not rsCama.EOF Then
    If Not IsNull(rsCama!AD13CODTIPOCAMA) Then
        For i = 1 To cboTipoCama.Rows
            If i = 1 Then cboTipoCama.MoveFirst Else cboTipoCama.MoveNext
            If rsCama!AD13CODTIPOCAMA = cboTipoCama.Columns(0).Value Then
                cboTipoCama.Text = cboTipoCama.Columns(1).Value
                Exit For
            End If
        Next
    End If
    If Not IsNull(rsCama!AD02CODDPTO) Then
        For i = 1 To cboDpto.Rows
            If i = 1 Then cboDpto.MoveFirst Else cboDpto.MoveNext
            If rsCama!AD02CODDPTO = cboDpto.Columns(0).Value Then
                cboDpto.Text = cboDpto.Columns(1).Value
                Exit For
            End If
        Next
    End If
    If Not IsNull(rsCama!AD15FECINICIO) Then dcboFecIni.Text = Format(rsCama!AD15FECINICIO, "dd/mm/yyyy")
    If Not IsNull(rsCama!AD15FECFIN) Then dcboFecfin.Text = Format(rsCama!AD15FECFIN, "dd/mm/yyyy hh:mm")
    sql = "SELECT /*+ INDEX(AD1600 AD1601)*/" 'A PARTIR DE AQUI OBTENEMOS LOS CAMPOS DEL GRID
    sql = sql & " AD1600.AD16FECCAMBIO,"
    sql = sql & " AD1600.AD14CODESTCAMA,"
    sql = sql & " DECODE(NVL(CI22PRIAPEL,'0'),'0','',CI22PRIAPEL||' '||NVL(CI22SEGAPEL,'')||', '||CI22NOMBRE) PACIENTE"
    sql = sql & " FROM AD1500,AD1600,AD0100,CI2200"
    sql = sql & " WHERE"
    sql = sql & " AD1600.AD15CODCAMA=AD1500.AD15CODCAMA"
    sql = sql & " AND AD1600.AD01CODASISTENCI=AD0100.AD01CODASISTENCI(+)"
    sql = sql & " AND AD0100.CI22NUMHISTORIA=CI2200.CI22NUMHISTORIA(+)"
    sql = sql & " AND AD1500.AD15CODCAMA=GCFN07(?)"
    sql = sql & " ORDER BY AD1600.AD16FECCAMBIO DESC"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = txtCama.Text
    Set rsGrid = qry.OpenResultset()
    i = 0
    If Not rsGrid.EOF Then
        'Si est� ocupada no deja modificar el estado
        If rsGrid!AD14CODESTCAMA = const_Cama_Ocupada Then 'OCUPADA POR PACIENTE Then
            frmNuevoEstado.Enabled = False
        Else
            frmNuevoEstado.Enabled = True
        End If

    Do While Not rsGrid.EOF
        
        i = i + 1
        ReDim Preserve arEstadoAnt(1 To i)
        grdEstadosCamas.AddItem (rsGrid!AD16FECCAMBIO & Chr(9) & _
                                    rsGrid!AD14CODESTCAMA & Chr(9) & _
                                    rsGrid!PACIENTE)
        With arEstadoAnt(i)
            .strEstado = rsGrid!AD14CODESTCAMA
            .strFecha = rsGrid!AD16FECCAMBIO
            If Not IsNull(rsGrid!PACIENTE) Then
                .strPaciente = rsGrid!PACIENTE
            Else
                .strPaciente = ""
            End If
        End With
        rsGrid.MoveNext
    Loop
   Else ' LA CAMA EST� DE BAJA Y NO TIENE REGISTRO EN LA AD1600
    grdEstadosCamas.AddItem (rsCama!AD15FECINICIO & Chr(9) & _
                                    3 & Chr(9) & _
                                    "")
'            ReDim Preserve arEstadoAnt(1 To 1)
'            .strEstado = constcama_de_baja
'            .strFecha = rsCama!AD15FECINICIO
'            .strPaciente = ""
    End If
Else 'no se ha econtrado la cama
    MsgBox "No existe ninguna cama con ese n�mero", vbInformation, Me.Caption
    Screen.MousePointer = vbDefault
    Exit Sub
   

End If
blnDatosCargados = True
Screen.MousePointer = vbDefault
End Sub

Private Sub Form_QueryUnload(cancel As Integer, UnloadMode As Integer)
Dim intRes As Integer
 If cmdGuardarCama.Enabled = True Then
    intRes = MsgBox(" Desea guardar los cambios?", vbQuestion + vbYesNo, Me.Caption)
    If intRes = vbYes Then
        Call pGuardarcama(cancel)
        cmdGuardarCama.Enabled = False
    Else
        cmdGuardarCama.Enabled = False
        txtCama.Text = ""
    End If
End If

End Sub
Private Sub txtCama_Change()
Dim intRes As Integer
If cmdGuardarCama.Enabled = True Then
    intRes = MsgBox(" Desea guardar los cambios?", vbQuestion + vbYesNo, Me.Caption)
    If intRes = vbYes Then
        Call pGuardarcama
    End If
    cmdGuardarCama.Enabled = False
End If
Call pLimpiarControles
End Sub

Private Sub txtCama_GotFocus()
txtCama.SelStart = 0
txtCama.SelLength = Len(txtCama.Text)
End Sub

Private Sub txtCama_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then txtCama_LostFocus
End Sub

Private Sub txtCama_LostFocus()
Dim intRes As Integer
    If blnNuevaCama And txtCama.Text <> "" Then
        cmdGuardarCama.Enabled = True
        Exit Sub
    End If
    If txtCama.Text <> "" And strCamaAnt <> txtCama.Text Then
        Call pConsultar
        strCamaAnt = txtCama.Text
    End If
End Sub
