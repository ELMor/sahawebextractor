VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Const INWinIngresosPend                        As String = "IN0101"
Const INWinConsultaCamas                       As String = "IN0102"
Const INWinGestionCamas                        As String = "IN0103"

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objCW = mobjCW
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objSecurity = mobjCW.objSecurity
End Sub
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
    On Error Resume Next
    LaunchProcess = True
    Select Case strProcess
    Case INWinIngresosPend
        frmIngresosPend.Show vbModal
        Set frmIngresosPend = Nothing
    
    Case INWinConsultaCamas
        frmConsCamas.Show vbModal
        Set frmConsCamas = Nothing
    
    Case INWinGestionCamas
        frmGestionCamas.Show vbModal
        Set frmGestionCamas = Nothing
    Case Else
        LaunchProcess = False
    End Select
    Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  ' Redimensionar la matriz al n� de procesos existentes
  ReDim aProcess(1 To 4, 1 To 4) As Variant
      
  ' VENTANAS
  'aProcess(1, 1) = Nombre_ventana
  'aProcess(1, 2) = "Titulo....."
  'aProcess(1, 3) = True (Si aparece en menu principal)
  'aProcess(1, 4) = cwTypeWindow /ventana
  'aProcess(1, 4) = cwTypereport/ Listado
  'aProcess(1, 4) = cwTypeRoutina
        
  aProcess(1, 1) = INWinIngresosPend
  aProcess(1, 2) = "Ingresos Pendientes"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = INWinConsultaCamas
  aProcess(2, 2) = "Consulta de Camas"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
      
  aProcess(3, 1) = INWinGestionCamas
  aProcess(3, 2) = "Gestion de camas"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
  
  End Sub
  
