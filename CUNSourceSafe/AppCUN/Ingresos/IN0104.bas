Attribute VB_Name = "modConstantes"
Option Explicit

Public Const const_Cama_Libre = 1
Public Const const_Cama_Ocupada = 2
Public Const const_Cama_Reservada = 6


Public Const Const_Dpto_Ingresos = 1
Public Const Const_Dpto_Tipo_Planta = 3

Public Const constESTCITA_CITADA = 1
Public Const constESTCITA_ANULADA = 2
Public Const constESTCITA_RECITADA = 3
Public Const constESTCITA_PENDRECITAR = 4
Public Const constESTCITA_RESERVADA = 5

Public Const constACTIVIDAD_HOSPITALIZACION = 209

'Constantes de los estados de las actuaciones
Public Const constESTACT_PLANIFICADA = 1
Public Const constESTACT_CITADA = 2
Public Const constESTACT_REALIZANDOSE = 3
Public Const constESTACT_REALIZADA = 4
Public Const constESTACT_INFORMADA = 5
Public Const constESTACT_CANCELADA = 6

'Constantes para los tipos de actividades
Public Const constACTIV_CONSULTA = 201
Public Const constACTIV_PRUEBA = 203
Public Const constACTIV_INTERVENCION = 207
Public Const constACTIV_HOSPITALIZACION = 209
Public Const constACTIV_CONTCONSULTA = 214
Public Const constACTIV_INFORME = 215

'Constantes de los Departamentos
Public Const constDPTO_QUIROFANO = 213
Public Const constDPTO_ANESTESIA = 223
Public Const constDPTO_URGENCIAS = 216
Public Const constDPTO_RAYOS = 209
Public Const constDPTO_REHABILITACION = 211
Public Const constDPTO_ADMISION = 3

