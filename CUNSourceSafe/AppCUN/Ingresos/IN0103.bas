Attribute VB_Name = "modFuncionesGenerales"
Option Explicit

Public Function fAhora() As String
'Devuelve la fecha y hora actual obtenida del servidor
    Dim SQL$, rs As rdoResultset

    SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    fAhora = rs(0)
    rs.Close
End Function

Public Function bDBPacienteIngresado(sCodPaciente As String, sMensaje As String) As Boolean

    Dim sStmSql     As String
    Dim qrySelect   As rdoQuery
    Dim rdoSelect    As rdoResultset

    bDBPacienteIngresado = False
    sMensaje = ""
    sStmSql = "SELECT AD0100.CI21CODPERSONA, "
    sStmSql = sStmSql & "CI2200.CI22NOMBRE, "
    sStmSql = sStmSql & "CI2200.CI22PRIAPEL, "
    sStmSql = sStmSql & "CI2200.CI22SEGAPEL, "
    sStmSql = sStmSql & "GCFN06(AD1500.AD15CODCAMA) "
    sStmSql = sStmSql & "FROM AD0100, CI2200, AD1500 "
    sStmSql = sStmSql & "WHERE AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA "
    sStmSql = sStmSql & "AND  AD0100.ad01CODASISTENCI= AD1500.ad01CODASISTENCI "
    sStmSql = sStmSql & "AND AD0100.AD01FECFIN IS NULL "
    sStmSql = sStmSql & "AND  AD0100.CI21CODPERSONA = ? "

    Set qrySelect = objApp.rdoConnect.CreateQuery("", sStmSql)
        qrySelect(0) = Trim(sCodPaciente)
        qrySelect.OpenResultset
        Set rdoSelect = qrySelect.OpenResultset(sStmSql)
        If Not rdoSelect.EOF Then
            If Trim(sCodPaciente) = Trim(rdoSelect.rdoColumns(0)) Then
                bDBPacienteIngresado = True
                sMensaje = "El paciente:  " & Trim(rdoSelect.rdoColumns(1)) & " " & Trim(rdoSelect.rdoColumns(2)) & " " & Trim(rdoSelect.rdoColumns(3)) & Chr(13)
                sMensaje = sMensaje & "esta ya ingresado en la cama: " & Trim(rdoSelect.rdoColumns(4))
            Else
                bDBPacienteIngresado = False
                sMensaje = ""
            End If
        End If
End Function
Public Function fblnCitarModiAnular(strDptoPrueba As String, strTipoPrueba As String, strCodAct As String, blnCitar As Boolean) As Boolean
Dim SQL As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim vntDeptNoCitables
Dim vntPruebasCitables
Dim i As Integer
Dim K As Integer

  vntDeptNoCitables = Array("lun", "209", "170", "208", "250", "210", "11", "121", "206", "104", "106", "207", "120", "123", "113", "155", "172")
  vntPruebasCitables = Array("2248", "2473", "2474", "2466", "2477", "3196", "2788", "2793", "2802", "2803", _
    "2804", "3198", "2795", "2796", "2806", "3479")

fblnCitarModiAnular = True
If strTipoPrueba = constACTIV_PRUEBA Then  'en caso de ser una prueba
    For i = 1 To 16
        'miramos si pertenece a alguno de los dptos que se citan elllos mismos
        If strDptoPrueba = vntDeptNoCitables(i) Then
        'si es asi miramos si el usuario pertenece o no a ese departamento
          SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? AND AD03FECFIN IS NULL"
          Set qry = objApp.rdoConnect.CreateQuery("", SQL)
                qry(0) = vntDeptNoCitables(i)
                qry(1) = objSecurity.strUser
          Set rs = qry.OpenResultset()
          If rs(0) = 0 Then fblnCitarModiAnular = False
          rs.Close
          qry.Close
          If strDptoPrueba = constDPTO_RAYOS Then
                For K = 0 To 15
                      If strCodAct = vntPruebasCitables(K) Then fblnCitarModiAnular = True
                Next K
                If Not blnCitar Then fblnCitarModiAnular = True
          End If
        End If
    Next i
End If
  
  If strTipoPrueba = constACTIV_INTERVENCION Then
    'SI ES UNA INTERVENCION COMPROBAMOS QUE EL USUARIO PERTENECE A QUIROFANO
    SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? "
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = constDPTO_QUIROFANO
    qry(1) = objSecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly)
    If rs(0) = 0 Then fblnCitarModiAnular = False
    rs.Close
    qry.Close
  End If
  If strTipoPrueba = constACTIV_HOSPITALIZACION Then
    'SI ES UNA HOSPITALLIZACION MIRAMOS QUE EL USUARIO SE A DE ADMISION
      SQL = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? "
      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
      qry(0) = constDPTO_ADMISION
      qry(1) = objSecurity.strUser
      Set rs = qry.OpenResultset(rdOpenForwardOnly)
      If rs(0) = 0 Then fblnCitarModiAnular = False
      rs.Close
      qry.Close
   End If
End Function

