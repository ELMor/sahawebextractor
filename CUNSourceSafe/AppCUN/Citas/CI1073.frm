VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form FRMProcesos 
   Caption         =   "Procesos Abiertos del Paciente:"
   ClientHeight    =   5565
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10440
   LinkTopic       =   "Form1"
   ScaleHeight     =   5565
   ScaleWidth      =   10440
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox TxtCita 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      Height          =   330
      Index           =   0
      Left            =   1875
      TabIndex        =   4
      Top             =   540
      Width           =   1570
   End
   Begin VB.CommandButton cmdProcesos 
      Caption         =   "Asociar Proceso"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   690
      TabIndex        =   1
      Top             =   4965
      Width           =   1335
   End
   Begin VB.CommandButton cmdProcesos 
      Caption         =   "Salir"
      Height          =   375
      Index           =   1
      Left            =   8310
      TabIndex        =   0
      Top             =   4965
      Width           =   1335
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   2865
      Index           =   0
      Left            =   135
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1950
      Width           =   10185
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   5
      SelectTypeRow   =   1
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   5
      Columns(0).Width=   2275
      Columns(0).Caption=   "C�d.Proceso "
      Columns(0).Name =   "C�d.Proceso "
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5900
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Caption=   "Fecha Inicio"
      Columns(2).Name =   "Fecha Inicio"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Caption=   "Dpto. Resp."
      Columns(3).Name =   "Dpto. Resp."
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Caption=   "Dr. Respon."
      Columns(4).Name =   "Dr. Respon."
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   17965
      _ExtentY        =   5054
      _StockProps     =   79
      Caption         =   "Procesos existentes"
   End
   Begin idperson.IdPersona IdPersona1 
      Height          =   1335
      Left            =   195
      TabIndex        =   3
      Top             =   300
      Width           =   10050
      _ExtentX        =   17727
      _ExtentY        =   2355
      BackColor       =   12648384
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Datafield       =   "CI21CodPersona"
      MaxLength       =   7
      blnAvisos       =   0   'False
   End
End
Attribute VB_Name = "FRMProcesos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    'Constantes de Botones
    Const iBUTAsociar = 0
    Const iBUTSALIR = 1
    Const iBUTBuscar = 2
        
    'Constantes de cajas de texto
    Const iTXTHIS = 0           'Historia
    
    'Booleana para saber si se ha buscado por persona o por historia
    Dim bHistoria   As Boolean
    Dim bPersona    As Boolean
    Dim bDePF       As Boolean
    
    'datos que nos llegan
    Dim lngActPedi  As Long
    Dim strRecurso  As String
    Dim strDpto     As String
    
Private Sub pAsociarProceso()
  
  Dim vnta          As Variant
  Dim intI          As Integer
  Dim strSQL        As String
  Dim rdoConsulta   As rdoQuery
  Dim rs            As rdoResultset
  Dim intResp       As Integer
 
    ' Comprobar que se ha seleccionado una actuaci�n si se desea asociar un proceso o una asistencia
    If grdDBGrid1(0).SelBookmarks.Count = 0 Then
        Call objError.SetError(cwCodeMsg, "Debe Seleccionar Algun Proceso para asociarlo", vnta)
        vnta = objError.Raise
        Exit Sub
    Else
        For intI = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
            'Asociamos el proceso a la cita
            strSQL = "UPDATE PR0800 SET AD07CODPROCESO =? "
            strSQL = strSQL & " WHERE PR0800.PR03NUMACTPEDI =? "
            strSQL = strSQL & " AND PR0800.PR08NUMSECUENCIA = 1"
            
            Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
                rdoConsulta(0) = grdDBGrid1(0).Columns(0).CellValue(grdDBGrid1(0).SelBookmarks(intI))
                rdoConsulta(1) = lngActPedi
                rdoConsulta.Execute


            strSQL = "UPDATE PR0400 SET AD07CODPROCESO =? "
            strSQL = strSQL & " WHERE PR0400.PR03NUMACTPEDI =? "
                Set rdoConsulta = objApp.rdoConnect.CreateQuery("", strSQL)
                rdoConsulta(0) = grdDBGrid1(0).Columns(0).CellValue(grdDBGrid1(0).SelBookmarks(intI))
                rdoConsulta(1) = lngActPedi
                rdoConsulta.Execute
        Next intI
    End If
      intResp = MsgBox("Proceso asociado a la actuaci�n", vbInformation, "Proceso Asociado")
End Sub
Private Sub cmdProcesos_Click(Index As Integer)
 Select Case Index
        Case iBUTSALIR
            Unload Me
        Case iBUTAsociar
            Call pAsociarProceso
    End Select
End Sub

Private Sub Form_Load()

    'Consulta SQL
    Dim sql As String
    Dim qryCombos As rdoQuery
    Dim rstCombos As rdoResultset

    Screen.MousePointer = vbArrowHourglass
    Call objApp.AddCtrl(TypeName(IdPersona1))
    Call IdPersona1.BeginControl(objApp, objGen)
   
    'iniciamos las variables
    lngActPedi = objPipe.PipeGet("PR03NUMACTPEDI")
    strRecurso = objPipe.PipeGet("RECURSO")
    strDpto = objPipe.PipeGet("DPTO")
    lngCodPers = objPipe.PipeGet("CI21CODPERSONA")
    IdPersona1.BackColor = &HFFFF&
    'Pasamos el c�digo de persona si lo tenemos
    If lngCodPers <> 0 Then
        IdPersona1.Text = lngCodPers
        Call pCargarProceso
        bDePF = True
    Else
        IdPersona1.Text = ""
        bDePF = False
    End If

    
    'Iniciamos las booleanas
    bHistoria = False
    bPersona = False
    Screen.MousePointer = vbDefault
    IdPersona1.Enabled = False
    IdPersona1.blnSearchButton = False
End Sub

Private Sub grdDBGrid1_DblClick(Index As Integer)
    Call pAsociarProceso
End Sub

Private Sub IdPersona1_Change()
  Dim sStmSql     As String
    Dim qryPersona  As rdoQuery
    Dim rstHistoria As rdoResultset
    If bPersona Then
        bPersona = False
        Exit Sub
    End If
    If IdPersona1.Text <> "" Then
        'Buscaremos el n�mero de Hist�ria por el c�digo de Persona
        sStmSql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = ? "
        Set qryPersona = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryPersona(0) = IdPersona1.Text
        Set rstHistoria = qryPersona.OpenResultset()
        If Not IsNull(rstHistoria(0)) And Not rstHistoria.EOF Then
            TxtCita(iTXTHIS) = rstHistoria(0)
        Else
            bHistoria = True
            TxtCita(iTXTHIS) = ""
            bHistoria = False
        End If
        rstHistoria.Close
        qryPersona.Close
    Else
        bHistoria = True
        TxtCita(iTXTHIS) = ""
        bHistoria = False
    End If
    If TxtCita(iTXTHIS) <> "" Then
         cmdProcesos(iBUTAsociar).Enabled = True
         cmdProcesos(iBUTSALIR).Enabled = True
    Else
         cmdProcesos(iBUTAsociar).Enabled = False
         cmdProcesos(iBUTSALIR).Enabled = True
    End If
End Sub

Private Sub IdPersona1_LostFocus()
 If IdPersona1.Text <> "" And IdPersona1.historia = "" Then
        MsgBox "Persona no valida para concertar una cita. " & Chr(13) & " Seleccione a un paciente con n�mero de historia " & Chr(13) & " o complete los datos (Personas F�sicas)", vbInformation
        IdPersona1.Text = ""
        TxtCita(iTXTHIS) = ""
        IdPersona1.SetFocus
        
    End If
    
End Sub

Private Sub TxtCita_Change(Index As Integer)
    
    Dim sStmSql     As String
    Dim qryPersona  As rdoQuery
    Dim rstHistoria As rdoResultset
    
    If bHistoria Then
        bHistoria = False
        Exit Sub
    End If
    'Buscaremos el c�digo de persona por el n�mero de Hist�ria
    If TxtCita(iTXTHIS) <> "" Then
        sStmSql = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA = ? "
        Set qryPersona = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryPersona(0) = TxtCita(iTXTHIS)
        Set rstHistoria = qryPersona.OpenResultset()
        If Not IsNull(rstHistoria(0)) And Not rstHistoria.EOF Then
            IdPersona1.Text = rstHistoria(0)
        Else
            bPersona = True
            IdPersona1.Text = ""
            bPersona = False
        End If
        rstHistoria.Close
        qryPersona.Close
    Else
        bPersona = True
        IdPersona1.Text = ""
        bPersona = False
    End If
    If TxtCita(iTXTHIS) <> "" Then
         cmdProcesos(iBUTAsociar).Enabled = True
         cmdProcesos(iBUTSALIR).Enabled = True
    Else
         cmdProcesos(iBUTAsociar).Enabled = False
         cmdProcesos(iBUTSALIR).Enabled = True
    End If
        
End Sub

Private Sub TxtCita_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If (Shift = 1 And KeyCode = 45) Or (Shift = 1 And KeyCode = 46) Or (Shift = 2 And KeyCode = 45) Then
        KeyCode = 0
        Shift = 0
    End If
    If KeyCode = 8 Then
        SendKeys Chr(0)
    End If
End Sub

Private Sub TxtCita_KeyPress(Index As Integer, KeyAscii As Integer)
   'Validaci�n de la tecla pulsada en funci�n de los
    'requisitos establecidos para el campo en la Tabla
    If KeyAscii = Asc("'") Then KeyAscii = 0
    If KeyAscii = Asc("�") Then KeyAscii = 0

    If (KeyAscii < Asc("0") Or KeyAscii > Asc("9")) And KeyAscii <> 8 Then
        KeyAscii = 0   'nada
    End If
End Sub
Private Sub pCargarProceso() 'Carga la lista de la Historia de Ingresos del paciente

    'Consultas SQL
    Dim sStmSql     As String
    Dim qryConsulta As rdoQuery
    Dim rstConsulta As rdoResultset
    Dim sRow        As String
'    Dim strRecurso  As String

    'Inicilizar los ComboBox de Pruebas y Recursos
    grdDBGrid1(0).RemoveAll

    'Procesos
    sStmSql = "SELECT AD0700.AD07CODPROCESO, "
    sStmSql = sStmSql & "AD0700.AD07DESNOMBPROCE, "
    sStmSql = sStmSql & "TO_CHAR(AD0700.AD07FECHORAINICI,'DD/MM/YYYY HH24:MI:SS'), "
    sStmSql = sStmSql & "AD0400.AD02CODDPTO, "
    sStmSql = sStmSql & "SG0200.SG02APE1 "
    sStmSql = sStmSql & "FROM AD0700, AD0400, SG0200, AG1100 "
    sStmSql = sStmSql & "WHERE AD0700.CI21CODPERSONA = ? "
    sStmSql = sStmSql & "AND AD0700.AD07FECHORAFIN IS NULL "
    sStmSql = sStmSql & "AND AD0700.AD07CODPROCESO = AD0400.AD07CODPROCESO "
    sStmSql = sStmSql & "AND AD0400.AD04FECFINRESPON IS NULL "
    sStmSql = sStmSql & "AND AD0400.SG02COD = SG0200.SG02COD (+) "
    sStmSql = sStmSql & "AND AD0400.AD02CODDPTO = ? "
    sStmSql = sStmSql & "AND AD0400.SG02COD = AG1100.SG02COD  "
    sStmSql = sStmSql & "AND AG1100.AG11CODRECURSO = ? "
    sStmSql = sStmSql & "ORDER BY AD0700.AD07FECHORAINICI DESC "
    
    Set qryConsulta = objApp.rdoConnect.CreateQuery("", sStmSql)
    qryConsulta(0) = lngCodPers
    qryConsulta(1) = strDpto
''    If frmHuecosFI.SSDBCmbRecurso.Columns(0).Value <> "" Then
''        qryConsulta(2) = frmHuecosFI.SSDBCmbRecurso.Columns(0).Value
''    Else
''         qryConsulta(2) = frmHuecosFI.tvwHuecos.SelectedItem.Parent.Key
''        qryConsulta(2) = Mid(qryConsulta(2), 2)
''        qryConsulta(2) = Left(qryConsulta(2), CInt(InStr(qryConsulta(2), "D")) - 1)
'''        qryConsulta(2) = strRecurso
''    End If
     qryConsulta(2) = strRecurso

        Set rstConsulta = qryConsulta.OpenResultset()
    Do While Not rstConsulta.EOF
        sRow = rstConsulta(0) & Chr(9)
        sRow = sRow & rstConsulta(1) & Chr(9)
        sRow = sRow & rstConsulta(2) & Chr(9)
        sRow = sRow & rstConsulta(3) & Chr(9)
        sRow = sRow & "Dr." & rstConsulta(4) & Chr(9)
        grdDBGrid1(0).AddItem sRow
        rstConsulta.MoveNext
    Loop
    rstConsulta.Close
    qryConsulta.Close

End Sub
