VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmIntroduccionListadoAcunsa 
   Caption         =   "Introducci�n de los datos de Acunsa"
   ClientHeight    =   5670
   ClientLeft      =   2850
   ClientTop       =   1755
   ClientWidth     =   8265
   LinkTopic       =   "Form1"
   ScaleHeight     =   5670
   ScaleWidth      =   8265
   Begin VB.CommandButton Command3 
      Caption         =   "Ver el fichero de Errores"
      Height          =   615
      Left            =   600
      TabIndex        =   6
      Top             =   3360
      Width           =   2055
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Ver el fichero de Resumen"
      Height          =   615
      Left            =   5160
      TabIndex        =   5
      Top             =   3360
      Width           =   2055
   End
   Begin ComctlLib.ProgressBar ProgressBar1 
      Height          =   495
      Left            =   1500
      TabIndex        =   4
      Top             =   2160
      Visible         =   0   'False
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   873
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   2160
      TabIndex        =   3
      Text            =   "c:\Acunsa\Acunsa.txt"
      Top             =   360
      Width           =   3375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5640
      TabIndex        =   2
      Top             =   360
      Width           =   375
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6240
      Top             =   240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdTransferencia 
      Caption         =   "Transferecia"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   1680
      TabIndex        =   1
      Top             =   960
      Width           =   4695
   End
   Begin VB.Label Label2 
      Height          =   255
      Left            =   1560
      TabIndex        =   7
      Top             =   2760
      Width           =   5055
   End
   Begin VB.Label Label1 
      Caption         =   "Ruta del fichero:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   600
      TabIndex        =   0
      Top             =   360
      Width           =   1575
   End
End
Attribute VB_Name = "frmIntroduccionListadoAcunsa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Public dbCUN As rdoConnection
'********************************************************
'el programa transfiere los datos del disquete de Acunsa
'a la base de datos de la clinica.
'Actualiza las registros que se han cambiado, no toca
'los ficheros que son iguales e inserta los nuevos.
'Print Adem�s; rellena; un; fichero; error.txt; para; indicar
'que tipo de erroeres hay y que registro es. Tambi�n va rellenando
'un archivo Resumen.txt en el que escribe cuantas modificaciones,
'cuantos errores, cuantos registros son iguales y cuantas nuevas
'Ademas incluye la fecha y la hora de la transacci�n.
'*********************************************************
Private Sub cmdTransferencia_Click()
Dim ncanal As String
Dim clinea As String
Dim NumPoliza As Variant
Dim NumAsegurado As Variant
Dim strFecInicio As String
Dim strFecFin As String
Dim Tomador As String
Dim posT As Integer
Dim posA As Integer
Dim DNI As String
Dim longNomTomador As Integer
Dim apelTomador As String
Dim nomTomador As String
Dim longNomAsegurado As Integer
Dim apelAsegurado As String
Dim nomAsegurado As String
Dim Asegurado As String
Dim sexo As String
Dim CodSexo As Byte
Dim strFec As String
Dim CorPago As String
Dim Exclusiones As String
Dim TipoPoliza As String
Dim error As Long
Dim cuentaerrores As Long
Dim FecInicio As Variant
Dim fec As Variant
Dim strsql As String
Dim rs As rdoResultset
Dim rsPoliza As rdoResultset
Dim SQL As String
Dim fecfin As Variant
Dim a�o As Integer
Dim mes As Integer
Dim dia As Integer
Dim QryInsert As rdoQuery
Dim QryUpdate As rdoQuery
Dim blnnohacertransferencia As Boolean
Dim introducido As Long
Dim modificado As Long
Dim igual As Long
Dim blnTipoPoliza As Boolean
Dim errorPeligroso As Long
Dim blnInterrumpida As Boolean
Dim i As Long
Dim j As Long
Dim strfecha_sistema As String

blnInterrumpida = False

On Error Resume Next
Err = 0
ProgressBar1.Visible = True
Label2.Visible = True
Me.MousePointer = vbHourglass
strsql = "SELECT CI36CODTIPPOLI FROM CI3600"
Set rs = objApp.rdoConnect.OpenResultset((strsql), rdOpenKeyset)
ncanal = 1
error = 0

Open Text1.Text For Input As #ncanal
Open "C:\Acunsa\Error.txt" For Output As #2

Do While Not EOF(ncanal)
    Err = 0
Line Input #ncanal, clinea
    NumPoliza = Left(clinea, 6)
    NumAsegurado = Mid(clinea, 7, 6)
    strFecInicio = Mid(clinea, 13, 8)
    a�o = Val(Left(strFecInicio, 4))
    mes = Val(Mid(strFecInicio, 5, 2))
    dia = Val(Mid(strFecInicio, 7, 2))
    FecInicio = Format(CDate(dia & "/" & mes & "/" & a�o), "DD/MM/YYYY")
    If Trim(strFecInicio) = "" Or Not IsDate(FecInicio) Then
        Print #2, " "
        Print #2, "Fecha de inicio de poliza no es una fecha v�lida. Resistro:" & clinea
        error = error + 1
        Err = 0
    End If
    strFecFin = Mid(clinea, 21, 8)
    If Trim(strFecFin) <> "" Then
        a�o = Val(Left(strFecFin, 4))
        mes = Val(Mid(strFecFin, 5, 2))
        dia = Val(Mid(strFecFin, 7, 2))
        fecfin = Format(CDate(dia & "/" & mes & "/" & a�o), "DD/MM/YYYY")
   Else
    fecfin = ""
   End If
   If fecfin <> "" And Not IsDate(fecfin) Then
        Print #2, " "
        Print #2, "Fecha de fin de poliza no es una fecha v�lida. Registro:" & clinea
        error = error + 1
        Err = 0
    End If

    
    Tomador = Mid(clinea, 29, 40)
    posT = InStr(1, Tomador, ",", 0)
    longNomTomador = 40 - posT
    nomTomador = Trim(Right(Tomador, longNomTomador))
        If posT <> 0 Then
            apelTomador = Left(Tomador, posT - 1)
        Else
            apelTomador = ""
        End If
    Asegurado = Mid(clinea, 69, 40)
    posA = InStr(1, Asegurado, ",", 0)
    longNomAsegurado = 40 - posA
    nomAsegurado = Trim(Right(Asegurado, longNomAsegurado))
        If posA <> 0 Then
            apelAsegurado = Left(Asegurado, posA - 1)
        Else
            apelAsegurado = ""
        End If
    DNI = Mid(clinea, 109, 12)
    sexo = Mid(clinea, 121, 1)
    strFec = Mid(clinea, 122, 8)
    If Trim(strFec) <> "" Then
        a�o = Val(Left(strFec, 4))
        mes = Val(Mid(strFec, 5, 2))
        dia = Val(Mid(strFec, 7, 2))
        fec = Format(CDate(dia & "/" & mes & "/" & a�o), "DD/MM/YYYY")
   Else
    fec = ""
   End If
   If fec <> "" And Not IsDate(fec) Then
        Print #2, " "
        Print #2, " Fecha de nacimiento no es una fecha v�lida. Registro:" & clinea
        error = error + 1
        Err = 0
    End If

    CorPago = Mid(clinea, 130, 1)
    Exclusiones = Mid(clinea, 131, 120)
    TipoPoliza = Mid(clinea, 251, 3)
    'COMPROBACI�N DE LOS DATOS********************
    '*********************************************
'NUMERO DE POLIZA
If Not IsNumeric(NumPoliza) Then
    Print #2, " "
    Print #2, "Su n�mero de poliza no es num�rico. Registro: " & clinea
    error = error + 1
End If
'NUMERO DE ASEGURADO
If Not IsNumeric(NumAsegurado) Then
    Print #2, " "
    Print #2, " Su n�mero de asegurado no es num�rico. Registro: " & clinea
    error = error + 1
End If
'TOMADOR
If Trim(nomTomador) = "" Then
    Print #2, " "
    Print #2, " No existe el nombre del tomador. Registro: " & clinea
    error = error + 1
End If
'asegurado
If Trim(nomAsegurado) = "" Then
    Print #2, " "
    Print #2, " No existe el nombre del Asegurado. Registro: " & clinea
    error = error + 1
End If
'sexo
If sexo <> "V" And sexo <> "H" Then
    Print #2, " "
    Print #2, " El sexo no es correcto. Registro: " & clinea
    error = error + 1
End If
'corriente de pago
If CorPago <> "S" And CorPago <> "N" Then
    Print #2, " "
    Print #2, " El campo corriente de pago es incorrecto. Registro: " & clinea
    error = error + 1
End If


    Do While Not rs.EOF 'se comprueba si el tipo de poliza esta en la base de datos
        If rs.rdoColumns(0) = TipoPoliza Then
           blnTipoPoliza = True
        End If
        rs.MoveNext
    Loop
        If Not blnTipoPoliza Then
        
            Print #2, " "
            Print #2, " El Tipo de Poliza es incorrecto. Registro: " & clinea
            error = error + 1
        End If
        blnTipoPoliza = False
 rs.MoveFirst
i = i + 1
Loop
ProgressBar1.Max = i
ProgressBar1.Min = 0
If error > 50 Then
    MsgBox "Transferencia interrumpida." & Chr(13) & "Hay m�s de 50 errores", vbOKOnly, "Acunsa"
    blnInterrumpida = True
    
 End If
rs.Close
Close #ncanal
Close #2


If blnInterrumpida = False Then
Open Text1.Text For Input As #ncanal
'Open "C:\Acunsa\Error.txt" For Output As #2
strsql = "SELECT CI36CODTIPPOLI FROM CI3600"
Set rs = objApp.rdoConnect.OpenResultset((strsql), rdOpenKeyset)
error = 0

Do While Not EOF(ncanal)
    Err = 0
    Line Input #ncanal, clinea
    NumPoliza = Left(clinea, 6)
    NumAsegurado = Mid(clinea, 7, 6)
    strFecInicio = Mid(clinea, 13, 8)
    a�o = Val(Left(strFecInicio, 4))
    mes = Val(Mid(strFecInicio, 5, 2))
    dia = Val(Mid(strFecInicio, 7, 2))
    FecInicio = Format(CDate(dia & "/" & mes & "/" & a�o), "DD/MM/YYYY")
    If Trim(strFecInicio) = "" Or Not IsDate(FecInicio) Then
'        Print #2, " "
'        Print #2, "Fecha de inicio de poliza no es una fecha v�lida. Resistro:" & clinea
        error = error + 1
        Err = 0
    End If
    strFecFin = Mid(clinea, 21, 8)
    If Trim(strFecFin) <> "" Then
        a�o = Val(Left(strFecFin, 4))
        mes = Val(Mid(strFecFin, 5, 2))
        dia = Val(Mid(strFecFin, 7, 2))
        fecfin = Format(CDate(dia & "/" & mes & "/" & a�o), "DD/MM/YYYY")
   Else
    fecfin = ""
   End If
   If fecfin <> "" And Not IsDate(fecfin) Then
'        Print #2, " "
'        Print #2, "Fecha de fin de poliza no es una fecha v�lida. Registro:" & clinea
        error = error + 1
        Err = 0
    End If


    Tomador = Mid(clinea, 29, 40)
    posT = InStr(1, Tomador, ",", 0)
    longNomTomador = 40 - posT
    nomTomador = Trim(Right(Tomador, longNomTomador))
        If posT <> 0 Then
            apelTomador = Left(Tomador, posT - 1)
        Else
            apelTomador = ""
        End If
    Asegurado = Mid(clinea, 69, 40)
    posA = InStr(1, Asegurado, ",", 0)
    longNomAsegurado = 40 - posA
    nomAsegurado = Trim(Right(Asegurado, longNomAsegurado))
        If posA <> 0 Then
            apelAsegurado = Left(Asegurado, posA - 1)
        Else
            apelAsegurado = ""
        End If
    DNI = Mid(clinea, 109, 12)
    sexo = Mid(clinea, 121, 1)
    strFec = Mid(clinea, 122, 8)
    If Trim(strFec) <> "" Then
        a�o = Val(Left(strFec, 4))
        mes = Val(Mid(strFec, 5, 2))
        dia = Val(Mid(strFec, 7, 2))
        fec = Format(CDate(dia & "/" & mes & "/" & a�o), "DD/MM/YYYY")
   Else
    fec = ""
   End If
   If fec <> "" And Not IsDate(fec) Then
'        Print #2, " "
'        Print #2, " Fecha de nacimiento no es una fecha v�lida. Registro:" & clinea
        error = error + 1
        Err = 0
    End If

    CorPago = Mid(clinea, 130, 1)
    Exclusiones = Mid(clinea, 131, 120)
    TipoPoliza = Mid(clinea, 251, 3)
    'COMPROBACI�N DE LOS DATOS********************
    '*********************************************
'NUMERO DE POLIZA
If Not IsNumeric(NumPoliza) Then
'    Print #2, " "
'    Print #2, "Su n�mero de poliza no es num�rico. Registro: " & clinea
    error = error + 1
End If
'NUMERO DE ASEGURADO
If Not IsNumeric(NumAsegurado) Then
'    Print #2, " "
'    Print #2, " Su n�mero de asegurado no es num�rico. Registro: " & clinea
    error = error + 1
End If
'TOMADOR
If Trim(nomTomador) = "" Then
'    Print #2, " "
'    Print #2, " No existe el nombre del tomador. Registro: " & clinea
    error = error + 1
End If
'asegurado
If Trim(nomAsegurado) = "" Then
'    Print #2, " "
'    Print #2, " No existe el nombre del tomador. Registro: " & clinea
    error = error + 1
End If
'sexo
If sexo <> "V" And sexo <> "H" Then
'    Print #2, " "
'    Print #2, " El sexo no es correcto. Registro: " & clinea
    error = error + 1
End If
'corriente de pago
If CorPago <> "S" And CorPago <> "N" Then
'    Print #2, " "
'    Print #2, " El campo corriente de pago es incorrecto. Registro: " & clinea
    error = error + 1
End If


    Do While Not rs.EOF 'se comprueba si el tipo de poliza esta en la base de datos
        If rs.rdoColumns(0) = TipoPoliza Then
           blnTipoPoliza = True
        End If
        rs.MoveNext
    Loop
        If Not blnTipoPoliza Then

'            Print #2, " "
'            Print #2, " El Tipo de Poliza es incorrecto. Registro: " & clinea
            error = error + 1
        End If
        blnTipoPoliza = False
 rs.MoveFirst

If error = 0 Then


 'COMPROBAMOS SI ESTA EL ASEGURADO
    SQL = "SELECT CI03NUMPOLIZA,CI03NUMASEGURA,CI36CODTIPPOLI," & _
    "CI30CODSEXO,CI03FECINIPOLI,CI03FECFINPOLI,CI03NOMBRTOMAD,CI03APELLITOMAD," & _
    "CI03NOMBRASEGU, CI03APELLIASEGU, CI03DNI,ci03fecnacimie,ci03corripaGO," & _
    "CI03EXCLUSION FROM CI0300 WHERE CI03NUMPOLIZA =" & _
    NumPoliza & " AND CI03NUMASEGURA=" & NumAsegurado
    Set rsPoliza = objApp.rdoConnect.OpenResultset((SQL), rdOpenKeyset)
 
    If sexo = "V" Then CodSexo = 1
    If sexo = "H" Then CodSexo = 2
    If CorPago = "S" Then CorPago = Val(-1)
    If CorPago = "N" Then CorPago = Val(0)
    objApp.BeginTrans
If Not rsPoliza.EOF Then 'EXISTE EL ASEGURADO
     If NumPoliza = llenar(rsPoliza(0), 6, 0, True) And NumAsegurado = llenar(rsPoliza(1), 6, 0, True) And TipoPoliza = rsPoliza(2) Then
       If CodSexo = rsPoliza(3) And FecInicio = Format(rsPoliza(4), "DD/MM/YYYY") And fecfin = Trim(llenar(Format(rsPoliza(5), "DD/MM/YYYY"), 10, " ", False)) Then
         If nomTomador = Trim(rsPoliza(6)) And apelTomador = llenar(rsPoliza(7), posT - 1, " ", False) And nomAsegurado = Trim(llenar(rsPoliza(8), 40 - posA, " ", False)) Then
            If apelAsegurado = Trim(llenar(rsPoliza(9), posA - 1, " ", False)) And Trim(DNI) = Trim(llenar(rsPoliza(10), 12, " ", False)) Then
                If fec = Trim(llenar(Format(rsPoliza(11), "DD/MM/YYYY"), 10, " ", False)) And CorPago = rsPoliza(12) And Exclusiones = llenar(rsPoliza(13), 120, " ", False) Then
           blnnohacertransferencia = True 'esta el asegurado exactamente igual entonces
                  igual = igual + 1                    'no se transfiere
                End If
            End If
         End If
       End If
      End If
   If Not blnnohacertransferencia Then
    'ESTA EL ASEGURADO PERO ALGO CAMBIA EN SU REGISTRO, ASI QUE SE ACTUALIZA
      SQL = "UPDATE CI0300 SET CI03NUMPOLIZA= ? ,CI03NUMASEGURA= ? ,CI36CODTIPPOLI= ?," & _
    "CI30CODSEXO=?,CI03FECINIPOLI=TO_DATE( ? ,'DD/MM/YYYY'),CI03FECFINPOLI=TO_DATE(?,'DD/MM/YYYY'),CI03NOMBRTOMAD=?,CI03APELLITOMAD=?," & _
    "CI03NOMBRASEGU=?, CI03APELLIASEGU=?, CI03DNI=?,ci03fecnacimie=TO_DATE(?,'DD/MM/YYYY'),ci03corripaGO=?," & _
    "CI03EXCLUSION=?  " & _
    "WHERE CI03NUMPOLIZA = " & NumPoliza & " AND CI03NUMASEGURA=" & NumAsegurado
    Set QryUpdate = objApp.rdoConnect.CreateQuery(" ", SQL)
    QryUpdate.SQL = SQL
        QryUpdate(0) = Val(NumPoliza)
        QryUpdate(1) = Val(NumAsegurado)
        QryUpdate(2) = TipoPoliza
        QryUpdate(3) = CodSexo
        QryUpdate(4) = FecInicio
        If Trim(fecfin) = "" Then
            QryUpdate(5) = Null
        Else
            QryUpdate(5) = fecfin
        End If
        QryUpdate(6) = nomTomador
        If Trim(apelTomador) = "" Then
            QryUpdate(7) = Null
        Else
            QryUpdate(7) = apelTomador
        End If
        QryUpdate(8) = nomAsegurado
        If Trim(apelAsegurado) = "" Then
            QryUpdate(9) = Null
        Else
             QryUpdate(9) = apelAsegurado
        End If

        If Trim(DNI) = "" Then
            QryUpdate(10) = Null
        Else
            QryUpdate(10) = Trim(DNI)
        End If
        If Trim(fec) = "" Then
            QryUpdate(11) = Null
        Else
            QryUpdate(11) = fec
        End If
        QryUpdate(12) = Val(CorPago)
        If Trim(Exclusiones) = "" Then
            QryUpdate(13) = Null
        Else
            QryUpdate(13) = Exclusiones
        End If
        QryUpdate.Execute
        QryUpdate.Close
        If Err = 0 Then modificado = modificado + 1
        If Err > 0 Then
            Print #2, " "
            Print #2, " Error al grabarse en la base de datos. Registro: " & clinea
            error = error + 1
         End If
     End If
Else

        SQL = "INSERT INTO CI0300 (CI03NUMPOLIZA,CI03NUMASEGURA,CI36CODTIPPOLI," & _
    "CI30CODSEXO,CI03FECINIPOLI,CI03FECFINPOLI,CI03NOMBRTOMAD,CI03APELLITOMAD," & _
    "CI03NOMBRASEGU, CI03APELLIASEGU, CI03DNI,ci03fecnacimie,ci03corripaGO," & _
    "CI03EXCLUSION ) VALUES (?, ?,?,?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),?,?)"
        Set QryInsert = objApp.rdoConnect.CreateQuery(" ", SQL)
        QryInsert.SQL = SQL
       QryInsert(0) = Val(NumPoliza)
         QryInsert(1) = Val(NumAsegurado)
         QryInsert(2) = TipoPoliza
         QryInsert(3) = CodSexo
         QryInsert(4) = FecInicio
        If Trim(fecfin) = "" Then
             QryInsert(5) = Null
        Else
             QryInsert(5) = fecfin
        End If
        QryInsert(6) = nomTomador
        If Trim(apelTomador) = "" Then
             QryInsert(7) = Null
        Else
             QryInsert(7) = apelTomador
        End If
         QryInsert(8) = nomAsegurado
        If Trim(apelAsegurado) = "" Then
             QryInsert(9) = Null
        Else
              QryInsert(9) = apelAsegurado
        End If

        If Trim(DNI) = "" Then
             QryInsert(10) = Null
        Else
            QryInsert(10) = Trim(DNI)
        End If
        If Trim(fec) = "" Then
             QryInsert(11) = Null
        Else
             QryInsert(11) = fec
        End If
         QryInsert(12) = CorPago
        If Trim(Exclusiones) = "" Then
             QryInsert(13) = Null
        Else
             QryInsert(13) = Exclusiones
        End If
        QryInsert.Execute
        QryInsert.Close
        If Err = 0 Then introducido = introducido + 1
        If Err > 0 Then
            Print #2, " "
            Print #2, " Error al grabarse en la base de datos. Registro: " & clinea
            error = error + 1
        End If
End If
    If Err > 0 Or blnnohacertransferencia Then objApp.RollbackTrans Else objApp.CommitTrans
    blnnohacertransferencia = False
End If
cuentaerrores = cuentaerrores + error
error = 0
ProgressBar1.Value = ProgressBar1.Value + 1
j = j + 1
Label2.Caption = j & " registros                                                                          de " & i
Label2.Refresh
Loop
rs.Close
Close #ncanal

  
  Set rs = objApp.rdoConnect.OpenResultset("select to_char(sysdate,'DD/MM/YYYY HH:MI') from dual", rdOpenKeyset)
  strfecha_sistema = rs.rdoColumns(0)
  rs.Close
  Set rs = Nothing

'Close #2
Open "C:\Acunsa\Resumen.txt" For Append As #3
Print #3, " "
Print #3, "Resumen(" & strfecha_sistema & "):Ha habido,"
Print #3, "                 " & igual & " registros iguales."
Print #3, "                 " & modificado & " registros modificados."
Print #3, "                 " & introducido & " registros nuevos."
Print #3, "                 " & cuentaerrores & " registros que no cumplen las restricciones"
Print #3, "                   impuestas por el programa para ser introducidas."
Print #3, "                 " & i & " en total."

Close #3
 Me.MousePointer = vbDefault
    MsgBox "Transferencia terminada", vbOKOnly, "Estado de la transferencia"
    If cuentaerrores > 0 Then
        MsgBox "Hay " & cuentaerrores & " errores."
    End If
End If
ProgressBar1.Value = 0
Label2.Caption = ""
ProgressBar1.Visible = False
Label2.Visible = False
 Me.MousePointer = vbDefault
End Sub

Private Sub Command1_Click()
With CommonDialog1
.DialogTitle = "Abrir fichero"
.DefaultExt = "TXT"
.Filter = "Archivos de texto|*.TXT|Todos los archivos|*.*"
.Flags = &H1000&
.ShowOpen
End With
Text1.Text = CommonDialog1.filename
End Sub

Private Sub Command2_Click()
Dim pro As Long
pro = Shell("notepad.exe c:\Acunsa\Resumen.txt", 1)
End Sub

Private Sub Command3_Click()
Dim pro As Long
pro = Shell("notepad.exe c:\Acunsa\Error.txt", 1)
End Sub

Private Sub Form_Load()
'Dim objEncrypt As New clsCWMRES
'Set dbCUN = rdoEnvironments(0).OpenConnection(dsName:="Oracle73", _
'            Connect:="SRVR=prod.CUN;UID=" & fstrINI("DataBaseUser") & ";PWD=" & _
'            objEncrypt.Encript(fstrINI("DataBasePassword")) & ";")
End Sub
'Private Function fstrINI(strBuscar As String) As String
'
'Dim strIni As String 'Almacena el Path del archivo INI
'Dim lngPosArchivo As Long 'Posici�n actual en el archivo INI
'Dim blnSalir As Boolean 'Control del bucle Do...Loop While
'
'strIni = "\\Desarrollo\Codewizard\Iniprod\cwUser.ini"
'Open strIni For Binary Access Read Shared As #1 Len = FileLen(strIni)
'lngPosArchivo = 1
'Do 'While blnSalir = False
'   Seek #1, lngPosArchivo
'   Line Input #1, strIni
'   If InStr(1, strIni, strBuscar) Then
'      fstrINI = Right(strIni, Len(strIni) - InStr(1, strIni, "=") - 1)
'      blnSalir = True
'   Else
'      lngPosArchivo = lngPosArchivo + Len(strIni) + 2
'   End If
'Loop While blnSalir = False
'Close #1
'
'End Function

