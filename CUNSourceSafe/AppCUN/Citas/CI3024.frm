VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Begin VB.Form frmCitasPaciente1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Citas del Paciente"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdavisos 
      Height          =   375
      Left            =   9420
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   240
      Width           =   675
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10980
      TabIndex        =   17
      Top             =   240
      Width           =   855
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular"
      Height          =   375
      Left            =   2760
      TabIndex        =   16
      Top             =   8160
      Width           =   1275
   End
   Begin VB.Frame Frame4 
      Caption         =   "Paciente"
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9195
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Left            =   3240
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   240
         Width           =   4575
      End
      Begin VB.TextBox txtHistoria 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Left            =   960
         TabIndex        =   0
         Top             =   240
         Width           =   975
      End
      Begin VB.TextBox txtCodpersona 
         Height          =   285
         Left            =   7260
         TabIndex        =   12
         Top             =   0
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   7920
         TabIndex        =   11
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Paciente:"
         Height          =   195
         Left            =   2280
         TabIndex        =   15
         Top             =   315
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Historia:"
         Height          =   195
         Left            =   240
         TabIndex        =   14
         Top             =   315
         Width           =   570
      End
   End
   Begin VB.CommandButton cmdDejarPend 
      Caption         =   "&Dejar Pdte"
      Height          =   375
      Left            =   5820
      TabIndex        =   9
      Top             =   8160
      Width           =   1275
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Citar"
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   8160
      Width           =   1275
   End
   Begin VB.CommandButton cmdIndSanit 
      Caption         =   "Ind &Sanitarios"
      Height          =   375
      Left            =   4380
      TabIndex        =   7
      Top             =   8160
      Width           =   1275
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "Dato&s Act."
      Height          =   375
      Left            =   1380
      TabIndex        =   6
      Top             =   8160
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   7200
      TabIndex        =   1
      Top             =   7920
      Width           =   4575
      Begin VB.CheckBox chkPet 
         Caption         =   "Peticion"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.CheckBox chkPac 
         Caption         =   "Prog. Paciente"
         Height          =   255
         Left            =   1200
         TabIndex        =   4
         Top             =   240
         Width           =   1455
      End
      Begin VB.CheckBox chkDpto 
         Caption         =   "Prog. Dpto"
         Height          =   255
         Left            =   2640
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdImprimir 
         Caption         =   "Im&primir"
         Height          =   375
         Left            =   3780
         TabIndex        =   2
         Top             =   200
         Width           =   735
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdCitadas 
      Height          =   2325
      Left            =   0
      TabIndex        =   18
      Top             =   3240
      Width           =   11835
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20876
      _ExtentY        =   4101
      _StockProps     =   79
      Caption         =   "Citadas"
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdPendientes 
      Height          =   2325
      Left            =   0
      TabIndex        =   19
      Top             =   5640
      Width           =   11895
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20981
      _ExtentY        =   4101
      _StockProps     =   79
      Caption         =   "Pendientes de recitar"
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdSinCitar 
      Height          =   2325
      Left            =   0
      TabIndex        =   20
      Top             =   840
      Width           =   11895
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20981
      _ExtentY        =   4101
      _StockProps     =   79
      Caption         =   "Sin Citar"
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCitasPaciente1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim blnDatosCargados As Boolean
Dim strHistoria_Ant As String
Private Sub pLimpiar()
'txtHistoria.Text = ""
txtPaciente.Text = ""
grdCitadas.RemoveAll
grdSinCitar.RemoveAll
grdPendientes.RemoveAll
End Sub

Private Sub pMensajes()
Dim sql As String
Dim rsMens As rdoResultset
Dim qry As rdoQuery
Dim imgX As ListImage

   sql = "SELECT count(*) from  CI0400 WHERE CI21CodPersona = ?" _
   & " and (ci04FecCadMens >= trunc(sysdate+1)" _
   & " Or ci04FecCadMens Is Null)"
     Set qry = objApp.rdoConnect.CreateQuery("", sql)
     If Trim(txtCodpersona.Text) <> "" Then
        qry(0) = Val(Trim(txtCodpersona.Text))
     Else
        qry(0) = 0
     End If
     Set rsMens = qry.OpenResultset()
     If rsMens(0) > 0 Then
       Set imgX = objApp.imlImageList.ListImages("i44")
       cmdavisos.Picture = imgX.Picture
     Else
       Set imgX = objApp.imlImageList.ListImages("i40")
       cmdavisos.Picture = imgX.Picture
     End If
End Sub
Private Sub pImprimir(ssgrid1 As SSDBGrid)
Dim strWhere As String
Dim i As Integer
    For i = 0 To ssgrid1.SelBookmarks.Count - 1
        strWhere = strWhere & "PR04NUMACTPLAN= " & Val(ssgrid1.Columns("NumActPlan").CellText(ssgrid1.SelBookmarks(i))) & " OR "
    Next i
    strWhere = Left$(strWhere, Len(strWhere) - 4)
    If ssgrid1.SelBookmarks.Count > 0 Then
        Call Imprimir_API(strWhere, "Peticion.rpt")
    Else
        Call Imprimir_API(strWhere, "PetAgrup.rpt")
    End If
End Sub


Private Function fGrid() As SSDBGrid 'para saber en que grid esta la actuaci�n
 If grdPendientes.SelBookmarks.Count > 0 Then
        Set fGrid = grdPendientes
    Else
        If grdCitadas.SelBookmarks.Count > 0 Then
            Set fGrid = grdCitadas
        Else
            If grdSinCitar.SelBookmarks.Count > 0 Then
                Set fGrid = grdSinCitar
            Else
                MsgBox "No se ha seleccionado ninguna Actuaci�n.", vbExclamation, Me.Caption
                Set fGrid = Nothing
                Exit Function
            End If
        End If
End If
End Function

Private Function fValidarEstado(numActPlan As Long) As Boolean
Dim sql As String
Dim rs As rdoResultset
sql = "select pr04fecentrcola from pr0400 where pr04numactplan=" & numActPlan
Set rs = objApp.rdoConnect.OpenResultset(sql)
If Not IsNull(rs(0)) Then fValidarEstado = True
End Function
Private Sub pPendientes(intCol As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim sqlOrder As String
Dim qry As rdoQuery
 If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY CI0100.CI01FECCONCERT" 'Opci�n por defecto: Fecha Planificacion
    Else
        Select Case grdCitadas.Columns(intCol).Caption
        Case "Fecha Cita": sqlOrder = " ORDER BY CI0100.CI01FECCONCERT"
        Case "Fecha Cancelaci�n": sqlOrder = " ORDER BY CI0100.CI01FECUPD"
        Case "Actuacion": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
        Case "Recurso": sqlOrder = " ORDER BY AG1100.AG11DESRECURSO"
        Case "Dpto Realizador": sqlOrder = " ORDER BY DESDPTOREA"
        Case "Dpto Solicitante": sqlOrder = " ORDER BY DESDPTOSOL"
        Case "Doctor Solicitante": sqlOrder = " ORDER BY DOCTOR"
        Case Else: Exit Sub
        End Select
End If
        sql = "SELECT TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI') FECHA,"
        sql = sql & "TO_CHAR(CI0100.CI01FECUPD,'DD/MM/YYYY HH24:MI') FECCANCEL,"
        sql = sql & "PR0100.PR01DESCORTA,"
        sql = sql & "AG1100.AG11DESRECURSO,"
        sql = sql & "AD02REL.AD02DESDPTO DESDPTOREA,"
        sql = sql & "AD02SOL.AD02DESDPTO DESDPTOSOL,"
        sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
        sql = sql & "PR0300.PR01CODACTUACION,"
        sql = sql & "PR0300.AD02CODDPTO CODDPTOREA,"
        sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL, "
        sql = sql & "PR0400.PR37CODESTADO,"
        sql = sql & "PR0400.PR04NUMACTPLAN,"
        sql = sql & "CI0100.AG11CODRECURSO,"
        sql = sql & "PR0100.PR12CODACTIVIDAD,"
        sql = sql & "PR0900.PR09NUMPETICION,"
        sql = sql & "CI0100.CI31NUMSOLICIT," 'eSTOS CAMPOS TAMPOCO SE PARA QUE SIRVEN
        sql = sql & "CI0100.CI01NUMCITA,"
        sql = sql & "CI0100.AD15CODCAMA,"
        sql = sql & "PR0300.PR03NUMACTPEDI"
        sql = sql & " FROM PR0400,PR0300,CI0100,PR0900,"
        sql = sql & "SG0200,PR0100,AD0200 AD02REL,AG1100,AD0200 AD02SOL"
        sql = sql & " WHERE CI0100.CI01SITCITA=?"
        sql = sql & "  AND PR0400.CI21CODPERSONA = ?"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION(+)"
        sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD"
        sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN"
        sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
        sql = sql & " AND PR0300.AD02CODDPTO=AD02REL.AD02CODDPTO"
        sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
        sql = sql & " AND CI0100.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
        'sql = sql & " ORDER BY PR0400.PR04FECPLANIFIC"
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = constESTCITA_PENDRECITAR
        qry(1) = Val(txtCodpersona.Text)
        Set rs = qry.OpenResultset(sql)
        Call pCargarGrid(grdPendientes, rs)
        rs.Close
        qry.Close

End Sub


Private Sub pCitadas(intCol As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim sqlOrder As String
Dim qry As rdoQuery
 If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY CI0100.CI01FECCONCERT" 'Opci�n por defecto: Fecha Planificacion
    Else
        Select Case grdCitadas.Columns(intCol).Caption
        Case "Fecha Cita": sqlOrder = " ORDER BY CI0100.CI01FECCONCERT"
        Case "Actuacion": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
        Case "Recurso": sqlOrder = " ORDER BY AG1100.AG11DESRECURSO"
        Case "Dpto Realizador": sqlOrder = " ORDER BY DESDPTOREA"
        Case "Dpto Solicitante": sqlOrder = " ORDER BY DESDPTOSOL"
        Case "Doctor Solicitante": sqlOrder = " ORDER BY DOCTOR"
        Case Else: Exit Sub
        End Select
End If

  sql = "SELECT TO_CHAR(CI0100.CI01FECCONCERT,'DD/MM/YYYY HH24:MI') FECHA,PR0100.PR01DESCORTA,"
        sql = sql & "AG1100.AG11DESRECURSO,"
        sql = sql & "AD02REA.AD02DESDPTO DESDPTOREA,AD02SOL.AD02DESDPTO DESDPTOSOL,"
        sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
        sql = sql & "PR0300.PR01CODACTUACION,"
        sql = sql & "PR0400.AD02CODDPTO CODDPTOREA,"
        sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL,"
        sql = sql & "PR0400.PR37CODESTADO,"
        sql = sql & "PR0400.PR04NUMACTPLAN,CI0100.AG11CODRECURSO,"
        sql = sql & "PR0100.PR12CODACTIVIDAD,PR0900.PR09NUMPETICION,"
        sql = sql & "CI0100.CI31NUMSOLICIT," 'Estos dos campos no tengo muy claro que hagan falta
        sql = sql & "CI0100.CI01NUMCITA,"
        sql = sql & "CI0100.AD15CODCAMA"
        sql = sql & " FROM PR0400,PR0300,CI0100,PR0900,SG0200,  "
        sql = sql & " PR0100,AD0200 AD02REA,AG1100,AD0200 AD02SOL"
        sql = sql & " WHERE CI0100.CI01SITCITA=?"
        sql = sql & " AND PR0400.PR37CODESTADO =?"
        sql = sql & " AND PR0400.CI21CODPERSONA = ?"
        sql = sql & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION "
        sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD"
        sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN"
        sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
        sql = sql & " AND PR0300.AD02CODDPTO=AD02REA.AD02CODDPTO"
        sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
        sql = sql & " AND CI0100.AG11CODRECURSO=AG1100.AG11CODRECURSO "
        'sql = sql & " ORDER BY CI0100.CI01FECCONCERT "
        sql = sql & sqlOrder
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = constESTCITA_CITADA
        qry(1) = constESTACT_CITADA
        qry(2) = Val(txtCodpersona.Text)
        Set rs = qry.OpenResultset(sql)
        Call pCargarGrid(grdCitadas, rs)
        rs.Close
        qry.Close
End Sub
Private Sub pSinCitar(intCol As Integer)
Dim sql As String
Dim rs As rdoResultset
Dim sqlOrder As String
Dim qry As rdoQuery
 If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PR0400.PR04FECPLANIFIC" 'Opci�n por defecto: Fecha Planificacion
    Else
        Select Case grdSinCitar.Columns(intCol).Caption
        Case "Fecha Planificaci�n": sqlOrder = " ORDER BY PR0400.PR04FECPLANIFIC"
        Case "Actuacion": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
        Case "Recurso Preferente": sqlOrder = " ORDER BY AG1100.AG11DESRECURSO"
        Case "Dpto Realizador": sqlOrder = " ORDER BY DESDPTOREA"
        Case "Dpto Solicitante": sqlOrder = " ORDER BY DESDPTOSOL"
        Case "Doctor Solicitante": sqlOrder = " ORDER BY DOCTOR"
        Case Else: Exit Sub
        End Select
End If
sql = "SELECT"
sql = sql & " TO_CHAR(PR0400.PR04FECPLANIFIC,'DD/MM/YYYY HH24:MI') FECHA,"
sql = sql & "PR0400.PR04FECPLANIFIC FECHAORDEN,"
sql = sql & "PR0100.PR01DESCORTA,"
sql = sql & "AG1100.AG11DESRECURSO,"
sql = sql & "AD02REA.AD02DESDPTO DESDPTOREA,"
sql = sql & "AD02SOL.AD02DESDPTO DESDPTOSOL,"
sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
sql = sql & "PR0300.PR03INDCITABLE,"
sql = sql & "PR0300.PR01CODACTUACION,"
sql = sql & "PR0400.AD02CODDPTO CODDPTOREA,"
sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL,"
sql = sql & "PR0400.PR37CODESTADO,"
sql = sql & "PR0400.PR04NUMACTPLAN,"
sql = sql & "PR0100.PR12CODACTIVIDAD,"
sql = sql & "PR0900.PR09NUMPETICION,"
sql = sql & "PR1400.AG11CODRECURSO"
sql = sql & " From"
sql = sql & " PR0400, PR0300, PR0100,CI0100,"
sql = sql & " AG1100, PR0900, SG0200,AD0200 AD02REA,"
sql = sql & " AD0200 AD02SOL, PR1400"
sql = sql & " Where PR0400.PR37CODESTADO =?"
sql = sql & " AND PR0400.CI21CODPERSONA =?"
sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0300.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION"
sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD(+)"
sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
sql = sql & " AND PR0400.AD02CODDPTO=AD02REA.AD02CODDPTO"
sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
sql = sql & " AND PR1400.PR14INDRECPREFE =-1"
sql = sql & " AND PR0300.PR03INDCITABLE=-1"
sql = sql & " AND PR1400.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN(+)"
sql = sql & " AND CI0100.CI01SITCITA IS NULL"
sql = sql & " Union"
sql = sql & " SELECT"
sql = sql & "  TO_CHAR(PR0400.PR04FECPLANIFIC,'DD/MM/YYYY HH24:MI') FECHA,"
sql = sql & " PR0400.PR04FECPLANIFIC FECHAORDEN,"
sql = sql & " PR0100.PR01DESCORTA,"
sql = sql & " AG1100.AG11DESRECURSO,"
sql = sql & " AD02REA.AD02DESDPTO DESDPTOREA,"
sql = sql & " AD02SOL.AD02DESDPTO DESDPTOSOL,"
sql = sql & " NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
sql = sql & " PR0300.PR03INDCITABLE,"
sql = sql & " PR0300.PR01CODACTUACION,"
sql = sql & " PR0400.AD02CODDPTO CODDPTOREA,"
sql = sql & " PR0900.AD02CODDPTO CODDPTOSOL,"
sql = sql & " PR0400.PR37CODESTADO,"
sql = sql & " PR0400.PR04NUMACTPLAN,"
sql = sql & " PR0100.PR12CODACTIVIDAD,"
sql = sql & " PR0900.PR09NUMPETICION ,"
sql = sql & " PR1400.AG11CODRECURSO"
sql = sql & " From"
sql = sql & " PR0400, PR0300, PR0100,"
sql = sql & " AG1100, PR0900, SG0200,AD0200 AD02REA,"
sql = sql & " AD0200 AD02SOL, PR1400"
sql = sql & " Where PR0400.PR37CODESTADO =?"
sql = sql & " AND PR0400.CI21CODPERSONA =?"
sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0300.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION"
sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD(+)"
sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
sql = sql & " AND PR0400.AD02CODDPTO=AD02REA.AD02CODDPTO"
sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
sql = sql & " AND (PR1400.PR14INDRECPREFE =0 AND PR0300.PR03INDCITABLE=0)"
sql = sql & " AND PR1400.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
sql = sql & " ORDER BY FECHAORDEN"

'************************************************
'Esta es la select de antes, no borrar
'**************************************

' sql = "SELECT  TO_CHAR(PR0400.PR04FECPLANIFIC,'DD/MM/YYYY HH24:MI') FECHA,PR0100.PR01DESCORTA,"
'        sql = sql & "AG1100.AG11DESRECURSO,"
'        sql = sql & "AD02REA.AD02DESDPTO DESDPTOREA,"
'        sql = sql & "AD02SOL.AD02DESDPTO DESDPTOSOL,"
'        sql = sql & "NVL(SG0200.SG02TXTFIRMA, SG02APE1|| ' ' ||SG02APE2|| ', ' ||SG02NOM)  DOCTOR,"
'        sql = sql & "PR0300.PR03INDCITABLE," 'HASTA AQU� SON LOS CAMPOS VISIBLES
'        sql = sql & "PR0300.PR01CODACTUACION,"
'        sql = sql & "PR0400.AD02CODDPTO CODDPTOREA,"
'        sql = sql & "PR0900.AD02CODDPTO CODDPTOSOL,"
'        sql = sql & "PR0400.PR37CODESTADO,"
'        sql = sql & "PR0400.PR04NUMACTPLAN,"
'        sql = sql & "PR1400.AG11CODRECURSO,"
'        sql = sql & "PR0100.PR12CODACTIVIDAD,"
'        sql = sql & "PR0900.PR09NUMPETICION"
'        sql = sql & " FROM PR0400, PR0300, PR0100,"
'        sql = sql & "PR1400,AG1100,CI0100,"
'        sql = sql & " PR0900, SG0200,AD0200 AD02REA,AD0200 AD02SOL"
'        sql = sql & " WHERE  PR0400.PR37CODESTADO =?"
'        sql = sql & " AND PR0400.CI21CODPERSONA = ?"
'        sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI "
'        sql = sql & " AND PR0300.PR03NUMACTPEDI=PR1400.PR03NUMACTPEDI"
'        sql = sql & " AND PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION "
'        sql = sql & " AND PR0900.SG02COD=SG0200.SG02COD(+) "
'        sql = sql & " AND (PR1400.PR14INDRECPREFE = -1 OR (PR1400.PR14INDRECPREFE =0 AND PR0300.PR03INDCITABLE=0))"
'        sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
'        sql = sql & " AND PR0400.AD02CODDPTO=AD02REA.AD02CODDPTO"
'        sql = sql & " AND PR0900.AD02CODDPTO=AD02SOL.AD02CODDPTO"
'        sql = sql & " AND PR1400.AG11CODRECURSO=AG1100.AG11CODRECURSO(+)"
'        sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN(+)"
'        sql = sql & " AND CI0100.CI01SITCITA IS NULL"
'        sql = sql & sqlOrder
'        'sql = sql & " ORDER BY PR0400.PR04FECPLANIFIC"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = constESTACT_PLANIFICADA
        qry(1) = Val(txtCodpersona.Text)
         qry(2) = constESTACT_PLANIFICADA
        qry(3) = Val(txtCodpersona.Text)
        Set rs = qry.OpenResultset(sql)
        Call pCargarGrid(grdSinCitar, rs)
        rs.Close
        qry.Close
End Sub
Private Sub pIniciar()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
If txtHistoria.Text = "" And txtCodpersona.Text = "" Then
    Exit Sub
End If
If txtHistoria.Text <> "" Then
    sql = "SELECT CI21CODPERSONA,"
    sql = sql & "CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PACIENTE "
    sql = sql & "FROM CI2200 WHERE CI22NUMHISTORIA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Val(txtHistoria.Text)
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        txtCodpersona.Text = rs!CI21CODPERSONA
        txtPaciente.Text = rs!PACIENTE
        strHistoria_Ant = txtHistoria.Text
    Else
        MsgBox "No existe nadie con ese n�mero de historia. Utilice el buscador", vbExclamation, Me.Caption
        Exit Sub
    End If
End If
If txtCodpersona.Text <> "" Then
    sql = "SELECT CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PACIENTE "
    sql = sql & "FROM CI2200 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Val(txtCodpersona.Text)
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        'txtCodpersona.Text = rs!CI21CODPERSONA
        txtPaciente.Text = rs!PACIENTE
        strHistoria_Ant = txtHistoria.Text
    
    End If
End If
'grdSinCitar.RemoveAll
'grdCitadas.RemoveAll
'grdPendientes.RemoveAll
Screen.MousePointer = vbHourglass
Call pSinCitar(-1)
Call pCitadas(-1)
Call pPendientes(-1)
Call pMensajes
Screen.MousePointer = vbDefault
End Sub
Private Sub pCargarGrid(rsgrid As SSDBGrid, rs As rdoResultset)
rsgrid.RemoveAll
If rsgrid.Name = "grdSinCitar" Then
    Do While Not rs.EOF
         rsgrid.AddItem (rs!fecha & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!AG11DESRECURSO & Chr(9) & _
                        rs!DESDPTOREA & Chr(9) & _
                        rs!DESDPTOSOL & Chr(9) & _
                        rs!DOCTOR & Chr(9) & _
                        rs!PR03INDCITABLE & Chr(9) & _
                        rs!PR01CODACTUACION & Chr(9) & _
                        rs!CODDPTOREA & Chr(9) & _
                        rs!CODDPTOSOL & Chr(9) & _
                        rs!PR37CODESTADO & Chr(9) & _
                        rs!PR04NUMACTPLAN & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!PR12CODACTIVIDAD & Chr(9) & _
                        rs!PR09NUMPETICION)
 
        rs.MoveNext
     Loop
    End If
    If rsgrid.Name = "grdCitadas" Then
     Do While Not rs.EOF
         rsgrid.AddItem (rs!fecha & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!AG11DESRECURSO & Chr(9) & _
                        rs!DESDPTOREA & Chr(9) & _
                        rs!DESDPTOSOL & Chr(9) & _
                        rs!DOCTOR & Chr(9) & _
                        rs!PR01CODACTUACION & Chr(9) & _
                        rs!CODDPTOREA & Chr(9) & _
                        rs!CODDPTOSOL & Chr(9) & _
                        rs!PR37CODESTADO & Chr(9) & _
                        rs!PR04NUMACTPLAN & Chr(9) & _
                        rs!AG11CODRECURSO & Chr(9) & _
                        rs!PR12CODACTIVIDAD & Chr(9) & _
                        rs!PR09NUMPETICION & Chr(9) & _
                        rs!CI31NUMSOLICIT & Chr(9) & _
                        rs!CI01NUMCITA & Chr(9) & _
                        rs!AD15CODCAMA)
        rs.MoveNext
     Loop
    End If
    If rsgrid.Name = "grdPendientes" Then
     Do While Not rs.EOF
         rsgrid.AddItem (rs!fecha & Chr(9) & _
                        rs!FECCANCEL & Chr(9) & _
                        rs!PR01DESCORTA & Chr(9) & _
                        rs!AG11DESRECURSO & Chr(9) & _
                        rs!DESDPTOREA & Chr(9) & _
                        rs!DESDPTOSOL & Chr(9) & _
                        rs!DOCTOR & Chr(9) & _
                        rs!PR01CODACTUACION & Chr(9) & _
                        rs!CODDPTOREA & Chr(9) & _
                        rs!CODDPTOSOL & Chr(9) & _
                        rs!PR37CODESTADO & Chr(9) & _
                        rs!PR04NUMACTPLAN & Chr(9) & _
                        rs!AG11CODRECURSO & Chr(9) & _
                        rs!PR12CODACTIVIDAD & Chr(9) & _
                        rs!PR09NUMPETICION & Chr(9) & _
                        rs!CI31NUMSOLICIT & Chr(9) & _
                        rs!CI01NUMCITA & Chr(9) & _
                        rs!AD15CODCAMA & Chr(9) & _
                        rs!PR03NUMACTPEDI)
        rs.MoveNext
    Loop
   End If
    
    
    
    
    
    
    
'
'
'      Do While Not rs.EOF
'
'
'
'            grdDBGrid1(1).AddItem respuesta1b!CI01FECCONCERT & Chr$(9) _
'            & respuesta1b!PR01DESCORTA & Chr$(9) _
'            & respuesta1b!DPTOREL & Chr$(9) _
'            & respuesta1b!AG11DESRECURSO & Chr$(9) _
'            & respuesta1b!DPTOSOL & Chr$(9) _
'            & respuesta1b!DOCTOR & Chr$(9) _
'            & respuesta1b!CI01INDLISESPE & Chr$(9) _
'            & respuesta1b!CI21CODPERSONA & Chr$(9) _
'            & respuesta1b!PR04NUMACTPLAN & Chr$(9) _
'            & respuesta1b!PR12CODACTIVIDAD & Chr$(9) _
'            & respuesta1b!CI31NUMSOLICIT & Chr$(9) _
'            & respuesta1b!CI01NUMCITA & Chr$(9) _
'            & respuesta1b(3) & Chr$(9) _
'            & respuesta1b(2) & Chr$(9) _
'            & respuesta1b!PR09NUMPETICION & Chr$(9) _
'            & respuesta1b!AD15CODCAMA
'            respuesta1b.MoveNext
'
'         Loop
'        Do While Not respuesta1c.EOF
'
'
'
'            grdDBGrid1(2).AddItem respuesta1c!CI01FECCONCERT & Chr$(9) _
'            & respuesta1c!PR01DESCORTA & Chr$(9) _
'            & respuesta1c!DPTOREL & Chr$(9) _
'            & respuesta1c!AG11DESRECURSO & Chr$(9) _
'            & respuesta1c!DPTOSOL & Chr$(9) _
'            & respuesta1c!DOCTOR & Chr$(9) _
'            & respuesta1c!CI21CODPERSONA & Chr$(9) _
'            & respuesta1c!PR04NUMACTPLAN & Chr$(9) _
'            & respuesta1c!PR12CODACTIVIDAD & Chr$(9) _
'            & respuesta1c!CI31NUMSOLICIT & Chr$(9) _
'            & respuesta1c!CI01NUMCITA & Chr$(9) _
'            & respuesta1c!PR03NUMACTPEDI & Chr$(9) _
'            & respuesta1c!CI01SITCITA & Chr$(9) _
'            & respuesta1c!PR01CODACTUACION & Chr$(9) _
'            & respuesta1c(3) & Chr$(9) _
'            & respuesta1c!PR09NUMPETICION & Chr$(9) _
'            & respuesta1c!AD15CODCAMA
'            respuesta1c.MoveNext
'
'        Loop
'        respuesta1a.Close
'        qry1.Close
'        respuesta1b.Close
'        qry2.Close
'        respuesta1c.Close
'        qry3.Close
  
    ' Screen.MousePointer = vbDefault

End Sub
Private Sub pFormatearGrids()
    With grdSinCitar
        .Columns(0).Caption = "Fecha Planificaci�n"
        .Columns(0).Width = 1600
        .Columns(1).Caption = "Actuacion"
        .Columns(1).Width = 2000
        .Columns(2).Caption = "Recurso Preferente"
        .Columns(2).Width = 2000
        .Columns(3).Caption = "Dpto Realizador"
        .Columns(3).Width = 3000
        .Columns(4).Caption = "Dpto Solicitante"
        .Columns(4).Width = 3000
        .Columns(5).Caption = "Doctor Solicitante"
        .Columns(5).Width = 2000
        .Columns(6).Caption = "Citable"
        .Columns(6).Width = 900
        .Columns(6).style = ssStyleCheckBox
        .Columns(7).Caption = "CodActuacion"
        .Columns(7).Visible = False
        .Columns(8).Caption = "CodDptoRea"
        .Columns(8).Visible = False
        .Columns(9).Caption = "CodDptoSol"
        .Columns(9).Visible = False
        .Columns(10).Caption = "CodEstadoAct"
        .Columns(10).Visible = False
        .Columns(11).Caption = "NumActPlan"
        .Columns(11).Visible = False
        .Columns(12).Caption = "CodRecurso"
        .Columns(12).Visible = False
        .Columns(13).Caption = "CodActividad"
        .Columns(13).Visible = False
        .Columns(14).Caption = "NumPeticion"
        .Columns(14).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    With grdCitadas
        .Columns(0).Caption = "Fecha Cita"
        .Columns(0).Width = 1600
        .Columns(1).Caption = "Actuacion"
        .Columns(1).Width = 2000
        .Columns(2).Caption = "Recurso"
        .Columns(2).Width = 2000
        .Columns(3).Caption = "Dpto Realizador"
        .Columns(3).Width = 2000
        .Columns(4).Caption = "Dpto Solicitante"
        .Columns(4).Width = 2000
        .Columns(5).Caption = "Doctor Solicitante"
        .Columns(5).Width = 2000
        .Columns(6).Caption = "CodActuacion"
        .Columns(6).Visible = False
        .Columns(7).Caption = "CodDptoRea"
        .Columns(7).Visible = False
        .Columns(8).Caption = "CodDptoSol"
        .Columns(8).Visible = False
        .Columns(9).Caption = "CodEstadoAct"
        .Columns(9).Visible = False
        .Columns(10).Caption = "NumActPlan"
        .Columns(10).Visible = False
        .Columns(11).Caption = "CodRecurso"
        .Columns(11).Visible = False
        .Columns(12).Caption = "CodActividad"
        .Columns(12).Visible = False
        .Columns(13).Caption = "NumPeticion"
        .Columns(13).Visible = False
        .Columns(14).Caption = "NumSolicitud"
        .Columns(14).Visible = False
        .Columns(15).Caption = "NumCita"
        .Columns(15).Visible = False
        .Columns(16).Caption = "CodCama"
        .Columns(16).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
   With grdPendientes
        .Columns(0).Caption = "Fecha Cita"
        .Columns(0).Width = 1600
        .Columns(1).Caption = "Fecha en Pendiente"
        .Columns(1).Width = 1600
        .Columns(2).Caption = "Actuacion"
        .Columns(2).Width = 2000
        .Columns(3).Caption = "Recurso"
        .Columns(3).Width = 2000
        .Columns(4).Caption = "Dpto Realizador"
        .Columns(4).Width = 1000
        .Columns(5).Caption = "Dpto Solicitante"
        .Columns(5).Width = 2000
        .Columns(6).Caption = "Doctor Solicitante"
        .Columns(6).Width = 2000
        .Columns(7).Caption = "CodActuacion"
        .Columns(7).Visible = False
        .Columns(8).Caption = "CodDptoRea"
        .Columns(8).Visible = False
        .Columns(9).Caption = "CodDptoSol"
        .Columns(9).Visible = False
        .Columns(10).Caption = "CodEstadoAct"
        .Columns(10).Visible = False
        .Columns(11).Caption = "NumActPlan"
        .Columns(11).Visible = False
        .Columns(12).Caption = "CodRecurso"
        .Columns(12).Visible = False
        .Columns(13).Caption = "CodActividad"
        .Columns(13).Visible = False
        .Columns(14).Caption = "NumPeticion"
        .Columns(14).Visible = False
        .Columns(15).Caption = "NumSolicitud"
        .Columns(15).Visible = False
        .Columns(16).Caption = "NumCita"
        .Columns(16).Visible = False
        .Columns(17).Caption = "CodCama"
        .Columns(17).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub cmdAnular_Click()
Dim sql As String
Dim qry As rdoQuery
  Dim blnCancelTrans      As Boolean
  Dim blnReturn           As Boolean
  Dim intGridIndex        As Integer
  Dim vnta                As Variant

  Dim blnAnularSolicitud As Boolean
  Dim lngNumActPlan As Long
  Dim NumCita As Byte
Dim rs As rdoResultset
  Dim blnAnular As Boolean
  Dim i As Integer
  Dim strSql As String
Dim rs1 As rdoResultset
Dim intRow   As Integer
Dim vntRowBookmark As Variant
Dim rsgrid As SSDBGrid
Dim intres As String
Dim lngActAnulada As Long
Dim lngPeticion As Long
Set rsgrid = fGrid
If rsgrid Is Nothing Then Exit Sub

If rsgrid.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
If rsgrid.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
    Exit Sub
End If
If fValidarEstado(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "La actuacion no puede ser anulada porque ya tiene fecha de entrada en cola", vbExclamation, Me.Caption
    Exit Sub
End If
If Not fblnCitarModiAnular(CStr(rsgrid.Columns("Dpto Realizador").CellValue(rsgrid.SelBookmarks(0))), _
    rsgrid.Columns("CodActuacion").CellValue(rsgrid.SelBookmarks(0)), _
    rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "No tiene autorizacion a Modificar esta prueba"
    Exit Sub
End If
sql = "SELECT PR09NUMPETICION,PR0300.PR03NUMACTPEDI FROM PR0300,PR0400 WHERE PR0400.PR03NUMACTPEDI=PR0300.PR03NUMACTPEDI AND PR0400.PR04NUMACTPLAN=?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))
Set rs = qry.OpenResultset()




sql = "SELECT COUNT(*) FROM PR0300,PR0400 WHERE PR09NUMPETICION=?"
sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0400.PR37CODESTADO in (2,1) and PR04FECENTRCOLA IS NULL"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = rs(0)
Set rs1 = qry.OpenResultset()




If rs1(0) = 1 Then
    intres = MsgBox("Esta seguro de eliminar la peticion?", vbQuestion + vbYesNoCancel, Me.Caption)
    If intres = vbYes Then
    If rsgrid.Name = "grdSinCitar" Then
        Call pAnular(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)), "")
    Else
        Call pAnular(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)), rsgrid.Columns("CodCama").CellValue(rsgrid.SelBookmarks(0)))
    End If

    '        lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
'            If lngincidencia <> -1 And lngincidencia <> 0 Then
'    rsGrid.DeleteSelected
'End If
Call pIniciar
    End If
Else
   
    If Not rs.EOF Then
        Call objPipe.PipeSet("ACT", rs(1))
        Call objPipe.PipeSet("PET", rs(0))
        lngPeticion = rs(0)
           frmAnular.Show 1
           Set frmAnular = Nothing
          If objPipe.PipeExist("ACT") Then
            lngActAnulada = Val(objPipe.PipeGet("ACT"))
            If lngActAnulada = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)) Then
                rsgrid.DeleteSelected
            End If
            objPipe.PipeRemove ("ACT")
          End If
          If objPipe.PipeExist("PET") Then
            'If lngPeticion = Val(objPipe.PipeGet("PET")) Then
             'lngPeticion = rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)) Then
                rsgrid.DeleteSelected
            'End If
            objPipe.PipeRemove ("PET")
          End If
    End If
    Call pIniciar
    
End If

End Sub

Private Sub cmdAvisos_Click()
Dim vntData(1)
If txtCodpersona.Text <> "" Then
    vntData(1) = Val(txtCodpersona.Text)
    Call objSecurity.LaunchProcess("PR0580", vntData)
    Call pMensajes
End If

End Sub

Private Sub cmdBuscar_Click()
frmBuscaPersonas.Show vbModal
Set frmBuscaPersonas = Nothing
 If objPipe.PipeExist("AD_CodPersona") Then
    txtCodpersona.Text = objPipe.PipeGet("AD_CodPersona")
    objPipe.PipeRemove ("AD_CodPersona")
End If
If objPipe.PipeExist("AD_NumHistoria") Then
    txtHistoria.Text = objPipe.PipeGet("AD_NumHistoria")
    objPipe.PipeRemove ("AD_NumHistoria")
End If
Call pIniciar
End Sub

Private Sub cmdCitar_Click()
Dim rsgrid As SSDBGrid
Dim sql As String
Dim rs As rdoResultset
Set rsgrid = fGrid
If rsgrid Is Nothing Then
    Exit Sub
End If
If rsgrid.SelBookmarks.Count > 1 Then
    MsgBox "Esta opcion no est� disponible para una selecci�n multiple", vbExclamation
    Exit Sub
  End If
If rsgrid.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar primero alguna actuacion", vbExclamation
    Exit Sub
End If

If fValidarEstado(rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "La actuacion no puede ser modificada porque ya tiene fecha de entrada en cola", vbInformation, Me.Caption
    Exit Sub
End If
If Not fblnCitarModiAnular(CStr(rsgrid.Columns("CodDptoRea").CellValue(rsgrid.SelBookmarks(0))), _
    rsgrid.Columns("CodActividad").CellValue(rsgrid.SelBookmarks(0)), _
    rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0))) Then
    MsgBox "No tiene autorizacion a Modificar esta cita", vbInformation, Me.Caption
    Exit Sub
End If
If rsgrid.Columns("CodActividad").CellValue(rsgrid.SelBookmarks(0)) = constACTIV_CONSULTA Then
    Call objSecurity.LaunchProcess("AG0211", rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)))
Else
      Call objSecurity.LaunchProcess("CI1150", rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)))
End If
Call pIniciar
End Sub

Private Sub cmdCuest_Click()

End Sub

Private Sub cmdDatosAct_Click()
Dim rsgrid As SSDBGrid
Set rsgrid = fGrid
If rsgrid Is Nothing Then
    Exit Sub
End If
Call objPipe.PipeSet("PR_NActPlan", rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)))
Call objSecurity.LaunchProcess("PR0502")
End Sub

Private Sub cmdDejarPend_Click()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim blnAbort As Boolean
Dim vntBookmark
Dim i As Integer
Dim intR As Integer
Dim strDpto As String

If grdCitadas.SelBookmarks.Count = 0 Then
MsgBox "Debe seleccionar alguna actuaci�n citada", vbExclamation, Me.Caption
  Exit Sub
End If
For i = 0 To grdCitadas.SelBookmarks.Count - 1
    If Not fblnCitarModiAnular(CStr(grdCitadas.Columns("CodDptoRea").CellValue(grdCitadas.SelBookmarks(i))), _
        grdCitadas.Columns("CodActuacion").CellValue(grdCitadas.SelBookmarks(i)), grdCitadas.Columns("NumActPlan").CellValue(grdCitadas.SelBookmarks(i))) Then
        blnAbort = True
        strDpto = strDpto & ", " & grdCitadas.Columns("Dpto Realizador").CellValue(grdCitadas.SelBookmarks(i))
    End If
Next
If blnAbort Then
    MsgBox "No tiene autorizacion para Modificar actuaciones del departamento" & Left(strDpto, Len(strDpto) - 1), vbInformation, Me.Caption
    Exit Sub
End If
intR = MsgBox("Desea Dejar en PENDIENTES las actuaciones seleccionadas", vbQuestion + vbYesNo, Me.Caption)
If intR = vbYes Then
    'objApp.BeginTrans
    For i = 0 To grdCitadas.SelBookmarks.Count - 1
        sql = "UPDATE PR0400 SET PR37CODESTADO = 1 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdCitadas.Columns("NumActPlan").CellValue(grdCitadas.SelBookmarks(i))
        qry.Execute
        qry.Close
        sql = "UPDATE CI0100 SET CI01SITCITA = '4' WHERE PR04NUMACTPLAN = ?"
        sql = sql & " AND CI01SITCITA = '1'"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = grdCitadas.Columns("NumActPlan").CellValue(grdCitadas.SelBookmarks(i))
        qry.Execute
        qry.Close
'        If Err > 0 Then
'            MsgBox "Imposible Dejar en Pendiente", vbCritical
'            objApp.RollbackTrans
'            Exit Sub
        'End If
    Next
    'objApp.CommitTrans
    MsgBox "Las pruebas se han quedado pendientes de citar", vbInformation, Me.Caption
    Call pIniciar
    
End If
    
'     SQL1 = "SELECT B.PR37CODESTADO,C.CI01FECCONCERT,A.PR01CODACTUACION,A.AD02CODDPTO,C.AG11CODRECURSO,D.AD02CODDPTO,E.SG02NOM || ' ' || E.SG02APE1 || ' ' || E.SG02APE2 DOCTOR,C.CI01INDLISESPE,A.CI21CODPERSONA,B.PR04NUMACTPLAN,C.CI31NUMSOLICIT,C.CI01NUMCITA,H.PR01DESCORTA,I.AD02DESDPTO,J.AG11DESRECURSO,H.PR12CODACTIVIDAD,A.PR03NUMACTPEDI,C.CI01SITCITA,D.PR09NUMPETICION"
'     SQL1 = SQL1 & " FROM PR0300 A, PR0400 B, CI0100 C, PR0900 D, SG0200 E,PR0100 H,AD0200 I,AG1100 J"
'     SQL1 = SQL1 & " WHERE C.CI01SITCITA=" & constESTCITA_PENDRECITAR & " AND B.PR04NUMACTPLAN = ?"
'     SQL1 = SQL1 & " AND B.PR37CODESTADO =" & constESTACT_PLANIFICADA
'     SQL1 = SQL1 & " AND A.PR03NUMACTPEDI = B.PR03NUMACTPEDI"
'     SQL1 = SQL1 & " AND A.PR09NUMPETICION=D.PR09NUMPETICION (+)"
'     SQL1 = SQL1 & " AND D.SG02COD=E.SG02COD"
'     SQL1 = SQL1 & " AND B.PR04NUMACTPLAN=C.PR04NUMACTPLAN"
'     SQL1 = SQL1 & " AND B.PR01CODACTUACION=H.PR01CODACTUACION"
'     SQL1 = SQL1 & " AND A.AD02CODDPTO=I.AD02CODDPTO"
'     SQL1 = SQL1 & " AND C.AG11CODRECURSO=J.AG11CODRECURSO"
'
'     Set qry3 = objApp.rdoConnect.CreateQuery("", SQL1)
'     qry3(0) = grdDBGrid1(1).Columns("N�mero").Value
'
'     Set respuesta1c = qry3.OpenResultset()
'            sql4c = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO=" & respuesta1c(5)
'            Set rs5c = objApp.rdoConnect.OpenResultset(sql4c)
'            grdDBGrid1(2).AddItem respuesta1c!CI01FECCONCERT & Chr$(9) _
'            & respuesta1c!PR01DESCORTA & Chr$(9) _
'            & respuesta1c!AD02DESDPTO & Chr$(9) _
'            & respuesta1c!AG11DESRECURSO & Chr$(9) _
'            & rs5c(0) & Chr$(9) _
'            & respuesta1c!DOCTOR & Chr$(9) _
'            & respuesta1c!CI21CODPERSONA & Chr$(9) _
'            & respuesta1c!PR04NUMACTPLAN & Chr$(9) _
'            & respuesta1c!PR12CODACTIVIDAD & Chr$(9) _
'            & respuesta1c!CI31NUMSOLICIT & Chr$(9) _
'            & respuesta1c!CI01NUMCITA & Chr$(9) _
'            & respuesta1c!PR03NUMACTPEDI & Chr$(9) _
'            & respuesta1c!CI01SITCITA & Chr$(9) _
'            & respuesta1c!PR01CODACTUACION & Chr$(9) _
'            & respuesta1c(3) & Chr$(9) _
'            & respuesta1c!PR09NUMPETICION
'            rs5c.Close
'        grdDBGrid1(1).SelBookmarks.Remove (0)
'    Next i
'    objApp.CommitTrans
'   'Me.Refresh
'
'
'    MsgBox "Las pruebas se han quedado pendientes de citar", vbOKOnly
'End If

End Sub

Private Sub cmdImprimir_Click()
Dim sql As String
Dim rs As rdoResultset
Dim strW As String
Dim rsgrid As SSDBGrid
Set rsgrid = fGrid
If rsgrid Is Nothing Then
    Exit Sub
End If
If chkPet.Value = 1 Then
    Call pImprimir(rsgrid)
End If
If chkPac.Value = 1 Then
   strW = "PR0460J.CI21CODPERSONA = " & Val(txtCodpersona.Text)
   Call Imprimir_API(strW, "PROPAC1.RPT")
 End If
If chkDpto.Value = 1 Then
        sql = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = 4 "
        sql = sql & " AND SG02COD = '" & objSecurity.strUser & "'"
        Set rs = objApp.rdoConnect.OpenResultset(sql)
        If rs(0) = 0 Then
            strW = "PR0463J.CI21CODPERSONA= " & Val(txtCodpersona.Text)
            Call Imprimir_API(strW, "PROPAC2.rpt")
        Else
            MsgBox "Este Programa es para uso interno del Departamento", vbCritical, Me.Caption
        End If
End If
End Sub

Private Sub cmdIndSanit_Click()
Dim rsgrid As SSDBGrid
Set rsgrid = fGrid
If rsgrid Is Nothing Then
    Exit Sub
End If
If rsgrid.SelBookmarks.Count > 1 Then
    MsgBox "Esta opci�n no est� disponible para una selecci�n m�ltiple", vbExclamation, Me.Caption
    Exit Sub
End If
 Call objPipe.PipeSet("Actuacion", rsgrid.Columns("NumActPlan").CellValue(rsgrid.SelBookmarks(0)))
    frmSanitarios2.Show vbModal
    Set frmSanitarios2 = Nothing
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
blnDatosCargados = False
Call pFormatearGrids

End Sub

Private Sub grdCitadas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False
End Sub

Private Sub grdCitadas_HeadClick(ByVal ColIndex As Integer)
If grdCitadas.Rows > 0 Then Call pCitadas(ColIndex)
End Sub

Private Sub grdCitadas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
grdPendientes.SelBookmarks.RemoveAll
grdSinCitar.SelBookmarks.RemoveAll
End Sub

Private Sub grdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False

End Sub

Private Sub grdPendientes_HeadClick(ByVal ColIndex As Integer)
If grdPendientes.Rows > 0 Then Call pPendientes(ColIndex)

End Sub

Private Sub grdPendientes_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
grdCitadas.SelBookmarks.RemoveAll
grdSinCitar.SelBookmarks.RemoveAll

End Sub

Private Sub grdSinCitar_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
DispPromptMsg = False

End Sub

Private Sub grdSinCitar_HeadClick(ByVal ColIndex As Integer)
If grdSinCitar.Rows > 0 Then Call pSinCitar(ColIndex)

End Sub

Private Sub grdSinCitar_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
grdCitadas.SelBookmarks.RemoveAll
grdPendientes.SelBookmarks.RemoveAll
End Sub

Private Sub txtHistoria_Change()
If txtHistoria.Text <> strHistoria_Ant Then
    Call pLimpiar
End If
End Sub

Private Sub txtHistoria_GotFocus()
txtHistoria.SelStart = 0
txtHistoria.SelLength = Len(Trim(txtHistoria.Text))

End Sub

Private Sub txtHistoria_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
  cmdBuscar.SetFocus
End If
End Sub

Private Sub txthistoria_LostFocus()
Call pIniciar
'Call pMensajes
End Sub

Private Sub txtPaciente_GotFocus()
txtPaciente.SelStart = 0
txtPaciente.SelLength = Len(Trim(txtPaciente.Text))

End Sub
