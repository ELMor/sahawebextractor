VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Begin VB.Form frmPlantas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Consulta de Pruebas por Plantas "
   ClientHeight    =   6915
   ClientLeft      =   720
   ClientTop       =   1470
   ClientWidth     =   10110
   ControlBox      =   0   'False
   FillColor       =   &H00404040&
   FillStyle       =   0  'Solid
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6915
   ScaleWidth      =   10110
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdDesterminar 
      Caption         =   "Deshacer Terminar"
      Enabled         =   0   'False
      Height          =   375
      Left            =   8280
      TabIndex        =   14
      Top             =   6360
      Width           =   1695
   End
   Begin VB.CommandButton cmdDesInicio 
      Caption         =   "Deshacer Inicio"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2040
      TabIndex        =   13
      Top             =   6360
      Width           =   1575
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Informes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   8280
      TabIndex        =   12
      Top             =   480
      Width           =   1455
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   4920
      Index           =   0
      Left            =   0
      TabIndex        =   5
      Top             =   1320
      Width           =   10095
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4425
         Index           =   0
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   9840
         _Version        =   131078
         DataMode        =   2
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   12632256
         BackColorOdd    =   12632256
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   10
         Columns(0).Width=   1085
         Columns(0).Caption=   "Cama"
         Columns(0).Name =   "Cama"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   1773
         Columns(1).Caption=   "Num.Historia"
         Columns(1).Name =   "Num.Historia"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   5027
         Columns(2).Caption=   "Paciente"
         Columns(2).Name =   "Paciente"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   2963
         Columns(3).Caption=   "F.Planificacion"
         Columns(3).Name =   "F.Planificacion"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(4).Width=   3200
         Columns(4).Visible=   0   'False
         Columns(4).Caption=   "Cod.Actuacion"
         Columns(4).Name =   "Cod.Actuacion"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(4).Locked=   -1  'True
         Columns(5).Width=   3995
         Columns(5).Caption=   "Descripci�n"
         Columns(5).Name =   "Descripci�n"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(5).Locked=   -1  'True
         Columns(6).Width=   2461
         Columns(6).Caption=   "Estado"
         Columns(6).Name =   "Estado"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "actividad"
         Columns(7).Name =   "actividad"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "PR04NUMACTPLAN"
         Columns(8).Name =   "PRO4NUMACPTLAN"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "PR03INDCITABLE"
         Columns(9).Name =   "PR03INDCITABLE"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         _ExtentX        =   17357
         _ExtentY        =   7805
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdTerminar 
      Caption         =   "Terminar Informes"
      Enabled         =   0   'False
      Height          =   375
      Left            =   6360
      TabIndex        =   11
      Top             =   6360
      Width           =   1695
   End
   Begin VB.CommandButton cmdIniciar 
      Caption         =   "Iniciar Informes"
      Enabled         =   0   'False
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   6360
      Width           =   1575
   End
   Begin VB.CommandButton BUTCon 
      Caption         =   "Salir"
      Height          =   375
      Index           =   1
      Left            =   8160
      TabIndex        =   3
      Top             =   840
      Width           =   1455
   End
   Begin VB.CommandButton BUTCon 
      Caption         =   "Consultar"
      Height          =   375
      Index           =   0
      Left            =   6240
      TabIndex        =   2
      Top             =   840
      Width           =   1455
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   6630
      Width           =   10110
      _ExtentX        =   17833
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   3960
      TabIndex        =   1
      Tag             =   "Fecha superior de las citas"
      Top             =   840
      Width           =   1650
      _Version        =   65537
      _ExtentX        =   2910
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   0
      Left            =   1200
      TabIndex        =   0
      Tag             =   "Fecha superior de las citas"
      Top             =   840
      Width           =   1650
      _Version        =   65537
      _ExtentX        =   2910
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16777215
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1900/1/1"
      MaxDate         =   "2100/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      StartofWeek     =   2
   End
   Begin VB.Label LBLCon 
      Alignment       =   2  'Center
      BackColor       =   &H8000000E&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   2160
      TabIndex        =   9
      Top             =   240
      Width           =   5655
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   2
      Left            =   480
      TabIndex        =   8
      Top             =   960
      Width           =   555
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   3360
      TabIndex        =   7
      Top             =   960
      Width           =   510
   End
End
Attribute VB_Name = "frmPlantas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS  (REALIZACI�N DE ACTUACIONES)                      *
'* NOMBRE:                                                              *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 10 DE NOVIEMBRE DE 1997                                       *
'* DESCRIPCI�N: Consultar Estado de Actuaciones                         *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMultiInfo As New clsCWForm
Dim blnCitManual As Boolean
'numero de filas
Dim clBuscador As New Collection
Dim intFilaActual As Integer
'Constantes de estados de camas
Const iEstado_Ocupado = 2
'Constante de Estados de citas
Const iCita_Citada = 1
Dim strWh As String
Const iBUTConsulta = 0
Const iBUTSALIR = 1
Private Sub BUTCon_Click(Index As Integer)
    Select Case Index
        Case iBUTConsulta
            Screen.MousePointer = vbHourglass
            If dtcDateCombo1(0) = "" Or dtcDateCombo1(1) = "" Then
                Call objError.SetError(cwCodeMsg, "La Fecha Desde y la Fecha Hasta son obligatorios")
                Call objError.Raise
                Exit Sub
            Else
                If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
                    If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
                        Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
                        Call objError.Raise
                    Exit Sub
                    End If
                End If
            End If
            Call pLlenarGrilla(0)
            Screen.MousePointer = vbDefault
        Case iBUTSALIR
            Unload Me
    End Select
End Sub

Private Sub Check1_Click()
If Check1.Value = 1 Then
    grdDBGrid1(0).RemoveAll
    pLlenarGrilla (-1)
End If
End Sub

Private Sub cmdDesInicio_Click()
pDesInicio
End Sub

Private Sub cmdDesterminar_Click()
pDesTerminar
End Sub

Private Sub cmdIniciar_Click()
pIniciar
End Sub

Private Sub cmdTerminar_Click()
pTerminar
End Sub

Private Sub Form_Activate()
  'Dependiendo de la planta desde que se invoca ... se muestra la descripci�n
  If strDpto <> "" Then
     LBLCon(0).Caption = GetTableColumn("SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = " & strDpto)("AD02DESDPTO")
     End If
End Sub
Private Sub Form_Load()
  
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
    dtcDateCombo1(0).Date = strFecha_Sistema
    dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
Dim SQL As String
Dim qry As rdoQuery
Dim rs  As rdoResultset
If grdDBGrid1(0).Columns("actividad").Value <> "" Then
 If grdDBGrid1(0).Columns("actividad").Value = constACTIV_INFORME And grdDBGrid1(0).Columns("PR03INDCITABLE").Value = 0 Then
    If grdDBGrid1(0).Columns("estado").Value = "Planificada" Then _
     cmdIniciar.Enabled = True Else cmdIniciar.Enabled = False
        If grdDBGrid1(0).Columns("estado").Value = "Realiz�ndose" Then
            cmdTerminar.Enabled = True
            cmdDesInicio.Enabled = True
        Else
            cmdTerminar.Enabled = False
            cmdDesInicio.Enabled = False
        End If
    If grdDBGrid1(0).Columns("estado").Value = "Realizada" Then _
        cmdDesterminar.Enabled = True Else cmdDesterminar.Enabled = False
 Else
    cmdIniciar.Enabled = False
    cmdTerminar.Enabled = False
    cmdDesInicio.Enabled = False
    cmdDesterminar.Enabled = False
 End If
End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
End Sub
Private Sub pLlenarGrilla(intInformes As Integer)

Dim sStmSql As String
Dim qry     As rdoQuery
Dim rst     As rdoResultset
Dim sRow    As String

If intInformes = 0 Then
    grdDBGrid1(0).RemoveAll
    
    sStmSql = "SELECT CAMA, CI22NUMHISTORIA, PACIENTE, "
    sStmSql = sStmSql & "PR01CODACTUACION, PR01DESCORTA, PR37DESESTADO, "
    sStmSql = sStmSql & "TO_CHAR(FECHA, 'DD/MM/YYYY HH24:MI:SS') FECHA, PR12CODACTIVIDAD, "
    sStmSql = sStmSql & "PR04NUMACTPLAN, PR03INDCITABLE "
    sStmSql = sStmSql & "FROM AD1503J "
    sStmSql = sStmSql & "WHERE AD02CODDPTO = ? "
    sStmSql = sStmSql & "AND (FECHA < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(1).Text) & "','DD/MM/YYYY') OR FECHA IS NULL) " & _
                                 "AND FECHA > =  TO_DATE('" & dtcDateCombo1(0).Text & "','DD/MM/YYYY') "
    sStmSql = sStmSql & "ORDER BY CAMA, FECHA"
Else
    sStmSql = " SELECT"
    sStmSql = sStmSql & " AD1500.AD02CODDPTO,"
    sStmSql = sStmSql & " GCFN06(AD1500.AD15CODCAMA) CAMA,"
    sStmSql = sStmSql & " CI2200.CI22NUMHISTORIA,"
    sStmSql = sStmSql & " CI2200.CI21CODPERSONA,"
    sStmSql = sStmSql & " CI2200.CI22NOMBRE || ' ' ||CI2200.CI22PRIAPEL || ' '||CI2200.CI22SEGAPEL PACIENTE,"
    sStmSql = sStmSql & " PR0400.PR01CODACTUACION, PR0100.PR01DESCORTA, PR3700.PR37DESESTADO,"
    sStmSql = sStmSql & " NVL(CI0100.CI01FECCONCERT, NVL(PR0400.PR04FECPLANIFIC,PR0300.PR03FECPREFEREN)) FECHA,"
    sStmSql = sStmSql & " PR0100.PR12CODACTIVIDAD,"
    sStmSql = sStmSql & " PR0400.PR04NUMACTPLAN,"
    sStmSql = sStmSql & " PR0300.PR03INDCITABLE"
    sStmSql = sStmSql & " From AD1500, CI2200, PR0400, PR0100, CI0100, PR0300, PR3700"
    sStmSql = sStmSql & " Where AD1500.AD14CODESTCAMA = 2"
    sStmSql = sStmSql & " AND AD1500.AD07CODPROCESO = PR0400.AD07CODPROCESO (+)"
    sStmSql = sStmSql & " AND AD1500.AD01CODASISTENCI = PR0400.AD01CODASISTENCI (+)"
    sStmSql = sStmSql & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    sStmSql = sStmSql & " AND PR0400.PR01CODACTUACION = pr0100.PR01CODACTUACION (+)"
    sStmSql = sStmSql & " AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA"
    sStmSql = sStmSql & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN (+)"
    sStmSql = sStmSql & " AND PR0400.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    sStmSql = sStmSql & " AND PR0400.PR37CODESTADO = PR3700.PR37CODESTADO (+)"
    sStmSql = sStmSql & " AND PR0400.PR37CODESTADO IN (1,3)"
    sStmSql = sStmSql & " AND PR0100.PR12CODACTIVIDAD = 215"
    sStmSql = sStmSql & " AND PR03INDCITABLE = 0"
    sStmSql = sStmSql & " AND CI0100.CI01SITCITA(+) = 1"
    sStmSql = sStmSql & " AND AD1500.AD02CODDPTO = ? "
    sStmSql = sStmSql & " ORDER BY CAMA, FECHA"
End If
    Set qry = objApp.rdoConnect.CreateQuery("", sStmSql)
        qry(0) = strDpto
    Set rst = qry.OpenResultset()
    'clBuscador.Remove All
    Do While Not rst.EOF
    
        sRow = rst!Cama & Chr$(9)
        sRow = sRow & rst!CI22NUMHISTORIA & Chr$(9)
        sRow = sRow & rst!PACIENTE & Chr$(9)
        sRow = sRow & rst!fecha & Chr$(9)
        sRow = sRow & rst!PR01CODACTUACION & Chr$(9)
        sRow = sRow & rst!PR01DESCORTA & Chr$(9)
        sRow = sRow & rst!PR37DESESTADO & Chr$(9)
        sRow = sRow & rst!PR12CODACTIVIDAD & Chr$(9)
        sRow = sRow & rst!PR04NUMACTPLAN & Chr$(9)
        sRow = sRow & rst!PR03INDCITABLE & Chr$(9)
        
        grdDBGrid1(0).AddItem sRow
        rst.MoveNext
    Loop

End Sub
Private Sub pIniciar()
Dim SQL$
Dim qry As rdoQuery
Dim I As Integer
Dim lngNumActPlan As Long
Screen.MousePointer = vbHourglass

'actualizamos a la vez la fecha de entrada en cola y la de incio.
For I = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
     lngNumActPlan = grdDBGrid1(0).Columns("PR04NUMACTPLAN").CellText(grdDBGrid1(0).SelBookmarks(I))
      SQL = "UPDATE PR0400 " + _
            "SET PR04FECINIACT = SYSDATE, " + _
            "    PR37CODESTADO = ?, " + _
            "    PR04FECENTRCOLA = SYSDATE " + _
            "WHERE PR04NUMACTPLAN = ? " + _
            "      AND (AD01CODASISTENCI IS NOT NULL) " + _
            "      AND (AD07CODPROCESO IS NOT NULL)"
      
      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
      qry(0) = constESTACT_REALIZANDOSE
      qry(1) = lngNumActPlan
      qry.Execute
      
      If qry.RowsAffected > 0 Then _
        grdDBGrid1(0).Columns("Estado").Value = "Realiz�ndose"
     qry.Close
 Next I
 cmdIniciar.Enabled = False
 cmdTerminar.Enabled = True
 cmdDesInicio.Enabled = True
 
 Screen.MousePointer = vbDefault
End Sub
Private Sub pDesInicio()
Dim SQL$
Dim qry As rdoQuery
Dim I As Integer
Dim lngNumActPlan As Long
Screen.MousePointer = vbHourglass
For I = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
     lngNumActPlan = grdDBGrid1(0).Columns("PR04NUMACTPLAN").CellText(grdDBGrid1(0).SelBookmarks(I))
      SQL = "UPDATE PR0400 " + _
            "SET PR04FECINIACT = NULL, " + _
            "    PR37CODESTADO = ?, " + _
            "    PR04FECENTRCOLA = NULL " + _
            "WHERE PR04NUMACTPLAN = ? " + _
            "      AND (AD01CODASISTENCI IS NOT NULL) " + _
            "      AND (AD07CODPROCESO IS NOT NULL)"
      
      Set qry = objApp.rdoConnect.CreateQuery("", SQL)
      qry(0) = constESTACT_PLANIFICADA
      qry(1) = lngNumActPlan
      qry.Execute
      
      If qry.RowsAffected > 0 Then _
        grdDBGrid1(0).Columns("Estado").Value = "Planificada"
     qry.Close
 Next I
 cmdIniciar.Enabled = True
 cmdTerminar.Enabled = False
 cmdDesInicio.Enabled = False
 
 Screen.MousePointer = vbDefault
End Sub

Private Sub pTerminar()
Dim SQL$
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim I As Integer
Dim lngNumActPlan As Long
Screen.MousePointer = vbHourglass
    For I = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
        lngNumActPlan = grdDBGrid1(0).Columns("PR04NUMACTPLAN").CellText(grdDBGrid1(0).SelBookmarks(I))
    
        'se anota la fecha-hora de inicio en la BD
        SQL = "UPDATE PR0400 SET PR04FECFINACT = SYSDATE,"
        SQL = SQL & " PR37CODESTADO = ?"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = constESTACT_REALIZADA
        qry(1) = lngNumActPlan
        qry.Execute
        
        'Se anotan las fases
        SQL = "INSERT INTO PR0700 (pr04numactplan, pr07numfase, pr07desfase,"
        SQL = SQL & " pr07numminocupac, pr07numfase_pre, pr07numminfpre, pr07nummaxfpre,"
        SQL = SQL & " pr07indhabnatu, pr07indinifin)"
        SQL = SQL & " SELECT ?, pr06numfase, pr06desfase, pr06numminocupac, pr06numfase_pre,"
        SQL = SQL & " pr06numminfpre, pr06nummaxfpre, pr06indhabnatu, pr06indinifin"
        SQL = SQL & " FROM PR0600"
        SQL = SQL & " WHERE pr03numactpedi ="
        SQL = SQL & " (SELECT pr03numactpedi from PR0400 WHERE pr04numactplan = ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPlan
        qry(1) = qry(0)
        qry.Execute
        qry.Close
        
        'Se anotan los recursos consumidos (por defecto)
        'se mira si la actuaci�n est� citada o no
        SQL = "SELECT ci31numsolicit, ci01numcita"
        SQL = SQL & " FROM CI0100"
        SQL = SQL & " WHERE pr04numactplan = ?"
        SQL = SQL & " AND ci01sitcita = 1"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPlan
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rs.EOF Then 'actuaci�n no citada: s�lo recursos no planificables
            'se cargan los recursos de la tabla PR1400 (recursos pedidos)
            SQL = "INSERT INTO PR1000 (pr04numactplan, pr07numfase, pr10numnecesid,"
            SQL = SQL & " ag11codrecurso, pr10numunirec, pr10numminocurec, ag11codrecurso_pre)"
            SQL = SQL & " SELECT ?, pr06numfase, pr14numnecesid, ag11codrecurso,"
            SQL = SQL & " pr14numunirec, pr14numminocu, ag11codrecurso"
            SQL = SQL & " FROM PR1400"
            SQL = SQL & " WHERE pr03numactpedi ="
            SQL = SQL & " (SELECT pr03numactpedi from PR0400 WHERE pr04numactplan = ?)"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = lngNumActPlan
            qry(1) = qry(0)
            qry.Execute
        rs.Close
        qry.Close
        End If
        grdDBGrid1(0).Columns("Estado").Value = "Realizada"
        'se muestran los recursos consumidos
        Call pVerRecursosConsumidos(lngNumActPlan)
    Next I
    cmdTerminar.Enabled = False
    cmdDesterminar.Enabled = True
    cmdDesInicio.Enabled = False
    Screen.MousePointer = vbDefault
End Sub
Private Sub pDesTerminar()
Dim SQL$
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim I As Integer
Dim lngNumActPlan As Long
Screen.MousePointer = vbHourglass
    For I = 0 To grdDBGrid1(0).SelBookmarks.Count - 1
        lngNumActPlan = grdDBGrid1(0).Columns("PR04NUMACTPLAN").CellText(grdDBGrid1(0).SelBookmarks(I))
    
        'se anota la fecha-hora de inicio en la BD
        SQL = "UPDATE PR0400 SET PR04FECFINACT = NULL,"
        SQL = SQL & " PR37CODESTADO = ?"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = constESTACT_REALIZANDOSE
        qry(1) = lngNumActPlan
        qry.Execute
        
        'se eliminan las fases y los recursos
        SQL = "DELETE FROM PR1000 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPlan
        qry.Execute
        SQL = "DELETE FROM PR0700 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPlan
        qry.Execute
        

        grdDBGrid1(0).Columns("Estado").Value = "Realiz�ndose"

    Next I
    cmdDesterminar.Enabled = False
    cmdDesInicio.Enabled = True
    cmdTerminar.Enabled = True
    Screen.MousePointer = vbDefault
End Sub

Private Sub pVerRecursosConsumidos(lngNumActPlan&)
    Dim vntdatos(2)
    
    vntdatos(1) = lngNumActPlan
    vntdatos(2) = 0
    Call objSecurity.LaunchProcess("PR0503", vntdatos)
'    Set frmRecConsumidos = Nothing
 End Sub
