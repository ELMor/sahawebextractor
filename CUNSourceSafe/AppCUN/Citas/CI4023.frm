VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmAnular 
   Caption         =   "Anulaciones"
   ClientHeight    =   4305
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4695
   LinkTopic       =   "Form1"
   ScaleHeight     =   4305
   ScaleWidth      =   4695
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCommand1 
      Caption         =   "&Cancelar"
      Height          =   495
      Index           =   1
      Left            =   12180
      TabIndex        =   4
      Top             =   3480
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   3300
      TabIndex        =   3
      Top             =   3900
      Width           =   1395
   End
   Begin VB.CommandButton cmdanuPrueba 
      Caption         =   "Anular Prueba"
      Height          =   375
      Left            =   1620
      TabIndex        =   2
      Top             =   3900
      Width           =   1455
   End
   Begin VB.CommandButton cmdAnularPeticion 
      Caption         =   "Anular Peticion"
      Height          =   375
      Left            =   0
      TabIndex        =   1
      Top             =   3900
      Width           =   1455
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   3735
      Left            =   0
      TabIndex        =   0
      Top             =   60
      Width           =   4680
      _Version        =   131078
      DataMode        =   2
      RecordSelectors =   0   'False
      Col.Count       =   0
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   3
      SelectByCell    =   -1  'True
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   8255
      _ExtentY        =   6588
      _StockProps     =   79
   End
   Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
      Height          =   330
      Index           =   0
      Left            =   7050
      TabIndex        =   5
      Tag             =   "Descripci�n"
      Top             =   -1065
      Width           =   6825
      DataFieldList   =   "Column 0"
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ","
      DefColWidth     =   4471
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   4471
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "C�digo"
      Columns(0).Name =   "C�digo"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasForeColor=   -1  'True
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16777215
      Columns(1).Width=   12039
      Columns(1).Caption=   "Descripci�n"
      Columns(1).Name =   "Descripci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).HasForeColor=   -1  'True
      Columns(1).HasBackColor=   -1  'True
      Columns(1).BackColor=   16777215
      _ExtentX        =   12039
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   -2147483643
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descripci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   0
      Left            =   7020
      TabIndex        =   7
      Top             =   -1320
      Width           =   1020
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Descripci�n del alcance"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   7020
      TabIndex        =   6
      Top             =   -480
      Width           =   2070
   End
End
Attribute VB_Name = "frmAnular"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngPeticion As Long
Dim lngActuacion As Long





Private Sub cmdAnularPeticion_Click()
Dim sql As String
Dim respuesta As rdoResultset
Dim qry As rdoQuery
Dim res As String
Dim lngincidencia As Long
Dim i As Integer
Dim vntData(0 To 2)
res = MsgBox("Esta seguro de que quiere anular la Peticion", vbQuestion + vbYesNo)
If res = vbYes Then
 'lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
 'If lngincidencia <> -1 And lngincidencia <> 0 Then 'ha generado la incidencia n�mero lngIncidencia
   
    grdDBGrid1.MoveFirst
    For i = 1 To grdDBGrid1.Rows
        Call pAnular(grdDBGrid1.Columns("Actuacion").CellText(grdDBGrid1.Row), grdDBGrid1.Columns("Cama").CellText(grdDBGrid1.Row))

        grdDBGrid1.MoveNext
    Next
        
        vntData(0) = grdDBGrid1.Columns("Actuacion").CellText(grdDBGrid1.Row)
        Call objPipe.PipeSet("PET", vntData(0))
        grdDBGrid1.RemoveAll
    Unload Me
' Else
'   Exit Sub
' End If
 
Else
   Exit Sub
End If


End Sub

Private Sub cmdanuPrueba_Click()
Dim sql As String
Dim qry As rdoQuery
Dim res As String
Dim lngincidencia As Long
Dim vntData(0 To 2)
Dim respuesta As rdoResultset
  res = MsgBox("Esta seguro que quiere anular esa prueba", vbQuestion + vbYesNo)
  If res = vbYes Then
   'lngincidencia = AddInciden(ciCodInciAnulacion) 'esta variable se encuentra en el m�dulo general
   'If lngincidencia <> -1 And lngincidencia <> 0 Then  'ha generado la incidencia n�mero lngIncidencia
    Call pAnular(grdDBGrid1.Columns("Actuacion").CellValue(grdDBGrid1.SelBookmarks(0)), grdDBGrid1.Columns("Cama").CellValue(grdDBGrid1.SelBookmarks(0)))
     vntData(0) = grdDBGrid1.Columns("Actuacion").CellText(grdDBGrid1.Row)
        Call objPipe.PipeSet("ACT", vntData(0))
      Unload Me
  Else
   Exit Sub
  End If
  
'  Else
'      Exit Sub
'  End If
'End If

  
End Sub

Private Sub Command1_Click()

End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim i As Integer
Dim intres As Integer

Dim sql As String
Dim respuesta As rdoResultset
Dim qry As rdoQuery
Dim fecha As String
If objPipe.PipeExist("PET") Then
    lngPeticion = objPipe.PipeGet("PET")
    objPipe.PipeRemove ("PET")
End If
If objPipe.PipeExist("ACT") Then
    lngActuacion = objPipe.PipeGet("ACT")
    objPipe.PipeRemove ("ACT")
End If

        
sql = "SELECT PR0400.PR04NUMACTPLAN,PR0400.PR03NUMACTPEDI,"
sql = sql & "nvl(CI0100.CI01FECCONCERT,PR0400.PR04FECPLANIFIC) FECHA,"
sql = sql & "PR0100.PR01DESCORTA,"
sql = sql & "CI0100.AD15CODCAMA"
sql = sql & " From PR0400, CI0100, PR0100,PR0300"
sql = sql & " Where"
sql = sql & " PR0300.PR09NUMPETICION=?"
sql = sql & " AND PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI"
sql = sql & " AND PR0400.PR04NUMACTPLAN=CI0100.PR04NUMACTPLAN(+)"
sql = sql & " AND PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION"
sql = sql & " AND PR37CODESTADO IN(1,2) AND PR04FECENTRCOLA IS NULL"
sql = sql & " ORDER BY FECHA"

Set qry = objApp.rdoConnect.CreateQuery("", sql)
qry(0) = lngPeticion
Set respuesta = qry.OpenResultset()
With grdDBGrid1
  .Columns(0).Caption = "Actuacion" 'ACTUACION PLANIFICADA
  .Columns(0).Width = 800
  .Columns(0).Visible = False
  .Columns(1).Caption = "Act. Pedi"
  .Columns(1).Visible = False
  .Columns(2).Caption = "Fecha Cita"
  .Columns(2).Width = 2000
  .Columns(3).Caption = "Descripcion"
  .Columns(3).Width = 5000
  .Columns(4).Caption = "Cama"
  .Columns(4).Visible = False
End With

i = 0
Do While Not respuesta.EOF
    
    grdDBGrid1.AddItem respuesta!PR04NUMACTPLAN & Chr$(9) _
                        & respuesta!PR03NUMACTPEDI & Chr$(9) _
                        & respuesta!fecha & Chr$(9) _
                        & respuesta!PR01DESCORTA & Chr$(9) _
                        & respuesta!AD15CODCAMA
    If respuesta!PR03NUMACTPEDI = lngActuacion Then grdDBGrid1.SelBookmarks.Add (i)
     i = i + 1
    respuesta.MoveNext
Loop
respuesta.Close
qry.Close

'End If
End Sub

