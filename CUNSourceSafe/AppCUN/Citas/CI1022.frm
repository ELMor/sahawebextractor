VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form frmCargaTextos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga Ficheros de Texto"
   ClientHeight    =   1845
   ClientLeft      =   1950
   ClientTop       =   3480
   ClientWidth     =   7380
   ControlBox      =   0   'False
   HelpContextID   =   47
   Icon            =   "CI1022.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1845
   ScaleWidth      =   7380
   Begin ComctlLib.StatusBar StatusBar2 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   1
      Top             =   1500
      Width           =   7380
      _ExtentX        =   13018
      _ExtentY        =   609
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   2
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Width           =   8819
            MinWidth        =   8819
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Width           =   3528
            MinWidth        =   3528
            Text            =   "Hora Inicio:"
            TextSave        =   "Hora Inicio:"
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   0
      Left            =   0
      TabIndex        =   0
      Top             =   1500
      Width           =   7380
      _ExtentX        =   13018
      _ExtentY        =   0
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   1
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cdbCommonDialog1 
      Left            =   6765
      Top             =   -60
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin ComctlLib.ProgressBar prbPBar1 
      Height          =   465
      Left            =   540
      TabIndex        =   2
      Top             =   915
      Visible         =   0   'False
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   820
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuFicheros 
         Caption         =   "Personal Universitario"
         Index           =   1
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "Asegurados ACUNSA"
         Index           =   2
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "C�digos Postales"
         Index           =   3
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu mnuFicheros 
         Caption         =   "Salir"
         Index           =   5
      End
   End
End
Attribute VB_Name = "frmCargaTextos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPath As String
Dim objConnect As New clsConnect
Public strCodProv As String

Private Sub ComenzarProceso()
  
  Dim objems1 As New clsProcess
  Dim strFichero As String
  Dim strProceso As String
  Dim intFichero As Integer
  
  frmCargaTextos.StatusBar2.Panels(1).Text = "Abriendo conexiones ODBC"
 ' frmCargaTextos.Timer1.Enabled = True
  DoEvents
  objConnect.emsOpen
 ' frmCargaTextos.Timer1.Enabled = False

  'Se abre la conexion y se crea el cursor de errores
  
  If Not objConnect.blnError Then
  
       
  'Borramos todos los Errores de las tablas de ERRORES
    
  'strDelError = "DELETE FROM EMS..ERRORES"
  
  'objConnect.rdoAppConnectSal.Execute strDelError
  
 ' strDelError = "DELETE FROM EMS..ERRORES2 "
  
  'objConnect.rdoAppConnectSal.Execute strDelError
  
  'Comienza el proceso de Datos
 ' prbBar1.Visible = True
  For intFichero = 1 To cllFicherosProcesos.Count
     
      strProceso = cllNombresProcesos(intFichero)
      strFichero = cllFicherosProcesos(intFichero)
     
      StatusBar2.Panels(2).Text = StatusBar2.Panels(2).Text & Format(Now, "HH:NN:SS")
      DoEvents
     ' If stdBuscarFichero(gstrPathTxt, strFichero) Then
        Call Reprocesando(strProceso, objConnect)
     ' End If
         
  Next intFichero
  'prbBar1.Visible = False
 ' Call ReprocesoErrores(objConnect, objems1)    'Proceso de Depuraci�n de Errores
     
  Call objConnect.emsClose                      'Se cierra la conexion
  StatusBar2.Panels(1).Text = "Fin volcado de datos ( " & Format(Now, "HH:NN:SS") & " )"
  DoEvents
  End If

End Sub



Private Sub mnuFicheros_Click(intIndex As Integer)

Dim intRespuesta As Integer
Dim strDelete As String
' Se llama a la funci�n que carga las colecciones de procesos
' y ficheros
   
If intIndex = 5 Then
  Unload Me
  Exit Sub
End If
  Call objError.SetError(cwCodeQuery, "Los datos anteriores van a ser borrados." & vbCrLf & _
                     "�Desea continuar?")
  intRespuesta = objError.Raise


  If intRespuesta = vbYes Then

    Select Case intIndex
       Case 2
         strDelete = "DELETE CI0300"
         Call A�adirProceso("CargaPolizaAcunsa", "ACUNSA.TXT")
       Case 3 '"CPOSTAL.TXT"
         Load frmSelProvincias
         frmSelProvincias.Show vbModal
         Unload frmSelProvincias
         strDelete = "DELETE CI0700 WHERE CI26CODPROVI IN (" & objGen.ReplaceStr(frmSelProvincias.strCods, "'", "", 0) & ")"
         
         If strCodProv <> "" Then
           Call A�adirProceso("CargaCodigosPostales", "CPOSTAL.TXT")
         Else
           Exit Sub
         End If
    End Select
    frmCargaTextos.StatusBar2.Panels(1).Text = "Borrando registros...Espere un momento"
    DoEvents
    objApp.rdoConnect.Execute strDelete
    Call ComenzarProceso
    strCodProv = ""
  End If
End Sub
