VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "Sscala32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmPersoUni 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Mantenimiento Personal Universidad"
   ClientHeight    =   7440
   ClientLeft      =   1050
   ClientTop       =   945
   ClientWidth     =   10230
   ControlBox      =   0   'False
   Icon            =   "CI1009.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   10230
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   10230
      _ExtentX        =   18045
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Centros de Cargo"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2235
      Index           =   1
      Left            =   120
      TabIndex        =   19
      Tag             =   "Mantenimiento Centros de Cargo"
      Top             =   4920
      Width           =   10065
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1740
         Index           =   1
         Left            =   135
         TabIndex        =   20
         Top             =   360
         Width           =   9795
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   16776960
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   17277
         _ExtentY        =   3069
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Personal Universidad"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4365
      Index           =   0
      Left            =   120
      TabIndex        =   13
      Tag             =   "Mantenimiento Personal Universidad"
      Top             =   480
      Width           =   10065
      Begin TabDlg.SSTab tabTab1 
         Height          =   3750
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   405
         Width           =   9780
         _ExtentX        =   17251
         _ExtentY        =   6615
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1009.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(4)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(7)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(10)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(11)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(12)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(14)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "cboSSDBCombo1(4)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "cboSSDBCombo1(3)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "cboSSDBCombo1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "dtcDateCombo1(2)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "dtcDateCombo1(1)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "cboSSDBCombo1(1)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "dtcDateCombo1(0)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "cboSSDBCombo1(0)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(3)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(2)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(5)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(6)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(1)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "Command1"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "cmdBenefi"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).ControlCount=   30
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1009.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.CommandButton cmdBenefi 
            Caption         =   "Consul. Beneficiarios"
            Height          =   330
            Left            =   7320
            TabIndex        =   36
            Top             =   360
            Width           =   1740
         End
         Begin VB.CommandButton Command1 
            Caption         =   "& Asociar Historia"
            Default         =   -1  'True
            Height          =   375
            Left            =   1800
            TabIndex        =   35
            Top             =   2520
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI24NUMBENEFIC"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   5280
            TabIndex        =   34
            Tag             =   "N�mero Asegurado"
            Top             =   360
            Width           =   1860
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI24CODEMPLEAD"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   3285
            TabIndex        =   33
            Tag             =   "N�mero Asegurado"
            Top             =   345
            Width           =   1860
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   6
            Left            =   240
            TabIndex        =   6
            Tag             =   "N�mero de Historia"
            Top             =   2520
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI24DNI"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   3300
            TabIndex        =   4
            Tag             =   "D.N.I."
            Top             =   1800
            Width           =   1905
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI24NOMBRBENEF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   2
            Left            =   240
            TabIndex        =   1
            Tag             =   "Nombre Beneficiario|Nombre"
            Top             =   1080
            Width           =   2865
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI24APELLIBENEF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   3300
            TabIndex        =   2
            Tag             =   "Apellidos Beneficiario|Apellidos"
            Top             =   1080
            Width           =   5952
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3450
            Index           =   0
            Left            =   -74910
            TabIndex        =   15
            Top             =   90
            Width           =   9165
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16166
            _ExtentY        =   6085
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "CI08CODCOLECTI"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Colectivo|C�digo"
            Top             =   360
            Width           =   2865
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5054
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   65535
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI24FECNACIMIE"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   3
            Tag             =   "Fecha Nacimiento"
            Top             =   1800
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   -2147483643
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "CI30CODSEXO"
            Height          =   330
            Index           =   1
            Left            =   6360
            TabIndex        =   5
            Tag             =   "Sexo"
            Top             =   1800
            Width           =   2955
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5212
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5203
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI24FECINICIO"
            Height          =   330
            Index           =   1
            Left            =   3300
            TabIndex        =   7
            Tag             =   "Fecha Inicio"
            Top             =   2520
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   16777215
            BevelColorFace  =   -2147483638
            BevelColorShadow=   8421504
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI24FECFIN"
            Height          =   330
            Index           =   2
            Left            =   6360
            TabIndex        =   8
            Tag             =   "Fecha Fin"
            Top             =   2520
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   16777215
            BevelColorFace  =   -2147483638
            BevelColorShadow=   8421504
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "CI42CODRELTITU"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   9
            Tag             =   "C�digo Relaci�n Titular|Relaci�n Titular"
            Top             =   3240
            Width           =   2955
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5212
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5203
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "CI41CODMOTDESC"
            Height          =   330
            Index           =   3
            Left            =   3300
            TabIndex        =   10
            Tag             =   "C�digo Motivo Descuento|Motivo Descuento"
            Top             =   3240
            Width           =   2955
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5212
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5203
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "CI37CODENTCOLA"
            Height          =   330
            Index           =   4
            Left            =   6360
            TabIndex        =   11
            Tag             =   "Entidad Colaboradora"
            Top             =   3240
            Width           =   2955
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5212
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5203
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Entidad Colaboradora"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   6360
            TabIndex        =   32
            Top             =   3000
            Width           =   1845
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Motivo Descuento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   3300
            TabIndex        =   31
            Top             =   3000
            Width           =   1560
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Relaci�n Titular"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   240
            TabIndex        =   30
            Top             =   3000
            Width           =   1365
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   6360
            TabIndex        =   29
            Top             =   2280
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   3300
            TabIndex        =   28
            Top             =   2280
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N�mero Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   240
            TabIndex        =   27
            Top             =   2280
            Width           =   1365
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Sexo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   6360
            TabIndex        =   26
            Top             =   1560
            Width           =   435
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "D.N.I."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   3300
            TabIndex        =   25
            Top             =   1560
            Width           =   525
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Nacimiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   240
            TabIndex        =   24
            Top             =   1560
            Width           =   1545
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Beneficiario"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   5280
            TabIndex        =   23
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Empleado/Alumno"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   3300
            TabIndex        =   22
            Top             =   120
            Width           =   1545
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Colectivo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   18
            Top             =   120
            Width           =   810
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   240
            TabIndex        =   17
            Top             =   840
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellidos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   192
            Index           =   1
            Left            =   3300
            TabIndex        =   16
            Top             =   852
            Width           =   804
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   288
      Left            =   0
      TabIndex        =   12
      Top             =   7152
      Width           =   10224
      _ExtentX        =   18045
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPersoUni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim blnBorrar As Boolean



Private Sub cmdBenefi_Click()
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
    sql = "SELECT COUNT(*) FROM CI2400 WHERE  CI24CODEMPLEAD= ? AND CI08CODCOLECTI= ?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = txtText1(0)
        qry(1) = cboSSDBCombo1(0).Columns(0).Value
    Set rs = qry.OpenResultset()
    If rs(0) = 0 Then
        MsgBox "Esta Persona no esta asociana a otras", vbOKOnly, Me.Caption
    Else
      Call objPipe.PipeSet("CI24CODEMPLEAD", txtText1(0))
      Call objPipe.PipeSet("CI08CODCOLECTI", cboSSDBCombo1(0).Columns(0).Value)
      frmBeneficiarios.Show vbModal
    End If
sql = sql & " "
End Sub

Private Sub Command1_Click()
If txtText1(0).Text = "" Or txtText1(1).Text = "" Or cboSSDBCombo1(0).Text = "" Then
MsgBox "Seleccione la persona a la que desea asociar el n�mero de Historia"
Exit Sub
End If
If objPipe.PipeExist("Histuni") Then
Dim strSql As String
Dim RDO As rdoResultset
txtText1(6).Text = objPipe.PipeGet("Histuni")
strSql = "SELECT * FROM CI2400 WHERE CI08CODCOLECTI='" & cboSSDBCombo1(0).Value
strSql = strSql & "' AND CI24CODEMPLEAD='" & txtText1(0).Text & "'"
strSql = strSql & " AND CI24NUMBENEFIC='" & txtText1(1).Text & "'"
Set RDO = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
RDO.Edit
RDO("CI22NUMHISTORIA") = objPipe.PipeGet("Histuni")
RDO.Update
RDO.Close
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form padre
  Dim objMasterInfo As New clsCWForm
' Form hijo
  Dim objMultiInfo As New clsCWForm
' Guarda el nombre de la base de datos y  la tabla
  Dim strKey As String
  Dim strSql As String
  Dim rdoUni As rdoResultset
  Dim rdoUni2 As rdoResultset
  Dim CODCOLECTI As String
  Dim NUMBENEF As Integer

' **********************************
' Fin declaraci�n de variables
' **********************************

' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  If objPipe.PipeExist("PrimerNumEmplead") Then
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  Else

  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  End If
' Documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Mantenimiento de Personal Universidad"
    .cwDAT = "22-07-97"
    .cwAUT = "Jokin"
    
    .cwDES = "Esta ventana permite mantener el Personal Universidad de la CUN"
    
    .cwUPD = "22-07-97 - Jokin - Creaci�n del m�dulo"
    
    .cwEVT = "En el evento de PreValidate se realizar�..."
  End With
  
' Declaraci�n de las caracter�sticas del form padre
  With objMasterInfo
' Asignaci�n del nombre del form
    .strName = "PersoUni"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2400"
    ' Asignaci�n del nivel de acceso del usuario al formulario
    .intAllowance = cwAllowAll
     If objPipe.PipeExist("PrimerNumEmplead") And Not objPipe.PipeExist("NOMBENEF") Then
    strSql = "SELECT * FROM CI2400 WHERE CI22NUMHISTORIA=" & objPipe.PipeGet("Histuni") _
    & " AND CI24CODEMPLEAD=" & objPipe.PipeGet("PrimerNumEmplead")
            Set rdoUni = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
            .strInitialWhere = "CI08CODCOLECTI='" & rdoUni!CI08CODCOLECTI & _
                            "' AND CI24CODEMPLEAD=" & rdoUni!CI24CODEMPLEAD & _
                            " AND CI24NUMBENEFIC = " & rdoUni!CI24NUMBENEFIC
        CODCOLECTI = rdoUni!CI08CODCOLECTI
         NUMBENEF = rdoUni!CI24NUMBENEFIC
          rdoUni.Close
    Else
     If objPipe.PipeExist("PrimerNumEmplead") And objPipe.PipeExist("NOMBENEF") Then
    strSql = "SELECT * FROM CI2400 WHERE (CI22NUMHISTORIA=" & objPipe.PipeGet("Histuni") _
    & " OR CI22NUMHISTORIA IS NULL)" _
    & " AND CI24CODEMPLEAD=" & objPipe.PipeGet("PrimerNumEmplead")
            Set rdoUni = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
            .strInitialWhere = "CI08CODCOLECTI='" & rdoUni!CI08CODCOLECTI & _
                            "' AND CI24CODEMPLEAD=" & rdoUni!CI24CODEMPLEAD & _
                            " AND CI24NUMBENEFIC = " & rdoUni!CI24NUMBENEFIC
        CODCOLECTI = rdoUni!CI08CODCOLECTI
         NUMBENEF = rdoUni!CI24NUMBENEFIC
          rdoUni.Close
     End If
    End If
    
'''
'''        If objPipe.PipeExist("CodColecti") Then
'''''        strSql = "SELECT * FROM CI2400 WHERE CI24NOMBRBENEF='" & objPipe.PipeGet("NomBenef") & "'"
'''''        strSql = strSql & " AND CI24APELLIBENEF='" & objPipe.PipeGet("ApelBenef") & "'"
'''        strSql = "SELECT * FROM CI2400 WHERE CI08CODCOLECTI='" & objPipe.PipeGet("CodColecti") & "'"
'''        strSql = strSql & " AND CI24CODEMPLEAD=" & objPipe.PipeGet("PrimerNumEmplead") & " AND CI24NUMBENEFIC= " & objPipe.PipeGet("NumBenefic")
'''
'''        Set rdoUni = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
'''        .strInitialWhere = "CI08CODCOLECTI='" & rdoUni!CI08CODCOLECTI & _
'''                            "' AND CI24CODEMPLEAD=" & rdoUni!CI24CODEMPLEAD & _
'''                            " AND CI24NUMBENEFIC = " & rdoUni!CI24NUMBENEFIC
'''
'''    CODCOLECTI = rdoUni!CI08CODCOLECTI
'''    NUMBENEF = rdoUni!CI24NUMBENEFIC
'''        End If
       
  
' Reports generados por el form
    Call .objPrinter.Add("CI0011", "Listado 1 Relaci�n de Personal de Universidad")
    

    
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI08CODCOLECTI", cwAscending)
    Call .FormAddOrderField("CI24CODEMPLEAD", cwAscending)
    Call .FormAddOrderField("CI24NUMBENEFIC", cwAscending)
    

  End With
  
  
' Declaraci�n de las caracter�sticas del form hijo
  With objMultiInfo

' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(1)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = fraFrame1(0)
' Asignaci�n del objeto tab del form
    Set .tabMainTab = Nothing
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(1)
' Definici�n del tipo de form
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    
' Asignaci�n de la tabla asociada al form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI0500"
    .intAllowance = cwAllowAll
        If objPipe.PipeExist("PrimerNumEmplead") Then
            strSql = "SELECT * FROM CI0500 WHERE CI08CODCOLECTI=" & _
            "'" & CODCOLECTI & "'" & " AND CI24CODEMPLEAD=" & objPipe.PipeGet("PrimerNumEmplead") & _
            " AND CI24NUMBENEFIC=" & NUMBENEF
            Set rdoUni2 = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
            'ajo Chequear si ha encontrado datos
            If rdoUni2.EOF Then
                MsgBox "No se ha encontrado ningun registro", vbCritical
            Else
                .strInitialWhere = "CI08CODCOLECTI= '" & rdoUni2!CI08CODCOLECTI & _
                            "' AND CI24CODEMPLEAD=" & rdoUni2!CI24CODEMPLEAD & _
                            " AND CI24NUMBENEFIC = " & rdoUni2!CI24NUMBENEFIC & _
                            " AND CI06CODCENCARG = " & rdoUni2!CI06CODCENCARG
            End If
        rdoUni2.Close
        End If

' M�todo de ordenacion del form
    Call .FormAddOrderField("CI08CODCOLECTI", cwAscending)
' Campo de relaci�n entre el form padre e hijo
    Call .FormAddRelation("CI08CODCOLECTI", cboSSDBCombo1(0))
    Call .FormAddRelation("CI24CODEMPLEAD", txtText1(0))
    Call .FormAddRelation("CI24NUMBENEFIC", txtText1(1))
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
    
  End With
  
  
' Declaraci�n de las caracter�sticas del objeto ventana
 With objWinInfo

' Se a�aden los formularios a la ventana
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
' Campos que aparecen en el grid
    Call .GridAddColumn(objMultiInfo, "C�digo", "CI08CODCOLECTI")
    Call .GridAddColumn(objMultiInfo, "Empleado", "CI24CODEMPLEAD")
    Call .GridAddColumn(objMultiInfo, "Beneficiario", "CI24NUMBENEFIC")
    Call .GridAddColumn(objMultiInfo, "Centro de Cargo        ", "CI06CODCENCARG")
    Call .GridAddColumn(objMultiInfo, "Contrapartida", "CI05CONTRAPART")
    Call .GridAddColumn(objMultiInfo, "Porcentaje de Cargo", "CI05PORCCARGO")

' Se obtiene informaci�n de las caracter�sticas de
' los controles del formulario
   Call .FormCreateInfo(objMasterInfo)
    
' Se establecen los colores de los controles
    Call .FormChangeColor(objMultiInfo)
  
  
' Sql de un campo del grid 1 del hijo
     .CtrlGetInfo(grdDBGrid1(1).Columns(6)).strSql = "SELECT CI06CODCENCARG, CI06DESCENCARG FROM " & objEnv.GetValue("DataBase") & "CI0600 ORDER BY CI06DESCENCARG"

' Valores con los que se cargaran las DbCombos
    .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT CI08CODCOLECTI, CI08DESCOLECTI FROM CI0800"
    .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT CI30CODSEXO, CI30DESSEXO FROM CI3000"
    .CtrlGetInfo(cboSSDBCombo1(2)).strSql = "SELECT CI42CODRELTITU, CI42DESRELTITU FROM CI4200"
    .CtrlGetInfo(cboSSDBCombo1(3)).strSql = "SELECT CI41CODMOTDESC, CI41DESMOTDESC FROM CI4100"
    .CtrlGetInfo(cboSSDBCombo1(4)).strSql = "SELECT CI37CODENTCOLA, CI37DESENTCOLA FROM CI3700"
            
  
   .CtrlGetInfo(grdDBGrid1(1).Columns(8)).vntMinValue = 0
   .CtrlGetInfo(grdDBGrid1(1).Columns(8)).vntMaxValue = 100
  'campos que intervienen en busquedas
   .CtrlGetInfo(txtText1(2)).blnInFind = True
   .CtrlGetInfo(txtText1(3)).blnInFind = True
   .CtrlGetInfo(txtText1(6)).blnInFind = True
   .CtrlGetInfo(txtText1(5)).blnInFind = True

If Not objPipe.PipeExist("Histuni") Then
Command1.Visible = False
End If
If objPipe.PipeExist("Histuni") Then
    If objPipe.PipeGet("Histuni") = 0 Then
     Command1.Enabled = False
    End If
End If
' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    
    Call .WinStabilize
  
  End With
' se ocultan columnas clave del hijo
  grdDBGrid1(1).Columns(3).Visible = False
  grdDBGrid1(1).Columns(4).Visible = False
  grdDBGrid1(1).Columns(5).Visible = False
' Se oculta el formulario de splash
 Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim lngCEmp As Long
Dim strCodColec As String

On Error GoTo Canceltrans
If objPipe.PipeExist("CI24CODEMPLEAD") Then
    lngCEmp = objPipe.PipeGet("CI24CODEMPLEAD")
    objPipe.PipeRemove ("CI24CODEMPLEAD")
Else
    lngCEmp = 0
End If
If objPipe.PipeExist("CI08CODCOLECTI") Then
    strCodColec = objPipe.PipeGet("CI08CODCOLECTI")
    objPipe.PipeRemove ("CI08CODCOLECTI")
Else
    strCodColec = ""
End If

If strFormName = "PersoUni" Then
    If lngCEmp <> 0 And strCodColec <> "" Then
        objApp.BeginTrans
            sql = "DELETE FROM CI0500 WHERE"
            sql = sql & " CI24CODEMPLEAD= ? AND CI08CODCOLECTI= ?"
            sql = sql & " AND CI24NUMBENEFIC <> 0"
            
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = lngCEmp
                qry(1) = strCodColec
            qry.Execute
            qry.Close
            sql = "DELETE FROM CI2400 WHERE "
            sql = sql & "CI24CODEMPLEAD= ? AND CI08CODCOLECTI= ?"
            sql = sql & " AND CI24NUMBENEFIC <> 0"
            Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = lngCEmp
                qry(1) = strCodColec
            qry.Execute
            qry.Close
        objApp.CommitTrans
    End If
End If
Exit Sub
Canceltrans:
    objApp.RollbackTrans
    MsgBox "No se han podido eliminar los beneficiarios", vbExclamation, Me.Caption
End Sub

Private Sub objWinInfo_cwPostValidate(ByVal strFormName As String, blnCancel As Boolean)
  If IsDate(dtcDateCombo1(2).Date) And IsDate(dtcDateCombo1(1).Date) Then
    If DateDiff("d", dtcDateCombo1(2).Date, dtcDateCombo1(1).Date) > 0 Then
       Call objError.SetError(cwCodeMsg, "La Fecha Fin es menor que la Fecha Inicial")
       Call objError.Raise
       blnCancel = True
    End If
  End If
  If IsDate(dtcDateCombo1(0).Date) Then
    If CDate(dtcDateCombo1(0).Date) > Format(objGen.GetDBDateTime, "DD/MM/YYYY") Then
       Call objError.SetError(cwCodeMsg, "Fecha Nacimiento Erronea")
       Call objError.Raise
       blnCancel = True
    End If
  End If
 End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim sql As String
Dim QyConsulta As rdoQuery
Dim RsRespuesta As rdoResultset

Dim SQL1 As String
Dim rsRespuesta1 As rdoResultset
Dim SQL2 As String
Dim RsRespuesta2 As rdoResultset
Dim QyUpdate As rdoQuery
Dim sfecini As String
Dim sfecfin As String
Dim i As Byte
Dim blnCargo As Boolean
On Error Resume Next
Err = 0
blnCargo = True

sql = "SELECT EMPL.CI22NUMHISTORIA HISTORIA, EMPL.CI08CODCOLECTI COLECTIVO, "
sql = sql & "EMPL.CI24CODEMPLEAD EMPLEADO, EMPL.CI24NUMBENEFIC BENEFICIARIO, "
sql = sql & "EMPL.CI24FECINICIO FECINICIO, EMPL.CI24FECFIN FECFIN, "
sql = sql & "EMPL.CI41CODMOTDESC MOTDESC, EMPL.CI37CODENTCOLA ENTIDAD, "
sql = sql & "EMPL.CI42CODRELTITU RELACION, EMPL.CI24DNI DNI, EMPL.CI30CODSEXO SEXO, "
sql = sql & "EMPL.CI24FECNACIMIE FECNACIMIEN, "
If blnCargo Then
   sql = sql & "CARGO.CI06CODCENCARG CENTROCARGO, CARGO.CI05CONTRAPART CONTRAPART, "
   sql = sql & "CARGO.CI05PORCCARGO PORCENTAJE, "
End If
sql = sql & "EMPL.CI24APELLIBENEF||','||EMPL.CI24NOMBRBENEF NOM "
sql = sql & "FROM CI2400 EMPL, CI0500 CARGO "
sql = sql & "WHERE EMPL.CI08CODCOLECTI = ? AND EMPL.CI24CODEMPLEAD = ? AND "
sql = sql & "EMPL.CI24NUMBENEFIC = ?"
If blnCargo Then
   sql = sql & " AND EMPL.CI08CODCOLECTI = CARGO.CI08CODCOLECTI(+) AND "
   sql = sql & "EMPL.CI24CODEMPLEAD = CARGO.CI24CODEMPLEAD(+) AND "
   sql = sql & "EMPL.CI24NUMBENEFIC = CARGO.CI24NUMBENEFIC(+)"
End If
Set QyConsulta = objApp.rdoConnect.CreateQuery("", sql)
   QyConsulta(0) = cboSSDBCombo1(0).Value
   QyConsulta(1) = txtText1(0).Text
   QyConsulta(2) = txtText1(1).Text
Set RsRespuesta = QyConsulta.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
If RsRespuesta.EOF Then
   RsRespuesta.Close
   QyConsulta.Close
   Exit Sub
End If 'RsRespuesta.EOF
sfecini = Format(RsRespuesta!FECINICIO, "DD/MM/YYYY")


If txtText1(1).Text = 0 Then
    SQL1 = "SELECT CI24NUMBENEFIC FROM CI2400 WHERE CI24CODEMPLEAD=" & RsRespuesta!EMPLEADO & " AND CI08CODCOLECTI= '" & RsRespuesta!COLECTIVO & "'"
    Set rsRespuesta1 = objApp.rdoConnect.OpenResultset(SQL1)
    Do While Not rsRespuesta1.EOF
         SQL2 = "UPDATE CI2400 SET CI41CODMOTDESC=?,CI37CODENTCOLA=?,CI24FECINICIO=TO_DATE(?,'DD/MM/YYYY'),CI24FECFIN=TO_DATE(?,'DD/MM/YYYY') WHERE CI24CODEMPLEAD=? AND CI24NUMBENEFIC=? AND CI08CODCOLECTI=?"
         Set QyUpdate = objApp.rdoConnect.CreateQuery("", SQL2)
         QyUpdate(0) = RsRespuesta!MOTDESC
         QyUpdate(1) = RsRespuesta!ENTIDAD
         QyUpdate(2) = sfecini
         If IsNull(RsRespuesta!FECFIN) Then
          QyUpdate(3) = Null
         Else
          sfecfin = Format(RsRespuesta!FECFIN, "DD/MM/YYYY")
          QyUpdate(3) = sfecfin
         End If
         QyUpdate(4) = RsRespuesta!EMPLEADO
         QyUpdate(5) = rsRespuesta1!CI24NUMBENEFIC
         QyUpdate(6) = RsRespuesta!COLECTIVO
         QyUpdate.Execute
         
        
         QyUpdate.Close
         rsRespuesta1.MoveNext
    Loop
    rsRespuesta1.Close
End If
RsRespuesta.Close
sfecfin = ""
sfecini = ""


End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim msg As String
Dim intR As Integer
msg = ""
If strFormName = "PersoUni" Then
    If txtText1(1).Text = "0" Then
        sql = "SELECT CI24APELLIBENEF||', '||CI24NOMBRBENEF BEN, "
        sql = sql & "CI22NUMHISTORIA FROM CI2400 WHERE "
        sql = sql & "CI24CODEMPLEAD= ? AND CI08CODCOLECTI= ?"
        sql = sql & " AND CI24NUMBENEFIC <> 0"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = txtText1(0).Text
            qry(1) = cboSSDBCombo1(0).Columns(0).Value
        Set rs = qry.OpenResultset()
        While Not rs.EOF
            msg = msg & "Hist: " & IIf(IsNull(rs!CI22NUMHISTORIA), " ", rs!CI22NUMHISTORIA) & _
                  "  Benf: " & rs!BEN & Chr$(13)
            rs.MoveNext
        Wend
        If msg <> "" Then
            intR = MsgBox("Las siguientes personas son beneficiarias " & Chr$(13) & _
                msg & "�Desea Continuar?", vbYesNo + vbQuestion, Me.Caption)
            If intR = vbNo Then
                blnCancel = True
            Else
                Call objPipe.PipeSet("CI24CODEMPLEAD", txtText1(0).Text)
                Call objPipe.PipeSet("CI08CODCOLECTI", cboSSDBCombo1(0).Columns(0).Value)
            End If
        End If
    End If
End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim strWhere As String
  Dim strOrder As String
  
  If strFormName = "PersoUni" Then
    With objWinInfo.FormPrinterDialog(True, "")
      intReport = .Selected
      If intReport > 0 Then
        strWhere = objWinInfo.DataGetWhere(True)
        If Not objGen.IsStrEmpty(.objFilter.strWhere) Then
           strWhere = strWhere & IIf(objGen.IsStrEmpty(strWhere), " WHERE ", " AND ")
           strWhere = strWhere & .objFilter.strWhere
        End If
        If Not objGen.IsStrEmpty(.objFilter.strOrderBy) Then
          strOrder = " ORDER BY " & .objFilter.strOrderBy
        End If
        Call .ShowReport(strWhere, strOrder)
      End If
    End With
  End If
 
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
  grdDBGrid1(1).Columns(3).Text = cboSSDBCombo1(0).Value
  grdDBGrid1(1).Columns(4).Text = objWinInfo.CtrlGet(txtText1(0))
  grdDBGrid1(1).Columns(5).Text = objWinInfo.CtrlGet(txtText1(1))
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del ssDBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub


Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


