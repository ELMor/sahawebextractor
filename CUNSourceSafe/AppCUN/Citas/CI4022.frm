VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmdatoshistoricos 
   Caption         =   "Citas. Datos Hist�ricos"
   ClientHeight    =   3960
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8565
   LinkTopic       =   "Form1"
   ScaleHeight     =   3960
   ScaleWidth      =   8565
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdsalir 
      Caption         =   "&Salir"
      Height          =   435
      Left            =   7200
      TabIndex        =   23
      Top             =   3540
      Width           =   1335
   End
   Begin VB.Frame dresponsables 
      Caption         =   "Responsable Familiar"
      ForeColor       =   &H00800000&
      Height          =   2100
      Left            =   0
      TabIndex        =   11
      Top             =   1380
      Width           =   8580
      Begin VB.Frame ddireccion 
         Caption         =   "Direcci�n"
         ForeColor       =   &H00800000&
         Height          =   1200
         Left            =   60
         TabIndex        =   14
         Top             =   720
         Width           =   8445
         Begin VB.TextBox txttfono 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   7200
            Locked          =   -1  'True
            TabIndex        =   24
            Top             =   720
            Width           =   1155
         End
         Begin VB.TextBox txtlocalidaddir 
            BackColor       =   &H00C0C0C0&
            Height          =   300
            Left            =   5880
            Locked          =   -1  'True
            MaxLength       =   30
            TabIndex        =   16
            Top             =   240
            Width           =   2475
         End
         Begin VB.TextBox txtdir 
            BackColor       =   &H00C0C0C0&
            Height          =   300
            Left            =   900
            Locked          =   -1  'True
            MaxLength       =   40
            TabIndex        =   15
            Top             =   720
            Width           =   5820
         End
         Begin SSDataWidgets_B.SSDBCombo cbopaisdir 
            Height          =   300
            Left            =   540
            TabIndex        =   17
            Top             =   240
            Width           =   1710
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   12632256
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16776960
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16776960
            _ExtentX        =   3016
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   12632256
         End
         Begin SSDataWidgets_B.SSDBCombo cboprovdir 
            Height          =   300
            Left            =   3060
            TabIndex        =   18
            Top             =   240
            Width           =   1845
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            AutoRestore     =   0   'False
            _Version        =   131078
            DataMode        =   2
            GroupHeaders    =   0   'False
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   12632256
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "cod"
            Columns(0).Name =   "cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(0).HasBackColor=   -1  'True
            Columns(0).BackColor=   16777215
            Columns(1).Width=   3200
            Columns(1).Caption=   "des"
            Columns(1).Name =   "des"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(1).HasBackColor=   -1  'True
            Columns(1).BackColor=   16777215
            _ExtentX        =   3254
            _ExtentY        =   529
            _StockProps     =   93
            BackColor       =   12632256
         End
         Begin VB.Label Label2 
            Caption         =   "Tfno."
            Height          =   255
            Left            =   6780
            TabIndex        =   25
            Top             =   780
            Width           =   375
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Provincia:"
            Height          =   195
            Index           =   22
            Left            =   2280
            TabIndex        =   22
            Top             =   300
            Width           =   705
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Pais:"
            Height          =   195
            Index           =   23
            Left            =   90
            TabIndex        =   21
            Top             =   300
            Width           =   345
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Localidad:"
            Height          =   195
            Index           =   24
            Left            =   5040
            TabIndex        =   20
            Top             =   300
            Width           =   735
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Direccion"
            Height          =   195
            Index           =   26
            Left            =   120
            TabIndex        =   19
            Top             =   780
            Width           =   675
         End
      End
      Begin VB.TextBox txtrespf 
         BackColor       =   &H00C0C0C0&
         Height          =   300
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   300
         Width           =   7545
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Familiar:"
         Height          =   195
         Index           =   16
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   720
      End
   End
   Begin VB.Frame dpersona 
      Caption         =   "Datos del paciente"
      ForeColor       =   &H00800000&
      Height          =   1230
      Left            =   0
      TabIndex        =   0
      Top             =   60
      Width           =   8565
      Begin VB.TextBox txtprof 
         BackColor       =   &H00C0C0C0&
         Height          =   300
         Left            =   5040
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   780
         Width           =   3315
      End
      Begin VB.TextBox txtreludn 
         BackColor       =   &H00C0C0C0&
         Height          =   300
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   720
         Width           =   2835
      End
      Begin VB.TextBox txtobser 
         Height          =   285
         Left            =   1350
         MaxLength       =   40
         TabIndex        =   6
         Top             =   4425
         Width           =   9885
      End
      Begin VB.TextBox txtpaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   300
         Left            =   2460
         Locked          =   -1  'True
         MaxLength       =   25
         TabIndex        =   2
         Top             =   300
         Width           =   6015
      End
      Begin VB.TextBox txthistoria 
         BackColor       =   &H00C0C0C0&
         Height          =   300
         Left            =   975
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   300
         Width           =   675
      End
      Begin VB.Label Label1 
         Caption         =   "Profesion"
         Height          =   375
         Left            =   4320
         TabIndex        =   10
         Top             =   840
         Width           =   735
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Observaciones:"
         Height          =   195
         Index           =   35
         Left            =   120
         TabIndex        =   7
         Top             =   4500
         Width           =   1110
      End
      Begin VB.Label lblLabel 
         AutoSize        =   -1  'True
         Caption         =   "Relacion UDN:"
         Height          =   195
         Index           =   10
         Left            =   105
         TabIndex        =   5
         Top             =   765
         Width           =   1080
      End
      Begin VB.Label lblLabel 
         Caption         =   "N� Historia:"
         Height          =   315
         Index           =   8
         Left            =   150
         TabIndex        =   4
         Top             =   300
         Width           =   825
      End
      Begin VB.Label lblLabel 
         Caption         =   "Nombre:"
         Height          =   255
         Index           =   0
         Left            =   1740
         TabIndex        =   3
         Top             =   300
         Width           =   630
      End
   End
End
Attribute VB_Name = "frmdatoshistoricos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim sql As String
Dim rs As rdoResultset


Private Sub pcargardatos()
Dim rs1 As rdoResultset
Dim rs2 As rdoResultset
If Not rs.EOF Then
    If Not IsNull(rs!CI22NUMHISTORIA) Then
        txthistoria.Text = rs!CI22NUMHISTORIA
    End If
    If Not IsNull(rs!CI22IBMPROFESION) Then
        txtprof.Text = rs!CI22IBMPROFESION
    End If
    If Not IsNull(rs!CI22IBMRELUDN) Then
        txtreludn.Text = rs!CI22IBMRELUDN
    End If
    If Not IsNull(rs!RESIBM) Then
        txtrespf.Text = rs!RESIBM
    End If
    If Not IsNull(rs!NOMBREPAC) Then
        txtpaciente.Text = (rs!NOMBREPAC)
    End If
    If Not IsNull(rs!CI22IBMRFDIRECC) Then
        txtdir.Text = rs!CI22IBMRFDIRECC
    End If
    If Not IsNull(rs!CI22IBMRFPOBLAC) Then
        txtlocalidaddir.Text = rs!CI22IBMRFPOBLAC
    End If
    If Not IsNull(rs!CI26CODPROVI_IRF) Then
        sql = "SELECT CI26DESPROVI FROM CI2600 WHERE CI26CODPROVI=" & rs!CI26CODPROVI_IRF
        Set rs1 = objApp.rdoConnect.OpenResultset(sql)
        If Not rs1.EOF Then
            cboprovdir.Text = rs1!CI26DESPROVI
            cboprovdir.Columns(0).Value = rs!CI26CODPROVI_IRF
        End If
    End If
    If Not IsNull(rs!CI19CODPAIS_IRF) Then
        sql = "SELECT CI19DESPAIS FROM CI1900 WHERE CI19CODPAIS=" & rs!CI19CODPAIS_IRF
        Set rs1 = objApp.rdoConnect.OpenResultset(sql)
        If Not rs1.EOF Then
            cbopaisdir.Text = rs1!CI19DESPAIS
            cbopaisdir.Columns(0).Value = rs!CI19CODPAIS_IRF
        End If
    End If
    If Not IsNull(rs!CI22IBMRFTELEFO) Then
        txttfono.Text = rs!CI22IBMRFTELEFO
    End If
End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
Dim lngCodPac As Long 'Codigo del paciente
If objPipe.PipeExist("PAC") Then
    lngCodPac = objPipe.PipeGet("PAC")
    objPipe.PipeRemove ("PAC")
     sql = "select CI22PRIAPEL||' '||NVL(CI22SEGAPEL,'')||', '||CI22NOMBRE NOMBREPAC,"
    sql = sql & " ci22numhistoria,"
    sql = sql & " CI22IBMPROFESION,CI22IBMRELUDN,"
    sql = sql & " CI22IBMRFPRIAPEL||' '||NVL(CI22IBMRFSEGAPEL,'')||' '||CI22IBMRFNOMBRE  RESIBM,"
    sql = sql & " CI22IBMRFDIRECC,"
    sql = sql & " CI22IBMRFPOBLAC,CI26CODPROVI_IRF,"
    sql = sql & " CI19CODPAIS_IRF,CI22IBMRFTELEFO"
    sql = sql & " From"
    sql = sql & " CI2200,CI2600,"
    sql = sql & " CI1900"
    sql = sql & " Where"
    sql = sql & " CI2200.CI26CODPROVI_IRF=CI2600.CI26CODPROVI(+)"
    sql = sql & " AND CI2200.CI19CODPAIS_IRF=CI1900.CI19CODPAIS(+)"
    sql = sql & " AND CI2200.CI21CODPERSONA=" & lngCodPac
    Set rs = objApp.rdoConnect.OpenResultset(sql)
    Call pcargardatos
End If
End Sub

