Attribute VB_Name = "Constantes"
Option Explicit

Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long 'para cargar los TreeViews a gran velocidad

' constantes para tipos de errores
Public Const gintEsErrorGrave As Integer = 9
Public Const gintEsErrorLeve As Integer = 5

' constantes para motivos de error
Public Const gstrAbrir As String = "Abrir"
Public Const gstrLeer As String = "Leer"
Public Const gstrA�adir As String = "Insertar"
Public Const gstrEditar As String = "Actualizar"
Public Const gstrEntrada As String = "Fichero"
Public Const gstrSalida As String = "Tabla"
Public Const gstrStored As String = "Stored Procedure"

' constantes para describir las tablas que se cargan en cada proceso

Public Const ACUNSA As Integer = 56

'EFS: constantes los codigos de  restrinciones
Public Const lngCodResUsu As Long = 16 'Restrincion por usuario
Public Const lngTipoPac As Long = 2 'Restricci�n por tipo de paciente
Public Const lngTipoEco As Long = 3 'Restricci�n por tipo economico
Public Const lngEdad As Long = 4 'REs. Edad
Public Const lngSexo As Long = 8 'Res. Sexo

'EFS: constante para saber si tenemos
'que citar o no el recurso de anestesisa
Public Const lngRestAnes As Long = 22 'restrincion por anestesia
Public Const lngRecurAnes As Long = 243 'Recurso general de anestesia

'Codigo del departamento de anestesia.
'PARA LA PANTALLA DE CITAS ANESTESIA
Public Const lngDptoAnes As Long = 223

'Huecos (frmHuecos)
   'Limitaciones a la b�squeda
   Public Const conMeses As Byte = 2 'N�mero de meses que se revisan para ver huecos
   Public Const conDias As Byte = 5 'N�mero de d�as con huecos que se presentan
   'Im�genes para el TreeView
   Public Const conImgDoctor As Byte = 1 'Imagen para el Doctor
   Public Const conImgDia As Byte = 2 'Imagen para el D�a
   Public Const conImgHora As Byte = 3 'Imagen para la Hora
   Public Const conImgIncl As Byte = 4 'Imagen para restricci�n incluyente
   Public Const conImgExcl As Byte = 5 'Imagen para restricci�n excluyente
   
'EFS: CONSTANTE CODIGO DE PAIS ESPA�A
Public Const intCodEsp As Integer = 45

'EFS Codigo del departamento de rayos
Public Const intDptoRayos   As Integer = 209

'constantes de recursos y doctores
Public Const constRECUR_PASTRANA = 310
Public Const constUSUARIO_PASTRANA = "HPA"
Public Const constDESUSUARIO_PASTRANA = "Dr. Pastrana"
Public Const constUSUARIO_CASADO = "MCC"

'Constantes de los Tipos de Asistencia
Public Const constASIST_HOSP = 1
Public Const constASIST_AMBUL = 2

'Constantes de los Estados de las Camas
Public Const constCAMA_LIBRE = 1
Public Const constCAMA_OCUPADA = 2
Public Const constCAMA_RESERVADA = 6

'Constantes de los Departamentos
Public Const constDPTO_QUIROFANO = 213
Public Const constDPTO_ANESTESIA = 223
Public Const constDPTO_URGENCIAS = 216
Public Const constDPTO_RAYOS = 209
Public Const constDPTO_REHABILITACION = 211

'Constantes de los estados de las actuaciones
Public Const constESTACT_PLANIFICADA = 1
Public Const constESTACT_CITADA = 2
Public Const constESTACT_REALIZANDOSE = 3
Public Const constESTACT_REALIZADA = 4
Public Const constESTACT_INFORMADA = 5
Public Const constESTACT_CANCELADA = 6

'Constantes de los estados de las citas
Public Const constESTCITA_CITADA = 1
Public Const constESTCITA_ANULADA = 2
Public Const constESTCITA_RECITADA = 3
Public Const constESTCITA_PENDRECITAR = 4
Public Const constESTCITA_RESERVADA = 5

'Constantes para los tipos de actividades
Public Const constACTIV_CONSULTA = 201
Public Const constACTIV_PRUEBA = 203
Public Const constACTIV_INTERVENCION = 207
Public Const constACTIV_HOSPITALIZACION = 209
Public Const constACTIV_INFORME = 215

'Constantes para el Tipo de Departamento
Public Const constDPTO_SERVCLINICOS = 1
Public Const constDPTO_SERVBASICOS = 2

'Constantes de los modos de asignaci�n de las citas
Public Const constCITA_PORCANTIDAD = 1
Public Const constCITA_SECUENCIAL = 2
Public Const constCITA_PORINTERVALOS = 3

'Constante para el n� m�ximo de actuaciones a citar conjuntamente en citaci�n por huecos
Public Const constMAXACTCITA = 3

'Constantes de los Tipos de Restricciones para las citas
Public Const constRESTRIC_TIPOPAC = 2
Public Const constRESTRIC_TIPOECON = 3
Public Const constRESTRIC_EDAD = 4
Public Const constRESTRIC_SEXO = 8
Public Const constRESTRIC_USUARIO = 16

'Constantes de Sexo
Public Const constSEXO_HOMBRE = 1
Public Const constSEXO_MUJER = 1
'Constantes de tipo de citaci�n
Public Const constCITA_MANUAL = 0
Public Const constCITA_ILOG = -1
'Constantes del tipo de persona
Public Const constPER_FISICA = 1
Public Const constPER_JURIDICA = 2

