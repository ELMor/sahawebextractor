VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form frmCargaMunicipios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga de Municipios y Localidades"
   ClientHeight    =   1560
   ClientLeft      =   150
   ClientTop       =   720
   ClientWidth     =   7320
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   47
   Icon            =   "CI1021.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   7320
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   1
      Top             =   1215
      Width           =   7320
      _ExtentX        =   12912
      _ExtentY        =   609
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   2
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Width           =   8819
            MinWidth        =   8819
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Object.Width           =   3528
            MinWidth        =   3528
            Text            =   "Hora Inicio:"
            TextSave        =   "Hora Inicio:"
            Key             =   ""
            Object.Tag             =   ""
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ProgressBar prbPBar1 
      Height          =   465
      Left            =   480
      TabIndex        =   0
      Top             =   345
      Visible         =   0   'False
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   820
      _Version        =   327682
      Appearance      =   1
   End
   Begin MSComDlg.CommonDialog cdbCommonDialog1 
      Left            =   6840
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuImportar 
         Caption         =   "Importar Localidades"
      End
      Begin VB.Menu mnuLinea 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSalir 
         Caption         =   "Salir"
      End
   End
End
Attribute VB_Name = "frmCargaMunicipios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strDirectorio As String
Function ObtenerLocalidad(strFichero As String) As String
  Select Case UCase(strFichero)
    Case "CASTMAN"
      ObtenerLocalidad = "CASTILLA LA MANCHA"
    Case "ANDALUCI"
      ObtenerLocalidad = "ANDALUCIA"
    Case "MELILLA"
      ObtenerLocalidad = "MELILLA"
    Case "CEUTA"
      ObtenerLocalidad = "CEUTA"
    Case "CANARIAS"
      ObtenerLocalidad = "CANARIAS"
    Case "GALICIA"
      ObtenerLocalidad = "GALICIA"
    Case "CATALU�A"
      ObtenerLocalidad = "CATALAU�A"
    Case "EXTREMA"
      ObtenerLocalidad = "EXTREMADURA"
    Case "COVALEN"
      ObtenerLocalidad = "VALENCIA"
    Case "CASTLEON"
      ObtenerLocalidad = "CASTILLA LEON"
    Case "PAISVAS"
      ObtenerLocalidad = "EUSKADI"
    Case "RIOJA"
      ObtenerLocalidad = "RIOJA"
    Case "NAVARRA"
      ObtenerLocalidad = "NAVARRA"
    Case "MURCIA"
      ObtenerLocalidad = "MURCIA"
    Case "MADRID"
      ObtenerLocalidad = "MADRID"
    Case "CANTABRI"
      ObtenerLocalidad = "CANTABRIA"
    Case "BALEARES"
      ObtenerLocalidad = "BALEARES"
    Case "ASTURIAS"
      ObtenerLocalidad = "ASTURIAS"
    Case "ARAGON"
      ObtenerLocalidad = "ARAGON"
    Case Else
      ObtenerLocalidad = ""
  End Select
End Function
'Funci�n que devuelve el Path del fichero seleccionado mediante
'la common dialog box
'Se le pasa como par�metro de entrada el camino entero del fichero
'(la propiedad filename del common dialog box)
Function PathFichero(strFileName As String) As String
  On Error Resume Next
  Dim intContador As Integer

  For intContador = Len(strFileName) To 1 Step -1
    If Mid(strFileName, intContador, 1) = "\" Then
      Exit For
    End If
  Next

  PathFichero = Mid(strFileName, 1, intContador - 1)

End Function

'Funci�n que devuelve el Nombre del fichero seleccionado mediante
'la common dialog box
'Se le pasa como par�metro de entrada el camino entero del fichero
'(la propiedad filename del common dialog box)

Function NombreFichero(strFileName As String) As String
  On Error Resume Next
  Dim intContador As Integer

  For intContador = Len(strFileName) To 1 Step -1
    If Mid(strFileName, intContador, 1) = "\" Then
      Exit For
    End If
  Next

  NombreFichero = Mid(strFileName, intContador + 1, Len(strFileName) - intContador + 1)

End Function



Private Sub mnuImportar_Click()
  Dim i As Integer, intNumFich As Integer, intRespuesta As Integer
  Dim strError As String
  Dim intErrors As Integer
  Dim rdTablaOracle As rdoResultset
  Dim strConnect As String
  Dim strPath As String
  Dim strNombrerdTabla As String
  Dim intBDCont As Integer
  Dim rdoEnv As rdoEnvironment
  Dim vntProv As Variant
  Dim vntMunicipio As Variant
  Dim rdoMunicipio As rdoResultset
  Dim rdoLocalidad As rdoResultset
  Dim strDuplicado As String
  Dim strSql As String
  Dim bdPrueba As rdoConnection
  Dim rdTabla As rdoResultset
  
  Static intCont As Integer
  'On Error GoTo Error:
  
  Call objError.SetError(cwCodeQuery, "Los datos de las Tablas de Municipios y Localidades van a ser borradas." & vbCrLf & _
                     "�Desea continuar?")
  intRespuesta = objError.Raise

  If intRespuesta = vbYes Then
    
  StatusBar1.Panels(1).Text = "Borrando Municipios y Localidades"
  DoEvents
  objApp.rdoConnect.BeginTrans
  objSecurity.RegSession
  strSql = "DELETE CI1600"
  objApp.rdoConnect.Execute strSql
  strSql = "DELETE CI1700"
  objApp.rdoConnect.Execute strSql
  
  StatusBar1.Panels(1).Text = ""
  DoEvents


  StatusBar1.Panels(1).Text = "Abriendo conexi�n con base de datos Oracle"
  DoEvents
  Set rdoMunicipio = objApp.rdoConnect.OpenResultset("select * from ci1700 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)
  Set rdoLocalidad = objApp.rdoConnect.OpenResultset("select * from ci1600 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)


  
  StatusBar1.Panels(1).Text = "Abriendo conexi�n con base de datos dBase"
  StatusBar1.Panels(2).Text = StatusBar1.Panels(2).Text & Format(Now, "HH:NN:SS")
  DoEvents
  
  Set bdPrueba = objApp.rdoEnv.OpenConnection("")
  
  For intBDCont = 0 To bdPrueba.rdoTables.Count - 1
    strNombrerdTabla = bdPrueba.rdoTables(intBDCont).Name
   
   ' Ojo!!!!!
    strSql = "select Prov,Municipio,ES,Nombre from  " & strNombrerdTabla & " where nuc='00' AND EC='00'"
    Set rdTabla = bdPrueba.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
    rdTabla.MoveLast
    prbPBar1.Min = 0
    prbPBar1.Value = 0
    prbPBar1.Max = rdTabla.RowCount
    prbPBar1.Visible = True
    
    StatusBar1.Panels(1).Text = "Volcando Localidades de " & ObtenerLocalidad(strNombrerdTabla)
    rdTabla.MoveFirst
   
 Do While Not rdTabla.EOF
     vntProv = rdTabla("Prov")
     While vntProv = rdTabla("Prov") And Not rdTabla.EOF
       vntMunicipio = rdTabla("Municipio")
       rdoMunicipio.AddNew
       rdoMunicipio(0) = vntProv
       rdoMunicipio(1) = vntMunicipio
       rdoMunicipio(2) = rdTabla("Nombre")
       rdoMunicipio.Update
      While vntMunicipio = rdTabla("Municipio") And Not rdTabla.EOF
         If strDuplicado <> Trim(rdTabla("Nombre")) Then
           rdoLocalidad.AddNew
           rdoLocalidad(0) = vntProv
           rdoLocalidad(1) = vntMunicipio
           rdoLocalidad(2) = rdTabla("ES")
           rdoLocalidad(3) = rdTabla("Nombre")
           rdoLocalidad.Update
           strDuplicado = rdTabla("Nombre")
         End If
         rdTabla.MoveNext
         If rdTabla.EOF Then
           Exit Do
         End If
         prbPBar1.Value = prbPBar1.Value + 1
         intCont = intCont + 1
         DoEvents
         If intCont > 100 Then
          rdoMunicipio.Close
          rdoLocalidad.Close
          Set rdoMunicipio = objApp.rdoConnect.OpenResultset("select * from ci1700 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)
          Set rdoLocalidad = objApp.rdoConnect.OpenResultset("select * from ci1600 WHERE CI17CODMUNICIP=0", rdOpenKeyset, rdConcurRowVer)
          intCont = 0
         End If
 
       
       Wend
     
     Wend
       
   Loop
   prbPBar1.Visible = False
   StatusBar1.Panels(1).Text = ""
   DoEvents
 Next intBDCont
 Call objError.SetError(cwCodeMsg, "Se ha terminado el volcado de Municipios y Localidades" & vbCrLf & _
         "Se han producido " & intErrors & " error/es.")
 Call objError.Raise

  objApp.rdoConnect.CommitTrans
 End If
Exit Sub
Error:
  intErrors = intErrors + 1
  intNumFich = FreeFile
  Open Format(Now, "ddmmyy") & ".log" For Append As intNumFich
  strError = "Reg n�" & prbPBar1.Value + 1 & " " & Err.Number & " " & _
            Err.Description
  Print #intNumFich, strError
  Close #intNumFich
  Call objError.SetError(cwCodeQuery, "Se ha producido el siguiente error en el " & strError & vbCrLf & "�Desea continuar con el volcado?")
  intRespuesta = objError.Raise
  If intRespuesta = vbYes Then
    Resume Next
  Else
    prbPBar1.Visible = False
    StatusBar1.Panels(1).Text = ""
    DoEvents
    objApp.rdoConnect.RollbackTrans

    Exit Sub
  End If
  StatusBar1.Panels(1).Text = ""
End Sub

Private Sub mnuPath_Click()
  cdbCommonDialog1.DialogTitle = "Seleccione el directorio donde se encuentran los ficheros"
  cdbCommonDialog1.ShowOpen
  strDirectorio = PathFichero(cdbCommonDialog1.filename)
End Sub

Private Sub mnuSalir_Click()
  Unload Me
End Sub
