VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmAsegurados 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento de Asegurados "
   ClientHeight    =   7170
   ClientLeft      =   630
   ClientTop       =   1110
   ClientWidth     =   10635
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   20000
   Icon            =   "CI1010.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7170
   ScaleWidth      =   10635
   ShowInTaskbar   =   0   'False
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   10635
      _ExtentX        =   18759
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Asegurados"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6255
      Index           =   0
      Left            =   120
      TabIndex        =   19
      Tag             =   "Mantenimiento de Asegurados"
      Top             =   480
      Width           =   10365
      Begin TabDlg.SSTab tabTab1 
         Height          =   5745
         Index           =   0
         Left            =   120
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   360
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   10134
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1010.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(4)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(10)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(11)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(12)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(13)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(14)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(15)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(16)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "cboSSDBCombo1(2)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "dtcDateCombo1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "cboSSDBCombo1(1)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "dtcDateCombo1(1)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "dtcDateCombo1(0)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(6)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(5)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(1)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(0)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(3)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(2)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "chkCheck1(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(8)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(9)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(10)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(11)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "Command1"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).ControlCount=   32
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1010.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.CommandButton Command1 
            Caption         =   "&Asociar Historia"
            Default         =   -1  'True
            Height          =   375
            Left            =   1920
            TabIndex        =   36
            Top             =   3600
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   240
            TabIndex        =   9
            Tag             =   "N�mero Historia"
            Top             =   3600
            Width           =   1635
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI03OBSERVACIO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   240
            TabIndex        =   15
            Tag             =   "Observaciones"
            Top             =   5040
            Width           =   6150
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI03EXCLUSION"
            Height          =   1050
            HelpContextID   =   30104
            Index           =   9
            Left            =   6600
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   14
            Tag             =   "Exclusiones"
            Top             =   4320
            Width           =   2950
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI03DNI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   8
            Left            =   4560
            TabIndex        =   2
            Tag             =   "D.N.I. / Pasaporte"
            Top             =   600
            Width           =   2115
         End
         Begin VB.CheckBox chkCheck1 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Corriente de Pago (s/n)"
            DataField       =   "CI03CORRIPAGO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   3420
            MaskColor       =   &H00808080&
            TabIndex        =   13
            Tag             =   "Corriente de Pago"
            Top             =   4320
            Width           =   2385
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI03NOMBRTOMAD"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   240
            TabIndex        =   5
            Tag             =   "Nombre Tomador"
            Top             =   2160
            Width           =   2950
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI03APELLITOMAD"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   3420
            TabIndex        =   6
            Tag             =   "Apellidos Tomador"
            Top             =   2160
            Width           =   6075
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI03NUMPOLIZA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "N�mero P�liza"
            Top             =   585
            Width           =   1860
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "CI03NUMASEGURA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   2400
            TabIndex        =   1
            Tag             =   "N�mero Asegurado"
            Top             =   585
            Width           =   1860
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI03NOMBRASEGU"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   7
            Tag             =   "Nombre Asegurado"
            Top             =   2880
            Width           =   2950
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            DataField       =   "CI03APELLIASEGU"
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   3420
            Locked          =   -1  'True
            TabIndex        =   8
            Tag             =   "Apellidos Asegurado"
            Top             =   2880
            Width           =   6075
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI03FECINIPOLI"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   3
            Tag             =   "Fecha de Alta"
            Top             =   1350
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   8421504
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   -2147483643
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5385
            Index           =   0
            Left            =   -74910
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   90
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   9499
            _StockProps     =   79
            BackColor       =   -2147483633
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI03FECFINPOLI"
            Height          =   330
            Index           =   1
            Left            =   2400
            TabIndex        =   4
            Tag             =   "Fecha de Baja"
            Top             =   1350
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   8421504
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "CI30CODSEXO"
            Height          =   330
            Index           =   1
            Left            =   3420
            TabIndex        =   10
            Tag             =   "Sexo"
            Top             =   3600
            Width           =   2955
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5212
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   8421504
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "CI03FECNACIMIE"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   12
            Tag             =   "Fecha Nacimiento"
            Top             =   4320
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   8421504
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2050/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "CI36CODTIPPOLI"
            Height          =   330
            Index           =   2
            Left            =   6600
            TabIndex        =   11
            Tag             =   "Tipo de P�liza"
            Top             =   3600
            Width           =   2850
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5027
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5027
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   8421504
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   240
            TabIndex        =   35
            Top             =   4800
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Exclusiones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   6615
            TabIndex        =   34
            Top             =   4080
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de P�liza"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   6600
            TabIndex        =   33
            Top             =   3360
            Width           =   1230
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Nacimiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   240
            TabIndex        =   32
            Top             =   4080
            Width           =   1545
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Sexo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   3480
            TabIndex        =   31
            Top             =   3360
            Width           =   795
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "D.N.I."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   4560
            TabIndex        =   30
            Top             =   360
            Width           =   525
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N�mero Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   240
            TabIndex        =   29
            Top             =   3360
            Width           =   1365
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellidos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   3420
            TabIndex        =   28
            Top             =   2640
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre Asegurado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   240
            TabIndex        =   27
            Top             =   2640
            Width           =   1620
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fin P�liza"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   2400
            TabIndex        =   16
            Top             =   1125
            Width           =   840
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre Tomador"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   240
            TabIndex        =   20
            Top             =   1920
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Inicio P�liza"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   240
            TabIndex        =   25
            Top             =   1125
            Width           =   1050
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Apellidos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   3420
            TabIndex        =   24
            Top             =   1920
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "P�liza"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   23
            Top             =   360
            Width           =   525
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Asegurado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2400
            TabIndex        =   22
            Top             =   360
            Width           =   915
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   21
      Top             =   6885
      Width           =   10635
      _ExtentX        =   18759
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         HelpContextID   =   20000
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAsegurados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Declaraci�n del objeto ventana
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub Command1_Click()
If txtText1(0).Text = "" Or txtText1(1).Text = "" Then
MsgBox "Seleccione la persona a la que desea asociar el n�mero de Historia"
Exit Sub
End If
If objPipe.PipeExist("numeroHistoria") Then
Dim strSql As String
Dim RDO As rdoResultset
txtText1(11).Text = objPipe.PipeGet("numeroHistoria")
strSql = "SELECT * FROM CI0300 WHERE CI03NUMPOLIZA='" & txtText1(0).Text
strSql = strSql & "' AND CI03NUMASEGURA='" & txtText1(1).Text & "'"
Set RDO = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
RDO.Edit
RDO("CI22NUMHISTORIA") = objPipe.PipeGet("numeroHistoria")
RDO.Update
RDO.Close
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form detalle
  Dim objDetailInfo As New clsCWForm
' Base de datos y tabla
  Dim strKey As String
  Dim historia As Long
    Dim numpoliza As Long
    Dim Nombre As String
    Dim Apel As String
    Dim strSql As String
    Dim rdoAcunsa As rdoResultset
  
' **********************************
' Fin declaraci�n de variables
' **********************************

' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  
    
  If objPipe.PipeExist("PrimerNumPoliza") Then
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
                                
  Else
  If strNombAsegu <> "" Then
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  Else
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  End If
                            
  End If
' Introducir la documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Mantenimiento de Asegurados Acunsa"
    .cwDAT = "11-07-97"
    .cwAUT = "Jokin"
    
    .cwDES = "Esta ventana permite mantener los asegurados de la CUN"
    
    .cwUPD = "16-05-97 - Jokin - Creaci�n del m�dulo"
    
    .cwEVT = "..."
  End With
  
' Declaraci�n de las caracter�sticas del form detalle
  With objDetailInfo
 
' Asignaci�n del nombre del form
    .strName = "Asegurados"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
' Asignaci�n de la base de datos y tabla del form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI0300"
   If objPipe.PipeExist("PrimerNumPoliza") Then
    
''     If objPipe.PipeExist("numeroHistoria") Then
        historia = objPipe.PipeGet("numeroHistoria")
        If objPipe.PipeExist("PrimerNumPoliza") Then
            numpoliza = objPipe.PipeGet("PrimerNumPoliza")
             strSql = "SELECT * FROM CI0300 WHERE CI22NUMHISTORIA=" & historia
            Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurReadOnly)
            .strInitialWhere = "CI03NUMPOLIZA=" & rdoAcunsa!CI03NUMPOLIZA & _
                            "AND CI03NUMASEGURA=" & rdoAcunsa!CI03NUMASEGURA
''            If IsNull(rdoAcunsa!CI22NUMHISTORIA) Then
''                rdoAcunsa.Edit
''                rdoAcunsa("CI22NUMHISTORIA") = objPipe.PipeGet("numeroHistoria")
''                rdoAcunsa.Update
''            End If
        End If
''
''    End If
''    If objPipe.PipeExist("Nombreac") Then
''        Nombre = objPipe.PipeGet("Nombreac")
''        If objPipe.PipeExist("Primerapac") Then
''            Apel = objPipe.PipeGet("Primerapac")
''        End If
''        If objPipe.PipeExist("Segapac") Then
''            Apel = Apel & " " & objPipe.PipeGet("Segapac")
''        End If
''        If objPipe.PipeExist("PrimerNumPoliza") Then
''            numpoliza = objPipe.PipeGet("PrimerNumPoliza")
''        End If
''        strSql = "SELECT * FROM CI0300 WHERE CI03NOMBRASEGU='" & Nombre & "'"
''        strSql = strSql & " AND CI03APELLIASEGU='" & Apel & "'"
''        Set rdoAcunsa = objApp.rdoConnect.OpenResultset(strSql, rdOpenKeyset, rdConcurValues)
''        .strInitialWhere = "CI03NUMPOLIZA=" & rdoAcunsa!CI03NUMPOLIZA & _
''                            "AND CI03NUMASEGURA=" & rdoAcunsa!CI03NUMASEGURA
''            If IsNull(rdoAcunsa!CI22NUMHISTORIA) Then
''                rdoAcunsa.Edit
''                rdoAcunsa("CI22NUMHISTORIA") = objPipe.PipeGet("numeroHistoria")
''                rdoAcunsa.Update
''            End If
''    End If
    
    
    End If
    'Ajo
    'Si viene de P.F. con nombre asegurado y apellido,
    'le hacemos la misma Where
    If strNombAsegu <> "" Then
        .strInitialWhere = " CI03NOMBRASEGU = '" & strNombAsegu & "' " & _
                    " AND CI03APELLIASEGU= '" & strApelAsegu & "' "
        'Limpio los campos
        strNombAsegu = ""
        strApelAsegu = ""
    End If
 
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI03NUMPOLIZA", cwAscending)
    
' Reports generados por el form
    'Call .objPrinter.Add("EMP1", "Listado 1 de empleados")
    'Call .objPrinter.Add("EMP2", "Listado 2 de empleados")
    'Call .objPrinter.Add("EMP3", "Listado 3 de empleados")
    
    '.blnHasMaint = True
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
' Creaci�n de los filtros de busqueda
    Call .FormCreateFilterWhere(strKey, "Asegurados")
    Call .FormAddFilterWhere(strKey, "CI03NUMPOLIZA", "N�mero P�liza", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI03NUMASEGURA", "N�mero Asegurado", cwNumeric)
    Call .FormAddFilterWhere(strKey, "CI03APELLITOMAD", "Apellido Tomador", cwString)
        
    Call .FormAddFilterOrder(strKey, "CI03NUMPOLIZA", "N�mero P�liza")
    Call .FormAddFilterOrder(strKey, "CI03NUMASEGURA", "N�mero Asegurado")
    Call .FormAddFilterOrder(strKey, "CI03APELLITOMAD", "Apellido Tomador")
  
  End With
   
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
 
' Se a�ade el formulario a la ventana
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
' Se obtiene informaci�n de las caracter�sticas de
' los controles del formulario
    Call .FormCreateInfo(objDetailInfo)
        
' Campos que intervienen en busquedas
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnReadOnly = False

  
' Valores con los que se cargara la DbCombo
    .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT CI30CODSEXO, CI30DESSEXO FROM CI3000"
    .CtrlGetInfo(cboSSDBCombo1(2)).strSql = "SELECT CI36CODTIPPOLI, CI36DESTIPPOLI FROM CI3600"
' Definici�n de controles relacionados entre s�
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(5), "dname")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(6), "loc")
        If Not objPipe.PipeExist("numeroHistoria") Then
         Command1.Visible = False
        End If
        If objPipe.PipeExist("numeroHistoria") Then
            If objPipe.PipeGet("numeroHistoria") = "" Then
             Command1.Enabled = False
            End If
        End If
   
' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    Call .WinStabilize

  End With

' Se oculta el formulario de splash
  Call objApp.SplashOff
' ajo

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
   
'4/1/99 SI > IBM
If objWinInfo.intWinStatus = cwModeSingleAddRest Then
   Call insertRegTransm(1)
ElseIf objWinInfo.intWinStatus = cwModeSingleEdit Then
   Call insertRegTransm(2)
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub insertRegTransm(bytAccion As Byte)

Dim SQL As String
Dim qryInsert As rdoQuery
Dim rstUsuario As rdoResultset

SQL = "INSERT INTO GC2800 (GC28NUMREGISTRO, GC28ESTADO, GC28ACCION, CI03NUMPOLIZA, "
SQL = SQL & "CI03NUMASEGURA, GC28NOMASEG, GC28HISTORIA, GC28USUARIO, GC28FECINICPOLIZA, "
SQL = SQL & "GC28FECEFECPOLIZA, GC28FECFINPOLIZA, GC28NOMTOMADOR, GC28DNI, GC28SEXO, "
SQL = SQL & "GC28FECNAC, GC28CORRIENTEPAGO, GC28OBSERVAC, GC28TIPOPOLIZA, GC28EXCLUSIONES1, "
SQL = SQL & "GC28EXCLUSIONES2) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
SQL = SQL & "?, ?, ?)"
Set qryInsert = objApp.rdoConnect.CreateQuery("", SQL)
   qryInsert(0) = fNextClaveSeq("GC28NUMREGISTRO")
   qryInsert(1) = 1 'Estado=1, pendiente de transmitir
   qryInsert(2) = bytAccion
   qryInsert(3) = llenar(txtText1(0).Text, 6, 0, True) 'CI03NUMPOLIZA
   qryInsert(4) = llenar(txtText1(1).Text, 6, 0, True) 'CI03NUMASEGURA
   qryInsert(5) = llenar(txtText1(6).Text & "," & txtText1(5).Text, 40, " ", False) 'GC28NOMASEG
   qryInsert(6) = llenar(txtText1(11).Text, 6, 0, True) 'GC28HISTORIA
   qryInsert(7) = llenar(objSecurity.strUser, 3, " ", False) 'GC28USUARIO
   qryInsert(8) = Format(dtcDateCombo1(0).Date, "YYYYMMDD") 'GC28FECINICPOLIZA
   qryInsert(9) = Null 'GC28FECEFECPOLIZA: no se utiliza
   qryInsert(10) = Format(dtcDateCombo1(1).Date, "YYYYMMDD") 'GC28FECFINPOLIZA
   qryInsert(11) = llenar(txtText1(3).Text & "," & txtText1(2).Text, 40, " ", False) 'GC28NOMTOMADOR
   qryInsert(12) = llenar(txtText1(8).Text, 12, " ", False) 'GC28DNI
   qryInsert(13) = cboSSDBCombo1(1).Columns(0).Text 'GC28SEXO
   qryInsert(14) = Format(dtcDateCombo1(2).Date, "YYYYMMDD")  'GC28FECNAC
   If chkCheck1(0).Value = 0 Then qryInsert(15) = "N" Else qryInsert(15) = "S" 'GC28CORRIENTEPAGO
   qryInsert(16) = llenar(txtText1(10).Text, 40, " ", False) 'GC28OBSERVAC
   qryInsert(17) = llenar(cboSSDBCombo1(2).Columns(0).Text, 3, 0, True) 'GC28TIPOPOLIZA
   qryInsert(18) = llenar(Left(txtText1(9).Text, 60), 60, " ", False) 'GC28EXCLUSIONES1
   'GC28EXCLUSIONES2
   If Len(txtText1(9).Text) > 60 Then _
      qryInsert(19) = llenar(Mid(txtText1(9).Text, 61), 60, " ", False) Else _
      qryInsert(19) = Null 'GC28EXCLUSIONES2
qryInsert.Execute
qryInsert.Close

End Sub

'Private Function fNextClaveSeq(campo$) As Long
''obtiene el siguiente c�digo a utilizar en un registro nuevo utilizando los sequences
'
'    Dim SQL$, rsMaxClave As rdoResultset
'
'    SQL = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
'    Set rsMaxClave = objApp.rdoConnect.OpenResultset(SQL)
'    fNextClaveSeq = rsMaxClave(0)
'    rsMaxClave.Close
'
'End Function
'
'Private Function llenar(strnombre As Variant, _
'intLength As Integer, strCh As String, blnIzq As Boolean) As String
'
'Dim strTextError As String
'
'If Len(strCh) <> 1 Then
'    strTextError = "Car�cter de relleno no v�lido"
''    MsgBox strTextError, vbCritical, "Error en Pac.llenar"
'    Exit Function
'End If
'
'If Len(strnombre) > intLength Then
'
'    If blnIzq Then
'        llenar = Mid(strnombre, Len(strnombre) - intLength + 1)
'    Else
'        llenar = Mid(strnombre, 1, intLength)
'    End If
'Else
'    If IsNull(strnombre) Then
'        llenar = " "
'    Else
'
'    Dim i As Integer, j As Integer
'
'        j = intLength - Len(strnombre)
'
'        llenar = ""
'        For i = 1 To j
'            llenar = llenar & strCh
'        Next i
'
'        If blnIzq = True Then
'            llenar = llenar & strnombre
'        Else
'            llenar = strnombre & llenar
'        End If
'    End If
'End If
'
'End Function
'
'
