VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmMovPacCamas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS.Historia de Movimientos del Paciente en Camas"
   ClientHeight    =   5475
   ClientLeft      =   3525
   ClientTop       =   1500
   ClientWidth     =   10485
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   10485
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame FRACitas 
      Height          =   1695
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   10290
      Begin VB.TextBox TxtCita 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   0
         Left            =   1800
         TabIndex        =   1
         Top             =   480
         Width           =   1570
      End
      Begin VB.CommandButton BUTCitas 
         Caption         =   "Perso. F�sicas"
         Height          =   375
         Index           =   2
         Left            =   7800
         TabIndex        =   5
         Top             =   480
         Width           =   1215
      End
      Begin idperson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   2355
         BackColor       =   12648384
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
   End
   Begin VB.CommandButton BUTCitas 
      Caption         =   "Consultar"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   480
      TabIndex        =   2
      Top             =   5040
      Width           =   1455
   End
   Begin VB.CommandButton BUTCitas 
      Caption         =   "Salir"
      Height          =   375
      Index           =   1
      Left            =   8385
      TabIndex        =   3
      Top             =   5040
      Width           =   1455
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   3060
      Index           =   0
      Left            =   135
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1875
      Width           =   10290
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   6
      SelectTypeRow   =   1
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   6
      Columns(0).Width=   3625
      Columns(0).Caption=   "Fecha Inicio"
      Columns(0).Name =   "Fecha Inicio"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4022
      Columns(1).Caption=   "Fecha Fin"
      Columns(1).Name =   "Fecha Fin"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1984
      Columns(2).Caption=   "Cama"
      Columns(2).Name =   "Cama"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2196
      Columns(3).Caption=   "Departamento"
      Columns(3).Name =   "Departamento"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3043
      Columns(4).Caption=   "M�dico Responsable"
      Columns(4).Name =   "M�dico Responsable"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Caption=   "T.Eco./Entidad"
      Columns(5).Name =   "T.Eco./Entidad"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   18150
      _ExtentY        =   5397
      _StockProps     =   79
      Caption         =   "Movimientos del Paciente en Cama"
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   3960
      Top             =   4305
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   5
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI0182.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI0182.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI0182.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI0182.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI0182.frx":0C68
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmMovPacCamas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

    'Datos necesarios para buscar huecos
    Dim strRecurso As String 'Almacena el recurso para el que busca los huecos disponibles
    Dim FechaActual As Date 'Almacena la Fecha en la que est� buscando Huecos
    Dim sHoraActual As String
    Dim bDePF       As Boolean
        
    'Constantes de Botones
    Const iBUTConsulta = 0
    Const iBUTSALIR = 1
    Const iBUTBuscar = 2
    
    'Constantes de cajas de texto
    Const iTXTHIS = 0           'Historia
   
    'Booleana para saber si se ha buscado por persona o por historia
    Dim bHistoria   As Boolean
    Dim bPersona    As Boolean
    
    'EFS: Variable para guardar el numero de peticion para luego insertar las
    'restricciones
    Dim lngNumPet As Long
    Dim lngNumActPlan As Long
    Dim intTipoRecur As Long
Private Sub BUTCitas_Click(Index As Integer)
    Select Case Index
        Case iBUTSALIR
            Unload Me
        Case iBUTConsulta
            Call pCargar_Historia
        Case iBUTBuscar
            If bDePF Then Exit Sub
            Screen.MousePointer = vbHourglass
            'Llamar a Mant. Personas F�sicas
            Call objSecurity.LaunchProcess("CI1017")
            Screen.MousePointer = vbHourglass
            'Si se ha seleccionado una persona...
            If lngCodPers <> 0 Then
                IdPersona1.Text = lngCodPers
            Else
                IdPersona1.Text = ""
            End If
        Screen.MousePointer = vbDefault
    End Select
End Sub

Private Sub Form_Load()

    'Consulta SQL
    Dim SQL As String
    Dim qryCombos As rdoQuery
    Dim rstCombos As rdoResultset

    Screen.MousePointer = vbArrowHourglass
    Call objApp.AddCtrl(TypeName(IdPersona1))
    Call IdPersona1.BeginControl(objApp, objGen)
   
    IdPersona1.BackColor = &HFFFF&
    'Pasamos el c�digo de persona si lo tenemos
    If lngCodPers <> 0 Then
        IdPersona1.Text = lngCodPers
        Call pCargar_Historia
        bDePF = True
    Else
        IdPersona1.Text = ""
        bDePF = False
    End If
        
    'Iniciamos las booleanas
    bHistoria = False
    bPersona = False
    Screen.MousePointer = vbDefault

End Sub

Private Sub Form_Unload(Cancel As Integer)
 Call IdPersona1.EndControl
 IdPersona1.Text = ""
End Sub

Private Sub IdPersona1_Change()

    Dim sStmSql     As String
    Dim qryPersona  As rdoQuery
    Dim rstHistoria As rdoResultset
    If bPersona Then
        bPersona = False
        Exit Sub
    End If
    If IdPersona1.Text <> "" Then
        'Buscaremos el n�mero de Hist�ria por el c�digo de Persona
        sStmSql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = ? "
        Set qryPersona = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryPersona(0) = IdPersona1.Text
        Set rstHistoria = qryPersona.OpenResultset()
        If Not IsNull(rstHistoria(0)) And Not rstHistoria.EOF Then
            TxtCita(iTXTHIS) = rstHistoria(0)
        Else
            bHistoria = True
            TxtCita(iTXTHIS) = ""
            bHistoria = False
        End If
        rstHistoria.Close
        qryPersona.Close
    Else
        bHistoria = True
        TxtCita(iTXTHIS) = ""
        bHistoria = False
    End If
    If TxtCita(iTXTHIS) <> "" Then
         BUTCitas(iBUTConsulta).Enabled = True
         BUTCitas(iBUTSALIR).Enabled = True
    Else
         BUTCitas(iBUTConsulta).Enabled = False
         BUTCitas(iBUTSALIR).Enabled = True
    End If
        
End Sub

Private Sub IdPersona1_LostFocus()
    If IdPersona1.Text <> "" And IdPersona1.historia = "" Then
        MsgBox "Persona no valida para concertar una cita. " & Chr(13) & " Seleccione a un paciente con n�mero de historia " & Chr(13) & " o complete los datos (Personas F�sicas)", vbInformation
        IdPersona1.Text = ""
        TxtCita(iTXTHIS) = ""
        IdPersona1.SetFocus
        
    End If
    
End Sub
Private Sub pCargar_Historia() 'Carga la lista de la Historia de Ingresos del paciente

'Consultas SQL
Dim sStmSql     As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset
Dim sRow        As String

'Inicilizar los ComboBox de Pruebas y Recursos
grdDBGrid1(0).RemoveAll

'Actuaciones Ingresos
sStmSql = "SELECT TO_CHAR(AD1621J.AD16FECCAMBIO,'DD/MM/YYYY HH24:MI:SS'), "
sStmSql = sStmSql & "TO_CHAR(AD1621J.AD16FECFIN,'DD/MM/YYYY HH24:MI:SS'), "
sStmSql = sStmSql & "AD1621J.CAMA, "
sStmSql = sStmSql & "AD1621J.AD02CODDPTO, "
sStmSql = sStmSql & "SG0200.SG02APE1, "
sStmSql = sStmSql & "AD1621J.CI32CODTIPECON, "
sStmSql = sStmSql & "AD1621J.CI13CODENTIDAD "
sStmSql = sStmSql & "FROM AD1621J, SG0200 "
sStmSql = sStmSql & "WHERE AD1621J.SG02COD = SG0200.SG02COD (+)"
sStmSql = sStmSql & "AND AD1621J.CI22NUMHISTORIA = ? "
sStmSql = sStmSql & "ORDER BY AD1621J.AD16FECCAMBIO DESC "
Set qryConsulta = objApp.rdoConnect.CreateQuery("", sStmSql)
   qryConsulta(0) = IdPersona1.historia
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   sRow = rstConsulta(0) & Chr(9)
   sRow = sRow & rstConsulta(1) & Chr(9)
   sRow = sRow & rstConsulta(2) & Chr(9)
   sRow = sRow & rstConsulta(3) & Chr(9)
   sRow = sRow & "Dr." & rstConsulta(4) & Chr(9)
   sRow = sRow & rstConsulta(5) & "/" & rstConsulta(6) & Chr(9)
   grdDBGrid1(0).AddItem sRow
   rstConsulta.MoveNext
Loop
rstConsulta.Close
qryConsulta.Close

End Sub

Private Sub TxtCita_Change(Index As Integer)
    
    Dim sStmSql     As String
    Dim qryPersona  As rdoQuery
    Dim rstHistoria As rdoResultset
    
    If bHistoria Then
        bHistoria = False
        Exit Sub
    End If
    'Buscaremos el c�digo de persona por el n�mero de Hist�ria
    If TxtCita(iTXTHIS) <> "" Then
        sStmSql = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA = ? "
        Set qryPersona = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryPersona(0) = TxtCita(iTXTHIS)
        Set rstHistoria = qryPersona.OpenResultset()
        If Not IsNull(rstHistoria(0)) And Not rstHistoria.EOF Then
            IdPersona1.Text = rstHistoria(0)
        Else
            bPersona = True
            IdPersona1.Text = ""
            bPersona = False
        End If
        rstHistoria.Close
        qryPersona.Close
    Else
        bPersona = True
        IdPersona1.Text = ""
        bPersona = False
    End If
    If TxtCita(iTXTHIS) <> "" Then
         BUTCitas(iBUTConsulta).Enabled = True
         BUTCitas(iBUTSALIR).Enabled = True
    Else
         BUTCitas(iBUTConsulta).Enabled = False
         BUTCitas(iBUTSALIR).Enabled = True
    End If
        
End Sub

Private Sub TxtCita_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If (Shift = 1 And KeyCode = 45) Or (Shift = 1 And KeyCode = 46) Or (Shift = 2 And KeyCode = 45) Then
        KeyCode = 0
        Shift = 0
    End If
    If KeyCode = 8 Then
        SendKeys Chr(0)
    End If
End Sub
Private Sub TxtCita_KeyPress(Index As Integer, KeyAscii As Integer)
    'Validaci�n de la tecla pulsada en funci�n de los
    'requisitos establecidos para el campo en la Tabla
    If KeyAscii = Asc("'") Then KeyAscii = 0
    If KeyAscii = Asc("�") Then KeyAscii = 0

    If (KeyAscii < Asc("0") Or KeyAscii > Asc("9")) And KeyAscii <> 8 Then
        KeyAscii = 0   'nada
    End If
End Sub
