VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmDetallePersona 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Consulta Posibles Personas Duplicadas"
   ClientHeight    =   4875
   ClientLeft      =   45
   ClientTop       =   2625
   ClientWidth     =   11910
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "CI1031.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   4875
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Personas (Posibles Duplicados)"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4125
      Index           =   0
      Left            =   90
      TabIndex        =   18
      Top             =   450
      Width           =   11745
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Aceptar"
         Height          =   390
         Index           =   1
         Left            =   7470
         TabIndex        =   27
         Top             =   3600
         Width           =   1650
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Cancelar"
         Height          =   390
         Index           =   0
         Left            =   9675
         TabIndex        =   26
         Top             =   3600
         Width           =   1650
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   3015
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   435
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   5318
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "CI1031.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "tabTab1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "CI1031.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2730
            Index           =   0
            Left            =   -74910
            TabIndex        =   29
            Top             =   90
            Width           =   10965
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19341
            _ExtentY        =   4815
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   2805
            Index           =   2
            Left            =   90
            TabIndex        =   30
            Tag             =   "Fecha Nacimiento"
            Top             =   120
            Width           =   10935
            _ExtentX        =   19288
            _ExtentY        =   4948
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            Tab             =   1
            TabsPerRow      =   5
            TabHeight       =   520
            TabCaption(0)   =   "Datos &Personales"
            TabPicture(0)   =   "CI1031.frx":0044
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "lblLabel1(43)"
            Tab(0).Control(1)=   "lblLabel1(1)"
            Tab(0).Control(2)=   "lblLabel1(5)"
            Tab(0).Control(3)=   "lblLabel1(6)"
            Tab(0).Control(4)=   "lblLabel1(8)"
            Tab(0).Control(5)=   "lblLabel1(10)"
            Tab(0).Control(6)=   "lblLabel1(11)"
            Tab(0).Control(7)=   "lblLabel1(12)"
            Tab(0).Control(8)=   "lblLabel1(13)"
            Tab(0).Control(9)=   "lblLabel1(14)"
            Tab(0).Control(10)=   "lblLabel1(15)"
            Tab(0).Control(11)=   "lblLabel1(17)"
            Tab(0).Control(12)=   "lblLabel1(18)"
            Tab(0).Control(13)=   "cboSSDBCombo1(12)"
            Tab(0).Control(14)=   "cboSSDBCombo1(2)"
            Tab(0).Control(15)=   "cboSSDBCombo1(1)"
            Tab(0).Control(16)=   "cboSSDBCombo1(0)"
            Tab(0).Control(17)=   "chkCheck1(1)"
            Tab(0).Control(18)=   "txtText1(0)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(1)"
            Tab(0).Control(20)=   "txtText1(2)"
            Tab(0).Control(21)=   "txtText1(3)"
            Tab(0).Control(22)=   "txtText1(4)"
            Tab(0).Control(23)=   "txtText1(6)"
            Tab(0).Control(24)=   "txtText1(7)"
            Tab(0).Control(25)=   "txtText1(9)"
            Tab(0).Control(26)=   "txtText1(8)"
            Tab(0).Control(27)=   "txtText1(26)"
            Tab(0).ControlCount=   28
            TabCaption(1)   =   "Datos &Nacimiento"
            TabPicture(1)   =   "CI1031.frx":0060
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "lblLabel1(42)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblLabel1(19)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "lblLabel1(20)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "lblLabel1(21)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "lblLabel1(22)"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).Control(5)=   "lblLabel1(23)"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "lblLabel1(25)"
            Tab(1).Control(6).Enabled=   0   'False
            Tab(1).Control(7)=   "cboSSDBCombo1(4)"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "cboSSDBCombo1(3)"
            Tab(1).Control(8).Enabled=   0   'False
            Tab(1).Control(9)=   "dtcDateCombo1(1)"
            Tab(1).Control(9).Enabled=   0   'False
            Tab(1).Control(10)=   "dtcDateCombo1(0)"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).Control(11)=   "txtText1(34)"
            Tab(1).Control(11).Enabled=   0   'False
            Tab(1).Control(12)=   "txtText1(32)"
            Tab(1).Control(12).Enabled=   0   'False
            Tab(1).Control(13)=   "txtText1(10)"
            Tab(1).Control(13).Enabled=   0   'False
            Tab(1).Control(14)=   "txtText1(11)"
            Tab(1).Control(14).Enabled=   0   'False
            Tab(1).Control(15)=   "txtText1(12)"
            Tab(1).Control(15).Enabled=   0   'False
            Tab(1).ControlCount=   16
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22DESLOCALID"
               Height          =   330
               HelpContextID   =   30104
               Index           =   12
               Left            =   345
               TabIndex        =   21
               Tag             =   "Localidad"
               Top             =   2280
               Width           =   5130
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22DESPROVI"
               Height          =   330
               HelpContextID   =   30104
               Index           =   11
               Left            =   5880
               TabIndex        =   20
               Tag             =   "Provincia"
               Top             =   1560
               Width           =   3570
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NUMDIRPRINC"
               Height          =   330
               HelpContextID   =   30101
               Index           =   26
               Left            =   -64695
               MaxLength       =   4
               TabIndex        =   31
               Tag             =   "C�digo Empleado|C�digo"
               Top             =   345
               Visible         =   0   'False
               Width           =   330
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NUMHISTORIA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   8
               Left            =   -67980
               TabIndex        =   12
               Tag             =   "N�mero Historia"
               Top             =   2175
               Width           =   1680
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA_REA"
               Height          =   330
               HelpContextID   =   30104
               Index           =   9
               Left            =   -66060
               MaxLength       =   10
               TabIndex        =   8
               Tag             =   "C�d. Persona Real"
               Top             =   1440
               Width           =   1635
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22TFNOMOVIL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   7
               Left            =   -68280
               TabIndex        =   7
               Tag             =   "Tel�fono Movil"
               Top             =   1440
               Width           =   2025
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NUMSEGSOC"
               Height          =   330
               HelpContextID   =   30104
               Index           =   6
               Left            =   -71295
               TabIndex        =   6
               Tag             =   "N� Seguridad Social"
               Top             =   1440
               Width           =   2730
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22DNI"
               Height          =   330
               HelpContextID   =   30104
               Index           =   4
               Left            =   -74745
               TabIndex        =   4
               Tag             =   "D.N.I|D.N.I "
               Top             =   1440
               Width           =   1725
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22SEGAPEL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   3
               Left            =   -67200
               TabIndex        =   3
               Tag             =   "Segundo Apellido|Segundo Apellido"
               Top             =   720
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22PRIAPEL"
               Height          =   330
               HelpContextID   =   30104
               Index           =   2
               Left            =   -70320
               TabIndex        =   2
               Tag             =   "Primer Apellido|Primer Apellido"
               Top             =   720
               Width           =   2850
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI22NOMBRE"
               Height          =   330
               HelpContextID   =   30104
               Index           =   1
               Left            =   -73150
               TabIndex        =   1
               Tag             =   "Nombre Persona|Nombre Persona"
               Top             =   720
               Width           =   2610
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               HelpContextID   =   30101
               Index           =   0
               Left            =   -74745
               TabIndex        =   0
               TabStop         =   0   'False
               Tag             =   "C�digo Persona|C�digo"
               Top             =   720
               Width           =   1275
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               HelpContextID   =   30104
               Index           =   10
               Left            =   1440
               TabIndex        =   17
               Tag             =   "Pa�s"
               Top             =   1560
               Width           =   2850
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Provisional"
               DataField       =   "CI22INDPROVISI"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   -65730
               TabIndex        =   13
               Tag             =   "Provisional"
               Top             =   2190
               Width           =   1290
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI17CODMUNICIP"
               Height          =   330
               HelpContextID   =   30104
               Index           =   32
               Left            =   6960
               TabIndex        =   22
               TabStop         =   0   'False
               Tag             =   "C�d.Municipio"
               Top             =   2265
               Visible         =   0   'False
               Width           =   705
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI16CODLOCALID"
               Height          =   330
               HelpContextID   =   30104
               Index           =   34
               Left            =   8025
               TabIndex        =   23
               TabStop         =   0   'False
               Tag             =   "C�d.Localidad"
               Top             =   2250
               Visible         =   0   'False
               Width           =   720
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI30CODSEXO"
               Height          =   330
               Index           =   0
               Left            =   -72795
               TabIndex        =   5
               Tag             =   "Sexo"
               Top             =   1440
               Width           =   1290
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   2355
               Columns(1).Caption=   "Sexo"
               Columns(1).Name =   "Sexo"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2275
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI14CODESTCIVI"
               Height          =   330
               Index           =   1
               Left            =   -74745
               TabIndex        =   9
               Tag             =   "Estado Civil"
               Top             =   2175
               Width           =   1725
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3043
               Columns(1).Caption=   "Estado Civil"
               Columns(1).Name =   "Estado Civil"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   3043
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI28CODRELUDN"
               Height          =   330
               Index           =   2
               Left            =   -72795
               TabIndex        =   10
               Tag             =   "Relaci�n UDN"
               Top             =   2175
               Width           =   2880
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4551
               Columns(1).Caption=   "Relaci�n UDN"
               Columns(1).Name =   "Relaci�n UDN"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5080
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI22FECNACIM"
               Height          =   330
               Index           =   0
               Left            =   360
               TabIndex        =   14
               Tag             =   "Fecha de Nacimiento"
               Top             =   840
               Width           =   1980
               _Version        =   65537
               _ExtentX        =   3492
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI22FECFALLE"
               Height          =   330
               Index           =   1
               Left            =   2880
               TabIndex        =   15
               Tag             =   "Fecha de Fallecimiento"
               Top             =   840
               Width           =   1980
               _Version        =   65537
               _ExtentX        =   3492
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               AutoSelect      =   0   'False
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI19CODPAIS"
               Height          =   330
               Index           =   3
               Left            =   360
               TabIndex        =   16
               Tag             =   "C�digo Pa�s"
               Top             =   1560
               Width           =   885
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   4022
               Columns(1).Caption=   "Pa�s"
               Columns(1).Name =   "Pa�s"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1561
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI26CODPROVI"
               Height          =   330
               Index           =   4
               Left            =   4680
               TabIndex        =   19
               Tag             =   "C�digo Provincia"
               Top             =   1560
               Width           =   915
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   1508
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3625
               Columns(1).Caption=   "Provincia"
               Columns(1).Name =   "Provincia"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   1614
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 0"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI34CODTRATAMI"
               Height          =   330
               Index           =   12
               Left            =   -69750
               TabIndex        =   11
               Tag             =   "Estado Civil"
               Top             =   2175
               Width           =   1590
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3043
               Columns(1).Caption=   "Estado Civil"
               Columns(1).Name =   "Estado Civil"
               Columns(1).DataField=   "Column 2"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   2805
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 1"
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   25
               Left            =   345
               TabIndex        =   51
               Top             =   2040
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Descripci�n Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   23
               Left            =   5880
               TabIndex        =   50
               Top             =   1320
               Width           =   1875
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   4680
               TabIndex        =   49
               Top             =   1320
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d. Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   360
               TabIndex        =   48
               Top             =   1320
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha de Fallecimiento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   20
               Left            =   2880
               TabIndex        =   47
               Top             =   600
               Width           =   1980
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha de Naciminento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   360
               TabIndex        =   46
               Top             =   600
               Width           =   1920
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�mero de Historia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   18
               Left            =   -67950
               TabIndex        =   45
               Top             =   1950
               Width           =   1635
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   " C�d. Persona Real"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   17
               Left            =   -66120
               TabIndex        =   44
               Top             =   1215
               Width           =   1665
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Relaci�n UDN"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   15
               Left            =   -72795
               TabIndex        =   43
               Top             =   1920
               Width           =   1230
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Estado Civil"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   14
               Left            =   -74745
               TabIndex        =   42
               Top             =   1920
               Width           =   1020
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono Movil"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   -68280
               TabIndex        =   41
               Top             =   1200
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�mero Seguridad Social"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   -71280
               TabIndex        =   40
               Top             =   1200
               Width           =   2160
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Sexo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   -72795
               TabIndex        =   39
               Top             =   1200
               Width           =   435
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "D.N.I"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   -74745
               TabIndex        =   38
               Top             =   1200
               Width           =   465
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Segundo Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   -67200
               TabIndex        =   37
               Top             =   480
               Width           =   1500
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Primer Apellido"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   -70320
               TabIndex        =   36
               Top             =   480
               Width           =   1275
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   -73155
               TabIndex        =   35
               Top             =   480
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   -74745
               TabIndex        =   34
               Top             =   480
               Width           =   600
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   42
               Left            =   1440
               TabIndex        =   33
               Top             =   1320
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tratamiento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   43
               Left            =   -69765
               TabIndex        =   32
               Top             =   1935
               Width           =   1020
            End
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   24
      Top             =   4590
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDetallePersona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Declaraci�n del objeto ventana
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdCommand1_Click(intIndex As Integer)
  If intIndex = 0 Then
    vntCodPersDup = 0
  Else
    vntCodPersDup = objWinInfo.objWinActiveForm.rdoCursor("CI21CODPERSONA")
  End If
  Me.Hide
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
' **********************************
' Declaraci�n de variables
' **********************************
  
' Form detalle
  Dim objDetailInfo As New clsCWForm
' Base de datos y tabla
  Dim strKey As String
  
' **********************************
' Fin declaraci�n de variables
' **********************************

' Se visualiza el formulario de splash
  Call objApp.SplashOn
  
' Creaci�n del objeto ventana
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
' Introducir la documentaci�n
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Consulta de posibles personas duplicadas"
    .cwDAT = "23-10-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite consultar las posibles personas coincidentes con la persona que se esta introduciendo"
    
    .cwUPD = "23-10-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = "En el evento de PreValidate se realizar�..."
  End With
  
' Declaraci�n de las caracter�sticas del form detalle
  With objDetailInfo
 
' Asignaci�n del nombre del form
    .strName = "Personas"
' Asignaci�n del contenedor(frame)del form
    Set .objFormContainer = fraFrame1(0)
' Asignaci�n del contenedor(frame) padre del form
    Set .objFatherContainer = Nothing
' Asignaci�n del objeto tab del form
    Set .tabMainTab = tabTab1(0)
' Asignaci�n del objeto grid del form
    Set .grdGrid = grdDBGrid1(0)
' Asignaci�n de la base de datos y tabla del form
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    .strWhere = strWherePersDup
    .intAllowance = cwAllowReadOnly
' M�todo de ordenacion del form
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
    Call .FormAddOrderField("CI21CODPERSONA", cwAscending)
    
    
  
' Asignaci�n a strKey del nombre de la base de datos y tabla
    strKey = .strDataBase & .strTable
  End With
   
' Declaraci�n de las caracter�sticas del objeto ventana
  With objWinInfo
 
' Se a�ade el formulario a la ventana
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
' Se obtiene informaci�n de las caracter�sticas de
' los controles del formulario
    Call .FormCreateInfo(objDetailInfo)
        
   .CtrlGetInfo(txtText1(0)).blnReadOnly = True
' Valores con los que se cargara la DbCombo
    .CtrlGetInfo(cboSSDBCombo1(0)).strSql = "SELECT CI30CODSEXO, CI30DESSEXO  FROM " & objEnv.GetValue("Database") & "CI3000 ORDER BY CI30DESSEXO"
    .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT CI14CODESTCIVI, CI14DESESTCIVI FROM " & objEnv.GetValue("Database") & "CI1400 ORDER BY CI14DESESTCIVI"
    .CtrlGetInfo(cboSSDBCombo1(2)).strSql = "SELECT CI28CODRELUDN, CI28DESRELUDN FROM " & objEnv.GetValue("Database") & "CI2800 ORDER BY CI28DESRELUDN"
    .CtrlGetInfo(cboSSDBCombo1(3)).strSql = "SELECT CI19CODPAIS, CI19DESPAIS FROM " & objEnv.GetValue("Database") & "CI1900 ORDER BY CI19DESPAIS"
    .CtrlGetInfo(cboSSDBCombo1(4)).strSql = "SELECT CI26CODPROVI, CI26DESPROVI FROM " & objEnv.GetValue("Database") & "CI2600 ORDER BY CI26DESPROVI"
  
' Definici�n de controles relacionados entre s�
    'Pais Nacimiento
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(3)), "CI19CODPAIS", "SELECT CI19CODPAIS, CI19DESPAIS FROM CI1900 WHERE CI19CODPAIS = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(3)), txtText1(10), "CI19DESPAIS")
           
    'Provincia Nacimiento
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(4)), "CI10DESLOCALID", "SELECT CI26CODPROVI, CI26DESPROVI FROM CI2600 WHERE CI26CODPROVI = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(4)), txtText1(11), "CI26DESPROVI")
    
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    .CtrlGetInfo(txtText1(26)).blnInGrid = False
    .CtrlGetInfo(cboSSDBCombo1(3)).blnInGrid = False
    .CtrlGetInfo(cboSSDBCombo1(4)).blnInGrid = False

' Se a�ade la ventana a la colecci�n de ventanas y se activa
    Call .WinRegister
' Se estabiliza la ventana configurando las propiedades
' de los controles
    Call .WinStabilize
  End With

' Se oculta el formulario de splash
  Call objApp.SplashOff
  tabTab1(0).Tab = 1
  objWinInfo.DataRefresh
  Me.MousePointer = vbArrow
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


