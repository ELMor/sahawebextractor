VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "crystl32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmCitasQuirofano 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citaci�n de Quir�fano"
   ClientHeight    =   8490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11520
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8490
   ScaleWidth      =   11520
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdAnular 
      Caption         =   "A&nular Intervenci�n"
      Height          =   375
      Left            =   2040
      TabIndex        =   35
      Top             =   8040
      Width           =   1815
   End
   Begin Crystal.CrystalReport crReport1 
      Left            =   10800
      Top             =   60
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   10440
      TabIndex        =   30
      Top             =   1500
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdPetPendientes 
      Caption         =   "&Interv. Pendientes"
      Height          =   375
      Left            =   5880
      TabIndex        =   29
      Top             =   8040
      Width           =   1815
   End
   Begin VB.CommandButton cmdCitar 
      Caption         =   "&Ci&tar"
      Height          =   375
      Left            =   9960
      TabIndex        =   28
      Top             =   8040
      Width           =   1335
   End
   Begin VB.CommandButton cmdMantCitasPac 
      Caption         =   "&Mant. Citas Paciente"
      Height          =   375
      Left            =   3960
      TabIndex        =   27
      Top             =   8040
      Width           =   1815
   End
   Begin VB.CommandButton cmdDatosAct 
      Caption         =   "&Datos Intervenci�n"
      Height          =   375
      Left            =   120
      TabIndex        =   26
      Top             =   8040
      Width           =   1815
   End
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   10320
      Top             =   60
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   "|"
   End
   Begin VB.Frame Frame5 
      Caption         =   "Consulta"
      ForeColor       =   &H00C00000&
      Height          =   2355
      Left            =   60
      TabIndex        =   16
      Top             =   0
      Width           =   6015
      Begin VB.Frame fraTipoDpto 
         Caption         =   "Tipo Dpto."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1155
         Left            =   4560
         TabIndex        =   36
         Top             =   600
         Width           =   1335
         Begin VB.OptionButton optTipoDpto 
            Caption         =   "Dpto. Rea."
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   38
            Top             =   780
            Width           =   1095
         End
         Begin VB.OptionButton optTipoDpto 
            Caption         =   "Dpto. Sol."
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   37
            Top             =   360
            Value           =   -1  'True
            Width           =   1155
         End
      End
      Begin VB.CheckBox chkAnteriores 
         Caption         =   "Ver interv. atrasadas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   2040
         Width           =   2355
      End
      Begin VB.Frame Frame3 
         Caption         =   "Tipo Asistencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404040&
         Height          =   1635
         Left            =   2880
         TabIndex        =   22
         Top             =   600
         Width           =   1635
         Begin VB.OptionButton optTipoAsist 
            Caption         =   "Hospitalizados"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   25
            Top             =   780
            Width           =   1455
         End
         Begin VB.OptionButton optTipoAsist 
            Caption         =   "Todos"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   24
            Top             =   360
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton optTipoAsist 
            Caption         =   "Ambulatorios"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   23
            Top             =   1200
            Width           =   1455
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Fechas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   1395
         Left            =   120
         TabIndex        =   17
         Top             =   600
         Width           =   2715
         Begin VB.CheckBox chkFecha 
            Caption         =   "Incluir interv. sin fecha planif."
            Height          =   255
            Left            =   180
            TabIndex        =   31
            Top             =   1080
            Value           =   1  'Checked
            Width           =   2475
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
            Height          =   315
            Left            =   900
            TabIndex        =   18
            Top             =   240
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
            Height          =   315
            Left            =   900
            TabIndex        =   19
            Top             =   720
            Width           =   1695
            _Version        =   65537
            _ExtentX        =   2990
            _ExtentY        =   556
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            StartofWeek     =   2
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   21
            Top             =   300
            Width           =   675
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            Height          =   255
            Index           =   1
            Left            =   180
            TabIndex        =   20
            Top             =   780
            Width           =   615
         End
      End
      Begin VB.CommandButton cmdConsultar 
         Caption         =   "&Consultar"
         Height          =   375
         Left            =   4620
         TabIndex        =   0
         Top             =   1860
         Width           =   1215
      End
      Begin SSDataWidgets_B.SSDBCombo cboDptos 
         Height          =   315
         Left            =   1200
         TabIndex        =   33
         Top             =   240
         Width           =   3075
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4974
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Desig"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5424
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Dpto.:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   300
         TabIndex        =   34
         Top             =   300
         Width           =   795
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "B�squeda"
      ForeColor       =   &H00C00000&
      Height          =   2355
      Left            =   6180
      TabIndex        =   9
      Top             =   0
      Width           =   4155
      Begin VB.CommandButton cmdB 
         Caption         =   "&Anterior"
         Height          =   375
         Index           =   1
         Left            =   3060
         TabIndex        =   15
         Top             =   1740
         Width           =   975
      End
      Begin VB.CommandButton cmdB 
         Caption         =   "Si&guiente"
         Height          =   375
         Index           =   0
         Left            =   3060
         TabIndex        =   10
         Top             =   1260
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   3
         Left            =   900
         TabIndex        =   4
         Top             =   1860
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   2
         Left            =   900
         TabIndex        =   3
         Top             =   1380
         Width           =   1995
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   3060
         TabIndex        =   5
         Top             =   360
         Width           =   975
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   1
         Left            =   900
         TabIndex        =   2
         Top             =   900
         Width           =   1995
      End
      Begin VB.TextBox txtBuscar 
         Height          =   285
         Index           =   0
         Left            =   900
         TabIndex        =   1
         Top             =   420
         Width           =   975
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "2� Apellido"
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   14
         Top             =   1920
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "1� Apellido"
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   1440
         Width           =   750
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   300
         TabIndex        =   12
         Top             =   960
         Width           =   555
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         Height          =   195
         Index           =   0
         Left            =   300
         TabIndex        =   11
         Top             =   480
         Width           =   525
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10440
      TabIndex        =   8
      Top             =   1980
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Intervenciones planificadas"
      ForeColor       =   &H00C00000&
      Height          =   5535
      Left            =   60
      TabIndex        =   6
      Top             =   2400
      Width           =   11385
      Begin SSDataWidgets_B.SSDBGrid grdActuaciones 
         Height          =   5205
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   11145
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   19659
         _ExtentY        =   9181
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmCitasQuirofano"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strFecDesde$, strFecHasta$
Dim lngDpto&
Dim blnQuirofano As Boolean
'variable utilizadas para b�squedas
Dim intBuscar%, blnBuscarOtro As Boolean
Dim cllBuscar As New Collection 'historia|nombre|apel1|apel2|numActPlan|n� persona

Private Sub cboDptos_Click()
    lngDpto = cboDptos.Columns(0).Value
End Sub

Private Sub cmdAnular_Click()
    Call pAnularAct
End Sub

Private Sub cmdB_Click(Index As Integer)
    If Index = 0 Then intBuscar = intBuscar + 1 Else intBuscar = intBuscar - 1
    If intBuscar < 0 Then intBuscar = 0
    blnBuscarOtro = True
    cmdBuscar_Click
End Sub

Private Sub cmdBuscar_Click()
    Dim intN%, i%, blnE As Boolean
    
    If fBlnBuscar Then
        cmdB(0).Enabled = True: cmdB(1).Enabled = True
        If blnBuscarOtro = True Then
            blnBuscarOtro = False
            If intBuscar = 0 Then intBuscar = 1: Exit Sub
        Else
            intBuscar = 1
        End If
        
        For i = 1 To cllBuscar.Count
            blnE = False
            awk1 = cllBuscar(i)
            If UCase(Left$(awk1.F(2), Len(txtBuscar(1).Text))) = UCase(txtBuscar(1).Text) _
            And UCase(Left$(awk1.F(3), Len(txtBuscar(2).Text))) = UCase(txtBuscar(2).Text) _
            And UCase(Left$(awk1.F(4), Len(txtBuscar(3).Text))) = UCase(txtBuscar(3).Text) Then
                If txtBuscar(0).Text = "" Then
                    blnE = True
                Else
                    If awk1.F(1) = txtBuscar(0).Text Then blnE = True
                End If
            End If
            If blnE Then
                intN = intN + 1
                If intN = intBuscar Then
                    grdActuaciones.SelBookmarks.RemoveAll
                    LockWindowUpdate Me.hwnd
                    grdActuaciones.MoveFirst
                    grdActuaciones.MoveRecords i - 1
                    LockWindowUpdate 0&
                    grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
                    cmdB(0).SetFocus
                    Exit Sub
                End If
            End If
        Next i
        
        If intBuscar = 1 Then
            MsgBox "No se ha encontrado ning�n registro que cumpla las condiciones de b�squeda.", vbInformation, Me.Caption
            Exit Sub
        Else
            intBuscar = intBuscar - 1
            Exit Sub
        End If
    Else
        blnBuscarOtro = False
    End If
End Sub

Private Sub cmdCitar_Click()
    Dim msg$

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0: msg = "No se ha seleccionado ninguna Intervenci�n."
    Case Else: Call pCitar
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdConsultar_Click()
    Call pActualizar(-1)
End Sub

Private Sub cmdDatosAct_Click()
    Dim msg$, lngNumActPlan&

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Intervenci�n."
    Case 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        'Acceso a los Datos de la Actuaci�n
        Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
        Call objSecurity.LaunchProcess("PR0502")
    Case Else
        msg = "Debe Ud. seleccionar una �nica Intervenci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdImprimir_Click()
''    Dim msg$, strW$, strFD$, strFH$
''
''    If grdActuaciones.Rows = 0 Then Exit Sub
''
''    crReport1.ReportFileName = objApp.strReportsPath & "PR0506.rpt"
''
''    strW = "{PR0462J.AD02CODDPTO} = " & constDPTO_ANESTESIA
''    strW = strW & " AND {PR0462J.PR37CODESTADO} = " & constESTACT_PLANIFICADA
''    If chkFecha.Value = 0 Then
''        strFD = strFecDesde
''        strFH = DateAdd("d", 1, strFecHasta)
''        strW = strW & " AND {PR0462J.PR04FECPLANIFIC} >= DATE(" & Year(strFD) & "," & Month(strFD) & "," & Day(strFD) & ")"
''        strW = strW & " AND {PR0462J.PR04FECPLANIFIC} < DATE(" & Year(strFH) & "," & Month(strFH) & "," & Day(strFH) & ")"
''    Else
''        strW = strW & " AND ISNULL({PR0462J.PR04FECPLANIFIC})"
''    End If
''    Select Case lngTipoAsist
''    Case constASIST_HOSP: strW = strW & " AND NOT ISNULL({PR0462J.AD15CODCAMA})"
''    Case constASIST_AMBUL: strW = strW & " AND ISNULL({PR0462J.AD15CODCAMA})"
''    End Select
''
''    With crReport1
''        .PrinterCopies = 1
''        .Destination = crptToPrinter
''        .SelectionFormula = "(" & strW & ")"
''        .Connect = objApp.rdoConnect.Connect
''        .DiscardSavedData = True
''        Screen.MousePointer = vbHourglass
''        .Action = 1
''        Screen.MousePointer = vbDefault
''    End With
End Sub

Private Sub cmdMantCitasPac_Click()
    Dim msg$, vntdatos As Variant

    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ning�n Paciente."
    Case Else 'vale cualquier actuaci�n del paciente
        'Acceso al Mantenimiento de Citas del Paciente
        vntdatos = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
        Call objSecurity.LaunchProcess("CI0202", vntdatos)
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdPetPendientes_Click()
    Call objSecurity.LaunchProcess("PR0550")
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub



Private Sub dcboDesde_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub dcboHasta_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboDesde.Date = dcboHasta.Date
    End If
End Sub

Private Sub Form_Load()
    Dim strHoy$
    
    blnQuirofano = fBlnEsQuirofano
    
    Call pFormatearGrids
    Call pCargarDptos
    
    strHoy = strFecha_Sistema
    dcboDesde.Date = strHoy
    'dcboHasta.Date = DateAdd("ww", 1, strHoy)
    dcboHasta.Date = dcboDesde.Date
    dcboDesde.BackColor = objApp.objUserColor.lngMandatory
    dcboHasta.BackColor = objApp.objUserColor.lngMandatory
    cboDptos.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub pActualizar(intCol%)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim lngTipoAsist&
    Dim lngTipoDpto&
    Dim strFecPlan$, strHoraPlan$, strCama$
    Dim strApel1$, strApel2$
    Dim n%
    Static sqlOrder$
        
    Screen.MousePointer = vbHourglass

    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PACIENTE, AD02DESDPTO, PR04FECPLANIFIC, PR01DESCORTA" 'Opci�n por defecto: Paciente
    Else
        Select Case UCase(grdActuaciones.Columns(intCol).Caption)
        Case UCase("Dpto. Sol."): sqlOrder = " ORDER BY AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("Cita"): sqlOrder = " ORDER BY PR03INDCITABLE DESC, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("Urg."): sqlOrder = " ORDER BY PR48CODURGENCIA, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("Intervenci�n"): sqlOrder = " ORDER BY PR01DESCORTA, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE"
        Case UCase("Fecha"), UCase("Hora"): sqlOrder = " ORDER BY PR04FECPLANIFIC, AD02DESDPTO, PACIENTE, PR01DESCORTA"
        Case UCase("Quir."), UCase("Ord."): sqlOrder = " ORDER BY LPAD(QUIROFANO,2,'0'), LPAD(ORDEN,2,'0'), AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("T(min)"): sqlOrder = " ORDER BY DURACION, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("Anest."): sqlOrder = " ORDER BY ANESTESIA, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, AD02DESDPTO, PR04FECPLANIFIC, PR01DESCORTA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, AD02DESDPTO, PR04FECPLANIFIC, PR01DESCORTA"
        Case UCase("Dr. Sol."): sqlOrder = " ORDER BY SG02TXTFIRMA, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("Fecha Pet."): sqlOrder = " ORDER BY PR03FECADD, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("Cama"): sqlOrder = " ORDER BY AD15CODCAMA, AD02DESDPTO, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case UCase("T.E."): sqlOrder = " ORDER BY AD1100.CI32CODTIPECON, PR04FECPLANIFIC, PACIENTE, PR01DESCORTA"
        Case Else: Exit Sub
        End Select
    End If
    
    'Fechas
    strFecDesde = dcboDesde.Date
    strFecHasta = dcboHasta.Date
    'Tipo Asistencia
    If optTipoAsist(1).Value = True Then 'Hospitalizados
        lngTipoAsist = constASIST_HOSP
    ElseIf optTipoAsist(2).Value = True Then 'Ambulatorios
        lngTipoAsist = constASIST_AMBUL
    Else 'Todos
        lngTipoAsist = 0
    End If
    'Tipo de Dpto.
    If optTipoDpto(0).Value = True Then 'Dpto Solicitante
        lngTipoDpto = 1
    ElseIf optTipoDpto(1).Value = True Then 'Dpto Realizador
        lngTipoDpto = 2
    End If
    
    'vaciado previo del grid y de la colecci�n de b�sqeda
    grdActuaciones.RemoveAll
    Do While cllBuscar.Count > 0: cllBuscar.Remove 1: Loop
    
    'se cambia el scrollbar para evitar el efecto de llenado del grid dejando la �ltima _
    fila vacia durante un cierto rato
    grdActuaciones.ScrollBars = ssScrollBarsNone

    SQL = "SELECT /*+ ORDERED INDEX(PR0400 PR0408) */"
    SQL = SQL & " PR0400.PR04NUMACTPLAN, NVL(PR04FECPLANIFIC, PR03FECPREFEREN) PR04FECPLANIFIC, PR03FECPREFEREN,"
    SQL = SQL & " PR0400.PR01CODACTUACION, PR01DESCORTA,"
    SQL = SQL & " DECODE(PR0400.PR48CODURGENCIA,1,'SI') PR48CODURGENCIA,"
    SQL = SQL & " PR0400.CI21CODPERSONA, CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " CI22PRIAPEL, CI22SEGAPEL, CI22NOMBRE,"
    SQL = SQL & " CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
    SQL = SQL & " PR03FECADD, DECODE(PR03INDCITABLE,-1,'SI',0,'NO','NO') PR03INDCITABLE,"
    SQL = SQL & " GCFN06(AD15CODCAMA) AD15CODCAMA,"
    SQL = SQL & " AD02DESDPTO, SG02TXTFIRMA,"
    SQL = SQL & " AD1100.CI32CODTIPECON,"
    SQL = SQL & " PR41DUR.PR41RESPUESTA DURACION, PR41ANE.PR41RESPUESTA ANESTESIA,"
    SQL = SQL & " PR41QUI.PR41RESPUESTA QUIROFANO, PR41ORD.PR41RESPUESTA ORDEN"
    SQL = SQL & " FROM PR0400, PR0100, CI2200, PR0300, AD1500, AD1100, PR0900, AD0200, SG0200,"
    SQL = SQL & " PR4100 PR41DUR, PR4100 PR41ANE, PR4100 PR41QUI, PR4100 PR41ORD"
    SQL = SQL & " WHERE PR0400.PR37CODESTADO = " & constESTACT_PLANIFICADA
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND AD1500.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    SQL = SQL & " AND AD1500.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    SQL = SQL & " AND AD1500.AD14CODESTCAMA (+)= " & constCAMA_OCUPADA
    SQL = SQL & " AND AD1100.AD07CODPROCESO (+)= PR0400.AD07CODPROCESO"
    SQL = SQL & " AND AD1100.AD01CODASISTENCI (+)= PR0400.AD01CODASISTENCI"
    SQL = SQL & " AND AD1100.AD11FECFIN IS NULL"
    SQL = SQL & " AND PR0900.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0900.AD02CODDPTO"
    SQL = SQL & " AND SG0200.SG02COD = PR0900.SG02COD"
    SQL = SQL & " AND PR41DUR.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41DUR.PR40CODPREGUNTA (+)= 1041"
    SQL = SQL & " AND PR41ANE.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41ANE.PR40CODPREGUNTA (+)= 1042"
    SQL = SQL & " AND PR41QUI.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41QUI.PR40CODPREGUNTA (+)= 1043"
    SQL = SQL & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41ORD.PR40CODPREGUNTA (+)= 1044"
    If chkAnteriores = 0 Then
        If chkFecha.Value = 0 Then 'NO incluir act. sin fecha de planificaci�n
            SQL = SQL & " AND NVL(PR04FECPLANIFIC, PR03FECPREFEREN) >= TO_DATE(?,'DD/MM/YYYY')"
            SQL = SQL & " AND NVL(PR04FECPLANIFIC, PR03FECPREFEREN) < TO_DATE(?,'DD/MM/YYYY')"
        Else 'Incluir act. sin fecha de planificaci�n
            SQL = SQL & " AND ((NVL(PR04FECPLANIFIC, PR03FECPREFEREN) >= TO_DATE(?,'DD/MM/YYYY')"
            SQL = SQL & " AND NVL(PR04FECPLANIFIC, PR03FECPREFEREN) < TO_DATE(?,'DD/MM/YYYY'))"
            SQL = SQL & " OR NVL(PR04FECPLANIFIC, PR03FECPREFEREN) IS NULL)"
        End If
    Else 'Ver actuaciones atrasadas
        SQL = SQL & " AND NVL(PR04FECPLANIFIC, PR03FECPREFEREN) < TRUNC(SYSDATE)"
    End If
    If blnQuirofano Then
        If lngDpto > 0 Then
            SQL = SQL & " AND (PR0400.AD02CODDPTO = " & constDPTO_QUIROFANO
            SQL = SQL & " OR PR0400.AD02CODDPTO = ?)"
        End If
    Else
        Select Case lngTipoDpto
        Case 1: SQL = SQL & " AND PR0900.AD02CODDPTO = ?" 'solicitante
        Case 2: SQL = SQL & " AND PR0400.AD02CODDPTO = ?" 'realizador
        End Select
    End If
    Select Case lngTipoAsist
    Case constASIST_HOSP: SQL = SQL & " AND AD15CODCAMA IS NOT NULL"
    Case constASIST_AMBUL: SQL = SQL & " AND AD15CODCAMA IS NULL"
    End Select
    SQL = SQL & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry.QueryTimeout = 0
    If chkAnteriores = 0 Then
        qry(0) = Format(strFecDesde, "dd/mm/yyyy")
        qry(1) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
        n = 2
    Else
        n = 0
    End If
    If lngDpto > 0 Then qry(n) = lngDpto 'n: n� de par�metro del query
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        If IsNull(rs!PR04FECPLANIFIC) Then
            strFecPlan = ""
            strHoraPlan = ""
        Else
            strFecPlan = Format(rs!PR04FECPLANIFIC, "dd/mm/yyyy")
            strHoraPlan = Format(rs!PR04FECPLANIFIC, "hh:mm")
        End If
        If IsNull(rs!AD15CODCAMA) Then strCama = "" Else strCama = rs!AD15CODCAMA
        grdActuaciones.AddItem rs!PR03INDCITABLE & Chr$(9) _
                            & rs!PR48CODURGENCIA & Chr$(9) _
                            & rs!PR01DESCORTA & Chr$(9) _
                            & strFecPlan & Chr$(9) _
                            & strHoraPlan & Chr$(9) _
                            & rs!orden & Chr$(9) _
                            & rs!QUIROFANO & Chr$(9) _
                            & rs!DURACION & Chr$(9) _
                            & rs!ANESTESIA & Chr$(9) _
                            & rs!CI22NUMHISTORIA & Chr$(9) _
                            & rs!Paciente & Chr$(9) _
                            & rs!CI32CODTIPECON & Chr$(9) _
                            & rs!AD02DESDPTO & Chr$(9) _
                            & rs!SG02TXTFIRMA & Chr$(9) _
                            & Format(rs!PR03FECADD, "dd/mm/yyyy") & Chr$(9) _
                            & strCama & Chr$(9) _
                            & rs!PR04NUMACTPLAN & Chr$(9) _
                            & rs!CI21CODPERSONA & Chr$(9) _
                            & rs!PR03FECPREFEREN
        If IsNull(rs!CI22PRIAPEL) Then strApel1 = "" Else strApel1 = rs!CI22PRIAPEL
        If IsNull(rs!CI22SEGAPEL) Then strApel2 = "" Else strApel2 = rs!CI22SEGAPEL
        cllBuscar.Add rs!CI22NUMHISTORIA & "|" & rs!CI22NOMBRE & "|" & strApel1 & "|" & strApel2 & "|" & rs!PR04NUMACTPLAN & "|" & rs!CI21CODPERSONA
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'se pone el scrollbar en su estado normal
    grdActuaciones.ScrollBars = ssScrollBarsAutomatic

    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatearGrids()
    Dim i%
    With grdActuaciones
        .Columns(0).Caption = "Cita"
        .Columns(0).Width = 500
        .Columns(1).Caption = "Urg."
        .Columns(1).Width = 500
        .Columns(1).Visible = False
        .Columns(2).Caption = "Intervenci�n"
        .Columns(2).Width = 2400
        .Columns(3).Caption = "Fecha"
        .Columns(3).Width = 1000
        .Columns(4).Caption = "Hora"
        .Columns(4).Width = 600
        .Columns(4).Visible = False
        .Columns(5).Caption = "Ord."
        .Columns(5).Width = 500
        .Columns(6).Caption = "Quir."
        .Columns(6).Width = 500
        .Columns(7).Caption = "T(min)"
        .Columns(7).Width = 600
        .Columns(8).Caption = "Anest."
        .Columns(8).Width = 600
        .Columns(9).Caption = "N� Hist."
        .Columns(9).Alignment = ssCaptionAlignmentRight
        .Columns(9).Width = 700
        .Columns(10).Caption = "Paciente"
        .Columns(10).Width = 3100
        .Columns(11).Caption = "T.E."
        .Columns(11).Alignment = ssCaptionAlignmentCenter
        .Columns(11).Width = 400
        .Columns(12).Caption = "Dpto. Sol."
        .Columns(12).Width = 1500
        .Columns(13).Caption = "Dr. Sol."
        .Columns(13).Width = 1500
        .Columns(14).Caption = "Fecha Pet."
        .Columns(14).Width = 1000
        .Columns(15).Caption = "Cama"
        .Columns(15).Width = 600
        .Columns(16).Caption = "Num. Act."
        .Columns(16).Width = 0
        .Columns(16).Visible = False
        .Columns(17).Caption = "Cod. Persona"
        .Columns(17).Width = 0
        .Columns(17).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Function fBlnBuscar() As Boolean
    Dim i%
    For i = 0 To txtBuscar.Count - 1
        If txtBuscar(i).Text <> "" Then fBlnBuscar = True: Exit Function
    Next i
End Function

Private Sub grdActuaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub grdActuaciones_Click()
    If grdActuaciones.SelBookmarks.Count > 1 Then
        If grdActuaciones.Columns("N� Hist.").CellText(grdActuaciones.SelBookmarks(0)) <> grdActuaciones.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, Me.Caption
            grdActuaciones.SelBookmarks.Remove grdActuaciones.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub grdActuaciones_HeadClick(ByVal ColIndex As Integer)
    If grdActuaciones.Rows > 0 Then Call pActualizar(ColIndex)
End Sub

Private Sub txtBuscar_Change(Index As Integer)
    cmdB(0).Enabled = False: cmdB(1).Enabled = False
End Sub

Private Sub txtBuscar_GotFocus(Index As Integer)
    txtBuscar(Index).SelStart = 0: txtBuscar(Index).SelLength = Len(txtBuscar(Index).Text)
End Sub

Private Sub txtBuscar_KeyPress(Index As Integer, KeyAscii As Integer)
    If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Private Sub pCargarDptos()
    Dim SQL$, rs As rdoResultset, qry As rdoQuery
    
    If blnQuirofano Then
        fraTipoDpto.Visible = False
        optTipoDpto(1).Value = True
        
        SQL = "SELECT AD02CODDPTO, AD02DESDPTO"
        SQL = SQL & " FROM AD0200"
        SQL = SQL & " WHERE AD02CODDPTO IN "
        SQL = SQL & " (SELECT DISTINCT AD02CODDPTO"
        SQL = SQL & " FROM PR0200, PR0100"
        SQL = SQL & " WHERE PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
        SQL = SQL & " AND PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION)"
        SQL = SQL & " AND AD02FECFIN IS NULL"
        SQL = SQL & " ORDER BY AD02DESDPTO"
        cboDptos.AddItem "0" & Chr$(9) & "TODOS"
        cboDptos.Text = "TODOS"
        lngDpto = 0
    Else
        cmdPetPendientes.Visible = False
        cmdCitar.Visible = False
        cmdAnular.Visible = False
        SQL = "SELECT AD0200.AD02CODDPTO, AD0200.AD02DESDPTO"
        SQL = SQL & " FROM AD0200,AD0300"
        SQL = SQL & " WHERE AD0200.AD02CODDPTO = AD0300.AD02CODDPTO "
        SQL = SQL & " AND AD0300.SG02COD = '" & objSecurity.strUser & "'"
        SQL = SQL & " AND AD0200.AD02FECFIN IS NULL"
        SQL = SQL & " ORDER BY AD0200.AD02DESDPTO"
    End If
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboDptos.AddItem rs(0) & Chr$(9) & rs(1)
        rs.MoveNext
    Loop
    rs.Close
    
    If Not blnQuirofano Then
        cboDptos.Text = cboDptos.Columns(1).Text
        lngDpto = cboDptos.Columns(0).Text
    End If
End Sub

Private Function fBlnEsQuirofano() As Boolean
    Dim SQL$, rs As rdoResultset, qry As rdoQuery

    SQL = "SELECT AD02CODDPTO"
    SQL = SQL & " FROM AD0300"
    SQL = SQL & " WHERE SG02COD = ?"
    SQL = SQL & " AND AD02CODDPTO = ?"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = objSecurity.strUser
    qry(1) = constDPTO_QUIROFANO
    Set rs = qry.OpenResultset(SQL)
    If Not rs.EOF Then fBlnEsQuirofano = True
    rs.Close
    qry.Close
End Function

Private Sub pCitar()
    Dim strCodPersona$
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim i%, n%
    Dim nCitas%, blnYaCitada As Boolean
    Dim msg$, msg1$
    
    'Query para comprobar si la actuaci�n ha sido citada
    SQL = "SELECT count(CI31NUMSOLICIT) FROM CI0100"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA = '" & constESTCITA_CITADA & "'"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    
    'C�digo de persona
    strCodPersona = grdActuaciones.Columns("Cod. Persona").CellText(grdActuaciones.SelBookmarks(0))
    
    'Se procesan todas las actuaciones del paciente que se encuentran en el grid
    n = cllBuscar.Count: i = 1
    Do While i <= n
        awk1 = cllBuscar(i)
        If awk1.F(6) = strCodPersona Then
            grdActuaciones.SelBookmarks.RemoveAll
            LockWindowUpdate Me.hwnd
            grdActuaciones.MoveFirst
            grdActuaciones.MoveRecords i - 1
            LockWindowUpdate 0&
            grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
            If grdActuaciones.Columns("Cita").Text = "SI" Then
                'se comprueba que la actuaci�n no ha sido citada desde otro ordenador _
                por si todav�a no se ha refrescado la pantalla
                qry(0) = grdActuaciones.Columns("Num. Act.").Value
                Set rs = qry.OpenResultset()
                If rs(0) = 0 Then 'NO ha sido citada todav�a
                    Call objSecurity.LaunchProcess("CI1150", grdActuaciones.Columns("Num. Act.").Value)
                    qry(0) = grdActuaciones.Columns("Num. Act.").Value
                    Set rs = qry.OpenResultset()
                    If rs(0) > 0 Then 'Ha sido citada con �xito
                        nCitas = nCitas + 1
                        cllBuscar.Remove i
                        n = cllBuscar.Count
                        grdActuaciones.DeleteSelected
                        i = i - 1
                    End If
                    rs.Close
                Else
                    blnYaCitada = True
                End If
            Else
                cllBuscar.Remove i
                n = cllBuscar.Count
                grdActuaciones.DeleteSelected 'asociada
                i = i - 1
            End If
        End If
        i = i + 1
    Loop
    qry.Close
    
    
    Select Case nCitas
    Case 0: msg = "No se ha citado ninguna intervenci�n."
'    Case 1: msg = "Se ha realizado la cita de 1 intervenci�n."
'    Case Else: msg = "Se ha realizado la cita de " & nCitas & " intervenciones."
    End Select
    If blnYaCitada Then
        msg1 = Chr$(13) & Chr$(13)
        msg1 = msg1 & "Alguna de las intervenciones del paciente ha sido citada previamente"
        msg1 = msg1 & " desde otro ordenador." & Chr$(13) & Chr$(13)
        msg1 = msg1 & "Se va a proceder a actualizar los datos de la pantalla."
    End If
    If msg & msg1 <> "" Then MsgBox msg & msg1, vbInformation, Me.Caption
    If msg1 <> "" Then
        Call pActualizar(-1)
    End If
End Sub

Private Sub pAnularAct()
'**************************************************************************************
'*  Anula una actuaci�n planificada cambiando su estado a anulada
'**************************************************************************************
    Dim strCancel$, SQL$, qryPR04 As rdoQuery, i%, j%, lngNumActPlan&, qryCI01 As rdoQuery
        
    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        SQL = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox SQL, vbExclamation, Me.Caption
        Exit Sub
    Case 1
        SQL = "Si Ud. anula la intervenci�n ya no estar� disponible para ser realizada." & Chr$(13)
        SQL = SQL & "Indique el motivo por el que se va a anular la intervenci�n:"
    Case Is > 1
        SQL = "Si Ud. anula las intervenciones ya no estar�n disponibles para ser realizadas." & Chr$(13)
        SQL = SQL & "Indique el motivo por el que se van a anular las intervenciones:"
    End Select
    strCancel = Trim$(InputBox(SQL, "Anular Intervenci�n"))
    If strCancel = "" Then
        SQL = "La intervenci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox SQL, vbExclamation, Me.Caption
        Exit Sub
    End If
        
    SQL = "UPDATE PR0400 SET PR04DESMOTCAN = ?,"
    SQL = SQL & " PR04FECCANCEL = SYSDATE,"
    SQL = SQL & " PR37CODESTADO = " & constESTACT_CANCELADA
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR04 = objApp.rdoConnect.CreateQuery("", SQL)
    
    SQL = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_ANULADA & "'"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA= '" & constESTCITA_CITADA & "'"
    Set qryCI01 = objApp.rdoConnect.CreateQuery("", SQL)
        
    For i = 0 To grdActuaciones.SelBookmarks.Count - 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(i))

        ' anula la actuacion
        If Len(strCancel) > 50 Then qryPR04(0) = Left$(strCancel, 50) Else qryPR04(0) = strCancel
        qryPR04(1) = lngNumActPlan
        qryPR04.Execute
        qryPR04.Close
        
        ' anula la cita
        qryCI01(0) = lngNumActPlan
        qryCI01.Execute
        qryCI01.Close
        
        For j = 1 To cllBuscar.Count
            awk1 = cllBuscar(j)
            If awk1.F(5) = lngNumActPlan Then
                cllBuscar.Remove j
                Exit For
            End If
        Next j

        Call pAnularAsociadas(lngNumActPlan)
    Next i
    
    ' se elimina la actuaci�n del Grid
    grdActuaciones.DeleteSelected
End Sub
