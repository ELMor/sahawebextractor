Attribute VB_Name = "Validaciones"
Option Explicit

Public Function UpdatePolizaACUNSA(ByVal objObjeto As clsProcess, _
                               ByVal blnModo As Boolean)
  With objObjeto
    If blnModo Then
      .objOutput.AddNew
      .objOutput.rdoColumns("CI03NUMPOLIZA") = .objInput.rdoColumns("NUMPOLIZA")
      .objOutput.rdoColumns("CI03NUMASEGURA") = .objInput.rdoColumns("NUMASEG")
    Else
      .objOutput.Edit
    End If
    '************
    'Que Hago con los Null !!!!!!??????* --> & ""
    '********++
    .objOutput.rdoColumns("CI03FECINIPOLI") = ConvertirFecha(.objInput.rdoColumns("FECINIPOL"))
    If Not IsNull(.objInput.rdoColumns("FECFINPOL")) Then
      .objOutput.rdoColumns("CI03FECFINPOLI") = ConvertirFecha(.objInput.rdoColumns("FECFINPOL"))
    End If
    .objOutput.rdoColumns("CI03NOMBRTOMAD") = Nombre(.objInput.rdoColumns("NOMBRETOM"))
    .objOutput.rdoColumns("CI03APELLITOMAD") = Apellidos(.objInput.rdoColumns("NOMBRETOM"))
    .objOutput.rdoColumns("CI03NOMBRASEGU") = Nombre(.objInput.rdoColumns("NOMBREASEG"))
    .objOutput.rdoColumns("CI03APELLIASEGU") = Apellidos(.objInput.rdoColumns("NOMBREASEG"))
    '.objOutput.rdoColumns("CI22NUMHISTORIA") = 0 ' ????????
    If Not IsNull(.objInput.rdoColumns("DNI")) Then
      .objOutput.rdoColumns("CI03DNI") = .objInput.rdoColumns("DNI")
    End If
    .objOutput.rdoColumns("CI30CODSEXO") = ConvertirSexo(.objInput.rdoColumns("SEXO"))
    .objOutput.rdoColumns("CI03FECNACIMIE") = ConvertirFecha(.objInput.rdoColumns("FECNACIMIENTO"))
    .objOutput.rdoColumns("CI03CORRIPAGO") = ConvertirCorPago(.objInput.rdoColumns("CORPAGO"))
    .objOutput.rdoColumns("CI36CODTIPPOLI") = .objInput.rdoColumns("TIPOPOLIZA")
     If Not IsNull(.objInput.rdoColumns("EXCLUSIONES")) Then
       .objOutput.rdoColumns("CI03EXCLUSION") = .objInput.rdoColumns("EXCLUSIONES")
     End If
  ' .objOutput.rdoColumns("CI03OBSERVACIO") = .objInput.rdoColumns("OBSERVACIONES")
    .objOutput.Update
  End With
End Function

Public Function UpdateCodigosPostales(ByVal objObjeto As clsProcess, _
                               ByVal blnModo As Boolean)
  Static cont As Long
  With objObjeto
   If .objInput.rdoColumns("CODPOSTAL") <> "00000" Then
    If blnModo Then
      .objOutput.AddNew
      lngSecuencia = lngSecuencia + 1
      .objOutput("CI07SECUENCIA") = lngSecuencia
     ' .objOutput.rdoColumns("CI03NUMPOLIZA") = .objInput.rdoColumns("NUMPOLIZA")
     ' .objOutput.rdoColumns("CI03NUMASEGURA") = .objInput.rdoColumns("NUMASEG")
    Else
      .objOutput.Edit
    End If
    '************
    'Que Hago con los Null !!!!!!??????* --> & ""
    '********++
    .objOutput.rdoColumns("CI07CODPOSTAL") = .objInput.rdoColumns("CODPOSTAL")
    .objOutput.rdoColumns("CI26CODPROVI") = .objInput.rdoColumns("CODPROV")
    .objOutput.rdoColumns("CI07TIPPOBLA") = .objInput.rdoColumns("CODPOB")
    If .objOutput.rdoColumns("CI07TIPPOBLA") <> "9" Then
      .objOutput.rdoColumns("CI07DESPOBLA") = NombreCiudad(.objInput.rdoColumns("NOMCIUDAD"))
      .objOutput.rdoColumns("CI07CALLE") = Trim(.objInput.rdoColumns("CALLE") & " " & .objInput.rdoColumns("NOMCALLE"))
    Else
      .objOutput.rdoColumns("CI07DESPOBLA") = .objInput.rdoColumns("NOMCALLE")
      .objOutput.rdoColumns("CI07CALLE") = " "
    End If
    .objOutput.rdoColumns("CI07NUMIMPCOM") = .objInput.rdoColumns("COMIMPAR")
    .objOutput.rdoColumns("CI07NUMIMPFIN") = .objInput.rdoColumns("FINIMPAR")
    .objOutput.rdoColumns("CI07NUMPARCOM") = .objInput.rdoColumns("COMPAR")
    .objOutput.rdoColumns("CI07NUMPARFIN") = .objInput.rdoColumns("FINPAR")
    .objOutput.Update
   End If
  End With
  
End Function

'funcion para comprobar que una prueba de rayos la esta modificando
'un usuario de rayos
Public Function blnCompDepto(strDptoPrueba As String, strDptCom As String) As Boolean
'strDptoPrueba departamento de la prueba
'strDptCom     departamento al que debe pertenecer el usuario
Dim sql As String
Dim qry As rdoQuery
Dim rst As rdoResultset

If strDptoPrueba = strDptCom Then
    sql = "SELECT COUNT(*) FROM AD0300 WHERE SG02COD = ? "
    sql = sql & "  AND AD02CODDPTO = ? AND AD03FECFIN IS NULL "
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = objSecurity.strUser
        qry(1) = strDptCom
    Set rst = qry.OpenResultset()
    If rst(0) = 0 Then blnCompDepto = False Else blnCompDepto = True
'Else
'    blnCompDepto = True
End If
End Function
Public Function fblnCitarModiAnular(strDptoPrueba As String, strTipoPrueba As String, strCodAct As String) As Boolean
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery
Dim vntDeptNoCitables
Dim vntPruebasCitables
Dim i As Integer
Dim K As Integer
vntDeptNoCitables = Array("lun", "209", "170", "208", "250", "210", "11", "121", "206", "104", "106", "207", "120", "123", "113", "155", "172")
vntPruebasCitables = Array("2248", "2473", "2474", "2466", "2477", "3196", "2788", "2793", "2802", "2803", _
"2804", "3198", "2795", "2796", "2806", "3479")

fblnCitarModiAnular = True
If strTipoPrueba = constACTIV_PRUEBA Then  'en caso de ser una prueba
    For i = 1 To 16
        'miramos si pertenece a alguno de los dptos que se citan elllos mismos
        If strDptoPrueba = vntDeptNoCitables(i) Then
        'si es asi miramos si el usuario pertenece o no a ese departamento
          sql = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? AND AD03FECFIN IS NULL"
          Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = vntDeptNoCitables(i)
                qry(1) = objSecurity.strUser
          Set rs = qry.OpenResultset()
          If rs(0) = 0 Then fblnCitarModiAnular = False
          rs.Close
          qry.Close
          If strDptoPrueba = constDPTO_RAYOS Then
                For K = 0 To 15
                      If strCodAct = vntPruebasCitables(K) Then fblnCitarModiAnular = True
                Next K
          End If
        End If
    Next i
End If
If strTipoPrueba = constACTIV_INTERVENCION Then
'SI ES UNA INTERVENCION COMPROBAMOS QUE EL USUARIO PERTENECE A QUIROFANO
sql = "SELECT COUNT(*) FROM AD0300 WHERE AD02CODDPTO = ? AND SG02COD = ? "
          Set qry = objApp.rdoConnect.CreateQuery("", sql)
                qry(0) = constDPTO_QUIROFANO
                qry(1) = objSecurity.strUser
          Set rs = qry.OpenResultset()
          If rs(0) = 0 Then fblnCitarModiAnular = False
          rs.Close
          qry.Close
End If
End Function
 
