VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.0#0"; "COMCT232.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Begin VB.Form frmCitaManual 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Citas Manuales"
   ClientHeight    =   5475
   ClientLeft      =   3525
   ClientTop       =   1500
   ClientWidth     =   10815
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   10815
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Frame FRACitas 
      Height          =   1695
      Index           =   0
      Left            =   120
      TabIndex        =   21
      Top             =   0
      Width           =   10575
      Begin VB.TextBox TxtCita 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         Index           =   0
         Left            =   1800
         TabIndex        =   1
         Top             =   480
         Width           =   1570
      End
      Begin VB.CommandButton BUTCitas 
         Caption         =   "Perso. F�sicas"
         Height          =   375
         Index           =   2
         Left            =   7800
         TabIndex        =   22
         Top             =   480
         Width           =   1215
      End
      Begin idperson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   2355
         BackColor       =   12648384
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
   End
   Begin VB.TextBox txtActual 
      Height          =   375
      Left            =   1920
      TabIndex        =   20
      Top             =   5040
      Visible         =   0   'False
      Width           =   6735
   End
   Begin VB.CommandButton BUTCitas 
      Caption         =   "Generar Cita"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   480
      TabIndex        =   11
      Top             =   5040
      Width           =   1455
   End
   Begin VB.CommandButton BUTCitas 
      Caption         =   "Salir"
      Height          =   375
      Index           =   1
      Left            =   8640
      TabIndex        =   12
      Top             =   5040
      Width           =   1455
   End
   Begin VB.Frame FRACitas 
      Caption         =   "Datos de la Cita"
      Height          =   3015
      Index           =   1
      Left            =   120
      TabIndex        =   14
      Top             =   1800
      Width           =   10575
      Begin VB.TextBox txtHora 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF00&
         Height          =   315
         Index           =   0
         Left            =   3000
         MaxLength       =   2
         TabIndex        =   3
         Tag             =   "Hora"
         Top             =   360
         Width           =   390
      End
      Begin VB.TextBox txtMinuto 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFF00&
         Height          =   315
         HelpContextID   =   3
         Index           =   0
         Left            =   3600
         MaxLength       =   2
         TabIndex        =   5
         Tag             =   "Minutos"
         Top             =   360
         Width           =   390
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFFFF&
         DataField       =   "PR09DESOBSERVAC"
         Height          =   810
         HelpContextID   =   30104
         Index           =   1
         Left            =   240
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         Tag             =   "Observaciones de la Petici�n|Observaciones"
         Top             =   2040
         Width           =   9945
      End
      Begin SSCalendarWidgets_A.SSDateCombo SSDCFecha 
         Height          =   315
         Left            =   840
         TabIndex        =   2
         Top             =   360
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbRecurso 
         Height          =   300
         Left            =   6600
         TabIndex        =   9
         Top             =   1440
         Width           =   3480
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   1349
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4604
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6138
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbPrueba 
         Height          =   300
         Left            =   1440
         TabIndex        =   8
         Top             =   1440
         Width           =   3360
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         BackColorEven   =   16776960
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   1879
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7726
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5927
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin ComCtl2.UpDown UpDownMinuto 
         Height          =   330
         Index           =   0
         Left            =   3960
         TabIndex        =   13
         Top             =   360
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   3060
         OrigTop         =   405
         OrigRight       =   3300
         OrigBottom      =   735
         Increment       =   5
         Max             =   55
         Enabled         =   -1  'True
      End
      Begin ComCtl2.UpDown UpDownHora 
         Height          =   330
         Index           =   0
         Left            =   3360
         TabIndex        =   4
         Top             =   360
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   582
         _Version        =   327681
         OrigLeft        =   2430
         OrigTop         =   405
         OrigRight       =   2670
         OrigBottom      =   735
         Max             =   23
         Enabled         =   -1  'True
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbActividad 
         Height          =   300
         Left            =   6600
         TabIndex        =   7
         Top             =   960
         Width           =   3480
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         BackColorEven   =   16776960
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   1879
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   4683
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6138
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCmbDpto 
         Height          =   300
         Left            =   1440
         TabIndex        =   6
         Top             =   960
         Width           =   3360
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         BackColorEven   =   16776960
         RowHeight       =   423
         ExtraHeight     =   26
         Columns.Count   =   2
         Columns(0).Width=   1429
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5953
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Descripci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5927
         _ExtentY        =   529
         _StockProps     =   93
         BackColor       =   16776960
      End
      Begin VB.Label Label4 
         Caption         =   "Departamento"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label6 
         Caption         =   "Tipo Actividad"
         Height          =   255
         Left            =   5400
         TabIndex        =   23
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Hora "
         Height          =   195
         Index           =   5
         Left            =   2520
         TabIndex        =   19
         Top             =   480
         Width           =   390
      End
      Begin VB.Label Label5 
         Caption         =   "Observaciones"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Prueba"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha "
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Recurso"
         Height          =   255
         Left            =   5400
         TabIndex        =   15
         Top             =   1440
         Width           =   735
      End
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   3960
      Top             =   4320
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   5
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1047.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1047.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1047.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1047.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "CI1047.frx":0C68
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmCitaManual"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

    'Datos necesarios para buscar huecos
    Dim strRecurso As String 'Almacena el recurso para el que busca los huecos disponibles
    Dim FechaActual As Date 'Almacena la Fecha en la que est� buscando Huecos
    Dim sHoraActual As String
   
    'Constantes de Botones
    Const iBUTCita = 0
    Const iBUTSALIR = 1
    Const iBUTBuscar = 2
    
    'Constantes de cajas de texto
    Const iTXTHIS = 0           'Historia
   
    'Booleana para saber si se ha buscado por persona o por historia
    Dim bHistoria   As Boolean
    Dim bPersona    As Boolean
    
    'EFS: Variable para guardar el numero de peticion para luego insertar las
    'restricciones
    Dim lngNumPet As Long
    Dim lngNumActPlan As Long
    Dim intTipoRecur As Long
Private Sub pGeneraCita()
    
    Dim intMsgBox   As Integer
    Dim intContador As Integer
    
    Screen.MousePointer = vbHourglass
    'Realizamos la creaci�n de la cita de la cita manual
    If IdPersona1.historia > 0 Then
        txtActual.Visible = True
        txtActual.Text = "Citando..."
        If fblnCitar Then 'Citar
            txtActual.Text = "Citado"
            'Asociar Proceso-Asistencia *****- SE QUITAR� CUANDO DESAPAREZCA EL IBM -*****
            ReDim vntData(2) As Variant
            vntData(1) = IdPersona1.Text
            vntData(2) = 2 'Viene de Citas   ??? AJO No se si ser� as�.
            Screen.MousePointer = vbDefault
            Call objSecurity.LaunchProcess("AD0105", vntData)
            If Not blnInsert_Restri Then
                MsgBox "Ha habido errores al asociar las restrinciones", vbCritical
            End If
        Else
            MsgBox "Se ha producido un error al citar. Por favor, int�ntelo de nuevo", vbCritical
        End If 'fblnCitar
        txtActual.Visible = False
        SSDBCmbDpto.Value = ""
        SSDBCmbActividad.Value = ""
        SSDBCmbPrueba.Value = ""
        SSDBCmbRecurso.Value = ""
        'Iniciamos la fecha actual
        SSDCFecha.DefaultDate = Format(Now, "dd/mm/yyyy")
        'Iniciamos la hora a la actual
        sHoraActual = strFechaHora_Sistema
        txtHora(0) = Mid(sHoraActual, 12, 2)
        txtMinuto(0) = Right(sHoraActual, 2)
        IdPersona1.Text = ""
        txtText1(1) = ""
        BUTCitas(iBUTCita).Enabled = False
    Else
        MsgBox "Persona no valida para concertar una cita. " & Chr(13) & " Seleccione a un paciente con n�mero de historia " & Chr(13) & " o complete los datos (Personas F�sicas)", vbInformation
        txtActual.Text = "Persona no valida para concertar una cita. " & Chr(13) & " Seleccione a un paciente con n�mero de historia " & Chr(13) & " o complete los datos (Personas F�sicas)"
        txtActual.Visible = True
    End If 'lngHistoria
    Screen.MousePointer = vbDefault
    
End Sub
Private Sub BUTCitas_Click(Index As Integer)
    Select Case Index
        Case iBUTSALIR
            Unload Me
        Case iBUTCita
            Call pGeneraCita
        Case iBUTBuscar
            Screen.MousePointer = vbHourglass
            'Llamar a Mant. Personas F�sicas
            Call objSecurity.LaunchProcess("CI1017")
            Screen.MousePointer = vbHourglass
            'Si se ha seleccionado una persona...
            If lngCodPers <> 0 Then
                IdPersona1.Text = lngCodPers
            Else
                IdPersona1.Text = ""
            End If
              Screen.MousePointer = vbDefault
    End Select
End Sub

Private Sub Form_Load()

    'Consulta SQL
    Dim SQL As String
    Dim qryCombos As rdoQuery
    Dim rstCombos As rdoResultset

    Screen.MousePointer = vbArrowHourglass
    Call objApp.AddCtrl(TypeName(IdPersona1))
    Call IdPersona1.BeginControl(objApp, objGen)
    

'Se cargan los Combo Box de...

'...Departamentos Responsables de Procesos
    Call pCargar_Departamentos
   
'... Tipo de Actividad
   Call pCargar_Actividad
   
   IdPersona1.BackColor = &HFFFF&
   'Iniciamos la hora a la actual
   sHoraActual = strFechaHora_Sistema
   txtHora(0) = Mid(sHoraActual, 12, 2)
   txtMinuto(0) = Right(sHoraActual, 2)
   'Iniciamos las booleanas
   bHistoria = False
   bPersona = False
   Screen.MousePointer = vbDefault

End Sub

Private Sub Form_Unload(Cancel As Integer)
 Call IdPersona1.EndControl
End Sub

Private Sub IdPersona1_Change()

    Dim sStmSql     As String
    Dim qryPersona  As rdoQuery
    Dim rstHistoria As rdoResultset
    If bPersona Then
        bPersona = False
        Exit Sub
    End If
    If IdPersona1.Text <> "" Then
        'Buscaremos el n�mero de Hist�ria por el c�digo de Persona
        sStmSql = "SELECT CI22NUMHISTORIA FROM CI2200 WHERE CI21CODPERSONA = ? "
        Set qryPersona = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryPersona(0) = IdPersona1.Text
        Set rstHistoria = qryPersona.OpenResultset()
        If Not IsNull(rstHistoria(0)) And Not rstHistoria.EOF Then
            TxtCita(iTXTHIS) = rstHistoria(0)
        Else
            bHistoria = True
            TxtCita(iTXTHIS) = ""
            bHistoria = False
        End If
        rstHistoria.Close
        qryPersona.Close
    Else
        bHistoria = True
        TxtCita(iTXTHIS) = ""
        bHistoria = False
    End If
    If TxtCita(iTXTHIS) <> "" And SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" And SSDBCmbRecurso.Value <> "" Then
         BUTCitas(iBUTCita).Enabled = True
         BUTCitas(iBUTSALIR).Enabled = True
    Else
         BUTCitas(iBUTCita).Enabled = False
         BUTCitas(iBUTSALIR).Enabled = True
    End If
        
End Sub

Private Sub IdPersona1_LostFocus()
    If IdPersona1.Text <> "" And IdPersona1.historia = "" Then
        MsgBox "Persona no valida para concertar una cita. " & Chr(13) & " Seleccione a un paciente con n�mero de historia " & Chr(13) & " o complete los datos (Personas F�sicas)", vbInformation
        IdPersona1.Text = ""
        TxtCita(iTXTHIS) = ""
        IdPersona1.SetFocus
        
    End If
    
End Sub

Private Sub SSDBCmbActividad_CloseUp()
    'Si seleccionan un departamento
    If SSDBCmbDpto.Text <> "" And SSDBCmbActividad.Text <> "" Then
        Call pCargar_Prueba 'Carga los combos de Prueba
    Else
        SSDBCmbPrueba.RemoveAll
        SSDBCmbRecurso.RemoveAll
    End If
End Sub

Private Sub SSDBCmbActividad_KeyUp(KeyCode As Integer, Shift As Integer)
    If SSDBCmbDpto.Text <> "" And SSDBCmbActividad.Text <> "" Then
        Call pCargar_Prueba 'Carga los combos de Prueba y Recurso
    Else
        MsgBox "Seleccione primero el departamento y el tipo de actividad " & Chr(13) & "para mostrar las diferentes pruebas", vbInformation
        SSDBCmbDpto.SetFocus
    End If
End Sub

Private Sub SSDBCmbDpto_CloseUp()
    'Si seleccionan un departamento
    If SSDBCmbDpto.Text <> "" And SSDBCmbActividad.Text <> "" Then
        Call pCargar_Prueba 'Carga los combos de Prueba
    Else
        SSDBCmbPrueba.RemoveAll
        SSDBCmbRecurso.RemoveAll
    End If
   
    'EFS: Si el departamento es urgencias cargamos directamente la
    'actuacion y el recurso
    If SSDBCmbDpto.Columns(0).Text = 216 Then
        Call pCargar_Recurso
        SSDBCmbRecurso.Text = SSDBCmbRecurso.Columns(1).Text
        SSDBCmbPrueba.Text = SSDBCmbPrueba.Columns(1).Text
        Call pCitas_Urgencias
        BUTCitas(iBUTSALIR).Enabled = False
        BUTCitas(iBUTCita).Enabled = False
    'Si esta rellena el paciente, la fecha, Dpto, Prueba y recurso
    'Se habilitar� el bot�n para citar el ingreso
    ElseIf IdPersona1.historia <> "" And SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" And SSDBCmbRecurso.Value <> "" Then
         BUTCitas(iBUTCita).Enabled = True
         BUTCitas(iBUTSALIR).Enabled = True
    Else
         BUTCitas(iBUTCita).Enabled = False
         BUTCitas(iBUTSALIR).Enabled = True
    End If
End Sub

Private Sub SSDBCmbDpto_KeyUp(KeyCode As Integer, Shift As Integer)
    If SSDBCmbDpto.Text <> "" And SSDBCmbActividad.Text <> "" Then
        Call pCargar_Prueba 'Carga los combos de Prueba y Recurso
    Else
        MsgBox "Seleccione primero el departamento y el tipo de actividad para mostrar los diferentes recursos para seleccionar", vbInformation
        SSDBCmbDpto.SetFocus
    End If
End Sub
Private Sub pCargar_Prueba() 'Carga los combos de Prueba y Recurso

'Consultas SQL
Dim SQL As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset

'Inicilizar los ComboBox de Pruebas y Recursos
SSDBCmbPrueba.Text = ""
SSDBCmbPrueba.RemoveAll

'Actuaciones
SQL = "SELECT PR0100.PR01CODACTUACION, PR01DESCORTA FROM PR0100, PR0200 "
SQL = SQL & "WHERE PR0200.AD02CODDPTO = ? AND "
SQL = SQL & "PR0100.PR12CODACTIVIDAD = ? AND "
SQL = SQL & "PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION "
SQL = SQL & "AND PR01FECINICO <= SYSDATE AND "
SQL = SQL & "(PR01FECFIN >= SYSDATE OR PR01FECFIN IS NULL)"
SQL = SQL & "ORDER BY PR0100.PR01DESCORTA "
Set qryConsulta = objApp.rdoConnect.CreateQuery("", SQL)
   qryConsulta(0) = SSDBCmbDpto.Columns(0).Text
   qryConsulta(1) = SSDBCmbActividad.Columns(0).Text
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   SSDBCmbPrueba.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
rstConsulta.Close
qryConsulta.Close

End Sub

Private Sub SSDBCmbPrueba_CloseUp()
    If SSDBCmbPrueba.Text <> "" Then
        'Rellenamos la combo de recursos
        Call pCargar_Recurso
    Else
        SSDBCmbRecurso.RemoveAll
    End If
    'Si esta rellenos los datos: - Historia - fecha  - Dpto - Prueba - Recurso
    'Se habilitar� el bot�n para citar
    If IdPersona1.historia <> "" And SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" And SSDBCmbRecurso.Value <> "" Then
        BUTCitas(iBUTCita).Enabled = True
    Else
        BUTCitas(iBUTCita).Enabled = False
    End If
End Sub

Private Function fblnCitar() As Boolean

Dim fecCita As Date
'Conversi�n de tiempos
   Dim intDias As Integer
   Dim bytHoras As Byte
   Dim bytMinutos As Byte
'Campos Primery Key que se obtienen antes de insertar un registro
   Dim numPetic As Long
   Dim numActPedi As Long
   Dim numActPlan As Long
   Dim numSolicit As Long
'Datos necesarios que se obtienen con funciones
   Dim codDpto As Integer 'Departamento del usuario
   'Fase
   Dim strDesFase As String 'Descripci�n
   Dim lngMinutFase As Long 'Minutos de ocupaci�n del paciente
   'Tipo Recurso- Fase Actuaci�n
   Dim lngNumNecesid As Long
   Dim intIndPlan As Integer
   Dim lngTiemRec As Long
   'C�digo de recurso y Tipo de Recurso para codDoctor
   Dim intTipRec As Integer
   Dim lngCodRecurso As Long
'Consultas SQL
   Dim SQL As String
   Dim QyInsert As rdoQuery
   Dim RsRespuesta As rdoResultset
'valor de las restrinciones
   Dim vntRest As Variant
   Dim doctorSolicit As String
   

'********************Inicializaci�n********************
On Error Resume Next
Err = 0
objApp.rdoConnect.BeginTrans

'COMIENZA EL BAILE**************************************************************************

'''Datos previos
''SQL = "SELECT AD02CODDPTO FROM AD0300 WHERE SG02COD = ? AND AD03FECINICIO <= SYSDATE AND "
''SQL = SQL & "(AD03FECFIN >= SYSDATE OR AD03FECFIN IS NULL)"
''Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
''   QyInsert(0) = objSecurity.strUser
''Set RsRespuesta = QyInsert.OpenResultset()
''If Not RsRespuesta.EOF Then codDpto = RsRespuesta(0)
''RsRespuesta.Close
''QyInsert.Close

SQL = "SELECT SG02COD FROM AG1100 WHERE AG11CODRECURSO = ?"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = Trim(SSDBCmbRecurso.Columns(0).Text)
Set RsRespuesta = QyInsert.OpenResultset()
If Not RsRespuesta.EOF And Not IsNull(RsRespuesta!sg02cod) Then
    doctorSolicit = RsRespuesta(0)
Else
    doctorSolicit = gstrSinDoctor
End If
RsRespuesta.Close
QyInsert.Close

'PR0900 PETICI�N
SQL = "INSERT INTO PR0900 (PR09NUMPETICION, CI21CODPERSONA, AD02CODDPTO, SG02COD, "
If txtText1(1).Text = "" Then
    SQL = SQL & "PR09FECPETICION, PR09NUMGRUPO) VALUES (?, ?, ?, ?, SYSDATE, ?)"
Else
    SQL = SQL & "PR09FECPETICION, PR09NUMGRUPO, PR09DESOBSERVAC) VALUES (?, ?, ?, ?, SYSDATE, ?, ?)"
End If

Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numPetic = flngNextClaveSeq("PR09NUMPETICION")
   lngNumPet = numPetic
   QyInsert(0) = numPetic
   QyInsert(1) = IdPersona1.Text
   QyInsert(2) = SSDBCmbDpto.Columns(0).Value
   QyInsert(3) = doctorSolicit
   QyInsert(4) = numPetic
   If txtText1(1).Text <> "" Then QyInsert(5) = txtText1(1).Text
QyInsert.Execute
QyInsert.Close

If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
'PR0300 ACTUACI�N PEDIDA
SQL = "INSERT INTO PR0300 (PR03NUMACTPEDI, AD02CODDPTO, CI21CODPERSONA, PR09NUMPETICION, "
SQL = SQL & "PR01CODACTUACION, PR03INDCONSFIRM, PR03INDLUNES, PR03INDMARTES, "
SQL = SQL & "PR03INDMIERCOLES, PR03INDJUEVES, PR03INDVIERNES, PR03INDSABADO, PR03INDDOMINGO,"
SQL = SQL & " PR03INDCITABLE) VALUES (?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0, -1)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numActPedi = flngNextClaveSeq("PR03NUMACTPEDI") 'Se usar� m�s adelante
   QyInsert(0) = numActPedi
   QyInsert(1) = SSDBCmbDpto.Columns(0).Text
   QyInsert(2) = IdPersona1.Text
   QyInsert(3) = numPetic
   QyInsert(4) = SSDBCmbPrueba.Columns(0).Text
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'PR0800 PETICI�N-ACTUACI�N PEDIDA
If txtText1(1).Text = "" Then
    SQL = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA) "
    SQL = SQL & " VALUES (?, ?, 1)"
Else
    SQL = "INSERT INTO PR0800 (PR09NUMPETICION, PR03NUMACTPEDI, PR08NUMSECUENCIA, PR08DESOBSERV) "
    SQL = SQL & " VALUES (?, ?, 1, ?)"
End If

Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numPetic
   QyInsert(1) = numActPedi
   If txtText1(1).Text <> "" Then QyInsert(2) = txtText1(1).Text
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
txtText1(1).Text = ""
'Datos previos
If Not fblnFase(SSDBCmbPrueba.Columns(0).Text, strDesFase, lngMinutFase) Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
'PR0600 FASE PEDIDA
SQL = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC, "
SQL = SQL & "PR06NUMMINFPRE, PR06NUMMAXFPRE, PR06INDHABNATU, PR06INDINIFIN) "
SQL = SQL & "VALUES (?, 1, ?, ?, 0, 0, 0, 0)"
'SQL = SQL & "VALUES (?, 1, ?, 0, 0, 0, 0, 0)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numActPedi
   QyInsert(1) = strDesFase
   QyInsert(2) = lngMinutFase
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'Datos previos
If Not fblnRecFaseAct(SSDBCmbPrueba.Columns(0).Text, lngNumNecesid, lngTiemRec, _
                                                                       intIndPlan) Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
If Trim(SSDBCmbRecurso.Columns(0).Text) <> "" Then
    strRecurso = SSDBCmbRecurso.Columns(0).Text
Else
    objApp.rdoConnect.RollbackTrans
    Exit Function
End If
If Not fblnTipRec(intTipRec) Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
'EFS: Variable que luego utilizamos en la inserci�n de restricciones
intTipoRecur = intTipRec
'PR1400 TIPO RECURSO PEDIDO
SQL = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, AG14CODTIPRECU, "
SQL = SQL & "AD02CODDPTO, PR14NUMUNIREC, PR14NUMMINOCU, PR14NUMMINDESREC, PR14INDRECPREFE, "
'SQL = SQL & "PR14INDPLANIF) VALUES (?, 1, ?, ?, ?, 1, ?, 0, -1, ?)"
SQL = SQL & "PR14INDPLANIF) VALUES (?, 1, ?, ?, ?, 1, 0, 0, -1, ?)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numActPedi 'PR03NUMACTPEDI
   QyInsert(1) = lngNumNecesid 'PR14NUMNECESID
   QyInsert(2) = intTipRec 'AG14CODTIPRECU
   QyInsert(3) = SSDBCmbDpto.Columns(0).Text 'AD02CODDPTO
   'QyInsert(4) = lngTiemRec 'PR14NUMMINOCU
   QyInsert(4) = intIndPlan 'PR14INDPLANIF
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If
If SSDCFecha.Text <> "" And txtHora(0) <> "" And txtMinuto(0) <> "" Then
    FechaC = SSDCFecha.Text & " " & txtHora(0) & ":" & txtMinuto(0)
Else
    FechaC = strFechaHora_Sistema
End If

'PR0400 ACTUACI�N PLANIFICADA
If SSDBCmbDpto.Columns(0).Text = "216" Then
    SQL = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, "
    SQL = SQL & "CI21CODPERSONA, PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC,PR04FECENTRCOLA) "
    SQL = SQL & "VALUES (?, ?, ?, ?, 2, ?, 0,SYSDATE)"
Else
    SQL = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR01CODACTUACION, AD02CODDPTO, "
    SQL = SQL & "CI21CODPERSONA, PR37CODESTADO, PR03NUMACTPEDI, PR04INDREQDOC) "
    SQL = SQL & "VALUES (?, ?, ?, ?, 2, ?, 0)"
End If
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numActPlan = flngNextClaveSeq("PR04NUMACTPLAN")
   lngNumActPlan = numActPlan
   QyInsert(0) = numActPlan
   QyInsert(1) = SSDBCmbPrueba.Columns(0).Text
   QyInsert(2) = SSDBCmbDpto.Columns(0).Text 'AD02CODDPTO
   QyInsert(3) = IdPersona1.Text
   QyInsert(4) = numActPedi
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'CI3100 SOLICITUD
SQL = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31FECPREFDES, CI31FECPREFHAS, CI31INDLUNPREF, "
SQL = SQL & "CI31INDMARPREF, CI31INDMIEPREF, CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF,"
SQL = SQL & " CI31INDDOMPREF) VALUES (?, SYSDATE, SYSDATE + 15,  1, 1, 1, 1, 1, 1, 1)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   numSolicit = flngNextClave("CI31NUMSOLICIT", "CI3100")
   QyInsert(0) = numSolicit
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   If Err = 40002 Then
      Err = 0
      SQL = "INSERT INTO CI3100 (CI31NUMSOLICIT, CI31FECPREFDES, CI31FECPREFHAS, CI31INDLUNPREF, "
      SQL = SQL & "CI31INDMARPREF, CI31INDMIEPREF, CI31INDJUEPREF, CI31INDVIEPREF, CI31INDSABPREF,"
      SQL = SQL & " CI31INDDOMPREF) VALUES (?, SYSDATE, SYSDATE + 15,  1, 1, 1, 1, 1, 1, 1)"
      Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
         numSolicit = flngNextClave("CI31NUMSOLICIT", "CI3100")
         QyInsert(0) = numSolicit
      QyInsert.Execute
      QyInsert.Close
      If Err > 0 Then
         objApp.rdoConnect.RollbackTrans
         Exit Function
      End If
   Else
      objApp.rdoConnect.RollbackTrans
      Exit Function
   End If
End If
If SSDCFecha.Text <> "" And txtHora(0) <> "" And txtMinuto(0) <> "" Then
    fecCita = SSDCFecha.Text & " " & txtHora(0) & ":" & txtMinuto(0)
Else
    fecCita = strFechaHora_Sistema
End If

'CI0100 ACTUACI�N CITADA
SQL = "INSERT INTO CI0100 (CI31NUMSOLICIT, CI01NUMCITA, AG11CODRECURSO, PR04NUMACTPLAN, "
SQL = SQL & "CI01FECCONCERT, CI01SITCITA, CI01INDRECORDA, CI01TIPRECORDA, CI01NUMENVRECO, "
SQL = SQL & "CI01INDLISESPE, CI01INDASIG) "
SQL = SQL & "VALUES (?, 1, ?, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), 1, -1, 1, 1, 0, 0)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
    QyInsert(0) = numSolicit
    QyInsert(1) = strRecurso
    QyInsert(2) = numActPlan
    QyInsert(3) = objGen.ReplaceStr(Format(fecCita, "DD/MM/YYYY HH:MM:SS"), ".", ":", 0)
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'CI1500 FASE CITADA
SQL = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, CI15DESFASECITA, "
SQL = SQL & "CI15FECCONCPAC, CI15NUMDIASPAC, CI15NUMHORAPAC, CI15NUMMINUPAC) "
SQL = SQL & "VALUES (?, 1, 1, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, 0)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numSolicit
   QyInsert(1) = strDesFase
   QyInsert(2) = objGen.ReplaceStr(Format(fecCita, "DD/MM/YYYY HH:MM:SS"), ".", ":", 0)
   QyInsert(3) = intDias
   QyInsert(4) = bytHoras
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

'CI2700 RECURSO CITADO
SQL = "INSERT INTO CI2700 (CI31NUMSOLICIT, CI01NUMCITA, CI15NUMFASECITA, AG11CODRECURSO, "
SQL = SQL & "CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC) "
SQL = SQL & "VALUES (?, 1, 1, ?, TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS'), ?, ?, 0)"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = numSolicit
   QyInsert(1) = strRecurso
   QyInsert(2) = objGen.ReplaceStr(Format(fecCita, "DD/MM/YYYY HH:MM:SS"), ".", ":", 0)
   QyInsert(3) = intDias
   QyInsert(4) = bytHoras
QyInsert.Execute
QyInsert.Close
If Err > 0 Then
   objApp.rdoConnect.RollbackTrans
   Exit Function
End If

objApp.rdoConnect.CommitTrans
fblnCitar = True

End Function
Private Function blnInsert_Restri() As Boolean
   Dim SQL As String
   Dim QyConsul As rdoQuery
   Dim QyInsert As rdoQuery
   Dim RsRespuesta As rdoResultset
'valor de las restrinciones
   Dim vntRest As Variant
   Dim lngProceso As Long
   Dim lngAsistencia As Long
On Error Resume Next
'Datos previos
'Buscamos el proceso asistencia
SQL = "SELECT AD01CODASISTENCI,AD07CODPROCESO FROM PR0400 WHERE PR04NUMACTPLAN = ? "
Set QyConsul = objApp.rdoConnect.CreateQuery("", SQL)
 QyConsul(0) = lngNumActPlan
Set RsRespuesta = QyConsul.OpenResultset()
'si hay proceso/asistencia lo pasamos a variables
If Not RsRespuesta.EOF Then
    If Not IsNull(RsRespuesta(0).Value) Then
        lngAsistencia = RsRespuesta(0).Value
    Else
        lngAsistencia = 0
    End If
    If Not IsNull(RsRespuesta(1).Value) Then
        lngProceso = RsRespuesta(1).Value
    Else
        lngProceso = 0
    End If
End If

'RESTRINCIONES: Miramos si el recurso tiene alguna restrincion por
'franja y si es asi guardamos el valor.
SQL = "SELECT AG16CODTIPREST FROM AG1500 WHERE AG14CODTIPRECU = ?"
Set QyConsul = objApp.rdoConnect.CreateQuery("", SQL)
   QyConsul(0) = intTipoRecur
Set RsRespuesta = QyConsul.OpenResultset()
If Not RsRespuesta.EOF Then
    objApp.BeginTrans
    Do While Not RsRespuesta.EOF
        vntRest = fValorRest(RsRespuesta(0).Value, lngProceso, lngAsistencia)
        If vntRest <> " " Then
            SQL = "INSERT INTO PR4700 (PR09NUMPETICION,AG16CODTIPREST,PR47VALOR) "
            SQL = SQL & "VALUES (?,?,?)"
            Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
               QyInsert(0) = lngNumPet
               QyInsert(1) = RsRespuesta(0).Value
               QyInsert(2) = vntRest
            QyInsert.Execute
            QyInsert.Close
            If Err > 0 Then
               objApp.rdoConnect.RollbackTrans
               Exit Function
            End If
        End If 'vntRest <>
    RsRespuesta.MoveNext
    Loop 'resRespuesta.eof
    objApp.CommitTrans
End If 'resRespuesta.eof
blnInsert_Restri = True
QyConsul.Close
End Function
'Devuelve el siguiente c�digo a utilizar en un registro nuevo utilizando los SEQUENCE
'Si se produce alg�n error, devuelve -1
Public Function flngNextClaveSeq(strCampo As String) As Long
    
Dim rstMaxClave As rdoResultset

On Error Resume Next
Err = 0
Set rstMaxClave = objApp.rdoConnect.OpenResultset("SELECT " & strCampo & "_SEQUENCE.NEXTVAL FROM DUAL")
flngNextClaveSeq = rstMaxClave(0)
rstMaxClave.Close
Set rstMaxClave = Nothing
If Err > 0 Then flngNextClaveSeq = -1
    
End Function

'HALLAR LA DESCRIPCI�N Y DURACI�N DE LA FASE QUE SE QUIERE CITAR
Private Function fblnFase(lngCodActuacion As Long, strDesFase As String, _
                           lngMinutFase As Long) As Boolean
Dim SQL As String
Dim QyInsert As rdoQuery
Dim RsRespuesta As rdoResultset

SQL = "SELECT PR05DESFASE, PR05NUMOCUPACI FROM PR0500 "
SQL = SQL & "WHERE PR01CODACTUACION = ? AND PR05NUMFASE = 1"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = lngCodActuacion
Set RsRespuesta = QyInsert.OpenResultset()
If RsRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   strDesFase = RsRespuesta(0)
   lngMinutFase = RsRespuesta(1)
   RsRespuesta.MoveNext
   If Not RsRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
RsRespuesta.Close
QyInsert.Close
Set RsRespuesta = Nothing
Set QyInsert = Nothing
fblnFase = True

End Function

Private Function fblnTipRec(intTipRec As Integer) As Boolean
Dim SQL As String
Dim qryInsert As rdoQuery
Dim rstRespuesta As rdoResultset

'Se obtiene el Tipo de Recurso al que pertenece al Doctor
SQL = "SELECT AG14CODTIPRECU FROM AG1100 WHERE AG11CODRECURSO = ?"
Set qryInsert = objApp.rdoConnect.CreateQuery("", SQL)
   qryInsert(0) = strRecurso
Set rstRespuesta = qryInsert.OpenResultset()
If rstRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   intTipRec = rstRespuesta(0)
   rstRespuesta.MoveNext
   If Not rstRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
rstRespuesta.Close
qryInsert.Close
Set rstRespuesta = Nothing
Set qryInsert = Nothing
fblnTipRec = True

End Function

'Devuelve el siguiente valor que se puede usar de un campo clave que no dispone de SEQUENCE
'Si se produce alg�n error, devuelve -1
Public Function flngNextClave(strCampo As String, strTabla As String) As Long

Dim rstClave As rdoResultset

On Error Resume Next
Err = 0
Set rstClave = objApp.rdoConnect.OpenResultset("SELECT MAX(" & strCampo & ") + 1 FROM " & strTabla)
If IsNull(rstClave(0)) Then flngNextClave = 1 Else flngNextClave = rstClave(0)
rstClave.Close
Set rstClave = Nothing
If Err > 0 Then flngNextClave = -1

End Function
Private Function fblnRecFaseAct(lngCodActuacion As Long, lngNumNecesid As Long, _
                                 lngTiemRec As Long, intIndPlan As Integer) As Boolean
Dim SQL As String
Dim QyInsert As rdoQuery
Dim RsRespuesta As rdoResultset

SQL = "SELECT PR13NUMNECESID, PR13NUMTIEMPREC, PR13INDPLANIF FROM PR1300 WHERE "
SQL = SQL & "PR01CODACTUACION = ? AND PR13INDPREFEREN = -1 AND PR13INDPLANIF = -1"
Set QyInsert = objApp.rdoConnect.CreateQuery("", SQL)
   QyInsert(0) = lngCodActuacion
Set RsRespuesta = QyInsert.OpenResultset()
If RsRespuesta.EOF Then 'Error, ning�n registro devuelto
   Exit Function
Else
   lngNumNecesid = RsRespuesta(0)
   lngTiemRec = RsRespuesta(1)
   intIndPlan = RsRespuesta(2)
   RsRespuesta.MoveNext
   If Not RsRespuesta.EOF Then 'Error, m�s de un registro
      Exit Function
   End If
End If
RsRespuesta.Close
QyInsert.Close
Set RsRespuesta = Nothing
Set QyInsert = Nothing
fblnRecFaseAct = True

End Function
Private Sub pCitar()
   txtActual.Text = "Citando..."
   If fblnCitar Then 'Citar
      txtActual.Text = "Citado"
      'Asociar Proceso-Asistencia *****- SE QUITAR� CUANDO DESAPAREZCA EL IBM -*****
      ReDim vntData(2) As Variant
      vntData(1) = IdPersona1.Text
      vntData(2) = 2 'Viene de Citas
      Screen.MousePointer = vbDefault
      Call objSecurity.LaunchProcess("AD0105", vntData)
      If Not blnInsert_Restri Then
        MsgBox "Ha habido errores al asociar las restrinciones", vbCritical
      End If
   Else
      MsgBox "Se ha producido un error al citar. Por favor, int�ntelo de nuevo", _
               vbCritical
   End If 'fblnCitar
    
End Sub
Private Sub pCitas_Urgencias()

    Dim intMsgBox As Integer
    Dim intContador As Integer
    
    Screen.MousePointer = vbHourglass
                  
    If IdPersona1.Text <> "" Then 'Si se ha seleccionado una persona...
        If MsgBox("�Quiere realizar la citaci�n?", vbYesNo, "Citaci�n") = vbYes Then
            FechaActual = SSDCFecha.Text
            If IdPersona1.historia > 0 Then
                strRecurso = SSDBCmbRecurso.Columns(0).Text
                Call pCitar
            Else
                MsgBox "C�digo de persona " & IdPersona1.Text, vbInformation, "Sin Historia, complete datos e int�ntelo m�s tarde"
            End If
        End If 'MsgBox
    End If 'idpersona1.text <> 0
    Screen.MousePointer = Default
        
End Sub
Private Function fValorRest(lngCodRest As Long, proceso As Long, asistencia As Long) As Variant
    Dim strSelect As String
    Dim rstSelect As rdoResultset
    
    Select Case lngCodRest
      Case lngCodResUsu
        fValorRest = objSecurity.strUser
      Case lngTipoPac
        If proceso <> 0 And asistencia <> 0 Then
            strSelect = "select AD10DESTIPPACIEN from ad1000 where AD10CODTIPPACIEN in " & _
                        " (select AD10CODTIPPACIEN from ad0800 where AD07CODPROCESO=" & _
                        proceso & " and AD01CODASISTENCI=" & asistencia & ")"
            Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
            If rstSelect.EOF = False Then
                fValorRest = rstSelect(0).Value
            Else
                fValorRest = " "
            End If
        Else
           fValorRest = " "
        End If
      Case lngTipoEco
        If proceso <> 0 And asistencia <> 0 Then
            strSelect = "select CI32DESTIPECON from ci3200 where CI32CODTIPECON in " & _
                        " (select CI32CODTIPECON from ad1100 where AD07CODPROCESO=" & _
                        proceso & " and AD01CODASISTENCI=" & asistencia & ")"
            Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
            If rstSelect.EOF = False Then
                fValorRest = rstSelect(0).Value
            Else
                fValorRest = " "
            End If
        Else
            fValorRest = " "
        End If
      Case lngEdad
        strSelect = "select CI22FECNACIM from ci2200 where CI21CODPERSONA=" & _
                    IdPersona1.Text
        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        If IsNull(rstSelect(0).Value) = False Then
            fValorRest = DateDiff("d", rstSelect(0).Value, Date) \ 365
        Else
            fValorRest = " "
        End If
      Case lngSexo
        strSelect = "select CI30DESSEXO from ci3000 where CI30CODSEXO in " & _
                    " (select CI30CODSEXO from ci2200 where CI21CODPERSONA=" & txtText1(0).Text & ")"
        Set rstSelect = objApp.rdoConnect.OpenResultset(strSelect)
        If rstSelect.EOF = False Then
            fValorRest = rstSelect(0).Value
        Else
            fValorRest = " "
        End If
'''      Case lngDpto
'''        fValorRest = frmpedir.txtText1(1).Text
      Case Else
        fValorRest = " "
    End Select
End Function

Private Sub SSDBCmbRecurso_CloseUp()
    
    'Si esta rellenos los datos: - Historia - fecha  - Dpto - Prueba - Recurso
    'Se habilitar� el bot�n para citar
    If IdPersona1.historia <> "" And SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" And SSDBCmbRecurso.Value <> "" Then
        BUTCitas(iBUTCita).Enabled = True
    Else
        BUTCitas(iBUTCita).Enabled = False
    End If

End Sub

Private Sub SSDCFecha_Change()
   'Si esta rellenos los datos: - Historia - fecha  - Dpto - Prueba - Recurso
    'Se habilitar� el bot�n para citar
    If IdPersona1.historia <> "" And SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" And SSDBCmbRecurso.Value <> "" Then
        BUTCitas(iBUTCita).Enabled = True
    Else
        BUTCitas(iBUTCita).Enabled = False
    End If

End Sub
Private Sub TxtCita_Change(Index As Integer)
    
    Dim sStmSql     As String
    Dim qryPersona  As rdoQuery
    Dim rstHistoria As rdoResultset
    
    If bHistoria Then
        bHistoria = False
        Exit Sub
    End If
    'Buscaremos el c�digo de persona por el n�mero de Hist�ria
    If TxtCita(iTXTHIS) <> "" Then
        sStmSql = "SELECT CI21CODPERSONA FROM CI2200 WHERE CI22NUMHISTORIA = ? "
        Set qryPersona = objApp.rdoConnect.CreateQuery("", sStmSql)
        qryPersona(0) = TxtCita(iTXTHIS)
        Set rstHistoria = qryPersona.OpenResultset()
        If Not IsNull(rstHistoria(0)) And Not rstHistoria.EOF Then
            IdPersona1.Text = rstHistoria(0)
        Else
            bPersona = True
            IdPersona1.Text = ""
            bPersona = False
        End If
        rstHistoria.Close
        qryPersona.Close
    Else
        bPersona = True
        IdPersona1.Text = ""
        bPersona = False
    End If
    If TxtCita(iTXTHIS) <> "" And SSDCFecha.Text <> "" And SSDBCmbDpto.Value <> "" And SSDBCmbPrueba.Value <> "" And SSDBCmbRecurso.Value <> "" Then
         BUTCitas(iBUTCita).Enabled = True
         BUTCitas(iBUTSALIR).Enabled = True
    Else
         BUTCitas(iBUTCita).Enabled = False
         BUTCitas(iBUTSALIR).Enabled = True
    End If
        
End Sub

Private Sub TxtCita_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If (Shift = 1 And KeyCode = 45) Or (Shift = 1 And KeyCode = 46) Or (Shift = 2 And KeyCode = 45) Then
        KeyCode = 0
        Shift = 0
    End If
    If KeyCode = 8 Then
        SendKeys Chr(0)
    End If
End Sub

Private Sub TxtCita_KeyPress(Index As Integer, KeyAscii As Integer)
    'Validaci�n de la tecla pulsada en funci�n de los
    'requisitos establecidos para el campo en la Tabla
    If KeyAscii = Asc("'") Then KeyAscii = 0
    If KeyAscii = Asc("�") Then KeyAscii = 0

    If (KeyAscii < Asc("0") Or KeyAscii > Asc("9")) And KeyAscii <> 8 Then
        KeyAscii = 0   'nada
    End If
End Sub

Private Sub txtHora_Change(Index As Integer)
 If Len(txtHora(Index)) = 2 Then
        If CInt(txtHora(Index)) < 0 Or CInt(txtHora(Index)) > 23 Then
            MsgBox "El rango para las horas es de 0 a 23", vbInformation
            txtHora(Index) = "00"
        End If
    End If
End Sub
Private Sub txtHora_LostFocus(Index As Integer)
    If Len(txtHora(Index)) < 2 Then
        txtHora(Index) = Format(txtHora(Index), "00")
    End If
End Sub
Private Sub txtMinuto_Change(Index As Integer)
 If Len(txtMinuto(Index)) = 2 Then
        If CInt(txtMinuto(Index)) < 0 Or CInt(txtMinuto(Index)) > 59 Then
            MsgBox "El rango para los minutos es de 0 a 59", vbInformation
            txtMinuto(Index) = "00"
        End If
    End If
End Sub

Private Sub UpDownHora_DownClick(Index As Integer)

    If txtHora(Index) > UpDownHora(Index).Max Then
        txtHora(Index) = UpDownHora(Index).Max
    ElseIf CInt(txtHora(Index)) <= UpDownHora(Index).Min Then
        txtHora(Index) = UpDownHora(Index).Min
    Else
        txtHora(Index) = txtHora(Index) - UpDownHora(Index).Increment
    End If
    If Len(txtHora(Index)) < 2 Then
        txtHora(Index) = Format(txtHora(Index), "00")
    End If
End Sub
Private Sub UpDownHora_UpClick(Index As Integer)
    
    If txtHora(Index) >= UpDownHora(Index).Max Then
        txtHora(Index) = UpDownHora(Index).Max
    ElseIf txtMinuto(Index) < UpDownHora(Index).Min Then
        txtHora(Index) = UpDownHora(Index).Min
    Else
        txtHora(Index) = txtHora(Index) + UpDownHora(Index).Increment
    End If
    If Len(txtHora(Index)) < 2 Then
        txtHora(Index) = Format(txtHora(Index), "00")
    End If
    
End Sub

Private Sub UpDownMinuto_DownClick(Index As Integer)
    If txtMinuto(Index) > UpDownMinuto(Index).Max Then
        txtMinuto(Index) = UpDownMinuto(Index).Max
    ElseIf txtMinuto(Index) <= UpDownMinuto(Index).Min Then
        txtMinuto(Index) = UpDownMinuto(Index).Min
    Else
        txtMinuto(Index) = txtMinuto(Index) - UpDownMinuto(Index).Increment
    End If
    If Len(txtMinuto(Index)) < 2 Then
        txtMinuto(Index) = Format(txtMinuto(Index), "00")
    End If
End Sub
Private Sub UpDownMinuto_UpClick(Index As Integer)
       
    If txtMinuto(Index) >= UpDownMinuto(Index).Max Then
        txtMinuto(Index) = UpDownMinuto(Index).Max
    ElseIf CInt(txtMinuto(Index)) < UpDownMinuto(Index).Min Then
        txtMinuto(Index) = UpDownMinuto(Index).Min
    Else
        txtMinuto(Index) = txtMinuto(Index) + UpDownMinuto(Index).Increment
    End If
    If Len(txtMinuto(Index)) < 2 Then
        txtMinuto(Index) = Format(txtMinuto(Index), "00")
    End If
End Sub
Private Sub pCargar_Recurso() 'Carga los combos Recurso en base a la prueba seleccionada

'Consultas SQL
Dim SQL As String
Dim qryConsulta As rdoQuery
Dim rstConsulta As rdoResultset

'Inicilizar los ComboBox de Recursos
SSDBCmbRecurso.Text = ""
SSDBCmbRecurso.RemoveAll

'Recursos
SQL = "SELECT DISTINCT AG11CODRECURSO, AG11DESRECURSO "
SQL = SQL & " FROM AG1100, AD0300, PR1300, AG1400 "
SQL = SQL & " WHERE  AD0300.AD02CODDPTO = ? "                                           'Dpto seleccionado
SQL = SQL & " AND     PR1300.PR01CODACTUACION = ? "                                     '-- actuaci�n seleccionada
SQL = SQL & " AND     PR1300.PR13INDPREFEREN = -1 "                                     '-- Sea preferente"
SQL = SQL & " AND     PR1300.PR13INDPLANIF = -1 "                                       '-- Planificable el tipo
SQL = SQL & " AND     AG1100.AG14CODTIPRECU = AG1400.AG14CODTIPRECU "                   '-- Recursos con tipos de recursos
SQL = SQL & " AND     PR1300.AG14CODTIPRECU  = AG1400.AG14CODTIPRECU "                  '-- Relacionado con AG1400 tipo recurso
SQL = SQL & " AND     AD0300.AD03FECINICIO <= SYSDATE "
SQL = SQL & " AND     (AD0300.AD03FECFIN >= SYSDATE OR AD0300.AD03FECFIN IS NULL) "
SQL = SQL & " AND     (AD0300.SG02COD = AG1100.SG02COD OR AG1100.SG02COD IS NULL ) "
SQL = SQL & " AND     AD0300.AD02CODDPTO = AG1100.AD02CODDPTO "
SQL = SQL & " AND     AG1100.AG11FECINIVREC <= SYSDATE "
SQL = SQL & " AND     (AG1100.AG11FECFINVREC >= SYSDATE OR AG1100.AG11FECFINVREC IS NULL) "
SQL = SQL & " ORDER BY AG11DESRECURSO "

Set qryConsulta = objApp.rdoConnect.CreateQuery("", SQL)
   qryConsulta(0) = SSDBCmbDpto.Columns(0).Text
   qryConsulta(1) = SSDBCmbPrueba.Columns(0).Text
Set rstConsulta = qryConsulta.OpenResultset()
Do While Not rstConsulta.EOF
   SSDBCmbRecurso.AddItem rstConsulta(0) & ";" & rstConsulta(1)
   rstConsulta.MoveNext
Loop
rstConsulta.Close
qryConsulta.Close

End Sub
Private Sub pCargar_Actividad() 'Carga los combos de Actividades
    Dim SQL As String
    Dim qryCombos As rdoQuery
    Dim rstCombos As rdoResultset
    
    '...Tipo de actividad para despues cargar las pruebas
    SQL = "SELECT PR12CODACTIVIDAD , PR12DESACTIVIDAD "
    SQL = SQL & "FROM PR1200 "
    SQL = SQL & "WHERE PR12INDPLANIFIC = ? "
    SQL = SQL & "ORDER BY PR12DESACTIVIDAD "
    Set qryCombos = objApp.rdoConnect.CreateQuery("Actividades", SQL)
       qryCombos(0) = -1
    Set rstCombos = qryCombos.OpenResultset()
    Do While Not rstCombos.EOF
       SSDBCmbActividad.AddItem rstCombos(0) & ";" & rstCombos(1)
       rstCombos.MoveNext
    Loop 'While Not rstCombos.EOF
    rstCombos.Close
    qryCombos.Close
End Sub
Private Sub pCargar_Departamentos() 'Carga la combos de Departamentos
        
    Dim SQL As String
    Dim qryCombos As rdoQuery
    Dim rstCombos As rdoResultset
       
    SQL = "SELECT AD0200.AD02CODDPTO, AD02DESDPTO FROM AD0200, AD0300 WHERE "
    'Departamentos a los que tiene acceso ese usuario (AD0300)
    SQL = SQL & "AD0300.SG02COD = ? AND AD03FECINICIO <= SYSDATE AND "
    SQL = SQL & "(AD03FECFIN >= SYSDATE OR AD03FECFIN IS NULL) AND "
    'Descripci�n del departamento (AD0200)
    SQL = SQL & "AD0300.AD02CODDPTO = AD0200.AD02CODDPTO AND AD02FECINICIO <= SYSDATE "
    SQL = SQL & "AND (AD02FECFIN >= SYSDATE OR AD02FECFIN IS NULL) AND "
    SQL = SQL & "(AD32CODTIPODPTO = 1 OR AD32CODTIPODPTO = 2)" 'Serv. Cl�nicos o B�sicos
    SQL = SQL & "AND AD02INDRESPONPROC  = -1 ORDER BY AD02DESDPTO"
    
    Set qryCombos = objApp.rdoConnect.CreateQuery("Departamentos", SQL)
        qryCombos(0) = objSecurity.strUser
    Set rstCombos = qryCombos.OpenResultset()
    Do While Not rstCombos.EOF
        SSDBCmbDpto.AddItem rstCombos(0) & ";" & rstCombos(1)
        rstCombos.MoveNext
    Loop 'While Not rstCombos.EOF
    rstCombos.Close
    qryCombos.Close
End Sub

