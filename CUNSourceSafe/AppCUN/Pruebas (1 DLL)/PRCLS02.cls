VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
'Const SGRepApps         As String = "SG0001"
'Const SGRepGroups       As String = "SG0002"
'Const SGRepRoles        As String = "SG0003"
'Const SGRepRolesGroup   As String = "SG0004"
'Const SGRepUsers        As String = "SG0005"
'Const SGRepUsersGroup   As String = "SG0006"
'Const SGRepUsersRol     As String = "SG0007"
'Const SGRepProcessRol   As String = "SG0008"
'Const SGRepProcessUser  As String = "SG0009"
'Const SGRepProcessApp   As String = "SG0010"
'Const SGRepTablesRol    As String = "SG0011"
'Const SGRepTablesUser   As String = "SG0012"
'Const SGRepColumnsRol   As String = "SG0013"
'Const SGRepColumnsUser  As String = "SG0014"
'Const SGRepAuditTables  As String = "SG0015"
'Const SGRepLogTable     As String = "SG0016"
'Const SGRepLogUser      As String = "SG0017"

' Ahora las ventanas. Continuan la numeraci�n a partir del SG1000


Const PRWinPedirR                   As String = "PR00152"
'Const PRWinRealActPrincipal        As String = "PR00133"
Const PRWinPeticPend               As String = "PR00137"
Const PRWinRecibirPaciente         As String = "PR00139"
Const PRWinPruebasAdicionales      As String = "PR00138"
Const PRWinTerminarPrueba          As String = "PR00174"
Const PRWinSituacionPaciente       As String = "PR00173"
Const PRWinCancelar                As String = "PR00132"
Const prwinEstadoPruebas        As String = "PR00134"
Const PRWinInformarActuaciones     As String = "PR00190"
Const PRWinFirmarActuaciones       As String = "PR00191"
Const PRWinIdentificacionUsuario   As String = "PR00192"
' Ahora las ventanas. Continuan la numeraci�n a partir del SG1000

'Const SGWinApps         As String = "SG1001"
'Const SGWinGroups       As String = "SG1002"
'Const SGWinUsers        As String = "SG1003"
'Const SGWinRoles        As String = "SG1004"
'Const SGWinRolesGroup   As String = "SG1005"
'Const SGWinTypeProcess  As String = "SG1006"
'Const SGWinProcess      As String = "SG1007"
'Const SGWinFuncs        As String = "SG1008"
'Const SGWinSegTabCol    As String = "SG1009"
'Const SGWinChangePW     As String = "SG1010"
'Const SGWinForzePW      As String = "SG1011"
'Const SGWinProcessRol   As String = "SG1012"
'Const SGWinDelega       As String = "SG1013"
'Const SGWinPurgeDelega  As String = "SG1014"
'Const SGWinQueryDelega  As String = "SG1015"
'Const SGWinVenLis       As String = "SG1016"
'Const SGWinPrnTabCol    As String = "SG1017"
'Const SGWinEncript      As String = "SG1018"
'Const SGWinAuditTables  As String = "SG1019"
'Const SGWinViewAudit    As String = "SG1020"
'Const SGWinMaintAudit   As String = "SG1021"
'Const SGWinLoadProcess  As String = "SG1022"


' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    'Case PRWinRealActPrincipal
    '  Load frmrealactprincipal
    '  Call objsecurity.AddHelpContext(25)
    '  Call frmrealactprincipal.Show(vbModal)
    '  Call objsecurity.RemoveHelpContext
    '  Unload frmrealactprincipal
    '  Set frmrealactprincipal = Nothing
    Case PRWinPedirR
      'Call PrjPeticionPruebas.LaunchProcess("prwinpedirR")
      Load frmpedir
      Call frmpedir.Show(vbModal)
      Unload frmpedir
      Set frmpedir = Nothing
    Case PRWinPeticPend
      Load frmPeticPend
      Call frmPeticPend.Show(vbModal)
      Unload frmPeticPend
      Set frmPeticPend = Nothing
    Case PRWinRecibirPaciente
      Load frmRecibirPaciente
      Call frmRecibirPaciente.Show(vbModal)
      Unload frmRecibirPaciente
      Set frmRecibirPaciente = Nothing
    Case PRWinPruebasAdicionales
      Load frmpruebasadicionales
      Call frmpruebasadicionales.Show(vbModal)
      Unload frmpruebasadicionales
      Set frmpruebasadicionales = Nothing
    Case PRWinTerminarPrueba
      Load frmTerminarPrueba
      Call frmTerminarPrueba.Show(vbModal)
      Unload frmTerminarPrueba
      Set frmTerminarPrueba = Nothing
    Case PRWinSituacionPaciente
      Load frmsituacionpaciente
      Call frmsituacionpaciente.Show(vbModal)
      Unload frmsituacionpaciente
      Set frmsituacionpaciente = Nothing
    Case PRWinCancelar
      Load frmcancelar
      Call frmcancelar.Show(vbModal)
      Unload frmcancelar
      Set frmcancelar = Nothing
    Case prwinEstadoPruebas
      Load frmestadopruebas
      Call frmestadopruebas.Show(vbModal)
      Unload frmestadopruebas
      Set frmestadopruebas = Nothing
    Case PRWinInformarActuaciones
      Load frmInformarActuaciones
      'If frmSelPaquetes.Prepare(objsecurity.strUser, False) Then
        Call frmInformarActuaciones.Show(vbModal)
      'End If
      Unload frmInformarActuaciones
      Set frmInformarActuaciones = Nothing
    Case PRWinFirmarActuaciones
      Load frmFirmarActuaciones
      'If frmSelProtocolos.Prepare(vntData, True) Then
        Call frmFirmarActuaciones.Show(vbModal)
      'End If
      Unload frmFirmarActuaciones
      Set frmFirmarActuaciones = Nothing
    Case PRWinIdentificacionUsuario
      Load frmIdentificacionUsuario
      Call frmIdentificacionUsuario.Show(vbModal)
      Unload frmIdentificacionUsuario
      Set frmIdentificacionUsuario = Nothing
    
    'Case SGRepProcessUser
    '  Call ReportProcessUser
    'Case SGRepLogTable
    '  Call ReportAudit(True)
    'Case SGRepLogUser
    '  Call ReportAudit(False)
    'Case Else
    '  ' el c�digo de proceso no es v�lido y se devuelve falso
    '  LaunchProcess = False
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 39 procesos
  ReDim aProcess(1 To 12, 1 To 4) As Variant
      
  ' VENTANAS
  'aProcess(1, 1) = PRWinRealActPrincipal
  'aProcess(1, 2) = "Menu Principal Realizar"
  'aProcess(1, 3) = True
  'aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = PRWinPedirR
  aProcess(2, 2) = "Peticion de Pruebas Adicionales"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = PRWinPeticPend
  aProcess(3, 2) = "Peticiones Pendientes"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = PRWinRecibirPaciente
  aProcess(4, 2) = "Recibir al Paciente"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = PRWinPruebasAdicionales
  aProcess(5, 2) = "Realizacion de Pruebas"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = PRWinTerminarPrueba
  aProcess(6, 2) = "Terminacion de Pruebas"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = PRWinSituacionPaciente
  aProcess(7, 2) = "Informar Situacion Paciente"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = PRWinCancelar
  aProcess(8, 2) = "Cancelacion de Pruebas"
  aProcess(8, 3) = False
  aProcess(8, 4) = cwTypeWindow
      
  aProcess(9, 1) = prwinEstadoPruebas
  aProcess(9, 2) = "Estados de las Pruebas"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = PRWinInformarActuaciones
  aProcess(10, 2) = "Informar Actuaciones"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow
      
  aProcess(11, 1) = PRWinFirmarActuaciones
  aProcess(11, 2) = "Firmar Actuaciones"
  aProcess(11, 3) = False
  aProcess(11, 4) = cwTypeWindow
      
  aProcess(12, 1) = PRWinIdentificacionUsuario
  aProcess(12, 2) = "Identificacion de Usuario"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
      
      
  ' LISTADOS
  'aProcess(23, 1) = SGRepApps
  'aProcess(23, 2) = "Relaci�n de Aplicaciones"
  'aProcess(23, 3) = False
  'aProcess(23, 4) = cwTypeReport
      
  'aProcess(24, 1) = SGRepGroups
  'aProcess(24, 2) = "Relaci�n de Grupos de Usuarios"
  'aProcess(24, 3) = False
  'aProcess(24, 4) = cwTypeReport
  
  'aProcess(25, 1) = SGRepRoles
  'aProcess(25, 2) = "Relaci�n de Roles"
  'aProcess(25, 3) = False
  'aProcess(25, 4) = cwTypeReport
  
  'aProcess(26, 1) = SGRepRolesGroup
  'aProcess(26, 2) = "Relaci�n de Roles por Grupo"
  'aProcess(26, 3) = False
  'aProcess(26, 4) = cwTypeReport

  'aProcess(27, 1) = SGRepUsers
  'aProcess(27, 2) = "Relaci�n de Usuarios"
  'aProcess(27, 3) = False
  'aProcess(27, 4) = cwTypeReport
  
  'aProcess(28, 1) = SGRepUsersGroup
  'aProcess(28, 2) = "Relaci�n de Usuarios por Grupo"
  'aProcess(28, 3) = False
  'aProcess(28, 4) = cwTypeReport

  'aProcess(29, 1) = SGRepUsersRol
  'aProcess(29, 2) = "Relaci�n de Usuarios por Rol"
  'aProcess(29, 3) = False
  'aProcess(29, 4) = cwTypeReport

  'aProcess(30, 1) = SGRepProcessRol
  'aProcess(30, 2) = "Relaci�n de Procesos por Rol"
  'aProcess(30, 3) = False
  'aProcess(30, 4) = cwTypeReport

  'aProcess(31, 1) = SGRepProcessUser
  'aProcess(31, 2) = "Relaci�n de Procesos por Usuarios"
  'aProcess(31, 3) = True
  'aProcess(31, 4) = cwTypeReport

  'aProcess(32, 1) = SGRepProcessApp
  'aProcess(32, 2) = "Relaci�n de Procesos por Aplicaci�n"
  'aProcess(32, 3) = False
  'aProcess(32, 4) = cwTypeReport

  'aProcess(33, 1) = SGRepTablesRol
  'aProcess(33, 2) = "Restricciones en Tablas por Rol"
  'aProcess(33, 3) = False
  'aProcess(33, 4) = cwTypeReport

  'aProcess(34, 1) = SGRepTablesUser
  'aProcess(34, 2) = "Restricciones en Tablas por Usuario"
  'aProcess(34, 3) = False
  'aProcess(34, 4) = cwTypeReport

  'aProcess(35, 1) = SGRepColumnsRol
  'aProcess(35, 2) = "Restricciones en Columnas por Rol"
  'aProcess(35, 3) = False
  'aProcess(35, 4) = cwTypeReport

  'aProcess(36, 1) = SGRepColumnsUser
  'aProcess(36, 2) = "Restricciones en Columnas por Usuario"
  'aProcess(36, 3) = False
  'aProcess(36, 4) = cwTypeReport

  'aProcess(37, 1) = SGRepAuditTables
  'aProcess(37, 2) = "Relaci�n de Tablas a auditar"
  'aProcess(37, 3) = False
  'aProcess(37, 4) = cwTypeReport

  'aProcess(38, 1) = SGRepLogTable
  'aProcess(38, 2) = "Relaci�n del Log de Auditor�a por Tabla"
  'aProcess(38, 3) = True
  'aProcess(38, 4) = cwTypeReport

  'aProcess(39, 1) = SGRepLogUser
  'aProcess(39, 2) = "Relaci�n del Log de Auditor�a por Usuario"
  'aProcess(39, 3) = True
  'aProcess(39, 4) = cwTypeReport
End Sub
