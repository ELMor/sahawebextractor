VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frminteraccion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Mantenimiento de Interacciones"
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0210.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Interacciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Index           =   0
      Left            =   360
      TabIndex        =   7
      Top             =   720
      Width           =   10725
      Begin TabDlg.SSTab tabTab1 
         Height          =   5940
         Index           =   0
         Left            =   240
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   480
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   10478
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0210.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(9)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboSSDBCombo1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(4)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(2)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(3)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(5)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(6)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtdia"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txthora"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtminuto"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0210.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtminuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   3720
            MaxLength       =   2
            TabIndex        =   6
            Top             =   4200
            Width           =   615
         End
         Begin VB.TextBox txthora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   5
            Top             =   4200
            Width           =   615
         End
         Begin VB.TextBox txtdia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   960
            MaxLength       =   4
            TabIndex        =   4
            Top             =   4200
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   3960
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n Destino"
            Top             =   840
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   5
            Left            =   3240
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n Destino"
            Top             =   3240
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   3
            Left            =   3240
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n Origen"
            Top             =   2040
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR01CODACTUACION_DES"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   960
            TabIndex        =   3
            Tag             =   "Actuaci�n Destino"
            Top             =   3240
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR20NUMTIEMPOINT"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   960
            TabIndex        =   8
            Tag             =   "Tiempo de Interacci�n|Tiempo de Interacci�n (medido en minutos)"
            Top             =   5160
            Visible         =   0   'False
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR18NUMDEFINTER"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   960
            Locked          =   -1  'True
            TabIndex        =   0
            Tag             =   "N�mero de Interacci�n"
            Top             =   825
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   960
            TabIndex        =   2
            Tag             =   "Actuaci�n Origen"
            Top             =   2040
            Width           =   1092
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5625
            Index           =   0
            Left            =   -74880
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   9922
            _StockProps     =   79
            Caption         =   "INTERACCIONES"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR19CODTIPINTERAC"
            Height          =   330
            Index           =   0
            Left            =   3240
            TabIndex        =   1
            Tag             =   "Tipo de Interacci�n"
            Top             =   840
            Width           =   660
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1164
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   4440
            TabIndex        =   25
            Top             =   4320
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   3000
            TabIndex        =   24
            Top             =   4320
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1680
            TabIndex        =   22
            Top             =   4320
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Actuaci�n Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   3240
            TabIndex        =   21
            Top             =   3000
            Width           =   2640
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Actuaci�n Origen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   3240
            TabIndex        =   20
            Top             =   1800
            Width           =   2550
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   960
            TabIndex        =   14
            Top             =   3000
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tiempo "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   960
            TabIndex        =   18
            Top             =   3960
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   3255
            TabIndex        =   11
            Top             =   600
            Width           =   450
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� de Interacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   960
            TabIndex        =   17
            Top             =   600
            Width           =   1515
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n Origen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   960
            TabIndex        =   16
            Top             =   1800
            Width           =   1485
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frminteraccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: MANTENIMIENTOS PRUEBAS C.U.N. (PR002.VBP)                  *
'* NOMBRE: FRMINTERACIONES (PR002010.FRM)                                *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: mantenimiento de interacciones                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************



Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim TiempoTotal As Variant
'intcambioalgo detecta si se cambia alguna caja de texto o combo
Dim intcambioalgo As Integer

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Interacciones"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR2000"
    .intAllowance = cwAllowDelete + cwAllowModify
    
    Call .FormAddOrderField("pr18numdefinter", cwAscending)
    Call .FormAddOrderField("pr01codactuacion", cwAscending)
    Call .FormAddOrderField("pr01codactuacion_des", cwAscending)
    
    .blnHasMaint = True
  
    Call .objPrinter.Add("PR2101", "Listado de Interacciones Actuaci�n-Actuaci�n")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Interacciones")
    Call .FormAddFilterWhere(strKey, "PR18NUMDEFINTER", "N�mero Interacci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "Actuaci�n Origen", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION_DES", "Actuaci�n Destino", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR19CODTIPINTERAC", "Tipo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR20NUMTIEMPOINT", "Tiempo", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "PR18NUMDEFINTER", "N�mero Interacci�n")
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "Actuaci�n Origen")
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION_DES", "Actuaci�n Destino")
    Call .FormAddFilterOrder(strKey, "PR19CODTIPINTERAC", "Tipo")
    Call .FormAddFilterOrder(strKey, "PR20NUMTIEMPOINT", "Tiempo")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    
    .CtrlGetInfo(txtText1(0)).blnMandatory = True
    .CtrlGetInfo(txtText1(1)).blnMandatory = True
    .CtrlGetInfo(txtText1(2)).blnMandatory = True
    .CtrlGetInfo(txtText1(4)).blnMandatory = True
    .CtrlGetInfo(cboSSDBCombo1(0)).blnMandatory = True
    
    .CtrlGetInfo(txtdia).blnNegotiated = False
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    
    'tipos de interacci�n
    .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR19CODTIPINTERAC, PR19DESTIPINTERAC FROM PR1900"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(3), "PR01DESCORTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "PR01DESCORTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR19CODTIPINTERAC", "SELECT * FROM PR1900 WHERE PR19CODTIPINTERAC=?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtText1(6), "PR19DESTIPINTERAC")
    
    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  intcambioalgo = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Interacciones" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

'*********************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" And txtText1(4).Text <> "" And _
    cboSSDBCombo1(0).Value <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR2000 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr20numtiempoint=" & TiempoTotal & _
         " AND pr19codtipinterac=" & cboSSDBCombo1(0).Value
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'***********************************************************************
If intcambioalgo = 1 Then
  objWinInfo.objWinActiveForm.blnChanged = True
End If
 
Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

 'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If btnButton.Index = 16 Then
    Call txtText1_Change(4)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" And txtText1(4).Text <> "" And _
    cboSSDBCombo1(0).Value <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR2000 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr20numtiempoint=" & TiempoTotal & _
         " AND pr19codtipinterac=" & cboSSDBCombo1(0).Value
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" And txtText1(4).Text <> "" And _
    cboSSDBCombo1(0).Value <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR2000 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr20numtiempoint=" & TiempoTotal & _
         " AND pr19codtipinterac=" & cboSSDBCombo1(0).Value
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" And txtText1(4).Text <> "" And _
    cboSSDBCombo1(0).Value <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR2000 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr20numtiempoint=" & TiempoTotal & _
         " AND pr19codtipinterac=" & cboSSDBCombo1(0).Value
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" And txtText1(4).Text <> "" And _
    cboSSDBCombo1(0).Value <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR2000 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr20numtiempoint=" & TiempoTotal & _
         " AND pr19codtipinterac=" & cboSSDBCombo1(0).Value
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)

 'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If intIndex = 16 Then
    Call txtText1_Change(4)
  End If

End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" And txtText1(4).Text <> "" And _
    cboSSDBCombo1(0).Value <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR2000 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr20numtiempoint=" & TiempoTotal & _
         " AND pr19codtipinterac=" & cboSSDBCombo1(0).Value
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  intcambioalgo = 1
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtdia_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 Then
    intcambioalgo = 0
  End If
  
  If intIndex = 4 Then
    If txtText1(4).Text = "" Then
      txtdia.Text = 0
      txthora.Text = 0
      txtminuto.Text = 0
    Else
      txtdia.Text = txtText1(4).Text \ 1440                 'd�as
      txthora.Text = (txtText1(4).Text Mod 1440) \ 60       'horas
      txtminuto.Text = (txtText1(4).Text Mod 1440) Mod 60   'minutos
    End If
  End If

End Sub
Private Sub txtdia_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja d�a vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And txtText1(2).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR2000 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text & _
        " AND pr01codactuacion=" & txtText1(1).Text & _
        " AND pr01codactuacion_des=" & txtText1(2).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR2000 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text & _
        " AND pr01codactuacion=" & txtText1(1).Text & _
        " AND pr01codactuacion_des=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia.Text) = False Then
    Beep
    txtdia.Text = ""
    txtdia.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 6500 Then
      txtdia.Text = 6500
     End If
  End If
End Sub

Private Sub txthora_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja d�a vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And txtText1(2).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR2000 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text & _
        " AND pr01codactuacion=" & txtText1(1).Text & _
        " AND pr01codactuacion_des=" & txtText1(2).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR2000 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text & _
        " AND pr01codactuacion=" & txtText1(1).Text & _
        " AND pr01codactuacion_des=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If
   
  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora.Text) = False Then
    Beep
    txthora.Text = ""
    txthora.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txtminuto_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja d�a vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And txtText1(2).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR2000 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text & _
        " AND pr01codactuacion=" & txtText1(1).Text & _
        " AND pr01codactuacion_des=" & txtText1(2).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR2000 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text & _
        " AND pr01codactuacion=" & txtText1(1).Text & _
        " AND pr01codactuacion_des=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto.Text) = False Then
    Beep
    txtminuto.Text = ""
    txtminuto.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub


Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 6500 Then
    d = 6500
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If

TiempoTotal = (d * 1440) + (h * 60) + m
  
End Sub
