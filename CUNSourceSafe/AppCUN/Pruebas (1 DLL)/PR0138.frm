VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmpruebasadicionales 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Inicio de la Actuaci�n"
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0138.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdTerminarPrueba 
      Caption         =   "Terminar Prueba"
      Height          =   375
      Left            =   6720
      TabIndex        =   17
      Top             =   7680
      Width           =   1935
   End
   Begin VB.CommandButton cmdCancelarPrueba 
      Caption         =   "Cancelar Prueba"
      Height          =   375
      Left            =   4560
      TabIndex        =   16
      Top             =   7680
      Width           =   1935
   End
   Begin VB.CommandButton cmdadicionales 
      BackColor       =   &H00808080&
      Caption         =   "Actuaciones Adicionales"
      Height          =   375
      Left            =   2400
      TabIndex        =   15
      Top             =   7680
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaci�n "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7095
      Index           =   0
      Left            =   120
      TabIndex        =   23
      Top             =   480
      Width           =   11700
      Begin TabDlg.SSTab tabTab1 
         Height          =   6660
         Index           =   0
         Left            =   120
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   11748
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0138.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(7)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(11)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "tabTab1(2)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "tabTab1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(4)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(27)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(26)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(28)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(2)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(3)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(24)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).ControlCount=   20
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0138.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FECHA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   24
            Left            =   7320
            TabIndex        =   63
            TabStop         =   0   'False
            Tag             =   "Fecha de Inicio"
            Top             =   480
            Width           =   1455
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "HORA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   3
            Left            =   9000
            TabIndex        =   43
            TabStop         =   0   'False
            Tag             =   "Hora de Inicio"
            Top             =   480
            Width           =   795
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR09NUMPETICION"
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   3840
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "C�digo de Petici�n"
            Top             =   960
            Visible         =   0   'False
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            HelpContextID   =   30101
            Index           =   28
            Left            =   5040
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Actuaci�n"
            Top             =   960
            Visible         =   0   'False
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR03NUMACTPEDI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   6360
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Actuaci�n Pedida"
            Top             =   960
            Visible         =   0   'False
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   26
            Left            =   10320
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "C�digo del Departamento"
            Top             =   240
            Visible         =   0   'False
            Width           =   555
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            HelpContextID   =   30101
            Index           =   27
            Left            =   10080
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Nombre Departamento Realizador"
            Top             =   840
            Visible         =   0   'False
            Width           =   840
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR04DESINSTREA"
            Height          =   1050
            HelpContextID   =   30104
            Index           =   4
            Left            =   360
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "Instrucciones de Realizaci�n"
            Top             =   5520
            Width           =   10260
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   1935
            Index           =   1
            Left            =   360
            TabIndex        =   27
            Top             =   1080
            Width           =   10215
            _ExtentX        =   18018
            _ExtentY        =   3413
            _Version        =   327681
            Style           =   1
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Paciente"
            TabPicture(0)   =   "PR0138.frx":0044
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(28)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(14)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(15)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(16)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(17)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(18)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtText1(25)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtText1(13)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtText1(14)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtText1(15)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtText1(16)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "txtText1(17)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).ControlCount=   12
            TabCaption(1)   =   "Solicitante de la Actuaci�n"
            TabPicture(1)   =   "PR0138.frx":0060
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(11)"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "txtText1(10)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "txtText1(9)"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "txtText1(21)"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "lblLabel1(9)"
            Tab(1).Control(5)=   "lblLabel1(1)"
            Tab(1).Control(6)=   "lblLabel1(24)"
            Tab(1).ControlCount=   7
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PETICIONARIO"
               Height          =   330
               HelpContextID   =   30101
               Index           =   11
               Left            =   -74640
               TabIndex        =   10
               TabStop         =   0   'False
               Tag             =   "C�d.Dpto. Peticionario"
               Top             =   660
               Width           =   372
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               HelpContextID   =   30104
               Index           =   10
               Left            =   -74640
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "C�d.Persona Peticionaria"
               Top             =   1440
               Width           =   852
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   9
               Left            =   -74040
               TabIndex        =   11
               TabStop         =   0   'False
               Tag             =   "Descripci�n Dpto. Peticionario"
               Top             =   660
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   21
               Left            =   -73200
               TabIndex        =   13
               TabStop         =   0   'False
               Tag             =   "Primer Apellido del Peticionario"
               Top             =   1440
               Width           =   2535
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   17
               Left            =   3360
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "D.N.I  del Paciente"
               Top             =   780
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   16
               Left            =   1920
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "N�mero de Historia"
               Top             =   780
               Width           =   852
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   15
               Left            =   6480
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Segundo Apellido del Paciente"
               Top             =   1440
               Width           =   3255
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   14
               Left            =   3120
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Primer Apellido del Paciente"
               Top             =   1440
               Width           =   3255
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   13
               Left            =   240
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "Nombre del Paciente"
               Top             =   1440
               Width           =   2655
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "CI21CODPERSONA"
               Height          =   330
               Index           =   25
               Left            =   240
               TabIndex        =   4
               TabStop         =   0   'False
               Tag             =   "C�digo del Paciente"
               Top             =   780
               Width           =   852
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Departamento Peticionario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   -74640
               TabIndex        =   36
               Top             =   420
               Width           =   2265
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Doctor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   -74640
               TabIndex        =   35
               Top             =   1200
               Width           =   1230
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   24
               Left            =   -73200
               TabIndex        =   34
               Top             =   1200
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 2�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   6480
               TabIndex        =   33
               Top             =   1200
               Width           =   1095
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Apellido 1�"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   3120
               TabIndex        =   32
               Top             =   1200
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   16
               Left            =   240
               TabIndex        =   31
               Top             =   1200
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               Caption         =   "D.N.I"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   3360
               TabIndex        =   30
               Top             =   540
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Historia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   1920
               TabIndex        =   29
               Top             =   540
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d.Persona"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   28
               Left            =   240
               TabIndex        =   28
               Top             =   540
               Width           =   1215
            End
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR04NUMACTPLAN"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   360
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "Actuaci�n Planificada"
            Top             =   480
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   1560
            Locked          =   -1  'True
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "Descripci�n de la Actuaci�n"
            Top             =   480
            Width           =   5400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5865
            Index           =   0
            Left            =   -74880
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   120
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   10345
            _StockProps     =   79
            Caption         =   "ACTUACIONES"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab tabTab1 
            Height          =   2055
            Index           =   2
            Left            =   360
            TabIndex        =   45
            Top             =   3120
            Width           =   10215
            _ExtentX        =   18018
            _ExtentY        =   3625
            _Version        =   327681
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   5
            TabHeight       =   520
            TabCaption(0)   =   "Motivo Actuaci�n"
            TabPicture(0)   =   "PR0138.frx":007C
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(13)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "txtText1(18)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).ControlCount=   2
            TabCaption(1)   =   "Observaciones Actuaci�n"
            TabPicture(1)   =   "PR0138.frx":0098
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblLabel1(19)"
            Tab(1).Control(1)=   "txtText1(19)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).ControlCount=   2
            TabCaption(2)   =   "Indicaciones Actuaci�n"
            TabPicture(2)   =   "PR0138.frx":00B4
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLabel1(20)"
            Tab(2).Control(1)=   "txtText1(20)"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).ControlCount=   2
            TabCaption(3)   =   "Observaciones Petici�n"
            TabPicture(3)   =   "PR0138.frx":00D0
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "lblLabel1(21)"
            Tab(3).Control(1)=   "txtText1(22)"
            Tab(3).Control(1).Enabled=   0   'False
            Tab(3).ControlCount=   2
            TabCaption(4)   =   "Indicaciones Petici�n"
            TabPicture(4)   =   "PR0138.frx":00EC
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "lblLabel1(22)"
            Tab(4).Control(1)=   "txtText1(23)"
            Tab(4).Control(1).Enabled=   0   'False
            Tab(4).ControlCount=   2
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR09DESINDICACIO"
               Height          =   1290
               HelpContextID   =   30104
               Index           =   23
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   61
               TabStop         =   0   'False
               Tag             =   "Instrucciones de Realizaci�n"
               Top             =   600
               Width           =   9180
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR09DESOBSERVAC"
               Height          =   1290
               HelpContextID   =   30104
               Index           =   22
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   59
               TabStop         =   0   'False
               Tag             =   "Instrucciones de Realizaci�n"
               Top             =   600
               Width           =   9180
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR08DESINDICAC"
               Height          =   1290
               HelpContextID   =   30104
               Index           =   20
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   57
               TabStop         =   0   'False
               Tag             =   "Instrucciones de Realizaci�n"
               Top             =   600
               Width           =   9180
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR08DESOBSERV"
               Height          =   1170
               HelpContextID   =   30104
               Index           =   19
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   55
               TabStop         =   0   'False
               Tag             =   "Instrucciones de Realizaci�n"
               Top             =   600
               Width           =   9180
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR08DESMOTPET"
               Height          =   1170
               HelpContextID   =   30104
               Index           =   18
               Left            =   240
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   53
               TabStop         =   0   'False
               Tag             =   "Instrucciones de Realizaci�n"
               Top             =   600
               Width           =   9180
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   12
               Left            =   -73200
               TabIndex        =   49
               TabStop         =   0   'False
               Tag             =   "Primer Apellido del Peticionario"
               Top             =   1380
               Width           =   2535
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00808080&
               Height          =   330
               Index           =   8
               Left            =   -74040
               TabIndex        =   48
               TabStop         =   0   'False
               Tag             =   "Descripci�n Dpto. Peticionario"
               Top             =   660
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "SG02COD"
               Height          =   330
               HelpContextID   =   30104
               Index           =   7
               Left            =   -74640
               TabIndex        =   47
               TabStop         =   0   'False
               Tag             =   "C�d.Persona Peticionaria"
               Top             =   1380
               Width           =   852
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PETICIONARIO"
               Height          =   330
               HelpContextID   =   30101
               Index           =   6
               Left            =   -74640
               TabIndex        =   46
               TabStop         =   0   'False
               Tag             =   "C�d.Dpto. Peticionario"
               Top             =   660
               Width           =   372
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Indicaciones de la petici�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   -74760
               TabIndex        =   62
               Top             =   360
               Width           =   2310
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Observaciones de la petici�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   -74760
               TabIndex        =   60
               Top             =   360
               Width           =   2490
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Indicaciones de la actuaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   20
               Left            =   -74760
               TabIndex        =   58
               Top             =   360
               Width           =   2475
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Observaciones de la actuaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   -74760
               TabIndex        =   56
               Top             =   360
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Motivo de la petici�n de la actuaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   240
               TabIndex        =   54
               Top             =   360
               Width           =   3180
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Dr."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   -73200
               TabIndex        =   52
               Top             =   1140
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Doctor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   -74640
               TabIndex        =   51
               Top             =   1080
               Width           =   1230
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Departamento Peticionario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   -74640
               TabIndex        =   50
               Top             =   420
               Width           =   2265
            End
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Hora de Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   9000
            TabIndex        =   44
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Petici�n"
            Height          =   255
            Index           =   10
            Left            =   2640
            TabIndex        =   42
            Top             =   120
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   7320
            TabIndex        =   41
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Actuaci�n "
            Height          =   255
            Index           =   5
            Left            =   3600
            TabIndex        =   40
            Top             =   120
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Actuaci�n Pedida"
            Height          =   255
            Index           =   3
            Left            =   4680
            TabIndex        =   39
            Top             =   120
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Departamento Realizador"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   7800
            TabIndex        =   38
            Top             =   840
            Visible         =   0   'False
            Width           =   2160
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Instrucciones de Realizaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   360
            TabIndex        =   37
            Top             =   5280
            Width           =   2475
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   360
            TabIndex        =   25
            Top             =   240
            Width           =   930
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   24
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmpruebasadicionales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS  (REALIZACI�N DE ACTUACIONES)                      *
'* NOMBRE:                                                              *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 7 DE NOVIEMBRE DE 1997                                        *
'* DESCRIPCI�N: Petici�n de Actuaciones Adicionales                     *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdadicionales_Click()
  Dim vntdatos(4)
  Dim strsql1 As String
  Dim rstA1 As rdoResultset
  
    cmdadicionales.Enabled = False
    If txtText1(2).Text <> "" And txtText1(25).Text <> "" Then
          'gintpadre es una variable global para indicar qui�n llama a la pantalla
          'de hacer una petici�n (frmpedir)
          'gintpadre = 1
          'Load frmpedir
          'Call frmpedir.Show(vbModal)
          strsql1 = "SELECT * FROM PR0800 WHERE pr09numpeticion=" & txtText1(2).Text & _
                    " and pr03numactpedi=" & txtText1(1).Text
          Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
          glngProceso = rstA1("ad07codproceso").Value
          glngAsistencia = rstA1("ad01codasistenci").Value
          vntdatos(2) = txtText1(2).Text
          vntdatos(3) = txtText1(25).Text
          Call objsecurity.LaunchProcess("PR0152", vntdatos)
          'Unload frmpedir
          'Set frmpedir = Nothing
    Else
          MsgBox "No hay ninguna actuaci�n para la que pedir actuaciones adicionales"
    End If
    cmdadicionales.Enabled = True
End Sub

Private Sub cmdCancelarPrueba_Click()
    cmdCancelarPrueba.Enabled = False
    If txtText1(0).Text <> "" Then
        glngnumactplancan = txtText1(0).Text
        'Load frmcancelar
        'Call frmcancelar.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0132")
        'Unload frmcancelar
        'Set frmcancelar = Nothing
     Else
        MsgBox "No hay ninguna actuaci�n para cancelar"
     End If
    cmdCancelarPrueba.Enabled = True
End Sub

Private Sub cmdTerminarPrueba_Click()
    Dim strsql1 As String
    Dim strsql2 As String
    Dim strsql3 As String
    Dim strsql4 As String
    Dim strsql5 As String
    Dim strsql6 As String
    Dim rstA1 As rdoResultset
    Dim rstA2 As rdoResultset
    Dim rstA3 As rdoResultset
    Dim rstA4 As rdoResultset
    Dim rstA5 As rdoResultset
    Dim rstA6 As rdoResultset
    Dim strupdate As String
    Dim strinsert07 As String
    Dim strinsert As String
    Dim necesidad As Long
    Dim NumuniRec As Long
    Dim NumMinOcuRec As Long
    
    cmdTerminarPrueba.Enabled = False
    If txtText1(0).Text <> "" Then
     strupdate = "UPDATE PR0400 SET PR04FECFINACT=(SELECT SYSDATE FROM DUAL),PR37CODESTADO=4" & _
       " WHERE pr04numactplan=" & txtText1(0).Text
     objApp.rdoConnect.Execute strupdate, 64
    End If

    'insertar en pr0700 FASE_REALIZADA
    strsql1 = "SELECT * FROM PR0400 WHERE pr04numactplan=" & txtText1(0).Text
    Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
    
    strsql2 = "SELECT * FROM PR0600 WHERE pr03numactpedi=" & rstA1.rdoColumns("PR03NUMACTPEDI").Value
    Set rstA2 = objApp.rdoConnect.OpenResultset(strsql2)

    While Not rstA2.EOF
        strinsert07 = "INSERT INTO PR0700 " & _
                       "(pr04numactplan,pr07numfase,pr07desfase,pr07numminocupac," & _
                       "pr07numfase_pre,pr07numminfpre,pr07nummaxfpre," & _
                       "pr07indhabnatu,pr07indinifin) " & _
                       " VALUES(" & txtText1(0).Text & "," & rstA2.rdoColumns("pr06numfase").Value
       
        If IsNull(rstA2.rdoColumns("pr06desfase").Value) Then
            strinsert07 = strinsert07 & ",null"
        Else
            strinsert07 = strinsert07 & ",'" & rstA2.rdoColumns("pr06desfase").Value & "'"
        End If
        If IsNull(rstA2.rdoColumns("pr06numminocupac").Value) Then
            strinsert07 = strinsert07 & ",null"
        Else
            strinsert07 = strinsert07 & "," & rstA2.rdoColumns("pr06numminocupac").Value
        End If
        If IsNull(rstA2.rdoColumns("pr06numfase_pre").Value) Then
            strinsert07 = strinsert07 & ",null"
        Else
            strinsert07 = strinsert07 & "," & rstA2.rdoColumns("pr06numfase_pre").Value
        End If
        If IsNull(rstA2.rdoColumns("pr06numminfpre").Value) Then
            strinsert07 = strinsert07 & ",null"
        Else
            strinsert07 = strinsert07 & "," & rstA2.rdoColumns("pr06numminfpre").Value
        End If
        If IsNull(rstA2.rdoColumns("pr06nummaxfpre").Value) Then
            strinsert07 = strinsert07 & ",null"
        Else
            strinsert07 = strinsert07 & "," & rstA2.rdoColumns("pr06nummaxfpre").Value
        End If
        If IsNull(rstA2.rdoColumns("pr06indhabnatu").Value) Then
            strinsert07 = strinsert07 & ",null"
        Else
            strinsert07 = strinsert07 & "," & rstA2.rdoColumns("pr06indhabnatu").Value
        End If
        If IsNull(rstA2.rdoColumns("pr06indinifin").Value) Then
            strinsert07 = strinsert07 & ",null"
        Else
            strinsert07 = strinsert07 & "," & rstA2.rdoColumns("pr06indinifin").Value
        End If
        strinsert07 = strinsert07 & ")"
         'On Error GoTo Err_Ejecutar
         objApp.rdoConnect.Execute strinsert07, 64
         rstA2.MoveNext
    Wend
    
    strsql4 = "SELECT * FROM CI0100 WHERE PR04NUMACTPLAN=" & txtText1(0).Text
    Set rstA4 = objApp.rdoConnect.OpenResultset(strsql4)
    If rstA4.EOF = True Then
      strsql3 = "SELECT * FROM PR1400 WHERE pr03numactpedi=" & rstA1.rdoColumns("PR03NUMACTPEDI").Value
      Set rstA3 = objApp.rdoConnect.OpenResultset(strsql3)
      While Not rstA3.EOF
          strinsert = "INSERT INTO PR1000 " & _
                     "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
                     "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
                     " VALUES(" & txtText1(0).Text & "," & _
                     rstA3.rdoColumns("pr06numfase").Value & "," & _
                     rstA3.rdoColumns("pr14numnecesid").Value
      
          If IsNull(rstA3.rdoColumns("ag11codrecurso").Value) Then
              strinsert = strinsert & ",null"
          Else
              strinsert = strinsert & "," & rstA3.rdoColumns("ag11codrecurso").Value
          End If
          If IsNull(rstA3.rdoColumns("pr14numunirec").Value) Then
              strinsert = strinsert & ",null"
          Else
              strinsert = strinsert & "," & rstA3.rdoColumns("pr14numunirec").Value
          End If
          If IsNull(rstA3.rdoColumns("pr14numminocu").Value) Then
              strinsert = strinsert & ",null"
          Else
              strinsert = strinsert & "," & rstA3.rdoColumns("pr14numminocu").Value
          End If
          If IsNull(rstA3.rdoColumns("ag11codrecurso").Value) Then
              strinsert = strinsert & ",null"
          Else
              strinsert = strinsert & "," & rstA3.rdoColumns("ag11codrecurso").Value
          End If
          strinsert = strinsert & ")"
          objApp.rdoConnect.Execute strinsert, 64
          objApp.rdoConnect.Execute "Commit", 64
          rstA3.MoveNext
      Wend
    Else
      necesidad = 0
      NumuniRec = 0
      NumMinOcuRec = 0
      strsql3 = "SELECT * FROM PR1400 WHERE pr03numactpedi=" & rstA1.rdoColumns("PR03NUMACTPEDI").Value
      Set rstA3 = objApp.rdoConnect.OpenResultset(strsql3)
      While Not rstA3.EOF
        strsql6 = "SELECT * FROM PR1300 WHERE pr01codactuacion=" & txtText1(28).Text & _
                  " and pr05numfase=" & rstA3("pr06numfase").Value & _
                  " and pr13numnecesid=" & rstA3("pr14numnecesid").Value & _
                  " and ag14codtiprecu=" & rstA3("ag14codtiprecu").Value
        Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
        If rstA6("pr13indplanif").Value <> -1 Then
            strinsert = "INSERT INTO PR1000 " & _
                       "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
                       "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
                       " VALUES(" & txtText1(0).Text & "," & _
                       rstA3.rdoColumns("pr06numfase").Value & "," & _
                       rstA3.rdoColumns("pr14numnecesid").Value
        
            If IsNull(rstA3.rdoColumns("ag11codrecurso").Value) Then
                strinsert = strinsert & ",null"
            Else
                strinsert = strinsert & "," & rstA3.rdoColumns("ag11codrecurso").Value
            End If
            If IsNull(rstA3.rdoColumns("pr14numunirec").Value) Then
                strinsert = strinsert & ",null"
            Else
                strinsert = strinsert & "," & rstA3.rdoColumns("pr14numunirec").Value
            End If
            If IsNull(rstA3.rdoColumns("pr14numminocu").Value) Then
                strinsert = strinsert & ",null"
            Else
                strinsert = strinsert & "," & rstA3.rdoColumns("pr14numminocu").Value
            End If
            If IsNull(rstA3.rdoColumns("ag11codrecurso").Value) Then
                strinsert = strinsert & ",null"
            Else
                strinsert = strinsert & "," & rstA3.rdoColumns("ag11codrecurso").Value
            End If
            strinsert = strinsert & ")"
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
        Else
            If necesidad = 0 Then
                necesidad = rstA3.rdoColumns("pr14numnecesid").Value
                NumuniRec = rstA3.rdoColumns("pr14numunirec").Value
                NumMinOcuRec = rstA3.rdoColumns("pr14numminocu").Value
            Else
                necesidad = 0
            End If
        End If
        rstA3.MoveNext
        rstA6.Close
        Set rstA6 = Nothing
      Wend
      strsql5 = "SELECT * FROM CI2700 WHERE CI31NUMSOLICIT=" & rstA4("CI31NUMSOLICIT").Value & _
                " AND CI01NUMCITA=" & rstA4("CI01NUMCITA").Value
      Set rstA5 = objApp.rdoConnect.OpenResultset(strsql5)
      While rstA5.EOF = False
            strinsert = "INSERT INTO PR1000 " & _
                       "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
                       "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
                       " VALUES(" & txtText1(0).Text & "," & _
                       rstA5.rdoColumns("ci15numfasecita").Value
            If necesidad = 0 Then
                strinsert = strinsert & "," & rstA5.rdoColumns("ag11codrecurso").Value
            Else
                strinsert = strinsert & "," & necesidad
            End If
            If IsNull(rstA5.rdoColumns("ag11codrecurso").Value) Then
                strinsert = strinsert & ",null"
            Else
                strinsert = strinsert & "," & rstA5.rdoColumns("ag11codrecurso").Value
            End If
            strinsert = strinsert & "," & NumuniRec
            strinsert = strinsert & "," & NumMinOcuRec
            If IsNull(rstA5.rdoColumns("ag11codrecurso").Value) Then
                strinsert = strinsert & ",null"
            Else
                strinsert = strinsert & "," & rstA5.rdoColumns("ag11codrecurso").Value
            End If
            strinsert = strinsert & ")"
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
            rstA5.MoveNext
      Wend
      rstA5.Close
      Set rstA5 = Nothing
    End If
    
    
    rstA1.Close
    Set rstA1 = Nothing
    rstA2.Close
    Set rstA2 = Nothing
    rstA3.Close
    Set rstA3 = Nothing
    rstA4.Close
    Set rstA4 = Nothing
    'Load frmTerminarPrueba
    If txtText1(0).Text <> "" Then
          frmTerminarPrueba.txtactText1(0).Text = txtText1(0).Text
          frmTerminarPrueba.txtactText1(1).Text = txtText1(5).Text
          frmTerminarPrueba.txtactText1(3).Text = txtText1(16).Text
          frmTerminarPrueba.txtactText1(2).Text = txtText1(13).Text
          frmTerminarPrueba.txtactText1(5).Text = txtText1(14).Text
          frmTerminarPrueba.txtactText1(6).Text = txtText1(15).Text
          frmTerminarPrueba.txtdptoText1(0).Text = txtText1(26).Text
          frmTerminarPrueba.txtdptoText1(1).Text = txtText1(27).Text
          'Call frmTerminarPrueba.Show(vbModal)
          Call objsecurity.LaunchProcess("PR0174")
          cmdTerminarPrueba.Enabled = True
          'Unload frmTerminarPrueba
          'Set frmTerminarPrueba = Nothing
          Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
      
          'Unload frmpruebasadicionales
          'Set frmpruebasadicionales = Nothing
      Else
          MsgBox "No hay ninguna actuaci�n para terminar"
          cmdTerminarPrueba.Enabled = True
      End If
          
          
          
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub


Private Sub Form_Activate()
  objWinInfo.DataRefresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objDetailInfo As New clsCWForm
    Dim strKey As String
    
    'Call objApp.SplashOn
    
    Set objWinInfo = New clsCWWin
    
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                  cwWithAll)
    
    With objDetailInfo
    .strName = "Actuaciones Planificadas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0404J"
    .intAllowance = cwAllowReadOnly
    .strWhere = "pr04numactplan=" & glngnumactplan
    
    Call .FormAddOrderField("PR04NUMACTPLAN", cwAscending)
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable

  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    
  
    .CtrlGetInfo(txtText1(2)).blnInGrid = False
    .CtrlGetInfo(txtText1(28)).blnInGrid = False
    .CtrlGetInfo(txtText1(1)).blnInGrid = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(28)), "PR01CODACTUACION", "SELECT pr01codactuacion,pr01descorta FROM PR0100 WHERE pr01codactuacion = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(28)), txtText1(5), "PR01DESCORTA")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(26)), "AD02CODDPTO", "SELECT ad02coddpto,ad02desdpto FROM AD0200 WHERE ad02coddpto = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(26)), txtText1(27), "AD02DESDPTO")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(25)), "CI21CODPERSONA", _
         "SELECT CI21CODPERSONA,CI22NUMHISTORIA,CI22DNI," & _
         "CI22NOMBRE,CI22PRIAPEL,CI22SEGAPEL FROM CI2200 WHERE CI21CODPERSONA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(16), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(17), "CI22DNI")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(13), "CI22NOMBRE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(14), "CI22PRIAPEL")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(25)), txtText1(15), "CI22SEGAPEL")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(11)), "AD02CODDPTO", "SELECT ad02coddpto,ad02desdpto FROM AD0200 WHERE ad02coddpto = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(11)), txtText1(9), "AD02DESDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(10)), "SG02COD", _
         "SELECT SG02COD," & _
         "SG02NOM,SG02APE1,SG02APE2 FROM SG0200 WHERE SG02COD = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(8), "SG02NOM")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(21), "SG02APE1")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(22), "SG02APE2")
    

    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  glngProceso = 0
  glngAsistencia = 0
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Empleados" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

'Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
'End Sub

'Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
 Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' ----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


