VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frminterplani 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Interacci�n entre actuaciones "
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0161.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Interacciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6855
      Index           =   0
      Left            =   480
      TabIndex        =   0
      Top             =   720
      Width           =   10980
      Begin TabDlg.SSTab tabTab1 
         Height          =   6180
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   480
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   10901
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0161.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(9)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "fraFrame1(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "fraFrame1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtminuto"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txthora"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtdia"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).ControlCount=   14
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0161.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtdia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   480
            MaxLength       =   2
            TabIndex        =   4
            Top             =   4680
            Width           =   615
         End
         Begin VB.TextBox txthora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   1800
            MaxLength       =   2
            TabIndex        =   5
            Top             =   4680
            Width           =   615
         End
         Begin VB.TextBox txtminuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   3240
            MaxLength       =   2
            TabIndex        =   6
            Top             =   4680
            Width           =   615
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Actuaci�n Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1095
            Index           =   2
            Left            =   360
            TabIndex        =   18
            Top             =   1920
            Width           =   8655
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               HelpContextID   =   30104
               Index           =   6
               Left            =   2160
               Locked          =   -1  'True
               TabIndex        =   9
               TabStop         =   0   'False
               Tag             =   "Descripci�n Actuaci�n  Destino"
               Top             =   480
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "PR03NUMACTPEDI_DES"
               Height          =   330
               HelpContextID   =   30104
               Index           =   2
               Left            =   360
               TabIndex        =   2
               TabStop         =   0   'False
               Tag             =   "Actuaci�n Destino"
               Top             =   480
               Width           =   1092
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Actuaci�n Origen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1095
            Index           =   1
            Left            =   360
            TabIndex        =   17
            Top             =   480
            Width           =   8655
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               HelpContextID   =   30104
               Index           =   5
               Left            =   2160
               Locked          =   -1  'True
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Descripci�n Actuaci�n  Origen"
               Top             =   480
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H0000FFFF&
               DataField       =   "PR03NUMACTPEDI"
               Height          =   330
               HelpContextID   =   30101
               Index           =   0
               Left            =   360
               TabIndex        =   1
               TabStop         =   0   'False
               Tag             =   "Actuaci�n Origen"
               Top             =   480
               Width           =   1092
            End
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00808080&
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   1200
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Descripci�n del Tipo de Interacci�n"
            Top             =   3720
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR39NUMMININTER"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   7200
            TabIndex        =   7
            Tag             =   "Tiempo M�nimo de Interacci�n"
            Top             =   4920
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR19CODTIPINTERAC"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   480
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Tipo de Interacci�n"
            Top             =   3720
            Width           =   400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5865
            Index           =   0
            Left            =   -74880
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   90
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   10345
            _StockProps     =   79
            Caption         =   "INTERACCIONES"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   7920
            TabIndex        =   22
            Top             =   5040
            Visible         =   0   'False
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1200
            TabIndex        =   21
            Top             =   4800
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   2520
            TabIndex        =   20
            Top             =   4800
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   3960
            TabIndex        =   19
            Top             =   4800
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tiempo de Interacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   480
            TabIndex        =   15
            Top             =   4440
            Width           =   1920
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Tipo de Interacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   480
            TabIndex        =   14
            Top             =   3480
            Width           =   1680
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frminterplani"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS  (PETICI�N DE ACTUACIONES)                         *
'* NOMBRE:  PR00161.FRM                                                 *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Visualizaci�n de las interacciones de las actuaciones   *
'*              de la misma petici�n con la actuaci�n pedida actual     *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************



Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim TiempoTotal As Variant



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Interacciones"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR3900"
    .intAllowance = cwAllowModify
    'se sacan las interacciones de la actuaci�n de la pantalla anterior que es la
    'actuaci�n destino con aquellas de la misma petici�n que son las de origen
    .strWhere = "pr03numactpedi_des=" & frmdatosactpedida.txtText1(0) & _
                " and pr03numactpedi in " & _
                "(select pr03numactpedi from pr0300 " & _
                "where pr09numpeticion=" & frmpedir.txtText1(0) & ")"
    
    
    Call .FormAddOrderField("PR03NUMACTPEDI_DES", cwAscending)
    
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Interacciones")
    Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI_DES", "Actuaci�n Destino", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR19CODTIPINTERAC", "Tipo de Interacci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR39NUMMININTER", "Tiempo M�nimo de Interacci�n", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "PR03NUMACTPEDI_DES", "Actuaci�n Destino")
    Call .FormAddFilterOrder(strKey, "PR19CODTIPINTERAC", "Tipo de Interacci�n")
    Call .FormAddFilterOrder(strKey, "PR39NUMMININTER", "Tiempo M�nimo de Interacci�n")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    
    
    .CtrlGetInfo(txtText1(0)).blnReadOnly = True
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True
    .CtrlGetInfo(txtText1(2)).blnReadOnly = True
    .CtrlGetInfo(txtText1(6)).blnReadOnly = True
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
    
    .CtrlGetInfo(txtdia).blnNegotiated = False
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    .CtrlGetInfo(txtText1(5)).blnNegotiated = False
    .CtrlGetInfo(txtText1(6)).blnNegotiated = False
  
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "pr03numactpedi", "SELECT pr03numactpedi,pr03descorta FROM PR0300 WHERE pr03numactpedi = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(5), "pr03descorta")
  
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "pr03numactpedi_des", "SELECT pr03numactpedi,pr03descorta FROM PR0300 WHERE pr03numactpedi = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(6), "pr03descorta")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "pr19codtipinterac", "SELECT pr19codtipinterac,pr19destipinterac FROM PR1900 WHERE pr19codtipinterac= ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(7), "pr19destipinterac")

    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Dim rsta As rdoResultset
Dim strsql As String

On Error GoTo Err_Ejecutar
  
      strsql = "select pr19codtipinterac from PR3900 where pr03numactpedi=" & txtText1(0) & _
           " and pr03numactpedi_des=" & txtText1(2)
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      If (rsta.rdoColumns(0).Value <> 2) Then
         objWinInfo.CtrlGetInfo(txtText1(4)).blnReadOnly = True
      Else
         objWinInfo.CtrlGetInfo(txtText1(4)).blnReadOnly = False
      End If

rsta.Close
Set rsta = Nothing
Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Empleados" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
'*********************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(2).Text <> "" And _
   txtText1(4).Text <> "" And txtText1(1) <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR3900 where" & _
         " pr03numactpedi=" & txtText1(0).Text & _
         " AND pr03numactpedi_des=" & txtText1(2).Text & _
         " AND pr19codtipinterac=" & txtText1(1).Text & _
         " AND pr39nummininter=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'***********************************************************************
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
 'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If btnButton.Index = 16 Then
    Call txtText1_Change(4)
  End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
'*********************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String

If txtText1(0).Text <> "" And txtText1(2).Text <> "" And _
   txtText1(4).Text <> "" And txtText1(1) <> "" Then
  Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
  strTiempo = "SELECT count(*) FROM PR3900 where" & _
         " pr03numactpedi=" & txtText1(0).Text & _
         " AND pr03numactpedi_des=" & txtText1(2).Text & _
         " AND pr19codtipinterac=" & txtText1(1).Text & _
         " AND pr39nummininter=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(4), TiempoTotal)
  Else
    objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'***********************************************************************
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
   'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If intIndex = 10 Then
    Call txtText1_Change(4)
  End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lbllabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)

  Dim rstactuacion As rdoResultset
  Dim stractuacion As String
  
  If intIndex = 0 Then
     If txtText1(0).Text <> "" Then
        stractuacion = "select pr0100.pr01descorta from PR0300,PR0100 where PR0300.PR01CODACTUACION=PR0100.PR01CODACTUACION and PR0300.PR03NUMACTPEDI=" & txtText1(0).Text
        Set rstactuacion = objApp.rdoConnect.OpenResultset(stractuacion)
        txtText1(5).Text = rstactuacion.rdoColumns(0).Value
        txtText1(5).Locked = True
        txtText1(5).BackColor = &HC0C0C0
        rstactuacion.Close
        Set rstactuacion = Nothing
      End If
  End If
  If intIndex = 2 Then
     If txtText1(2).Text <> "" Then
        stractuacion = "select pr0100.pr01descorta from PR0300,PR0100 where PR0300.PR01CODACTUACION=PR0100.PR01CODACTUACION and PR0300.PR03NUMACTPEDI=" & txtText1(2).Text
        Set rstactuacion = objApp.rdoConnect.OpenResultset(stractuacion)
        txtText1(6).Text = rstactuacion.rdoColumns(0).Value
        txtText1(6).Locked = True
        txtText1(6).BackColor = &HC0C0C0
        rstactuacion.Close
        Set rstactuacion = Nothing
      End If
  End If

  Call objWinInfo.CtrlDataChange
  If intIndex = 4 Then
    If txtText1(4).Text = "" Then
      txtdia.Text = 0
      txthora.Text = 0
      txtminuto.Text = 0
    Else
      txtdia.Text = txtText1(4).Text \ 1440                 'd�as
      txthora.Text = (txtText1(4).Text Mod 1440) \ 60       'horas
      txtminuto.Text = (txtText1(4).Text Mod 1440) Mod 60   'minutos
    End If
  End If
End Sub


Private Sub txtdia_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia.Text) = False Then
    Beep
    txtdia.Text = ""
    txtdia.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 65 Then
      txtdia.Text = 65
     End If
  End If
End Sub

Private Sub txthora_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora.Text) = False Then
    Beep
    txthora.Text = ""
    txthora.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txtminuto_Change()
  objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto.Text) = False Then
    Beep
    txtminuto.Text = ""
    txtminuto.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub


Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 65 Then
    d = 65
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If

TiempoTotal = (d * 1440) + (h * 60) + m
  
End Sub

