VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmSeleccionarAct 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Selecci�n de Actuaciones."
   ClientHeight    =   8280
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11655
   ControlBox      =   0   'False
   Icon            =   "PR0153.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8280
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdNoReal 
      Caption         =   "Establecer Motivo"
      Height          =   375
      Left            =   6960
      TabIndex        =   32
      Top             =   7440
      Width           =   1935
   End
   Begin VB.CommandButton cmdCambPlanific 
      Caption         =   "Cambiar Planificaci�n"
      Height          =   375
      Left            =   3720
      TabIndex        =   31
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H80000014&
      Height          =   1695
      Left            =   3840
      TabIndex        =   27
      Top             =   3480
      Visible         =   0   'False
      Width           =   4215
      Begin VB.PictureBox Picture1 
         BackColor       =   &H80000014&
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   720
         Picture         =   "PR0153.frx":000C
         ScaleHeight     =   615
         ScaleWidth      =   495
         TabIndex        =   28
         Top             =   720
         Width           =   495
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Se est� generando la Petici�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   360
         TabIndex        =   30
         Top             =   240
         Width           =   3495
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Espere un momento por favor..."
         Height          =   375
         Left            =   1320
         TabIndex        =   29
         Top             =   840
         Width           =   2415
      End
   End
   Begin VB.Frame fraFrame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Actuaciones Seleccionadas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   4800
      Index           =   0
      Left            =   120
      TabIndex        =   15
      Top             =   2520
      Width           =   9015
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4305
         Index           =   0
         Left            =   120
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   360
         Width           =   8655
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   11
         stylesets.count =   1
         stylesets(0).Name=   "seleccionar"
         stylesets(0).Picture=   "PR0153.frx":0316
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         StyleSet        =   "seleccionar"
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   11
         Columns(0).Width=   1058
         Columns(0).Caption=   "Selec"
         Columns(0).Name =   "Selec"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   1058
         Columns(1).Caption=   "Cond?"
         Columns(1).Name =   "Cond?"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   11
         Columns(1).FieldLen=   256
         Columns(1).Style=   2
         Columns(2).Width=   1720
         Columns(2).Caption=   "C�d.Act"
         Columns(2).Name =   "C�d.Act"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   4577
         Columns(3).Caption=   "Descripci�n Actuaci�n"
         Columns(3).Name =   "Descripci�n Actuaci�n"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1667
         Columns(4).Caption=   "C�d.Dpto"
         Columns(4).Name =   "C�d.Dpto"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   4577
         Columns(5).Caption=   "Descripci�n Dpto"
         Columns(5).Name =   "Descripci�n Dpto"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         Columns(6).Width=   3200
         Columns(6).Visible=   0   'False
         Columns(6).Caption=   "N�m Act"
         Columns(6).Name =   "N�m Act"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   8
         Columns(6).FieldLen=   256
         Columns(7).Width=   3200
         Columns(7).Visible=   0   'False
         Columns(7).Caption=   "NumAct0800"
         Columns(7).Name =   "NumAct0800"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   8
         Columns(7).FieldLen=   256
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "NumSec0800"
         Columns(8).Name =   "NumSec0800"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         Columns(9).Width=   3200
         Columns(9).Visible=   0   'False
         Columns(9).Caption=   "Asociada"
         Columns(9).Name =   "Asociada"
         Columns(9).DataField=   "Column 9"
         Columns(9).DataType=   8
         Columns(9).FieldLen=   256
         Columns(10).Width=   1058
         Columns(10).Caption=   "Real?"
         Columns(10).Name=   "Real?"
         Columns(10).DataField=   "Column 10"
         Columns(10).DataType=   11
         Columns(10).FieldLen=   256
         Columns(10).Style=   2
         _ExtentX        =   15266
         _ExtentY        =   7594
         _StockProps     =   79
         Caption         =   "ACTUACIONES SELECCIONADAS"
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Height          =   975
      Index           =   3
      Left            =   9480
      TabIndex        =   26
      Top             =   6360
      Width           =   2055
      Begin VB.CommandButton cmdcondiciones 
         Caption         =   "Condiciones"
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame fraFrame1 
      BackColor       =   &H00C0C0C0&
      Height          =   3495
      Index           =   2
      Left            =   9480
      TabIndex        =   25
      Top             =   2760
      Width           =   2055
      Begin VB.CommandButton cmdActuaciones 
         Caption         =   "Actuaciones"
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   2760
         Width           =   1815
      End
      Begin VB.CommandButton cmdprincipales 
         Caption         =   "Principales"
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   1815
      End
      Begin VB.CommandButton cmdpaquetes 
         Caption         =   "Paquetes"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   840
         Width           =   1815
      End
      Begin VB.CommandButton cmdprotocolos 
         Caption         =   "Protocolos"
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   1320
         Width           =   1815
      End
      Begin VB.CommandButton cmddepartamentos 
         Caption         =   "Departamentos"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   1800
         Width           =   1815
      End
      Begin VB.CommandButton cmdgrupo 
         Caption         =   "Grupos"
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   2280
         Width           =   1815
      End
   End
   Begin VB.CommandButton cmdpeticion 
      Caption         =   "Planificar"
      Height          =   375
      Left            =   1320
      TabIndex        =   0
      Top             =   7440
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Datos del Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Index           =   1
      Left            =   120
      TabIndex        =   18
      Top             =   480
      Width           =   10980
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "C�digo del Paciente"
         Top             =   720
         Width           =   852
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   3
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Tag             =   "Nombre del Paciente"
         Top             =   1440
         Width           =   3200
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   4
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Tag             =   "Primer Apellido del Paciente"
         Top             =   1440
         Width           =   3200
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   5
         Left            =   7080
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "Segundo Apellido del Paciente"
         Top             =   1440
         Width           =   3200
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "N�mero de Historia"
         Top             =   720
         Width           =   852
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   2
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "D.N.I."
         Top             =   720
         Width           =   2000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�digo Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   24
         Top             =   480
         Width           =   1695
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   2040
         TabIndex        =   23
         Top             =   480
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         Caption         =   "D.N.I"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   3720
         TabIndex        =   22
         Top             =   480
         Width           =   615
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   360
         TabIndex        =   21
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 1�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   3720
         TabIndex        =   20
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 2�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   7080
         TabIndex        =   19
         Top             =   1200
         Width           =   1575
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   7995
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSeleccionarAct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim numero_pedida As Long  'cmdcondiciones
Attribute numero_pedida.VB_VarHelpID = -1
Dim numero_secuencia As Long 'cmdcondiciones
Dim pedida As Boolean   'cmdcondiciones
Dim gblnactivar As Boolean
Dim FechaVigencia As Date
Dim gblnPedReal As Integer
Dim glngPedir As Long

Private Sub Borrar_Restricciones_PR4700()
Dim stract As String
Dim rstact As rdoResultset
Dim strtiporec As String
Dim rsttiporec As rdoResultset
Dim strrest As String
Dim rstrest As rdoResultset
Dim strdelete As String

If grdDBGrid1(0).Columns(6).Value <> "" Then
'este procedimiento borra de PR4700 aquellas restricciones que pertenezcan
'a los tipos de recurso de la actuaci�n a eliminar
stract = "SELECT count(*) FROM PR0300 WHERE pr01codactuacion=" & _
         grdDBGrid1(0).Columns(2).Value
Set rstact = objApp.rdoConnect.OpenResultset(stract)
If rstact.rdoColumns(0).Value > 0 Then
  stract = "SELECT pr03numactpedi FROM PR0300 WHERE pr01codactuacion=" & _
         grdDBGrid1(0).Columns(2).Value & " AND pr09numpeticion=" & frmpedir.txtText1(0).Text
  Set rstact = objApp.rdoConnect.OpenResultset(stract)
  strtiporec = "SELECT AG14CODTIPRECU FROM PR1400 WHERE pr03numactpedi=" & _
               rstact.rdoColumns("pr03numactpedi").Value
  Set rsttiporec = objApp.rdoConnect.OpenResultset(strtiporec)
  While Not rsttiporec.EOF
    strrest = "SELECT AG16CODTIPREST FROM AG1500 WHERE ag14codtiprecu=" & _
              rsttiporec.rdoColumns("ag14codtiprecu").Value
    Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
    While Not rstrest.EOF
      strdelete = "DELETE PR4700 WHERE " & _
                  "PR09NUMPETICION=" & frmpedir.txtText1(0).Text & " AND " & _
                  "AG16CODTIPREST=" & rstrest.rdoColumns(0).Value
      objApp.rdoConnect.Execute strdelete, 64
    rstrest.MoveNext
    Wend
    rstrest.Close
    Set rstrest = Nothing
  rsttiporec.MoveNext
  Wend
  'se crean otra vez los registros en PR4700 excepto para la actuaci�n pedida
  'que se pasa como par�metro. Esto se hace porque como en PR4700 no se repiten
  'las restricciones aunque el tipo de recurso se repita pues puede usarse en
  'varias actuaciones de una petici�n podr�a borrarse una actuaci�n que tenga
  'una restricci�n y se borrar�a de la PR4700 pero puede haber otra actuaci�n
  'con esa misma restricci�n y al llamar a esta funci�n(crear_restricciones_sin_actuacion)
  'se volver�a a crear el registro en PR4700
  Call crear_restricciones_sin_actuacion(rstact.rdoColumns("pr03numactpedi").Value)
  rsttiporec.Close
  Set rsttiporec = Nothing
  End If
          
 rstact.Close
 Set rstact = Nothing
End If

End Sub

Private Sub esta_pedida(numero_pedida As Long, pedida As Boolean)
    Dim rstA As rdoResultset
    Dim sqlstr As String
    
    sqlstr = "SELECT PR03NUMACTPEDI FROM PR0300 WHERE AD02CODDPTO=" & _
                grdDBGrid1(0).Columns(4).Value & " AND PR01CODACTUACION=" & _
                grdDBGrid1(0).Columns(2).Value & " AND CI21CODPERSONA=" & _
                frmpedir.IdPersona1.Text
    Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
    If rstA.EOF = False Then
        pedida = True
        numero_pedida = rstA.rdoColumns(0).Value
    Else
        pedida = False
    End If
    rstA.Close
    Set rstA = Nothing
End Sub
Private Sub crear_actuacion_pedida(numero_pedida As Long)
    Dim sqlstr As String
    Dim rstasel As rdoResultset
    Dim sqlstrsel As String
    
    sqlstrsel = "SELECT PR03NUMACTPEDI_SEQUENCE.nextval FROM dual"
    Set rstasel = objApp.rdoConnect.OpenResultset(sqlstrsel)
    numero_pedida = rstasel.rdoColumns(0).Value
                
    sqlstr = "INSERT INTO PR0300 (PR03NUMACTPEDI,AD02CODDPTO,CI21CODPERSONA," & _
                "CI32CODTIPECON,CI13CODENTIDAD,PR09NUMPETICION,PR01CODACTUACION," & _
                "PR03NUMACPEDI_PRI) VALUES (" & numero_pedida & "," & grdDBGrid1(0).Columns(4).Value & _
                "," & frmpedir.IdPersona1.Text & "," & "Null" & "," & "Null" & "," & frmpedir.txtText1(0) & "," & _
                grdDBGrid1(0).Columns(2).Value & "," & _
                grdDBGrid1(0).Columns(9).Value & ")"
    objApp.rdoConnect.Execute sqlstr, 64
    grdDBGrid1(0).Columns(6).Value = numero_pedida
    rstasel.Close
    Set rstasel = Nothing
End Sub
Private Sub crear_relacion(ByVal numero_pedida As Long, numero_secuencia As Long)
    Dim sqlstr As String
    Dim rstasel As rdoResultset
    Dim sqlstrsel As String
    
    sqlstrsel = "SELECT COUNT (*) FROM PR0800 WHERE PR09NUMPETICION=" & _
                frmpedir.txtText1(0).Text & _
                " AND PR03NUMACTPEDI=" & numero_pedida
                '" AND PR03NUMACTPEDI IN (SELECT PR03NUMACTPEDI FROM PR0300 WHERE PR01CODACTUACION=" & _
                 grdDBGrid1(0).Columns(6).Value & ")"
    Set rstasel = objApp.rdoConnect.OpenResultset(sqlstrsel)
    numero_secuencia = rstasel.rdoColumns(0) + 1
    If glngProceso <> 0 Then
      sqlstr = "INSERT INTO PR0800 (PR09NUMPETICION,PR03NUMACTPEDI,PR08NUMSECUENCIA,AD07CODPROCESO,AD01CODASISTENCI) VALUES (" & _
                frmpedir.txtText1(0).Text & "," & numero_pedida & "," & numero_secuencia & "," & glngProceso & "," & glngAsistencia & ")"
    Else
      sqlstr = "INSERT INTO PR0800 (PR09NUMPETICION,PR03NUMACTPEDI,PR08NUMSECUENCIA) VALUES (" & frmpedir.txtText1(0).Text & "," & numero_pedida & "," & numero_secuencia & ")"
    End If
    objApp.rdoConnect.Execute sqlstr, 64
    grdDBGrid1(0).Columns(7).Value = numero_pedida
    grdDBGrid1(0).Columns(8).Value = numero_secuencia
    rstasel.Close
    Set rstasel = Nothing
End Sub
Private Sub crear_condiciones()
    Dim sqlstr As String
    Dim rstasel As rdoResultset
    Dim sqlstrsel As String
    Dim mucodcondicion As Long
    Dim sqlstrselcondic As String
    Dim rstAselcondic As rdoResultset
    
    sqlstrselcondic = "SELECT * FROM pr3800 " _
      & "WHERE PR03NUMACTPEDI = " & numero_pedida
    Set rstAselcondic = objApp.rdoConnect.OpenResultset(sqlstrselcondic)
    
    If rstAselcondic.EOF Then
        sqlstrsel = "SELECT * FROM pr2300 " _
          & "WHERE PR01CODACTUACION = " & grdDBGrid1(0).Columns(2).Value
        Set rstasel = objApp.rdoConnect.OpenResultset(sqlstrsel)
        
        
        With rstasel
            Do Until rstasel.EOF
                mucodcondicion = .rdoColumns("PR23CODCOND")
                sqlstr = "INSERT INTO PR3800 (PR03NUMACTPEDI,PR01CODACTUACION,PR23CODCOND,PR38INDRESPUESTA) VALUES " _
                      & "(" & numero_pedida & "," & grdDBGrid1(0).Columns(2).Value & _
                        "," & mucodcondicion & ",0)"
                objApp.rdoConnect.Execute sqlstr, 64
                rstasel.MoveNext
            Loop
        End With
        rstasel.Close
        Set rstasel = Nothing
    End If
    rstAselcondic.Close
    Set rstAselcondic = Nothing
End Sub
Private Sub borrar_relacion()
    Dim sqlstr As String
    Dim rstasel As rdoResultset
    Dim sqlstrsel As String
    
    
    If grdDBGrid1(0).Columns(7).Value <> "" And grdDBGrid1(0).Columns(8).Value <> "" Then
        sqlstrsel = "SELECT count(*) FROM pr0800 " _
          & "WHERE pr09numpeticion=" & frmpedir.txtText1(0).Text & _
            " and pr03numactpedi=" & grdDBGrid1(0).Columns(7).Value
            
        Set rstasel = objApp.rdoConnect.OpenResultset(sqlstrsel)
        sqlstr = "delete from PR0800 where " & _
                 "pr09numpeticion=" & frmpedir.txtText1(0).Text & _
                 " and pr03numactpedi=" & grdDBGrid1(0).Columns(7).Value & _
                 " and pr08numsecuencia=" & rstasel.rdoColumns(0).Value
        objApp.rdoConnect.Execute sqlstr
        grdDBGrid1(0).Columns(7).Value = ""
        grdDBGrid1(0).Columns(8).Value = ""
        rstasel.Close
        Set rstasel = Nothing
    End If
End Sub
Private Sub borrar_peticion()
On Error GoTo Borrar_Peticion_Error
    Dim sqlstr As String
    
    sqlstr = "delete from PR0300 where " & _
             "pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
    objApp.rdoConnect.Execute sqlstr
    grdDBGrid1(0).Columns(6).Value = ""
    
Borrar_Peticion_Error:
  Exit Sub
End Sub
Private Sub comprobar_vigencia(vigencia As Boolean)
    Dim rstA As rdoResultset
    Dim sqlstr As String
    Dim rstA2 As rdoResultset
    Dim sqlstr2 As String
    Dim rstA3 As rdoResultset
    Dim sqlstr3 As String
    Dim rstAreal As rdoResultset
    Dim sqlstrreal As String
    Dim mlngvalresu As Long
    
    vigencia = False
    
                
    sqlstrreal = "SELECT * FROM PR0100 WHERE  PR01CODACTUACION=" & _
                grdDBGrid1(0).Columns(2).Value
    Set rstAreal = objApp.rdoConnect.OpenResultset(sqlstrreal)
    
    sqlstr = "SELECT * FROM PR0400 WHERE AD02CODDPTO=" & _
                grdDBGrid1(0).Columns(4).Value & " AND PR01CODACTUACION=" & _
                grdDBGrid1(0).Columns(2).Value & " AND CI21CODPERSONA=" & _
                txtText1(0).Text & " ORDER BY PR04NUMACTPLAN DESC"
    Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
    
    With rstA
        Do Until rstA.EOF
            vigencia = False
            If IsNull(rstAreal.rdoColumns("PR01NUMVALRESU").Value) = False Then
              If rstAreal.rdoColumns("PR01NUMVALRESU").Value > 0 Then
                  If (.rdoColumns("PR37CODESTADO").Value = 1) Or (.rdoColumns("PR37CODESTADO").Value = 2) Then
                      vigencia = True
                      sqlstr2 = "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI=" & rstA("PR03NUMACTPEDI").Value
                      Set rstA2 = objApp.rdoConnect.OpenResultset(sqlstr2)
                      If .rdoColumns("PR37CODESTADO").Value = 1 Then
                          gblnPedReal = 1
                          If IsNull(.rdoColumns("PR04FECPLANIFIC").Value) = False Then
                              FechaVigencia = .rdoColumns("PR04FECPLANIFIC").Value
                              If DateDiff("d", Date, FechaVigencia) < 0 Then
                                vigencia = False
                              End If
                          Else
                            vigencia = False
                          End If
                      Else
                          gblnPedReal = 2
                          sqlstr3 = "SELECT * FROM CI0100 WHERE PR04NUMACTPLAN=" & rstA("PR04NUMACTPLAN").Value
                          Set rstA3 = objApp.rdoConnect.OpenResultset(sqlstr3)
                          If IsNull(rstA3.rdoColumns("CI01FECCONCERT").Value) = False Then
                              FechaVigencia = rstA3.rdoColumns("CI01FECCONCERT").Value
                              If DateDiff("d", Date, FechaVigencia) < 0 Then
                                vigencia = False
                              End If
                          End If
                          rstA3.Close
                          Set rstA3 = Nothing
                      End If
                      glngPedir = rstA2("PR09NUMPETICION").Value
                      rstA2.Close
                      Set rstA2 = Nothing
                      If vigencia = True Then
                        Exit Do
                      End If
                  Else
                      If .rdoColumns("PR37CODESTADO").Value <> 6 Then
                          If IsNull(rstAreal.rdoColumns("PR01NUMVALRESU").Value) = False Then
                            mlngvalresu = rstAreal.rdoColumns("PR01NUMVALRESU").Value
                            If DateDiff("d", rstA.rdoColumns("PR04FECFINACT").Value, Date) < mlngvalresu Or rstAreal.rdoColumns("PR01NUMVALRESU").Value = 0 Then
                                vigencia = True
                                gblnPedReal = 3
                                sqlstr2 = "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI=" & rstA("PR03NUMACTPEDI").Value
                                Set rstA2 = objApp.rdoConnect.OpenResultset(sqlstr2)
                                If IsNull(.rdoColumns("PR04FECFINACT").Value) = False Then
                                  FechaVigencia = .rdoColumns("PR04FECFINACT").Value
                                End If
                                glngPedir = rstA2("PR09NUMPETICION").Value
                                rstA2.Close
                                Set rstA2 = Nothing
                                Exit Do
                            End If
                          'Else
                          '  vigencia = True
                          '  Exit Do
                          End If
                      End If
                  End If
                End If
              End If
        rstA.MoveNext
        Loop
   End With
  rstAreal.Close
  Set rstAreal = Nothing
  rstA.Close
 Set rstA = Nothing
End Sub
Private Sub Crear_Recursos_Pedidos(ByVal mlngnumfase As Long)

    Dim rstAselrecursos As rdoResultset
    Dim sqlstrselrecursos As String
    Dim sqlstrinrecursos As String
    Dim mlngnumneces As Long
    Dim mlngnumuni As Long
    Dim mlngcodtiprec As Long
    Dim mblnindpref As Boolean
    Dim mlngcoddpto As Long
    
    sqlstrselrecursos = "SELECT * FROM PR1300 WHERE PR01CODACTUACION=" & _
                      grdDBGrid1(0).Columns(2).Value & " AND PR05NUMFASE=" & _
                      mlngnumfase
    Set rstAselrecursos = objApp.rdoConnect.OpenResultset(sqlstrselrecursos)
    
    Do Until rstAselrecursos.EOF
         If rstAselrecursos("pr13indplanif").Value <> 0 Then
            sqlstrinrecursos = "UPDATE PR0300 SET PR03indcitable=" & rstAselrecursos("pr13indplanif").Value _
              & ",PR03INDCITAANT=" & rstAselrecursos("pr13indplanif").Value & " WHERE pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
            objApp.rdoConnect.Execute sqlstrinrecursos, 64
         End If

        sqlstrinrecursos = "INSERT INTO PR1400 (PR03NUMACTPEDI,PR06NUMFASE,PR14NUMNECESID,AG14CODTIPRECU," & _
                            "AD02CODDPTO,PR14NUMUNIREC,PR14NUMMINOCU,PR14NUMMINDESREC,PR14INDRECPREFE) VALUES (" & grdDBGrid1(0).Columns(6).Value & "," & mlngnumfase
        
        If IsNull(rstAselrecursos.rdoColumns("PR13NUMNECESID").Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns("PR13NUMNECESID").Value
        End If
        If IsNull(rstAselrecursos.rdoColumns("AG14CODTIPRECU").Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns("AG14CODTIPRECU").Value
        End If
        If IsNull(rstAselrecursos.rdoColumns("AD02CODDPTO").Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns("AD02CODDPTO").Value
        End If
        If IsNull(rstAselrecursos.rdoColumns("PR13NUMUNIREC").Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns("PR13NUMUNIREC").Value
        End If
        If IsNull(rstAselrecursos.rdoColumns("PR13NUMTIEMPREC").Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns("PR13NUMTIEMPREC").Value
        End If
        If IsNull(rstAselrecursos.rdoColumns("PR13NUMMINDESF").Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns("PR13NUMMINDESF").Value
        End If
        If IsNull(rstAselrecursos.rdoColumns("PR13INDPREFEREN").Value) Then
            sqlstrinrecursos = sqlstrinrecursos & ",null"
        Else
            sqlstrinrecursos = sqlstrinrecursos & "," & rstAselrecursos.rdoColumns("PR13INDPREFEREN").Value
        End If
        sqlstrinrecursos = sqlstrinrecursos & ")"
        objApp.rdoConnect.Execute sqlstrinrecursos, 64
        rstAselrecursos.MoveNext
    Loop
    rstAselrecursos.Close
    Set rstAselrecursos = Nothing
End Sub
Private Sub Borrar_Recursos_Pedidos()
    Dim sqlborrar As String
  If grdDBGrid1(0).Columns(6).Value <> "" Then
    sqlborrar = "DELETE FROM PR1400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
    objApp.rdoConnect.Execute sqlborrar, 64
  End If
End Sub
Private Sub Crear_Fases_Pedidas()
    
    Dim rstaselfases As rdoResultset
    Dim sqlstrselfases As String
    Dim sqlstrinfases As String
    Dim mlngnumfase As Long
    Dim mstrdesfase As String
    Dim mlngnumocupaci As Long
    Dim mlngnumfasepre As Long
    Dim mlngnumminpre As Long
    Dim mlngnummaxpre As Long
    Dim mblnindhabilnat As Boolean
    Dim mblnindinicfin As Boolean
    
    sqlstrselfases = "SELECT * FROM PR0500 WHERE PR01CODACTUACION=" & _
                      grdDBGrid1(0).Columns(2).Value _
                    & " AND PR05NUMFASE IN  (SELECT PR05NUMFASE FROM PR1300 WHERE PR01CODACTUACION= " & grdDBGrid1(0).Columns(2).Value & ")"
    Set rstaselfases = objApp.rdoConnect.OpenResultset(sqlstrselfases)
    
    Do Until rstaselfases.EOF
        sqlstrinfases = "INSERT INTO PR0600 (PR03NUMACTPEDI,PR06NUMFASE,PR06DESFASE,PR06NUMMINOCUPAC," & _
                        "PR06NUMFASE_PRE,PR06NUMMINFPRE,PR06NUMMAXFPRE,PR06INDHABNATU,PR06INDINIFIN) VALUES " _
              & "(" & grdDBGrid1(0).Columns(6).Value
              
        If IsNull(rstaselfases.rdoColumns("PR05NUMFASE").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns("PR05NUMFASE").Value
        End If
        If IsNull(rstaselfases.rdoColumns("PR05DESFASE").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & ",'" & rstaselfases.rdoColumns("PR05DESFASE").Value & "'"
        End If
        If IsNull(rstaselfases.rdoColumns("PR05NUMOCUPACI").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns("PR05NUMOCUPACI").Value
        End If
        If IsNull(rstaselfases.rdoColumns("PR05NUMFASE_PRE").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns("PR05NUMFASE_PRE").Value
        End If
        If IsNull(rstaselfases.rdoColumns("PR05NUMTMINFPRE").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns("PR05NUMTMINFPRE").Value
        End If
        If IsNull(rstaselfases.rdoColumns("PR05NUMTMAXFPRE").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns("PR05NUMTMAXFPRE").Value
        End If
        If IsNull(rstaselfases.rdoColumns("PR05INDHABILNATU").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns("PR05INDHABILNATU").Value
        End If
        If IsNull(rstaselfases.rdoColumns("PR05INDINICIOFIN").Value) Then
            sqlstrinfases = sqlstrinfases & ",null"
        Else
            sqlstrinfases = sqlstrinfases & "," & rstaselfases.rdoColumns("PR05INDINICIOFIN").Value
        End If
        sqlstrinfases = sqlstrinfases & ")"
        
        objApp.rdoConnect.Execute sqlstrinfases, 64
        Call Crear_Recursos_Pedidos(rstaselfases.rdoColumns("PR05NUMFASE").Value)
        rstaselfases.MoveNext
    Loop
    rstaselfases.Close
    Set rstaselfases = Nothing
End Sub
'**************************************************************************jcr7/8/98
'Esta estructura se crea al realizar
'************************************************************************************
'Private Sub Borrar_Recurso_Consumido()
'Dim stract As String
'Dim rstact As rdoResultset
'Dim strdelete As String
'
'stract = "SELECT pr04numactplan FROM pr0400 where pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
'Set rstact = objApp.rdoConnect.OpenResultset(stract)
'While Not rstact.EOF
'    strdelete = "DELETE FROM PR1000 WHERE PR04NUMACTPLAN=" & rstact.rdoColumns(0).Value
'    objApp.rdoConnect.Execute strdelete, 64
'    rstact.MoveNext
'Wend
'rstact.Close
'Set rstact = Nothing
'End Sub

'Private Sub Borrar_Fase_Realizada()
'Dim stract As String
'Dim rstact As rdoResultset
'Dim strdelete As String

'stract = "SELECT pr04numactplan FROM pr0400 where pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
'Set rstact = objApp.rdoConnect.OpenResultset(stract)
'While Not rstact.EOF
'    strdelete = "DELETE FROM PR0700 WHERE PR04NUMACTPLAN=" & rstact.rdoColumns(0).Value
'    objApp.rdoConnect.Execute strdelete, 64
'    rstact.MoveNext
'Wend
'rstact.Close
'Set rstact = Nothing
'End Sub
Private Sub Borrar_Fases_Pedidas()
    Dim sqlborrar As String
  If grdDBGrid1(0).Columns(6).Value <> "" Then
    sqlborrar = "DELETE FROM PR0600 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
    objApp.rdoConnect.Execute sqlborrar, 64
  End If
End Sub
Private Sub Borrar_Planificadas()
    Dim sqlborrar As String
    
    sqlborrar = "DELETE FROM PR0400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
    objApp.rdoConnect.Execute sqlborrar, 64
End Sub
Private Sub Crear_Cuestionario_Peticion()

    Dim rstAselcuest As rdoResultset
    Dim rstAselrepet As rdoResultset
    Dim sqlstrselcuest As String
    Dim sqlstrincuest As String
    Dim sqlstrselrepet As String
    Dim mlngcodpreg As Long
    Dim mlngcodtipres As Long
    Dim mblnindoblig As Boolean
    Dim mlngcodlistresp As Long
    
    sqlstrselcuest = "SELECT * FROM PR2900 WHERE PR01CODACTUACION=" & _
                      grdDBGrid1(0).Columns(2).Value

    Set rstAselcuest = objApp.rdoConnect.OpenResultset(sqlstrselcuest)
    
    Do Until rstAselcuest.EOF
        
        sqlstrselrepet = "SELECT * FROM PR4100 WHERE PR40CODPREGUNTA=" & _
                          rstAselcuest.rdoColumns("PR40CODPREGUNTA").Value & " AND PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value 'IN (SELECT PR03NUMACTPEDI FROM PR0300 WHERE " & _
                          "PR09NUMPETICION = " & frmpedir.txtText1(0).Text & ")"
                          
                  
        Set rstAselrepet = objApp.rdoConnect.OpenResultset(sqlstrselrepet)
        If rstAselrepet.EOF = True Then
            sqlstrincuest = "INSERT INTO PR4100 (PR03NUMACTPEDI,PR40CODPREGUNTA,PR27CODTIPRESPU," & _
                            "PR41INDROBLIG,PR46CODLISTRESP,PR41RESPUESTA) VALUES " _
                  & "(" & grdDBGrid1(0).Columns(6).Value
                  
            If IsNull(rstAselcuest.rdoColumns("PR40CODPREGUNTA").Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR40CODPREGUNTA").Value
            End If
            
            If IsNull(rstAselcuest.rdoColumns("PR27CODTIPRESPU").Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR27CODTIPRESPU").Value
            End If
            
            If IsNull(rstAselcuest.rdoColumns("PR29INDROBLIG").Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR29INDROBLIG").Value
            End If
            
            If IsNull(rstAselcuest.rdoColumns("PR46CODLISTRESP").Value) Then
                sqlstrincuest = sqlstrincuest & ",null"
            Else
                sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR46CODLISTRESP").Value
            End If
            
            sqlstrincuest = sqlstrincuest & ",null)"
            objApp.rdoConnect.Execute sqlstrincuest, 64
        End If
        rstAselrepet.Close
        Set rstAselrepet = Nothing
        rstAselcuest.MoveNext
    Loop
    rstAselcuest.Close
    Set rstAselcuest = Nothing
End Sub
Private Sub Crear_Cuestionario_Grupo_Peticion()
    Dim rstAselgrupo As rdoResultset
    Dim sqlstrselgrupo As String
    Dim rstAselcuest As rdoResultset
    Dim rstAselrepet As rdoResultset
    Dim sqlstrselcuest As String
    Dim sqlstrselrepet As String
    Dim sqlstrincuest As String
    Dim mlngcodpreg As Long
    Dim mlngcodtipres As Long
    Dim mblnindoblig As Boolean
    Dim mlngcodlistresp As Long
    
    sqlstrselgrupo = "SELECT * FROM PR1700 WHERE PR01CODACTUACION=" & _
                      grdDBGrid1(0).Columns(2).Value

    Set rstAselgrupo = objApp.rdoConnect.OpenResultset(sqlstrselgrupo)
    Do Until rstAselgrupo.EOF
        
        sqlstrselcuest = "SELECT * FROM PR3000 WHERE PR16CODGRUPO=" & _
                          rstAselgrupo.rdoColumns("PR16CODGRUPO").Value
                          
        Set rstAselcuest = objApp.rdoConnect.OpenResultset(sqlstrselcuest)
        
        Do Until rstAselcuest.EOF
            
            sqlstrselrepet = "SELECT * FROM PR4100 WHERE PR40CODPREGUNTA=" & _
                              rstAselcuest.rdoColumns("PR40CODPREGUNTA").Value & " AND PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
            Set rstAselrepet = objApp.rdoConnect.OpenResultset(sqlstrselrepet)
            If rstAselrepet.EOF = True Then
                sqlstrincuest = "INSERT INTO PR4100 (PR03NUMACTPEDI,PR40CODPREGUNTA,PR27CODTIPRESPU," & _
                            "PR41INDROBLIG,PR46CODLISTRESP,PR41RESPUESTA) VALUES (" & grdDBGrid1(0).Columns(6).Value
                If IsNull(rstAselcuest.rdoColumns("PR40CODPREGUNTA").Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR40CODPREGUNTA").Value
                End If
                If IsNull(rstAselcuest.rdoColumns("PR27CODTIPRESPU").Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR27CODTIPRESPU").Value
                End If
                If IsNull(rstAselcuest.rdoColumns("PR30INDOBLIG").Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR30INDOBLIG").Value
                End If
                If IsNull(rstAselcuest.rdoColumns("PR46CODLISTRESP").Value) Then
                    sqlstrincuest = sqlstrincuest & ",null"
                Else
                    sqlstrincuest = sqlstrincuest & "," & rstAselcuest.rdoColumns("PR46CODLISTRESP").Value
                End If
                sqlstrincuest = sqlstrincuest & ",null)"
                objApp.rdoConnect.Execute sqlstrincuest, 64
            End If
            rstAselrepet.Close
            Set rstAselrepet = Nothing
            rstAselcuest.MoveNext
        Loop
        rstAselcuest.Close
         Set rstAselcuest = Nothing
        rstAselgrupo.MoveNext
    Loop
    rstAselgrupo.Close
    Set rstAselgrupo = Nothing

End Sub
Private Sub Borrar_Cuestionario_Peticion()
    Dim sqlborrar As String
  If grdDBGrid1(0).Columns(6).Value <> "" Then
    sqlborrar = "DELETE FROM PR4100 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
    objApp.rdoConnect.Execute sqlborrar, 64
  End If
End Sub
Private Sub Crear_Muestras_Pedidas()
    Dim rstAselmuestras As rdoResultset
    Dim sqlstrselmuestras As String
    Dim sqlstrinmuestras As String
    Dim mlngcodmuestra As Long
    Dim mstrdesmuestra As String
    Dim mlngcodtipmuestra  As Long
    
    sqlstrselmuestras = "SELECT * FROM PR2500 WHERE PR01CODACTUACION=" & _
                      grdDBGrid1(0).Columns(2).Value

    Set rstAselmuestras = objApp.rdoConnect.OpenResultset(sqlstrselmuestras)
    
    Do Until rstAselmuestras.EOF
        sqlstrinmuestras = "INSERT INTO PR4200 (PR03NUMACTPEDI,PR42NUMMUESTRA,PR24CODTIPMUESTR,PR42DESMUESTRA) VALUES (" & grdDBGrid1(0).Columns(6).Value
        If IsNull(rstAselmuestras.rdoColumns("PR25CODMUESTRA").Value) Then
            sqlstrinmuestras = sqlstrinmuestras & ",null"
        Else
            sqlstrinmuestras = sqlstrinmuestras & "," & rstAselmuestras.rdoColumns("PR25CODMUESTRA").Value
        End If
        If IsNull(rstAselmuestras.rdoColumns("PR24CODTIPMUESTR").Value) Then
            sqlstrinmuestras = sqlstrinmuestras & ",null"
        Else
            sqlstrinmuestras = sqlstrinmuestras & "," & rstAselmuestras.rdoColumns("PR24CODTIPMUESTR").Value
        End If
        If IsNull(rstAselmuestras.rdoColumns("PR25DESMUESTRA").Value) Then
            sqlstrinmuestras = sqlstrinmuestras & ",null"
        Else
            sqlstrinmuestras = sqlstrinmuestras & ",'" & rstAselmuestras.rdoColumns("PR25DESMUESTRA").Value & "'"
        End If
        sqlstrinmuestras = sqlstrinmuestras & ")"
        objApp.rdoConnect.Execute sqlstrinmuestras, 64

        rstAselmuestras.MoveNext
    Loop
    rstAselmuestras.Close
    Set rstAselmuestras = Nothing
End Sub
Private Sub crear_restricciones()
Dim rstact As rdoResultset
Dim stract As String
Dim rsttipo As rdoResultset
Dim strtipo As String
Dim strrest As String
Dim rstrest As rdoResultset
Dim strA As String
Dim rstA As rdoResultset
Dim strinsert As String

stract = "select pr03numactpedi from PR0300 where pr09numpeticion=" & frmpedir.txtText1(0).Text
Set rstact = objApp.rdoConnect.OpenResultset(stract)
While Not rstact.EOF
  strtipo = "select ag14codtiprecu from pr1400 where pr03numactpedi=" & _
           rstact.rdoColumns(0).Value
  Set rsttipo = objApp.rdoConnect.OpenResultset(strtipo)
  While Not rsttipo.EOF
    strrest = "select ag16codtiprest from AG1500 where ag14codtiprecu=" & rsttipo.rdoColumns(0).Value
    Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
    While Not rstrest.EOF
      strA = "select count(*) from PR4700 where pr09numpeticion=" & _
          frmpedir.txtText1(0).Text & " AND ag16codtiprest=" & rstrest.rdoColumns(0).Value
      Set rstA = objApp.rdoConnect.OpenResultset(strA)
      If rstA.rdoColumns(0).Value = 0 Then
         strinsert = "INSERT INTO PR4700(pr09numpeticion,ag16codtiprest,pr47valor) " & _
                   "VALUES(" & frmpedir.txtText1(0).Text & "," & _
                   rstrest.rdoColumns(0).Value & ",' ')"
         objApp.rdoConnect.Execute strinsert, 64
      End If
      rstA.Close
      Set rstA = Nothing
      rstrest.MoveNext
    Wend
    rstrest.Close
    Set rstrest = Nothing
    rsttipo.MoveNext
  Wend
  rsttipo.Close
  Set rsttipo = Nothing
  rstact.MoveNext
  Wend
  
  rstact.Close
  Set rstact = Nothing

End Sub
Private Sub crear_restricciones_sin_actuacion(actuacionpedida)
Dim rstact As rdoResultset
Dim stract As String
Dim rsttipo As rdoResultset
Dim strtipo As String
Dim strrest As String
Dim rstrest As rdoResultset
Dim strA As String
Dim rstA As rdoResultset
Dim strinsert As String

stract = "select pr03numactpedi from PR0300 where " & _
         "pr09numpeticion=" & frmpedir.txtText1(0).Text & _
         " and pr03numactpedi<>" & actuacionpedida
Set rstact = objApp.rdoConnect.OpenResultset(stract)
While Not rstact.EOF
  strtipo = "select ag14codtiprecu from pr1400 where pr03numactpedi=" & _
           rstact.rdoColumns(0).Value
  Set rsttipo = objApp.rdoConnect.OpenResultset(strtipo)
  While Not rsttipo.EOF
    strrest = "select ag16codtiprest from AG1500 where ag14codtiprecu=" & rsttipo.rdoColumns(0).Value
    Set rstrest = objApp.rdoConnect.OpenResultset(strrest)
    While Not rstrest.EOF
      strA = "select count(*) from PR4700 where pr09numpeticion=" & _
          frmpedir.txtText1(0).Text & " AND ag16codtiprest=" & rstrest.rdoColumns(0).Value
      Set rstA = objApp.rdoConnect.OpenResultset(strA)
      If rstA.rdoColumns(0).Value = 0 Then
         strinsert = "INSERT INTO PR4700(pr09numpeticion,ag16codtiprest,pr47valor) " & _
                   "VALUES(" & frmpedir.txtText1(0).Text & "," & _
                   rstrest.rdoColumns(0).Value & ",' ')"
         objApp.rdoConnect.Execute strinsert, 64
      End If
      rstA.Close
      Set rstA = Nothing
      rstrest.MoveNext
    Wend
    rstrest.Close
    Set rstrest = Nothing
    rsttipo.MoveNext
  Wend
  rsttipo.Close
  Set rsttipo = Nothing
  rstact.MoveNext
  Wend
  
  rstact.Close
  Set rstact = Nothing

End Sub

Private Sub Borrar_Muestras_Pedidas()
    Dim sqlborrar As String
    Dim sqlstrsel As String
    Dim rstasel As rdoResultset
    
    
    sqlstrsel = "select pr52nummuestra from pr4200 where pr04numactplan=" & grdDBGrid1(0).Columns(6).Value
    Set rstasel = objApp.rdoConnect.OpenResultset(sqlstrsel)
    
    While rstasel.EOF = False
      sqlborrar = "DELETE FROM PR4300 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value & ")"
      objApp.rdoConnect.Execute sqlborrar, 64
      sqlborrar = "DELETE FROM PR4200 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value & ")"
      objApp.rdoConnect.Execute sqlborrar, 64
      sqlborrar = "DELETE FROM PR5400 WHERE PR52NUMMUESTRA=" & rstasel(0).Value
      objApp.rdoConnect.Execute sqlborrar, 64
      sqlborrar = "DELETE FROM PR5200 WHERE PR52NUMMUESTRA=" & rstasel(0).Value
      objApp.rdoConnect.Execute sqlborrar, 64
      rstasel.MoveNext
    Wend
    
    rstasel.Close
    Set rstasel = Nothing
    
End Sub
Private Sub Crear_Interacciones()
    Dim mlngcodactorigen As Long
    Dim mlngcodactdestino As Long
    Dim sqlstrselinter As String
    Dim sqlstrininter As String
    Dim rstaselinter As rdoResultset
    Dim sqlstrselactdes As String
    Dim rstaselactdes As rdoResultset
    Dim sqlstrselcod As String
    Dim rstaselcod As rdoResultset
    Dim sqlstrorigen As String
    Dim rstorigen As rdoResultset
    Dim sqlstrinsert As String
    Dim rstinsert As rdoResultset
    
    'Call Buscar_Actuaciones(mlngcodactorigen, mlngcodactdestino)
    
    sqlstrselactdes = "SELECT * FROM PR0800 WHERE PR09NUMPETICION=" & _
                        frmpedir.txtText1(0).Text & " AND PR08NUMSECUENCIA=1"

    Set rstaselactdes = objApp.rdoConnect.OpenResultset(sqlstrselactdes)
    
    Do Until rstaselactdes.EOF
    
        sqlstrselcod = "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI=" & rstaselactdes.rdoColumns("PR03NUMACTPEDI").Value
        Set rstaselcod = objApp.rdoConnect.OpenResultset(sqlstrselcod)
          
        sqlstrorigen = "SELECT * FROM PR0300 WHERE pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
        Set rstorigen = objApp.rdoConnect.OpenResultset(sqlstrorigen)
        If rstorigen.EOF = False Then
            mlngcodactorigen = grdDBGrid1(0).Columns(2).Value
            mlngcodactdestino = rstaselcod.rdoColumns("PR01CODACTUACION").Value
            
            sqlstrselinter = "SELECT * FROM PR2000 WHERE PR01CODACTUACION=" & _
                                mlngcodactorigen & " AND PR01CODACTUACION_DES=" & _
                                mlngcodactdestino
            Set rstaselinter = objApp.rdoConnect.OpenResultset(sqlstrselinter)
        
            
            If rstaselinter.EOF = False Then
                sqlstrininter = "INSERT INTO PR3900 (PR03NUMACTPEDI,PR03NUMACTPEDI_DES,PR19CODTIPINTERAC,PR39NUMMININTER) VALUES ("
                
                If IsNull(grdDBGrid1(0).Columns(6).Value) Then
                    sqlstrininter = sqlstrininter & "null"
                Else
                    sqlstrininter = sqlstrininter & grdDBGrid1(0).Columns(6).Value
                End If
                If IsNull(rstaselactdes.rdoColumns("PR03NUMACTPEDI").Value) Then
                    sqlstrininter = sqlstrininter & ",null"
                Else
                    sqlstrininter = sqlstrininter & "," & rstaselactdes.rdoColumns("PR03NUMACTPEDI").Value
                End If
                If IsNull(rstaselinter.rdoColumns("PR19CODTIPINTERAC").Value) Then
                    sqlstrininter = sqlstrininter & ",null"
                Else
                    sqlstrininter = sqlstrininter & "," & rstaselinter.rdoColumns("PR19CODTIPINTERAC").Value
                End If
                If IsNull(rstaselinter.rdoColumns("PR20NUMTIEMPOINT").Value) Then
                    sqlstrininter = sqlstrininter & ",null"
                Else
                    sqlstrininter = sqlstrininter & "," & rstaselinter.rdoColumns("PR20NUMTIEMPOINT").Value
                End If
                sqlstrininter = sqlstrininter & ")"
                'se comprueba que la interacci�n no est� guardada ya
                sqlstrinsert = "SELECT * FROM PR3900 WHERE " & _
                      "pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value & " AND " & _
                      "pr03numactpedi_des=" & rstaselactdes.rdoColumns("PR03NUMACTPEDI").Value
                Set rstinsert = objApp.rdoConnect.OpenResultset(sqlstrinsert)
                If rstinsert.EOF Then
                  objApp.rdoConnect.Execute sqlstrininter, 64
                End If
                rstinsert.Close
                Set rstinsert = Nothing
            End If
            rstaselinter.Close
            Set rstaselinter = Nothing
            rstaselactdes.MoveNext
        End If
        rstaselcod.Close
        rstorigen.Close
        Set rstaselcod = Nothing
        Set rstorigen = Nothing
    Loop
    rstaselactdes.Close
    Set rstaselactdes = Nothing
End Sub
Private Sub Borrar_Interacciones()
    Dim sqlborrar As String
  If grdDBGrid1(0).Columns(6).Value <> "" Then
    sqlborrar = "DELETE FROM PR3900 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value & _
                    " OR PR03NUMACTPEDI_DES=" & grdDBGrid1(0).Columns(6).Value
    objApp.rdoConnect.Execute sqlborrar, 64
  End If
End Sub
Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub
Private Sub Inicializar_Grid()
    Dim sqlstrselact As String
    Dim rstaselact As rdoResultset
    Dim sqlstrselact2 As String
    Dim rstaselact2 As rdoResultset
    Dim sqlstrselcond As String
    Dim rstaselcond As rdoResultset
    Dim mblncumplircond As Boolean
    Dim rstaselcodact As rdoResultset
    Dim sqlstrselcodact As String
    Dim rstaseldesdpto As rdoResultset
    Dim sqlstrseldesdpto As String
    Dim mblncondiciones As Boolean
    Dim rstdescripcion As rdoResultset
    Dim strdescripcion As String
    
    grdDBGrid1(0).Columns(1).Locked = True
    grdDBGrid1(0).Columns(2).Locked = True
    grdDBGrid1(0).Columns(3).Locked = True
    grdDBGrid1(0).Columns(4).Locked = True
    grdDBGrid1(0).Columns(5).Locked = True
    grdDBGrid1(0).Columns(6).Locked = True
    grdDBGrid1(0).Columns(7).Locked = True
    grdDBGrid1(0).Columns(8).Locked = True
    grdDBGrid1(0).Columns(10).Locked = True
    
    If frmpedir.txtText1(0).Text <> "" Then
      sqlstrselact = "SELECT * FROM PR0800 WHERE PR09NUMPETICION=" & _
                          frmpedir.txtText1(0).Text
  
      Set rstaselact = objApp.rdoConnect.OpenResultset(sqlstrselact)
      Do Until rstaselact.EOF
          'sacar Selec
          grdDBGrid1(0).AddNew
          grdDBGrid1(0).Columns(0).Value = True
          'sacar Cond
          
          sqlstrselact2 = "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI=" & _
                          rstaselact.rdoColumns("PR03NUMACTPEDI").Value & " AND PR09NUMPETICION=" & frmpedir.txtText1(0).Text
      
          Set rstaselact2 = objApp.rdoConnect.OpenResultset(sqlstrselact2)
          
          sqlstrselcond = "SELECT * FROM PR3800 WHERE PR03NUMACTPEDI=" & _
                          rstaselact.rdoColumns("PR03NUMACTPEDI").Value
      
          Set rstaselcond = objApp.rdoConnect.OpenResultset(sqlstrselcond)
          mblncumplircond = False
          mblncondiciones = False
          Do Until rstaselcond.EOF
              mblncumplircond = True
              mblncondiciones = True
              If rstaselact2.EOF = False Then
                  If rstaselcond.rdoColumns("PR38INDRESPUESTA").Value = False Then
                      mblncumplircond = False
                      Exit Do
                  End If
              End If
              rstaselcond.MoveNext
          Loop
          If mblncumplircond = True Or mblncondiciones = False Then
              grdDBGrid1(0).Columns(1).Value = True
          Else
              grdDBGrid1(0).Columns(1).Value = False
              glngSelCond = glngSelCond + 1
          End If
          'sacar CodAct
          sqlstrselcodact = "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI=" & _
                              rstaselact.rdoColumns("PR03NUMACTPEDI").Value
      
          Set rstaselcodact = objApp.rdoConnect.OpenResultset(sqlstrselcodact)
          
          strdescripcion = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & _
                         rstaselcodact.rdoColumns("PR01CODACTUACION").Value
          Set rstdescripcion = objApp.rdoConnect.OpenResultset(strdescripcion)
          
          grdDBGrid1(0).Columns(2).Value = rstaselcodact.rdoColumns("PR01CODACTUACION").Value
          'sacar DesAct
          'grdDBGrid1(0).Columns(3).Value = rstaselcodact.rdoColumns("PR03DESCORTA").Value
          grdDBGrid1(0).Columns(3).Value = rstdescripcion.rdoColumns(0).Value
          rstdescripcion.Close
          Set rstdescripcion = Nothing
          
          'sacar CodDpto
          grdDBGrid1(0).Columns(4).Value = rstaselcodact.rdoColumns("AD02CODDPTO").Value
          'sacar DesDpto
          sqlstrseldesdpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO=" & _
                              rstaselcodact.rdoColumns("AD02CODDPTO").Value
      
          Set rstaseldesdpto = objApp.rdoConnect.OpenResultset(sqlstrseldesdpto)
          
          grdDBGrid1(0).Columns(5).Value = rstaseldesdpto.rdoColumns("AD02DESDPTO").Value
          
          'sacar Pr0300
          If rstaselcodact.rdoColumns("PR09NUMPETICION").Value = rstaselact.rdoColumns("PR09NUMPETICION").Value Then
              grdDBGrid1(0).Columns(6).Value = rstaselact.rdoColumns("PR03NUMACTPEDI").Value
              grdDBGrid1(0).Columns(10).Value = True
          Else
              grdDBGrid1(0).Columns(10).Value = False
          End If
          'sacar Pr0800
          grdDBGrid1(0).Columns(7).Value = rstaselact.rdoColumns("PR03NUMACTPEDI").Value
          'sacar Pr0800Sec
          grdDBGrid1(0).Columns(8).Value = rstaselact.rdoColumns("PR08NUMSECUENCIA").Value
          'sacar Asociada a
          If IsNull(rstaselcodact.rdoColumns("PR03NUMACTPEDI").Value) Then
              grdDBGrid1(0).Columns(9).Value = 0
          Else
              grdDBGrid1(0).Columns(9).Value = rstaselcodact.rdoColumns("PR03NUMACTPEDI").Value
          End If
          grdDBGrid1(0).Update
          grdDBGrid1(0).MoveNext
          rstaselact.MoveNext
          rstaselact2.Close
          rstaselcond.Close
          rstaselcodact.Close
          rstaseldesdpto.Close
          Set rstaselact2 = Nothing
          Set rstaselcond = Nothing
          Set rstaselcodact = Nothing
          Set rstaseldesdpto = Nothing
      Loop
      grdDBGrid1(0).MoveFirst
      If glngSelCond = 0 Then
          cmdpeticion.Enabled = True
          cmdCambPlanific.Enabled = True
          cmdNoReal.Enabled = True
      Else
          cmdpeticion.Enabled = False
          cmdCambPlanific.Enabled = False
          cmdNoReal.Enabled = False
      End If
      rstaselact.Close
      Set rstaselact = Nothing
    End If
End Sub
Private Sub preguntar_medico(mblncrearact As Boolean)
    Dim strrespuesta As String
    
    If gblnPedReal = 1 Then
        strrespuesta = MsgBox("La actuaci�n '" _
                & frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value & "'" _
                 & " del Departamento '" & _
                frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value & "'" _
                 & Chr(13) & " ya ha sido planificada con fecha " & FechaVigencia & Chr(13) _
                 & " en la Petici�n n�mero " & glngPedir & Chr(13) & _
                "                      �Desea que se la vuelva a realizar al paciente?", vbYesNo, "SELECCION DE ACTUACIONES")
    Else
      If gblnPedReal = 2 Then
        strrespuesta = MsgBox("La actuaci�n '" _
                & frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value & "'" _
                 & " del Departamento '" & _
                frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value & "'" _
                 & Chr(13) & " ya ha sido citada con fecha " & FechaVigencia & Chr(13) _
                 & " en la Petici�n n�mero " & glngPedir & Chr(13) & _
                "                      �Desea que se la vuelva a realizar al paciente?", vbYesNo, "SELECCION DE ACTUACIONES")
      Else
        strrespuesta = MsgBox("La actuaci�n '" _
                & frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value & "'" _
                 & " del Departamento '" & _
                frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value & "'" _
                & Chr(13) & " ya ha sido realizada con fecha " & FechaVigencia & Chr(13) _
                & " en la Petici�n n�mero " & glngPedir & Chr(13) & _
                "                      �Desea que se la vuelva a realizar al paciente?", vbYesNo, "SELECCION DE ACTUACIONES")
      End If
    End If
    If strrespuesta = vbYes Then
        mblncrearact = True
    Else
        mblncrearact = False
    End If
End Sub
Private Sub Activacion_Formularpet()

Dim mblnSinActuaciones As Boolean
Dim cont As Integer
    
    mblnSinActuaciones = True
    
    If grdDBGrid1(0).Rows = 0 Then
        cmdpeticion.Enabled = False
        cmdCambPlanific.Enabled = False
        cmdNoReal.Enabled = False
    Else
        grdDBGrid1(0).Redraw = False
        grdDBGrid1(0).MoveFirst
        cont = 0
        cmdpeticion.Enabled = True
        cmdCambPlanific.Enabled = True
        cmdNoReal.Enabled = True
        Do While cont < grdDBGrid1(0).Rows
            If (grdDBGrid1(0).Columns(0).Value = True) And _
               (grdDBGrid1(0).Columns(1).Value = False) Then
                    cmdpeticion.Enabled = False
                    cmdCambPlanific.Enabled = False
                    cmdNoReal.Enabled = False
                Exit Do
            Else
                If grdDBGrid1(0).Columns(6).Value <> "" Then
                    mblnSinActuaciones = False
                End If
            End If
            If cont = grdDBGrid1(0).Rows - 1 Then
                Exit Do
            Else
                grdDBGrid1(0).MoveNext
                cont = cont + 1
            End If
        Loop
        grdDBGrid1(0).MoveFirst
        grdDBGrid1(0).Redraw = True
    End If
    If mblnSinActuaciones = True Then
        cmdpeticion.Enabled = False
        cmdCambPlanific.Enabled = False
        cmdNoReal.Enabled = False
    End If
End Sub
Private Sub Borrar_Condiciones()
    Dim sqlstr As String
    If grdDBGrid1(0).Columns(6).Value <> "" Then
        sqlstr = "Delete From pr3800 where " & _
                 "pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
        objApp.rdoConnect.Execute sqlstr
    End If
End Sub

Private Sub cmdActuaciones_Click()
   Call objsecurity.LaunchProcess("PR0193")

End Sub

Private Sub cmdCambPlanific_Click()
  Dim strsqlact As String
  Dim rstact As rdoResultset
  
  cmdCambPlanific.Enabled = False
  strsqlact = "SELECT COUNT(*) FROM PR0600 WHERE PR03NUMACTPEDI IN (SELECT PR03NUMACTPEDI FROM PR0300 WHERE PR09NUMPETICION=" & frmpedir.txtText1(0).Text & ")"
  Set rstact = objApp.rdoConnect.OpenResultset(strsqlact)
  If rstact(0).Value > 0 Then
    frmPlanifActuaciones.txtText1(0).Text = txtText1(0).Text
    frmPlanifActuaciones.txtText1(1).Text = txtText1(1).Text
    frmPlanifActuaciones.txtText1(2).Text = txtText1(2).Text
    frmPlanifActuaciones.txtText1(3).Text = txtText1(3).Text
    frmPlanifActuaciones.txtText1(4).Text = txtText1(4).Text
    frmPlanifActuaciones.txtText1(5).Text = txtText1(5).Text
    Call objsecurity.LaunchProcess("PR0199")
  Else
    Call MsgBox("Antes debe Planificar", vbExclamation)
  End If
  cmdCambPlanific.Enabled = True
End Sub

Private Sub cmdcondiciones_Click()
    Dim vigencia As Boolean
    Dim mblncrearact As Boolean
    Dim cont As Integer
    
    grdDBGrid1(0).MoveFirst
    cont = 0
    Do While cont < grdDBGrid1(0).Rows
        If grdDBGrid1(0).Columns(1).Value = False Then
            If grdDBGrid1(0).Columns(0).Value = True Then
                If grdDBGrid1(0).Columns(7).Value = "" Then
                    mblncrearact = True
                    Call esta_pedida(numero_pedida, pedida)
                    Call comprobar_vigencia(vigencia)
                    If pedida And vigencia Then
                            Call preguntar_medico(mblncrearact)
                    End If
                    If mblncrearact = False Then
                        Call crear_relacion(numero_pedida, numero_secuencia) 'pr0800
                        grdDBGrid1(0).Columns(10).Value = False
                    Else
                        Call crear_actuacion_pedida(numero_pedida)  'pr0300
                        Call crear_relacion(numero_pedida, numero_secuencia) 'pr0800
                        grdDBGrid1(0).Columns(10).Value = True
                        Call crear_condiciones
                    End If
                Else
                    numero_pedida = grdDBGrid1(0).Columns(7).Value
                    crear_condiciones
                End If
            Else
            'borrar ocurrencias de pr0300 y pr0800 si existen
                Call borrar_relacion
                If grdDBGrid1(0).Columns(6).Value <> "" Then
                  Call borrar_peticion
                  '!  Call borrar_peticion
                End If
    
            End If
        End If
        If cont = grdDBGrid1(0).Rows - 1 Then
            Exit Do
        Else
            grdDBGrid1(0).MoveNext
            cont = cont + 1
        End If
    Loop
   frmSeleccionarAct.grdDBGrid1(0).MoveFirst
   'Load frmpedircond
   'Call frmpedircond.Show(vbModal)
   Call objsecurity.LaunchProcess("PR0158")
   Call Borrar_No_Seleccionadas
   Call Activacion_Formularpet
   'Unload frmpedircond
   'Set frmpedircond = Nothing
End Sub
Private Sub Borrar_No_Seleccionadas()
Dim mblnborrar As Boolean
Dim sqlstr As String
Dim rstA As rdoResultset
Dim intResp As String
Dim mblnseborra As Boolean
Dim cont As Integer

    grdDBGrid1(0).MoveFirst
    cont = 0
    Do While cont < grdDBGrid1(0).Rows
        mblnseborra = False
        If grdDBGrid1(0).Columns(0).Value = False Then
            mblnborrar = True
            If grdDBGrid1(0).Columns(6).Value <> "" Then
                sqlstr = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & _
                            grdDBGrid1(0).Columns(6).Value
                Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
                If rstA.EOF = False Then
                    intResp = MsgBox("La actuaci�n ya ha sido planificada", vbExclamation)
                    mblnborrar = False
                End If
                rstA.Close
            End If
            If mblnborrar = True Then
                Call borrar_relacion
                If grdDBGrid1(0).Columns(6).Value <> "" Then
                    Call borrar_peticion
                End If
                If grdDBGrid1(0).Rows = 0 Or grdDBGrid1(0).Rows = 1 Then
                    grdDBGrid1(0).RemoveAll
                Else
                    Call grdDBGrid1(0).RemoveItem(cont)
                End If
                mblnseborra = True
            End If
        End If
        If cont = grdDBGrid1(0).Rows Then
            Exit Do
        Else
            If mblnseborra = False Then
                grdDBGrid1(0).MoveNext
                cont = cont + 1
            End If
        End If
    Loop
    Set rstA = Nothing
End Sub


Private Sub cmdgrupo_Click()
   Call objsecurity.LaunchProcess("PR0171")
End Sub

Private Sub cmdNoReal_Click()
  cmdNoReal.Enabled = False
  Call objsecurity.LaunchProcess("PR0218")
  cmdNoReal.Enabled = True
End Sub

Private Sub Form_Activate()
    Call Activacion_Formularpet
    Call Form_Paint
End Sub
Private Sub Incrementar_Solicitadas()
    Dim sqlstrselactsol As String
    Dim rstAselactsol As rdoResultset
    Dim sqlstrinactsol As String
    Dim sqlstrupactsol As String
    
    sqlstrselactsol = "SELECT * FROM PR4400 WHERE AD02CODDPTO=" & frmpedir.txtText1(1).Text & _
                        " AND PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value & _
                        " AND AD02CODDPTO_REA=" & grdDBGrid1(0).Columns(4).Value
    Set rstAselactsol = objApp.rdoConnect.OpenResultset(sqlstrselactsol)
    If rstAselactsol.EOF = True Then
        sqlstrinactsol = "INSERT INTO PR4400 " _
                          & " (AD02CODDPTO,PR01CODACTUACION,AD02CODDPTO_REA,PR44NUMCUENTA) VALUES " _
                          & " (" & frmpedir.txtText1(1).Text & "," & grdDBGrid1(0).Columns(2).Value & "," & grdDBGrid1(0).Columns(4).Value & ",1)"
        objApp.rdoConnect.Execute sqlstrinactsol, 64
    Else
        sqlstrupactsol = "UPDATE PR4400 SET PR44NUMCUENTA=" & rstAselactsol.rdoColumns("PR44NUMCUENTA") + 1 _
           & " WHERE AD02CODDPTO=" & frmpedir.txtText1(1).Text & _
             " AND PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value & _
             " AND AD02CODDPTO_REA=" & grdDBGrid1(0).Columns(4).Value
        objApp.rdoConnect.Execute sqlstrupactsol, 64
    End If
    rstAselactsol.Close
    Set rstAselactsol = Nothing
End Sub
Private Sub Decrementar_Solicitadas()
    Dim sqlstrselactsol As String
    Dim rstAselactsol As rdoResultset
    Dim sqlstrinactsol As String
    Dim sqlstrupactsol As String
    
  If grdDBGrid1(0).Columns(6).Value <> "" Then
    sqlstrselactsol = "SELECT * FROM PR4400 WHERE AD02CODDPTO=" & frmpedir.txtText1(1).Text & _
                        " AND PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value & _
                        " AND AD02CODDPTO_REA=" & grdDBGrid1(0).Columns(4).Value
    Set rstAselactsol = objApp.rdoConnect.OpenResultset(sqlstrselactsol)
    If rstAselactsol.EOF <> True Then
        If rstAselactsol.rdoColumns("PR44NUMCUENTA").Value = 1 Then
            sqlstrinactsol = "DELETE FROM PR4400 WHERE AD02CODDPTO=" & frmpedir.txtText1(1).Text & _
                        " AND PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value & _
                        " AND AD02CODDPTO_REA=" & grdDBGrid1(0).Columns(4).Value
            objApp.rdoConnect.Execute sqlstrinactsol, 64
        Else
            sqlstrupactsol = "UPDATE PR4400 SET PR44NUMCUENTA=" & rstAselactsol.rdoColumns("PR44NUMCUENTA") - 1 _
                & " WHERE AD02CODDPTO=" & frmpedir.txtText1(1).Text & _
                " AND PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value & _
                " AND AD02CODDPTO_REA=" & grdDBGrid1(0).Columns(4).Value
            objApp.rdoConnect.Execute sqlstrupactsol, 64
        End If
    End If
    rstAselactsol.Close
    Set rstAselactsol = Nothing
  End If
End Sub

Private Sub cmdpeticion_Click()
  Dim sqlstrselfases As String
  Dim rstaselfases As rdoResultset
  Dim sqlstrorigen As String
  Dim rstorigen As rdoResultset
  Dim cont As Integer
  Dim strsql As String
  Dim rstA As rdoResultset
  'Dim strsqlAct As String
  'Dim rstact As rdoResultset
  Dim ir_siguiente As Boolean
  Dim mintContes As Integer
  Dim mblnPlanif As Boolean
  'Dim mblnNueva As Boolean
  Dim sqlCitadas As String
  Dim rstCitadas As rdoResultset
  Dim fecha1 As String
  Dim strsql900 As String
  Dim rsta900 As rdoResultset
  Dim strsqlact As String
  Dim mensaje As Boolean
  Dim sqlInstrReal As String
  Dim rstInstrReal As rdoResultset
  
      mensaje = False
      ir_siguiente = False
      cmdpeticion.Enabled = False
      Frame2.Visible = True
      Frame2.ZOrder (0)
      Picture1.ZOrder (0)
      Label1.ZOrder (0)
      Label2.ZOrder (0)
      Call Form_Paint
        strsql = "SELECT COUNT(*) FROM PR0400 WHERE PR03NUMACTPEDI IN " & _
                  "(SELECT PR03NUMACTPEDI FROM PR0300 WHERE PR09NUMPETICION=" & frmpedir.txtText1(0).Text & ")"
        Set rstA = objApp.rdoConnect.OpenResultset(strsql)
        If rstA.rdoColumns(0).Value > 0 Then
          mblnPlanif = True
          mintContes = MsgBox("La Petici�n tiene Actuaciones planificadas. �Desea replanificarlas?", vbExclamation + vbYesNo)
        Else
          mblnPlanif = False
        End If
        grdDBGrid1(0).MoveFirst
        cont = 0
        Do While cont < grdDBGrid1(0).Rows
            If grdDBGrid1(0).Columns(0).Value = True And grdDBGrid1(0).Columns(1).Value = True Then
                If grdDBGrid1(0).Columns(6).Value <> "" Then
                
                  sqlstrorigen = "SELECT * FROM PR0300 WHERE pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
                  Set rstorigen = objApp.rdoConnect.OpenResultset(sqlstrorigen)
                  If rstorigen.EOF = False Then
                  '***************************************************************jcr 3/8/98
                    sqlInstrReal = "SELECT * FROM PR0100 WHERE PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value
                    Set rstInstrReal = objApp.rdoConnect.OpenResultset(sqlInstrReal)
                  '***************************************************************
                    sqlstrselfases = "SELECT * FROM PR0600 WHERE PR03NUMACTPEDI=" & _
                                      grdDBGrid1(0).Columns(6).Value
                    Set rstaselfases = objApp.rdoConnect.OpenResultset(sqlstrselfases)
                    If rstaselfases.EOF Then
                        Call Crear_Fases_Pedidas
                        Call Crear_Cuestionario_Peticion
                        Call Crear_Cuestionario_Grupo_Peticion
                        'Crear las muestras de las actuaciones
                        'Call Crear_Muestras_Pedidas
                        'Crear las interacciones de las actuaciones
                        Call Crear_Interacciones
                        'Crear las actuaciones mas solicitadas
                        Call Incrementar_Solicitadas
                        Call crear_restricciones
                    End If
                    '**************************************************************jcr3/8/98
                    If rstInstrReal.EOF = False Then
                      If rstInstrReal("PR01INDINSTRREA").Value = -1 Then
                        sqlInstrReal = "UPDATE PR0300 SET PR03indcitable=0 WHERE pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
                        objApp.rdoConnect.Execute sqlInstrReal, 64
                      End If
                    End If
                    rstInstrReal.Close
                    Set rstInstrReal = Nothing
                    '***************************************************************
                    If Es_Posible Then
                      If mblnPlanif = True Then  'Alguna actuaci�n planificada
                        sqlCitadas = "SELECT PR37CODESTADO FROM PR0400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
                        Set rstCitadas = objApp.rdoConnect.OpenResultset(sqlCitadas)
                        If rstCitadas.EOF = False Then 'Se ha Planificado
                            If mintContes = vbYes Then   'Se desea Replanificar
                              If rstCitadas(0).Value = 1 Then   'No est� Citada
                                Call Borrar_Planificacion
                                Call Crear_Planificacion
                                mensaje = True
                              End If
                            End If
                        Else     'No se ha planificado
                            Call Crear_Planificacion
                            mensaje = True
                        End If
                      Else    'Ninguna Actuaci�n planificada
                        Call Crear_Planificacion
                        mensaje = True
                      End If
                    Else
                      strsql900 = "SELECT * FROM PR0900 WHERE PR09NUMPETICION=" & frmpedir.txtText1(0).Text
                      Set rsta900 = objApp.rdoConnect.OpenResultset(strsql900)
                      If IsNull(rsta900.rdoColumns("PR09FECPLANIFIC").Value) Then
                        fecha1 = "NULL"
                      Else
                        If Left(Right(rsta900.rdoColumns("PR09FECPLANIFIC").Value, 3), 1) = "." Then
                          fecha1 = Left(rsta900.rdoColumns("PR09FECPLANIFIC").Value, Len(rsta900.rdoColumns("PR09FECPLANIFIC")) - 3)
                          fecha1 = "to_date('" & fecha1 & "','dd/mm/yyyy hh24:mi')"
                        Else
                          fecha1 = rsta900.rdoColumns("PR09FECPLANIFIC").Value
                          fecha1 = "to_date('" & fecha1 & "','dd/mm/yyyy')"
                        End If
                      End If
                      strsqlact = "UPDATE PR0300 SET PR03FECPREFEREN=" & fecha1 & " WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
                      objApp.rdoConnect.Execute strsqlact, 64
                      rsta900.Close
                      Set rsta900 = Nothing
                      ir_siguiente = True
                    End If
                    rstaselfases.Close
                  Else
                    '*****************
                    sqlstrselfases = "SELECT * FROM PR0600 WHERE PR03NUMACTPEDI=" & _
                                      grdDBGrid1(0).Columns(6).Value
                    Set rstaselfases = objApp.rdoConnect.OpenResultset(sqlstrselfases)
                    If rstaselfases.EOF Then
                        'Crear las actuaciones mas solicitadas
                        Call Incrementar_Solicitadas
                    End If
                    '**************
                    rstaselfases.Close
                  End If
                  rstorigen.Close
                End If
            End If
            If cont = grdDBGrid1(0).Rows - 1 Then
                cont = cont + 1
                Exit Do
            Else
                grdDBGrid1(0).MoveNext
                cont = cont + 1
            End If
        Loop
        'frmdatosactpedida.grdDBGrid1(0).MoveFirst
        
        Frame2.Visible = False
        If cont > 0 Then
          If ir_siguiente = True Then
            frmPlanifActuaciones.txtText1(0).Text = txtText1(0).Text
            frmPlanifActuaciones.txtText1(1).Text = txtText1(1).Text
            frmPlanifActuaciones.txtText1(2).Text = txtText1(2).Text
            frmPlanifActuaciones.txtText1(3).Text = txtText1(3).Text
            frmPlanifActuaciones.txtText1(4).Text = txtText1(4).Text
            frmPlanifActuaciones.txtText1(5).Text = txtText1(5).Text
            Call objsecurity.LaunchProcess("PR0199")
          Else
            If mensaje = True Then
              Call MsgBox("Todas las Actuaciones de la petici�n se han planificado correctamente", vbInformation)
            End If
          End If
        Else
          Call MsgBox("La petici�n no tiene actuaciones", vbInformation)
        End If
        cmdpeticion.Enabled = True
        Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
      
    Set rstaselfases = Nothing
    Set rstorigen = Nothing
End Sub
Private Sub cmddepartamentos_Click()
   Call objsecurity.LaunchProcess("PR0157")
End Sub

Private Sub cmdpaquetes_Click()
   Call objsecurity.LaunchProcess("PR0155")
End Sub

Private Sub cmdprincipales_Click()
        frmSelPrincipales.txtdeptext1(0).Text = frmpedir.txtText1(1).Text
        frmSelPrincipales.txtdeptext1(1).Text = frmpedir.txtText1(5).Text
   Call objsecurity.LaunchProcess("PR0154")
End Sub

Private Sub cmdprotocolos_Click()
   Call objsecurity.LaunchProcess("PR0156")
End Sub


Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

   grdDBGrid1(0).Columns(0).Visible = False
   Inicializar_Toolbar
   Inicializar_Grid
  grdDBGrid1(0).Columns(3).BackColor = &HC0FFFF
  'Call objApp.SplashOff
End Sub

Private Sub Form_Paint()
    Call grdDBGrid1(0).Refresh
    Call Frame2.Refresh
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  'intCancel = objWinInfo.WinExit
End Sub


Private Sub Form_Unload(intCancel As Integer)
  'Call objWinInfo.WinDeRegister
  'Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_Change(Index As Integer)
    Dim mlngbuscar As Long
    Dim mvrtmarca As Variant
    Dim cont As Integer
    
    If grdDBGrid1(0).Col = 0 Then
        If grdDBGrid1(0).Columns(0).Value = False Then
            If grdDBGrid1(0).Columns(1).Value = False Then
                glngSelCond = glngSelCond - 1
            End If
            mlngbuscar = grdDBGrid1(0).Columns(2).Value
            mvrtmarca = grdDBGrid1(0).Bookmark
            grdDBGrid1(0).MoveFirst
            cont = 0
            Do While cont < grdDBGrid1(0).Rows
                If grdDBGrid1(0).Columns(9).Value = mlngbuscar Then
                    If grdDBGrid1(0).Columns(0).Value = True Then
                        grdDBGrid1(0).Columns(0).Value = False
                        grdDBGrid1(0).Col = 0
                        grdDBGrid1_Change (0)
                    End If
                End If
                If cont = grdDBGrid1(0).Rows - 1 Then
                    Exit Do
                Else
                    grdDBGrid1(0).MoveNext
                    cont = cont + 1
                End If
            Loop
            grdDBGrid1(0).Bookmark = mvrtmarca
        Else
            If grdDBGrid1(0).Columns(1).Value = False Then
                glngSelCond = glngSelCond + 1
            End If
        End If
    End If
    If glngSelCond <= 0 Then
        cmdpeticion.Enabled = True
        cmdCambPlanific.Enabled = True
        cmdNoReal.Enabled = True
    Else
        cmdpeticion.Enabled = False
        cmdCambPlanific.Enabled = False
        cmdNoReal.Enabled = False
    End If
End Sub


Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Dim mvarBkmrk As Variant
    Dim fila As Long
    Dim vigencia As Boolean
    Dim mblncrearact As Boolean
    Dim sqlstrcodcond As String
    Dim rstAcodcond As rdoResultset
    Dim sqlstr As String
    Dim rstA As rdoResultset
    Dim mblnborrar As Boolean
    Dim mblnborrado As Boolean
    Dim intResp As Integer
    Dim cont As Integer
    Dim mintfilatotal As Integer
    Dim mvntmarca As Variant
    
    Select Case btnButton.Index
    Case 4:
        grdDBGrid1(0).Redraw = False
        grdDBGrid1(0).MoveFirst
        cont = 0
        Do While cont < grdDBGrid1(0).Rows
            If grdDBGrid1(0).Columns(1).Value = False Then
                If grdDBGrid1(0).Columns(0).Value = True And grdDBGrid1(0).Columns(1).Value = False Then
                    If grdDBGrid1(0).Columns(7).Value = "" Then
                        mblncrearact = True
                        Call esta_pedida(numero_pedida, pedida)
                        Call comprobar_vigencia(vigencia)
                        If pedida And vigencia Then
                            Call preguntar_medico(mblncrearact)

                        End If
                        If mblncrearact = False Then
                            Call crear_relacion(numero_pedida, numero_secuencia) 'pr0800
                            grdDBGrid1(0).Columns(10).Value = False
                            frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = True
                        Else
                            Call crear_actuacion_pedida(numero_pedida)  'pr0300
                            Call crear_relacion(numero_pedida, numero_secuencia) 'pr0800
                            Call crear_condiciones
                            grdDBGrid1(0).Columns(10).Value = True
                            sqlstrcodcond = "SELECT * FROM PR2300 WHERE PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value
                            Set rstAcodcond = objApp.rdoConnect.OpenResultset(sqlstrcodcond)
                            If rstAcodcond.EOF = True Then
                              frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = True
                            End If
                            rstAcodcond.Close
                        End If
                    End If
                Else
                'borrar ocurrencias de pr0300 y pr0800 si existen
                    mblnborrar = True
                    If grdDBGrid1(0).Columns(6).Value <> "" Then
                        sqlstr = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & _
                                    grdDBGrid1(0).Columns(6).Value
                        Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
                        If rstA.EOF = False Then
                            intResp = MsgBox("La actuaci�n ya ha sido planificada", vbExclamation)
                            mblnborrar = False
                        End If
                        rstA.Close
                    End If
                    If mblnborrar = True Then
                        Call borrar_relacion
                        If grdDBGrid1(0).Columns(6).Value <> "" Then
                            Call borrar_peticion
                        End If
                    End If
                End If
            Else
                If grdDBGrid1(0).Columns(0).Value = False Then
                'borrar ocurrencias de pr0300 y pr0800 si existen
                    mblnborrar = True
                    If grdDBGrid1(0).Columns(6).Value <> "" Then
                        sqlstr = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & _
                                    grdDBGrid1(0).Columns(6).Value
                        Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
                        If rstA.EOF = False Then
                            intResp = MsgBox("La actuaci�n ya ha sido planificada", vbExclamation)
                            mblnborrar = False
                        End If
                        rstA.Close
                    End If
                    If mblnborrar = True Then
                        Call Borrar_Condiciones
                        Call borrar_relacion
                        If grdDBGrid1(0).Columns(6).Value <> "" Then
                            Call borrar_peticion
                        End If
                    Else
                        grdDBGrid1(0).Columns(0).Value = True
                    End If
                End If
            End If
            If cont = grdDBGrid1(0).Rows - 1 Then
                Exit Do
            Else
                grdDBGrid1(0).MoveNext
                cont = cont + 1
            End If
        Loop
        grdDBGrid1(0).Redraw = True
        Call Activacion_Formularpet
    Case 8:
            
            mblnborrar = True
            mblnborrado = False
            grdDBGrid1(0).Redraw = False
            If grdDBGrid1(0).Columns(6).Value <> "" Then
                sqlstr = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & _
                            grdDBGrid1(0).Columns(6).Value
                Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
                If rstA.EOF = False Then
                    If rstA.rdoColumns("pr37codestado").Value = 1 Then
                        intResp = MsgBox("La actuaci�n ya ha sido planificada. �Desea Borrarla?", vbQuestion + vbYesNo)
                        If intResp = vbYes Then
                            'si se pulsa Borrar se deben eliminar los registros correspondientes
                            'de PR4700
                            Call Borrar_Restricciones_PR4700
                            'Call Borrar_Recurso_Consumido
                            'Call Borrar_Fase_Realizada
                            Call Borrar_Muestras_Pedidas
                            Call Borrar_Planificadas
                            Call Decrementar_Solicitadas
                            Call Borrar_Interacciones
                            'Call Borrar_Cuestionario_Muestra
                            Call Borrar_Cuestionario_Peticion
                            Call Borrar_Recursos_Pedidos
                            Call Borrar_Fases_Pedidas
                            mblnborrar = True
                            mblnborrado = True
                        Else
                            mblnborrar = False
                        End If
                    Else
                        intResp = MsgBox("La actuaci�n ya ha sido citada o realizada. No puede ser borrada", vbExclamation)
                        mblnborrar = False
                    End If
                End If
                rstA.Close
            End If
            If mblnborrar = True Then
                'si se pulsa Borrar se deben eliminar los registros correspondientes
                'de PR4700
                If mblnborrado = False Then
                Call Borrar_Restricciones_PR4700
                End If
                Call Borrar_Condiciones
                '*JCR 9/9/97 ********************************************
                Call Decrementar_Solicitadas
                Call Borrar_Interacciones
                'Call Borrar_Cuestionario_Muestra
                Call Borrar_Cuestionario_Peticion
                Call Borrar_Recursos_Pedidos
                Call Borrar_Fases_Pedidas
                '********************************************************
                Call borrar_relacion
                If grdDBGrid1(0).Columns(6).Value <> "" Then
                    Call borrar_peticion
                End If
                If grdDBGrid1(0).Rows = 0 Or grdDBGrid1(0).Rows = 1 Then
                    grdDBGrid1(0).RemoveAll
                Else
                    mvntmarca = grdDBGrid1(0).Bookmark
                    mintfilatotal = grdDBGrid1(0).AddItemRowIndex(mvntmarca)
                    Call grdDBGrid1(0).RemoveItem(mintfilatotal)
                End If
            End If
        grdDBGrid1(0).Redraw = True
        Call Activacion_Formularpet
    Case 21:
            Call grdDBGrid1(0).MoveFirst
    Case 22:
            Call grdDBGrid1(0).MovePrevious
    Case 23:
            Call grdDBGrid1(0).MoveNext
    Case 24:
            Call grdDBGrid1(0).MoveLast
    Case 26:
            Call grdDBGrid1(0).Refresh
    Case 6, 30:
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
        'If btnButton.Index = 30 Then
        '  Call crear_condiciones
        'End If
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End Select
    Set rstA = Nothing
    Set rstAcodcond = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Dim mvarBkmrk As Variant
    Dim fila As Long
    Dim vigencia As Boolean
    Dim mblncrearact As Boolean
    Dim sqlstrcodcond As String
    Dim rstAcodcond As rdoResultset
    Dim sqlstr As String
    Dim rstA As rdoResultset
    Dim mblnborrar As Boolean
    Dim intResp As Integer
    Dim cont As Integer
    Dim mintfilatotal As Integer
    Dim mvntmarca As Variant
    Dim mblnborrado As Boolean
    
  'Call objWinInfo.WinProcess(cwProcessData, intindex, 0)
    Select Case intIndex
        Case 40:
            grdDBGrid1(0).Redraw = False
            grdDBGrid1(0).MoveFirst
            cont = 0
            Do While cont < grdDBGrid1(0).Rows
                If grdDBGrid1(0).Columns(1).Value = False Then
                    If grdDBGrid1(0).Columns(0).Value = True And grdDBGrid1(0).Columns(1).Value = False Then
                        If grdDBGrid1(0).Columns(7).Value = "" Then
                            mblncrearact = True
                            Call esta_pedida(numero_pedida, pedida)
                            Call comprobar_vigencia(vigencia)
                            If pedida And vigencia Then
                                Call preguntar_medico(mblncrearact)
    
                            End If
                            If mblncrearact = False Then
                                Call crear_relacion(numero_pedida, numero_secuencia) 'pr0800
                                grdDBGrid1(0).Columns(10).Value = False
                            Else
                                Call crear_actuacion_pedida(numero_pedida)  'pr0300
                                Call crear_relacion(numero_pedida, numero_secuencia) 'pr0800
                                grdDBGrid1(0).Columns(10).Value = True
                                grdDBGrid1(0).Columns(1).Value = True
                            End If
                            sqlstrcodcond = "SELECT * FROM PR2300 WHERE PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value
                            Set rstAcodcond = objApp.rdoConnect.OpenResultset(sqlstrcodcond)
                            If rstAcodcond.EOF = True Then
                                frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = True
                                
                            End If
                            rstAcodcond.Close
                        End If
                    Else
                    'borrar ocurrencias de pr0300 y pr0800 si existen
                        mblnborrar = True
                        If grdDBGrid1(0).Columns(6).Value <> "" Then
                            sqlstr = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & _
                                        grdDBGrid1(0).Columns(6).Value
                            Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
                            If rstA.EOF = False Then
                                intResp = MsgBox("La actuaci�n ya ha sido planificada", vbExclamation)
                                mblnborrar = False
                            End If
                            rstA.Close
                        End If
                        If mblnborrar = True Then
                            Call borrar_relacion
                            If grdDBGrid1(0).Columns(6).Value <> "" Then
                                Call borrar_peticion
                            End If
                        End If
                    End If
                Else
                    If grdDBGrid1(0).Columns(0).Value = False Then
                    'borrar ocurrencias de pr0300 y pr0800 si existen
                        mblnborrar = True
                        If grdDBGrid1(0).Columns(6).Value <> "" Then
                            sqlstr = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & _
                                        grdDBGrid1(0).Columns(6).Value
                            Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
                            If rstA.EOF = False Then
                                intResp = MsgBox("La actuaci�n ya ha sido planificada", vbExclamation)
                                mblnborrar = False
                            End If
                            rstA.Close
                        End If
                        If mblnborrar = True Then
                            Call Borrar_Condiciones
                            Call borrar_relacion
                            If grdDBGrid1(0).Columns(6).Value <> "" Then
                                Call borrar_peticion
                            End If
                        Else
                            grdDBGrid1(0).Columns(0).Value = True
                        End If
                    End If
                End If
                If cont = grdDBGrid1(0).Rows - 1 Then
                    Exit Do
                Else
                    grdDBGrid1(0).MoveNext
                    cont = cont + 1
                End If
            Loop
            grdDBGrid1(0).Redraw = True
            Call Activacion_Formularpet
        Case 60:
            mblnborrar = True
            mblnborrado = False
            grdDBGrid1(0).Redraw = False
            If grdDBGrid1(0).Columns(6).Value <> "" Then
                sqlstr = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI=" & _
                            grdDBGrid1(0).Columns(6).Value
                Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
                If rstA.EOF = False Then
                    If rstA.rdoColumns("pr37codestado").Value = 1 Then
                        intResp = MsgBox("La actuaci�n ya ha sido planificada. �Desea Borrarla?", vbQuestion + vbYesNo)
                        If intResp = vbYes Then
                            Call Borrar_Restricciones_PR4700
                            'Call Borrar_Recurso_Consumido
                            'Call Borrar_Fase_Realizada
                            Call Borrar_Muestras_Pedidas
                            Call Borrar_Planificadas
                            Call Decrementar_Solicitadas
                            Call Borrar_Interacciones
                            'Call Borrar_Cuestionario_Muestra
                            Call Borrar_Cuestionario_Peticion
                            Call Borrar_Recursos_Pedidos
                            Call Borrar_Fases_Pedidas
                            mblnborrar = True
                            mblnborrado = True
                        Else
                            mblnborrar = False
                        End If
                    Else
                        intResp = MsgBox("La actuaci�n ya ha sido citada o realizada. No puede ser borrada", vbExclamation)
                        mblnborrar = False
                    End If
                End If
                rstA.Close
            End If
            If mblnborrar = True Then
                'si se pulsa Borrar se deben eliminar los registros correspondientes
                'de PR4700
                If mblnborrado = False Then
                  Call Borrar_Restricciones_PR4700
                End If
                Call Borrar_Condiciones
                Call borrar_relacion
                If grdDBGrid1(0).Columns(6).Value <> "" Then
                    Call borrar_peticion
                End If
                If grdDBGrid1(0).Rows = 0 Or grdDBGrid1(0).Rows = 1 Then
                    grdDBGrid1(0).RemoveAll
                Else
                    mvntmarca = grdDBGrid1(0).Bookmark
                    mintfilatotal = grdDBGrid1(0).AddItemRowIndex(mvntmarca)
                    Call grdDBGrid1(0).RemoveItem(mintfilatotal)
                End If
            End If
            grdDBGrid1(0).Redraw = True
        Case 80:
            'Se imprime
        Case 100:
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
            'If intIndex = 100 Then
            '  Call crear_condiciones
            'End If
            Unload Me
    End Select
    Set rstA = Nothing
    Set rstAcodcond = Nothing
End Sub


Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessRegister, intindex, 0)
    Select Case intIndex
        Case 40:
            Call grdDBGrid1(0).MoveFirst
        Case 50:
            Call grdDBGrid1(0).MovePrevious
        Case 60:
            Call grdDBGrid1(0).MoveNext
        Case 70:
            Call grdDBGrid1(0).MoveLast
    End Select
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  'Call objWinInfo.WinProcess(cwProcessOptions, intindex, 0)
    If intIndex = 10 Then
        Call grdDBGrid1(0).Refresh
    End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Function Es_Posible()
  Dim strSelect As String
  Dim rstA As rdoResultset
  Dim strSelect1 As String
  Dim rstA1 As rdoResultset

    strSelect = "SELECT count(*) FROM PR4100 WHERE PR41INDROBLIG=-1 AND PR41RESPUESTA IS NULL AND PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
    Set rstA = objApp.rdoConnect.OpenResultset(strSelect)
    If rstA(0).Value = 0 Then
      strSelect1 = "SELECT COUNT(*) FROM PR4700,AG1500,PR1400 WHERE " & _
                    "PR4700.PR09NUMPETICION=" & frmpedir.txtText1(0).Text & _
                    " AND PR4700.AG16CODTIPREST = AG1500.AG16CODTIPREST" & _
                    " AND AG1500.AG14CODTIPRECU = PR1400.AG14CODTIPRECU" & _
                    " AND PR1400.PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value & _
                    " AND PR4700.PR47VALOR=' '"
      Set rstA1 = objApp.rdoConnect.OpenResultset(strSelect1)
      If rstA1(0).Value = 0 Then
        Es_Posible = True
      Else
        Es_Posible = False
      End If
    Else
      Es_Posible = False
    End If
End Function
Private Sub Crear_Planificacion()
Dim strinsert As String
'Dim strinsert07 As String
'Dim strRec As String
'Dim rstrec As rdoResultset
'Dim strinsert1000 As String
Dim strsql As String
Dim rstA As rdoResultset
'Dim strsql2 As String
'Dim rstA2 As rdoResultset
Dim strsql0300  As String
Dim rsta0300 As rdoResultset
Dim strsql0900  As String
Dim rsta0900 As rdoResultset
Dim fecha As String
Dim strsql2500 As String
Dim rstsql2500 As rdoResultset
Dim mintCodEstado As Integer
Dim rstasis As rdoResultset
Dim strasis As String
Dim rstdoc As rdoResultset
Dim strdoc As String

  strasis = "SELECT AD07CODPROCESO,AD01CODASISTENCI FROM PR0800 WHERE " & _
          "PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value & " AND " & _
          "PR08NUMSECUENCIA=1"
  Set rstasis = objApp.rdoConnect.OpenResultset(strasis)

  strsql0300 = "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
  Set rsta0300 = objApp.rdoConnect.OpenResultset(strsql0300)
  
  strdoc = "SELECT pr01indreqdoc FROM PR0100 WHERE PR01CODACTUACION=" & rsta0300.rdoColumns("PR01CODACTUACION").Value
  Set rstdoc = objApp.rdoConnect.OpenResultset(strdoc)
  
  strsql0900 = "SELECT TO_CHAR(PR09FECPLANIFIC, 'DD/MM/YYYY HH24:MI') FROM PR0900 WHERE PR09NUMPETICION=" & frmpedir.txtText1(0).Text
  Set rsta0900 = objApp.rdoConnect.OpenResultset(strsql0900)
  
  If IsNull(rsta0900.rdoColumns(0).Value) Then
    fecha = "NULL"
  Else
    If Left(Right(rsta0900.rdoColumns(0).Value, 3), 1) = ":" Then
      fecha = rsta0900.rdoColumns(0).Value
      fecha = "to_date('" & fecha & "','dd/mm/yyyy hh24:mi')"
    Else
      fecha = rsta0900.rdoColumns(0).Value
      fecha = "to_date('" & fecha & "','dd/mm/yyyy')"
    End If
  End If
  
  strsql2500 = "SELECT COUNT(*) FROM PR2500 WHERE PR01CODACTUACION=" & grdDBGrid1(0).Columns(2).Value
  Set rstsql2500 = objApp.rdoConnect.OpenResultset(strsql2500)
  If rstsql2500(0).Value = 0 Then
    mintCodEstado = 0
  Else
    mintCodEstado = 1
  End If
  
  strsql = "SELECT PR04NUMACTPLAN_SEQUENCE.nextval FROM dual"
  Set rstA = objApp.rdoConnect.OpenResultset(strsql)
  
  strinsert = "INSERT INTO PR0400 " & _
            "(pr04numactplan,pr01codactuacion,ad02coddpto," & _
            "ci21codpersona,ci32codtipecon,ci13codentidad,pr37codestado," & _
            "pr03numactpedi,pr04fecplanific,pr04fecentrcola,pr04feciniact," & _
            "pr04fecfinact,pr04feccancel,pr04desmotcan,pr04inddispfact," & _
            "pr04nummindur,pr04desinstrea,pr04indfacturab,pr04desinforme," & _
            "im01numdoc,pr04indpadre,pr56codestmues,ad01codasistenci,ad07codproceso,pr04indreqdoc)" & _
            " VALUES (" & rstA(0).Value & "," & grdDBGrid1(0).Columns(2).Value & "," & _
            grdDBGrid1(0).Columns(4).Value & "," & txtText1(0).Text & "," & "'" & rsta0300.rdoColumns("CI32CODTIPECON").Value & "'" & "," & "'" & rsta0300.rdoColumns("CI13CODENTIDAD").Value & "'" & ",1," & _
             grdDBGrid1(0).Columns(6).Value & "," & _
             fecha & ",null,null,null,null,null,null,null,null,null,null,null,null,"
    If mintCodEstado = 1 Then
      strinsert = strinsert & "1,"
    Else
      strinsert = strinsert & "null,"
    End If
    If IsNull(rstasis.rdoColumns("ad01codasistenci").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad01codasistenci").Value & ","
    End If
    If IsNull(rstasis.rdoColumns("ad07codproceso").Value) Then
       strinsert = strinsert & "null,"
    Else
       strinsert = strinsert & rstasis.rdoColumns("ad07codproceso").Value & ","
    End If
    If IsNull(rstdoc.rdoColumns("pr01indreqdoc").Value) Then
       strinsert = strinsert & "null)"
    Else
       strinsert = strinsert & rstdoc.rdoColumns("pr01indreqdoc").Value & ")"
    End If
    
    objApp.rdoConnect.Execute strinsert, 64
    
    strinsert = "UPDATE PR0300 SET PR03FECPREFEREN=" & fecha & " WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
    objApp.rdoConnect.Execute strinsert, 64
    
    '**************************************************************************jcr7/8/98
    'Esta estructura se crea al realizar
    '************************************************************************************
    
    'strsql2 = "SELECT * FROM PR0600 WHERE pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
    'Set rstA2 = objApp.rdoConnect.OpenResultset(strsql2)
    '
    'While Not rstA2.EOF
    'strinsert07 = "INSERT INTO PR0700 " & _
    '                 "(pr04numactplan,pr07numfase,pr07desfase,pr07numminocupac," & _
    '                "pr07numfase_pre,pr07numminfpre,pr07nummaxfpre," & _
    '                "pr07indhabnatu,pr07indinifin) " & _
    '                " VALUES (" & rsta(0).Value & "," & rstA2.rdoColumns("pr06numfase").Value & "," & "'" & rstA2.rdoColumns("pr06desfase").Value & "'" & ","
   '
    'If IsNull(rstA2.rdoColumns("pr06numminocupac").Value) Then
    '  strinsert07 = strinsert07 & "null" & ","
    'Else
    '  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminocupac").Value & ","
    'End If
    'If IsNull(rstA2.rdoColumns("pr06numfase_pre").Value) Then
    '  strinsert07 = strinsert07 & "null" & ","
    'Else
    '  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numfase_pre").Value & ","
    'End If
    'If IsNull(rstA2.rdoColumns("pr06numminfpre").Value) Then
    '  strinsert07 = strinsert07 & "null" & ","
    'Else
    '  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06numminfpre").Value & ","
    'End If
    'If IsNull(rstA2.rdoColumns("pr06nummaxfpre").Value) Then
    '  strinsert07 = strinsert07 & "null" & ","
    'Else
    '  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06nummaxfpre").Value & ","
    'End If
    'If IsNull(rstA2.rdoColumns("pr06indhabnatu").Value) Then
    '  strinsert07 = strinsert07 & "null" & ","
    'Else
    '  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indhabnatu").Value & ","
    'End If
    'If IsNull(rstA2.rdoColumns("pr06indinifin").Value) Then
    '  strinsert07 = strinsert07 & "null" & ")"
    'Else
    '  strinsert07 = strinsert07 & rstA2.rdoColumns("pr06indinifin").Value & ")"
    'End If
    'objApp.rdoConnect.Execute strinsert07, 64
    'rstA2.MoveNext
   'Wend
  ''insertar en pr1000 RECURSO CONSUMIDO
  'strRec = "SELECT * FROM PR1400 WHERE pr03numactpedi=" & grdDBGrid1(0).Columns(6).Value
  'Set rstrec = objApp.rdoConnect.OpenResultset(strRec)
  'While Not rstrec.EOF
  'strinsert1000 = "INSERT INTO PR1000 " & _
  '              "(pr04numactplan,pr07numfase,pr10numnecesid,ag11codrecurso," & _
  '              "pr10numunirec,pr10numminocurec,ag11codrecurso_pre)" & _
  '              "VALUES (" & rsta(0).Value & "," & _
  '              rstrec.rdoColumns("pr06numfase").Value & "," & _
  '              rstrec.rdoColumns("pr14numnecesid").Value & ","
  'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ","
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ","
  'End If
  'If IsNull(rstrec.rdoColumns("pr14numunirec").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ","
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numunirec").Value & ","
  'End If
  'If IsNull(rstrec.rdoColumns("pr14numminocu").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ","
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("pr14numminocu").Value & ","
  'End If
  'If IsNull(rstrec.rdoColumns("AG11CODRECURSO").Value) Then
  '  strinsert1000 = strinsert1000 & "null" & ")"
  'Else
  ' strinsert1000 = strinsert1000 & rstrec.rdoColumns("AG11CODRECURSO").Value & ")"
  'End If
  'objApp.rdoConnect.Execute strinsert1000, 64
  'rstrec.MoveNext
  'Wend
  rstsql2500.Close
  rstA.Close
  'rstrec.Close
  'rstA2.Close
  rsta0300.Close
  Set rstsql2500 = Nothing
  Set rstA = Nothing
  'Set rstrec = Nothing
  'Set rstA2 = Nothing
  Set rsta0300 = Nothing
  rstasis.Close
  Set rstasis = Nothing
  rstdoc.Close
  Set rstdoc = Nothing
  
End Sub
Private Sub Borrar_Planificacion()
  Dim strdelete As String
  
  Call Borrar_Muestras_Pedidas
  
  'strdelete = "DELETE PR1000 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value & ")"
  'objApp.rdoConnect.Execute strdelete, 64
  
  'strdelete = "DELETE PR0700 WHERE PR04NUMACTPLAN IN (SELECT PR04NUMACTPLAN FROM PR0400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value & ")"
  'objApp.rdoConnect.Execute strdelete, 64
  
  strdelete = "DELETE PR0400 WHERE PR03NUMACTPEDI=" & grdDBGrid1(0).Columns(6).Value
  objApp.rdoConnect.Execute strdelete, 64
  
End Sub


