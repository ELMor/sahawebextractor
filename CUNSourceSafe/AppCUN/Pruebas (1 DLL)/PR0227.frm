VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmMuesLabPend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Muestras Pedientes de Extracci�n"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Muestras Pendientes de Extracci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7320
      Index           =   0
      Left            =   480
      TabIndex        =   1
      Top             =   600
      Width           =   11055
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6825
         Index           =   0
         Left            =   135
         TabIndex        =   2
         Top             =   360
         Width           =   10680
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   18838
         _ExtentY        =   12039
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMuesLabPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMultiInfo As New clsCWForm
  
  Dim strKey As String
  
  Call Seleccionar_Dpto
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .strName = "Extracciones Pendientes"
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    .strTable = "PR5202J"

    .intAllowance = cwAllowReadOnly
  
    Call .objPrinter.Add("PR2271", "Listado Extracciones Pendientes")
    
    Call .FormAddOrderField("PR04NUMACTPLAN", cwAscending)
    
    .strWhere = "PR24MODO<>2 AND AD02CODDPTO=" & glngdptologin
    
    .intCursorSize = 0
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Muestras Paciente")
    
    Call .FormAddFilterWhere(strKey, "PR24CODMUESTRA", "C�d. Muestra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR24DESCORTA", "Descripci�n Corta", cwString, 30)
    Call .FormAddFilterWhere(strKey, "PR24CAPACIDADTUBO", "Capacidad Tubo", cwNumeric, 9)
    Call .FormAddFilterWhere(strKey, "PR24MODO", "Modo", cwNumeric, 1)
    Call .FormAddFilterWhere(strKey, "PR25TIEMPO", "Tiempo respecto primera", cwNumeric, 5)
    Call .FormAddFilterWhere(strKey, "PR25VOLUMENMIN", "Volumen m�nimo", cwNumeric, 9)
    Call .FormAddFilterWhere(strKey, "PR25VOLUMENPREF", "Volumen preferente", cwNumeric, 9)
    Call .FormAddFilterWhere(strKey, "PR25INDHORA", "Indicativo hora exacta", cwNumeric, 1)
    Call .FormAddFilterWhere(strKey, "PR25ETIQADICIONAL", "N�mero de etiquetas adicionales", cwNumeric, 2)
    Call .FormAddFilterWhere(strKey, "PR25INDISTINTO", "Grupos de muestras", cwString, 6)
    Call .FormAddFilterWhere(strKey, "PR25COMENTINFORMA", "Comentario pantalla", cwString, 255)
    Call .FormAddFilterWhere(strKey, "PR25GRUPOMUESTRA", "Grupos entubables", cwString, 12)
    Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "C�digo Persona", cwNumeric, 7)
    Call .FormAddFilterWhere(strKey, "CI22NUMHISTORIA", "N�mero de Historia", cwNumeric, 7)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString, 25)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString, 25)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString, 25)
    Call .FormAddFilterWhere(strKey, "PR04NUMACTPLAN", "N�mero de actuaci�n planificada", cwNumeric, 9)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric, 9)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Descripci�n corta de la Actuaci�n", cwString, 30)
    Call .FormAddFilterWhere(strKey, "PR04FECPLANIFIC", "Fecha planificada", cwDate)
    Call .FormAddFilterWhere(strKey, "hora", "Hora", cwString, 75)
    
    Call .FormAddFilterOrder(strKey, "PR24CODMUESTRA", "C�d. Muestra")
    Call .FormAddFilterOrder(strKey, "PR24DESCORTA", "Descripci�n Corta")
    Call .FormAddFilterOrder(strKey, "PR24CAPACIDADTUBO", "Capacidad Tubo")
    Call .FormAddFilterOrder(strKey, "PR24MODO", "Modo")
    Call .FormAddFilterOrder(strKey, "PR25TIEMPO", "Tiempo respecto primera")
    Call .FormAddFilterOrder(strKey, "PR25VOLUMENMIN", "Volumen m�nimo")
    Call .FormAddFilterOrder(strKey, "PR25VOLUMENPREF", "Volumen preferente")
    Call .FormAddFilterOrder(strKey, "PR25INDHORA", "Indicativo hora exacta")
    Call .FormAddFilterOrder(strKey, "PR25ETIQADICIONAL", "N�mero de etiquetas adicionales")
    Call .FormAddFilterOrder(strKey, "PR25INDISTINTO", "Grupos de muestras")
    Call .FormAddFilterOrder(strKey, "PR25COMENTINFORMA", "Comentario pantalla")
    Call .FormAddFilterOrder(strKey, "PR25GRUPOMUESTRA", "Grupos entubables")
    Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "C�digo Persona")
    Call .FormAddFilterOrder(strKey, "CI22NUMHISTORIA", "N�mero de Historia")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR04NUMACTPLAN", "N�mero de actuaci�n planificada")
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Descripci�n corta de la Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR04FECPLANIFIC", "Fecha planificada")

  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d. Muestra", "PR24CODMUESTRA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Descripci�n Corta", "PR24DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Capacidad Tubo", "PR24CAPACIDADTUBO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Modo", "PR24MODO", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "Tiempo respecto primera", "PR25TIEMPO", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Volumen m�nimo", "PR25VOLUMENMIN", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Volumen preferente", "PR25VOLUMENPREF", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Indicativo hora exacta", "PR25INDHORA", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "N�mero de etiquetas adicionales", "PR25ETIQADICIONAL", cwNumeric, 2)
    Call .GridAddColumn(objMultiInfo, "Grupos de muestras", "PR25INDISTINTO", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Comentario pantalla", "PR25COMENTINFORMA", cwString, 255)
    Call .GridAddColumn(objMultiInfo, "Grupos entubables", "PR25GRUPOMUESTRA", cwString, 12)
    Call .GridAddColumn(objMultiInfo, "C�digo Persona", "CI21CODPERSONA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "N�mero de Historia", "CI22NUMHISTORIA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "N�mero de actuaci�n planificada", "PR04NUMACTPLAN", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�digo Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Descripci�n corta de la Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Fecha planificada", "PR04FECPLANIFIC", cwDate)
    
    Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1


    Call .FormChangeColor(objMultiInfo)
    
  
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(10)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(11)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(12)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(13)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(14)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(15)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(16)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(17)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(18)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(19)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(20)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(21)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(22)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(23)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With

grdDBGrid1(0).Columns(3).Width = 6 * 200 '"PR24CODMUESTRA"
grdDBGrid1(0).Columns(4).Width = 15 * 200 '"PR24DESCORTA"
grdDBGrid1(0).Columns(5).Width = 9 * 200 '"PR24CAPACIDADTUBO"
grdDBGrid1(0).Columns(7).Width = 10 * 200 '"PR25TIEMPO"
grdDBGrid1(0).Columns(8).Width = 9 * 200 '"PR25VOLUMENMIN"
grdDBGrid1(0).Columns(9).Width = 9 * 200 '"PR25VOLUMENPREF"
grdDBGrid1(0).Columns(10).Width = 9 * 200 '"PR25INDHORA"
grdDBGrid1(0).Columns(11).Width = 13 * 200 '"PR25ETIQADICIONAL"
grdDBGrid1(0).Columns(12).Width = 8 * 200 '"PR25INDISTINTO"
grdDBGrid1(0).Columns(13).Width = 20 * 200 '"PR25COMENTINFORMA"
grdDBGrid1(0).Columns(14).Width = 12 * 200 '"PR25GRUPOMUESTRA"
grdDBGrid1(0).Columns(15).Width = 7 * 200 '"CI21CODPERSONA"
grdDBGrid1(0).Columns(16).Width = 8 * 200 '"CI22NUMHISTORIA"
grdDBGrid1(0).Columns(17).Width = 15 * 200 '"CI22NOMBRE"
grdDBGrid1(0).Columns(18).Width = 15 * 200 '"CI22PRIAPEL"
grdDBGrid1(0).Columns(19).Width = 15 * 200 '"CI22SEGAPEL"
grdDBGrid1(0).Columns(20).Width = 14 * 200 '"PR04NUMACTPLAN"
grdDBGrid1(0).Columns(21).Width = 9 * 200 '"PR01CODACTUACION"

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Extracciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


