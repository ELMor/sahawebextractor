VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmintergrpgrp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Interacciones. Grupo - Grupo"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdinternogen 
      BackColor       =   &H000000FF&
      Caption         =   "Ver Interacciones no Generadas"
      Height          =   375
      Left            =   4800
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   7560
      Width           =   2450
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Interacci�n Grupo - Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6495
      Index           =   0
      Left            =   600
      TabIndex        =   0
      Top             =   840
      Width           =   10605
      Begin TabDlg.SSTab tabTab1 
         Height          =   5820
         Index           =   0
         Left            =   240
         TabIndex        =   12
         Top             =   480
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   10266
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0118.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(6)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(5)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(9)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(10)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "cboSSDBCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(2)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(3)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(4)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(5)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(6)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(8)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtdia"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txthora"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtminuto"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0118.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtminuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   3120
            MaxLength       =   2
            TabIndex        =   7
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txthora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   1680
            MaxLength       =   2
            TabIndex        =   6
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txtdia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   360
            MaxLength       =   4
            TabIndex        =   5
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   8
            Left            =   3960
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "Descripci�n Grupo Origen"
            Top             =   840
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   3120
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Descripci�n Grupo Destino"
            Top             =   3120
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   7
            Left            =   3120
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Descripci�n Grupo Origen"
            Top             =   1920
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR18NUMTIEMPOINT"
            Height          =   330
            Index           =   5
            Left            =   6960
            TabIndex        =   9
            Tag             =   "Tiempo|Tiempo de la Interacci�n"
            Top             =   4680
            Visible         =   0   'False
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR01CODACTUACION_DES"
            Height          =   330
            Index           =   4
            Left            =   7440
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "C�d.Act_Dest.|C�digo de la Actuaci�n Destino"
            Top             =   4200
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   3
            Left            =   7440
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "C�d.Act_Orig.|C�digo de la Actuaci�n Origen"
            Top             =   3720
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR16CODGRUPO_DES"
            Height          =   330
            Index           =   2
            Left            =   360
            TabIndex        =   4
            Tag             =   "C�d.Grp_Dest.|C�digo del Grupo Destino"
            Top             =   3120
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR16CODGRUPO"
            Height          =   330
            Index           =   1
            Left            =   360
            TabIndex        =   3
            Tag             =   "C�d.Grp_Orig.|C�digo del Grupo Origen"
            Top             =   1920
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR18NUMDEFINTER"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   1
            Tag             =   "N�mero|N�mero de Interacci�n"
            Top             =   840
            Width           =   852
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5265
            Index           =   0
            Left            =   -74760
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   240
            Width           =   9375
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowHeight       =   423
            Columns(0).Width=   3200
            UseDefaults     =   0   'False
            _ExtentX        =   16536
            _ExtentY        =   9287
            _StockProps     =   79
            Caption         =   "INTERACCIONES GRUPO - GRUPO"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR19CODTIPINTERAC"
            Height          =   330
            Index           =   0
            Left            =   3120
            TabIndex        =   2
            Tag             =   "Tipo|Tipo de Interacci�n"
            Top             =   840
            Width           =   750
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1402
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3519
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1323
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   3840
            TabIndex        =   29
            Top             =   4440
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   2400
            TabIndex        =   28
            Top             =   4440
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1080
            TabIndex        =   27
            Top             =   4440
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   8040
            TabIndex        =   25
            Top             =   4800
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Grupo Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   3120
            TabIndex        =   24
            Top             =   2880
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Grupo Origen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   3120
            TabIndex        =   23
            Top             =   1680
            Width           =   2205
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Grupo Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   360
            TabIndex        =   22
            Top             =   2880
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Grupo Origen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   21
            Top             =   1680
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   360
            TabIndex        =   20
            Top             =   4080
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo Interacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   3120
            TabIndex        =   19
            Top             =   600
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�mero de Interacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   18
            Top             =   600
            Width           =   2055
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmintergrpgrp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS C.U.N.                                             *
'* NOMBRE: PR00118.FRM                                                  *
'* AUTOR: JAVIER OSTOLAZA LASA                                          *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: permite definir interacciones entre dos grupos          *
'*              diferentes                                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
' tipo,tiempo,origen,destino guardan los valores de los campos de la
' pantalla para hacer las comprobaciones cuando se hacen cambios en esos
' campos y hay que guardar las tuplas en PR2000
Dim tipo As Variant
Dim tiempo As Variant
Dim origen As Variant
Dim destino As Variant
' intabrir indica si se ha pulsado Abrir Registro
Dim intabrir As Variant
' intnuevo indica si se ha pulsado Nuevo
Dim intnuevo As Variant
' strinternogen guarda las interacciones ya guardadas
Dim strinternogen As Variant
' nogenerada indica si hay alguna interacci�n que ya estaba generada
Dim nogenerada As Integer
Dim TiempoTotal As Variant
Dim intcambioalgo As Integer


Private Function LlenarBlancos(strPal As String, intLon As Integer)
' funci�n para rellenar con blancos los campos de la interacci�n
' ya generada y que se mostrar� en una lista
Dim strpal1 As String
    
    strpal1 = strPal
    While Len(strPal) < intLon
       strpal1 = "  " & strpal1
       intLon = intLon - 1
    Wend
    LlenarBlancos = strpal1
End Function


Private Sub LlenarTabla(ByVal act As Long, ByVal actdest As Long)
' procedimiento que va llenando un string con las interacciones que
' ya estaban generadas

Dim sqlstr As String
Dim rsta As rdoResultset
Dim intinter As Long
Dim inttipo As Long
Dim inttiempo As Long

 sqlstr = "SELECT pr18numdefinter,pr19codtipinterac,pr20numtiempoint " _
         & "FROM pr2000 " _
         & "WHERE pr01codactuacion = " & act _
         & " AND pr01codactuacion_des=" & actdest
  On Error GoTo Err_Ejecutar
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  intinter = rsta.rdoColumns(0).Value
  inttipo = rsta.rdoColumns(1).Value
  inttiempo = rsta.rdoColumns(2).Value
  strinternogen = strinternogen & "*" & LlenarBlancos(Str(intinter), 15) _
              & LlenarBlancos(Str(act), 15) _
              & LlenarBlancos(Str(actdest), 15) _
              & LlenarBlancos(Str(inttipo), 15) _
              & LlenarBlancos(Str(inttiempo), 15) & "    "
  rsta.Close
  Set rsta = Nothing
  Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub pasaGRUPOxGRUPO(ByVal SQL1 As String, ByVal SQL2 As String, ByVal SQL3 As String)
  Dim sqlstr As String
  Dim act As Long
  Dim actdest As Long
  Dim rsta As rdoResultset
  Dim rstB As rdoResultset
  Dim sqlstr1 As String
  Dim rstA1 As rdoResultset
  
  
  On Error GoTo Err_Ejecutar
  'actuaciones del grupo origen
  sqlstr = "SELECT pr01codactuacion " _
         & "FROM pr1700 " _
         & "WHERE pr16codgrupo = " & txtText1(1)
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  'actuaciones del grupo destino
  sqlstr = "SELECT pr01codactuacion " _
         & "FROM pr1700 " _
         & "WHERE pr16codgrupo = " & txtText1(2)
  Set rstB = objApp.rdoConnect.OpenResultset(sqlstr, rdOpenKeyset, rdConcurReadOnly, rdExecDirect)
  'combinaci�n
  With rsta
    Do Until .EOF
      act = .rdoColumns(0).Value
      With rstB
        Do Until .EOF
          actdest = .rdoColumns(0).Value
          If act <> actdest Then
            ' Se controla que no se guarde una interacci�n entre dos actuaciones
            ' entre las que ya existe una interacci�n
            
              sqlstr1 = "SELECT * " _
                  & "FROM PR2000 " _
                  & "WHERE pr01codactuacion=" & act _
                  & " AND pr01codactuacion_des=" & actdest
             On Error GoTo Err_Ejecutar
             Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
             ' ya existe interacci�n entre las dos actuaciones
             If Not rstA1.EOF Then
                  LlenarTabla act, actdest
                  nogenerada = 1
               objWinInfo.objWinActiveForm.blnChanged = False
             Else ' no hay interacci�n entre las actuaciones
                sqlstr = SQL1 & act & SQL2 & actdest & SQL3
                objApp.rdoConnect.Execute sqlstr, 64
             End If
             rstA1.Close
             Set rstA1 = Nothing
          End If
          .MoveNext
        Loop
        .MoveFirst
      End With
      .MoveNext
    Loop
  End With
  objApp.rdoConnect.Execute "Commit", 64
  
  rsta.Close
  Set rsta = Nothing
  rstB.Close
  Set rstB = Nothing
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub

Private Sub pasaGRUPOxGRUPO2()
  Dim sqlstr As String
  Dim act As Long
  Dim actdest As Long
  Dim rsta As rdoResultset
  Dim rstB As rdoResultset
  Dim sqlstrAA1 As String
  Dim rstAA1 As rdoResultset
  Dim sqlstr1 As String
  Dim rstAA As rdoResultset
  Dim sqlstrAA As String
  Dim sqlstr2 As String
  Dim sqlstr4 As String
  
  ' si cambia alg�n campo se borran las tuplas
  If (txtText1(1) <> origen Or txtText1(2) <> destino Or _
      txtText1(5) <> tiempo Or cboSSDBCombo1(0) <> tipo) Then
             sqlstr4 = "DELETE FROM PR2000 " _
                    & "where pr18numdefinter=" & txtText1(0)
             On Error GoTo Err_Ejecutar
             objApp.rdoConnect.Execute sqlstr4, 64
  End If
  
  'actuaciones del grupo origen
  sqlstr = "SELECT pr01codactuacion " _
         & "FROM pr1700 " _
         & "WHERE pr16codgrupo = " & txtText1(1)
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  'actuaciones del grupo destino
  sqlstr = "SELECT pr01codactuacion " _
         & "FROM pr1700 " _
         & "WHERE pr16codgrupo = " & txtText1(2)
  Set rstB = objApp.rdoConnect.OpenResultset(sqlstr, rdOpenKeyset, rdConcurReadOnly, rdExecDirect)
  'combinaci�n
  With rsta
    Do Until .EOF
      act = .rdoColumns(0).Value
      With rstB
        Do Until .EOF
          actdest = .rdoColumns(0).Value
          If (act <> actdest) Then
            ' se hacen cambios si los c�digos de grupo no cambian
            If (origen = txtText1(1) And destino = txtText1(2)) And _
               (tipo <> cboSSDBCombo1(0).Value Or tiempo <> txtText1(5)) Then
                    If (act <> actdest) Then
                      sqlstrAA = "SELECT * " _
                        & "FROM PR2000 " _
                        & "WHERE pr01codactuacion=" & act _
                        & " AND pr01codactuacion_des=" & actdest
                        On Error GoTo Err_Ejecutar
                        Set rstAA = objApp.rdoConnect.OpenResultset(sqlstrAA)
                        ' ya existe interacci�n entre las dos actuaciones
                        If Not rstAA.EOF Then
                          LlenarTabla act, actdest
                          nogenerada = 1
                          objWinInfo.objWinActiveForm.blnChanged = False
                        Else ' no hay interacci�n entre las actuaciones
                          sqlstr4 = "INSERT INTO PR2000 " _
                           & "(PR01CODACTUACION,PR01CODACTUACION_DES,PR19CODTIPINTERAC," _
                           & "PR20NUMTIEMPOINT,PR18NUMDEFINTER) " _
                           & "VALUES (" _
                           & act & "," _
                           & actdest & "," _
                           & cboSSDBCombo1(0).Text & "," _
                           & txtText1(5).Text & "," _
                           & txtText1(0).Text & ")"
                           On Error GoTo Err_Ejecutar
                           objApp.rdoConnect.Execute sqlstr4, 64
                        End If
                        rstAA.Close
                        Set rstAA = Nothing
                      End If
            Else
            ' ha cambiado el grupo origen y no el destino
            If (origen <> txtText1(1) And destino = txtText1(2)) Then
             sqlstrAA1 = "SELECT pr01codactuacion " _
                     & "FROM pr1700 " _
                     & "WHERE pr16codgrupo = " & txtText1(1)
             Set rstAA1 = objApp.rdoConnect.OpenResultset(sqlstrAA1)
             With rstAA1
              Do Until .EOF
                  act = .rdoColumns(0).Value
                  If (act <> actdest) Then
                  sqlstrAA = "SELECT * " _
                    & "FROM PR2000 " _
                    & "WHERE pr01codactuacion=" & act _
                    & " AND pr01codactuacion_des=" & actdest
                  On Error GoTo Err_Ejecutar
                  Set rstAA = objApp.rdoConnect.OpenResultset(sqlstrAA)
                   ' ya existe interacci�n entre las dos actuaciones
                  If Not rstAA.EOF Then
                        LlenarTabla act, actdest
                        nogenerada = 1
                        objWinInfo.objWinActiveForm.blnChanged = False
                  Else ' no hay interacci�n entre las actuaciones
                         sqlstr4 = "INSERT INTO PR2000 " _
                           & "(PR01CODACTUACION,PR01CODACTUACION_DES,PR19CODTIPINTERAC," _
                           & "PR20NUMTIEMPOINT,PR18NUMDEFINTER) " _
                           & "VALUES (" _
                           & act & "," _
                           & actdest & "," _
                           & cboSSDBCombo1(0).Text & "," _
                           & txtText1(5).Text & "," _
                           & txtText1(0).Text & ")"
                        On Error GoTo Err_Ejecutar
                        objApp.rdoConnect.Execute sqlstr4, 64
                  End If
                  rstAA.Close
                  Set rstAA = Nothing
                  End If
                  .MoveNext
            Loop
            End With
            rstAA1.Close
            Set rstAA1 = Nothing
            Exit Do
            Exit Do
           End If
           ' ha cambiado el grupo destino o los dos(origen y destino)
            If (destino <> txtText1(2)) Then
         
             sqlstrAA1 = "SELECT pr01codactuacion " _
                     & "FROM pr1700 " _
                     & "WHERE pr16codgrupo = " & txtText1(2)
             Set rstAA1 = objApp.rdoConnect.OpenResultset(sqlstrAA1)
             With rstAA1
              Do Until .EOF
                  actdest = .rdoColumns(0).Value
                  If (act <> actdest) Then
                  sqlstrAA = "SELECT * " _
                    & "FROM PR2000 " _
                    & "WHERE pr01codactuacion=" & act _
                    & " AND pr01codactuacion_des=" & actdest
                  On Error GoTo Err_Ejecutar
                  Set rstAA = objApp.rdoConnect.OpenResultset(sqlstrAA)
                   ' ya existe interacci�n entre las dos actuaciones
                  If Not rstAA.EOF Then
                        LlenarTabla act, actdest
                        nogenerada = 1
                        objWinInfo.objWinActiveForm.blnChanged = False
                  Else ' no hay interacci�n entre las actuaciones
                         sqlstr4 = "INSERT INTO PR2000 " _
                           & "(PR01CODACTUACION,PR01CODACTUACION_DES,PR19CODTIPINTERAC," _
                           & "PR20NUMTIEMPOINT,PR18NUMDEFINTER) " _
                           & "VALUES (" _
                           & act & "," _
                           & actdest & "," _
                           & cboSSDBCombo1(0).Text & "," _
                           & txtText1(5).Text & "," _
                           & txtText1(0).Text & ")"
                        On Error GoTo Err_Ejecutar
                        objApp.rdoConnect.Execute sqlstr4, 64
                  End If
                  rstAA.Close
                  Set rstAA = Nothing
                  End If
                  .MoveNext
            Loop
            End With
            rstAA1.Close
            Set rstAA1 = Nothing
            Exit Do
            Exit Do
           End If
           
           
           End If
          End If
          
          rstB.MoveNext
        Loop
        rstB.MoveFirst
      End With
      
      rsta.MoveNext
    Loop
  End With
  objApp.rdoConnect.Execute "Commit", 64
  
  rsta.Close
  Set rsta = Nothing
  rstB.Close
  Set rstB = Nothing
  
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub





Private Sub cmdinternogen_Click()
     cmdinternogen.Enabled = False
     frminternogen.txtText1(0).Text = strinternogen
     'Call frminternogen.Show(vbModal)
     Call objsecurity.LaunchProcess("PR0130")
     cmdinternogen.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Activate()
  origen = txtText1(1)
  destino = txtText1(2)
  tiempo = txtText1(5)
  tipo = cboSSDBCombo1(0).Value
End Sub


Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Dim i As Integer
  
  'Call objApp.SplashOn
  
  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Interacci�n Grupo-Grupo"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR1800"
    .strWhere = "(PR16CODGRUPO IS NOT NULL) AND (PR16CODGRUPO_DES IS NOT NULL)"
    .blnMasive = False
    
    Call .FormAddOrderField("PR18NUMDEFINTER", cwAscending)
    
    .blnHasMaint = True
    
    Call .objPrinter.Add("PR1181", "Listado de Interacciones Grupo-Grupo")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Interacci�n Grupo-Grupo")
    Call .FormAddFilterWhere(strKey, "PR18NUMDEFINTER", "N�mero", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR16CODGRUPO", "Grupo Origen", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR16CODGRUPO_DES", "Grupo Destino", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR18NUMTIEMPOINT", "Tiempo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR19CODTIPINTERAC", "Tipo", cwNumeric)

    Call .FormAddFilterOrder(strKey, "PR18NUMDEFINTER", "N�mero")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
    'tratamiento especial para los campos de actuaci�n
    .CtrlGetInfo(txtText1(3)).blnInGrid = False
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(1)).blnMandatory = True
    .CtrlGetInfo(txtText1(2)).blnMandatory = True
    .CtrlGetInfo(txtText1(5)).blnMandatory = True
    .CtrlGetInfo(cboSSDBCombo1(0)).blnMandatory = True
    .CtrlGetInfo(txtText1(3)).blnMandatory = False
    .CtrlGetInfo(txtText1(4)).blnMandatory = False
    
    .CtrlGetInfo(txtdia).blnNegotiated = False
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    .CtrlGetInfo(txtText1(2)).blnForeign = True

    ' tipos de interacci�n
    .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR19CODTIPINTERAC, PR19DESTIPINTERAC FROM PR1900"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "PR16CODGRUPO", "SELECT * FROM PR1600 WHERE PR16CODGRUPO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(7), "PR16DESGRUPO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "PR16CODGRUPO", "SELECT * FROM PR1600 WHERE PR16CODGRUPO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(6), "PR16DESGRUPO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR19CODTIPINTERAC", "SELECT * FROM PR1900 WHERE PR19CODTIPINTERAC=?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtText1(8), "PR19DESTIPINTERAC")
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  'tratamiento especial para los campos de actuaci�n
  txtText1(3) = ""
  txtText1(4) = ""
  txtText1(3).Visible = False
  txtText1(4).Visible = False

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

  'NOTA: el objeto que contiene la base de datos abierta (el Database de VB)
  '      es aqu� el objeto objApp.rdoConnect

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'inserto los valores en la tabla PR2000
  Dim sqlstr1 As String
  Dim sqlstr2 As String
  Dim sqlstr3 As String
  
  intcambioalgo = 0
  
  If Not blnError Then
    
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then   'NUEVO
      'busco las actuaciones del grupo y grabo tantas entradas como actuaciones tenga
      sqlstr1 = "INSERT INTO PR2000 " _
                & "(PR01CODACTUACION,PR01CODACTUACION_DES,PR19CODTIPINTERAC," _
                & "PR20NUMTIEMPOINT,PR18NUMDEFINTER) " _
                & "VALUES ("
                'act
      sqlstr2 = ","
                'actdest
      sqlstr3 = "," & cboSSDBCombo1(0).Text & "," _
                 & txtText1(5).Text & "," _
                 & txtText1(0).Text & ")"
      pasaGRUPOxGRUPO sqlstr1, sqlstr2, sqlstr3
    
    ElseIf objWinInfo.intWinStatus = cwModeSingleEdit Then  'ACTUALIZAR
      'busco las actuaciones del grupo y actualizo tantas entradas como actuaciones tenga
      pasaGRUPOxGRUPO2
    End If
  End If
  If (nogenerada = 1) Then
     cmdinternogen.Visible = True
  End If

End Sub



Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, ByRef blnCancel As Boolean)
'borro las entradas de la tabla PR2000
  Dim sqlstr As String
  sqlstr = "DELETE FROM PR2000 " _
           & "WHERE (pr18numdefinter = " & txtText1(0) & ")"
  On Error GoTo Err_Ejecutar
  objApp.rdoConnect.Execute sqlstr, 64
  objApp.rdoConnect.Execute "Commit", 64
  Exit Sub
Err_Ejecutar:
  blnCancel = True
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Interacci�n Grupo-Grupo" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  If strFormName = "Interacci�n Grupo-Grupo" And strCtrl = "txtText1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR1600"
     .strOrder = "ORDER BY pr16codgrupo ASC"
         
     Set objField = .AddField("pr16codgrupo")
     objField.strSmallDesc = "C�digo del Grupo"
         
     Set objField = .AddField("pr16desgrupo")
     objField.strSmallDesc = "Descripci�n del Grupo"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(1)), .cllValues("pr16codgrupo"))
      Call objWinInfo.CtrlSet(txtText1(1), .cllValues("pr16codgrupo"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Interacci�n Grupo-Grupo" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR1600"
     .strOrder = "ORDER BY pr16codgrupo ASC"
         
     Set objField = .AddField("pr16codgrupo")
     objField.strSmallDesc = "C�digo del Grupo"
         
     Set objField = .AddField("pr16desgrupo")
     objField.strSmallDesc = "Descripci�n del Grupo"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("pr16codgrupo"))
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("pr16codgrupo"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim mensaje As String
Dim rsta As rdoResultset
Dim sqlstr As String

'*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" Then
  'se mira si la interacci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr16codgrupo=" & txtText1(1).Text & _
         " AND pr16codgrupo_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'*********************************************************************

cmdinternogen.Visible = False
strinternogen = ""
nogenerada = 0


If btnButton.Index = 3 Then
    intabrir = 1
End If
If btnButton.Index = 2 Then
    intnuevo = 1
End If
If (btnButton.Index = 4) And (intnuevo = 1) Then
    intnuevo = 0
    tipo = cboSSDBCombo1(0).Value
    tiempo = txtText1(5)
    origen = txtText1(1)
    destino = txtText1(2)
End If

  'bot�n GRABAR
  If btnButton.Index = 4 Then
    If (txtText1(1) <> "" And txtText1(2) <> "") And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Un grupo no puede interaccionar consigo mismo.", vbOKOnly + vbExclamation, _
        "Interacci�n Grupo - Grupo")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    rsta.Close
    Set rsta = Nothing
    Exit Sub
    End If
  
    '  Para la integridad referencial se debe comprobar que los c�digos de grupo
    'tecleados est�n en la tabla PR1600.
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(1) & " no existe. " _
           & "Elija otro, por favor.", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
             & "El grupo " & txtText1(2) & " no existe. " _
           & "Elija otro, por favor.", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
    
    ' Se controla que no se guarde una interacci�n entre un grupo y
    ' otro entre los que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR1800 " _
          & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe la interacci�n
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
            objWinInfo.objWinActiveForm.blnChanged = False
            rsta.Close
            Set rsta = Nothing
            Exit Sub
        End If
      End If
      Else 'la interacci�n est� guardada
       sqlstr = "SELECT pr19codtipinterac,pr18numtiempoint,pr16codgrupo," _
                & "pr16codgrupo_des FROM PR1800 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
        txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de grupo no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            rsta.Close
            Set rsta = Nothing
            If btnButton.Index = 30 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Exit Sub
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y  " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
                objWinInfo.objWinActiveForm.blnChanged = False
                rsta.Close
                Set rsta = Nothing
               Exit Sub
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
  
  If (btnButton.Index <> 6 And btnButton.Index <> 8 And btnButton.Index <> 4) Then
    objWinInfo.objWinActiveForm.blnChanged = False
    If intcambioalgo = 1 Then
      objWinInfo.objWinActiveForm.blnChanged = True
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  End If
  
  
  '****************************************
  If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  '******************************************
  If txtText1(0).Text <> "" Then
  'bot�n distinto de grabar
   If btnButton.Index <> 4 Then
    If (txtText1(1) <> "" And txtText1(2) <> "") And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Un grupo no puede interaccionar consigo mismo.", vbOKOnly + vbExclamation, _
        "Interacci�n Grupo - Grupo")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    End If
  
    '  Para la integridad referencial se debe comprobar que los c�digos de grupo
    'tecleados est�n en la tabla PR1600.
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(1) & " no existe. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
             & "El grupo " & txtText1(2) & " no existe. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    
    ' Se controla que no se guarde una interacci�n entre un grupo y
    ' otro entre los que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR1800 " _
          & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe la interacci�n
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
       sqlstr = "SELECT pr19codtipinterac,pr18numtiempoint,pr16codgrupo," _
                & "pr16codgrupo_des FROM PR1800 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
        txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de grupo no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            If btnButton.Index = 30 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y  " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
End If
'***************************************
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 30 Then
    Exit Sub
  End If
  
  'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If btnButton.Index = 16 Then
    Call txtText1_Change(5)
  End If
  
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  'bot�n NUEVO
  If btnButton.Index = 2 Then
    'ponemos autom�ticamente el c�digo (PK es sequence)
    On Error GoTo Err_Ejecutar
    sqlstr = "SELECT PR18NUMDEFINTER_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    txtText1(0).Locked = True
    ' se activa el bot�n Guardar
    txtText1(1).SetFocus
    txtText1(0).SetFocus
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
  End If
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim mensaje As String
  
  '*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" Then
  'se mira si la interacci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr16codgrupo=" & txtText1(1).Text & _
         " AND pr16codgrupo_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'*********************************************************************
  
  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
If intIndex = 20 Then
    intabrir = 1
 End If
 If intIndex = 10 Then
    intnuevo = 1
 End If
If (intIndex = 40) And (intnuevo = 1) Then
    intnuevo = 0
    tipo = cboSSDBCombo1(0).Value
    tiempo = txtText1(5)
    origen = txtText1(1)
    destino = txtText1(2)
End If
  
  'bot�n GRABAR
  If (intIndex = 40) Then
    If (txtText1(1) <> "" And txtText1(2) <> "") And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Un grupo no puede interaccionar consigo mismo.", vbOKOnly + vbExclamation, _
        "Interacci�n Grupo - Grupo")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    rsta.Close
    Set rsta = Nothing
    Exit Sub
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(1) & " no existe o est� en fase de definici�n. " _
           & "Elija otro, por favor. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(2) & " no existe o est� en fase de definici�n. " _
           & "Elija otro, por favor. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
    
     ' Se controla que no se guarde una interacci�n entre una actuaci�n y
    '  un grupo entre los que ya existe una interacci�n
    sqlstr = "SELECT * " _
             & "FROM PR1800 " _
             & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe la interacci�n
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
            objWinInfo.objWinActiveForm.blnChanged = False
            rsta.Close
            Set rsta = Nothing
            Exit Sub
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr18numtiempoint,pr16codgrupo," _
                & "pr16codgrupo_des FROM PR1800 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de grupo no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
            rsta.Close
            Set rsta = Nothing
            If intIndex = 100 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Exit Sub
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y el grupo " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
                objWinInfo.objWinActiveForm.blnChanged = False
                rsta.Close
                Set rsta = Nothing
               Exit Sub
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
      
   ' control si la opci�n es distinta de Eliminar y De Imprimir
  If (intIndex <> 40 And intIndex <> 60 And intIndex <> 80) Then
     objWinInfo.objWinActiveForm.blnChanged = False
     If intcambioalgo = 1 Then
      objWinInfo.objWinActiveForm.blnChanged = True
    End If
      If (txtText1(0) <> "") Then
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  End If
  End If

  '****************************************
  If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  '******************************************
  If txtText1(0).Text <> "" Then
  'bot�n distinto de grabar
  If (intIndex <> 40) Then
    If (txtText1(1) <> "" And txtText1(2) <> "") And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Un grupo no puede interaccionar consigo mismo.", vbOKOnly + vbExclamation, _
        "Interacci�n Grupo - Grupo")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(1) & " no existe o est� en fase de definici�n. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(2) & " no existe o est� en fase de definici�n. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    
     ' Se controla que no se guarde una interacci�n entre una actuaci�n y
    '  un grupo entre los que ya existe una interacci�n
    sqlstr = "SELECT * " _
             & "FROM PR1800 " _
             & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe la interacci�n
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr18numtiempoint,pr16codgrupo," _
                & "pr16codgrupo_des FROM PR1800 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de grupo no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
            If intIndex = 100 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y el grupo " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
End If
'**************************************
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  If intIndex = 100 Then
      Exit Sub
  End If
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  'NUEVO
  If (intIndex = 10) Then
    'ponemos autom�ticamente el c�digo (PK es sequence)
    On Error GoTo Err_Ejecutar
    sqlstr = "SELECT PR18NUMDEFINTER_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    txtText1(0).Locked = True
    ' se activa el bot�n Guardar
    txtText1(1).SetFocus
    txtText1(0).SetFocus
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
  End If
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim mensaje As String
'*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" Then
  'se mira si la interacci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr16codgrupo=" & txtText1(1).Text & _
         " AND pr16codgrupo_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'*********************************************************************
  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  
  ' control al pulsar Filtro
  If (intIndex = 10) Then
  objWinInfo.objWinActiveForm.blnChanged = False
    '  Para la integridad referencial se debe comprobar que los c�digos de grupo
    'tecleados est�n en la tabla PR1600.
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  End If
  
    '****************************************
  If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  '****************************************************
    If txtText1(0).Text <> "" Then
    If (txtText1(1) <> "" And txtText1(2) <> "") And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Un grupo no puede interaccionar consigo mismo.", vbOKOnly + vbExclamation, _
        "Interacci�n Grupo - Grupo")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(1) & " no existe o est� en fase de definici�n. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(2) & " no existe o est� en fase de definici�n. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    
     ' Se controla que no se guarde una interacci�n entre una actuaci�n y
    '  un grupo entre los que ya existe una interacci�n
    sqlstr = "SELECT * " _
             & "FROM PR1800 " _
             & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe la interacci�n
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr18numtiempoint,pr16codgrupo," _
                & "pr16codgrupo_des FROM PR1800 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de grupo no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y el grupo " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If

  '*************************************************
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim mensaje As String
  
   '*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" Then
  'se mira si la interacci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr16codgrupo=" & txtText1(1).Text & _
         " AND pr16codgrupo_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If
'*********************************************************************
  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  
   objWinInfo.objWinActiveForm.blnChanged = False
    '  Para la integridad referencial se debe comprobar que los c�digos de grupo
    'tecleados est�n en la tabla PR1600.
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  
  '****************************************
  If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
'**************************************************
  If txtText1(0).Text <> "" Then
    If (txtText1(1) <> "" And txtText1(2) <> "") And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Un grupo no puede interaccionar consigo mismo.", vbOKOnly + vbExclamation, _
        "Interacci�n Grupo - Grupo")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(1) & " no existe o est� en fase de definici�n. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR1600 " _
           & "WHERE pr16codgrupo = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'grupo destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "El grupo " & txtText1(2) & " no existe o est� en fase de definici�n. ", vbCritical, _
           "Interacci�n Grupo - Grupo")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    
     ' Se controla que no se guarde una interacci�n entre una actuaci�n y
    '  un grupo entre los que ya existe una interacci�n
    sqlstr = "SELECT * " _
             & "FROM PR1800 " _
             & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe la interacci�n
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr18numtiempoint,pr16codgrupo," _
                & "pr16codgrupo_des FROM PR1800 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de grupo no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR1800 " _
               & "WHERE pr16codgrupo=" & txtText1(1) _
               & " AND pr16codgrupo_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre los grupos " & txtText1(1) _
                 & " y el grupo " & txtText1(2), vbCritical, _
                 "Interacci�n Grupo - Grupo")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If

  '*******************************************+
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  
  'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If intIndex = 10 Then
    Call txtText1_Change(5)
  End If
 
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)

  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  cmdinternogen.Visible = False
  strinternogen = ""
  nogenerada = 0
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtdia_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja d�a vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia.Text) = False Then
    Beep
    txtdia.Text = ""
    txtdia.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 6500 Then
      txtdia.Text = 6500
     End If
  End If
End Sub

Private Sub txtdia_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja hora vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora.Text) = False Then
    Beep
    txthora.Text = ""
    txthora.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja minuto vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto.Text) = False Then
    Beep
    txtminuto.Text = ""
    txtminuto.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub

Private Sub txtminuto_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 Then
    intcambioalgo = 0
  End If
  
  '****************************************************************************
  'cuando cambie la columna invisible Tiempo que se actualice dias,horas,minutos
  If intIndex = 5 Then
    If txtText1(5).Text = "" Then
      txtdia.Text = 0
      txthora.Text = 0
      txtminuto.Text = 0
    Else
      txtdia.Text = txtText1(5).Text \ 1440                 'd�as
      txthora.Text = (txtText1(5).Text Mod 1440) \ 60       'horas
      txtminuto.Text = (txtText1(5).Text Mod 1440) Mod 60   'minutos
    End If
  End If
  '****************************************************************************
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  intcambioalgo = 1
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 6500 Then
    d = 6500
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If

TiempoTotal = (d * 1440) + (h * 60) + m
  
End Sub



