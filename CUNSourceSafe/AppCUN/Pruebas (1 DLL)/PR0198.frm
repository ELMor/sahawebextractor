VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmcuestionario3 
   BackColor       =   &H00FFC0C0&
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Cuestionario de los Tipos de Recurso asociados a la Actuaci�n"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFrame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Cuestionario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6255
      Index           =   1
      Left            =   240
      TabIndex        =   4
      Top             =   1560
      Width           =   11295
      Begin VB.TextBox txtcodigo 
         BackColor       =   &H00FFFF00&
         Height          =   315
         Index           =   5
         Left            =   1800
         MaxLength       =   20
         TabIndex        =   31
         Top             =   2400
         Visible         =   0   'False
         Width           =   2040
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00FFFF00&
         Height          =   315
         Index           =   5
         Left            =   360
         MaxLength       =   20
         TabIndex        =   30
         Top             =   3240
         Visible         =   0   'False
         Width           =   5400
      End
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "Arial Black"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   5
         Left            =   6360
         TabIndex        =   29
         Top             =   3240
         Visible         =   0   'False
         Width           =   375
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcfechadesde 
         Height          =   375
         Left            =   8640
         TabIndex        =   27
         Top             =   3720
         Visible         =   0   'False
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcfecha 
         Height          =   315
         Index           =   5
         Left            =   2760
         TabIndex        =   21
         Top             =   840
         Visible         =   0   'False
         Width           =   1815
         _Version        =   65537
         _ExtentX        =   3201
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcfechahasta 
         Height          =   375
         Left            =   8640
         TabIndex        =   28
         Top             =   4200
         Visible         =   0   'False
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   661
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   375
         Index           =   4
         Left            =   8520
         TabIndex        =   26
         Top             =   3120
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   375
         Index           =   3
         Left            =   8520
         TabIndex        =   25
         Top             =   2400
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   375
         Index           =   2
         Left            =   8520
         TabIndex        =   24
         Top             =   1800
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   375
         Index           =   1
         Left            =   8640
         TabIndex        =   23
         Top             =   1200
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   375
         Index           =   0
         Left            =   8760
         TabIndex        =   22
         Top             =   720
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   840
         TabIndex        =   19
         Top             =   570
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   840
         TabIndex        =   18
         Top             =   1725
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   840
         TabIndex        =   17
         Top             =   2820
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   840
         TabIndex        =   16
         Top             =   3930
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   840
         TabIndex        =   15
         Top             =   5040
         Width           =   9000
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   14
         Top             =   570
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   360
         TabIndex        =   13
         Top             =   1725
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   360
         TabIndex        =   12
         Top             =   2820
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   360
         TabIndex        =   11
         Top             =   3930
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   360
         TabIndex        =   10
         Top             =   5040
         Width           =   495
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   255
         Index           =   0
         Left            =   9960
         TabIndex        =   9
         Top             =   720
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   375
         Index           =   1
         Left            =   10080
         TabIndex        =   8
         Top             =   1320
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   375
         Index           =   2
         Left            =   10080
         TabIndex        =   7
         Top             =   1920
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   495
         Index           =   3
         Left            =   10200
         TabIndex        =   6
         Top             =   2520
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   255
         Index           =   4
         Left            =   10200
         TabIndex        =   5
         Top             =   3360
         Visible         =   0   'False
         Width           =   735
      End
   End
   Begin VB.Frame fraFrame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Actuaci�n Pedida"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   1095
      Index           =   0
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Width           =   11295
      Begin VB.TextBox txtactuacionpedida 
         BackColor       =   &H00FFC0FF&
         Height          =   330
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   480
         Width           =   5400
      End
   End
   Begin VB.CommandButton cmdfinal 
      Caption         =   "Fin del Cuestionario"
      Height          =   375
      Index           =   0
      Left            =   9000
      TabIndex        =   2
      Top             =   8040
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdatras 
      Caption         =   "Atr�s"
      Height          =   375
      Left            =   5400
      TabIndex        =   1
      Top             =   8040
      Width           =   1695
   End
   Begin VB.CommandButton cmdsiguiente 
      Caption         =   "Siguiente"
      Height          =   375
      Left            =   7200
      TabIndex        =   0
      Top             =   8040
      Width           =   1695
   End
End
Attribute VB_Name = "frmcuestionario3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00145.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Se muestra el cuestionario asociado a una actuaci�n     *
'*               para ser respondido                                    *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Dim strsql As String
Dim rsta As rdoResultset
' inthay lleva la cuenta de los registros leidos
Dim inthay As Integer
' intsobran cuenta las preguntas en blanco de la pantalla
Dim intsobran As Integer
' texto es una matriz para contabilizar los TextBox activados
Dim texto(5) As Integer
' fecha es una matriz para contabilizar los dtcfecha activados
Dim fecha(5) As Integer
' guardarfecha sirve para guardar los valores de los campos de tipo fecha
' ya que al hacer Load de un control fecha borra el contenido del resto de
' los controles tipo fecha que est�n ya en pantalla
Dim guardarfecha(5) As String
'blncargado indica si el bot�n "Fin del Cuestionario" est� cargado o no
Dim blncargado As Integer
'strmensaje,strmensaje1,strmensaje2 para guardar los mensajes que se sacan por pantalla
Dim strmensaje As String
Dim strmensaje1 As String
Dim strmensaje2 As String

Private Sub cmdatras_Click()
Dim strmodif As String
Dim strrespuesta As String
Dim rstrespuesta As rdoResultset
Dim respuesta As String
Dim rstcodigo As rdoResultset
Dim strcodigo As String
Dim rstvalor As rdoResultset
Dim strvalor As String
Dim respuestacodigo As String

' intcuenta cuenta cu�ntos registros hay que recorrer
' antes de escribir los de la p�g anterior
Dim intcuenta As Integer

'se miran qu� preguntas con respuesta obligatoria no han sido contestadas
'For i = 0 To 4
' If texto(i) = 1 Then
'   If (txtText1(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'       End If
' End If
' If fecha(i) = 1 Then
'   If (dtcfecha(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'  End If
' End If
'Next i
'If Cancel = 1 Then
'    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
'    strmensaje = ""
'    strmensaje1 = ""
'    strmensaje2 = ""
'    Exit Sub
'End If




' Unload de los controles de la pantalla y UPDATE en la BD
For i = 0 To 4
    If texto(i) = 1 Then
       txtText1(i).Locked = False
       On Error GoTo Err_Ejecutar
       If txtText1(i) = "" Then
          strmodif = "UPDATE  PR4700 SET pr47valor= " & "' '" _
               & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
               " AND ag16codtiprest=" & lblLabel4(i).Caption
       Else
            If txtcodigo(i).Text = "" Then
               strmodif = "UPDATE  PR4700 SET pr47valor= " & "'" & txtText1(i) & "'" _
                    & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
                    " AND ag16codtiprest=" & lblLabel4(i).Caption
            Else
               strmodif = "UPDATE  PR4700 SET pr47valor= " & "'" & txtcodigo(i) & "'" _
                    & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
                    " AND ag16codtiprest=" & lblLabel4(i).Caption
            End If
        End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       Unload txtcodigo(i)
       texto(i) = 0
       Unload cmdbuscar(i)
    End If
    If fecha(i) = 1 Then
       On Error GoTo Err_Ejecutar
       If dtcfecha(i) Then
        strmodif = "UPDATE  PR4700 SET pr47valor=' '" & _
              " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
              " AND ag16codtiprest=" & lblLabel4(i).Caption
       Else
        strmodif = "UPDATE  PR4700 SET pr47valor=" & _
              "TO_DATE('" & dtcfecha(i) & "','DD/MM/YYYY')" & _
              " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
              " AND ag16codtiprest=" & lblLabel4(i).Caption
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload dtcfecha(i)
       Unload txtcodigo(i)
       fecha(i) = 0
       Unload cmdbuscar(i)
    End If
    
Next i

If blncargado = 1 Then
    Unload cmdfinal(1)
    blncargado = 0
End If

'se calcula en intcuenta los registros a recorrer
intcuenta = (inthay + intsobran) - 10
' el bot�n Atr�s se deshabilita si estamos en las 5 primeras preguntas
If (intcuenta = 0) Then
    cmdatras.Enabled = False
End If

cmdsiguiente.Enabled = True
inthay = intcuenta

strsql = "select DISTINCT * from PR0301J where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
For k = 1 To intcuenta
    rsta.MoveNext
Next k
'sacar en pantalla 5 preguntas
    For i = 0 To 4
       'se saca el texto de la pregunta
       lblLabel1(i) = rsta.rdoColumns("AG16DESTIPREST").Value
       lblLabel2(i) = inthay + 1
       
       'se mira si el registro ya tiene una respuesta guardada
       strrespuesta = "select PR4700.pr47valor,AG1600.ag18codtipdato FROM PR4700,AG1600 " & _
                    "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
                    " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
                    " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
       Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
       If Trim(rstrespuesta.rdoColumns(0).Value) = "" Then
          respuesta = ""
       Else
       '******************************************************************
       If (rstrespuesta.rdoColumns(1).Value = 1) Or _
         (rstrespuesta.rdoColumns(1).Value = 2) Then
       'se mira si la respuesta es un c�digo o no
       strcodigo = "select PR4700.pr47valor,AG1600.ag16tablavalor," & _
                   "AG1600.ag16colcodvalo,AG1600.ag16coldesvalo " & _
                   "FROM AG1600,PR4700 WHERE " & _
                   "PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
                   " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
                   " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
       Set rstcodigo = objApp.rdoConnect.OpenResultset(strcodigo)
       If rstcodigo.rdoColumns(1).Value = "" Or IsNull(rstcodigo.rdoColumns(1).Value) Then
         respuesta = rstrespuesta.rdoColumns(0).Value
       Else
         strvalor = "SELECT " & rstcodigo.rdoColumns(2).Value & _
                  "," & rstcodigo.rdoColumns(3).Value & _
                  " FROM " & _
                  rstcodigo.rdoColumns(1) & " WHERE " & _
                  rstcodigo.rdoColumns(2).Value & "='" & _
                  rstrespuesta.rdoColumns(0).Value & "'"
         Set rstvalor = objApp.rdoConnect.OpenResultset(strvalor)
         respuesta = rstvalor.rdoColumns(1).Value
         respuestacodigo = rstvalor.rdoColumns(0).Value
         rstvalor.Close
         Set rstvalor = Nothing
       End If
       rstcodigo.Close
       Set rstcodigo = Nothing
       End If
       If (rstrespuesta.rdoColumns(1).Value = 3) Then
          strrespuesta = "select to_char(to_date(PR4700.pr47valor),'DD/MM/YYYY') " & _
              "FROM PR4700,AG1600 " & _
              "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
              " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
              " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest" & _
              " AND AG1600.ag18codtipdato=3"
            Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
            respuesta = rstrespuesta.rdoColumns(0).Value
       End If
       
       '******************************************************************
          'If (rstrespuesta.rdoColumns(1).Value = 1) Or _
          '   (rstrespuesta.rdoColumns(1).Value = 2) Then
          '        respuesta = rstrespuesta.rdoColumns(0).Value
          'End If
          'If (rstrespuesta.rdoColumns(1).Value = 3) Then
          '        strrespuesta = "select to_char(to_date(PR4700.pr47valor),'DD/MM/YYYY') " & _
          '          "FROM PR4700,AG1600 " & _
          '          "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
          '          " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
          '          " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest" & _
          '          " AND AG1600.ag18codtipdato=3"
          '        Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
          '        respuesta = rstrespuesta.rdoColumns(0).Value
          'End If
       End If
       rstrespuesta.Close
       Set rstrespuesta = Nothing
       
       'AG18CODTIPDATO (num�rico,texto,fecha)
       Select Case (rsta.rdoColumns("ag18codtipdato").Value)
        Case 1 'NUMERICO
            Load txtText1(i)
            Load txtcodigo(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
             '**********************************************************
            'si los posibles valores est�n en ag1700 y no existe HASTA la caja
            'de texto donde se mete la respuesta debe estar bloqueada
            Dim strbloqueo As String
            Dim rstbloqueo As rdoResultset
            Dim strbusqueda As String
            Dim rstbusqueda As rdoResultset
            
            strbloqueo = "select ag16tablavalor from AG1600 where " & _
                       "ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest").Value
            Set rstbloqueo = objApp.rdoConnect.OpenResultset(strbloqueo)
            If rstbloqueo.rdoColumns("ag16tablavalor") = "" Or _
               IsNull(rstbloqueo.rdoColumns("ag16tablavalor")) = True Then
               strbusqueda = "select ag17valhastatre from ag1700 where " & _
                           "ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest").Value
               Set rstbusqueda = objApp.rdoConnect.OpenResultset(strbusqueda)
               While Not rstbusqueda.EOF
                     If IsNull(rstbusqueda.rdoColumns(0).Value) = True Or _
                        rstbusqueda.rdoColumns(0).Value = "" Then
                        txtText1(i).Locked = True
                      End If
               rstbusqueda.MoveNext
               Wend
             rstbusqueda.Close
             Set rstbusqueda = Nothing
             Else
              txtText1(i).Locked = True
             End If
             rstbloqueo.Close
             Set rstbloqueo = Nothing
            '***************************************************************************
            txtText1(i) = respuesta
            txtcodigo(i).Text = respuestacodigo
            respuestacodigo = ""
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            txtText1(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            txtText1(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
        Case 2 'TEXTO
            Load txtText1(i)
            Load txtcodigo(i)
            texto(i) = 1
            txtText1(i).Visible = True
            'txtText1(i).Locked = False
            txtText1(i).Locked = True
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            txtText1(i) = respuesta
            txtcodigo(i).Text = respuestacodigo
            respuestacodigo = ""
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            txtText1(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            txtText1(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
        Case 3 'FECHA
            Load dtcfecha(i)
            Load txtcodigo(i)
            fecha(i) = 1
            dtcfecha(i).Visible = True
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            dtcfecha(i) = respuesta
            guardarfecha(i) = respuesta
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            dtcfecha(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            dtcfecha(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            dtcfecha(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            dtcfecha(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            dtcfecha(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            dtcfecha(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
       End Select
       If (rsta.EOF) Then
          Exit For
       Else
          inthay = inthay + 1
          rsta.MoveNext
          If rsta.EOF Then
             cmdsiguiente.Enabled = False
             Load cmdfinal(1)
             blncargado = 1
             cmdfinal(1).Visible = True
             Exit For
          End If
       End If
    Next i
    
    'se rellenan los campos tipo fecha que se han borrado
    For i = 0 To 4
      If guardarfecha(i) <> "" Then
          dtcfecha(i).Text = guardarfecha(i)
      End If
    Next i
    For i = 0 To 4
      guardarfecha(i) = ""
    Next i
    
    intsobran = 0
 


Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub cmdbuscar_Click(Index As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim haybuscador As Integer

cuest3 = 1
stra = "SELECT ag16tablavalor,ag16colcodvalo,ag16coldesvalo FROM AG1600 WHERE " & _
     "ag16codtiprest=" & lblLabel4(Index).Caption & " AND " & _
     "ag16destiprest=" & "'" & lblLabel1(Index).Caption & "'" & " AND " & _
     "ag18codtipdato=" & "'" & lblLabel3(Index).Caption & "'"
Set rsta = objApp.rdoConnect.OpenResultset(stra)

If Not IsNull(rsta.rdoColumns(0).Value) And Trim(rsta.rdoColumns(0).Value) <> "" Then
  If Not IsNull(rsta.rdoColumns(1).Value) And Trim(rsta.rdoColumns(1).Value) <> "" And _
     Not IsNull(rsta.rdoColumns(2).Value) And Trim(rsta.rdoColumns(2).Value) <> "" Then
     haybuscador = 1
     gtabla = rsta.rdoColumns(0).Value   'columna tabla
     gcolumna = rsta.rdoColumns(1).Value 'columna c�digo
     gcoldesc = rsta.rdoColumns(2).Value 'columna descripci�n
     gcodrest = lblLabel4(Index).Caption
     gdesrest = lblLabel1(Index).Caption
     gtipodato = lblLabel3(Index).Caption
     gindice = Index
     Call objsecurity.LaunchProcess("PR0215")
  End If
End If

If IsNull(rsta.rdoColumns(0).Value) Or Trim(rsta.rdoColumns(0).Value) = "" Then
     haybuscador = 1
     gdesde = lblLabel4(Index).Caption
     '******************************************************************
     'gtabla = rsta.rdoColumns(0).Value   'columna tabla
     'gcolumna = rsta.rdoColumns(1).Value 'columna c�digo
     'gcoldesc = rsta.rdoColumns(2).Value 'columna descripci�n
     'gcodrest = lblLabel4(Index).Caption
     'gdesrest = lblLabel1(Index).Caption
     gtipodato = lblLabel3(Index).Caption
     gindice = Index
     '*******************************************************************
     Call objsecurity.LaunchProcess("PR0214")
End If

If haybuscador = 0 Then
   MsgBox "No hay informaci�n sobre franja de restricciones asociada a la pregunta"
Else
  haybuscador = 0
End If

rsta.Close
Set rsta = Nothing
End Sub

Private Sub cmdfinal_Click(Index As Integer)
  Unload Me
End Sub

Private Sub cmdsiguiente_Click()
Dim strmodif As String
Dim strrespuesta As String
Dim rstrespuesta As rdoResultset
Dim respuesta As String
Dim rstcodigo As rdoResultset
Dim strcodigo As String
Dim rstvalor As rdoResultset
Dim strvalor As String
Dim respuestacodigo As String


'se miran qu� preguntas con respuesta obligatoria no han sido contestadas
'For i = 0 To 4
' If texto(i) = 1 Then
'   If (txtText1(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'       End If
' End If
' If fecha(i) = 1 Then
'   If (dtcfecha(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'  End If
' End If
'Next i
'If Cancel = 1 Then
'    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
'    strmensaje = ""
'    strmensaje1 = ""
'    strmensaje2 = ""
'    Exit Sub
'End If

cmdatras.Enabled = True

' Unload de los controles de la pantalla y UPDATE en la BD
For i = 0 To 4
    If texto(i) = 1 Then
       txtText1(i).Locked = False
       On Error GoTo Err_Ejecutar
       If txtText1(i) = "" Then
          strmodif = "UPDATE  PR4700 SET pr47valor= " & "' '" _
               & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
               " AND ag16codtiprest=" & lblLabel4(i).Caption
       Else
            If txtcodigo(i).Text = "" Then
               strmodif = "UPDATE  PR4700 SET pr47valor= " & "'" & txtText1(i) & "'" _
                 & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
                   " AND ag16codtiprest=" & lblLabel4(i).Caption
            Else
               strmodif = "UPDATE  PR4700 SET pr47valor= " & "'" & txtcodigo(i) & "'" _
                 & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
                   " AND ag16codtiprest=" & lblLabel4(i).Caption
            End If
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       Unload txtcodigo(i)
       texto(i) = 0
       Unload cmdbuscar(i)
    End If

    If fecha(i) = 1 Then
       On Error GoTo Err_Ejecutar
       If dtcfecha(i) = "" Then
        strmodif = "UPDATE  PR4700 SET pr47valor=' '" & _
              " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
              " AND ag16codtiprest=" & lblLabel4(i).Caption
       Else
        strmodif = "UPDATE  PR4700 SET pr47valor=" & _
               "TO_DATE('" & dtcfecha(i) & "','DD/MM/YYYY')" & _
               " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
               " AND ag16codtiprest=" & lblLabel4(i).Caption
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload dtcfecha(i)
       Unload txtcodigo(i)
       fecha(i) = 0
       Unload cmdbuscar(i)
    End If
Next i
    'sacar en pantalla 5 preguntas
    For i = 0 To 4
       'se saca el texto de la pregunta
       lblLabel1(i) = rsta.rdoColumns("AG16DESTIPREST").Value
       lblLabel2(i) = inthay + 1
       
       'se mira si el registro ya tiene una respuesta guardada
       strrespuesta = "select PR4700.pr47valor,AG1600.ag18codtipdato FROM PR4700,AG1600 " & _
                    "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
                    " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
                    " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
       Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
       If Trim(rstrespuesta.rdoColumns(0).Value) = "" Then
          respuesta = ""
       Else
       '***************************************************************
       If (rstrespuesta.rdoColumns(1).Value = 1) Or _
         (rstrespuesta.rdoColumns(1).Value = 2) Then
       'se mira si la respuesta es un c�digo o no
       strcodigo = "select PR4700.pr47valor,AG1600.ag16tablavalor," & _
                   "AG1600.ag16colcodvalo,AG1600.ag16coldesvalo " & _
                   "FROM AG1600,PR4700 WHERE " & _
                   "PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
                   " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
                   " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
       Set rstcodigo = objApp.rdoConnect.OpenResultset(strcodigo)
       If rstcodigo.rdoColumns(1).Value = "" Or IsNull(rstcodigo.rdoColumns(1).Value) Then
         respuesta = rstrespuesta.rdoColumns(0).Value
       Else
         strvalor = "SELECT " & rstcodigo.rdoColumns(2).Value & _
                  "," & rstcodigo.rdoColumns(3).Value & _
                  " FROM " & _
                  rstcodigo.rdoColumns(1) & " WHERE " & _
                  rstcodigo.rdoColumns(2).Value & "='" & _
                  rstrespuesta.rdoColumns(0).Value & "'"
         Set rstvalor = objApp.rdoConnect.OpenResultset(strvalor)
         respuesta = rstvalor.rdoColumns(1).Value
         respuestacodigo = rstvalor.rdoColumns(0).Value
         rstvalor.Close
         Set rstvalor = Nothing
       End If
       rstcodigo.Close
       Set rstcodigo = Nothing
       End If
       If (rstrespuesta.rdoColumns(1).Value = 3) Then
          strrespuesta = "select to_char(to_date(PR4700.pr47valor),'DD/MM/YYYY') " & _
              "FROM PR4700,AG1600 " & _
              "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
              " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
              " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest" & _
              " AND AG1600.ag18codtipdato=3"
            Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
            respuesta = rstrespuesta.rdoColumns(0).Value
       End If
       '***************************************************************
          'If (rstrespuesta.rdoColumns(1).Value = 1) Or _
          '   (rstrespuesta.rdoColumns(1).Value = 2) Then
          '        respuesta = rstrespuesta.rdoColumns(0).Value
          'End If
          'If (rstrespuesta.rdoColumns(1).Value = 3) Then
          '        strrespuesta = "select to_char(to_date(PR4700.pr47valor),'DD/MM/YYYY') " & _
          '          "FROM PR4700,AG1600 " & _
          '          "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
          '          " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
          '          " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest" & _
          '          " AND AG1600.ag18codtipdato=3"
          '        Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
          '        respuesta = rstrespuesta.rdoColumns(0).Value
          'End If
       End If
       rstrespuesta.Close
       Set rstrespuesta = Nothing
       
       'AG18CODTIPDATO (num�rico,texto,fecha)
       Select Case (rsta.rdoColumns("ag18codtipdato").Value)
        Case 1 'NUMERICO
            Load txtText1(i)
            Load txtcodigo(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            '**********************************************************
            'si los posibles valores est�n en ag1700 y no existe HASTA la caja
            'de texto donde se mete la respuesta debe estar bloqueada
            Dim strbloqueo As String
            Dim rstbloqueo As rdoResultset
            Dim strbusqueda As String
            Dim rstbusqueda As rdoResultset
            
            strbloqueo = "select ag16tablavalor from AG1600 where " & _
                       "ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest").Value
            Set rstbloqueo = objApp.rdoConnect.OpenResultset(strbloqueo)
            If rstbloqueo.rdoColumns("ag16tablavalor") = "" Or _
               IsNull(rstbloqueo.rdoColumns("ag16tablavalor")) = True Then
               strbusqueda = "select ag17valhastatre from ag1700 where " & _
                           "ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest").Value
               Set rstbusqueda = objApp.rdoConnect.OpenResultset(strbusqueda)
               While Not rstbusqueda.EOF
                     If IsNull(rstbusqueda.rdoColumns(0).Value) = True Or _
                        rstbusqueda.rdoColumns(0).Value = "" Then
                        txtText1(i).Locked = True
                      End If
               rstbusqueda.MoveNext
               Wend
             rstbusqueda.Close
             Set rstbusqueda = Nothing
             Else
              txtText1(i).Locked = True
             End If
             rstbloqueo.Close
             Set rstbloqueo = Nothing
            '**********************************************************
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            txtText1(i) = respuesta
            txtcodigo(i).Text = respuestacodigo
            respuestacodigo = ""
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            txtText1(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            txtText1(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
        Case 2 'TEXTO
            Load txtText1(i)
            Load txtcodigo(i)
            texto(i) = 1
            txtText1(i).Visible = True
            'txtText1(i).Locked = False
            txtText1(i).Locked = True
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            txtText1(i) = respuesta
            txtcodigo(i).Text = respuestacodigo
            respuestacodigo = ""
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            txtText1(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            txtText1(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
        Case 3 'FECHA
            Load dtcfecha(i)
            Load txtcodigo(i)
            fecha(i) = 1
            dtcfecha(i).Visible = True
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            dtcfecha(i) = respuesta
            txtcodigo(i).Text = respuesta
            guardarfecha(i) = respuesta
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            dtcfecha(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            dtcfecha(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            dtcfecha(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            dtcfecha(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            dtcfecha(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            dtcfecha(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
       End Select
       If (rsta.EOF) Then
          Exit For
       Else
          inthay = inthay + 1
          rsta.MoveNext
          If rsta.EOF Then
             cmdsiguiente.Enabled = False
             Load cmdfinal(1)
             blncargado = 1
             cmdfinal(1).Visible = True
             Exit For
          End If
       End If
    Next i
    
    intsobran = 0
    For j = (i + 1) To 4 'vaciar el resto de campos
        lblLabel1(j) = ""
        lblLabel2(j) = ""
        intsobran = intsobran + 1
    Next j
    
     'se rellenan los campos tipo fecha que se han borrado
    For i = 0 To 4
      If guardarfecha(i) <> "" Then
          dtcfecha(i).Text = guardarfecha(i)
      End If
    Next i
    For i = 0 To 4
      guardarfecha(i) = ""
    Next i
    
    
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub
Private Sub dtcfecha_Lostfocus(Index As Integer)
Dim strmen As String
Dim rsta As rdoResultset
Dim stra As String
Dim rstcol As rdoResultset
Dim strcol As String
Dim datoincorrecto As String

'datoincorrecto = 0
'
'On Error GoTo Err_Ejecutar

'stra = "SELECT ag16tablavalor,ag16colcodvalo,ag16coldesvalo FROM AG1600 WHERE " & _
'      "AG16CODTIPREST=" & lblLabel4(Index).Caption & " AND " & _
'      "AG16DESTIPREST=" & "'" & lblLabel1(Index).Caption & "'" & " AND " & _
'      "AG18CODTIPDATO=" & "'" & lblLabel3(Index).Caption & "'"
'Set rsta = objApp.rdoConnect.OpenResultset(stra)
''Se buscan los posibles valores en la tabla y columna indicadas en AG1600
'If (IsNull(rsta.rdoColumns("ag16tablavalor").Value)) = False And _
'    Trim(rsta.rdoColumns("ag16tablavalor").Value) <> "" Then
'    If (IsNull(rsta.rdoColumns("ag16colcodvalo").Value)) = False And _
'       Trim(rsta.rdoColumns("ag16colcodvalo").Value) <> "" And _
'       (IsNull(rsta.rdoColumns("ag16coldesvalo").Value)) = False And _
'       Trim(rsta.rdoColumns("ag16coldesvalo").Value) <> "" Then
'     strcol = "SELECT to_char(to_date(" & rsta.rdoColumns("ag16coldesvalo").Value & "),'DD/MM/YYYY')" & _
'           " FROM " & _
'           rsta.rdoColumns("ag16tablavalor").Value
'     Set rstcol = objApp.rdoConnect.OpenResultset(strcol)
'     Do While Not rstcol.EOF
'        If dtcfecha(Index).Text <> "" And IsNull(rstcol.rdoColumns(0).Value) = False Then
'         dtcfechadesde.Text = rstcol.rdoColumns(0).Value
'         If dtcfechadesde.Text <> "" Then
'          If DateDiff("d", dtcfechadesde.Text, dtcfecha(Index).Text) = 0 Or _
'            dtcfecha(Index).Text = "" Then
'           datoincorrecto = 0
'           Exit Do
'          Else
'          datoincorrecto = 1
'          End If
'         End If
'        End If
'    rstcol.MoveNext
'    Loop
'    rstcol.Close
'    Set rstcol = Nothing
'End If
'End If



''Se buscan los posibles valores en AG1700
'If (IsNull(rsta.rdoColumns("ag16tablavalor").Value)) = True Then
'    strcol = "SELECT to_char(to_date(ag17valdesdetre),'DD/MM/YYYY')," & _
'                     "to_char(to_date(ag17valhastatre),'DD/MM/YYYY') FROM AG1700 " & _
'             "WHERE ag16codtiprest=" & lblLabel4(Index).Caption
'    Set rstcol = objApp.rdoConnect.OpenResultset(strcol)
'    Do While Not rstcol.EOF
'       'no existe el campo HASTA
'       If IsNull(rstcol.rdoColumns(1).Value) = True Or _
'          Trim(rstcol.rdoColumns(1).Value) = "" Then
'          'si la respuesta es tipo fecha
'          If lblLabel3(Index).Caption = 3 Then
'              If IsNull(rstcol.rdoColumns(0).Value) = False And _
'                  Trim(rstcol.rdoColumns(0).Value) <> "" Then
'               dtcfechadesde.Text = rstcol.rdoColumns(0).Value
'               If dtcfecha(Index).Text <> "" And dtcfechadesde.Text <> "" Then
'                If DateDiff("d", dtcfechadesde.Text, dtcfecha(Index).Text) > 0 Or _
'                  DateDiff("d", dtcfechadesde.Text, dtcfecha(Index).Text) = 0 Or _
'                  dtcfecha(Index).Text = "" Then
'                  datoincorrecto = 0
'                  Exit Do
'                Else
'                  datoincorrecto = 1
'                End If
'              End If
'             End If
'          End If
'        End If
'        'existe el campo HASTA
'        If IsNull(rstcol.rdoColumns(1).Value) = False And _
'           Trim(rstcol.rdoColumns(1).Value) <> "" Then
'          'si la respuesta es tipo fecha
'          If lblLabel3(Index).Caption = 3 Then
'            If IsNull(rstcol.rdoColumns(0).Value) = False And _
'               IsNull(rstcol.rdoColumns(1).Value) = False Then
'               dtcfechadesde.Text = rstcol.rdoColumns(0).Value
'               dtcfechahasta.Text = rstcol.rdoColumns(1).Value
'               If dtcfecha(Index).Text <> "" And dtcfechadesde.Text <> "" And _
'                  dtcfechahasta.Text <> "" Then
'                If (DateDiff("d", dtcfechadesde.Text, dtcfecha(Index).Text) > 0 Or _
'                  DateDiff("d", dtcfechadesde.Text, dtcfecha(Index).Text) = 0) _
'                  And _
'                  (DateDiff("d", dtcfechahasta.Text, dtcfecha(Index).Text) < 0 Or _
'                   DateDiff("d", dtcfechahasta.Text, dtcfecha(Index).Text) = 0) _
'                  Or _
'                  (dtcfecha(Index).Text = "") Then
'                  datoincorrecto = 0
'                  Exit Do
'               Else
'                  datoincorrecto = 1
'               End If
'              End If
'             End If
'          End If
'        End If
'
'      rstcol.MoveNext
'      Loop
'      rstcol.Close
'      Set rstcol = Nothing
' End If
              
            

'If datoincorrecto = 1 Then
'   datoincorrecto = 0
'   strmen = MsgBox("Datos Incorrectos", vbCritical, "Pregunta " & lblLabel2(Index).Caption)
'   dtcfecha(Index).Text = ""
'   dtcfecha(Index).SetFocus
'End If

'rsta.Close
'Set rsta = Nothing
'Exit Sub

'Err_Ejecutar:
'Exit Sub
End Sub



Private Sub Form_Activate()
'actuaciones pedidas de la peticion

Dim strsql4 As String
Dim intcontador As Integer
Dim intnumact As Integer
Dim strrespuesta As String
Dim rstrespuesta As rdoResultset
Dim respuesta As String
Dim strcodigo As String
Dim rstcodigo As rdoResultset
Dim strvalor As String
Dim rstvalor As rdoResultset
Dim respuestacodigo As String


'si gsalirag17=1 indica que se vuelve del buscador de la tabla AG1700(FRMAG17)
If gsalirag17 = 1 Then
   gsalirag17 = 0
   Exit Sub
End If

'se inicializa el array guardarfecha
For i = 0 To 4
      guardarfecha(i) = ""
Next i

txtactuacionpedida.Text = frmdatosactpedida.txtText1(4).Text
cmdatras.Enabled = False
On Error GoTo Err_Ejecutar

strsql = "select DISTINCT * from PR0301J where pr03numactpedi=" & frmdatosactpedida.txtText1(0).Text
Set rsta = objApp.rdoConnect.OpenResultset(strsql)

    'sacar en pantalla 5 preguntas
    For i = 0 To 4
       'se saca el texto de la pregunta
       lblLabel1(i) = rsta.rdoColumns("AG16DESTIPREST").Value
       lblLabel2(i) = inthay + 1
       'se mira si el registro ya tiene una respuesta guardada
       strrespuesta = "select PR4700.pr47valor,AG1600.ag18codtipdato FROM PR4700,AG1600 " & _
                    "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
                    " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
                    " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
       Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
       If Trim(rstrespuesta.rdoColumns(0).Value) = "" Then
          respuesta = ""
       Else
       '**************************************************************
      'si la respuesta es num�rica o tipo string
      If (rstrespuesta.rdoColumns(1).Value = 1) Or _
         (rstrespuesta.rdoColumns(1).Value = 2) Then
       'se mira si la respuesta es un c�digo o no
       strcodigo = "select PR4700.pr47valor,AG1600.ag16tablavalor," & _
                   "AG1600.ag16colcodvalo,AG1600.ag16coldesvalo " & _
                   "FROM AG1600,PR4700 WHERE " & _
                   "PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
                   " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
                   " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest"
       Set rstcodigo = objApp.rdoConnect.OpenResultset(strcodigo)
       If rstcodigo.rdoColumns(1).Value = "" Or IsNull(rstcodigo.rdoColumns(1).Value) Then
         respuesta = rstrespuesta.rdoColumns(0).Value
       Else
         strvalor = "SELECT " & rstcodigo.rdoColumns(2).Value & _
                  "," & rstcodigo.rdoColumns(3).Value & _
                  " FROM " & _
                  rstcodigo.rdoColumns(1) & " WHERE " & _
                  rstcodigo.rdoColumns(2).Value & "='" & _
                  rstrespuesta.rdoColumns(0).Value & "'"
         Set rstvalor = objApp.rdoConnect.OpenResultset(strvalor)
         respuesta = rstvalor.rdoColumns(1).Value
         respuestacodigo = rstvalor.rdoColumns(0).Value
         rstvalor.Close
         Set rstvalor = Nothing
       End If
       rstcodigo.Close
       Set rstcodigo = Nothing
       End If
       If (rstrespuesta.rdoColumns(1).Value = 3) Then
          strrespuesta = "select to_char(to_date(PR4700.pr47valor),'DD/MM/YYYY') " & _
              "FROM PR4700,AG1600 " & _
              "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
              " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
              " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest" & _
              " AND AG1600.ag18codtipdato=3"
            Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
            respuesta = rstrespuesta.rdoColumns(0).Value
       End If
       '**************************************************************
          'If (rstrespuesta.rdoColumns(1).Value = 1) Or _
          '   (rstrespuesta.rdoColumns(1).Value = 2) Then
          '        respuesta = rstrespuesta.rdoColumns(0).Value
          'End If
          'If (rstrespuesta.rdoColumns(1).Value = 3) Then
          '       strrespuesta = "select to_char(to_date(PR4700.pr47valor),'DD/MM/YYYY') " & _
          '          "FROM PR4700,AG1600 " & _
          '          "WHERE PR4700.pr09numpeticion=" & frmdatosactpedida.txtText1(12).Text & _
          '          " AND PR4700.ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest") & _
          '          " AND PR4700.ag16codtiprest=AG1600.ag16codtiprest" & _
          '          " AND AG1600.ag18codtipdato=3"
          '        Set rstrespuesta = objApp.rdoConnect.OpenResultset(strrespuesta)
          '        respuesta = rstrespuesta.rdoColumns(0).Value
          'End If
       End If
       rstrespuesta.Close
       Set rstrespuesta = Nothing
       
       'AG18CODTIPDATO (num�rico,texto,fecha)
       Select Case (rsta.rdoColumns("ag18codtipdato").Value)
        Case 1 'NUMERICO
            Load txtText1(i)
            Load txtcodigo(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            '**********************************************************
            'si los posibles valores est�n en ag1700 y no existe HASTA la caja
            'de texto donde se mete la respuesta debe estar bloqueada
            Dim strbloqueo As String
            Dim rstbloqueo As rdoResultset
            Dim strbusqueda As String
            Dim rstbusqueda As rdoResultset
            
            strbloqueo = "select ag16tablavalor from AG1600 where " & _
                       "ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest").Value
            Set rstbloqueo = objApp.rdoConnect.OpenResultset(strbloqueo)
            If rstbloqueo.rdoColumns("ag16tablavalor") = "" Or _
               IsNull(rstbloqueo.rdoColumns("ag16tablavalor")) = True Then
               strbusqueda = "select ag17valhastatre from ag1700 where " & _
                           "ag16codtiprest=" & rsta.rdoColumns("ag16codtiprest").Value
               Set rstbusqueda = objApp.rdoConnect.OpenResultset(strbusqueda)
               While Not rstbusqueda.EOF
                     If IsNull(rstbusqueda.rdoColumns(0).Value) = True Or _
                        rstbusqueda.rdoColumns(0).Value = "" Then
                        txtText1(i).Locked = True
                      End If
               rstbusqueda.MoveNext
               Wend
             rstbusqueda.Close
             Set rstbusqueda = Nothing
             Else
              txtText1(i).Locked = True
             End If
             rstbloqueo.Close
             Set rstbloqueo = Nothing
            '***************************************************************************
            txtText1(i).Text = respuesta
            txtcodigo(i).Text = respuestacodigo
            respuestacodigo = ""
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            txtText1(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            txtText1(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
        Case 2 'TEXTO
            Load txtText1(i)
            Load txtcodigo(i)
            texto(i) = 1
            txtText1(i).Visible = True
            'txtText1(i).Locked = False
            txtText1(i).Locked = True
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            txtText1(i).Text = respuesta
            txtcodigo(i).Text = respuestacodigo
            respuestacodigo = ""
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            txtText1(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            txtText1(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
        Case 3 'FECHA
            Load dtcfecha(i)
            Load txtcodigo(i)
            fecha(i) = 1
            dtcfecha(i).Visible = True
            'txtcodigo(i).Visible = True
            txtcodigo(i).Locked = False
            dtcfecha(i) = respuesta
            guardarfecha(i) = respuesta
            lblLabel3(i) = rsta.rdoColumns("ag18codtipdato").Value
            lblLabel4(i) = rsta.rdoColumns("ag16codtiprest").Value
            dtcfecha(i).Left = 840
            txtcodigo(i).Left = 6840
            Load cmdbuscar(i)
            cmdbuscar(i).Visible = True
            cmdbuscar(i).Left = 6360
            Select Case i
            Case 0
            dtcfecha(i).Top = 840
            txtcodigo(i).Top = 840
            cmdbuscar(i).Top = 840
            Case 1
            dtcfecha(i).Top = 2040
            txtcodigo(i).Top = 2040
            cmdbuscar(i).Top = 2040
            Case 2
            dtcfecha(i).Top = 3120
            txtcodigo(i).Top = 3120
            cmdbuscar(i).Top = 3120
            Case 3
            dtcfecha(i).Top = 4200
            txtcodigo(i).Top = 4200
            cmdbuscar(i).Top = 4200
            Case 4
            dtcfecha(i).Top = 5280
            txtcodigo(i).Top = 5280
            cmdbuscar(i).Top = 5280
            End Select
       End Select
       If (rsta.EOF) Then
          Exit For
       Else
          inthay = inthay + 1
          rsta.MoveNext
          If rsta.EOF Then
             cmdsiguiente.Enabled = False
             Load cmdfinal(1)
             blncargado = 1
             cmdfinal(1).Visible = True
             Exit For
          End If
       End If
    Next i
    
    intsobran = 0
    For j = (i + 1) To 4 'vaciar el resto de campos
        lblLabel1(j) = ""
        lblLabel2(j) = ""
        intsobran = intsobran + 1
    Next j
    
    'se rellenan los campos tipo fecha que se han borrado
    For i = 0 To 4
      If guardarfecha(i) <> "" Then
          dtcfecha(i).Text = guardarfecha(i)
      End If
    Next i
    For i = 0 To 4
      guardarfecha(i) = ""
    Next i


Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

'se miran qu� preguntas con respuesta obligatoria no han sido contestadas
'For i = 0 To 4
' If texto(i) = 1 Then
'   If (txtText1(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'       End If
' End If
' If fecha(i) = 1 Then
'   If (dtcfecha(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'  End If
' End If
'Next i
'If Cancel = 1 Then
'    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
'    strmensaje = ""
'    strmensaje1 = ""
'    strmensaje2 = ""
'    Exit Sub
'End If



' Unload de los controles de la pantalla y UPDATE en la BD antes de salir
' Unload de los controles de la pantalla y UPDATE en la BD
For i = 0 To 4
    If texto(i) = 1 Then
       txtText1(i).Locked = False
       On Error GoTo Err_Ejecutar
       If txtText1(i) = "" Then
          strmodif = "UPDATE  PR4700 SET pr47valor= " & "' '" _
               & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
               " AND ag16codtiprest=" & lblLabel4(i).Caption
       Else
            If txtcodigo(i).Text = "" Then
               strmodif = "UPDATE  PR4700 SET pr47valor= " & "'" & txtText1(i) & "'" _
                   & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
                    " AND ag16codtiprest=" & lblLabel4(i).Caption
            Else
               strmodif = "UPDATE  PR4700 SET pr47valor= " & "'" & txtcodigo(i) & "'" _
                   & " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
                    " AND ag16codtiprest=" & lblLabel4(i).Caption
            End If
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       Unload txtcodigo(i)
       texto(i) = 0
       Unload cmdbuscar(i)
    End If
    If fecha(i) = 1 Then
       On Error GoTo Err_Ejecutar
       If dtcfecha(i) = "" Then
        strmodif = "UPDATE  PR4700 SET pr47valor=' '" & _
              " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
              " AND ag16codtiprest=" & lblLabel4(i).Caption
       Else
        strmodif = "UPDATE  PR4700 SET pr47valor=" & _
               "TO_DATE('" & dtcfecha(i) & "','DD/MM/YYYY')" & _
               " WHERE pr09numpeticion=" & frmdatosactpedida.txtText1(12) & _
               " AND ag16codtiprest=" & lblLabel4(i).Caption
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload dtcfecha(i)
       Unload txtcodigo(i)
       fecha(i) = 0
       Unload cmdbuscar(i)
    End If
Next i

If blncargado = 1 Then
 Unload cmdfinal(1)
 blncargado = 0
End If

rsta.Close
Set rsta = Nothing

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub



Private Sub txtText1_LostFocus(Index As Integer)
Dim strmen As String
Dim rsta As rdoResultset
Dim stra As String
Dim rstcol As rdoResultset
Dim strcol As String
Dim datoincorrecto As String
Dim cajadetexto As String



'datoincorrecto = 0

'On Error GoTo Err_Ejecutar

'' verificar que se metan n�meros en los campos num�ricos
'   If (lblLabel3(Index).Caption = "1") Then
'        If (IsNumeric(txtText1(Index).Text)) = False And (txtText1(Index).Text <> "") Then
'            Beep
'            strmen = MsgBox("Datos no Num�ricos", vbCritical, "Pregunta " & lblLabel2(Index).Caption)
'            txtText1(Index).Text = ""
'            txtText1(Index).SetFocus
'            Exit Sub
'        End If
'   End If
'
'stra = "SELECT ag16tablavalor,ag16colcodvalo,ag16coldesvalo FROM AG1600 WHERE " & _
'      "AG16CODTIPREST=" & lblLabel4(Index).Caption & " AND " & _
'      "AG16DESTIPREST=" & "'" & lblLabel1(Index).Caption & "'" & " AND " & _
'      "AG18CODTIPDATO=" & "'" & lblLabel3(Index).Caption & "'"
'Set rsta = objApp.rdoConnect.OpenResultset(stra)
'Se buscan los posibles valores en la tabla y columna indicadas en AG1600
'If (IsNull(rsta.rdoColumns("ag16tablavalor").Value)) = False And _
'    Trim(rsta.rdoColumns("ag16tablavalor").Value) <> "" Then
'  If (IsNull(rsta.rdoColumns("ag16colcodvalo").Value)) = False And _
'      Trim(rsta.rdoColumns("ag16colcodvalo").Value) <> "" And _
'      (IsNull(rsta.rdoColumns("ag16coldesvalo").Value)) = False And _
'      Trim(rsta.rdoColumns("ag16coldesvalo").Value) <> "" Then
'    strcol = "SELECT " & rsta.rdoColumns("ag16coldesvalo").Value & " FROM " & _
'           rsta.rdoColumns("ag16tablavalor").Value
'    Set rstcol = objApp.rdoConnect.OpenResultset(strcol)
'    Do While Not rstcol.EOF
'        cajadetexto = Trim(txtText1(Index).Text)
'        If (UCase(cajadetexto) = UCase(Trim(rstcol.rdoColumns(0).Value))) Or _
'         (Trim(txtText1(Index).Text) = "") Then
'           datoincorrecto = 0
'           Exit Do
'        Else
'          datoincorrecto = 1
'        End If
'    rstcol.MoveNext
'    Loop
'    rstcol.Close
'    Set rstcol = Nothing
'  End If
'End If


''Se buscan los posibles valores en AG1700
'If (IsNull(rsta.rdoColumns("ag16tablavalor").Value)) = True Or _
'    Trim(rsta.rdoColumns("ag16tablavalor").Value) = "" Then
'    strcol = "SELECT ag17valdesdetre,ag17valhastatre FROM AG1700 " & _
'           "WHERE ag16codtiprest=" & lblLabel4(Index).Caption
'    Set rstcol = objApp.rdoConnect.OpenResultset(strcol)
'    Do While Not rstcol.EOF
'       If IsNull(rstcol.rdoColumns("ag17valhastatre").Value) = True Or _
'          Trim(rstcol.rdoColumns("ag17valhastatre").Value) = "" Then
'          'si la respuesta es tipo texto
'          If lblLabel3(Index).Caption = 2 Then
'              cajadetexto = Trim(txtText1(Index).Text)
'              If (UCase(cajadetexto) > UCase(Trim(rstcol.rdoColumns("ag17valdesdetre").Value))) Or _
'                 (UCase(cajadetexto) = UCase(Trim(rstcol.rdoColumns("ag17valdesdetre").Value))) Or _
'                  (Trim(txtText1(Index).Text) = "") Then
'                  datoincorrecto = 0
'                  Exit Do
'              Else
'                  datoincorrecto = 1
'              End If
'          End If
'          'si la respuesta es tipo num�rico
'          If lblLabel3(Index).Caption = 1 Then
'              If Val(Trim(txtText1(Index).Text)) > Val(Trim(rstcol.rdoColumns("ag17valdesdetre").Value)) Or _
'                 Val(Trim(txtText1(Index).Text)) = Val(Trim(rstcol.rdoColumns("ag17valdesdetre").Value)) Or _
'                  (Trim(txtText1(Index).Text) = "") Then
'                  datoincorrecto = 0
'                  Exit Do
'              Else
'                  datoincorrecto = 1
'              End If
'          End If
'        End If
'        If IsNull(rstcol.rdoColumns("ag17valhastatre").Value) = False And _
'           Trim(rstcol.rdoColumns("ag17valhastatre").Value) <> "" Then
'        'si la respuesta es tipo texto
'          If lblLabel3(Index).Caption = 2 Then
'              cajadetexto = Trim(txtText1(Index).Text)
'              If (((UCase(cajadetexto) > UCase(Trim(rstcol.rdoColumns("ag17valdesdetre").Value))) Or _
'                 (UCase(cajadetexto) = UCase(Trim(rstcol.rdoColumns("ag17valdesdetre").Value)))) _
'                 And _
'                 ((UCase(cajadetexto) < UCase(Trim(rstcol.rdoColumns("ag17valhastatre").Value))) Or _
'                 (UCase(cajadetexto) = UCase(Trim(rstcol.rdoColumns("ag17valhastatre").Value))))) _
'                 Or _
'                 (Trim(txtText1(Index).Text) = "") Then
'                 datoincorrecto = 0
'                 Exit Do
'              Else
'                  datoincorrecto = 1
'              End If
'          End If
'          'si la respuesta es tipo num�rico
'          If lblLabel3(Index).Caption = 1 Then
'              If ((Val(Trim(txtText1(Index).Text)) > Val(Trim(rstcol.rdoColumns("ag17valdesdetre").Value)) Or _
'                  Val(Trim(txtText1(Index).Text)) = Val(Trim(rstcol.rdoColumns("ag17valdesdetre").Value))) _
'                  And _
'                  (Val(Trim(txtText1(Index).Text)) < Val(Trim(rstcol.rdoColumns("ag17valhastatre").Value)) Or _
'                  Val(Trim(txtText1(Index).Text)) = Val(Trim(rstcol.rdoColumns("ag17valhastatre").Value)))) _
'                  Or _
'                  (Trim(txtText1(Index).Text) = "") Then
'                  datoincorrecto = 0
'                  Exit Do
'              Else
'                  datoincorrecto = 1
'              End If
'          End If
'        End If
'
'      rstcol.MoveNext
'      Loop
'      rstcol.Close
'      Set rstcol = Nothing
' End If
            

'If datoincorrecto = 1 Then
'   datoincorrecto = 0
'   strmen = MsgBox("Datos Incorrectos", vbCritical, "Pregunta " & lblLabel2(Index).Caption)
'   txtText1(Index).Text = ""
'   txtText1(Index).SetFocus
'End If

'rsta.Close
'Set rsta = Nothing
'Exit Sub

'Err_Ejecutar:
'  Exit Sub
   
End Sub





