VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmrecursos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Selecci�n del Recurso"
   ClientHeight    =   5145
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0150.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5145
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   21
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Tipos de Recurso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6735
      Index           =   0
      Left            =   360
      TabIndex        =   18
      Top             =   720
      Width           =   11415
      Begin TabDlg.SSTab tabTab1 
         Height          =   6135
         HelpContextID   =   90001
         Index           =   0
         Left            =   240
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   360
         Width           =   10890
         _ExtentX        =   19209
         _ExtentY        =   10821
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0150.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(16)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(13)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(19)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "Frame1"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(24)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(12)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(11)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "chkCheck1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtminuto"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txthora"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtdia"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).ControlCount=   20
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0150.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtdia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   2640
            MaxLength       =   2
            TabIndex        =   10
            ToolTipText     =   "D�as de ocupaci�n"
            Top             =   4920
            Width           =   615
         End
         Begin VB.TextBox txthora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   3960
            MaxLength       =   2
            TabIndex        =   11
            ToolTipText     =   "Horas de ocupaci�n"
            Top             =   4920
            Width           =   615
         End
         Begin VB.TextBox txtminuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   5400
            MaxLength       =   2
            TabIndex        =   12
            ToolTipText     =   "Minutos de ocupaci�n"
            Top             =   4920
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   40101
            Index           =   2
            Left            =   3120
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Descripci�n Recurso Seleccionado"
            Top             =   3960
            Width           =   5400
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�El Tipo de Recurso tiene preferencia de uso sobre los dem�s?"
            DataField       =   "PR14INDRECPREFE"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   600
            TabIndex        =   14
            Tag             =   "�El Tipo de Recurso tiene preferencia sobre los dem�s?|�Tipo Recurso Preferente?"
            Top             =   5640
            Width           =   6375
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR14NUMUNIREC"
            Height          =   330
            HelpContextID   =   40101
            Index           =   11
            Left            =   600
            TabIndex        =   9
            Tag             =   "N�m.Unidades"
            Top             =   4920
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR14NUMMINOCU"
            Height          =   330
            HelpContextID   =   40101
            Index           =   12
            Left            =   7920
            TabIndex        =   17
            Tag             =   "Tiempo Ocupaci�n|Tiempo M�nimo de Ocupaci�n"
            Top             =   5640
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR14NUMMINDESREC"
            Height          =   330
            HelpContextID   =   40101
            Index           =   13
            Left            =   7920
            TabIndex        =   13
            Tag             =   "Desfase del Tipo de Recurso respecto al inicio de la fase"
            Top             =   4920
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   24
            Left            =   480
            TabIndex        =   15
            Tag             =   "C�d. Recurso Seleccionado"
            Top             =   3960
            Width           =   1092
         End
         Begin VB.Frame Frame1 
            Height          =   3135
            Left            =   360
            TabIndex        =   24
            Top             =   120
            Width           =   9615
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AD02CODDPTO"
               Height          =   330
               HelpContextID   =   40101
               Index           =   14
               Left            =   360
               TabIndex        =   7
               TabStop         =   0   'False
               Tag             =   "C�d.Dpto"
               Top             =   2520
               Width           =   400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   15
               Left            =   2400
               MultiLine       =   -1  'True
               TabIndex        =   8
               TabStop         =   0   'False
               Tag             =   "Descripci�n Departamento"
               Top             =   2520
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "AG14CODTIPRECU"
               Height          =   330
               HelpContextID   =   40101
               Index           =   6
               Left            =   360
               TabIndex        =   5
               TabStop         =   0   'False
               Tag             =   "Tipo de Recurso"
               Top             =   1920
               Width           =   400
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   7
               Left            =   2400
               MultiLine       =   -1  'True
               TabIndex        =   6
               TabStop         =   0   'False
               Tag             =   "Descripci�n Tipo de Recurso"
               Top             =   1920
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR14NUMNECESID"
               Height          =   330
               HelpContextID   =   40101
               Index           =   60
               Left            =   8040
               TabIndex        =   4
               Tag             =   "N�mero de Necesidad"
               Top             =   1200
               Visible         =   0   'False
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               Height          =   330
               Index           =   9
               Left            =   2400
               MultiLine       =   -1  'True
               TabIndex        =   3
               TabStop         =   0   'False
               Tag             =   "Descripci�n de la Fase"
               Top             =   1200
               Width           =   5400
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR06NUMFASE"
               Height          =   330
               HelpContextID   =   40101
               Index           =   8
               Left            =   360
               TabIndex        =   2
               TabStop         =   0   'False
               Tag             =   "N�mero de Fase"
               Top             =   1200
               Width           =   612
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               BackColor       =   &H00FFFFFF&
               DataField       =   "PR03NUMACTPEDI"
               Height          =   330
               HelpContextID   =   40101
               Index           =   0
               Left            =   360
               TabIndex        =   0
               TabStop         =   0   'False
               Tag             =   "Actuaci�n Pedida"
               Top             =   600
               Width           =   1092
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   1
               Left            =   2400
               MultiLine       =   -1  'True
               TabIndex        =   1
               TabStop         =   0   'False
               Tag             =   "Descripci�n Actuaci�n Pedida"
               Top             =   600
               Width           =   5400
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   15
               Left            =   2400
               TabIndex        =   33
               Top             =   2280
               Width           =   2295
            End
            Begin VB.Label lblLabel1 
               Caption         =   "C�d Departamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   17
               Left            =   360
               TabIndex        =   32
               Top             =   2280
               Width           =   1695
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Tipo de Recurso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   9
               Left            =   2400
               TabIndex        =   31
               Top             =   1680
               Width           =   2640
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Tipo de Recurso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   10
               Left            =   360
               TabIndex        =   30
               Top             =   1680
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Necesidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   12
               Left            =   8040
               TabIndex        =   29
               Top             =   960
               Visible         =   0   'False
               Width           =   975
            End
            Begin VB.Label lblLabel1 
               Caption         =   "N�m.Fase"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   11
               Left            =   360
               TabIndex        =   28
               Top             =   960
               Width           =   855
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Fase"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   0
               Left            =   2400
               TabIndex        =   27
               Top             =   960
               Width           =   1575
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Actuaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   2
               Left            =   2400
               TabIndex        =   26
               Top             =   360
               Width           =   2175
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Actuaci�n Pedida"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   1
               Left            =   360
               TabIndex        =   25
               Top             =   360
               Width           =   1575
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5850
            Index           =   0
            Left            =   -74880
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   120
            Width           =   9735
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17171
            _ExtentY        =   10319
            _StockProps     =   79
            Caption         =   "TIPOS DE RECURSO"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   8640
            TabIndex        =   44
            Top             =   5040
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   3360
            TabIndex        =   43
            Top             =   5040
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   4680
            TabIndex        =   42
            Top             =   5040
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   6120
            TabIndex        =   41
            Top             =   5040
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Recurso Seleccionado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   3120
            TabIndex        =   40
            Top             =   3720
            Width           =   3015
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Recurso Seleccionado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   480
            TabIndex        =   39
            Top             =   3720
            Width           =   2415
         End
         Begin VB.Label lblLabel1 
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   8640
            TabIndex        =   38
            Top             =   5760
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo M�nimo de Ocupaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   2640
            TabIndex        =   36
            Top             =   4680
            Width           =   2655
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� de Unidades"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   600
            TabIndex        =   35
            Top             =   4680
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Desfase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   7920
            TabIndex        =   34
            Top             =   4680
            Width           =   855
         End
      End
      Begin VB.Label lblLabel1 
         Caption         =   "minutos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   3480
         TabIndex        =   37
         Top             =   4440
         Width           =   735
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   20
      Top             =   4860
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   "Tipo Grupo"
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Nombre"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   32
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   1935
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmrecursos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS  (PETICI�N DE ACTUACIONES)                         *
'* NOMBRE:  PR00150.FRM                                                 *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Determinaci�n de un recurso concreto                    *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
'TiempoTotal calcula en minutos el tiempo introducido en dias-horas-minutos
Dim TiempoTotal As Variant
'intcambioalgo para detectar si se cambia alg�n campo
Dim intcambioalgo As Integer
'intnecrec controla el cambio de necesidad y cambio de recurso
Dim intnecrec As Integer
Dim intsalir As Integer




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Recursos"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR1400"
    .strWhere = "pr03numactpedi=" & frmdatosactpedida.txtText1(0).Text
    .intAllowance = cwAllowModify
    
    Call .FormAddOrderField("pr06numfase", cwAscending)
    Call .FormAddOrderField("pr14numnecesid", cwAscending)
    Call .FormAddOrderField("ag14codtiprecu", cwAscending)

    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "Tipos de Recurso")
        Call .FormAddFilterWhere(strKey, "AG14CODTIPRECU", "Tipo de Recurso", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AG11CODRECURSO", "Recurso Seleccionado", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR06NUMFASE", "N�mero de Fase", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR14NUMNECESID", "N�mero de Necesidad", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR14NUMUNIREC", "Cantidad de Tipo de Recurso", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR14NUMMINOCU", "Tiempo M�nimo de Ocupaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR14NUMMINDESREC", "Cantidad M�nima Destinada", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR14INDRECPREFE", "�Tiene prefernecia este tipo sobre los dem�s?", cwBoolean)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo de Departamento", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "AG14CODTIPRECU", "Tipo de Recurso")
        Call .FormAddFilterOrder(strKey, "PR06NUMFASE", "N�mero de Fase")
        Call .FormAddFilterOrder(strKey, "PR14NUMNECESID", "N�mero de Necesidad")
        Call .FormAddFilterOrder(strKey, "PR14NUMUNIREC", "Cantidad de Tipo de Recurso")
        Call .FormAddFilterOrder(strKey, "PR14NUMMINOCU", "Tiempo M�nimo de Ocupaci�n")
        Call .FormAddFilterOrder(strKey, "PR14NUMMINDESREC", "Cantidad M�nima Destinada")
        Call .FormAddFilterOrder(strKey, "PR14INDRECPREFE", "�Tiene prefernecia este tipo sobre los dem�s?")
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo de Departamento")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
     'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(8)).blnInFind = True
        .CtrlGetInfo(txtText1(60)).blnInFind = True
        .CtrlGetInfo(txtText1(6)).blnInFind = True
        .CtrlGetInfo(txtText1(11)).blnInFind = True
        .CtrlGetInfo(txtText1(12)).blnInFind = True
        .CtrlGetInfo(txtText1(13)).blnInFind = True
        .CtrlGetInfo(txtText1(14)).blnInFind = True
        .CtrlGetInfo(chkCheck1(0)).blnInFind = True
        
        .CtrlGetInfo(txtText1(0)).blnReadOnly = True
        .CtrlGetInfo(txtText1(8)).blnReadOnly = True
        .CtrlGetInfo(txtText1(6)).blnReadOnly = True
        .CtrlGetInfo(txtText1(14)).blnReadOnly = True
        .CtrlGetInfo(txtText1(60)).blnReadOnly = True
        '.CtrlGetInfo(txtText1(24)).blnReadOnly = True
        
        .CtrlGetInfo(txtdia).blnNegotiated = False
        .CtrlGetInfo(txthora).blnNegotiated = False
        .CtrlGetInfo(txtminuto).blnNegotiated = False
        .CtrlGetInfo(txtText1(1)).blnNegotiated = False
        
        .CtrlGetInfo(txtText1(24)).blnForeign = True
        
        
        'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "PR03NUMACTPEDI", "SELECT * FROM PR0300 WHERE PR03NUMACTPEDI = ?")
        'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(1), "PR03DESCORTA")
        
        'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "PR03NUMACTPEDI", "SELECT * FROM PR0300,PR0100 WHERE PR0300.PR03NUMACTPEDI = ?  AND PR0300.PR01CODACTUACION=PR0100.PR01CODACTUACION")
        'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(1), "PR0100.PR01DESCORTA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "PR03NUMACTPEDI", _
               "SELECT * " & _
               "  FROM PR0500 " & _
               " WHERE PR05NUMFASE = ? " & _
               "   AND PR01CODACTUACION IN (SELECT PR01CODACTUACION " & _
                                           "  FROM PR0300 " & _
                                           " WHERE PR03NUMACTPEDI = " & frmdatosactpedida.txtText1(0).Text & ")")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "PR05DESFASE")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "AG14CODTIPRECU", "SELECT * FROM AG1400 WHERE AG14CODTIPRECU = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "AG14DESTIPRECU")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "AD02DESDPTO")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(24)), "AG11CODRECURSO", "SELECT * FROM AG1100 WHERE AG11CODRECURSO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(24)), txtText1(2), "AG11DESRECURSO")
   
        
      

    Call .WinRegister
    Call .WinStabilize
  End With
  'se inicializa la variable intcambioalgo
  intcambioalgo = 0

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)

  
Dim strupdate As String
Dim strsql1 As String
Dim rsta1 As rdoResultset
Dim strsql As String
Dim rsta As rdoResultset
Dim stract As String
Dim rstact As rdoResultset
Dim strBuscar As String
Dim rstBuscar As rdoResultset
Dim strcambio As String
Dim totalminutos As Integer

'despu�s de escribir intcambioalgo se pone a cero
intcambioalgo = 0

'se controla (lo mismo que en LostFocus de la caja de texto) que el recurso que se
'ha introducido sea v�lido, de lo contrario como ya lo habr� guardado se hace un
'Update y se introduce null en el recurso.
If txtText1(0).Text <> "" And txtText1(8).Text <> "" And _
   txtText1(60).Text <> "" And txtText1(6).Text <> "" Then
   
stract = "select pr01codactuacion from PR0300 where pr03numactpedi=" & txtText1(0).Text
Set rstact = objApp.rdoConnect.OpenResultset(stract)

If txtText1(24).Text <> "" Then
  If txtText1(14).Text <> "" Then 'el tipo de recurso tiene dpto. asociado
      strsql1 = "select count(*) " & _
              "  from PR1100 " & _
              " where pr01codactuacion=" & rstact.rdoColumns(0).Value & _
              "  and pr05numfase=" & txtText1(8).Text & _
              "  and pr13numnecesid=" & txtText1(60).Text & _
              "  and ag11codrecurso=" & txtText1(24).Text & _
              "  and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                         " where ag14codtiprecu=" & txtText1(6).Text & _
                                         "   and ad02coddpto=" & txtText1(14).Text & ")"
      Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)

      If rsta1.rdoColumns(0).Value = 0 Then
        strsql = "SELECT count(*) FROM AG1100 WHERE ag14codtiprecu=" & txtText1(6).Text & _
                 " AND ad02coddpto=" & txtText1(14).Text & _
                 " AND ag11codrecurso=" & txtText1(24).Text
        Set rsta = objApp.rdoConnect.OpenResultset(strsql)
        If rsta.rdoColumns(0).Value = 0 Then
            strupdate = "UPDATE PR1400 SET ag11codrecurso=null " & _
                         "WHERE pr14numnecesid=" & txtText1(60).Text
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
            tlbToolbar1.Buttons(4).Enabled = False
        End If
      Else
        strsql = "SELECT count(*) FROM PR1101J WHERE pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                  " and pr05numfase=" & txtText1(8).Text & _
                  " and pr13numnecesid=" & txtText1(60).Text & _
                  " and ag11codrecurso=" & txtText1(24).Text & _
                  " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                           " where ag14codtiprecu=" & txtText1(6).Text & _
                                           " and ad02coddpto=" & txtText1(14).Text & ")"
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      If rsta.rdoColumns(0).Value = 0 Then
          strupdate = "UPDATE PR1400 SET ag11codrecurso=null " & _
                         "WHERE pr14numnecesid=" & txtText1(60).Text
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
            tlbToolbar1.Buttons(4).Enabled = False
      End If
      rsta.Close
      Set rsta = Nothing
      rsta1.Close
      Set rsta1 = Nothing
    End If
  Else 'el tipo de recurso no tiene dpto.asociado
      strsql1 = "select count(*) " & _
              "  from PR1100 " & _
              " where pr01codactuacion=" & rstact.rdoColumns(0).Value & _
              "  and pr05numfase=" & txtText1(8).Text & _
              "  and pr13numnecesid=" & txtText1(60).Text & _
              "  and ag11codrecurso=" & txtText1(24).Text & _
              "  and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                         " where ag14codtiprecu=" & txtText1(6).Text & ")"
      Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)

      If rsta1.rdoColumns(0).Value = 0 Then
        strsql = "SELECT count(*) FROM AG1100 WHERE ag14codtiprecu=" & txtText1(6).Text & _
                 " AND ag11codrecurso=" & txtText1(24).Text
        Set rsta = objApp.rdoConnect.OpenResultset(strsql)
        If rsta.rdoColumns(0).Value = 0 Then
            strupdate = "UPDATE PR1400 SET ag11codrecurso=null " & _
                         "WHERE pr14numnecesid=" & txtText1(60).Text
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
            tlbToolbar1.Buttons(4).Enabled = False
        End If
      Else
        strsql = "SELECT count(*) FROM PR1101J WHERE pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                  " and pr05numfase=" & txtText1(8).Text & _
                  " and pr13numnecesid=" & txtText1(60).Text & _
                  " and ag11codrecurso=" & txtText1(24).Text & _
                  " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                           " where ag14codtiprecu=" & txtText1(6).Text & ")"
        Set rsta = objApp.rdoConnect.OpenResultset(strsql)
        If rsta.rdoColumns(0).Value = 0 Then
            strupdate = "UPDATE PR1400 SET ag11codrecurso=null " & _
                         "WHERE pr14numnecesid=" & txtText1(60).Text
            objApp.rdoConnect.Execute strupdate, 64
            objApp.rdoConnect.Execute "Commit", 64
            tlbToolbar1.Buttons(4).Enabled = False
        End If
      End If
      rsta.Close
      Set rsta = Nothing
      rsta1.Close
      Set rsta1 = Nothing

  End If
End If
rstact.Close
Set rstact = Nothing
End If

'**************************************************************************jcr7/8/98
'Esta estructura se crea al realizar
'************************************************************************************

''METER LA INFORMACION EN PR1000
'strBuscar = "SELECT pr04numactplan FROM PR0400 WHERE pr03numactpedi=" & txtText1(0).Text
'Set rstBuscar = objApp.rdoConnect.OpenResultset(strBuscar)
'While Not rstBuscar.EOF
'      totalminutos = (txtdia.Text * 1440) + (txthora.Text * 60) + txtminuto.Text
'      If txtText1(24).Text <> "" Then
'          strcambio = "UPDATE PR1000 SET ag11codrecurso=" & txtText1(24).Text & "," & _
'                                       "pr10numunirec=" & txtText1(11).Text & "," & _
'                                       "pr10numminocurec=" & totalminutos & "," & _
'                                       "ag11codrecurso_pre=" & txtText1(24).Text & _
'                    " WHERE pr04numactplan=" & rstBuscar.rdoColumns(0).Value & _
'                    " AND pr07numfase=" & txtText1(8).Text & _
'                    " AND pr10numnecesid=" & txtText1(60).Text
'      Else
'          strcambio = "UPDATE PR1000 SET ag11codrecurso=null" & "," & _
'                                       "pr10numunirec=" & txtText1(11).Text & "," & _
'                                       "pr10numminocurec=" & totalminutos & "," & _
'                                       "ag11codrecurso_pre=null" & _
'                    " WHERE pr04numactplan=" & rstBuscar.rdoColumns(0).Value & _
'                    " AND pr07numfase=" & txtText1(8).Text & _
'                    " AND pr10numnecesid=" & txtText1(60).Text
'      End If
'      objApp.rdoConnect.Execute strcambio, 64
'      objApp.rdoConnect.Execute "Commit", 64
'      rstBuscar.MoveNext
'Wend
'rstBuscar.Close
'Set rstBuscar = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Empleados" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
 Dim objField As clsCWFieldSearch
 Dim rsta As rdoResultset
 Dim strsql As String
 Dim rsta1 As rdoResultset
 Dim strsql1 As String
 
If strFormName = "Recursos" And strCtrl = "txtText1(24)" Then
  Set objSearch = New clsCWSearch
  With objSearch
    
  If txtText1(14).Text <> "" Then  'el tipo de recurso tiene dpto asociado
    strsql = "select pr01codactuacion from PR0300 where pr03numactpedi=" & txtText1(0)
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    strsql1 = "select count(*) " & _
              "  from PR1100 " & _
              " where pr01codactuacion=" & rsta.rdoColumns(0).Value & _
              "   and pr05numfase=" & txtText1(8).Text & _
              "   and pr13numnecesid=" & txtText1(60).Text & _
              "   and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                         " where ag14codtiprecu=" & txtText1(6).Text & _
                                         "   and ad02coddpto=" & txtText1(14).Text & ")"

    Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)
    If rsta1.rdoColumns(0).Value = 0 Then
     .strTable = "AG1100"
     .strWhere = "WHERE ag14codtiprecu=" & txtText1(6).Text & _
                 "  AND ad02coddpto=" & txtText1(14).Text
     Else
      .strTable = "PR1101J"
      .strWhere = "WHERE pr01codactuacion=" & rsta.rdoColumns(0).Value & _
                  " and pr05numfase=" & txtText1(8).Text & _
                  " and pr13numnecesid=" & txtText1(60).Text & _
                  " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                           " where ag14codtiprecu=" & txtText1(6).Text & _
                                           "   and ad02coddpto=" & txtText1(14).Text & ")"
    End If
    
  Else 'el tipo de recurso no tiene dpto asociado
    strsql = "select pr01codactuacion from PR0300 where pr03numactpedi=" & txtText1(0)
    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
    strsql1 = "select count(*) " & _
              "  from PR1100 " & _
              " where pr01codactuacion=" & rsta.rdoColumns(0).Value & _
              "   and pr05numfase=" & txtText1(8).Text & _
              "   and pr13numnecesid=" & txtText1(60).Text & _
              "   and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                         " where ag14codtiprecu=" & txtText1(6).Text & ")"

    Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)
    If rsta1.rdoColumns(0).Value = 0 Then
     .strTable = "AG1100"
     .strWhere = "WHERE ag14codtiprecu=" & txtText1(6).Text
     Else
      .strTable = "PR1101J"
      .strWhere = "WHERE pr01codactuacion=" & rsta.rdoColumns(0).Value & _
                  " and pr05numfase=" & txtText1(8).Text & _
                  " and pr13numnecesid=" & txtText1(60).Text & _
                  " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                           " where ag14codtiprecu=" & txtText1(6).Text & ")"
    End If
    rsta.Close
    Set rsta = Nothing
    rsta1.Close
    Set rsta1 = Nothing
  End If
    
  Set objField = .AddField("ag11codrecurso")
  objField.strSmallDesc = "C�digo del Recurso"
         
  Set objField = .AddField("ag11desrecurso")
  objField.strSmallDesc = "Descripci�n del Recurso "
         
  If .Search Then
    Call objWinInfo.CtrlSet(txtText1(24), .cllValues("ag11codrecurso"))
  End If
  End With
 End If
 
 Set objSearch = Nothing
 
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

Dim rstTiempo As rdoResultset
Dim strTiempo As String

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(8).Text <> "" And _
    txtText1(6).Text <> "" And txtText1(60).Text <> "" Then
  strTiempo = "SELECT count(*) FROM PR1400 where pr03numactpedi=" & txtText1(0).Text & _
         " AND pr06numfase=" & txtText1(8).Text & _
         " AND pr14numnecesid=" & txtText1(60).Text & _
         " AND ag14codtiprecu=" & txtText1(6) & _
         " AND pr14numminocu=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
    Call objWinInfo.CtrlSet(txtText1(12), TiempoTotal)
  End If
  rstTiempo.Close
  Set rstTiempo = Nothing
End If

'intcambio algo dice si se ha cambiado algo en alguna caja de texto o en la Check para
'que se detecten los cambios o no.
If intcambioalgo = 1 Then
  objWinInfo.objWinActiveForm.blnChanged = True
End If

'se llama a la funci�n recurso_no_valido que si es igual a 1 indica que no se
'permite el recurso introducido
If recurso_no_valido = 0 Then
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If

'como Localizar no activa los Change de las cajas de texto no actualiza bien
'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
If btnButton.Index = 16 Then
    Call txtText1_Change(12)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  'intcambio algo dice si se ha cambiado algo en alguna caja de texto o en la Check para
  'que se detecten los cambios o no.
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If recurso_no_valido = 0 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  'intcambio algo dice si se ha cambiado algo en alguna caja de texto o en la Check para
  'que se detecten los cambios o no.
  If intcambioalgo = 1 Then
   objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If recurso_no_valido = 0 Then
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  End If
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  'intcambio algo dice si se ha cambiado algo en alguna caja de texto o en la Check para
  'que se detecten los cambios o no.
  If intcambioalgo = 1 Then
   objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If recurso_no_valido = 0 Then
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  End If
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  'intcambio algo dice si se ha cambiado algo en alguna caja de texto o en la Check para
  'que se detecten los cambios o no.
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If recurso_no_valido = 0 Then
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If
  
   'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If intIndex = 10 Then
    Call txtText1_Change(12)
  End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  'intcambio algo dice si se ha cambiado algo en alguna caja de texto o en la Check para
  'que se detecten los cambios o no.
  If intcambioalgo = 1 Then
   objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If recurso_no_valido = 0 Then
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  End If
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  intcambioalgo = 1
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtdia_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
  If (Index = 11 Or Index = 13 Or Index = 24) Then
    intcambioalgo = 1
  End If
  If Index = 24 Then
    intnecrec = intnecrec + 1
  End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim lanzarmensaje As String
Dim strsql1 As String
Dim rsta1 As rdoResultset
Dim strsql As String
Dim rsta As rdoResultset
Dim stract As String
Dim rstact As rdoResultset

Call objWinInfo.CtrlLostFocus

If intnecrec = 1 And intIndex = 24 And intsalir = 0 Then
  intsalir = 1
  Exit Sub
End If


'hay que controlar que el recurso introducido sea v�lido
If intIndex = 24 Then 'c�digo del recurso

    If txtText1(0).Text <> "" And txtText1(8).Text <> "" And _
       txtText1(60).Text <> "" And txtText1(6).Text <> "" Then
       
        stract = "select pr01codactuacion from PR0300 where pr03numactpedi=" & txtText1(0).Text
        Set rstact = objApp.rdoConnect.OpenResultset(stract)
        
        If txtText1(24).Text <> "" Then
          If txtText1(14).Text <> "" Then 'el tipo de recurso tiene dpto. asociado
              strsql1 = "select count(*) " & _
                      "  from PR1100 " & _
                      " where pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                      "  and pr05numfase=" & txtText1(8).Text & _
                      "  and pr13numnecesid=" & txtText1(60).Text & _
                      "  and ag11codrecurso=" & txtText1(24).Text & _
                      "  and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                                 " where ag14codtiprecu=" & txtText1(6).Text & _
                                                 "   and ad02coddpto=" & txtText1(14).Text & ")"
              Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)
        
              If rsta1.rdoColumns(0).Value = 0 Then
                    strsql = "SELECT count(*) FROM AG1100 WHERE ag14codtiprecu=" & txtText1(6).Text & _
                             " AND ad02coddpto=" & txtText1(14).Text & _
                             " AND ag11codrecurso=" & txtText1(24).Text
                    Set rsta = objApp.rdoConnect.OpenResultset(strsql)
                    If rsta.rdoColumns(0).Value = 0 Then
                        lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
                        txtText1(24).Text = ""
                        txtText1(24).SetFocus
                    End If
                    rsta.Close
                    Set rsta = Nothing
              Else
                 strsql = "SELECT count(*) FROM PR1101J WHERE pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                           " and pr05numfase=" & txtText1(8).Text & _
                           " and pr13numnecesid=" & txtText1(60).Text & _
                           " and ag11codrecurso=" & txtText1(24).Text & _
                           " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                                    " where ag14codtiprecu=" & txtText1(6).Text & _
                                                    " and ad02coddpto=" & txtText1(14).Text & ")"
                Set rsta = objApp.rdoConnect.OpenResultset(strsql)
                If rsta.rdoColumns(0).Value = 0 Then
                   lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
                   txtText1(24).Text = ""
                   txtText1(24).SetFocus
                End If
                rsta.Close
                Set rsta = Nothing
              End If
          Else 'el tipo de recurso no tiene dpto.asociado
               strsql1 = "select count(*) " & _
                      "  from PR1100 " & _
                      " where pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                      "  and pr05numfase=" & txtText1(8).Text & _
                      "  and pr13numnecesid=" & txtText1(60).Text & _
                      "  and ag11codrecurso=" & txtText1(24).Text & _
                      "  and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                                 " where ag14codtiprecu=" & txtText1(6).Text & ")"
              Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)
        
              If rsta1.rdoColumns(0).Value = 0 Then
                strsql = "SELECT count(*) FROM AG1100 WHERE ag14codtiprecu=" & txtText1(6).Text & _
                         " AND ag11codrecurso=" & txtText1(24).Text
                Set rsta = objApp.rdoConnect.OpenResultset(strsql)
                If rsta.rdoColumns(0).Value = 0 Then
                    lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
                    txtText1(24).Text = ""
                    txtText1(24).SetFocus
                End If
                rsta.Close
                Set rsta = Nothing
              Else
                strsql = "SELECT count(*) FROM PR1101J WHERE pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                          " and pr05numfase=" & txtText1(8).Text & _
                          " and pr13numnecesid=" & txtText1(60).Text & _
                          " and ag11codrecurso=" & txtText1(24).Text & _
                          " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                                   " where ag14codtiprecu=" & txtText1(6).Text & ")"
                Set rsta = objApp.rdoConnect.OpenResultset(strsql)
                If rsta.rdoColumns(0).Value = 0 Then
                  lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
                  txtText1(24).Text = ""
                  txtText1(24).SetFocus
                End If
                rsta.Close
                Set rsta = Nothing
              End If
          End If
          rsta1.Close
          Set rsta1 = Nothing
        End If
        rstact.Close
        Set rstact = Nothing
    End If
End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
 
  Dim rstactuacion As rdoResultset
  Dim stractuacion As String
  
  
  If intIndex = 60 Then 'necesidad
      intnecrec = 0
      intsalir = 0
  End If
  
  
  
  If intIndex = 0 Then
     If txtText1(0).Text <> "" Then
        stractuacion = "select pr0100.pr01descorta from PR0300,PR0100 where PR0300.PR01CODACTUACION=PR0100.PR01CODACTUACION and PR0300.PR03NUMACTPEDI=" & txtText1(0).Text
        Set rstactuacion = objApp.rdoConnect.OpenResultset(stractuacion)
        txtText1(1).Text = rstactuacion.rdoColumns(0).Value
        txtText1(1).Locked = True
        txtText1(1).BackColor = &HC0C0C0
        rstactuacion.Close
        Set rstactuacion = Nothing
      End If
  End If

  Call objWinInfo.CtrlDataChange
  
  'si la necesidad cambia
  If intIndex = 60 Then
    intcambioalgo = 0
  End If
  
   If intIndex = 12 Then
    If txtText1(12).Text = "" Then
      txtdia.Text = 0
      txthora.Text = 0
      txtminuto.Text = 0
    Else
      txtdia.Text = txtText1(12).Text \ 1440                 'd�as
      txthora.Text = (txtText1(12).Text Mod 1440) \ 60       'horas
      txtminuto.Text = (txtText1(12).Text Mod 1440) Mod 60   'minutos
    End If
    If TiempoTotal <> 0 Then
      'txtdia.Text = TiempoTotal \ 1440                 'd�as
      'txthora.Text = (TiempoTotal Mod 1440) \ 60       'horas
      'txtminuto.Text = (TiempoTotal Mod 1440) Mod 60   'minutos
    End If
  End If
End Sub

Private Sub txtdia_Change()

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia.Text) = False Then
    Beep
    txtdia.Text = ""
    txtdia.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 65 Then
      txtdia.Text = 65
     End If
  End If
End Sub

Private Sub txthora_Change()
   
  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora.Text) = False Then
    Beep
    txthora.Text = ""
    txthora.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txtminuto_Change()

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto.Text) = False Then
    Beep
    txtminuto.Text = ""
    txtminuto.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub


Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 65 Then
    d = 65
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If

TiempoTotal = (d * 1440) + (h * 60) + m
  
End Sub

Public Function recurso_no_valido() As Integer
'funci�n que hace lo mismo que el LostFocus de la caja de texto del recurso.
'Si el recurso no es v�lido pone la funci�n recurso_no_valido=1

Dim lanzarmensaje As String
Dim strsql1 As String
Dim rsta1 As rdoResultset
Dim strsql As String
Dim rsta As rdoResultset
Dim stract As String
Dim rstact As rdoResultset

recurso_no_valido = 0

If txtText1(0).Text <> "" And txtText1(8).Text <> "" And _
   txtText1(60).Text <> "" And txtText1(6).Text <> "" Then
   
    stract = "select pr01codactuacion from PR0300 where pr03numactpedi=" & txtText1(0).Text
    Set rstact = objApp.rdoConnect.OpenResultset(stract)
    
    If txtText1(24).Text <> "" Then
      If txtText1(14).Text <> "" Then 'el tipo de recurso tiene dpto. asociado
          strsql1 = "select count(*) " & _
                  "  from PR1100 " & _
                  " where pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                  "  and pr05numfase=" & txtText1(8).Text & _
                  "  and pr13numnecesid=" & txtText1(60).Text & _
                  "  and ag11codrecurso=" & txtText1(24).Text & _
                  "  and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                             " where ag14codtiprecu=" & txtText1(6).Text & _
                                             "   and ad02coddpto=" & txtText1(14).Text & ")"
          Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)
    
          If rsta1.rdoColumns(0).Value = 0 Then
            strsql = "SELECT count(*) FROM AG1100 WHERE ag14codtiprecu=" & txtText1(6).Text & _
                     " AND ad02coddpto=" & txtText1(14).Text & _
                     " AND ag11codrecurso=" & txtText1(24).Text
            Set rsta = objApp.rdoConnect.OpenResultset(strsql)
            If rsta.rdoColumns(0).Value = 0 Then
                lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
                txtText1(24).Text = ""
                txtText1(24).SetFocus
                recurso_no_valido = 1
            End If
            rsta.Close
            Set rsta = Nothing
          Else
            strsql = "SELECT count(*) FROM PR1101J WHERE pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                      " and pr05numfase=" & txtText1(8).Text & _
                      " and pr13numnecesid=" & txtText1(60).Text & _
                      " and ag11codrecurso=" & txtText1(24).Text & _
                      " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                               " where ag14codtiprecu=" & txtText1(6).Text & _
                                               " and ad02coddpto=" & txtText1(14).Text & ")"
            Set rsta = objApp.rdoConnect.OpenResultset(strsql)
            If rsta.rdoColumns(0).Value = 0 Then
               lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
               txtText1(24).Text = ""
               txtText1(24).SetFocus
               recurso_no_valido = 1
            End If
            rsta.Close
            Set rsta = Nothing
          End If
          rsta1.Close
          Set rsta1 = Nothing
      Else 'el tipo de recurso no tiene dpto.asociado
          strsql1 = "select count(*) " & _
                  "  from PR1100 " & _
                  " where pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                  "  and pr05numfase=" & txtText1(8).Text & _
                  "  and pr13numnecesid=" & txtText1(60).Text & _
                  "  and ag11codrecurso=" & txtText1(24).Text & _
                  "  and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                             " where ag14codtiprecu=" & txtText1(6).Text & ")"
          Set rsta1 = objApp.rdoConnect.OpenResultset(strsql1)
    
          If rsta1.rdoColumns(0).Value = 0 Then
            strsql = "SELECT count(*) FROM AG1100 WHERE ag14codtiprecu=" & txtText1(6).Text & _
                     " AND ag11codrecurso=" & txtText1(24).Text
            Set rsta = objApp.rdoConnect.OpenResultset(strsql)
            If rsta.rdoColumns(0).Value = 0 Then
                lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
                txtText1(24).Text = ""
                txtText1(24).SetFocus
                recurso_no_valido = 1
            End If
            rsta.Close
            Set rsta = Nothing
          Else
            strsql = "SELECT count(*) FROM PR1101J WHERE pr01codactuacion=" & rstact.rdoColumns(0).Value & _
                      " and pr05numfase=" & txtText1(8).Text & _
                      " and pr13numnecesid=" & txtText1(60).Text & _
                      " and ag11codrecurso=" & txtText1(24).Text & _
                      " and ag11codrecurso in  (select ag11codrecurso from AG1100" & _
                                               " where ag14codtiprecu=" & txtText1(6).Text & ")"
            Set rsta = objApp.rdoConnect.OpenResultset(strsql)
            If rsta.rdoColumns(0).Value = 0 Then
              lanzarmensaje = MsgBox("El recurso no es v�lido", vbInformation, "Aviso")
              txtText1(24).Text = ""
              txtText1(24).SetFocus
              recurso_no_valido = 1
            End If
            rsta.Close
            Set rsta = Nothing
          End If
          rsta1.Close
          Set rsta1 = Nothing
      End If
    End If
    rstact.Close
    Set rstact = Nothing
End If

End Function
