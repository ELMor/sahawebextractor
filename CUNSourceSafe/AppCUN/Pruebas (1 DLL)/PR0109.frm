VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmCuestActuacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaci�n. Cuestionario"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   Icon            =   "PR0109.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Respuestas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Index           =   2
      Left            =   360
      TabIndex        =   7
      Top             =   5400
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2175
         Index           =   1
         Left            =   120
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   3
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Caption=   "Lista Respuestas"
         Columns(0).Name =   "Lista Respuestas"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "N� Respuesta"
         Columns(1).Name =   "N� Respuesta"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   11800
         Columns(2).Caption=   "Respuesta"
         Columns(2).Name =   "Respuesta"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   18785
         _ExtentY        =   3836
         _StockProps     =   79
         Caption         =   "RESPUESTAS"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Cuestionario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3705
      Index           =   1
      Left            =   360
      TabIndex        =   9
      Top             =   1680
      Width           =   10935
      Begin TabDlg.SSTab tabTab1 
         Height          =   3180
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   10650
         _ExtentX        =   18785
         _ExtentY        =   5609
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0109.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lbllabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lbllabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lbllabel1(2)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cboSSDBCombo1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "cboSSDBCombo1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "chkCheck1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtDesResp"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtDesLista"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0109.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtDesLista 
            Height          =   330
            Left            =   1800
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Texto de la Pregunta"
            Top             =   2520
            Width           =   3240
         End
         Begin VB.TextBox txtDesResp 
            Height          =   330
            Left            =   5040
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Texto de la Pregunta"
            Top             =   480
            Width           =   3240
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   3
            Left            =   600
            TabIndex        =   3
            Tag             =   "Texto de la Pregunta"
            Top             =   1440
            Width           =   9000
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR40CODPREGUNTA"
            Height          =   330
            Index           =   2
            Left            =   600
            TabIndex        =   0
            Tag             =   "C�digo de la Pregunta"
            Top             =   480
            Width           =   372
         End
         Begin VB.CheckBox chkCheck1 
            DataField       =   "PR29INDROBLIG"
            Height          =   255
            Index           =   0
            Left            =   5880
            TabIndex        =   8
            Tag             =   "� La respuesta es Obligatoria ?"
            Top             =   2520
            Width           =   255
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   4
            Left            =   4800
            TabIndex        =   17
            Tag             =   "C�digo de la Actuaci�n"
            Top             =   1080
            Visible         =   0   'False
            Width           =   1815
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2880
            Index           =   0
            Left            =   -74880
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   120
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17595
            _ExtentY        =   5080
            _StockProps     =   79
            Caption         =   "CUESTIONARIO"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR27CODTIPRESPU"
            Height          =   330
            Index           =   0
            Left            =   4200
            TabIndex        =   1
            Tag             =   "Tipo de la Respuesta"
            Top             =   480
            Width           =   660
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   873
            Columns(0).Caption=   "N�M"
            Columns(0).Name =   "N�MERO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   9525
            Columns(1).Caption=   "DESCRIPCI�N"
            Columns(1).Name =   "DESCRIPCI�N"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1164
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR46CODLISTRESP"
            Height          =   330
            Index           =   1
            Left            =   600
            TabIndex        =   4
            Tag             =   "Respuesta"
            Top             =   2520
            Width           =   1100
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   900
            Columns(0).Caption=   "C�d."
            Columns(0).Name =   "C�d."
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7197
            Columns(1).Caption=   "Lista de Respuestas"
            Columns(1).Name =   "Lista de Respuestas"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1940
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            Caption         =   "C�digo Pregunta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   600
            TabIndex        =   22
            Top             =   240
            Width           =   1575
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Texto de la Pregunta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   600
            TabIndex        =   21
            Top             =   1200
            Width           =   1935
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Lista de Respuestas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   600
            TabIndex        =   20
            Top             =   2280
            Width           =   1815
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Tipo de la Respuesta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   4200
            TabIndex        =   19
            Top             =   240
            Width           =   2295
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Respuesta Obligatoria (si/no)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   6120
            TabIndex        =   6
            Top             =   2520
            Width           =   2535
         End
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1125
      Index           =   0
      Left            =   360
      TabIndex        =   14
      Top             =   480
      Width           =   10935
      Begin VB.TextBox txtCuestext1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   0
         Left            =   480
         TabIndex        =   24
         TabStop         =   0   'False
         Tag             =   "C�digo de la Actuaci�n"
         Top             =   600
         Width           =   1092
      End
      Begin VB.TextBox txtCuestext1 
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR01DESACTUACION"
         Height          =   330
         Index           =   1
         Left            =   2880
         TabIndex        =   23
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   600
         Width           =   5400
      End
      Begin VB.Label lblactlabel1 
         Caption         =   "Descripci�n Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2880
         TabIndex        =   16
         Top             =   360
         Width           =   2655
      End
      Begin VB.Label lblactlabel1 
         Caption         =   "C�digo Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   15
         Top             =   360
         Width           =   1815
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   12
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmCuestActuacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00109.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 14 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: permite crear el cuestionario de actuaciones.           *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim refrescar As Boolean
Dim gintIndice As Integer


Private Sub Mostrar_Respuestas(strCodLista As String)
  'Este procedimiento sirve para llenar el grid que muestra las respuestas
  Dim strSelect As String
  Dim rsta As rdoResultset

  If strCodLista <> "" Then
    'Hay que llener el grid que contiene la respuestas
    strSelect = "SELECT PR46CODLISTRESP,PR28NUMRESPUESTA, PR28DESRESPUESTA " _
             & " FROM PR2800 " _
             & " WHERE PR46CODLISTRESP = " & strCodLista _
             & " ORDER BY PR28NUMRESPUESTA"
             
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    grdDBGrid1(1).RemoveAll
    Do While Not rsta.EOF
      grdDBGrid1(1).AddNew
      grdDBGrid1(1).Columns(0).Value = rsta.rdoColumns(0).Value
      grdDBGrid1(1).Columns(1).Value = rsta.rdoColumns(1).Value
      grdDBGrid1(1).Columns(2).Value = rsta.rdoColumns(2).Value
      grdDBGrid1(1).Update
      rsta.MoveNext
    Loop
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.DataSave
    rsta.Close
    Set rsta = Nothing
  Else
    'Hay que limpiar el grid que contiene las respuestas
    grdDBGrid1(1).RemoveAll
  End If
  
End Sub
'jcr 17/3/98
'Private Sub cmdpreguntas_Click()
''    cmdpreguntas.Enabled = False
''    guCodigo = ""
''    Load frmPreguntas
'''    frmPreguntas.Show (vbModal)
''    Unload frmPreguntas
''    Set frmPreguntas = Nothing

''   If guCodigo <> "" Then
''    txtText1(2) = guCodigo
''    objWinInfo.objWinActiveForm.blnChanged = True
''  End If
''  guCodigo = ""
''  cmdpreguntas.Enabled = True
'
'  cmdpreguntas.Enabled = False
'  gstrClausula = " PR40CODPREGUNTA NOT IN " & _
'                 "(SELECT PR40CODPREGUNTA " & _
'                 " FROM PR2900 " & _
'                 " WHERE PR01CODACTUACION = " & txtCuestext1(0).Text & ")"
'
'
'  'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), "")
'  Call objWinInfo.CtrlSet(txttext1(2), "")
'
'  guCodigo = ""
'  frmPreguntas.cmdaceptar.Visible = True
'  'Load frmPreguntas
'  'frmPreguntas.Show (vbModal)
'  'Unload frmPreguntas
'  'Set frmPreguntas = Nothing
'  Call objsecurity.LaunchProcess("PR0131")
'  If guCodigo <> "" Then
'    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), guCodigo)
'    Call objWinInfo.CtrlSet(txttext1(2), guCodigo)
'  End If
'  cmdpreguntas.Enabled = True
'  txttext1(2).SetFocus
'  guCodigo = ""
'  gstrClausula = ""

'End Sub

Private Sub Form_Activate()
  grdDBGrid1(1).ZOrder (0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    cboSSDBCombo1(1).Enabled = False
  Else
    cboSSDBCombo1(0).Enabled = True
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
    With objMasterInfo
        .strName = "Cuestionario_Actuaci�n"
        Set .objFormContainer = fraFrame1(1)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(0)
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR2900"
        .strWhere = "PR01CODACTUACION=" & frmdefactuacionesCUN.txttext1(0).Text
    
        Call .FormAddOrderField("PR40CODPREGUNTA", cwAscending)
    
        .blnHasMaint = True
        .blnMasive = False
    
        Call .objPrinter.Add("PR1091", "Listado del Cuestionario de la Actuaci�n")
        
        strKey = .strDataBase & .strTable
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Cuestionario de Actuaci�n")
        Call .FormAddFilterWhere(strKey, "PR40CODPREGUNTA", "C�digo Pregunta ", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR29INDOBLIG", "�Obligatoria?", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR40CODPREGUNTA", "C�digo Pregunta")
        Call .FormAddFilterOrder(strKey, "PR29INDOBLIG", "�Obligatoria?")
   
    
    End With
  
  
    With objWinInfo
        Call .FormAddInfo(objMasterInfo, cwFormDetail)
                
        Call .FormCreateInfo(objMasterInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txttext1(2)).blnInFind = True
        .CtrlGetInfo(txttext1(3)).blnInFind = True
        .CtrlGetInfo(chkCheck1(0)).blnInFind = True
        
        'Creamos los buscadores Search
        .CtrlGetInfo(txttext1(2)).blnForeign = True
        .CtrlGetInfo(cboSSDBCombo1(0)).blnForeign = True
        .CtrlGetInfo(cboSSDBCombo1(1)).blnForeign = True
        
        .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR27CODTIPRESPU,PR27DESTIPRESPU FROM PR2700 order by PR27CODTIPRESPU asc"
        .CtrlGetInfo(cboSSDBCombo1(1)).strsql = "SELECT PR46CODLISTRESP,PR46DESLISTRESP FROM PR4600 order by PR46CODLISTRESP asc"
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txttext1(2)), "PR40CODPREGUNTA", "SELECT * FROM PR4000 WHERE PR40CODPREGUNTA = ? ")
        Call .CtrlAddLinked(.CtrlGetInfo(txttext1(2)), txttext1(3), "PR40DESPREGUNTA")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR27CODTIPRESPU", "SELECT PR27CODTIPRESPU, PR27DESTIPRESPU FROM PR2700 WHERE PR27CODTIPRESPU = ? ")
        Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtDesResp, "PR27DESTIPRESPU")
        
        Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(1)), "PR46CODLISTRESP", "SELECT PR46CODLISTRESP, PR46DESLISTRESP FROM PR4600 WHERE PR46CODLISTRESP = ? ")
        Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(1)), txtDesLista, "PR46DESLISTRESP")
        
        Call .WinRegister
        Call .WinStabilize
    End With

    'Call objApp.SplashOff
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Cuestionario_Actuaci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub



Private Sub lblactLabel1_Click(Index As Integer)
txttext1(2).SetFocus
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
    txttext1(4).Text = txtCuestext1(0).Text
End Sub
Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
 ' cmdpreguntas.Enabled = False
End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  If gintIndice = 0 Then
    Call objsecurity.LaunchProcess("PR0207")
  Else
    If gintIndice = 1 Then
      Call objsecurity.LaunchProcess("PR0135")
    Else
      If gintIndice = 2 Then
        Call objsecurity.LaunchProcess("PR0131")
      End If
    End If
  End If
End Sub
Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Cuestionario_Actuaci�n" And strCtrl = "cboSSDBCombo1(0)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR2700"
     .strOrder = "ORDER BY PR27CODTIPRESPU ASC"
         
     Set objField = .AddField("PR27CODTIPRESPU")
     objField.strSmallDesc = "C�digo del Tipo de Respuestas"
         
     Set objField = .AddField("PR27DESTIPRESPU")
     objField.strSmallDesc = "Descripci�n del Tipo de Respuestas"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("pr01codactuacion"))
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), .cllValues("PR27CODTIPRESPU"))
     End If
   End With
    'Call objCW.objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.DataSave
    Call cboSSDBCombo1_Click(0)
   Set objSearch = Nothing
 End If
 
  If strFormName = "Cuestionario_Actuaci�n" And strCtrl = "cboSSDBCombo1(1)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR4600"
     .strOrder = "ORDER BY PR46CODLISTRESP ASC"
         
     Set objField = .AddField("PR46CODLISTRESP")
     objField.strSmallDesc = "C�digo de la Lista de Respuestas"
         
     Set objField = .AddField("PR46DESLISTRESP")
     objField.strSmallDesc = "Descripci�n de la Lista de Respuestas"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("pr01codactuacion"))
      Call objWinInfo.CtrlSet(cboSSDBCombo1(1), .cllValues("PR46CODLISTRESP"))
     End If
   End With
  'Call objCW.objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)))
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataSave
  Call cboSSDBCombo1_Click(1)
  Set objSearch = Nothing
 End If
 
  If strFormName = "Cuestionario_Actuaci�n" And strCtrl = "txtText1(2)" Then
     Set objSearch = New clsCWSearch
    With objSearch
  
     .strTable = "PR4000"
     .strOrder = "ORDER BY PR40CODPREGUNTA ASC"
  
     Set objField = .AddField("PR40CODPREGUNTA")
     objField.strSmallDesc = "C�digo de la Pregunta"
  
     Set objField = .AddField("PR40DESPREGUNTA")
     objField.strSmallDesc = "Descripci�n de la Pregunta "
  
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("pr01codactuacion"))
      Call objWinInfo.CtrlSet(txttext1(2), .cllValues("PR40CODPREGUNTA"))
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    'jcr 17/3/98
    'If btnButton.Index = 2 Then
    '    cmdpreguntas.Enabled = True
    'Else
    '    cmdpreguntas.Enabled = False
    'End If
    
    If (btnButton.Index = 30) Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Exit Sub
    End If
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    
    If (cboSSDBCombo1(0) <> "4") Then
      cboSSDBCombo1(1).Enabled = False
    Else
      cboSSDBCombo1(1).Enabled = False
    End If
    
    If (btnButton.Index = 8) Or (btnButton.Index = 4) Then
        If mnuOpcionesOpcion.Item(50).Checked = True Then
           objWinInfo.DataNew
        End If
    End If
    
    If btnButton.Index = 3 Then
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      objWinInfo.DataRefresh
    End If
    
    Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  'jcr 17/3/98
  'If intIndex = 10 Then
  '  cmdpreguntas.Enabled = True
  'Else
  '  cmdpreguntas.Enabled = False
  'End If
  
  If intIndex = 100 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    Exit Sub
  End If
  
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
 
  If (intIndex = 60) Or (intIndex = 40) Then
    If mnuOpcionesOpcion.Item(50).Checked = True Then
      objWinInfo.DataNew
    End If
  End If
  
  If (cboSSDBCombo1(0).Text <> "4") Then
    cboSSDBCombo1(1).Enabled = False
  Else
    cboSSDBCombo1(1).Enabled = True
  End If
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  
  If (cboSSDBCombo1(0).Text <> "4") Then
    cboSSDBCombo1(1).Enabled = False
  Else
    cboSSDBCombo1(1).Enabled = True
  End If
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    cboSSDBCombo1(1).Enabled = False
  Else
    cboSSDBCombo1(1).Enabled = True
  End If
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    cboSSDBCombo1(1).Enabled = False
  Else
    cboSSDBCombo1(1).Enabled = True
  End If
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    cboSSDBCombo1(1).Enabled = False
  Else
    cboSSDBCombo1(1).Enabled = True
  End If
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    cboSSDBCombo1(1).Enabled = False
  Else
    cboSSDBCombo1(1).Enabled = True
  End If
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(lbllabel1(intIndex).Container, False, True)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
    gintIndice = intIndex
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)
End Sub
Private Sub cboSSDBCombo1_Change(Index As Integer)
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim intResp As Integer
  Dim intCodLis As Integer

  Call objWinInfo.CtrlDataChange
  
  If Index = 0 Then
     'Se est� manipulando el combo que determina el tipo de respuesta
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         cboSSDBCombo1(1).Text = ""
         cboSSDBCombo1(1).Enabled = False
     Else
         'La respuesta ser� una lista de valores
         cboSSDBCombo1(1).Enabled = True
         If cboSSDBCombo1(1).Text = "" Then
           strSelect = "SELECT COUNT(*) FROM PR2800"
           Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
           If rsta.rdoColumns(0).Value = 0 Then
             intResp = MsgBox("No existe ninguna lista de respuestas con respuestas, Defina una", vbInformation, "Importante")
             'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)), 1)
             Call objWinInfo.CtrlSet(cboSSDBCombo1(1), 1)
             rsta.Close
             Set rsta = Nothing
           Else
             strSelect = "select min(pr46codlistresp) from pr2800"
             Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
             intCodLis = rsta.rdoColumns(0).Value
             'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)), intCodLis)
             Call objWinInfo.CtrlSet(cboSSDBCombo1(1), intCodLis)
             rsta.Close
             Set rsta = Nothing
           End If
         End If
     End If
  End If
  
  If (Index = 1) Then
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         cboSSDBCombo1(1).Text = ""
         cboSSDBCombo1(1).Enabled = False
     Else
         'La respuesta ser� una lista de valores
         cboSSDBCombo1(1).Enabled = True
     End If
  End If
  'Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)

End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim intResp As Integer
  Dim intCodLis As Integer

  Call objWinInfo.CtrlDataChange

  If Index = 0 Then
     'Se est� manipulando el combo que determina el tipo de respuesta
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         cboSSDBCombo1(1).Text = ""
         cboSSDBCombo1(1).Enabled = False
     Else
         'La respuesta ser� una lista de valores
         cboSSDBCombo1(1).Enabled = True
         If cboSSDBCombo1(1).Text = "" Then
           strSelect = "SELECT COUNT(*) FROM PR2800"
           Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
           If rsta.rdoColumns(0).Value = 0 Then
             intResp = MsgBox("No existe ninguna lista de respuestas con respuestas, Defina una", vbInformation, "Importante")
             'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)), 1)
             Call objWinInfo.CtrlSet(cboSSDBCombo1(1), 1)
             rsta.Close
             Set rsta = Nothing
           Else
             strSelect = "select min(pr46codlistresp) from pr2800"
             Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
             intCodLis = rsta.rdoColumns(0).Value
             'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(cboSSDBCombo1(1)), intCodLis)
             Call objWinInfo.CtrlSet(cboSSDBCombo1(1), intCodLis)
             rsta.Close
             Set rsta = Nothing
           End If
         End If
     End If
  End If
  
  If (Index = 1) Then
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         cboSSDBCombo1(1).Text = ""
         cboSSDBCombo1(1).Enabled = False
     Else
         'La respuesta ser� una lista de valores
         cboSSDBCombo1(1).Enabled = True
     End If
  End If
  Call Mostrar_Respuestas(cboSSDBCombo1(1).Text)

End Sub




Private Sub txtCuestext1_GotFocus(Index As Integer)
txttext1(2).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
    gintIndice = intIndex
End Sub
Private Sub txtText1_LostFocus(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim rstB As rdoResultset
  Dim strsqlA As String
  Dim strsqlB As String
  Dim strPregunta As String
  Dim strMensage As String
  Dim intCont As Integer
  Dim intRespuesta As Integer
  
  Call objWinInfo.CtrlLostFocus
  
  intCont = 0
  
  If (txttext1(3).Text = "") And (intIndex = 2) And (txttext1(2).Text <> "") Then
     strsqlB = "SELECT COUNT(*) " _
             & "FROM PR4000 " _
             & "WHERE PR40CODPREGUNTA =" & txttext1(2).Text
    Set rstB = objApp.rdoConnect.OpenResultset(strsqlB)
    intCont = rstB.rdoColumns(0).Value
    rstB.Close
    Set rstB = Nothing
    If intCont > 0 Then
      strsqlA = "SELECT PR40DESPREGUNTA " _
             & "FROM PR4000 " _
             & "WHERE PR40CODPREGUNTA =" & txttext1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
      txttext1(3).Text = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
    Else
      strMensage = "No existe ninguna pregunta con el c�digo " & txttext1(2).Text
      intRespuesta = MsgBox(strMensage, vbInformation, "Aviso")
      objWinInfo.objWinActiveForm.blnChanged = False
      objWinInfo.DataNew
    End If
  End If
  
End Sub
Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub





