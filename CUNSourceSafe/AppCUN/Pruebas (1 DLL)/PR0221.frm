VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmMuestrasPendientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Extracciones. Asignaci�n de Muestras."
   ClientHeight    =   6690
   ClientLeft      =   315
   ClientTop       =   1005
   ClientWidth     =   11610
   ControlBox      =   0   'False
   Icon            =   "PR0221.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6690
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdAsignar 
      Caption         =   "Asignar"
      Height          =   375
      Left            =   5040
      TabIndex        =   26
      Top             =   7560
      Width           =   2175
   End
   Begin VB.Frame Frame1 
      Caption         =   " Pacientes  "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   5640
      TabIndex        =   22
      Top             =   480
      Width           =   5535
      Begin VB.OptionButton optpaciente 
         Caption         =   "Hospitalizados"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   25
         Top             =   360
         Width           =   1455
      End
      Begin VB.OptionButton optpaciente 
         Caption         =   "Ambulatorios"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   2280
         TabIndex        =   24
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton optpaciente 
         Caption         =   "Todos"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   4320
         TabIndex        =   23
         Top             =   360
         Value           =   -1  'True
         Width           =   975
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Muestras de la Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   2
      Left            =   240
      TabIndex        =   6
      Top             =   4440
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2535
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         UseDefaults     =   0   'False
         _ExtentX        =   18785
         _ExtentY        =   4471
         _StockProps     =   79
         Caption         =   "MUESTRAS DE LA ACTUACI�N"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Index           =   0
      Left            =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   10935
      Begin TabDlg.SSTab tabTab1 
         Height          =   2535
         HelpContextID   =   90001
         Index           =   0
         Left            =   240
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   360
         Width           =   10410
         _ExtentX        =   18362
         _ExtentY        =   4471
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Tab 0"
         TabPicture(0)   =   "PR0221.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(0)"
         Tab(0).Control(1)=   "txtText1(1)"
         Tab(0).Control(2)=   "txtText1(2)"
         Tab(0).Control(3)=   "txtText1(3)"
         Tab(0).Control(4)=   "txtText1(4)"
         Tab(0).Control(5)=   "txtText1(5)"
         Tab(0).Control(6)=   "txtText1(6)"
         Tab(0).Control(7)=   "txtText1(7)"
         Tab(0).Control(8)=   "txtText1(8)"
         Tab(0).Control(9)=   "txtText1(9)"
         Tab(0).Control(10)=   "txtText1(10)"
         Tab(0).Control(11)=   "txtText1(11)"
         Tab(0).Control(12)=   "txtText1(12)"
         Tab(0).Control(13)=   "txtText1(13)"
         Tab(0).Control(14)=   "txtText1(14)"
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tab 1"
         TabPicture(1)   =   "PR0221.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   40101
            Index           =   14
            Left            =   -68640
            TabIndex        =   21
            Tag             =   "Desc Dpto"
            Top             =   960
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   13
            Left            =   -69720
            TabIndex        =   20
            Tag             =   "Cod Dpto"
            Top             =   960
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            HelpContextID   =   40101
            Index           =   12
            Left            =   -70800
            TabIndex        =   19
            Tag             =   "Segundo Apellido"
            Top             =   840
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            HelpContextID   =   40101
            Index           =   11
            Left            =   -71760
            TabIndex        =   18
            Tag             =   "Primer Apellido"
            Top             =   960
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            HelpContextID   =   40101
            Index           =   10
            Left            =   -72720
            TabIndex        =   17
            Tag             =   "Nombre"
            Top             =   840
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   9
            Left            =   -73680
            TabIndex        =   16
            Tag             =   "Historia"
            Top             =   840
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   8
            Left            =   -74640
            TabIndex        =   15
            Tag             =   "Cod Persona"
            Top             =   840
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR56DESESTMUES"
            Height          =   330
            HelpContextID   =   40101
            Index           =   7
            Left            =   -67800
            TabIndex        =   14
            Tag             =   "Estado Muestra"
            Top             =   360
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR37DESESTADO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   -68640
            TabIndex        =   13
            Tag             =   "Estado Actuaci�n"
            Top             =   240
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "HORA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   5
            Left            =   -69840
            TabIndex        =   12
            Tag             =   "Hora"
            Top             =   240
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR04FECPLANIFIC"
            Height          =   330
            HelpContextID   =   40101
            Index           =   4
            Left            =   -70920
            TabIndex        =   11
            Tag             =   "Fecha Planificaci�n"
            Top             =   240
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01DESCORTA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   3
            Left            =   -72000
            TabIndex        =   10
            Tag             =   "Desc Actuaci�n"
            Top             =   240
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            HelpContextID   =   40101
            Index           =   2
            Left            =   -72960
            TabIndex        =   9
            Tag             =   "Cod Actuaci�n"
            Top             =   240
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR03NUMACTPEDI"
            Height          =   330
            HelpContextID   =   40101
            Index           =   1
            Left            =   -73920
            TabIndex        =   8
            Tag             =   "Act Pedida"
            Top             =   240
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR04NUMACTPLAN"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   -74760
            TabIndex        =   4
            Tag             =   "Act Planificada"
            Top             =   240
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2520
            Index           =   0
            Left            =   0
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   0
            Width           =   10410
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18362
            _ExtentY        =   4445
            _StockProps     =   79
            Caption         =   "ACTUACIONES"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   6405
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMuestrasPendientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00244.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 14 DE SEPTIEMBRE DE 1998                                      *
'* DESCRIPCION:                                                         *
'*                                                                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim refrescar As Boolean





Private Sub Form_Activate()
Dim i As Integer
Dim stra As String
Dim rsta As rdoResultset
    If txtText1(0).Text <> "" Then
        Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
            objWinInfo.objWinActiveForm.strWhere = "(PR24MODO IN (1,3) AND PR01CODACTUACION IN (SELECT PR0300.PR01CODACTUACION FROM PR0400,PR0300 WHERE " & _
                      "PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI AND PR04NUMACTPLAN=" & txtText1(0).Text & _
                      " AND PR0400.AD02CODDPTO=" & glngdptologin & ")) OR " & _
                      "(PR24MODO=2 AND PR01CODACTUACION IN (SELECT PR0300.PR01CODACTUACION FROM PR0300,PR0900,PR0400 WHERE " & _
                      "PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND " & _
                      "PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI AND " & _
                      "PR0400.PR04NUMACTPLAN=" & txtText1(0).Text & " AND PR0900.AD02CODDPTO=" & glngdptologin & "))"
        objWinInfo.DataRefresh
        grdDBGrid1(2).MoveFirst
        For i = 0 To grdDBGrid1(2).Rows
            stra = "SELECT COUNT(*) FROM PR5200,PR4200,PR0400 WHERE " & _
                    "PR5200.PR52NUMMUESTRA=PR4200.PR52NUMMUESTRA AND " & _
                    "PR0400.PR04NUMACTPLAN=PR4200.PR04NUMACTPLAN AND " & _
                    "PR0400.CI21CODPERSONA=" & grdDBGrid1(0).Columns(6).Value & " AND " & _
                    "PR5200.PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).Value & " AND " & _
                    "PR0400.PR37CODESTADO IN (1,2,3)"
            Set rsta = objApp.rdoConnect.OpenResultset(stra)
            If rsta(0).Value > 0 Then
                grdDBGrid1(2).Columns(3).Value = True
            End If
            stra = "SELECT COUNT(*) FROM PR5200,PR4200 WHERE PR04NUMACTPLAN=" & txtText1(0).Text & _
                    " AND PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).Value & " AND PR4200.PR52NUMMUESTRA=PR5200.PR52NUMMUESTRA"
            Set rsta = objApp.rdoConnect.OpenResultset(stra)
            If rsta(0).Value > 0 Then
                grdDBGrid1(2).Columns(4).Value = True
            End If
            grdDBGrid1(2).MoveNext
        Next i
        grdDBGrid1(2).MoveFirst
    End If
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
    Call Seleccionar_Dpto
    
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
    With objMasterInfo
        .strName = "Actuaciones"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR5204J"
        strKey = .strDataBase & .strTable
        .intAllowance = cwAllowReadOnly
        .intCursorSize = 0
        .strWhere = "(PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO IN (1,3)) AND AD02CODDPTO=" & glngdptologin & ") OR" & _
                    "(PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO=2) AND " & glngdptologin & " IN (SELECT AD02CODDPTO FROM PR0900 WHERE PR0900.PR09NUMPETICION=PR5204J.PR09NUMPETICION))"
                    
        'Call .objPrinter.Add("PR1061", "Listado de grupos y actuaciones")
        'Call .objPrinter.Add("PR1062", "Listado de cuestionario de un grupo")
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormAddOrderField("PR04NUMACTPLAN", cwAscending)
        '.blnHasMaint = True
        Call .FormCreateFilterWhere(strKey, "Actuaci�n")
        Call .FormAddFilterWhere(strKey, "PR04NUMACTPLAN", "N�m Act Planific", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI", "N�m Act Pedi", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�d Actuaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Desc Actuaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR04FECPLANIFIC", "Fecha Planificaci�n", cwString)
        Call .FormAddFilterWhere(strKey, "HORA", "Hora", cwDate)
        Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado", cwString)
        Call .FormAddFilterWhere(strKey, "PR56DESESTMUES", "Estado Muestra", cwString)
        Call .FormAddFilterWhere(strKey, "CI21CODPERSONA", "Cod Persona", cwNumeric)
        Call .FormAddFilterWhere(strKey, "CI22NUMHISTORIA", "N�m. Historia", cwNumeric)
        Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
        Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
        Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�d Dpto", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR04NUMACTPLAN", "N�m Act Planific")
        Call .FormAddFilterOrder(strKey, "PR03NUMACTPEDI", "N�m Act Pedi")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�d Actuaci�n")
        Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Desc Actuaci�n")
        Call .FormAddFilterOrder(strKey, "PR04FECPLANIFIC", "Fecha Planificaci�n")
        Call .FormAddFilterOrder(strKey, "HORA", "Hora")
        Call .FormAddFilterOrder(strKey, "PR37DESESTADO", "Estado")
        Call .FormAddFilterOrder(strKey, "PR56DESESTMUES", "Estado Muestra")
        Call .FormAddFilterOrder(strKey, "CI21CODPERSONA", "Cod Persona")
        Call .FormAddFilterOrder(strKey, "CI22NUMHISTORIA", "N�m. Historia")
        Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
        Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
        Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�d Dpto")
    End With
  
  
    With objMultiInfo
        .strName = "Muestras de la Actuaci�n"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = fraframe1(0)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(2)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR5205J"
        '.intAllowance = cwAllowDelete
        .intCursorSize = 0
         .intAllowance = cwAllowReadOnly
         
        Call .FormAddOrderField("PR24CODMUESTRA", cwAscending)
        Call .FormAddRelation("pr01codactuacion", txtText1(2))
 
        strKey = .strDataBase & .strTable
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Muestras de la Actuaci�n")
        Call .FormAddFilterWhere(strKey, "PR24CODMUESTRA", "C�digo Muestra", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR24DESCORTA", "Descripci�n Muestra", cwString)
        Call .FormAddFilterWhere(strKey, "PR24DESCOMPLETA", "Descripci�n Larga", cwString)
        Call .FormAddFilterWhere(strKey, "PR24CAPACIDADTUBO", "Capacidad Tubo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR24MODO", "Modo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR25TIEMPO", "Tiempo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR25VOLUMENMIN", "Volumen M�nimo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR25DILUCION", "Diluci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR25COMENTIMPRI", "Comentario en la Impresi�n", cwString)
        Call .FormAddFilterWhere(strKey, "PR25VOLUMENPREF", "Volumen Preferible", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR25INDHORA", "Anotar hora", cwBoolean)
        Call .FormAddFilterWhere(strKey, "PR25ETIQADICIONAL", "N� Etiquetas", cwString)
        Call .FormAddFilterWhere(strKey, "PR25INDISTINTO", "Grupo Muestras Necesaria", cwString)
        Call .FormAddFilterWhere(strKey, "PR25COMENTINFORMA", "Comentarios Pantalla", cwString)
        Call .FormAddFilterWhere(strKey, "PR25GRUPOMUESTRA", "Grupo entubable", cwString)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR24CODMUESTRA", "C�digo Muestra")
        Call .FormAddFilterOrder(strKey, "PR24DESCORTA", "Descripci�n Muestra")
        Call .FormAddFilterOrder(strKey, "PR24DESCOMPLETA", "Descripci�n Larga")
        Call .FormAddFilterOrder(strKey, "PR24CAPACIDADTUBO", "Capacidad Tubo")
        Call .FormAddFilterOrder(strKey, "PR24MODO", "Modo")
        Call .FormAddFilterOrder(strKey, "PR25TIEMPO", "Tiempo")
        Call .FormAddFilterOrder(strKey, "PR25VOLUMENMIN", "Volumen M�nimo")
        Call .FormAddFilterOrder(strKey, "PR25DILUCION", "Diluci�n")
        Call .FormAddFilterOrder(strKey, "PR25COMENTIMPRI", "Comentario en la Impresi�n")
        Call .FormAddFilterOrder(strKey, "PR25VOLUMENPREF", "Volumen Preferible")
        Call .FormAddFilterOrder(strKey, "PR25INDHORA", "Anotar hora")
        Call .FormAddFilterOrder(strKey, "PR25ETIQADICIONAL", "N� Etiquetas")
        Call .FormAddFilterOrder(strKey, "PR25INDISTINTO", "Grupo Muestras Necesaria")
        Call .FormAddFilterOrder(strKey, "PR25COMENTINFORMA", "Comentarios Pantalla")
        Call .FormAddFilterOrder(strKey, "PR25GRUPOMUESTRA", "Grupo entubable")
    End With

    With objWinInfo

        

        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
        
        
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del grupo
        Call .GridAddColumn(objMultiInfo, "Extraida", "", cwBoolean, 1)
        Call .GridAddColumn(objMultiInfo, "Asignada", "", cwBoolean, 1)
        Call .GridAddColumn(objMultiInfo, "C�digo Muestra", "PR24CODMUESTRA", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Descripci�n Muestra", "PR24DESCORTA", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Descripci�n Larga", "PR24DESCOMPLETA", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Capacidad Tubo", "PR24CAPACIDADTUBO", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Modo", "PR24MODO", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Tiempo", "PR25TIEMPO", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Volumen M�nimo", "PR25VOLUMENMIN", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Diluci�n", "PR25DILUCION", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Comentario en la Impresi�n", "PR25COMENTIMPRI", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Volumen Preferible", "PR25VOLUMENPREF", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Anotar hora", "PR25INDHORA", cwBoolean, 1)
        Call .GridAddColumn(objMultiInfo, "N� Etiquetas", "PR25ETIQADICIONAL", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Grupo Muestras Necesaria", "PR25INDISTINTO", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "Comentarios Pantalla", "PR25COMENTINFORMA", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Grupo entubable", "PR25GRUPOMUESTRA", cwString, 30)

        Call .FormCreateInfo(objMasterInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True
        .CtrlGetInfo(txtText1(2)).blnInFind = True
        .CtrlGetInfo(txtText1(3)).blnInFind = True
        .CtrlGetInfo(txtText1(4)).blnInFind = True
        .CtrlGetInfo(txtText1(5)).blnInFind = True
        .CtrlGetInfo(txtText1(6)).blnInFind = True
        .CtrlGetInfo(txtText1(7)).blnInFind = True
        .CtrlGetInfo(txtText1(8)).blnInFind = True
        .CtrlGetInfo(txtText1(9)).blnInFind = True
        .CtrlGetInfo(txtText1(10)).blnInFind = True
        .CtrlGetInfo(txtText1(11)).blnInFind = True
        .CtrlGetInfo(txtText1(12)).blnInFind = True
        .CtrlGetInfo(txtText1(13)).blnInFind = True
        .CtrlGetInfo(txtText1(14)).blnInFind = True
        '.CtrlGetInfo(cboSSDBCombo1(0)).blnInFind = True '20/11 da problemas
        .CtrlGetInfo(txtText1(0)).blnInGrid = False
        .CtrlGetInfo(txtText1(1)).blnInGrid = False
        .CtrlGetInfo(txtText1(2)).blnInGrid = False
        .CtrlGetInfo(txtText1(8)).blnInGrid = False
        '.CtrlGetInfo(txtText1(13)).blnInGrid = False
        
        'Se indica que campos son obligatorios y cuales son clave primaria
        'en el grid que contiene las actuaciones del grupo
        '.CtrlGetInfo(grdDBGrid1(2).Columns(3)).intKeyNo = 1
        '.CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnMandatory = True
        '.CtrlGetInfo(grdDBGrid1(2).Columns(5)).intKeyNo = 1
        '.CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(2).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(6)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(7)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(8)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(9)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(10)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(11)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(12)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(13)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(14)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(15)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(16)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(17)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(18)).blnInFind = True
        'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
        'contiene las actuaciones del grupo
        'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), grdDBGrid1(2).Columns(4), "PR01DESCORTA")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(13)), "AD02CODDPTO", "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(txtText1(13)), txtText1(14), "AD02DESDPTO")
    
        
        'Para sacar el Search del tipo de grupo
        '.CtrlGetInfo(cboSSDBCombo1(0)).blnForeign = True
        '.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR15CODTIPGRUPO,PR15DESTIPGRUPO FROM PR1500 ORDER BY PR15CODTIPGRUPO ASC"
        
        Call .WinRegister
        Call .WinStabilize
    End With
    'gstrLlamadorSel = "Grupos"
    'Call objApp.SplashOff
    'Call objWinInfo.WinProcess(6, 24, 0)
    grdDBGrid1(0).Columns(1).Width = 3000
    grdDBGrid1(0).Columns(2).Width = 800
    grdDBGrid1(0).Columns(3).Width = 600
    grdDBGrid1(0).Columns(4).Width = 1400
    grdDBGrid1(0).Columns(5).Width = 2000
    grdDBGrid1(0).Columns(6).Width = 1000
    grdDBGrid1(0).Columns(7).Width = 2000
    grdDBGrid1(0).Columns(8).Width = 2000
    grdDBGrid1(0).Columns(9).Width = 2000
    grdDBGrid1(0).Columns(10).Width = 800
    grdDBGrid1(0).Columns(11).Width = 2000
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub




Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
    If strFormName = "Muestras de la Actuaci�n" Then
        Frame1.Enabled = False
    Else
        Frame1.Enabled = True
    End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------



'Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'  If (strFormName = "Grupos") And (strCtrl = "cboSSDBCombo1(0)") Then
'    frmtipodegrupo.cmdaceptar.Visible = True
'    'Load frmtipodegrupo
'    'Call frmtipodegrupo.Show(vbModal)
'    Call objsecurity.LaunchProcess("PR0202")
'    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)), guCodigo)
'    Call objWinInfo.CtrlSet(cboSSDBCombo1(0), guCodigo)
'    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtAct(2)), gstrdescripcion)
'    'Unload frmtipodegrupo
'    'Set frmtipodegrupo = Nothing
'    Call objCW.objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
'  End If
' guCodigo = ""
' gstrdescripcion = ""
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    If btnButton.Index = 3 Then
        Call MsgBox("Imposible abrir registros", vbExclamation)
    Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    If intIndex = 20 Then
      Call MsgBox("Imposible abrir registros", vbExclamation)
    Else
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)

    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
      
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)

  Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Dim i As Integer
    Dim rsta As rdoResultset
    Dim stra As String
    
    Call objWinInfo.CtrlDataChange
    If intIndex = 0 Then
        If grdDBGrid1(0).Rows > 0 Then
'************************
            If txtText1(0).Text <> "" Then
                Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
                objWinInfo.objWinActiveForm.strWhere = "(PR24MODO IN (1,3) AND PR01CODACTUACION IN (SELECT PR0300.PR01CODACTUACION FROM PR0400,PR0300 WHERE " & _
                      "PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI AND PR04NUMACTPLAN=" & txtText1(0).Text & _
                      " AND PR0400.AD02CODDPTO=" & glngdptologin & ")) OR " & _
                      "(PR24MODO=2 AND PR01CODACTUACION IN (SELECT PR0300.PR01CODACTUACION FROM PR0300,PR0900,PR0400 WHERE " & _
                      "PR0300.PR09NUMPETICION=PR0900.PR09NUMPETICION AND " & _
                      "PR0300.PR03NUMACTPEDI=PR0400.PR03NUMACTPEDI AND " & _
                      "PR0400.PR04NUMACTPLAN=" & txtText1(0).Text & " AND PR0900.AD02CODDPTO=" & glngdptologin & "))"
                objWinInfo.DataRefresh
                Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
            
    '************************
                grdDBGrid1(2).MoveFirst
                For i = 0 To grdDBGrid1(2).Rows
                    stra = "SELECT COUNT(*) FROM PR5200,PR4200,PR0400 WHERE " & _
                            "PR5200.PR52NUMMUESTRA=PR4200.PR52NUMMUESTRA AND " & _
                            "PR0400.PR04NUMACTPLAN=PR4200.PR04NUMACTPLAN AND " & _
                            "PR0400.CI21CODPERSONA=" & grdDBGrid1(0).Columns(6).Value & " AND " & _
                            "PR5200.PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).Value & " AND " & _
                            "PR0400.PR37CODESTADO IN (1,2,3)"
                    Set rsta = objApp.rdoConnect.OpenResultset(stra)
                    If rsta(0).Value > 0 Then
                        grdDBGrid1(2).Columns(3).Value = True
                    End If
                    stra = "SELECT COUNT(*) FROM PR5200,PR4200 WHERE PR04NUMACTPLAN=" & txtText1(0).Text & _
                            " AND PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).Value & " AND PR4200.PR52NUMMUESTRA=PR5200.PR52NUMMUESTRA"
                    Set rsta = objApp.rdoConnect.OpenResultset(stra)
                    If rsta(0).Value > 0 Then
                        grdDBGrid1(2).Columns(4).Value = True
                    End If
                    grdDBGrid1(2).MoveNext
                Next i
                grdDBGrid1(2).MoveFirst
            Else
                grdDBGrid1(2).RemoveAll
            End If
        Else
            grdDBGrid1(2).RemoveAll
        End If
    End If
End Sub

Private Sub optpaciente_Click(Index As Integer)
'hospitalizados
If optpaciente(0).Value = True Then
   objWinInfo.objWinActiveForm.strWhere = "((PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO IN (1,3)) AND AD02CODDPTO=" & glngdptologin & ") OR" & _
                    "(PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO=2) AND " & glngdptologin & " IN (SELECT AD02CODDPTO FROM PR0900 WHERE PR0900.PR09NUMPETICION=PR5204J.PR09NUMPETICION))" & _
    ") AND (" & _
    "CI21CODPERSONA IN (SELECT CI21CODPERSONA FROM AD0100 WHERE " & _
                            "AD01CODASISTENCI IN (SELECT AD01CODASISTENCI FROM AD1500)))"
  objWinInfo.DataRefresh
End If
'ambulatorios
If optpaciente(1) = True Then
   objWinInfo.objWinActiveForm.strWhere = "((PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO IN (1,3)) AND AD02CODDPTO=" & glngdptologin & ") OR" & _
                    "(PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO=2) AND " & glngdptologin & " IN (SELECT AD02CODDPTO FROM PR0900 WHERE PR0900.PR09NUMPETICION=PR5204J.PR09NUMPETICION))" & _
    ") AND (" & _
    "CI21CODPERSONA NOT IN (SELECT CI21CODPERSONA FROM AD0100 WHERE " & _
                            "AD01CODASISTENCI IN (SELECT AD01CODASISTENCI FROM AD1500)))"
  objWinInfo.DataRefresh
End If
'todos
If optpaciente(2).Value = True Then
    objWinInfo.objWinActiveForm.strWhere = "(PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO IN (1,3)) AND AD02CODDPTO=" & glngdptologin & ") OR" & _
                    "(PR01CODACTUACION IN (SELECT PR01CODACTUACION FROM PR2400,PR2500 " & _
                                "WHERE PR2500.PR24CODMUESTRA=PR2400.PR24CODMUESTRA AND " & _
                                "PR24MODO=2) AND " & glngdptologin & " IN (SELECT AD02CODDPTO FROM PR0900 WHERE PR0900.PR09NUMPETICION=PR5204J.PR09NUMPETICION))"
    objWinInfo.DataRefresh
End If
End Sub

Private Sub cmdAsignar_Click()
    Dim mintisel As Integer
    Dim mintNTotalSelRows As Integer
    Dim mvarBkmrk As Variant
    Dim mblnLlamar As Boolean
    Dim stra As String
    Dim rsta As rdoResultset
    Dim strinsertcuest As String
    Dim strSelCuest As String
    Dim mstrListaResp As String
    Dim rst52 As rdoResultset
    Dim rstSelCuest As rdoResultset
    Dim sqlstr As String
    Dim intPregExt As Integer
    Dim strAsignar As String
    Dim intpregextr As Integer
    Dim strmodif As String
    
    cmdAsignar.Enabled = False
    mblnLlamar = False
    'gstrplanif = ""
    'Guardamos el n�mero de filas seleccionadas
    mintNTotalSelRows = grdDBGrid1(2).SelBookmarks.Count
    
    For mintisel = 0 To mintNTotalSelRows - 1
        'Guardamos el n�mero de fila que est� seleccionada
        strAsignar = "Todo"
        mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel + 1)
        If grdDBGrid1(2).Columns(3).CellValue(mvarBkmrk) = True Then
            intpregextr = MsgBox("La Muestra " & grdDBGrid1(2).Columns(6).CellValue(mvarBkmrk) & " ya ha sido extra�da, �Desea hacer otra Extracci�n?", vbQuestion + vbYesNoCancel)
            Select Case intpregextr
                Case vbYes:
                    strAsignar = "Todo"
                Case vbNo:
                    strAsignar = "Asignar"
                Case vbCancel:
                    strAsignar = "Nada"
            End Select
        End If
        If strAsignar = "Todo" Then
            sqlstr = "SELECT PR52NUMMUESTRA_SEQUENCE.nextval FROM dual"
            Set rst52 = objApp.rdoConnect.OpenResultset(sqlstr)
            sqlstr = "INSERT INTO PR5200 " _
                    & "(PR52NUMMUESTRA,PR24CODMUESTRA)" _
                    & "VALUES (" _
                    & rst52(0).Value & "," _
                    & grdDBGrid1(2).Columns(5).CellValue(mvarBkmrk) & ")"
            objApp.rdoConnect.Execute sqlstr, 64
            objApp.rdoConnect.Execute "Commit", 64
            'se rellena el cuestionario si es que lo hay.
            'si el cuestionario ya est� insertado en PR5400 entonces no se llena
            stra = "SELECT COUNT(*) FROM PR5100 WHERE PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).CellValue(mvarBkmrk)
            Set rsta = objApp.rdoConnect.OpenResultset(stra)
            If rsta.rdoColumns(0).Value > 0 Then 'hay cuestionario
               'cmdCuestionario.Visible = True
               stra = "SELECT COUNT(*) FROM PR5400 WHERE PR52NUMMUESTRA=" & rst52(0).Value
               Set rsta = objApp.rdoConnect.OpenResultset(stra)
               If rsta.rdoColumns(0).Value = 0 Then 'el cuestionario no est� insertado en PR5400
                      stra = "SELECT * FROM PR5100 WHERE PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).CellValue(mvarBkmrk)
                      Set rsta = objApp.rdoConnect.OpenResultset(stra)
                      While Not rsta.EOF
                          'If rsta.rdoColumns("pr51indroblig").Value = -1 Then
                          '    cmdAsignar.Visible = False
                          'End If
                          strinsertcuest = "INSERT INTO PR5400(PR52NUMMUESTRA,PR40CODPREGUNTA," & _
                                         "PR27CODTIPRESPU,PR46CODLISTRESP,PR54INDOBLIG,PR54RESPUESTA) " & _
                                         "VALUES(" & rst52(0).Value & "," & _
                                         rsta.rdoColumns("pr40codpregunta").Value & "," & _
                                         rsta.rdoColumns("pr27codtiprespu").Value & ","
                          If IsNull(rsta.rdoColumns("pr46codlistresp").Value) = True Then
                            strinsertcuest = strinsertcuest & "null" & ","
                          Else
                            strinsertcuest = strinsertcuest & rsta.rdoColumns("pr46codlistresp").Value & ","
                          End If
                          strinsertcuest = strinsertcuest & rsta.rdoColumns("pr51indroblig").Value & _
                                        "," & "null)"
                          objApp.rdoConnect.Execute strinsertcuest, 64
                          rsta.MoveNext
                      Wend
               Else 'el cuestionario est� insertado
                  'stra = "SELECT * FROM PR5400 WHERE PR52NUMMUESTRA=" & grdDBGrid1(0).Columns(5).CellValue(mvarBkmrk)
                  'Set rsta = objApp.rdoConnect.OpenResultset(stra)
                  'norespondido = 0
                  'While Not rsta.EOF
                  '    If rsta.rdoColumns("pr54indoblig").Value = -1 And _
                  '       IsNull(rsta.rdoColumns("pr54respuesta").Value) = True Then
                  '       norespondido = 1
                  '    End If
                  'rsta.MoveNext
                  'Wend
                  'If norespondido = 1 Then
                  '  cmdAsignar.Visible = False
                  'Else
                  '  cmdAsignar.Visible = True
                  'End If
               End If
            'Else 'no hay cuestionario
            ' cmdAsignar.Visible = True
            End If
        End If
        If strAsignar = "Asignar" Then
            sqlstr = "SELECT PR5200.PR52NUMMUESTRA FROM PR5200,PR4200,PR0400 WHERE " & _
                    "PR5200.PR52NUMMUESTRA=PR4200.PR52NUMMUESTRA AND " & _
                    "PR0400.PR04NUMACTPLAN=PR4200.PR04NUMACTPLAN AND " & _
                    "PR0400.CI21CODPERSONA=" & grdDBGrid1(0).Columns(6).Value & " AND " & _
                    "PR5200.PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).Value & " AND " & _
                    "PR0400.PR37CODESTADO IN (1,2,3)"
            Set rst52 = objApp.rdoConnect.OpenResultset(sqlstr)
        End If
        If strAsignar = "Todo" Or strAsignar = "Asignar" Then
            mblnLlamar = True
            sqlstr = "INSERT INTO PR4200 " _
                          & "(PR04NUMACTPLAN,PR52NUMMUESTRA)" _
                          & "VALUES (" _
                          & txtText1(0).Text & "," _
                          & rst52(0).Value & ")"
            objApp.rdoConnect.Execute sqlstr, 64
            objApp.rdoConnect.Execute "Commit", 64
            strmodif = "UPDATE PR0400 SET PR56CODESTMUES=2 WHERE PR04NUMACTPLAN=" & txtText1(0).Text
            objApp.rdoConnect.Execute strmodif, 64
            objApp.rdoConnect.Execute "Commit", 64
            strSelCuest = "SELECT * from pr2600 where " _
                     & "PR01CODACTUACION = " & txtText1(2).Text & " AND PR24CODMUESTRA=" & grdDBGrid1(2).Columns(5).Value
            Set rstSelCuest = objApp.rdoConnect.OpenResultset(strSelCuest)
            While rstSelCuest.EOF = False
                If IsNull(rstSelCuest("PR46CODLISTRESP").Value) Then
                    mstrListaResp = "NULL"
                Else
                    mstrListaResp = rstSelCuest("PR46CODLISTRESP").Value
                End If
                sqlstr = "INSERT INTO PR4300 " _
                        & "(PR04NUMACTPLAN,PR52NUMMUESTRA,PR40CODPREGUNTA,PR46CODLISTRESP,PR27CODTIPRESPU,PR43INDOBLIG,PR43RESPUESTA)" _
                        & "VALUES (" & txtText1(0).Text & "," & rst52(0).Value & "," & rstSelCuest("pr40codpregunta").Value & "," _
                        & mstrListaResp & "," & rstSelCuest("PR27CODTIPRESPU").Value & "," & rstSelCuest("PR26INDROBLIG").Value & ",NULL)"
                objApp.rdoConnect.Execute sqlstr, 64
                objApp.rdoConnect.Execute "Commit", 64
                rstSelCuest.MoveNext
            Wend
        End If
     Next mintisel
     If mblnLlamar = True Then
        frmAsigMuestras.Text1(0).Text = txtText1(10).Text
        frmAsigMuestras.Text1(1).Text = txtText1(11).Text
        frmAsigMuestras.Text1(2).Text = txtText1(12).Text
        frmAsigMuestras.Text1(3).Text = txtText1(2).Text
        frmAsigMuestras.Text1(4).Text = txtText1(3).Text
        
        Call objsecurity.LaunchProcess("PR0224")
     End If
  'If gblnunloadrellenardatosmuestra = True Then
  '   gblnunloadrellenardatosmuestra = False
  '   objWinInfo.DataRefresh
  'End If
  cmdAsignar.Enabled = True
End Sub



