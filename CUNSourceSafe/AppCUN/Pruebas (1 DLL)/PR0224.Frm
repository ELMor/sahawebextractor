VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmAsigMuestras 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Extracciones. Asignaci�n de Muestras."
   ClientHeight    =   6690
   ClientLeft      =   315
   ClientTop       =   1005
   ClientWidth     =   11610
   ControlBox      =   0   'False
   Icon            =   "PR0224.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6690
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdCuestionario 
      Caption         =   "Cuestionario"
      DragIcon        =   "PR0224.frx":000C
      Height          =   375
      Left            =   5160
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   7440
      Width           =   1695
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Extracciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4560
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   2640
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3855
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   480
         Width           =   11250
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         UseDefaults     =   0   'False
         _ExtentX        =   19844
         _ExtentY        =   6800
         _StockProps     =   79
         Caption         =   "EXTRACCIONES"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2055
      Index           =   3
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   11700
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   4
         Left            =   2760
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "Nombre del Paciente"
         Top             =   1320
         Width           =   5400
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   3
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Tag             =   "Nombre del Paciente"
         Top             =   1320
         Width           =   1042
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   2
         Left            =   7320
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "Segundo Apellido del Paciente"
         Top             =   600
         Width           =   3200
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   1
         Left            =   3840
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Tag             =   "Primer Apellido del Paciente"
         Top             =   600
         Width           =   3200
      End
      Begin VB.TextBox Text1 
         Height          =   330
         Index           =   0
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   3
         TabStop         =   0   'False
         Tag             =   "Nombre del Paciente"
         Top             =   600
         Width           =   3200
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2760
         TabIndex        =   14
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�d Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   12
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 2�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   7320
         TabIndex        =   8
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 1�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   3840
         TabIndex        =   7
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   360
         TabIndex        =   6
         Top             =   360
         Width           =   735
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   6405
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAsigMuestras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdAsignar_Click()
  Dim mintNTotalSelRows As Integer
  Dim mvarBkmrk As Variant
  Dim mintNumPlan As Long
  Dim mintCodMuestra As Long
  Dim mintCodActuacion As Long
  Dim sqlstr As String
  Dim mintI As Integer
  Dim strselect As String
  Dim rstselect As rdoResultset
  Dim strSelCuest As String
  Dim rstSelCuest As rdoResultset
  Dim mstrListaResp As String
  Dim sacarmensaje As String
  
    mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
    If mintNTotalSelRows = 0 Then
       sacarmensaje = MsgBox("No hay ninguna actuaci�n seleccionada para asignar.", vbInformation, "Aviso")
       Exit Sub
    End If
      For mintI = 0 To mintNTotalSelRows - 1
        'Guardamos el n�mero de fila que est� seleccionada
        mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintI)
        'Guardamos los valores de esa fila en variables
        mintNumPlan = grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk)
        mintCodActuacion = grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk)
        mintCodMuestra = frmrellenardatosmuestra.txtText1(2).Text
        strselect = "SELECT count(*) from pr4200 where " _
                 & "PR52NUMMUESTRA = " & Text1(3).Text & " AND PR04NUMACTPLAN=" & mintNumPlan
        Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
        If rstselect(0).Value = 0 Then
          sqlstr = "INSERT INTO PR4200 " _
                  & "(PR04NUMACTPLAN,PR52NUMMUESTRA)" _
                  & "VALUES (" _
                  & mintNumPlan & "," _
                  & Text1(3).Text & ")"
          objApp.rdoConnect.Execute sqlstr, 64
          objApp.rdoConnect.Execute "Commit", 64
          strSelCuest = "SELECT * from pr2600 where " _
                   & "PR01CODACTUACION = " & mintCodActuacion & " AND PR24CODMUESTRA=" & mintCodMuestra
          Set rstSelCuest = objApp.rdoConnect.OpenResultset(strSelCuest)
          While rstSelCuest.EOF = False
            If IsNull(rstSelCuest("PR46CODLISTRESP").Value) Then
              mstrListaResp = "NULL"
            Else
              mstrListaResp = rstSelCuest("PR46CODLISTRESP").Value
            End If
            sqlstr = "INSERT INTO PR4300 " _
                    & "(PR04NUMACTPLAN,PR52NUMMUESTRA,PR40CODPREGUNTA,PR46CODLISTRESP,PR27CODTIPRESPU,PR43INDOBLIG,PR43RESPUESTA)" _
                    & "VALUES (" & mintNumPlan & "," & Text1(3).Text & "," & rstSelCuest("pr40codpregunta").Value & "," _
                    & mstrListaResp & "," & rstSelCuest("PR27CODTIPRESPU").Value & "," & rstSelCuest("PR26INDROBLIG").Value & ",NULL)"
            objApp.rdoConnect.Execute sqlstr, 64
            objApp.rdoConnect.Execute "Commit", 64
            rstSelCuest.MoveNext
          Wend
          rstSelCuest.Close
          Set rstSelCuest = Nothing
        End If
        rstselect.Close
        Set rstselect = Nothing
    Next mintI
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    objWinInfo.DataRefresh
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
End Sub


Private Sub cmdcuestionario_Click()
  Dim strSelect1 As String
  Dim rstselect1 As rdoResultset
  Dim strSelect2 As String
  Dim rstselect2 As rdoResultset
  
  cmdCuestionario.Enabled = False
  strSelect1 = "SELECT COUNT(*) FROM PR5400 WHERE PR52NUMMUESTRA=" & grdDBGrid1(1).Columns(3).Value
  Set rstselect1 = objApp.rdoConnect.OpenResultset(strSelect1)
  strSelect2 = "SELECT COUNT(*) FROM PR4300 WHERE PR52NUMMUESTRA=" & grdDBGrid1(1).Columns(3).Value & _
            " AND PR04NUMACTPLAN=" & frmMuestrasPendientes.txtText1(0).Text
  Set rstselect2 = objApp.rdoConnect.OpenResultset(strSelect2)
  
  If rstselect1(0).Value > 0 Then
    If rstselect2(0).Value > 0 Then
        Call objsecurity.LaunchProcess("PR0245")
    Else
        Call objsecurity.LaunchProcess("PR0216")
    End If
  Else
    If rstselect2(0).Value > 0 Then
        Call objsecurity.LaunchProcess("PR0217")
    Else
        Call MsgBox("No hay Cuestionario para esta Muestra", vbInformation)
    End If
  End If

  cmdCuestionario.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMultiInfo1 As New clsCWForm
  'Dim objMultiInfo2 As New clsCWForm
  'Dim objMultiInfo3 As New clsCWForm
  
  Dim strKey As String
  
  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
    With objMultiInfo1
    .strName = "Muestras Extra�das"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
     
    .strTable = "PR5200"
    .intAllowance = cwAllowModify
    '.strWhere = "CI21CODPERSONA=" & frmPacienteMuestra.grdDBGrid1(0).Columns(3).Value
                 '& " AND PR37CODESTADO=9"
    .strWhere = "PR52NUMMUESTRA IN (SELECT PR52NUMMUESTRA FROM PR4200 WHERE PR04NUMACTPLAN=" & frmMuestrasPendientes.txtText1(0).Text & ")"
    Call .FormAddOrderField("PR52NUMMUESTRA", cwAscending)

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Muestras Extra�das")
    Call .FormAddFilterWhere(strKey, "PR52NUMMUESTRA", "N�mero Muestra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "SG02COD", "Realizador de la Extracci�n", cwString)
    Call .FormAddFilterWhere(strKey, "PR24CODMUESTRA", "C�digo Muestra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR52DESMUESTRA", "Descripci�n Muestra", cwString)
    Call .FormAddFilterWhere(strKey, "PR52FECEXTRAC", "Fecha Extracci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "PR52INDINTRA", "Muestra Intraoperatoria", cwBoolean)
    Call .FormAddFilterWhere(strKey, "PR55CODESTADO", "C�digo Estado", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR52NUMIDMUESTRA", "N�m. Identificaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR52TIEMPO", "Tiempo Retardo", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "PR52NUMMUESTRA", "N�mero Muestra")
  End With

  
  
  With objWinInfo
    
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo1, "N�m.Muestra", "PR52NUMMUESTRA", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "Cod.Muestra", "PR24CODMUESTRA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo1, "Desc.Corta", "", cwString, 30)
    Call .GridAddColumn(objMultiInfo1, "Cod.Real", "SG02COD", cwString, 6)
    Call .GridAddColumn(objMultiInfo1, "Nombre", "", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Descripci�n", "PR52DESMUESTRA", cwString, 255)
    Call .GridAddColumn(objMultiInfo1, "Fecha Extracci�n", "PR52FECEXTRAC", cwDate, 12)
    Call .GridAddColumn(objMultiInfo1, "Intraoperatoria", "PR52INDINTRA", cwBoolean, 1)
    Call .GridAddColumn(objMultiInfo1, "Cod.Estado", "PR55CODESTADO", cwNumeric, 3)
    Call .GridAddColumn(objMultiInfo1, "Estado", "", cwString, 20)
    Call .GridAddColumn(objMultiInfo1, "N�m.identif", "PR52NUMIDMUESTRA", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo1, "Tiempo Retardo", "PR52TIEMPO", cwNumeric, 5)
    
    Call .FormCreateInfo(objMultiInfo1)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
    
    Call .FormChangeColor(objMultiInfo1)
  
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(10)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(12)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(13)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnInFind = True
    
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(11)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), "PR24CODMUESTRA", "SELECT * FROM PR2400 WHERE PR24CODMUESTRA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(4)), grdDBGrid1(1).Columns(5), "PR24DESCORTA")
 
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "SG02COD", "SELECT * FROM SG0200 WHERE SG02COD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(7), "SG02APE1")
 
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), "PR04NUMACTPLAN", "SELECT * FROM PR5500 WHERE PR55CODESTADO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(11)), grdDBGrid1(1).Columns(12), "PR55DESESTADO")
 
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(6)), "PR04NUMACTPLAN", "SELECT * FROM PR0428J WHERE PR04NUMACTPLAN = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(6)), grdDBGrid1(2).Columns(8), "PR01CODACTUACION")
 
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(7)), "PR04NUMACTPLAN", "SELECT * FROM PR0428J WHERE PR04NUMACTPLAN = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(7)), grdDBGrid1(2).Columns(9), "PR01DESCORTA")
 
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(6)), "PR04NUMACTPLAN", "SELECT * FROM PR0400 WHERE PR04NUMACTPLAN = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(6)), grdDBGrid1(2).Columns(10), "PR04FECPLANIFIC")
 
    Call .WinRegister
    Call .WinStabilize
  End With
  'grdDBGrid1(1).Columns(3).Width = 1150
  'grdDBGrid1(1).Columns(4).Width = 1150
  'grdDBGrid1(1).Columns(5).Width = 1150
  'grdDBGrid1(1).Columns(7).Width = 1600
  'grdDBGrid1(2).Columns(3).Visible = False
  'grdDBGrid1(2).Columns(4).Visible = False
  'grdDBGrid1(2).Columns(5).Visible = False
  'grdDBGrid1(2).Columns(3).Width = 1150
  'grdDBGrid1(2).Columns(4).Width = 1150
  'grdDBGrid1(2).Columns(5).Width = 1150
  'grdDBGrid1(2).Columns(6).Width = 5200
  'grdDBGrid1(2).Columns(7).Width = 1600
  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim sqlstr As String
  Dim rstsql As rdoResultset
  Dim strselect As String
  Dim rstselect As rdoResultset
  Dim strselect43 As String
  Dim rstselect43 As rdoResultset
  Dim strselect54 As String
  Dim rstselect54 As rdoResultset
  Dim intResp As Integer
  Dim blnPrimeravez As Boolean
  Dim blnsalir As Boolean
  Dim blnborrar As Boolean
  Dim strselect42 As String
  Dim rstselect42 As rdoResultset
  Dim strselect25Act As String
  Dim rstselect25Act As rdoResultset
  
  
  
  Select Case btnButton.Index
    Case 8:
      sqlstr = "delete from pr4300 where pr52nummuestra=" & grdDBGrid1(1).Columns(3).Value & _
               " and pr04numactplan=" & frmMuestrasPendientes.txtText1(0).Text
      objApp.rdoConnect.Execute sqlstr, 64
      sqlstr = "delete from pr4200 where pr52nummuestra=" & grdDBGrid1(1).Columns(3).Value & _
               " and pr04numactplan=" & frmMuestrasPendientes.txtText1(0).Text
      objApp.rdoConnect.Execute sqlstr, 64
      strselect = "SELECT COUNT(*) from pr4200 where " _
               & "PR52NUMMUESTRA = " & grdDBGrid1(1).Columns(3).Value
      Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
      If rstselect(0).Value = 0 Then
        sqlstr = "delete from pr5400 where pr52nummuestra=" & grdDBGrid1(1).Columns(3).Value
        objApp.rdoConnect.Execute sqlstr, 64
        sqlstr = "delete from pr5200 where pr52nummuestra=" & grdDBGrid1(1).Columns(3).Value
        objApp.rdoConnect.Execute sqlstr, 64
      End If
      objApp.rdoConnect.Execute "Commit", 64
      objWinInfo.DataRefresh
    Case 30:
      blnPrimeravez = True
      blnsalir = True
      strselect42 = "SELECT PR52NUMMUESTRA from pr4200 where " _
               & "PR04NUMACTPLAN = " & frmMuestrasPendientes.txtText1(0).Text
      Set rstselect42 = objApp.rdoConnect.OpenResultset(strselect42)
      Do While rstselect42.EOF = False
        blnborrar = False
        strselect43 = "SELECT COUNT(*) from pr4300 where " & _
                   "PR52NUMMUESTRA=" & rstselect42("PR52NUMMUESTRA").Value & _
                   " AND PR04NUMACTPLAN=" & frmMuestrasPendientes.txtText1(0).Text & _
                   " AND PR43INDOBLIG=-1 AND PR43RESPUESTA IS NULL"
        Set rstselect43 = objApp.rdoConnect.OpenResultset(strselect43)
        strselect54 = "SELECT COUNT(*) from pr5400 where " & _
                   "PR52NUMMUESTRA=" & rstselect42("PR52NUMMUESTRA").Value & _
                   " AND PR54INDOBLIG=-1 AND PR54RESPUESTA IS NULL"
        Set rstselect54 = objApp.rdoConnect.OpenResultset(strselect54)
        If rstselect43(0).Value + rstselect54(0).Value > 0 Then
          If blnPrimeravez Then
            intResp = MsgBox("No ha contestado al Cuestionario obligatorio de alguna Actuaci�n asignada." & Chr(13) & _
                           "Si sale se desasignar�n las Actuaciones con Cuestionario obligatorio." & Chr(13) & _
                           "                �Desea salir?", vbExclamation + vbYesNo)
            If intResp = vbYes Then
              blnsalir = True
            Else
              blnsalir = False
              Exit Do
            End If
            blnPrimeravez = False
          End If
        sqlstr = "delete from pr4300 where pr52nummuestra=" & rstselect42("PR52NUMMUESTRA").Value & _
                 " and pr04numactplan=" & frmMuestrasPendientes.txtText1(0).Text
        objApp.rdoConnect.Execute sqlstr, 64
        sqlstr = "delete from pr4200 where pr52nummuestra=" & rstselect42("PR52NUMMUESTRA").Value & _
                 " and pr04numactplan=" & frmMuestrasPendientes.txtText1(0).Text
        objApp.rdoConnect.Execute sqlstr, 64
        strselect = "SELECT COUNT(*) from pr4200 where " _
                 & "PR52NUMMUESTRA = " & rstselect42("PR52NUMMUESTRA").Value
        Set rstselect = objApp.rdoConnect.OpenResultset(strselect)
        If rstselect(0).Value = 0 Then
          sqlstr = "delete from pr5400 where pr52nummuestra=" & rstselect42("PR52NUMMUESTRA").Value
          objApp.rdoConnect.Execute sqlstr, 64
          sqlstr = "delete from pr5200 where pr52nummuestra=" & rstselect42("PR52NUMMUESTRA").Value
          objApp.rdoConnect.Execute sqlstr, 64
        End If
        objApp.rdoConnect.Execute "Commit", 64
        End If
        rstselect43.Close
        Set rstselect43 = Nothing
        rstselect54.Close
        Set rstselect54 = Nothing
        rstselect42.MoveNext
      Loop
      rstselect42.Close
      Set rstselect42 = Nothing
      If blnsalir = True Then
        'strselect42 = "SELECT COUNT(*) from pr4200 where " _
        '         & "PR52NUMMUESTRA = " & Text1(3).Text
        'Set rstselect42 = objApp.rdoConnect.OpenResultset(strselect42)
        'If rstselect42(0).Value > 0 Then
        '  gblnunloadrellenardatosmuestra = True
        '  gblnactivarbotones = False
        'Else
        '  gblnunloadrellenardatosmuestra = False
        '  gblnactivarbotones = True
        'End If
        'rstselect42.Close
        'Set rstselect42 = Nothing
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      End If
    Case Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End Select
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "Muestras Extra�das" And strCtrl = "grdDBGrid1(1).Cod.Real" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "SG0200"
     
     Set objField = .AddField("SG02COD")
     objField.strSmallDesc = "C�digo Realizador"
         
     Set objField = .AddField("SG02APE1")
     objField.strSmallDesc = "Nombre"
     
         
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(6), .cllValues("SG02COD"))
      'objWinInfo.objWinActiveForm.blnChanged = True
     End If
   End With
   Set objSearch = Nothing
 End If
 
  If strFormName = "Muestras Extra�das" And strCtrl = "grdDBGrid1(1).Cod.Estado" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR5500"
     
     Set objField = .AddField("PR55CODESTADO")
     objField.strSmallDesc = "C�digo Estado"
         
     Set objField = .AddField("PR55DESESTADO")
     objField.strSmallDesc = "Estado"
     
         
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(11), .cllValues("PR55CODESTADO"))
      'objWinInfo.objWinActiveForm.blnChanged = True
     End If
   End With
   Set objSearch = Nothing
 End If

End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
    Case 60:
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
    Case 100:
        Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  End Select
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


