VERSION 5.00
Begin VB.Form frmcuestionario2parte 
   BackColor       =   &H00FFC0C0&
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Cuestionario de la Muestra"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFrame1 
      Caption         =   "Muestra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   1095
      Index           =   1
      Left            =   240
      TabIndex        =   22
      Top             =   240
      Width           =   11295
      Begin VB.TextBox txtmuestra 
         BackColor       =   &H00FF80FF&
         Height          =   330
         Left            =   240
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   480
         Width           =   5400
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Cuestionario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6255
      Index           =   0
      Left            =   240
      TabIndex        =   3
      Top             =   1560
      Width           =   11295
      Begin VB.CheckBox chkCheck2 
         Caption         =   "NO"
         Height          =   255
         Index           =   5
         Left            =   6240
         TabIndex        =   24
         Top             =   2640
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox txtText1 
         Height          =   315
         Index           =   5
         Left            =   6720
         MaxLength       =   30
         TabIndex        =   6
         Top             =   4800
         Visible         =   0   'False
         Width           =   5400
      End
      Begin VB.ComboBox cboCombo1 
         Height          =   315
         Index           =   5
         Left            =   6720
         TabIndex        =   5
         Top             =   4080
         Visible         =   0   'False
         Width           =   5650
      End
      Begin VB.CheckBox chkCheck1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "SI"
         Height          =   255
         Index           =   5
         Left            =   5040
         MaskColor       =   &H00FFFFFF&
         TabIndex        =   4
         Top             =   2640
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   375
         Index           =   4
         Left            =   10320
         TabIndex        =   21
         Top             =   3480
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   375
         Index           =   3
         Left            =   9960
         TabIndex        =   20
         Top             =   2640
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   375
         Index           =   2
         Left            =   10200
         TabIndex        =   19
         Top             =   1800
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   375
         Index           =   1
         Left            =   10440
         TabIndex        =   18
         Top             =   1440
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   375
         Index           =   0
         Left            =   10440
         TabIndex        =   17
         Top             =   840
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   360
         TabIndex        =   16
         Top             =   5040
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   360
         TabIndex        =   15
         Top             =   3930
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "lblLabel2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   360
         TabIndex        =   14
         Top             =   2820
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   360
         TabIndex        =   13
         Top             =   1725
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   12
         Top             =   600
         Width           =   495
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   840
         TabIndex        =   11
         Top             =   5040
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   840
         TabIndex        =   10
         Top             =   3930
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   840
         TabIndex        =   9
         Top             =   2820
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   840
         TabIndex        =   8
         Top             =   1725
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   840
         TabIndex        =   7
         Top             =   600
         Width           =   9000
      End
   End
   Begin VB.CommandButton cmdfinal 
      Caption         =   "Fin del Cuestionario"
      Height          =   375
      Index           =   0
      Left            =   9000
      TabIndex        =   2
      Top             =   8040
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdatras 
      Caption         =   "Atr�s"
      Height          =   375
      Left            =   5400
      TabIndex        =   1
      Top             =   8040
      Width           =   1695
   End
   Begin VB.CommandButton cmdsiguiente 
      Caption         =   "Siguiente"
      Height          =   375
      Left            =   7200
      TabIndex        =   0
      Top             =   8040
      Width           =   1695
   End
End
Attribute VB_Name = "frmcuestionario2parte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00148.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 20 DE OCTUBRE DE 1997                                         *
'* DESCRIPCI�N: Cuestionario de las muestras de una actuaci�n           *
'* ARGUMENTOS: <NINGUNO>                                                *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


' texto es una matriz para contabilizar los TextBox activados
Dim texto(5) As Integer
' casilla es una matriz para contabilizar los checkBox activados
Dim casilla(5) As Integer
' lista es una matriz para contabilizar los ComboBox activados
Dim lista(5) As Integer
'strmodif recoge la sentencia UPDATE
Dim strmodif As String
'intcuenta lleva la cuenta de los registros que hay que retroceder
Dim intcuenta As Integer
'inthay cuenta el n� de preguntas sacadas
Dim inthay As Integer
'blncargado indica si el bot�n "Fin del Cuestionario" est� cargado o no
Dim blncargado As Integer
'contmuestra lleva la cuenta del n�mero de muestras recorridas
Dim contmuestra As Integer
'intsobran cuenta el n� de preguntas sobrantes en una pantalla
Dim intsobran As Integer
'blnultimo indica si es la �ltima pregunta de una muestra(para ir a la siguiente muestra)
Dim blnultimo As Integer
'blnprimero indica si es la primera pregunta de la muestra(para ir a la muestra anterior)
Dim blnprimero As Integer
'rstM recorre las muestras
Dim rstM As rdoResultset
Dim strsqlM As String
'rstMultima es paralela a rstM ya que solo sirve para saber si estamos
'en la �ltima muestra o no, pero sin avanzar
Dim rstMultima As rdoResultset
Dim strsqlMultima As String
'rstA recorre las preguntas de una muestra
Dim rsta As rdoResultset
Dim strsqlA As String
'rstA1 recoge la descripci�n de la pregunta
Dim rstA1 As rdoResultset
Dim strsql1 As String
'rstA2 recoge cu�ntas preguntas tiene una muestra
Dim rstA2 As rdoResultset
Dim strsql2 As String
'rstA3 cuenta las preguntas de una muestra al pulsar el bot�n Atr�s
Dim rstA3 As rdoResultset
Dim strsql3 As String
'rstA4 cuenta el n� de muestras de la actuaci�n
Dim rstM4 As rdoResultset
Dim strsqlM4 As String
'rstA5 recoge las respuestas para llenar las Combo
Dim rstA5 As rdoResultset
Dim strsql5 As String
'rstA6 recoge el c�digo de las preguntas que est�n en pantalla
Dim rstA6 As rdoResultset
Dim strsql6 As String
'rstmuestra recoge la descripci�n de la muestra
Dim rstmuestra As rdoResultset
Dim strsqlmuestra As String
'strmensaje,strmensaje1,strmensaje2 para sacar mensajes por pantall
Dim strmensaje As String
Dim strmensaje1 As String
Dim strmensaje2 As String
'blnmantener_pantalla indica si hay que seguir en la misma pantalla porque alguna
'respuesta obligatoria no ha sido contestada
Dim blnmantener_pantalla As Integer
'blnvolver controla si se ha cargado el bot�n de ir al Cuestionario
'de la actuaci�n o no
Dim blnvolver As Integer



Private Sub cboCombo1_Click(Index As Integer)
    txtText1(Index).Text = cboCombo1(Index).Text
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  If chkCheck1(Index).Value = 1 Then
    chkCheck2(Index).Value = 0
  End If
  If chkCheck1(Index).Value = 0 Then
    chkCheck2(Index).Value = 1
  End If
  
End Sub
Private Sub chkCheck2_Click(Index As Integer)
  If chkCheck2(Index).Value = 1 Then
    chkCheck1(Index).Value = 0
  End If
  If chkCheck2(Index).Value = 0 Then
    chkCheck1(Index).Value = 1
  End If
  
End Sub

Private Sub cmdatras_Click()

gblniratras = 1

'se controla si estamos en la primera muestra o no
If contmuestra = 1 Then
If ((inthay + intsobran) - 10) = 0 Then
        'la muestra no ha terminado
        blnvolver = 0
End If
If ((inthay + intsobran) - 10) = -5 Then
        'la muestra ha acabado hay que visualizar el cuestionario de la actuaci�n
        blnvolver = 1
End If
End If


If blnvolver = 1 Then
  gblncuestionario2 = 1
  Unload Me
  blnvolver = 0
  Exit Sub
End If
'se descarga el bot�n de Fin del Cuestionario si es que estaba cargado
If blncargado = 1 Then
   Unload cmdfinal(1)
   blncargado = 0
End If
cmdsiguiente.Enabled = True
blnultimo = 0
'se guardan los cambios de la pantalla actual antes de ir al anterior
Call Guardar_Cambios
If blnmantener_pantalla = 1 Then
blnmantener_pantalla = 0
Exit Sub
End If
'se calcula en intcuenta los registros a retroceder
intcuenta = (inthay + intsobran) - 10


'si (intcuenta=0) hay que visualizar las 5 primeras preguntas de la muestra
If (intcuenta = 0 And contmuestra = 1) Then
    blnvolver = 1
    cmdatras.Enabled = False
    cmdatras.Enabled = True

End If
'si estamos en la segunda muestra o m�s, y hay que retroceder a la muestra anterior
If (contmuestra > 1 And intcuenta = (-5)) Then
    Call Pasar_A_Muestra_Anterior
End If


If (intcuenta <> (-5)) Then
'vamos a los 5 anteriores registros de la muestra actual
inthay = intcuenta
strsqlA = "select * from PR4300 where (pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & "pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value & ")"
Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
For k = 1 To intcuenta
    rsta.MoveNext
Next k
End If


'se carga la nueva pantalla
Call rellenar_pantalla
End Sub

Private Sub cmdfinal_Click(Index As Integer)
'se miran qu� preguntas con respuesta obligatoria no han sido contestadas
For i = 0 To 4
 If texto(i) = 1 Then
   If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
          Cancel = 1
          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
       End If
 End If
 If casilla(i) = 1 Then
   If (chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0) Then
          Cancel = 1
          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
       End If
 End If
Next i
If Cancel = 1 Then
    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
    strmensaje = ""
    strmensaje1 = ""
    strmensaje2 = ""
    Exit Sub
End If
gblncuestionario2 = 2 'para que se descargue tambi�n frmcuestionario1parte
Unload Me
End Sub

Private Sub cmdsiguiente_Click()
blnvolver = 0
cmdatras.Enabled = True

'se guardan los cambios de la pantalla anterior
Call Guardar_Cambios
If blnmantener_pantalla = 1 Then
blnmantener_pantalla = 0
Exit Sub
End If
'si se cambia de muestra se llama al procedimiento siguiente_muestra
If (blnultimo = 1) Then
    blnultimo = 0
    Call siguiente_muestra
End If
'se carga la nueva pantalla
Call rellenar_pantalla
End Sub





Private Sub Form_Activate()
'pone la variable global gblncuestionario2 a 1 para que se recoja en la pantalla
'cuestionario1parte
gblncuestionario2 = 1
blnvolver = 1
cmdatras.Enabled = True


'se recorren las muestras
strsqlM = "select pr42nummuestra from PR4203J " & _
          "where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
On Error GoTo Err_Ejecutar
Set rstM = objApp.rdoConnect.OpenResultset(strsqlM)


strsqlmuestra = "select pr42desmuestra from PR4203J " & _
                "where pr03numactpedi=" & frmdatosactpedida.txtText1(0) & _
                " and pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value
On Error GoTo Err_Ejecutar
Set rstmuestra = objApp.rdoConnect.OpenResultset(strsqlmuestra)
txtmuestra = rstmuestra.rdoColumns("pr42desmuestra").Value
'se contabilizan las muestras recorridas
contmuestra = contmuestra + 1
'en paralelo se recorren con rstMultima
strsqlMultima = "select pr42nummuestra from PR4203J " & _
                "where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
On Error GoTo Err_Ejecutar
Set rstMultima = objApp.rdoConnect.OpenResultset(strsqlMultima)

'preguntas de la muestra
strsqlA = "select * from PR4300 where (pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
          & "pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value & ")"
On Error GoTo Err_Ejecutar
Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)

Call rellenar_pantalla

rstmuestra.Close
Set rstmuestra = Nothing
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub rellenar_pantalla()
'PROCEDIMIENTO QUE RELLENA UNA PANTALLA CON PREGUNTAS Y RESPUESTAS

'se rellena la pantalla con las 5 primeras preguntas de la muestra
For i = 0 To 4
       strsql1 = "select pr40despregunta from PR4000 " _
                & "where pr40codpregunta=" & rsta.rdoColumns("pr40codpregunta").Value
       Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
       lblLabel1(i) = rstA1.rdoColumns("pr40despregunta").Value
       lblLabel2(i) = inthay + 1
       'se testea el tipo de respuesta
       Select Case (rsta.rdoColumns("pr27codtiprespu").Value)
        Case 1
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("pr43respuesta").Value) Then 'respuesta
               txtText1(i).Text = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("pr43respuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            'se testea si la respuesta es obligatoria
            If (rsta.rdoColumns("pr43indoblig").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 2
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("pr43respuesta").Value) Then
                txtText1(i).Text = ""
            Else
                txtText1(i).Text = rsta.rdoColumns("pr43respuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            'Respuesta Obligatoria
            If (rsta.rdoColumns("pr43indoblig").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 3
            Load chkCheck1(i)
            Load chkCheck2(i)
            casilla(i) = 1
            chkCheck1(i).Visible = True
            chkCheck2(i).Visible = True
            If IsNull(rsta.rdoColumns("pr43respuesta").Value) Then
               chkCheck1(i).Value = 0
               chkCheck2(i).Value = 0
            Else
               If rsta.rdoColumns("pr43respuesta").Value = "-1" Then
                chkCheck1(i).Value = 1
               End If
               If rsta.rdoColumns("pr43respuesta").Value = "0" Then
                chkCheck2(i).Value = 1
               End If
            End If
            'Respuesta Obligatoria
            If (rsta.rdoColumns("pr43indoblig").Value = -1) Then
                chkCheck1(i).BackColor = &HFFFF00
                chkCheck2(i).BackColor = &HFFFF00
            End If
            chkCheck1(i).Left = 840
            chkCheck1(i).Left = 1700
            Select Case i
            Case 0
            chkCheck1(i).Top = 840
            chkCheck2(i).Top = 840
            Case 1
            chkCheck1(i).Top = 2040
            chkCheck2(i).Top = 2040
            Case 2
            chkCheck1(i).Top = 3120
            chkCheck2(i).Top = 3120
            Case 3
            chkCheck1(i).Top = 4200
            chkCheck2(i).Top = 4200
            Case 4
            chkCheck1(i).Top = 5280
            chkCheck2(i).Top = 5280
            End Select
        Case 4
            Load cboCombo1(i)
            Load txtText1(i)
            txtText1(i).ZOrder (0)
            lista(i) = 1
            texto(i) = 1
            cboCombo1(i).Visible = True
            txtText1(i).Visible = True
            txtText1(i).Locked = True
            If IsNull(rsta.rdoColumns("pr43respuesta").Value) Then
               txtText1(i) = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("pr43respuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            'Respuesta Obligatoria
            If (rsta.rdoColumns("pr43indoblig").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            cboCombo1(i).Left = 840
            txtText1(i).Left = 840
            Select Case i
            Case 0
            cboCombo1(i).Top = 840
            txtText1(i).Top = 840
            txtText1(i).ZOrder (0)
            Case 1
            cboCombo1(i).Top = 2040
            txtText1(i).Top = 2040
            txtText1(i).ZOrder (0)
            Case 2
            cboCombo1(i).Top = 3120
            txtText1(i).Top = 3120
            txtText1(i).ZOrder (0)
            Case 3
            cboCombo1(i).Top = 4200
            txtText1(i).Top = 4200
            txtText1(i).ZOrder (0)
            Case 4
            cboCombo1(i).Top = 5280
            txtText1(i).Top = 5280
            txtText1(i).ZOrder (0)
            End Select
            ' se rellena la Combo seg�n el c�digo de lista de respuestas
            strsql5 = "select pr28desrespuesta from pr2800 " _
                  & "where pr46codlistresp=" & rsta.rdoColumns("pr46codlistresp").Value _
                  & " order by pr28numrespuesta"
            Set rstA5 = objApp.rdoConnect.OpenResultset(strsql5)
            While (Not rstA5.EOF)
                cboCombo1(i).AddItem rstA5.rdoColumns("pr28desrespuesta").Value
                rstA5.MoveNext
            Wend
            rstA5.Close
            Set rstA5 = Nothing
       End Select
    'si acaban las preguntas se sale del bucle
    If (rsta.EOF) Then
        Exit For
    Else
        inthay = inthay + 1
        rsta.MoveNext
        If rsta.EOF Then
            'blnultimo=1 indica que se han recorrido las preguntas de la muestra
            blnultimo = 1
            Exit For
        End If
    End If
  rstA1.Close
  Set rstA1 = Nothing
  Next i
  
    
  intsobran = 0
  For j = (i + 1) To 4 'vaciar el resto de campos
        lblLabel1(j) = ""
        lblLabel2(j) = ""
        intsobran = intsobran + 1
  Next j
  'se mira si hay m�s de 5 preguntas para activar el bot�n Siguiente
  strsql2 = "select count(*) from PR4300 where pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & "pr42nummuestra = " & rstM.rdoColumns("pr42nummuestra").Value
  On Error GoTo Err_Ejecutar
  Set rstA2 = objApp.rdoConnect.OpenResultset(strsql2)
  If (rstA2.rdoColumns(0).Value > 5) Then
      cmdsiguiente.Enabled = True
  End If
  rstA2.Close
  Set rstA2 = Nothing
      

   If (blnultimo = 1) Then
    strsqlM4 = "select count(*) from PR4203J where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rstM4 = objApp.rdoConnect.OpenResultset(strsqlM4)
    If (contmuestra = (rstM4.rdoColumns(0).Value)) Then
         cmdsiguiente.Enabled = False
         Load cmdfinal(1)
         blncargado = 1
         cmdfinal(1).Visible = True
    End If
    rstM4.Close
    Set rstM4 = Nothing
   End If
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
       
End Sub
Private Sub Guardar_Cambios()
'se miran qu� preguntas con respuesta obligatoria no han sido contestadas
For i = 0 To 4
 If texto(i) = 1 Then
   If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
          Cancel = 1
          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
       End If
 End If
 If casilla(i) = 1 Then
   If (chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0) Then
          Cancel = 1
          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
       End If
 End If
Next i
If Cancel = 1 Then
    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
    strmensaje = ""
    strmensaje1 = ""
    strmensaje2 = ""
    blnmantener_pantalla = 1
    Exit Sub
End If

'DESCARGA LOS CONTROLES Y HACE EL UPDATE DE LAS RESPUESTAS
  For i = 0 To 4
    If texto(i) = 1 Then
       strsql6 = "select pr40codpregunta from PR4000 " _
                & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       On Error GoTo Err_Ejecutar
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       strmodif = "UPDATE  PR4300 SET pr43respuesta= " & "'" & txtText1(i) & "'" _
            & " WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & "pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra") & "AND " _
            & "pr40codpregunta = " & rstA6.rdoColumns("pr40codpregunta").Value
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       txtText1(i).Locked = False
       Unload txtText1(i)
       texto(i) = 0
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If casilla(i) = 1 Then
       strsql6 = "select pr40codpregunta from PR4000 " _
                 & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If chkCheck1(i).Value = 1 Then
        strmodif = "UPDATE  PR4300 SET pr43respuesta= " & "'" & -1 & "'" _
            & " WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & " pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra") & "AND " _
            & "pr40codpregunta =" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       If chkCheck2(i).Value = 1 Then
        strmodif = "UPDATE  PR4300 SET pr43respuesta= " & "'" & 0 & "'" _
            & " WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & " pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra") & "AND " _
            & "pr40codpregunta =" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload chkCheck1(i)
       Unload chkCheck2(i)
       casilla(i) = 0
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If lista(i) = 1 Then
        cboCombo1(i).Clear
        Unload cboCombo1(i)
        lista(i) = 0
    End If
Next i
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


Private Sub siguiente_muestra()
'LA MUESTRA ANTERIOR HA ACABADO, SE RECORRE UNA NUEVA MUESTRA

blnultimo = 0
'se coge la siguiente muestra
rstM.MoveNext

'se busca la descripci�n de la muestra para escribirla en pantalla
strsqlmuestra = "select pr42desmuestra from PR4203J " & _
                "where pr03numactpedi=" & frmdatosactpedida.txtText1(0) _
               & " and pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value
On Error GoTo Err_Ejecutar
Set rstmuestra = objApp.rdoConnect.OpenResultset(strsqlmuestra)
txtmuestra = rstmuestra.rdoColumns("pr42desmuestra").Value



'se incrementa el n� de muestras recorridas
contmuestra = contmuestra + 1
inthay = 0
intsobran = 0
'se mira si existe esa siguiente muestra o no
If (rstM.EOF) Then
    MsgBox "no hay m�s muestras"
Else
    'se recogen las preguntas de la nueva muestra
    strsqlA = "select * from PR4300 where (pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
              & "pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value & ")"
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
End If
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub Pasar_A_Muestra_Anterior()
Dim intresto As Integer
Dim intcontador As Integer
inthay = 0
intsobran = 0
'hay que ir a la muestra anterior
contmuestra = contmuestra - 1
'se recorren las muestras
    strsqlM = "select pr42nummuestra from PR4203J " & _
              "where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rstM = objApp.rdoConnect.OpenResultset(strsqlM)
    'en paralelo se recorren con rstMultima
    strsqlMultima = "select pr42nummuestra from PR4203J " & _
                    "where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rstMultima = objApp.rdoConnect.OpenResultset(strsqlMultima)
For l = 1 To (contmuestra - 1)
    rstM.MoveNext
    rstMultima.MoveNext
Next l

'se busca la descripci�n de la muestra para escribirla en pantalla
strsqlmuestra = "select pr42desmuestra from PR4203J " & _
                "where pr03numactpedi=" & frmdatosactpedida.txtText1(0) _
               & " and pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value
On Error GoTo Err_Ejecutar
Set rstmuestra = objApp.rdoConnect.OpenResultset(strsqlmuestra)
txtmuestra = rstmuestra.rdoColumns("pr42desmuestra").Value

'ya estamos en la muestra anterior,se cuenta el n� de preguntas en intcontador
strsqlA3 = "select count(*) from PR4300 " & _
           "where (pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
              & "pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value & ")"
On Error GoTo Err_Ejecutar
Set rstA3 = objApp.rdoConnect.OpenResultset(strsqlA3)
intcontador = rstA3.rdoColumns(0).Value
rstA3.Close
Set rstA3 = Nothing
intresto = intcontador Mod 5
strsqlA = "select * from PR4300 " & _
          "where (pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
              & "pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra").Value & ")"
On Error GoTo Err_Ejecutar
Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)

If (intresto = 0) Then
    For m = 1 To (intcontador - 5)
        rsta.MoveNext
    Next m
    inthay = (intcontador - 5)
    intsobran = 0
Else
    For n = 1 To (intcontador - intresto)
        rsta.MoveNext
    Next n
    inthay = (intcontador - intresto)
    intsobran = (5 - intresto)
End If

rstmuestra.Close
Set rstmuestra = Nothing
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'se miran qu� preguntas con respuesta obligatoria no han sido contestadas
For i = 0 To 4
 If texto(i) = 1 Then
   If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
          Cancel = 1
          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
       End If
 End If
 If casilla(i) = 1 Then
   If (chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0) Then
          Cancel = 1
          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
       End If
 End If
Next i
If Cancel = 1 Then
    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
    strmensaje = ""
    strmensaje1 = ""
    strmensaje2 = ""
    Exit Sub
End If

'DESCARGA LOS CONTROLES Y HACE EL UPDATE DE LAS RESPUESTAS
  For i = 0 To 4
    If texto(i) = 1 Then
       txtText1(i).Locked = False
       strsql6 = "select pr40codpregunta from PR4000 " _
                & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       On Error GoTo Err_Ejecutar
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       strmodif = "UPDATE  PR4300 SET pr43respuesta= " & "'" & txtText1(i) & "'" _
            & " WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & "pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra") & "AND " _
            & "pr40codpregunta = " & rstA6.rdoColumns("pr40codpregunta").Value
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       texto(i) = 0
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If casilla(i) = 1 Then
       strsql6 = "select pr40codpregunta from PR4000 " _
                 & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If chkCheck1(i).Value = 1 Then
        strmodif = "UPDATE  PR4300 SET pr43respuesta= " & "'" & -1 & "'" _
            & " WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & " pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra") & "AND " _
            & "pr40codpregunta =" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       If chkCheck2(i).Value = 1 Then
        strmodif = "UPDATE  PR4300 SET pr43respuesta= " & "'" & 0 & "'" _
            & " WHERE pr03numactpedi=" & frmdatosactpedida.txtText1(0) & " AND " _
            & " pr42nummuestra=" & rstM.rdoColumns("pr42nummuestra") & "AND " _
            & "pr40codpregunta =" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload chkCheck1(i)
       Unload chkCheck2(i)
       casilla(i) = 0
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If lista(i) = 1 Then
        cboCombo1(i).Clear
        Unload cboCombo1(i)
        lista(i) = 0
    End If
Next i

'*****************IRENE VIERNES 13-3-98
Call expandir_respuesta
Call expandir_respuesta_muestra


rstM.Close
Set rstM = Nothing
rstMultima.Close
Set rstMultima = Nothing
rsta.Close
Set rsta = Nothing
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub



Private Sub txtText1_LostFocus(Index As Integer)
Dim strmen As String
' verificar que se metan s�lo n�meros en los campos num�ricos
If (lblLabel3(Index).Caption = "1") Then
  If (IsNumeric(txtText1(Index).Text)) = False And (txtText1(Index).Text <> "") Then
    Beep
    strmen = MsgBox("Datos no Num�ricos", vbCritical, "Datos no Num�ricos")
    txtText1(Index).SetFocus
  End If
End If
End Sub

Private Sub expandir_respuesta()
' Procedimiento llamado al salir del cuestionario(QUERYUNLOAD). Coge cada una de las preguntas del
' cuestionario y mira si alguna de las actuaciones de la misma petici�n contiene alguna
' de esas preguntas bien en su cuestionario o bien en el cuestionario de sus muestras.
' Si alguna de estas preguntas coincide se copia la respuesta para que el usuario tenga
' ya esas preguntas contestadas.

' rstpreg recorre las preguntas del cuestionario que se acaba de responder
Dim rstpreg As rdoResultset
Dim strpreg As String
' rstact recorre las actuaciones de la misma petici�n
Dim rstact As rdoResultset
Dim stract As String
' rstpregact recorre las preguntas de las actuaciones de la petici�n
Dim rstpregact As rdoResultset
Dim strpregact As String
Dim strupdate As String

'se cogen todas las preguntas del cuestionario en curso
strpreg = "select PR40CODPREGUNTA,PR41RESPUESTA from PR4100 " & _
          "where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
Set rstpreg = objApp.rdoConnect.OpenResultset(strpreg)

' mientras el cuestionario tenga preguntas
While Not rstpreg.EOF
        'cuespreg = rstpreg.rdoColumns("pr40codpregunta").Value
        'cuesres = rstpreg.rdoColumns("pr41respuesta").Value
    'se buscan las actuaciones que pertenecen a la misma petici�n
    stract = "select PR03NUMACTPEDI from PR0300 where pr09numpeticion=" & frmdatosactpedida.txtText1(12)
    Set rstact = objApp.rdoConnect.OpenResultset(stract)
    'mientras haya actuaciones de la misma petici�n
    While Not rstact.EOF
        'la actuaci�n en curso no se trata
        If rstact.rdoColumns("pr03numactpedi").Value <> frmdatosactpedida.txtText1(0) Then
        
            ' se recorren las preguntas de la actuacion (PR4100)
            strpregact = "select PR40CODPREGUNTA from PR4100 " & _
                     "where pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value
            Set rstpregact = objApp.rdoConnect.OpenResultset(strpregact)
            While Not rstpregact.EOF
                'si la pregunta est� repetida en otra actuaci�n se actualiza la respuesta
                If rstpreg.rdoColumns("pr40codpregunta").Value = rstpregact.rdoColumns("pr40codpregunta") Then
                    strupdate = "UPDATE  PR4100 SET pr41respuesta= " & "'" & _
                    rstpreg.rdoColumns("pr41respuesta").Value & "'" & _
                    " WHERE pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value & _
                    " AND pr40codpregunta=" & rstpregact.rdoColumns("pr40codpregunta").Value
                    On Error GoTo Err_Ejecutar
                    objApp.rdoConnect.Execute strupdate, 64
                    objApp.rdoConnect.Execute "Commit", 64
                End If
            rstpregact.MoveNext
            Wend
            rstpregact.Close
            Set rstpregact = Nothing
            
            ' se recorren las preguntas de las muestras de la actuacion (PR4300)
            strpregact = "select PR40CODPREGUNTA from PR4300 " & _
                     "where pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value
            Set rstpregact = objApp.rdoConnect.OpenResultset(strpregact)
            While Not rstpregact.EOF
                'si la pregunta est� repetida en una muestra se actualiza la respuesta
                If rstpreg.rdoColumns("pr40codpregunta").Value = rstpregact.rdoColumns("pr40codpregunta") Then
                    strupdate = "UPDATE  PR4300 SET pr43respuesta= " & "'" & _
                    rstpreg.rdoColumns("pr41respuesta").Value & "'" & _
                    " WHERE pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value & _
                    " AND pr40codpregunta=" & rstpregact.rdoColumns("pr40codpregunta").Value
                    On Error GoTo Err_Ejecutar
                    objApp.rdoConnect.Execute strupdate, 64
                    objApp.rdoConnect.Execute "Commit", 64
                End If
            rstpregact.MoveNext
            Wend
            rstpregact.Close
            Set rstpregact = Nothing
        End If
    rstact.MoveNext
    Wend
rstact.Close
Set rstact = Nothing
rstpreg.MoveNext
Wend

rstpreg.Close
Set rstpreg = Nothing

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



Private Sub expandir_respuesta_muestra()
' Procedimiento llamado al salir del cuestionario(QUERYUNLOAD). Coge cada una de las preguntas
' de las muestras y mira si alguna de las actuaciones de la misma petici�n contiene alguna
' de esas preguntas bien en su cuestionario o bien en el cuestionario de sus muestras.
' Si alguna de estas preguntas coincide se copia la respuesta para que el usuario tenga
' ya esas preguntas contestadas.

' rstpreg recorre las preguntas del cuestionario que se acaba de responder
Dim rstpreg As rdoResultset
Dim strpreg As String
' rstact recorre las actuaciones de la misma petici�n
Dim rstact As rdoResultset
Dim stract As String
' rstpregact recorre las preguntas de las actuaciones de la petici�n
Dim rstpregact As rdoResultset
Dim strpregact As String

Dim strupdate As String

'se cogen todas las preguntas del cuestionario en curso
strpreg = "select PR40CODPREGUNTA,PR43RESPUESTA from PR4300 " & _
          "where pr03numactpedi=" & frmdatosactpedida.txtText1(0)
Set rstpreg = objApp.rdoConnect.OpenResultset(strpreg)

' mientras el cuestionario tenga preguntas
While Not rstpreg.EOF
    'se buscan las actuaciones que pertenecen a la misma petici�n
    stract = "select PR03NUMACTPEDI from PR0300 where pr09numpeticion=" & frmdatosactpedida.txtText1(12)
    Set rstact = objApp.rdoConnect.OpenResultset(stract)
    'mientras haya actuaciones de la misma petici�n
    While Not rstact.EOF
        'la actuaci�n en curso no se trata
        If rstact.rdoColumns("pr03numactpedi").Value <> frmdatosactpedida.txtText1(0) Then
        
            ' se recorren las preguntas de la actuacion (PR4100)
            strpregact = "select PR40CODPREGUNTA from PR4100 " & _
                     "where pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value
            Set rstpregact = objApp.rdoConnect.OpenResultset(strpregact)
            While Not rstpregact.EOF
                'si la pregunta est� repetida en otra actuaci�n se actualiza la respuesta
                If rstpreg.rdoColumns("pr40codpregunta").Value = rstpregact.rdoColumns("pr40codpregunta") Then
                    strupdate = "UPDATE  PR4100 SET pr41respuesta= " & "'" & _
                    rstpreg.rdoColumns("pr43respuesta").Value & "'" & _
                    " WHERE pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value & _
                    " AND pr40codpregunta=" & rstpregact.rdoColumns("pr40codpregunta").Value
                    On Error GoTo Err_Ejecutar
                    objApp.rdoConnect.Execute strupdate, 64
                    objApp.rdoConnect.Execute "Commit", 64
                End If
            rstpregact.MoveNext
            Wend
            rstpregact.Close
            Set rstpregact = Nothing
            
            ' se recorren las preguntas de las muestras de la actuacion (PR4300)
            strpregact = "select PR40CODPREGUNTA from PR4300 " & _
                     "where pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value
            Set rstpregact = objApp.rdoConnect.OpenResultset(strpregact)
            While Not rstpregact.EOF
                'si la pregunta est� repetida en una muestra se actualiza la respuesta
                If rstpreg.rdoColumns("pr40codpregunta").Value = rstpregact.rdoColumns("pr40codpregunta") Then
                    strupdate = "UPDATE  PR4300 SET pr43respuesta= " & "'" & _
                    rstpreg.rdoColumns("pr43respuesta").Value & "'" & _
                    " WHERE pr03numactpedi=" & rstact.rdoColumns("pr03numactpedi").Value & _
                    " AND pr40codpregunta=" & rstpregact.rdoColumns("pr40codpregunta").Value
                    On Error GoTo Err_Ejecutar
                    objApp.rdoConnect.Execute strupdate, 64
                    objApp.rdoConnect.Execute "Commit", 64
                End If
            rstpregact.MoveNext
            Wend
            rstpregact.Close
            Set rstpregact = Nothing
        End If
    rstact.MoveNext
    Wend
    rstact.Close
    Set rstact = Nothing
 rstpreg.MoveNext
 Wend

rstpreg.Close
Set rstpreg = Nothing


Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub






