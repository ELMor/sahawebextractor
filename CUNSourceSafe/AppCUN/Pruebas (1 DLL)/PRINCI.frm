VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.tlb"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form frmdefactprincipal 
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Principal"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFrame1 
      Height          =   2655
      Left            =   4440
      TabIndex        =   2
      Top             =   960
      Width           =   4335
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   1320
         TabIndex        =   1
         Top             =   1680
         Width           =   1815
      End
      Begin VB.CommandButton cmdContrPetic 
         Caption         =   "Controlar Peticiones"
         Height          =   375
         Left            =   1320
         TabIndex        =   0
         Top             =   840
         Width           =   1815
      End
   End
   Begin Crystal.CrystalReport crCrystalReport1 
      Left            =   3360
      Top             =   360
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog1 
      Left            =   2280
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin ComctlLib.ImageList imlImageList1 
      Left            =   1080
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Grupos"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Actuaciones C.U.N"
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Interacciones"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Paquetes"
         Index           =   40
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "P&rotocolos"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   70
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda    F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmdefactprincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00121.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: Men� principal de Definici�n de Actuaciones             *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Private Sub cmdCommand1_Click(Index As Integer)
    Select Case Index
        Case 0
            Load frmPeticPend
            'frmpedir!tabTab1(0).Tab = 0 'para mostrar el detalle del Tab
            Call frmPeticPend.Show(vbModal)
            Unload frmPeticPend
            Set frmPeticPend = Nothing
        Case 1
            Unload Me
    End Select
End Sub
Private Sub cmdPetPend_Click()
    Load frmPeticPend
    Call frmPeticPend.Show(vbModal)
    Unload frmPeticPend
    Set frmPeticPend = Nothing
End Sub

Private Sub Command1_Click()
            Load frmdatosactpedida
            
            Call frmdatosactpedida.Show(vbModal)
            Unload frmdatosactpedida
            Set frmdatosactpedida = Nothing
End Sub

Private Sub cmdRecibirPac_Click()
    Load frmRecibirPaciente
    Call frmRecibirPaciente.Show(vbModal)
    Unload frmRecibirPaciente
    Set frmRecibirPaciente = Nothing
End Sub

Private Sub cmdContrPetic_Click()
    cmdContrPetic.Enabled = False
    Load frmControlarPet
    Call frmControlarPet.Show(vbModal)
    Unload frmControlarPet
    Set frmControlarPet = Nothing
    cmdContrPetic.Enabled = True
End Sub


Private Sub Form_Load()
  Dim strEntorno1 As String
  Dim strEntorno2 As String
  
  Call InitApp
 
  With objApp
  
    Call .Register(App, Screen)
    
    Set .frmFormMain = Me
    Set .imlImageList = imlImageList1
    Set .crCrystal = crCrystalReport1
    Set .dlgCommon = dlgCommonDialog1
    
  End With
  
  strEntorno1 = "Entorno de Desarrollo"
  strEntorno2 = "Entorno de Explotaci�n"
  
  With objEnv
    Call .AddEnv(strEntorno1)
    Call .AddValue(strEntorno1, "Main", "")
  
    Call .AddEnv(strEntorno2)
    Call .AddValue(strEntorno2, "Main", "")
  
  End With
  
  objApp.blnUseRegistry = True
  
  
  'JOL 24-7-1997
  objApp.strUserName = "cun"
  objApp.strPassword = "cun"
  objApp.strDataSource = "Oracle7 tables"
  If Not objApp.CreateInfo Then
    Call ExitApp
  End If
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
  Call ExitApp
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
    Case 20
      Call objApp.About
  End Select
End Sub



