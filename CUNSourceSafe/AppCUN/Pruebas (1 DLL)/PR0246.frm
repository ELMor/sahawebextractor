VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmcuestMuestra2 
   BackColor       =   &H00FFC0C0&
   Caption         =   "GESTI�N DE ACTUACIONES. Extracciones. Cuestionario de la muestra asignada"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFrame1 
      Caption         =   "Cuestionario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6255
      Index           =   1
      Left            =   240
      TabIndex        =   4
      Top             =   1560
      Width           =   11295
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   0
         Left            =   10200
         TabIndex        =   34
         Text            =   "Text1"
         Top             =   4200
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   1
         Left            =   10200
         TabIndex        =   33
         Text            =   "Text1"
         Top             =   4560
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   2
         Left            =   10200
         TabIndex        =   32
         Text            =   "Text1"
         Top             =   4920
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   3
         Left            =   10200
         TabIndex        =   31
         Text            =   "Text1"
         Top             =   5280
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Index           =   4
         Left            =   10200
         TabIndex        =   30
         Text            =   "Text1"
         Top             =   5640
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.CheckBox chkCheck2 
         Caption         =   "NO"
         Height          =   255
         Index           =   5
         Left            =   1320
         TabIndex        =   23
         Top             =   2160
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkCheck1 
         Caption         =   "SI"
         Height          =   255
         Index           =   5
         Left            =   360
         TabIndex        =   22
         Top             =   2160
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox txtText1 
         Height          =   315
         Index           =   5
         Left            =   840
         MaxLength       =   30
         TabIndex        =   5
         Top             =   3240
         Visible         =   0   'False
         Width           =   5400
      End
      Begin SSDataWidgets_B.SSDBCombo cboCombo1 
         Height          =   315
         Index           =   5
         Left            =   1320
         TabIndex        =   29
         Top             =   1080
         Visible         =   0   'False
         Width           =   5655
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   9975
         Columns(0).Caption=   "Respuesta"
         Columns(0).Name =   "Respuesta"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "C�digo"
         Columns(1).Name =   "C�digo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   9966
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   255
         Index           =   4
         Left            =   10080
         TabIndex        =   28
         Top             =   3720
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   255
         Index           =   3
         Left            =   10080
         TabIndex        =   27
         Top             =   3240
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   255
         Index           =   2
         Left            =   10080
         TabIndex        =   26
         Top             =   2880
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   255
         Index           =   1
         Left            =   10080
         TabIndex        =   25
         Top             =   2520
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblLabel4 
         Caption         =   "Label4"
         Height          =   255
         Index           =   0
         Left            =   10080
         TabIndex        =   24
         Top             =   2160
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   840
         TabIndex        =   20
         Top             =   570
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   840
         TabIndex        =   19
         Top             =   1725
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   840
         TabIndex        =   18
         Top             =   2820
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   840
         TabIndex        =   17
         Top             =   3930
         Width           =   9000
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   840
         TabIndex        =   16
         Top             =   5040
         Width           =   9000
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   15
         Top             =   570
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   360
         TabIndex        =   14
         Top             =   1725
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   360
         TabIndex        =   13
         Top             =   2820
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   360
         TabIndex        =   12
         Top             =   3930
         Width           =   495
      End
      Begin VB.Label lblLabel2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   360
         TabIndex        =   11
         Top             =   5040
         Width           =   495
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   255
         Index           =   0
         Left            =   10080
         TabIndex        =   10
         Top             =   360
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   255
         Index           =   1
         Left            =   10080
         TabIndex        =   9
         Top             =   720
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   255
         Index           =   2
         Left            =   10080
         TabIndex        =   8
         Top             =   1080
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   255
         Index           =   3
         Left            =   10080
         TabIndex        =   7
         Top             =   1440
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblLabel3 
         Caption         =   "Label3"
         Height          =   255
         Index           =   4
         Left            =   10080
         TabIndex        =   6
         Top             =   1800
         Visible         =   0   'False
         Width           =   735
      End
   End
   Begin VB.Frame fraFrame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Muestra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   1095
      Index           =   0
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Width           =   11295
      Begin VB.TextBox txtactuacionpedida 
         BackColor       =   &H00FFC0FF&
         Height          =   330
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   480
         Width           =   5400
      End
   End
   Begin VB.CommandButton cmdfinal 
      Caption         =   "Fin del Cuestionario"
      Height          =   375
      Index           =   0
      Left            =   9000
      TabIndex        =   2
      Top             =   8040
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.CommandButton cmdatras 
      Caption         =   "Atr�s"
      Height          =   375
      Left            =   5400
      TabIndex        =   1
      Top             =   8040
      Width           =   1695
   End
   Begin VB.CommandButton cmdsiguiente 
      Caption         =   "Siguiente"
      Height          =   375
      Left            =   7200
      TabIndex        =   0
      Top             =   8040
      Width           =   1695
   End
End
Attribute VB_Name = "frmcuestMuestra2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'*******************************************************************************
'* PROYECTO: PRUEBAS                                                           *
'* NOMBRE: PR00217.FRM                                                         *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                               *
'* FECHA: 25 DE MAYO DE 1998                                                   *
'* DESCRIPCI�N: Se muestra el cuestionario asociado a una actuaci�n-muestra    *
'*               para ser respondido                                           *
'* ACTUALIZACIONES:                                                            *
'*******************************************************************************
Dim strsql As String
Dim rsta As rdoResultset
Dim strsql1 As String
Dim rstA1 As rdoResultset
' inthay lleva la cuenta de los registros leidos
Dim inthay As Integer
' intsobran cuenta las preguntas en blanco de la pantalla
Dim intsobran As Integer
' texto es una matriz para contabilizar los TextBox activados
Dim texto(5) As Integer
' casilla es una matriz para contabilizar los checkBox activados
Dim casilla(5) As Integer
' lista es una matriz para contabilizar los ComboBox activados
Dim lista(5) As Integer
'blncargado indica si el bot�n "Fin del Cuestionario" est� cargado o no
Dim blncargado As Integer
'strmensaje,strmensaje1,strmensaje2 para guardar los mensajes que se sacan por pantalla
Dim strmensaje As String
Dim strmensaje1 As String
Dim strmensaje2 As String
'intnoqueryunload controla que no se haga el evento Unload
Dim intnoqueryunload As Integer
'pantallas indica cu�ntas pantallas se recorren para saber en cmdatras si hay
'que descargar la segunda parte del cuestionario y volver a la primera o no
Dim pantallas As Integer



Private Sub cboCombo1_Click(Index As Integer)
    txtText1(Index).Text = cboCombo1(Index).Columns(0).Text
    Text1(Index).Text = cboCombo1(Index).Columns(1).Text
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  If chkCheck1(Index).Value = 1 Then
    chkCheck2(Index).Value = 0
  End If
  If chkCheck1(Index).Value = 0 Then
    chkCheck2(Index).Value = 1
  End If
End Sub
Private Sub chkCheck2_Click(Index As Integer)
  If chkCheck2(Index).Value = 1 Then
    chkCheck1(Index).Value = 0
  End If
  If chkCheck2(Index).Value = 0 Then
    chkCheck1(Index).Value = 1
  End If
End Sub

Private Sub cmdatras_Click()
Dim strsql5 As String
Dim rstA5 As rdoResultset
Dim strsql6 As String
Dim rstA6 As rdoResultset
Dim strmodif As String
' intcuenta cuenta cu�ntos registros hay que recorrer
' antes de escribir los de la p�g anterior
Dim intcuenta As Integer

'si pantallas=0 estoy en la inicial,debo volver a la primera parte del cuestionario
If pantallas = 0 Then

''se miran qu� preguntas con respuesta obligatoria no han sido contestadas
'For i = 0 To 4
' If texto(i) = 1 Then
'   If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'       End If
' End If
' If casilla(i) = 1 Then
'   If (chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0) Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'  End If
' End If
'Next i
'If Cancel = 1 Then
'    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
'    strmensaje = ""
'    strmensaje1 = ""
'    strmensaje2 = ""
'    Exit Sub
'End If

'gblncuestvolvermuestra=1 para que no se haga el activate de la 1� parte
gblncuestvolvermuestra = 1

' Unload de los controles de la pantalla y UPDATE en la BD
For i = 0 To 4
    If texto(i) = 1 Then
      
       txtText1(i).Locked = False
       strsql6 = "select pr40codpregunta from PR4000 " _
                & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       On Error GoTo Err_Ejecutar
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If Text1(i).Text <> "" Then
       strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
            & ",PR28NUMRESPUESTA=" & Text1(i).Text _
            & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
              " AND pr04numactplan=" & lblLabel4(i).Caption & _
              " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       Else
       strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
            & ",PR28NUMRESPUESTA=null" _
            & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
              " AND pr04numactplan=" & lblLabel4(i).Caption & _
              " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       texto(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If casilla(i) = 1 Then
       strsql6 = "select pr40codpregunta from PR4000 " _
                 & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If chkCheck1(i).Value = 1 Then
            If Text1(i).Text <> "" Then
              strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" & _
                   ",PR28NUMRESPUESTA=" & Text1(i).Text & _
                   " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                   " AND pr04numactplan=" & lblLabel4(i).Caption & _
                   " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
            Else
              strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" & _
                   ",PR28NUMRESPUESTA=null" & _
                   " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                   " AND pr04numactplan=" & lblLabel4(i).Caption & _
                   " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
            End If
       End If
       If chkCheck2(i).Value = 1 Then
        If Text1(i).Text <> "" Then
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" & _
                ",PR28NUMRESPUESTA=" & Text1(i).Text & _
                " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        Else
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" & _
               ",PR28NUMRESPUESTA=null" & _
               " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        End If
       End If
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload chkCheck1(i)
       Unload chkCheck2(i)
       casilla(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If lista(i) = 1 Then
        cboCombo1(i).RemoveAll
        Unload cboCombo1(i)
        lista(i) = 0
        Text1(i).Text = ""
    End If
Next i
intnoqueryunload = 1
Unload Me
End If

'---------------------------------------------------------------------
If pantallas <> 0 Then
''se miran qu� preguntas con respuesta obligatoria no han sido contestadas
'For i = 0 To 4
' If texto(i) = 1 Then
'   If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'       End If
' End If
' If casilla(i) = 1 Then
'   If (chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0) Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'  End If
' End If
'Next i
'If Cancel = 1 Then
'    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
'    strmensaje = ""
'    strmensaje1 = ""
'    strmensaje2 = ""
'    Exit Sub
'End If

pantallas = pantallas - 1

' Unload de los controles de la pantalla y UPDATE en la BD
For i = 0 To 4
    If texto(i) = 1 Then
      
       txtText1(i).Locked = False
       strsql6 = "select pr40codpregunta from PR4000 " _
                & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       On Error GoTo Err_Ejecutar
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If Text1(i).Text <> "" Then
       strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
            & ",PR28NUMRESPUESTA=" & Text1(i).Text _
            & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
              " AND pr04numactplan=" & lblLabel4(i).Caption & _
              " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       Else
       strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
            & ",PR28NUMRESPUESTA=null" _
            & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
              " AND pr04numactplan=" & lblLabel4(i).Caption & _
              " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       texto(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If casilla(i) = 1 Then
       strsql6 = "select pr40codpregunta from PR4000 " _
                 & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If chkCheck1(i).Value = 1 Then
            If Text1(i).Text <> "" Then
              strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" & _
                   ",PR28NUMRESPUESTA=" & Text1(i).Text & _
                   " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                   " AND pr04numactplan=" & lblLabel4(i).Caption & _
                   " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
            Else
              strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" & _
                   ",PR28NUMRESPUESTA=null" & _
                   " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                   " AND pr04numactplan=" & lblLabel4(i).Caption & _
                   " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
            End If
       End If
       If chkCheck2(i).Value = 1 Then
        If Text1(i).Text <> "" Then
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" & _
                ",PR28NUMRESPUESTA=" & Text1(i).Text & _
                " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        Else
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" & _
               ",PR28NUMRESPUESTA=null" & _
               " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        End If
       End If
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload chkCheck1(i)
       Unload chkCheck2(i)
       casilla(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If lista(i) = 1 Then
        cboCombo1(i).RemoveAll
        Unload cboCombo1(i)
        lista(i) = 0
        Text1(i).Text = ""
    End If
Next i

If blncargado = 1 Then
    Unload cmdfinal(1)
    blncargado = 0
End If

'se calcula en intcuenta los registros a recorrer
intcuenta = (inthay + intsobran) - 10
' el bot�n Atr�s se deshabilita si estamos en las 5 primeras preguntas
If (intcuenta = 0) Then
    cmdatras.Enabled = True
End If

cmdsiguiente.Enabled = True
inthay = intcuenta

strsql = "select * from PR4300 where PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
         " AND pr04numactplan in " & gstractcuest
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
For k = 1 To intcuenta
    rsta.MoveNext
Next k
For i = 0 To 4
       strsql1 = "select pr40despregunta from PR4000 " _
                & "where pr40codpregunta=" & rsta.rdoColumns("pr40codpregunta").Value
       Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
       lblLabel1(i) = rstA1.rdoColumns("pr40despregunta").Value
       lblLabel2(i) = inthay + 1
    
       ' C�digo Tipo de Respuesta(PR27CODTIPRESPU)
       Select Case (rsta.rdoColumns("pr27codtiprespu").Value)
        Case 1
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               txtText1(i).Text = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 2
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
                txtText1(i).Text = ""
            Else
                txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 3
            Load chkCheck1(i)
            Load chkCheck2(i)
            casilla(i) = 1
            chkCheck1(i).Visible = True
            chkCheck2(i).Visible = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
                chkCheck1(i).Value = 0
                chkCheck2(i).Value = 0
            Else
                If rsta.rdoColumns("PR43RESPUESTA").Value = "-1" Then
                  chkCheck1(i).Value = 1
                End If
                If rsta.rdoColumns("PR43RESPUESTA").Value = "0" Then
                  chkCheck2(i).Value = 1
                End If
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                chkCheck1(i).BackColor = &HFFFF00
                chkCheck2(i).BackColor = &HFFFF00
            End If
            chkCheck1(i).Left = 840
            chkCheck2(i).Left = 1700
            Select Case i
            Case 0
            chkCheck1(i).Top = 840
            chkCheck2(i).Top = 840
            Case 1
            chkCheck1(i).Top = 2040
            chkCheck2(i).Top = 2040
            Case 2
            chkCheck1(i).Top = 3120
            chkCheck2(i).Top = 3120
            Case 3
            chkCheck1(i).Top = 4200
            chkCheck2(i).Top = 4200
            Case 4
            chkCheck1(i).Top = 5280
            chkCheck2(i).Top = 5280
            End Select
        Case 4
            Load cboCombo1(i)
            cboCombo1(i).RemoveAll
            Load txtText1(i)
            txtText1(i).ZOrder (0)
            lista(i) = 1
            texto(i) = 1
            cboCombo1(i).Visible = True
            txtText1(i).Visible = True
            txtText1(i).Locked = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               txtText1(i) = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            cboCombo1(i).Left = 840
            txtText1(i).Left = 840
            Select Case i
            Case 0
            cboCombo1(i).Top = 840
            txtText1(i).Top = 840
            txtText1(i).ZOrder (0)
            Case 1
            cboCombo1(i).Top = 2040
            txtText1(i).Top = 2040
            txtText1(i).ZOrder (0)
            Case 2
            cboCombo1(i).Top = 3120
            txtText1(i).Top = 3120
            txtText1(i).ZOrder (0)
            Case 3
            cboCombo1(i).Top = 4200
            txtText1(i).Top = 4200
            txtText1(i).ZOrder (0)
            Case 4
            cboCombo1(i).Top = 5280
            txtText1(i).Top = 5280
            txtText1(i).ZOrder (0)
            End Select
             ' se rellena la Combo
            'CODLISTRESP
            strsql5 = "select pr28desrespuesta,pr28numrespuesta from PR2800 " _
                  & "where PR46CODLISTRESP=" & rsta.rdoColumns("PR46CODLISTRESP").Value _
                  & " order by pr28numrespuesta"
            Set rstA5 = objApp.rdoConnect.OpenResultset(strsql5)
            While (Not rstA5.EOF)
                Call cboCombo1(i).AddItem(rstA5.rdoColumns("pr28desrespuesta").Value & ";" & rstA5.rdoColumns("pr28numrespuesta").Value)
                rstA5.MoveNext
            Wend
            rstA5.Close
            Set rstA5 = Nothing
            
       End Select
       
    
       If (rsta.EOF) Then
          Exit For
       Else
          inthay = inthay + 1
          rsta.MoveNext
          If rsta.EOF Then
             cmdsiguiente.Enabled = False
             Load cmdfinal(1)
             blncargado = 1
             cmdfinal(1).Visible = True
             Exit For
          End If
       End If
    rstA1.Close
    Set rstA1 = Nothing
    Next i
    
    intsobran = 0
 
End If

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub cmdfinal_Click(Index As Integer)
'gblncuestionariomuestra=1 para que al ir a la primera parte no se haga el activate
gblncuestionariomuestra = 1
Unload Me

End Sub

Private Sub cmdsiguiente_Click()
Dim strsql5 As String
Dim rstA5 As rdoResultset
Dim strmodif As String
Dim rstA6 As rdoResultset
Dim strsql6 As String


''se miran qu� preguntas con respuesta obligatoria no han sido contestadas
'For i = 0 To 4
' If texto(i) = 1 Then
'   If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'       End If
' End If
' If casilla(i) = 1 Then
'   If (chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0) Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'  End If
' End If
'Next i
'If Cancel = 1 Then
'    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
'    strmensaje = ""
'    strmensaje1 = ""
'    strmensaje2 = ""
'    Exit Sub
'End If

pantallas = pantallas + 1
cmdatras.Enabled = True

'Unload de los controles de la pantalla y UPDATE en la BD
For i = 0 To 4
    If texto(i) = 1 Then
       txtText1(i).Locked = False
       strsql6 = "select pr40codpregunta from PR4000 " _
                & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       On Error GoTo Err_Ejecutar
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If Text1(i).Text <> "" Then
        strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
             & ",PR28NUMRESPUESTA=" & Text1(i).Text _
             & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       Else
        strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
             & ",PR28NUMRESPUESTA=null" _
             & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       texto(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If casilla(i) = 1 Then
       strsql6 = "select pr40codpregunta from PR4000 " _
                 & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If chkCheck1(i).Value = 1 Then
        If Text1(i).Text <> "" Then
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
               & ",PR28NUMRESPUESTA=" & Text1(i).Text _
              & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        Else
          strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
               & ",PR28NUMRESPUESTA=null" _
              & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        End If
       End If
       If chkCheck2(i).Value = 1 Then
        If Text1(i).Text <> "" Then
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
              & ",PR28NUMRESPUESTA=" & Text1(i).Text _
             & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        Else
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
              & ",PR28NUMRESPUESTA=null" _
             & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        End If
       End If
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload chkCheck1(i)
       Unload chkCheck2(i)
       casilla(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If lista(i) = 1 Then
        cboCombo1(i).RemoveAll
        Unload cboCombo1(i)
        lista(i) = 0
        Text1(i).Text = ""
    End If
Next i
'se rellena la pantalla
For i = 0 To 4
       strsql1 = "select pr40despregunta from PR4000 " _
                & "where pr40codpregunta=" & rsta.rdoColumns("pr40codpregunta").Value
       Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
       lblLabel1(i) = rstA1.rdoColumns("pr40despregunta").Value
       lblLabel2(i) = inthay + 1
    
       'PR27CODTIPRESPU
       Select Case (rsta.rdoColumns("pr27codtiprespu").Value)
        Case 1
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
                txtText1(i).Text = ""
            Else
                txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 2
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
                txtText1(i).Text = ""
            Else
                txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 3
            Load chkCheck1(i)
            Load chkCheck2(i)
            casilla(i) = 1
            chkCheck1(i).Visible = True
            chkCheck2(i).Visible = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
                chkCheck1(i).Value = 0
                chkCheck2(i).Value = 0
            Else
                If rsta.rdoColumns("PR43RESPUESTA").Value = "-1" Then
                  chkCheck1(i).Value = 1
                End If
                If rsta.rdoColumns("PR43RESPUESTA").Value = "0" Then
                  chkCheck2(i).Value = 1
                End If
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                chkCheck1(i).BackColor = &HFFFF00
                chkCheck2(i).BackColor = &HFFFF00
            End If
            chkCheck1(i).Left = 840
            chkCheck2(i).Left = 1700
            Select Case i
            Case 0
            chkCheck1(i).Top = 840
            chkCheck2(i).Top = 840
            Case 1
            chkCheck1(i).Top = 2040
            chkCheck2(i).Top = 2040
            Case 2
            chkCheck1(i).Top = 3120
            chkCheck2(i).Top = 3120
            Case 3
            chkCheck1(i).Top = 4200
            chkCheck2(i).Top = 4200
            Case 4
            chkCheck1(i).Top = 5280
            chkCheck2(i).Top = 5280
            End Select
            
        Case 4
            Load cboCombo1(i)
            cboCombo1(i).RemoveAll
            Load txtText1(i)
            txtText1(i).ZOrder (0)
            lista(i) = 1
            texto(i) = 1
            cboCombo1(i).Visible = True
            txtText1(i).Visible = True
            txtText1(i).Locked = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               txtText1(i) = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            cboCombo1(i).Left = 840
            txtText1(i).Left = 840
            Select Case i
            Case 0
            cboCombo1(i).Top = 840
            txtText1(i).Top = 840
            txtText1(i).ZOrder (0)
            Case 1
            cboCombo1(i).Top = 2040
            txtText1(i).Top = 2040
            txtText1(i).ZOrder (0)
            Case 2
            cboCombo1(i).Top = 3120
            txtText1(i).Top = 3120
            txtText1(i).ZOrder (0)
            Case 3
            cboCombo1(i).Top = 4200
            txtText1(i).Top = 4200
            txtText1(i).ZOrder (0)
            Case 4
            cboCombo1(i).Top = 5280
            txtText1(i).Top = 5280
            txtText1(i).ZOrder (0)
            End Select
             ' se rellena la Combo
            'CODLISTRESP
            strsql5 = "select pr28desrespuesta,pr28numrespuesta from PR2800 " _
                  & "where PR46CODLISTRESP=" & rsta.rdoColumns("PR46CODLISTRESP").Value _
                  & " order by pr28numrespuesta"
            Set rstA5 = objApp.rdoConnect.OpenResultset(strsql5)
            While (Not rstA5.EOF)
                Call cboCombo1(i).AddItem(rstA5.rdoColumns("pr28desrespuesta").Value & ";" & rstA5.rdoColumns("pr28numrespuesta").Value)
                rstA5.MoveNext
            Wend
            rstA5.Close
            Set rstA5 = Nothing
       End Select
       
       
       If (rsta.EOF) Then
          Exit For
       Else
          inthay = inthay + 1
          rsta.MoveNext
          If rsta.EOF Then
             cmdsiguiente.Enabled = False
             Load cmdfinal(1)
             blncargado = 1
             cmdfinal(1).Visible = True
             Exit For
          End If
       End If
    rstA1.Close
    Set rstA1 = Nothing
    Next i
    
    intsobran = 0
    For j = (i + 1) To 4 'vaciar el resto de campos
        lblLabel1(j) = ""
        lblLabel2(j) = ""
        intsobran = intsobran + 1
    Next j
    
    
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


Private Sub Form_Activate()
'actuaciones pedidas de la peticion

Dim strsql4 As String
Dim rstA5 As rdoResultset
Dim intcontador As Integer
Dim intnumact As Integer

pantallas = 0

txtactuacionpedida.Text = frmMuestrasPendientes.txtText1(1).Text
cmdatras.Enabled = True

strsql = "select * from PR4300 where PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
         " AND pr04numactplan=" & frmMuestrasPendientes.txtText1(0).Text
On Error GoTo Err_Ejecutar
Set rsta = objApp.rdoConnect.OpenResultset(strsql)

    'sacar en pantalla 5 preguntas
    For i = 0 To 4
       strsql1 = "select pr40despregunta from PR4000 " _
                & "where pr40codpregunta=" & rsta.rdoColumns("pr40codpregunta").Value
       Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
       lblLabel1(i) = rstA1.rdoColumns("PR40DESPREGUNTA").Value
       lblLabel2(i) = inthay + 1
       
       'PR27CODTIPRESPU
       Select Case (rsta.rdoColumns("pr27codtiprespu").Value)
        Case 1
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               txtText1(i).Text = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 2
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
                txtText1(i).Text = ""
            Else
                txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 840
            Select Case i
            Case 0
            txtText1(i).Top = 840
            Case 1
            txtText1(i).Top = 2040
            Case 2
            txtText1(i).Top = 3120
            Case 3
            txtText1(i).Top = 4200
            Case 4
            txtText1(i).Top = 5280
            End Select
        Case 3
            Load chkCheck1(i)
            Load chkCheck2(i)
            casilla(i) = 1
            chkCheck1(i).Visible = True
            chkCheck2(i).Visible = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               chkCheck1(i).Value = 0
               chkCheck2(i).Value = 0
            Else
               If rsta.rdoColumns("PR43RESPUESTA").Value = "-1" Then
                chkCheck1(i).Value = 1
               End If
               If rsta.rdoColumns("PR43RESPUESTA").Value = "0" Then
                chkCheck2(i).Value = 1
              End If
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                chkCheck1(i).BackColor = &HFFFF00
                chkCheck2(i).BackColor = &HFFFF00
            End If
            chkCheck1(i).Left = 840
            chkCheck2(i).Left = 1700
            Select Case i
            Case 0
            chkCheck1(i).Top = 840
            chkCheck2(i).Top = 840
            Case 1
            chkCheck1(i).Top = 2040
            chkCheck2(i).Top = 2040
            Case 2
            chkCheck1(i).Top = 3120
            chkCheck2(i).Top = 3120
            Case 3
            chkCheck1(i).Top = 4200
            chkCheck2(i).Top = 4200
            Case 4
            chkCheck1(i).Top = 5280
            chkCheck2(i).Top = 5280
            End Select
        Case 4
            Load cboCombo1(i)
            cboCombo1(i).RemoveAll
            Load txtText1(i)
            txtText1(i).ZOrder (0)
            lista(i) = 1
            texto(i) = 1
            cboCombo1(i).Visible = True
            txtText1(i).Visible = True
            txtText1(i).Locked = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               txtText1(i) = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel4(i) = rsta.rdoColumns("pr04numactplan").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("PR43INDOBLIG").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            cboCombo1(i).Left = 840
            txtText1(i).Left = 840
            Select Case i
            Case 0
            cboCombo1(i).Top = 840
            txtText1(i).Top = 840
            txtText1(i).ZOrder (0)
            Case 1
            cboCombo1(i).Top = 2040
            txtText1(i).Top = 2040
            txtText1(i).ZOrder (0)
            Case 2
            cboCombo1(i).Top = 3120
            txtText1(i).Top = 3120
            txtText1(i).ZOrder (0)
            Case 3
            cboCombo1(i).Top = 4200
            txtText1(i).Top = 4200
            txtText1(i).ZOrder (0)
            Case 4
            cboCombo1(i).Top = 5280
            txtText1(i).Top = 5280
            txtText1(i).ZOrder (0)
            End Select
             ' se rellena la Combo
            'CODLISTRESP
            strsql5 = "select pr28desrespuesta,pr28numrespuesta from PR2800 " _
                  & "where PR46CODLISTRESP=" & rsta.rdoColumns("PR46CODLISTRESP").Value _
                  & " order by pr28numrespuesta"
            Set rstA5 = objApp.rdoConnect.OpenResultset(strsql5)
            While (Not rstA5.EOF)
                Call cboCombo1(i).AddItem(rstA5.rdoColumns("pr28desrespuesta").Value & ";" & rstA5.rdoColumns("pr28numrespuesta").Value)
                rstA5.MoveNext
            Wend
            rstA5.Close
            Set rstA5 = Nothing
            
       End Select
       
       
       If (rsta.EOF) Then
          Exit For
       Else
          inthay = inthay + 1
          rsta.MoveNext
          If rsta.EOF Then
             cmdsiguiente.Enabled = False
             Load cmdfinal(1)
             blncargado = 1
             cmdfinal(1).Visible = True
             Exit For
          End If
       End If
    rstA1.Close
    Set rstA1 = Nothing
    Next i
    
    
    intsobran = 0
    For j = (i + 1) To 4 'vaciar el resto de campos
        lblLabel1(j) = ""
        lblLabel2(j) = ""
        intsobran = intsobran + 1
    Next j


Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
If intnoqueryunload = 1 Then
    intnoqueryunload = 0
    Exit Sub
End If

If intnoqueryunload = 0 Then

''se miran qu� preguntas con respuesta obligatoria no han sido contestadas
'For i = 0 To 4
' If texto(i) = 1 Then
'   If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'       End If
' End If
' If casilla(i) = 1 Then
'   If (chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0) Then
'          Cancel = 1
'          strmensaje1 = "La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria."
'          strmensaje2 = strmensaje2 & Chr(13) & strmensaje1
'  End If
' End If
'Next i
'If Cancel = 1 Then
'    strmensaje = MsgBox(strmensaje2, vbCritical, "Cuestionario")
'    strmensaje = ""
'    strmensaje1 = ""
'    strmensaje2 = ""
'    Exit Sub
'End If

For i = 0 To 4
    If texto(i) = 1 Then
       'If (txtText1(i).BackColor = &HFFFF00 And txtText1(i) = "") Then
       '   Cancel = 1
       '   strmensaje = MsgBox("La respuesta a la pregunta " & lblLabel2(i) & " es obligatoria.", _
       '                vbCritical, "Cuestionario")
       '   Exit Sub
       'End If
       txtText1(i).Locked = False
       strsql6 = "select pr40codpregunta from PR4000 " _
                & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       On Error GoTo Err_Ejecutar
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If Text1(i).Text <> "" Then
        strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
             & ",PR28NUMRESPUESTA=" & Text1(i).Text _
             & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       Else
        strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
             & ",PR28NUMRESPUESTA=null" _
             & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr04numactplan=" & lblLabel4(i).Caption & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
       End If
       On Error GoTo Err_Ejecutar
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload txtText1(i)
       texto(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If casilla(i) = 1 Then
       strsql6 = "select pr40codpregunta from PR4000 " _
                 & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If chkCheck1(i).Value = 1 Then
        If Text1(i).Text <> "" Then
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
              & ",PR28NUMRESPUESTA=" & Text1(i).Text _
              & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        Else
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
              & ",PR28NUMRESPUESTA=null" _
              & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        End If
       End If
       If chkCheck2(i).Value = 1 Then
        If Text1(i).Text <> "" Then
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
              & ",PR28NUMRESPUESTA=" & Text1(i).Text _
              & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        Else
         strmodif = "UPDATE  PR4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
              & ",PR28NUMRESPUESTA=null" _
              & " WHERE PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                " AND pr04numactplan=" & lblLabel4(i).Caption & _
                " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        End If
       End If
       objApp.rdoConnect.Execute strmodif, 64
       objApp.rdoConnect.Execute "Commit", 64
       Unload chkCheck1(i)
       Unload chkCheck2(i)
       casilla(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
    End If
    If lista(i) = 1 Then
        cboCombo1(i).RemoveAll
        Unload cboCombo1(i)
        lista(i) = 0
        Text1(i).Text = ""
    End If
Next i
If blncargado = 1 Then
 Unload cmdfinal(1)
 blncargado = 0
End If

rsta.Close
Set rsta = Nothing
End If
Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub



Private Sub txtText1_LostFocus(Index As Integer)
Dim strmen As String
' verificar que se metan n�meros en los campos num�ricos
   If (lblLabel3(Index).Caption = "1") Then
        If (IsNumeric(txtText1(Index).Text)) = False And (txtText1(Index).Text <> "") Then
            Beep
            strmen = MsgBox("Datos no Num�ricos", vbCritical, "Datos no Num�ricos")
            txtText1(Index).SetFocus
        End If
   End If
End Sub



