VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmtiposdemuestra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Mantenimiento de Tipos de Muestra"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11610
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11610
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdcuestionario 
      Caption         =   "Cuestionario"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3600
      TabIndex        =   13
      Top             =   7680
      Width           =   1695
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   6600
      TabIndex        =   12
      Top             =   7680
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Muestra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4560
      Index           =   0
      Left            =   480
      TabIndex        =   1
      Top             =   3000
      Width           =   9735
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4065
         Index           =   0
         Left            =   135
         TabIndex        =   2
         Top             =   360
         Width           =   9360
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   16510
         _ExtentY        =   7170
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipo de Muestra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Index           =   1
      Left            =   480
      TabIndex        =   4
      Top             =   480
      Width           =   9780
      Begin TabDlg.SSTab tabTab1 
         Height          =   1815
         Index           =   0
         Left            =   240
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   480
         Width           =   9255
         _ExtentX        =   16325
         _ExtentY        =   3201
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0206.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(0)"
         Tab(0).Control(1)=   "txtText1(1)"
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0206.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR49DESTIPMUESTR"
            Height          =   330
            Index           =   0
            Left            =   -74760
            TabIndex        =   11
            Tag             =   "Descripci�n del Tipo de Muestra"
            Top             =   1200
            Width           =   8175
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H0000FFFF&
            DataField       =   "PR49CODTIPMUESTR"
            Height          =   330
            Index           =   1
            Left            =   -74760
            TabIndex        =   9
            Tag             =   "C�digo de Tipo de Muestra"
            Top             =   480
            Width           =   1215
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1545
            Index           =   2
            Left            =   120
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   120
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   2725
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n del Tipo de Muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   -74760
            TabIndex        =   10
            Top             =   960
            Width           =   3015
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo de Muestra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74760
            TabIndex        =   8
            Top             =   240
            Width           =   1455
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmtiposdemuestra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdaceptar_Click()
  guCodigo = grdDBGrid1(0).Columns(3).Text
  gstrdescripcion = grdDBGrid1(0).Columns(4).Text
  Unload Me
End Sub

Private Sub cmdcuestionario_Click()
      
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      Call objWinInfo.DataSave
      Call objsecurity.LaunchProcess("PR0225")

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  With objMasterInfo
    '.strName = "Tipos de Muestra"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
      
    .strTable = "PR4900"
    .intAllowance = cwAllowAll
    
    Call .FormAddOrderField("PR49CODTIPMUESTR", cwAscending)
    Call .objPrinter.Add("PR2061", "Listado de Tipos de Muestra")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Tipo de Muestra")
    Call .FormAddFilterWhere(strKey, "PR49CODTIPMUESTR", "Tipo de Muestra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR49DESTIPMUESTR", "Descripci�n del Tipo de Muestra", cwString)
    
    Call .FormAddFilterOrder(strKey, "PR49CODTIPMUESTR", "Tipo de Muestra")
    Call .FormAddFilterOrder(strKey, "PR49DESTIPMUESTR", "Descripci�n del Tipo de Muestra")
   
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    
    .strTable = "PR2400"
    .intAllowance = cwAllowAll
    .intCursorSize = 0
    
    Call .FormAddOrderField("PR24CODMUESTRA", cwAscending)
    Call .FormAddRelation("PR49CODTIPMUESTR", txtText1(1))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Muestra")
    Call .FormAddFilterWhere(strKey, "PR24CODMUESTRA", "C�digo de Muestra", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR24DESCORTA", "Descripci�n corta de la Muestra", cwString)
    Call .FormAddFilterWhere(strKey, "PR24DESCOMPLETA", "Descripci�n larga de la Muestra", cwString)
    Call .FormAddFilterWhere(strKey, "PR24CAPACIDADTUBO", "Capacidad en CC de 1 tubo necesario", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR24MODO", "Modo de obtenci�n de la Muestra", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "PR24CODMUESTRA", "C�digo de Muestra")
    Call .FormAddFilterOrder(strKey, "PR24DESCORTA", "Descripci�n corta de la Muestra")
    Call .FormAddFilterOrder(strKey, "PR24DESCOMPLETA", "Descripci�n larga de la Muestra")
    Call .FormAddFilterOrder(strKey, "PR24CAPACIDADTUBO", "Capacidad en CC de 1 tubo necesario")
    Call .FormAddFilterOrder(strKey, "PR24MODO", "Modo de obtenci�n de la Muestra")
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "Muestra", "PR24CODMUESTRA", cwNumeric, 5)
    Call .GridAddColumn(objMultiInfo, "Descripci�n corta", "PR24DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Descripci�n larga", "PR24DESCOMPLETA", cwString, 250)
    Call .GridAddColumn(objMultiInfo, "Capacidad de 1 tubo", "PR24CAPACIDADTUBO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Modo de obtenci�n", "PR24MODO", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "Tipo de Muestra", "PR49CODTIPMUESTR", cwNumeric, 3)
    
    Call .FormCreateInfo(objMasterInfo)
    'Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1


    Call .FormChangeColor(objMultiInfo)
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
  
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnReadOnly = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  grdDBGrid1(0).Columns(8).Visible = False

  'Call objApp.SplashOff

End Sub
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Tipos de Muestra" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_Click(Index As Integer)
    'Activamos o desactivamos el bot�n del cuestionario de la muestra
    If (grdDBGrid1(0).SelBookmarks.Count <> 0) Then
        cmdCuestionario.Enabled = True
    Else
        cmdCuestionario.Enabled = False
    End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  Dim rsta As rdoResultset
  Dim sqlstr As String
  
  If (btnButton.Index = 8) Then
    If objWinInfo.objWinActiveForm.strName = "fraFrame1(1)" Then
        If (Tiene_Muestra = True) Then
            Exit Sub
        End If
    Else
        If (Tiene_Cuestionario = True) Then
            Exit Sub
        End If
        If (Tiene_Actuacion = True) Then
            Exit Sub
        End If
    End If
  End If

  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

    If btnButton.Index = 2 Then
        On Error GoTo Err_Ejecutar
        ' generaci�n autom�tica del c�digo
        If objWinInfo.objWinActiveForm.strName = "fraFrame1(1)" Then
            sqlstr = "SELECT PR49CODTIPMUESTR_SEQUENCE.nextval FROM dual"
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            txtText1(1) = rsta.rdoColumns(0).Value
            txtText1(1).Locked = True
            rsta.Close
            Set rsta = Nothing
        Else
            sqlstr = "SELECT PR24CODMUESTRA_SEQUENCE.nextval FROM dual"
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            grdDBGrid1(0).Columns(3).Text = rsta.rdoColumns(0).Value
            grdDBGrid1(0).Columns(3).Locked = True
            grdDBGrid1(0).Col = 4
            rsta.Close
            Set rsta = Nothing
        End If
    End If
 Exit Sub
 
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Dim sqlstr As String
    Dim rsta As rdoResultset
  
  If (intIndex = 60) Then
    If objWinInfo.objWinActiveForm.strName = "fraFrame1(1)" Then
        If (Tiene_Muestra = True) Then
            Exit Sub
        End If
    Else
        If (Tiene_Cuestionario = True) Then
            Exit Sub
        End If
        If (Tiene_Actuacion = True) Then
            Exit Sub
        End If
    End If
  End If
  
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    
    If intIndex = 10 Then
        On Error GoTo Err_Ejecutar
        ' generaci�n autom�tica del c�digo
        If objWinInfo.objWinActiveForm.strName = "fraFrame1(1)" Then
            sqlstr = "SELECT PR49CODTIPMUESTR_SEQUENCE.nextval FROM dual"
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            txtText1(1) = rsta.rdoColumns(0).Value
            txtText1(1).Locked = True
            rsta.Close
            Set rsta = Nothing
        Else
            sqlstr = "SELECT PR24CODMUESTRA_SEQUENCE.nextval FROM dual"
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            grdDBGrid1(0).Columns(3).Text = rsta.rdoColumns(0).Value
            grdDBGrid1(0).Columns(3).Locked = True
            grdDBGrid1(0).Col = 4
            rsta.Close
            Set rsta = Nothing
        End If
    End If
 Exit Sub
 
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    
    
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    If grdDBGrid1(0).Columns(7).Text <> "1" And _
    grdDBGrid1(0).Columns(7).Text <> "2" And _
    grdDBGrid1(0).Columns(7).Text <> "3" And _
    grdDBGrid1(0).Columns(7).ColChanged = True _
    Then
        If grdDBGrid1(0).Columns(7).Text <> "" Then
        Call MsgBox("   Valores v�lidos:" & Chr$(13) & "      1 - Extracci�n" _
        & Chr$(13) & "      2 - Aportada" & Chr$(13) & "      3 - En vivo" _
        , vbExclamation, "Valor de Modo no v�lido")
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(7), "1")
        Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(7), "")
        grdDBGrid1(0).Columns(7).Nullable = ssColumnNullableNull
        Else
            Exit Sub
        End If
    End If
    
    Call objWinInfo.CtrlDataChange

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Public Function Tiene_Cuestionario() As Boolean

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "La muestra NO puede se borrada por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
  strSelect = "SELECT COUNT(*) FROM PR5100" & _
              " WHERE PR24CODMUESTRA=" & grdDBGrid1(0).Columns(3).Text
  
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La muestra TIENE CUESTIONARIO." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
      
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Cuestionario = True
  Else
    Tiene_Cuestionario = False
  End If
  

End Function

Public Function Tiene_Actuacion() As Boolean

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "La muestra NO puede se borrada por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
  strSelect = "SELECT COUNT(*) FROM PR2500" & _
                " WHERE PR24CODMUESTRA=" & grdDBGrid1(0).Columns(3).Text
  
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La muestra TIENE ACTUACION ASOCIADA." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
      
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Actuacion = True
  Else
    Tiene_Actuacion = False
  End If
  

End Function

Public Function Tiene_Muestra() As Boolean

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "El tipo de muestra NO puede se borrada por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
  strSelect = "SELECT COUNT(*) FROM PR2400" & _
                " WHERE PR49CODTIPMUESTR=" & txtText1(1).Text
  
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� El tipo de muestra TIENE MUESTRA ASOCIADA." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
      
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Muestra = True
  Else
    Tiene_Muestra = False
  End If
  

End Function



