VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmpedircond 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Condiciones de la Petici�n"
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   Icon            =   "PR0158.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   4800
      TabIndex        =   0
      Top             =   7440
      Width           =   2055
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Condiciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6480
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   11415
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   5865
         Index           =   0
         Left            =   120
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   480
         Width           =   11160
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   19685
         _ExtentY        =   10345
         _StockProps     =   79
         Caption         =   "CONDICIONES"
         BackColor       =   -2147483633
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmpedircond"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim stractnosel As String
Private Sub deseleccionar_actuacion(ByVal lngcodact As Long)
    Dim mintI As Integer
    Dim strrespuesta As String
    Dim cont As Integer
    
    frmSeleccionarAct.grdDBGrid1(0).MoveFirst
    cont = 0
    Do While cont < frmSeleccionarAct.grdDBGrid1(0).Rows
        If (lngcodact = frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value) Then
            frmSeleccionarAct.grdDBGrid1(0).Columns(0).Value = False
            frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = False
            glngactpedida = frmSeleccionarAct.grdDBGrid1(0).Columns(6).Value
            frmSeleccionarAct.grdDBGrid1(0).Columns(6).Value = ""
            frmSeleccionarAct.grdDBGrid1(0).Columns(7).Value = ""
            frmSeleccionarAct.grdDBGrid1(0).Columns(8).Value = ""
        End If
        If cont = frmSeleccionarAct.grdDBGrid1(0).Rows - 1 Then
            Exit Do
        Else
            frmSeleccionarAct.grdDBGrid1(0).MoveNext
            cont = cont + 1
        End If
    Loop
   frmSeleccionarAct.grdDBGrid1(0).MoveFirst
End Sub
Private Sub seleccionar_condiciones()
Dim cont As Integer

    Do While cont < frmSeleccionarAct.grdDBGrid1(0).Rows
        If frmSeleccionarAct.grdDBGrid1(0).Columns(0).Value = True Then
            frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = True
        End If
        If cont = frmSeleccionarAct.grdDBGrid1(0).Rows - 1 Then
            Exit Do
        Else
            frmSeleccionarAct.grdDBGrid1(0).MoveNext
            cont = cont + 1
        End If
    Loop
   frmSeleccionarAct.grdDBGrid1(0).MoveFirst
End Sub

Private Sub Command1_Click()
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim intnoclicada As Integer
  Dim strrespuesta As String
  Dim intencontrado As Integer
  Dim i As Integer
  Dim j As Integer
  
  Call seleccionar_condiciones
  ' se guardan los posibles cambios en los check si
  ' el usuario se ha olvidado de pulsar Guardar
  Call objWinInfo.WinProcess(6, 4, 0)
  ' se refresca el multil�nea
  objWinInfo.objWinActiveForm.blnChanged = False
  Call objWinInfo.WinProcess(6, 26, 0)
  
   numelem = 1
   ReDim tabla(1)
   'inicializaci�n del array que guarda c�d.actuaci�n
   For i = 1 To numelem
    tabla(i) = 0
   Next i
   numelem = 1
   ReDim tabla2(1)
   'inicializaci�n del array que guarda n�m.act.pedida
   For j = 1 To numelem
    tabla2(j) = 0
   Next j
 
  intnoclicada = 0
  intencontrado = 0
  'se mira si se han clicado todas las codiciones
  sqlstr = "SELECT * FROM  PR3801J " _
         & "WHERE pr03numactpedi in (select pr03numactpedi from pr0300 where pr09numpeticion =" & frmpedir.txtText1(0) & ")"
  On Error GoTo Err_Ejecutar
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  While Not rsta.EOF
    ' si se encuentra una condici�n sin clicar
    If (rsta.rdoColumns("PR38INDRESPUESTA").Value = 0) Then
        intnoclicada = 1
        For i = 1 To numelem
            ReDim Preserve tabla(numelem)
            ReDim Preserve tabla2(numelem)
            If tabla(i) = rsta.rdoColumns("PR01CODACTUACION").Value Then
               'la actuaci�n ya est� en la tabla
               intencontrado = 1
            End If
        Next i
        If (intencontrado = 1) Then
            intencontrado = 0
        Else
            tabla(numelem) = rsta.rdoColumns("PR01CODACTUACION").Value 'c�d.actuaci�n
            tabla2(numelem) = rsta.rdoColumns("PR03NUMACTPEDI").Value 'num.petici�n
            deseleccionar_actuacion (rsta.rdoColumns("PR01CODACTUACION").Value)
            numelem = numelem + 1
        End If
    End If
    rsta.MoveNext
  Wend
  
  If (intnoclicada = 1) Then
    strrespuesta = MsgBox("No est�n contestadas todas las condiciones." _
                & "Aquellas no contestadas no ser�n seleccionadas. " & Chr(13) _
                 & "�Desea modificar sus respuestas? ", 36, "Condiciones")
    If strrespuesta = vbNo Then
            ' se muestra la pantalla con las actuaciones que no ser�n seleccionadas
            ' porque no se ha clicado alguna de sus condiciones
            'Load frmactnosel
            'Call frmactnosel.Show(vbModal)
            Call objsecurity.LaunchProcess("PR0159")
            objWinInfo.objWinActiveForm.blnChanged = False
            Unload Me
            'Unload frmactnosel
            'Set frmactnosel = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    Exit Sub
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
  End If
   
  rsta.Close
  Set rsta = Nothing
  glngSelCond = 0
  'frmSeleccionarAct.cmdpeticion.Enabled = True
  
  Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMultiInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .intAllowance = cwAllowModify
    
    
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR3801J" ' vista de PR3800xPR2300
    .strWhere = "pr03numactpedi in (select pr03numactpedi from pr0300 where pr09numpeticion =" & frmpedir.txtText1(0).Text & ")"
    '.strWhere = "pr03numactpedi in (select pr03numactpedi from pr0300 where pr09numpeticion =" _
                    & frmpedir.txttext(0).Text & ")"
    '*****************************
    .intCursorSize = 0
    strKey = .strDataBase & .strTable
    'Call .FormAddOrderField(strKey, "PR03NUMACTPEDI", cwAscending)
    'Call .FormAddOrderField(strKey, "PR01CODACTUACION", cwAscending)
    'Call .FormAddOrderField(strKey, "PR23CODCOND", cwAscending)
     
    Call .FormCreateFilterWhere(strKey, "Condiciones de la Petici�n")
    Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI", "Actuaci�n Pedida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR23CODCOND", "C�digo Condici�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR23DESCOND", "Descripci�n Condici�n", cwString)
        
    Call .FormAddFilterOrder(strKey, "PR03NUMACTPEDI", "Actuaci�n Pedida")
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR23CODCOND", "C�digo Condici�n")
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�digo Condici�n", "PR23CODCOND", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Condici�n", "PR23DESCOND", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "�Se cumple la condici�n?", "PR38INDRESPUESTA", cwBoolean, 1)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n Pedida", "PR03NUMACTPEDI", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�digo Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
   
    
    Call .FormCreateInfo(objMultiInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    
     'c�digo actuaci�n pedida
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnMandatory = True
    ' c�digo actuaci�n
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnMandatory = True
    ' c�digo condici�n
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).intKeyNo = 1
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnMandatory = True
    
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnReadOnly = True
    
    
    Call .FormChangeColor(objMultiInfo)
  
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  grdDBGrid1(0).Columns(3).Width = 1800
  grdDBGrid1(0).Columns(4).Width = 6200
  grdDBGrid1(0).Columns(5).Width = 2000
  'Call objApp.SplashOff
  
  'JRC 28/4/98
  grdDBGrid1(0).Columns(3).Visible = False
  grdDBGrid1(0).Columns(7).Visible = False

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim intnoclicada As Integer
  Dim strrespuesta As String
  Dim intencontrado As Integer
  Dim i As Integer
  Dim j As Integer
  
  If btnButton.Index <> 30 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Exit Sub
  End If
  
  If btnButton.Index = 30 Then
  '*************************************************************
  Call seleccionar_condiciones
  ' se guardan los posibles cambios en los check si
  ' el usuario se ha olvidado de pulsar Guardar
  Call objWinInfo.WinProcess(6, 4, 0)
  ' se refresca el multil�nea
  objWinInfo.objWinActiveForm.blnChanged = False
  Call objWinInfo.WinProcess(6, 26, 0)
  
   numelem = 1
   ReDim tabla(1)
   'inicializaci�n del array que guarda c�d.actuaci�n
   For i = 1 To numelem
    tabla(i) = 0
   Next i
   numelem = 1
   ReDim tabla2(1)
   'inicializaci�n del array que guarda n�m.act.pedida
   For j = 1 To numelem
    tabla2(j) = 0
   Next j
 
  intnoclicada = 0
  intencontrado = 0
  'se mira si se han clicado todas las codiciones
  sqlstr = "SELECT * FROM  PR3801J " _
         & "WHERE pr03numactpedi in (select pr03numactpedi from pr0300 where pr09numpeticion =" & frmpedir.txtText1(0) & ")"
  On Error GoTo Err_Ejecutar
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  While Not rsta.EOF
    ' si se encuentra una condici�n sin clicar
    If (rsta.rdoColumns("PR38INDRESPUESTA").Value = 0) Then
        intnoclicada = 1
        For i = 1 To numelem
            ReDim Preserve tabla(numelem)
            ReDim Preserve tabla2(numelem)
            If tabla(i) = rsta.rdoColumns("PR01CODACTUACION").Value Then
               'la actuaci�n ya est� en la tabla
               intencontrado = 1
            End If
        Next i
        If (intencontrado = 1) Then
            intencontrado = 0
        Else
            tabla(numelem) = rsta.rdoColumns("PR01CODACTUACION").Value 'c�d.actuaci�n
            tabla2(numelem) = rsta.rdoColumns("PR03NUMACTPEDI").Value 'num.petici�n
            deseleccionar_actuacion (rsta.rdoColumns("PR01CODACTUACION").Value)
            numelem = numelem + 1
        End If
    End If
    rsta.MoveNext
  Wend
  
  If (intnoclicada = 1) Then
    strrespuesta = MsgBox("No est�n contestadas todas las condiciones." _
                & "Aquellas no contestadas no ser�n seleccionadas. " & Chr(13) _
                 & "�Desea modificar sus respuestas? ", 36, "Condiciones")
    If strrespuesta = vbNo Then
            ' se muestra la pantalla con las actuaciones que no ser�n seleccionadas
            ' porque no se ha clicado alguna de sus condiciones
            'Load frmactnosel
            'Call frmactnosel.Show(vbModal)
            Call objsecurity.LaunchProcess("PR0159")
            objWinInfo.objWinActiveForm.blnChanged = False
            Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
            'Unload frmactnosel
            'Set frmactnosel = Nothing
    End If
    rsta.Close
    Set rsta = Nothing
    Exit Sub
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
  End If
  
  rsta.Close
  Set rsta = Nothing
  glngSelCond = 0
  frmSeleccionarAct.cmdpeticion.Enabled = True
  Exit Sub
  '************************************************
End If

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


