VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{D2FFAA40-074A-11D1-BAA2-444553540000}#3.0#0"; "VsVIEW3.ocx"
Begin VB.Form frm_Recordatorio 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Cartas de recordatorio"
   ClientHeight    =   5730
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   3690
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdTodas 
      Caption         =   "Todas las facturas"
      Height          =   600
      Left            =   1890
      TabIndex        =   29
      Top             =   4995
      Width           =   1500
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   9
      Left            =   1800
      TabIndex        =   28
      Text            =   "0"
      Top             =   4455
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   8
      Left            =   1800
      TabIndex        =   27
      Text            =   "0"
      Top             =   4095
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   7
      Left            =   1800
      TabIndex        =   26
      Text            =   "0"
      Top             =   3735
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   6
      Left            =   1800
      TabIndex        =   25
      Text            =   "0"
      Top             =   3375
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   5
      Left            =   1800
      TabIndex        =   24
      Text            =   "0"
      Top             =   3015
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   4
      Left            =   1800
      TabIndex        =   23
      Text            =   "0"
      Top             =   2655
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   3
      Left            =   1800
      TabIndex        =   22
      Text            =   "0"
      Top             =   2295
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   2
      Left            =   1800
      TabIndex        =   21
      Text            =   "0"
      Top             =   1935
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   1
      Left            =   1800
      TabIndex        =   20
      Text            =   "0"
      Top             =   1575
      Width           =   1590
   End
   Begin VB.TextBox txtRespEcon 
      Alignment       =   1  'Right Justify
      Height          =   285
      Index           =   0
      Left            =   1800
      TabIndex        =   19
      Text            =   "0"
      Top             =   1215
      Width           =   1590
   End
   Begin VB.TextBox txtDias 
      Alignment       =   1  'Right Justify
      Height          =   285
      Left            =   1800
      TabIndex        =   18
      Text            =   "1"
      Top             =   855
      Width           =   420
   End
   Begin VB.PictureBox Picture1 
      Height          =   90
      Left            =   0
      Picture         =   "FA00135.frx":0000
      ScaleHeight     =   30
      ScaleWidth      =   30
      TabIndex        =   2
      Top             =   45
      Visible         =   0   'False
      Width           =   90
   End
   Begin VB.CommandButton cmdRevisar 
      Caption         =   "Facturas a revisar"
      Height          =   600
      Left            =   180
      TabIndex        =   1
      Top             =   4995
      Width           =   1500
   End
   Begin vsViewLib.vsPrinter vsPrinter 
      Height          =   60
      Left            =   180
      TabIndex        =   0
      Top             =   45
      Width           =   150
      _Version        =   196608
      _ExtentX        =   265
      _ExtentY        =   106
      _StockProps     =   229
      Appearance      =   1
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ConvInfo        =   1418783674
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaInicio 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1800
      TabIndex        =   3
      Tag             =   "Fecha Inicio"
      Top             =   135
      Width           =   1635
      _Version        =   65537
      _ExtentX        =   2884
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo SDCFechaCarta 
      DataField       =   "FA02FECINICIO"
      Height          =   285
      Left            =   1800
      TabIndex        =   5
      Tag             =   "Fecha Inicio"
      Top             =   495
      Width           =   1635
      _Version        =   65537
      _ExtentX        =   2884
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 10:"
      Height          =   255
      Index           =   10
      Left            =   180
      TabIndex        =   17
      Top             =   4500
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 9:"
      Height          =   255
      Index           =   9
      Left            =   180
      TabIndex        =   16
      Top             =   4140
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 8:"
      Height          =   255
      Index           =   8
      Left            =   180
      TabIndex        =   15
      Top             =   3780
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 7:"
      Height          =   255
      Index           =   7
      Left            =   180
      TabIndex        =   14
      Top             =   3420
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 6:"
      Height          =   255
      Index           =   6
      Left            =   180
      TabIndex        =   13
      Top             =   3060
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 5:"
      Height          =   255
      Index           =   5
      Left            =   180
      TabIndex        =   12
      Top             =   2700
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 4:"
      Height          =   255
      Index           =   4
      Left            =   180
      TabIndex        =   11
      Top             =   2340
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 3:"
      Height          =   255
      Index           =   3
      Left            =   180
      TabIndex        =   10
      Top             =   1980
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 2:"
      Height          =   255
      Index           =   2
      Left            =   180
      TabIndex        =   9
      Top             =   1620
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "Resp. Econ�mico 1:"
      Height          =   255
      Index           =   1
      Left            =   180
      TabIndex        =   8
      Top             =   1260
      Width           =   1650
   End
   Begin VB.Label Label1 
      Caption         =   "D�as de Proceso:"
      Height          =   255
      Index           =   0
      Left            =   180
      TabIndex        =   7
      Top             =   900
      Width           =   1650
   End
   Begin VB.Label lblFecCarta 
      Caption         =   "Fecha de Carta:"
      Height          =   255
      Left            =   225
      TabIndex        =   6
      Top             =   540
      Width           =   1650
   End
   Begin VB.Label lblFecInicio 
      Caption         =   "Fecha de Proceso:"
      Height          =   255
      Left            =   225
      TabIndex        =   4
      Top             =   180
      Width           =   1650
   End
End
Attribute VB_Name = "frm_Recordatorio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ConvX = 54.0809
Const ConvY = 54.6101086

Dim qry(1 To 10) As New rdoQuery

Function ComprobarDatos() As Boolean
Dim cont As Integer

  If Not IsNumeric(txtDias.Text) Then
    MsgBox "Los d�as de proceso deben ser un dato num�rico", vbInformation + vbOKOnly, "Aviso"
    If txtDias.Enabled = True Then
      txtDias.SetFocus
    End If
    ComprobarDatos = False
    Exit Function
  End If
  
  For cont = 0 To 9
    If Not IsNumeric(txtRespEcon(cont)) Then
      MsgBox "El responsable econ�mico " & cont + 1 & "debe ser un dato num�rico", vbInformation + vbOKOnly, "aviso"
      If txtRespEcon(cont).Enabled = True Then
        txtRespEcon(cont).SetFocus
      End If
      ComprobarDatos = False
      Exit Function
    End If
  Next
  
  ComprobarDatos = True
  
End Function

Sub ImprimirCarta(CodPersona As Long, TipoPersona As Integer)
  
  vsPrinter.Preview = False
  vsPrinter.Action = 3
  Call Me.EncabezadoPagina(CodPersona)
  Call Me.ImprimirCuerpo(CodPersona, Me.SDCFechaInicio, Me.SDCFechaCarta, txtDias)
  'Call Me.ImprimirFacturas(CodPersona, Me.SDCFechaInicio, Me.SDCFechaCarta, txtDias)
  vsPrinter.Action = 6
  
End Sub

Sub ImprimirCuerpo(CodPersona As Long, FechaInicio As Date, FechaCarta As Date, Dias As Integer)
Dim Texto As String
Dim MiSql As String
Dim MiRs As String
Dim rsBuscar As rdoResultset
Dim Cantidad As Double
Dim Total As Double
Dim Pagado As Double
Dim Abonos As Double
Dim FechaLimite As Date
Dim Dia As Integer

  Dia = 0 - Dias
  FechaLimite = DateAdd("d", Dia, FechaInicio)
  qry(4).rdoParameters(0) = CodPersona
  qry(4).rdoParameters(1) = FechaLimite
  Set rsBuscar = qry(4).OpenResultset
  If Not rsBuscar.EOF Then
    If rsBuscar(0) = "" Then
      Total = 0
    Else
      Total = rsBuscar(0)
    End If
  End If
  qry(5).rdoParameters(0) = CodPersona
  qry(5).rdoParameters(1) = FechaLimite
  Set rsBuscar = qry(5).OpenResultset
  If Not rsBuscar.EOF Then
    If rsBuscar(0) = "" Then
      Total = 0
    Else
      Total = rsBuscar(0)
    End If
  End If
  With vsPrinter
    .FontName = "Courier New"
    .FontSize = 11
    .TextAlign = taJustBaseline
    .MarginRight = 15 * ConvX
    .MarginLeft = 50 * ConvX
    .Paragraph = ""
    Texto = "       Nos permitimos recordarle que, seg�n nuestras anotaciones ,a fecha " & Format(FechaInicio, "dd/mm/yyyy") & ","
    Texto = Texto & " tenemos pendiente de cobro el importe de PESETAS, " & Cantidad
    Texto = Texto & " correspondientes a las facturas que detallamos a continuacion, el cual nos puede remitir"
    Texto = Texto & " por el medio que crea m�s conveniente."
    .Paragraph = Texto
    .Paragraph = ""
    Texto = "       Si ya lo ha enviado agradeceremos nos lo comunique, remiti�ndonos una copia"
    Texto = Texto & " del comprobante bancario."
    .Paragraph = Texto
    .Paragraph = ""
    Texto = "        Tan pronto como el mismo obre en nuestro poder, cursaremos el oportuno"
    Texto = Texto & " escrito confirmando la recepci�n del mismo"
    .Paragraph = Texto
    .Paragraph = ""
    Texto = "       Pendientes de sus noticias y ofreci�ndonos a su disposici�n, le saludamos atentamente."
    .Paragraph = Texto
    .Paragraph = ""
  End With

End Sub

Sub IniciarQRY()
Dim MiSql As String
Dim x As Integer

  'Seleccionamos los datos personales de una persona.
  MiSql = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
              " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
              " CI07CODPOSTAL, CI10DESLOCALID,CI26DESPROVI, CI34DESTRATAMI, CI22NUMHISTORIA,CI22DNI" & _
              " from CI2200, CI1000,CI3400,CI2600 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA  " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI)" & _
              " AND (CI2200.CI26CODPROVI = CI2600.CI26CODPROVI)"
  qry(1).SQL = MiSql

  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSql = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qry(2).SQL = MiSql
  MiSql = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID,CI26DESPROVI" & _
              " From CI2300, CI1000, CI0900,CI2600 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODERSONA " & _
              " And CI2300.CI23NUMDIRPRINC = CI1000.CI10NUMDIRECCI) " & _
              " AND (CI2200.CI26CODPROVI = CI2600.CI26CODPROVI)"
  qry(3).SQL = MiSql
  
  'Seleccionamos el total de las facturas que no est�n totalmente compensadas.
  MiSql = "Select Sum(FA04CANTFACT) From FA0400 Where CI21CODPERSONA = ?" & _
              "and FA04FECFACTURA <= TO_DATE(?,'DD/MM/YYYY') And FA04INCOMPENSA <> 2"
  qry(4).SQL = MiSql
  
  'Seleccionamos el impote compensado de las facturas que no est�n totalmente compensadas.
  MiSql = "Select Sum(FA18IMPCOMP) From FA1800 Where FA04CODFACT IN (" & _
              " SELECT FA04CODFACT FROM FROM FA040O Where CI21CODPERSONA = ?" & _
              "and FA04FECFACTURA <= TO_DATE(?,'DD/MM/YYYY') And FA04INCOMPENSA <> 2"
  qry(5).SQL = MiSql
  
  
  
  For x = 1 To 5
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next

End Sub



'Mediante esta funci�n comprobaremos si se ha seleccionado alg�n responsable econ�mico.
'Si se ha introducido un responsable econ�mico, sacaremos las cartas de recordatorio de
'los responsables econ�micos seleccionados.
Function ResponsableEconomico() As String
Dim Cadena As String
Dim cont As Integer

  Cadena = ""
  For cont = 0 To 9
    'Comprobamos si se ha introducido un responsable econ�mico.
    If Me.txtRespEcon(cont).Text <> 0 Then
      Cadena = Cadena & txtRespEcon(cont).Text & ","
    End If
  Next
  
  ResponsableEconomico = Trim(Cadena)
  
End Function

Sub EncabezadoPagina(Persona As Long)
Dim MiSql As String
Dim rsBuscar As rdoResultset
Dim Nombre As String
Dim Direccion As String
Dim Poblacion As String
Dim Provincia As String

    qry(1).rdoParameters(0) = Persona
    Set rsBuscar = qry(1).OpenResultset
    If Not rsBuscar.EOF Then
      Nombre = rsBuscar("CI34DESTRATAMI") & " " & rsBuscar("CI22NOMBRE") & " " & rsBuscar("CI22PRIAPEL") & " " & rsBuscar("CI22SEGAPEL")
      Direccion = rsBuscar("CI10CALLE") & " " & rsBuscar("CI10PORTAL") & " " & rsBuscar("CI10RESTODIREC")
      Poblacion = rsBuscar("CI07CODPOSTAL") & " " & rsBuscar("CI10DESLOCALID")
      Provincia = rsBuscar("CI26DESPROVI")
    Else
      Nombre = ""
      Direccion = ""
      Poblacion = ""
      Provincia = ""
    End If
    vsPrinter.Preview = False
    With vsPrinter
        .TextColor = QBColor(0)
        '-------------------------------------------------------
        ' REALIZAMOS LA IMPRESI�N DE LA CABECERA
        '-------------------------------------------------------
        'Imprimimos el escudo de la cl�nica.
        .X1 = 35 * ConvX
        .Y1 = 5 * ConvY
        .X2 = 59 * ConvX
        .Y2 = 35 * ConvY
        .Picture = Me.Picture1.Picture
                
        'Imprimimos el texto de debajo del escudo CLINICA...
        .FontName = "Arial"
        .CurrentX = 0 * ConvX
        .CurrentY = 35 * ConvY
        .MarginLeft = 0
        'Le asignamos un gran margen derecho y le decimos que lo centre
        .MarginRight = 120 * ConvX
        .FontSize = 14
        .TextAlign = taCenterBaseline
        .FontName = "Times New Roman"
        .Paragraph = "CLINICA UNIVERSITARIA"
        .SpaceAfter = 2 * ConvX
        .FontSize = 8
        .FontBold = False
        .CurrentY = 40 * ConvY
        .Paragraph = "FACULTAD DE MEDICINA"
        .CurrentY = 43 * ConvY
        .Paragraph = "UNIVERSIDAD DE NAVARRA"
        .DrawLine 39 * ConvX, 45 * ConvY, 49 * ConvX, 45 * ConvY
        .CurrentY = 49 * ConvY
        .FontName = "Arial"
        .FontBold = True
        .Paragraph = "ADMINISTRACION"
        .FontBold = False
        .TextAlign = taLeftBaseline
        .MarginRight = 0
        
        'Imprimimos la direcci�n y los tel�fonos (Parte derecha del encabezado)
        .CurrentX = 140 * ConvX
        .FontSize = 8
        .CurrentY = 16 * ConvY
        .Paragraph = "Avda. P�o XII, 36"
        .CurrentX = 140 * ConvX
        .CurrentY = 19 * ConvY
        .Paragraph = "Apartado, 4209"
        .CurrentX = 140 * ConvX
        .CurrentY = 25 * ConvY
        .Paragraph = "Tel�fonos:"
        .CurrentX = 140 * ConvX
        .CurrentY = 28 * ConvY
        .Paragraph = "Centralita 948.25.54.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 31 * ConvY
        .Paragraph = "Administraci�n 948.29.63.94"
        .CurrentX = 140 * ConvX
        .CurrentY = 37 * ConvY
        .Paragraph = "Fax: 948.29.65.00"
        .CurrentX = 140 * ConvX
        .CurrentY = 40 * ConvY
        .Paragraph = "C.I.F.: Q.3168001 J"
        .CurrentX = 140 * ConvX
        .CurrentY = 43 * ConvY
        .Paragraph = "31080 PAMPLONA (ESPA�A)"
        
        .FontName = "Courier New"
        .FontSize = 11
        .CurrentX = 100 * ConvX
        .CurrentY = 59 * ConvY
        .Paragraph = Nombre
        .CurrentX = 100 * ConvX
        .CurrentY = 63 * ConvY
        .Paragraph = Direccion
        .CurrentX = 100 * ConvX
        .CurrentY = 67 * ConvY
        .Paragraph = Poblacion
        .CurrentX = 100 * ConvX
        .CurrentY = 71 * ConvY
        .Paragraph = Provincia
        
        .CurrentX = 50 * ConvX
        .CurrentY = 90 * ConvY
        .Paragraph = "Muy se�or nuestro: "
      End With
End Sub

Private Sub cmdRevisar_Click()
Dim MiSelect As String
Dim MiWhere As String
Dim MiSql As String
Dim Cadena As String
Dim IntPosComa As Integer
Dim MiRs As rdoResultset
Dim FechaProceso
Dim Dias As Integer
  
  Dias = 0 - CInt(txtDias)
  FechaProceso = DateAdd("D", Dias, Me.SDCFechaInicio.Date)
  MiSelect = "Select CI2100.CI21CODPERSONA, CI33CODTIPPERSONA From CI2100, FA0400 "
  MiWhere = "Where FA21CODDESTCARE = 2 AND FA04INDCOMPENSA <> 2 AND FA04INDRECORDAR = 1"
  MiWhere = MiWhere & " AND (CI2100.CI21CODPERSONA = FA0400.CI21CODPERSONA)"
  MiWhere = MiWhere & " AND FA04FECFACTURA <= TO_DATE('" & FechaProceso & "','dd/mm/yyyy')"
  Cadena = ResponsableEconomico
  If Cadena <> "" Then
    While Cadena <> ""
      IntPosComa = InStr(1, Cadena, ",")
      If IntPosComa <> 0 Then
        MiWhere = MiWhere & " AND CI2100.CODPERSONA = " & Left(Cadena, IntPosComa - 1)
        Cadena = Right(Cadena, Len(Cadena) - IntPosComa)
      End If
    Wend
  End If
  If Right(MiWhere, 1) = "," Then
      MiWhere = Left(MiWhere, Len(MiWhere) - 1)
  End If
  MiSql = Trim(MiSelect) & " " & Trim(MiWhere)
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      Call ImprimirCarta(MiRs("CI21CODPERSONA"), MiRs("CI22CODTIPPERSONA"))
      MiRs.MoveNext
    Wend
  End If
End Sub

Private Sub cmdTodas_Click()
Dim MiSelect As String
Dim MiWhere As String
Dim MiOrder As String
Dim MiSql As String
Dim Cadena As String
Dim IntPosComa As Integer
Dim MiRs As rdoResultset
Dim FechaProceso
Dim Dias As Integer
  
  Dias = 0 - CInt(txtDias)
  FechaProceso = DateAdd("D", Dias, Me.SDCFechaInicio.Date)
  MiSelect = "Select CI2100.CI21CODPERSONA, CI33CODTIPPERSONA From CI2100, FA0400 "
  MiWhere = "Where FA04INDCOMPENSA <> 2 AND FA04INDRECORDAR = 1"
  MiWhere = MiWhere & " AND (CI2100.CI21CODPERSONA = FA0400.CI21CODPERSONA)"
  MiWhere = MiWhere & " AND FA04FECFACTURA <= TO_DATE('" & FechaProceso & "','dd/mm/yyyy')"
  Cadena = ResponsableEconomico
  If Cadena <> "" Then
    While Cadena <> ""
      IntPosComa = InStr(1, Cadena, ",")
      If IntPosComa <> 0 Then
        MiWhere = MiWhere & " AND CI2100.CODPERSONA = " & Left(Cadena, IntPosComa - 1)
        Cadena = Right(Cadena, Len(Cadena) - IntPosComa)
      End If
    Wend
  End If
  If Right(MiWhere, 1) = "," Then
      MiWhere = Left(MiWhere, Len(MiWhere) - 1)
  End If
  MiOrder = "Order by FA21CODDESTCARE"
  MiSql = Trim(MiSelect) & " " & Trim(MiWhere) & " " & Trim(MiOrder)
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSql, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      Call ImprimirCarta(MiRs("CI21CODPERSONA"), MiRs("CI22CODTIPPERSONA"))
      MiRs.MoveNext
    Wend
  End If

End Sub


Private Sub Form_Load()
Dim MiRsFecha
  
  Set MiRsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRsFecha.EOF Then
    Me.SDCFechaInicio = Format(MiRsFecha(0), "dd/mm/yyyy")
    Me.SDCFechaCarta = Format(MiRsFecha(0), "dd/mm/YYYY")
  End If

  Call Me.IniciarQRY
End Sub


Private Sub txtDias_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub


Private Sub txtDias_LostFocus()
  
  If Not IsNumeric(txtDias.Text) Then
    MsgBox "Los d�as de proceso deben ser un dato num�rico", vbInformation + vbOKOnly, "Aviso"
    If txtDias.Enabled = True Then
      txtDias.SetFocus
    End If
  End If
  
End Sub


Private Sub txtRespEcon_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub


Private Sub txtRespEcon_LostFocus(Index As Integer)
  If Not IsNumeric(txtRespEcon(Index)) Then
    MsgBox "El responsable econ�mico " & Index + 1 & "debe ser un dato num�rico", vbInformation + vbOKOnly, "aviso"
    If txtRespEcon(Index).Enabled = True Then
      txtRespEcon(Index).SetFocus
    End If
  End If
End Sub


