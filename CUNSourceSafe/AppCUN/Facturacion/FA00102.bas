Attribute VB_Name = "modFacturacion"
'*************************************************************
'*
'*  Modulo que contiene las funciones necesarias
'*  para la facturaci�n de RNF's
'*
'*************************************************************



Private Type CabFact
  NFactura As String
  Fecha As Date
  NomPaciente As String
  CodEntidad As String
  NomEntidad As String
  Principal As String
  EntioPrivado As String
  Direccion As String
  CPPoblac As String
  Observac As String
  Descrip As String
  NumHistoria As Long
  NumCaso As Long
  TotFactura As Double
  Dto As Integer
  Tratamiento As String
  DNI As String
  codConcierto As Integer
End Type

Private Type LinFact
  NFactura As Long
  NLinea As Integer
  Descripcion As String
  Importe As Double
End Type

Private Type RnfLinea
  NFactura As Long
  NLinea As Integer
  CodCateg As String
  Cantidad As Integer
  Descripcion As String
  Desglose As Boolean
End Type
       
       
Global ArrayCabecera() As CabFact
Global ArrayLineas() As LinFact
Global arrayRNFs() As RnfLinea

' tipo de datos para almacenar los datos generados en la pantalla de
' modificaci�n de atributos y que indican :
'   - nombre del parametro
'   - si es actualizable o no
'   - el nuevo valor
Public Type CamposAtrib
    campo As String
    Actualizar As Boolean
    Valor As String
End Type
' constantes que se corresponden con el orden de los parametros de atributos
' en la sql
Public Const FA15PRECIOREF = 1
Public Const FA15PRECIODIA = 2
Public Const FA15DESCUENTO = 3
Public Const FA15PERIODOGARA = 4
Public Const FA15INDNECESARIO = 5
Public Const FA15INDSUFICIENTE = 6
Public Const FA15INDEXCLUIDO = 7
Public Const FA15INDLINOBLIG = 8
Public Const FA15INDFACTOBLIG = 9
Public Const FA15INDDESCONT = 10
Public Const CI13CODENTIDAD = 11
Public Const FA15INDSUPLEMENTO = 12
Public Const FA15INDDESGLOSE = 13
Public Const FA15OrdImp = 14

' estas dos siempre deben ser las ultimas
Public Const FA15Variaci�nPorcentual = 15
Public Const FA15Redondeo = 16

'Variable que utilizaremos para comprobar si se ha pulsado el boton salir (X)
Global BotonSalir As Boolean
'Variable que utilizaremos para facturar otra propuesta de factura de una historia
Global ContinuarFactura As Boolean


Public Enum faTipoAsignacion
    TieneRrnfSuficiente = 0
    NoTieneRnfNecesario = 1
    RnfOpcional = 2
    NoTieneRNFs = 3
    TieneRNFs = 4
End Enum

' tipo de dato enumerado para definir los diferentes precios
' que puede tener un objeto de tipo nodo.
Public Enum TipoPrecio
    tpSinPrecio = 0
    tpPrecioRef = 1
    tpPrecioDia = 2
    tpPrecioCantidad = 3
    tpPrecioRelacionado = 4
    tpPrecioCantidadDias = 5
End Enum
'Querys que utilizaremos para la anulaci�n de las facturas
Dim qryAnula(1 To 11) As New rdoQuery


'N� de historia que utilizaremos para rehacer una factura. Lo recogeremos a la hora del borrado de
'la tabla FA0300 en base a la factura que estamos borrando.
Dim Historia As Long

Sub BorradoFactura(Factura As String)
Dim Cont As Integer
Dim MiRs As rdoResultset
Dim rsCodigo As rdoResultset
Dim rsHistoria As rdoResultset

  On Error GoTo ErrorEnAnulacion
  objApp.rdoConnect.BeginTrans
    'Comprobaremos que la factura no se ha contabilizado.
    qryAnula(2).rdoParameters(0) = Factura
    Set MiRs = qryAnula(2).OpenResultset
    If Not IsNull(MiRs(0)) Then
      'La factura est� contabilizada habr� que estudiarlo.
      'Asignaremos el 0 al N�mero de factura real para indicarle que ha sido anulada.
      qryAnula(3).rdoParameters(0) = 0
      qryAnula(3).rdoParameters(1) = Factura 'N� de la factura
      qryAnula(3).Execute
      'Le indicaremos que la factura est� compensada para que no apareza en los recordatorios.
      qryAnula(9).rdoParameters(0) = Factura 'n� de la factura
      qryAnula(9).Execute
      'Seleccionaremos todos los c�digos de factura que tienen ese n� de factura para borrar los rnfs y las compensaciones
      qryAnula(10).rdoParameters(0) = Factura 'N� de la factura
      Set rsCodigo = qryAnula(10).OpenResultset
      If Not rsCodigo.EOF Then
        While Not rsCodigo.EOF
          'Seleccionamos el n� de la historia en base al c�digo y la FA0300
          qryAnula(11).rdoParameters(0) = rsCodigo(0)
          Set rsHistoria = qryAnula(11).OpenResultset
          If Not rsHistoria.EOF Then
            Historia = rsHistoria(0)
          End If
          'Borraremos de la FA0300 los rnfs relacionados con esa factura
          qryAnula(5).rdoParameters(0) = rsCodigo(0) 'C�digo de la factura
          qryAnula(5).Execute
          'Borraremos de la FA1800 las posibles compensaciones que se refieran a esa factura
          qryAnula(8).rdoParameters(0) = rsCodigo(0) 'Codigo de la factura
          qryAnula(8).Execute
          rsCodigo.MoveNext
        Wend
      End If
    Else
      'La factura est� sin contabilizar la anulamos sin m�s.
      'Asignaremos el 0 al N�mero de factura real para indicarle que ha sido anulada.
      qryAnula(3).rdoParameters(0) = 0
      qryAnula(3).rdoParameters(1) = Factura 'N� de la factura
      qryAnula(3).Execute
      'Le indicaremos que la factura est� compensada para que no apareza en los recordatorios.
      qryAnula(9).rdoParameters(0) = Factura 'n� de la factura
      qryAnula(9).Execute
      'Seleccionaremos todos los c�digos de factura que tienen ese n� de factura para borrar los rnfs y las compensaciones
      qryAnula(10).rdoParameters(0) = Factura 'N� de la factura
      Set rsCodigo = qryAnula(10).OpenResultset
      If Not rsCodigo.EOF Then
        While Not rsCodigo.EOF
          'Seleccionamos el n� de la historia en base al c�digo y la FA0300
          qryAnula(11).rdoParameters(0) = rsCodigo(0)
          Set rsHistoria = qryAnula(11).OpenResultset
          If Not rsHistoria.EOF Then
            Historia = rsHistoria(0)
          End If
          'Borraremos de la FA0300 los rnfs relacionados con esa factura
          qryAnula(5).rdoParameters(0) = rsCodigo(0) 'C�digo de la factura
          qryAnula(5).Execute
          'Borraremos de la FA1800 las posibles compensaciones que se refieran a esa factura
          qryAnula(8).rdoParameters(0) = rsCodigo(0) 'Codigo de la factura
          qryAnula(8).Execute
          rsCodigo.MoveNext
        Wend
      End If
    End If
  objApp.rdoConnect.CommitTrans
  
Exit Sub

ErrorEnAnulacion:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Se ha producido un error en la anulaci�n de las facturas, vuelva a realizar el proceso", vbOKOnly + vbCritical, "Atenci�n"

End Sub

Sub IniciarQRY()
Dim MiSqL As String
Dim x As Integer
  
  'Seleccionar los datos de una factura en base al n� de factura
  MiSqL = "Select * from FA0400 Where FA04NUMFACT = ?"
  qryAnula(1).SQL = MiSqL
  
  'Comprobaremos que la factura no se ha contabilizado.
  MiSqL = "Select FA04FECCONTABIL From FA0400 Where FA04NUMFACT = ?"
  qryAnula(2).SQL = MiSqL
  
  'Poner en FA0400 el campo de factura real a 0 si es anulaci�n o el N� si es reconstrucci�n
  MiSqL = "Update FA0400 Set FA04NUMFACREAL = ? Where FA04NUMFACT = ?"
  qryAnula(3).SQL = MiSqL
  
  'Seleccionamos los datos personales de una persona.
  MiSqL = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
              " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
              " CI07CODPOSTAL, CI10DESLOCALID, CI34DESTRATAMI, CI22NUMHISTORIA,CI22DNI" & _
              " from CI2200, CI1000,CI3400 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA  " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI)"
  qryAnula(4).SQL = MiSqL

  'Borrar en FA0300 los rnfs asociados con esa factura
  MiSqL = "Delete From FA0300 Where FA04NUMFACT = ? "
  qryAnula(5).SQL = MiSqL
  
  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSqL = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qryAnula(6).SQL = MiSqL
  MiSqL = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID" & _
              " From CI2300, CI1000, CI0900 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODERSONA " & _
              " And CI2300.CI23NUMDIRPRINC = CI1000.CI10NUMDIRECCI) " & _
  qryAnula(7).SQL = MiSqL
        
  'Borrar en FA1800 las compensaciones que se corresponden a la factura
  MiSqL = "Delete From FA1800 Where FA04CODFACT = ?"
  qryAnula(8).SQL = MiSqL
  
  'En la tabla FA0400 indicaremos que la factura est� compensada.
  MiSqL = "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04NUMFACT = ?"
  qryAnula(9).SQL = MiSqL
  
  'Buscaremos los c�digos de factura que se corresponden con un determinado n� de factura
  MiSqL = "Select FA04CODFACT From FA0400 Where FA04NUMFACT = ?"
  qryAnula(10).SQL = MiSqL
  
  'Seleccionaremos el n� de historia que se ha aplicado para hacer una determinada factura de cara
  'a rehacerla posteriormente.
  MiSqL = "Select CI22NUMHISTORIA From FA0300 Where FA04NUMFACT = ?"
  qryAnula(11).SQL = MiSqL
  
  'Activamos las querys.
  For x = 1 To 11
    Set qryAnula(x).ActiveConnection = objApp.rdoConnect
    qryAnula(x).Prepared = True
  Next
End Sub


Sub pFacturacion()

Dim objFactura As New Factura
Dim Propuesta As String
Dim SQL As String

    BotonSalir = False
    Call frmSeleccion.pRNF(objFactura, 0)
    If BotonSalir = False Then
'    Call frmFactura.pConciertos(objFactura)
'    objFactura.InicializarConciertos
    objFactura.AsignarRNFs
    Propuesta = frm_Propuestas.pPropuestas(objFactura)
    End If
    If BotonSalir = False Then
      ContinuarFactura = True
      Call frmEditFactura.pFactura(objFactura, Propuesta)
      While ContinuarFactura = True
        ContinuarFactura = False
        Propuesta = frm_Propuestas.pPropuestas(objFactura)
        If BotonSalir = False Then
          Call frmEditFactura.pFactura(objFactura, Propuesta)
        End If
      Wend
    End If
    
    'If BotonSalir = False Then
    '  Call frm_Impresion.pImpresion(objFactura, Propuesta)
    'End If
End Sub

Sub pRehacerFactura()

Dim objFactura As New Factura
Dim Propuesta As String
Dim SQL As String
Dim respuesta As String
  
    Call IniciarQRY
    respuesta = InputBox("N� de factura que desea rehacer", "Anulaci�n de facturas", 0)
    If IsNumeric(respuesta) Then
      respuesta = "13/" & respuesta
    Else
      respuesta = "0"
    End If
    If Left(respuesta, 3) = "13/" Then
      Call BorradoFactura(CStr(respuesta))
      BotonSalir = False
      Call frmSeleccion.pRNF(objFactura, Historia)
      If BotonSalir = False Then
      objFactura.AsignarRNFs
      Propuesta = frm_Propuestas.pPropuestas(objFactura)
      End If
      If BotonSalir = False Then
        ContinuarFactura = True
        Call frmEditFactura.pFactura(objFactura, Propuesta)
        While ContinuarFactura = True
          ContinuarFactura = False
          Propuesta = frm_Propuestas.pPropuestas(objFactura)
          If BotonSalir = False Then
            Call frmEditFactura.pFactura(objFactura, Propuesta)
          End If
        Wend
      End If
    End If
    
    'If BotonSalir = False Then
    '  Call frm_Impresion.pImpresion(objFactura, Propuesta)
    'End If
End Sub


Sub Main()

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
' Function VentanaAtributos(aAtributos() As CamposAtrib) As Boolean
'
' Esta funci�n llama a la ventana de petici�n de nuevos atributos
' y nos devolvera un array con los campos y valores a modificar
' el formato del array es una linea por cada atributo que es de un tipo definido
' por el usuario con los siguientes propiedades :
'       .Campo : nombre del campo de la BD a modificar (p.e. FA15FECINICIO)
'       .Actualizar : si se ha seleccionado este campo como modificable
'       .Valor : es el valor a insertar en la BD
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function VentanaAtributos(aAtributos() As CamposAtrib, Nombre As String) As Boolean
    Dim i As Integer
    With frm_Atributos
        ' mostrar la  pantalla de modificacion de atributos
        .Caption = .Caption & Nombre
        .Show vbModal
        
        
        VentanaAtributos = .blnAceptar
        
        ' recoger los valores de los atributos
        If .blnAceptar Then
            '**************************************************
            aAtributos(FA15PRECIOREF).campo = "FA15PRECIOREF"
            aAtributos(FA15PRECIOREF).Actualizar = .cat(0).Value
            aAtributos(FA15PRECIOREF).Valor = .at(0).Text
            '**************************************************
            aAtributos(FA15PRECIODIA).campo = "FA15PRECIODIA"
            aAtributos(FA15PRECIODIA).Actualizar = .cat(1).Value
            aAtributos(FA15PRECIODIA).Valor = .at(1).Text
            '**************************************************
            aAtributos(FA15DESCUENTO).campo = "FA15DESCUENTO"
            aAtributos(FA15DESCUENTO).Actualizar = .cat(2).Value
            aAtributos(FA15DESCUENTO).Valor = .at(2).Text
            '**************************************************
            aAtributos(FA15PERIODOGARA).campo = "FA15PERIODOGARA"
            aAtributos(FA15PERIODOGARA).Actualizar = .cat(3).Value
            aAtributos(FA15PERIODOGARA).Valor = .at(3).Text
            '**************************************************
            aAtributos(FA15INDNECESARIO).campo = "FA15INDNECESARIO"
            aAtributos(FA15INDNECESARIO).Actualizar = .cac(0).Value
            aAtributos(FA15INDNECESARIO).Valor = .ac(0).Value
            '**************************************************
            aAtributos(FA15INDSUFICIENTE).campo = "FA15INDSUFICIENTE"
            aAtributos(FA15INDSUFICIENTE).Actualizar = .cac(1).Value
            aAtributos(FA15INDSUFICIENTE).Valor = .ac(1).Value
            '**************************************************
            aAtributos(FA15INDEXCLUIDO).campo = "FA15INDEXCLUIDO"
            aAtributos(FA15INDEXCLUIDO).Actualizar = .cac(2).Value
            aAtributos(FA15INDEXCLUIDO).Valor = .ac(2).Value
            '**************************************************
            aAtributos(FA15INDLINOBLIG).campo = "FA15INDLINOBLIG"
            aAtributos(FA15INDLINOBLIG).Actualizar = .cac(4).Value
            aAtributos(FA15INDLINOBLIG).Valor = .ac(4).Value
            '**************************************************
            aAtributos(FA15INDFACTOBLIG).campo = "FA15INDFACTOBLIG"
            aAtributos(FA15INDFACTOBLIG).Actualizar = .cac(5).Value
            aAtributos(FA15INDFACTOBLIG).Valor = .ac(5).Value
            '**************************************************
            aAtributos(FA15INDDESCONT).campo = "FA15INDDESCONT"
            aAtributos(FA15INDDESCONT).Actualizar = .cac(3).Value
            aAtributos(FA15INDDESCONT).Valor = .ac(3).Value
            '**************************************************
            aAtributos(CI13CODENTIDAD).campo = "CI13CODENTIDAD"
            aAtributos(CI13CODENTIDAD).Actualizar = .ccbossEntidad.Value
            If .cbossEntidad.Value = "" Then
                aAtributos(CI13CODENTIDAD).Valor = "''"
            Else
                aAtributos(CI13CODENTIDAD).Valor = "'" & .cbossEntidad.Columns(0).Text & "'"
            End If
            '**************************************************
            aAtributos(FA15Variaci�nPorcentual).campo = "Variaci�nPorcentual"
            aAtributos(FA15Variaci�nPorcentual).Actualizar = .cat(4).Value
            aAtributos(FA15Variaci�nPorcentual).Valor = IIf(.at(4).Text = "", 0, .at(4).Text)
            '**************************************************
            aAtributos(FA15Redondeo).campo = "Redondeo"
            aAtributos(FA15Redondeo).Actualizar = .cat(5).Value
            ' si el campo de redondeo esta vacio se pone por defecto que lo redondee a
            ' la unidad
            aAtributos(FA15Redondeo).Valor = IIf(.at(5).Text = "", 1, .at(5).Text)
            '**************************************************
            aAtributos(FA15INDSUPLEMENTO).campo = "FA15INDSUPLEMENTO"
            aAtributos(FA15INDSUPLEMENTO).Actualizar = .cac(6).Value
            aAtributos(FA15INDSUPLEMENTO).Valor = .ac(6).Value
            '**************************************************
            aAtributos(FA15INDDESGLOSE).campo = "FA15INDDESGLOSE"
            aAtributos(FA15INDDESGLOSE).Actualizar = .cac(7).Value
            aAtributos(FA15INDDESGLOSE).Valor = .ac(7).Value
            '**************************************************
            aAtributos(FA15OrdImp).campo = "FA15OrdImp"
            aAtributos(FA15OrdImp).Actualizar = .cat(6).Value
            aAtributos(FA15OrdImp).Valor = .at(6).Text
            '**************************************************
        End If
    End With
    
    ' descargar la pantalla de atributos
    Unload frm_Atributos
End Function
