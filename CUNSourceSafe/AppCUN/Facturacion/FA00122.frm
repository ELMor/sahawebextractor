VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frm_Atributos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Modificaci�n de atributos a partir de : "
   ClientHeight    =   3510
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11655
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3510
   ScaleWidth      =   11655
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   510
      Left            =   10395
      TabIndex        =   33
      Top             =   2970
      Width           =   1230
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Actualizar"
      Default         =   -1  'True
      Height          =   510
      Left            =   9000
      TabIndex        =   32
      Top             =   2970
      Width           =   1230
   End
   Begin VB.Frame fraAtributos 
      Caption         =   "Nuevos Atributos"
      ForeColor       =   &H8000000D&
      Height          =   2295
      Left            =   0
      TabIndex        =   34
      Top             =   585
      Width           =   11655
      Begin VB.CheckBox cat 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   6
         Left            =   10080
         TabIndex        =   42
         Top             =   1620
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   7
         Left            =   7695
         TabIndex        =   37
         Top             =   1215
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   6
         Left            =   11130
         TabIndex        =   36
         Top             =   1215
         Width           =   195
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "Desglose"
         Enabled         =   0   'False
         Height          =   255
         Index           =   7
         Left            =   6660
         TabIndex        =   39
         Top             =   1215
         Width           =   1455
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "Suplemento"
         Enabled         =   0   'False
         Height          =   255
         Index           =   6
         Left            =   9315
         TabIndex        =   38
         Top             =   1215
         Width           =   2235
      End
      Begin VB.CheckBox cat 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   5
         Left            =   4365
         TabIndex        =   5
         Top             =   1215
         Width           =   195
      End
      Begin VB.CheckBox ccbossEntidad 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   8055
         TabIndex        =   12
         Top             =   1935
         Width           =   195
      End
      Begin VB.CheckBox cat 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   4
         Left            =   1485
         TabIndex        =   2
         Top             =   1215
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   3
         Left            =   11130
         TabIndex        =   11
         Top             =   885
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   5
         Left            =   11130
         TabIndex        =   10
         Top             =   585
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   4
         Left            =   11130
         TabIndex        =   9
         Top             =   285
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   2
         Left            =   7695
         TabIndex        =   8
         Top             =   885
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   7695
         TabIndex        =   7
         Top             =   585
         Width           =   195
      End
      Begin VB.CheckBox cac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   7695
         TabIndex        =   6
         Top             =   285
         Width           =   195
      End
      Begin VB.CheckBox cat 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   3
         Left            =   4365
         TabIndex        =   4
         Top             =   630
         Width           =   195
      End
      Begin VB.CheckBox cat 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   2
         Left            =   4365
         TabIndex        =   3
         Top             =   315
         Width           =   195
      End
      Begin VB.CheckBox cat 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   1
         Left            =   1485
         TabIndex        =   1
         Top             =   630
         Width           =   195
      End
      Begin VB.CheckBox cat 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   195
         Index           =   0
         Left            =   1485
         TabIndex        =   0
         Top             =   315
         Width           =   195
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "Facturaci�n Obligatoria ?"
         Enabled         =   0   'False
         Height          =   255
         Index           =   5
         Left            =   9315
         TabIndex        =   29
         Top             =   585
         Width           =   2235
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea Obligatoria ?"
         Enabled         =   0   'False
         Height          =   255
         Index           =   4
         Left            =   9315
         TabIndex        =   28
         Top             =   285
         Width           =   2235
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "Descontable ?"
         Enabled         =   0   'False
         Height          =   255
         Index           =   3
         Left            =   9315
         TabIndex        =   30
         Top             =   885
         Width           =   2235
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "Excluido ?"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   6660
         TabIndex        =   27
         Top             =   885
         Width           =   1455
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "Suficiente ?"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   6660
         TabIndex        =   26
         Top             =   585
         Width           =   1455
      End
      Begin VB.CheckBox ac 
         Alignment       =   1  'Right Justify
         Caption         =   "Necesario ?"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   6660
         TabIndex        =   25
         Top             =   285
         Width           =   1455
      End
      Begin SSDataWidgets_B.SSDBCombo cbossEntidad 
         Height          =   285
         Left            =   8280
         TabIndex        =   31
         Top             =   1905
         Width           =   3300
         DataFieldList   =   "Column 1"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   7064
         Columns(1).Caption=   "Nombre"
         Columns(1).Name =   "Nombre"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   5821
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16777215
         Enabled         =   0   'False
         DataFieldToDisplay=   "Column 1"
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   0
         Left            =   1695
         TabIndex        =   14
         Top             =   270
         Width           =   1250
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         Format          =   "###,###,###.###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   1
         Left            =   1695
         TabIndex        =   16
         Top             =   585
         Width           =   1250
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         MaxLength       =   11
         Format          =   "###,###,###.###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   2
         Left            =   4575
         TabIndex        =   20
         Top             =   270
         Width           =   1250
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         MaxLength       =   7
         Format          =   "###.###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   3
         Left            =   4575
         TabIndex        =   22
         Top             =   585
         Width           =   1250
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         MaxLength       =   5
         Format          =   "#,###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   4
         Left            =   1695
         TabIndex        =   18
         Top             =   1170
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         MaxLength       =   11
         Format          =   "+###.###;-###.###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   5
         Left            =   4575
         TabIndex        =   24
         Top             =   1170
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         MaxLength       =   11
         Format          =   "###.###"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox at 
         Height          =   285
         Index           =   6
         Left            =   10305
         TabIndex        =   40
         Top             =   1575
         Width           =   1245
         _ExtentX        =   2196
         _ExtentY        =   503
         _Version        =   327681
         AutoTab         =   -1  'True
         Enabled         =   0   'False
         MaxLength       =   15
         Format          =   "###,###,##0.###"
         PromptChar      =   " "
      End
      Begin VB.Label al 
         Caption         =   "N� de orden en factura"
         Height          =   255
         Index           =   6
         Left            =   8100
         TabIndex        =   41
         Top             =   1605
         Width           =   1785
      End
      Begin VB.Label al 
         Caption         =   "Redondeo"
         Enabled         =   0   'False
         Height          =   255
         Index           =   5
         Left            =   3015
         TabIndex        =   23
         Top             =   1215
         Width           =   1650
      End
      Begin VB.Label al 
         Caption         =   " % Variaci�n  (+/-)"
         Enabled         =   0   'False
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   17
         Top             =   1215
         Width           =   1650
      End
      Begin VB.Label al 
         Caption         =   "Per�odo Garant�a"
         Enabled         =   0   'False
         Height          =   255
         Index           =   3
         Left            =   3015
         TabIndex        =   21
         Top             =   635
         Width           =   1455
      End
      Begin VB.Label al 
         Caption         =   "Precio Referencia"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   13
         Top             =   330
         Width           =   1785
      End
      Begin VB.Label al 
         Caption         =   "Descuento"
         Enabled         =   0   'False
         Height          =   255
         Index           =   2
         Left            =   3015
         TabIndex        =   19
         Top             =   335
         Width           =   975
      End
      Begin VB.Label al 
         Caption         =   "Precio D�a"
         Enabled         =   0   'False
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   635
         Width           =   975
      End
      Begin VB.Label lcbossEntidad 
         Caption         =   "Entidad Respons."
         Enabled         =   0   'False
         Height          =   255
         Left            =   6690
         TabIndex        =   35
         Top             =   1950
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frm_Atributos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Ruta As String
Public Nombre As String
Public Fecha As String
Public ObjAppAtrib As Object
Public blnAceptar As Boolean


' Constantes para los MaskEditBox
Const PrecioRef = 0
Const PrecioDia = 1
Const Descuento = 2
Const PeriodoGara = 3
Const PorcentVariacion = 4
Const Redondeo = 5
Const OrdImp = 6

' Constantes para los CheckBox
Const Necesario = 0
Const Suficiente = 1
Const Excluido = 2
Const Descont = 3
Const LinOblig = 4
Const FactOblig = 5
Const Suplemento = 6
Const Desglose = 7

Private Sub at_Change(Index As Integer)
    ' si se est� cambiando el Porcentaje de variaci�n de precios
    ' cambiar el color seg�n sea positivo (Negro) o negativo (Rojo)
    If Index = 4 Then
        If IsNumeric(at(Index)) Then
            If at(Index) >= 0 Then
                at(Index).ForeColor = vbBlack
            Else
                at(Index).ForeColor = vbRed
            End If
        End If
    End If
End Sub

Private Sub at_GotFocus(Index As Integer)
    at(Index).SelStart = 0
    at(Index).SelLength = Len(at(Index))
End Sub

Private Sub cat_Click(Index As Integer)
    at(Index).Enabled = cat(Index).Value
    al(Index).Enabled = cat(Index).Value
    
    If cat(Index).Value = vbChecked Then
        at(Index).SetFocus
    Else
        at(Index).Text = ""
    End If
    
    
    ' las casillas para introducir los precios de Referencia, por Dia o Porcentual
    ' son excluyentes entre ellas, no se pueden seleccionar a la vez
    Select Case Index
        Case PorcentVariacion
            ' si elige el cambio de precios porcentual => desactivar otros precios
            If cat(PorcentVariacion).Value = vbChecked Then
                cat(PrecioRef).Value = vbUnchecked
                cat(PrecioDia).Value = vbUnchecked
                ' se activa la casilla de redondeo
                cat(Redondeo).Enabled = True
            Else
                ' se desactiva la casilla de redondeo
                cat(Redondeo).Value = vbUnchecked
                cat(Redondeo).Enabled = False
                
            End If
        Case PrecioRef
            ' si elige precio fijo => desactivar el porcentaje de variaci�n
            If cat(PrecioRef).Value = vbChecked Then
                cat(PorcentVariacion).Value = vbUnchecked
                cat(PrecioDia).Value = vbUnchecked
            End If
        Case PrecioDia
            ' si elige precio dia => desactivar el porcentaje de variaci�n
            If cat(PrecioDia).Value = vbChecked Then
                cat(PorcentVariacion).Value = vbUnchecked
                cat(PrecioRef).Value = vbUnchecked
            End If
    End Select
End Sub

Private Sub cac_Click(Index As Integer)
    ac(Index).Enabled = cac(Index).Value
    If cac(Index).Value = vbUnchecked Then
        ac(Index).Value = vbUnchecked
    End If
End Sub

Private Sub ccbossEntidad_Click()
    cbossEntidad.Enabled = ccbossEntidad.Value
    lcbossEntidad.Enabled = ccbossEntidad.Value
End Sub

Private Sub cmdAceptar_Click()
    If ValidarAtrib = True Then
        blnAceptar = True
        Me.Hide
    End If
End Sub

Private Sub cmdCancelar_Click()
    blnAceptar = False
    Me.Hide
End Sub

Private Sub Form_Load()
    Call CargarComboEntidades
End Sub

Private Sub CargarComboEntidades()
    Dim rq As New rdoQuery
    
    rq.SQL = "SELECT CI13CODENTIDAD, CI13DESENTIDAD FROM CI1300 ORDER BY CI13DESENTIDAD"
            
    Set rq.ActiveConnection = objApp.rdoConnect
    rq.Prepared = True
    
    Dim rsEntidad As rdoResultset
    
    cbossEntidad.RemoveAll
    
    
    Set rsEntidad = rq.OpenResultset
    
    Do While Not rsEntidad.EOF
        cbossEntidad.AddItem rsEntidad(0) & Chr(9) & rsEntidad(1)
        rsEntidad.MoveNext
    Loop
    
    rsEntidad.Close
    Set rq = Nothing
End Sub


Private Function ValidarAtrib() As Boolean
    Dim i As Integer
    Dim blnActualizar As Boolean
    
    ValidarAtrib = False
    blnActualizar = False
    
    ' comprobar que se ha seleccionado alg�n campo para modificar
    For i = 0 To 6
        If cat(i).Value = Checked Then
            blnActualizar = True
            Exit For
        End If
    Next
    For i = 0 To 7
        If cac(i).Value = Checked Then
            blnActualizar = True
            Exit For
        End If
    Next
    If ccbossEntidad.Value = Checked Then
        blnActualizar = True
    End If
    
    If blnActualizar = False Then
        Call MsgBox("No ha seleccionado nada para modificar", , "Error de validaci�n")
        Exit Function
    End If
    
        
    ' ahora se revisan los valores a modificar
    If (at(Descuento).Text <> "") Then
        If ((at(Descuento).Text < 0) Or (100 < at(Descuento).Text)) Then
            Call MsgBox("El Dto debe estar entre 0% y 100%", , "Error de validaci�n")
            at(Descuento).SetFocus
            Exit Function
        End If
    End If
    
    If (at(PorcentVariacion).Text <> "") Then
        If ((at(PorcentVariacion).Text < -100) Or (100 < at(PorcentVariacion).Text)) Then
            Call MsgBox("El % de variaci�n  debe estar entre -100% y 100%", , "Error de validaci�n")
            at(PorcentVariacion).SetFocus
            Exit Function
        End If
    End If
    
    If (at(Redondeo).Text <> "") Then
        If at(Redondeo).Text < 0 Then
            Call MsgBox("El Redondeo debe ser mayor que 0", , "Error de validaci�n")
            at(Redondeo).SetFocus
            Exit Function
        End If
    End If
    
    ' si ha llegado hasta aqui todo es correcto
    ValidarAtrib = True
End Function


