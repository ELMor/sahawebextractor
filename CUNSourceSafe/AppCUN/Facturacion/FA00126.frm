VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_Busqueda 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "B�squeda de Categor�as y Nodos"
   ClientHeight    =   6990
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9570
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6990
   ScaleWidth      =   9570
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   " Resultados de la b�squeda "
      Height          =   5550
      Left            =   90
      TabIndex        =   2
      Top             =   945
      Width           =   9375
      Begin SSDataWidgets_B.SSDBGrid grdCatNodos 
         Height          =   1815
         Left            =   135
         TabIndex        =   3
         Top             =   225
         Width           =   9150
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   3
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "CodCateg"
         Columns(0).Name =   "CodCateg"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).VertScrollBar=   -1  'True
         Columns(1).Width=   15002
         Columns(1).Caption=   "Descripci�n"
         Columns(1).Name =   "Categoria"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "CatNodo"
         Columns(2).Name =   "CatNodo"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   16140
         _ExtentY        =   3201
         _StockProps     =   79
         Caption         =   "Categor�as o Nodos"
      End
      Begin SSDataWidgets_B.SSDBGrid grdConciertos 
         Height          =   3210
         Left            =   135
         TabIndex        =   4
         Top             =   2115
         Width           =   9150
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   3
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   1032
         Columns(0).Caption=   "Editar"
         Columns(0).Name =   "Editar"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Ruta"
         Columns(1).Name =   "Ruta"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   14023
         Columns(2).Caption=   "Concierto"
         Columns(2).Name =   "Concierto"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).VertScrollBar=   -1  'True
         _ExtentX        =   16140
         _ExtentY        =   5662
         _StockProps     =   79
         Caption         =   "Conciertos en los que se encuentra"
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   5355
      TabIndex        =   6
      Top             =   6570
      Width           =   1545
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   2385
      TabIndex        =   5
      Top             =   6570
      Width           =   1545
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   90
      TabIndex        =   1
      Top             =   90
      Width           =   9375
      Begin SSCalendarWidgets_A.SSDateCombo SDCProceso 
         Height          =   285
         Left            =   5535
         TabIndex        =   9
         Top             =   270
         Width           =   1950
         _Version        =   65537
         _ExtentX        =   3440
         _ExtentY        =   503
         _StockProps     =   93
      End
      Begin VB.TextBox txtCadena 
         Height          =   285
         Left            =   1575
         TabIndex        =   0
         Top             =   270
         Width           =   3750
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "Buscar"
         Height          =   510
         Left            =   7650
         TabIndex        =   7
         Top             =   180
         Width           =   1365
      End
      Begin VB.Label Label1 
         Caption         =   "Cadena a buscar:"
         Height          =   240
         Left            =   225
         TabIndex        =   8
         Top             =   315
         Width           =   1320
      End
   End
End
Attribute VB_Name = "frm_Busqueda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private qry(1 To 8) As New rdoQuery
Dim ArrayRutas() As String
Dim ContLineas As Integer

Private Sub Actualizar2(Codigo As String, Nombre As String, FechaProceso As String, Descuento As CamposAtrib)
    Dim Ruta As String
    
    ' quitar la barra final si la tiene
    If Right(Codigo, 1) = "/" Then
        Ruta = Left(Codigo, Len(Codigo) - 1)
    Else
        Ruta = Codigo
    End If
    
    ' borrar los atributos posteriores a la fecha
    qry(1).rdoParameters(0) = Ruta & "%"
    qry(1).rdoParameters(1) = FechaProceso
    qry(1).Execute
    
    ' marcar los registros de atributos afectados por la modificacion
    qry(5).rdoParameters(0) = Ruta & "%"
    qry(5).rdoParameters(1) = FechaProceso
    qry(5).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    qry(3).rdoParameters(0) = FechaProceso
    qry(3).rdoParameters(1) = Ruta & "%"
    qry(3).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    If Descuento.Actualizar = True Then
        qry(6).rdoParameters(0) = Descuento.Valor
        qry(6).rdoParameters(1) = Ruta & "%"
        qry(6).rdoParameters(2) = FechaProceso
        qry(6).Execute
    End If
    ' Borrar los registros de atributos afectados por la modificacion
    qry(2).rdoParameters(0) = FechaProceso
    qry(2).rdoParameters(1) = Ruta & "%"
'    qry(2).rdoParameters(2) = FechaProceso
    qry(2).Execute
    
    ' depurar los atributos
    qry(4).Execute
    
End Sub

Private Sub InitQRY2(SQLInsert As String)
    Dim i As Integer
    
'   Borrar una rama de atributos posteriores a la fecha de proceso
    qry(1).SQL = "DELETE FROM FA1500 " & _
                 "WHERE FA15RUTA = ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECINICIO )"
    
'   Marcar (L�gicamente) una rama de registros de atributos con fecha de fin='9/9/0999 00:00:00'
'   estas ramas ser�n las que se copien para crear los nuevos atrib.
'   Parametros:
'            0 - RUTA sin barra final (si la tuviera)
'            1 - Fecha proceso
    qry(5).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA = ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN )"
                
'   Borrar (L�gicamente) una rama de registros de atributos con fecha de fin=fecha de proceso
'   Parametros:
'            0 - Nueva Fecha Fin (es la variable FechaProceso)
'            1 - RUTA sin barra final (si la tuviera)
    qry(2).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA = ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS'))"
                
    qry(3).SQL = SQLInsert
    
    ' DEPURAR los atributos
    qry(4).SQL = "DELETE FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN"
    
'   Grabar el descuento en los atributos recien generados y que cumplan una serie de condiciones.

'   Parametros:
'            0 - Descuento
'            1 - RUTA sin barra final (si la tuviera)
'            2 - Fecha proceso

    qry(6).SQL = "UPDATE FA1500 SET " & _
                        "FA15DESCUENTO = ? " & _
                 "WHERE FA15RUTA = ? " & _
                   "AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') = FA15FECINICIO )" & _
                   "AND (TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS') = FA15FECFIN )" & _
                   "AND ((FA15PRECIOREF IS NOT NULL) " & _
                   "  OR (FA15PRECIODIA IS NOT NULL)" & _
                   "  OR (FA15FRANQSUMA <> 0)" & _
                   "  OR (FA15FRANQUNI  <> 0)" & _
                   "  OR (FA15FACTOBLIG <> 0)" & _
                   "  OR (FA15LINOBLIG <> 0))" & _
                   "AND (FA15INDDESCONT <>0)"
                   
    
    For i = 1 To UBound(qry)
        Set qry(i).ActiveConnection = objApp.rdoConnect
        qry(i).Prepared = True
    Next
    
End Sub

Private Sub InitQRY(SQLInsert As String)
    Dim i As Integer
    
'   Borrar una rama de atributos posteriores a la fecha de proceso
    qry(1).SQL = "DELETE FROM FA1500 " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECINICIO )"
    
    qry(8).SQL = "DELETE FROM FA0700 WHERE FA15CODATRIB IN (select fa15codatrib FROM FA1500 " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECINICIO ))"

'   Marcar (L�gicamente) una rama de registros de atributos con fecha de fin='9/9/0999 00:00:00'
'   estas ramas ser�n las que se copien para crear los nuevos atrib.
'   Parametros:
'            0 - RUTA sin barra final (si la tuviera)
'            1 - Fecha proceso
    qry(2).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA LIKE ? AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN )"
                
    ' esta es la sql que copia los atributos y los modifica a la vez
    ' se genera a partir de la ventana de petici�n de atributos.
    qry(3).SQL = SQLInsert
    
'   Grabar el descuento en los atributos recien generados y que cumplan una serie de condiciones.
'   para que solo se aplique en aquellos nodos que tengan precio o sea susceptibles de generar lineas
'   de factura y que sean descontables
'   Parametros:
'            0 - Descuento
'            1 - RUTA sin barra final (si la tuviera)
'            2 - Fecha proceso
    qry(4).SQL = "UPDATE FA1500 SET " & _
                        "FA15DESCUENTO = ? " & _
                 "WHERE FA15RUTA LIKE ? " & _
                   "AND (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') = FA15FECINICIO ) " & _
                   "AND (TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS') = FA15FECFIN ) " & _
                   "AND (" & _
                   "     (FA15INDFRANQSUMA <> 0)" & _
                   "  OR (FA15INDFRANQUNI  <> 0)" & _
                   "  OR (FA15INDFACTOBLIG <> 0)" & _
                   "  OR (FA15INDLINOBLIG <> 0)" & _
                   "  OR (FA15INDSUPLEMENTO <> 0)" & _
                   "    ) " & _
                   "AND (FA15INDDESCONT <>0)"


'                   "     (FA15PRECIOREF IS NOT NULL)" & _
'                   "  OR (FA15PRECIODIA IS NOT NULL)" & _
'                   "  OR (FA15INDFRANQSUMA <> 0)" & _

'   Borrar (L�gicamente) una rama de registros de atributos con fecha de fin=fecha de proceso
'   Parametros:
'            0 - Nueva Fecha Fin (es la variable FechaProceso)
'            1 - RUTA sin barra final (si la tuviera)
    qry(5).SQL = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA15RUTA LIKE ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS'))"
                
    
  ' DEPURAR los atributos
  ' se ejecuta primero la 7 (precios por cantidad) y despues la 6 (atributos)
    qry(6).SQL = "DELETE FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN"
    qry(7).SQL = "DELETE FROM FA0700 WHERE FA15CODATRIB IN (SELECT FA15CODATRIB FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN)"

    For i = 1 To UBound(qry)
        Set qry(i).ActiveConnection = objApp.rdoConnect
        qry(i).Prepared = True
    Next
    
End Sub


Sub MostrarRutas()
Dim cont As Integer
Dim Cadena As String
Dim intPosPunto As Integer
Dim intPosBarra As Integer
Dim CatNodo As String
Dim EsCateg As Boolean
Dim CadenaResult As String
Dim rsDesig As rdoResultset

  Me.grdConciertos.RemoveAll
  For cont = 1 To UBound(ArrayRutas)
    CadenaResult = ""
    Cadena = ArrayRutas(cont)
    EsCateg = False
    While Cadena <> ""
      intPosPunto = InStr(1, Cadena, ".")
      If intPosPunto = 1 Then
        Cadena = Right(Cadena, Len(Cadena) - 1)
      Else
        CatNodo = Left(Cadena, intPosPunto - 1)
        If EsCateg = True Then
          qry(7).rdoParameters(0) = CatNodo
          Set rsDesig = qry(7).OpenResultset
          If Not rsDesig.EOF Then
            CadenaResult = CadenaResult & rsDesig(0) & "/"
          End If
        ElseIf EsCateg = False Then
          qry(8).rdoParameters(0) = CatNodo
          Set rsDesig = qry(8).OpenResultset
          If Not rsDesig.EOF Then
            CadenaResult = CadenaResult & rsDesig(0) & "/"
          End If
        End If
        Cadena = Right(Cadena, Len(Cadena) - intPosPunto)
        If InStr(1, Cadena, "/") = 1 Then
          EsCateg = True
          Cadena = Right(Cadena, Len(Cadena) - 1)
        End If
      End If
    Wend
    If Right(CadenaResult, 1) = "/" Then
      CadenaResult = Left(CadenaResult, Len(CadenaResult) - 1)
    End If
    Me.grdConciertos.AddItem 0 & Chr(9) & ArrayRutas(cont) & Chr(9) & CadenaResult
  Next
End Sub

Private Sub cmdAceptar_Click()
    Dim aAtributos(1 To 15) As CamposAtrib
    Dim i As Integer
    Dim Porcentaje As Single
    Dim strRedondeo As String
    Dim miSQLInsert As String
    Dim miSQLSelect As String
    Dim miSQLFrom As String
    Dim miSQLCompleta As String
    Dim Cadena As String
    Dim IntPosComa As Integer
    Dim Concierto As String
    Dim cont As Integer
    'Llamar a la ventana de petici�n de atributos
    If VentanaAtributos(aAtributos, Me.grdCatNodos.Columns(1).Text) = True Then
    
        Screen.MousePointer = vbHourglass
        
        ' Generar la SQL de creaci�n de nuevos atributos con los parametros no configurables
        miSQLInsert = "INSERT INTO FA1500 (FA15CODATRIB, FA15RUTA,  FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15FECINICIO, FA15FECFIN, FA15INDOPCIONAL"
        miSQLSelect = ")(SELECT FA15CODATRIB_SEQUENCE.NEXTVAL, FA15RUTA, FA15INDFRANQSUMA, FA15INDFRANQUNI, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS'), FA15INDOPCIONAL"
        miSQLFrom = " FROM DUAL, FA1500 where fa15ruta like ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS')))"
    
    
        ' a�adir a la SQL los nombres de los campos y los valores posibles
        
        ' las modificaciones de precios dependen de si se modifica
        ' el precio referencia, el precio dia o la variaci�n es porcentual
        miSQLInsert = miSQLInsert & ", " & aAtributos(1).campo
        miSQLInsert = miSQLInsert & ", " & aAtributos(2).campo
            
        If aAtributos(FA15PRECIOREF).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).Valor
            miSQLSelect = miSQLSelect & ", NULL"
                
        ElseIf aAtributos(FA15PRECIODIA).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", NULL"
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).Valor
        
        ElseIf aAtributos(FA15Variaci�nPorcentual).Actualizar = True Then
            ' calculo del porcentaje a aplicar
            Porcentaje = (100 + aAtributos(FA15Variaci�nPorcentual).Valor) / 100
            If aAtributos(FA15Redondeo).Actualizar = False Then
                ' no se aplica redondeo
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje)
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje)
            Else
                strRedondeo = str(aAtributos(FA15Redondeo).Valor)
                miSQLSelect = miSQLSelect & ", ROUND ( " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje) & " / " & strRedondeo & " ) * " & strRedondeo
                miSQLSelect = miSQLSelect & ", ROUND ( " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje) & " / " & strRedondeo & " ) * " & strRedondeo
'                miSQLSelect = miSQLSelect & ", MOD( " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje) & ", " & strRedondeo & " ) * " & strRedondeo
            End If
                
        Else
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo
            
        End If
        
        
        For i = 3 To UBound(aAtributos) - 2
            miSQLInsert = miSQLInsert & ", " & aAtributos(i).campo
            
            If (aAtributos(i).Actualizar = True) And (i <> FA15DESCUENTO) Then
                If aAtributos(i).Valor = "" Then
                    miSQLSelect = miSQLSelect & ", Null"
                Else
                    If IsNumeric(aAtributos(i).Valor) Then
                        miSQLSelect = miSQLSelect & ", " & str(aAtributos(i).Valor)
                    Else
                        miSQLSelect = miSQLSelect & ", " & aAtributos(i).Valor
                    End If
                End If
            Else
                miSQLSelect = miSQLSelect & ", " & aAtributos(i).campo
            End If
        Next
        ' concatenar las tres partes de la SQL
        miSQLCompleta = miSQLInsert + miSQLSelect + miSQLFrom
        
        InitQRY (miSQLCompleta)
        For cont = 1 To ContLineas
          If grdConciertos.Columns(0).Value = -1 Then
            Call Actualizar(grdConciertos.Columns(1).Text, SDCProceso.Date & " 00:00:00", aAtributos(FA15DESCUENTO))
          End If
        Next
    End If

  Screen.MousePointer = vbDefault
End Sub

Private Sub defconc1_CambiarAtribHijos(Codigo As String, Nombre As String, FechaProceso As String)
    Dim aAtributos(1 To 15) As CamposAtrib
    Dim i As Integer
    Dim Porcentaje As Single
    Dim strRedondeo As String
    Dim miSQLInsert As String
    Dim miSQLSelect As String
    Dim miSQLFrom As String
    Dim miSQLCompleta As String
    
    'Llamar a la ventana de petici�n de atributos
    If VentanaAtributos(aAtributos, Nombre) = True Then
        
        Screen.MousePointer = vbHourglass
        
        ' Generar la SQL de creaci�n de nuevos atributos con los parametros no configurables
        miSQLInsert = "INSERT INTO FA1500 (FA15CODATRIB, FA15RUTA,  FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15FECINICIO, FA15FECFIN, FA15INDOPCIONAL"
        miSQLSelect = ")(SELECT FA15CODATRIB_SEQUENCE.NEXTVAL, FA15RUTA, FA15INDFRANQSUMA, FA15INDFRANQUNI, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), TO_DATE('31/12/3000 00:00:00','DD/MM/YYYY HH24:MI:SS'), FA15INDOPCIONAL"
        miSQLFrom = " FROM DUAL, FA1500 where fa15ruta like ? AND (FA15FECFIN = TO_DATE('9/9/0999 00:00:00','DD/MM/YYYY HH24:MI:SS')))"
    
    
        ' a�adir a la SQL los nombres de los campos y los valores posibles
        
        ' las modificaciones de precios dependen de si se modifica
        ' el precio referencia, el precio dia o la variaci�n es porcentual
        miSQLInsert = miSQLInsert & ", " & aAtributos(1).campo
        miSQLInsert = miSQLInsert & ", " & aAtributos(2).campo
            
        If aAtributos(FA15PRECIOREF).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).Valor
            miSQLSelect = miSQLSelect & ", NULL"
                
        ElseIf aAtributos(FA15PRECIODIA).Actualizar = True Then
            miSQLSelect = miSQLSelect & ", NULL"
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).Valor
        
        ElseIf aAtributos(FA15Variaci�nPorcentual).Actualizar = True Then
            ' calculo del porcentaje a aplicar
            Porcentaje = (100 + aAtributos(FA15Variaci�nPorcentual).Valor) / 100
            If aAtributos(FA15Redondeo).Actualizar = False Then
                ' no se aplica redondeo
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje)
                miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje)
            Else
                strRedondeo = str(aAtributos(FA15Redondeo).Valor)
                miSQLSelect = miSQLSelect & ", ROUND ( " & aAtributos(FA15PRECIOREF).campo & " * " & str(Porcentaje) & " / " & strRedondeo & " ) * " & strRedondeo
                miSQLSelect = miSQLSelect & ", ROUND ( " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje) & " / " & strRedondeo & " ) * " & strRedondeo
'                miSQLSelect = miSQLSelect & ", MOD( " & aAtributos(FA15PRECIODIA).campo & " * " & str(Porcentaje) & ", " & strRedondeo & " ) * " & strRedondeo
            End If
                
        Else
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIOREF).campo
            miSQLSelect = miSQLSelect & ", " & aAtributos(FA15PRECIODIA).campo
            
        End If
        
        
        For i = 3 To UBound(aAtributos) - 2
            miSQLInsert = miSQLInsert & ", " & aAtributos(i).campo
            
            If (aAtributos(i).Actualizar = True) And (i <> FA15DESCUENTO) Then
                If aAtributos(i).Valor = "" Then
                    miSQLSelect = miSQLSelect & ", Null"
                Else
                    If IsNumeric(aAtributos(i).Valor) Then
                        miSQLSelect = miSQLSelect & ", " & str(aAtributos(i).Valor)
                    Else
                        miSQLSelect = miSQLSelect & ", " & aAtributos(i).Valor
                    End If
                End If
            Else
                miSQLSelect = miSQLSelect & ", " & aAtributos(i).campo
            End If
        Next
        ' concatenar las tres partes de la SQL
        miSQLCompleta = miSQLInsert + miSQLSelect + miSQLFrom
        
        InitQRY (miSQLCompleta)
        Call Actualizar(Codigo, FechaProceso, aAtributos(FA15DESCUENTO))
        
        Screen.MousePointer = vbDefault
    End If
End Sub


Private Sub cmdBuscar_Click()
Dim rsBusqueda As rdoResultset
Dim MiSqL As String

  Me.grdCatNodos.RemoveAll
  MiSqL = "Select FA05CODCATEG, FA05DESIG From FA0500 where UPPER(FA05DESIG) Like '%" & UCase(Me.txtCadena.Text) & "%'" & _
              " And FA05FECINICIO < TO_DATE('" & Me.SDCProceso.Date & "','DD/MM/YYYY') AND" & _
              " FA05FECFIN >= TO_DATE('" & Me.SDCProceso.Date & "','DD/MM/YYYY')"
  Set rsBusqueda = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not rsBusqueda.EOF Then
    rsBusqueda.MoveLast
    rsBusqueda.MoveFirst
    While Not rsBusqueda.EOF
      Me.grdCatNodos.AddItem rsBusqueda("FA05CODCATEG") & Chr(9) & rsBusqueda("FA05DESIG") & Chr(9) & "C"
      rsBusqueda.MoveNext
    Wend
  End If
  
  MiSqL = "Select FA09CODNODOCONC, FA09DESIG From FA0900 where FA09DESIG Like '%" & Me.txtCadena.Text & "%'"
  Set rsBusqueda = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not rsBusqueda.EOF Then
    rsBusqueda.MoveLast
    rsBusqueda.MoveFirst
    While Not rsBusqueda.EOF
      Me.grdCatNodos.AddItem rsBusqueda("FA09CODNODOCONC") & Chr(9) & rsBusqueda("FA09DESIG") & Chr(9) & "N"
      rsBusqueda.MoveNext
    Wend
  End If
  
  Me.grdCatNodos.Enabled = True
  
End Sub

Private Sub Form_Load()
Dim MiRs As rdoResultset
Dim DIA, MES, ANYO As String

  Set MiRs = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not MiRs.EOF Then
      DIA = Day(MiRs(0))
      MES = Month(MiRs(0))
      ANYO = Year(MiRs(0)) - 1
      SDCProceso.MinDate = Format(DIA & "/" & MES & "/" & ANYO, "dd/mm/yyyy")
      SDCProceso.MaxDate = Format(DateAdd("m", 12, MiRs(0)))
  End If
'  MSKHora.Text = Format("00:00:00", "hh:mm:ss")
  qry(7).SQL = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  Set qry(7).ActiveConnection = objApp.rdoConnect
  qry(7).Prepared = True
  
  qry(8).SQL = "Select FA09DESIG From FA0900 Where FA09CODNODOCONC = ?"
  Set qry(8).ActiveConnection = objApp.rdoConnect
  qry(8).Prepared = True
    

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Realiza las llamadas a las SQL correspondientes para actualizar los atributos de
' todas las ramas hijas de un nodo.
' recibe como parametros :
'       - Ruta : del nodo del que parte la actualizaci�n
'       - FechaProceso
'       - Descuento : tipo de datos que contiene el descuento a aplicar y si es aplicable
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Private Sub Actualizar(Ruta As String, FechaProceso As String, Descuento As CamposAtrib)
    
    objApp.BeginTrans
    On Error GoTo error
    
    ' quitar la barra final si la tiene
    If Right(Ruta, 1) = "/" Then
        Ruta = Left(Ruta, Len(Ruta) - 1)
    End If
    
    Ruta = Ruta & "%"
    
    ' borra primero los precios por cantidad
    qry(8).rdoParameters(0) = Ruta
    qry(8).rdoParameters(1) = FechaProceso
    qry(8).Execute
    ' borrar los atributos posteriores a la fecha
    qry(1).rdoParameters(0) = Ruta
    qry(1).rdoParameters(1) = FechaProceso
    qry(1).Execute
    
    ' marcar los registros de atributos afectados por la modificacion
    qry(2).rdoParameters(0) = Ruta
    qry(2).rdoParameters(1) = FechaProceso
    qry(2).Execute
    
    ' Copiar los atributos y modificarlos a la vez
    qry(3).rdoParameters(0) = FechaProceso
    qry(3).rdoParameters(1) = Ruta
    qry(3).Execute
    
    ' aplicar ( si corresponde) el descuento
    If Descuento.Actualizar = True Then
        qry(4).rdoParameters(0) = Descuento.Valor
        qry(4).rdoParameters(1) = Ruta
        qry(4).rdoParameters(2) = FechaProceso
        qry(4).Execute
    End If
    
    ' Borrar los registros de atributos afectados por la modificacion
    qry(5).rdoParameters(0) = FechaProceso
    qry(5).rdoParameters(1) = Ruta
    qry(5).Execute
    
    
    ' depurar primero los precios por cantidad
    qry(7).Execute
'     depurar los atributos
    qry(6).Execute
    
    objApp.CommitTrans
    Exit Sub
error:
    Call MsgBox("Error en la actualizaci�n", vbCritical, "Error")
    objApp.RollbackTrans
'    Resume
End Sub



Private Sub grdCatNodos_DblClick()
Dim rsBusqueda As rdoResultset
Dim MiSqL As String
Dim ContArray As Integer
Dim Buscar As Boolean

  Buscar = False
  If grdCatNodos.Columns(2).Text = "N" Then
    MiSqL = "Select FA15RUTA From FA1500 where FA15RUTA Like '%." & grdCatNodos.Columns(0).Text & "./'" & _
              " And FA15FECINICIO < TO_DATE('" & Me.SDCProceso.Date & "','DD/MM/YYYY') AND" & _
              " FA15FECFIN >= TO_DATE('" & Me.SDCProceso.Date & "','DD/MM/YYYY')"
  ElseIf grdCatNodos.Columns(2).Text = "C" Then
    MiSqL = "Select FA15RUTA From FA1500 where FA15RUTA Like '%/%." & grdCatNodos.Columns(0).Text & ".'" & _
              " And FA15FECINICIO < TO_DATE('" & Me.SDCProceso.Date & "','DD/MM/YYYY') AND" & _
              " FA15FECFIN >= TO_DATE('" & Me.SDCProceso.Date & "','DD/MM/YYYY')"
  End If
  
  Set rsBusqueda = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not rsBusqueda.EOF Then
    rsBusqueda.MoveLast
    rsBusqueda.MoveFirst
    ContArray = 0
    While Not rsBusqueda.EOF
      If ContArray = 0 Then
        If rsBusqueda("FA15RUTA") <> "" And Left(rsBusqueda("FA15RUTA"), 1) <> "/" Then
          ContArray = 1
          ReDim ArrayRutas(1 To 1)
          ArrayRutas(ContArray) = rsBusqueda("FA15RUTA")
          Buscar = True
        End If
      Else
        If rsBusqueda("FA15RUTA") <> "" And Left(rsBusqueda("FA15RUTA"), 1) <> "/" Then
          ContArray = ContArray + 1
          ReDim Preserve ArrayRutas(1 To ContArray)
          ArrayRutas(ContArray) = rsBusqueda("FA15RUTA")
          Buscar = True
        End If
      End If
      rsBusqueda.MoveNext
    Wend
  End If
  
  ContLineas = ContArray
  If Buscar = True Then
    Call MostrarRutas
  Else
    Me.grdConciertos.RemoveAll
  End If
  
End Sub

