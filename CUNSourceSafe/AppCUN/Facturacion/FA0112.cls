VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "TramoPrecioCantidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Clase para implementar los precios de categorias por cantidad
' para una cantidad x se aplicar� el precio correspondiente a cada tramo
Public HastaCantidad As Double
Public Precio As Double
Public Descripcion As String
