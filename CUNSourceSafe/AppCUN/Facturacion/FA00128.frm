VERSION 5.00
Begin VB.Form frm_EditLinea 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Edici�n de una l�nea de factura"
   ClientHeight    =   2730
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   7245
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2730
   ScaleWidth      =   7245
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   420
      Left            =   6030
      TabIndex        =   12
      Top             =   135
      Width           =   1140
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   6030
      TabIndex        =   11
      Top             =   675
      Width           =   1140
   End
   Begin VB.Frame Frame1 
      Height          =   2625
      Left            =   90
      TabIndex        =   0
      Top             =   0
      Width           =   5865
      Begin VB.TextBox txtCantidad 
         Height          =   285
         Left            =   1395
         MaxLength       =   5
         TabIndex        =   5
         Top             =   270
         Width           =   735
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   285
         Left            =   1350
         MaxLength       =   70
         TabIndex        =   4
         Top             =   765
         Width           =   4470
      End
      Begin VB.TextBox txtPrecio 
         Height          =   285
         Left            =   1350
         MaxLength       =   12
         TabIndex        =   3
         Top             =   1215
         Width           =   1095
      End
      Begin VB.TextBox txtDescuento 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1350
         MaxLength       =   5
         TabIndex        =   2
         Top             =   1665
         Width           =   1095
      End
      Begin VB.TextBox txtImporte 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   285
         Left            =   1350
         MaxLength       =   12
         TabIndex        =   1
         Top             =   2160
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cantidad:"
         Height          =   285
         Index           =   0
         Left            =   45
         TabIndex        =   10
         Top             =   360
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Descripci�n:"
         Height          =   285
         Index           =   1
         Left            =   45
         TabIndex        =   9
         Top             =   810
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio:"
         Height          =   195
         Index           =   2
         Left            =   45
         TabIndex        =   8
         Top             =   1305
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Descuento (%):"
         Height          =   195
         Index           =   3
         Left            =   45
         TabIndex        =   7
         Top             =   1755
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         Height          =   195
         Index           =   4
         Left            =   45
         TabIndex        =   6
         Top             =   2205
         Width           =   1185
      End
   End
End
Attribute VB_Name = "frm_EditLinea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private rnf As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo

Dim PacDesg As String
Dim AsistDesg As String
Dim PropFacDesg As String
Dim NodoDesg As Integer
Dim RespEconom As String
Dim CantAnt As Double
Dim DesigAnt As String
Dim PrecioAnt As Double
Dim DtoAnt As Double
Dim ImporteAnt As Double
'Funci�n que controla la obligatoriedad y validez de los datos del formulario
Function ValidarDatos() As Boolean
  '----------------------------------------------------
  'Comprobamos la obligatoriedad de los datos
  '----------------------------------------------------
  'Cantidad
  If Trim(txtCantidad.Text) = "" Then
    MsgBox "La Cantidad es un dato obligatorio", vbOKOnly + vbCritical, "Atenci�n"
    If txtCantidad.Enabled = True Then
      txtCantidad.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Descripci�n
  If Trim(txtDescripcion.Text) = "" Then
    MsgBox "La Descripci�n es un dato obligatorio", vbOKOnly + vbCritical, "Atenci�n"
    If txtDescripcion.Enabled = True Then
      txtDescripcion.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Precio
  If Trim(txtPrecio.Text) = "" Then
    MsgBox "El Precio es un dato obligatorio", vbOKOnly + vbCritical, "Atenci�n"
    If txtPrecio.Enabled = True Then
      txtPrecio.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Descuento (si est� en blanco le asignamos autom�ticamente 0)
  If Trim(txtDescuento.Text) = "" Then
    txtDescuento.Text = 0
  End If
  
  '------------------------------------------------------------
  'Comprobamos los datos que deben ser num�ricos
  '------------------------------------------------------------
  'Cantidad
  If Not IsNumeric(Me.txtCantidad.Text) And Trim(txtCantidad.Text) <> "" Then
    MsgBox "La cantidad es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If txtCantidad.Enabled = True Then
      txtCantidad.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Precio
  If Not IsNumeric(Me.txtPrecio.Text) And Trim(txtPrecio.Text) <> "" Then
    MsgBox "El Precio es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If txtPrecio.Enabled = True Then
      txtPrecio.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Descuento
  If Not IsNumeric(Me.txtDescuento.Text) And Trim(txtDescuento.Text) <> "" Then
    MsgBox "El Descuento es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If txtDescuento.Enabled = True Then
      txtDescuento.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Comprobamos que el descuento no sea mayor al 100%
  If CDbl(txtDescuento.Text) > 100 Then
    MsgBox "El descuento no puede ser superior al 100%", vbCritical + vbOKOnly, "Atenci�n"
    If txtDescuento.Enabled = True Then
      txtDescuento.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Importe
  If Not IsNumeric(Me.txtImporte.Text) And Trim(txtImporte.Text) <> "" Then
    MsgBox "El Importe es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If txtImporte.Enabled = True Then
      txtImporte.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  'Si el importe est� en blanco, lo calculamos.
  If Trim(txtImporte.Text) = "" Then
    txtImporte.Text = (txtPrecio.Text * txtCantidad.Text - (txtPrecio.Text * txtCantidad.Text - (txtPrecio.Text * txtCantidad.Text * txtDescuento.Text) / 100))
  End If

  ValidarDatos = True
  
End Function

Private Sub cmdAceptar_Click()
Dim NodosFACT As New Nodo
Dim Descripcion As String
Dim intPosDospuntos As Integer
Dim Seguir As Boolean

  Seguir = ValidarDatos
  If Seguir Then
    Descripcion = Me.Caption
    intPosDospuntos = InStr(1, Descripcion, ":")
    Descripcion = Trim(Right(Descripcion, Len(Descripcion) - intPosDospuntos - 1))
    'Nos posiciconamos en el nodo de factura que corresponde al nodo a insertar
    Set NodoFACT = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(1)
    For Each Nodos In NodoFACT.Nodos
      If Nodos.Name = Descripcion Then
        If Trim(txtCantidad.Text) <> "" Then
          Nodos.LineaFact.Cantidad = txtCantidad.Text
        End If
        If Trim(Me.txtDescripcion.Text) <> "" Then
          Nodos.Name = txtDescripcion.Text
        End If
        If Trim(Me.txtPrecio.Text) <> "" Then
          Nodos.LineaFact.Precio = txtPrecio.Text
          Nodos.LineaFact.PrecioNoDescontable = 0
        End If
        If Trim(Me.txtDescuento.Text) <> "" Then
          Nodos.Descuento = Me.txtDescuento.Text
        End If
        Nodos.LineaFact.Importe = Me.txtImporte
        Nodos.LineaFact.ImporteNoDescontable = 0
      End If
    Next
    Unload Me
  End If
End Sub

Public Function pEditarLinea(objFactura As Factura, paciente As String, Asistencia As String, PropuestaFact As String, NodoFactura As String, Cantidad As Double, Designa As String, Precio As Double, Dto As Double, Importe As Double) As Factura
    Set varFact = objFactura
    PacDesg = paciente
    AsistDesg = Asistencia
    PropFacDesg = PropuestaFact
    NodoDesg = CInt(NodoFactura)
    CantAnt = Cantidad
    DesigAnt = Designa
    PrecioAnt = Precio
    DtoAnt = Dto
    ImporteAnt = Importe
    
    Load frm_EditLinea
    'Set frmSeleccion.varFact = objFactura
      frm_EditLinea.Show (vbModal)
            
    Set objFactura = varFact
    Unload frm_EditLinea
End Function

Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub Form_Load()

  Me.Caption = Me.Caption & ": " & DesigAnt
  txtCantidad.Text = CantAnt
  Me.txtDescripcion = DesigAnt
  Me.txtPrecio = PrecioAnt
  Me.txtDescuento = DtoAnt
  Me.txtImporte = ImporteAnt
  
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
    KeyAscii = fValidarDecimales(KeyAscii, txtCantidad)
End Sub


Private Sub txtCantidad_LostFocus()
Dim Cantidad As Double
Dim Precio As Double
Dim Dto As Double
Dim Importe As Double
Dim SubImporte As Double

  If Trim(txtCantidad.Text) <> "" Then
    Cantidad = txtCantidad.Text
  Else
    Cantidad = 0
  End If
  If Trim(txtDescuento.Text) <> "" Then
    Dto = txtDescuento.Text
  Else
    Dto = 0
  End If
  If Trim(Me.txtPrecio.Text) <> "" Then
    Precio = txtPrecio.Text
  Else
    Precio = 0
  End If
  SubImporte = Cantidad * Precio
  Importe = SubImporte - ((SubImporte * Dto) / 100)
  txtImporte.Text = Importe
End Sub


Private Sub txtDescuento_KeyPress(KeyAscii As Integer)
    KeyAscii = fValidarDecimales(KeyAscii, txtCantidad)
End Sub

Private Sub txtDescuento_LostFocus()
Dim Cantidad As Double
Dim Precio As Double
Dim Dto As Double
Dim Importe As Double
Dim SubImporte As Double
  
  If Trim(txtDescuento.Text) <> "" Then
    If txtDescuento.Text > 100 Then
      MsgBox "El descuento no puede ser mayor del 100%", vbInformation + vbOKOnly, "Aviso"
      txtDescuento.Text = 0
    Else
      If Trim(txtDescuento.Text) <> "" Then
         Dto = txtDescuento.Text
      Else
        Dto = 0
      End If
      If Trim(txtCantidad.Text) <> "" Then
        Cantidad = txtCantidad.Text
      Else
        Cantidad = 0
      End If
      If Trim(Me.txtPrecio.Text) <> "" Then
        Precio = txtPrecio.Text
      Else
        Precio = 0
      End If
      SubImporte = Cantidad * Precio
      Importe = SubImporte - ((SubImporte * Dto) / 100)
      txtImporte.Text = Importe
    End If
  Else
      If Trim(txtCantidad.Text) <> "" Then
        Cantidad = txtCantidad.Text
      Else
        Cantidad = 0
      End If
      If Trim(Me.txtPrecio.Text) <> "" Then
        Precio = txtPrecio.Text
      Else
        Precio = 0
      End If
      txtImporte = Cantidad * Precio
  End If
End Sub

Private Sub txtImporte_KeyPress(KeyAscii As Integer)
    KeyAscii = fValidarDecimales(KeyAscii, txtCantidad)
End Sub

Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
    KeyAscii = fValidarDecimales(KeyAscii, txtCantidad)
End Sub


Private Sub txtPrecio_LostFocus()
Dim Cantidad As Double
Dim Precio As Double
Dim Dto As Double
Dim Importe As Double
Dim SubImporte As Double

  If Trim(txtPrecio.Text) <> "" Then
    Precio = txtPrecio.Text
  Else
    Precio = 0
  End If
  If Trim(txtCantidad.Text) <> "" Then
    Cantidad = txtCantidad.Text
  Else
    Cantidad = 0
  End If
  If Trim(Me.txtDescuento.Text) <> "" Then
    Dto = txtDescuento.Text
  Else
    Dto = 0
  End If
  SubImporte = Cantidad * Precio
  Importe = SubImporte - ((SubImporte * Dto) / 100)
  txtImporte.Text = Importe
  
End Sub


