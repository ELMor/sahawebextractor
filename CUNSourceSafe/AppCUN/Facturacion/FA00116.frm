VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_Propuestas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Seleccione una propuesta de factura"
   ClientHeight    =   3060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6930
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3060
   ScaleWidth      =   6930
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "Aceptar"
      Height          =   600
      Left            =   5715
      TabIndex        =   1
      Top             =   90
      Width           =   1140
   End
   Begin SSDataWidgets_B.SSDBGrid grdPropuestas 
      Height          =   2895
      Left            =   90
      TabIndex        =   0
      Top             =   45
      Width           =   5520
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      Col.Count       =   2
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3016
      Columns(0).Caption=   "Paciente"
      Columns(0).Name =   "Paciente"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5741
      Columns(1).Caption=   "Propuesta"
      Columns(1).Name =   "Propuesta"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9737
      _ExtentY        =   5106
      _StockProps     =   79
      Caption         =   "Propuestas de Factura"
   End
End
Attribute VB_Name = "frm_Propuestas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private PropuestaFactura As PropuestaFactura


Public Function pPropuestas(objFactura As Factura) As String
Dim Posicion As Integer
Dim Texto As String

    Set varFact = objFactura
    Load frm_Propuestas
      'Set frmSeleccion.varFact = objFactura
      frm_Propuestas.Show (vbModal)
            
    'Set objFactura = varFact
    '-
    Posicion = InStr(1, grdPropuestas.Columns(0).Text, "-")
    Texto = grdPropuestas.Columns(0).Text
    'Texto = Right(Texto, Len(Texto) - Posicion)
    pPropuestas = Texto & "/" & Me.grdPropuestas.Columns(1).Text
      Unload frm_Propuestas
End Function



Private Sub cmdAceptar_Click()
  MsgBox "Ha seleccionado la propuesta " & grdPropuestas.Columns(1).Text
  Me.Hide
End Sub

Private Sub Form_Load()
Dim txtNombre As String
Dim txtHistoria As String
Dim txtAsistencia As String

  For Each paciente In varFact.Pacientes
    txtHistoria = paciente.Codigo
    For Each Asistencia In paciente.Asistencias
      txtAsistencia = txtHistoria & "-" & Asistencia.Codigo
      For Each PropuestaFactura In Asistencia.PropuestasFactura
        If Asistencia.Facturado = False Then
          txtNombre = txtAsistencia & Chr(9) & PropuestaFactura.Name
          Me.grdPropuestas.AddItem txtNombre
        End If
      Next
    Next
  Next
  
  
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode = 0 Then
    BotonSalir = True
  End If
End Sub


Private Sub grdPropuestas_DblClick()
  MsgBox "Ha seleccionado la propuesta " & grdPropuestas.Columns(1).Text
  Me.Hide
End Sub

