VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmReimpresion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reimpresi�n de facturas"
   ClientHeight    =   3405
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7545
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   7545
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdFacturar 
      Caption         =   "&Facturar"
      Height          =   510
      Left            =   6480
      TabIndex        =   2
      Top             =   180
      Width           =   1005
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   510
      Left            =   6480
      TabIndex        =   1
      Top             =   2790
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid grdFacturas 
      Height          =   3150
      Left            =   135
      TabIndex        =   0
      Top             =   135
      Width           =   6270
      _Version        =   131078
      DataMode        =   2
      ColumnHeaders   =   0   'False
      HeadLines       =   0
      Col.Count       =   5
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupSwapping=   0   'False
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      RowNavigation   =   1
      MaxSelectedRows =   10
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   5
      Columns(0).Width=   2328
      Columns(0).Caption=   "Historia"
      Columns(0).Name =   "Historia"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7752
      Columns(1).Caption=   "Nombre"
      Columns(1).Name =   "Nombre"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "Paciente"
      Columns(2).Name =   "Direccion"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   3200
      Columns(3).Visible=   0   'False
      Columns(3).Caption=   "FecNacim"
      Columns(3).Name =   "FecNacim"
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(4).Width=   3200
      Columns(4).Visible=   0   'False
      Columns(4).Caption=   "FecIngreso"
      Columns(4).Name =   "FecIngreso"
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      _ExtentX        =   11060
      _ExtentY        =   5556
      _StockProps     =   79
      Caption         =   "Facturas a imprimir"
   End
End
Attribute VB_Name = "frmReimpresion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim qry(1 To 8) As New rdoQuery

Sub IniciarQRY()
Dim MiSqL As String
Dim x As Integer
  
  'Seleccionar los datos de una factura en base al n� de factura
  MiSqL = "Select * from FA0400 Where FA04NUMFACT = ?"
  qry(1).SQL = MiSqL
  
  'Seleccionamos las l�neas de una factura en base al c�digo de factura
  MiSqL = "Select * from FA1600 Where FA04CODFACT = ? ORDER BY FA16ORDFACT"
  qry(2).SQL = MiSqL
  
  'Seleccionamos los RNFs de una l�nea de factura en base al n� de factura y al n� de l�nea"
  MiSqL = "Select * from FA0300 Where FA04NUMFACT = ? And FA16NUMLINEA = ?"
  qry(3).SQL = MiSqL
  
  'Seleccionamos los datos de un responsable econ�mico
  
    'CI21CODPERSONA,CI23RAZONSOCIAL,CI23NUMDIRPRINC,CI10CALLE,CI10PORTAL,CI10RESTODIREC
    'CI07CODPOSTAL,CI10DESLOCALID DONDE EL CI21CODPERSONA ESTE RECOGIDO DE CI0900 EN BASE A CI13CODENTIDAD
    
  'Seleccionamos los datos personales de una persona.
  MiSqL = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
              " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
              " CI07CODPOSTAL, CI10DESLOCALID, CI34DESTRATAMI, CI22NUMHISTORIA,CI22DNI" & _
              " from CI2200, CI1000,CI3400 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA  " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI)"
  qry(4).SQL = MiSqL

  'Buscamos las designaci�n del RNF
  MiSqL = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  qry(5).SQL = MiSqL
  
  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSqL = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qry(6).SQL = MiSqL
  MiSqL = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID" & _
              " From CI2300, CI1000, CI0900 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODERSONA " & _
              " And CI2300.CI23NUMDIRPRINC = CI1000.CI10NUMDIRECCI) " & _
  qry(7).SQL = MiSqL
  
  'Activamos las querys.
  For x = 1 To 7
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next
End Sub

' Mediante el procedimiento LlenadoTiposImpresi�n recogeremos los datos de determinadas facturas
' y los introduciremos en los tipos de impresi�n definidos para el efecto.
Private Sub LlenadoTiposImpresion()
Dim rsCabecera As rdoResultset
Dim rsLineas As rdoResultset
Dim rsRnfs As rdoResultset
Dim rsDireccion As rdoResultset
Dim rsDescripcion As rdoResultset
Dim intContCabecera As Integer
Dim IntContLineas As Integer
Dim intContRnfs As Integer
Dim Acumulado As Boolean
Dim PrimerRNF As Boolean
Dim x As Integer
Dim Y As Integer
  'Nos posicionamos en la primera l�nea del grid e inicializamos contadores.
  Me.grdFacturas.MoveFirst
  intContCabecera = 0
  IntContLineas = 0
  intContRnfs = 0
  
  'Recorremos las distintas filas del grid para introducir los datos
  For x = 1 To 10
    'Se ha introducido un n�mero de factura
    If grdFacturas.Columns(0).Text <> "" Then
      qry(1).rdoParameters(0) = grdFacturas.Columns(0).Text   'N� de la factura introducido
      Set rsCabecera = qry(1).OpenResultset
      If Not rsCabecera.EOF Then
        '------------------------------------------------------------------------------------------
        ' DATOS DE LA CABECERA DE LA FACTURA
        '------------------------------------------------------------------------------------------
        'Llenamos los datos referidos a la cabecera
        intContCabecera = intContCabecera + 1
        ReDim Preserve ArrayCabecera(1 To intContCabecera)
        'Buscamos los datos relativos a la entidad (si la hubiera)
        If Not IsNull(rsCabecera("CI13CODENTIDAD")) Then
          'Seleccionamos los datos de una Entidad que sea responsable econ�mico
          ArrayCabecera(intContCabecera).CodEntidad = rsCabecera("CI13CODENTIDAD")
          'Averiguamos cual es el Codigo de Persona que se relaciona con esa entidad
          qry(6).rdoParameters(0) = rsCabecera("CI13CODENTIDAD")
          Set rsDescripcion = qry(6).OpenResultset
          If Not rsDescripcion.EOF Then
            'Asignamos la direcci�n y Raz�n social de la Entidad.
            qry(7).rdoParameters(0) = rsDescripcion("CI21CODPERSONA")
            Set rsDireccion = qry(7).OpenResultset
            If Not rsDireccion.EOF Then
              ArrayCabecera(intContCabecera).NomEntidad = rsDireccion("CI23RAZONSOCIAL")
              ArrayCabecera(intContCabecera).CPPoblac = rsDireccion("CI07CODPOSTAL") & " " & rsDireccion("CI10DESLOCALID")
              ArrayCabecera(intContCabecera).Direccion = rsDireccion("CI10CALLE") & " " & rsDireccion("CI10PORTAL") & _
                                                                                rsDireccion("CI10RESTODIREC")
            End If
          End If
          ArrayCabecera(intContCabecera).NomEntidad = ""
        Else
          'Si no tiene una entidad responsable los dejamos en blanco
          ArrayCabecera(intContCabecera).CodEntidad = ""
          ArrayCabecera(intContCabecera).NomEntidad = ""
        End If
        'Buscamos los datos relativos al paciente
        qry(4).rdoParameters(0) = rsCabecera("CI21CODPERSONA")
        Set rsDireccion = qry(4).OpenResultset
        If Not rsDireccion.EOF Then
          'Tratamiento que se le da al paciente (Sr. D., Sra. D�a., Srta.)
          ArrayCabecera(intContCabecera).Tratamiento = rsDireccion("CI34DESTRATAMI")
          'Nombre y apellidos del paciente
          ArrayCabecera(intContCabecera).NomPaciente = rsDireccion("CI22NOMBRE") & " " & rsDireccion("CI22PRIAPEL") & " " & _
                                                                                  rsDireccion("CI22SEGAPEL")
          'Si existe una entidad responsable de esa factura no asignamos la direcci�n del paciente.
          If ArrayCabecera(intContCabecera).NomEntidad = "" Then
            'Direcci�n del Paciente
            ArrayCabecera(intContCabecera).NomEntidad = ArrayCabecera(intContCabecera).NomPaciente
            ArrayCabecera(intContCabecera).CPPoblac = rsDireccion("CI07CODPOSTAL") & " " & rsDireccion("CI10DESLOCALID")
            ArrayCabecera(intContCabecera).Direccion = rsDireccion("CI10CALLE") & " " & rsDireccion("CI10PORTAL") & _
                                                                               rsDireccion("CI10RESTODIREC")
            ArrayCabecera(intContCabecera).DNI = rsDireccion("CI22DNI") & ""
          End If
        End If
        ArrayCabecera(intContCabecera).Dto = rsCabecera("FA04DESCUENTO") 'Dto aplicado
        ArrayCabecera(intContCabecera).Fecha = Format(rsCabecera("FA04FECFACTURA"), "DD/MM/YYYY") 'Fecha
        ArrayCabecera(intContCabecera).NFactura = rsCabecera("FA04NUMFACT") 'Numero de factura
        ArrayCabecera(intContCabecera).Observac = rsCabecera("FA04OBSERV") & "" 'Observaciones
        ArrayCabecera(intContCabecera).Descrip = rsCabecera("FA04DESCRIP") & ""
        ArrayCabecera(intContCabecera).NumHistoria = rsDireccion("CI22NUMHISTORIA")
        ArrayCabecera(intContCabecera).NumCaso = rsCabecera("AD01CODASISTENCI")
        ArrayCabecera(intContCabecera).TotFactura = rsCabecera("FA04CANTFACT") 'Total Facturado
        'Rellenamos los parametros que nos van a servir para buscar las lineas de esa factura
        qry(2).rdoParameters(0) = rsCabecera("FA04CODFACT") 'C�digo de la factura
        Set rsLineas = qry(2).OpenResultset
        If Not rsLineas.EOF Then
          '----------------------------------------------------------------------------------------
          ' DATOS DE LAS L�NEAS DE LA FACTURA
          '----------------------------------------------------------------------------------------
          'rsLineas.MoveFirst
          While Not rsLineas.EOF
            'Llenamos los datos referidos a cada l�nea de la cabecera
            IntContLineas = IntContLineas + 1
            ReDim Preserve ArrayLineas(1 To IntContLineas)
            ArrayLineas(IntContLineas).NFactura = intContCabecera 'Contador L�neas de cabecera.
            ArrayLineas(IntContLineas).NLinea = IntContLineas 'Contador l�neas de factura.
            ArrayLineas(IntContLineas).Descripcion = rsLineas("FA16DESCRIP") 'Descripci�n de la l�nea
            ArrayLineas(IntContLineas).Importe = rsLineas("FA16IMPORTE") 'Importe total de la l�nea (dto incluido)
            'Rellenamos los par�metros que nos van a servir para buscar los RNFs de esa factura
            qry(3).rdoParameters(0) = rsCabecera("FA04CODFACT")   'N� de la factura
            qry(3).rdoParameters(1) = rsLineas("FA16NUMLINEA")       'N� de l�nea
            PrimerRNF = True
            Set rsRnfs = qry(3).OpenResultset
            If Not rsRnfs.EOF Then
              '------------------------------------------------------------------------------------
              ' DATOS DE LOS RNFS DE LAS L�NEAS DE FACTURA
              '------------------------------------------------------------------------------------
              'rsRnfs.MoveFirst
              While Not rsRnfs.EOF
                Acumulado = False
                If PrimerRNF Then
                  PrimerRNF = False
                Else
                  For Y = 1 To UBound(arrayRNFs)
                    'El rnf existe en el array, por lo cual lo agrupamos.
                    If arrayRNFs(Y).NFactura = intContCabecera And arrayRNFs(Y).NLinea = IntContLineas And arrayRNFs(Y).CodCateg = rsRnfs("FA05CODCATEG") Then
                      arrayRNFs(Y).Cantidad = arrayRNFs(Y).Cantidad + rsRnfs("FA03CANTIDAD")
                      Acumulado = True
                    End If
                  Next
                End If
                'No existe el RNF en el array y lo introducimos
                If Acumulado = False Then
                  intContRnfs = intContRnfs + 1
                  ReDim Preserve arrayRNFs(1 To intContRnfs)
                  'Llenamos los datos referidos a cada rnf de la l�nea
                  arrayRNFs(intContRnfs).NFactura = intContCabecera 'Contador l�neas de cabecera.
                  arrayRNFs(intContRnfs).CodCateg = rsRnfs("FA05CODCATEG")
                  arrayRNFs(intContRnfs).NLinea = IntContLineas 'Contador l�neas de factura.
                  arrayRNFs(intContRnfs).Cantidad = rsRnfs("FA03CANTIDAD") 'Cantidad
                  If rsRnfs("FA03INDDESGLOSE") = 1 Then 'Indicador del deglose del RNF en factura
                    arrayRNFs(intContRnfs).Desglose = True
                  Else
                    arrayRNFs(intContRnfs).Desglose = False
                  End If
                  'Habr� que buscarla dentro de la tabla FA0500
                  qry(5).rdoParameters(0) = rsRnfs("FA05CODCATEG")
                  Set rsDescripcion = qry(5).OpenResultset
                  If Not rsDescripcion.EOF Then
                    arrayRNFs(intContRnfs).Descripcion = rsDescripcion("FA05DESIG")
                  Else
                    arrayRNFs(intContRnfs).Descripcion = ""
                  End If
                End If
                rsRnfs.MoveNext
              Wend
            End If
            rsLineas.MoveNext
          Wend
        End If
        Else
          'No existen l�neas en la factura.
          MsgBox "La factura " & grdFacturas.Columns(0).Text & " no tiene l�neas.", vbExclamation + vbOKOnly, "Atenci�n"
        End If
      Else
        'No existe la factura en la BD.
        'MsgBox "El n�mero de factura " & grdFacturas.Columns(0).Text & " no existe en la BD", vbExclamation + vbOKOnly, "Atenci�n"
      End If
    'End If
    grdFacturas.MoveNext
  Next
        
      
End Sub
Private Sub cmdFacturar_Click()
Dim x As Integer
Dim Cadena As String
Dim MiSqL As String
Dim RsAsistencias As rdoResultset
Dim EnBlanco As Boolean
  
  'Comprobamos que al menos una de las casillas tenga valor
  EnBlanco = True
  Me.grdFacturas.MoveFirst
  For x = 1 To 10
    If grdFacturas.Columns(0).Text <> "" Then
      EnBlanco = False
      Exit For
    Else
      grdFacturas.MoveNext
    End If
  Next
  If EnBlanco Then
    MsgBox "Se debe introducir al menos un n�mero de factura", vbExclamation, "Atenci�n"
    If grdFacturas.Enabled = True Then grdFacturas.SetFocus
    Exit Sub
  End If
  
  
  'Una vez comprobado lo anterior llenamos los tipos definidos por el usario correspondientes a las facturas
  Call LlenadoTiposImpresion
  Me.Hide
  frm_Impresion.Show vbModal
End Sub


Private Sub cmdSalir_Click()
  Me.Hide
End Sub


Private Sub Form_Load()
Dim x As Integer
  Call Me.IniciarQRY
  For x = 1 To 10
    Me.grdFacturas.AddItem Space(0)
  Next
End Sub
  Private Sub grdFacturas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim strFactura As String
Dim rsFactura As rdoResultset
Dim rsNombre As rdoResultset
Dim MiSqL As String

  If grdFacturas.Columns(0).Text <> "" Then
    strFactura = Me.grdFacturas.Columns(0).Text
    MiSqL = "Select * from FA0400 Where FA04NUMFACT = '" & strFactura & "'"
    Set rsFactura = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not rsFactura.EOF Then
      MiSqL = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, CI22FECNACIM, CI22NUMDIRPRINC, " & _
              "CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID" & _
              " from CI2200, CI1000" & _
              " Where CI2200.CI21CODPERSONA = " & rsFactura("CI21CODPERSONA") & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)"
      Set rsNombre = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    
      If Not rsNombre.EOF Then
        rsNombre.MoveFirst
        grdFacturas.Columns(1).Text = rsNombre("CI22NOMBRE") & " " & rsNombre("CI22PRIAPEL") & " " & rsNombre("CI22SEGAPEL")
        grdFacturas.Columns(2).Text = rsNombre("CI10CALLE") & " " & rsNombre("CI10PORTAL") & " " & rsNombre("CI10RESTODIREC") & ", " & rsNombre("CI10DESLOCALID")
        grdFacturas.Columns(3).Text = rsNombre("CI22FECNACIM") & ""
      Else
        MsgBox "No se puede facturar una historia que no est� asociada a un paciente", vbExclamation + vbOKOnly, "Atenci�n"
        grdFacturas.Columns(0).Text = ""
        grdFacturas.Columns(1).Text = ""
        grdFacturas.Columns(2).Text = ""
        grdFacturas.Columns(3).Text = ""
        'GRDFACTURAS.Col = 0
      End If
    Else
      MsgBox "La factura no existe en la base de datos", vbExclamation + vbOKOnly, "Atenci�n"
      grdFacturas.Columns(0).Text = ""
      grdFacturas.Columns(1).Text = ""
      grdFacturas.Columns(2).Text = ""
      grdFacturas.Columns(3).Text = ""
    End If
  Else
    grdFacturas.Columns(1).Text = ""
    grdFacturas.Columns(2).Text = ""
    grdFacturas.Columns(3).Text = ""
  End If

End Sub
Private Sub grdFacturas_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 22 Then
      KeyAscii = fValidarNumFactura(KeyAscii, grdFacturas.Columns(1).Text)
    End If
End Sub






