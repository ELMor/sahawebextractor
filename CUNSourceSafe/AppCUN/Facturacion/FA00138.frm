VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frm_AnulacionFacturas 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Anulaci�n de Facturas"
   ClientHeight    =   3345
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   9885
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3345
   ScaleWidth      =   9885
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   510
      Left            =   8820
      TabIndex        =   1
      Top             =   2745
      Width           =   1005
   End
   Begin VB.CommandButton cmdAnular 
      Caption         =   "&Anular"
      Height          =   510
      Left            =   8820
      TabIndex        =   0
      Top             =   135
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid grdFacturas 
      Height          =   3150
      Left            =   180
      TabIndex        =   2
      Top             =   90
      Width           =   8475
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   4
      MultiLine       =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      RowNavigation   =   1
      MaxSelectedRows =   10
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   4
      Columns(0).Width=   2302
      Columns(0).Caption=   "N� Factura"
      Columns(0).Name =   "NoFactura"
      Columns(0).Alignment=   1
      Columns(0).CaptionAlignment=   2
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   7646
      Columns(1).Caption=   "Responsable econ�mico"
      Columns(1).Name =   "Responsable"
      Columns(1).CaptionAlignment=   2
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(1).Locked=   -1  'True
      Columns(2).Width=   1984
      Columns(2).Caption=   "Fecha Fra."
      Columns(2).Name =   "Fecha"
      Columns(2).Alignment=   1
      Columns(2).CaptionAlignment=   2
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(2).Locked=   -1  'True
      Columns(3).Width=   1984
      Columns(3).Caption=   "Importe"
      Columns(3).Name =   "Cantidad"
      Columns(3).Alignment=   1
      Columns(3).CaptionAlignment=   2
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).Locked=   -1  'True
      _ExtentX        =   14949
      _ExtentY        =   5556
      _StockProps     =   79
      Caption         =   "Facturas a anular"
   End
End
Attribute VB_Name = "frm_AnulacionFacturas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim qry(1 To 10) As New rdoQuery

Sub AnularFacturas()
Dim Cont As Integer
Dim MiRs As rdoResultset
Dim rsCodigo As rdoResultset

  On Error GoTo ErrorEnAnulacion
  objApp.rdoConnect.BeginTrans
    For Cont = 1 To 10
      If grdFacturas.Columns(0).Text <> "" Then
        '2 -> numfact
        'Comprobaremos que la factura no se ha contabilizado.
        qry(2).rdoParameters(0) = grdFacturas.Columns(0).Text
        Set MiRs = qry(2).OpenResultset
        If Not IsNull(MiRs(0)) Then
          'La factura est� contabilizada habr� que estudiarlo.
          'Asignaremos el 0 al N�mero de factura real para indicarle que ha sido anulada.
          qry(3).rdoParameters(0) = 0
          qry(3).rdoParameters(1) = grdFacturas.Columns(0).Text 'N� de la factura
          qry(3).Execute
          'Le indicaremos que la factura est� compensada para que no apareza en los recordatorios.
          qry(9).rdoParameters(0) = grdFacturas.Columns(0).Text 'n� de la factura
          qry(9).Execute
          'Seleccionaremos todos los c�digos de factura que tienen ese n� de factura para borrar los rnfs y las compensaciones
          qry(10).rdoParameters(0) = grdFacturas.Columns(0).Text 'N� de la factura
          Set rsCodigo = qry(10).OpenResultset
          If Not rsCodigo.EOF Then
            While Not rsCodigo.EOF
              'Borraremos de la FA0300 los rnfs relacionados con esa factura
              qry(5).rdoParameters(0) = rsCodigo(0) 'C�digo de la factura
              qry(5).Execute
              'Borraremos de la FA1800 las posibles compensaciones que se refieran a esa factura
              qry(8).rdoParameters(0) = rsCodigo(0) 'Codigo de la factura
              qry(8).Execute
              rsCodigo.MoveNext
            Wend
          End If
        Else
          'La factura est� sin contabilizar la anulamos sin m�s.
          'Asignaremos el 0 al N�mero de factura real para indicarle que ha sido anulada.
          qry(3).rdoParameters(0) = 0
          qry(3).rdoParameters(1) = grdFacturas.Columns(0).Text 'N� de la factura
          qry(3).Execute
          'Le indicaremos que la factura est� compensada para que no apareza en los recordatorios.
          qry(9).rdoParameters(0) = grdFacturas.Columns(0).Text 'n� de la factura
          qry(9).Execute
          'Seleccionaremos todos los c�digos de factura que tienen ese n� de factura para borrar los rnfs y las compensaciones
          qry(10).rdoParameters(0) = grdFacturas.Columns(0).Text 'N� de la factura
          Set rsCodigo = qry(10).OpenResultset
          If Not rsCodigo.EOF Then
            While Not rsCodigo.EOF
              'Borraremos de la FA0300 los rnfs relacionados con esa factura
              qry(5).rdoParameters(0) = rsCodigo(0) 'C�digo de la factura
              qry(5).Execute
              'Borraremos de la FA1800 las posibles compensaciones que se refieran a esa factura
              qry(8).rdoParameters(0) = rsCodigo(0) 'Codigo de la factura
              qry(8).Execute
              rsCodigo.MoveNext
            Wend
          End If
        End If
      End If
      grdFacturas.MoveNext
    Next
  objApp.rdoConnect.CommitTrans
  
Exit Sub

ErrorEnAnulacion:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Se ha producido un error en la anulaci�n de las facturas, vuelva a realizar el proceso", vbOKOnly + vbCritical, "Atenci�n"
End Sub

Sub IniciarQRY()
Dim MiSqL As String
Dim x As Integer
  
  'Seleccionar los datos de una factura en base al n� de factura
  MiSqL = "Select * from FA0400 Where FA04NUMFACT = ?"
  qry(1).SQL = MiSqL
  
  'Comprobaremos que la factura no se ha contabilizado.
  MiSqL = "Select FA04FECCONTABIL From FA0400 Where FA04NUMFACT = ?"
  qry(2).SQL = MiSqL
  
  'Poner en FA0400 el campo de factura real a 0 si es anulaci�n o el N� si es reconstrucci�n
  MiSqL = "Update FA0400 Set FA04NUMFACREAL = ? Where FA04NUMFACT = ?"
  qry(3).SQL = MiSqL
  
  'Seleccionamos los datos personales de una persona.
  MiSqL = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
              " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
              " CI07CODPOSTAL, CI10DESLOCALID, CI34DESTRATAMI, CI22NUMHISTORIA,CI22DNI" & _
              " from CI2200, CI1000,CI3400 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA  " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI)"
  qry(4).SQL = MiSqL

  'Borrar en FA0300 los rnfs asociados con esa factura
  MiSqL = "Delete From FA0300 Where FA04NUMFACT = ? "
  qry(5).SQL = MiSqL
  
  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSqL = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qry(6).SQL = MiSqL
  MiSqL = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID" & _
              " From CI2300, CI1000, CI0900 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODERSONA " & _
              " And CI2300.CI23NUMDIRPRINC = CI1000.CI10NUMDIRECCI) " & _
  qry(7).SQL = MiSqL
        
  'Borrar en FA1800 las compensaciones que se corresponden a la factura
  MiSqL = "Delete From FA1800 Where FA04CODFACT = ?"
  qry(8).SQL = MiSqL
  
  'En la tabla FA0400 indicaremos que la factura est� compensada.
  MiSqL = "Update FA0400 Set FA04INDCOMPENSA = 2 Where FA04NUMFACT = ?"
  qry(9).SQL = MiSqL
  
  'Buscaremos los c�digos de factura que se corresponden con un determinado n� de factura
  MiSqL = "Select FA04CODFACT From FA0400 Where FA04NUMFACT = ?"
  qry(10).SQL = MiSqL
  
  'Activamos las querys.
  For x = 1 To 10
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next
End Sub

Private Sub cmdAnular_Click()
Dim x As Integer
Dim Cadena As String
Dim MiSqL As String
Dim RsAsistencias As rdoResultset
Dim EnBlanco As Boolean
  
  'Comprobamos que al menos una de las casillas tenga valor
  EnBlanco = True
  Me.grdFacturas.MoveFirst
  For x = 1 To 10
    If grdFacturas.Columns(0).Text <> "" Then
      EnBlanco = False
      Exit For
    Else
      grdFacturas.MoveNext
    End If
  Next
  If EnBlanco Then
    MsgBox "Se debe introducir al menos un n�mero de factura", vbExclamation, "Atenci�n"
    If grdFacturas.Enabled = True Then grdFacturas.SetFocus
    Exit Sub
  End If
  
  
  'Una vez comprobado lo anterior llenamos los tipos definidos por el usario correspondientes a las facturas
  Call Me.AnularFacturas
  Me.Hide
End Sub


Private Sub cmdAnular_Click()

End Sub


Private Sub cmdSalir_Click()
  Me.Hide
End Sub


Private Sub Form_Load()
Dim x As Integer
  Call Me.IniciarQRY
  For x = 1 To 10
    Me.grdFacturas.AddItem Space(0)
  Next
End Sub
  Private Sub grdFacturas_BeforeColUpdate(ByVal ColIndex As Integer, ByVal OldValue As Variant, Cancel As Integer)
Dim strFactura As String
Dim rsFactura As rdoResultset
Dim rsNombre As rdoResultset
Dim MiSqL As String

  If grdFacturas.Columns(0).Text <> "" Then
    strFactura = Me.grdFacturas.Columns(0).Text
    MiSqL = "Select * from FA0400 Where FA04NUMFACT = '" & strFactura & "'"
    Set rsFactura = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not rsFactura.EOF Then
      MiSqL = "Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, CI2200.CI21CODPERSONA, CI22FECNACIM, CI22NUMDIRPRINC, " & _
              "CI10CALLE, CI10PORTAL, CI10RESTODIREC, CI10DESLOCALID" & _
              " from CI2200, CI1000" & _
              " Where CI2200.CI21CODPERSONA = " & rsFactura("CI21CODPERSONA") & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)"
      Set rsNombre = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    
      If Not rsNombre.EOF Then
        rsNombre.MoveFirst
        grdFacturas.Columns(1).Text = rsNombre("CI22NOMBRE") & " " & rsNombre("CI22PRIAPEL") & " " & rsNombre("CI22SEGAPEL")
        grdFacturas.Columns(2).Text = Format(rsFactura("FA04FECFACTURA"), "DD/MM/YYYY")
        grdFacturas.Columns(3).Text = Format(rsFactura("FA04CANTFACT"), "###,###,##0.##")
      Else
        MsgBox "No se puede anular una factura que no est� asociada a un paciente", vbExclamation + vbOKOnly, "Atenci�n"
        grdFacturas.Columns(0).Text = ""
        grdFacturas.Columns(1).Text = ""
        grdFacturas.Columns(2).Text = ""
        grdFacturas.Columns(3).Text = ""
        'GRDFACTURAS.Col = 0
      End If
    Else
      MsgBox "La factura no existe en la base de datos", vbExclamation + vbOKOnly, "Atenci�n"
      grdFacturas.Columns(0).Text = ""
      grdFacturas.Columns(1).Text = ""
      grdFacturas.Columns(2).Text = ""
      grdFacturas.Columns(3).Text = ""
    End If
  Else
    grdFacturas.Columns(1).Text = ""
    grdFacturas.Columns(2).Text = ""
    grdFacturas.Columns(3).Text = ""
  End If

End Sub
Private Sub grdFacturas_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 22 Then
      KeyAscii = fValidarNumFactura(KeyAscii, grdFacturas.Columns(0).Text)
    End If
End Sub








