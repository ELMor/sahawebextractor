VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "PropuestaFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public NodosFactura As New Collection  'copia local
Private mvarName As String 'copia local

Public Property Let Name(ByVal vData As String)
'se usa cuando se asigna un valor a una propiedad, en el lado izquierdo de la asignación.
'Syntax: X.Name = 5
    mvarName = vData
End Property


Public Property Get Name() As String
Attribute Name.VB_UserMemId = 0
'se usa cuando se asigna un valor a una propiedad, en el lado derecho de la asignación.
'Syntax: Debug.Print X.Name
    mvarName = ""
    For Each NodoFactura In NodosFactura
        mvarName = mvarName & IIf(mvarName = "", "", " + ") & "(" & NodoFactura.NombreConcierto & ") " & NodoFactura.Name
    Next
    Name = mvarName
End Property




