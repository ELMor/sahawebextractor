VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "idperson.ocx"
Begin VB.Form frm_Historico 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Hist�rico de factura"
   ClientHeight    =   6855
   ClientLeft      =   150
   ClientTop       =   390
   ClientWidth     =   10410
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6855
   ScaleWidth      =   10410
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   10410
      _ExtentX        =   18362
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   " Responsable econ�mico "
      Height          =   2070
      Index           =   0
      Left            =   45
      TabIndex        =   1
      Top             =   450
      Width           =   10350
      Begin TabDlg.SSTab tabPacientes 
         Height          =   1725
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   270
         Width           =   10185
         _ExtentX        =   17965
         _ExtentY        =   3043
         _Version        =   327681
         Style           =   1
         Tabs            =   1
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FA00137.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "txtText1(5)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "IdPersona1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         Begin idperson.IdPersona IdPersona1 
            Height          =   1320
            Left            =   90
            TabIndex        =   3
            Top             =   360
            Width           =   10050
            _ExtentX        =   17727
            _ExtentY        =   2328
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   0
            Left            =   -74880
            TabIndex        =   9
            Top             =   450
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18865
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   4575
            TabIndex        =   8
            Tag             =   "Nombre Persona|Nombre Persona"
            Top             =   405
            Visible         =   0   'False
            Width           =   1950
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   6840
            TabIndex        =   5
            Tag             =   "D.N.I|D.N.I "
            Top             =   405
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   7380
            TabIndex        =   6
            Tag             =   "Segundo Apellido|Segundo Apellido"
            Top             =   1200
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   7140
            TabIndex        =   7
            Tag             =   "Primer Apellido|Primer Apellido"
            Top             =   855
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   8790
            TabIndex        =   4
            Tag             =   "N�mero Historia"
            Top             =   420
            Visible         =   0   'False
            Width           =   1410
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   10
      Top             =   6585
      Width           =   10410
      _ExtentX        =   18362
      _ExtentY        =   476
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.TreeView tvwHistorico 
      Height          =   3975
      Left            =   0
      TabIndex        =   11
      Top             =   2565
      Width           =   10365
      _ExtentX        =   18283
      _ExtentY        =   7011
      _Version        =   327682
      Indentation     =   587
      LineStyle       =   1
      Style           =   7
      ImageList       =   "ImageList1"
      Appearance      =   1
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   2565
      Top             =   5355
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   1
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "FA00137.frx":001C
            Key             =   "Fact"
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frm_Historico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public intIndGrid As Integer
Dim objMasterInfo     As New clsCWForm
Private qry(1 To 8) As New rdoQuery
Sub CargarDatos(Historia As Long)
Dim n As Node
Dim MiRs As rdoResultset
Dim Texto As String
Dim Clave As String
Dim ClaveAnt As String
Dim ClaveFact As String
Dim TextoFact As String

  tvwHistorico.Nodes.Clear
  qry(1).rdoParameters(0) = Historia
  Set MiRs = qry(1).OpenResultset
  If Not MiRs.EOF Then
    While Not MiRs.EOF
      Clave = MiRs("AD07CODPROCESO") & "-"
      Clave = Clave & MiRs("AD01CODASISTENCI") & ""
      'Texto = MiRs("AD01CODASISTENCI") & "-"
      'Texto = Texto & MiRs("AD07CODPROCESO") & "/"
      Texto = Format(MiRs("AD01FECINICIO"), "dd-mm-yyyy") & "   "
      Texto = Texto & Format(MiRs("AD01FECFIN"), "dd-mm-yyyy") & "   "
      Texto = Texto & MiRs("AD02DESDPTO") & "  "
      Texto = Texto & MiRs("SG02NOM") & " "
      Texto = Texto & MiRs("SG02APE1") & " "
      Texto = Texto & MiRs("SG02APE2") & "   "
      Texto = Texto & MiRs("CI32CODTIPECON") & ""
      Texto = Texto & MiRs("CI13CODENTIDAD") & ""
      ClaveFact = MiRs("FA04CODFACT") & ""
      TextoFact = MiRs("FA04NUMFACT") & "   "
      TextoFact = TextoFact & Format(MiRs("FA04FECFACTURA"), "dd-mm-yyyy") & "   "
      TextoFact = TextoFact & Format(MiRs("FA04CANTFACT"), "###,###,##0.##")
      If Clave <> ClaveAnt Then
        Set n = tvwHistorico.Nodes.Add(, , Clave, Left(Texto, 99))
        If ClaveFact <> "" Then
          Set n = tvwHistorico.Nodes.Add(Clave, tvwChild, "F" & ClaveFact, Left(TextoFact, 99))
          n.Tag = MiRs("FA04NUMFACT") & ""
          n.Parent.Image = "Fact"
        End If
        ClaveAnt = Clave
      Else
        If ClaveFact <> "" Then
          Set n = tvwHistorico.Nodes.Add(Clave, tvwChild, "F" & ClaveFact, Left(TextoFact, 99))
          n.Tag = MiRs("FA04NUMFACT") & ""
        End If
      End If
      MiRs.MoveNext
    Wend
  End If
End Sub

Sub IniciarQRY()
Dim MiSql As String
Dim i As Integer

  MiSql = "Select AD0800.AD01CODASISTENCI, AD0800.AD07CODPROCESO,AD0100.AD01FECINICIO," & _
               " AD0100.AD01FECFIN,AD0200.AD02DESDPTO,SG0200.SG02NOM,SG0200.SG02APE1,SG0200.SG02APE2," & _
               " AD1100.CI32CODTIPECON,AD1100.CI13CODENTIDAD, FA04CODFACT,FA04NUMFACT, FA04FECFACTURA, FA04CANTFACT" & _
               " FROM AD0800, AD0100,AD0200,SG0200,AD1100,AD0500,FA0400 " & _
               " WHERE AD0100.CI22NUMHISTORIA = ? " & _
               " AND (AD0100.AD01CODASISTENCI = AD0800.AD01CODASISTENCI) " & _
               " AND (AD0800.AD01CODASISTENCI = AD0500.AD01CODASISTENCI AND " & _
                     "AD0800.AD07CODPROCESO = AD0500.AD07CODPROCESO) " & _
               " AND (AD0500.AD02CODDPTO = AD0200.AD02CODDPTO) AND " & _
                     "(AD0500.SG02COD = SG0200. SG02COD) " & _
               " AND AD0500.AD05FECFINRESPON IS NULL " & _
               " AND (AD0800.AD01CODASISTENCI = AD1100.AD01CODASISTENCI AND " & _
                     "AD0800.AD07CODPROCESO = AD1100.AD07CODPROCESO)" & _
               " AND AD1100.AD11FECFIN IS NULL " & _
               " AND (FA0400.AD01CODASISTENCI (+)= AD0800.AD01CODASISTENCI " & _
               " AND FA0400.AD07CODPROCESO (+)= AD0800.AD07CODPROCESO)" & _
               " ORDER BY AD07CODPROCESO, AD01CODASISTENCI"
    qry(1).SQL = MiSql
    
      'Seleccionar los datos de una factura en base al n� de factura
  MiSql = "Select * from FA0400 Where FA04NUMFACT = ?"
  qry(2).SQL = MiSql
  
  'Seleccionamos las l�neas de una factura en base al c�digo de factura
  MiSql = "Select * from FA1600 Where FA04CODFACT = ? ORDER BY FA16ORDFACT"
  qry(3).SQL = MiSql
  
  'Seleccionamos los RNFs de una l�nea de factura en base al n� de factura y al n� de l�nea"
  MiSql = "Select * from FA0300 Where FA04NUMFACT = ? And FA16NUMLINEA = ?"
  qry(4).SQL = MiSql
  
  'Seleccionamos los datos de un responsable econ�mico
  
    'CI21CODPERSONA,CI23RAZONSOCIAL,CI23NUMDIRPRINC,CI10CALLE,CI10PORTAL,CI10RESTODIREC
    'CI07CODPOSTAL,CI10DESLOCALID DONDE EL CI21CODPERSONA ESTE RECOGIDO DE CI0900 EN BASE A CI13CODENTIDAD
    
  'Seleccionamos los datos personales de una persona.
  MiSql = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
              " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
              " CI07CODPOSTAL, CI10DESLOCALID, CI34DESTRATAMI, CI22NUMHISTORIA,CI22DNI" & _
              " from CI2200, CI1000,CI3400 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA  " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI)"
  qry(5).SQL = MiSql

  'Buscamos las designaci�n del RNF
  MiSql = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  qry(6).SQL = MiSql
  
  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSql = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qry(7).SQL = MiSql
  MiSql = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID" & _
              " From CI2300, CI1000, CI0900 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODERSONA " & _
              " And CI2300.CI23NUMDIRPRINC = CI1000.CI10NUMDIRECCI) " & _
  qry(8).SQL = MiSql

    For i = 1 To 8
        Set qry(i).ActiveConnection = objApp.rdoConnect
        qry(i).Prepared = True
    Next
                         
                
End Sub


Private Sub Form_Load()

  Dim intGridIndex      As Integer
  Dim lngUserCode As Long
  Dim blncommit As Boolean
  Call Me.IniciarQRY
  Call objApp.SplashOn
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
  
  With objMasterInfo
    '.strName = "Paciente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPacientes
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    If lngUserCode <> 0 Then
         .strWhere = "CI21CODPERSONA = " & lngUserCode
    End If
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", False)
    .blnChanged = False
  End With
  
  
  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormCreateInfo(objMasterInfo)
    
    IdPersona1.ToolTipText = ""
    .CtrlGetInfo(IdPersona1).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
 Call .WinRegister
    Call .WinStabilize
    IdPersona1.BackColor = objApp.objUserColor.lngKey
    blncommit = False
  End With
      
      

 Call objApp.SplashOff
 
    'Invisible bot�n de B�squeda
  IdPersona1.blnAvisos = False
  IdPersona1.Text = ""
End Sub

Private Sub grdDBGrid1_Change(Index As Integer)

  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
Dim strFieldOrder       As String
  strFieldOrder = objWinInfo.CtrlGetInfo(grdDBGrid1(intIndex).Columns(grdDBGrid1(intIndex).Columns(ColIndex).Name)).objControl.DataField
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
    Call objWinInfo.DataRefresh
  End If
End Sub

Private Sub grdDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, Y As Single)
'  If Button = 2 Then
'    PopupMenu mnuEstado
'  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub IdPersona1_GotFocus()
'  frmCitasPaciente.MousePointer = vbHourglass
'  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.CtrlGotFocus
'  frmCitasPaciente.MousePointer = vbDefault
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  If strFormName = "fraFrame1(0)" And strCtrl = "IdPersona1" Then
     IdPersona1.SearchPersona
     objWinInfo.DataRefresh
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  'fraFrame1(9).ForeColor = fraFrame1(tabTab1(2).Tab + 1).ForeColor
  If strFormName = "fraFrame1(0)" Then
    IdPersona1.blnAvisos = True
    tlbToolbar1.Buttons(26).Enabled = True

  Else
    IdPersona1.blnAvisos = False
    tlbToolbar1.Buttons(26).Enabled = False
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
      objWinInfo.DataRefresh
    End If
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
    End If
  End If
  
  
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If strFormName = "fraFrame1(0)" Then
'    Call IdPersona1.ReadPersona
'  End If
  If Trim(objMasterInfo.rdoCursor("CI21CODPERSONA")) = "" Then
    tvwHistorico.Nodes.Clear
  ElseIf Trim(IdPersona1.Historia) <> "" Then
    Call Me.CargarDatos(IdPersona1.Historia)
  Else
    tvwHistorico.Nodes.Clear
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabPacientes_Click(PreviousTab As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Me.MousePointer = vbDefault
End Sub

Private Sub tabPacientes_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)

End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  objMasterInfo.blnChanged = False
  If btnButton.Index = 16 Then
    'Call IdPersona1.Buscar
  ElseIf btnButton.Index >= 21 And btnButton.Index <= 24 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'En base a la historia cargar los datos
    If Trim(IdPersona1.Historia) <> "" Then
      Call CargarDatos(IdPersona1.Historia)
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  'Me.MousePointer = vbDefault
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   
End Sub




Private Sub tvwHistorico_DblClick()

  If Left(tvwHistorico.SelectedItem.Key, 1) = "F" Then
    If tvwHistorico.SelectedItem.Tag <> "" Then
      Call LlenadoTiposImpresion(tvwHistorico.SelectedItem.Tag)
    End If
  End If
End Sub


' Mediante el procedimiento LlenadoTiposImpresi�n recogeremos los datos de determinadas facturas
' y los introduciremos en los tipos de impresi�n definidos para el efecto.
Private Sub LlenadoTiposImpresion(Factura As String)
Dim rsCabecera As rdoResultset
Dim rsLineas As rdoResultset
Dim rsRnfs As rdoResultset
Dim rsDireccion As rdoResultset
Dim rsDescripcion As rdoResultset
Dim intContCabecera As Integer
Dim IntContLineas As Integer
Dim intContRnfs As Integer
Dim Acumulado As Boolean
Dim PrimerRNF As Boolean
Dim x As Integer
Dim Y As Integer
  'Nos posicionamos en la primera l�nea del grid e inicializamos contadores.
  intContCabecera = 0
  IntContLineas = 0
  intContRnfs = 0
  
'Se ha introducido un n�mero de factura
  qry(2).rdoParameters(0) = Factura 'N� de la factura introducido
  Set rsCabecera = qry(2).OpenResultset
  If Not rsCabecera.EOF Then
    '------------------------------------------------------------------------------------------
    ' DATOS DE LA CABECERA DE LA FACTURA
    '------------------------------------------------------------------------------------------
    'Llenamos los datos referidos a la cabecera
    intContCabecera = intContCabecera + 1
    ReDim Preserve ArrayCabecera(1 To intContCabecera)
    'Buscamos los datos relativos a la entidad (si la hubiera)
    If Not IsNull(rsCabecera("CI13CODENTIDAD")) Then
      'Seleccionamos los datos de una Entidad que sea responsable econ�mico
      ArrayCabecera(intContCabecera).CodEntidad = rsCabecera("CI13CODENTIDAD")
      'Averiguamos cual es el Codigo de Persona que se relaciona con esa entidad
      qry(7).rdoParameters(0) = rsCabecera("CI13CODENTIDAD")
      Set rsDescripcion = qry(7).OpenResultset
      If Not rsDescripcion.EOF Then
        'Asignamos la direcci�n y Raz�n social de la Entidad.
        qry(8).rdoParameters(0) = rsDescripcion("CI21CODPERSONA")
        Set rsDireccion = qry(8).OpenResultset
        If Not rsDireccion.EOF Then
          ArrayCabecera(intContCabecera).NomEntidad = rsDireccion("CI23RAZONSOCIAL")
          ArrayCabecera(intContCabecera).CPPoblac = rsDireccion("CI07CODPOSTAL") & " " & rsDireccion("CI10DESLOCALID")
          ArrayCabecera(intContCabecera).Direccion = rsDireccion("CI10CALLE") & " " & rsDireccion("CI10PORTAL") & _
                                                                            rsDireccion("CI10RESTODIREC")
        End If
      End If
      ArrayCabecera(intContCabecera).NomEntidad = ""
    Else
      'Si no tiene una entidad responsable los dejamos en blanco
      ArrayCabecera(intContCabecera).CodEntidad = ""
      ArrayCabecera(intContCabecera).NomEntidad = ""
    End If
    'Buscamos los datos relativos al paciente
    qry(5).rdoParameters(0) = rsCabecera("CI21CODPERSONA")
    Set rsDireccion = qry(5).OpenResultset
    If Not rsDireccion.EOF Then
      'Tratamiento que se le da al paciente (Sr. D., Sra. D�a., Srta.)
      ArrayCabecera(intContCabecera).Tratamiento = rsDireccion("CI34DESTRATAMI")
      'Nombre y apellidos del paciente
      ArrayCabecera(intContCabecera).NomPaciente = rsDireccion("CI22NOMBRE") & " " & rsDireccion("CI22PRIAPEL") & " " & _
                                                                              rsDireccion("CI22SEGAPEL")
      'Si existe una entidad responsable de esa factura no asignamos la direcci�n del paciente.
      If ArrayCabecera(intContCabecera).NomEntidad = "" Then
        'Direcci�n del Paciente
        ArrayCabecera(intContCabecera).NomEntidad = ArrayCabecera(intContCabecera).NomPaciente
        ArrayCabecera(intContCabecera).CPPoblac = rsDireccion("CI07CODPOSTAL") & " " & rsDireccion("CI10DESLOCALID")
        ArrayCabecera(intContCabecera).Direccion = rsDireccion("CI10CALLE") & " " & rsDireccion("CI10PORTAL") & _
                                                                           rsDireccion("CI10RESTODIREC")
        ArrayCabecera(intContCabecera).DNI = rsDireccion("CI22DNI") & ""
      End If
    End If
    ArrayCabecera(intContCabecera).Dto = rsCabecera("FA04DESCUENTO") 'Dto aplicado
    ArrayCabecera(intContCabecera).Fecha = Format(rsCabecera("FA04FECFACTURA"), "DD/MM/YYYY") 'Fecha
    ArrayCabecera(intContCabecera).NFactura = rsCabecera("FA04NUMFACT") 'Numero de factura
    ArrayCabecera(intContCabecera).Observac = rsCabecera("FA04OBSERV") & "" 'Observaciones
    ArrayCabecera(intContCabecera).Descrip = rsCabecera("FA04DESCRIP") & ""
    ArrayCabecera(intContCabecera).NumHistoria = rsDireccion("CI22NUMHISTORIA")
    ArrayCabecera(intContCabecera).NumCaso = rsCabecera("AD01CODASISTENCI")
    ArrayCabecera(intContCabecera).TotFactura = rsCabecera("FA04CANTFACT") 'Total Facturado
    'Rellenamos los parametros que nos van a servir para buscar las lineas de esa factura
    qry(3).rdoParameters(0) = rsCabecera("FA04CODFACT") 'C�digo de la factura
    Set rsLineas = qry(3).OpenResultset
    If Not rsLineas.EOF Then
      '----------------------------------------------------------------------------------------
      ' DATOS DE LAS L�NEAS DE LA FACTURA
      '----------------------------------------------------------------------------------------
      'rsLineas.MoveFirst
      While Not rsLineas.EOF
        'Llenamos los datos referidos a cada l�nea de la cabecera
        IntContLineas = IntContLineas + 1
        ReDim Preserve ArrayLineas(1 To IntContLineas)
        ArrayLineas(IntContLineas).NFactura = intContCabecera 'Contador L�neas de cabecera.
        ArrayLineas(IntContLineas).NLinea = IntContLineas 'Contador l�neas de factura.
        ArrayLineas(IntContLineas).Descripcion = rsLineas("FA16DESCRIP") 'Descripci�n de la l�nea
        ArrayLineas(IntContLineas).Importe = rsLineas("FA16IMPORTE") 'Importe total de la l�nea (dto incluido)
        'Rellenamos los par�metros que nos van a servir para buscar los RNFs de esa factura
        qry(4).rdoParameters(0) = rsCabecera("FA04CODFACT")   'N� de la factura
        qry(4).rdoParameters(1) = rsLineas("FA16NUMLINEA")       'N� de l�nea
        PrimerRNF = True
        Set rsRnfs = qry(4).OpenResultset
        If Not rsRnfs.EOF Then
          '------------------------------------------------------------------------------------
          ' DATOS DE LOS RNFS DE LAS L�NEAS DE FACTURA
          '------------------------------------------------------------------------------------
          'rsRnfs.MoveFirst
          While Not rsRnfs.EOF
            Acumulado = False
            If PrimerRNF Then
              PrimerRNF = False
            Else
              For Y = 1 To UBound(arrayRNFs)
                'El rnf existe en el array, por lo cual lo agrupamos.
                If arrayRNFs(Y).NFactura = intContCabecera And arrayRNFs(Y).NLinea = IntContLineas And arrayRNFs(Y).CodCateg = rsRnfs("FA05CODCATEG") Then
                  arrayRNFs(Y).Cantidad = arrayRNFs(Y).Cantidad + rsRnfs("FA03CANTIDAD")
                  Acumulado = True
                End If
              Next
            End If
            'No existe el RNF en el array y lo introducimos
            If Acumulado = False Then
              intContRnfs = intContRnfs + 1
              ReDim Preserve arrayRNFs(1 To intContRnfs)
              'Llenamos los datos referidos a cada rnf de la l�nea
              arrayRNFs(intContRnfs).NFactura = intContCabecera 'Contador l�neas de cabecera.
              arrayRNFs(intContRnfs).CodCateg = rsRnfs("FA05CODCATEG")
              arrayRNFs(intContRnfs).NLinea = IntContLineas 'Contador l�neas de factura.
              arrayRNFs(intContRnfs).Cantidad = rsRnfs("FA03CANTIDAD") 'Cantidad
              If rsRnfs("FA03INDDESGLOSE") = 1 Then 'Indicador del deglose del RNF en factura
                arrayRNFs(intContRnfs).Desglose = True
              Else
                arrayRNFs(intContRnfs).Desglose = False
              End If
              'Habr� que buscarla dentro de la tabla FA0500
              qry(6).rdoParameters(0) = rsRnfs("FA05CODCATEG")
              Set rsDescripcion = qry(6).OpenResultset
              If Not rsDescripcion.EOF Then
                arrayRNFs(intContRnfs).Descripcion = rsDescripcion("FA05DESIG")
              Else
                arrayRNFs(intContRnfs).Descripcion = ""
              End If
            End If
            rsRnfs.MoveNext
          Wend
        End If
        rsLineas.MoveNext
      Wend
    End If
    Else
      'No existen l�neas en la factura.
      MsgBox "La factura " & Factura & " no tiene l�neas.", vbExclamation + vbOKOnly, "Atenci�n"
    End If
      
      
End Sub


