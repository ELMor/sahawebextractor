VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Asistencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Public Codigo As String
Public RNFs As New Collection
Public PropuestasFactura As New Collection
Public CodConciertos As New Collection
Public FecInicio As String
Public FecFin As String
Public Facturado As Boolean

' Se utiliza solo para contener los nodos de concierto
' aplicables que se encuentren despues de hacer una primera
' asignacion de RNFs. a partir de esta colecci�n se realizan
' las diferentes combinaciones de conciertos que se puedan
' aplicar simultaneamente.
Public NodosAplicables As New Collection

Public FecIngreso As String

Public Sub QuitarAsignacion()
    For Each rnf In RNFs
        rnf.Asignado = False
        rnf.Ruta = ""
    Next
End Sub

Public Function BuscarRNF(ByVal Codigo As Long, ByVal FecInicio As String, ByVal FecFin As String, Ruta) As Collection
    Dim rnf As rnf
    Dim RNFOrigen As rnf
    Dim Fecha As String
    Dim cont As Integer
    Dim RNFsAsignados As New Collection
    
  
    FecInicio = Format(FecInicio, "yyyymmdd")
    FecFin = Format(FecFin, "yyyymmdd")
    
    On Error GoTo error
    
    cont = 0
    Do While True

Buscar:
        Set RNFOrigen = New rnf
        Set RNFOrigen = RNFs.Item(Codigo & "-" & Format(cont, "000"))
        
        ' Crear una copia del RNForigen
        Set rnf = New rnf
        rnf.Copiar RNFOrigen
        
        Fecha = Format(rnf.Fecha, "yyyymmdd")
        ' Comprobar que est� entre las fechas del nodo del concierto
        If (FecInicio <= Fecha) And (Fecha < FecFin) Then
            ' si todav�a no est� asignado a ning�n nodo se los asignamos
            ' a este
            If RNFOrigen.Asignado = False Then
                RNFOrigen.Asignado = True
                rnf.Ruta = Ruta
                RNFsAsignados.Add rnf, rnf.Key
            End If
        End If
        cont = cont + 1
        
'            Err.Raise vbObjectError + 1, , "El RNF no est� entre las fechas"
    Loop
    
Exit Function

error:
    Select Case Err.Number
        Case 5
            ' El Rnf no se encuentra en la colecci�n
            Set BuscarRNF = RNFsAsignados
            
        Case vbObjectError + 1
            ' El RNF no est� entre las fechas
            ' hay que buscar el siguiente
            cont = cont + 1
            Resume Buscar
            
        Case Else
            MsgBox "Error n� " & Err.Number & " " & Err.Description
            
    End Select
End Function

Public Function AsignacionCompleta() As Boolean
    Dim blnAsigCompleta As Boolean
    Dim rnf As rnf
    
    ' Por defecto consideramos que todos los RNFs est�n asignados
    
    blnAsigCompleta = True
    
    ' Revisamos la colecci�n de rnf's y en cuanto encontramos uno sin
    ' asignar nos salimos
    For Each rnf In RNFs
        If rnf.Asignado = False Then
            blnAsigCompleta = False
            Exit For
        End If
    Next
    
    AsignacionCompleta = blnAsigCompleta
End Function

Public Sub AddConcierto(codconcierto As Long, Optional CodNodoFact As Long)
    ' a�ade un nuevo codigo de concierto a la asistencia
    Dim Concierto As New CodigosConcierto
    
    Concierto.Concierto = codconcierto
    If IsMissing(CodNodoFact) Then
        Concierto.NodoAFacturar = 0
    Else
        Concierto.NodoAFacturar = CodNodoFact
    End If
    
    CodConciertos.Add Concierto
    
End Sub

Public Function Dias() As Integer
    ' Devuelve el n� de dias transcurridos entre las
    ' fechas de inicio y fin de la asistencia
    On Error GoTo error
    
    Dias = DateDiff("d", FecInicio, FecFin)
    Exit Function
    
error:
    Dias = 0
End Function
