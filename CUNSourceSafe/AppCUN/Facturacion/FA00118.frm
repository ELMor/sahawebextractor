VERSION 5.00
Begin VB.Form frmNuevaLinea 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Introducci�n de una nueva l�nea de facturaci�n"
   ClientHeight    =   2865
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7335
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2865
   ScaleWidth      =   7335
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   420
      Left            =   6120
      TabIndex        =   12
      Top             =   720
      Width           =   1140
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   420
      Left            =   6120
      TabIndex        =   11
      Top             =   180
      Width           =   1140
   End
   Begin VB.Frame Frame1 
      Height          =   2760
      Left            =   45
      TabIndex        =   5
      Top             =   45
      Width           =   5910
      Begin VB.TextBox txtImporte 
         Height          =   285
         Left            =   1395
         MaxLength       =   12
         TabIndex        =   4
         Top             =   2115
         Width           =   1095
      End
      Begin VB.TextBox txtDescuento 
         Height          =   285
         Left            =   1395
         MaxLength       =   5
         TabIndex        =   3
         Top             =   1665
         Width           =   735
      End
      Begin VB.TextBox txtPrecio 
         Height          =   285
         Left            =   1395
         MaxLength       =   12
         TabIndex        =   2
         Top             =   1215
         Width           =   1095
      End
      Begin VB.TextBox txtDescripcion 
         Height          =   285
         Left            =   1395
         MaxLength       =   70
         TabIndex        =   1
         Top             =   765
         Width           =   4245
      End
      Begin VB.TextBox txtCantidad 
         Height          =   285
         Left            =   1395
         MaxLength       =   5
         TabIndex        =   0
         Top             =   315
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         Height          =   285
         Index           =   4
         Left            =   45
         TabIndex        =   10
         Top             =   2160
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Descuento (%):"
         Height          =   285
         Index           =   3
         Left            =   45
         TabIndex        =   9
         Top             =   1710
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio:"
         Height          =   285
         Index           =   2
         Left            =   45
         TabIndex        =   8
         Top             =   1260
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Descripci�n:"
         Height          =   285
         Index           =   1
         Left            =   45
         TabIndex        =   7
         Top             =   810
         Width           =   1185
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cantidad:"
         Height          =   285
         Index           =   0
         Left            =   45
         TabIndex        =   6
         Top             =   360
         Width           =   1185
      End
   End
End
Attribute VB_Name = "frmNuevaLinea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private rnf As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo

Dim PacDesg As String
Dim AsistDesg As String
Dim PropFacDesg As String
Dim NodoDesg As Integer
Dim RespEconom As String
Public Function pNuevaLinea(objFactura As Factura, paciente As String, Asistencia As String, PropuestaFact As String, NodoFactura As String, Responsable As String) As Factura
    Set varFact = objFactura
    PacDesg = paciente
    AsistDesg = Asistencia
    PropFacDesg = PropuestaFact
    NodoDesg = CInt(NodoFactura)
    RespEconom = Responsable
    Load frmNuevaLinea
    'Set frmSeleccion.varFact = objFactura
      frmNuevaLinea.Show (vbModal)
            
    Set objFactura = varFact
    Unload frmNuevaLinea
End Function



Function ValidarDatos() As Boolean
Dim Importe As Double

  If Not IsNumeric(Me.txtCantidad.Text) Then
    MsgBox "La Cantidad es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtCantidad.Enabled Then
      Me.txtCantidad.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  
  If Not IsNumeric(Me.txtPrecio.Text) Then
    MsgBox "El Precio es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtPrecio.Enabled Then
      Me.txtPrecio.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  
  If Not IsNumeric(Me.txtDescuento.Text) Then
    MsgBox "El Descuento es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtDescuento.Enabled Then
      Me.txtDescuento.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  
  If CDbl(txtDescuento.Text) > 100 Then
    MsgBox "El descuento no puede ser superior al 100%", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtDescuento.Enabled Then
      Me.txtDescuento.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  
  If Not IsNumeric(Me.txtImporte.Text) Then
    MsgBox "El Importe es un dato num�rico", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtImporte.Enabled Then
      Me.txtImporte.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  

  If Trim(Me.txtCantidad.Text) = "" Then
    MsgBox "La Cantidad es un dato obligatorio", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtCantidad.Enabled Then
      Me.txtCantidad.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  
  If Trim(Me.txtDescripcion.Text) = "" Then
    MsgBox "La Descripci�n es un dato obligatorio", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtDescripcion.Enabled Then
      Me.txtDescripcion.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  
  If Trim(Me.txtPrecio.Text) = "" Then
    MsgBox "El Precio es un dato obligatorio", vbCritical + vbOKOnly, "Atenci�n"
    If Me.txtPrecio.Enabled Then
      Me.txtPrecio.SetFocus
    End If
    ValidarDatos = False
    Exit Function
  End If
  
  If Trim(Me.txtDescuento.Text) = "" Then
    Me.txtDescuento.Text = 0
  End If
  
  If Trim(Me.txtImporte.Text) = "" Then
    Importe = txtCantidad.Text * txtPrecio.Text
    txtImporte.Text = Importe - ((Importe * Me.txtDescuento.Text) / 100)
  End If
      
  ValidarDatos = True
  
End Function

Private Sub cmdAceptar_Click()
Dim NodosFACT As New Nodo
Dim Seguir As Boolean
  Seguir = ValidarDatos
  If Seguir Then
  'Nos posiciconamos en el nodo de factura que corresponde al nodo a insertar
    Set NodoFACT = varFact.Pacientes(PacDesg).Asistencias(AsistDesg).PropuestasFactura(PropFacDesg).NodosFactura(NodoDesg)
    'Le indicamos que muestre el nuevo nodo y si el importe es distinto de 0 que lo facture
    NodosFACT.LinOblig = True
    NodosFACT.FactOblig = True  'Importe # 0, lo facturaremos.
    NodosFACT.Name = Me.txtDescripcion & " (" & txtCantidad & ")"
    NodosFACT.LineaFact.Cantidad = 0 & txtCantidad
    NodosFACT.LineaFact.Importe = txtCantidad * txtPrecio
    'Si el precio es 0 directamente le indicamos que el descuento tambi�n es 0.
    NodosFACT.LineaFact.Dto = txtDescuento
    NodosFACT.EntidadResponsable = RespEconom
    'Finalmente a�adimos la nueva l�nea de factura (nodo) y su rnf.
    NodoFACT.Nodos.Add NodosFACT
    Unload Me
  End If
End Sub


Private Sub cmdCancelar_Click()
  Unload Me
End Sub

Private Sub txtCantidad_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtCantidad)
End Sub


Private Sub txtDescuento_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtDescuento)
End Sub

Private Sub txtDescuento_LostFocus()
  If Trim(txtDescuento) = "" Then
    txtDescuento.Text = 0
  End If
  If txtDescuento > 100 Then
    MsgBox "El descuento no puede ser mayor que 100", vbInformation + vbOKOnly, "Aviso"
  Else
    If IsNumeric(txtPrecio) And IsNumeric(Me.txtCantidad) Then
      txtImporte = (txtPrecio * txtCantidad) - (((txtPrecio * txtCantidad) * txtDescuento) / 100)
    End If
  End If
End Sub


Private Sub txtImporte_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub


Private Sub txtPrecio_KeyPress(KeyAscii As Integer)
  KeyAscii = fValidarDecimales(KeyAscii, txtPrecio)
End Sub

Private Sub txtPrecio_LostFocus()
  If Trim(txtDescuento) = "" Then
    txtDescuento.Text = 0
  End If
  If txtDescuento > 100 Then
    MsgBox "El descuento no puede ser mayor que 100", vbInformation + vbOKOnly, "Aviso"
  Else
    If IsNumeric(txtPrecio) And IsNumeric(Me.txtCantidad) Then
      txtImporte = (txtPrecio * txtCantidad) - (((txtPrecio * txtCantidad) * txtDescuento) / 100)
    End If
  End If
End Sub


