VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmEditFactura 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edici�n Factura"
   ClientHeight    =   7500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9990
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7500
   ScaleWidth      =   9990
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkFinalFactura 
      Caption         =   "Observacion en factura?"
      Height          =   240
      Index           =   1
      Left            =   6435
      TabIndex        =   23
      Top             =   6840
      Width           =   2085
   End
   Begin VB.ComboBox cboColetillas 
      Height          =   315
      ItemData        =   "FA00115.frx":0000
      Left            =   45
      List            =   "FA00115.frx":0002
      Sorted          =   -1  'True
      TabIndex        =   22
      Top             =   7155
      Width           =   9870
   End
   Begin VB.TextBox txtColetillas 
      Height          =   285
      Left            =   360
      TabIndex        =   21
      Text            =   "Text1"
      Top             =   7155
      Width           =   690
   End
   Begin VB.CommandButton cmdEditarLinea 
      Caption         =   "&Editar L�nea"
      Height          =   405
      Left            =   8685
      TabIndex        =   20
      Top             =   4770
      Width           =   1215
   End
   Begin VB.Frame Frame2 
      Caption         =   "Responsable econ�mico"
      Height          =   1095
      Left            =   60
      TabIndex        =   2
      Top             =   1680
      Width           =   8535
      Begin SSDataWidgets_B.SSDBGrid grdssResponsable 
         Height          =   855
         Left            =   60
         TabIndex        =   13
         Top             =   180
         Width           =   8415
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   6
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   6
         Columns(0).Width=   12012
         Columns(0).Caption=   "Responsable"
         Columns(0).Name =   "Responsable"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "CodPersona"
         Columns(1).Name =   "CodPersona"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Entidad"
         Columns(2).Name =   "Entidad"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   397
         Columns(3).Caption=   "Linea"
         Columns(3).Name =   "Linea"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1799
         Columns(4).Caption=   "NFactura"
         Columns(4).Name =   "NFactura"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "EntiPrivado"
         Columns(5).Name =   "EntiPrivado"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         _ExtentX        =   14843
         _ExtentY        =   1508
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Pacientes"
      Height          =   1575
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   8535
      Begin SSDataWidgets_B.SSDBGrid grdSSPac 
         Height          =   1275
         Left            =   60
         TabIndex        =   1
         Top             =   180
         Width           =   8415
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   5
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   1402
         Columns(0).Caption=   "Historia"
         Columns(0).Name =   "Historia"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3704
         Columns(1).Caption=   "Paciente"
         Columns(1).Name =   "Paciente"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1746
         Columns(2).Caption=   "F. Ingreso"
         Columns(2).Name =   "F. Ingreso"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   4974
         Columns(3).Caption=   "Direcci�n"
         Columns(3).Name =   "Direcci�n"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1984
         Columns(4).Caption=   "F. Nacimiento"
         Columns(4).Name =   "F. Nacimiento"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   14843
         _ExtentY        =   2249
         _StockProps     =   79
      End
   End
   Begin VB.TextBox txtObservaciones 
      Height          =   735
      Index           =   1
      Left            =   45
      MaxLength       =   200
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   17
      Top             =   6300
      Width           =   5910
   End
   Begin VB.CommandButton cmdRedondeo 
      Caption         =   "&Redondeo"
      Height          =   405
      Left            =   8685
      TabIndex        =   16
      Top             =   5490
      Width           =   1215
   End
   Begin VB.CommandButton cmdNuevaLinea 
      Caption         =   "&Nueva Linea"
      Height          =   405
      Left            =   8685
      TabIndex        =   15
      Top             =   4275
      Width           =   1215
   End
   Begin VB.CommandButton cmdA�adir 
      Caption         =   "&A�adir ..."
      Height          =   405
      Left            =   8700
      TabIndex        =   14
      Top             =   1800
      Width           =   1215
   End
   Begin VB.CommandButton cmdConcierto 
      Caption         =   "Ver Conciertos"
      Height          =   405
      Left            =   8700
      TabIndex        =   12
      Top             =   3810
      Width           =   1215
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   405
      Left            =   8700
      TabIndex        =   11
      Top             =   6585
      Width           =   1215
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   8700
      TabIndex        =   10
      Top             =   6090
      Width           =   1215
   End
   Begin VB.CommandButton cmdHistorico 
      Caption         =   "Ver Hist�rico"
      Height          =   405
      Left            =   8700
      TabIndex        =   9
      Top             =   3345
      Width           =   1215
   End
   Begin VB.CommandButton cmdRNF 
      Caption         =   "Ver Desglose"
      Height          =   405
      Left            =   8730
      TabIndex        =   8
      Top             =   2880
      Width           =   1215
   End
   Begin VB.TextBox txtTotal 
      Alignment       =   1  'Right Justify
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   360
      Index           =   1
      Left            =   7260
      TabIndex        =   7
      Top             =   6420
      Width           =   1275
   End
   Begin VB.TextBox txtDcto 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00000000&
      Height          =   285
      Index           =   1
      Left            =   7650
      TabIndex        =   5
      Top             =   6075
      Width           =   900
   End
   Begin SSDataWidgets_B.SSDBGrid grdssFact 
      Height          =   3105
      Left            =   60
      TabIndex        =   3
      Top             =   2835
      Width           =   8535
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   6
      AllowDelete     =   -1  'True
      AllowUpdate     =   0   'False
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowDragDrop   =   0   'False
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   6
      Columns(0).Width=   8017
      Columns(0).Caption=   "Descripci�n"
      Columns(0).Name =   "Descripci�n"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   1931
      Columns(1).Caption=   "Precio"
      Columns(1).Name =   "Precio"
      Columns(1).Alignment=   1
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   1244
      Columns(2).Caption=   "Dcto. %"
      Columns(2).Name =   "Dcto. %"
      Columns(2).Alignment=   1
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      Columns(3).Width=   2064
      Columns(3).Caption=   "Importe"
      Columns(3).Name =   "Importe"
      Columns(3).Alignment=   1
      Columns(3).DataField=   "Column 3"
      Columns(3).DataType=   8
      Columns(3).FieldLen=   256
      Columns(3).HasForeColor=   -1  'True
      Columns(3).ForeColor=   16711680
      Columns(4).Width=   741
      Columns(4).Caption=   "U."
      Columns(4).Name =   "Cantidad"
      Columns(4).CaptionAlignment=   1
      Columns(4).DataField=   "Column 4"
      Columns(4).DataType=   8
      Columns(4).FieldLen=   256
      Columns(5).Width=   3200
      Columns(5).Visible=   0   'False
      Columns(5).Caption=   "Franquicia"
      Columns(5).Name =   "Franquicia"
      Columns(5).DataField=   "Column 5"
      Columns(5).DataType=   8
      Columns(5).FieldLen=   256
      _ExtentX        =   15055
      _ExtentY        =   5477
      _StockProps     =   79
      Caption         =   "L�neas de Factura"
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   240
      Left            =   1350
      TabIndex        =   19
      Top             =   6030
      Width           =   4605
   End
   Begin VB.Label Label2 
      Caption         =   "Observaciones:"
      Height          =   255
      Index           =   1
      Left            =   90
      TabIndex        =   18
      Top             =   6030
      Width           =   1170
   End
   Begin VB.Label Label2 
      Caption         =   "TOTAL"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   6360
      TabIndex        =   6
      Top             =   6465
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Dcto %"
      Height          =   255
      Index           =   0
      Left            =   6360
      TabIndex        =   4
      Top             =   6075
      Width           =   675
   End
End
Attribute VB_Name = "frmEditFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private varFact As Factura
Private paciente As paciente
Private Asistencia As Asistencia
Private NuevoRNF As rnf
Private PropuestaFact As PropuestaFactura
Private NodoFACT As Nodo
Private Nodos As Nodo
Private ConciertoAplicable As Concierto
Private Entidad As Nodo
Private rnf As rnf
Dim numAsist As String
Dim NumProc As String
Dim Propuesta As String
Dim Asist As String
Dim NodoFactura As String
Dim CodPaciente As String
Dim UltimaLinea As Integer
Dim qry(1 To 10) As New rdoQuery
Dim qrys(1 To 8) As New rdoQuery
Dim lngAsistencia As Long
Dim lngProceso As Long

'Este tipo de usuario lo utilizaremos para agrupar rnfs de cara la impresi�n
Private Type TypeRNFs
  NumFact As Integer
  NumLinea As Integer
  Codigo As Long
  Descrip As String
  Cantidad As Integer
  Desglose As Boolean
End Type

Dim ArrayContador() As TypeRNFs
Dim ArrayOrdenacion() As Integer

Sub BuscarResponsableEconomico(NumAsistencia As Long, NumProceso As Long)
Dim rsBusqueda As rdoResultset
Dim rsBusqueda2 As rdoResultset
Dim x As Integer
Dim cont As Integer

  For x = 1 To grdSSPac.Rows
    If grdSSPac.Columns(0).Text = CodPaciente Then
      Me.grdssResponsable.AddItem Me.grdSSPac.Columns(1).Text & Chr(9) & grdSSPac.Columns(0).Text & Chr(9) & "T" & Chr(9) & 1 & Chr(9) & "" & Chr(9) & "P"
    End If
    grdSSPac.MoveNext
    cont = x
    If x > 1 Then
      Load txtObservaciones(x)
      Load txtTotal(x)
      Load txtDcto(x)
      Load chkFinalFactura(x)
    End If
  Next
  'Le pasamos como par�metro 0 a la primera query el n�mero de asistencia
  qry(1).rdoParameters(0) = NumAsistencia
  qry(1).rdoParameters(1) = NumProceso
  Set rsBusqueda = qry(1).OpenResultset
  'Se ha encontrado el c�digo de entidad...
  If Not rsBusqueda.EOF Then
    'Le pasamos como par�metro el c�digo de entidad.
    qry(2).rdoParameters(0) = rsBusqueda(0)
    Set rsBusqueda2 = qry(2).OpenResultset
    'Si ha encontrado la descripci�n, a�adimos descripci�n, c�digo y el indicador de que es la entidad
    'principal, para asignarle las l�neas sin responsable econ�mico.
    If Not rsBusqueda2.EOF Then
      Me.grdssResponsable.AddItem rsBusqueda2(0) & Chr(9) & rsBusqueda(0) & Chr(9) & "T" & Chr(9) & cont + 1 & Chr(9) & "" & Chr(9) & "E"
      Load txtObservaciones(cont + 1)
      Load txtTotal(cont + 1)
      Load txtDcto(cont + 1)
      Load chkFinalFactura(cont + 1)
    End If
  Else
  End If
  
End Sub


Sub CargarComboColetillas()
Dim MiSqL As String
Dim MiRs As rdoResultset

  MiSqL = "Select FA19DESCOL From FA1900 Where FA19INDACT = 1"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      cboColetillas.AddItem MiRs("FA19DESCOL")
      MiRs.MoveNext
    Wend
  End If
End Sub

' Mediante la funci�n GenerarTipoImprei�n, rellenaremos los tres tipos definidos por el usuario
' que vamos a utilizar en la pantalla de impresi�n de facturas, para la realizaci�n de la misma.
' Los arrays son
' ArrayCabecera -> Contiene los datos relativos a la cabecera y al pi� de la factura
' ArrayL�neas -> Contiene cada una de las l�neas de la factura
' ArrayRnfs -> Contiene cada uno de los rnfs en los que se desglosa una l�nea de factura
' El objetivo es que se correspondan mediante la siguiente uni�n:
' ArrayCabecera             ArrayLineas               ArrayRnfs           Ej:
' NFactura-------------->   NFactura  ---------->  NFactura             1 ----> 1-----> 1
'                                    NLinea-------------->  NLinea                           1-----> 1
'                                                                   NRnf                                          1
Sub GenerarTipoImpresion()
Dim strTexto As String
Dim lngPrecio As Long
Dim lngSuma As Long
Dim lngAsistencia As Long
Dim Dto As Double
Dim IntCont As Integer
Dim MiSelect As String
Dim rsPersona As rdoResultset
Dim rsCategoria As rdoResultset
Dim ContArray As Integer
Dim ContArrayLineas As Integer
Dim ContArrayRnfs As Integer
Dim ContAgruprnfs As Integer
Dim IntContLineas As Integer
Dim rnfAsignado As Boolean
Dim x As Integer
Dim SubImporte As Double
Dim Anterior As String

    'Nos posicionamos en la propuesta seleccionada dentro del objeto myFactura
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
    Set paciente = varFact.Pacientes(CodPaciente)
    Set Asistencia = paciente.Asistencias(numAsist)
    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    
    '--------------------------------------------------------------------------------------------
    'Seleccionar los Responsables econ�micos del grid de responsables econ�micos.
    'Comprobaremos que el responsable tiene l�neas de factura.
    '--------------------------------------------------------------------------------------------
    'Nos posicionamos en la primera linea del grid.
    ContArray = 1
    grdssResponsable.MoveFirst
    For IntCont = 1 To Me.grdssResponsable.Rows
        For Each NodoFACT In PropuestaFact.NodosFactura
           For Each Nodos In NodoFACT.Nodos
             'Comprobamos que la l�nea se va a facturar
             If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
               'Miramos si la l�nea coincide con el responsable.
               If Nodos.EntidadResponsable = grdssResponsable.Columns(1).Text Then
                If Nodos.EntidadResponsable <> Anterior Then
                 'A�adir los datos de ese responsable ya que tiene nodos
                    ReDim Preserve ArrayCabecera(1 To ContArray)
                    ArrayCabecera(ContArray).CodEntidad = Me.grdssResponsable.Columns(1).Text
                    ArrayCabecera(ContArray).Dto = 0 & txtDcto(CInt(grdssResponsable.Columns(3).Text))
                    'ArrayCabecera(ContArray).Fecha
                    ArrayCabecera(ContArray).NFactura = grdssResponsable.Columns(4).Text
                    ArrayCabecera(ContArray).NomEntidad = Me.grdssResponsable.Columns(0).Text
                    ArrayCabecera(ContArray).NomPaciente = paciente.Name
                    ArrayCabecera(ContArray).Observac = txtObservaciones(grdssResponsable.Columns(3).Text)
                    ArrayCabecera(ContArray).Principal = Me.grdssResponsable.Columns(2).Text
                    ArrayCabecera(ContArray).EntioPrivado = Me.grdssResponsable.Columns(5).Text
                    ArrayCabecera(ContArray).TotFactura = txtTotal(CInt(grdssResponsable.Columns(3).Text))
                    'A�adir descripci�n de la cabecera.
                    ArrayCabecera(ContArray).NumHistoria = Me.grdSSPac.Columns(0).Text
                    ArrayCabecera(ContArray).NumCaso = lngAsistencia
                    ArrayCabecera(ContArray).codConcierto = NodoFACT.codConcierto
                    ContArray = ContArray + 1
                    Anterior = Nodos.EntidadResponsable
                  End If
               End If
             End If
           Next
         Next
        'Pasamos al siguiente responsable.
        grdssResponsable.MoveNext
    Next
    
   'Una vez seleccionados los responsables econ�micos a los que hay que facturarles.
   'buscamos los datos de los responsables.
  
    For IntCont = 1 To UBound(ArrayCabecera)
      
    'Cuando es una persona hay que coger los datos de la siguiente select
      If ArrayCabecera(IntCont).Principal = "T" Then
        If ArrayCabecera(IntCont).EntioPrivado = "P" Then
          MiSelect = "Select CI22NUMDIRPRINC, CI07CODPOSTAL, CI10CALLE,CI10PORTAL," & _
                          " CI10RESTODIREC, CI10DESLOCALID,CI34DESTRATAMI,CI22DNI" & _
                          " from CI2200, CI1000,CI3400" & _
                          " where CI22NUMHISTORIA = " & ArrayCabecera(IntCont).CodEntidad & _
                          " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
                          " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
                          " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI)"

        ElseIf ArrayCabecera(IntCont).EntioPrivado = "E" Then
        End If
      Else
        MiSelect = "Select CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
                        " CI07CODPOSTAL, CI10DESLOCALID,CI34DESTRATAMI,CI22DNI" & _
                        " from CI2200, CI1000, CI3400" & _
                        " where CI2200.CI21CODPERSONA = " & ArrayCabecera(IntCont).CodEntidad & _
                        " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA " & _
                        " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
                        " AND (CI3400.CI34DESTRATAMI = CI2200.CI34DESTRATAMI)"
      End If
      Set rsPersona = objApp.rdoConnect.OpenResultset(MiSelect)
      If Not rsPersona.EOF Then
        ArrayCabecera(IntCont).DNI = rsPersona("CI22DNI") & ""
        ArrayCabecera(IntCont).Tratamiento = rsPersona("CI34DESTRATAMI")
        ArrayCabecera(IntCont).Direccion = rsPersona("CI10CALLE") & " " & rsPersona("CI10PORTAL") & rsPersona("ci10RESTODIREC")
        ArrayCabecera(IntCont).CPPoblac = rsPersona("CI07CODPOSTAL") & " " & rsPersona("CI10DESLOCALID")
      End If
    Next
    '--------------------------------------------------------------------------------------------
    ' Introducimos en el array de l�neas de factura todas las l�neas de factura que
    ' se corresponden con ese responsable econ�mico.
    ' Dentro de cada una de las l�neas de factura introducimos en el array de rnfs
    ' los rnfs que se corresponden con esa l�nea de factura.
    '--------------------------------------------------------------------------------------------
    
    'Iniciamos el contador de l�neas de la factura
    IntContLineas = 1
    ContArrayLineas = 1
    ContArrayRnfs = 1
    For IntCont = 1 To UBound(ArrayCabecera)
      For Each NodoFACT In PropuestaFact.NodosFactura
        For Each Nodos In NodoFACT.Nodos
          'Si es una l�nea que se debe mostrar o facturar
          If Nodos.EntidadResponsable = ArrayCabecera(IntCont).CodEntidad Then
            If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
              If ArrayCabecera(IntCont).Principal = "T" Then
                'En el caso de que sea el responsable econ�mico principal de la asistencia, le asignaremos
                'los que tengan ese responsable y los que no tengan ning�n responsable econ�mico
                If Nodos.EntidadResponsable = ArrayCabecera(IntCont).CodEntidad Or Nodos.EntidadResponsable = "" Then
                  'A�adimos la l�nea de factura
                  ReDim Preserve ArrayLineas(1 To ContArrayLineas)
                  ArrayLineas(ContArrayLineas).NFactura = IntCont 'N� de �ndice en el array de cabecera
                  ArrayLineas(ContArrayLineas).NLinea = IntContLineas 'N� de indice en el array de l�neas
                  ArrayLineas(ContArrayLineas).Descripcion = Nodos.Name 'Descripci�n de la l�nea
                  Dto = Nodos.LineaFact.Dto + Nodos.LineaFact.DescuentoManual
                  ArrayLineas(ContArrayLineas).Importe = Nodos.LineaFact.TotalNoDescontable + Nodos.LineaFact.Total
                  'Inicializamos el array que va a contener los RNFs agrupados
                  ReDim ArrayContador(0 To 0)
                  ContAgruprnfs = 1
                  For Each rnf In Nodos.RNFs
                    'Realizamos la agrupaci�n de los rnfs que cuelgan de ella
                    If UBound(ArrayContador) = 0 Then
                      ReDim ArrayContador(1 To ContAgruprnfs)
                      qry(3).rdoParameters(0) = rnf.CodCateg
                        Set rsCategoria = qry(3).OpenResultset
                        ArrayContador(ContAgruprnfs).NumFact = IntCont
                        ArrayContador(ContAgruprnfs).NumLinea = IntContLineas
                        ArrayContador(ContAgruprnfs).Codigo = rnf.CodCateg
                        ArrayContador(ContAgruprnfs).Desglose = rnf.Desglose
                        If Not rsCategoria.EOF Then
                          ArrayContador(ContAgruprnfs).Descrip = rsCategoria("FA05DESIG")
                        End If
                        ArrayContador(ContAgruprnfs).Cantidad = rnf.Cantidad
                    Else
                      rnfAsignado = False
                      For x = 1 To UBound(ArrayContador)
                        If ArrayContador(x).Codigo = rnf.CodCateg Then
                          If ArrayContador(ContAgruprnfs).NumFact = IntCont And ArrayContador(ContAgruprnfs).NumLinea = IntContLineas Then
                            ArrayContador(x).Cantidad = ArrayContador(x).Cantidad + rnf.Cantidad
                            rnfAsignado = True
                            Exit For
                          End If
                        End If
                      Next
                      If rnfAsignado = False Then
                        ContAgruprnfs = ContAgruprnfs + 1
                        ReDim Preserve ArrayContador(1 To ContAgruprnfs)
                        qry(3).rdoParameters(0) = rnf.CodCateg
                        Set rsCategoria = qry(3).OpenResultset
                        ArrayContador(ContAgruprnfs).NumFact = IntCont
                        ArrayContador(ContAgruprnfs).NumLinea = IntContLineas
                        ArrayContador(ContAgruprnfs).Codigo = rnf.CodCateg
                        ArrayContador(ContAgruprnfs).Desglose = rnf.Desglose
                        If Not rsCategoria.EOF Then
                          ArrayContador(ContAgruprnfs).Descrip = rsCategoria("FA05DESIG")
                        End If
                        ArrayContador(ContAgruprnfs).Cantidad = rnf.Cantidad
                      End If
                    End If
                  Next
                  'Anadimos los rnfs agrupados al tipo de los arrays
                  For x = 1 To UBound(ArrayContador)
                    ContArrayRnfs = ContArrayRnfs + 1
                    If ContArrayRnfs = 1 Then
                      ReDim arrayRNFs(1 To 1)
                    Else
                      ReDim Preserve arrayRNFs(1 To ContArrayRnfs)
                    End If
                    arrayRNFs(ContArrayRnfs).NFactura = ArrayContador(x).NumFact
                    arrayRNFs(ContArrayRnfs).NLinea = ArrayContador(x).NumLinea
                    arrayRNFs(ContArrayRnfs).CodCateg = ArrayContador(x).Codigo
                    arrayRNFs(ContArrayRnfs).Descripcion = ArrayContador(x).Descrip
                    arrayRNFs(ContArrayRnfs).Cantidad = ArrayContador(x).Cantidad
                    arrayRNFs(ContArrayRnfs).Desglose = ArrayContador(x).Desglose
                  Next
                  ContArrayLineas = ContArrayLineas + 1
                  IntContLineas = IntContLineas + 1
                End If
              Else
                If Nodos.EntidadResponsable = ArrayCabecera(IntCont).CodEntidad Then
                  'A�adimos la l�nea de factura
                  ReDim Preserve ArrayLineas(1 To ContArrayLineas)
                  ArrayLineas(ContArrayLineas).NFactura = IntCont 'N� de �ndice en el array de cabecera.
                  ArrayLineas(ContArrayLineas).NLinea = IntContLineas 'N� de �ndice en el array de l�neas
                  ArrayLineas(ContArrayLineas).Descripcion = Nodos.Name 'Descripci�n de la l�nea
                  Dto = Nodos.LineaFact.Dto + Nodos.LineaFact.DescuentoManual
                  ArrayLineas(ContArrayLineas).Importe = Nodos.LineaFact.TotalNoDescontable + Nodos.LineaFact.Total
                  For Each rnf In Nodos.RNFs
                     'A�adimos los rnfs que cuelgan de ella
                    If UBound(ArrayContador) = 0 Then
                      ReDim ArrayContador(1 To ContAgruprnfs)
                      qry(3).rdoParameters(0) = rnf.CodCateg
                        Set rsCategoria = qry(3).OpenResultset
                        ArrayContador(ContAgruprnfs).Codigo = rnf.CodCateg
                        If Not rsCategoria.EOF Then
                          ArrayContador(ContAgruprnfs).Descrip = rsCategoria("FA05DESIG")
                        End If
                        ArrayContador(ContAgruprnfs).Cantidad = rnf.Cantidad
                        ArrayContador(ContAgruprnfs).Desglose = rnf.Desglose
                    Else
                      rnfAsignado = False
                      For x = 1 To UBound(ArrayContador)
                        If ArrayContador(x).Codigo = rnf.CodCateg Then
                          ArrayContador(x).Cantidad = ArrayContador(x).Cantidad = rnf.Cantidad
                          rnfAsignado = True
                          Exit For
                        End If
                      Next
                      If rnfAsignado = False Then
                        ContAgruprnfs = ContAgruprnfs + 1
                        ReDim Preserve ArrayContador(1 To ContAgruprnfs)
                        qry(3).rdoParameters(0) = rnf.CodCateg
                        Set rsCategoria = qry(3).OpenResultset
                        ArrayContador(ContAgruprnfs).Codigo = rnf.CodCateg
                        If Not rsCategoria.EOF Then
                          ArrayContador(ContAgruprnfs).Descrip = rsCategoria("FA05DESIG")
                        End If
                        ArrayContador(ContAgruprnfs).Cantidad = rnf.Cantidad
                        ArrayContador(ContAgruprnfs).Desglose = rnf.Desglose
                      End If
                    End If
                  Next
                  'Anadimos los rnfs agrupados al tipo de los arrays
                  For x = 1 To UBound(ArrayContador)
                    If ArrayContador(x).NumFact = IntCont And ArrayContador(x).NumLinea = IntContLineas Then
                      ContArrayRnfs = ContArrayRnfs + 1
                      If ContArrayRnfs = 1 Then
                        ReDim arrayRNFs(1 To 1)
                      Else
                        ReDim Preserve arrayRNFs(1 To ContArrayRnfs)
                      End If
                      arrayRNFs(ContArrayRnfs).NFactura = ArrayContador(x).NumFact
                      arrayRNFs(ContArrayRnfs).NLinea = ArrayContador(x).NumLinea
                      arrayRNFs(ContArrayRnfs).CodCateg = ArrayContador(x).Codigo
                      arrayRNFs(ContArrayRnfs).Descripcion = ArrayContador(x).Descrip
                      arrayRNFs(ContArrayRnfs).Cantidad = ArrayContador(x).Cantidad
                      arrayRNFs(ContArrayRnfs).Desglose = ArrayContador(x).Desglose
                    End If
                  Next
                  ContArrayLineas = ContArrayLineas + 1
                  IntContLineas = IntContLineas + 1
                End If
'                lngPrecio = Nodos.PrecioRef - ((Nodos.PrecioRef * Dto) / 100)
                'A�adir datos
                lngSuma = lngSuma + Nodos.LineaFact.TotalNoDescontable + Nodos.LineaFact.Total
              End If
            End If
          End If
        Next
      Next
      IntContLineas = 1
    Next
    
    

End Sub

' Mediante el procedimiento LlenadoTiposImpresi�n recogeremos los datos de determinadas facturas
' y los introduciremos en los tipos de impresi�n definidos para el efecto.
Private Sub LlenadoTiposImpresion()
Dim rsCabecera As rdoResultset
Dim rsLineas As rdoResultset
Dim rsRnfs As rdoResultset
Dim rsDireccion As rdoResultset
Dim rsDescripcion As rdoResultset
Dim intContCabecera As Integer
Dim IntContLineas As Integer
Dim intContRnfs As Integer
Dim Acumulado As Boolean
Dim PrimerRNF As Boolean
Dim MiSqL As String
Dim x As Integer
Dim Y As Integer
  grdssResponsable.MoveFirst
  'Recorremos las distintas filas del grid para introducir los datos
  For x = 1 To grdssResponsable.Rows
    'Se ha introducido un n�mero de factura
    If grdssResponsable.Columns(1).Text <> "" Then
      qrys(1).rdoParameters(0) = Me.grdssResponsable.Columns(4).Text  'N� de la factura introducido
      Set rsCabecera = qrys(1).OpenResultset
      If Not rsCabecera.EOF Then
        '------------------------------------------------------------------------------------------
        ' DATOS DE LA CABECERA DE LA FACTURA
        '------------------------------------------------------------------------------------------
        'Llenamos los datos referidos a la cabecera
        intContCabecera = intContCabecera + 1
        ReDim Preserve ArrayCabecera(1 To intContCabecera)
        'Buscamos los datos relativos a la entidad (si la hubiera)
        If Not IsNull(rsCabecera("CI13CODENTIDAD")) Then
          'Seleccionamos los datos de una Entidad que sea responsable econ�mico
          ArrayCabecera(intContCabecera).CodEntidad = rsCabecera("CI13CODENTIDAD")
          'Averiguamos cual es el Codigo de Persona que se relaciona con esa entidad
          qrys(6).rdoParameters(0) = rsCabecera("CI13CODENTIDAD")
          Set rsDescripcion = qrys(6).OpenResultset
          If Not rsDescripcion.EOF Then
            'Asignamos la direcci�n y Raz�n social de la Entidad.
            qrys(7).rdoParameters(0) = rsDescripcion("CI21CODPERSONA")
            Set rsDireccion = qrys(7).OpenResultset
            If Not rsDireccion.EOF Then
              ArrayCabecera(intContCabecera).NomEntidad = rsDireccion("CI23RAZONSOCIAL")
              ArrayCabecera(intContCabecera).CPPoblac = rsDireccion("CI07CODPOSTAL") & " " & rsDireccion("CI10DESLOCALID")
              ArrayCabecera(intContCabecera).Direccion = rsDireccion("CI10CALLE") & " " & rsDireccion("CI10PORTAL") & _
                                                                                rsDireccion("CI10RESTODIREC")
            End If
          End If
          ArrayCabecera(intContCabecera).NomEntidad = ""
        Else
          'Si no tiene una entidad responsable los dejamos en blanco
          ArrayCabecera(intContCabecera).CodEntidad = ""
          ArrayCabecera(intContCabecera).NomEntidad = ""
        End If
        'Buscamos los datos relativos al paciente
        qrys(4).rdoParameters(0) = rsCabecera("CI21CODPERSONA")
        Set rsDireccion = qrys(4).OpenResultset
        If Not rsDireccion.EOF Then
          'Tratamiento que se le da al paciente (Sr. D., Sra. D�a., Srta.)
          ArrayCabecera(intContCabecera).Tratamiento = rsDireccion("CI34DESTRATAMI")
          'Nombre y apellidos del paciente
          ArrayCabecera(intContCabecera).NomPaciente = rsDireccion("CI22NOMBRE") & " " & rsDireccion("CI22PRIAPEL") & " " & _
                                                                                  rsDireccion("CI22SEGAPEL")
          'Si existe una entidad responsable de esa factura no asignamos la direcci�n del paciente.
          If ArrayCabecera(intContCabecera).NomEntidad = "" Then
            'Direcci�n del Paciente
            ArrayCabecera(intContCabecera).NomEntidad = ArrayCabecera(intContCabecera).NomPaciente
            ArrayCabecera(intContCabecera).CPPoblac = rsDireccion("CI07CODPOSTAL") & " " & rsDireccion("CI10DESLOCALID")
            ArrayCabecera(intContCabecera).Direccion = rsDireccion("CI10CALLE") & " " & rsDireccion("CI10PORTAL") & _
                                                                               rsDireccion("CI10RESTODIREC")
            ArrayCabecera(intContCabecera).DNI = rsDireccion("CI22DNI") & ""
          End If
        End If
        ArrayCabecera(intContCabecera).Dto = rsCabecera("FA04DESCUENTO") 'Dto aplicado
        ArrayCabecera(intContCabecera).Fecha = Format(rsCabecera("FA04FECFACTURA"), "DD/MM/YYYY") 'Fecha
        ArrayCabecera(intContCabecera).NFactura = rsCabecera("FA04NUMFACT") 'Numero de factura
        ArrayCabecera(intContCabecera).Observac = rsCabecera("FA04OBSERV") & "" 'Observaciones
        ArrayCabecera(intContCabecera).Descrip = rsCabecera("FA04DESCRIP") & ""
        ArrayCabecera(intContCabecera).NumHistoria = 0 & rsDireccion("CI22NUMHISTORIA")
        ArrayCabecera(intContCabecera).NumCaso = rsCabecera("AD01CODASISTENCI")
        ArrayCabecera(intContCabecera).TotFactura = rsCabecera("FA04CANTFACT") 'Total Facturado
        ArrayCabecera(intContCabecera).codConcierto = 0 & rsCabecera("FA09CODNODOCONC") 'C�digo de concierto
        'Rellenamos los parametros que nos van a servir para buscar las lineas de esa factura
        qrys(2).rdoParameters(0) = rsCabecera("FA04CODFACT") 'C�digo de la factura
        Set rsLineas = qrys(2).OpenResultset
        If Not rsLineas.EOF Then
          '----------------------------------------------------------------------------------------
          ' DATOS DE LAS L�NEAS DE LA FACTURA
          '----------------------------------------------------------------------------------------
          'rsLineas.MoveFirst
          While Not rsLineas.EOF
            'Llenamos los datos referidos a cada l�nea de la cabecera
            IntContLineas = IntContLineas + 1
            ReDim Preserve ArrayLineas(1 To IntContLineas)
            ArrayLineas(IntContLineas).NFactura = intContCabecera 'Contador L�neas de cabecera.
            ArrayLineas(IntContLineas).NLinea = IntContLineas 'Contador l�neas de factura.
            ArrayLineas(IntContLineas).Descripcion = rsLineas("FA16DESCRIP") 'Descripci�n de la l�nea
            ArrayLineas(IntContLineas).Importe = rsLineas("FA16IMPORTE") 'Importe total de la l�nea (dto incluido)
            'Rellenamos los par�metros que nos van a servir para buscar los RNFs de esa factura
            qrys(3).rdoParameters(0) = rsCabecera("FA04CODFACT")   'N� de la factura
            qrys(3).rdoParameters(1) = rsLineas("FA16NUMLINEA")       'N� de l�nea
            PrimerRNF = True
            Set rsRnfs = qrys(3).OpenResultset
            If Not rsRnfs.EOF Then
              '------------------------------------------------------------------------------------
              ' DATOS DE LOS RNFS DE LAS L�NEAS DE FACTURA
              '------------------------------------------------------------------------------------
              'rsRnfs.MoveFirst
              While Not rsRnfs.EOF
                Acumulado = False
                If PrimerRNF Then
                  PrimerRNF = False
                Else
                  For Y = 1 To UBound(arrayRNFs)
                    'El rnf existe en el array, por lo cual lo agrupamos.
                    If arrayRNFs(Y).NFactura = intContCabecera And arrayRNFs(Y).NLinea = IntContLineas And arrayRNFs(Y).CodCateg = rsRnfs("FA05CODCATEG") Then
                      arrayRNFs(Y).Cantidad = arrayRNFs(Y).Cantidad + rsRnfs("FA03CANTIDAD")
                      Acumulado = True
                    End If
                  Next
                End If
                'No existe el RNF en el array y lo introducimos
                If Acumulado = False Then
                  intContRnfs = intContRnfs + 1
                  ReDim Preserve arrayRNFs(1 To intContRnfs)
                  'Llenamos los datos referidos a cada rnf de la l�nea
                  arrayRNFs(intContRnfs).NFactura = intContCabecera 'Contador l�neas de cabecera.
                  arrayRNFs(intContRnfs).CodCateg = rsRnfs("FA05CODCATEG")
                  arrayRNFs(intContRnfs).NLinea = IntContLineas 'Contador l�neas de factura.
                  arrayRNFs(intContRnfs).Cantidad = rsRnfs("FA03CANTIDAD") 'Cantidad
                  If rsRnfs("FA03INDDESGLOSE") = 1 Then
                    arrayRNFs(intContRnfs).Desglose = True
                  Else
                    arrayRNFs(intContRnfs).Desglose = False
                  End If
                  MiSqL = "Select  FA15DESCRIPCION from FA1500 Where FA15CODATRIB = " & rsRnfs("FA15CODATRIB")
                  Set rsDescripcion = objApp.rdoConnect.OpenResultset(MiSqL, 3)
                  If Not rsDescripcion.EOF Then
                    If rsDescripcion("FA15DESCRIPCION") <> "" Then
                      arrayRNFs(intContRnfs).Descripcion = rsDescripcion("FA15DESCRIPCION")
                    Else
                      qrys(5).rdoParameters(0) = rsRnfs("FA05CODCATEG")
                      Set rsDescripcion = qrys(5).OpenResultset
                      If Not rsDescripcion.EOF Then
                        arrayRNFs(intContRnfs).Descripcion = rsDescripcion("FA05DESIG")
                      Else
                        arrayRNFs(intContRnfs).Descripcion = ""
                      End If
                    End If
                  End If
                End If
                rsRnfs.MoveNext
              Wend
            End If
            rsLineas.MoveNext
          Wend
        End If
        Else
          'No existen l�neas en la factura.
          MsgBox "La factura " & grdssResponsable.Columns(4).Text & " no tiene l�neas.", vbExclamation + vbOKOnly, "Atenci�n"
        End If
      Else
        'No existe la factura en la BD.
        'MsgBox "El n�mero de factura " & grdFacturas.Columns(0).Text & " no existe en la BD", vbExclamation + vbOKOnly, "Atenci�n"
      End If
    'End If
    grdssResponsable.MoveNext
  Next
        
      
End Sub

' Mediante la funci�n GuardarFactura rellenaremos los ficheros FA0400 (cabeceras de factura) y
' FA1600 (L�neas de factura)
'
Sub GuardarFactura()
Dim CodFactura As Long
Dim Dto As Double
Dim IntContLineas As Integer
Dim Facturada As Boolean
Dim SubImporte As Double
Dim IntCont As Integer
Dim rsPaciente As rdoResultset
Dim IntPos As Integer

  On Error GoTo ErrorenGrabacion
  
  'Abrimos una transacci�n para tratar toda la inserci�n de las facturas.
  objApp.BeginTrans
    
    'Nos posicionamos en la propuesta seleccionada dentro del objeto myFactura
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
    Set paciente = varFact.Pacientes(CodPaciente)
    Set Asistencia = paciente.Asistencias(numAsist)
    Asistencia.Facturado = True
    IntPos = InStr(1, Asistencia.Codigo, "-")
    lngProceso = CLng(Left(Asistencia.Codigo, IntPos - 1))
    lngAsistencia = CLng(Right(Asistencia.Codigo, Len(Asistencia.Codigo) - IntPos))
    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    
    '--------------------------------------------------------------------------------------------
    'Seleccionar los Responsables econ�micos del grid de responsables econ�micos.
    'Comprobaremos que el responsable tiene l�neas de factura.
    'En caso afirmativo a�adiremos una nueva factura con ese responsable.
    '--------------------------------------------------------------------------------------------
    'Nos posicionamos en la primera linea del grid.
    grdssResponsable.MoveFirst
    For IntCont = 1 To Me.grdssResponsable.Rows
      'Inicializamos la varible que nos indica si la factura est� a�adida a la BD.
        Facturada = False
        IntContLineas = 1
        For Each NodoFACT In PropuestaFact.NodosFactura
          For Each Nodos In NodoFACT.Nodos
            'Comprobamos que la l�nea se va a facturar
            If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
              'Comprobamos si es el responsable principal, en ese caso le asignaremos tambi�n los que no tienen R.E.
              If grdssResponsable.Columns(2).Text = "T" Then
                'Responsable Principal
                If Nodos.EntidadResponsable = grdssResponsable.Columns(1).Text Or Nodos.EntidadResponsable = "" Then
                  If Facturada = False Then
                    'A�adiremos la factura
                    CodFactura = fNextClave("FA04CODFACT", "FA0400")
                    qry(4).rdoParameters(0) = CodFactura 'Secuencia
                    qry(4).rdoParameters(1) = "13/" & CodFactura
                    grdssResponsable.Columns(4).Text = "13/" & CodFactura
                    qry(7).rdoParameters(0) = paciente.Codigo
                    Set rsPaciente = qry(7).OpenResultset
                    If Not rsPaciente.EOF Then
                      qry(4).rdoParameters(2) = rsPaciente(0) 'Paciente
                    Else
                      qry(4).rdoParameters(2) = Null
                    End If
                    If grdssResponsable.Columns(5).Text = "E" Then
                      qry(4).rdoParameters(3) = Nodos.EntidadResponsable 'Entidad
                    Else
                      qry(4).rdoParameters(3) = Null
                    End If
                    If Trim(Me.cboColetillas.Text) = "" Then
                      qry(4).rdoParameters(4) = Null
                    Else
                      qry(4).rdoParameters(4) = Trim(Me.cboColetillas.Text) 'Descripcion
                    End If
                    qry(4).rdoParameters(5) = txtTotal(grdssResponsable.Columns(3).Text) 'Cantidad Facturada
                    qry(4).rdoParameters(6) = 0 & txtDcto(grdssResponsable.Columns(3).Text) 'Dto aplicado
                    qry(4).rdoParameters(7) = lngAsistencia 'N�mero de la asistencia
                    If Trim(txtObservaciones(grdssResponsable.Columns(3).Text)) = "" Then
                      qry(4).rdoParameters(8) = Null
                    Else
                      qry(4).rdoParameters(8) = txtObservaciones(grdssResponsable.Columns(3).Text) 'Observaciones
                    End If
                    qry(4).rdoParameters(9) = NodoFACT.codConcierto
                    qry(4).rdoParameters(10) = lngProceso
                    qry(4).Execute
                    Facturada = True
                  End If
                  qry(5).rdoParameters(0) = CodFactura 'C�digo de la factura
                  qry(5).rdoParameters(1) = IntContLineas 'N� de L�nea
                  If Not IsNull(Nodos.Codigo) Then
                    qry(5).rdoParameters(2) = Nodos.Codigo 'C�digo de la categor�a (si tiene)
                  Else
                    qry(5).rdoParameters(2) = Null
                  End If
                  If Nodos.Descripcion <> "" Then
                    qry(5).rdoParameters(3) = Nodos.Descripcion
                  Else
                    qry(5).rdoParameters(3) = Nodos.Name 'Descripci�n
                  End If
                  qry(5).rdoParameters(4) = Null '?????????????????????????????
                  Dto = Nodos.LineaFact.Dto + Nodos.LineaFact.DescuentoManual
                  qry(5).rdoParameters(5) = Nodos.LineaFact.Precio + Nodos.LineaFact.PrecioNoDescontable
                  qry(5).rdoParameters(6) = Dto 'Dto de la l�nea incluido redondeo
                  qry(5).rdoParameters(7) = Nodos.LineaFact.Total + Nodos.LineaFact.TotalNoDescontable
                  qry(5).rdoParameters(8) = Nodos.OrdImp
                  qry(5).Execute
                  For Each rnf In Nodos.RNFs
                    'Asignamos a los rnfs de la FA0300, el n�mero de la factura y la l�nea
                    qry(6).rdoParameters(0) = CodFactura
                    qry(6).rdoParameters(1) = IntContLineas
                    If rnf.Desglose = True Then
                      qry(6).rdoParameters(2) = 1
                    Else
                      qry(6).rdoParameters(2) = 0
                    End If
                    qry(6).rdoParameters(3) = rnf.CodAtrib
                    qry(6).rdoParameters(4) = rnf.CodCateg
                    qry(6).rdoParameters(5) = lngAsistencia
                    qry(6).rdoParameters(6) = lngProceso
                    qry(6).Execute
                  Next
                  IntContLineas = IntContLineas + 1
                End If
              Else
                'Responsable a�adido
                If Nodos.EntidadResponsable = grdssResponsable.Columns(1).Text Then
                  If Facturada = False Then
                    'A�adiremos la factura
                    CodFactura = fNextClave("FA04CODFACT", "FA0400")
                    qry(4).rdoParameters(0) = CodFactura 'Secuencia
                    qry(4).rdoParameters(1) = "13/" & CodFactura 'N� de Factura
                    grdssResponsable.Columns(5).Text = "13/" & CodFactura
                    qry(4).rdoParameters(2) = paciente.Codigo 'Paciente
                    qry(4).rdoParameters(3) = Null 'Nodos.EntidadResponsable 'Entidad
                    qry(4).rdoParameters(4) = txtObservaciones(grdssResponsable.Columns(4).Text) 'Observaciones
                    qry(4).rdoParameters(5) = txtTotal(grdssResponsable.Columns(4).Text) 'Cantidad Facturada
                    qry(4).rdoParameters(6) = 0 & txtDcto(grdssResponsable.Columns(4).Text) 'Dto Aplicado
                    qry(4).rdoParameters(7) = lngAsistencia 'N�mero de la asistencia
                    If Trim(txtObservaciones(grdssResponsable.Columns(3).Text)) = "" Then
                      qry(4).rdoParameters(8) = Null
                    Else
                      qry(4).rdoParameters(8) = txtObservaciones(grdssResponsable.Columns(3).Text) 'Observaciones
                    End If
                    qry(4).Execute
                    Facturada = True
                  End If
                  'A�adiremos esa l�nea a la factura
                  qry(5).rdoParameters(0) = CodFactura 'C�digo de la factura
                  qry(5).rdoParameters(1) = IntContLineas 'N� de L�nea
                  qry(5).rdoParameters(2) = Null 'Nodo o categor�a facturada
                  qry(5).rdoParameters(3) = Nodos.Name 'Descripci�n
                  qry(5).rdoParameters(4) = Null '?????????????????????????????
                  Dto = Nodos.LineaFact.Dto + Nodos.LineaFact.DescuentoManual
                  qry(5).rdoParameters(5) = Nodos.LineaFact.Precio + Nodos.LineaFact.PrecioNoDescontable
                  qry(5).rdoParameters(6) = 0 & Dto 'Dto
                  qry(5).rdoParameters(7) = Nodos.LineaFact.Total + Nodos.LineaFact.TotalNoDescontable
                  qry(5).rdoParameters(8) = Nodos.OrdImp
                  qry(5).Execute
                  For Each rnf In Nodos.RNFs
                    'Asignamos a los rnfs de la FA0300, el n�mero de la factura y la l�nea
                    qry(6).rdoParameters(0) = CodFactura
                    qry(6).rdoParameters(1) = IntContLineas
                    If rnf.Desglose = True Then
                      qry(6).rdoParameters(2) = 1
                    Else
                      qry(6).rdoParameters(2) = 0
                    End If
                    qry(6).rdoParameters(3) = rnf.CodAtrib
                    qry(6).rdoParameters(4) = rnf.CodCateg
                    qry(6).rdoParameters(5) = Asistencia.Codigo
                    qry(6).Execute
                  Next
                  IntContLineas = IntContLineas + 1
                End If
              End If
            End If
          Next
        Next
        'Pasamos al siguiente responsable.
        grdssResponsable.MoveNext
    Next
    
  'Se ha grabado todo cerramos la transacci�n
  objApp.CommitTrans
Exit Sub

ErrorenGrabacion:
  'Se ha producido un error en la grabaci�n, anulamos la transacci�n
  objApp.RollbackTrans
  MsgBox "Se ha producido un error en la grabaci�n.", vbOKOnly + vbCritical, "Atenci�n"
End Sub


Sub IniciarQRY()
Dim MiSqL As String
Dim x As Integer
  
  ''Queries relacionadas con la obtenci�n de una entidad a partir del n�mero de asistencia.
  'Seleccionar el c�digo de entidad a partir del n�mero de asistencia.
  MiSqL = "SELECT CI13CODENTIDAD FROM AD1100 WHERE AD01CODASISTENCI = ? " & _
               "AND AD07CODPROCESO = ? AND AD11FECFIN IS NULL"
  qry(1).SQL = MiSqL
  'Seleccionar la descripci�n de la entidad
  MiSqL = "SELECT CI13DESENTIDAD FROM CI1300 WHERE CI13CODENTIDAD = ?"
  qry(2).SQL = MiSqL
  
  'Seleccionar la descripci�n de una categor�a.
  MiSqL = "Select FA05DESIG from FA0500 Where FA05CODCATEG = ?"
  qry(3).SQL = MiSqL
  
  'Insertar una cabecera de factura en FA0400
  MiSqL = "Insert Into FA0400 (FA04CODFACT,FA04NUMFACT,CI21CODPERSONA,CI13CODENTIDAD," & _
              "FA04DESCRIP,FA04CANTFACT,FA04DESCUENTO,AD01CODASISTENCI,FA04OBSERV," & _
              "FA09CODNODOCONC,AD07CODPROCESO) Values (?,?,?,?,?,?,?,?,?,?,?)"
  qry(4).SQL = MiSqL
  'Isertar una l�nea de factura en FA1600
  MiSqL = "Insert into FA1600 (FA04CODFACT,FA16NUMLINEA,FA14CODNODOCONC,FA16DESCRIP," & _
              "FA16PRCTGRNF,FA16PRECIO,FA16DCTO,FA16IMPORTE,FA16ORDFACT) Values (?,?,?,?,?,?,?,?,?)"
  qry(5).SQL = MiSqL
  'Asignar a FA0300 el numero de la factura y el n�mero de l�nea
  MiSqL = "Update FA0300 set FA04NUMFACT = ?, FA16NUMLINEA = ?, FA03INDDESGLOSE = ?, FA15CODATRIB = ? " & _
              "Where FA05CODCATEG = ? AND AD01CODASISTENCI = ? AND AD07CODPROCESO = ?"
  qry(6).SQL = MiSqL
  
  MiSqL = "Select CI21CODPERSONA From CI2200 Where CI22NUMHISTORIA = ?"
  qry(7).SQL = MiSqL
  'Activamos las querys.
  For x = 1 To 7
    Set qrys(x).ActiveConnection = objApp.rdoConnect
    qrys(x).Prepared = True
  Next
End Sub
Public Function pFactura(objFactura As Factura, Prop As String) As Factura
Dim Posicion As Integer

    Set varFact = objFactura
    'Buscamos el / para separar el n�mero de asistencia y la propuesta seleccionada
    Posicion = InStr(1, Prop, "/")
    If Posicion <> 0 Then
      'Propuesta seleccionada
      Propuesta = Right(Prop, Len(Prop) - Posicion)
      'N�mero de asistencia.
      numAsist = Left(Prop, Posicion - 1)
      Posicion = InStr(1, numAsist, "-")
      CodPaciente = Left(Prop, Posicion - 1)
      numAsist = Right(numAsist, Len(numAsist) - Posicion)
    End If
    Load frmEditFactura
      'Set frmSeleccion.varFact = objFactura
      frmEditFactura.Show (vbModal)
            
    Set objFactura = varFact
      Unload frmEditFactura
End Function

Sub IniciarQRYs()
Dim MiSqL As String
Dim x As Integer
  
  'Seleccionar los datos de una factura en base al n� de factura
  MiSqL = "Select * from FA0400 Where FA04NUMFACT = ?"
  qrys(1).SQL = MiSqL
  
  'Seleccionamos las l�neas de una factura en base al c�digo de factura
  MiSqL = "Select * from FA1600 Where FA04CODFACT = ? ORDER BY FA16ORDFACT"
  qrys(2).SQL = MiSqL
  
  'Seleccionamos los RNFs de una l�nea de factura en base al n� de factura y al n� de l�nea"
  MiSqL = "Select * from FA0300 Where FA04NUMFACT = ? And FA16NUMLINEA = ?"
  qrys(3).SQL = MiSqL
  
  'Seleccionamos los datos de un responsable econ�mico
  
    'CI21CODPERSONA,CI23RAZONSOCIAL,CI23NUMDIRPRINC,CI10CALLE,CI10PORTAL,CI10RESTODIREC
    'CI07CODPOSTAL,CI10DESLOCALID DONDE EL CI21CODPERSONA ESTE RECOGIDO DE CI0900 EN BASE A CI13CODENTIDAD
    
  'Seleccionamos los datos personales de una persona.
  MiSqL = " Select CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL, " & _
              " CI22NUMDIRPRINC, CI10CALLE, CI10PORTAL, CI10RESTODIREC, " & _
              " CI07CODPOSTAL, CI10DESLOCALID, CI34DESTRATAMI, CI22NUMHISTORIA,CI22DNI" & _
              " from CI2200, CI1000,CI3400 " & _
              " where CI2200.CI21CODPERSONA = ?" & _
              " AND (CI2200.CI21CODPERSONA = CI1000.CI21CODPERSONA  " & _
              " AND CI2200.CI22NUMDIRPRINC = CI1000.CI10NUMDIRECCI)" & _
              " AND (CI2200.CI34CODTRATAMI = CI3400.CI34CODTRATAMI)"
  qrys(4).SQL = MiSqL

  'Buscamos las designaci�n del RNF
  MiSqL = "Select FA05DESIG From FA0500 Where FA05CODCATEG = ?"
  qrys(5).SQL = MiSqL
  
  'Seleccionamos los datos de una Entidad que sea responsable econ�mico
  MiSqL = " Select CI21CODPERSONA From CI0900 Where CI13CODENTIDAD = ?"
  qrys(6).SQL = MiSqL
  MiSqL = " Select CI23RAZONSOCIAL, CI23NUMDIRPRINC, CI10CALLE, CI10PORTAL," & _
              " CI10RESTODIREC,CI07CODPOSTAL,CI10DESLOCALID" & _
              " From CI2300, CI1000, CI0900 " & _
              " Where CI2300.CI21CODPERSONA = ?" & _
              " And (CI2300.CI21CODPERSONA = CI1000.CI21CODERSONA " & _
              " And CI2300.CI23NUMDIRPRINC = CI1000.CI10NUMDIRECCI) " & _
  qrys(7).SQL = MiSqL
  
  'Activamos las querys.
  For x = 1 To 7
    Set qry(x).ActiveConnection = objApp.rdoConnect
    qry(x).Prepared = True
  Next
End Sub


Private Sub pCargarLineas(CodEntidad As String, Principal As Boolean)
Dim strTexto As String
Dim lngPrecio As Long
Dim lngSuma As Long
Dim Dto As Double
Dim IntCont As Integer
Dim IntContNodo As Integer
Dim intContLin As Integer
Dim Cont1, Cont2 As Integer
Dim TmpOrdImp As Integer
Dim TmpClave1, TmpClave2 As Integer
Dim IntPos As Integer
    'Iniciamos el contador del Importe total.
    lngSuma = 0
    IntCont = 1
    intContLin = 1
    IntContNodo = 1
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
    Set paciente = varFact.Pacientes(CodPaciente)
    Set Asistencia = paciente.Asistencias(numAsist)
    IntPos = InStr(1, Asistencia.Codigo, "-")
    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    For Each NodoFACT In PropuestaFact.NodosFactura
      For Each Nodos In NodoFACT.Nodos
        'Si en una l�nea que se debe mostrar o facturar.
        If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
          'Si el indicador de Entidad del grid es T.
          If IntCont = 1 Then
            ReDim ArrayOrdenacion(1 To 3, 1 To 1)
          Else
            ReDim Preserve ArrayOrdenacion(1 To 3, 1 To IntCont)
          End If
          If Nodos.OrdImp = 0 Then
            ArrayOrdenacion(1, IntCont) = 32767
            Nodos.OrdImp = 32767
          Else
            ArrayOrdenacion(1, IntCont) = Nodos.OrdImp
          End If
          ArrayOrdenacion(2, IntCont) = IntContNodo
          ArrayOrdenacion(3, IntCont) = intContLin
          IntCont = IntCont + 1
        End If
        intContLin = intContLin + 1
      Next
      IntContNodo = IntContNodo + 1
      intContLin = 1
    Next
    For Cont1 = UBound(ArrayOrdenacion, 2) To 1 Step -1
      For Cont2 = Cont1 To UBound(ArrayOrdenacion, 2) - 1
        If ArrayOrdenacion(1, Cont2) > ArrayOrdenacion(1, Cont2 + 1) Then
          'Copiar a dos variables los valores del siguiente
          TmpOrdImp = ArrayOrdenacion(1, Cont2 + 1)
          TmpClave1 = ArrayOrdenacion(2, Cont2 + 1)
          TmpClave2 = ArrayOrdenacion(3, Cont2 + 1)
          'Copiar los valores al siguiente
          ArrayOrdenacion(1, Cont2 + 1) = ArrayOrdenacion(1, Cont2)
          ArrayOrdenacion(2, Cont2 + 1) = ArrayOrdenacion(2, Cont2)
          ArrayOrdenacion(3, Cont2 + 1) = ArrayOrdenacion(3, Cont2)
          'Copiar los valores al anterior
          ArrayOrdenacion(1, Cont2) = TmpOrdImp
          ArrayOrdenacion(2, Cont2) = TmpClave1
          ArrayOrdenacion(3, Cont2) = TmpClave2
        ElseIf ArrayOrdenacion(1, Cont2) < ArrayOrdenacion(1, Cont2 + 1) Then
          Exit For
        End If
      Next
    Next
    For Cont1 = 1 To UBound(ArrayOrdenacion, 2)
      Set NodoFACT = PropuestaFact.NodosFactura(ArrayOrdenacion(2, Cont1))
      Set Nodos = NodoFACT.Nodos(ArrayOrdenacion(3, Cont1))
      If Principal = True Then
        'Le asignamos los que coincidan o est�n en blanco.
        If Nodos.EntidadResponsable = CodEntidad Or Nodos.EntidadResponsable = "" Then
          If Nodos.EntidadResponsable = "" Then
            Nodos.EntidadResponsable = CodPaciente
          End If
          If Nodos.LineaFact.DescuentoManual <> 0 Then
            Dto = Nodos.LineaFact.Dto + Nodos.LineaFact.DescuentoManual
          Else
            Dto = Nodos.LineaFact.Dto
          End If
          lngPrecio = Nodos.LineaFact.Total + Nodos.LineaFact.TotalNoDescontable
          strTexto = Nodos.Name & " (" & Nodos.LineaFact.Cantidad & ")" _
                          & Chr$(9) & Nodos.LineaFact.Precio + Nodos.LineaFact.PrecioNoDescontable _
                          & Chr$(9) & Dto _
                          & Chr$(9) & lngPrecio _
                          & Chr$(9) & Nodos.Cantidad _
                          & Chr$(9) & Nodos.Editable
          grdssFact.AddItem strTexto
          lngSuma = lngSuma + lngPrecio
        End If
      Else
        'Si es F, le asignamos s�lo los que coinciden.
        If Nodos.EntidadResponsable = CodEntidad Then
          If CStr(Nodos.LineaFact.DescuentoManual) = "" Then
            Nodos.LineaFact.DescuentoManual = 0
          End If
          Dto = Nodos.LineaFact.Dto + Nodos.LineaFact.DescuentoManual
          lngPrecio = Nodos.LineaFact.Total + Nodos.LineaFact.TotalNoDescontable
          strTexto = Nodos.Name & " (" & Nodos.LineaFact.Cantidad & ")" _
                          & Chr$(9) & Nodos.LineaFact.Precio + Nodos.LineaFact.PrecioNoDescontable _
                          & Chr$(9) & Dto _
                          & Chr$(9) & lngPrecio _
                          & Chr$(9) & Nodos.LineaFact.Cantidad _
                          & Chr$(9) & Nodos.Editable
          grdssFact.AddItem strTexto
          lngSuma = lngSuma + lngPrecio
        End If
      End If
    Next
'
    NodoFactura = 1
    If Me.grdssResponsable.Columns(3).Text = "" Then
      Me.txtTotal(1).Text = lngSuma
    Else
      Me.txtTotal(Me.grdssResponsable.Columns(3).Text).Text = lngSuma
    End If
End Sub

Function pCalcularImportes(CodEntidad As String, Principal As Boolean, NoDescont As Double) As Long
Dim strTexto As String
Dim lngPrecio As Long
Dim lngSuma As Long
Dim lngSumaNoDto As Double
    
    'Iniciamos el contador del Importe total.
    lngSuma = 0
    
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
    Set paciente = varFact.Pacientes(CodPaciente)
    Set Asistencia = paciente.Asistencias(numAsist)
    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    For Each NodoFACT In PropuestaFact.NodosFactura
      For Each Nodos In NodoFACT.Nodos
        'Si en una l�nea que se debe mostrar o facturar.
        If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
          'Si el indicador de Entidad del grid es T.
          If Principal = True Then
            'Le asignamos los que coincidan o est�n en blanco.
            If Nodos.EntidadResponsable = CodEntidad Or Nodos.EntidadResponsable = "" Then
              'Nodos.Descont = True
              If Nodos.Descont = True Then
                lngPrecio = Nodos.LineaFact.Importe - ((Nodos.LineaFact.Importe * Nodos.LineaFact.Dto) / 100)
                lngSumaNoDto = lngSumaNoDto + Nodos.LineaFact.ImporteNoDescontable
                lngSuma = lngSuma + lngPrecio
              Else
                lngSumaNoDto = lngSumaNoDto + Nodos.LineaFact.ImporteNoDescontable
              End If
            End If
          Else
            'Si es F, le asignamos s�lo los que coinciden.
            If Nodos.EntidadResponsable = CodEntidad Then
              If Nodos.Descont = True Then
                lngPrecio = Nodos.LineaFact.Importe - ((Nodos.LineaFact.Importe * Nodos.LineaFact.Dto) / 100)
                lngSumaNoDto = lngSumaNoDto + Nodos.LineaFact.ImporteNoDescontable
                lngSuma = lngSuma + lngPrecio
              End If
            End If
          End If
        End If
      Next
    Next
    pCalcularImportes = lngSuma
    NoDescont = lngSumaNoDto
End Function


Private Sub pAsignarDescuento(CodEntidad As String, DtoAplicar As Double, Principal As Boolean)
Dim strTexto As String
Dim lngPrecio As Long
Dim lngPrecioDto As Long
Dim Dto As Double
    
    'CodPaciente = CStr(Me.grdSSPac.Columns(0).Text)
    Set paciente = varFact.Pacientes(CodPaciente)
    Set Asistencia = paciente.Asistencias(numAsist)
    Set PropuestaFact = Asistencia.PropuestasFactura(Propuesta)
    For Each NodoFACT In PropuestaFact.NodosFactura
      For Each Nodos In NodoFACT.Nodos
        'Si en una l�nea que se debe mostrar o facturar.
        If Nodos.FactOblig = True Or Nodos.LinOblig = True Then
          'Si el indicador de Entidad del grid es T.
          If Principal = True Then
            'Le asignamos los que coincidan o est�n en blanco.
            If Nodos.EntidadResponsable = CodEntidad Or Nodos.EntidadResponsable = "" Then
              If Nodos.Descont = True Then
                'A la hora de aplicar el descuento habr� que comprobar que no tenga un descuento
                'por l�nea y responsable
                'En el caso de que lo tenga deberemos recalcular el dto manual en base al dto
                'Anterior que ya exist�a 20 + 4 # 24 puede ser 23,332324
                If Nodos.LineaFact.Dto = 0 Then
                  Nodos.LineaFact.DescuentoManual = DtoAplicar
                Else
                  'Calcular Importe Final
                  lngPrecio = Nodos.LineaFact.Importe - ((Nodos.LineaFact.Importe * Nodos.LineaFact.Dto) / 100)
                  lngPrecioDto = lngPrecio - ((lngPrecio * DtoAplicar) / 100)
                  Dto = Nodos.LineaFact.Importe - lngPrecioDto
                  Dto = (Dto / Nodos.LineaFact.Precio) * 100
                  Nodos.LineaFact.DescuentoManual = Dto - Nodos.LineaFact.Dto
                End If
                    'lngPrecio = Nodos.PrecioDia - ((Nodos.PrecioDia * Nodos.Descuento) / 100)
                    ' lngPrecioDto = lngPrecio - ((lngPrecio * DtoAplicar) / 100)
                    ''Recalcular % en base a final
                    'Dto = Nodos.PrecioDia - lngPrecioDto
                    'Dto = (Dto / Nodos.PrecioDia) * 100
                  
                End If
              End If
            End If
          Else
            'Si es F, le asignamos s�lo los que coinciden.
            If Nodos.EntidadResponsable = CodEntidad Then
              If Nodos.Descont = True Then
                'A la hora de aplicar el descuento habr� que comprobar que no tenga un descuento
                'por l�nea y responsable
                'En el caso de que lo tenga deberemos recalcular el dto manual en base al dto
                'Anterior que ya exist�a 20 + 4 # 24 puede ser 23,332324
                If Nodos.LineaFact.Dto = 0 Then
                  Nodos.LineaFact.DescuentoManual = DtoAplicar
                Else
                  'Calcular Importe Final
                  lngPrecio = Nodos.LineaFact.Precio - ((Nodos.LineaFact.Precio * Nodos.LineaFact.Dto) / 100)
                  lngPrecioDto = lngPrecio - ((lngPrecio * DtoAplicar) / 100)
                  Dto = Nodos.LineaFact.Precio - lngPrecioDto
                  Dto = (Dto / Nodos.LineaFact.Precio) * 100
                  Nodos.LineaFact.DescuentoManual = Dto - Nodos.LineaFact.Dto
                End If
              End If
            End If
          End If
      Next
    Next
End Sub



Private Sub pCargarPaciente()
    Dim strTexto As String
        
    'Recorremos los diferentes pacientes que hemos a�adido a la colecci�n.
    For Each paciente In varFact.Pacientes
      'A�adimos cada uno de ellos al grid de pacientes.
      strTexto = paciente.Codigo _
                  & Chr$(9) & paciente.Name _
                  & Chr$(9) & " " _
                  & Chr$(9) & paciente.Direccion _
                  & Chr$(9) & paciente.FecNacimiento
      grdSSPac.AddItem strTexto
    Next
End Sub

Private Sub chkFinalFactura_Click(Index As Integer)
  If chkFinalFactura(Index).Value = 1 Then
    txtObservaciones(Index).Text = "Nota: Si se produce alg�n nuevo cargo hasta el momento de alta " & _
                                                   "le remitiremos una nueva factura complementaria"
  Else
    txtObservaciones(Index).Text = ""
  End If
End Sub

Private Sub cmdAceptar_Click()
Dim Opcion As Integer

    Call GuardarFactura
    Call LlenadoTiposImpresion
    frm_Impresion.Show 1
    Opcion = MsgBox("�Facturar otra propuesta?", vbQuestion + vbYesNo, "Facturaci�n")
    If Opcion = vbYes Then
      ContinuarFactura = True
    ElseIf Opcion = vbNo Then
      ContinuarFactura = False
    End If
    Unload Me
End Sub


Private Sub cmdA�adir_Click()
Dim Nombre As String
  'Llamamos a la pantalla de selecci�n de un nuevo responsable econ�mico.
  Nombre = frmNuevoResponsable.pNuevoResponsable
  'Si se ha seleccionado alguno lo a�adimos al grid de responsables econ�micos.
  If Nombre <> "" Then
    Me.grdssResponsable.AddItem Nombre & Chr(9) & grdssResponsable.Rows + 1 'Nombre est� compuesto de Nombre, C�digo y Entidad (T/F)
    If grdssResponsable.Rows <> 1 Then
      Load txtObservaciones(grdssResponsable.Rows)
      Load txtTotal(grdssResponsable.Rows)
      Load txtDcto(grdssResponsable.Rows)
      Load chkFinalFactura(grdssResponsable.Rows)
    End If
    txtObservaciones(grdssResponsable.Rows).Visible = True
    txtTotal(grdssResponsable.Rows).Visible = True
    txtDcto(grdssResponsable.Rows).Visible = True
    chkFinalFactura(grdssResponsable.Rows).Visible = True
    txtObservaciones(grdssResponsable.Rows).Text = ""
    txtTotal(grdssResponsable.Rows).Text = ""
    txtDcto(grdssResponsable.Rows).Text = ""
    chkFinalFactura(grdssResponsable.Rows).Value = 0
    UltimaLinea = grdssResponsable.Rows
  End If
End Sub
Private Sub cmdConcierto_Click()
  
    Call frmConcSeleccionado.pConcSelec(varFact, CodPaciente, numAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value, ArrayOrdenacion)

End Sub


Private Sub cmdEditarLinea_Click()
Dim Posicion
Dim Designacion As String
  
  Designacion = grdssFact.Columns(0).Text
  Posicion = InStr(1, Designacion, "(") - 1
  Designacion = Left(Designacion, Posicion)
  'Comprobamos que la l�nea que queremos editar no tiene ninguna restricci�n
  If grdssFact.Columns("Franquicia").Text = "True" Then
    'Llamamos a la pantalla que utilizamos para crear nuevas l�neas.
    'Call frmNuevaLinea.pNuevaLinea(varFact, CodPaciente, NumAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value)
    Call frm_EditLinea.pEditarLinea(varFact, CodPaciente, numAsist, Propuesta, Me.grdssFact.Row + 1, grdssFact.Columns(4).Text, Designacion, grdssFact.Columns(1).Text, grdssFact.Columns(2).Text, grdssFact.Columns(3).Text)
    'Como la l�nea se a�ade desde la pantalla anterior vaciamos el grid y lo volvemos a llenar.
    Me.grdssFact.RemoveAll
    'Le indicamos si es la entidad principal para que le asigne las l�neas en blanco.
    If Me.grdssResponsable.Columns(2).Value = "T" Then
      'Las asigna
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
    Else
      'No las asigna.
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
    End If
  Else
    MsgBox "La l�nea seleccionada no puede ser editada", vbInformation + vbOKOnly, "Aviso"
  End If
End Sub

Private Sub cmdNuevaLinea_Click()
    'Llamamos a la pantalla que utilizamos para crear nuevas l�neas.
    Call frmNuevaLinea.pNuevaLinea(varFact, CodPaciente, numAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value)
    'Como la l�nea se a�ade desde la pantalla anterior vaciamos el grid y lo volvemos a llenar.
    Me.grdssFact.RemoveAll
    'Le indicamos si es la entidad principal para que le asigne las l�neas en blanco.
    If Me.grdssResponsable.Columns(2).Value = "T" Then
      'Las asigna
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
    Else
      'No las asigna.
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
    End If
End Sub

Private Sub cmdRedondeo_Click()
Dim DtoMaximo As Long
Dim strCantDto As String
Dim lngCantDto As Long
Dim dblDesto As Double
Dim NoDtable As Double
  
    NoDtable = 0
    If Me.grdssResponsable.Columns(2).Value = "T" Then
      'Las asigna
      DtoMaximo = pCalcularImportes(grdssResponsable.Columns(1).Value, True, NoDtable)
    Else
      'No las asigna.
      DtoMaximo = pCalcularImportes(grdssResponsable.Columns(1).Value, False, NoDtable)
    End If
    strCantDto = InputBox("Cantidad que desea facturar. M�ximo: " & NoDtable + DtoMaximo & " M�nimo: " & NoDtable, "Redondeo de factura", 0)
    If IsNumeric(strCantDto) Then
      If CLng(strCantDto) > NoDtable + DtoMaximo Or CLng(strCantDto) < NoDtable Then
        While CLng(strCantDto) > DtoMaximo
          strCantDto = InputBox("Cantidad que desea facturar. M�ximo: " & NoDtable + DtoMaximo & " M�nimo: " & NoDtable, "Redondeo de factura")
          If Not IsNumeric(strCantDto) Then
            strCantDto = "0"
          End If
        Wend
      End If
    Else
      strCantDto = "0"
    End If
    If strCantDto <> "0" Then
      lngCantDto = CLng(strCantDto)
      dblDesto = DtoMaximo - (lngCantDto - NoDtable)
      dblDesto = ((dblDesto / DtoMaximo) * 100)
    End If
    If Me.grdssResponsable.Columns(2).Value = "T" Then
      Call pAsignarDescuento(grdssResponsable.Columns(1).Value, dblDesto, True)
    Else
      Call pAsignarDescuento(grdssResponsable.Columns(1).Value, dblDesto, False)
    End If
    
    Me.grdssFact.RemoveAll
    'Le indicamos si es la entidad principal para que le asigne las l�neas en blanco.
    If Me.grdssResponsable.Columns(2).Value = "T" Then
      'Las asigna
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
    Else
      'No las asigna.
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
    End If

End Sub


Private Sub cmdRNF_Click()
    'Llamamos a la pantalla en la que se realiza el desglose.
    Call frm_Desglose.pDesglose(varFact, CodPaciente, numAsist, Propuesta, Me.grdssFact.Row + 1, Me.grdssResponsable.Columns(1).Value)
    'Como el movimiento en las l�neas de factura lo realizamos en la pantalla de desglose,
    'aqu� nos limitamos a llenar las l�neas.
    Me.grdssFact.RemoveAll
    'Le indicamos si es la entidad principal para que le asigne las l�neas en blanco.
    If Me.grdssResponsable.Columns(2).Value = "T" Then
      'Las asigna
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
    Else
      'No las asigna.
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
    End If
End Sub

Private Sub cmdSalir_Click()
Dim Opcion As Integer

    Opcion = MsgBox("�Facturar otra propuesta?", vbQuestion + vbYesNo, "Facturaci�n")
    If Opcion = vbYes Then
      ContinuarFactura = True
    ElseIf Opcion = vbNo Then
      ContinuarFactura = False
    End If
    Unload Me
End Sub

Private Sub Form_Load()
Dim Posicion As Integer
Dim IntPos As Integer
        
    Call IniciarQRY
    Call IniciarQRYs
    Call pCargarPaciente
    If numAsist <> "" Then
      IntPos = InStr(1, numAsist, "-")
      lngAsistencia = CLng(Left(numAsist, IntPos - 1))
      lngProceso = CLng(Right(numAsist, Len(numAsist) - IntPos))
      Call BuscarResponsableEconomico(lngAsistencia, lngProceso)
      Call pCargarLineas(CodPaciente, True)
    End If
    Call CargarComboColetillas
    txtObservaciones(1).Visible = True
    txtTotal(1).Visible = True
    txtDcto(1).Visible = True
    chkFinalFactura(1).Visible = True
    UltimaLinea = 1
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim Opcion As Integer

    Opcion = MsgBox("�Facturar otra propuesta?", vbQuestion + vbYesNo, "Facturaci�n")
    If Opcion = vbYes Then
      ContinuarFactura = True
    ElseIf Opcion = vbNo Then
      ContinuarFactura = False
    End If
    Unload Me

End Sub


Private Sub grdssFact_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
  If grdssFact.Columns("Franquicia").Text = "False" Then
    MsgBox "No se puede borrar una l�nea que implique a una franquicia", vbInformation + vbOKOnly, "Aviso"
    Cancel = 1
  End If
End Sub

Private Sub grdssFact_DblClick()
'    Call frm_Desglose.pDesglose(varFact, CodPaciente, Asist, Propuesta, Me.grdssFact.Row + 1)
'    grdssFact.RemoveAll
'    If Me.grdssResponsable.Columns(2).Value = "T" Then
'      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
'    Else
'      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
'    End If
End Sub

Private Sub grdssResponsable_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    'Llenamos el grid de l�neas de factura en base al responsable econ�mico que se corresponda
    'con la l�nea seleccionada, la selecci�n puede ser nula.
    Me.grdssFact.RemoveAll
    If Me.grdssResponsable.Columns(2).Value = "T" Then
      'Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
      txtObservaciones(UltimaLinea).Visible = False
      txtTotal(UltimaLinea).Visible = False
      txtDcto(UltimaLinea).Visible = False
      chkFinalFactura(UltimaLinea).Visible = False
      txtObservaciones(grdssResponsable.Columns(3).Text).Visible = True
      txtTotal(grdssResponsable.Columns(3).Text).Visible = True
      txtDcto(grdssResponsable.Columns(3).Text).Visible = True
      chkFinalFactura(grdssResponsable.Columns(3).Text).Visible = True
      UltimaLinea = grdssResponsable.Columns(3).Text
      Label1.Caption = grdssResponsable.Columns(0).Text
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
    Else
      'Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
      txtObservaciones(UltimaLinea).Visible = False
      txtTotal(UltimaLinea).Visible = False
      txtDcto(UltimaLinea).Visible = False
      chkFinalFactura(UltimaLinea).Visible = False
      txtObservaciones(grdssResponsable.Columns(3).Text).Visible = True
     ' MsgBox txtObservaciones(grdssResponsable.Columns(3).Text).Text & Chr(10) & Chr(13) & txtTotal(grdssResponsable.Columns(3).Text).Text & Chr(10) & Chr(13) & txtDcto(grdssResponsable.Columns(3).Text).Text
      txtTotal(grdssResponsable.Columns(3).Text).Visible = True
      txtDcto(grdssResponsable.Columns(3).Text).Visible = True
      chkFinalFactura(grdssResponsable.Columns(3).Text).Visible = True
      UltimaLinea = grdssResponsable.Columns(3).Text
      Label1.Caption = grdssResponsable.Columns(0).Text
      Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
    End If
End Sub




Private Sub txtDcto_KeyPress(Index As Integer, KeyAscii As Integer)
  KeyAscii = fValidarEnteros(KeyAscii)
End Sub

Private Sub txtDcto_LostFocus(Index As Integer)
  If txtDcto(Index) = 0 Then
    txtDcto(Index) = ""
  End If
  If Trim(txtDcto(Index)) <> "" Then
    'Call GenerarDescuento(Responsable, Cantidad)
  End If
  Me.grdssFact.RemoveAll
  If Me.grdssResponsable.Columns(2).Value = "T" Then
    Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, True)
  Else
    Call pCargarLineas(Me.grdssResponsable.Columns(1).Value, False)
  End If
End Sub




