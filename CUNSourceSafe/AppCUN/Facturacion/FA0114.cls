VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "TramoDia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Fecha As String
Public Key As String
Public Facturado As Boolean
Public TramDias As Boolean
Public TramAcum As Boolean
Public PlazoInter As Integer
Public DiasAcumulados As Integer
Public EsMismoIngreso As Boolean
