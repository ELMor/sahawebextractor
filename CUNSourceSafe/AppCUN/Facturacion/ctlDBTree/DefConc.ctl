VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.UserControl DefConc 
   ClientHeight    =   4005
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11760
   LockControls    =   -1  'True
   ScaleHeight     =   4005
   ScaleWidth      =   11760
   Begin VB.Frame fraAtributos 
      BorderStyle     =   0  'None
      Caption         =   "Atributos"
      Height          =   1665
      Left            =   45
      TabIndex        =   1
      Top             =   2295
      Width           =   11655
      Begin VB.Frame frmAtrib 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   1230
         Index           =   3
         Left            =   50
         TabIndex        =   44
         Top             =   360
         Visible         =   0   'False
         Width           =   11525
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Los tramos se aplican por dias"
            Height          =   195
            Index           =   12
            Left            =   7245
            TabIndex        =   46
            Top             =   270
            Width           =   4245
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Los tramos se acumulan entre los diferentes nodos"
            Height          =   195
            Index           =   11
            Left            =   7245
            TabIndex        =   47
            Top             =   585
            Width           =   4245
         End
         Begin VB.CheckBox chkBorrado 
            Alignment       =   1  'Right Justify
            Caption         =   "Ha Borrado alguna linea ?"
            Enabled         =   0   'False
            Height          =   285
            Left            =   9135
            TabIndex        =   50
            Top             =   -45
            Visible         =   0   'False
            Width           =   2265
         End
         Begin SSDataWidgets_B.SSDBGrid grdPrecCant 
            Height          =   1230
            Left            =   315
            TabIndex        =   45
            Top             =   0
            Width           =   6765
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   7
            AllowAddNew     =   -1  'True
            AllowDelete     =   -1  'True
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowColumnSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowGroupSwapping=   0   'False
            AllowColumnSwapping=   0
            AllowGroupShrinking=   0   'False
            AllowColumnShrinking=   0   'False
            AllowDragDrop   =   0   'False
            RowHeight       =   423
            CaptionAlignment=   0
            Columns.Count   =   7
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "CodDto"
            Columns(0).Name =   "CodDto"
            Columns(0).Alignment=   2
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   2910
            Columns(1).Caption=   "Hasta Cant (incluido)"
            Columns(1).Name =   "Hasta Cant (incluido)"
            Columns(1).Alignment=   1
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   5
            Columns(1).FieldLen=   256
            Columns(2).Width=   2196
            Columns(2).Caption=   "Precio"
            Columns(2).Name =   "Precio"
            Columns(2).Alignment=   1
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   5
            Columns(2).FieldLen=   256
            Columns(3).Width=   3200
            Columns(3).Visible=   0   'False
            Columns(3).Caption=   "Cant_Aux"
            Columns(3).Name =   "Cant_Aux"
            Columns(3).DataField=   "Column 3"
            Columns(3).DataType=   5
            Columns(3).FieldLen=   256
            Columns(4).Width=   3200
            Columns(4).Visible=   0   'False
            Columns(4).Caption=   "Precio_aux"
            Columns(4).Name =   "Precio_aux"
            Columns(4).DataField=   "Column 4"
            Columns(4).DataType=   5
            Columns(4).FieldLen=   256
            Columns(5).Width=   5715
            Columns(5).Caption=   "Descripci�n"
            Columns(5).Name =   "Descripci�n"
            Columns(5).DataField=   "Column 5"
            Columns(5).DataType=   8
            Columns(5).FieldLen=   256
            Columns(6).Width=   3200
            Columns(6).Visible=   0   'False
            Columns(6).Caption=   "Descrip_aux"
            Columns(6).Name =   "Descrip_aux"
            Columns(6).DataField=   "Column 6"
            Columns(6).DataType=   8
            Columns(6).FieldLen=   256
            _ExtentX        =   11933
            _ExtentY        =   2170
            _StockProps     =   79
         End
         Begin MSMask.MaskEdBox at 
            Height          =   285
            Index           =   8
            Left            =   10245
            TabIndex        =   48
            Top             =   900
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   5
            Format          =   "#,###"
            PromptChar      =   " "
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "Plazo interrupci�n de tramos en dias"
            Height          =   195
            Index           =   1
            Left            =   7245
            TabIndex        =   51
            Top             =   930
            Width           =   2550
         End
      End
      Begin VB.TextBox txtNombre 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   6000
         Locked          =   -1  'True
         TabIndex        =   49
         Text            =   "Descripci�n"
         Top             =   90
         Width           =   5500
      End
      Begin VB.Frame frmAtrib 
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         Height          =   1250
         Index           =   0
         Left            =   50
         TabIndex        =   6
         Top             =   360
         Visible         =   0   'False
         Width           =   11520
         Begin VB.Frame Frame1 
            Caption         =   "Precio Calculado Relativo al nodo :"
            Height          =   1230
            Left            =   3060
            TabIndex        =   9
            Top             =   0
            Width           =   2895
            Begin VB.TextBox txtRutaRel 
               Height          =   285
               Left            =   900
               TabIndex        =   11
               ToolTipText     =   "Ruta del atributo"
               Top             =   585
               Visible         =   0   'False
               Width           =   420
            End
            Begin VB.TextBox txtRutaRelDesc 
               Height          =   285
               Left            =   75
               TabIndex        =   10
               ToolTipText     =   "Ruta del atributo"
               Top             =   225
               Width           =   2745
            End
            Begin MSMask.MaskEdBox at 
               Height          =   285
               Index           =   6
               Left            =   1575
               TabIndex        =   12
               Top             =   890
               Width           =   1245
               _ExtentX        =   2196
               _ExtentY        =   503
               _Version        =   327681
               AutoTab         =   -1  'True
               Enabled         =   0   'False
               MaxLength       =   7
               Format          =   "##0.###"
               PromptChar      =   " "
            End
            Begin MSMask.MaskEdBox at 
               Height          =   285
               Index           =   5
               Left            =   1575
               TabIndex        =   13
               Top             =   575
               Width           =   1245
               _ExtentX        =   2196
               _ExtentY        =   503
               _Version        =   327681
               AutoTab         =   -1  'True
               Enabled         =   0   'False
               MaxLength       =   15
               Format          =   "###,###,##0.###"
               PromptChar      =   " "
            End
            Begin VB.Label al 
               AutoSize        =   -1  'True
               Caption         =   "Precio Fijo"
               Height          =   195
               Index           =   10
               Left            =   75
               TabIndex        =   15
               Top             =   600
               Width           =   735
            End
            Begin VB.Label al 
               AutoSize        =   -1  'True
               Caption         =   "% del importe"
               Height          =   195
               Index           =   9
               Left            =   75
               TabIndex        =   14
               Top             =   915
               Width           =   930
            End
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Franquicia Unitaria ?"
            Height          =   255
            Index           =   8
            Left            =   6255
            TabIndex        =   8
            Top             =   90
            Width           =   1770
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Franquicia Suma ?"
            Height          =   255
            Index           =   7
            Left            =   6255
            TabIndex        =   7
            Top             =   360
            Width           =   1770
         End
         Begin MSMask.MaskEdBox at 
            Height          =   285
            Index           =   0
            Left            =   1485
            TabIndex        =   17
            Top             =   105
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   15
            Format          =   "###,###,##0.###"
            PromptChar      =   " "
         End
         Begin SSCalendarWidgets_A.SSDateCombo SDCConc 
            Height          =   285
            Index           =   1
            Left            =   9675
            TabIndex        =   18
            Top             =   330
            Width           =   1725
            _Version        =   65537
            _ExtentX        =   3043
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483634
            Enabled         =   0   'False
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo SDCConc 
            Height          =   285
            Index           =   0
            Left            =   9675
            TabIndex        =   19
            Top             =   30
            Width           =   1725
            _Version        =   65537
            _ExtentX        =   3043
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483634
            Enabled         =   0   'False
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "3000/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            NullDateLabel   =   "__/__/____"
            StartofWeek     =   2
         End
         Begin MSMask.MaskEdBox at 
            Height          =   285
            Index           =   1
            Left            =   1485
            TabIndex        =   20
            Top             =   510
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   11
            Format          =   "###,###,##0.###"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox at 
            Height          =   285
            Index           =   2
            Left            =   9675
            TabIndex        =   21
            Top             =   630
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   7
            Format          =   "##0.###"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox at 
            Height          =   285
            Index           =   3
            Left            =   9675
            TabIndex        =   22
            Top             =   930
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   5
            Format          =   "#,###"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox at 
            Height          =   285
            Index           =   4
            Left            =   5490
            TabIndex        =   23
            ToolTipText     =   "C�digo del atributo"
            Top             =   360
            Visible         =   0   'False
            Width           =   375
            _ExtentX        =   661
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            PromptChar      =   " "
         End
         Begin VB.TextBox txtRuta 
            Height          =   285
            Left            =   5490
            TabIndex        =   16
            ToolTipText     =   "Ruta del atributo"
            Top             =   45
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "Per�odo Garant�a"
            Height          =   195
            Index           =   3
            Left            =   8340
            TabIndex        =   29
            Top             =   1005
            Width           =   1245
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            Height          =   195
            Index           =   4
            Left            =   8340
            TabIndex        =   28
            Top             =   105
            Width           =   870
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            Height          =   195
            Index           =   5
            Left            =   8340
            TabIndex        =   27
            Top             =   405
            Width           =   705
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "% Descuento"
            Height          =   195
            Index           =   7
            Left            =   8340
            TabIndex        =   26
            Top             =   705
            Width           =   945
         End
         Begin VB.Label al 
            Caption         =   "Precio D�a"
            Height          =   255
            Index           =   6
            Left            =   45
            TabIndex        =   25
            Top             =   525
            Width           =   975
         End
         Begin VB.Label al 
            Caption         =   "Precio Referencia"
            Height          =   255
            Index           =   0
            Left            =   45
            TabIndex        =   24
            Top             =   135
            Width           =   1785
         End
      End
      Begin VB.Frame frmAtrib 
         BorderStyle     =   0  'None
         Caption         =   "Frame4"
         Height          =   1185
         Index           =   2
         Left            =   50
         TabIndex        =   2
         Top             =   360
         Visible         =   0   'False
         Width           =   11525
         Begin VB.TextBox txtDescripcion 
            Height          =   825
            Left            =   0
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   3
            Text            =   "DefConc.ctx":0000
            Top             =   345
            Width           =   11505
         End
         Begin VB.Label al 
            AutoSize        =   -1  'True
            Caption         =   "Esta descripci�n sustituye a la del nodo o categoria en la linea de la factura :"
            Height          =   195
            Index           =   2
            Left            =   0
            TabIndex        =   4
            Top             =   45
            Width           =   5415
         End
      End
      Begin VB.Frame frmAtrib 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Height          =   1250
         Index           =   1
         Left            =   50
         TabIndex        =   30
         Top             =   360
         Visible         =   0   'False
         Width           =   11525
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Mostrar Desglose ?"
            Height          =   255
            Index           =   9
            Left            =   5385
            TabIndex        =   41
            Top             =   75
            Width           =   2010
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Opcional ?"
            Height          =   195
            Index           =   6
            Left            =   10350
            TabIndex        =   38
            Top             =   0
            Visible         =   0   'False
            Width           =   1110
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Es un suplemento ?"
            Height          =   255
            Index           =   10
            Left            =   5385
            TabIndex        =   37
            Top             =   475
            Width           =   2010
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Facturaci�n Obligatoria ?"
            Height          =   255
            Index           =   5
            Left            =   2430
            TabIndex        =   36
            Top             =   475
            Width           =   2235
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "L�nea Obligatoria ?"
            Height          =   255
            Index           =   4
            Left            =   2430
            TabIndex        =   35
            Top             =   75
            Width           =   2235
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Descontable ?"
            Height          =   255
            Index           =   3
            Left            =   2430
            TabIndex        =   34
            Top             =   875
            Width           =   2235
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Excluido ?"
            Height          =   255
            Index           =   2
            Left            =   0
            TabIndex        =   33
            Top             =   875
            Width           =   1455
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Suficiente ?"
            Height          =   255
            Index           =   1
            Left            =   0
            TabIndex        =   32
            Top             =   475
            Width           =   1455
         End
         Begin VB.CheckBox ac 
            Alignment       =   1  'Right Justify
            Caption         =   "Necesario ?"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   31
            Top             =   75
            Width           =   1455
         End
         Begin SSDataWidgets_B.SSDBCombo cbossEntidad 
            Height          =   285
            Left            =   7200
            TabIndex        =   39
            Top             =   870
            Width           =   4065
            DataFieldList   =   "Column 1"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   7064
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).CaptionAlignment=   0
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   7170
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   16777215
            DataFieldToDisplay=   "Column 1"
         End
         Begin MSMask.MaskEdBox at 
            Height          =   285
            Index           =   7
            Left            =   10020
            TabIndex        =   40
            Top             =   75
            Width           =   1245
            _ExtentX        =   2196
            _ExtentY        =   503
            _Version        =   327681
            AutoTab         =   -1  'True
            MaxLength       =   15
            Format          =   "###,###,##0.###"
            PromptChar      =   " "
         End
         Begin VB.Label al 
            Caption         =   "N� de orden en factura"
            Height          =   255
            Index           =   12
            Left            =   8175
            TabIndex        =   43
            Top             =   100
            Width           =   1785
         End
         Begin VB.Label al 
            Caption         =   "Entidad Responsable"
            Height          =   255
            Index           =   8
            Left            =   5385
            TabIndex        =   42
            Top             =   900
            Width           =   1695
         End
      End
      Begin TabDlg.SSTab SSTabAtrib 
         Height          =   1575
         Left            =   0
         TabIndex        =   5
         Top             =   90
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   2778
         _Version        =   327681
         Tabs            =   4
         Tab             =   1
         TabsPerRow      =   8
         TabHeight       =   397
         ForeColor       =   16711680
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Precios"
         TabPicture(0)   =   "DefConc.ctx":0006
         Tab(0).ControlEnabled=   0   'False
         Tab(0).ControlCount=   0
         TabCaption(1)   =   "Varios"
         TabPicture(1)   =   "DefConc.ctx":0022
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).ControlCount=   0
         TabCaption(2)   =   "Descripci�n"
         TabPicture(2)   =   "DefConc.ctx":003E
         Tab(2).ControlEnabled=   0   'False
         Tab(2).ControlCount=   0
         TabCaption(3)   =   "Prec. x Cantidad"
         TabPicture(3)   =   "DefConc.ctx":005A
         Tab(3).ControlEnabled=   0   'False
         Tab(3).ControlCount=   0
      End
   End
   Begin ctlConc.DBConc DBT 
      Height          =   1950
      Left            =   135
      TabIndex        =   0
      Top             =   225
      Width           =   11580
      _ExtentX        =   20426
      _ExtentY        =   3440
   End
End
Attribute VB_Name = "DefConc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private rdoc As rdoConnection
Private miObjApp As Object

Public Event CambiarAtribHijos(codigo As String, nombre As String, FechaProceso As String)
Public Event DtosCantidad(CodAtrib As String)

' Constantes para los MaskEditBox
Const PrecioRef = 0
Const PrecioDia = 1
Const Descuento = 2
Const PeriodoGara = 3
Const CodAtrib = 4
Const RelFijo = 5
Const RelPor = 6
Const OrdImp = 7
Const PlazoInter = 8

' Constantes para los CheckBox
Const Necesario = 0
Const Suficiente = 1
Const Excluido = 2
Const Descont = 3
Const LinOblig = 4
Const FactOblig = 5
Const Opcional = 6
Const FranqSuma = 7
Const FranqUni = 8
Const Desglose = 9
Const Suplemento = 10
Const TramAcum = 11
Const TramDias = 12


' Constantes para los Sheridan DateControl
Const FecInicio = 0
Const FecFin = 1


Const MaxAC = 12
Const MaxAT = 8

Private rq(1 To 10) As New rdoQuery

Private Sub ac_Click(Index As Integer)
    Select Case Index
        Case 7  ' Franquicia de suma
            If ac(Index).Value = 1 Then
                ac(8).Value = 0
            End If
        Case 8  ' Franquicia Unitaria
            If ac(Index).Value = 1 Then
                ac(7).Value = 0
            End If
'        Case TramDias  ' los tramos son por dias
'            If ac(Index).Value = 0 Then
'                ac(TramAcum).Value = 0
'                ac(TramAcum).Enabled = False
'                at(PlazoInter).Text = ""
'                at(PlazoInter).Enabled = False
'            Else
'                ac(TramAcum).Enabled = True
'                at(PlazoInter).Enabled = True
'            End If
'        Case 9  ' Precios por cantidad
'            If ac(Index).Value = 1 Then
''                cmdDtoCantidad.Enabled = True
'                cmdDtoCantidad.Caption = "Modificar"
'            Else
''                cmdDtoCantidad.Enabled = False
'                cmdDtoCantidad.Caption = "Crear"
'            End If
'        Case 2  ' Excluido
'            If ac(Index).Value = 1 Then
'                ac(0).Value = 0
'                ac(1).Value = 0
'            End If
'        Case 0, 1 ' si es necesario o suficiente, desmarcar Excluido
'            If ac(Index).Value = 1 Then
'                ac(Excluido).Value = 0
'            End If
    End Select
End Sub

'Private Sub cmdDtoCantidad_Click()
''    SaveData
'    RaiseEvent DtosCantidad(at(CodAtrib).Text)
'
'End Sub

Private Sub dbt_CambiarAtribHijos(codigo As String, nombre As String)
    RaiseEvent CambiarAtribHijos(codigo, nombre, FechaProceso)
End Sub

Private Sub at_GotFocus(Index As Integer)
    at(Index).SelStart = 0
    at(Index).SelLength = Len(at(Index))
End Sub

Private Sub dbt_CambiarConcierto()
  CambiarConcierto
End Sub

Private Sub DBT_CrearRelacion(codigo As String, nombre As String)
    Dim RutaComun As String
    Dim i As Integer
    Dim PosPunto As Integer
    
    
    PosPunto = 1
    i = 1
    Do While PosPunto < Len(txtRuta.Text) And PosPunto <> 0
        
        PosPunto = InStr(PosPunto + 1, txtRuta.Text, ".")
        If InStr(PosPunto + 1, txtRuta.Text, ".") <> 0 Then
            RutaComun = Left(txtRuta.Text, PosPunto)
            i = i + 1
        Else
            PosPunto = 0
        End If
    Loop
    
    If InStr(codigo, RutaComun) = 1 And _
       Right(codigo, Len(codigo) - Len(RutaComun)) <> "/" Then
        txtRutaRel.Text = Right(codigo, Len(codigo) - Len(RutaComun))
        If Left(txtRutaRel.Text, 1) <> "." Or Left(txtRutaRel.Text, 1) <> "." Then
            txtRutaRel.Text = "." & txtRutaRel.Text
        End If
        txtRutaRelDesc.Text = nombre
        
        at(RelFijo).Enabled = True
        at(RelPor).Enabled = True
        
    Else
        MsgBox "No se puede crear una relaci�n con este nodo", vbOKOnly
    End If
End Sub

Private Sub dbt_NodoSeleccionado(codigo As String, nombre As String, editable As Boolean)
    Dim rs As rdoResultset
    Dim RS2 As rdoResultset
    Dim i As Integer
    
    If ChangedAtrib = True Then
        If ValidarAtrib = False Then
            Exit Sub
        End If
        SaveData
    End If
    
    txtNombre.Text = UCase(nombre)
    If editable Then
        txtNombre.ForeColor = vbBlue
    Else
        txtNombre.ForeColor = vbRed
    End If
'    fraAtributos.Enabled = editable
    SSTabAtrib.Enabled = True
    frmAtrib(0).Enabled = editable
    frmAtrib(1).Enabled = editable
    frmAtrib(2).Enabled = editable
    frmAtrib(3).Enabled = editable
    
    rq(1).rdoParameters(0) = codigo
    rq(1).rdoParameters(1) = DBT.FechaProceso
    rq(1).rdoParameters(2) = DBT.FechaProceso
    
    Set rs = rq(1).OpenResultset

    If Not rs.EOF Then
        at(PrecioRef).Text = IIf(IsNull(rs!FA15PRECIOREF), "", rs!FA15PRECIOREF)
        at(PrecioRef).Tag = at(PrecioRef).Text
        
        at(PrecioDia).Text = IIf(IsNull(rs!FA15PrecioDia), "", rs!FA15PrecioDia)
        at(PrecioDia).Tag = at(PrecioDia).Text
        
        at(Descuento).Text = IIf(IsNull(rs!FA15Descuento), "", rs!FA15Descuento)
        at(Descuento).Tag = at(Descuento).Text
        
        at(PeriodoGara).Text = IIf(IsNull(rs!FA15PeriodoGara), "", rs!FA15PeriodoGara)
        at(PeriodoGara).Tag = at(PeriodoGara).Text
        
        at(CodAtrib).Text = IIf(IsNull(rs!FA15CODATRIB), "", rs!FA15CODATRIB)
        at(CodAtrib).Tag = at(CodAtrib).Text
        
        txtRutaRel.Text = IIf(IsNull(rs!FA15RutaRel), "", rs!FA15RutaRel)
        txtRutaRel.Tag = txtRutaRel.Text
        
        If txtRutaRel.Text <> "" Then
            at(RelFijo).Enabled = True
            at(RelPor).Enabled = True
            
            If Right(txtRutaRel.Text, 1) = "/" Then
                rq(6).rdoParameters(0) = Left(txtRutaRel.Text, Len(txtRutaRel.Text) - 1)
                rq(6).rdoParameters(1) = Left(txtRutaRel.Text, Len(txtRutaRel.Text) - 1)
                rq(6).rdoParameters(2) = Left(txtRutaRel.Text, Len(txtRutaRel.Text) - 1)
                rq(6).rdoParameters(3) = Left(txtRutaRel.Text, Len(txtRutaRel.Text) - 1)
                
                Set RS2 = rq(6).OpenResultset
                If Not RS2.EOF() Then
                    txtRutaRelDesc.Text = IIf(IsNull(RS2!FA09DESIG), "", RS2!FA09DESIG)
                Else
                    txtRutaRelDesc.Text = ""
                End If
            Else
                rq(7).rdoParameters(0) = txtRutaRel.Text
                rq(7).rdoParameters(1) = txtRutaRel.Text
                rq(7).rdoParameters(2) = txtRutaRel.Text
                rq(7).rdoParameters(3) = txtRutaRel.Text
                
                Set RS2 = rq(7).OpenResultset
                If Not RS2.EOF() Then
                    txtRutaRelDesc.Text = IIf(IsNull(RS2!FA05DESIG), "", RS2!FA05DESIG)
                Else
                    txtRutaRelDesc.Text = ""
                End If
            End If
            RS2.Close
        Else
            txtRutaRelDesc.Text = ""
            at(RelFijo).Enabled = False
            at(RelPor).Enabled = False
        End If
        
        at(RelFijo).Text = IIf(IsNull(rs!FA15RelFijo), "", rs!FA15RelFijo)
        at(RelFijo).Tag = at(RelFijo).Text
        
        at(RelPor).Text = IIf(IsNull(rs!FA15relpor), "", rs!FA15relpor)
        at(RelPor).Tag = at(RelPor).Text
        
        at(OrdImp).Text = IIf(IsNull(rs!FA15OrdImp), "", rs!FA15OrdImp)
        at(OrdImp).Tag = at(OrdImp).Text
        
        txtRuta.Text = codigo
        txtRuta.Tag = codigo
        
        
        txtDescripcion.Text = IIf(IsNull(rs!FA15DESCRIPCION), "", rs!FA15DESCRIPCION)
        txtDescripcion.Tag = txtDescripcion.Text
        
        SDCConc(FecInicio).Date = IIf(IsNull(rs!FA15FecInicio), "", rs!FA15FecInicio)
        SDCConc(FecInicio).Tag = SDCConc(FecInicio).Date
        
        SDCConc(FecFin).Date = IIf(IsNull(rs!FA15FecFin), "", rs!FA15FecFin)
        SDCConc(FecFin).Tag = SDCConc(FecFin).Date
        
        If SDCConc(FecFin).Date <> "31/12/3000" Then
'          fraAtributos.Enabled = False
'          SSTabAtrib.Enabled = False
            frmAtrib(0).Enabled = False
            frmAtrib(1).Enabled = False
            frmAtrib(2).Enabled = False
            frmAtrib(3).Enabled = False
        End If

        
        ac(Necesario).Value = IIf(IsNull(rs!FA15INDNecesario), 0, rs!FA15INDNecesario)
        ac(Necesario).Tag = ac(Necesario).Value
        
        ac(Suficiente).Value = IIf(IsNull(rs!FA15INDSuficiente), 0, rs!FA15INDSuficiente)
        ac(Suficiente).Tag = ac(Suficiente).Value
        
        ac(Excluido).Value = IIf(IsNull(rs!FA15INDExcluido), 0, rs!FA15INDExcluido)
        ac(Excluido).Tag = ac(Excluido).Value
        
        ac(Descont).Value = IIf(IsNull(rs!FA15INDDescont), 0, rs!FA15INDDescont)
        ac(Descont).Tag = ac(Descont).Value
        
        ac(LinOblig).Value = IIf(IsNull(rs!FA15INDLinOblig), 0, rs!FA15INDLinOblig)
        ac(LinOblig).Tag = ac(LinOblig).Value
        
        ac(FactOblig).Value = IIf(IsNull(rs!FA15INDFactOblig), 0, rs!FA15INDFactOblig)
        ac(FactOblig).Tag = ac(FactOblig).Value
        
        ac(Opcional).Value = IIf(IsNull(rs!FA15INDOpcional), 0, rs!FA15INDOpcional)
        ac(Opcional).Tag = ac(Opcional).Value
        
        ac(FranqSuma).Value = IIf(IsNull(rs!FA15INDFranqSuma), 0, rs!FA15INDFranqSuma)
        ac(FranqSuma).Tag = ac(FranqSuma).Value
        
        ac(FranqUni).Value = IIf(IsNull(rs!FA15INDFranqUni), 0, rs!FA15INDFranqUni)
        ac(FranqUni).Tag = ac(FranqUni).Value
        
        ac(Desglose).Value = IIf(IsNull(rs!FA15INDDesglose), 0, rs!FA15INDDesglose)
        ac(Desglose).Tag = ac(Desglose).Value
        
        ac(Suplemento).Value = IIf(IsNull(rs!FA15INDSuplemento), 0, rs!FA15INDSuplemento)
        ac(Suplemento).Tag = ac(Suplemento).Value
        
        ac(TramAcum).Value = IIf(IsNull(rs!FA15INDTramAcum), 0, rs!FA15INDTramAcum)
        ac(TramAcum).Tag = ac(TramAcum).Value
        
        ac(TramDias).Value = IIf(IsNull(rs!FA15INDTramDias), 0, rs!FA15INDTramDias)
        ac(TramDias).Tag = ac(TramDias).Value
        
        at(PlazoInter).Text = IIf(IsNull(rs!FA15PlazoInter), "", rs!FA15PlazoInter)
        at(PlazoInter).Tag = at(PlazoInter).Text
        
        
        cbossEntidad.Text = IIf(IsNull(rs!CI13CODENTIDAD), "", rs!CI13DESENTIDAD)
        cbossEntidad.Tag = cbossEntidad.Text
        
        ' Cargar los precios por cantidad ( si los tiene)
        
        chkBorrado.Value = vbUnchecked
        grdPrecCant.RemoveAll
        
        rq(9).rdoParameters(0) = at(CodAtrib).Text
        Set RS2 = rq(9).OpenResultset
        

        Do While Not RS2.EOF
            grdPrecCant.AddNew
            
            grdPrecCant.Columns(0).Value = RS2!FA07CODDESCUENT
            
            grdPrecCant.Columns(1).Value = RS2!FA07ACANTFIN
            grdPrecCant.Columns(3).Value = RS2!FA07ACANTFIN
            
            grdPrecCant.Columns(2).Value = RS2!FA07PRECIO
            grdPrecCant.Columns(4).Value = RS2!FA07PRECIO
            
            grdPrecCant.Columns(5).Value = RS2!FA07DESIG
            grdPrecCant.Columns(6).Value = RS2!FA07DESIG
            
            RS2.MoveNext
        Loop

        RS2.Close
        grdPrecCant.MoveFirst
    Else
        LimpiarAtrib
        txtRuta.Text = codigo
        txtRuta.Tag = codigo
    End If
    
IsNothing:
    rs.Close
    Set rs = Nothing
End Sub


Private Sub SaveData()
  Dim i%
        
  miObjApp.BeginTrans
     
  On Error GoTo error
  
  If txtRuta.Text <> "" Then
      BorrarAtrib
      CrearAtrib
  Else
      CrearAtrib
  End If
  DBT.DepurarBD
  
  miObjApp.CommitTrans
  
  ' Marcar como modificado para la grabaci�n
  DBT.indModificado = True
  
  Exit Sub

error:
    
  miObjApp.RollbackTrans
End Sub

Private Function ValidarAtrib() As Boolean
    Dim i As Integer
    ValidarAtrib = False

    If (at(PrecioRef).Text <> "") Then
        If (at(PrecioDia).Text <> "") Then
            Call MsgBox("No puede tener precio de referencia y precio dia a la vez," & Chr$(13) & _
                    "      Debe dejar al menos uno de ellos en blanco.", , "Error de validaci�n")
            at(PrecioRef).SetFocus
            Exit Function
        End If
    End If
    
    If (at(Descuento).Text <> "") Then
        If ((at(Descuento).Text < 0) Or (100 < at(Descuento).Text)) Then
            Call MsgBox("El Dto debe estar entre 0% y 100%.", , "Error de validaci�n")
            at(Descuento).SetFocus
            Exit Function
        End If
    End If

'    If (at(1).Text <> "") And (Not IsNumeric(at(1).Text)) Then
'        MsgBox ("La franquicia de suma debe ser un n�mero")
'        at(1).SetFocus
'        Exit Function
'    End If
'
'    If (at(2).Text <> "") And (Not IsNumeric(at(2).Text)) Then
'        MsgBox ("La franquicia unitaria debe ser un n�mero")
'        at(2).SetFocus
'        Exit Function
'    End If
'
'    If (at(3).Text <> "") And (Not IsNumeric(at(3).Text)) Then
'        MsgBox ("El periodo de garant�a debe ser un n�mero")
'        at(3).SetFocus
'        Exit Function
'    End If

    ' Validar los precios por cantidad
    grdPrecCant.MoveFirst
    For i = 1 To grdPrecCant.Rows
        
        ' Si la cantidad esta vacia avisar
        If grdPrecCant.Columns(1).Value = "" Or Not IsNumeric(grdPrecCant.Columns(1).Value) Then
            MsgBox "La cantidad debe ser un n�mero", vbOKOnly, "Error de validaci�n"
            SSTabAtrib.Tab = 3
            grdPrecCant.SetFocus
            
            Exit Function
        End If
        
        ' Si la cantidad es demasiado grande avisar
        If Val(grdPrecCant.Columns(1).Value) > 99999 Then
            MsgBox "La cantidad no puede ser tan grande", vbOKOnly, "Error de validaci�n"
            SSTabAtrib.Tab = 3
            
            grdPrecCant.SetFocus
            
            Exit Function
        End If
        
        ' Si el precio esta vacio avisar
        If grdPrecCant.Columns(2).Value = "" Or Not IsNumeric(grdPrecCant.Columns(2).Value) Then
            MsgBox "El precio debe ser un n�mero", vbOKOnly, "Error de validaci�n"
            SSTabAtrib.Tab = 3
            
            grdPrecCant.SetFocus
            
            Exit Function
        End If
        
'         Si el nuevo precio no tiene codigo asignarselo
'        If grdPrecCant.Columns(0).Value = "" Then
'            grdPrecCant.Columns(0).Value = NuevoCodigoDesc
'        End If
        
        grdPrecCant.MoveNext
    Next
    

    ValidarAtrib = True
End Function

Private Function ChangedAtrib() As Boolean
    Dim i As Integer
    ChangedAtrib = True
    
    For i = 0 To MaxAT
        If at(i).Tag <> at(i).Text Then
            Exit Function
        End If
    Next
      
    If txtRuta.Tag <> txtRuta.Text Then
        Exit Function
    End If
    
    If txtRutaRel.Tag <> txtRutaRel.Text Then
        Exit Function
    End If
    
    If txtDescripcion.Tag <> txtDescripcion.Text Then
        Exit Function
    End If
    
    For i = 0 To MaxAC
        If Val(ac(i).Tag) <> ac(i).Value Then
            Exit Function
        End If
    Next
    
    If cbossEntidad.Tag <> cbossEntidad.Text Then
        Exit Function
    End If
    
    ' Revisar si se han modificado los precios por cantidad
    ' Si se ha borrado alguna linea se revisa el CheckBox
    If chkBorrado.Value = vbChecked Then
        Exit Function
    End If
    
    ' Si no se revisa el grid para ver si se ha modificado
    ' la comparaci�n se realiza entre las columnas (1 y 3) y (2 y 4)
    grdPrecCant.MoveFirst
    For i = 1 To grdPrecCant.Rows
        If grdPrecCant.Columns(1).Value <> grdPrecCant.Columns(3).Value Then
            Exit Function
        End If
        
        If grdPrecCant.Columns(2).Value <> grdPrecCant.Columns(4).Value Then
            Exit Function
        End If
        
        If grdPrecCant.Columns(5).Value <> grdPrecCant.Columns(6).Value Then
            Exit Function
        End If
        
        grdPrecCant.MoveNext
    Next
    ' si se llega hasta aqui no se ha modificado ning�n dato
    ChangedAtrib = False
End Function

Private Sub BorrarAtrib()

    On Error GoTo error
  
    rq(2).rdoParameters(0) = DBT.FechaProceso
    rq(2).rdoParameters(1) = at(CodAtrib).Text
    rq(2).Execute
    
Exit Sub
error:
    MsgBox "Error : " & Err.Number & " " & Err.Description
End Sub

Private Sub CrearAtrib()
    Dim i As Integer
    Dim CodAtribOld As String
    Dim CodAtribNew As String
    
    On Error GoTo error
    ' guardar el codigo de atributo anterior para copiar los
    ' descuentos por cantidad (si los hubiera) al nuevo registro de
    ' atributos
    CodAtribOld = at(CodAtrib).Text
    ' CALCULAR EL CODIGO DEL ATRIBUTO (sequencia)
    
    at(CodAtrib).Text = DBT.fNextClave("FA15CODATRIB", "FA1500")
    CodAtribNew = at(CodAtrib).Text
      
    For i = 0 To MaxAT
        rq(3).rdoParameters(i) = IIf(at(i).Text = "", Null, at(i).Text)
    Next
    
    rq(3).rdoParameters(MaxAT + 1) = txtRuta.Text
      
    rq(3).rdoParameters(MaxAT + 2) = DBT.FechaProceso
    
    For i = 0 To MaxAC
        rq(3).rdoParameters(i + MaxAT + 3) = ac(i).Value
    Next
    
    
    If cbossEntidad.Text = "" Then
        rq(3).rdoParameters(MaxAT + MaxAC + 4) = ""
    Else
        rq(3).rdoParameters(MaxAT + MaxAC + 4) = cbossEntidad.Columns(0).Text
    End If
    
    rq(3).rdoParameters(MaxAT + MaxAC + 5) = txtRutaRel.Text
    rq(3).rdoParameters(MaxAT + MaxAC + 6) = txtDescripcion.Text
    
    rq(3).Execute
  
  
  
    'Copiar los descuentos por cantidad
    grdPrecCant.MoveFirst
    For i = 1 To grdPrecCant.Rows
        rq(10).rdoParameters(0) = CodAtribNew
        rq(10).rdoParameters(1) = i
        rq(10).rdoParameters(2) = grdPrecCant.Columns(1).Value
        rq(10).rdoParameters(3) = grdPrecCant.Columns(2).Value
        rq(10).rdoParameters(4) = grdPrecCant.Columns(5).Value
        
        rq(10).Execute
        
        grdPrecCant.MoveNext
    Next

'  If CodAtribOld <> "" Then
'    rq(5).rdoParameters(0) = CodAtribNew
'    rq(5).rdoParameters(1) = CodAtribOld
'    rq(5).Execute
'  End If
  
Exit Sub
error:
  MsgBox "Error : " & Err.Number & " " & Err.Description
'  Resume
End Sub

Private Sub grdPrecCant_AfterDelete(RtnDispErrMsg As Integer)
        chkBorrado.Value = vbChecked
End Sub

Private Sub SSTabAtrib_Click(PreviousTab As Integer)
    frmAtrib(PreviousTab).Visible = False
    frmAtrib(SSTabAtrib.Tab).Visible = True
End Sub

Private Sub txtRutaRelDesc_Change()
    If txtRutaRelDesc.Text = "" Then txtRutaRel.Text = ""
End Sub

Private Sub UserControl_Resize()
    Dim i As Integer
    
    If UserControl.Width < 3500 Then UserControl.Width = 3500
    If UserControl.Height < 2900 Then UserControl.Height = 2900
    
    DBT.Height = UserControl.Height - fraAtributos.Height
    DBT.Width = UserControl.Width - 40
    DBT.Top = 0
    DBT.Left = 40
    
'    fraAtributos.Height = UserControl.Height
    fraAtributos.Width = DBT.Width
    fraAtributos.Top = UserControl.Height - fraAtributos.Height
    fraAtributos.Left = 30
    
    SSTabAtrib.Width = fraAtributos.Width
    SSTabAtrib.TabMaxWidth = 1400
    txtNombre.Text = ""
    txtNombre.Width = SSTabAtrib.Width - 6000
    
    
    SSTabAtrib.Tab = 0
    frmAtrib(0).Visible = True
    
    For i = 0 To 3
        frmAtrib(i).Width = SSTabAtrib.Width - 225
        frmAtrib(i).Left = 100
        frmAtrib(i).Top = 360
    Next
    
End Sub

Public Sub Inicializa(objapp As Object)
    Dim i%
    
    Set miObjApp = objapp
    Set rdoc = objapp.rdoConnect

    rq(1).Sql = "SELECT FA15PRECIOREF," & _
                        "FA15PRECIODIA, " & _
                        "FA15DESCUENTO, " & _
                        "FA15PERIODOGARA," & _
                        "FA15CODATRIB," & _
                        "FA15RELFIJO," & _
                        "FA15RELPOR," & _
                        "FA15ORDIMP," & _
                        "FA15RUTA," & _
                        "FA15PlazoInter,"
    rq(1).Sql = rq(1).Sql & _
                        "FA15FECINICIO," & _
                        "FA15FECFIN," & _
                        "FA15INDNECESARIO," & _
                        "FA15INDSUFICIENTE," & _
                        "FA15INDEXCLUIDO," & _
                        "FA15INDDESCONT," & _
                        "FA15INDLINOBLIG," & _
                        "FA15INDFACTOBLIG," & _
                        "FA15INDOPCIONAL, " & _
                        "FA15INDFRANQSUMA, FA15INDFRANQUNI," & _
                        "FA15INDDESGLOSE," & _
                        "FA15INDSUPLEMENTO," & _
                        "FA15INDTramAcum, FA15INDTramDias," & _
                        "FA1500.CI13CODENTIDAD, CI13DESENTIDAD, " & _
                        "FA15RUTAREL," & _
                        "FA15DESCRIPCION "
    rq(1).Sql = rq(1).Sql & _
                "FROM FA1500, CI1300 " & _
                "WHERE FA15RUTA = ? AND " & _
                        "(FA1500.CI13CODENTIDAD IS NULL OR " & _
                        "FA1500.CI13CODENTIDAD=CI1300.CI13CODENTIDAD) AND " & _
                        "FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                        "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN"
                        
                        
    rq(2).Sql = "UPDATE FA1500 SET " & _
                        "FA15FECFIN=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                "WHERE FA15CODATRIB = ?"
                

    rq(3).Sql = "INSERT INTO FA1500 (" & _
                        "FA15PRECIOREF," & _
                        "FA15PRECIODIA, " & _
                        "FA15DESCUENTO, " & _
                        "FA15PERIODOGARA," & _
                        "FA15CODATRIB," & _
                        "FA15RELFIJO," & _
                        "FA15RELPOR," & _
                        "FA15ORDIMP," & _
                        "FA15PlazoInter,"
    rq(3).Sql = rq(3).Sql & _
                        "FA15RUTA," & _
                        "FA15FECINICIO," & _
                        "FA15FECFIN," & _
                        "FA15INDNECESARIO," & _
                        "FA15INDSUFICIENTE," & _
                        "FA15INDEXCLUIDO," & _
                        "FA15INDDESCONT," & _
                        "FA15INDLINOBLIG," & _
                        "FA15INDFACTOBLIG, FA15INDOPCIONAL, " & _
                        "FA15INDFRANQSUMA, FA15INDFRANQUNI, " & _
                        "FA15INDDESGLOSE, FA15INDSUPLEMENTO," & _
                        "FA15INDTramAcum, " & _
                        "FA15INDTramDias, "
    rq(3).Sql = rq(3).Sql & _
                        "CI13CODENTIDAD, " & _
                        "FA15RUTAREL, " & _
                        "FA15DESCRIPCION) " & _
                "VALUES (?,?,?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE('31/12/3000','DD/MM/YYYY'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                
    rq(4).Sql = "SELECT CI13CODENTIDAD, CI13DESENTIDAD FROM CI1300 ORDER BY CI13DESENTIDAD"
                
    rq(5).Sql = "INSERT INTO FA0700 (FA15CODATRIB, FA07CODDESCUENT, FA07CANTINI, FA07ACANTFIN, FA07PRECIO) SELECT ?, FA07CODDESCUENT, FA07CANTINI, FA07ACANTFIN, FA07PRECIO FROM FA0700 WHERE FA15CODATRIB=?"
    
    rq(6).Sql = "SELECT FA09DESIG FROM FA0900 WHERE FA09CODNODOCONC = SUBSTR(?, INSTR(?,'.',-2)+1 , INSTR(?,'.',-1) - INSTR(?,'.',-2)-1 )"
    
    rq(7).Sql = "SELECT FA05DESIG FROM FA0500 WHERE FA05CODCATEG = SUBSTR(?, INSTR(?,'.',-2)+1 , INSTR(?,'.',-1) - INSTR(?,'.',-2)-1 )"
    
    rq(8).Sql = "SELECT MAX(FA07CODDESCUENT) + 1 Nuevo FROM FA0700 WHERE FA15CODATRIB = ?"
    
    rq(9).Sql = "SELECT * FROM FA0700 WHERE FA15CODATRIB = ? ORDER BY FA07ACANTFIN"
    
    ' grabar los nuevos precios por cantidad
    rq(10).Sql = "INSERT INTO FA0700 (FA15CODATRIB, FA07CODDESCUENT, FA07ACANTFIN, FA07PRECIO, FA07DESIG) VALUES (?, ?, ?, ?, ?)"
    

    For i = 1 To UBound(rq)
        Set rq(i).ActiveConnection = miObjApp.rdoConnect
        rq(i).Prepared = True
    Next
    
    Call DBT.Inicializa(objapp)
    CargarComboEntidades
End Sub

Public Sub InitTV(CodConcierto As Long, Desig As String)
    Call DBT.InitTV(CodConcierto, Desig)
    LimpiarAtrib
End Sub

Public Sub InitTV2()
    Call DBT.InitTVCat
    Call DBT.InitTVConcMod
End Sub

Public Sub Nuevo()
    If ActiveControl.Name = grdPrecCant.Name Then
        grdPrecCant.AddNew
    Else
        DBT.Nuevo
    End If
End Sub

Public Sub Borrar()
    If ActiveControl.Name <> grdPrecCant.Name Then
        DBT.Borrar
    Else
        On Error GoTo error
        grdPrecCant.RemoveItem (grdPrecCant.Row)
        chkBorrado.Value = vbChecked
    End If
    
Exit Sub
error:
    MsgBox "No se ha podido borrar esa linea de precio", vbCritical + vbOKOnly, "Error"
End Sub

Public Sub BorrarConcierto(CodConcierto As Long)
    If ChangedAtrib = True Then
        If ValidarAtrib = False Then
            Exit Sub
        End If
        SaveData
    End If
    
    DBT.BorrarConcierto CodConcierto
End Sub

Public Sub CrearAtribVacio(Ruta As String)
  DBT.CrearAtribVacio Ruta
End Sub

Public Property Get Modificado() As Boolean
  Modificado = DBT.indModificado Or ChangedAtrib
End Property

Public Property Let Modificado(indModif As Boolean)
  DBT.indModificado = indModif
End Property

Private Sub CargarComboEntidades()
    Dim rsEntidad As rdoResultset
    
    cbossEntidad.RemoveAll
    
    
    Set rsEntidad = rq(4).OpenResultset
    
    Do While Not rsEntidad.EOF
        cbossEntidad.AddItem rsEntidad(0) & Chr(9) & rsEntidad(1)
        rsEntidad.MoveNext
    Loop
    
    rsEntidad.Close
    
'    cbossEntidad.MoveFirst
End Sub

Private Sub LimpiarAtrib()
    ' Inicializar los TextBox, etc. para la petici�n de atributos
    Dim i As Integer
    
    fraAtributos.Caption = "Atributos"
    
    For i = 0 To MaxAT
        at(i).Text = ""
        at(i).Tag = ""
    Next
    
    '  txtRuta.Text = ""
    '  txtRuta.Tag = ""
    
    txtRutaRel.Text = ""
    txtRutaRel.Tag = ""
    
    txtDescripcion.Text = ""
    txtDescripcion.Tag = ""
    
    SDCConc(0).Date = ""
    SDCConc(0).Tag = ""
    
    SDCConc(1).Date = ""
    SDCConc(1).Tag = ""
    
    For i = 0 To MaxAC
        ac(i).Value = 0
        ac(i).Tag = 0
    Next
    
    cbossEntidad.Text = ""
    cbossEntidad.Tag = ""
  
    grdPrecCant.RemoveAll
End Sub

Public Function CambiarConcierto() As Boolean
  Dim Grabar As Integer
  
  If Modificado = True Then
    Grabar = MsgBox("� Desea salvar los cambios realizados ?", vbYesNoCancel + vbQuestion + vbDefaultButton1)
    Select Case Grabar
    
        Case vbYes
            If ChangedAtrib = True Then
        
                If ValidarAtrib = False Then
                    Exit Function
                Else
                    SaveData
                End If
            End If

            miObjApp.CommitTrans
            CambiarConcierto = True
            Modificado = False
            LimpiarAtrib
        
      Case vbNo
        miObjApp.RollbackTrans
        CambiarConcierto = True
        Modificado = False
        LimpiarAtrib
        
      Case vbCancel
        CambiarConcierto = False   ' no hacer el unload
    End Select
  Else
    miObjApp.RollbackTrans
    CambiarConcierto = True
    LimpiarAtrib
  End If
  
  miObjApp.BeginTrans
End Function


Public Function FechaProceso()
  FechaProceso = DBT.FechaProceso
End Function

Private Function NuevoCodigoDesc() As String
    Dim rs As rdoResultset

    rq(8).rdoParameters(0) = CodAtrib
    
    Set rs = rq(8).OpenResultset

    If Not rs.EOF Then
        NuevoCodigoDesc = IIf(IsNull(rs!Nuevo), "1", rs!Nuevo)
    Else
        NuevoCodigoDesc = "1"
    End If
    
    Set rs = Nothing
End Function

