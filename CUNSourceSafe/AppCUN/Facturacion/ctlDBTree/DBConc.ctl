VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.UserControl DBConc 
   ClientHeight    =   4305
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8460
   LockControls    =   -1  'True
   ScaleHeight     =   4305
   ScaleWidth      =   8460
   Begin ComctlLib.TreeView tvCat 
      Height          =   1545
      Left            =   2745
      TabIndex        =   8
      Top             =   2160
      Width           =   1860
      _ExtentX        =   3281
      _ExtentY        =   2725
      _Version        =   327682
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin ComctlLib.TreeView tvConcMod 
      Height          =   1860
      Left            =   4230
      TabIndex        =   7
      Top             =   1080
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   3281
      _Version        =   327682
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin TabDlg.SSTab ssTabTV 
      Height          =   3300
      Left            =   2655
      TabIndex        =   6
      Top             =   630
      Width           =   4020
      _ExtentX        =   7091
      _ExtentY        =   5821
      _Version        =   327681
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      ForeColor       =   16711680
      TabCaption(0)   =   "Categor�as"
      TabPicture(0)   =   "DBConc.ctx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Conciertos"
      TabPicture(1)   =   "DBConc.ctx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
   End
   Begin MSMask.MaskEdBox mskHoraProceso 
      Height          =   285
      Left            =   7515
      TabIndex        =   2
      Top             =   0
      Visible         =   0   'False
      Width           =   870
      _ExtentX        =   1535
      _ExtentY        =   503
      _Version        =   327681
      MaxLength       =   8
      Format          =   "hh:mm:ss"
      Mask            =   "##:##:##"
      PromptChar      =   "_"
   End
   Begin SSCalendarWidgets_A.SSDateCombo sdcFechaProceso 
      Height          =   285
      Left            =   5715
      TabIndex        =   1
      Top             =   0
      Width           =   1770
      _Version        =   65537
      _ExtentX        =   3122
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483634
      MinDate         =   "1900/1/1"
      MaxDate         =   "3000/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin ComctlLib.TreeView tv 
      DragIcon        =   "DBConc.ctx":0038
      Height          =   3990
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2505
      _ExtentX        =   4419
      _ExtentY        =   7038
      _Version        =   327682
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
   End
   Begin ComctlLib.ImageList ImageList 
      Left            =   0
      Top             =   3780
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      UseMaskColor    =   0   'False
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   9
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":047A
            Key             =   "NodoCerrado"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":0794
            Key             =   "NodoAbierto"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":0AAE
            Key             =   "Hoja"
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":0DC8
            Key             =   "Editando"
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":10E2
            Key             =   "Borrado"
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":13FC
            Key             =   "Concierto"
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":1716
            Key             =   "Facturable"
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":1A30
            Key             =   "CatCerrado"
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "DBConc.ctx":1D4A
            Key             =   "CatAbierto"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblFechaProceso 
      AutoSize        =   -1  'True
      Caption         =   "Fecha de proceso"
      Height          =   195
      Left            =   4365
      TabIndex        =   5
      Top             =   45
      Width           =   1290
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      Caption         =   "lbl Drag_Drop"
      Height          =   195
      Left            =   900
      TabIndex        =   3
      Top             =   4050
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblXY 
      AutoSize        =   -1  'True
      Caption         =   "lblXY"
      Height          =   195
      Left            =   2355
      TabIndex        =   4
      Top             =   4050
      Visible         =   0   'False
      Width           =   945
   End
   Begin VB.Menu menuTv 
      Caption         =   "Definici�n de Conciertos"
      Begin VB.Menu mnuCambiarNombre 
         Caption         =   "Cambiar Nombre"
      End
      Begin VB.Menu mnuCrearNodoHijo 
         Caption         =   "Crear Nodo Hijo"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuCrearNodoPadre 
         Caption         =   "Crear Nodo Padre"
      End
      Begin VB.Menu mnuAtribHijos 
         Caption         =   "Poner Atributos a los hijos"
      End
      Begin VB.Menu mnuraya 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRelativoOrigen 
         Caption         =   "Hacer Relativo"
      End
      Begin VB.Menu mnuRaya2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuBorrar 
         Caption         =   "Borrar"
         Enabled         =   0   'False
      End
      Begin VB.Menu mnuCortar 
         Caption         =   "Cortar"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuCopiar 
         Caption         =   "Copiar"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuPegar 
         Caption         =   "Pegar"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
End
Attribute VB_Name = "DBConc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Base 1

Public Event NodoSeleccionado(codigo As String, nombre As String, editable As Boolean)
Public Event CambiarAtribHijos(codigo As String, nombre As String)
Public Event CrearRelacion(codigo As String, nombre As String)
Public Event CambiarConcierto()

Private rq(1 To 23) As New rdoQuery
Private cSrc$, cDst$

Public dragging As Boolean
Public FechaProceso As String
Private aNodosFacturables() As String

Private rdoConnect As rdoConnection
Private miObjApp As Object

Public indModificado As Boolean

Private Enum TipoDeNodo
  tnRaiz = 0
  tnNodo = 1
  tnCatRaiz = 2
  tnCatFinal = 3
  tnErrorNodo = 4
End Enum

Private Enum TipoDeArbol
  taConcierto = 0
  taCategoria = 1
  taConcMod = 2
End Enum


'El programa principal debe llamar a esta funci�n con ObjApp
'ya establecida.
Public Sub Inicializa(objapp As Object)
    mskHoraProceso.Text = "00:00:00"
    mskHoraProceso.Tag = "00:00:00"
    mskHoraProceso_LostFocus
    
    Set miObjApp = objapp
    Set rdoConnect = miObjApp.rdoConnect
    
    Call InitRQ
    
    tv.ImageList = ImageList
    
    tvCat.ImageList = ImageList
    
    tvConcMod.ImageList = ImageList
    
    ssTabTV.Tab = 0
    tvCat.Visible = True
    
    Call InitTVConcMod
    Call InitTVCat
End Sub

'Inicializacion de los rdoQuery
Private Sub InitRQ()
    Dim i%
'   Select de inicializacion de nodos (aquellos NODOS que son raices)
'   Parametros:
'            0 - Fecha Proceso
'            1 - Fecha Proceso
    rq(1).Sql = "Select FA09CODNODOCONC, FA09DESIG FROM FA0900, FA1500 WHERE FA09INDCONCIERTO=-1 AND " & _
                "FA15RUTA = '.' || FA09CODNODOCONC || './' AND " & _
                "FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN"
    
'   Select para recoger los nodos hijo
'   Parametros:
'            0 - Cod. Nodo Padre
'            1 - Fecha Proceso
'            2 - Fecha Proceso
    rq(2).Sql = "SELECT FA09CODNODOCONC_H, FA09DESIG, FA09INDFACTURABLE FROM FA1400, FA0900 " & _
                "WHERE FA09CODNODOCONC_H = FA09CODNODOCONC AND FA09CODNODOCONC_P = ? AND " & _
                "FA14FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA14FECFIN " & _
                "ORDER BY FA09DESIG"
                
'   Select para recoger las categorias hijas de un nodo
'   Parametros:
'            0 - Cod. Nodo Padre
'            1 - Fecha Proceso
'            2 - Fecha Proceso
    rq(3).Sql = "SELECT FA05CODCATEG_H, FA05DESIG FROM FA1400, FA0500 " & _
                "WHERE FA05CODCATEG_H = FA05CODCATEG AND FA09CODNODOCONC_P = ? AND " & _
                "FA14FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA14FECFIN " & _
                "ORDER BY FA05DESIG"
                
'   Select para recoger las categorias hijas de una categoria
'   Parametros:
'            0 - Cod. Categor�a Padre
'            1 - Fecha Proceso
'            2 - Fecha Proceso
    rq(4).Sql = "SELECT FA0500.FA05CODCATEG, FA05DESIG FROM FA0500, FA0600 " & _
                "WHERE FA0500.FA05CODCATEG = FA0600.FA05CODCATEG AND FA0600.FA05CODCATEG_P = ? AND " & _
                "FA06FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN " & _
                "ORDER BY FA05DESIG"

'   Select de inicializaci�n de Categor�as (aquellas que son raices)
'   Parametros:
'            0 - Fecha Proceso
'            1 - Fecha Proceso
    rq(5).Sql = "SELECT FA0500.FA05CODCATEG, FA05DESIG FROM FA0500, FA0600 " & _
                "WHERE FA0500.FA05CODCATEG = FA0600.FA05CODCATEG AND FA0600.FA05CODCATEG_P IS NULL AND " & _
                "FA06FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA06FECFIN "

                
    ' Crea nuevo Nodo de conciertos
    rq(6).Sql = "Insert into FA0900 (FA09CODNODOCONC, FA09DESIG, FA09INDCONCIERTO, FA09INDFACTURABLE) values (?, ?, 0, ?)"
    
    ' Crea nuevo RAMA de conciertos (el hijo es un nodo)
    rq(7).Sql = "Insert into FA1400 (FA09CODNODOCONC_P, FA09CODNODOCONC_H, FA05CODCATEG_H, FA14CODNODOCONC, FA14FECINICIO, FA14FECFIN) " & _
                "values (?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE('31/12/3000','DD/MM/YYYY'))"
    
    ' Crea nuevo RAMA de conciertos (el hijo es una categoria)
    rq(18).Sql = "Insert into FA1400 (FA09CODNODOCONC_P, FA09CODNODOCONC_H, FA05CODCATEG_H, FA14CODNODOCONC, FA14FECINICIO, FA14FECFIN) " & _
                "values (?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE('31/12/3000','DD/MM/YYYY'))"
    
'   Grabar atributos en vacio
'   Parametros:
'            0 - FA15CODATRIB (SEQUENCE)
'            1 - RUTA
'            2 - Fecha Inicio (es la variable FechaProceso)
    rq(8).Sql = "INSERT INTO FA1500 (" & _
                        "FA15CODATRIB," & _
                        "FA15RUTA," & _
                        "FA15FECINICIO," & _
                        "FA15FECFIN," & _
                        "FA15INDNECESARIO,FA15INDSUFICIENTE,FA15INDEXCLUIDO,FA15INDDESCONT," & _
                        "FA15INDLINOBLIG,FA15INDFACTOBLIG,FA15INDOPCIONAL, FA15INDFRANQSUMA, FA15INDFRANQUNI) " & _
                "VALUES (?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE('31/12/3000','DD/MM/YYYY'),0,0,0,1,0,0,0,0,0) "
    
'   Borrar un registro de atributos con fecha de fin=fecha de proceso
'   Parametros:
'            0 - Nueva Fecha Fin (es la variable FechaProceso)
'            1 - RUTA
'            2 - Fecha proceso
'            3 - Fecha proceso
    rq(9).Sql = "UPDATE FA1500 SET " & _
                        "FA15FECFIN = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                "WHERE FA15RUTA LIKE ? AND FA15CODCONC IS NOT NULL " & _
                        "AND ((FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                        "AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN ) " & _
                        "OR (TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') <= FA15FECINICIO ))"
                
    'Comprobaci�n de Referencias c�clica
    rq(10).Sql = "Select count(*) from dual where ? in (" & _
                 "Select FA09CODNODOCONC_H from FA1400 start with FA09CODNODOCONC_P = ? " & _
                 "Connect by prior FA09CODNODOCONC_H = FA09CODNODOCONC_P)"
           
'   Copiar los atributos de la rama del concierto modelo al destino
'   Parametros:
'            0 - Ruta origen sin barra final (si la tuviera)
'            1 - Ruta destino sin barra final (si la tuviera)
'            2 - Fecha Inicio (es la variable FechaProceso)
'            3 - ruta a buscar sin barra final (si la tuviera)
'            4 - Fecha Inicio (es la variable FechaProceso)
'            5 - Fecha Inicio (es la variable FechaProceso)
'            6 - Fecha Final(es la variable FechaProceso)
    rq(11).Sql = "INSERT INTO FA1500 (FA15CODATRIB, FA15RUTA, FA15PRECIOREF, FA15PRECIODIA, FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15PERIODOGARA, FA15FECINICIO, FA15FECFIN, FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, FA15INDDESCONT, FA15INDLINOBLIG, FA15INDFACTOBLIG, FA15INDOPCIONAL, FA15DESCUENTO, CI13CODENTIDAD, FA15INDDESGLOSE, FA15INDSUPLEMENTO, FA15DESCRIPCION, FA15RUTAREL, FA15RELFIJO, FA15RELPOR, FA15ORDIMP, FA15INDTRAMACUM, FA15INDTRAMDIAS, FA15PLAZOINTER ) " & _
                 "(SELECT FA15CODATRIB_SEQUENCE.NEXTVAL, REPLACE(FA15RUTA, ?, ?), FA15PRECIOREF, FA15PRECIODIA, FA15INDFRANQSUMA, FA15INDFRANQUNI, FA15PERIODOGARA, GREATEST(FA15FECINICIO,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')) , FA15FECFIN, FA15INDNECESARIO, FA15INDSUFICIENTE, FA15INDEXCLUIDO, FA15INDDESCONT, FA15INDLINOBLIG, FA15INDFACTOBLIG, FA15INDOPCIONAL, FA15DESCUENTO, CI13CODENTIDAD, FA15INDDESGLOSE, FA15INDSUPLEMENTO, FA15DESCRIPCION, FA15RUTAREL, FA15RELFIJO, FA15RELPOR, FA15ORDIMP, FA15INDTRAMACUM, FA15INDTRAMDIAS, FA15PLAZOINTER " & _
                 "FROM fa1500 where fa15ruta like ? AND (FA15FECINICIO >= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') OR (FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                 "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN))) "
                 
'   Copiar los atributos de la rama del concierto modelo al destino
'   Parametros:
'            0 - ruta a buscar sin barra final (si la tuviera)
'            1 - Fecha Inicio (es la variable FechaProceso)
'            2 - Fecha Inicio (es la variable FechaProceso)
'            3 - Fecha Final(es la variable FechaProceso)
'            4 - Ruta origen sin barra final (si la tuviera)
'            5 - Ruta destino sin barra final (si la tuviera)
    rq(23).Sql = "INSERT INTO FA0700 (FA15CODATRIB, FA07CODDESCUENT, FA07ACANTFIN, FA07PRECIO) " & _
                    "(SELECT FA1500_1.FA15CODATRIB, FA07CODDESCUENT, FA07ACANTFIN, FA07PRECIO " & _
                "FROM FA1500, FA1500 FA1500_1, FA0700 " & _
                "WHERE FA1500.FA15ruta LIKE ? " & _
                    "AND (FA1500.FA15FECINICIO >= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') OR (FA1500.FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA1500.FA15FECFIN)) " & _
                    "AND FA1500_1.FA15RUTA= REPLACE(FA1500.FA15RUTA, ?, ?) " & _
                    "AND FA1500_1.FA15FECFIN = FA1500.FA15FECFIN " & _
                    "AND FA0700.FA15CODATRIB = FA1500.FA15CODATRIB)"

                 
'   Borrar un registro de ramas de conciertos (fecha de fin=fecha de proceso)
'   cuando el hijo es un nodo
'   Parametros:
'            0 - Nueva Fecha Fin (es la variable FechaProceso)
'            1 - C�digo de nodo padre
'            2 - C�digo de nodo hijo
'            3 - Fecha proceso
    rq(13).Sql = "UPDATE FA1400 SET " & _
                 "FA14FECFIN=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                 "WHERE FA09CODNODOCONC_P =? AND FA09CODNODOCONC_H = ? AND FA14FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                 "AND FA14FECFIN = TO_DATE('31/12/3000','DD/MM/YYYY')"
    
'   Borrar un registro de ramas de conciertos (fecha de fin=fecha de proceso)
'   cuando el hijo es una categoria
'            0 - Nueva Fecha Fin (es la variable FechaProceso)
'            1 - C�digo de nodo padre
'            2 - C�digo de cat. hija
'            3 - Fecha proceso
    rq(14).Sql = "UPDATE FA1400 SET " & _
                        "FA14FECFIN=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                "WHERE FA09CODNODOCONC_P = ? AND FA05CODCATEG_H =? AND FA14FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
                        "AND FA14FECFIN =TO_DATE('31/12/3000','DD/MM/YYYY')"
    
  ' DEPURAR los atributos
  ' se ejecuta primero la 20 (precios por cantidad) y despues la 15 (atributos)
    rq(15).Sql = "DELETE FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN"
    rq(20).Sql = "DELETE FROM FA0700 WHERE FA15CODATRIB IN (SELECT FA15CODATRIB FROM FA1500 WHERE FA15FECINICIO >= FA15FECFIN)"
    ' DEPURAR las ramas de conciertos
    rq(16).Sql = "DELETE FROM FA1400 WHERE FA14FECINICIO >= FA14FECFIN"
    
    ' Recoger la fecha del sistema
    rq(17).Sql = "SELECT SYSDATE FROM DUAL"
    
    
'   Buscar las ramas de conciertos que tienen el codigo de padre determinado para colgarles
'   a todas ellas los atributos de la nueva rama que se crea en ese padre.
'   Parametros:
'            0 - C�digo del Padre
'            1 - Fecha proceso
'            2 - Fecha proceso
    rq(19).Sql = "SELECT FA15RUTA " & _
                 "FROM FA1500 " & _
                 "WHERE FA15CODFIN = ? AND FA15ESCAT = 0 AND " & _
                       "FA15FECINICIO <= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') AND " & _
                       "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') < FA15FECFIN"
                       
    ' comprobar que existan las rutas
    rq(21).Sql = "SELECT FA15RUTA " & _
                 "FROM FA1500 " & _
                 "WHERE FA15RUTA = ? "
             
'   Cambiar la descripci�n a un nodo de conciertos
'            0 - Nueva Descripci�n
'            1 - C�digo de nodo de conciertos a modificar
    rq(22).Sql = "UPDATE FA0900 SET FA09DESIG = ? " & _
                        "WHERE FA09CODNODOCONC = ?"
    
    ' preparar las query's en el servidor
    For i = 1 To UBound(rq)
        Set rq(i).ActiveConnection = miObjApp.rdoConnect
        rq(i).Prepared = True
    Next
                       
    
    ' preparar las query's en el servidor
    For i = 1 To UBound(rq)
        Set rq(i).ActiveConnection = miObjApp.rdoConnect
        rq(i).Prepared = True
    Next
End Sub

Public Sub InitTV(CodConcierto As Long, Desig As String)
    Dim n As Node
    tv.ImageList = ImageList
    
    tv.Nodes.Clear
    
    If Desig = "" Then Exit Sub
    
    Set n = tv.Nodes.Add(, , "." & CodConcierto & "./", Desig, "Concierto")
    n.Tag = CodConcierto
    tv.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
    
    ReDim Preserve aNodosFacturables(1)

End Sub

Public Sub InitTVConcMod()
    Dim Sql As String
    Dim rs As rdoResultset, n As Node
    tvConcMod.Nodes.Clear
     
    mskHoraProceso_LostFocus

    Sql = "Select FA09CODNODOCONC, FA09DESIG FROM FA0900, FA1500 WHERE FA09INDCONCIERTO=-1 AND " & _
                "FA15RUTA = '.' || FA09CODNODOCONC || './' AND " & _
                "FA15FECINICIO <= TO_DATE('" & FechaProceso & "','DD/MM/YYYY HH24:MI:SS') AND " & _
                "TO_DATE('" & FechaProceso & "','DD/MM/YYYY HH24:MI:SS') < FA15FECFIN"

'    rq(1).rdoParameters(0).Value = FechaProceso
'    rq(1).rdoParameters(1).Value = FechaProceso
    Set rs = rdoConnect.OpenResultset(Sql)
    
    While Not rs.EOF
      ' No crear nodos con el nombre nulo (vacio)
      If "" & rs(1) <> "" Then
        Set n = tvConcMod.Nodes.Add(, , "." & rs(0) & "./", rs(1), "Concierto")
        n.Tag = rs(0)
        tvConcMod.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
      End If
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
End Sub

Public Sub InitTVCat()
    Dim rs As rdoResultset, n As Node
    tvCat.Nodes.Clear
    
    mskHoraProceso_LostFocus
    
    rq(5).rdoParameters(0).Value = FechaProceso
    rq(5).rdoParameters(1).Value = FechaProceso
    
    Set rs = rq(5).OpenResultset
    While Not rs.EOF
        Set n = tvCat.Nodes.Add(, , "/." & rs(0) & ".", rs(1), "CatCerrado", "Editando")
        n.Tag = rs(0)
        tvCat.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
End Sub

Private Sub BorrarNodo(n As Node)
  Dim Ruta As String
  Dim Padre As String
  Dim NodoHijo As String
  Dim CatHija As String
  Dim FechaActual As String
  FechaActual = SysDate
  
'  If DateDiff("d", FechaActual, FechaProceso) < 0 Then
'    msgError ("No se pueden realizar modificaciones con fecha anterior a la actual")
'    Exit Sub
'  End If
  
  If FechaAntigua = True Then Exit Sub
  
  ' Comprobar que no se borra el raiz ni un categor�a intermedia
  
  ' es un nodo raiz
  If TipoNodo(n) = tnRaiz Then
    If MsgBox("� Desea de baja el Concierto: " & n & " con fecha " & Format(FechaProceso, "dd/mm/yyyy") & _
            " a las " & mskHoraProceso.Text & " ?", vbYesNo + vbDefaultButton2, "Borrar Concierto") = vbNo Then
            
      Exit Sub
    Else
      
      Padre = ""
      NodoHijo = n.Tag
      CatHija = ""
      
      Ruta = "." & NodoHijo & ".%"

    End If
    
  ' es un nodo intermedio
  ElseIf TipoNodo(n) = tnNodo Then
    If MsgBox("Se borrar� el nodo de todos los conciertos" & Chr(13) & Chr(10) & _
              "Para desactivarlo s�lo en este concierto marque el atributo 'Excluido'" & Chr(13) & Chr(10) & Chr(13) & Chr(10) & _
              "� Desea Borrar el Nodo: " & n & " con fecha " & Format(FechaProceso, "dd/mm/yyyy") & " ?", _
              vbYesNo + vbDefaultButton2, "Borrar Nodo") = vbNo Then
            
      Exit Sub
    
    Else
    
      Padre = n.Parent.Tag
      NodoHijo = n.Tag
      CatHija = ""
      
      Ruta = "%." & Padre & "." & NodoHijo & ".%/%"
    End If
    
  ' es una categor�a raiz
  ElseIf TipoNodo(n) = tnCatRaiz Then
    If MsgBox("Se borrar� la categor�a de todos los conciertos" & Chr(13) & Chr(10) & _
               "Para desactivarla s�lo en este concierto marque el atributo 'Excluido'" & Chr(13) & Chr(10) & Chr(13) & Chr(10) & _
                "� Desea Borrar la Categor�a: " & n & " con fecha " & Format(FechaProceso, "dd/mm/yyyy") & " ?", _
                vbYesNo + vbDefaultButton2, "Borrar Categor�a") = vbNo Then
            
      Exit Sub
    Else
      
      Padre = n.Parent.Tag
      NodoHijo = ""
      CatHija = n.Tag
      
      Ruta = "%." & Padre & "./." & CatHija & ".%"
    End If
    
  ' es una categoria intermedia
  ElseIf TipoNodo(n) = tnCatFinal Then
    msgError "No se puede modificar la estructura de categor�as"
    Exit Sub
  End If
  
    Screen.MousePointer = vbHourglass
    
  ' Iniciar transaccion
  miObjApp.BeginTrans
  On Error GoTo error

  ' Borrar logicamente los atributos del nodo o categoria
  rq(9).rdoParameters(0).Value = FechaProceso
  rq(9).rdoParameters(1).Value = Ruta   ' teniendo en cuenta la relacion padre-hijo
  rq(9).rdoParameters(2).Value = FechaProceso
  rq(9).rdoParameters(3).Value = FechaProceso
  rq(9).rdoParameters(4).Value = FechaProceso
  rq(9).Execute
  
  ' Borrar logicamente la rama del nodo o categoria
  If TipoNodo(n) = tnNodo Then
    rq(13).rdoParameters(0).Value = FechaProceso
    rq(13).rdoParameters(1).Value = Padre
    rq(13).rdoParameters(2).Value = NodoHijo
    rq(13).rdoParameters(3).Value = FechaProceso
    rq(13).Execute
  Else
    rq(14).rdoParameters(0).Value = FechaProceso
    rq(14).rdoParameters(1).Value = Padre
    rq(14).rdoParameters(2).Value = CatHija
    rq(14).rdoParameters(3).Value = FechaProceso
    rq(14).Execute
  End If
  tv.Nodes.Remove n.key
  
  ' Depurar los registros con fechas iguales
  DepurarBD
  
  ' Hay modificaciones que grabar antes de salir
  indModificado = True
  
  ' Ejecutar los cambios
  miObjApp.CommitTrans

    Screen.MousePointer = vbDefault
Exit Sub
error:
  MsgBox "Error : " & Err.Number & " " & Err.Description
    
    ' Deshacer los cambios
    miObjApp.RollbackTrans
End Sub

Private Sub CrearNodoHijo(nodoPadre As Node)
    Dim Ruta As String
    Dim Padre As String
    Dim Hijo As String
    Dim Desig As String
    Dim Facturable As Integer
    
    Dim n As Node
    Dim FechaActual As String
    FechaActual = SysDate
    
'    If DateDiff("d", FechaActual, FechaProceso) < 0 Then
'      msgError ("No se pueden realizar modificaciones con fecha anterior a la actual")
'      Exit Sub
'    End If
    
    If FechaAntigua = True Then Exit Sub

    If TipoNodo(nodoPadre) = tnErrorNodo Then
        msgError "No hay ning�n nodo seleccionado"
        Exit Sub
    End If
    
    If TipoNodo(nodoPadre) <> tnRaiz And _
        TipoNodo(nodoPadre) <> tnNodo Then
        msgError "No se puede a�adir un nodo a una categor�a"
        Exit Sub
    Else
    
        Padre = nodoPadre.Tag
        Desig = InputBox("Descripci�n : ", "Crear Nuevo nodo")
        If Desig <> "" Then
            
            ' Iniciar transaccion
            miObjApp.BeginTrans
            On Error GoTo error
            
            Hijo = fNextClave("FA09CODNODOCONC", "FA0900")
            
            If EsNodoFacturable(Left(nodoPadre.key, Len(nodoPadre.key) - 1), Hijo) Then
                Facturable = -1
            Else
                Facturable = 0
            End If
            
            Call Expand(tv, nodoPadre)
            
            ' Insertar en FA0900 el nuevo nodo
            rq(6).rdoParameters(0).Value = Hijo
            rq(6).rdoParameters(1).Value = Desig
            rq(6).rdoParameters(2).Value = Facturable
            rq(6).Execute
            
            
            ' Insertar en FA1400 la rama
            Call GrabarRama(Padre, Hijo, "")
            
            ' Crear sus atributos en vacio
            ' con fecha de proceso elegida y de fin = "31/12/3000"
            Ruta = Left(nodoPadre.key, Len(nodoPadre.key) - 1) & Hijo & "./"
            Call CrearAtribVacio(Ruta)
            
            ' Copiar los atributos al resto de conciertos
            Call CopiarAtrib(Ruta, Padre, Hijo)
            
            ' Crear la rama en el TreeView
'            Call Expand(tv, nodoPadre)
            If Facturable = -1 Then
                Set n = tv.Nodes.Add(nodoPadre.key, tvwChild, Ruta, Desig, "Facturable", "Editando")
            Else
                Set n = tv.Nodes.Add(nodoPadre.key, tvwChild, Ruta, Desig, "NodoCerrado", "Editando")
            End If
            
            n.Tag = Hijo
            tv.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
            
            tv.SelectedItem = n
  
            ' Hay modificaciones que grabar antes de salir
            indModificado = True
                                            
            ' Ejecutar los cambios
            miObjApp.CommitTrans
        End If
    End If
Exit Sub

error:
    
    msgError ("Error al crear el nodo")
    ' Deshacer los cambios
    miObjApp.RollbackTrans
'    Resume
End Sub

Private Sub CrearNodoPadre(NodoHijo As Node)
  Dim Padre As String
  Dim Hijo As String
  Dim Desig As String
  Dim n As Node
  Dim FechaActual As String
  FechaActual = SysDate
  
'  If DateDiff("d", FechaActual, FechaProceso) < 0 Then
'    msgError ("No se pueden realizar modificaciones con fecha anterior a la actual")
'    Exit Sub
'  End If
    
  If FechaAntigua = True Then Exit Sub

  If TipoNodo(NodoHijo) = tnErrorNodo Then
      msgError "No hay ning�n nodo seleccionado"
      Exit Sub
  End If
  
  If TipoNodo(NodoHijo) <> tnCatRaiz And _
      TipoNodo(NodoHijo) <> tnNodo Then
      msgError "No se puede a�adir un nodo a una categor�a"
      Exit Sub
  Else
        
    Screen.MousePointer = vbHourglass
    
    miObjApp.BeginTrans
    
    On Error GoTo error
    Call CrearNodoHijo(NodoHijo.Parent)
    
    Call AppendHija(tv.SelectedItem.Tag, tv.SelectedItem.key, NodoHijo.Tag, NodoHijo.Text, tv)
    
    Call BorrarNodo(NodoHijo)
  
    ' Ejecutar los cambios
    miObjApp.CommitTrans

    Screen.MousePointer = vbDefault
  End If
  
Exit Sub

error:
    ' Deshacer los cambios
    miObjApp.RollbackTrans
    
End Sub

Private Sub PegarNodo(nodoPadre As Node, NodoHijo As Node)
  Dim Padre As String
  Dim Hijo As String
  Dim Desig As String
  Dim n As Node
  
  If TipoNodo(nodoPadre) = tnErrorNodo Then
    msgError "No hay ning�n nodo seleccionado"
    Exit Sub
  End If
  
  If TipoNodo(nodoPadre) <> tnRaiz And _
    TipoNodo(nodoPadre) <> tnNodo Then
    msgError "No se puede a�adir un nodo a una categor�a"
    Exit Sub
  Else
    Padre = nodoPadre.Tag
      
    ' Iniciar transaccion
    miObjApp.BeginTrans
    On Error GoTo error
    
    Hijo = NodoHijo.Tag
    
    ' Insertar en FA1400 la rama
    Call GrabarRama(Padre, Hijo, "")
    
    ' Crear sus atributos en vacio
    ' con fecha de proceso elegida y de fin = "31/12/3000"
    Call CrearAtribVacio(Left(nodoPadre.key, Len(nodoPadre.key) - 1) & Hijo & "./")
    
    ' Crear la rama en el TreeView
    Call Expand(tv, nodoPadre)
    Set n = tv.Nodes.Add(nodoPadre.key, tvwChild, Left(nodoPadre.key, Len(nodoPadre.key) - 1) & Hijo & "./", Desig, "NodoCerrado", "Editando")
    n.Tag = Hijo
    tv.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"

    ' Hay modificaciones que grabar antes de salir
    indModificado = True
              
    ' Ejecutar los cambios
    miObjApp.CommitTrans
  End If
Exit Sub

error:
  ' Deshacer los cambios
  miObjApp.RollbackTrans
End Sub

Private Sub AppendHija(Padre$, padreKey$, Hijo$, hijoText$, Source As Control)
    Dim n As Node, rs As rdoResultset
    Dim str As String
    Dim RutaOrigen As String


        Screen.MousePointer = vbHourglass
        
    On Error Resume Next
    Set n = tv.Nodes.Item(padreKey & "dummy")
    str = n.key
    If str <> "" Then Call Expand(tv, tv.Nodes(padreKey))

    On Error GoTo 0
    
    ' Iniciar transaccion
    miObjApp.BeginTrans
    
    ' SE A�ADE DESDE EL ARBOL DE CONCIERTOS
    If Source.Name = "tvConcMod" Or Source.Name = "tv" Then
    
        RutaOrigen = Source.HitTest(lblXY.Left, lblXY.Top).key
        
        If TipoNodo(Source.HitTest(lblXY.Left, lblXY.Top)) = tnNodo Then
            
            ' ES UN NODO, HAY QUE CONTROLAR QUE NO SEA = QUE EL PADRE
            If Padre = Hijo Then
                GoSub RefCircular
            End If
            
            ' VALIDAR QUE NO SE CREEN REFERENCIAS CIRCULARES
            rq(10).rdoParameters(0) = Padre
            rq(10).rdoParameters(1) = Hijo
            
            ' Si da error es que hay Referencias circulares en la base de Datos
            On Error GoTo RefCircular
            Set rs = rq(10).OpenResultset
            
            If rs(0) <> 0 Then
                GoSub RefCircular
            End If
            
            ' ES CORRECTO SE PUEDE A�ADIR AL ARBOL
            On Error GoTo YaEsHijo
            Set n = tv.Nodes.Add(padreKey, tvwChild, Left(padreKey, Len(padreKey) - 1) & Hijo & "./", hijoText, "NodoCerrado", "Editando")
            n.Tag = Hijo
            tv.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
            
            ' ES CORRECTO SE PUEDE GRABAR
            On Error GoTo ErrorGrabacion
            Call GrabarRama(Padre, Hijo, "")
            Call CopiarAtrib(RutaOrigen, Padre, Hijo)
            Call DepurarBD
        
        Else
            ' ES CORRECTO SE PUEDE A�ADIR AL ARBOL
            On Error GoTo YaEsHijo
            Set n = tv.Nodes.Add(padreKey, tvwChild, padreKey & "." & Hijo & ".", hijoText, "CatCerrado", "Editando")
            n.Tag = Hijo
            tv.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
            
            ' ES CORRECTO SE PUEDE GRABAR
            On Error GoTo ErrorGrabacion
            Call GrabarRama(Padre, "", Hijo)
            Call CopiarAtrib(RutaOrigen, Padre, Hijo)
            Call DepurarBD
        
        End If
        
    ' SE A�ADE DESDE EL ARBOL DE CATEGORIAS
    Else
        Set n = tvCat.HitTest(lblXY.Left, lblXY.Top)
        RutaOrigen = n.key
    
        ' ES CORRECTO SE PUEDE A�ADIR AL ARBOL
        On Error GoTo YaEsHijo
        Set n = tv.Nodes.Add(padreKey, tvwChild, padreKey & "." & Hijo & ".", hijoText, "CatCerrado", "Editando")
        n.Tag = Hijo
        tv.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
            
        ' ES CORRECTO SE PUEDE GRABAR
        On Error GoTo ErrorGrabacion
        Call GrabarRama(Padre, "", Hijo)
        Call CopiarAtrib(RutaOrigen, Padre, Hijo)
        Call DepurarBD
    
    End If
    
    ' Ejecutar los cambios
    miObjApp.CommitTrans

    On Error GoTo 0
    
        Screen.MousePointer = vbDefault
Exit Sub
RefCircular:
    On Error GoTo 0
    msgError "No se puede a�adir como hijo" & Chr(10) & Chr(13) & Chr(10) & Chr(13) & _
             "Se crear�a una Referencia circular."
    
    miObjApp.RollbackTrans
    Exit Sub
    
YaEsHijo:
    On Error GoTo 0
    msgError "No se puede a�adir como hijo" & Chr(10) & Chr(13) & Chr(10) & Chr(13) & _
             "Ya es hijo de ese nodo."
'             Resume
    miObjApp.RollbackTrans
    Exit Sub
    
ErrorGrabacion:
    Exit Sub
End Sub


Private Sub mnuAtribHijos_Click()
'    CrearAtributosArbol
    RaiseEvent CambiarAtribHijos(tv.SelectedItem.key, tv.SelectedItem.Text)
End Sub

Private Sub mnuBorrar_Click()
  Borrar
End Sub

Private Sub mnuCambiarNombre_Click()
    tv.StartLabelEdit
End Sub

Private Sub mnuCrearNodoHijo_Click()
  Nuevo
End Sub

Private Sub mnuCrearNodoPadre_Click()
  Call CrearNodoPadre(tv.SelectedItem)
End Sub

Private Sub mnuRelativoOrigen_Click()
    RaiseEvent CrearRelacion(tv.SelectedItem.key, tv.SelectedItem.Text)
End Sub

Private Sub mskHoraProceso_LostFocus()
  Dim Hora As String
  
  Hora = mskHoraProceso.ClipText
  
  If Val(Left(Hora, 2)) < 0 Or Val(Left(Hora, 2)) > 23 Then
    mskHoraProceso.Text = "00:00:00"
  
  ElseIf Val(Mid(Hora, 3, 2)) < 0 Or Val(Mid(Hora, 3, 2)) > 59 Then
    mskHoraProceso.Text = "00:00:00"
  
  ElseIf Val(Right(Hora, 2)) < 0 Or Val(Right(Hora, 2)) > 59 Then
    mskHoraProceso.Text = "00:00:00"
  End If
  
  FechaProceso = sdcFechaProceso & " " & mskHoraProceso
  
  If mskHoraProceso.Text = mskHoraProceso.Tag Then
    Exit Sub
  Else
    mskHoraProceso.Tag = mskHoraProceso.Text
  End If
  
  
  RaiseEvent CambiarConcierto
  
  Call InitTV(tv.Nodes(1).Tag, tv.Nodes(1).Text)
  Call InitTVCat
  Call InitTVConcMod
End Sub

Private Sub sdcFechaProceso_Change()
  On Error Resume Next
  
  Screen.MousePointer = vbHourglass
  
  mskHoraProceso.Text = "00:00:00"
  mskHoraProceso.Tag = "00:00:00"
  
  FechaProceso = sdcFechaProceso & " " & mskHoraProceso
  
  If sdcFechaProceso.Tag = sdcFechaProceso.Text Then
    Exit Sub
  Else
    sdcFechaProceso.Tag = sdcFechaProceso.Text
  End If

  RaiseEvent CambiarConcierto
  
  Call InitTV(tv.Nodes(1).Tag, tv.Nodes(1).Text)
  Call InitTVCat
  Call InitTVConcMod
  
  MsgBox "ATENCION !" & Chr$(10) & Chr$(13) & _
         "Las modificaciones se grabar�n con fecha :" & Chr$(10) & Chr$(13) & Chr$(10) & Chr$(13) & _
         "                       " & sdcFechaProceso & Chr$(10) & Chr$(13) & Chr$(10) & Chr$(13) & _
         "Si no est� de acuerdo modifique la fecha de proceso", vbCritical + vbOKOnly, "Comprobar la fecha de proceso"
  
  Screen.MousePointer = vbDefault
End Sub

Private Sub sdcFechaProceso_CloseUp()
  sdcFechaProceso_Change
End Sub

Private Sub ssTabTV_Click(PreviousTab As Integer)
    If ssTabTV.Tab = 0 Then
        tvCat.Visible = True
        tvConcMod.Visible = False
    Else
        tvCat.Visible = False
        tvConcMod.Visible = True
    End If
End Sub

Private Sub tv_AfterLabelEdit(Cancel As Integer, NewString As String)
    
    On Error GoTo error
    
    ' confirmar que se quiere cambiar el nombre
    If MsgBox("� Est� seguro de cambiar el nombre ?" & Chr(10) & Chr(13) & Chr(10) & Chr(13) & _
              "Nombre Actual : " & tv.SelectedItem.Text & Chr(10) & Chr(13) & _
              "Nombre Nuevo : " & NewString _
            , vbYesNo, "Cambiar nombre a un nodo") = vbYes Then
        
        ' Iniciar transaccion
        miObjApp.BeginTrans
            
        ' ejecutar la SQL de cambio de nombre
        rq(22).rdoParameters(0).Value = NewString
        rq(22).rdoParameters(1).Value = tv.SelectedItem.Tag
        rq(22).Execute
        
        ' Ejecutar los cambios
        miObjApp.CommitTrans
    
    Else
        Cancel = True
    End If
    
Exit Sub

error:
    MsgBox "No se ha podido cambiar el Nombre", vbCritical + vbOKOnly, "Error"
    Cancel = True
    
    ' Deshacer los cambios
    miObjApp.RollbackTrans
    
End Sub

Private Sub tv_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  tv.Drag vbEndDrag
End Sub

Private Sub tvCat_DragDrop(Source As Control, X As Single, Y As Single)
  Source.Drag 0
End Sub

Private Sub tvCat_NodeClick(ByVal Node As ComctlLib.Node)
  RaiseEvent NodoSeleccionado(Node.key, Node.Text, False)
End Sub

Private Sub tvConcMod_DragDrop(Source As Control, X As Single, Y As Single)
  Source.Drag 0
End Sub

Private Sub tv_Collapse(ByVal Node As ComctlLib.Node)
    Call Collapse(tv, Node)
End Sub

Private Sub tvCat_Collapse(ByVal Node As ComctlLib.Node)
    Call Collapse(tvCat, Node)
End Sub

Private Sub tvConcMod_Collapse(ByVal Node As ComctlLib.Node)
    Call Collapse(tvConcMod, Node)
End Sub

Private Sub tv_Expand(ByVal Node As ComctlLib.Node)
    Call Expand(tv, Node)
End Sub

Private Sub tvConcMod_Expand(ByVal Node As ComctlLib.Node)
    Call Expand(tvConcMod, Node)
End Sub

Private Sub tvCat_Expand(ByVal Node As ComctlLib.Node)
    Call Expand(tvCat, Node)
End Sub

Private Sub tv_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then 'tecla suprimir
        Call BorrarNodo(tv.SelectedItem)
    End If
End Sub

Private Sub tv_NodeClick(ByVal Node As ComctlLib.Node)
    Screen.MousePointer = vbHourglass
    RaiseEvent NodoSeleccionado(Node.key, Node.Text, True)
    Screen.MousePointer = vbDefault
End Sub

Private Sub tv_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  On Error GoTo IsNothing
  
  tv.SelectedItem = tv.HitTest(X, Y)
'  Call tv_NodeClick(tv.SelectedItem)
  
  If Button = vbRightButton Then
    lblXY.Left = X
    lblXY.Top = Y
    
    dragging = False
    
    Select Case TipoNodo(tv.SelectedItem)
      Case tnRaiz
        mnuBorrar.Enabled = True
        mnuCrearNodoHijo.Enabled = True
        mnuCrearNodoPadre.Enabled = False
        mnuCopiar.Enabled = False
        mnuCortar.Enabled = False
        mnuPegar.Enabled = False
        mnuCambiarNombre.Enabled = True
      Case tnNodo
        mnuBorrar.Enabled = True
        mnuCrearNodoHijo.Enabled = True
        mnuCrearNodoPadre.Enabled = False
        mnuCopiar.Enabled = False
        mnuCortar.Enabled = False
        mnuPegar.Enabled = False
        mnuCambiarNombre.Enabled = True
    Case tnCatRaiz
        mnuBorrar.Enabled = True
        mnuCrearNodoHijo.Enabled = False
        mnuCrearNodoPadre.Enabled = False
        mnuCopiar.Enabled = False
        mnuCortar.Enabled = False
        mnuPegar.Enabled = False
        mnuCambiarNombre.Enabled = False
    Case tnCatFinal
        mnuBorrar.Enabled = False
        mnuCrearNodoHijo.Enabled = False
        mnuCrearNodoPadre.Enabled = False
        mnuCopiar.Enabled = False
        mnuCortar.Enabled = False
        mnuPegar.Enabled = False
        mnuCambiarNombre.Enabled = False
      Case tnErrorNodo
        Exit Sub
    End Select
    PopupMenu menuTv
  Else
  
  End If
  Exit Sub
  
IsNothing:
  tv.Drag 0
End Sub

Private Sub tv_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 0 Then
    tv.Drag 0
  End If
End Sub

Private Sub tvConcMod_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  On Error GoTo IsNothing
    
    ' Si se pulsa el boton derecho salir de este evento para que se ejecute el
    ' evento NodeClick y aparezcan los atributos del nodo
    If Button <> vbLeftButton Then
        Exit Sub
    End If
    

  If TipoNodo(tvConcMod.HitTest(X, Y)) = tnRaiz Then
      Exit Sub
  End If
  
  tvConcMod.SelectedItem = tvConcMod.HitTest(X, Y)
'  Call tvConcMod_NodeClick(tvConcMod.SelectedItem)
  
  cSrc = tvConcMod.HitTest(X, Y).Tag
  
  lblXY.Top = Y
  lblXY.Left = X

  tvConcMod.DragIcon = ImageList.ListImages(tvConcMod.HitTest(X, Y).Image).Picture

  tvConcMod.Drag vbBeginDrag
  Exit Sub
  
IsNothing:
  tvConcMod.Drag 0
End Sub

Private Sub tvConcMod_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 0 Then
    tvConcMod.Drag 0
  End If
End Sub

Private Sub tvCat_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo IsNothing
    
    ' Si se pulsa el boton derecho salir de este evento para que se ejecute el
    ' evento NodeClick y aparezcan los atributos del nodo
    If Button <> vbLeftButton Then
        Exit Sub
    End If
    
    tvCat.SelectedItem = tvCat.HitTest(X, Y)
'    Call tvCat_NodeClick(tvCat.SelectedItem)
    
    If TipoNodo(tvCat.HitTest(X, Y)) = tnCatRaiz Then
        Exit Sub
    End If
    
    cSrc = tvCat.HitTest(X, Y).Tag
    
    lblXY.Top = Y
    lblXY.Left = X

    tvCat.DragIcon = ImageList.ListImages(tvCat.HitTest(X, Y).Image).Picture
    
    tvCat.Drag vbBeginDrag
    Exit Sub
IsNothing:
    tvCat.Drag 0
End Sub

Private Sub tvCat_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 0 Then
    tvCat.Drag 0
  End If
End Sub

Private Sub tvCat_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    tvCat.Drag vbEndDrag
End Sub

Private Sub tv_DragDrop(Source As Control, X As Single, Y As Single)
    Dim TipoNodoDest As TipoDeNodo
    Dim FechaActual As String
    
    FechaActual = SysDate
    
'    If DateDiff("d", FechaActual, FechaProceso) < 0 Then
'      msgError ("No se pueden realizar modificaciones con fecha anterior a la actual")
'      Exit Sub
'    End If
    
    If FechaAntigua = True Then Exit Sub

    TipoNodoDest = TipoNodo(tv.HitTest(X, Y))
    ' Solo se pueden insertar nodos o en el raiz o en un nodo intermedio
    If TipoNodoDest <> tnRaiz And TipoNodoDest <> tnNodo Then
        If TipoNodoDest = tnErrorNodo Then
            msgError "El nodo seleccionado no es correcto."
        Else
            msgError "No se pueden a�adir hijos a categor�as" & Chr(10) & Chr(13) & Chr(10) & Chr(13) & _
                     "Esto modificar�a el arbol de categorias."
        End If
        
        Exit Sub
    End If
    
        On Error GoTo IsNothing
        cDst = tv.HitTest(X, Y).Tag
        If Source.Name = "tvConcMod" Then
            ' se a�ade desde el arbol de conciertos
            Call AppendHija(cDst, tv.HitTest(X, Y).key, cSrc, tvConcMod.HitTest(lblXY.Left, lblXY.Top).Text, Source)
        
        ElseIf Source.Name = "tvCat" Then
            ' se a�ade desde el arbol de categorias
            Call AppendHija(cDst, tv.HitTest(X, Y).key, cSrc, tvCat.HitTest(lblXY.Left, lblXY.Top).Text, Source)
        End If
            
        ' Hay modificaciones que grabar antes de salir
        indModificado = True
                      
    Screen.MousePointer = vbDefault
        
IsNothing:
    
    lblXY.Top = Y
    lblXY.Left = X
    
    Screen.MousePointer = 0
End Sub


Private Sub tvConcMod_NodeClick(ByVal Node As ComctlLib.Node)
  RaiseEvent NodoSeleccionado(Node.key, Node.Text, False)
End Sub

'El TreeView Ocupa todo el control
Private Sub UserControl_Resize()
    
    tv.Left = 0
    tv.Top = 0
    tv.Height = UserControl.Height
    tv.Width = UserControl.Width / 2 - 15
    
    ssTabTV.Left = UserControl.Width / 2 + 15
    ssTabTV.Top = 310
    ssTabTV.Height = tv.Height - 310
    ssTabTV.Width = tv.Width
    
    tvCat.Left = ssTabTV.Left + 60
    tvCat.Top = ssTabTV.Top + 360
    tvCat.Height = ssTabTV.Height - 420
    tvCat.Width = ssTabTV.Width - 120
    
'    tvConcMod.Left = 60
'    tvConcMod.Top = 360
'    tvConcMod.Height = ssTabTV.Height - 420
'    tvConcMod.Width = ssTabTV.Width - 120
    
    tvConcMod.Left = tvCat.Left
    tvConcMod.Top = tvCat.Top
    tvConcMod.Height = tvCat.Height
    tvConcMod.Width = tvCat.Width
    
    mskHoraProceso.Left = UserControl.Width - mskHoraProceso.Width
    sdcFechaProceso.Left = UserControl.Width - sdcFechaProceso.Width - 30
    lblFechaProceso.Left = sdcFechaProceso.Left - lblFechaProceso.Width - 45
End Sub

Private Sub Collapse(TreeView As TreeView, ByVal Node As ComctlLib.Node)
    Screen.MousePointer = vbHourglass
    
    If Node.Image = "NodoAbierto" Then
        Node.Image = "NodoCerrado"
    Else
        If Node.Image = "CatAbierto" Then
            Node.Image = "CatCerrado"
        End If
    End If
    
    Screen.MousePointer = vbDefault
End Sub

Private Sub Expand(TreeView As TreeView, ByVal Node As ComctlLib.Node)
    Dim n As Node, rs As rdoResultset, str$
    Dim PosBarra As Integer
    Dim PosPunto As Integer
    Dim CatRaiz As Long
    
    On Error Resume Next
    
    Screen.MousePointer = vbHourglass
    
    If Node.Image = "NodoCerrado" Then
        Node.Image = "NodoAbierto"
    Else
        If Node.Image = "CatCerrado" Then
            Node.Image = "CatAbierto"
        End If
    End If
    
    Err.Clear
    Set n = TreeView.Nodes.Item(Node.key & "dummy")
    str = n.key
    On Error GoTo 0
    If str <> "" Then
        ' es la primera vez que se abre
        
        If TipoNodo(Node) = tnRaiz Or TipoNodo(Node) = tnNodo Then
        ' Todav�a es una condicion de concierto
            TreeView.Nodes.Remove n.key
            
            ' Buscar sus nodos hijos
            rq(2).rdoParameters(0).Value = Node.Tag
            rq(2).rdoParameters(1).Value = FechaProceso
            rq(2).rdoParameters(2).Value = FechaProceso
            
            Set rs = rq(2).OpenResultset
            While Not rs.EOF
                If rs(2).Value = 0 Then
                    ' No es facturable -> Poner icono de nodo cerrado
                    Set n = TreeView.Nodes.Add(Node, tvwChild, Left(Node.key, Len(Node.key) - 1) & rs(0) & "./", rs(1), "NodoCerrado", "Editando")
                Else
                    ' Es facturable -> Poner icono de Nodo Facturable
                    Set n = TreeView.Nodes.Add(Node, tvwChild, Left(Node.key, Len(Node.key) - 1) & rs(0) & "./", rs(1), "Facturable", "Editando")
                    
                    If TreeView.Name = "tv" Then
                        ReDim Preserve aNodosFacturables(UBound(aNodosFacturables) + 1)
                        aNodosFacturables(UBound(aNodosFacturables)) = Left(Node.key, Len(Node.key) - 1) & rs(0) & "."
                    End If
                End If
                n.Tag = rs(0)
                TreeView.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"

                rs.MoveNext
            Wend
            rs.Close
            
            ' Buscar sus categorias hijas
            rq(3).rdoParameters(0).Value = Node.Tag
            rq(3).rdoParameters(1).Value = FechaProceso
            rq(3).rdoParameters(2).Value = FechaProceso
            Set rs = rq(3).OpenResultset
            While Not rs.EOF
'                Debug.Print rs(0), rs(1)
                Set n = TreeView.Nodes.Add(Node, tvwChild, Left(Node.key, Len(Node.key) - 1) & "/." & rs(0) & ".", rs(1), "CatCerrado", "Editando")
                n.Tag = rs(0)
                TreeView.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
                rs.MoveNext
                
            Wend
            rs.Close
        Else
            ' empezamos a buscar en categor�as
            TreeView.Nodes.Remove n.key

            rq(4).rdoParameters(0).Value = Node.Tag
            rq(4).rdoParameters(1).Value = FechaProceso
            rq(4).rdoParameters(2).Value = FechaProceso
            
            Set rs = rq(4).OpenResultset
            While Not rs.EOF
                Set n = TreeView.Nodes.Add(Node, tvwChild, Node.key & rs(0) & ".", rs(1), "CatCerrado", "Editando")
                n.Tag = rs(0)
                TreeView.Nodes.Add n, tvwChild, n.key & "dummy", "dummy"
                rs.MoveNext
            Wend
            rs.Close
        
        End If
    End If
    
    Screen.MousePointer = vbDefault
End Sub

Public Function fNextClave(campo$, tabla$) As String
'    'Devuelve el siguiente valor de la clave principal obtenido del sequence
    Dim Sql$, rsMaxClave As rdoResultset
    
    Sql = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = rdoConnect.OpenResultset(Sql)
    fNextClave = rsMaxClave(0)
    rsMaxClave.Close
End Function

Private Function TipoNodo(n As Node) As TipoDeNodo
    On Error GoTo ErrorNodo
    Dim key As String
    
    key = n.key
    
    ' es un nodo
    If n.key Like "*/" Then
        If n.key Like ".*.*.*/" Then
            ' es un nodo intermedio
            TipoNodo = tnNodo
        Else
            ' es un raiz de concierto
            TipoNodo = tnRaiz
        End If
        
    ' es una categor�a
    Else
        If n.key Like "*/*.*.*." Then
            ' es categor�a intermedia o final
            TipoNodo = tnCatFinal
        Else
            ' es categor�a raiz
            TipoNodo = tnCatRaiz
        End If
    End If
    Exit Function

ErrorNodo:
    TipoNodo = tnErrorNodo
End Function

Private Sub GrabarRama(Padre$, NodoHijo As Variant, CatHija As Variant)
  
    On Error GoTo error
    ' Crear la rama de conciertos con un hijo que es NODO
    rq(7).rdoParameters(0).Value = Padre
    rq(7).rdoParameters(1).Value = NodoHijo
    rq(7).rdoParameters(2).Value = CatHija
    rq(7).rdoParameters(3).Value = fNextClave("FA14CODNODOCONC", "FA1400")
    rq(7).rdoParameters(4).Value = FechaProceso
    rq(7).Execute
    
    ' Hay modificaciones que grabar antes de salir
    indModificado = True
  
Exit Sub
error:
  MsgBox "Error : " & Err.Number & " " & Err.Description
  
End Sub

Public Sub CrearAtribVacio(Ruta As String)
    On Error GoTo error
    
    ' Crear los atributos de un nuevo nodo en vacio
    rq(8).rdoParameters(0) = fNextClave("FA15CODATRIB", "FA1500")
    rq(8).rdoParameters(1) = Ruta
    rq(8).rdoParameters(2) = FechaProceso
    
    rq(8).Execute
    
    ' Hay modificaciones que grabar antes de salir
    indModificado = True
  
Exit Sub
error:
    MsgBox "Error : " & Err.Number & " " & Err.Description
  
End Sub

Private Sub CopiarAtrib(RutaOrigen As String, Padre As String, Hijo As String)
  Dim rs As rdoResultset
  Dim Pos As Long
  
  On Error GoTo error
  
  Screen.MousePointer = vbHourglass
      
  ' Seleccionar los nodos que contengan ese padre para coger su
  ' ruta y colgarles el nuevo nodo
  rq(19).rdoParameters(0).Value = Padre
  rq(19).rdoParameters(1).Value = FechaProceso
  rq(19).rdoParameters(2).Value = FechaProceso
  Set rs = rq(19).OpenResultset
  
  Do While Not rs.EOF
    
    ' a�adir los atributos del nuevo nodo a partir de esa ruta
    
    If InStr(RutaOrigen, "/") = Len(RutaOrigen) Then
      ' Tiene la barra en el ultimo lugar hay que borrarla
      rq(11).rdoParameters(0).Value = Left(RutaOrigen, Len(RutaOrigen) - 1)
      rq(11).rdoParameters(1).Value = Left(rs(0).Value, Len(rs(0).Value) - 1) & Hijo & "."
      rq(11).rdoParameters(3).Value = Left(RutaOrigen, Len(RutaOrigen) - 1) & "%"
      
      ' Tiene la barra en el ultimo lugar hay que borrarla
      rq(23).rdoParameters(4).Value = Left(RutaOrigen, Len(RutaOrigen) - 1)
      rq(23).rdoParameters(5).Value = Left(rs(0).Value, Len(rs(0).Value) - 1) & Hijo & "."
      rq(23).rdoParameters(0).Value = Left(RutaOrigen, Len(RutaOrigen) - 1) & "%"
    Else
      rq(11).rdoParameters(0).Value = RutaOrigen
      rq(11).rdoParameters(1).Value = rs(0).Value & "." & Hijo & "."
      rq(11).rdoParameters(3).Value = RutaOrigen & "%"
      
      rq(23).rdoParameters(4).Value = RutaOrigen
      rq(23).rdoParameters(5).Value = rs(0).Value & "." & Hijo & "."
      rq(23).rdoParameters(0).Value = RutaOrigen & "%"
    End If
    
    ' Si la ruta de origen y destino son iguales no hay que copiar los atributos
    If rq(11).rdoParameters(0).Value <> rq(11).rdoParameters(1).Value Then
      rq(11).rdoParameters(2) = FechaProceso
      
      rq(11).rdoParameters(4) = FechaProceso
      rq(11).rdoParameters(5) = FechaProceso
      rq(11).rdoParameters(6) = FechaProceso
      rq(11).Execute
      
      rq(23).rdoParameters(1) = FechaProceso
      rq(23).rdoParameters(2) = FechaProceso
      rq(23).rdoParameters(3) = FechaProceso
      rq(23).Execute
      
    End If
    
    rs.MoveNext
    ' Hay modificaciones que grabar antes de salir
    indModificado = True
  Loop
  
    Screen.MousePointer = vbDefault
Exit Sub
error:
    MsgBox "Error : " & Err.Number & " " & Err.Description
'Resume
End Sub

Private Sub msgError(txt$)
  ' Muestra un mensaje de error
  MsgBox txt, vbCritical + vbOKOnly, "Definici�n de Conciertos"
End Sub

Public Sub DepurarBD()
  ' Depurar las tablas de Atributos y Ramas de concierto para que no queden
  ' registros con fecha de inicio >= fecha de final

    On Error GoTo error
  ' Depurar Atributos
  rq(20).Execute
  rq(15).Execute
  ' Depurar Ramas
  rq(16).Execute
  
  indModificado = True
  
Exit Sub
error:
  MsgBox "Error : " & Err.Number & " " & Err.Description

End Sub

Private Function SysDate() As String
  Dim rsDate As rdoResultset
  
  Set rsDate = rq(17).OpenResultset
  SysDate = Format(rsDate(0), "dd/mm/yyyy hh:mm:ss")
End Function

Private Function EsNodoFacturable(RutaPadre As String, Hijo As String) As Boolean
    Dim i As Integer
    Dim Encontrado As Boolean
    
    Encontrado = False
    i = 2
    Do While i <= UBound(aNodosFacturables) And Encontrado = False
        If InStr(RutaPadre, aNodosFacturables(i)) = 1 Then
            Encontrado = True
        Else
            i = i + 1
        End If
    Loop
    
    If Not Encontrado Then
        If MsgBox("� Es un nodo facturable ?", vbQuestion + vbYesNo, "Definici�n de Conciertos") = vbYes Then
            EsNodoFacturable = True
            
            ReDim Preserve aNodosFacturables(1 To UBound(aNodosFacturables) + 1)
            aNodosFacturables(UBound(aNodosFacturables)) = RutaPadre & Hijo & "."
            
        Else
            EsNodoFacturable = False
        End If
    Else
        EsNodoFacturable = False
    End If
    
End Function

' Funciones que hacen de interfaz con el control
Public Sub Nuevo()
    Call CrearNodoHijo(tv.SelectedItem)
End Sub

Public Sub Borrar()
    Call BorrarNodo(tv.SelectedItem)
End Sub

Public Sub BorrarConcierto(CodConcierto As Long)
    Set tv.SelectedItem = tv.Nodes.Item("." & CodConcierto & "./")
    RaiseEvent NodoSeleccionado(tv.SelectedItem.key, tv.SelectedItem.Text, True)
    Call BorrarNodo(tv.SelectedItem)
End Sub

Public Function FechaAntigua() As Boolean
    Dim FechaActual As String
    FechaActual = SysDate

'    If DateDiff("d", FechaActual, FechaProceso) < 0 Then
'        msgError ("No se pueden realizar modificaciones con fecha anterior a la actual")
'        FechaAntigua = True
'    Else
        FechaAntigua = False
'    End If
End Function

Private Sub CrearAtributosArbol()
    Dim rs As rdoResultset
    Dim objNodo As Node

    For Each objNodo In tvCat.Nodes
        If Right(objNodo.key, 5) <> "dummy" Then
        
            rq(21).rdoParameters(0) = objNodo.key
            
            Set rs = rq(21).OpenResultset
            
            If rs.EOF Then
                CrearAtribVacio (objNodo.key)
            End If
        End If
    Next
    
End Sub

