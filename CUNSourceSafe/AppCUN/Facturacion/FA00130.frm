VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "idperson.ocx"
Begin VB.Form frm_Cobros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gesti�n de cobros y compensaciones de facturas"
   ClientHeight    =   7740
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   11340
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7740
   ScaleWidth      =   11340
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame4 
      Caption         =   " Pagos "
      Height          =   3525
      Left            =   5175
      TabIndex        =   15
      Top             =   3870
      Width           =   3840
      Begin VB.OptionButton optPagos 
         Caption         =   "Compensaciones"
         Height          =   240
         Index           =   1
         Left            =   1890
         TabIndex        =   8
         Top             =   3240
         Width           =   1635
      End
      Begin VB.OptionButton optPagos 
         Caption         =   "Pagos"
         Height          =   240
         Index           =   0
         Left            =   495
         TabIndex        =   7
         Top             =   3240
         Value           =   -1  'True
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBGrid grdPagos 
         Height          =   2940
         Left            =   135
         TabIndex        =   6
         Top             =   225
         Width           =   3660
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   5
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   5
         Columns(0).Width=   2037
         Columns(0).Caption=   "Fecha"
         Columns(0).Name =   "Fecha"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   7
         Columns(0).FieldLen=   256
         Columns(1).Width=   1693
         Columns(1).Caption=   "Cantidad"
         Columns(1).Name =   "Cantidad"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3200
         Columns(2).Visible=   0   'False
         Columns(2).Caption=   "Numero"
         Columns(2).Name =   "Numero"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "Persona"
         Columns(3).Name =   "Persona"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   1693
         Columns(4).Caption=   "Pendiente"
         Columns(4).Name =   "Pendiente"
         Columns(4).Alignment=   1
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         _ExtentX        =   6456
         _ExtentY        =   5186
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   " Facturas "
      Height          =   3525
      Left            =   0
      TabIndex        =   14
      Top             =   3870
      Width           =   5100
      Begin VB.CheckBox chkTodas 
         Caption         =   "Mostrar todas las facturas"
         Height          =   195
         Left            =   1575
         TabIndex        =   5
         Top             =   3240
         Width           =   2220
      End
      Begin SSDataWidgets_B.SSDBGrid grdFacturas 
         Height          =   2940
         Left            =   90
         TabIndex        =   4
         Top             =   225
         Width           =   4920
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         Col.Count       =   4
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3096
         Columns(0).Caption=   "NoFactura"
         Columns(0).Name =   "NoFactura"
         Columns(0).Alignment=   1
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(0).Locked=   -1  'True
         Columns(1).Width=   2037
         Columns(1).Caption=   "Fecha"
         Columns(1).Name =   "Fecha"
         Columns(1).Alignment=   1
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(1).Locked=   -1  'True
         Columns(2).Width=   1958
         Columns(2).Caption=   "Importe"
         Columns(2).Name =   "Importe"
         Columns(2).Alignment=   1
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(2).Locked=   -1  'True
         Columns(3).Width=   609
         Columns(3).Caption=   "Pag."
         Columns(3).Name =   "Seleccion"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(3).Locked=   -1  'True
         Columns(3).Style=   2
         _ExtentX        =   8678
         _ExtentY        =   5186
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdSupCompensa 
      Caption         =   "E&liminar Compensaci�n"
      Enabled         =   0   'False
      Height          =   555
      Left            =   9225
      TabIndex        =   13
      Top             =   6750
      Width           =   1905
   End
   Begin VB.CommandButton cmdModCompensa 
      Caption         =   "Modificar &Compensacion"
      Enabled         =   0   'False
      Height          =   555
      Left            =   9225
      TabIndex        =   12
      Top             =   6075
      Width           =   1905
   End
   Begin VB.CommandButton cmdSupPago 
      Caption         =   "&Eliminar un Pago"
      Height          =   555
      Left            =   9225
      TabIndex        =   10
      Top             =   4725
      Width           =   1905
   End
   Begin VB.CommandButton cmdModPago 
      Caption         =   "&Modificar un Pago"
      Height          =   555
      Left            =   9225
      TabIndex        =   11
      Top             =   5400
      Width           =   1905
   End
   Begin VB.CommandButton cmdNuevoCobro 
      Caption         =   "Introducir &Pago Nuevo"
      Height          =   555
      Left            =   9225
      TabIndex        =   9
      Top             =   4050
      Width           =   1905
   End
   Begin VB.Frame fraFrame1 
      Caption         =   " Responsable econ�mico "
      Height          =   2070
      Index           =   0
      Left            =   0
      TabIndex        =   17
      Top             =   450
      Width           =   11295
      Begin TabDlg.SSTab tabPacientes 
         Height          =   1725
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   270
         Width           =   11040
         _ExtentX        =   19473
         _ExtentY        =   3043
         _Version        =   327681
         Style           =   1
         Tabs            =   1
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FA00130.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "txtText1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "IdPersona1"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "cmdFactura"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         Begin VB.CommandButton cmdFactura 
            Caption         =   "Por Factura"
            Height          =   375
            Left            =   9405
            TabIndex        =   2
            Top             =   540
            Width           =   1500
         End
         Begin idperson.IdPersona IdPersona1 
            Height          =   1365
            Left            =   90
            TabIndex        =   1
            Top             =   315
            Width           =   10005
            _ExtentX        =   17648
            _ExtentY        =   2408
            BackColor       =   12648384
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Datafield       =   "CI21CodPersona"
            MaxLength       =   7
            blnAvisos       =   0   'False
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   8790
            TabIndex        =   23
            Tag             =   "N�mero Historia"
            Top             =   420
            Visible         =   0   'False
            Width           =   1410
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   6840
            TabIndex        =   22
            Tag             =   "D.N.I|D.N.I "
            Top             =   405
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   7380
            TabIndex        =   21
            Tag             =   "Segundo Apellido|Segundo Apellido"
            Top             =   1200
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   7140
            TabIndex        =   20
            Tag             =   "Primer Apellido|Primer Apellido"
            Top             =   855
            Visible         =   0   'False
            Width           =   2580
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   4575
            TabIndex        =   19
            Tag             =   "Nombre Persona|Nombre Persona"
            Top             =   405
            Visible         =   0   'False
            Width           =   1950
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1305
            Index           =   0
            Left            =   -74880
            TabIndex        =   24
            Top             =   450
            Width           =   10695
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   16776960
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18865
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Conciertos "
      Height          =   1365
      Left            =   0
      TabIndex        =   0
      Top             =   2520
      Width           =   9015
      Begin SSDataWidgets_B.SSDBGrid grdConciertos 
         Height          =   1050
         Left            =   90
         TabIndex        =   3
         Top             =   225
         Width           =   8835
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         GroupHeaders    =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   3
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   767
         Columns(0).Name =   "Marcado"
         Columns(0).AllowSizing=   0   'False
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "Codigo"
         Columns(1).Name =   "Codigo"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   13573
         Columns(2).Caption=   "Concierto"
         Columns(2).Name =   "Concierto"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   15584
         _ExtentY        =   1852
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   16
      Top             =   7470
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   476
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frm_Cobros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public intIndGrid As Integer
Dim objMasterInfo     As New clsCWForm
  
Private Sub Conciertos(Persona As Long)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim MiRsDes As rdoResultset
Dim MiInsercion As String
  
  'Vaciamos todos los grid que contienen datos
  grdConciertos.RemoveAll
  grdFacturas.RemoveAll
  grdPagos.RemoveAll
  'Seleccionamos de las facturas, los distintos conciertos con los que se ha facturado a ese paciente.
  MiSqL = "Select distinct FA09CODNODOCONC from FA0400 Where CI21CODPERSONA = " & Persona
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      'Seleccionamos el nombre del concierto y lo a�adimos al grid de conciertos.
      MiSqL = "Select  FA09DESIG From FA0900 Where FA09CODNODOCONC = " & MiRs(0)
      Set MiRsDes = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not MiRsDes.EOF Then
        MiInsercion = False & Chr(9) & MiRs(0) & Chr(9) & MiRsDes("FA09DESIG")
        grdConciertos.AddItem MiInsercion
      End If
      MiRs.MoveNext
    Wend
  End If
End Sub

Private Sub Pagos(Persona As Long)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim MiRsPendiente As rdoResultset
Dim MiInsertar As String
Dim Acumulado As Double
  'Vaciamos el grid que contiene los pagos realizados por ese paciente.
  grdPagos.RemoveAll
  MiSqL = "Select * From FA1700 Where CI21CODPERSONA = " & Persona & " Order by FA17CODPAGO"
  Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not MiRs.EOF Then
    MiRs.MoveLast
    MiRs.MoveFirst
    While Not MiRs.EOF
      MiSqL = "Select * from fa1800 Where FA17CODPAGO = " & MiRs("FA17CODPAGO")
      Set MiRsPendiente = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not MiRsPendiente.EOF Then
        MiRsPendiente.MoveLast
        MiRsPendiente.MoveFirst
        Acumulado = 0
        While Not MiRsPendiente.EOF
          Acumulado = Acumulado + CDbl(MiRsPendiente("FA18IMPCOMP"))
          MiRsPendiente.MoveNext
        Wend
      Else
        Acumulado = 0
      End If
      MiInsertar = Format(MiRs("FA17FECPAGO"), "DD/MM/YYYY") & Chr(9) & _
                        Format(MiRs("FA17CANTIDAD"), "###,###,##0.##") & Chr(9) & _
                        MiRs("FA17CODPAGO") & Chr(9) & MiRs("CI21CODPERSONA") & Chr(9) & _
                        Format(MiRs("FA17CANTIDAD") - Acumulado, "###,###,##0.##")
      Me.grdPagos.AddItem MiInsertar
      MiRs.MoveNext
    Wend
  End If
End Sub
Private Sub Facturas(Persona As Long, Todas As Boolean)
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim MisqlSuma As String
Dim MiRsSuma As rdoResultset
Dim MiInsertar As String
Dim IntCont As Integer
Dim Conc As Integer
Dim Compensa As Boolean

  'Vaciamos el grid que contiene los n�meros de facturas de esa persona
  grdFacturas.RemoveAll
  Conc = grdConciertos.Rows
  grdConciertos.MoveFirst
  For IntCont = 1 To Conc
    If grdConciertos.Columns(0).Value = True Then
      'Por cada uno de los conciertos  seleccionados mostramos las facturas para esa persona y concierto
      MiSqL = "Select FA04NUMFACT, FA04CODFACT,FA04CANTFACT,FA04FECFACTURA, FA04INDCOMPENSA" & _
                   " From FA0400 Where FA09CODNODOCONC = " & grdConciertos.Columns(1).Text & _
                   " And CI21CODPERSONA = " & Persona
      If Not Todas Then
        MiSqL = MiSqL & " And FA04INDCOMPENSA <> 2"
      End If
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        While Not MiRs.EOF
          MisqlSuma = "Select NVL(SUM(FA18IMPCOMP),0) AS IMPORTE FROM FA1800 Where FA04CODFACT = " & MiRs("FA04CODFACT")
          Set MiRsSuma = objApp.rdoConnect.OpenResultset(MisqlSuma, 3)
          If Not MiRsSuma.EOF Then
            Importe = MiRsSuma(0)
          End If
          If MiRs("FA04INDCOMPENSA") = 0 Then
            Compensa = False
          ElseIf MiRs("FA04INDCOMPENSA") = 1 Then
            Compensa = False
          ElseIf MiRs("FA04INDCOMPENSA") = 2 Then
            Compensa = True
          End If
          'MiInsertar = MiRs("FA04NUMFACT") & Chr(9) & MiRs("FA04FECFACTURA") & Chr(9) & _
                             Format(MiRs("FA04CANTFACT"), "###,###,##0.##") & Chr(9) & _
                             Format(MiRs("FA04CANTFACT") - Importe, "###,###,##0.##") & Chr(9) & Compensa
          MiInsertar = MiRs("FA04NUMFACT") & Chr(9) & MiRs("FA04FECFACTURA") & Chr(9) & _
                             Format(MiRs("FA04CANTFACT"), "###,###,##0.##") & Chr(9) & _
                             Compensa
          grdFacturas.AddItem MiInsertar
          MiRs.MoveNext
        Wend
      End If
    End If
    grdConciertos.MoveNext
  Next
End Sub


Private Sub chkTodas_Click()
  If chkTodas.Value = 1 Then
    Call Facturas(IdPersona1.Text, True)
  ElseIf chkTodas.Value = 0 Then
    Call Facturas(IdPersona1.Text, False)
  End If
End Sub

Private Sub cmdFactura_Click()
Dim Resultado As String
Dim MiSqL As String
Dim MiRs As rdoResultset

  Resultado = InputBox("Introduzca el n� de factura", "Selecci�n por factura", 0)
  If Resultado <> "" Then
    If Left(Resultado, 3) <> "13/" Then
      Resultado = "13/" & Resultado
    End If
    MiSqL = "Select CI21Codpersona From FA0400 Where FA04NUMFACT = '" & Resultado & "'"
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      IdPersona1.Text = MiRs(0)
    Else
      MsgBox "No existe responsable para la factura " & Resultado
    End If
  End If
      
    
End Sub

Private Sub cmdModCompensa_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim rsFecha As rdoResultset
Dim FechaDia As Date
Dim Cadena As String
Dim Resultado As Integer
Dim Acumulado As Double
Dim Nombre As String
Dim ImpPago As Double
Dim ImpFact As Double
Dim AcumFact As Double

 Set rsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not rsFecha.EOF Then
    FechaDia = rsFecha(0)
  End If
  If Month(CDate(grdPagos.Columns(0).Text)) <> Month(FechaDia) Then
    MsgBox "No es posible modificar una compensaci�n de un mes distinto al actual", vbInformation + vbOKOnly, "Aviso"
    Exit Sub
  Else
    'Mostraremos un mensaje indicando las facturas que se compensan con ese pago.
    Cadena = "�Desea modificar la compensaci�n de fecha " & grdPagos.Columns(0).Text & _
                   " por un importe de " & grdPagos.Columns(1).Text & "?"
    Resultado = MsgBox(Cadena, vbYesNo + vbQuestion, "Aviso")
    If Resultado = vbYes Then
      'Calcularemos el pendiente de asignaci�n del pago en cuesti�n.
      MiSqL = "Select FA18IMPCOMP From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        Acumulado = 0
        While Not MiRs.EOF
          Acumulado = Acumulado + CDbl(MiRs("FA18IMPCOMP"))
          MiRs.MoveNext
        Wend
      Else
        Acumulado = 0
      End If
      If Acumulado <> 0 Then
        Acumulado = Acumulado - CDbl(grdPagos.Columns(1).Text)
      End If
      'Recuperamos el importe del pago
      MiSqL = "Select FA17CANTIDAD From FA1700 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not MiRs.EOF Then
        ImpPago = MiRs("FA17CANTIDAD")
      Else
        ImpPago = 0
      End If
      'Recuperaremos el total de la factura
      MiSqL = "Select FA04CANTFACT From FA0400 Where FA04CODFACT = " & grdPagos.Columns(3).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not MiRs.EOF Then
        ImpFact = MiRs("FA04CANTFACT")
      Else
        ImpFact = 0
      End If
      'Calcularemos la cantidad ya facturada
      MiSqL = "Select FA18IMPCOMP From FA1800 Where FA04CODFACT = " & grdPagos.Columns(3).Text
      Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not MiRs.EOF Then
        MiRs.MoveLast
        MiRs.MoveFirst
        AcumFact = 0
        While Not MiRs.EOF
          AcumFact = AcumFact + CDbl(MiRs("FA18IMPCOMP"))
          AcumFact = AcumFact - CDbl(grdPagos.Columns(1).Text)
          MiRs.MoveNext
        Wend
      Else
        AcumFact = 0
      End If
      
      'Crearemos una cadena con los datos de la persona que hace el pago
      Nombre = IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2
      'Llamaremos a la pantalla que nos servir� para modificar la compensaci�n.
      Call frm_ModCompensa.pEditarCompensacion(grdPagos.Columns(2).Text, grdPagos.Columns(3).Text, Nombre, ImpPago - Acumulado, ImpFact - AcumFact)
    End If
  End If
  If Me.chkTodas.Value = 1 Then
    Call Facturas(IdPersona1.Text, True)
  Else
    Call Facturas(IdPersona1.Text, False)
  End If
  Call Pagos(IdPersona1.Text)
End Sub

Private Sub cmdModPago_Click()
Dim x
Dim Conciertos As String
  grdConciertos.MoveFirst
  For x = 1 To grdConciertos.Rows
    If grdConciertos.Columns(0).Value = True Then
      Conciertos = Conciertos & grdConciertos.Columns(1).Text & ";"
    End If
    grdConciertos.MoveNext
  Next
  If Trim(Conciertos) <> "" And Right(Conciertos, 1) = ";" Then
    Conciertos = Left(Conciertos, Len(Conciertos) - 1)
  End If
  Call frm_NuevoPago.NuevoPago(IdPersona1.Text, IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2, grdPagos.Columns(0).Text, Conciertos, False, CDbl(grdPagos.Columns(1).Text), grdPagos.Columns(2).Text)


End Sub

Private Sub cmdNuevoCobro_Click()
Dim x
Dim Conciertos As String
Dim rsFecha As rdoResultset
Dim FechaDia As String

 Set rsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not rsFecha.EOF Then
    FechaDia = Format(rsFecha(0), "dd/mm/yyyy")
  End If
  grdConciertos.MoveFirst
  For x = 1 To grdConciertos.Rows
    If grdConciertos.Columns(0).Value = True Then
      Conciertos = Conciertos & grdConciertos.Columns(1).Text & ";"
    End If
    grdConciertos.MoveNext
  Next
  If Trim(Conciertos) <> "" And Right(Conciertos, 1) = ";" Then
    Conciertos = Left(Conciertos, Len(Conciertos) - 1)
  End If
  Call frm_NuevoPago.NuevoPago(IdPersona1.Text, IdPersona1.Nombre & " " & IdPersona1.Apellido1 & " " & IdPersona1.Apellido2, FechaDia, Conciertos, True, 0, 0)
  If Me.chkTodas.Value = 1 Then
    Call Facturas(IdPersona1.Text, True)
  Else
    Call Facturas(IdPersona1.Text, False)
  End If
  Call Pagos(IdPersona1.Text)

End Sub


Private Sub cmdSupCompensa_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim rsFecha As rdoResultset
Dim FechaDia As Date
Dim Cadena As String
Dim Resultado As Integer
Dim ImpFact As Double
Dim AcumComp As Double
  
 On Error GoTo ErrorenBorrado
 Set rsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not rsFecha.EOF Then
    FechaDia = rsFecha(0)
  End If
  If Month(CDate(grdPagos.Columns(0).Text)) <> Month(FechaDia) Then
    MsgBox "No es posible eliminar una compensaci�n de un mes distinto al actual", vbInformation + vbOKOnly, "Aviso"
    Exit Sub
  Else
    'Mostraremos un mensaje indicando las facturas que se compensan con ese pago.
    Cadena = "�Desea borrar la compensaci�n de fecha " & grdPagos.Columns(0).Text & _
                   " por un importe de " & grdPagos.Columns(1).Text & "?"
    Resultado = MsgBox(Cadena, vbYesNo + vbQuestion, "Aviso")
    If Resultado = vbYes Then
      objApp.rdoConnect.BeginTrans
        'Borraremos todas las compensaciones que se correspondan con ese pago
        MiSqL = "Delete From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text & _
                  " And FA04CODFACT =  " & grdPagos.Columns(3).Text
        objApp.rdoConnect.Execute MiSqL
        'Asignaremos el nuevo estado de la factura
        MiSqL = "Select FA18IMPCOMP From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
        Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
        If Not MiRs.EOF Then
          AcumComp = 0
          MiRs.MoveLast
          MiRs.MoveFirst
          While Not MiRs.EOF
            AcumComp = AcumComp + CDbl(MiRs("FA18IMPCOMP"))
            MiRs.MoveNext
          Wend
        Else
          AcumComp = 0
        End If
        If AcumComp = 0 Then
          MiSqL = "Update FA0400 Set FA04INDCOMPENSA = 0 Where FA04CODFACT = " & grdPagos.Columns(3).Text
        Else
          MiSqL = "Update FA0400 Set FA04INDCOMPENSA = 1 Where FA04CODFACT = " & grdPagos.Columns(3).Text
        End If
        objApp.rdoConnect.Execute MiSqL
      'Recargaremos la pantalla
     objApp.rdoConnect.CommitTrans
     Call Facturas(IdPersona1.Text, False)
     Call Pagos(IdPersona1.Text)
    End If
  End If
Exit Sub
ErrorenBorrado:
  objApp.rdoConnect.RollbackTrans
  MsgBox "Se ha producido un error en el borrado de la compensaci�n. Vuelva a intentarlo", vbCritical + vbOKOnly, "Atenci�n"
End Sub

Private Sub cmdSupPago_Click()
Dim MiSqL As String
Dim MiRs As rdoResultset
Dim rsFecha As rdoResultset
Dim FechaDia As Date
Dim Cadena As String
Dim Resultado As Integer

 Set rsFecha = objApp.rdoConnect.OpenResultset("SELECT SYSDATE FROM DUAL")
  If Not rsFecha.EOF Then
    FechaDia = rsFecha(0)
  End If
  If Format(grdPagos.Columns(0).Text, "dd/mm/yyyy") <> Format(FechaDia, "dd/mm/yyyy") Then
    MsgBox "No es posible eliminar un pago que no se haya introducido hoy", vbInformation + vbOKOnly, "Aviso"
    Exit Sub
  Else
    'Mostraremos un mensaje indicando las facturas que se compensan con ese pago.
    MiSqL = "Select FA04CODFACT From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
    Set MiRs = objApp.rdoConnect.OpenResultset(MiSqL, 3)
    If Not MiRs.EOF Then
      MiRs.MoveLast
      MiRs.MoveFirst
      Cadena = ""
      While Not MiRs.EOF
        Cadena = "13/" & MiRs("FA04CODFACT") & Chr(13) & Chr(10)
        MiRs.MoveNext
      Wend
    Else
      Cadena = ""
    End If
    Cadena = "�Desea borrar el pago de fecha " & grdPagos.Columns(0).Text & " que compensa las facturas " & Chr(13) & Chr(10) & Cadena
    Resultado = MsgBox(Cadena, vbYesNo + vbQuestion, "Aviso")
    If Resultado = vbYes Then
      'Borraremos todas las compensaciones que se correspondan con ese pago
      MiSqL = "Delete From FA1800 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
      objApp.rdoConnect.Execute MiSqL
      'Borraremos el pago
      MiSqL = "Delete From FA1700 Where FA17CODPAGO = " & grdPagos.Columns(2).Text
      objApp.rdoConnect.Execute MiSqL
       'Recargaremos la pantalla
       If Me.chkTodas.Value = 1 Then
          Call Facturas(IdPersona1.Text, True)
       Else
          Call Facturas(IdPersona1.Text, False)
       End If
       Call Pagos(IdPersona1.Text)
    End If
  End If
End Sub

Private Sub Form_Load()
  
  Dim intGridIndex      As Integer
  Call objApp.SplashOn
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
  
  With objMasterInfo
    '.strName = "Paciente"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabPacientes
    Set .grdGrid = grdDBGrid1(0)
    .strDataBase = objEnv.GetValue("Database")
    .strTable = "CI2200"
    If lngUserCode <> 0 Then
         .strWhere = "CI21CODPERSONA = " & lngUserCode
    End If
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("CI21CODPERSONA", False)
    .blnChanged = False
  End With
  
  
  With objWinInfo
    
    Call .FormAddInfo(objMasterInfo, cwFormDetail)

    Call .FormCreateInfo(objMasterInfo)
    
    IdPersona1.ToolTipText = ""
    .CtrlGetInfo(IdPersona1).blnForeign = True
    
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
 Call .WinRegister
    Call .WinStabilize
    IdPersona1.BackColor = objApp.objUserColor.lngKey
    blnCommit = False
  End With
      
      

 Call objApp.SplashOff
 
    'Invisible bot�n de B�squeda
  IdPersona1.blnAvisos = False

End Sub

Private Sub grdDBGrid1_Change(Index As Integer)

  Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdDBGrid1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub grdDBGrid1_HeadClick(intIndex As Integer, ByVal ColIndex As Integer)
Dim strFieldOrder       As String
  strFieldOrder = objWinInfo.CtrlGetInfo(grdDBGrid1(intIndex).Columns(grdDBGrid1(intIndex).Columns(ColIndex).Name)).objControl.DataField
  If strFieldOrder <> "" Then
    Call objGen.RemoveCollection(objWinInfo.objWinActiveForm.cllOrderBy)
    Call objWinInfo.objWinActiveForm.FormAddOrderField(strFieldOrder, False)
    Call objWinInfo.DataRefresh
  End If
End Sub

Private Sub grdDBGrid1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, Y As Single)
'  If Button = 2 Then
'    PopupMenu mnuEstado
'  End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdFacturas_DblClick()
Dim MiSqL As String
Dim NoFactura As Long
Dim rsPagos As rdoResultset
Dim Insertar As String
  
  grdPagos.Columns(0).Width = 1811
  grdPagos.Columns(1).Width = 1319
  grdPagos.Columns(4).Visible = False
  NoFactura = Right(grdFacturas.Columns(0).Text, Len(grdFacturas.Columns(1).Text) - 4)
  'Buscaremos todos los pagos quellevan asociado el n�mero de esa factura.
  MiSqL = "Select FA1700.FA17FECPAGO, FA1800.FA18IMPCOMP FROM FA1700, FA1800 " & _
              "Where FA1700.FA17CODPAGO = FA1800.FA17CODPAGO AND FA1800.FA04CODFACT = " & NoFactura
  Set rsPagos = objApp.rdoConnect.OpenResultset(MiSqL, 3)
  If Not rsPagos.EOF Then
    grdPagos.RemoveAll
    rsPagos.MoveLast
    rsPagos.MoveFirst
    While Not rsPagos.EOF
      Insertar = Format(rsPagos("FA17FECPAGO"), "DD/MM/YYYY") & Chr(9) & _
                     Format(0 & rsPagos("FA18IMPCOMP"), "###,###,##0.##") & Chr(9) & " " & Chr(9) & " "
      grdPagos.AddItem Insertar
      rsPagos.MoveNext
    Wend
  Else
    grdPagos.RemoveAll
    MsgBox "La factura: " & grdFacturas.Columns(0).Text & " no tiene pagos que la compensen", vbInformation + vbOKOnly, "Aviso"
  End If
  'Fecha/Cantidad/N�mero/Persona
  'FA17CODPAGO/CI21CODPERSONA/FA17FECPAGO/FA17CANTIDAD
  'FA17CODPAGO/FA04CODFACT/FA18FECCOMP/FA18IMPCOMP
  'NoFactura/Fecha/Importe/Compensado(s/n)
  
End Sub

Private Sub IdPersona1_Change()
  Call objWinInfo.CtrlDataChange
  If Trim(IdPersona1.Text) <> "" And IsNumeric(IdPersona1.Text) Then
    Call Conciertos(IdPersona1.Text)
    Call Pagos(IdPersona1.Text)
  End If
End Sub

Private Sub IdPersona1_GotFocus()
'  frmCitasPaciente.MousePointer = vbHourglass
'  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.CtrlGotFocus
'  frmCitasPaciente.MousePointer = vbDefault
End Sub

Private Sub IdPersona1_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  If strFormName = "fraFrame1(0)" And strCtrl = "IdPersona1" Then
     IdPersona1.SearchPersona
     objWinInfo.DataRefresh
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)
'Poner el color en titulo del frame exterior de actuaciones
  'fraFrame1(9).ForeColor = fraFrame1(tabTab1(2).Tab + 1).ForeColor
  If strFormName = "fraFrame1(0)" Then
    IdPersona1.blnAvisos = True
    tlbToolbar1.Buttons(26).Enabled = True

  Else
    IdPersona1.blnAvisos = False
    tlbToolbar1.Buttons(26).Enabled = False
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) = 0 Then
      objWinInfo.DataRefresh
    End If
    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
    End If
  End If
  
  
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'  If strFormName = "fraFrame1(0)" Then
'    Call IdPersona1.ReadPersona
'  End If

End Sub

Private Sub optPagos_Click(Index As Integer)
Dim MiSqL As String
Dim NoFactura As Long
Dim rsPagos As rdoResultset
Dim Insertar As String

  
  If optPagos(0).Value = True Then '1150/950/950
    Me.cmdSupPago.Enabled = True
    Me.cmdModPago.Enabled = True
    Me.cmdModCompensa.Enabled = False
    Me.cmdSupCompensa.Enabled = False
    grdPagos.Columns(0).Width = 1150
    grdPagos.Columns(1).Width = 950
    grdPagos.Columns(4).Width = 950
    grdPagos.Columns(4).Visible = True
    Call Pagos(IdPersona1.Text)
  ElseIf optPagos(1).Value = True Then
    Me.cmdModPago.Enabled = False
    Me.cmdSupPago.Enabled = False
    Me.cmdSupCompensa.Enabled = True
    Me.cmdModCompensa.Enabled = True
    grdPagos.Columns(0).Width = 1811
    grdPagos.Columns(1).Width = 1319
    grdPagos.Columns(4).Visible = False
    If grdFacturas.Rows <> 0 Then
      NoFactura = Right(grdFacturas.Columns(0).Text, Len(grdFacturas.Columns(1).Text) - 4)
      'Buscaremos todos los pagos quellevan asociado el n�mero de esa factura.
      MiSqL = "Select FA18FECCOMP, FA18IMPCOMP,FA17CODPAGO,FA04CODFACT FROM FA1800 " & _
                  "Where FA1800.FA04CODFACT = " & NoFactura
      Set rsPagos = objApp.rdoConnect.OpenResultset(MiSqL, 3)
      If Not rsPagos.EOF Then
        grdPagos.RemoveAll
        rsPagos.MoveLast
        rsPagos.MoveFirst
        While Not rsPagos.EOF
          Insertar = Format(rsPagos("FA18FECCOMP"), "DD/MM/YYYY") & Chr(9) & _
                         Format(0 & rsPagos("FA18IMPCOMP"), "###,###,##0.##") & Chr(9) & _
                         rsPagos("FA17CODPAGO") & Chr(9) & rsPagos("FA04CODFACT") & Chr(9) & " "
          grdPagos.AddItem Insertar
          rsPagos.MoveNext
        Wend
      Else
        grdPagos.RemoveAll
        MsgBox "La factura: " & grdFacturas.Columns(0).Text & " no tiene pagos que la compensen", vbInformation + vbOKOnly, "Aviso"
      End If
    End If
  End If
  
  'Fecha/Importe 1811/1319
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabPacientes_Click(PreviousTab As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'Me.MousePointer = vbDefault
End Sub

Private Sub tabPacientes_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)

End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  objMasterInfo.blnChanged = False
  If btnButton.Index = 16 Then
    'Call IdPersona1.Buscar
    If UCase(ActiveControl.Name) = "GRDCONCIERTOS" Then
      Call Facturas(IdPersona1.Text, False)
    ElseIf UCase(ActiveControl.Name) = "IDPERSONA1" Then
      Call Conciertos(IdPersona1.Text)
      Call Pagos(IdPersona1.Text)
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  If intIndex = 10 Then
    Call IdPersona1.Buscar
  Else
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  'Me.MousePointer = vbHourglass
  'Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  'Me.MousePointer = vbDefault
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   
End Sub


