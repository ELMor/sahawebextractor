Attribute VB_Name = "VBCP_Module"
Declare Function OSTimeGetTime& Lib "WINMM.DLL" Alias "timeGetTime" ()
Dim mlVBCP_Time As Long
Dim mdbVBCPDB As Database
Dim mtblVBCPTbl As Recordset
Dim mnVBCPFlag As Integer
Dim mbStarted As Boolean
Sub VBCP_Startup ()
  On Error Goto ErrH
  Dim recTmp As Recordset
  mbStarted = True
  If bDone Then Exit Sub
  bDone = True
  Set mdbVBCPDB = OpenDatabase("\\CPU02208\Rayos\vbp\RX0100.VDB", True)
  Set recTmp = mdbVBCPDB.OpenRecordset("Select FlagValue From Flags Where FlagName='OverwriteData'")
  If recTmp!FlagValue.Value Then
    mdbVBCPDB.Execute "Update Functions Set TimeSpent=0,Hits=0"
  End If
  Set recTmp = mdbVBCPDB.OpenRecordset("Modules", dbOpenDynaset)
  ChDir App.Path
  Do Until recTmp.EOF
    If FileLen(recTmp!ModName.Value) <> recTmp!ModSize.Value And recTmp!ModSize.Value > 0 Then
      Beep
      MsgBox recTmp!ModName.Value & " has changed since Proflier code was added, Profiling has been turned off!", vbInformation
      mnVBCPFlag = 0
      recTmp.Close
      mdbVBCPDB.Close
      Exit Sub
    End If
    recTmp.MoveNext
  Loop
  recTmp.Close
  mdbVBCPDB.Execute "Update Flags Set FlagValue = True Where FlagName='ExistingData'"
  Set recTmp = mdbVBCPDB.OpenRecordset("Select FlagValue From Flags Where FlagName='AnalysisType'")
  mnVBCPFlag = recTmp!FlagValue.Value
  Set mtblVBCPTbl = mdbVBCPDB.OpenRecordset("Functions")
  mtblVBCPTbl.Index = "IDX1"
  mlVBCP_Time = OSTimeGetTime()
  Exit Sub
ErrH:
  If Err = 3356 Then
    MsgBox "Database is Open by another process. Make sure the Profiler Analysis Grid is not open!"
    mnVBCPFlag = 0
  Else
    MsgBox Error
  End If
  Exit Sub
End Sub
Sub VBCP_Update (rModuleNum As Integer, sFunctionName As String, nLineNum As Integer)
  If Not mbStarted Then VBCP_Startup
  Dim dTimeSpent As Double
  If mnVBCPFlag = 0 Then Exit Sub
  If mnVBCPFlag = 4 And nLineNum > 1 Then Exit Sub
  If mnVBCPFlag < 3 And nLineNum > 1 Then dTimeSpent = (OSTimeGetTime() - mlVBCP_Time)/1000
  If mnVBCPFlag Mod 2 = 0 Then
    mtblVBCPTbl.Seek "=", rModuleNum, sFunctionName, 1
  Else
    mtblVBCPTbl.Seek "=", rModuleNum, sFunctionName, nLineNum
  End If
  If mtblVBCPTbl.NoMatch = False Then
    mtblVBCPTbl.Edit
    If (mnVBCPFlag Mod 2 <> 0) or nLineNum = 1 Then
      mtblVBCPTbl!Hits.Value = mtblVBCPTbl!Hits.Value + 1
    End If
    If mnVBCPFlag < 3 And nLineNum > 1 Then mtblVBCPTbl!TimeSpent.Value = mtblVBCPTbl!TimeSpent.Value + dTimeSpent
    mtblVBCPTbl.Update
  End If
  If mnVBCPFlag < 3 Then mlVBCP_Time = OSTimeGetTime()
End Sub
Function VBCP_UpdateIf (rModuleNum As Integer, sFunctionName As String, nLineNum As Integer) As Boolean
  If Not mbStarted Then VBCP_Startup
  VBCP_UpdateIf = True
  If mnVBCPFlag = 0 Then Exit Function
  If mnVBCPFlag = 4 Then Exit Function
  If mnVBCPFlag < 3 Then dTimeSpent = (OSTimeGetTime() - mlVBCP_Time)/1000
  If mnVBCPFlag Mod 2 = 0 Then
    mtblVBCPTbl.Seek "=", rModuleNum, sFunctionName, 1
  Else
    mtblVBCPTbl.Seek "=", rModuleNum, sFunctionName, nLineNum
  End If
  If mtblVBCPTbl.NoMatch = False Then
    mtblVBCPTbl.Edit
    If (mnVBCPFlag Mod 2 <> 0) or nLineNum = 1 Then
      mtblVBCPTbl!Hits.Value = mtblVBCPTbl!Hits.Value + 1
    End If
    If mnVBCPFlag < 3 Then mtblVBCPTbl!TimeSpent.Value = mtblVBCPTbl!TimeSpent.Value + dTimeSpent
    mtblVBCPTbl.Update
  End If
  If mnVBCPFlag < 3 Then mlVBCP_Time = OSTimeGetTime()
End Function
