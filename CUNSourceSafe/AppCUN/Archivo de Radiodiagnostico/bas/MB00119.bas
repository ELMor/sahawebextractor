Attribute VB_Name = "modAutotexto"
Option Explicit


Public Sub pAutotexto(KeyCode%)
    Dim texto As String
    Dim txtBox As TextBox
    Dim SQL  As String
    Dim qryAutoTexto As rdoQuery
    Dim rsAutoTexto As rdoResultset
    
    On Error Resume Next
    Set txtBox = Screen.ActiveForm.ActiveControl
    If KeyCode = vbKeyF3 Then
        'txtBox.SetFocus
        SendKeys "+^{LEFT}"
        DoEvents
        texto = Trim$(txtBox.SelText)
        
        ' Si se pulsa F3 se activa el autoTexto
        SQL = "SELECT cDptoSecc,nombre,texto FROM autotextos"
        SQL = SQL & " WHERE cDptoSecc = ?"
        SQL = SQL & " AND UPPER(nombre) = ?"
        
        Set qryAutoTexto = objApp.rdoConnect.CreateQuery("Autotexto", SQL)
        qryAutoTexto(0) = departamento
        qryAutoTexto(1) = UCase(texto)
        
        Set rsAutoTexto = qryAutoTexto.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        'If Not rsAutoTexto.EOF Then
            txtBox.SelText = rsAutoTexto!texto
        'End If
        rsAutoTexto.Close
        qryAutoTexto.Close
    End If
End Sub


