Attribute VB_Name = "modHojasRealizacion"
Option Explicit
Const constSaltoLinea = 200
Const margenIzquierdo = 1000

Public Function fImprimirHojaObDirecta(cCarpeta As String) As Integer
    ' Se recogen las t�cnicas a preparar, se imprimen y se
    ' cambian de estado. El cambio de estado lleva consigo la
    ' actualizaci�n de la fecha en la que hay que volver a
    ' ver las t�cnicas
    
    Dim SQL As String
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim nRef As String
    Dim res As Boolean
    Dim reali As String
    Dim reali1 As String
    Dim cTecOrig As String
    Dim fechaCreacion As String
    Dim estado As Integer
    Dim arEtiquetas() As typeEtiquetas
    Dim cAutoanalizador As String
    
    
    Screen.MousePointer = vbHourglass
    fechaCreacion = fFechaHoraActual()
    ' Para las t�cnicas se obtiene lo siguiente:
    '       - N� Referencia
    '       - C�digo interno de la t�cnica
    '       - N�mero de Placa (si se trata de un cultivo)
    '       - C�digo de la t�cnica
    '       - Nombre de la t�cnica
    '       - C�digo de la condici�n ambiental
    '       - Nombre de la condici�n
    '       - Sobre qu� se realiza la t�cnica
    
    SQL = "SELECT MB2000.nRef, MB2000.MB20_CODTecAsist, MB2000.MB20_CODPlaca,MB2000.MB20_Reali"
    SQL = SQL & "   , MB0900.MB09_CodTec, MB0900.MB09_Desig"
    SQL = SQL & "   , MB0200.MB02_CodCond, MB0200.MB02_Desig"
    SQL = SQL & "   , MB2000.MB20_tiOrig, MB2000.MB20_CodOrig"
    SQL = SQL & "   , MB34_CODEstTecAsist, MB2000.MB20_CODTecAsist_Orig"
    SQL = SQL & "   , MB04_CODTITEC, MB0900.cAutoanalizador,MB0300.MB03_desig"
    SQL = SQL & " FROM MB2000, MB0900,MB0200,MB2600,MB0300"
    SQL = SQL & " WHERE MB2600.nRef=MB2000.nRef"
    SQL = SQL & "   AND MB2600.MB20_CodTecAsist=MB2000.MB20_CodTecAsist"
    SQL = SQL & "   AND MB2600.MB35_CODEstSegui=" & constSEGUITECVALIDA
    SQL = SQL & "   AND MB0900.MB09_CodTec=MB2000.MB09_CodTec"
    SQL = SQL & "   AND MB0200.MB02_CodCond=MB2600.MB02_CodCond"
    SQL = SQL & "   AND MB0300.MB03_codRecip=MB0900.MB03_codRecip"
    SQL = SQL & "   AND MB2000.nRef IN"
    SQL = SQL & "       (SELECT nRef FROM pruebaAsistencia"
    'SQL = SQL & "        WHERE estado = ? "
    SQL = SQL & "        WHERE estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ") "
    If Trim$(cCarpeta) <> "" Then
        SQL = SQL & "       AND cCarpeta = ?"
    Else
        SQL = SQL & "       AND cCarpeta IN (SELECT cCarpeta FROM carpetas WHERE cDptoSecc=?)"
    End If
    SQL = SQL & "       )"
    SQL = SQL & "   AND MB34_CODEstTecAsist = " & constTECREALIZADA
    SQL = SQL & "   AND MB04_CODTITEC = " & constOBSERVDIRECTA
    SQL = SQL & "   ORDER BY MB2000.nRef, MB2000.MB20_CODPlaca ,MB2000.MB20_CODTecAsist"
                                                                               
    Set qryTec = objApp.rdoConnect.CreateQuery("Tecnica", SQL)
    'qryTec(0) = constPRUEBAREALIZANDO
    If Trim$(cCarpeta) <> "" Then
        qryTec(0) = cCarpeta
    Else
        qryTec(0) = departamento
    End If
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Call pComenzarImpresion     ' Se prepara la impresora para imprimir
    Do While Not rsTec.EOF
        ' En el primer registro se carga el valor de nRef, para que no genere
        ' nueva p�gina
        'If nRef = "" Then nRef = rsTec!nRef
                
        reali = ""
        If Not IsNull(rsTec!MB20_Reali) Then
            reali = rsTec!MB20_Reali
        End If
        ' Si el origen es una colonia -> se obtienen los comentarios a la colonia/placa
        If rsTec!MB20_tiOrig = constORIGENCOLONIA Then
            reali1 = fObtenerObservPlacaColonia(rsTec!nRef, rsTec!MB20_codOrig, rsTec!MB20_CODTecAsist_Orig)
            If reali1 <> "" Then
                If reali <> "" Then
                    reali = reali & Chr$(13) & Chr$(10)
                End If
                reali = reali & reali1
            End If
        End If
        
        ' Se imprime la t�cnica
        If Not IsNull(rsTec!MB20_CODTecAsist_Orig) Then
            cTecOrig = rsTec!MB20_CODTecAsist_Orig
        End If
        If Not IsNull(rsTec!cAutoanalizador) Then
            cAutoanalizador = rsTec!cAutoanalizador
        Else
            cAutoanalizador = ""
        End If

        res = fImprimirTecnicaEnHoja(rsTec!nRef, rsTec!MB20_CODPlaca, rsTec!MB09_Desig, rsTec!MB02_Desig, rsTec!MB20_tiOrig, rsTec!MB20_codOrig, nRef, reali, rsTec!MB34_codEstTecAsist, cTecOrig, rsTec!MB04_CODTITEC, rsTec!MB20_CODTECASIST, rsTec!MB03_desig, cAutoanalizador, arEtiquetas(), fechaCreacion)
        res = fImprimirResultTecnicaObDirecta(rsTec!nRef, rsTec!MB20_CODTECASIST, rsTec!mb09_CodTec)
        rsTec.MoveNext
    Loop
    Call pFinalizarImpresion
    Call pImprimirEtiquetas(arEtiquetas)
    rsTec.Close
    qryTec.Close
    Screen.MousePointer = vbDefault
End Function



Private Function fImprimirResultTecnicaObDirecta(nRef$, codTecAsist$, codTec) As Integer
    ' Se incorporan los resultados a la hoja de realizaci�n de observaci�n directa
    
    Dim SQL As String
    Dim qryResult As rdoQuery
    Dim rsResult As rdoResultset
    Dim posicionY As Integer
    Dim res As Integer
    Dim texto As String
    
    posicionY = Printer.CurrentY
    SQL = "SELECT "
    SQL = SQL & " MB32_DESIG, MB33_RESULT"
    SQL = SQL & " FROM "
    SQL = SQL & "   MB3200,MB3300"
    SQL = SQL & " WHERE "
    SQL = SQL & "   MB3200.MB09_CODTEC = MB3300.MB09_CODTEC"
    SQL = SQL & "   AND MB3200.MB32_CODRESULT = MB3300.MB32_CODRESULT"
    SQL = SQL & "   AND MB3300.NREF = ?"
    SQL = SQL & "   AND MB3300.MB20_CODTECASIST = ?"
    SQL = SQL & "   ORDER BY MB3200.MB32_CODRESULT"
    
    Set qryResult = objApp.rdoConnect.CreateQuery("Resultado", SQL)
    qryResult(0) = nRef
    qryResult(1) = codTecAsist
    Set rsResult = qryResult.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsResult.EOF
        posicionY = posicionY + constSaltoLinea
        res = fSaltoPagina(posicionY)
        Printer.CurrentY = posicionY
        Printer.CurrentX = margenIzquierdo + 2500
        texto = ""
        texto = rsResult!MB32_desig & ": "
        If Not IsNull(rsResult!MB33_result) Then
            texto = texto & rsResult!MB33_result
        End If
        Printer.Print texto
        rsResult.MoveNext
    Loop
    rsResult.Close
    qryResult.Close
End Function


Public Function fImprimirHojaRealizacion(cCarpeta$, report$) As Integer
    Dim SQL As String
    Dim res As Integer
    Dim rptCrystal As CrystalReport
    
    Set rptCrystal = frmPrincipal.crCrystalReport1
    
    If rptCrystal.Connect = "" Then
        rptCrystal.Connect = objApp.rdoConnect.Connect
    End If
    rptCrystal.WindowTitle = "Impresi�n de Hojas de Realizaci�n"
    
    'rptCrystal.ReportFileName = rptPath & "\" & report & ".rpt"
    rptCrystal.ReportFileName = report
    
    SQL = " {pruebaAsistencia.cCarpeta} = " & cCarpeta
    'SQL = SQL & " AND {MB2000.MB20_fecLect} < TO_DATE('" & Format$(DateAdd("d", 1, fFechaHoraActual), "dd/mm/yyyy") & "','DD/MM/YYYY')"
    SQL = SQL & " AND {MB2000.MB20_fecLect} < " & fFechaCrystal(Format$(DateAdd("d", 1, fFechaHoraActual), "dd/mm/yyyy"))
    'SQL = SQL & " AND {pruebaAsistencia.estado}=" & constPRUEBAREALIZANDO
    SQL = SQL & " AND ({pruebaAsistencia.estado}=" & constPRUEBAREALIZANDO & " OR {pruebaAsistencia.estado}=" & constPRUEBAVALIDADA & ") "
    SQL = SQL & " AND {MB2000.MB34_codEstTecAsist} = " & constTECREALIZADA

    rptCrystal.SelectionFormula = SQL
    res = rptCrystal.PrintReport
    If res <> 0 Then
        rptCrystal.Action = 1
    End If
    
    rptCrystal.SelectionFormula = ""
    rptCrystal.WindowTitle = ""
    rptCrystal.ReportFileName = ""

End Function
Public Sub pImprimirHojaRealizacionCrystal()
    
    Dim res As Integer
    Dim SQL As String
    Dim texto As String

    
    texto = "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    texto = texto & "wwwwwwwwwwwwwwwwwwww wwwwwwwwww wADLKAJD LASDJ LAJSDLAKJSDL ALSDJ LASD LASD LADL ASLD "
    
    frmPrincipal.crCrystalReport1.ReportFileName = rptPath & "\mb00110.rpt"
    
    SQL = "{PRUEBAASISTENCIA.NREF}='AAA-1111'"
    frmPrincipal.crCrystalReport1.SelectionFormula = SQL
    frmPrincipal.crCrystalReport1.Formulas(0) = "DATOS = '" & texto & "'"
    res = frmPrincipal.crCrystalReport1.PrintReport
    If res <> 0 Then
        frmPrincipal.crCrystalReport1.Action = 1
    End If
    
    frmPrincipal.crCrystalReport1.SelectionFormula = ""
    frmPrincipal.crCrystalReport1.WindowTitle = ""
    frmPrincipal.crCrystalReport1.ReportFileName = ""
    frmPrincipal.crCrystalReport1.Formulas(0) = ""

End Sub

Public Sub pImprimirHojaRealizacion(cCarpeta$, nRef$, historia$, caso$)
    ' Se imprime en euna hoja los mismo s datos que se muestran en la
    ' ventana de petici�n. El objetivo es que se pueda leer una
    ' mesa sin depender del ordenador.
    
    Dim SQL As String
    Dim qryHoja As rdoQuery
    Dim rsHoja As rdoResultset
    Dim fLect As String
    Dim i As Integer
    Dim paramCarpeta As Integer
    Dim paramNRef As Integer
    Dim paramHistoria As Integer
    Dim paramCaso As Integer
    
    fLect = Format(DateAdd("d", 1, fFechaActual()), "dd/mm/yyyy")
    
    SQL = "SELECT "
    SQL = SQL & "   pA.nRef, pac.nom||' '||pac.ap1||' '||pac.ap2 as paciente,"
    SQL = SQL & "   camas.cama, pr.designacion, pA.historia, pA.caso,"
    SQL = SQL & "   NVL(dr.txtFirma,dr.nom||' '||dr.ap1||' '||dr.ap2) as doctor,"
    SQL = SQL & "   dpt.desig, p.comentarios as comentPet,"
    SQL = SQL & "   sp.comentarios as comentSP, pA.urgenciaRealizacion"
    SQL = SQL & " FROM "
    SQL = SQL & "   pruebaAsistencia pA, pac, camas, dr, dpt"
    SQL = SQL & "   , seguimientoPrueba sp, peticiones p, pruebas pr"
    SQL = SQL & " WHERE "
    SQL = SQL & "           pac.nh = pA.historia"
    SQL = SQL & "   AND     camas.nh (+) = pA.historia"
    SQL = SQL & "   AND     camas.nc (+) = pA.caso"
    SQL = SQL & "   AND     dr.cDr = pA.drSolicitud"
    SQL = SQL & "   AND     dpt.cDpt = pA.dptoSolicitud"
    SQL = SQL & "   AND     sp.historia = pA.historia"
    SQL = SQL & "   AND     sp.caso = pA.caso"
    SQL = SQL & "   AND     sp.secuencia = pA.secuencia"
    SQL = SQL & "   AND     sp.nRepeticion = pA.nRepeticion"
    SQL = SQL & "   AND     p.historia = pA.historia"
    SQL = SQL & "   AND     p.caso = pA.caso"
    SQL = SQL & "   AND     p.nPeticion = pA.nPeticion"
    SQL = SQL & "   AND     pr.cPrueba = pA.cTipoPrueba"
    SQL = SQL & "   AND     pA.cCarpeta IN "
    SQL = SQL & "           (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = ?)"
    SQL = SQL & "   AND     NRef IN "
    SQL = SQL & "               (SELECT NRef FROM MB2000 "
    SQL = SQL & "               WHERE MB20_FECLECT < TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & "               AND MB34_CODESTTECASIST = " & constTECREALIZADA
    SQL = SQL & "               )"
    i = 2
    If nRef <> "" Then
        SQL = SQL & " AND     pA.nRef = ?"
        paramNRef = i
        i = i + 1
    End If
    If historia <> "" Then
        SQL = SQL & " AND     pA.historia = ?"
        paramHistoria = i
        i = i + 1
    End If
    If caso <> "" Then
        SQL = SQL & " AND     pA.caso = ?"
        paramCaso = i
        i = i + 1
    End If
    If cCarpeta <> "" Then
        SQL = SQL & " AND     pA.cCarpeta = ?"
        paramCarpeta = i
        i = i + 1
    End If
    
    
    Set qryHoja = objApp.rdoConnect.CreateQuery("Hoja Realizacion", SQL)
    
    qryHoja(0) = departamento
    qryHoja(1) = fLect
    If nRef <> "" Then
        qryHoja(paramNRef) = nRef
    End If
    If historia <> "" Then
        qryHoja(paramHistoria) = historia
    End If
    If caso <> "" Then
        qryHoja(paramCaso) = caso
    End If
    If cCarpeta <> "" Then
        qryHoja(paramCarpeta) = cCarpeta
    End If

    Set rsHoja = qryHoja.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsHoja.EOF
        'pImprimirHojaRealizacionCabecera
    
        rsHoja.MoveNext
    Loop
    rsHoja.Close
    qryHoja.Close
End Sub
Private Sub pImprimirHojaRealizacionCabecera(nRef$, paciente$, cama$, prueba$, historia$, caso$, Dpt$, Dr$, peticion$, solicitud$, urgencia$)


End Sub


