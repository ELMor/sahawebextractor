Attribute VB_Name = "modLlamadaVentana"
Option Explicit

Public Sub pVentanaAlmacenar(nRef$, Optional CodCol$, Optional cMuestra$)
    ' Recoge la colonia a almacenar y llama a la ventana de almacenamiento de colonias
    
    ' Se pasan a la ventana los parámetros de la colonia a almacenar
    Call objPipe.PipeSet("nRef", nRef)
    Call objPipe.PipeSet("codCol", CodCol)
    Call objPipe.PipeSet("cMuestra", cMuestra)
    
    frmG_Almacenamiento.Show vbModal
End Sub
Public Sub pVentanaAutotexto(tabInicial%)
    'llama a la ventana de pruebas
    
    Call objPipe.PipeSet("MB_tabInicial", tabInicial)
    
    frmA_Autotexto.Show vbModal
    Set frmA_Autotexto = Nothing
    
End Sub

Public Sub pVentanaPruebas(nRef$, Optional historia$, Optional caso$)
    'llama a la ventana de pruebas
    
    ' Se mira primero si dispone de acceso
    If fAcceso(constCONSULTARESULTADOS) = True Then

        Call objPipe.PipeSet("BuscNRef", nRef)
        Call objPipe.PipeSet("BuscHistoria", historia)
        Call objPipe.PipeSet("BuscCaso", caso)
        
        frmA_Pruebas.Show vbModal
        Set frmA_Pruebas = Nothing
    Else
        MsgBox "No dispone de derechos de consulta de históricos.", vbExclamation, "Acceso a Pacientes"
    End If
End Sub
Public Sub pVentanaRefSensibilidad(codAnti$, desigAnti$)
    'llama a la ventana de referencias de sensibilidad de antibióticos
    Call objPipe.PipeSet("MB_Antibiotico", codAnti)
    Call objPipe.PipeSet("MB_desigAntibiotico", desigAnti)
    frmH_RefSensibilidad.Show vbModal
    Set frmH_RefSensibilidad = Nothing
    
End Sub

Public Sub pVentanaHojaResultados(cCarpeta$, fechaReali$, cUser$)
    
    Call objPipe.PipeSet("MB_cCarpeta_Result", cCarpeta)
    Call objPipe.PipeSet("MB_fechaReali_Result", fechaReali)
    Call objPipe.PipeSet("MB_cUser_Result", cUser)
    frmP_Resultados.Show vbModal
    Set frmP_Resultados = Nothing
    Call objPipe.PipeRemove("MB_cCarpeta_Result")
    Call objPipe.PipeRemove("MB_fechaReali_Result")
    Call objPipe.PipeRemove("MB_cUser_Result")
End Sub

Public Function fVentanaCalendario(strFechaActual$, strFechaMinima$, strFechaMaxima$) As String
    
    If strFechaActual <> "" Then
        Call objPipe.PipeSet("MB_FechaActual", strFechaActual)
    End If
    If strFechaMinima <> "" Then
        Call objPipe.PipeSet("MB_FechaMinima", strFechaMinima)
    End If
    If strFechaMaxima <> "" Then
        Call objPipe.PipeSet("MB_FechaMaxima", strFechaMaxima)
    End If
    
    frmH_Calendar.Show vbModal
    Set frmH_Calendar = Nothing
    
    If objPipe.PipeExist("MB_FechaActual") Then
        fVentanaCalendario = objPipe.PipeGet("MB_FechaActual")
        Call objPipe.PipeRemove("MB_FechaActual")
    Else
        fVentanaCalendario = strFechaActual
    End If
    If objPipe.PipeExist("MB_FechaMinima") Then
        Call objPipe.PipeRemove("MB_FechaMinima")
    End If
    If objPipe.PipeExist("MB_FechaMaxima") Then
        Call objPipe.PipeRemove("MB_FechaMaxima")
    End If
    
End Function
Public Sub pVentanaHojaTrabajoRepeticion(cCarpeta$, blnSiembras As Boolean, blnHojaTrabajo As Boolean)
    
    Call objPipe.PipeSet("MB_cCarpeta", cCarpeta)
    Call objPipe.PipeSet("MB_Siembras", blnSiembras)
    Call objPipe.PipeSet("MB_HojaTrabajo", blnHojaTrabajo)
    frmI_HojaTrabajoRepeticion.Show vbModal
    Set frmI_HojaTrabajoRepeticion = Nothing
    Call objPipe.PipeRemove("MB_cCarpeta")
    Call objPipe.PipeRemove("MB_Siembras")
    Call objPipe.PipeRemove("MB_HojaTrabajo")
End Sub


Public Sub pVentanaPeticion(cCarpeta$, desig$)
    ' Recoge la carpeta a buscar y llama a la ventana de peticion
    
    ' Se pasan a la ventana los parámetros de la carpeta a mostrar
    Call objPipe.PipeSet("cCarpeta", cCarpeta)
    Call objPipe.PipeSet("Carpeta", desig)
    
    pAccesoPeticion
    
End Sub


Public Sub pVentanaPaciente(historia$)
    ' Recoge la historia a buscar y llama a la ventana de paciente
    
    
    ' Se mira primero si dispone de acceso
    If fAcceso(constCONSULTARESULTADOS) = True Then

        ' Se pasan a la ventana los parámetros de la historia a localizar
        Call objPipe.PipeSet("BuscHistoria", historia)
        
        frmA_Paciente.Show vbModal
        
        Set frmA_Paciente = Nothing
    Else
        MsgBox "No dispone de derechos de consulta de históricos.", vbExclamation, "Acceso a Pacientes"
    End If
End Sub


