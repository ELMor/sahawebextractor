Attribute VB_Name = "modInformeResultado"
Option Explicit

' Constantes utilizadas en la impresi�n de informes, m�s concretamente
' en la impresi�n del antibiograma

Const xNombreAntib = 500       ' Primera posici�n del nombre de Antibi�tico en eje x
Const dxNombreElec = 2000       ' Distancia relativa desde el nombre de antibi�tico al nombre de la elecci�n
Const dxNombreColonia = 2100        ' Distancia relativa desde el nombre de antibi�tico a la primera columna de colonia
Const dxResultColonia = 300         ' Distancia entre las diferentes columnas de resultados de colonias
'Const dxColumna = 2500          ' Distancia entre diferentes columnas de elecciones
Const dxFinInicio = 100         ' Distancia del final de una columna al comienzo de la siguiente

Const margenIzquierdo = 1000
Const margenDerecho = 500
Const constSaltoLinea = 300
Const constSaltoParrafo = 500
Private Function fCargarAntibiograma(nRef$, arInfAntib() As typeInfAntib, arPosColonias() As Integer, nAntib%) As Integer
    ' Carga en las matrices la informaci�n de la antibiograma
    ' ordenada seg�n columnas por elecci�n, filas por antibi�tico y
    ' columnas por colonias
    
    Dim SQL As String
    Dim qryAntib As rdoQuery
    Dim rsAntib As rdoResultset
    Dim iElec As Integer
    Dim cEleccion As Integer
    Dim iAntib As Integer
    Dim cAntib As Integer
    Dim iCol As Integer
    Dim cCol As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim l As Integer
    Dim dimension1 As Integer
    Dim dimension2 As Integer
    Dim nColonias As Integer
    Dim localizado As Boolean
    
    On Error Resume Next
      
    
    SQL = "SELECT"
    SQL = SQL & "   MB2900.MB27_CODCol, MB2900.MB29_Conc,"
    SQL = SQL & "   MB0700.MB07_CODAnti, MB0700.MB07_Descrip,"
    SQL = SQL & "   MB4000.MB40_Desig, MB0700.MB01_CODElec,"
    SQL = SQL & "   MB0100.MB01_Desig,MB29_INDInf_Conc"
    SQL = SQL & " FROM "
    SQL = SQL & "   MB2700, MB2900, MB0700, MB4000, MB0100"
    SQL = SQL & " WHERE "
    SQL = SQL & "   MB2900.nRef = MB2700.nRef"
    SQL = SQL & "   AND MB2900.MB27_CODCol=MB2700.MB27_CODCol"
    SQL = SQL & "   AND MB0700.MB07_CODAnti=MB2900.MB07_CODAnti"
    SQL = SQL & "   AND MB4000.MB40_CODReac=MB2900.MB40_CODReac"
    SQL = SQL & "   AND MB0100.MB01_CODElec=MB0700.MB01_CODElec"
    SQL = SQL & "   AND MB2700.nRef=?"
    SQL = SQL & "   AND MB2700.MB36_CODEstCol = ?"
    SQL = SQL & "   AND MB2900.MB29_Recom=-1"
'    SQL = SQL & " GROUP BY "
'    SQL = SQL & "   MB0700.MB01_CODElec, MB0700.MB07_Descrip,"
'    SQL = SQL & "   MB2900.MB27_CODCol"
    SQL = SQL & " ORDER BY "
    SQL = SQL & "   MB0700.MB01_CODElec, MB0700.MB07_Descrip,"
    SQL = SQL & "   MB2900.MB27_CODCol"
    
    Set qryAntib = objApp.rdoConnect.CreateQuery("Informe Resultado", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Function
    End If
    qryAntib(0) = nRef
    qryAntib(1) = constCOLINFORMADA
    Set rsAntib = qryAntib.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Err <> 0 Then
        MsgBox Error
        Exit Function
    End If
    
    ' Por limitaciones del printer y para ordenar los antibi�ticos
    ' seg�n elecci�n, nombre del antibi�tico y n� de colonia,
    ' introducen primero los datos en una matriz para ordenarlos
    Do While Not rsAntib.EOF
        If cEleccion <> rsAntib!MB01_CODElec Then
            cEleccion = rsAntib!MB01_CODElec
            iElec = UBound(arInfAntib)
            iElec = iElec + 1
            ReDim Preserve arInfAntib(1 To iElec)
            arInfAntib(iElec).cElec = cEleccion
            arInfAntib(iElec).elec = rsAntib!MB01_Desig
            ' Se indica la posici�n en que comienza a escribirse
            ' en la impresora cada columna de elecci�n
            If iElec > 1 Then
                ' Se guardan cinco posiciones para las colonias
                ' Ser�: (Posici�n de columna anterior)  +  (distancia a columna 1 de resultados)  +  (n� de colonias en elecci�n anterior * distancia entre colonias)  +  (separaci�n entre columnas)
                'arInfAntib(iElec).posicion = arInfAntib(iElec - 1).posicion + dxNombreColonia + arInfAntib(iElec - 1).nCol * dxResultColonia + dxFinInicio
            
                ' Ser�: (Posici�n de columna anterior)  +  (distancia a columna 1 de resultados)  +  (n� de colonias * distancia entre colonias)  +  (separaci�n entre columnas)
                arInfAntib(iElec).posicion = arInfAntib(iElec - 1).posicion + dxNombreColonia + constNColInforme * dxResultColonia + dxFinInicio
            
            Else
                arInfAntib(iElec).posicion = xNombreAntib
            End If
            iAntib = 0
            cAntib = 0
            iCol = 0
            cCol = 0
        End If
        If cAntib <> rsAntib!MB07_CODANTI Then
            cAntib = rsAntib!MB07_CODANTI
            iAntib = iAntib + 1
            ReDim Preserve arInfAntib(iElec).arAntib(1 To iAntib)
            arInfAntib(iElec).arAntib(iAntib).cAntib = cAntib
            arInfAntib(iElec).arAntib(iAntib).antib = Left(rsAntib!MB07_Descrip, 20)
            iCol = 0
            cCol = 0
            nAntib = nAntib + 1 ' n� de Antibi�ticos diferentes
        End If
        If cCol <> rsAntib!MB27_CODCOL Then
            cCol = rsAntib!MB27_CODCOL
            iCol = iCol + 1
            ReDim Preserve arInfAntib(iElec).arAntib(iAntib).arCol(1 To iCol)
            arInfAntib(iElec).arAntib(iAntib).arCol(iCol).cCol = cCol
            arInfAntib(iElec).arAntib(iAntib).arCol(iCol).result = Left(rsAntib!MB40_Desig, 2)
            If Not IsNull(rsAntib!MB29_INDInf_Conc) Then
                If rsAntib!MB29_INDInf_Conc = True Then
                    If Not IsNull(rsAntib!MB29_Conc) Then
                        arInfAntib(iElec).arAntib(iAntib).arCol(iCol).conc = rsAntib!MB29_Conc
                        arInfAntib(iElec).arAntib(iAntib).infConc = True
                    End If
                End If
            End If

            ' Se se�ala el n� de colonias enfrentadas a los
            ' antibi�ticos de la elecci�n
            If iCol > arInfAntib(iElec).nCol Then
                arInfAntib(iElec).nCol = iCol
            End If
            ' Se mira dentro de la columna de elecci�n cual es la posici�n que le corresponde
            ' seg�n el c�digo de colonia
            
            nColonias = UBound(arPosColonias)
            If nColonias = 0 Then
                i = 0
                ReDim arPosColonias(1 To 1)
                arPosColonias(1) = cCol
                i = i + 1
            Else
                localizado = False
                For i = 1 To nColonias
                    If arPosColonias(i) > cCol Then
                        ' Se inserta delante la nueva colonia
                        For j = constNColInforme To i + 1
                            arPosColonias(j) = arPosColonias(j - 1)
                        Next j
                        arPosColonias(i) = cCol
                        localizado = True
                        Exit For
                    ElseIf arPosColonias(i) = 0 Then
                        arPosColonias(i) = cCol
                        localizado = True
                        Exit For
                    ElseIf arPosColonias(i) = cCol Then
                        localizado = True
                        Exit For
                    End If
                Next i
                If localizado = False Then
                    i = nColonias + 1
                    ReDim Preserve arPosColonias(1 To i)
                    arPosColonias(i) = cCol
                End If
            End If
            ' Se indica a la colonia en qu� columna debe escribirse
            arInfAntib(iElec).arAntib(iAntib).arCol(iCol).columna = i
        End If
        rsAntib.MoveNext
    Loop
    rsAntib.Close
    qryAntib.Close
    ' Se indica el lugar donde se escribe cada colonia
    For i = 1 To iElec
        dimension1 = 0
        dimension1 = UBound(arInfAntib(i).arAntib)
        For j = 1 To dimension1
            dimension2 = 0
            dimension2 = UBound(arInfAntib(i).arAntib(j).arCol)
            For k = 1 To dimension2
                For l = 1 To nColonias
                    If arPosColonias(l) = arInfAntib(i).arAntib(j).arCol(k).cCol Then
                        arInfAntib(i).arAntib(j).arCol(k).columna = l
                        Exit For
                    End If
                Next l
            Next k
        Next j
    Next i
    fCargarAntibiograma = iElec
End Function

Private Sub pEscribirAntibiograma(arInfAntib() As typeInfAntib, arPosColonias() As Integer, iElec As Integer, nAntib%, blnConc As Boolean)
    ' blnConc: indica si hay que imprimir reacciones o concentraciones
    ' Manda a la impresora las �rdenes de impresi�n de los antibiogramas
    
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim columna As Integer
    Dim res As Integer
    Dim concentracion As String
    Dim posicionY As Integer
    Dim lineaNueva As Boolean
    Dim lngInicioYLineaVertical As Long
    Dim lngFinYLineaVertical As Long
    Dim lngPosicionXLineaVertical As Long
    Dim nColonias As Integer
    Dim nPasos As Integer
    Dim intBloque As Integer
    
    On Error Resume Next
    
    ' Se escriben las cabeceras de las elecciones
    Printer.FontSize = 7
    
    ' Se imprimen las colonias de 5 en cinco
    nColonias = UBound(arPosColonias)
    
    nPasos = nColonias / constNColInforme
    If nColonias Mod constNColInforme > 0 Then
        nPasos = nPasos + 1
    End If
    
        
        
    For intBloque = 1 To nPasos
        res = fSaltoPagina(posicionY + 2 * constSaltoLinea)
        posicionY = Printer.CurrentY
        posicionY = posicionY + constSaltoLinea
        Printer.CurrentY = posicionY
        lngInicioYLineaVertical = posicionY ' Se se�ala el comienzo de las lineas verticales que separan las columnas de elecciones
        Printer.FontBold = True
        Printer.FontUnderline = True
        For i = 1 To iElec
            ' posici�n: (comienzo columna) + (distancia a nombre de elecci�n)
            Printer.CurrentX = arInfAntib(i).posicion + dxNombreElec
            Printer.Print arInfAntib(i).elec
            Printer.CurrentY = posicionY
        Next i
        Printer.FontBold = False
        Printer.FontUnderline = False
    
        posicionY = posicionY + constSaltoLinea
        Printer.CurrentY = posicionY
        ' Para cada elecci�n se escriben los identificadores de las colonias
        For i = 1 To iElec
            ' posicion: (comienzo de columna) + (distancia a primera columna) + ((j-1) * ancho columna)
            For j = (intBloque - 1) * constNColInforme + 1 To (intBloque) * constNColInforme
                If arPosColonias(j) <> 0 Then
                    Printer.CurrentX = arInfAntib(i).posicion + dxNombreColonia + ((j - 1) - (intBloque - 1) * constNColInforme) * dxResultColonia
                    Printer.Print arPosColonias(j)
                    Printer.CurrentY = posicionY
                Else
                    Exit For
                End If
            Next j
        Next i
        
        posicionY = posicionY + constSaltoLinea / 2
        Printer.CurrentY = posicionY
        ' Dibuja una l�nea atravesando la p�gina.
        Printer.Line (xNombreAntib, posicionY)-(Printer.ScaleWidth, posicionY)    ' Dibuja una l�nea.
        
        posicionY = posicionY + constSaltoLinea / 2
        Printer.CurrentY = posicionY
        ' Se escribe el antibiograma
        For j = 1 To nAntib
                For i = 1 To iElec
                    If UBound(arInfAntib(i).arAntib) >= j Then
                        If arInfAntib(i).arAntib(j).infConc = True Or blnConc = False Then
                            ' Se escriben los antibi�ticos de cada elecci�n
                            If arInfAntib(i).arAntib(j).antib <> "" Then
                                ' Nombre del antibi�tico.
                                ' Posici�n: (comienzo de columna de elecci�n)
                                res = fSaltarPaginaAntibiotico(posicionY, lngInicioYLineaVertical, lngFinYLineaVertical, iElec)
                                
                                Printer.CurrentX = arInfAntib(i).posicion
                                'Printer.CurrentX = arInfAntib(i).posicion + dxNombreElec - Printer.TextWidth(arInfAntib(i).arAntib(j).antib)
                                
                                Printer.FontBold = True
                                Printer.Print arInfAntib(i).arAntib(j).antib
                                Printer.FontBold = False
                                Printer.CurrentY = posicionY
                                For k = 1 To UBound(arInfAntib(i).arAntib(j).arCol)
                                    ' Para cada antibi�tico se escribe la reacci�n del microorganismo
                                    columna = arInfAntib(i).arAntib(j).arCol(k).columna - (intBloque - 1) * constNColInforme
                                    If columna > 0 And columna <= constNColInforme Then
                                        ' Posici�n: (comienzo de columna de elecci�n) + (distancia a primera columna) + ((n� columna-1)*ancho columna)
                                        Printer.CurrentX = arInfAntib(i).posicion + dxNombreColonia + (columna - 1) * dxResultColonia
                                        
                                        ' Se mira si hay que imprimir concentraciones
                                        If blnConc = True Then
                                            concentracion = Left(arInfAntib(i).arAntib(j).arCol(k).conc, 4)
                                            Printer.Print concentracion
                                            Printer.CurrentY = posicionY
                                        Else
                                            Printer.Print arInfAntib(i).arAntib(j).arCol(k).result
                                            Printer.CurrentY = posicionY
                                        End If
                                    ElseIf columna > constNColInforme Then
                                        Exit For
                                    End If
                                Next k
                                lineaNueva = True
                            Else
                                Exit For
                            End If
                        End If
                    End If
                Next i
                If lineaNueva = True Then
                    posicionY = posicionY + constSaltoLinea
                    Printer.CurrentY = posicionY
                    lineaNueva = False
                End If
        Next j
        lngFinYLineaVertical = posicionY ' Se se�ala el final de las lineas verticales que separan las columnas de elecciones
        
        ' Dibuja una l�nea atravesando la p�gina.
        Printer.Line (xNombreAntib, posicionY)-(Printer.ScaleWidth, posicionY)   ' Dibuja una l�nea.
        
        ' Bibuja las lineas verticales que dividen las columnas de elecciones
        For i = 2 To iElec + 2
            lngPosicionXLineaVertical = xNombreAntib + (i - 1) * (dxNombreColonia + constNColInforme * dxResultColonia + dxFinInicio / 2)
            If i = iElec + 1 Then
                lngPosicionXLineaVertical = lngPosicionXLineaVertical + dxFinInicio / 2
            End If
            Printer.Line (lngPosicionXLineaVertical, lngInicioYLineaVertical)-(lngPosicionXLineaVertical, lngFinYLineaVertical)   ' Dibuja una l�nea.
        Next i
    Next intBloque
    
    
    
    Printer.FontSize = 10

End Sub


Private Sub pImprimirCabecera(nRef$)
    ' Se imprime la cabecera del informe de resultados
    
    Dim SQL As String
    Dim qryPrueba As rdoQuery
    Dim rsPrueba As rdoResultset
    Dim historia As String
    Dim caso As String
    Dim posicionY As Integer
    
    On Error Resume Next

    SQL = "SELECT "
    SQL = SQL & "   asist.nh,asist.nc,"
    SQL = SQL & "   (pac.nom||' '||pac.ap1||' '||pac.ap2) as paciente,"
    SQL = SQL & "   d.designacion as desigLab,"
    SQL = SQL & "   asist.cama, dpt.desig as desigDpt, dr.txtFirma,"
    SQL = SQL & "   sp1.fecha as fechaPeticion,"
    SQL = SQL & "   (u.nombre||' '||u.apellido1) as drRealiza,"
    SQL = SQL & "   sp2.fecha as fechaFirma,"
    SQL = SQL & "   p.designacion as desigPrueba"
    SQL = SQL & " FROM "
    SQL = SQL & "   pac,asist,pruebaasistencia pA,seguimientoPrueba sp1,"
    SQL = SQL & "   seguimientoPrueba sp2, carpetas c,"
    SQL = SQL & "   dptosecc d,dpt,dr, users u, pruebas p "
    SQL = SQL & " WHERE "
    SQL = SQL & "   pac.nh=pA.historia"
    SQL = SQL & "   AND asist.nh=pA.historia"
    SQL = SQL & "   AND asist.nc=pA.caso"
    SQL = SQL & "   AND sp1.historia=pA.historia"
    SQL = SQL & "   AND sp1.caso=pA.caso"
    SQL = SQL & "   AND sp1.secuencia=pA.secuencia"
    SQL = SQL & "   AND sp1.nRepeticion=pA.nRepeticion"
    SQL = SQL & "   AND sp1.proceso=" & constPROCESOSOLICITUD
    SQL = SQL & "   AND sp2.historia (+)=pA.historia"
    SQL = SQL & "   AND sp2.caso (+)=pA.caso"
    SQL = SQL & "   AND sp2.secuencia (+)=pA.secuencia"
    SQL = SQL & "   AND sp2.nRepeticion (+)=pA.nRepeticion"
    SQL = SQL & "   AND sp2.proceso (+)=" & constPROCESOVALIDACION
    SQL = SQL & "   AND c.cCarpeta=pA.cCarpeta"
    SQL = SQL & "   AND d.cDptoSecc=c.cDptoSecc"
    SQL = SQL & "   AND dpt.cdpt (+) = pA.dptoSolicitud"
    SQL = SQL & "   AND dr.cDr (+) = pA.drSolicitud"
    SQL = SQL & "   AND u.cUser (+)= sp2.responsable"
    SQL = SQL & "   AND p.cPrueba = pA.cTipoPrueba"
    SQL = SQL & "   AND pA.nRef = ?"
    SQL = SQL & "   AND pA.estado <> " & constPRUEBAREPETIDA
    
    Err = 0
    Set qryPrueba = objApp.rdoConnect.CreateQuery("Informe Resultado", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    qryPrueba(0) = nRef
    Set rsPrueba = qryPrueba.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Err <> 0 Then
        MsgBox Error
        qryPrueba.Close
        Exit Sub
    End If
    
    historia = rsPrueba!nh
    caso = rsPrueba!nc
    
    posicionY = 500
    Printer.CurrentY = posicionY
    Printer.FontName = "Arial"
    Printer.FontSize = 12
    Printer.FontBold = True
    Printer.CurrentX = margenIzquierdo
    Printer.Print rsPrueba("desigPrueba")
    
    posicionY = Printer.CurrentY + constSaltoLinea ' Establece la posici�n de la l�nea.
    ' Dibuja una l�nea atravesando la p�gina.
    Printer.Line (margenIzquierdo, posicionY)-(Printer.ScaleWidth - margenDerecho, posicionY)   ' Dibuja una l�nea.

    Printer.FontSize = 10
    Printer.FontBold = False

    ' Nombre del Paciente
    posicionY = posicionY + 50
    Printer.CurrentY = posicionY
    Printer.CurrentX = margenIzquierdo
    Printer.Print rsPrueba("paciente")
    
    ' Historia/Caso
    Printer.CurrentY = posicionY
    Printer.CurrentX = margenIzquierdo + 8000
    Printer.Print historia & "/" & caso
    posicionY = posicionY + constSaltoLinea
    ' Secci�n de Laboratorio
    Printer.CurrentY = posicionY
    Printer.CurrentX = margenIzquierdo
    Printer.Print rsPrueba("desigLab")
    Printer.CurrentY = posicionY
    
    ' Cama del paciente
    If Not IsNull(rsPrueba("cama")) Then
        Printer.CurrentX = margenIzquierdo + 8000
        Printer.Print rsPrueba("cama")
        Printer.CurrentY = posicionY
    End If
    
    ' Datos de la petici�n
    posicionY = posicionY + constSaltoLinea ' Establece la posici�n de la l�nea.
    ' Dibuja una l�nea atravesando la p�gina.
    Printer.Line (margenIzquierdo, posicionY)-(Printer.ScaleWidth - margenDerecho, posicionY)   ' Dibuja una l�nea.
    
    posicionY = posicionY + 50
    Printer.CurrentY = posicionY

    Printer.CurrentX = margenIzquierdo
    Printer.FontBold = True
    Printer.FontUnderline = True
    Printer.Print "Solicita"
    Printer.CurrentY = posicionY
        
    Printer.FontBold = False
    Printer.FontUnderline = False
    
    ' Dpto Solicitante
    Printer.CurrentX = margenIzquierdo + 2000
    Printer.Print rsPrueba("desigDpt")
    Printer.CurrentY = posicionY
    
    ' Dr Solicitante
    If Not IsNull(rsPrueba("txtFirma")) Then
        Printer.CurrentX = margenIzquierdo + 5000
        Printer.Print rsPrueba("txtFirma")
        Printer.CurrentY = posicionY
    End If
    
    ' Fecha de Petici�n
    Printer.CurrentX = margenIzquierdo + 8000
    Printer.Print Format$(rsPrueba("fechaPeticion"), "dd/mm/yy hh:nn")
    Printer.CurrentY = posicionY
    
        ' Datos de la realizaci�n
    posicionY = posicionY + constSaltoLinea
    
    Printer.CurrentY = posicionY
    
    Printer.CurrentX = margenIzquierdo
    Printer.FontBold = True
    Printer.FontUnderline = True
    Printer.Print "Realiza"
    Printer.CurrentY = posicionY
    
    Printer.FontBold = False
    Printer.FontUnderline = False
    
    ' Dpto que realiza
    Printer.CurrentX = margenIzquierdo + 2000
    Printer.Print "Microbiolog�a"
    Printer.CurrentY = posicionY
    
    ' Dr que realiza
    If Not IsNull(rsPrueba("drRealiza")) Then
        Printer.CurrentX = margenIzquierdo + 5000
        Printer.Print rsPrueba("drRealiza")
        Printer.CurrentY = posicionY
    End If
    
    ' Fecha de Realizaci�n
    If Not IsNull(rsPrueba("fechaFirma")) Then
        Printer.CurrentX = margenIzquierdo + 8000
        Printer.Print Format$(rsPrueba("fechaFirma"), "dd/mm/yy hh:nn")
        Printer.CurrentY = posicionY
    End If
    
    posicionY = posicionY + constSaltoLinea
    Printer.CurrentY = posicionY
    Printer.CurrentX = margenIzquierdo
    Printer.FontBold = True
    Printer.Print nRef
    Printer.FontBold = False
    Printer.CurrentY = posicionY
    
    
    posicionY = posicionY + constSaltoLinea ' Establece la posici�n de la l�nea.
    ' Dibuja una l�nea atravesando la p�gina.
    Printer.Line (margenIzquierdo, posicionY)-(Printer.ScaleWidth - margenDerecho, posicionY)   ' Dibuja una l�nea.
    
    posicionY = posicionY + constSaltoLinea
    Printer.CurrentY = posicionY

    rsPrueba.Close
    qryPrueba.Close
End Sub


Public Sub pImprimirInformeResultado(nRef$)
    ' Imprime el informe exhaustivo de los resultados de las pruebas
    
    Screen.MousePointer = vbHourglass
    ' Se inicializa la impresi�n. La subrutina se encuentra en modHojasTrabajo
    pComenzarImpresion
    
    ' Se pone la cabecera de la prueba
    pImprimirCabecera nRef
    
    ' Se introducen los datos de la observaci�n directa
    pImprimirObservacionDirecta nRef
    
    ' Se introducen los microorganismos identificados
    pImprimirMicroorganismos nRef
    
    ' Se introducen las colonias identificadas
    pImprimirColonias nRef
    
    ' Se introducen los antibiogramas de los microorganismos
    ' identificados
    pImprimirAntibiograma nRef
    
    ' Se imprimen los resultados tipo 'Resultado'
    pImprimirResultadosTipoResultado nRef
    
    ' Se imprimen las observaciones a la validaci�n
    'pImprimirObservacionesValidacion nRef
    
    ' Se ordena la finalizaci�n de la impresi�n
    pFinalizarImpresion
    
    Screen.MousePointer = vbDefault

End Sub

Private Sub pImprimirAntibiograma(nRef$)
    ' Se imprimen los antibiogramas de los microorganismos
    ' identificados
    
    Dim arInfAntib() As typeInfAntib    ' Vector que almacena las dieferntes elecciones encontradas
    'Dim arPosColonias(1 To 4 * constNColInforme) As Integer ' Posici�n de las colonias en las columnas de elecci�n
    Dim arPosColonias() As Integer
    Dim iElec As Integer
    Dim nAntib As Integer   ' N� de Antibi�ticos utilizados
    Dim blnLocalizado As Boolean
    Dim i As Integer
    Dim j As Integer

    
    On Error Resume Next
    
    
    iElec = fCargarAntibiograma(nRef, arInfAntib(), arPosColonias(), nAntib)
    
    If iElec > 0 Then
        Printer.FontBold = True
        Printer.CurrentX = margenIzquierdo
        ' Se obtiene el nombre del antibiograma

        Printer.Print strDesignacionAntibiograma & ":"
        
        Printer.FontBold = False
        
        pEscribirAntibiograma arInfAntib(), arPosColonias(), iElec, nAntib, False
    
        ' Si se intenta informar concentraciones, se mira primero si hay alguna
        ' Si no -> se sale de la funci�n

        For i = 1 To iElec
            For j = 1 To nAntib
                If arInfAntib(i).arAntib(i).infConc = True Then
                    blnLocalizado = True
                    Exit For
                End If
            Next j
            If blnLocalizado = True Then
                Exit For
            End If
        Next i
        If blnLocalizado = True Then
            Printer.FontBold = True
            Printer.CurrentX = margenIzquierdo
            ' Se obtiene el nombre del antibiograma
    
            Printer.CurrentY = Printer.CurrentY + constSaltoLinea
            Printer.Print "Concentraci�n (mg/mL): "
            
            Printer.FontBold = False
            
            pEscribirAntibiograma arInfAntib(), arPosColonias(), iElec, nAntib, True
        End If

                
        'Printer.CurrentY = Printer.CurrentY + 50
    End If

    
End Sub
Private Sub pImprimirColonias(nRef$)
    ' Se imprimen las colonias identificadas y que se ha indicado
    ' que se informa
    
    Dim SQL As String
    Dim qryMicro As rdoQuery
    Dim rsMicro As rdoResultset
    Dim qryRecuento As rdoQuery
    Dim rsRecuento As rdoResultset
    Dim texto As String
    Dim cMuestra As String
    Dim posicionY As Integer
    Dim observMicro As String
    Dim recuento As String
    
    On Error Resume Next
    
    posicionY = Printer.CurrentY
    
    SQL = "SELECT"
    SQL = SQL & "   MB2700.MB27_Descrip, MB1100.MB11_Descrip, "
    SQL = SQL & "   MB1200.MB12_Descrip, MB1800.MB18_Descrip, "
    SQL = SQL & "   MB2700.MB27_CODCol"
    SQL = SQL & " FROM "
    SQL = SQL & "   MB2700,MB1100,MB1200,MB1800"
    SQL = SQL & " WHERE "
    SQL = SQL & "   MB1100.MB11_CODGeMicro (+) = MB2700.MB11_CODGeMicro"
    SQL = SQL & "   AND MB1200.MB12_CODMorf (+) = MB2700.MB12_CODMorf"
    SQL = SQL & "   AND MB1800.MB18_CODMicro (+) = MB2700.MB18_CODMicro"
    SQL = SQL & "   AND MB2700.nref = ?"
    SQL = SQL & "   AND MB2700.MB36_CODEstCol = ?"
    SQL = SQL & " ORDER BY "
    SQL = SQL & "   MB2700.MB27_CODCol"
    
    Set qryMicro = objApp.rdoConnect.CreateQuery("Informe Resultado", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    qryMicro(0) = nRef
    qryMicro(1) = constCOLINFORMADA
    Set rsMicro = qryMicro.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    
    
    ' Se prepara el statment para ver si hay alguna prueba tipo recuento
    SQL = "SELECT MB32_Descrip, MB33_Result, unidades.designacion"
    SQL = SQL & " FROM "
    SQL = SQL & "   MB2000,MB0900,MB3200,MB3300, unidades"
    SQL = SQL & " WHERE "
    SQL = SQL & "   MB3300.NREF = MB2000.NREF"
    SQL = SQL & "   AND MB3300.MB20_CODTECASIST = MB2000.MB20_CODTECASIST"
    SQL = SQL & "   AND MB3200.MB09_CODTEC= MB3300.MB09_CODTEC"
    SQL = SQL & "   AND MB3200.MB32_CODRESULT= MB3300.MB32_CODRESULT"
    SQL = SQL & "   AND MB0900.MB09_CODTEC = MB2000.MB09_CODTEC"
    SQL = SQL & "   AND unidades.cUnidad = MB3300.cUnidad"
    SQL = SQL & "   AND MB2000.NREF = ?"
    SQL = SQL & "   AND MB0900.MB04_CODTITEC=?"
    SQL = SQL & "   AND MB3300.MB38_CODESTRESULT=?"
    SQL = SQL & "   AND MB2000.MB20_TIORIG = " & constORIGENCOLONIA
    SQL = SQL & "   AND MB2000.MB20_CODORIG = ?"
    Set qryRecuento = objApp.rdoConnect.CreateQuery("Informe Resultado Recuento", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If

    If Not rsMicro.EOF Then
        Printer.FontBold = True
        Printer.CurrentX = margenIzquierdo
        Printer.Print "Cultivos:"
        Printer.CurrentY = posicionY
        
        Printer.FontBold = False
            
        Do While Not rsMicro.EOF
            observMicro = ""
            observMicro = rsMicro!MB27_Descrip
            'Printer.CurrentX = margenIzquierdo + 250
            posicionY = posicionY + constSaltoLinea
            Printer.CurrentY = posicionY
            texto = rsMicro!MB27_CODCOL & "- "
            If Not IsNull(rsMicro!MB18_DESCRIP) Then
                texto = texto & rsMicro!MB18_DESCRIP
            ElseIf Not IsNull(rsMicro!MB12_DESCRIP) Then
                texto = texto & rsMicro!MB12_DESCRIP
            ElseIf Not IsNull(rsMicro!MB11_DESCRIP) Then
                texto = texto & rsMicro!MB11_DESCRIP
            End If
            ' Se mira si dispone de t�cnicas de recuento
            qryRecuento(0) = nRef
            qryRecuento(1) = constRECUENTO
            qryRecuento(2) = constRESULTINFORMADO
            qryRecuento(3) = rsMicro!MB27_CODCOL
            Set rsRecuento = qryRecuento.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If Not rsRecuento.EOF Then
                recuento = ""
                recuento = rsRecuento!MB33_result
                If Val(recuento) > constMAXRECUENTO Then
                    recuento = ">" & constMAXRECUENTO
                End If
                texto = texto & " (" & rsRecuento!MB32_DESCRIP & ": " & recuento & rsRecuento!Designacion & ")"
            End If
            rsRecuento.Close
            'Printer.Print texto
            'Printer.CurrentY = posicionY
            If observMicro <> "" Then
                'posicionY = posicionY + constSaltoLinea
                'Printer.CurrentY = posicionY
                'Printer.CurrentX = margenIzquierdo + 500
                texto = texto & ". " & observMicro
                'Printer.Print " - " & rsMicro!MB27_Descrip
                'Printer.CurrentY = posicionY
            End If
            posicionY = fImprimirTexto(texto, 250, 0)
            Printer.CurrentY = posicionY
            rsMicro.MoveNext
        Loop
        Printer.CurrentY = posicionY + constSaltoParrafo
    End If
    qryRecuento.Close
    rsMicro.Close
    qryMicro.Close
    
End Sub

Private Sub pImprimirMicroorganismos(nRef$)
    ' Se imprimen los microorganismos identificados
    
    Dim SQL As String
    Dim qryMicro As rdoQuery
    Dim rsMicro As rdoResultset
    Dim texto As String
    Dim posicionY As Integer
    On Error Resume Next
    
    posicionY = Printer.CurrentY
    
    SQL = "SELECT"
    SQL = SQL & "   MB1200.MB12_Descrip, MB2100.MB21_numLoc "
    SQL = SQL & " FROM "
    SQL = SQL & "   MB2100, MB1200"
    SQL = SQL & " WHERE "
    SQL = SQL & "   MB1200.MB12_CODMORF = MB2100.MB12_CODMORF"
    SQL = SQL & "   AND MB2100.nref = ?"
    SQL = SQL & " ORDER BY "
    SQL = SQL & "   MB1200.MB12_Descrip"
    
    Set qryMicro = objApp.rdoConnect.CreateQuery("Morfolog�as en Informe Resultado", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    qryMicro(0) = nRef
    Set rsMicro = qryMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    If Not rsMicro.EOF Then
        Printer.FontBold = True
        Printer.CurrentX = margenIzquierdo
        Printer.Print "Microorganismos:"
        Printer.CurrentY = posicionY
        
        Printer.FontBold = False
                        
        Do While Not rsMicro.EOF
            texto = texto & rsMicro!MB12_DESCRIP
            If rsMicro!MB21_numLoc > 1 Then
                texto = texto & "(" & rsMicro!MB21_numLoc & "); "
            Else
                texto = texto & "; "
            End If
            rsMicro.MoveNext
        Loop
        If Right(texto, 2) = "; " Then
            texto = Left(texto, Len(texto) - 2)
        End If
        
        posicionY = posicionY + constSaltoLinea
        Printer.CurrentY = posicionY
    
        posicionY = fImprimirTexto(texto, 500, 0)
    End If
    rsMicro.Close
    qryMicro.Close
   
End Sub
Public Sub pImprimirModelo()
    Dim i As Integer
    Dim posicion As Integer
    Printer.Orientation = vbPRORPortrait
    Printer.PaperSize = vbPRPSA4 'Se establece como p�gina el A4
    
    For i = 1 To 15000 Step 500
        Printer.CurrentX = i
        Printer.Print i
    Next i
    
    For i = 1 To 20000 Step 500
        If posicion > 15000 Then
            Printer.NewPage
            posicion = 0
        End If
        posicion = posicion + 500
        Printer.CurrentY = posicion
        Printer.Print i
    Next i
    Printer.EndDoc
End Sub

Private Sub pImprimirObservacionDirecta(nRef$)
    ' Se rellena la secci�n de observaci�n directa en el
    ' informe de resultados.
    
    Dim SQL As String
    Dim qryPrueba As rdoQuery
    Dim rsPrueba As rdoResultset
    Dim qryMicro As rdoQuery
    Dim rsMicro As rdoResultset
    Dim texto As String
    Dim cMuestra As String
    Dim posicionY As Integer
    
    posicionY = Printer.CurrentY
    
    Printer.FontBold = True
    Printer.CurrentX = margenIzquierdo
    Printer.Print "Datos de las muestras:"
    Printer.CurrentY = posicionY
    
    Printer.FontBold = False
    
    ' Se obtienen los datos de las muestras
    SQL = "SELECT"
    SQL = SQL & "   MB2000.cMuestra, MB3300.MB33_Result,"
    SQL = SQL & "   MB3200.MB32_Descrip, u.designacion, ma.fechaExtraccion, tm.designacion as tipoMuestra"
    SQL = SQL & " FROM "
    SQL = SQL & "   MB2000, MB3300, MB3200, unidades u, MB0900, muestraAsistencia ma, tiposMuestras tm"
    SQL = SQL & " WHERE "
    SQL = SQL & "   MB3300.nRef = MB2000.nRef"
    SQL = SQL & "   AND MB3300.MB20_CODTecAsist=MB2000.MB20_CODTecAsist"
    SQL = SQL & "   AND MB3200.MB09_CODTec = MB3300.MB09_CODTec"
    SQL = SQL & "   AND MB3200.MB32_CODResult = MB3300.MB32_CODResult"
    SQL = SQL & "   AND u.cUnidad = MB3300.cUnidad"
    SQL = SQL & "   AND MB0900.MB09_CODTEC=MB2000.MB09_CODTEC"
    SQL = SQL & "   AND ma. cMuestra = MB2000.cMuestra"
    SQL = SQL & "   AND tm. cTipoMuestra = ma.cTipoMuestra"
    SQL = SQL & "   AND MB2000.nRef = ?"
    SQL = SQL & "   AND MB2000.MB20_tiOrig = ?"
    SQL = SQL & "   AND MB38_CODESTRESULT = " & constRESULTINFORMADO
    SQL = SQL & "   AND MB0900.MB04_CODTITEC<> " & constRESULTADOS
    SQL = SQL & " ORDER BY "
    SQL = SQL & "   MB2000.cMuestra, MB2000.MB20_CODTecAsist"
    
    On Error Resume Next
    Set qryPrueba = objApp.rdoConnect.CreateQuery("Informe Resultado", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    qryPrueba(0) = nRef
    qryPrueba(1) = constORIGENMUESTRA
    Set rsPrueba = qryPrueba.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    Do While Not rsPrueba.EOF
        If cMuestra <> rsPrueba!cMuestra Then
            cMuestra = rsPrueba!cMuestra
            Printer.FontBold = True
            posicionY = posicionY + constSaltoLinea
            Printer.CurrentY = posicionY
            Printer.CurrentX = margenIzquierdo + 250
            Printer.Print rsPrueba!cMuestra & " " & rsPrueba!tipoMuestra & " (" & rsPrueba!fechaExtraccion & ")"
            Printer.CurrentY = posicionY
            Printer.FontBold = False
        End If
        posicionY = posicionY + constSaltoLinea
        Printer.CurrentY = posicionY
        texto = rsPrueba!MB32_DESCRIP & ": " & rsPrueba!MB33_result & " " & rsPrueba!Designacion
        posicionY = fImprimirTexto(texto, 500, 0)
        Printer.CurrentY = posicionY
        rsPrueba.MoveNext
'        On Error Resume Next
'        If rsPrueba.EOF Or cMuestra <> rsPrueba!cMuestra Then
'            ' Se muestran los microorganismos identificados
'            ' en la visi�n directa sobre la muestra
'            SQL = "SELECT"
'            SQL = SQL & "   MB1200.MB12_Descrip, MB2100.MB21_NumLoc"
'            SQL = SQL & " FROM"
'            SQL = SQL & "   MB2100, MB1200"
'            SQL = SQL & " WHERE"
'            SQL = SQL & "   MB1200.MB12_CODMorf=MB2100.MB12_CODMorf"
'            SQL = SQL & "   AND MB2100.nRef = ?"
'            SQL = SQL & "   AND MB2100.cMuestra = ?"
'            Err = 0
'            Set qryMicro = objApp.rdoConnect.CreateQuery("Informe Resultado Micro", SQL)
'            If Err <> 0 Then
'                MsgBox Error
'                Exit Sub
'            End If
'            qryMicro(0) = nRef
'            qryMicro(1) = cMuestra
'            Set rsMicro = qryMicro.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'            If Err <> 0 Then
'                MsgBox Error
'                Exit Sub
'            End If
'            Do While Not rsMicro.EOF
'                posicionY = posicionY + constSaltoLinea
'                Printer.CurrentY = posicionY
'                Printer.CurrentX = margenIzquierdo + 500
'                texto = rsMicro!MB21_numLoc & " " & rsMicro!MB12_Descrip
'                Printer.Print texto
'                Printer.CurrentY = posicionY
'                rsMicro.MoveNext
'            Loop
'            cMuestra = rsPrueba!cMuestra
'            rsMicro.Close
'            qryMicro.Close
'        End If
    Loop
    rsPrueba.Close
    qryPrueba.Close
    Printer.CurrentY = posicionY + constSaltoParrafo
End Sub

Private Sub pImprimirObservacionesValidacion(nRef$)
    Dim posicionY As Long
    Dim qryObserv As rdoQuery
    Dim rsObserv As rdoResultset
    Dim SQL As String
    Dim texto As String
    
    posicionY = Printer.CurrentY
    Err = 0
    ' Se obtienen las observaciones a la validaci�n
    
    SQL = "SELECT "
    SQL = SQL & "  pa.historia,pa.caso,pa.secuencia,pa.nrepeticion, sp.comentarios"
    SQL = SQL & " FROM "
    SQL = SQL & "   seguimientoPrueba sp, pruebaAsistencia pa"
    SQL = SQL & " WHERE"
    SQL = SQL & "   sp.historia=pa.historia"
    SQL = SQL & "   AND sp.caso=pa.caso"
    SQL = SQL & "   AND sp.secuencia=pa.secuencia"
    SQL = SQL & "   AND sp.nRepeticion = pa.nRepeticion"
    SQL = SQL & "   AND sp.proceso=" & constPROCESOVALIDACION
    SQL = SQL & "   AND pa.NRef = ?"
    
    Set qryObserv = objApp.rdoConnect.CreateQuery("Informe Resultado", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    qryObserv(0) = nRef
    
    Set rsObserv = qryObserv.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If

    If Not rsObserv.EOF Then
        posicionY = posicionY + constSaltoParrafo
        Printer.CurrentY = posicionY
        Printer.FontBold = True
        Printer.CurrentX = margenIzquierdo
        Printer.Print "Observaciones:"
        Printer.CurrentY = posicionY
        
        Printer.FontBold = False
        
        texto = ""
        If Not IsNull(rsObserv!comentarios) Then
            texto = rsObserv!comentarios
        End If
        posicionY = fImprimirTexto(texto, 500, 0)
        
    End If
    rsObserv.Close
    qryObserv.Close
End Sub

 Private Sub pImprimirResultadosTipoResultado(nRef$)
    ' Se rellena la secci�n observaciones final con los resultados tipo Resultado
    
    Dim SQL As String
    Dim qryPrueba As rdoQuery
    Dim rsPrueba As rdoResultset
    Dim qryMicro As rdoQuery
    Dim rsMicro As rdoResultset
    Dim texto As String
    Dim cMuestra As String
    Dim posicionY As Integer
    
    posicionY = Printer.CurrentY
        
    Printer.FontBold = False
    
    ' Se obtienen los datos de las muestras
    SQL = "SELECT"
    SQL = SQL & "   MB2000.cMuestra, MB3300.MB33_Result,"
    SQL = SQL & "   MB3200.MB32_Descrip, u.designacion"
    SQL = SQL & " FROM "
    SQL = SQL & "   MB2000, MB3300, MB3200, unidades u, MB0900"
    SQL = SQL & " WHERE "
    SQL = SQL & "   MB3300.nRef = MB2000.nRef"
    SQL = SQL & "   AND MB3300.MB20_CODTecAsist=MB2000.MB20_CODTecAsist"
    SQL = SQL & "   AND MB3200.MB09_CODTec = MB3300.MB09_CODTec"
    SQL = SQL & "   AND MB3200.MB32_CODResult = MB3300.MB32_CODResult"
    SQL = SQL & "   AND u.cUnidad = MB3300.cUnidad"
    SQL = SQL & "   AND MB0900.MB09_CODTEC=MB2000.MB09_CODTEC"
    SQL = SQL & "   AND MB2000.nRef = ?"
    SQL = SQL & "   AND MB2000.MB20_tiOrig = ?"
    SQL = SQL & "   AND MB38_CODESTRESULT = " & constRESULTINFORMADO
    SQL = SQL & "   AND MB0900.MB04_CODTITEC = " & constRESULTADOS
    SQL = SQL & " ORDER BY "
    SQL = SQL & "   MB2000.cMuestra, MB2000.MB20_CODTecAsist"
    
    On Error Resume Next
    Set qryPrueba = objApp.rdoConnect.CreateQuery("Informe Resultado", SQL)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    qryPrueba(0) = nRef
    qryPrueba(1) = constORIGENMUESTRA
    Set rsPrueba = qryPrueba.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Err <> 0 Then
        MsgBox Error
        Exit Sub
    End If
    Do While Not rsPrueba.EOF
        posicionY = posicionY + constSaltoLinea
        Printer.CurrentY = posicionY
        Printer.FontBold = True
        Printer.CurrentX = margenIzquierdo
        Printer.Print rsPrueba!MB32_DESCRIP
        Printer.FontBold = False
        texto = rsPrueba!MB33_result & " " & rsPrueba!Designacion
        posicionY = fImprimirTexto(texto, 500, 0)
        Printer.CurrentY = posicionY
        rsPrueba.MoveNext
    Loop
    rsPrueba.Close
    qryPrueba.Close
    Printer.CurrentY = posicionY + constSaltoParrafo
 
 End Sub


Private Function fSaltarPaginaAntibiotico(posicionY%, lngInicioYLineaVertical As Long, lngFinYLineaVertical As Long, iElec%) As Boolean
    Dim res As Boolean
    Dim i As Integer
    Dim lngPosicionXLineaVertical As Long
    
    
    res = fSaltarPagina(posicionY)
    
    ' Si va a saltar de p�gina, se introducen las l�neas verticales
    If res = True Then
        For i = 2 To iElec + 2
            lngFinYLineaVertical = posicionY '- constSaltoLinea
            lngPosicionXLineaVertical = xNombreAntib + (i - 1) * (dxNombreColonia + constNColInforme * dxResultColonia + dxFinInicio / 2)
            If i = iElec + 1 Then
                lngPosicionXLineaVertical = lngPosicionXLineaVertical + dxFinInicio / 2
            End If
            Printer.Line (lngPosicionXLineaVertical, lngInicioYLineaVertical)-(lngPosicionXLineaVertical, lngFinYLineaVertical)   ' Dibuja una l�nea.
        Next i
    
        res = fSaltoPagina(posicionY)
        'posicionY = posicionY + constSaltoParrafo
        Printer.CurrentY = posicionY
        lngInicioYLineaVertical = Printer.CurrentY
    End If
    
End Function


