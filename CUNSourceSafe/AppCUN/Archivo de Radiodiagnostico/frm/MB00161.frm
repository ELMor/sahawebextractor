VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmI_HojaRealizacion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Impresi�n de Hojas de Realizaci�n"
   ClientHeight    =   4290
   ClientLeft      =   1200
   ClientTop       =   1635
   ClientWidth     =   9315
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4290
   ScaleWidth      =   9315
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbInicio 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   688
      _Version        =   327682
   End
   Begin VB.Frame fraCarpeta 
      Caption         =   "Carpetas"
      Height          =   3375
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   9075
      Begin TabDlg.SSTab tabCarpeta 
         Height          =   2775
         HelpContextID   =   90001
         Index           =   0
         Left            =   180
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   300
         Width           =   8715
         _ExtentX        =   15372
         _ExtentY        =   4895
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00161.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtCarpeta(3)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtCarpeta(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtCarpeta(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtCarpeta(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cmdCarpetas(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtCarpeta(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtCarpeta(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).ControlCount=   11
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdSSCarpeta(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H80000004&
            DataField       =   "cCarpeta"
            Height          =   285
            Index           =   5
            Left            =   6120
            TabIndex        =   15
            Top             =   660
            Visible         =   0   'False
            Width           =   795
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   4
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "N� T�cnicas|N� T�cnicas a Realizar en la Carpeta"
            Text            =   "40"
            Top             =   1620
            Width           =   855
         End
         Begin VB.CommandButton cmdCarpetas 
            Caption         =   "Pruebas"
            Height          =   495
            Index           =   0
            Left            =   6720
            TabIndex        =   0
            Top             =   1140
            Width           =   1215
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H80000004&
            DataField       =   "cCarpeta"
            Height          =   285
            Index           =   0
            Left            =   5100
            TabIndex        =   11
            Top             =   660
            Visible         =   0   'False
            Width           =   795
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H00C0C0C0&
            DataField       =   "designacion"
            Height          =   285
            Index           =   1
            Left            =   2220
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Carpeta|Carpeta"
            Top             =   660
            Width           =   2175
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   2
            Left            =   2280
            MaxLength       =   2
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "N� T�cnicas|N� T�cnicas a Realizar en la Carpeta"
            Text            =   "40"
            Top             =   1260
            Width           =   855
         End
         Begin VB.TextBox txtCarpeta 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   3
            Left            =   5100
            MaxLength       =   2
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "N� Muestras|N�mero de muestras a utilizar para las t�cnicas"
            Text            =   "11"
            Top             =   1260
            Width           =   855
         End
         Begin SSDataWidgets_B.SSDBGrid grdSSCarpeta 
            Height          =   2565
            Index           =   0
            Left            =   -74880
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   60
            Width           =   8175
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   14420
            _ExtentY        =   4524
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� T�cnicas Observ. Directa"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   14
            Top             =   1620
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carpeta"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   10
            Top             =   660
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� T�cnicas"
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   9
            Top             =   1260
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Pruebas"
            Height          =   255
            Index           =   1
            Left            =   3660
            TabIndex        =   8
            Top             =   1260
            Width           =   1215
         End
      End
   End
   Begin ComctlLib.StatusBar stbInicio 
      Align           =   2  'Align Bottom
      Height          =   240
      Left            =   0
      TabIndex        =   2
      Top             =   4050
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   423
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.ImageList imlIconos 
      Left            =   480
      Top             =   300
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
End
Attribute VB_Name = "frmI_HojaRealizacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim objCarpetaInfo As New clsCWForm
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Private Sub pActualizarDatos()
    ' Actualiza los datos de n� de pruebas y t�cnicas en la ventana
    
    Dim SQL As String
    Dim qryDatos As rdoQuery
    Dim rsDatos As rdoResultset
    
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec, COUNT(distinct pA.nRef) as nPrueba"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND MB2000.MB20_fecLect < TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & " AND pA.estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND MB2000.MB34_codEstTecAsist = ?"
    SQL = SQL & " GROUP BY pA.cCarpeta"
    
    Set qryDatos = objApp.rdoConnect.CreateQuery("Datos Carpeta", SQL)
    qryDatos(0) = txtCarpeta(0).Text
    qryDatos(1) = Format$(DateAdd("d", 1, fFechaHoraActual), "dd/mm/yyyy")
    qryDatos(2) = constTECREALIZADA
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        txtCarpeta(2).Text = rsDatos!nTec
        txtCarpeta(3).Text = rsDatos!nPrueba
    Else
        txtCarpeta(2).Text = ""
        txtCarpeta(3).Text = ""
    End If
    rsDatos.Close
    qryDatos.Close
    
    ' N� T�cnicas de Observaci�n Directa
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP, " & objEnv.GetValue("Main") & "MB0900"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND MB0900.MB09_CODTEC = MB2000.MB09_CODTEC"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND MB2000.MB20_fecLect < TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & " AND pA.estado=" & constPRUEBAREALIZANDO
    SQL = SQL & " AND MB2000.MB34_codEstTecAsist = ?"
    SQL = SQL & " AND MB0900.MB04_CODTITEC=" & constOBSERVDIRECTA
    SQL = SQL & " GROUP BY pA.cCarpeta"
    
    Set qryDatos = objApp.rdoConnect.CreateQuery("Datos Carpeta", SQL)
    qryDatos(0) = txtCarpeta(0).Text
    qryDatos(1) = Format$(DateAdd("d", 1, fFechaHoraActual), "dd/mm/yyyy")
    qryDatos(2) = constTECREALIZADA
    Set rsDatos = qryDatos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsDatos.EOF Then
        txtCarpeta(4).Text = rsDatos!nTec
    Else
        txtCarpeta(4).Text = ""
    End If
    rsDatos.Close
    qryDatos.Close
    
End Sub

Private Sub pImprimirHoja()
        Dim res As Integer
        res = MsgBox("�Desea imprimir la hoja de realizaci�n de la carpeta '" & txtCarpeta(1).Text & "'?", vbQuestion + vbYesNo, "Hoja de Trabajo")
        If res = vbYes Then
            Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
            res = fImprimirHojaRealizacion(txtCarpeta(0).Text, "")
            Screen.MousePointer = vbDefault 'Call objApp.SplashOff
        End If
End Sub

Private Sub cmdCarpetas_Click(intIndex As Integer)
    If txtCarpeta(0).Text <> "" Then
        pVentanaPeticion (txtCarpeta(0).Text), (txtCarpeta(1).Text)
        ' Se actualizan los datos de n� de pruebas y n� de t�cnicas
        pActualizarDatos
    End If

End Sub


Private Sub Form_Load()

  Dim strKey As String
  Dim SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbInicio, stbInicio, _
                                cwWithToolBar + cwWithStatusBar + cwWithoutMenu)
  
  With objCarpetaInfo
    .strName = "CARPETA"
    Set .objFormContainer = fraCarpeta(0)
Set .objFatherContainer = Nothing
    Set .tabMainTab = tabCarpeta(0)
    Set .grdGrid = grdSSCarpeta(0)
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "carpetas"
    .strWhere = "cDptoSecc=" & departamento
    .intAllowance = cwAllowReadOnly
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    
    Call .FormAddOrderField("designacion", cwAscending)
    
    Call .objPrinter.Add("MB00110", "Hoja de Realizaci�n Completa")
    Call .objPrinter.Add("MB00111", "Hoja de Realizaci�n Resumida")
    Call .objPrinter.Add("MB00101", "Hoja de Trabajo de Observ. Directa por Carpeta", 1, crptToPrinter)
    Call .objPrinter.Add("MB00102", "Hoja de Trabajo de Observ. Directa Completa", 1, crptToPrinter)
 
    strKey = .strDataBase & .strTable
  End With
  
  With objWinInfo
    Call .FormAddInfo(objCarpetaInfo, cwFormDetail)
    Call .FormCreateInfo(objCarpetaInfo)
    
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec, COUNT(distinct pA.nRef) as nPrueba"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND MB2000.MB20_fecLect < TO_DATE(?,'DD/MM/YYYY')"
    'SQL = SQL & " AND pA.estado =" & constPRUEBAREALIZANDO
    SQL = SQL & " AND pA.estado IN (" & constPRUEBAREALIZANDO & "," & constPRUEBAVALIDADA & ")"
    SQL = SQL & " AND MB2000.MB34_codEstTecAsist = ?"
    SQL = SQL & " GROUP BY pA.cCarpeta"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtCarpeta(0)), "cCarpeta", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpeta(0)), txtCarpeta(2), "nTec")
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpeta(0)), txtCarpeta(3), "nPrueba")
  
    ' N� T�cnicas de Observaci�n Directa
    SQL = "SELECT cCarpeta, COUNT(distinct MB2000.NREF||MB20_CODTecAsist) as nTec"
    SQL = SQL & " FROM " & objEnv.GetValue("Main") & "pruebaAsistencia pA, " & objEnv.GetValue("Main") & "MB2000, " & objEnv.GetValue("Main") & "muestraPrueba mP, " & objEnv.GetValue("Main") & "MB0900"
    SQL = SQL & " WHERE MB2000.nRef= pa.nRef"
    SQL = SQL & " AND mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND MB0900.MB09_CODTEC = MB2000.MB09_CODTEC"
    SQL = SQL & " AND pa.cCarpeta =?"
    SQL = SQL & " AND MB2000.MB20_fecLect < TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & " AND pA.estado=" & constPRUEBAREALIZANDO
    SQL = SQL & " AND MB2000.MB34_codEstTecAsist = ?"
    SQL = SQL & " AND MB0900.MB04_CODTITEC=" & constOBSERVDIRECTA
    SQL = SQL & " GROUP BY pA.cCarpeta"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtCarpeta(5)), "cCarpeta", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtCarpeta(5)), txtCarpeta(4), "nTec")
  
    .CtrlGetInfo(txtCarpeta(0)).blnReadOnly = True
    .CtrlGetInfo(txtCarpeta(1)).blnReadOnly = True
    .CtrlGetInfo(txtCarpeta(2)).blnReadOnly = True
    .CtrlGetInfo(txtCarpeta(3)).blnReadOnly = True
    .CtrlGetInfo(txtCarpeta(3)).blnReadOnly = True
    
    .CtrlGetInfo(txtCarpeta(0)).blnInGrid = False
    '.CtrlGetInfo(txtCarpeta(2)).blnInGrid = True
    '.CtrlGetInfo(txtCarpeta(2)).blnNegotiated = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, UnloadMode As Integer)
intCancel = objWinInfo.WinExit
End Sub


Private Sub Form_Unload(Cancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo

End Sub


Private Sub fraCarpeta_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraCarpeta(intIndex), False, True)
End Sub

Private Sub grdSSCarpeta_DblClick(Index As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdSSCarpeta_GotFocus(Index As Integer)
   Call objWinInfo.CtrlGotFocus

End Sub


Private Sub grdSSCarpeta_RowColChange(Index As Integer, ByVal intLastRow As Variant, ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(intLastRow, intLastCol)

End Sub


Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
    If strFormName = objCarpetaInfo.strName Then
        If strCtrlName = "txtCarpeta(0)" Then
            aValues(2) = Format$(DateAdd("d", 1, fFechaHoraActual), "dd/mm/yyyy")
            aValues(3) = constTECREALIZADA
        ElseIf strCtrlName = "txtCarpeta(5)" Then
            aValues(2) = Format$(DateAdd("d", 1, fFechaHoraActual), "dd/mm/yyyy")
            aValues(3) = constTECREALIZADA
        End If
    End If
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim res As Integer
  Dim cCarpeta As String
  
    cCarpeta = objCarpetaInfo.rdoCursor("cCarpeta")
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    Select Case intReport
        Case 1, 2
            res = fImprimirHojaRealizacion(cCarpeta, objPrinter.filename(intReport))
        Case 2  ' Hoja de realizaci�n de t�cnicas directas por carpeta
            res = fImprimirHojaObDirecta(cCarpeta)
        Case 3  ' Hoja de realizaci�n de t�cnicas directas por secci�n
            res = fImprimirHojaObDirecta("")
    End Select
    Set objPrinter = Nothing



End Sub


Private Sub tabCarpeta_MouseDown(intIndex As Integer, Button As Integer, Shift As Integer, X As Single, y As Single)
  Call objWinInfo.FormChangeActive(tabCarpeta(intIndex), False, True)

End Sub


Private Sub tlbInicio_ButtonClick(ByVal btnButton As ComctlLib.Button)
    
    
    'Select Case btnButton.Key
    '    Case cwToolBarButtonPrint  ' Imprimir
    '        pImprimirHoja
    '    Case Else
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'End Select
End Sub

Private Sub txtCarpeta_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub txtCarpeta_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus

End Sub


Private Sub txtCarpeta_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus

End Sub


