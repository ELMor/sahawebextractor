VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmP_Recepcion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recepci�n de Muestras"
   ClientHeight    =   7470
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7470
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraTecnicaAsistencia 
      Caption         =   "T�cnicas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2235
      Index           =   0
      Left            =   60
      TabIndex        =   49
      Top             =   4920
      Width           =   11775
      Begin TabDlg.SSTab tabTecnicaAsistencia 
         Height          =   1890
         Index           =   0
         Left            =   120
         TabIndex        =   50
         Top             =   240
         Width           =   11475
         _ExtentX        =   20241
         _ExtentY        =   3334
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00150.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(12)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(17)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(18)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(19)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboSSTecnicaAsistencia(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cboSSTecnicaAsistencia(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtTecnicaAsistencia(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtTecnicaAsistencia(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cmdTecnicaAsistencia(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtTecnicaAsistencia(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtTecnicaAsistencia(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtTecnicaAsistencia(4)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtTecnicaAsistencia(5)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtTecnicaAsistencia(6)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "cmdTecnicaAsistencia(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "chkTecnicaAsistencia(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).ControlCount=   16
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "MB00150.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTecnicaAsistencia(0)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkTecnicaAsistencia 
            Caption         =   "Siembra"
            DataField       =   "MB20_INDSiembra"
            Height          =   195
            Index           =   0
            Left            =   1080
            TabIndex        =   64
            Top             =   1140
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.CommandButton cmdTecnicaAsistencia 
            Caption         =   "Hoja de Trabajo"
            Height          =   375
            Index           =   1
            Left            =   9660
            TabIndex        =   62
            Top             =   60
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox txtTecnicaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "MB20_CODPLACA"
            Height          =   315
            Index           =   6
            Left            =   960
            TabIndex        =   60
            Tag             =   "C�digo|C�digo T�cnica Asistencia"
            Top             =   720
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.TextBox txtTecnicaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "MB20_CODORIG"
            Height          =   315
            Index           =   5
            Left            =   960
            TabIndex        =   59
            Tag             =   "C�digo|C�digo T�cnica Asistencia"
            Top             =   420
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.TextBox txtTecnicaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "MB20_TIORIG"
            Height          =   315
            Index           =   4
            Left            =   60
            TabIndex        =   58
            Tag             =   "C�digo|C�digo T�cnica Asistencia"
            Top             =   1020
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.TextBox txtTecnicaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "cMuestra"
            Height          =   315
            Index           =   3
            Left            =   60
            TabIndex        =   57
            Tag             =   "C�digo|C�digo T�cnica Asistencia"
            Top             =   720
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.TextBox txtTecnicaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "NREF"
            Height          =   315
            Index           =   2
            Left            =   60
            TabIndex        =   56
            Tag             =   "C�digo|C�digo T�cnica Asistencia"
            Top             =   420
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.CommandButton cmdTecnicaAsistencia 
            Caption         =   "Problema"
            Height          =   375
            Index           =   0
            Left            =   9660
            TabIndex        =   24
            Top             =   780
            Width           =   1335
         End
         Begin VB.TextBox txtTecnicaAsistencia 
            DataField       =   "MB20_Reali"
            Height          =   975
            Index           =   1
            Left            =   3060
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   23
            Tag             =   "Indicaciones|Indicaciones a la Realizaci�n de la T�cnica"
            Top             =   780
            Width           =   5835
         End
         Begin VB.TextBox txtTecnicaAsistencia 
            BackColor       =   &H0000FFFF&
            DataField       =   "MB20_CODTECASIST"
            Height          =   315
            Index           =   0
            Left            =   1020
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "C�digo|C�digo T�cnica Asistencia"
            Top             =   120
            Width           =   915
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSTecnicaAsistencia 
            Height          =   315
            Index           =   0
            Left            =   6540
            TabIndex        =   22
            Tag             =   "Ambiente|Condiciones Ambientales de Realizaci�n"
            Top             =   120
            Width           =   2295
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5292
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4048
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTecnicaAsistencia 
            Height          =   1650
            Index           =   0
            Left            =   -74880
            TabIndex        =   55
            Top             =   120
            Width           =   10020
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17674
            _ExtentY        =   2910
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSTecnicaAsistencia 
            DataField       =   "MB09_CODTec"
            Height          =   315
            Index           =   1
            Left            =   3060
            TabIndex        =   21
            Tag             =   "T�cnica|Nombre de la T�cnica"
            Top             =   120
            Width           =   2295
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   3
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5292
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            Columns(2).Width=   3200
            Columns(2).Visible=   0   'False
            Columns(2).Caption=   "Tipo Tecnica"
            Columns(2).Name =   "Tipo Tecnica"
            Columns(2).DataField=   "Column 2"
            Columns(2).DataType=   8
            Columns(2).FieldLen=   256
            _ExtentX        =   4048
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicaciones:"
            Height          =   255
            Index           =   19
            Left            =   2100
            TabIndex        =   54
            Top             =   780
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Ambiente:"
            Height          =   255
            Index           =   18
            Left            =   5520
            TabIndex        =   53
            Top             =   120
            Width           =   915
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            Height          =   255
            Index           =   17
            Left            =   2100
            TabIndex        =   52
            Top             =   180
            Width           =   915
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            Height          =   255
            Index           =   12
            Left            =   120
            TabIndex        =   51
            Top             =   180
            Width           =   705
         End
      End
   End
   Begin VB.Frame fraPruebaAsistencia 
      Caption         =   "Pruebas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Index           =   0
      Left            =   60
      TabIndex        =   34
      Top             =   2280
      Width           =   11775
      Begin TabDlg.SSTab tabPruebaAsistencia 
         Height          =   2250
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   240
         Width           =   11445
         _ExtentX        =   20188
         _ExtentY        =   3969
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00150.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(11)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(13)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(14)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(15)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(16)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(20)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "cboSSPruebaAsistencia(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtPruebaAsistencia(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtPruebaAsistencia(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtPruebaAsistencia(2)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtPruebaAsistencia(3)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtPruebaAsistencia(4)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtPruebaAsistencia(5)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtPruebaAsistencia(6)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtPruebaAsistencia(7)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtPruebaAsistencia(8)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtPruebaAsistencia(10)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "chkPruebaAsistencia(0)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "chkPruebaAsistencia(1)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "cmdPruebaAsistencia(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "lvwPruebaAsistencia(0)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtPruebaAsistencia(9)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtPruebaAsistencia(11)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).ControlCount=   30
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPruebaAsistencia(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "Nacimiento\\pac.fNac"
            Height          =   285
            Index           =   11
            Left            =   9720
            TabIndex        =   65
            TabStop         =   0   'False
            Tag             =   "Fecha de Nac.|Fecha de Nacimiento del Paciente"
            Top             =   420
            Width           =   1215
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00FFFF00&
            Height          =   285
            Index           =   9
            Left            =   1020
            TabIndex        =   61
            TabStop         =   0   'False
            Tag             =   "N� Referencia|N�mero de Referencia"
            Top             =   1020
            Width           =   915
         End
         Begin ComctlLib.ListView lvwPruebaAsistencia 
            Height          =   1455
            Index           =   0
            Left            =   6360
            TabIndex        =   16
            Top             =   720
            Width           =   2415
            _ExtentX        =   4260
            _ExtentY        =   2566
            View            =   3
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            _Version        =   327682
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.CommandButton cmdPruebaAsistencia 
            Caption         =   "Aceptar"
            Height          =   375
            Index           =   0
            Left            =   9660
            TabIndex        =   0
            Top             =   1800
            Width           =   1335
         End
         Begin VB.CheckBox chkPruebaAsistencia 
            Alignment       =   1  'Right Justify
            Caption         =   "Externo:"
            Height          =   255
            Index           =   1
            Left            =   8820
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   1020
            Visible         =   0   'False
            Width           =   1125
         End
         Begin VB.CheckBox chkPruebaAsistencia 
            Alignment       =   1  'Right Justify
            Caption         =   "Urgente:"
            DataField       =   "urgenciaRealizacion"
            Height          =   255
            Index           =   0
            Left            =   8820
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Urgente|Requiere urgencia en la realizaci�n"
            Top             =   720
            Width           =   1125
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   825
            Index           =   10
            Left            =   1020
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   19
            TabStop         =   0   'False
            Tag             =   "Indicaciones|Indicaciones a la Solicitud de la Prueba"
            Top             =   1350
            Width           =   4395
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   8
            Left            =   3000
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Prueba|Prueba Solicitada"
            Top             =   690
            Width           =   2415
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   7
            Left            =   6360
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Dr. Solicitud|Doctor Solicitante"
            Top             =   390
            Width           =   2415
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   6
            Left            =   3000
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Dpto. Solicitud|Departamento Solicitante"
            Top             =   420
            Width           =   2415
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            Height          =   285
            Index           =   5
            Left            =   9720
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Cama|Cama de Ingreso del Paciente"
            Top             =   60
            Width           =   1215
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00C0C0C0&
            DataField       =   "paciente\\pac.nom||' '||pac.ap1||' '||pac.ap2"
            Height          =   285
            Index           =   4
            Left            =   3000
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Paciente|Nombre del Paciente"
            Top             =   90
            Width           =   5775
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H00FFFF00&
            DataField       =   "NRef"
            Height          =   285
            Index           =   3
            Left            =   1020
            TabIndex        =   9
            Tag             =   "N� Referencia|N�mero de Referencia"
            Top             =   990
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H0000FFFF&
            Height          =   285
            Index           =   2
            Left            =   1020
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "N� Secuencia|N�mero de Secuencia"
            Top             =   690
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H0000FFFF&
            Height          =   285
            Index           =   1
            Left            =   1020
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "N� Caso|N�mero de Caso"
            Top             =   390
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H0000FFFF&
            DataField       =   "historia\\pa.historia"
            Height          =   285
            Index           =   0
            Left            =   1020
            MaxLength       =   7
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "N� Historia|N�mero de Historia"
            Top             =   90
            Width           =   915
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPruebaAsistencia 
            Height          =   1830
            Index           =   0
            Left            =   -74910
            TabIndex        =   36
            Top             =   90
            Width           =   10200
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17992
            _ExtentY        =   3228
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSPruebaAsistencia 
            DataField       =   "cCarpeta"
            Height          =   315
            Index           =   0
            Left            =   3000
            TabIndex        =   14
            Tag             =   "Carpeta|Carpeta de destino de la Prueba"
            Top             =   990
            Width           =   2415
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1693
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5292
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4260
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Caption         =   "F. Nac.:"
            Height          =   255
            Index           =   20
            Left            =   8880
            TabIndex        =   66
            Top             =   420
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicaciones:"
            Height          =   255
            Index           =   16
            Left            =   60
            TabIndex        =   48
            Top             =   1350
            Width           =   960
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Protocolos:"
            Height          =   255
            Index           =   15
            Left            =   5520
            TabIndex        =   47
            Top             =   720
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Carpeta:"
            Height          =   255
            Index           =   14
            Left            =   2040
            TabIndex        =   46
            Top             =   990
            Width           =   975
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Prueba:"
            Height          =   255
            Index           =   13
            Left            =   2040
            TabIndex        =   45
            Top             =   690
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dr. Solic.:"
            Height          =   255
            Index           =   11
            Left            =   5520
            TabIndex        =   44
            Top             =   390
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Dpto. Solic.:"
            Height          =   255
            Index           =   10
            Left            =   2040
            TabIndex        =   43
            Top             =   390
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cama:"
            Height          =   255
            Index           =   9
            Left            =   8880
            TabIndex        =   42
            Top             =   60
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Paciente:"
            Height          =   255
            Index           =   8
            Left            =   2040
            TabIndex        =   41
            Top             =   90
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Ref:"
            Height          =   255
            Index           =   7
            Left            =   60
            TabIndex        =   40
            Top             =   990
            Width           =   900
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Secuencia:"
            Height          =   255
            Index           =   4
            Left            =   60
            TabIndex        =   39
            Top             =   690
            Width           =   900
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Caso:"
            Height          =   255
            Index           =   5
            Left            =   60
            TabIndex        =   38
            Top             =   390
            Width           =   720
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia:"
            Height          =   255
            Index           =   6
            Left            =   60
            TabIndex        =   37
            Top             =   90
            Width           =   705
         End
      End
   End
   Begin VB.Frame fraMuestraAsistencia 
      Caption         =   "Muestras"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Index           =   0
      Left            =   60
      TabIndex        =   25
      Top             =   420
      Width           =   11760
      Begin TabDlg.SSTab tabMuestraAsistencia 
         Height          =   1410
         Index           =   0
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   300
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   2487
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00150.frx":0054
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(3)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboSSMuestraAsistencia(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtMuestraAsistencia(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtMuestraAsistencia(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtMuestraAsistencia(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cmdMuestraAsistencia(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "cmdMuestraAsistencia(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdSSMuestraAsistencia(0)"
         Tab(1).ControlCount=   1
         Begin VB.CommandButton cmdMuestraAsistencia 
            Caption         =   "Paciente"
            Height          =   375
            Index           =   1
            Left            =   9660
            TabIndex        =   63
            Top             =   60
            Width           =   1335
         End
         Begin VB.CommandButton cmdMuestraAsistencia 
            Caption         =   "Problema"
            Height          =   375
            Index           =   0
            Left            =   9660
            TabIndex        =   5
            Top             =   750
            Width           =   1335
         End
         Begin VB.TextBox txtMuestraAsistencia 
            BackColor       =   &H00FFFF00&
            DataField       =   "fechaExtraccion"
            Height          =   315
            Index           =   2
            Left            =   7320
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "F. Extracci�n|Fecha de Extracci�n de la Muestra"
            Top             =   90
            Width           =   2175
         End
         Begin VB.TextBox txtMuestraAsistencia 
            BackColor       =   &H00FFFFFF&
            DataField       =   "comentariosExtraccion"
            Height          =   645
            Index           =   1
            Left            =   1440
            MaxLength       =   50
            ScrollBars      =   2  'Vertical
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Indicaciones|Indicaciones a la extracci�n de la Muestra"
            Top             =   510
            Width           =   8055
         End
         Begin VB.TextBox txtMuestraAsistencia 
            BackColor       =   &H0000FFFF&
            DataField       =   "cMuestra"
            Height          =   315
            Index           =   0
            Left            =   1440
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "C�digo|C�digo Muestra"
            Top             =   60
            Width           =   1035
         End
         Begin SSDataWidgets_B.SSDBGrid grdSSMuestraAsistencia 
            Height          =   1305
            Index           =   0
            Left            =   -74940
            TabIndex        =   29
            TabStop         =   0   'False
            Top             =   30
            Width           =   10275
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18124
            _ExtentY        =   2302
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSMuestraAsistencia 
            DataField       =   "cTipoMuestra"
            Height          =   315
            Index           =   0
            Left            =   3840
            TabIndex        =   2
            Tag             =   "Tipo de Muestra|Tipo de Muestra"
            Top             =   120
            Width           =   2295
            ScrollBars      =   2
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   -2147483640
            ForeColorOdd    =   -2147483640
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1693
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5292
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4048
            _ExtentY        =   556
            _StockProps     =   93
            Text            =   "Heces"
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha:"
            Height          =   255
            Index           =   1
            Left            =   6480
            TabIndex        =   33
            Top             =   90
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            Height          =   255
            Index           =   0
            Left            =   2700
            TabIndex        =   32
            Top             =   90
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            Height          =   255
            Index           =   2
            Left            =   180
            TabIndex        =   31
            Top             =   90
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Indicaciones:"
            Height          =   255
            Index           =   3
            Left            =   180
            TabIndex        =   30
            Top             =   510
            Width           =   1140
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   26
      Top             =   7185
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imlPruebaAsistencia 
      Index           =   0
      Left            =   0
      Top             =   420
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   7
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00150.frx":0070
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00150.frx":04C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00150.frx":091C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00150.frx":0D72
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00150.frx":11C8
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00150.frx":161E
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MB00150.frx":1A74
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmP_Recepcion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMuestraInfo As New clsCWForm
Dim objPruebaInfo As New clsCWForm
Dim objTecnicaInfo As New clsCWForm
Dim protocolos() As typeProtocolo
Dim cCond As String                 ' C�digo de condici�n ambiental de la t�cnica en pantalla
Dim cCarpeta As String              ' C�digo de carpeta de la prueba en pantalla


Private Sub pFormatearBotonAceptar()
    If txtPruebaAsistencia(0).Text <> "" And txtPruebaAsistencia(3).Text = "" Then
        cmdPruebaAsistencia(0).Enabled = True
    Else
        cmdPruebaAsistencia(0).Enabled = False
    End If
End Sub

Private Sub pFormatearNumeroMicro()
    If txtPruebaAsistencia(3).Text = "" Then
        fraTecnicaAsistencia(0).Enabled = False
    Else
        fraTecnicaAsistencia(0).Enabled = True
    End If
    txtPruebaAsistencia(9).Text = fNumeroMicro(txtPruebaAsistencia(3).Text)
End Sub

Private Sub pImprimirHoja()
    ' Se recogen las t�cnicas a preparar, se imprimen y se
    ' cambian de estado. El cambio de estado lleva consigo la
    ' actualizaci�n de la fecha en la que hay que volver a
    ' ver las t�cnicas
    
    Dim SQL As String
    Dim qryTec As rdoQuery
    Dim rsTec As rdoResultset
    Dim nRef As String
    Dim res As Boolean
    Dim reali As String
    Dim cTecOrig As String
    Dim fechaCreacion As String
    Dim arEtiquetas() As typeEtiquetas
    Dim blnSiembras As Boolean
    Dim cAutoanalizador  As String
    Dim estado As Integer
    
    
    blnSiembras = True
    fechaCreacion = fFechaHoraActual()
    ' Para las t�cnicas se obtiene lo siguiente:
    '       - N� Referencia
    '       - C�digo interno de la t�cnica
    '       - N�mero de Placa (si se trata de un cultivo)
    '       - C�digo de la t�cnica
    '       - Nombre de la t�cnica
    '       - C�digo de la condici�n ambiental
    '       - Nombre de la condici�n
    '       - Sobre qu� se realiza la t�cnica
    
    SQL = "SELECT MB2000.nRef, MB2000.MB20_CODTecAsist, MB2000.MB20_CODPlaca,MB2000.MB20_Reali"
    SQL = SQL & "   , MB0900.MB09_CodTec, MB0900.MB09_Desig"
    SQL = SQL & "   , MB0200.MB02_CodCond, MB0200.MB02_Desig"
    SQL = SQL & "   , MB2000.MB20_tiOrig, MB2000.MB20_CodOrig"
    SQL = SQL & "   , MB34_CODEstTecAsist, MB2000.MB20_CODTecAsist_Orig"
    SQL = SQL & "   , MB04_CODTITEC,MB03_Desig, mb0900.cAutoanalizador"
    SQL = SQL & " FROM MB2000, MB0900,MB0200,MB2600, MB0300"
    SQL = SQL & " WHERE MB2600.nRef=MB2000.nRef"
    SQL = SQL & "   AND MB2600.MB20_CodTecAsist=MB2000.MB20_CodTecAsist"
    SQL = SQL & "   AND MB2600.MB35_CODEstSegui=" & constSEGUITECVALIDA
    SQL = SQL & "   AND MB0900.MB09_CodTec=MB2000.MB09_CodTec"
    SQL = SQL & "   AND MB0200.MB02_CodCond=MB2600.MB02_CodCond"
    SQL = SQL & "   AND MB0300.MB03_codRecip=MB0900.MB03_codRecip"
    SQL = SQL & "   AND MB2000.nRef = ?"
    SQL = SQL & "   AND MB34_CODEstTecAsist = " & constTECSOLICITADA
    SQL = SQL & "   ORDER BY MB2000.nRef, MB2000.MB20_CODPlaca ,MB2000.MB20_CODTecAsist"
                                                                               
    Set qryTec = objApp.rdoConnect.CreateQuery("Tecnica", SQL)
    qryTec(0) = txtPruebaAsistencia(3).Text
    Set rsTec = qryTec.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rsTec.EOF Then
        Call pComenzarImpresion     ' Se prepara la impresora para imprimir
        Do While Not rsTec.EOF
            ' En el primer registro se carga el valor de nRef, para que no genere
            ' nueva p�gina
            'If nRef = "" Then nRef = rsTec!nRef
            
            reali = ""
            If Not IsNull(rsTec!MB20_Reali) Then
                reali = rsTec!MB20_Reali
            End If
            ' Se imprime la t�cnica
            If Not IsNull(rsTec!MB20_CODTecAsist_Orig) Then
                cTecOrig = rsTec!MB20_CODTecAsist_Orig
            End If
            If Not IsNull(rsTec!cAutoanalizador) Then
                cAutoanalizador = rsTec!cAutoanalizador
            Else
                cAutoanalizador = ""
            End If
            
            res = fImprimirTecnicaEnHoja(rsTec!nRef, rsTec!MB20_CODPlaca, rsTec!MB09_Desig, rsTec!MB02_Desig, rsTec!MB20_tiOrig, rsTec!MB20_codOrig, nRef, reali, rsTec!MB34_codEstTecAsist, cTecOrig, rsTec!MB04_CODTITEC, rsTec!MB20_CODTECASIST, rsTec!MB03_desig, cAutoanalizador, arEtiquetas(), fechaCreacion)
            If cAutoanalizador <> "" Then
                estado = constTECSOLICITAREALI
            Else
                estado = constTECREALIZADA
            End If
            ' Se da la t�cnica como realizada
            res = fRealizarTecnica(rsTec!nRef, rsTec!MB20_CODTECASIST, rsTec!MB09_codTec, rsTec!MB02_CODCond, "", estado, fechaCreacion, blnSiembras)
            rsTec.MoveNext
            ' Si es la �ltima o se cambia de paciente, se salta de p�gina
            'If Not rsTec.EOF Then
            '    If nRef <> rsTec!nRef Then
            '        nRef = rsTec!nRef
                '    Call pSaltoPagina
            '    End If
            'End If
        Loop
        Call pFinalizarImpresion
        Call pImprimirEtiquetas(arEtiquetas)
    End If
    rsTec.Close
    qryTec.Close

End Sub

Private Sub pInsertarResultados()
    Dim qryTecResult As rdoQuery
    Dim rsTecResult As rdoResultset
    Dim qryTecResultAsist As rdoQuery
    Dim rsTecResultAsist As rdoResultset
    
    Dim SQL As String
    Dim nRef As String
    Dim cTec As String
    Dim nTec As String
    
    
    nRef = txtPruebaAsistencia(3).Text
    nTec = txtTecnicaAsistencia(0).Text
    cTec = cboSSTecnicaAsistencia(1).Columns(0).Text
    
    ' Se define el 'statement' de lectura de los resultados de las t�cnicas
    SQL = "SELECT "
    SQL = SQL & "       MB32_CODRESULT, cUnidad "
    SQL = SQL & " FROM "
    SQL = SQL & "       MB3200"
    SQL = SQL & " WHERE "
    SQL = SQL & "       MB09_CODTEC = ?"
    SQL = SQL & "       AND MB32_INDACTIVA=-1"
    Set qryTecResult = objApp.rdoConnect.CreateQuery("TecResult", SQL)
    
    ' Se define el 'statement' de inserci�n de los resultados de las t�cnicas en asistencia
    SQL = "INSERT INTO MB3300"
    SQL = SQL & " (nRef,MB20_CODTECAsist,MB09_CODTec,MB32_CODResult,cUnidad,MB38_CODEstResult)"
    SQL = SQL & " VALUES "
    SQL = SQL & " (?,?,?,?,?,?)"
    Set qryTecResultAsist = objApp.rdoConnect.CreateQuery("TecResultAsist", SQL)

    ' Se generan los registros de resultados
    qryTecResult(0) = cTec
    Set rsTecResult = qryTecResult.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsTecResult.EOF
        qryTecResultAsist(0) = nRef
        qryTecResultAsist(1) = nTec
        qryTecResultAsist(2) = cTec
        qryTecResultAsist(3) = rsTecResult!MB32_CODResult
        qryTecResultAsist(4) = rsTecResult!cUnidad
        qryTecResultAsist(5) = constRESULTSOLICITADO
        Set rsTecResultAsist = qryTecResultAsist.OpenResultset(rdOpenKeyset)
        rsTecResult.MoveNext
    Loop
    rsTecResult.Close

End Sub

Private Sub pMostrarCarpeta()
    Dim qryCarpeta As rdoQuery
    Dim rsCarpeta As rdoResultset
    Dim SQL As String
    
    ' Muestra en el combo cbossPruebaAsistencia(0) la carpeta correspondiente a la prueba en pantalla
    cCarpeta = ""
    cboSSPruebaAsistencia(0).Text = ""
    If Not objPruebaInfo.rdoCursor.RowCount = 0 And Not IsNull(objPruebaInfo.rdoCursor("cCarpeta")) Then
        cCarpeta = objPruebaInfo.rdoCursor("cCarpeta")
        If Not cCarpeta = "" Then
            ' Se indica en el combo de carpetas el activo
            SQL = "SELECT designacion FROM carpetas"
            SQL = SQL & " WHERE cCarpeta=?"
            
            Set qryCarpeta = objApp.rdoConnect.CreateQuery("Carpeta", SQL)
        
            qryCarpeta(0) = cCarpeta
            
            Set rsCarpeta = qryCarpeta.OpenResultset(rdOpenKeyset)
            If rsCarpeta.RowCount <> 0 Then
                cboSSPruebaAsistencia(0).Text = rsCarpeta(0)
            End If
            rsCarpeta.Close
            qryCarpeta.Close
        End If
    End If

End Sub

Private Sub cmdMuestraAsistencia_Click(intIndex As Integer)
    Select Case intIndex
        Case 0
            If txtPruebaAsistencia(3).Text <> "" Then
                Call pVentanaProblemas(txtPruebaAsistencia(3).Text, txtMuestraAsistencia(0).Text)
            Else
                MsgBox "Primero ha de aceptar la prueba.", vbExclamation, "Problemas"
            End If
        Case 1
            If txtPruebaAsistencia(0).Text <> "" Then
                Call pVentanaPaciente(txtPruebaAsistencia(0).Text)
            End If
    End Select
End Sub

Private Sub cmdPruebaAsistencia_Click(intIndex As Integer)
    Dim protSelec As Integer
    Dim i As Integer
    Dim cProtocolos As String
    Dim res As Integer
    Dim nRef As String
    Dim childForms As Collection ' Crea un objeto Collection  de formularios a refrescar
    Dim arTecProt() As typeTecProt ' Recoge las t�cnicas generadas
    
    
    
    protSelec = lvwPruebaAsistencia(0).ListItems.Count
    For i = 1 To protSelec
        If lvwPruebaAsistencia(0).ListItems(i).Selected = True Then
            'protocolos = protocolos & lvwPruebaAsistencia(0).ListItems(i).SubItems(1) & ","
            cProtocolos = cProtocolos & CStr(protocolos(i).cProtocolo) & ","
        End If
    Next i
    If Right(cProtocolos, 1) = "," Then cProtocolos = Left(cProtocolos, Len(cProtocolos) - 1)

    If cProtocolos = "" And protSelec > 0 Then
        res = MsgBox("No se ha seleccionado ning�n protocolo por defecto. �Desea continuar y aceptar la prueba?", vbYesNo, "Protocolos")
    Else
        res = vbYes
    End If
    If res = vbYes Then
        ' Se acepta la prueba, asign�ndole n�mero de referencia
        ' Habr� que pasar el dpto o secci�n, para asignar diferentes sequences.
        objApp.rdoConnect.BeginTrans
        If txtPruebaAsistencia(3).Text <> "" Then
            nRef = txtPruebaAsistencia(3).Text
        Else
            nRef = fNumeroReferencia("")
        End If
        If nRef = "" Then
            MsgBox "Se ha producido un error en la obtenci�n del n�mero de referencia.", vbError
            objApp.rdoConnect.RollbackTrans
        Else
            ' Se asigna el n�mero a la prueba
            res = fAceptarPrueba(objPruebaInfo.rdoCursor("historia"), objPruebaInfo.rdoCursor("caso"), objPruebaInfo.rdoCursor("secuencia"), objPruebaInfo.rdoCursor("nRepeticion"), nRef)
            If res = False Then
                MsgBox "Se ha producido un error en la asignaci�n del n�mero de referencia.", vbError, "Aceptar Muestra"
                objApp.rdoConnect.RollbackTrans
            Else
                ' Se generan las t�cnicas correspondientes a los protoclos seleccionados
                If protSelec > 0 And cProtocolos <> "" Then
                    res = fGenerarTecProt(nRef, cProtocolos, constORIGENMUESTRA, objMuestraInfo.rdoCursor("cMuestra"), objMuestraInfo.rdoCursor("cMuestra"), arTecProt(), "", True)
                Else
                    res = vbYes
                End If
                If res = False Then
                    MsgBox "Se ha producido un error en la aplicaci�n de los protocolos.", vbError
                    objApp.rdoConnect.RollbackTrans
                Else
                    objApp.rdoConnect.CommitTrans
                    ' Agrega los formularios a refrescar a la colecci�n.
                    'childForms.Add "TECNICAASISTENCIA"
                    ' Se actualizan los formularios
                    'Call objWinInfo.FormRefreshNextLevels(childForms)
                    Call objWinInfo.FormChangeActive(fraPruebaAsistencia(0), True, True)
                    objWinInfo.DataRefresh
                    'Call objWinInfo.FormChangeActive(fraTecnicaAsistencia(0), True, True)
                    'objWinInfo.DataRefresh
                    Call objWinInfo.FormChangeActive(fraMuestraAsistencia(0), True, True)
                End If
            End If
        End If
    End If
End Sub

Private Sub cmdTecnicaAsistencia_Click(intIndex As Integer)
    Select Case intIndex
        Case 0
            Select Case objWinInfo.intWinStatus
                Case cwModeSingleAddRest
                    MsgBox "Debe guardar primerarmente la t�cnica.", vbExclamation
                Case Else
                    Call pVentanaProblemas(txtPruebaAsistencia(3).Text, , txtTecnicaAsistencia(0).Text, txtTecnicaAsistencia(6).Text)
            End Select
        Case 1
            pImprimirHoja
    End Select
End Sub

Private Sub Form_Activate()
    Screen.MousePointer = vbHourglass
    Call pChequeoSequence
    Screen.MousePointer = vbDefault
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  pPrepararListView
 
  With objMuestraInfo
    .strName = "MUESTRAASISTENCIA"
    Set .objFormContainer = fraMuestraAsistencia(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabMuestraAsistencia(0)
    Set .grdGrid = grdSSMuestraAsistencia(0)
    .intAllowance = cwAllowModify
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "MUESTRAASISTENCIA"
    
    SQL = "SELECT cMuestra FROM muestraPrueba mP, pruebaAsistencia pA"
    SQL = SQL & " WHERE mP.historia=pA.historia"
    SQL = SQL & " AND mP.caso=pA.caso"
    SQL = SQL & " AND mP.secuencia=pA.secuencia"
    SQL = SQL & " AND pA.estado = " & constPRUEBAEXTRAIDA
    SQL = SQL & " AND pA.cCarpeta IN "
    SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    
    .strWhere = "cMuestra IN (" & SQL & ")"
    '.strInitialWhere = "cMuestra IN (" & SQL & ")"
    Call .FormAddOrderField("CMUESTRA", cwAscending)
    
    'Call .objPrinter.Add("EMP1", "Listado 1 de empleados")
    'Call .objPrinter.Add("EMP2", "Listado 2 de empleados")
    'Call .objPrinter.Add("EMP3", "Listado 3 de empleados")
    
    '.blnHasMaint = True
    .blnHasMaint = False
    
    'Call .FormAddOrderField("fechaExtraccion", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "MUESTRAASISTENCIA")
    Call .FormAddFilterWhere(strKey, "CMUESTRA", "C�digo", cwString)
    Call .FormAddFilterWhere(strKey, "CTIPOMUESTRA", "Designaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FECHAEXTRACCION", "FECHA", cwDate)
    
    Call .FormAddFilterOrder(strKey, "CMUESTRA", "C�digo")
    Call .FormAddFilterOrder(strKey, "CTIPOMUESTRA", "Designaci�n")
    Call .FormAddFilterOrder(strKey, "FECHAEXTRACCION", "FECHA")
  End With
   
  With objPruebaInfo
    .strName = "PRUEBAASISTENCIA"
    Set .objFormContainer = fraPruebaAsistencia(0)
    Set .objFatherContainer = fraMuestraAsistencia(0)
    Set .tabMainTab = tabPruebaAsistencia(0)
    Set .grdGrid = grdssPruebaAsistencia(0)
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "muestraPrueba mP, pruebaAsistencia pA, pac, dpt, dr, camas c, peticiones p,seguimientoPrueba sp, pacientes paci"
    SQL = "pA.historia=mP.historia AND pA.caso=mP.caso AND pA.secuencia=mP.secuencia"
    SQL = SQL & " AND sP.historia=pA.historia AND sP.caso=pA.caso AND sP.secuencia=pA.secuencia AND sP.nRepeticion=pA.nRepeticion AND sp.Proceso=" & constPROCESOSOLICITUD
    SQL = SQL & " AND p.historia= pA.historia AND p.caso=pA.caso AND p.nPeticion=pA.nPeticion"
    SQL = SQL & " AND pac.nh(+)=pA.historia"
    SQL = SQL & " AND dpt.cDpt(+)=pA.dptoSolicitud"
    SQL = SQL & " AND dr.cdr (+)=pA.drSolicitud"
    SQL = SQL & " AND c.NH (+)=pA.Historia AND c.nc(+)=pA.caso"
    SQL = SQL & " AND paci.historia=pA.historia"
    SQL = SQL & " AND pa.estado IN(" & constPRUEBAEXTRAIDA & "," & constPRUEBAREALIZANDO & ")"
    SQL = SQL & " AND pA.cCarpeta IN "
    SQL = SQL & "       (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    .strWhere = SQL
    .strColumns = "pA.historia,pA.caso,pA.secuencia, pA.nRepeticion,pA.cTipoPrueba, pA.cCarpeta,sP.comentarios as comPrueba,p.comentarios as comPetic,pac.nom||' '||pac.ap1||' '||pac.ap2 as paciente, dpt.cdpt as cDepartamento, dpt.desig as departamento, dr.txtFirma as doctor, c.cama, paci.comentarios as comPaci"
    
    Call .FormAddOrderField("fExtraccionSolicitada", cwAscending)

'    Call .FormAddOrderField("urgenciaRealizacion", cwAscending)
'    Call .FormAddOrderField("fExtraccionSolicitada", cwAscending)
'    Call .FormAddOrderField("pA.secuencia", cwAscending)
    
    Call .FormAddRelation("cMuestra", txtMuestraAsistencia(0))
  
    strKey = .strDataBase & .strTable
  End With
  
  With objTecnicaInfo
    .strName = "TECNICAASISTENCIA"
    Set .objFormContainer = fraTecnicaAsistencia(0)
    Set .objFatherContainer = fraPruebaAsistencia(0)
    Set .tabMainTab = tabTecnicaAsistencia(0)
    Set .grdGrid = grdssTecnicaAsistencia(0)
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    .blnAskPrimary = False
    '.strColumns = "NRef,cMuestra,MB20_TIOrig,MB20_CODOrig,MB20_CODPlaca"
    '.intAllowance = cwAllowModify
    
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "MB2000"
    .strColumns = "MB20_INDsiembra"
    
    Call .FormAddOrderField("MB20_CODTecAsist", cwAscending)
    
    Call .FormAddRelation("NRef", txtPruebaAsistencia(3))
  
    strKey = .strDataBase & .strTable
  End With

  With objWinInfo
    Call .FormAddInfo(objMuestraInfo, cwFormDetail)
    Call .FormAddInfo(objPruebaInfo, cwFormDetail)
    Call .FormAddInfo(objTecnicaInfo, cwFormDetail)

    Call .FormCreateInfo(objMuestraInfo)
    '.CtrlGetInfo(cboSSMuestraAsistencia(0)).strSQL = "SELECT cTipoMuestra,designacion FROM " & objEnv.GetValue("Main") & "tiposMuestras ORDER BY designacion"
    SQL = "SELECT cTipoMuestra,designacion FROM tiposMuestras"
    SQL = SQL & " WHERE cTipoMuestra IN "
    SQL = SQL & "       (SELECT cTipoMuestra FROM pruebasExtracciones pE, pruebasCarpetas pC"
    SQL = SQL & "        WHERE pE.cPrueba = pC.cPrueba"
    SQL = SQL & "        AND pC.cCarpeta IN"
    SQL = SQL & "           (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
    SQL = SQL & "        )"
    SQL = SQL & " ORDER BY designacion"
    .CtrlGetInfo(cboSSMuestraAsistencia(0)).strSQL = SQL
    
    .CtrlGetInfo(txtMuestraAsistencia(0)).blnInFind = True
    .CtrlGetInfo(txtMuestraAsistencia(1)).blnInFind = True
    .CtrlGetInfo(txtMuestraAsistencia(0)).blnReadOnly = True
    .CtrlGetInfo(txtMuestraAsistencia(1)).blnReadOnly = True
    .CtrlGetInfo(txtMuestraAsistencia(2)).blnReadOnly = True
  
       
    '.CtrlGetInfo(txtText1(1)).blnForeign = True
  
    '.CtrlGetInfo(cboSSDBCombo1(0)).strSQL = "SELECT deptno, dname, loc FROM dept"
  
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "deptno", "SELECT deptno, dname, loc FROM dept WHERE deptno = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(5), "dname")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(6), "loc")

    .CtrlGetInfo(txtPruebaAsistencia(0)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(1)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(2)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(3)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(4)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(5)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(6)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(7)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(8)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(9)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(10)).blnReadOnly = True
    .CtrlGetInfo(txtPruebaAsistencia(11)).blnReadOnly = True
    
    .CtrlGetInfo(chkPruebaAsistencia(0)).blnReadOnly = True
    
    .CtrlGetInfo(txtPruebaAsistencia(9)).blnNegotiated = True
    .CtrlGetInfo(cboSSPruebaAsistencia(0)).blnReadOnly = True

'    .CtrlGetInfo(txtPruebaAsistencia(0)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(1)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(2)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(3)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(4)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(5)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(6)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(7)).blnInGrid = True
'    .CtrlGetInfo(txtPruebaAsistencia(8)).blnInGrid = True
    
    .CtrlGetInfo(cboSSPruebaAsistencia(0)).strSQL = "SELECT cCarpeta,designacion FROM carpetas WHERE cDptoSecc = " & departamento
    '.CtrlGetInfo(cboSSPruebaAsistencia(1)).strSQL = "SELECT MB05_CodProt,MB05_desig FROM MB0500 WHERE MB05_INDActiva= -1"
        
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtTecnicaAsistencia(1)), "MB09_CODTec", "SELECT MB09_CODTec, MB09_DESIG FROM MB0900 WHERE MB09_CODTec = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtTecnicaAsistencia(1)), txtTecnicaAsistencia(2), "MB09_Desig")

    .CtrlGetInfo(txtTecnicaAsistencia(0)).blnReadOnly = True
    
    .CtrlGetInfo(cboSSTecnicaAsistencia(0)).strSQL = "SELECT MB02_CODCOND,MB02_DESIG FROM MB0200 WHERE MB02_INDACTIVA =-1 ORDER BY MB02_DESIG"
    .CtrlGetInfo(cboSSTecnicaAsistencia(1)).strSQL = "SELECT MB09_CODTEC,MB09_DESIG,MB04_CODTiTec FROM MB0900 WHERE cDptoSecc=" & departamento & " AND MB09_INDACTIVA =-1 ORDER BY MB09_DESIG"

    '.CtrlGetInfo(cboSSTecnicaAsistencia(0)).blnNegotiated = False
    .CtrlGetInfo(cboSSTecnicaAsistencia(0)).blnMandatory = True
        
    .CtrlGetInfo(txtTecnicaAsistencia(0)).blnReadOnly = True

    Call .WinRegister
    Call .WinStabilize
  End With

  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  'intCancel = fGuardarCarpeta()
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, blnCancel As Boolean)
    ' Antes de eliminar una t�cnica se elimina los registros correspondientes
    ' en la tabla MB2600 de Seguimiento T�cnica
    
    Dim res As Integer
    Select Case UCase(strFormName)
        Case "TECNICAASISTENCIA"
            If objTecnicaInfo.rdoCursor.RowCount <> 0 Then
                res = fBorrarSeguimientoTecnica(txtPruebaAsistencia(3).Text, txtTecnicaAsistencia(0).Text)
                If res = False Then
                    MsgBox "No se han podido eliminar los datos de la t�cnica.", vbError, "Eliminar T�cnica"
                    blnCancel = True
                End If
            End If
    End Select
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
    Dim res As Integer
    Dim codTecAsist As Integer
    Dim nPlaca As Integer

    Select Case objWinInfo.objWinActiveForm.strName
        Case "PRUEBAASISTENCIA"

        Case "TECNICAASISTENCIA"
            ' Se introducen los primary key en el caso de adici�n de t�cnicas
            If objWinInfo.intWinStatus = cwModeSingleAddRest Then
                codTecAsist = fNumTecAsist(txtPruebaAsistencia(3).Text, nPlaca)
                txtTecnicaAsistencia(2).Text = txtPruebaAsistencia(3).Text
                txtTecnicaAsistencia(0).Text = codTecAsist + 1
                If Val(cboSSTecnicaAsistencia(1).Columns(2).Text) = constCULTIVO Then
                    txtTecnicaAsistencia(6).Text = nPlaca + 1
                Else
                    txtTecnicaAsistencia(6).Text = 0
                End If
                txtTecnicaAsistencia(4).Text = constORIGENMUESTRA
                txtTecnicaAsistencia(5).Text = txtMuestraAsistencia(0).Text
                txtTecnicaAsistencia(3).Text = txtMuestraAsistencia(0).Text
                chkTecnicaAsistencia(0).Value = 1
            End If
    End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Empleados" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    If btnButton = cwToolBarButtonNew Then
        Select Case objWinInfo.objWinActiveForm.strName
            Case "TECNICAASISTENCIA"
                txtTecnicaAsistencia(1).Text = txtPruebaAsistencia(3).Text
        End Select
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdSSMuestraAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdSSMuestraAsistencia_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdSSMuestraAsistencia_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdSSMuestraAsistencia_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdSSPruebaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdSSPruebaAsistencia_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdSSPruebaAsistencia_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdSSPruebaAsistencia_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub grdSSTecnicaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdSSTecnicaAsistencia_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdSSTecnicaAsistencia_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdSSTecnicaAsistencia_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub lvwPruebaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabMuestraAsistencia_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabMuestraAsistencia(intIndex), False, True)
End Sub
Private Sub tabPruebaAsistencia_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabPruebaAsistencia(intIndex), False, True)
End Sub
Private Sub tabTecnicaAsistencia_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTecnicaAsistencia(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraMuestraAsistencia_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraMuestraAsistencia(intIndex), False, True)
End Sub
Private Sub fraPruebaAsistencia_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraPruebaAsistencia(intIndex), False, True)
End Sub
Private Sub fraTecnicaAsistencia_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraTecnicaAsistencia(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkPruebaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkPruebaAsistencia_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkPruebaAsistencia_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSMuestraAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSMuestraAsistencia_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSMuestraAsistencia_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSMuestraAsistencia_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSMuestraAsistencia_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub cboSSPruebaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSPruebaAsistencia_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSPruebaAsistencia_CloseUp(intIndex As Integer)
    Select Case intIndex
        Case 0
            Call objWinInfo.CtrlDataChange
    End Select
End Sub

Private Sub cboSSPruebaAsistencia_Change(intIndex As Integer)
    Select Case intIndex
        Case 0
            Call objWinInfo.CtrlDataChange
    End Select

End Sub

Private Sub cboSSPruebaAsistencia_Click(intIndex As Integer)
    Select Case intIndex
        Case 0
            Call objWinInfo.CtrlDataChange
    End Select
End Sub
Private Sub cboSSTecnicaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSTecnicaAsistencia_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSTecnicaAsistencia_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSTecnicaAsistencia_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSTecnicaAsistencia_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtMuestraAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtMuestraAsistencia_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
pAutotexto KeyCode
End Sub

Private Sub txtMuestraAsistencia_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtMuestraAsistencia_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtPruebaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtPruebaAsistencia_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtPruebaAsistencia_Change(intIndex As Integer)
'    Select Case intIndex
'        Case 3
'            If txtPruebaAsistencia(intIndex).Text = "" Then
'                fraTecnicaAsistencia(0).Enabled = False
'            Else
'                fraTecnicaAsistencia(0).Enabled = True
'            End If
'            txtPruebaAsistencia(9).Text = fNumeroMicro(txtPruebaAsistencia(intIndex).Text)
'        Case Else
            Call objWinInfo.CtrlDataChange
'    End Select
End Sub
Private Sub txtTecnicaAsistencia_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTecnicaAsistencia_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    pAutotexto KeyCode
End Sub

Private Sub txtTecnicaAsistencia_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTecnicaAsistencia_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    Dim i As Integer
    
    Select Case UCase(strFormName)
        Case "PRUEBAASISTENCIA"
            If objPruebaInfo.rdoCursor.RowCount <> 0 Then
'                If Not IsNull(objPruebaInfo.rdoCursor("historia")) Then
'                    txtPruebaAsistencia(0).Text = objPruebaInfo.rdoCursor("historia")
'                End If
                If Not IsNull(objPruebaInfo.rdoCursor("caso")) Then
                    txtPruebaAsistencia(1).Text = objPruebaInfo.rdoCursor("caso")
                End If
                If Not IsNull(objPruebaInfo.rdoCursor("secuencia")) Then
                    txtPruebaAsistencia(2).Text = objPruebaInfo.rdoCursor("secuencia")
                End If
'                If Not IsNull(objPruebaInfo.rdoCursor("paciente")) Then
'                    txtPruebaAsistencia(4).Text = objPruebaInfo.rdoCursor("paciente")
'                End If
                If Not IsNull(objPruebaInfo.rdoCursor("cama")) Then
                    txtPruebaAsistencia(5).Text = objPruebaInfo.rdoCursor("cama")
                End If
                If Not IsNull(objPruebaInfo.rdoCursor("departamento")) Then
                    txtPruebaAsistencia(6).Text = objPruebaInfo.rdoCursor("departamento")
                End If
                If Not IsNull(objPruebaInfo.rdoCursor("doctor")) Then
                    txtPruebaAsistencia(7).Text = objPruebaInfo.rdoCursor("doctor")
                End If
                If Not IsNull(objPruebaInfo.rdoCursor("comPaci")) Then
                    txtPruebaAsistencia(10).Text = "PACIENTE: " & Chr$(13) & Chr$(10) & objPruebaInfo.rdoCursor("comPaci") & Chr$(13) & Chr$(10)
                End If
                If Not IsNull(objPruebaInfo.rdoCursor("comPetic")) Then
                    txtPruebaAsistencia(10).Text = txtPruebaAsistencia(10).Text & "PETICION: " & Chr$(13) & Chr$(10) & objPruebaInfo.rdoCursor("comPetic") & Chr$(13) & Chr$(10)
                End If
                If Not IsNull(objPruebaInfo.rdoCursor("comPrueba")) Then
                    txtPruebaAsistencia(10).Text = txtPruebaAsistencia(10).Text & "PRUEBA: " & Chr$(13) & Chr$(10) & objPruebaInfo.rdoCursor("comPrueba")
                End If
                If Not IsNull(objPruebaInfo.rdoCursor("cTipoPrueba")) Then
                    txtPruebaAsistencia(8).Text = fDesignacionPrueba(objPruebaInfo.rdoCursor("cTipoPrueba"))
                End If
                Call pProtocoloPorDefecto
                Call pMostrarCarpeta
                For i = 0 To 1
                    cmdMuestraAsistencia(i).Enabled = True
                Next i
            Else
                For i = 0 To 1
                    cmdMuestraAsistencia(i).Enabled = False
                Next i
            End If
            pFormatearBotonAceptar
            pFormatearNumeroMicro
        Case "TECNICAASISTENCIA"
            ' Se obtiene el medio activo
            pMedioActivo
    End Select
End Sub
Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As cwFormStatus, ByVal intOldStatus As cwFormStatus)
    If strFormName = "TECNICAASISTENCIA" Then
        Select Case intNewStatus
            Case cwModeSingleAddRest
                cmdTecnicaAsistencia(0).Enabled = False
                'cboSSTecnicaAsistencia(1).Enabled = True
                objWinInfo.CtrlGetInfo(cboSSTecnicaAsistencia(1)).blnReadOnly = False
                objWinInfo.WinPrepareScr
                cCond = 0
            Case Else
                cmdTecnicaAsistencia(0).Enabled = True
                'cboSSTecnicaAsistencia(1).Enabled = False
                objWinInfo.CtrlGetInfo(cboSSTecnicaAsistencia(1)).blnReadOnly = True
                objWinInfo.WinPrepareScr
        End Select
    End If

End Sub
Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim res As Integer
    Select Case objWinInfo.objWinActiveForm.strName
        Case "PRUEBAASISTENCIA"
        
        Case "TECNICAASISTENCIA"
            pActualizarSeguimientoTecnica
            pInsertarResultados
    End Select
        
End Sub
Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Dim res As Integer
    Select Case objWinInfo.objWinActiveForm.strName
        Case "PRUEBAASISTENCIA"
            'MsgBox "PostValidate " & objWinInfo.objWinActiveForm.strName
            MsgBox objPruebaInfo.rdoCursor("cCarpeta")
            'SQL = "UPDATE pruebaAsistencia SET cCarpeta = "
            blnError = False
        Case "TECNICAASISTENCIA"

    End Select
End Sub


Private Sub pActualizarSeguimientoTecnica()
    Dim SQL As String
    Dim qryCond As rdoQuery
    Dim rsCond As rdoResultset
    
    If Val(cCond) = 0 Then
        SQL = "INSERT INTO MB2600"
        SQL = SQL & "(nRef, MB20_CODTecAsist, MB26_CODPaso, MB02_CODCond"
        SQL = SQL & ", MB35_CODEstSegui)"
        SQL = SQL & " VALUES "
        SQL = SQL & "(?,?,?,?,?)"
        Set qryCond = objApp.rdoConnect.CreateQuery("ActualizacionCondicion", SQL)
        qryCond(0) = txtPruebaAsistencia(3).Text
        qryCond(1) = txtTecnicaAsistencia(0).Text
        qryCond(2) = 1
        qryCond(3) = cboSSTecnicaAsistencia(0).Columns(0).Text
        qryCond(4) = constSEGUITECVALIDA
        Set rsCond = qryCond.OpenResultset(rdOpenKeyset)
        rsCond.Close
        qryCond.Close
     ElseIf Val(cCond) <> Val(cboSSTecnicaAsistencia(0).Columns(0).Text) Then
        SQL = "UPDATE MB2600"
        SQL = SQL & " SET MB02_CODCond = ?"
        SQL = SQL & " WHERE NRef = ?"
        SQL = SQL & " AND MB20_CODTecAsist=?"
        SQL = SQL & " AND MB26_CODPaso=?"
        Set qryCond = objApp.rdoConnect.CreateQuery("ActualizacionCondicion", SQL)
        qryCond(0) = cboSSTecnicaAsistencia(0).Columns(0).Text
        qryCond(1) = txtPruebaAsistencia(3).Text
        qryCond(2) = objTecnicaInfo.rdoCursor("MB20_CODTecAsist")
        qryCond(3) = 1
        Set rsCond = qryCond.OpenResultset(rdOpenKeyset)
        rsCond.Close
        qryCond.Close
    End If
    cCond = cboSSTecnicaAsistencia(0).Columns(0).Text

End Sub

Private Function fGuardarCarpeta() As Boolean
    Dim res As Long
    Dim qryCarpeta As rdoQuery
    Dim rsCarpeta As rdoResultset
    Dim cCarpeta As String
    Dim SQL As String
    
    If objPruebaInfo.blnChanged = True Then
        'Call SetError(cwUserQuery, "")
        'res = Raise()
        Select Case res
            Case vbYes
                cCarpeta = objWinInfo.CtrlGetInfo(cboSSPruebaAsistencia(0)).vntLastValue
                ' Se actualiza la carpeta
                SQL = "UPDATE pruebaasistencia SET ccarpeta = ?"
                SQL = SQL & " WHERE historia = ?"
                SQL = SQL & " AND caso= ?"
                SQL = SQL & " AND secuencia= ?"
                                
                Set qryCarpeta = objApp.rdoConnect.CreateQuery("ActualizaCarpeta", SQL)
            
                qryCarpeta(0) = cCarpeta
                qryCarpeta(1) = objPruebaInfo.rdoCursor("historia")
                qryCarpeta(2) = objPruebaInfo.rdoCursor("caso")
                qryCarpeta(3) = objPruebaInfo.rdoCursor("secuencia")
                
                Set rsCarpeta = qryCarpeta.OpenResultset(rdOpenKeyset)
                
            Case vbNo
            
            Case vbCancel
                fGuardarCarpeta = True
        End Select
        objPruebaInfo.blnChanged = False
    End If
End Function
Private Sub pProtocoloPorDefecto()
    Dim cTipoPrueba As String
    Dim cTipoMuestra As String
    Dim cDpt As String
    Dim numProtocolo As Integer
    
    cTipoPrueba = objPruebaInfo.rdoCursor("cTipoPrueba")
    cTipoMuestra = objMuestraInfo.rdoCursor("cTipoMuestra")
    cDpt = objPruebaInfo.rdoCursor("cDepartamento")
    
    numProtocolo = fBuscarProtocolo(protocolos(), cTipoPrueba, cTipoMuestra, cDpt)
    pCargarProtocolo
End Sub
Private Sub pCargarProtocolo()
    Dim dimension As Integer
    Dim i As Integer
    Dim itmProtocolo As ListItem
    
    On Error Resume Next
    lvwPruebaAsistencia(0).ListItems.Clear
    dimension = UBound(protocolos)
    For i = 1 To dimension
        Set itmProtocolo = lvwPruebaAsistencia(0).ListItems.Add(i, "a" & protocolos(i).cProtocolo, i) ' Autor.
        itmProtocolo.SubItems(1) = protocolos(i).desig
        itmProtocolo.SubItems(2) = fRazonProtocolo(protocolos(i).aplicProt)
        lvwPruebaAsistencia(0).ListItems(i).Selected = True
    Next i
    
    
End Sub
Private Sub pMedioActivo()
    Dim qryCond As rdoQuery
    Dim rsCond As rdoResultset
    Dim SQL As String
    
    cCond = ""
    cboSSTecnicaAsistencia(0).Text = ""
    If Not objTecnicaInfo.rdoCursor.RowCount = 0 And Not IsNull(objTecnicaInfo.rdoCursor("MB20_CODTecAsist")) Then
        cCond = fMedioActivo(objPruebaInfo.rdoCursor("nRef"), objTecnicaInfo.rdoCursor("MB20_CODTecAsist"))
        If Not cCond = "" Then
            ' Se indica en el combo de ambientes el activo
            SQL = "SELECT MB02_DESIG FROM MB0200"
            SQL = SQL & " WHERE MB02_CODCond=?"
            
            Set qryCond = objApp.rdoConnect.CreateQuery("Condicion_Ambiental", SQL)
        
            qryCond(0) = cCond
            
            Set rsCond = qryCond.OpenResultset(rdOpenKeyset)
            If rsCond.RowCount <> 0 Then
                cboSSTecnicaAsistencia(0).Text = rsCond(0)
            End If
            rsCond.Close
            qryCond.Close
        End If
    End If
End Sub
Private Sub pPrepararListView()
    
    lvwPruebaAsistencia(0).ColumnHeaders.Add , , "N�", 20
    lvwPruebaAsistencia(0).ColumnHeaders.Add , , "Protocolo", 1200
    lvwPruebaAsistencia(0).ColumnHeaders.Add , , "Raz�n", 1500

End Sub
