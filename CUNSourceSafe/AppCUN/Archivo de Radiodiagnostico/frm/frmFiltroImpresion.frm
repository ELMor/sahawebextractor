VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmImpresion 
   Caption         =   "Filtro de Impresi�n"
   ClientHeight    =   4365
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7560
   LinkTopic       =   "Form1"
   ScaleHeight     =   4365
   ScaleWidth      =   7560
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdQuitar 
      Caption         =   "&Quitar Condiciones"
      Height          =   435
      Left            =   3000
      TabIndex        =   30
      Top             =   3840
      Width           =   1575
   End
   Begin VB.CommandButton cmdAccion 
      Caption         =   "&Consultar"
      Height          =   435
      Left            =   600
      TabIndex        =   29
      Top             =   3840
      Width           =   1575
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   435
      Left            =   5400
      TabIndex        =   28
      Top             =   3840
      Width           =   1575
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   3
      Left            =   5220
      MaxLength       =   15
      TabIndex        =   24
      Tag             =   "1|RX0100.""RX01NUMDISCO"""
      Top             =   480
      Width           =   1575
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "frmFiltroImpresion.frx":0000
      Left            =   1740
      List            =   "frmFiltroImpresion.frx":000D
      TabIndex        =   21
      Tag             =   "3|RX0101J.""RX03INDCONFIRMADO"""
      Top             =   3360
      Width           =   1935
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   1
      Left            =   1740
      MaxLength       =   50
      TabIndex        =   20
      Tag             =   "1|RX0100.""RX01PRIAPEL"""
      Top             =   840
      Width           =   3735
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   2
      Left            =   1740
      MaxLength       =   50
      TabIndex        =   19
      Tag             =   "1|RX0100.""RX01SEGAPEL"""
      Top             =   1200
      Width           =   3735
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   4
      Left            =   1740
      MaxLength       =   25
      TabIndex        =   18
      Tag             =   "5|RX0101J.""RX05DESTECNICA"""
      Top             =   1560
      Width           =   3735
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   5
      Left            =   1740
      MaxLength       =   50
      TabIndex        =   17
      Tag             =   "2|RX0200.""RX02DESORGANO"""
      Top             =   1920
      Width           =   3735
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   6
      Left            =   1740
      MaxLength       =   7
      TabIndex        =   16
      Tag             =   "3|RX0101J.""RX06CODDIAGNOSTICO"""
      Top             =   2640
      Width           =   975
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   7
      Left            =   4080
      MaxLength       =   50
      TabIndex        =   15
      Tag             =   "6|RX0101J.""RX06DESDIAGNOSTICO"""
      Top             =   2640
      Width           =   3375
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   8
      Left            =   1740
      MaxLength       =   4
      TabIndex        =   14
      Tag             =   "3|RX0101J.""RX04CODAREA"""
      Top             =   2280
      Width           =   975
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   9
      Left            =   4080
      MaxLength       =   50
      TabIndex        =   13
      Tag             =   "4|RX0101J.""RX04DESAREA"""
      Top             =   2280
      Width           =   3375
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   10
      Left            =   1740
      MaxLength       =   50
      TabIndex        =   12
      Tag             =   "3|RX0101J.""RX03OBSERVAC"""
      Top             =   3000
      Width           =   3735
   End
   Begin VB.TextBox Text1 
      Height          =   315
      Index           =   0
      Left            =   1740
      MaxLength       =   20
      TabIndex        =   11
      Tag             =   "1|RX0100.""RX01NOMBRE"""
      Top             =   480
      Width           =   2355
   End
   Begin MSMask.MaskEdBox MaskEdBox1 
      Height          =   315
      Index           =   0
      Left            =   1740
      TabIndex        =   22
      Tag             =   "1|RX0101J.""RX01HISTORIA"""
      Top             =   120
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   556
      _Version        =   327681
      MaxLength       =   7
      Mask            =   "#######"
      PromptChar      =   " "
   End
   Begin MSMask.MaskEdBox MaskEdBox1 
      Height          =   315
      Index           =   1
      Left            =   5220
      TabIndex        =   25
      Tag             =   "1|RX0101J.""RX01NUMSOBRE"""
      Top             =   120
      Width           =   795
      _ExtentX        =   1402
      _ExtentY        =   556
      _Version        =   327681
      MaxLength       =   5
      Mask            =   "#####"
      PromptChar      =   " "
   End
   Begin VB.Label Label4 
      Caption         =   "N� Sobre"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4260
      TabIndex        =   27
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label Label3 
      Caption         =   "N� Disco"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4260
      TabIndex        =   26
      Top             =   480
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Historia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   23
      Top             =   120
      Width           =   1815
   End
   Begin VB.Label Label14 
      Caption         =   "C�digo de Area"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   10
      Top             =   2280
      Width           =   1815
   End
   Begin VB.Label Label13 
      Caption         =   "Cod. Diagn�stico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   2640
      Width           =   1815
   End
   Begin VB.Label Label12 
      Caption         =   "Confirmado (S/N)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   3360
      Width           =   1815
   End
   Begin VB.Label Label11 
      Caption         =   "2� Apellido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   1815
   End
   Begin VB.Label Label10 
      Caption         =   "Diagn�stico"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   6
      Top             =   2640
      Width           =   1815
   End
   Begin VB.Label Label9 
      Caption         =   "1er Apellido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   840
      Width           =   1815
   End
   Begin VB.Label Label8 
      Caption         =   "Organo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   1920
      Width           =   1815
   End
   Begin VB.Label Label7 
      Caption         =   "T�cnica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   1560
      Width           =   1815
   End
   Begin VB.Label Label6 
      Caption         =   "Area"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2880
      TabIndex        =   2
      Top             =   2280
      Width           =   1815
   End
   Begin VB.Label Label5 
      Caption         =   "Observaciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   3000
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Nombre"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   1815
   End
End
Attribute VB_Name = "frmImpresion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Function SqlConstr(dato As String, Tipo As Byte) As String
Dim a%
a = InStr(1, dato, "*")
    While a <> 0
        a = InStr(1, dato, "*")
        If a > 0 Then
            dato = Left$(dato, a - 1) & "%" & Right$(dato, Len(dato) - a)
        End If
    Wend
    If InStr(1, dato, "%") Then
        SqlConstr = " LIKE '" & IIf(Tipo = 1, UCase$(dato), (dato)) & "'"
    Else
        SqlConstr = " = '" & IIf(Tipo = 1, UCase$(dato), (dato)) & "'"
    End If
End Function

Sub Impresion(STRWHERE$)
Dim StrSql$
Dim printjob%
Dim lngText As Long
Dim inttext%
Dim errortext$
Dim Leftw%
Dim Topw%
  
  Call PELogOnSQLServerWithPrivateInfo("pdsodbc.dll", objApp.rdoConnect.hDbc)
  'printjob = PEOpenPrintJob(App.Path & "\RPT\Rx001.rpt")
    printjob = PEOpenPrintJob("\\Desarrollo\CODEWIZARD\ReportsRptZip\Rx001.rpt")
'    MsgBox (App.Path & "\RPT\Rx001.rpt")
'    MsgBox (STRWHERE)
'    STRWHERE = " AND" & vbCrLf & "RX0100." & Chr$(34) & "RX01NOMBRE" & Chr$(34) & " = 'ANTONIO'"
    
    StrSql = "SELECT" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01NUMSOBRE" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX04CODAREA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX04DESAREA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX05CODTECNICA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX05DESTECNICA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX06CODDIAGNOSTICO" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX06DESDIAGNOSTICO" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX03INDCONFIRMADO" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX03OBSERVAC" & Chr$(34) & "," & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01NOMBRE" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01PRIAPEL" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01SEGAPEL" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01NUMDISCO" & Chr$(34) & "," & vbCrLf
    StrSql = StrSql & "RX0200." & Chr$(34) & "RX02FECEXPLORA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0200." & Chr$(34) & "RX02DESORGANO" & Chr$(34) & vbCrLf
    StrSql = StrSql & "From" & vbCrLf
    StrSql = StrSql & "RX0101J, RX0100, RX0200" & vbCrLf
    StrSql = StrSql & "Where" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " = RX0100." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " AND" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " = RX0200." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " AND" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & " = RX0200." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & STRWHERE & vbCrLf
    'strsql = strsql & "upper(RX0101J." & Chr$(34) & "RX06CODDIAGNOSTICO" & Chr$(34) & ") = '433'"
    'strsql = strsql & "RX0101J." & Chr$(34) & "RX01NUMSOBRE" & Chr$(34) & " = 1" & vbCrLf
    StrSql = StrSql & "Order By" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01NUMSOBRE" & Chr$(34) & " ASC," & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & " ASC" & vbCrLf
    
  Call PESetSQLQuery(printjob, StrSql)
  'Debemos refrescar los datos
  Call PEDiscardSavedData(printjob)
  
'  If blnExtraInfo Then
'    On Error Resume Next
''    .Formulas(0) = "CODIGO_INFORME='" & .ReportFileName & "'"
'    Call PESetFormula(printjob, "CODIGO_INFORME", "RX01NUMSOBRE = 1 ")
'    On Error GoTo cwIntError
'  End If
  
'  If mintDestination = 0 Then
    Leftw = (Screen.Width \ Screen.TwipsPerPixelX) \ 16
    Topw = (Screen.Height \ Screen.TwipsPerPixelY) \ 16
    Call PEOutputToWindow(printjob, "", Leftw, Topw, Leftw * 14, Topw * 14, 0, 0)
'  Else
'    Call PEOutputToPrinter(printJob, 1)
'  End If
  
  Call PEStartPrintJob(printjob, True)
  
  If PEGetErrorText(printjob, lngText, inttext) <> 0 Then
    errortext = String(inttext, 0)
    If PEGetHandleString(lngText, errortext, inttext) <> 1 Then
'        Call objError.SetError(cwCodeMsg, msgPrintError, _
'                           mcllDescriptions(mintSelected), vbCrLf, 1, ErrorText)
'        Call objError.Raise
'        ShowReport = False
'    Else
'        ShowReport = True
    End If
  End If
'
  Call PEClosePrintJob(printjob)
  End Sub

Private Sub cmdConsultar()
Dim StrMyWhere$, i%
Dim blnTabla(1 To 6) As Boolean
Dim StrSqlHistoria As String
Dim StrSqlFicha As String
Dim StrSqlDiagnostico As String
Dim StrSql(1 To 6) As String
Dim sqlcount As String
Dim Num%

'    StrMyWhere = ""
'    For i = 0 To Text1.Count - 1
'        If Trim$(Text1(i).Text) <> "" Then
'            StrMyWhere = StrMyWhere & " AND " & vbCrLf & "upper(" & Right$(Text1(i).Tag, Len(Text1(i).Tag) - 2) & ")" & SqlConstr(Text1(i).Text, 1)
'            blnTabla(CInt(Left$(Text1(i).Tag, Len(Text1(i).Tag) - 2))) = True
'
'        End If
'    Next i
'    For i = 0 To MaskEdBox1.Count - 1
'        If IsNumeric(MaskEdBox1(i).Text) Then
'            StrMyWhere = StrMyWhere & " AND " & vbCrLf & Right$(MaskEdBox1(i).Tag, Len(MaskEdBox1(i).Tag) - 2) & SqlConstr(MaskEdBox1(i).Text, 2)
'            blnTabla(CInt(Left$(MaskEdBox1(i).Tag, Len(MaskEdBox1(i).Tag) - 2))) = True
'        End If
'    Next i
'    Select Case Combo1.ListIndex
'    Case 1
'        StrMyWhere = StrMyWhere & " AND " & vbCrLf & Right$(Text1(i).Tag, Len(Text1(i).Tag) - 2) & " = -1 "
'        blnTabla(3) = True
'    Case 2
'        StrMyWhere = StrMyWhere & " AND (" & vbCrLf & Right$(Text1(i).Tag, Len(Text1(i).Tag) - 2) & " <> -1 "
'        StrMyWhere = StrMyWhere & " OR " & vbCrLf & Right$(Text1(i).Tag, Len(Text1(i).Tag) - 2) & " IS NULL )"
'        blnTabla(3) = True
'    End Select
    
    
    For i = 0 To Text1.Count - 1
        If Trim$(Text1(i).Text) <> "" Then
            Num = CInt(Left$(Text1(i).Tag, 1))
            StrSql(Num) = StrSql(Num) & " AND " & vbCrLf & "upper(" & Campo(Text1(i).Tag) & ")" & SqlConstr(Text1(i).Text, 1)
        End If
    Next i
    For i = 0 To MaskEdBox1.Count - 1
        If IsNumeric(MaskEdBox1(i).Text) Then
            Num = CInt(Left$(MaskEdBox1(i).Tag, 1))
            StrSql(Num) = StrSql(Num) & " AND " & vbCrLf & Campo(MaskEdBox1(i).Tag) & SqlConstr(MaskEdBox1(i).Text, 2)
        End If
    Next i
    Select Case Combo1.ListIndex
        Case 1
            Num = 3
            StrSql(Num) = StrSql(Num) & " AND " & vbCrLf & Campo(Combo1.Tag) & " = -1 "
        Case 2
            Num = 3
            StrSql(Num) = StrSql(Num) & " AND (" & vbCrLf & Campo(Combo1.Tag) & " <> -1 "
            StrSql(Num) = StrSql(Num) & " OR " & vbCrLf & Campo(Combo1.Tag) & " IS NULL )"
    End Select
    
    For i = 1 To 6
        If StrSql(i) <> "" Then
            StrSql(i) = Right$(StrSql(i), Len(StrSql(i)) - 5)
        End If
    Next i
        
        
    frmA_Archivo.objDiagnostico.STRWHERE = StrSql(3) & _
    IIf(StrSql(4) <> "", IIf(StrSql(3) <> "", " AND ", "") & "RX04CODAREA IN (SELECT RX04CODAREA FROM RX0400 WHERE " & StrSql(4) & ")", "") & _
    IIf(StrSql(6) <> "", IIf(StrSql(3) <> "" Or StrSql(4) <> "", " AND ", "") & "RX06CODDIAGNOSTICO IN (SELECT RX06CODDIAGNOSTICO FROM RX0600 WHERE " & StrSql(6) & ")", "")
        
    frmA_Archivo.objFicha.STRWHERE = StrSql(2) & _
    IIf(StrSql(5) <> "", IIf(StrSql(2) <> "", " AND ", "") & "RX05CODTECNICA IN (SELECT RX05CODTECNICA FROM RX0500 WHERE " & StrSql(5) & ")", "") & _
    IIf(frmA_Archivo.objDiagnostico.STRWHERE <> "", IIf(StrSql(2) <> "" Or StrSql(5) <> "", " AND ", "") & "(RX01HISTORIA,RX02NUMFICHA) IN (SELECT RX01HISTORIA,RX02NUMFICHA FROM RX0300 WHERE " & frmA_Archivo.objDiagnostico.STRWHERE & ")", "")
        
    frmA_Archivo.objHistoria.STRWHERE = StrSql(1) & _
    IIf(frmA_Archivo.objFicha.STRWHERE <> "", IIf(StrSql(1) <> "", " AND ", "") & "RX01HISTORIA IN (SELECT RX01HISTORIA FROM RX0200 WHERE " & frmA_Archivo.objFicha.STRWHERE & ")", "")
    Me.Hide
    
    Call frmA_Archivo.objWinInfo.FormChangeActive(frmA_Archivo.tabHistoria, False, True)
    Call frmA_Archivo.objWinInfo.DataRefresh
    Call frmA_Archivo.objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
    
    frmA_Archivo.ContarPacientes
        
End Sub
Function Campo(origen As String) As String
Dim a%
Dim b$
b = Chr$(34)
a = InStr(3, origen, ".")
Campo = Mid$(origen, a + 2, Len(origen) - a - 2)
End Function
Private Sub cmdImprimir()
Dim StrMyWhere$
Dim i%

    StrMyWhere = ""
    For i = 0 To Text1.Count - 1
        If Trim$(Text1(i).Text) <> "" Then
            StrMyWhere = StrMyWhere & " AND " & vbCrLf & "upper(" & Right$(Text1(i).Tag, Len(Text1(i).Tag) - 2) & ")" & SqlConstr(Text1(i).Text, 1)
        End If
    Next i
    For i = 0 To MaskEdBox1.Count - 1
        If IsNumeric(MaskEdBox1(i).Text) Then
            StrMyWhere = StrMyWhere & " AND " & vbCrLf & Right$(MaskEdBox1(i).Tag, Len(MaskEdBox1(i).Tag) - 2) & SqlConstr(MaskEdBox1(i).Text, 2)
        End If
    Next i
    Select Case Combo1.ListIndex
    Case 1
        StrMyWhere = StrMyWhere & " AND " & vbCrLf & Right$(Combo1.Tag, Len(Combo1.Tag) - 2) & " = -1 "
    Case 2
        StrMyWhere = StrMyWhere & " AND (" & vbCrLf & Combo1.Tag & " <> -1 "
        StrMyWhere = StrMyWhere & " OR " & vbCrLf & Combo1.Tag & " IS NULL )"
    End Select
    Impresion (StrMyWhere)
End Sub

Private Sub cmdAccion_Click()
If cmdAccion.Caption = "&Consultar" Then cmdConsultar Else cmdImprimir
End Sub

Private Sub cmdQuitar_Click()
Dim i%
    For i = 0 To Text1.Count - 1
        Text1(i).Text = ""
    Next i
    For i = 0 To MaskEdBox1.Count - 1
        MaskEdBox1(i).Text = Space$(Len(MaskEdBox1(i).Mask))
    Next i
    Combo1.ListIndex = 0
    
    frmA_Archivo.objDiagnostico.STRWHERE = ""
    frmA_Archivo.objFicha.STRWHERE = ""
    frmA_Archivo.objHistoria.STRWHERE = ""
    
    Me.Hide
    
    Call frmA_Archivo.objWinInfo.FormChangeActive(frmA_Archivo.tabHistoria, False, True)
    Call frmA_Archivo.objWinInfo.DataRefresh
    Call frmA_Archivo.objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
    
    frmA_Archivo.ContarPacientes
    
    
End Sub

Private Sub cmdSalir_Click()
    Me.Hide
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Load()
Combo1.ListIndex = 0
'MaskEdBox1(0).Text = objPipe.PipeGet("flthistoria")
'MaskEdBox1(1).Text = objPipe.PipeGet("fltsobre")
'Text1(4).Text = objPipe.PipeGet("fltdestecnica")
'Text1(5).Text = objPipe.PipeGet("fltcodarea")
'Text1(8).Text = objPipe.PipeGet("fltdesarea")
'Text1(9).Text = objPipe.PipeGet("fltcoddiagnostico")
'Text1(6).Text = objPipe.PipeGet("fltdesdiagnostico")
'Text1(10).Text = objPipe.PipeGet("fltobservac")
'Text1(3).Text = objPipe.PipeGet("fltnombre")
'Combo1.ListIndex = objPipe.PipeGet("fltnombre")

End Sub
