VERSION 5.00
Begin VB.Form frmH_Etiquetas 
   Caption         =   "Impesi�n de Etiquetas"
   ClientHeight    =   1860
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4260
   LinkTopic       =   "Form1"
   ScaleHeight     =   1860
   ScaleWidth      =   4260
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtEtiquetas 
      Height          =   375
      Index           =   1
      Left            =   1260
      MaxLength       =   3
      TabIndex        =   1
      Tag             =   "N� de Cultivo"
      Top             =   720
      Width           =   555
   End
   Begin VB.TextBox txtEtiquetas 
      Height          =   375
      Index           =   2
      Left            =   1260
      MaxLength       =   4
      TabIndex        =   2
      Tag             =   "A�o"
      Top             =   1260
      Width           =   675
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Cancelar"
      Height          =   375
      Index           =   1
      Left            =   2880
      TabIndex        =   4
      Top             =   780
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Imprimir"
      Height          =   375
      Index           =   0
      Left            =   2880
      TabIndex        =   3
      Top             =   180
      Width           =   1215
   End
   Begin VB.TextBox txtEtiquetas 
      Height          =   375
      Index           =   0
      Left            =   1260
      MaxLength       =   7
      TabIndex        =   0
      Tag             =   "N� de Referencia"
      Top             =   180
      Width           =   1215
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "N� Cultivo:"
      Height          =   315
      Index           =   1
      Left            =   180
      TabIndex        =   7
      Top             =   780
      Width           =   915
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "A�o:"
      Height          =   315
      Index           =   2
      Left            =   180
      TabIndex        =   6
      Top             =   1320
      Width           =   915
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "N� Ref.:"
      Height          =   315
      Index           =   0
      Left            =   180
      TabIndex        =   5
      Top             =   240
      Width           =   915
   End
End
Attribute VB_Name = "frmH_Etiquetas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub pPrepararDatosEtiqueta(nRef$, cPlaca%)

    Dim rsDatosEtiqueta As rdoResultset, qryDatosEtiqueta As rdoQuery
    Dim SQL As String
    Dim res As Boolean
    Dim historia As String
    Dim paciente As String
    Dim fecha As String
    Dim strTipoMuestra As String
    Dim strInicialesPac As String
    Dim arEtiquetas() As typeEtiquetas
    
    SQL = "SELECT MB2000.nRef, MB2000.MB20_CODPlaca, MB2000.MB20_fecReali,"
    SQL = SQL & " MB0900.MB09_Desig, MB0200.MB02_Desig"
    SQL = SQL & " FROM MB2000, MB0900, MB0200, MB2600"
    SQL = SQL & " WHERE MB2000.nRef = ?"
    SQL = SQL & " AND MB2000.MB20_codPlaca = ?"
    SQL = SQL & " AND MB0900.MB09_codTec = MB2000.MB09_codTec"
    SQL = SQL & " AND MB2600.nRef = MB2000.nRef"
    SQL = SQL & " AND MB2600.MB20_codTecAsist = MB2000.MB20_codTecAsist"
    SQL = SQL & " AND MB2600.MB35_codEstSegui = " & constSEGUITECVALIDA
    SQL = SQL & " AND MB0200.MB02_codCond = MB2600.MB02_codCond"
    Set qryDatosEtiqueta = objApp.rdoConnect.CreateQuery("", SQL)
    qryDatosEtiqueta(0) = nRef
    qryDatosEtiqueta(1) = cPlaca
    Set rsDatosEtiqueta = qryDatosEtiqueta.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsDatosEtiqueta.EOF = False Then
        If fDatosNReferencia(nRef, historia, paciente, fecha, strTipoMuestra, strInicialesPac) = True Then
            ReDim arEtiquetas(1)
            arEtiquetas(1).strNRef = rsDatosEtiqueta!nRef
            arEtiquetas(1).intCodPlaca = rsDatosEtiqueta!MB20_CODPlaca
            arEtiquetas(1).strFecha = Format$(rsDatosEtiqueta!MB20_fecReali, "d/m/yy hh:mm")
            arEtiquetas(1).strTecnica = rsDatosEtiqueta!MB09_Desig
            arEtiquetas(1).strCondicion = rsDatosEtiqueta!MB02_Desig
            arEtiquetas(1).strHistoria = historia
            arEtiquetas(1).strMuestra = strTipoMuestra
            arEtiquetas(1).strPaciente = strInicialesPac
            Call pImprimirEtiquetas(arEtiquetas())
        End If
    Else
        MsgBox "La etiqueta solicitada no corresponde con ninguna prueba activa.", vbExclamation, "Impresi�n de Etiquetas"
    End If
 
End Sub

Private Sub Command1_Click(Index As Integer)
    
    Dim nReferencia As String
    Dim Anyo As Integer
    
    Select Case Index
        Case 0
            If txtEtiquetas(0).Text = "" Then
                MsgBox "Es obligatorio indicar el N� de Referencia.", vbExclamation, "Impresi�n de Etiquetas"
                txtEtiquetas(0).SetFocus
            ElseIf txtEtiquetas(1).Text = "" Then
                MsgBox "Es obligatorio indicar el N� de Cultivo.", vbExclamation, "Impresi�n de Etiquetas"
                txtEtiquetas(1).SetFocus
            ElseIf txtEtiquetas(2).Text <> "" And Len(CStr(Val(txtEtiquetas(2).Text))) < 4 Then
                MsgBox "El a�o introducido es incorrecto.", vbExclamation, "Impresi�n de Etiquetas"
                txtEtiquetas(2).SetFocus
                txtEtiquetas(2).SelStart = 0
                txtEtiquetas(2).SelLength = Len(txtEtiquetas(2).Text)
            Else
                If txtEtiquetas(2).Text = "" Then
                    Anyo = Year(fFechaActual)
                Else
                    Anyo = txtEtiquetas(2).Text
                End If
                nReferencia = letraNumRef & Anyo & Left$("0000000", 7 - Len(txtEtiquetas(0).Text)) & txtEtiquetas(0).Text
                Call pPrepararDatosEtiqueta(nReferencia, txtEtiquetas(1).Text)
                txtEtiquetas(0).SetFocus
            End If
        Case 1
            Unload Me
    End Select
    
End Sub


Private Sub Form_Load()

    txtEtiquetas(0).BackColor = objApp.objUserColor.lngMandatory
    txtEtiquetas(1).BackColor = objApp.objUserColor.lngMandatory
    
End Sub

Private Sub txtEtiquetas_GotFocus(Index As Integer)

    txtEtiquetas(Index).SelStart = 0
    txtEtiquetas(Index).SelLength = Len(txtEtiquetas(Index).Text)
    
End Sub

Private Sub txtEtiquetas_KeyPress(Index As Integer, KeyAscii As Integer)

    If (KeyAscii < 48 Or KeyAscii > 57) And KeyAscii <> 8 Then
        If KeyAscii = 13 Then
            If Index < 2 Then
                txtEtiquetas(Index + 1).SetFocus
            Else
                Call Command1_Click(0)
            End If
        Else
            KeyAscii = 0
        End If
    End If
        
End Sub


