VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Begin VB.Form frmA_Pruebas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consulta de Pruebas"
   ClientHeight    =   8340
   ClientLeft      =   705
   ClientTop       =   900
   ClientWidth     =   11910
   ControlBox      =   0   'False
   Icon            =   "MB00158.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   8308.125
   ScaleMode       =   0  'Usuario
   ScaleWidth      =   18557.44
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      Appearance      =   1
      _Version        =   327682
   End
   Begin VB.Frame fraPruebaAsistencia 
      Caption         =   "Paciente/Prueba"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2235
      Left            =   120
      TabIndex        =   17
      Top             =   480
      Width           =   11655
      Begin TabDlg.SSTab tabPruebaAsistencia 
         Height          =   1755
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   360
         Width           =   11385
         _ExtentX        =   20082
         _ExtentY        =   3096
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "MB00158.frx":0442
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblPruebaAsistencia(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblPruebaAsistencia(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblPruebaAsistencia(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblPruebaAsistencia(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblPruebaAsistencia(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblPruebaAsistencia(5)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblPruebaAsistencia(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblPruebaAsistencia(8)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblPruebaAsistencia(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblPruebaAsistencia(10)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtPruebaAsistencia(7)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtPruebaAsistencia(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtPruebaAsistencia(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtPruebaAsistencia(2)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtPruebaAsistencia(3)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtPruebaAsistencia(4)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtPruebaAsistencia(5)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtPruebaAsistencia(6)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtPruebaAsistencia(8)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtPruebaAsistencia(9)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtPruebaAsistencia(10)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtPruebaAsistencia(11)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtPruebaAsistencia(12)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtPruebaAsistencia(13)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "MB00158.frx":045E
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPruebaAsistencia"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPac"
            Height          =   285
            Index           =   13
            Left            =   2520
            TabIndex        =   13
            Top             =   1380
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPrueba"
            Height          =   285
            Index           =   12
            Left            =   2280
            TabIndex        =   12
            Top             =   1380
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "comPetic"
            Height          =   285
            Index           =   11
            Left            =   2040
            TabIndex        =   11
            Top             =   1380
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "fNac"
            Height          =   285
            Index           =   10
            Left            =   6900
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "Fecha Nac.|Fecha de Nacimiento"
            ToolTipText     =   "Fecha de nacimiento"
            Top             =   120
            Width           =   1035
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "caso"
            Height          =   285
            Index           =   9
            Left            =   720
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Caso|Caso"
            ToolTipText     =   "Caso"
            Top             =   840
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "historia"
            Height          =   285
            Index           =   8
            Left            =   720
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "Historia|Historia"
            ToolTipText     =   "Historia"
            Top             =   480
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            Height          =   855
            Index           =   6
            Left            =   2760
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Indicaciones|Indicaciones del Peticionario"
            ToolTipText     =   "Indicaciones y observaciones"
            Top             =   840
            Width           =   8235
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "drSolicitante"
            Height          =   285
            Index           =   5
            Left            =   6900
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Dr. solicitante|Doctor solicitante"
            ToolTipText     =   "Doctor solicitante"
            Top             =   480
            Width           =   4095
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "dptSolicitante"
            Height          =   285
            Index           =   4
            Left            =   2760
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Dpto. solicitante|Departamento solicitante"
            ToolTipText     =   "Departamento solicitante"
            Top             =   480
            Width           =   3255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "desigPrueba"
            Height          =   285
            Index           =   3
            Left            =   8580
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Prueba|Prueba"
            ToolTipText     =   "Prueba a realizar"
            Top             =   120
            Width           =   2430
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "Cama"
            Height          =   285
            Index           =   2
            Left            =   720
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Cama|Cama de Ingreso"
            ToolTipText     =   "N�mero de cama"
            Top             =   1200
            Width           =   915
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "paciente"
            Height          =   285
            Index           =   1
            Left            =   2760
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Paciente|Nombre del Paciente"
            ToolTipText     =   "Nombre y apellidos del paciente"
            Top             =   120
            Width           =   3255
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            Height          =   285
            Index           =   0
            Left            =   720
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "N� Ref.|N�mero de referencia"
            ToolTipText     =   "N�mero de referencia"
            Top             =   120
            Width           =   915
         End
         Begin SSDataWidgets_B.SSDBGrid SSDBGrid2 
            Height          =   2055
            Left            =   -74880
            TabIndex        =   19
            Top             =   360
            Width           =   9855
            _Version        =   131078
            DataMode        =   1
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelColorFrame =   65535
            BevelColorHighlight=   8454143
            BevelColorShadow=   65535
            BevelColorFace  =   65535
            BackColorEven   =   65535
            BackColorOdd    =   65535
            RowHeight       =   423
            Columns(0).Width=   3200
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   3625
            _StockProps     =   79
            BackColor       =   65535
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPruebaAsistencia 
            Height          =   1635
            Left            =   -74880
            TabIndex        =   30
            Top             =   60
            Width           =   10875
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            Columns(0).Width=   3200
            UseDefaults     =   0   'False
            _ExtentX        =   19182
            _ExtentY        =   2884
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.TextBox txtPruebaAsistencia 
            BackColor       =   &H80000000&
            DataField       =   "nRef"
            Height          =   285
            Index           =   7
            Left            =   720
            TabIndex        =   0
            Tag             =   "N� Ref.|N�mero de referencia"
            Top             =   120
            Visible         =   0   'False
            Width           =   1275
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "F. Nac.:"
            Height          =   255
            Index           =   10
            Left            =   6240
            TabIndex        =   36
            Top             =   120
            Width           =   615
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Caso:"
            Height          =   255
            Index           =   9
            Left            =   240
            TabIndex        =   33
            Top             =   840
            Width           =   615
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Historia:"
            Height          =   255
            Index           =   8
            Left            =   60
            TabIndex        =   32
            Top             =   480
            Width           =   615
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Indicaciones:"
            Height          =   255
            Index           =   6
            Left            =   1740
            TabIndex        =   26
            Top             =   840
            Width           =   975
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Dr. Solic.:"
            Height          =   255
            Index           =   5
            Left            =   6120
            TabIndex        =   25
            Top             =   480
            Width           =   735
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Dpto. Solic.:"
            Height          =   255
            Index           =   4
            Left            =   1800
            TabIndex        =   24
            Top             =   480
            Width           =   975
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Prueba:"
            Height          =   255
            Index           =   3
            Left            =   7980
            TabIndex        =   23
            Top             =   120
            Width           =   735
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Cama:"
            Height          =   255
            Index           =   2
            Left            =   180
            TabIndex        =   22
            Top             =   1200
            Width           =   495
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "Paciente:"
            Height          =   255
            Index           =   1
            Left            =   1980
            TabIndex        =   21
            Top             =   120
            Width           =   735
         End
         Begin VB.Label lblPruebaAsistencia 
            Caption         =   "N� Ref.:"
            Height          =   255
            Index           =   0
            Left            =   60
            TabIndex        =   20
            Top             =   120
            Width           =   615
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   29
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Frame fraMuestras 
      Caption         =   "Vista resumida"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5175
      Left            =   120
      TabIndex        =   27
      Top             =   2820
      Width           =   11655
      Begin VB.TextBox txtObserv 
         Height          =   3975
         Left            =   7440
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   37
         Tag             =   "Observaciones|Observaciones"
         Top             =   540
         Width           =   4035
      End
      Begin VB.CommandButton cmdPruebaAsistencia 
         Caption         =   "&Buscar  N� Ref."
         Default         =   -1  'True
         Height          =   435
         Left            =   10200
         TabIndex        =   14
         Tag             =   "Buscar por n�mero de referencia"
         ToolTipText     =   "Busca un n�mero de referencia"
         Top             =   4560
         Width           =   1335
      End
      Begin MSRDC.MSRDC msrdcObserv 
         Height          =   330
         Left            =   9540
         Top             =   4740
         Visible         =   0   'False
         Width           =   2100
         _ExtentX        =   3704
         _ExtentY        =   582
         _Version        =   327681
         Options         =   0
         CursorDriver    =   1
         BOFAction       =   0
         EOFAction       =   0
         RecordsetType   =   1
         LockType        =   1
         QueryType       =   0
         Prompt          =   1
         Appearance      =   1
         QueryTimeout    =   30
         RowsetSize      =   100
         LoginTimeout    =   15
         KeysetSize      =   0
         MaxRows         =   0
         ErrorThreshold  =   -1
         BatchSize       =   15
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Enabled         =   -1  'True
         ReadOnly        =   0   'False
         Appearance      =   -1  'True
         DataSourceName  =   ""
         RecordSource    =   ""
         UserName        =   ""
         Password        =   ""
         Connect         =   ""
         LogMessages     =   ""
         Caption         =   "MSRDC1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.TextBox txtAmpliacion 
         DataField       =   "result"
         Height          =   1575
         Left            =   1440
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   35
         Top             =   1680
         Visible         =   0   'False
         Width           =   5415
      End
      Begin VB.CommandButton cmdTreeView 
         Caption         =   "&Completa"
         Enabled         =   0   'False
         Height          =   435
         Left            =   7440
         TabIndex        =   15
         ToolTipText     =   "Cambiar a vista completa"
         Top             =   4560
         Width           =   1335
      End
      Begin ComctlLib.TreeView tvwMuestras 
         Height          =   4635
         Left            =   120
         TabIndex        =   28
         Tag             =   "Datos de la prueba|Datos de la Prueba"
         Top             =   360
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   8176
         _Version        =   327682
         Indentation     =   529
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         BorderStyle     =   1
         Appearance      =   1
      End
      Begin VB.Label Label2 
         Caption         =   "Observaciones:"
         Height          =   255
         Left            =   7500
         TabIndex        =   38
         Top             =   300
         Width           =   1095
      End
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      Height          =   1095
      Left            =   2520
      TabIndex        =   34
      Top             =   4560
      Width           =   5895
   End
   Begin VB.Label lblPruebaAsistencia 
      Caption         =   "N� Ref."
      Height          =   255
      Index           =   7
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   615
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edicion"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener"
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero       Ctrl+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior      Re P�g"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente    Av P�g"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo         Ctrl+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Pruebas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'-------------------------
'Declaraciones generales
'-------------------------

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objPruebaAsistencia As New clsCWForm    'Maestro de Paciente/Prueba
Dim whereInicial As String                  'Se guarda la consulta inicial
Dim nReferencia As String                   'N�mero de referencia completo
Dim nReferenciaPostRead As String           'guarda la nRef de la prueba. Se utiliza _
                                            en objWinInfo.cwPostRead para no actualizar _
                                            m�s veces de las necesarias.

Private Sub pDatosColonia(nReferencia$, cCol%)

Dim SQL As String

SQL = "SELECT nRef, MB27_codCol, MB27_descrip"
SQL = SQL & " FROM MB2700"
SQL = SQL & " WHERE nRef = '" & nReferencia & "'"
SQL = SQL & " AND MB27_codCol = " & cCol
msrdcObserv.SQL = SQL
msrdcObserv.Refresh
txtObserv.Text = "COLONIA: " & msrdcObserv.Resultset(2)
msrdcObserv.Resultset.Close

End Sub

Private Sub pDatosTecnica(nReferencia$, cTecAsist%)

Dim SQL As String

'On Error Resume Next
SQL = "SELECT nRef, MB20_codTecAsist, MB20_reali, MB20_observ"
SQL = SQL & " FROM MB2000"
SQL = SQL & " WHERE nRef = '" & nReferencia & "'"
SQL = SQL & " AND MB20_codTecAsist = " & cTecAsist
msrdcObserv.SQL = SQL
msrdcObserv.Refresh
txtObserv.Text = "A LA REALIZACI�N: " & msrdcObserv.Resultset(2)
txtObserv.Text = txtObserv.Text & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
txtObserv.Text = txtObserv.Text & "OTRAS: " & msrdcObserv.Resultset(3)
msrdcObserv.Resultset.Close

End Sub
Private Sub pDatosColoniaPlaca(nReferencia$, cCol%, cTecAsist%)

Dim SQL As String

SQL = "SELECT nRef, MB20_codTecAsist, MB27_codCol, MB30_observ"
SQL = SQL & " FROM MB3000"
SQL = SQL & " WHERE nRef = '" & nReferencia & "'"
SQL = SQL & " AND MB20_codTecAsist = " & cTecAsist
SQL = SQL & " AND MB27_codCol = " & cCol
msrdcObserv.SQL = SQL
msrdcObserv.Refresh
txtObserv.Text = txtObserv.Text & Chr$(13) & Chr$(10) & Chr$(13) & Chr$(10)
txtObserv.Text = txtObserv.Text & "COLONIA/PLACA: " & msrdcObserv.Resultset(3)
msrdcObserv.Resultset.Close

End Sub


Private Sub cmdPruebaAsistencia_Click()
    Dim numRef As String
    Dim SQL As String
    
    txtObserv.Text = ""
    'Toma el n�mero de referencia que se quiere buscar
    numRef = fVentanaNRef()
    If numRef <> "" Then
        'Como no va la linternita en una segunda pantalla...
        Screen.MousePointer = vbHourglass
        'Nueva consulta
        SQL = whereInicial & " AND nRef = '" & numRef & "'"
        objPruebaAsistencia.strWhere = SQL
        'Refresca los datos de la nueva consulta
        Call objWinInfo.DataRefresh
    End If
    'Se quita el relojito de arena
    Screen.MousePointer = vbDefault
End Sub

'Bot�n que cambia de vista el tvwMuestras Completa/Resumen
Private Sub cmdTreeView_Click()
    'On Error Resume Next
    Screen.MousePointer = vbHourglass
    txtObserv.Text = ""
    If cmdTreeView.Caption = "&Completa" Then
        'Presenta el treeview Completo
        Call pTVMuestrasMuestras(tvwMuestras, nReferencia)
        cmdTreeView.Caption = "Re&sumida"
        cmdTreeView.ToolTipText = "Cambiar a vista resumida"
        fraMuestras.Caption = "Vista completa"
    Else
        'Presenta el treeview Resumen
        Call pTVResumenTecnicas(tvwMuestras, nReferencia)
        Call pTVResumenColonias(tvwMuestras, nReferencia)
        cmdTreeView.Caption = "&Completa"
        cmdTreeView.ToolTipText = "Cambiar a vista completa"
        fraMuestras.Caption = "Vista resumida"
    End If
    Screen.MousePointer = vbDefault
End Sub

' ---------------------------------------------------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' ---------------------------------------------------------------------------------------
Private Sub Form_Load()
    Dim nHistoria As String
    Dim nCaso As String
    Dim i As Integer
    Dim strKey As String
    Dim SQL As String
    
    tvwMuestras.ImageList = frmPrincipal.imlImageList2
    Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    With objPruebaAsistencia
        .strName = "PRUEBAASISTENCIA"
        Set .objFormContainer = fraPruebaAsistencia
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabPruebaAsistencia
        Set .grdGrid = grdssPruebaAsistencia
        .strDataBase = objEnv.GetValue("Main")
        .intAllowance = cwAllowReadOnly
        
        'Se traen las pruebas que todav�a est�n en es estado de Solicitud en pruebaAsistencia
        .strTable = "PRUEBAASISTENCIA_2"
        SQL = "Proceso=" & constPROCESOSOLICITUD
        SQL = SQL & " AND nRef IS NOT NULL"
        SQL = SQL & " AND cCarpeta IN "
        SQL = SQL & "    (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
        'Se guarda el Where para utilizarlo con el bot�n seleccionar
        whereInicial = SQL
        'Recoge los datos enviados a trav�s de pVentanaPruebas
        'N�mero de referencia
        If objPipe.PipeExist("BuscNRef") Then
            nReferencia = objPipe.PipeGet("BuscNRef")
        Else
            nReferencia = ""
        End If
        'N�mero de historia
        If objPipe.PipeExist("BuscHistoria") Then
            nHistoria = objPipe.PipeGet("BuscHistoria")
        Else
            nHistoria = ""
        End If
        'N�mero de caso
        If objPipe.PipeExist("BuscCaso") Then
            nCaso = objPipe.PipeGet("BuscCaso")
        Else
            nCaso = ""
        End If
        'Introduce las nuevas restricciones a la b�squeda
        If nReferencia <> "" Then SQL = SQL & " AND nRef = '" & nReferencia & "'"
        If nHistoria <> "" Then SQL = SQL & " AND historia = " & nHistoria
        If nCaso <> "" Then SQL = SQL & " AND caso = " & nCaso
        'Se inicia la pantalla:...
        If nReferencia <> "" Or nHistoria <> "" Then
            '...con un caso, si se especifican nref y/o historia
            Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                            Me, tlbToolbar1, stbStatusBar1, _
                            cwWithAll)
            cmdPruebaAsistencia.Enabled = False
            cmdTreeView.Enabled = True
        Else
            '...vac�a, si no se especifica nada
            Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, Me, _
                            tlbToolbar1, stbStatusBar1, cwWithAll)
        End If
        .strWhere = SQL
        Call .FormAddOrderField("NRef", cwAscending)
      
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "PRUEBAASISTENCIA")
        Call .FormAddFilterWhere(strKey, "nRef", "N� Ref.", cwString)
        Call .FormAddFilterWhere(strKey, "historia", "Historia", cwNumeric)
        Call .FormAddFilterWhere(strKey, "caso", "Caso", cwNumeric)
        Call .FormAddFilterWhere(strKey, "paciente", "Paciente", cwString)
        Call .FormAddFilterWhere(strKey, "cama", "Cama", cwNumeric)
        Call .FormAddFilterWhere(strKey, "fnac", "Fecha Nac.", cwNumeric)
        Call .FormAddFilterWhere(strKey, "desigPrueba", "Prueba", cwString)
        Call .FormAddFilterWhere(strKey, "dptSolicitante", "Dpto. solicitud", cwString)
        Call .FormAddFilterWhere(strKey, "drSolicitante", "Dr. Solicitud", cwString)
        
        Call .FormAddFilterOrder(strKey, "nRef", "N� Ref.")
        Call .FormAddFilterOrder(strKey, "historia", "Historia")
        Call .FormAddFilterOrder(strKey, "caso", "Caso")
        Call .FormAddFilterOrder(strKey, "paciente", "Paciente")
        Call .FormAddFilterOrder(strKey, "cama", "Cama")
        Call .FormAddFilterOrder(strKey, "fnac", "Fecha Nac.")
        Call .FormAddFilterOrder(strKey, "desigPrueba", "Prueba")
        Call .FormAddFilterOrder(strKey, "dptSolicitante", "Dpto. solicitud")
        Call .FormAddFilterOrder(strKey, "drSolicitante", "Dr. Solicitud")
        
        Call .objPrinter.Add("MB00101", "Informe de resultados informados", 1, crptToPrinter)
    End With

    With objWinInfo
        Call .FormAddInfo(objPruebaAsistencia, cwFormDetail)
        
        Call .FormCreateInfo(objPruebaAsistencia)
        'controles ReadOnly
        For i = 0 To txtPruebaAsistencia.Count - 1
            .CtrlGetInfo(txtPruebaAsistencia(i)).blnReadOnly = True
        Next i
        'datos no visibles en la tabla
        .CtrlGetInfo(txtPruebaAsistencia(11)).blnInGrid = False 'comPetic
        .CtrlGetInfo(txtPruebaAsistencia(12)).blnInGrid = False 'comPrueba
        .CtrlGetInfo(txtPruebaAsistencia(13)).blnInGrid = False 'comPac
        'informaci�n sobre b�squedas
        .CtrlGetInfo(txtPruebaAsistencia(1)).blnInFind = True 'paciente
        .CtrlGetInfo(txtPruebaAsistencia(2)).blnInFind = True 'cama
        .CtrlGetInfo(txtPruebaAsistencia(3)).blnInFind = True 'prueba
        .CtrlGetInfo(txtPruebaAsistencia(4)).blnInFind = True 'dpt
        .CtrlGetInfo(txtPruebaAsistencia(5)).blnInFind = True 'dr
        .CtrlGetInfo(txtPruebaAsistencia(7)).blnInFind = True 'num. ref.
        .CtrlGetInfo(txtPruebaAsistencia(8)).blnInFind = True 'historia
        .CtrlGetInfo(txtPruebaAsistencia(9)).blnInFind = True 'caso
        '.CtrlGetInfo(txtPruebaAsistencia(10).blnInFind = True 'la cWCun no soporta b�squedas sobre fechas
            
        Call .FormChangeColor(objPruebaAsistencia)
        
        Call .WinRegister
        Call .WinStabilize
        
        msrdcObserv.Connect = objApp.rdoConnect.Connect
    End With
    
    Screen.MousePointer = vbDefault 'Call objApp.SplashOff   'Eliminar la linternita
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub
Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid y del tab
' -----------------------------------------------

Private Sub grdSSPruebaAsistencia_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdSSPruebaAsistencia_RowLoaded(ByVal Bookmark As Variant)
    grdssPruebaAsistencia.Columns(1).Text = Val(Mid$(grdssPruebaAsistencia.Columns(1).Text, 6))
End Sub

Private Sub grdSSPruebaAsistencia_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdSSPruebaAsistencia_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdSSPruebaAsistencia_Change()
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub tabPruebaAsistencia_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabPruebaAsistencia(), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los men�es
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' ---------------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones y de la barra status
' ---------------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As ComctlLib.Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub

Private Sub stbStatusBar1_PanelClick(ByVal panPanel As ComctlLib.Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------

Private Sub fraPruebaAsistencia_Click()
  Call objWinInfo.FormChangeActive(fraPruebaAsistencia(), False, True)
End Sub

Private Sub fraMuestras_Click()
  Call objWinInfo.FormChangeActive(fraMuestras(), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  
  Select Case strFormName
    Case objPruebaAsistencia.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        Select Case intReport
            Case 1
                pImprimirInformeResultado (nReferencia)
        End Select
        Set objPrinter = Nothing
  End Select
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub
Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

'Se completan los datos no tra�dos directamente de la base de datos
Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
    
      If objPruebaAsistencia.strName = strFormName Then
        txtObserv.Text = ""
        On Error Resume Next
        'Si no hay pruebas para leer, aparece un mensaje
        If objPruebaAsistencia.rdoCursor.EOF = True Then
            If Err = 91 Then
                Exit Sub
            End If
            MsgBox "No hay ninguna prueba para leer", vbInformation, Me.Caption
            tvwMuestras.Nodes.Clear 'Se limpia el treeview
            cmdTreeView.Enabled = False
            nReferenciaPostRead = ""
            On Error GoTo 0
        Else
            'Se completan y formatean los datos que no se han podido traer directamente
            txtPruebaAsistencia(0).Text = Val(Mid$(txtPruebaAsistencia(7).Text, 6))
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPetic")) Then
                txtPruebaAsistencia(6).Text = "PETICI�N: " & objPruebaAsistencia.rdoCursor("comPetic")
            End If
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPrueba")) Then
                txtPruebaAsistencia(6).Text = txtPruebaAsistencia(6).Text & Chr$(13) & Chr$(10) & "PRUEBA: " & objPruebaAsistencia.rdoCursor("comPrueba")
            End If
            If Not IsNull(objPruebaAsistencia.rdoCursor("comPac")) Then
                txtPruebaAsistencia(6).Text = txtPruebaAsistencia(6).Text & Chr$(13) & Chr$(10) & "PACIENTE: " & objPruebaAsistencia.rdoCursor("comPac")
            End If
            
            'si anota el n� de referencia en la variable. Si la prueba es la misma que hay _
            en pantalla no se refrescan los treeviews
            If txtPruebaAsistencia(7).Text = nReferenciaPostRead Then
                Exit Sub
            Else
                nReferencia = txtPruebaAsistencia(7).Text
                nReferenciaPostRead = txtPruebaAsistencia(7).Text
                tvwMuestras.Nodes.Clear             'Se limpia el treeView
            End If
            
            'Se llena el tvwMuestras en formato resumido
            Call pTVResumenTecnicas(tvwMuestras, nReferencia)
            Call pTVResumenColonias(tvwMuestras, nReferencia)
            cmdTreeView.Caption = "&Completa"
            fraMuestras.Caption = "Vista resumida"
            cmdTreeView.Enabled = True
        End If
    End If
    
End Sub

Private Sub tvwMuestras_KeyPress(KeyAscii As Integer)
    txtAmpliacion.Visible = False
End Sub

Private Sub tvwMuestras_MouseDown(Button As Integer, Shift As Integer, X As Single, y As Single)
Dim nodo As Node
Dim KeyNodo As String
Dim codResult As Integer
Dim codTecAsist As Integer
Dim codTec As Integer
    
    On Error Resume Next
    Set tvwMuestras.SelectedItem = tvwMuestras.HitTest(X, y)
    'Ver el Keynodo del nodo seleccionado en el treeview
    KeyNodo = tvwMuestras.SelectedItem.Key
    'Si el Keynodo es de un resultado...
    If Left(KeyNodo, 1) = "R" Then
        codResult = Val(Mid(KeyNodo, 2))
        codTecAsist = Val(Mid(KeyNodo, InStr(KeyNodo, "T") + 1))
        codTec = Val(Mid(KeyNodo, InStr(KeyNodo, "N") + 1))
        nReferencia = txtPruebaAsistencia(7)
        'Si el texto del nodo es muy largo...
        If Len(tvwMuestras.SelectedItem.Text) >= constVISIBLE_LONG_RESULT Then
            'Se llama a la funci�n que ampl�a el texto del resultado
            Call pAmpliarTexto(nReferencia, codTecAsist, codTec, codResult, X, y)
        End If
    End If
End Sub

Private Sub pAmpliarTexto(nRef$, codTecAsist%, codTec%, codResult%, X!, y!)
Dim SQL As String
Dim rsResult As rdoResultset, qryResult As rdoQuery
Dim cotax As Integer
Dim cotay As Integer
    'Se realiza la consulta del texto del resultado
    SQL = "SELECT MB33_result"
    SQL = SQL & " FROM MB3300"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB09_codTec = ?"
    SQL = SQL & " AND MB32_codResult = ?"
    Set qryResult = objApp.rdoConnect.CreateQuery("Resultados", SQL)
    qryResult(0) = nRef
    qryResult(1) = codTecAsist
    qryResult(2) = codTec
    qryResult(3) = codResult
    Set rsResult = qryResult.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    
    'Se coloca el resultado en el label
    txtAmpliacion.Text = rsResult(0)
    cotax = tvwMuestras.Width - txtAmpliacion.Width
    If X > cotax Then
        txtAmpliacion.Left = cotax
    Else
        txtAmpliacion.Left = X
    End If
    cotay = tvwMuestras.Height - txtAmpliacion.Height

'    If y > cotay Then
'        txtAmpliacion.Top = cotay
'    Else
'        txtAmpliacion.Top = y
'    End If

    If y < tvwMuestras.Height / 2 Then
        txtAmpliacion.Top = y + 500
    Else
        txtAmpliacion.Top = y - txtAmpliacion.Height + 300
    End If
    txtAmpliacion.Visible = True
    
End Sub

Private Sub tvwMuestras_MouseMove(Button As Integer, Shift As Integer, X As Single, y As Single)
    If Button = 0 Then txtAmpliacion.Visible = False
End Sub

Private Sub tvwMuestras_MouseUp(Button As Integer, Shift As Integer, X As Single, y As Single)
    txtAmpliacion.Visible = False
End Sub

Private Sub tvwMuestras_NodeClick(ByVal Node As ComctlLib.Node)

Select Case Left$(Node.Key, 1)
    Case "K"
        If cmdTreeView.Caption = "&Completa" Then 'vista resumida
            Call pDatosColonia(txtPruebaAsistencia(7).Text, Val(Mid$(Node.Key, InStr(Node.Key, "K") + 1)))
        Else 'vista completa
            Call pDatosColonia(txtPruebaAsistencia(7).Text, Val(Mid$(Node.Key, InStr(Node.Key, "K") + 1)))
            Call pDatosColoniaPlaca(txtPruebaAsistencia(7).Text, Val(Mid$(Node.Key, InStr(Node.Key, "K") + 1)), Val(Mid$(Node.Parent.Key, InStr(Node.Parent.Key, "C") + 1)))
        End If
        
    Case "C"
        Call pDatosTecnica(txtPruebaAsistencia(7).Text, Val(Mid$(Node.Key, InStr(Node.Key, "C") + 1)))
    
    Case "T"
        Call pDatosTecnica(txtPruebaAsistencia(7).Text, Val(Mid$(Node.Key, InStr(Node.Key, "T") + 1)))
    
    Case Else
        txtObserv.Text = ""
        
End Select

End Sub

Private Sub txtAmpliacion_Click()
    txtAmpliacion.Visible = False
End Sub
