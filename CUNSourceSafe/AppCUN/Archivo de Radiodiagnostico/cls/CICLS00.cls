VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWLauncher
' **********************************************************************************

'Const CIWinArchivo            As String = "FO001"
'Const CIWinArea               As String = "FO002"
'Const CIWinDiagnoctico        As String = "FO003"
'Const CIWinTecnica            As String = "FO004"
Const CIWinArchivo            As String = "RD0100"
Const CIWinArea               As String = "RD0200"
Const CIWinDiagnoctico        As String = "RD0300"
Const CIWinTecnica            As String = "RD0400"


' las aplicaciones y listados externos no se meten por aqu�.

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
 
  ' comienza la selecci�n del proceso
  Select Case strProcess
  Case CIWinArchivo
      Load frmA_Archivo
      Call frmA_Archivo.Show(vbModal)
      Set frmA_Archivo = Nothing
'      Load frmA_Grupo
'      Call frmA_Grupo.Show(vbModal)
'      Set frmA_Grupo = Nothing

  Case CIWinArea
      Load frmA_Area
      Call frmA_Area.Show(vbModal)
      Set frmA_Area = Nothing
  Case CIWinDiagnoctico
      Load frmA_Diagnostico
      Call frmA_Diagnostico.Show(vbModal)
      Set frmA_Diagnostico = Nothing
  Case CIWinTecnica
      Load frmA_Tecnica
      Call frmA_Tecnica.Show(vbModal)
      Set frmA_Tecnica = Nothing
  Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz al n�mero de procesos
'  ReDim aProcess(1 To 58, 1 To 4) As Variant
'
'  ' VENTANAS
'  aProcess(1, 1) = CIWinTipoPoliza
'  aProcess(1, 2) = "Mantenimiento de Tipos de Poliza"
'  aProcess(1, 3) = True
'  aProcess(1, 4) = cwTypeWindow
'
'  aProcess(2, 1) = CIWinColectivos
'  aProcess(2, 2) = "Mantenimiento de Colectivos"
'  aProcess(2, 3) = True
'  aProcess(2, 4) = cwTypeWindow
'
'  aProcess(3, 1) = CIWinEntidadColab
'  aProcess(3, 2) = "Mantenimiento de Entidades Colaboradoras"
'  aProcess(3, 3) = True
'  aProcess(3, 4) = cwTypeWindow
'
'  aProcess(4, 1) = CIWinCentroUniversidad
'  aProcess(4, 2) = "Mantenimiento de Centros Cargo"
'  aProcess(4, 3) = True
'  aProcess(4, 4) = cwTypeWindow
'
'  aProcess(5, 1) = CIWinMotivoDescuento
'  aProcess(5, 2) = "Mantenimiento de Motivos de Descuento"
'  aProcess(5, 3) = True
'  aProcess(5, 4) = cwTypeWindow
'
'  aProcess(6, 1) = CIWinRelTitular
'  aProcess(6, 2) = "Mantenimiento de Relaciones del Titular"
'  aProcess(6, 3) = True
'  aProcess(6, 4) = cwTypeWindow
'
'  aProcess(7, 1) = CIWinConciertos
'  aProcess(7, 2) = "Mantenimiento de Conciertos Econ�micos"
'  aProcess(7, 3) = True
'  aProcess(7, 4) = cwTypeWindow
'
'  aProcess(8, 1) = CIWinTipoEconomico
'  aProcess(8, 2) = "Mantenimiento de Tipos Econ�micos"
'  aProcess(8, 3) = True
'  aProcess(8, 4) = cwTypeWindow
'
'  aProcess(9, 1) = CIWinPersonalUni
'  aProcess(9, 2) = "Mantenimiento de Personal Universitario"
'  aProcess(9, 3) = True
'  aProcess(9, 4) = cwTypeWindow
'
'  aProcess(10, 1) = CIWinACUNSA
'  aProcess(10, 2) = "Mantenimiento de Asegurados ACUNSA"
'  aProcess(10, 3) = True
'  aProcess(10, 4) = cwTypeWindow
'
'  aProcess(11, 1) = CIWinEstadoCivil
'  aProcess(11, 2) = "Mantenimiento de Estados Civiles"
'  aProcess(11, 3) = True
'  aProcess(11, 4) = cwTypeWindow
'
'  aProcess(12, 1) = CIWinProfesion
'  aProcess(12, 2) = "Mantenimiento de Profesiones"
'  aProcess(12, 3) = True
'  aProcess(12, 4) = cwTypeWindow
'
'  aProcess(13, 1) = CIWinTratamiento
'  aProcess(13, 2) = "Mantenimiento de Tratamientos"
'  aProcess(13, 3) = True
'  aProcess(13, 4) = cwTypeWindow
'
'  aProcess(14, 1) = CIWinVinculoFamiliar
'  aProcess(14, 2) = "Mantenimiento de V�nculos Familiares"
'  aProcess(14, 3) = True
'  aProcess(14, 4) = cwTypeWindow
'
'  aProcess(15, 1) = CIWinPais
'  aProcess(15, 2) = "Mantenimiento de Pa�ses"
'  aProcess(15, 3) = True
'  aProcess(15, 4) = cwTypeWindow
'
'  aProcess(16, 1) = CIWinProvincia
'  aProcess(16, 2) = "Mantenimiento de Provincias-Municipios"
'  aProcess(16, 3) = True
'  aProcess(16, 4) = cwTypeWindow
'
'  aProcess(17, 1) = CIWinPersFisicas
'  aProcess(17, 2) = "Mantenimiento de Personas F�sicas"
'  aProcess(17, 3) = True
'  aProcess(17, 4) = cwTypeWindow
'
'  aProcess(18, 1) = CIWinPersJuridicas
'  aProcess(18, 2) = "Mantenimiento de Personas Jur�dicas"
'  aProcess(18, 3) = True
'  aProcess(18, 4) = cwTypeWindow
'
'  aProcess(19, 1) = CIWinPatologias
'  aProcess(19, 2) = "Mantenimiento de Patolog�as"
'  aProcess(19, 3) = True
'  aProcess(19, 4) = cwTypeWindow
'
'  aProcess(20, 1) = CIWinRecordatorio
'  aProcess(20, 2) = "Emitir Recordatorio"
'  aProcess(20, 3) = False
'  aProcess(20, 4) = cwTypeWindow
'
'  aProcess(21, 1) = CIWinCargaMunicip
'  aProcess(21, 2) = "Carga de Municipios-Localidades"
'  aProcess(21, 3) = True
'  aProcess(21, 4) = cwTypeWindow
'
'  aProcess(22, 1) = CIWinCargaTextos
'  aProcess(22, 2) = "Carga de Ficheros de Texto"
'  aProcess(22, 3) = True
'  aProcess(22, 4) = cwTypeWindow
'
'  ' LISTADOS
'  aProcess(23, 1) = CIRepTipoPoliza
'  aProcess(23, 2) = "Relaci�n de Tipos de Poliza"
'  aProcess(23, 3) = False
'  aProcess(23, 4) = cwTypeReport
'
'  aProcess(24, 1) = CIRepColectivos
'  aProcess(24, 2) = "Relaci�n de Colectivos"
'  aProcess(24, 3) = False
'  aProcess(24, 4) = cwTypeReport
'
'  aProcess(25, 1) = CIRepEntidadColab
'  aProcess(25, 2) = "Relaci�n de Entidades Colaboradoras"
'  aProcess(25, 3) = False
'  aProcess(25, 4) = cwTypeReport
'
'  aProcess(26, 1) = CIRepCentroUniversidad
'  aProcess(26, 2) = "Relaci�n de Centros Universidad"
'  aProcess(26, 3) = False
'  aProcess(26, 4) = cwTypeReport
'
'  aProcess(27, 1) = CIRepMotivoDescuento
'  aProcess(27, 2) = "Relaci�n de Motivos de Descuento"
'  aProcess(27, 3) = False
'  aProcess(27, 4) = cwTypeReport
'
'  aProcess(28, 1) = CIRepConciertos
'  aProcess(28, 2) = "Relaci�n de Conciertos Econ�micos"
'  aProcess(28, 3) = False
'  aProcess(28, 4) = cwTypeReport
'
'  aProcess(29, 1) = CIRepRelTitular
'  aProcess(29, 2) = "Relaci�n de Relaciones del Titular"
'  aProcess(29, 3) = False
'  aProcess(29, 4) = cwTypeReport
'
'  aProcess(30, 1) = CIRepTipoEconomico
'  aProcess(30, 2) = "Relaci�n de Tipos Econ�micos"
'  aProcess(30, 3) = False
'  aProcess(30, 4) = cwTypeReport
'
'  aProcess(31, 1) = CIRepPersonalUni
'  aProcess(31, 2) = "Relaci�n de Personal de Universidad"
'  aProcess(31, 3) = False
'  aProcess(31, 4) = cwTypeReport
'
' ' aProcess(32, 1) = CIRepACUNSA
' ' aProcess(32, 2) = "Relaci�n de Asegurados ACUNSA"
' ' aProcess(32, 3) = False
' ' aProcess(32, 4) = cwTypeReport
'
'  aProcess(33, 1) = CIRepEstadoCivil
'  aProcess(33, 2) = "Relaci�n de Estados Civiles"
'  aProcess(33, 3) = False
'  aProcess(33, 4) = cwTypeReport
'
'  aProcess(34, 1) = CIRepProfesion
'  aProcess(34, 2) = "Relaci�n de Profesiones"
'  aProcess(34, 3) = False
'  aProcess(34, 4) = cwTypeReport
'
'  aProcess(35, 1) = CIRepTratamiento
'  aProcess(35, 2) = "Relaci�n de Tratamientos"
'  aProcess(35, 3) = False
'  aProcess(35, 4) = cwTypeReport
'
'  aProcess(36, 1) = CIRepVinculoFamiliar
'  aProcess(36, 2) = "Relaci�n de V�nculos Familiares"
'  aProcess(36, 3) = False
'  aProcess(36, 4) = cwTypeReport
'
'  aProcess(37, 1) = CIRepPais
'  aProcess(37, 2) = "Relaci�n de Pa�ses"
'  aProcess(37, 3) = False
'  aProcess(37, 4) = cwTypeReport
'
'  aProcess(38, 1) = CIRepProvincia
'  aProcess(38, 2) = "Relaci�n de Provincias-Municipios"
'  aProcess(38, 3) = False
'  aProcess(38, 4) = cwTypeReport
'
'  aProcess(39, 1) = CIRepPersFisicas
'  aProcess(39, 2) = "Relaci�n de Personas F�sicas"
'  aProcess(39, 3) = False
'  aProcess(39, 4) = cwTypeReport
'
'  aProcess(40, 1) = CIRepPersJuridicas
'  aProcess(40, 2) = "Relaci�n de Personas Jur�dicas"
'  aProcess(40, 3) = False
'  aProcess(40, 4) = cwTypeReport
'
'  aProcess(41, 1) = CIRepPatologias
'  aProcess(41, 2) = "Relaci�n de Patolog�as"
'  aProcess(41, 3) = False
'  aProcess(41, 4) = cwTypeReport
'
'  aProcess(42, 1) = CIRepRecordatorio
'  aProcess(42, 2) = "Recordatorio (Correo)"
'  aProcess(42, 3) = False
'  aProcess(42, 4) = cwTypeReport
'
''Proceso de Peticiones de Citas
'  aProcess(43, 1) = CIWinCitasPaciente
'  aProcess(43, 2) = "Mantenimiento de Citas (Pacientes)"
'  aProcess(43, 3) = True
'  aProcess(43, 4) = cwTypeWindow
'
'  aProcess(44, 1) = CIWinPeticiones
'  aProcess(44, 2) = "Peticiones de Citas"
'  aProcess(44, 3) = True
'  aProcess(44, 4) = cwTypeWindow
'
'  aProcess(45, 1) = CIWinRecitacion
'  aProcess(45, 2) = "Mantenimiento de Recitaciones"
'  aProcess(45, 3) = False
'  aProcess(45, 4) = cwTypeWindow
'
'  aProcess(46, 1) = CIWinSoluciones
'  aProcess(46, 2) = "Mantenimiento de Soluciones a Peticiones"
'  aProcess(46, 3) = False
'  aProcess(46, 4) = cwTypeWindow
'
'  aProcess(47, 1) = CIWinFasesRecursos
'  aProcess(47, 2) = "Fases/Recursos por Actuaci�n"
'  aProcess(47, 3) = False
'  aProcess(47, 4) = cwTypeWindow
'
'  aProcess(48, 1) = CIWinCitasRecurso
'  aProcess(48, 2) = "Mantenimiento de Citas (Recursos)"
'  aProcess(48, 3) = True
'  aProcess(48, 4) = cwTypeWindow
'
'  aProcess(49, 1) = CIWinListaEsperaPac
'  aProcess(49, 2) = "Mantenimiento Lista Espera (Pacientes)"
'  aProcess(49, 3) = True
'  aProcess(49, 4) = cwTypeWindow
'
'  aProcess(50, 1) = CIWinListaEsperaRec
'  aProcess(50, 2) = "Mantenimiento Lista Espera (Recursos)"
'  aProcess(50, 3) = True
'  aProcess(50, 4) = cwTypeWindow
'
'  aProcess(51, 1) = CIWinDocAutorizacion
'  aProcess(51, 2) = "Mantenimiento Documentos Autorizaci�n"
'  aProcess(51, 3) = False
'  aProcess(51, 4) = cwTypeWindow
'
'  aProcess(52, 1) = CIRutCitarSinPlanificar
'  aProcess(52, 2) = "Rutina que Cita sin tener en cuenta la Agenda"
'  aProcess(52, 3) = False
'  aProcess(52, 4) = cwTypeRoutine
'
'  aProcess(53, 1) = CIWinFasesSolucion
'  aProcess(53, 2) = "Fases de las Actuaciones devueltas como soluciones"
'  aProcess(53, 3) = False
'  aProcess(53, 4) = cwTypeWindow
'
'  aProcess(54, 1) = CIRepRecordatorioMano
'  aProcess(54, 2) = "Recordatorio (Mano)"
'  aProcess(54, 3) = False
'  aProcess(54, 4) = cwTypeReport
'
'  aProcess(55, 1) = CIRepCitasConfirmadas
'  aProcess(55, 2) = "Relaci�n de Citas Confirmadas"
'  aProcess(55, 3) = True
'  aProcess(55, 4) = cwTypeReport
'
'  aProcess(56, 1) = CIRepCitasGestionadas
'  aProcess(56, 2) = "Informe Estad�stico de Citas Gestionadas"
'  aProcess(56, 3) = True
'  aProcess(56, 4) = cwTypeReport
'
'  aProcess(57, 1) = CIWinRelacionUDN
'  aProcess(57, 2) = "Mantenimiento de Relaciones con la Universidad"
'  aProcess(57, 3) = True
'  aProcess(57, 4) = cwTypeWindow
'
'  aProcess(58, 1) = CIRepRelacionesUDN
'  aProcess(58, 2) = "Listado de Relaciones con la Universidad"
'  aProcess(58, 3) = False
'  aProcess(58, 4) = cwTypeReport

End Sub
