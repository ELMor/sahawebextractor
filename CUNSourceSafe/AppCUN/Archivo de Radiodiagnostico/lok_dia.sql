
# 
# Table structure for table 'Lokalisation' 
# 
CREATE TABLE Lokalisation ( 
  code CHAR(5), 
  text_data CHAR(200) 
) \g 
 
 
# 
# Table structure for table 'Diagnose' 
# 
CREATE TABLE Diagnose ( 
  code CHAR(10), 
  text_data CHAR(250) 
) \g 



INSERT INTO Lokalisation VALUES ('0','   Breast      ')\g
INSERT INTO Lokalisation VALUES ('00','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('01','  UPPER OUTER QUADRANT      ')\g
INSERT INTO Lokalisation VALUES ('02','  LOWER OUTER QUADRANT      ')\g
INSERT INTO Lokalisation VALUES ('03','  UPPER INNER QUADRANT      ')\g
INSERT INTO Lokalisation VALUES ('04','  LOWER INNER QUADRANT      ')\g
INSERT INTO Lokalisation VALUES ('05','  NIPPLE, AREOLA      ')\g
INSERT INTO Lokalisation VALUES ('06','  SKIN OF BREAST      ')\g
INSERT INTO Lokalisation VALUES ('07','  AXILLA      ')\g
INSERT INTO Lokalisation VALUES ('08','  MORE THAN ONE OF THE ABOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('09','  OTHER    include: internal mammary area, supraclavicular area      ')\g
INSERT INTO Lokalisation VALUES ('1','   Skull and Contents      ')\g
INSERT INTO Lokalisation VALUES ('10','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('11','  CALVARIA, SCALP      ')\g
INSERT INTO Lokalisation VALUES ('111',' Frontal      ')\g
INSERT INTO Lokalisation VALUES ('112',' Parietal      ')\g
INSERT INTO Lokalisation VALUES ('113',' Occipital squamosa      ')\g
INSERT INTO Lokalisation VALUES ('114',' Temporal squamosa      ')\g
INSERT INTO Lokalisation VALUES ('115',' Suture      ')\g
INSERT INTO Lokalisation VALUES ('116',' Fontanel      ')\g
INSERT INTO Lokalisation VALUES ('117',' Scalp      ')\g
INSERT INTO Lokalisation VALUES ('118',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('119',' Other      ')\g
INSERT INTO Lokalisation VALUES ('12','  BASE OF SKULL      ')\g
INSERT INTO Lokalisation VALUES ('121',' Ethmoid   include: crista galli, cribriform plate, olfactory groove      ')\g
INSERT INTO Lokalisation VALUES ('122',' Sella turcica, clivus   include: clinoid process, dorsum  sellae   exclude: pituitary gland and stalk  (145.), tuberculum sellae (123.)      ')\g
INSERT INTO Lokalisation VALUES ('123',' Body of sphenoid   include: planum sphenoidale,  tuberculum sellae, chiasmatic groove  exclude: sella turcica (122.), sphenoid sinus (234.)      ')\g
INSERT INTO Lokalisation VALUES ('124',' Greater, lesser wing of sphenoid   include: foramina, superior orbital fissure, pterion, pterygoid process and pterygoid plate      ')\g
INSERT INTO Lokalisation VALUES ('1241','Optic canal   exclude: anterior clinoid process (122.)      ')\g
INSERT INTO Lokalisation VALUES ('125',' Acoustic (internal auditory) canal      ')\g
INSERT INTO Lokalisation VALUES ('126',' Petrous bone   exclude: acoustic (internal auditory) canal (125.), jugular tubercle and foramen (127.)      ')\g
INSERT INTO Lokalisation VALUES ('127',' Base of occipital bone   include: foramen magnum, hypoglossal canal, occipital condyle, jugular tubercle, jugular foramen      ')\g
INSERT INTO Lokalisation VALUES ('128',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('129',' Other      ')\g
INSERT INTO Lokalisation VALUES ('13','  BRAIN AND MENINGES, SUPRATENTORIAL   exclude: insula (141.), basal ganglia (142.), thalamus (143.), hypothalamus (144.), pituitary gland (145.), incisural structure (146.)      ')\g
INSERT INTO Lokalisation VALUES ('131',' Frontal lobe   exclude: cingulate gyrus (135.)      ')\g
INSERT INTO Lokalisation VALUES ('1311','Frontal pole      ')\g
INSERT INTO Lokalisation VALUES ('1312','Frontal operculum      ')\g
INSERT INTO Lokalisation VALUES ('1313','Inferior portion of frontal lobe  include: olfactory bulb and tract, inferior frontal gyrus, gyrus rectus      ')\g
INSERT INTO Lokalisation VALUES ('1318','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1319','Other      ')\g
INSERT INTO Lokalisation VALUES ('132',' Parietal lobe      ')\g
INSERT INTO Lokalisation VALUES ('133',' Occipital lobe      ')\g
INSERT INTO Lokalisation VALUES ('134',' Temporal lobe      ')\g
INSERT INTO Lokalisation VALUES ('1341','Hippocampal, inferior temporal gyri      ')\g
INSERT INTO Lokalisation VALUES ('1342','Uncus      ')\g
INSERT INTO Lokalisation VALUES ('1343','Temporal operculum      ')\g
INSERT INTO Lokalisation VALUES ('1344','Temporal pole      ')\g
INSERT INTO Lokalisation VALUES ('1348','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1349','Other      ')\g
INSERT INTO Lokalisation VALUES ('135',' Corpus callosum, fornix, cingulate gyrus  exclude: septum pellucidum (1613.)      ')\g
INSERT INTO Lokalisation VALUES ('136',' Subdural space      ')\g
INSERT INTO Lokalisation VALUES ('137',' Epidural space      ')\g
INSERT INTO Lokalisation VALUES ('138',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('139',' Other   include: meninges, falx, tentorium, diaphragma sella, petroclinoid ligament      ')\g
INSERT INTO Lokalisation VALUES ('14','  INSULA, BASAL GANGLIA, THALAMUS, HYPOTHALAMUS, PITUITARY GLAND, INCISURAL STRUCTURE      ')\g
INSERT INTO Lokalisation VALUES ('141',' Insula      ')\g
INSERT INTO Lokalisation VALUES ('142',' Basal ganglia   include: internal capsule      ')\g
INSERT INTO Lokalisation VALUES ('143',' Thalamus      ')\g
INSERT INTO Lokalisation VALUES ('144',' Hypothalamus   include: optic nerve and chiasm      ')\g
INSERT INTO Lokalisation VALUES ('145',' Pituitary gland, stalk      ')\g
INSERT INTO Lokalisation VALUES ('146',' Incisural structure   include: midbrain, cranial nerve III, IV, cerebral peduncle      ')\g
INSERT INTO Lokalisation VALUES ('147',' Pineal gland, midline of brain (ultrasonography)      ')\g
INSERT INTO Lokalisation VALUES ('148',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('149',' Other      ')\g
INSERT INTO Lokalisation VALUES ('15','  BRAIN AND MENINGES, INFRATENTORIAL   exclude: tentorium (139.)      ')\g
INSERT INTO Lokalisation VALUES ('151',' Pons   include: cranial nerve VI, VII  exclude: cranial nerve V, VIII (.154)      ')\g
INSERT INTO Lokalisation VALUES ('152',' Medulla oblongata   include: cranial nerve IX, X, XI, XII      ')\g
INSERT INTO Lokalisation VALUES ('153',' Cerebellum      ')\g
INSERT INTO Lokalisation VALUES ('1531','Hemisphere      ')\g
INSERT INTO Lokalisation VALUES ('1532','Tonsil      ')\g
INSERT INTO Lokalisation VALUES ('1533','Vermis      ')\g
INSERT INTO Lokalisation VALUES ('1538','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('154',' Cerebellopontine angle   include: cranial nerve V, VII, VIII      ')\g
INSERT INTO Lokalisation VALUES ('156',' Subdural space      ')\g
INSERT INTO Lokalisation VALUES ('157',' Epidural space      ')\g
INSERT INTO Lokalisation VALUES ('158',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('159',' Other   include: dural extensions      ')\g
INSERT INTO Lokalisation VALUES ('16','  VENTRICLE      ')\g
INSERT INTO Lokalisation VALUES ('161',' Lateral ventricle      ')\g
INSERT INTO Lokalisation VALUES ('1611','Frontal horn      ')\g
INSERT INTO Lokalisation VALUES ('1612','Foramen of Munro      ')\g
INSERT INTO Lokalisation VALUES ('1613','Body   include: septum pellucidum      ')\g
INSERT INTO Lokalisation VALUES ('1614','Atrium      ')\g
INSERT INTO Lokalisation VALUES ('1615','Occipital horn      ')\g
INSERT INTO Lokalisation VALUES ('1616','Temporal horn      ')\g
INSERT INTO Lokalisation VALUES ('1617','Choroid plexus, glomus      ')\g
INSERT INTO Lokalisation VALUES ('1618','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1619','Other      ')\g
INSERT INTO Lokalisation VALUES ('162',' Third ventricle      ')\g
INSERT INTO Lokalisation VALUES ('1621','Anterior portion   include: optic, infundibular recess      ')\g
INSERT INTO Lokalisation VALUES ('1622','Posterior portion   include: suprapineal recess      ')\g
INSERT INTO Lokalisation VALUES ('1628','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1629','Other      ')\g
INSERT INTO Lokalisation VALUES ('163',' Aqueduct of Sylvius      ')\g
INSERT INTO Lokalisation VALUES ('164',' Fourth ventricle      ')\g
INSERT INTO Lokalisation VALUES ('1641','Roof      ')\g
INSERT INTO Lokalisation VALUES ('1642','Floor      ')\g
INSERT INTO Lokalisation VALUES ('1643','Lateral recess      ')\g
INSERT INTO Lokalisation VALUES ('1644','Foramen of Luschka, Magendie      ')\g
INSERT INTO Lokalisation VALUES ('1648','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1649','Other      ')\g
INSERT INTO Lokalisation VALUES ('165',' Supratentorial cistern, sulcus, fissure      ')\g
INSERT INTO Lokalisation VALUES ('166',' Infratentorial cistern, sulcus, fissure  exclude: cerebellopontine angle (154.)      ')\g
INSERT INTO Lokalisation VALUES ('167',' Cerebrospinal fluid      ')\g
INSERT INTO Lokalisation VALUES ('168',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('169',' Other      ')\g
INSERT INTO Lokalisation VALUES ('17','  VESSEL      ')\g
INSERT INTO Lokalisation VALUES ('171',' External carotid artery   include: meningeal branch      ')\g
INSERT INTO Lokalisation VALUES ('172',' Common carotid artery bifurcation, internal carotid artery      ')\g
INSERT INTO Lokalisation VALUES ('1721','Common carotid artery bifurcation      ')\g
INSERT INTO Lokalisation VALUES ('1722','Internal carotid artery (cervical portion)      ')\g
INSERT INTO Lokalisation VALUES ('1723','Internal carotid artery (petrosal, precavernous, cavernous portion)   include: tentorial, pituitary, other dural branch      ')\g
INSERT INTO Lokalisation VALUES ('1724','Ophthalmic artery   include: falx, anterior meningeal or ethmoid branch      ')\g
INSERT INTO Lokalisation VALUES ('1725','Posterior communicating artery      ')\g
INSERT INTO Lokalisation VALUES ('1726','Anterior choroidal artery      ')\g
INSERT INTO Lokalisation VALUES ('1727','Internal carotid artery (supraclinoid portion)  include: internal carotid bifurcation      ')\g
INSERT INTO Lokalisation VALUES ('1728','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1729','Other   include: meningeal branches      ')\g
INSERT INTO Lokalisation VALUES ('173',' Anterior cerebral artery      ')\g
INSERT INTO Lokalisation VALUES ('1731','Anterior communicating artery      ')\g
INSERT INTO Lokalisation VALUES ('1732','Frontopolar artery      ')\g
INSERT INTO Lokalisation VALUES ('1733','Pericallosal artery      ')\g
INSERT INTO Lokalisation VALUES ('1734','Callosal marginal artery      ')\g
INSERT INTO Lokalisation VALUES ('1738','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1739','Other      ')\g
INSERT INTO Lokalisation VALUES ('174',' Middle cerebral artery      ')\g
INSERT INTO Lokalisation VALUES ('1741','Lenticulostriate arteries      ')\g
INSERT INTO Lokalisation VALUES ('1742','Middle cerebral bifurcation or trifurcation      ')\g
INSERT INTO Lokalisation VALUES ('1743','Insular arteries   include: Sylvian triangle, Sylvian point      ')\g
INSERT INTO Lokalisation VALUES ('1744','Ascending frontal artery      ')\g
INSERT INTO Lokalisation VALUES ('1745','Parietal arteries      ')\g
INSERT INTO Lokalisation VALUES ('1746','Angular and supramarginal arteries      ')\g
INSERT INTO Lokalisation VALUES ('1747','Temporal arteries      ')\g
INSERT INTO Lokalisation VALUES ('1748','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1749','Other      ')\g
INSERT INTO Lokalisation VALUES ('175',' Vertebral, basilar artery      ')\g
INSERT INTO Lokalisation VALUES ('1751','Vertebral artery      ')\g
INSERT INTO Lokalisation VALUES ('1752','Posterior inferior cerebellar artery      ')\g
INSERT INTO Lokalisation VALUES ('1753','Basilar artery      ')\g
INSERT INTO Lokalisation VALUES ('1754','Anterior inferior cerebellar artery, pontine branches      ')\g
INSERT INTO Lokalisation VALUES ('1755','Superior cerebellar artery      ')\g
INSERT INTO Lokalisation VALUES ('1756','Posterior cerebral artery      ')\g
INSERT INTO Lokalisation VALUES ('1758','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1759','Other   include: meningeal branches      ')\g
INSERT INTO Lokalisation VALUES ('176',' Supratentorial vein, dural sinus      ')\g
INSERT INTO Lokalisation VALUES ('1761','Septal vein      ')\g
INSERT INTO Lokalisation VALUES ('1762','Internal cerebral vein      ')\g
INSERT INTO Lokalisation VALUES ('1763','Thalamostriate vein, subependymal veins      ')\g
INSERT INTO Lokalisation VALUES ('1764','Basal vein of Rosenthal      ')\g
INSERT INTO Lokalisation VALUES ('1765','Vein of Galen      ')\g
INSERT INTO Lokalisation VALUES ('1766','Superficial veins      ')\g
INSERT INTO Lokalisation VALUES ('1767','Dural sinus      ')\g
INSERT INTO Lokalisation VALUES ('1768','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1769','Other      ')\g
INSERT INTO Lokalisation VALUES ('177',' Infratentorial vein and dural sinus      ')\g
INSERT INTO Lokalisation VALUES ('1771','Precentral cerebellar vein      ')\g
INSERT INTO Lokalisation VALUES ('1772','Anterior pontomesencephalic vein      ')\g
INSERT INTO Lokalisation VALUES ('1773','Lateral and posterior mesencephalic veins      ')\g
INSERT INTO Lokalisation VALUES ('1774','Superior, inferior vermian vein      ')\g
INSERT INTO Lokalisation VALUES ('1775','Petrosal veins      ')\g
INSERT INTO Lokalisation VALUES ('1776','Dural sinus      ')\g
INSERT INTO Lokalisation VALUES ('1778','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('1779','Other      ')\g
INSERT INTO Lokalisation VALUES ('178',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('179',' Other      ')\g
INSERT INTO Lokalisation VALUES ('18','  MORE THAN ONE OF THE ABOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('19','  OTHER      ')\g
INSERT INTO Lokalisation VALUES ('2','   Face, Mastoids and Neck  exclude: cervical spine (31.), cervical esophagus (711., 712.)      ')\g
INSERT INTO Lokalisation VALUES ('20','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('21','  EAR, TEMPORAL BONE (see also skull 1.)      ')\g
INSERT INTO Lokalisation VALUES ('211',' External ear      ')\g
INSERT INTO Lokalisation VALUES ('2111','Pinna      ')\g
INSERT INTO Lokalisation VALUES ('2112','External auditory canal      ')\g
INSERT INTO Lokalisation VALUES ('2118','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('2119','Other      ')\g
INSERT INTO Lokalisation VALUES ('212',' Tympanic portion, middle ear      ')\g
INSERT INTO Lokalisation VALUES ('2121','Ossicle      ')\g
INSERT INTO Lokalisation VALUES ('2122','Attic      ')\g
INSERT INTO Lokalisation VALUES ('2123','Facial nerve canal      ')\g
INSERT INTO Lokalisation VALUES ('2124','Oval window      ')\g
INSERT INTO Lokalisation VALUES ('2125','Round window      ')\g
INSERT INTO Lokalisation VALUES ('2128','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('2129','Other   include: temporal ring      ')\g
INSERT INTO Lokalisation VALUES ('213',' Petrous portion, inner ear      ')\g
INSERT INTO Lokalisation VALUES ('2131','Semicircular canal      ')\g
INSERT INTO Lokalisation VALUES ('2132','Vestibule      ')\g
INSERT INTO Lokalisation VALUES ('2133','Cochlea      ')\g
INSERT INTO Lokalisation VALUES ('2134','Internal auditory canal      ')\g
INSERT INTO Lokalisation VALUES ('2135','Jugular fossa      ')\g
INSERT INTO Lokalisation VALUES ('2136','Bony petrous pyramid      ')\g
INSERT INTO Lokalisation VALUES ('2137','Eustachian tube      ')\g
INSERT INTO Lokalisation VALUES ('2138','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('2139','Other      ')\g
INSERT INTO Lokalisation VALUES ('214',' Mastoid portion      ')\g
INSERT INTO Lokalisation VALUES ('2141','Antrum      ')\g
INSERT INTO Lokalisation VALUES ('2142','Air cells      ')\g
INSERT INTO Lokalisation VALUES ('2148','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('2149','Other      ')\g
INSERT INTO Lokalisation VALUES ('215',' Temporal squamosa      ')\g
INSERT INTO Lokalisation VALUES ('216',' Zygomatic portion   include: styloid process      ')\g
INSERT INTO Lokalisation VALUES ('218',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('219',' Other      ')\g
INSERT INTO Lokalisation VALUES ('22','  ORBIT (see also skull 121. and 124.)      ')\g
INSERT INTO Lokalisation VALUES ('221',' Bony structures      ')\g
INSERT INTO Lokalisation VALUES ('222',' Optic canal      ')\g
INSERT INTO Lokalisation VALUES ('223',' Lacrimal apparatus      ')\g
INSERT INTO Lokalisation VALUES ('224',' Eyeball, other soft tissue      ')\g
INSERT INTO Lokalisation VALUES ('2241','Cornea      ')\g
INSERT INTO Lokalisation VALUES ('2242','Aqueous      ')\g
INSERT INTO Lokalisation VALUES ('2243','Lens      ')\g
INSERT INTO Lokalisation VALUES ('2244','Vitreous      ')\g
INSERT INTO Lokalisation VALUES ('2245','Retina      ')\g
INSERT INTO Lokalisation VALUES ('2246','Extraocular muscles      ')\g
INSERT INTO Lokalisation VALUES ('2247','Periorbital fat      ')\g
INSERT INTO Lokalisation VALUES ('2248','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('2249','Other      ')\g
INSERT INTO Lokalisation VALUES ('225',' Superior orbital fissure      ')\g
INSERT INTO Lokalisation VALUES ('228',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('229',' Other      ')\g
INSERT INTO Lokalisation VALUES ('23','  PARANASAL SINUS      ')\g
INSERT INTO Lokalisation VALUES ('231',' Frontal      ')\g
INSERT INTO Lokalisation VALUES ('232',' Ethmoid      ')\g
INSERT INTO Lokalisation VALUES ('233',' Maxillary      ')\g
INSERT INTO Lokalisation VALUES ('234',' Sphenoid      ')\g
INSERT INTO Lokalisation VALUES ('238',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('239',' Other      ')\g
INSERT INTO Lokalisation VALUES ('24','  MIDFACE, MANDIBLE, TEMPOROMANDIBULAR JOINT      ')\g
INSERT INTO Lokalisation VALUES ('241',' Maxillary bone      ')\g
INSERT INTO Lokalisation VALUES ('242',' Zygomatic bone      ')\g
INSERT INTO Lokalisation VALUES ('243',' Mandible      ')\g
INSERT INTO Lokalisation VALUES ('244',' Temporomandibular joint      ')\g
INSERT INTO Lokalisation VALUES ('248',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('249',' Other   exclude: nasal bone (261.), orbit (22.), paranasal sinus (23.)      ')\g
INSERT INTO Lokalisation VALUES ('25','  TEETH      ')\g
INSERT INTO Lokalisation VALUES ('26','  NOSE, NASOPHARYNX, MOUTH, OROPHARYNX, SALIVARY GLAND      ')\g
INSERT INTO Lokalisation VALUES ('261',' Nose   include: nasal bone, septum, turbinate, choana      ')\g
INSERT INTO Lokalisation VALUES ('262',' Mouth, oropharynx   include: tongue, uvula, tonsil, palate   exclude: teeth (25.), laryngopharynx (272.)      ')\g
INSERT INTO Lokalisation VALUES ('263',' Nasopharynx, adenoids      ')\g
INSERT INTO Lokalisation VALUES ('264',' Salivary gland      ')\g
INSERT INTO Lokalisation VALUES ('2641','Parotid      ')\g
INSERT INTO Lokalisation VALUES ('2642','Submandibular      ')\g
INSERT INTO Lokalisation VALUES ('2643','Minor      ')\g
INSERT INTO Lokalisation VALUES ('2644','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('2649','Other      ')\g
INSERT INTO Lokalisation VALUES ('268',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('269',' Other   exclude: laryngopharynx (272.)      ')\g
INSERT INTO Lokalisation VALUES ('27','  LARYNX AND LARYNGOPHARYNX, SOFT TISSUES OF NECK      ')\g
INSERT INTO Lokalisation VALUES ('271',' Larynx      ')\g
INSERT INTO Lokalisation VALUES ('2711','Epiglottic      ')\g
INSERT INTO Lokalisation VALUES ('2712','Aryepiglottic fold, arytenoid      ')\g
INSERT INTO Lokalisation VALUES ('2713','Vestibule      ')\g
INSERT INTO Lokalisation VALUES ('2714','True vocal cord      ')\g
INSERT INTO Lokalisation VALUES ('2715','Ventricle      ')\g
INSERT INTO Lokalisation VALUES ('2716','False vocal cord      ')\g
INSERT INTO Lokalisation VALUES ('2717','Subglottic portion of larynx      ')\g
INSERT INTO Lokalisation VALUES ('2718','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('2719','Other      ')\g
INSERT INTO Lokalisation VALUES ('272',' Laryngopharynx (hypopharynx)   include: piriform sinus      ')\g
INSERT INTO Lokalisation VALUES ('273',' Thyroid gland      ')\g
INSERT INTO Lokalisation VALUES ('274',' Parathyroid      ')\g
INSERT INTO Lokalisation VALUES ('275',' Trachea, cervical      ')\g
INSERT INTO Lokalisation VALUES ('276',' Other soft tissue of neck   include: lymph nodes      ')\g
INSERT INTO Lokalisation VALUES ('278',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('279',' Other      ')\g
INSERT INTO Lokalisation VALUES ('28','  MORE THAN ONE OF ABOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('29','  OTHER      ')\g
INSERT INTO Lokalisation VALUES ('3','   Spine and Contents include: contiguous soft tissue   exclude: mediastinum (67.), neck (27.), retroperitoneum (81.), pelvis (.875)      ')\g
INSERT INTO Lokalisation VALUES ('30','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('31','  CERVICAL SPINE   exclude: cervical spinal canal and  contents (34.)      ')\g
INSERT INTO Lokalisation VALUES ('311',' Vertebral body   include: dens, odontoid process      ')\g
INSERT INTO Lokalisation VALUES ('312',' Pedicle, lamina      ')\g
INSERT INTO Lokalisation VALUES ('313',' Transverse, spinous process      ')\g
INSERT INTO Lokalisation VALUES ('314',' Apophyseal, Luschka joint      ')\g
INSERT INTO Lokalisation VALUES ('315',' Intervertebral foramen      ')\g
INSERT INTO Lokalisation VALUES ('316',' Intervertebral disk      ')\g
INSERT INTO Lokalisation VALUES ('318',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('319',' Other   include: paravertebral soft tissues      ')\g
INSERT INTO Lokalisation VALUES ('32','  THORACIC SPINE   exclude: thoracic spinal canal and contents (35.)      ')\g
INSERT INTO Lokalisation VALUES ('321',' Vertebral body      ')\g
INSERT INTO Lokalisation VALUES ('322',' Pedicle, lamina      ')\g
INSERT INTO Lokalisation VALUES ('323',' Transverse, spinous process      ')\g
INSERT INTO Lokalisation VALUES ('324',' Apophyseal, costovertebral joint      ')\g
INSERT INTO Lokalisation VALUES ('325',' Intervertebral foramen      ')\g
INSERT INTO Lokalisation VALUES ('326',' Intervertebral disk      ')\g
INSERT INTO Lokalisation VALUES ('328',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('329',' Other   include: paravertebral soft tissues      ')\g
INSERT INTO Lokalisation VALUES ('33','  LUMBOSACRAL SPINE, COCCYX   exclude: lumbosacral spinal canal and contents (36.)      ')\g
INSERT INTO Lokalisation VALUES ('331',' Vertebral body      ')\g
INSERT INTO Lokalisation VALUES ('332',' Pedicle, lamina      ')\g
INSERT INTO Lokalisation VALUES ('333',' Transverse, spinous process      ')\g
INSERT INTO Lokalisation VALUES ('334',' Apophyseal joint      ')\g
INSERT INTO Lokalisation VALUES ('335',' Intervertebral foramen      ')\g
INSERT INTO Lokalisation VALUES ('336',' Intervertebral disk      ')\g
INSERT INTO Lokalisation VALUES ('337',' Sacroiliac joint      ')\g
INSERT INTO Lokalisation VALUES ('338',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('339',' Other   include: paravertebral soft tissues      ')\g
INSERT INTO Lokalisation VALUES ('34','  CERVICAL SPINAL CANAL AND CONTENTS      ')\g
INSERT INTO Lokalisation VALUES ('341',' Spinal cord      ')\g
INSERT INTO Lokalisation VALUES ('342',' Subdural space      ')\g
INSERT INTO Lokalisation VALUES ('343',' Extradural space      ')\g
INSERT INTO Lokalisation VALUES ('348',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('349',' Other      ')\g
INSERT INTO Lokalisation VALUES ('35','  THORACIC SPINAL CANAL AND CONTENTS      ')\g
INSERT INTO Lokalisation VALUES ('351',' Spinal cord      ')\g
INSERT INTO Lokalisation VALUES ('352',' Subdural space      ')\g
INSERT INTO Lokalisation VALUES ('353',' Extradural space      ')\g
INSERT INTO Lokalisation VALUES ('358',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('359',' Other      ')\g
INSERT INTO Lokalisation VALUES ('36','  LUMBOSACRAL SPINAL CANAL AND CONTENTS      ')\g
INSERT INTO Lokalisation VALUES ('361',' Conus medullaris      ')\g
INSERT INTO Lokalisation VALUES ('362',' Subdural space      ')\g
INSERT INTO Lokalisation VALUES ('363',' Extradural space      ')\g
INSERT INTO Lokalisation VALUES ('364',' Cauda equina      ')\g
INSERT INTO Lokalisation VALUES ('365',' Filum terminale      ')\g
INSERT INTO Lokalisation VALUES ('368',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('369',' Other      ')\g
INSERT INTO Lokalisation VALUES ('37','  ARTERIES AND VEINS OF SPINE AND SPINAL CORD      ')\g
INSERT INTO Lokalisation VALUES ('371',' Anterior spinal artery      ')\g
INSERT INTO Lokalisation VALUES ('372',' Posterolateral spinal artery      ')\g
INSERT INTO Lokalisation VALUES ('373',' Arterioradicularis medullara magna (Adamkiewicz) (thegreat artery to the lumbar enlargement)      ')\g
INSERT INTO Lokalisation VALUES ('374',' Anterior radicular medullary artery      ')\g
INSERT INTO Lokalisation VALUES ('375',' Posterior radicular medullary artery      ')\g
INSERT INTO Lokalisation VALUES ('376',' Anastomotic arterial loop of the conus  include: vessel of the filum terminale      ')\g
INSERT INTO Lokalisation VALUES ('377',' Spinal cord vein      ')\g
INSERT INTO Lokalisation VALUES ('378',' Epidural venous plexus (Batson)      ')\g
INSERT INTO Lokalisation VALUES ('379',' Other   include: dural vessels, vessel of the vertebra, muscular artery      ')\g
INSERT INTO Lokalisation VALUES ('38','  MORE THAN ONE OF THE ABOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('39','  OTHER      ')\g
INSERT INTO Lokalisation VALUES ('4','   Skeletal System include: contiguous soft tissues    exclude: skull (1.), face (2.), spine (3.)      ')\g
INSERT INTO Lokalisation VALUES ('40','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('41','  SHOULDER GIRDLE AND ARM      ')\g
INSERT INTO Lokalisation VALUES ('411',' Clavicle      ')\g
INSERT INTO Lokalisation VALUES ('412',' Scapula      ')\g
INSERT INTO Lokalisation VALUES ('413',' Acromioclavicular joint      ')\g
INSERT INTO Lokalisation VALUES ('414',' Shoulder joint      ')\g
INSERT INTO Lokalisation VALUES ('415',' Proximal end of humerus      ')\g
INSERT INTO Lokalisation VALUES ('416',' Shaft of humerus      ')\g
INSERT INTO Lokalisation VALUES ('418',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('419',' Other      ')\g
INSERT INTO Lokalisation VALUES ('42','  ELBOW AND FOREARM      ')\g
INSERT INTO Lokalisation VALUES ('421',' Distal end of humerus      ')\g
INSERT INTO Lokalisation VALUES ('422',' Elbow joint      ')\g
INSERT INTO Lokalisation VALUES ('423',' Proximal end of ulna      ')\g
INSERT INTO Lokalisation VALUES ('424',' Proximal end of radius      ')\g
INSERT INTO Lokalisation VALUES ('425',' Proximal radioulnar joint      ')\g
INSERT INTO Lokalisation VALUES ('426',' Shaft of ulna      ')\g
INSERT INTO Lokalisation VALUES ('427',' Shaft of radius      ')\g
INSERT INTO Lokalisation VALUES ('428',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('429',' Other      ')\g
INSERT INTO Lokalisation VALUES ('43','  WRIST AND HAND      ')\g
INSERT INTO Lokalisation VALUES ('431',' Distal end of radius      ')\g
INSERT INTO Lokalisation VALUES ('432',' Distal end of ulna      ')\g
INSERT INTO Lokalisation VALUES ('433',' Carpal bone      ')\g
INSERT INTO Lokalisation VALUES ('4330','Pisiform      ')\g
INSERT INTO Lokalisation VALUES ('4331','Scaphoid      ')\g
INSERT INTO Lokalisation VALUES ('4332','Lunate      ')\g
INSERT INTO Lokalisation VALUES ('4333','Triangular (triquetrum)      ')\g
INSERT INTO Lokalisation VALUES ('4334','Trapezium (greater multangular)      ')\g
INSERT INTO Lokalisation VALUES ('4335','Trapezoid (lesser multangular)      ')\g
INSERT INTO Lokalisation VALUES ('4336','Capitate      ')\g
INSERT INTO Lokalisation VALUES ('4337','Hamate      ')\g
INSERT INTO Lokalisation VALUES ('4338','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('4339','Other    exclude: accessory ossicle (.131)      ')\g
INSERT INTO Lokalisation VALUES ('434',' Joint of wrist      ')\g
INSERT INTO Lokalisation VALUES ('435',' Metacarpal      ')\g
INSERT INTO Lokalisation VALUES ('436',' Phalanx      ')\g
INSERT INTO Lokalisation VALUES ('437',' Metacarpophalangeal joint, interphalangeal joint      ')\g
INSERT INTO Lokalisation VALUES ('438',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('439',' Other      ')\g
INSERT INTO Lokalisation VALUES ('44','  PELVIS, HIP JOINT, THIGH    exclude: sacrum (33.)      ')\g
INSERT INTO Lokalisation VALUES ('441',' Hip bone      ')\g
INSERT INTO Lokalisation VALUES ('4411','Ilium      ')\g
INSERT INTO Lokalisation VALUES ('4412','Ischium      ')\g
INSERT INTO Lokalisation VALUES ('4413','Pubis      ')\g
INSERT INTO Lokalisation VALUES ('4414','Symphysis pubis      ')\g
INSERT INTO Lokalisation VALUES ('4419','Other      ')\g
INSERT INTO Lokalisation VALUES ('442',' Hip joint     include: acetabulum      ')\g
INSERT INTO Lokalisation VALUES ('443',' Proximal end of femur      ')\g
INSERT INTO Lokalisation VALUES ('444',' Shaft of femur      ')\g
INSERT INTO Lokalisation VALUES ('448',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('449',' Other     include: trochanteric bursa      ')\g
INSERT INTO Lokalisation VALUES ('45','  KNEE AND LEG      ')\g
INSERT INTO Lokalisation VALUES ('451',' Distal end of femur      ')\g
INSERT INTO Lokalisation VALUES ('452',' Knee joint, related soft tissue structure      ')\g
INSERT INTO Lokalisation VALUES ('4521','Joint cartilage      ')\g
INSERT INTO Lokalisation VALUES ('4522','Joint capsule, synovium      ')\g
INSERT INTO Lokalisation VALUES ('4523','Suprapatellar bursa      ')\g
INSERT INTO Lokalisation VALUES ('4524','Medial meniscus      ')\g
INSERT INTO Lokalisation VALUES ('4525','Lateral meniscus      ')\g
INSERT INTO Lokalisation VALUES ('4526','Cruciate ligament      ')\g
INSERT INTO Lokalisation VALUES ('4527','Collateral ligament      ')\g
INSERT INTO Lokalisation VALUES ('4528','Patellar tendon      ')\g
INSERT INTO Lokalisation VALUES ('4529','Other      ')\g
INSERT INTO Lokalisation VALUES ('453',' Patella      ')\g
INSERT INTO Lokalisation VALUES ('454',' Proximal end of tibia      ')\g
INSERT INTO Lokalisation VALUES ('4541','Tibial tubercle      ')\g
INSERT INTO Lokalisation VALUES ('455',' Proximal end of fibula      ')\g
INSERT INTO Lokalisation VALUES ('456',' Shaft of tibia      ')\g
INSERT INTO Lokalisation VALUES ('457',' Shaft of fibula      ')\g
INSERT INTO Lokalisation VALUES ('458',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('459',' Other      ')\g
INSERT INTO Lokalisation VALUES ('46','  ANKLE AND FOOT      ')\g
INSERT INTO Lokalisation VALUES ('461',' Distal end of tibia      ')\g
INSERT INTO Lokalisation VALUES ('462',' Distal end of fibula      ')\g
INSERT INTO Lokalisation VALUES ('463',' Ankle joint, tarsal joint      ')\g
INSERT INTO Lokalisation VALUES ('464',' Tarsal bone      ')\g
INSERT INTO Lokalisation VALUES ('4641','Talus      ')\g
INSERT INTO Lokalisation VALUES ('4642','Calcaneus      ')\g
INSERT INTO Lokalisation VALUES ('4643','Navicular      ')\g
INSERT INTO Lokalisation VALUES ('4644','Cuboid      ')\g
INSERT INTO Lokalisation VALUES ('4645','Cuneiform      ')\g
INSERT INTO Lokalisation VALUES ('4648','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('4649','Other      ')\g
INSERT INTO Lokalisation VALUES ('465',' Metatarsal      ')\g
INSERT INTO Lokalisation VALUES ('466',' Phalanx      ')\g
INSERT INTO Lokalisation VALUES ('467',' Metatarsophalangeal joint, interphalangeal joint      ')\g
INSERT INTO Lokalisation VALUES ('468',' More than one of the above, generalized      ')\g
INSERT INTO Lokalisation VALUES ('469',' Other      ')\g
INSERT INTO Lokalisation VALUES ('47','  CHEST WALL      ')\g
INSERT INTO Lokalisation VALUES ('471',' Rib      ')\g
INSERT INTO Lokalisation VALUES ('472',' Sternum      ')\g
INSERT INTO Lokalisation VALUES ('473',' Sternoclavicular joint      ')\g
INSERT INTO Lokalisation VALUES ('474',' Soft tissues      ')\g
INSERT INTO Lokalisation VALUES ('478',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('479',' Other      ')\g
INSERT INTO Lokalisation VALUES ('48','  MORE THAN ONE OF THE ABOVE, GENERALIZED    include: cartilage      ')\g
INSERT INTO Lokalisation VALUES ('49','  OTHER    include: skull      ')\g
INSERT INTO Lokalisation VALUES ('5','   Heart and Great Vessels exclude: abdominal and cervical vessels (9.)      ')\g
INSERT INTO Lokalisation VALUES ('51','  HEART    exclude: chamber (52.), valve (53.), pericardium (55.)      ')\g
INSERT INTO Lokalisation VALUES ('511',' Myocardium      ')\g
INSERT INTO Lokalisation VALUES ('512',' Endocardium      ')\g
INSERT INTO Lokalisation VALUES ('513',' Papillary muscle, chordae tendineae      ')\g
INSERT INTO Lokalisation VALUES ('514',' Atrial septum      ')\g
INSERT INTO Lokalisation VALUES ('515',' Ventricular septum      ')\g
INSERT INTO Lokalisation VALUES ('518',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('519',' Other      ')\g
INSERT INTO Lokalisation VALUES ('52','  CARDIAC CHAMBER      ')\g
INSERT INTO Lokalisation VALUES ('521',' Right atrium and appendage      ')\g
INSERT INTO Lokalisation VALUES ('522',' Left atrium and appendage      ')\g
INSERT INTO Lokalisation VALUES ('523',' Right ventricle      ')\g
INSERT INTO Lokalisation VALUES ('524',' Left ventricle      ')\g
INSERT INTO Lokalisation VALUES ('528',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('529',' Other      ')\g
INSERT INTO Lokalisation VALUES ('53','  CARDIAC VALVE      ')\g
INSERT INTO Lokalisation VALUES ('531',' Tricuspid valve      ')\g
INSERT INTO Lokalisation VALUES ('532',' Pulmonary infundibulum      ')\g
INSERT INTO Lokalisation VALUES ('533',' Pulmonary valve      ')\g
INSERT INTO Lokalisation VALUES ('534',' Mitral valve, annulus      ')\g
INSERT INTO Lokalisation VALUES ('535',' Aortic valve, annulus      ')\g
INSERT INTO Lokalisation VALUES ('536',' Thebesian      ')\g
INSERT INTO Lokalisation VALUES ('537',' Eustachian      ')\g
INSERT INTO Lokalisation VALUES ('538',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('539',' Other      ')\g
INSERT INTO Lokalisation VALUES ('54','  CORONARY VESSEL      ')\g
INSERT INTO Lokalisation VALUES ('541',' Right coronary artery      ')\g
INSERT INTO Lokalisation VALUES ('542',' Left coronary artery      ')\g
INSERT INTO Lokalisation VALUES ('543',' Circumflex artery      ')\g
INSERT INTO Lokalisation VALUES ('544',' Anterior descending artery      ')\g
INSERT INTO Lokalisation VALUES ('545',' Obtuse marginal artery      ')\g
INSERT INTO Lokalisation VALUES ('546',' Diagonal artery      ')\g
INSERT INTO Lokalisation VALUES ('547',' Coronary sinus, cardiac veins      ')\g
INSERT INTO Lokalisation VALUES ('548',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('549',' Other      ')\g
INSERT INTO Lokalisation VALUES ('55','  PERICARDIUM    include: pericardial sac      ')\g
INSERT INTO Lokalisation VALUES ('56','  ARTERIES, VEINS OF THE THORAX (see also 60., 94.)      ')\g
INSERT INTO Lokalisation VALUES ('562',' Aortic arch    include: innominate artery, left  common carotid artery (thoracic portion), subclavian artery      ')\g
INSERT INTO Lokalisation VALUES ('563',' Descending aorta    include: bronchial arteries      ')\g
INSERT INTO Lokalisation VALUES ('564',' Pulmonary artery      ')\g
INSERT INTO Lokalisation VALUES ('565',' Pulmonary vein      ')\g
INSERT INTO Lokalisation VALUES ('566',' Superior vena cava      ')\g
INSERT INTO Lokalisation VALUES ('567',' Azygos vein, hemiazygos vein      ')\g
INSERT INTO Lokalisation VALUES ('568',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('569',' Other    include: inferior vena cava (thoracic portion)   exclude: chest wall, diaphragmatic and thymic vessels (949.)      ')\g
INSERT INTO Lokalisation VALUES ('57','  BLOOD      ')\g
INSERT INTO Lokalisation VALUES ('58','  MORE THAN ONE OF THE ABOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('59','  OTHER      ')\g
INSERT INTO Lokalisation VALUES ('6','   Lung, Mediastinum, and Pleura exclude: ribs (471.), sternum (472.), chest wall (474.), heart and great vessels (5.), esophagus (71.)      ')\g
INSERT INTO Lokalisation VALUES ('60','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('61','  RIGHT UPPER LOBE      ')\g
INSERT INTO Lokalisation VALUES ('62','  RIGHT MIDDLE LOBE      ')\g
INSERT INTO Lokalisation VALUES ('63','  RIGHT LOWER LOBE      ')\g
INSERT INTO Lokalisation VALUES ('64','  LEFT UPPER LOBE      ')\g
INSERT INTO Lokalisation VALUES ('641',' Apical posterior and anterior segments      ')\g
INSERT INTO Lokalisation VALUES ('642',' Lingula      ')\g
INSERT INTO Lokalisation VALUES ('65','  LEFT LOWER LOBE      ')\g
INSERT INTO Lokalisation VALUES ('66','  PLEURA    include: superior surface of the diaphragm      ')\g
INSERT INTO Lokalisation VALUES ('67','  MEDIASTINUM      ')\g
INSERT INTO Lokalisation VALUES ('671',' Trachea, right and left main bronchi      ')\g
INSERT INTO Lokalisation VALUES ('672',' Superior mediastinum      ')\g
INSERT INTO Lokalisation VALUES ('673',' Anterior mediastinum      ')\g
INSERT INTO Lokalisation VALUES ('674',' Middle mediastinum      ')\g
INSERT INTO Lokalisation VALUES ('675',' Thymus      ')\g
INSERT INTO Lokalisation VALUES ('677',' Hilar lymph node      ')\g
INSERT INTO Lokalisation VALUES ('678',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('679',' Other    include: other mediastinal lymph nodes      ')\g
INSERT INTO Lokalisation VALUES ('68','  MORE THAN ONE OF THF THOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('69','  OTHER      ')\g
INSERT INTO Lokalisation VALUES ('7','   Gastrointestinal System include: inferior surface of the diaphragm, intraperitoneal struc-tures, abdominal wall   exclude: retroperitoneal and pelvic structures (8.) except pancreas (77.)      ')\g
INSERT INTO Lokalisation VALUES ('70','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('71','  ESOPHAGUS      ')\g
INSERT INTO Lokalisation VALUES ('711',' Pharyngoesophageal sphincter      ')\g
INSERT INTO Lokalisation VALUES ('712',' Upper third      ')\g
INSERT INTO Lokalisation VALUES ('713',' Middle third      ')\g
INSERT INTO Lokalisation VALUES ('714',' Lower third      ')\g
INSERT INTO Lokalisation VALUES ('715',' Lower esophageal sphincter      ')\g
INSERT INTO Lokalisation VALUES ('716',' Gastroesophageal junction      ')\g
INSERT INTO Lokalisation VALUES ('718',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('719',' Other    exclude: pharynx (26., 27.)      ')\g
INSERT INTO Lokalisation VALUES ('72','  STOMACH      ')\g
INSERT INTO Lokalisation VALUES ('721',' Fundus      ')\g
INSERT INTO Lokalisation VALUES ('722',' Body      ')\g
INSERT INTO Lokalisation VALUES ('723',' Antrum      ')\g
INSERT INTO Lokalisation VALUES ('724',' Pylorus      ')\g
INSERT INTO Lokalisation VALUES ('725',' Lesser curvature      ')\g
INSERT INTO Lokalisation VALUES ('726',' Greater curvature      ')\g
INSERT INTO Lokalisation VALUES ('728',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('729',' Other      ')\g
INSERT INTO Lokalisation VALUES ('73','  DUODENUM      ')\g
INSERT INTO Lokalisation VALUES ('731',' First portion, bulb      ')\g
INSERT INTO Lokalisation VALUES ('732',' Second portion (descending)      ')\g
INSERT INTO Lokalisation VALUES ('733',' Third portion (horizontal)      ')\g
INSERT INTO Lokalisation VALUES ('734',' Fourth portion (ascending)      ')\g
INSERT INTO Lokalisation VALUES ('735',' Duodenojejunal junction      ')\g
INSERT INTO Lokalisation VALUES ('738',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('739',' Other      ')\g
INSERT INTO Lokalisation VALUES ('74','  SMALL INTESTINE    exclude: duodenum (73.)      ')\g
INSERT INTO Lokalisation VALUES ('741',' Jejunum      ')\g
INSERT INTO Lokalisation VALUES ('742',' Ileum      ')\g
INSERT INTO Lokalisation VALUES ('748',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('749',' Other      ')\g
INSERT INTO Lokalisation VALUES ('75','  COLON      ')\g
INSERT INTO Lokalisation VALUES ('751',' Appendix      ')\g
INSERT INTO Lokalisation VALUES ('752',' Cecum    include: ileocecal valve      ')\g
INSERT INTO Lokalisation VALUES ('753',' Ascending colon    include: hepatic flexure      ')\g
INSERT INTO Lokalisation VALUES ('754',' Transverse colon    include: splenic flexure      ')\g
INSERT INTO Lokalisation VALUES ('755',' Descending colon      ')\g
INSERT INTO Lokalisation VALUES ('756',' Sigmoid colon, rectosigmoid colon      ')\g
INSERT INTO Lokalisation VALUES ('757',' Rectum, anus      ')\g
INSERT INTO Lokalisation VALUES ('758',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('759',' Other      ')\g
INSERT INTO Lokalisation VALUES ('76','  LIVER, BILIARY SYSTEM      ')\g
INSERT INTO Lokalisation VALUES ('761',' Liver      ')\g
INSERT INTO Lokalisation VALUES ('762',' Gallbladder      ')\g
INSERT INTO Lokalisation VALUES ('763',' Cystic duct      ')\g
INSERT INTO Lokalisation VALUES ('764',' Common hepatic duct      ')\g
INSERT INTO Lokalisation VALUES ('765',' Intrahepatic duct      ')\g
INSERT INTO Lokalisation VALUES ('766',' Common duct      ')\g
INSERT INTO Lokalisation VALUES ('767',' Papilla, ampulla of Vater      ')\g
INSERT INTO Lokalisation VALUES ('768',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('769',' Other      ')\g
INSERT INTO Lokalisation VALUES ('77','  PANCREAS, SPLEEN      ')\g
INSERT INTO Lokalisation VALUES ('770',' Pancreas      ')\g
INSERT INTO Lokalisation VALUES ('771',' Head of pancreas      ')\g
INSERT INTO Lokalisation VALUES ('772',' Body of pancreas      ')\g
INSERT INTO Lokalisation VALUES ('773',' Tail of pancreas      ')\g
INSERT INTO Lokalisation VALUES ('774',' Pancreatic ducts      ')\g
INSERT INTO Lokalisation VALUES ('775',' Spleen      ')\g
INSERT INTO Lokalisation VALUES ('778',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('779',' Other      ')\g
INSERT INTO Lokalisation VALUES ('78','  MORE THAN ONE OF THE ABOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('79','  OTHER      ')\g
INSERT INTO Lokalisation VALUES ('791',' Peritoneal cavity      ')\g
INSERT INTO Lokalisation VALUES ('792',' Mesentery      ')\g
INSERT INTO Lokalisation VALUES ('793',' Supramesocolic intraperitoneal recess      ')\g
INSERT INTO Lokalisation VALUES ('7931','Right subphrenic      ')\g
INSERT INTO Lokalisation VALUES ('7932','Right anterior subhepatic      ')\g
INSERT INTO Lokalisation VALUES ('7933','Right posterior subhepatic (Morison pouch)      ')\g
INSERT INTO Lokalisation VALUES ('7934','Left perihepatic   include: left subphrenic and perisplenic      ')\g
INSERT INTO Lokalisation VALUES ('7935','Lesser peritoneal sac (omental bursa)      ')\g
INSERT INTO Lokalisation VALUES ('7938','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('7939','Other      ')\g
INSERT INTO Lokalisation VALUES ('794',' Inframesocolic intraperitoneal recess      ')\g
INSERT INTO Lokalisation VALUES ('7941','Paracolic gutter      ')\g
INSERT INTO Lokalisation VALUES ('7942','Right infracolic space      ')\g
INSERT INTO Lokalisation VALUES ('7943','Left infracolic space      ')\g
INSERT INTO Lokalisation VALUES ('7948','More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('7949','Other      ')\g
INSERT INTO Lokalisation VALUES ('795',' Inferior surface of diaphragm      ')\g
INSERT INTO Lokalisation VALUES ('796',' Abdominal wall   exclude: retroperitoneal structure (8.)      ')\g
INSERT INTO Lokalisation VALUES ('798',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('799',' Other      ')\g
INSERT INTO Lokalisation VALUES ('8','   Genitourinary System include: retroperitoneal and pelvic structures    exclude: pancreas (77.)      ')\g
INSERT INTO Lokalisation VALUES ('80','  LOCATION NOT SPECIFIED OR UNKNOWN      ')\g
INSERT INTO Lokalisation VALUES ('81','  KIDNEY      ')\g
INSERT INTO Lokalisation VALUES ('811',' Renal parenchyma      ')\g
INSERT INTO Lokalisation VALUES ('812',' Calyx      ')\g
INSERT INTO Lokalisation VALUES ('813',' Pelvis      ')\g
INSERT INTO Lokalisation VALUES ('814',' Renal sinus      ')\g
INSERT INTO Lokalisation VALUES ('815',' Capsule      ')\g
INSERT INTO Lokalisation VALUES ('818',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('819',' Other      ')\g
INSERT INTO Lokalisation VALUES ('82','  URETER      ')\g
INSERT INTO Lokalisation VALUES ('821',' Ureteropelvic junction      ')\g
INSERT INTO Lokalisation VALUES ('822',' Upper third      ')\g
INSERT INTO Lokalisation VALUES ('823',' Middle third      ')\g
INSERT INTO Lokalisation VALUES ('824',' Lower third      ')\g
INSERT INTO Lokalisation VALUES ('825',' Ureterovesical junction      ')\g
INSERT INTO Lokalisation VALUES ('828',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('829',' Other      ')\g
INSERT INTO Lokalisation VALUES ('83','  BLADDER      ')\g
INSERT INTO Lokalisation VALUES ('831',' Dome      ')\g
INSERT INTO Lokalisation VALUES ('832',' Trigone      ')\g
INSERT INTO Lokalisation VALUES ('833',' Vesicourethral junction (bladder neck)      ')\g
INSERT INTO Lokalisation VALUES ('838',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('839',' Other   include: urachus      ')\g
INSERT INTO Lokalisation VALUES ('84','  MALE URETHRA, GENITALIA      ')\g
INSERT INTO Lokalisation VALUES ('841',' Prostatic urethra      ')\g
INSERT INTO Lokalisation VALUES ('842',' Membranous urethra      ')\g
INSERT INTO Lokalisation VALUES ('843',' Cavernous urethra    include: bulbous urethra, meatus      ')\g
INSERT INTO Lokalisation VALUES ('844',' Prostate      ')\g
INSERT INTO Lokalisation VALUES ('845',' Seminal vesicle      ')\g
INSERT INTO Lokalisation VALUES ('846',' Vas deferens      ')\g
INSERT INTO Lokalisation VALUES ('847',' External genitalia      ')\g
INSERT INTO Lokalisation VALUES ('848',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('849',' Other      ')\g
INSERT INTO Lokalisation VALUES ('85','  FEMALE URETHRA, GENITALIA, FETUS, PLACENTA      ')\g
INSERT INTO Lokalisation VALUES ('851',' Urethra      ')\g
INSERT INTO Lokalisation VALUES ('852',' Ovary      ')\g
INSERT INTO Lokalisation VALUES ('853',' Fallopian tube      ')\g
INSERT INTO Lokalisation VALUES ('854',' Uterus      ')\g
INSERT INTO Lokalisation VALUES ('855',' Vagina      ')\g
INSERT INTO Lokalisation VALUES ('856',' Fetus-amniotic sac, gestational sac      ')\g
INSERT INTO Lokalisation VALUES ('857',' Placenta      ')\g
INSERT INTO Lokalisation VALUES ('858',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('859',' Other      ')\g
INSERT INTO Lokalisation VALUES ('86','  ADRENAL      ')\g
INSERT INTO Lokalisation VALUES ('861',' Medulla      ')\g
INSERT INTO Lokalisation VALUES ('862',' Cortex      ')\g
INSERT INTO Lokalisation VALUES ('87','  EXTRAPERITONEAL SPACE      ')\g
INSERT INTO Lokalisation VALUES ('871',' Anterior pararenal      ')\g
INSERT INTO Lokalisation VALUES ('872',' Perirenal      ')\g
INSERT INTO Lokalisation VALUES ('873',' Posterior pararenal      ')\g
INSERT INTO Lokalisation VALUES ('874',' Iliac      ')\g
INSERT INTO Lokalisation VALUES ('875',' Pelvic      ')\g
INSERT INTO Lokalisation VALUES ('876',' Retrofascial      ')\g
INSERT INTO Lokalisation VALUES ('878',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('879',' Other      ')\g
INSERT INTO Lokalisation VALUES ('88','  MORE THAN ONE OF THE ABOVE, GENERALIZED      ')\g
INSERT INTO Lokalisation VALUES ('89','  OTHER   include: abdominal aorta, extraperitoneal vessels.  Also see 9. for diagnosis made by angiography      ')\g
INSERT INTO Lokalisation VALUES ('9','   Vascular and Lymphatic System  exclude: heart (5.), vessels of head (17.), vessels of spine (37.)      ')\g
INSERT INTO Lokalisation VALUES ('90','  ARTERIES, VEINS OF NECK      ')\g
INSERT INTO Lokalisation VALUES ('901',' Vertebral artery (cervical portion)      ')\g
INSERT INTO Lokalisation VALUES ('902',' Thyrocervical trunk and branches      ')\g
INSERT INTO Lokalisation VALUES ('903',' Costocervical trunk      ')\g
INSERT INTO Lokalisation VALUES ('904',' Common carotid artery (cervical portion)   exclude: internal carotid artery (172.)      ')\g
INSERT INTO Lokalisation VALUES ('905',' External carotid artery      ')\g
INSERT INTO Lokalisation VALUES ('906',' Superior thyroid artery      ')\g
INSERT INTO Lokalisation VALUES ('907',' Veins of the neck      ')\g
INSERT INTO Lokalisation VALUES ('908',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('909',' Other      ')\g
INSERT INTO Lokalisation VALUES ('91','  ARTERIES, VEINS OF UPPER LIMB      ')\g
INSERT INTO Lokalisation VALUES ('911',' Axillary artery      ')\g
INSERT INTO Lokalisation VALUES ('912',' Brachial artery      ')\g
INSERT INTO Lokalisation VALUES ('913',' Arteries of forearm      ')\g
INSERT INTO Lokalisation VALUES ('914',' Arteries of hand      ')\g
INSERT INTO Lokalisation VALUES ('915',' Axillary vein      ')\g
INSERT INTO Lokalisation VALUES ('916',' Veins of arm, forearm, hand      ')\g
INSERT INTO Lokalisation VALUES ('918',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('919',' Other      ')\g
INSERT INTO Lokalisation VALUES ('92','  ARTERIES OF LOWER LIMB      ')\g
INSERT INTO Lokalisation VALUES ('921',' Common femoral artery      ')\g
INSERT INTO Lokalisation VALUES ('922',' Superficial femoral artery      ')\g
INSERT INTO Lokalisation VALUES ('923',' Profunda femoral artery      ')\g
INSERT INTO Lokalisation VALUES ('924',' Popliteal artery      ')\g
INSERT INTO Lokalisation VALUES ('925',' Anterior tibial artery      ')\g
INSERT INTO Lokalisation VALUES ('926',' Posterior tibial artery      ')\g
INSERT INTO Lokalisation VALUES ('927',' Peroneal artery      ')\g
INSERT INTO Lokalisation VALUES ('928',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('929',' Other    include: arteries of foot      ')\g
INSERT INTO Lokalisation VALUES ('93','  VEINS OF LOWER LIMB      ')\g
INSERT INTO Lokalisation VALUES ('931',' Common femoral vein      ')\g
INSERT INTO Lokalisation VALUES ('932',' Femoral vein, deep system      ')\g
INSERT INTO Lokalisation VALUES ('933',' Superficial veins of thigh   include: saphenous      ')\g
INSERT INTO Lokalisation VALUES ('934',' Popliteal vein      ')\g
INSERT INTO Lokalisation VALUES ('935',' Deep venous system of leg (calf)      ')\g
INSERT INTO Lokalisation VALUES ('936',' Superficial venous system of leg (calf)      ')\g
INSERT INTO Lokalisation VALUES ('937',' Veins of foot      ')\g
INSERT INTO Lokalisation VALUES ('938',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('939',' Other      ')\g
INSERT INTO Lokalisation VALUES ('94','  ARTERIES, VEINS OF THORAX      ')\g
INSERT INTO Lokalisation VALUES ('941',' Ascending aorta      ')\g
INSERT INTO Lokalisation VALUES ('942',' Aortic arch    include:  innominate artery, common carotid artery (thoracic portion), subclavian artery      ')\g
INSERT INTO Lokalisation VALUES ('943',' Descending aorta    include: bronchial arteries      ')\g
INSERT INTO Lokalisation VALUES ('944',' Pulmonary artery      ')\g
INSERT INTO Lokalisation VALUES ('945',' Pulmonary vein      ')\g
INSERT INTO Lokalisation VALUES ('946',' Superior vena cava      ')\g
INSERT INTO Lokalisation VALUES ('9461','Innominate veins      ')\g
INSERT INTO Lokalisation VALUES ('9462','Subclavian veins      ')\g
INSERT INTO Lokalisation VALUES ('947',' Azygous vein, hemiazgous vein      ')\g
INSERT INTO Lokalisation VALUES ('948',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('949',' Other    include: inferior vena cava (thoracic portion), diaphragmatic vessels, veins of thymus, internal mammary vessels, intercostal vessels, other vessels of chest wall      ')\g
INSERT INTO Lokalisation VALUES ('95','  ARTERIES, VEINS OF GASTROINTESTINAL SYSTEM, DIAPHRAGM AND ABDOMINAL WALL      ')\g
INSERT INTO Lokalisation VALUES ('951',' Celiac artery      ')\g
INSERT INTO Lokalisation VALUES ('952',' Hepatic artery    include: gastroduodenal artery, pancreaticoduodenal arcades      ')\g
INSERT INTO Lokalisation VALUES ('953',' Left gastric artery, inferior phrenic artery, superior adrenal artery      ')\g
INSERT INTO Lokalisation VALUES ('954',' Splenic artery      ')\g
INSERT INTO Lokalisation VALUES ('955',' Superior mesenteric artery      ')\g
INSERT INTO Lokalisation VALUES ('956',' Inferior mesenteric artery      ')\g
INSERT INTO Lokalisation VALUES ('957',' Portal vein, splenic vein      ')\g
INSERT INTO Lokalisation VALUES ('958',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('959',' Other    include: umbilical vein, other veins of abdomen      ')\g
INSERT INTO Lokalisation VALUES ('96','  ARTERIES, VEINS OF GENITOURINARY SYSTEM      ')\g
INSERT INTO Lokalisation VALUES ('961',' Renal artery      ')\g
INSERT INTO Lokalisation VALUES ('962',' Dorsal branch, ventral branch      ')\g
INSERT INTO Lokalisation VALUES ('963',' Interlobar renal artery, arcuate arteries, interlobular arteries      ')\g
INSERT INTO Lokalisation VALUES ('964',' Ureteral artery      ')\g
INSERT INTO Lokalisation VALUES ('965',' Middle adrenal artery, inferior adrenal artery      ')\g
INSERT INTO Lokalisation VALUES ('966',' Renal veins, intrarenal veins      ')\g
INSERT INTO Lokalisation VALUES ('967',' Adrenal veins      ')\g
INSERT INTO Lokalisation VALUES ('968',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('969',' Other    include: gonadal veins      ')\g
INSERT INTO Lokalisation VALUES ('98','  ARTERIES, VEINS OF RETROPERITONEUM AND PELVIS   exclude: inferior phrenic artery (953.)      ')\g
INSERT INTO Lokalisation VALUES ('981',' Abdominal aorta      ')\g
INSERT INTO Lokalisation VALUES ('982',' Inferior vena cava      ')\g
INSERT INTO Lokalisation VALUES ('983',' Lumbar arteries      ')\g
INSERT INTO Lokalisation VALUES ('984',' Common iliac arteries      ')\g
INSERT INTO Lokalisation VALUES ('985',' Common iliac veins      ')\g
INSERT INTO Lokalisation VALUES ('986',' External, internal iliac arteries      ')\g
INSERT INTO Lokalisation VALUES ('987',' External, internal iliac veins      ')\g
INSERT INTO Lokalisation VALUES ('988',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('989',' Other    include: umbilical arteries      ')\g
INSERT INTO Lokalisation VALUES ('99','  LYMPHATIC VESSELS, LYMPH NODES (use with .8)      ')\g
INSERT INTO Lokalisation VALUES ('991',' Inguinal nodes      ')\g
INSERT INTO Lokalisation VALUES ('992',' Pelvic nodes      ')\g
INSERT INTO Lokalisation VALUES ('993',' Abdominal para-aortic nodes, lateral aortic nodes (direct drainage from testicle and ovary)      ')\g
INSERT INTO Lokalisation VALUES ('994',' Combination of inguinal, pelvic and abdominal nodes      ')\g
INSERT INTO Lokalisation VALUES ('995',' Thoracic duct, cisterna chyli      ')\g
INSERT INTO Lokalisation VALUES ('996',' Pulmonary hilar and paratracheal nodes      ')\g
INSERT INTO Lokalisation VALUES ('997',' Supraclavicular, cervical and axillary nodes      ')\g
INSERT INTO Lokalisation VALUES ('998',' More than one of the above      ')\g
INSERT INTO Lokalisation VALUES ('999',' Other    include: peripheral lymphatic channels      ')\g


INSERT INTO Diagnose VALUES ('01','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('011','     NORMAL MAMMOGRAM      ')\g
INSERT INTO Diagnose VALUES ('0111','    Film mammography      ')\g
INSERT INTO Diagnose VALUES ('0112','    Film-screen mammography      ')\g
INSERT INTO Diagnose VALUES ('0113','    Magnification      ')\g
INSERT INTO Diagnose VALUES ('0114','    Spot compression      ')\g
INSERT INTO Diagnose VALUES ('0115','    Angled view      ')\g
INSERT INTO Diagnose VALUES ('0119','    Other      ')\g
INSERT INTO Diagnose VALUES ('012','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION   exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('0121','    Digital technique      ')\g
INSERT INTO Diagnose VALUES ('01211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('01214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('012141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('0121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('0121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('0121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('0121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('0121415',' Specific resonance suppressed  include: fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('0121416',' High-speed imaging include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('0121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('0121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('012142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('012143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('012144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('012145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('012146','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('012147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('012149','  Other      ')\g
INSERT INTO Diagnose VALUES ('01215','   Digital radiography      ')\g
INSERT INTO Diagnose VALUES ('01216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('012161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('012162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('012163','  PET      ')\g
INSERT INTO Diagnose VALUES ('012164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('012165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('012166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('012167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('012168','  Therapeutic procedure, include use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('012169','  Other.  See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('01219','   Other      ')\g
INSERT INTO Diagnose VALUES ('0122','    Xeroradiography      ')\g
INSERT INTO Diagnose VALUES ('0123','    Thermography      ')\g
INSERT INTO Diagnose VALUES ('0125','    Lesion localization study      ')\g
INSERT INTO Diagnose VALUES ('0126','    Biopsy or interventional      ')\g
INSERT INTO Diagnose VALUES ('01261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('01262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('01267','   Operative procedure   include: stereotaxis (see also, intraoperative ultrasound .12982)      ')\g
INSERT INTO Diagnose VALUES ('01269','   Other      ')\g
INSERT INTO Diagnose VALUES ('0127','    Duct injection  include: pneumocystography      ')\g
INSERT INTO Diagnose VALUES ('0128','    Specimen radiography      ')\g
INSERT INTO Diagnose VALUES ('0129','    Other      ')\g
INSERT INTO Diagnose VALUES ('01291','   Diaphanography      ')\g
INSERT INTO Diagnose VALUES ('01292','   Heavy ion mammography      ')\g
INSERT INTO Diagnose VALUES ('01298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('012981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('012982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('012983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('012984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('012985','  Guided procedure for diagnosis    include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('012986','  Guided procedure for therapy    include: drainage      ')\g
INSERT INTO Diagnose VALUES ('012987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('012988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('012989','  Special nonroutine projection, special mode, or transducer type      ')\g
INSERT INTO Diagnose VALUES ('01299','   Other      ')\g
INSERT INTO Diagnose VALUES ('013','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('014','     CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('019','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('02','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('020','     INFECTION CLASSIFIED BY ORGANISM (see 6.20 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('021','     INFLAMMATION, ACUTE      ')\g
INSERT INTO Diagnose VALUES ('0211','    Cellulitis      ')\g
INSERT INTO Diagnose VALUES ('0212','    Abscess      ')\g
INSERT INTO Diagnose VALUES ('0219','    Other      ')\g
INSERT INTO Diagnose VALUES ('022','     INFLAMMATION, SUBACUTE OR CHRONIC      ')\g
INSERT INTO Diagnose VALUES ('029','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('03','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('031','     NEOPLASM, BENIGN      ')\g
INSERT INTO Diagnose VALUES ('0311','    Fibroadenoma      ')\g
INSERT INTO Diagnose VALUES ('03111','   Multiple      ')\g
INSERT INTO Diagnose VALUES ('03112','   Giant (cystosarcoma phylloides)      ')\g
INSERT INTO Diagnose VALUES ('03113','   Calcified      ')\g
INSERT INTO Diagnose VALUES ('03114','   Unusual appearance      ')\g
INSERT INTO Diagnose VALUES ('03115','   Lipofibroadenoma      ')\g
INSERT INTO Diagnose VALUES ('03116','   In lactating breast      ')\g
INSERT INTO Diagnose VALUES ('03117','   Juvenile      ')\g
INSERT INTO Diagnose VALUES ('03119','   Other      ')\g
INSERT INTO Diagnose VALUES ('0312','    Papilloma (intraductal)      ')\g
INSERT INTO Diagnose VALUES ('0313','    Lipoma      ')\g
INSERT INTO Diagnose VALUES ('0319','    Other      ')\g
INSERT INTO Diagnose VALUES ('03191','   Fibroma      ')\g
INSERT INTO Diagnose VALUES ('03192','   Granular cell myoblastoma      ')\g
INSERT INTO Diagnose VALUES ('03193','   Myoma      ')\g
INSERT INTO Diagnose VALUES ('03199','   Other      ')\g
INSERT INTO Diagnose VALUES ('032','     NEOPLASM, MALIGNANT (for 4th number see box following .329)      ')\g
INSERT INTO Diagnose VALUES ('0321','    Lobular carcinoma in situ      ')\g
INSERT INTO Diagnose VALUES ('0322','    Scirrhous (adenocarcinoma with desmoplasia)      ')\g
INSERT INTO Diagnose VALUES ('0323','    Medullary      ')\g
INSERT INTO Diagnose VALUES ('0324','    Intraductal      ')\g
INSERT INTO Diagnose VALUES ('03241','   Comedocarcinoma      ')\g
INSERT INTO Diagnose VALUES ('03242','   Paget disease      ')\g
INSERT INTO Diagnose VALUES ('0325','    Papillary (intracystic) adenocarcinoma      ')\g
INSERT INTO Diagnose VALUES ('0326','    Inflammatory      ')\g
INSERT INTO Diagnose VALUES ('0327','    Invasive lobular carcinoma      ')\g
INSERT INTO Diagnose VALUES ('0328','    Colloid (mucinous)      ')\g
INSERT INTO Diagnose VALUES ('0329','    Other   include: carcinoma of male breast, tubular carcinoma, sarcoma, neoplasm associated with AIDS      ')\g
INSERT INTO Diagnose VALUES ('033','     MALIGNANT NEOPLASM-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('0331','    Skin thickening only      ')\g
INSERT INTO Diagnose VALUES ('0332','    Parenchymal mass      ')\g
INSERT INTO Diagnose VALUES ('0339','    Other      ')\g
INSERT INTO Diagnose VALUES ('034','     LEUKEMIA, LYMPHOMA      ')\g
INSERT INTO Diagnose VALUES ('039','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('04','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('043','     POST-TRAUMATIC      ')\g
INSERT INTO Diagnose VALUES ('0431','    Hematoma      ')\g
INSERT INTO Diagnose VALUES ('0432','    Fat necrosis      ')\g
INSERT INTO Diagnose VALUES ('0433','    Contusion      ')\g
INSERT INTO Diagnose VALUES ('0434','    Laceration      ')\g
INSERT INTO Diagnose VALUES ('0435','    Gunshot wound      ')\g
INSERT INTO Diagnose VALUES ('0439','    Other      ')\g
INSERT INTO Diagnose VALUES ('044','     EFFECT OF ASPIRATION      ')\g
INSERT INTO Diagnose VALUES ('045','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('0451','    Postmastectomy, simple      ')\g
INSERT INTO Diagnose VALUES ('0452','    Postmastectomy, radical      ')\g
INSERT INTO Diagnose VALUES ('0453','    Effect of biopsy      ')\g
INSERT INTO Diagnose VALUES ('04531','   Asymmetric removal of tissue      ')\g
INSERT INTO Diagnose VALUES ('04532','   Skin thickening or deformity      ')\g
INSERT INTO Diagnose VALUES ('04533','   Architectural distortion      ')\g
INSERT INTO Diagnose VALUES ('04534','   Parenchymal mass (scar)      ')\g
INSERT INTO Diagnose VALUES ('04535','   Calcification      ')\g
INSERT INTO Diagnose VALUES ('04536','   Fat necrosis      ')\g
INSERT INTO Diagnose VALUES ('04537','   Foreign body      ')\g
INSERT INTO Diagnose VALUES ('04538','   Combination of above      ')\g
INSERT INTO Diagnose VALUES ('04539','   Other      ')\g
INSERT INTO Diagnose VALUES ('0454','    Prosthesis, mammoplasty      ')\g
INSERT INTO Diagnose VALUES ('04541','   Reduction      ')\g
INSERT INTO Diagnose VALUES ('04542','   Augmentation      ')\g
INSERT INTO Diagnose VALUES ('04543','   Silicone implant      ')\g
INSERT INTO Diagnose VALUES ('04544','   Saline implant      ')\g
INSERT INTO Diagnose VALUES ('04545','   Muscle flap      ')\g
INSERT INTO Diagnose VALUES ('04549','   Other      ')\g
INSERT INTO Diagnose VALUES ('0455','    Lumpectomy, segmental resection      ')\g
INSERT INTO Diagnose VALUES ('0458','    Complication of surgery or interventional procedure   include: of biopsy, of aspiration      ')\g
INSERT INTO Diagnose VALUES ('0459','    Other      ')\g
INSERT INTO Diagnose VALUES ('046','     FOREIGN BODY      ')\g
INSERT INTO Diagnose VALUES ('047','     EFFECT OF RADIATION      ')\g
INSERT INTO Diagnose VALUES ('048','     FAT NECROSIS   exclude: fat necrosis following biopsy (.4536)      ')\g
INSERT INTO Diagnose VALUES ('049','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('05','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('051','     HYPERESTROGENISM      ')\g
INSERT INTO Diagnose VALUES ('052','     POSTMENOPAUSAL CHANGE      ')\g
INSERT INTO Diagnose VALUES ('053','     MENSTRUAL CHANGES      ')\g
INSERT INTO Diagnose VALUES ('054','     LACTATING BREAST      ')\g
INSERT INTO Diagnose VALUES ('059','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('06','      OTHER GENERALIZED SYSTEMIC DISORDER      ')\g
INSERT INTO Diagnose VALUES ('07','      DUCT DISEASE, MAMMARY DYSPLASIA, CUTANEOUS LESION OF BREAST, LYMPHADENOPATHY      ')\g
INSERT INTO Diagnose VALUES ('071','     DUCT DISEASE      ')\g
INSERT INTO Diagnose VALUES ('0711','    Ductal ectasia      ')\g
INSERT INTO Diagnose VALUES ('0712','    Plasma cell mastitis      ')\g
INSERT INTO Diagnose VALUES ('0713','    Secretory disease      ')\g
INSERT INTO Diagnose VALUES ('0714','    Galactocele      ')\g
INSERT INTO Diagnose VALUES ('0719','    Other      ')\g
INSERT INTO Diagnose VALUES ('072','     MAMMARY DYSPLASIA      ')\g
INSERT INTO Diagnose VALUES ('0721','    Fibrocystic disease      ')\g
INSERT INTO Diagnose VALUES ('07211','   Single cyst      ')\g
INSERT INTO Diagnose VALUES ('07212','   Multiple cysts      ')\g
INSERT INTO Diagnose VALUES ('07213','   Unusual appearance      ')\g
INSERT INTO Diagnose VALUES ('07219','   Other      ')\g
INSERT INTO Diagnose VALUES ('0722','    Adenosis      ')\g
INSERT INTO Diagnose VALUES ('0723','    Fibrosis      ')\g
INSERT INTO Diagnose VALUES ('0724','    Papillomatosis      ')\g
INSERT INTO Diagnose VALUES ('07241','   Diffuse      ')\g
INSERT INTO Diagnose VALUES ('07242','   Localized      ')\g
INSERT INTO Diagnose VALUES ('0725','    Radial scar      ')\g
INSERT INTO Diagnose VALUES ('0728','    More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('0729','    Other      ')\g
INSERT INTO Diagnose VALUES ('073','     CUTANEOUS LESION OF BREAST (see also skin thickening .83)      ')\g
INSERT INTO Diagnose VALUES ('0731','    Inclusion cyst      ')\g
INSERT INTO Diagnose VALUES ('0732','    Nevus      ')\g
INSERT INTO Diagnose VALUES ('0733','    Keratosis      ')\g
INSERT INTO Diagnose VALUES ('0734','    Keloid      ')\g
INSERT INTO Diagnose VALUES ('0739','    Other      ')\g
INSERT INTO Diagnose VALUES ('074','     LYMPHADENOPATHY      ')\g
INSERT INTO Diagnose VALUES ('0741','    Due to fat infiltration      ')\g
INSERT INTO Diagnose VALUES ('0742','    Due to infection      ')\g
INSERT INTO Diagnose VALUES ('0743','    Due to neoplasm      ')\g
INSERT INTO Diagnose VALUES ('0744','    Intramammary lymph nodes      ')\g
INSERT INTO Diagnose VALUES ('0749','    Other      ')\g
INSERT INTO Diagnose VALUES ('075','     GYNECOMASTIA      ')\g
INSERT INTO Diagnose VALUES ('079','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('08','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('081','     CALCIFICATION (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('0811','    Benign      ')\g
INSERT INTO Diagnose VALUES ('08111','   Skin calcification      ')\g
INSERT INTO Diagnose VALUES ('08112','   Milk of calcium in tiny cysts      ')\g
INSERT INTO Diagnose VALUES ('08113','   Cyst wall calcification      ')\g
INSERT INTO Diagnose VALUES ('08114','   Rim calcification of silicone nodules      ')\g
INSERT INTO Diagnose VALUES ('08119','   Other      ')\g
INSERT INTO Diagnose VALUES ('0812','    Malignant      ')\g
INSERT INTO Diagnose VALUES ('0813','    Carcinoma in situ      ')\g
INSERT INTO Diagnose VALUES ('0814','    Plasma cell mastitis      ')\g
INSERT INTO Diagnose VALUES ('0815','    Sclerosing adenosis, papillomatosis, apocrine metaplasia      ')\g
INSERT INTO Diagnose VALUES ('0816','    Ductal (secretory disease)      ')\g
INSERT INTO Diagnose VALUES ('0817','    Vascular      ')\g
INSERT INTO Diagnose VALUES ('0818','    Stromal      ')\g
INSERT INTO Diagnose VALUES ('0819','    Other    exclude: fat necrosis (.48)      ')\g
INSERT INTO Diagnose VALUES ('083','     SKIN THICKENING (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('0831','    Post-surgical scar      ')\g
INSERT INTO Diagnose VALUES ('0832','    Edema   include: congestive heart failure, chronic renal failure      ')\g
INSERT INTO Diagnose VALUES ('0833','    Lymphedema   include: axillary adenopathy, axillary dissection      ')\g
INSERT INTO Diagnose VALUES ('0834','    Skin infection or inflammation      ')\g
INSERT INTO Diagnose VALUES ('0835','    Malignant dermal invasion      ')\g
INSERT INTO Diagnose VALUES ('0836','    Prior radiation therapy      ')\g
INSERT INTO Diagnose VALUES ('0839','    Other   include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('089','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('09','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('091','     FUNDAMENTAL OBSERVATION      ')\g
INSERT INTO Diagnose VALUES ('092','     ANATOMICAL DETAIL      ')\g
INSERT INTO Diagnose VALUES ('093','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('094','     ERROR IN DIAGNOSIS (see also box following .329)      ')\g
INSERT INTO Diagnose VALUES ('099','     OTHER    include: phenomenon related to technique of examination      ')\g
INSERT INTO Diagnose VALUES ('11','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('111','     NORMAL ROUTINE PLAIN FILM   exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('112','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION      ')\g
INSERT INTO Diagnose VALUES ('1121','    Digital radiographic techniques, tomography, MR, nuclear medicine      ')\g
INSERT INTO Diagnose VALUES ('11211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('112111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('112112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('112113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('112114','  Delayed scanning following enhancement      ')\g
INSERT INTO Diagnose VALUES ('112115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('112116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('112117','  Three-dimensional reconstruction      ')\g
INSERT INTO Diagnose VALUES ('112118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('112119','  Other, including serial enhancement studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('11214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('112141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('1121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('1121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('1121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('1121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('1121415',' Specific resonance suppressed  include: fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('1121416',' High-speed imaging;  include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('1121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('1121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('112142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('112143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('112144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('112145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('112146','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('112147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('112149','  Other      ')\g
INSERT INTO Diagnose VALUES ('11215','   Digital radiography    exclude: with angiography (.123_3, .124_4)      ')\g
INSERT INTO Diagnose VALUES ('11216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('112161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('112162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('112163','  PET      ')\g
INSERT INTO Diagnose VALUES ('112164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('112165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('112166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('112167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('112168','  Therapeutic procedure  include: use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('112169','  Other.  See also 1.12178 Receptor-avid  radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('11217','   Nuclear medicine-CNS      ')\g
INSERT INTO Diagnose VALUES ('112172','  Regional brain perfusion imaging      ')\g
INSERT INTO Diagnose VALUES ('112173','  Regional brain metabolism imaging (e.g., 18F-FDG)      ')\g
INSERT INTO Diagnose VALUES ('112174','  Blood-brain barrier study      ')\g
INSERT INTO Diagnose VALUES ('112175','  Cisternography (include CSF leak detection)      ')\g
INSERT INTO Diagnose VALUES ('112176','  CSF shunt study      ')\g
INSERT INTO Diagnose VALUES ('112177','  Dacrocystography      ')\g
INSERT INTO Diagnose VALUES ('112178','  Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('112179','  Other      ')\g
INSERT INTO Diagnose VALUES ('11218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('11219','   Other      ')\g
INSERT INTO Diagnose VALUES ('1122','    Encephalography, ventriculography      ')\g
INSERT INTO Diagnose VALUES ('11221','   Gas encephalography      ')\g
INSERT INTO Diagnose VALUES ('11222','   Positive contrast encephalography      ')\g
INSERT INTO Diagnose VALUES ('11223','   Gas ventriculography      ')\g
INSERT INTO Diagnose VALUES ('11224','   Positive contrast ventriculography      ')\g
INSERT INTO Diagnose VALUES ('11225','   Positive contrast cisternography      ')\g
INSERT INTO Diagnose VALUES ('11229','   Other      ')\g
INSERT INTO Diagnose VALUES ('1123','    Venography   include: jugular, dural sinus, orbital      ')\g
INSERT INTO Diagnose VALUES ('1124','    Arteriography      ')\g
INSERT INTO Diagnose VALUES ('11241','   Aortic arch      ')\g
INSERT INTO Diagnose VALUES ('11242','   Subclavian      ')\g
INSERT INTO Diagnose VALUES ('11243','   Vertebral      ')\g
INSERT INTO Diagnose VALUES ('11244','   Brachial      ')\g
INSERT INTO Diagnose VALUES ('11245','   Common carotid      ')\g
INSERT INTO Diagnose VALUES ('11246','   External carotid      ')\g
INSERT INTO Diagnose VALUES ('11247','   Internal carotid      ')\g
INSERT INTO Diagnose VALUES ('11248','   More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('11249','   Other   include: stereoangiography      ')\g
INSERT INTO Diagnose VALUES ('1125','    Special nonroutine projection, cine or video study      ')\g
INSERT INTO Diagnose VALUES ('1126','    Biopsy or interventional      ')\g
INSERT INTO Diagnose VALUES ('11261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('11262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('11263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('11264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('11265','   Thrombolytic infusion      ')\g
INSERT INTO Diagnose VALUES ('11266','   Chemotherapeutic infusion      ')\g
INSERT INTO Diagnose VALUES ('11267','   Operative procedure   include: stereotaxis (see also  intraoperative ultrasound .12982 )      ')\g
INSERT INTO Diagnose VALUES ('11269','   Other      ')\g
INSERT INTO Diagnose VALUES ('1127','    Image enhancement technique   include: subtraction  exclude: digital technique .123_3, .124_3      ')\g
INSERT INTO Diagnose VALUES ('1128','    Foreign body localization      ')\g
INSERT INTO Diagnose VALUES ('1129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('11292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('11298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('112981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('112982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('112983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('112984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('112985','  Guided procedure for diagnosis   include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('112986','  Guided procedure for therapy   include: drainage      ')\g
INSERT INTO Diagnose VALUES ('112987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('112988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('112989','  Special nonroutine projection, special mode, or transducer type  include: transcranial Doppler      ')\g
INSERT INTO Diagnose VALUES ('11299','   Other   include: xeroradiography, thermography, biomagnetism study      ')\g
INSERT INTO Diagnose VALUES ('113','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('1131','    Variation in shape      ')\g
INSERT INTO Diagnose VALUES ('11311','   Dolichocephaly      ')\g
INSERT INTO Diagnose VALUES ('11312','   Brachycephaly      ')\g
INSERT INTO Diagnose VALUES ('11313','   Molding      ')\g
INSERT INTO Diagnose VALUES ('11314','   Postural flattening, occipital      ')\g
INSERT INTO Diagnose VALUES ('11315','   Postural flattening, other   include: asymmetrical skull      ')\g
INSERT INTO Diagnose VALUES ('11319','   Other   exclude: craniosynostosis (.143),  micrencephaly (.1421)      ')\g
INSERT INTO Diagnose VALUES ('1132','    Variation in bone thickness or density      ')\g
INSERT INTO Diagnose VALUES ('11321','   Prominent digital markings      ')\g
INSERT INTO Diagnose VALUES ('11322','   Unusually thick calvaria   exclude: due to phenytoin (Dilantin)      ')\g
INSERT INTO Diagnose VALUES ('11323','   Benign internal hyperostosis      ')\g
INSERT INTO Diagnose VALUES ('11324','   Benign external hyperostosis      ')\g
INSERT INTO Diagnose VALUES ('11325','   Unusually thin calvaria      ')\g
INSERT INTO Diagnose VALUES ('11326','   Parietal thinning      ')\g
INSERT INTO Diagnose VALUES ('11329','   Other   include: bony spur      ')\g
INSERT INTO Diagnose VALUES ('1133','    Suture or fontanel variant, anomalous aperture      ')\g
INSERT INTO Diagnose VALUES ('11331','   Persistent metopic suture      ')\g
INSERT INTO Diagnose VALUES ('11332','   Cranium bifidum occultum (see 2.148)      ')\g
INSERT INTO Diagnose VALUES ('11333','   Late fontanel closure      ')\g
INSERT INTO Diagnose VALUES ('11334','   Early fontanel closure      ')\g
INSERT INTO Diagnose VALUES ('11335','   Accessory fontanel      ')\g
INSERT INTO Diagnose VALUES ('11336','   Accessory suture      ')\g
INSERT INTO Diagnose VALUES ('11337','   Wormian bone, interparietal bone      ')\g
INSERT INTO Diagnose VALUES ('11338','   Parietal foramen      ')\g
INSERT INTO Diagnose VALUES ('11339','   Other   include: fontanel bone, Kerckring process, accessory bones at foramen magnum, other persistent suture   exclude: dermal sinus (.1463), dorsal cyst (.1469), vascular groove or lake (.1361)      ')\g
INSERT INTO Diagnose VALUES ('1134','    Physiological calcification      ')\g
INSERT INTO Diagnose VALUES ('11341','   Choroid      ')\g
INSERT INTO Diagnose VALUES ('11342','   Dura      ')\g
INSERT INTO Diagnose VALUES ('11343','   Falx      ')\g
INSERT INTO Diagnose VALUES ('11344','   Habenula      ')\g
INSERT INTO Diagnose VALUES ('11345','   Pacchionian bodies      ')\g
INSERT INTO Diagnose VALUES ('11346','   Pineal      ')\g
INSERT INTO Diagnose VALUES ('11347','   Petroclinoid ligament      ')\g
INSERT INTO Diagnose VALUES ('11348','   Tentorium      ')\g
INSERT INTO Diagnose VALUES ('11349','   Other   exclude: carotid artery (.81)      ')\g
INSERT INTO Diagnose VALUES ('1135','    Variation in ventricular system      ')\g
INSERT INTO Diagnose VALUES ('11351','   Prominent collateral eminence      ')\g
INSERT INTO Diagnose VALUES ('11352','   Prominent calcar avis      ')\g
INSERT INTO Diagnose VALUES ('11353','   Corpus callosum striations      ')\g
INSERT INTO Diagnose VALUES ('11354','   Absent septum pellucidum      ')\g
INSERT INTO Diagnose VALUES ('11355','   Cavum septum pellucidum      ')\g
INSERT INTO Diagnose VALUES ('11356','   Cavum vergae      ')\g
INSERT INTO Diagnose VALUES ('11357','   Cavum velum interpositi      ')\g
INSERT INTO Diagnose VALUES ('11358','   Coarctation of ventricle      ')\g
INSERT INTO Diagnose VALUES ('11359','   Other      ')\g
INSERT INTO Diagnose VALUES ('1136','    Vascular variant      ')\g
INSERT INTO Diagnose VALUES ('11361','   Prominent vascular groove, vascular lake      ')\g
INSERT INTO Diagnose VALUES ('11362','   Prominent emissary vessel      ')\g
INSERT INTO Diagnose VALUES ('11363','   Caroticobasilar anastomosis   include: trigeminal, hypoglossal, otic artery      ')\g
INSERT INTO Diagnose VALUES ('11364','   Duplication, fenestration      ')\g
INSERT INTO Diagnose VALUES ('11365','   Hypoplasia, aplasia      ')\g
INSERT INTO Diagnose VALUES ('11366','   Unusual origin or course of vessel      ')\g
INSERT INTO Diagnose VALUES ('11367','   Functional dilatation      ')\g
INSERT INTO Diagnose VALUES ('11369','   Other      ')\g
INSERT INTO Diagnose VALUES ('1139','    Other   include: small sella turcica (see also .513)      ')\g
INSERT INTO Diagnose VALUES ('114','     CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('1141','    Aplasia, agenesis      ')\g
INSERT INTO Diagnose VALUES ('11411','   Anencephaly      ')\g
INSERT INTO Diagnose VALUES ('11412','   Holoprosencephaly, arhinencephaly  include: trigonocephaly   exclude: median cleft face (2.1485)      ')\g
INSERT INTO Diagnose VALUES ('11413','   Other partial agenesis   include: agenesis of corpus callosum      ')\g
INSERT INTO Diagnose VALUES ('11419','   Other   exclude: hydranencephaly (.1459), vascular (.1365)      ')\g
INSERT INTO Diagnose VALUES ('1142','    Hypoplasia, atrophy      ')\g
INSERT INTO Diagnose VALUES ('11421','   Micrencephaly, microgyria      ')\g
INSERT INTO Diagnose VALUES ('11422','   Hemiatrophy      ')\g
INSERT INTO Diagnose VALUES ('11423','   Porencephaly      ')\g
INSERT INTO Diagnose VALUES ('11424','   Cystic disease of brain   exclude: ependymal cyst (.1465), subarachnoid cyst (.1466)      ')\g
INSERT INTO Diagnose VALUES ('11429','   Other   exclude: Sturge-Weber (.1833), Arnold-Chiari (.1473), small sella turcica (.139)      ')\g
INSERT INTO Diagnose VALUES ('1143','    Craniosynostosis   exclude: trigonocephaly (.1412)      ')\g
INSERT INTO Diagnose VALUES ('1143',' -  More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('11431','   One suture      ')\g
INSERT INTO Diagnose VALUES ('11432','   Two sutures      ')\g
INSERT INTO Diagnose VALUES ('11433','   Three sutures      ')\g
INSERT INTO Diagnose VALUES ('11435','   All sutures   include: Kleeblatschadel (clover leaf skull")"      ')\g
INSERT INTO Diagnose VALUES ('11439','   Other   include: plagiocephaly      ')\g
INSERT INTO Diagnose VALUES ('1144','    Hyperplasia   include: macrencephaly, macrogyria, hemihypertrophy, accessory commissures      ')\g
INSERT INTO Diagnose VALUES ('1145','    Hydrocephalus, congenital   exclude: acquired (code cause)      ')\g
INSERT INTO Diagnose VALUES ('11451','   Aqueduct stenosis      ')\g
INSERT INTO Diagnose VALUES ('11452','   Dandy-Walker cyst      ')\g
INSERT INTO Diagnose VALUES ('11453','   Communicating with basilar arachnoiditis      ')\g
INSERT INTO Diagnose VALUES ('11454','   Communicating with surface arachnoiditis      ')\g
INSERT INTO Diagnose VALUES ('11455','   Ependymal cyst (see also .1465, .3619)      ')\g
INSERT INTO Diagnose VALUES ('11456','   Gliosis      ')\g
INSERT INTO Diagnose VALUES ('11459','   Other   include: hydranencephaly      ')\g
INSERT INTO Diagnose VALUES ('1146','    Cephalocele, dermal sinus, scalp defect, cyst      ')\g
INSERT INTO Diagnose VALUES ('11461','   Encephalocele      ')\g
INSERT INTO Diagnose VALUES ('11462','   Meningocele      ')\g
INSERT INTO Diagnose VALUES ('11463','   Dermal sinus      ')\g
INSERT INTO Diagnose VALUES ('11464','   Congenital scalp defect      ')\g
INSERT INTO Diagnose VALUES ('11465','   Ependymal cyst (see also .1455)      ')\g
INSERT INTO Diagnose VALUES ('11466','   Subarachnoid cyst (see also .3619)      ')\g
INSERT INTO Diagnose VALUES ('11469','   Other   include: dorsal cyst   exclude: cranium bifidum occultum (.1332), cleidocranial dysplasia (.1524), pyknodysostosis (.1553), cystic disease of brain (.1424)      ')\g
INSERT INTO Diagnose VALUES ('1147','    Craniovertebral anomaly (see also 31.147)      ')\g
INSERT INTO Diagnose VALUES ('11471','   Ossiculum terminale      ')\g
INSERT INTO Diagnose VALUES ('11472','   Assimilation of atlas (atlanto-occipital fusion)      ')\g
INSERT INTO Diagnose VALUES ('11473','   Arnold-Chiari malformation      ')\g
INSERT INTO Diagnose VALUES ('11474','   Occipital vertebra      ')\g
INSERT INTO Diagnose VALUES ('11475','   Basilar impression (invagination)      ')\g
INSERT INTO Diagnose VALUES ('11476','   Os odontoideum      ')\g
INSERT INTO Diagnose VALUES ('11479','   Other   exclude: normal variant (.1339), atlantoaxial subluxation (3.1478)      ')\g
INSERT INTO Diagnose VALUES ('1149','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('11491','   Lacunar skull      ')\g
INSERT INTO Diagnose VALUES ('11494','   Congenital arteriovenous fistula, malformation, plain film findings (see also .75 for angiographic findings)      ')\g
INSERT INTO Diagnose VALUES ('11499','   Other      ')\g
INSERT INTO Diagnose VALUES ('115','     DYSPLASIA (see 4.15 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('11521','   Achondroplasia      ')\g
INSERT INTO Diagnose VALUES ('11524','   Cleidocranial dysplasia      ')\g
INSERT INTO Diagnose VALUES ('11551','   Osteogenesis imperfecta      ')\g
INSERT INTO Diagnose VALUES ('11553','   Pyknodysostosis      ')\g
INSERT INTO Diagnose VALUES ('11555','   Craniodiaphyseal dysplasia, hyperphosphatasemia      ')\g
INSERT INTO Diagnose VALUES ('11556','   Craniometaphyseal dysplasia      ')\g
INSERT INTO Diagnose VALUES ('11559','   Other   include: pachydermoperiostosis      ')\g
INSERT INTO Diagnose VALUES ('116','     DYSOSTOSIS (see 4.16 for detailed classification)  exclude: craniosynostosis (.143)      ')\g
INSERT INTO Diagnose VALUES ('1165','    Spinal dysostosis (see 3.165 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('1166','    Facial dysostosis (see 2.166 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('1169','    Other      ')\g
INSERT INTO Diagnose VALUES ('117','     PRIMARY DISTURBANCE OF GROWTH      ')\g
INSERT INTO Diagnose VALUES ('1171','    Increased growth   include: Marfan syndrome, cerebral gigantism      ')\g
INSERT INTO Diagnose VALUES ('1172','    Decreased growth   include: progeria, bird-headed dwarfism, Menkes (kinky hair) syndrome      ')\g
INSERT INTO Diagnose VALUES ('118','     OTHER CONSTITUTIONAL DISORDERS      ')\g
INSERT INTO Diagnose VALUES ('1181','    Mucopolysaccharidosis (see 4.181 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('1182','    Dysostosis multiplex, other cause (see 4.182 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('1183','    Neurocutaneous syndrome      ')\g
INSERT INTO Diagnose VALUES ('11831','   Neurofibromatosis      ')\g
INSERT INTO Diagnose VALUES ('11832','   Tuberous sclerosis      ')\g
INSERT INTO Diagnose VALUES ('11833','   Sturge-Weber syndrome      ')\g
INSERT INTO Diagnose VALUES ('11834','   Hippel-Lindau syndrome      ')\g
INSERT INTO Diagnose VALUES ('11839','   Other  include: ataxia telangiectasia      ')\g
INSERT INTO Diagnose VALUES ('1184','    Autosomal aberration   include: trisomy 21 (Down syndrome)      ')\g
INSERT INTO Diagnose VALUES ('1185','    Sex chromosome aberration   include: Turner syndrome      ')\g
INSERT INTO Diagnose VALUES ('1186','    Idiopathic osteolysis   include: acro-osteolysis  exclude: Gorham disease (.3143)      ')\g
INSERT INTO Diagnose VALUES ('1189','    Other      ')\g
INSERT INTO Diagnose VALUES ('119','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('12','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('120','     INFECTION CLASSIFIED BY ORGANISM (also code type: e.g., meningitis, .251; vasculitis, .258; abscess, .257, etc.) (see 6.20 for detailed classification) (for 5th number see box following .208)      ')\g
INSERT INTO Diagnose VALUES ('1201','    Gram-positive facultative bacterium      ')\g
INSERT INTO Diagnose VALUES ('12011','   Pneumococcus      ')\g
INSERT INTO Diagnose VALUES ('12012','   Staphylococcus      ')\g
INSERT INTO Diagnose VALUES ('12013','   Streptococcus   include: enterococcus      ')\g
INSERT INTO Diagnose VALUES ('12019','   Other organism      ')\g
INSERT INTO Diagnose VALUES ('1202','    Gram-negative bacterium      ')\g
INSERT INTO Diagnose VALUES ('12022','   Klebsiella pneumoniae      ')\g
INSERT INTO Diagnose VALUES ('12026','   Hemophilus species include: H. influenzae      ')\g
INSERT INTO Diagnose VALUES ('12029','   Other      ')\g
INSERT INTO Diagnose VALUES ('1203','    Mycobacterium   exclude: tuberculosis (.230)      ')\g
INSERT INTO Diagnose VALUES ('1204','    Anaerobic bacterium      ')\g
INSERT INTO Diagnose VALUES ('12044','   Actinomycosis      ')\g
INSERT INTO Diagnose VALUES ('12049','   Other      ')\g
INSERT INTO Diagnose VALUES ('1205','    Fungus      ')\g
INSERT INTO Diagnose VALUES ('12052','   Coccidioidomycosis      ')\g
INSERT INTO Diagnose VALUES ('12054','   Cryptococcus (torulopsis)      ')\g
INSERT INTO Diagnose VALUES ('12059','   Other   exclude: actinomycosis (.2044)      ')\g
INSERT INTO Diagnose VALUES ('1206','    Virus, mycoplasma (for 5th number see box following .2079)      ')\g
INSERT INTO Diagnose VALUES ('12065','   Herpes      ')\g
INSERT INTO Diagnose VALUES ('12066','   Cytomegalovirus      ')\g
INSERT INTO Diagnose VALUES ('12068','   Human immunodeficiency virus (HIV)      ')\g
INSERT INTO Diagnose VALUES ('1207','    Protozoa, spirochete      ')\g
INSERT INTO Diagnose VALUES ('12074','   Toxoplasma      ')\g
INSERT INTO Diagnose VALUES ('12076','   Syphilis, congenital      ')\g
INSERT INTO Diagnose VALUES ('12077','   Syphilis, acquired      ')\g
INSERT INTO Diagnose VALUES ('12079','   Other      ')\g
INSERT INTO Diagnose VALUES ('1208','    Helminth, roundworm, flatworm   include: Taenia solium (cysticercosis), Echinococcus      ')\g
INSERT INTO Diagnose VALUES ('121','     OSTEOMYELITIS      ')\g
INSERT INTO Diagnose VALUES ('122','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('123','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('124','     SOFT TISSUE INFLAMMATION, SINUSTRACT, FISTULA      ')\g
INSERT INTO Diagnose VALUES ('125','     INTRACRANIAL INFLAMMATION (also code specific organism .20)      ')\g
INSERT INTO Diagnose VALUES ('1251','    Meningitis      ')\g
INSERT INTO Diagnose VALUES ('1252','    Arachnoiditis      ')\g
INSERT INTO Diagnose VALUES ('12521','   Secondary to subarachnoid hemorrhage      ')\g
INSERT INTO Diagnose VALUES ('12522','   Secondary to trauma      ')\g
INSERT INTO Diagnose VALUES ('12523','   Chemical      ')\g
INSERT INTO Diagnose VALUES ('12524','   Postoperative      ')\g
INSERT INTO Diagnose VALUES ('12529','   Other      ')\g
INSERT INTO Diagnose VALUES ('1253','    Encephalitis      ')\g
INSERT INTO Diagnose VALUES ('1254','    Ependymitis      ')\g
INSERT INTO Diagnose VALUES ('1255','    Cerebritis      ')\g
INSERT INTO Diagnose VALUES ('1256','    Abscess or granuloma, brain   exclude: sarcoidosis (.22), tuberculosis (.23)      ')\g
INSERT INTO Diagnose VALUES ('1257','    Abscess or granuloma, subdural or epidural      ')\g
INSERT INTO Diagnose VALUES ('1258','    Vasculitis   exclude: mycotic aneurysm (.733), systemic vasculitis (.62)      ')\g
INSERT INTO Diagnose VALUES ('12581','   Arteritis      ')\g
INSERT INTO Diagnose VALUES ('12582','   Arterial occlusion      ')\g
INSERT INTO Diagnose VALUES ('12583','   Venous occlusion      ')\g
INSERT INTO Diagnose VALUES ('12584','   Phlebitis      ')\g
INSERT INTO Diagnose VALUES ('12589','   Other      ')\g
INSERT INTO Diagnose VALUES ('1259','    Other      ')\g
INSERT INTO Diagnose VALUES ('129','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('1298','    Infectious complications of AIDS      ')\g
INSERT INTO Diagnose VALUES ('13','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('131','     BENIGN NEOPLASM OF SCALP OR SKULL      ')\g
INSERT INTO Diagnose VALUES ('1311','    Cartilaginous origin      ')\g
INSERT INTO Diagnose VALUES ('13113','   Osteochondroma (osteocartilagenous exostosis)      ')\g
INSERT INTO Diagnose VALUES ('13114','   Enchondroma      ')\g
INSERT INTO Diagnose VALUES ('13119','   Other      ')\g
INSERT INTO Diagnose VALUES ('1312','    Osseous origin      ')\g
INSERT INTO Diagnose VALUES ('13121','   Osteoma   include: Gardner syndrome      ')\g
INSERT INTO Diagnose VALUES ('13129','   Other      ')\g
INSERT INTO Diagnose VALUES ('1313','    Fibrous origin   exclude: fibrous dysplasia (.85)      ')\g
INSERT INTO Diagnose VALUES ('1314','    Vascular origin      ')\g
INSERT INTO Diagnose VALUES ('13141','   Hemangioma      ')\g
INSERT INTO Diagnose VALUES ('13142','   Lymphangioma      ')\g
INSERT INTO Diagnose VALUES ('13143','   Skeletal angiomatosis   include: Gorham disease (vanishing bone")"      ')\g
INSERT INTO Diagnose VALUES ('13149','   Other   include: hemangiopericytoma      ')\g
INSERT INTO Diagnose VALUES ('1315','    Nerve tissue origin  include: melanotic proganoma      ')\g
INSERT INTO Diagnose VALUES ('1318','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('13185','   Fibrous histiocytoma (benign) (see .3285)      ')\g
INSERT INTO Diagnose VALUES ('1319','    Other  chymal neoplasm, epidermoid, dermal sinus      ')\g
INSERT INTO Diagnose VALUES ('132','     MALIGNANT NEOPLASM OF SCALP OR SKULL-PRIMARY      ')\g
INSERT INTO Diagnose VALUES ('1321','    Cartilaginous origin      ')\g
INSERT INTO Diagnose VALUES ('13211','   Chondrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('13219','   Other      ')\g
INSERT INTO Diagnose VALUES ('1322','    Osseous origin      ')\g
INSERT INTO Diagnose VALUES ('13221','   Osteosarcoma      ')\g
INSERT INTO Diagnose VALUES ('13229','   Other      ')\g
INSERT INTO Diagnose VALUES ('1323','    Fibrous origin      ')\g
INSERT INTO Diagnose VALUES ('13231','   Fibrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('13239','   Other      ')\g
INSERT INTO Diagnose VALUES ('1324','    Vascular origin      ')\g
INSERT INTO Diagnose VALUES ('13241','   Angiosarcoma      ')\g
INSERT INTO Diagnose VALUES ('13249','   Other   exclude: hemangiopericytoma (.3149)      ')\g
INSERT INTO Diagnose VALUES ('1327','    Notochordal origin   include: chordoma      ')\g
INSERT INTO Diagnose VALUES ('1328','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('13281','   Ewing tumor      ')\g
INSERT INTO Diagnose VALUES ('13282','   Primary lymphoma of bone      ')\g
INSERT INTO Diagnose VALUES ('13285','   Fibrous histiocytoma (malignant) (see .3185)      ')\g
INSERT INTO Diagnose VALUES ('13289','   Other      ')\g
INSERT INTO Diagnose VALUES ('1329','    Other  include: neoplasm associated with AIDS      ')\g
INSERT INTO Diagnose VALUES ('133','     MALIGNANT NEOPLASM OF SCALP OR SKULL-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('1331','    Osteolytic      ')\g
INSERT INTO Diagnose VALUES ('1332','    Osteoblastic      ')\g
INSERT INTO Diagnose VALUES ('1333','    Mixed osteolytic and osteoblastic      ')\g
INSERT INTO Diagnose VALUES ('1339','    Other      ')\g
INSERT INTO Diagnose VALUES ('134','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('1341','    Leukemia      ')\g
INSERT INTO Diagnose VALUES ('1342','    Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('1343','    Non-Hodgkin lymphoma      ')\g
INSERT INTO Diagnose VALUES ('13431','   Lymphocytic well differentiated      ')\g
INSERT INTO Diagnose VALUES ('13432','   Nodular      ')\g
INSERT INTO Diagnose VALUES ('13433','   Diffuse      ')\g
INSERT INTO Diagnose VALUES ('13434','   Histiocytic      ')\g
INSERT INTO Diagnose VALUES ('13435','   Burkitt      ')\g
INSERT INTO Diagnose VALUES ('13439','   Other      ')\g
INSERT INTO Diagnose VALUES ('1345','    Myeloma (see amyloidosis .68)      ')\g
INSERT INTO Diagnose VALUES ('13451','   Plasmacytoma      ')\g
INSERT INTO Diagnose VALUES ('13452','   Multiple myeloma      ')\g
INSERT INTO Diagnose VALUES ('13459','   Other      ')\g
INSERT INTO Diagnose VALUES ('1346','    Kaposi sarcoma      ')\g
INSERT INTO Diagnose VALUES ('1349','    Other   include: Waldenstrom macroglobulinemia   exclude: neoplasm associated with AIDS (.329)      ')\g
INSERT INTO Diagnose VALUES ('135','     NEOPLASM ARISING IN PREEXISTING BENIGN DISORDER  (for 5th number see box following .39)      ')\g
INSERT INTO Diagnose VALUES ('1351','    Bone neoplasm (e.g., enchondroma, osteochondroma)      ')\g
INSERT INTO Diagnose VALUES ('1352','    Infection      ')\g
INSERT INTO Diagnose VALUES ('1353','    Paget disease      ')\g
INSERT INTO Diagnose VALUES ('1354','    Irradiated bone      ')\g
INSERT INTO Diagnose VALUES ('1355','    Fibrous dysplasia      ')\g
INSERT INTO Diagnose VALUES ('1356','    Bone infarction      ')\g
INSERT INTO Diagnose VALUES ('1359','    Other      ')\g
INSERT INTO Diagnose VALUES ('136','     INTRACRANIAL NEOPLASM-PRIMARY, NEOPLASTIC-LIKE CONDITION  (for 5th number see box following .39)      ')\g
INSERT INTO Diagnose VALUES ('1361','    Developmental origin      ')\g
INSERT INTO Diagnose VALUES ('13611','   Craniopharyngioma      ')\g
INSERT INTO Diagnose VALUES ('13612','   Colloid cyst      ')\g
INSERT INTO Diagnose VALUES ('13619','   Other   include: ependymal cyst, subarachnoid cyst (see also .1455, .1465, .1466)   exclude: chordoma (.327)      ')\g
INSERT INTO Diagnose VALUES ('1362','    Teratoma      ')\g
INSERT INTO Diagnose VALUES ('13621','   Pinealoma   include: neoplasm of pineal gland origin      ')\g
INSERT INTO Diagnose VALUES ('13622','   Epidermoid (cholesteatoma), dermoid      ')\g
INSERT INTO Diagnose VALUES ('13629','   Other      ')\g
INSERT INTO Diagnose VALUES ('1363','    Glial origin      ')\g
INSERT INTO Diagnose VALUES ('13631','   Astrocytoma grade 1      ')\g
INSERT INTO Diagnose VALUES ('13632','   Astrocytoma grade 2      ')\g
INSERT INTO Diagnose VALUES ('13633','   Astrocytoma grade 3      ')\g
INSERT INTO Diagnose VALUES ('13634','   Astrocytoma grade 4, glioblastoma multiforme      ')\g
INSERT INTO Diagnose VALUES ('13635','   Oligodendroglioma      ')\g
INSERT INTO Diagnose VALUES ('13636','   Ependymoma      ')\g
INSERT INTO Diagnose VALUES ('13637','   Medulloblastoma   include: cerebellar sarcoma      ')\g
INSERT INTO Diagnose VALUES ('13638','   Mixed glial origin      ')\g
INSERT INTO Diagnose VALUES ('13639','   Other   include: choroid plexus papilloma      ')\g
INSERT INTO Diagnose VALUES ('1364','    Nerve, nerve sheath origin      ')\g
INSERT INTO Diagnose VALUES ('13641','   Neurinoma (schwannoma, neurofibroma, neurilemmoma)      ')\g
INSERT INTO Diagnose VALUES ('13642','   Chemodectoma (nonchromaffin paraganglioma, glomus jugulare)      ')\g
INSERT INTO Diagnose VALUES ('13649','   Other      ')\g
INSERT INTO Diagnose VALUES ('1365','    Vascular origin      ')\g
INSERT INTO Diagnose VALUES ('13651','   Hemangioblastoma      ')\g
INSERT INTO Diagnose VALUES ('13659','   Other      ')\g
INSERT INTO Diagnose VALUES ('1366','    Meningeal   include: meningioma, meningeal sarcoma      ')\g
INSERT INTO Diagnose VALUES ('1367','    Spontaneous hemorrhage, hematoma   include: intraventricular   exclude: traumatic (.4)      ')\g
INSERT INTO Diagnose VALUES ('1368','    Pseudotumor cerebri      ')\g
INSERT INTO Diagnose VALUES ('1369','    Other   include: intracranial mass lesion of unknown etiology, myxoma, lipoma, melanoma   exclude: generalized neurofibromatosis (.1831), tuberous sclerosis (.1832)      ')\g
INSERT INTO Diagnose VALUES ('137','     INTRASELLAR NEOPLASM, NEOPLASTIC-LIKE CONDITION  (for 5th number see box following .39)      ')\g
INSERT INTO Diagnose VALUES ('1371','    Nonfunctioning pituitary adenoma  include: chromophobe adenoma      ')\g
INSERT INTO Diagnose VALUES ('13711','   Macrocystic      ')\g
INSERT INTO Diagnose VALUES ('13712','   Microcystic      ')\g
INSERT INTO Diagnose VALUES ('13719','   Other      ')\g
INSERT INTO Diagnose VALUES ('1372','    Functioning pituitary adenoma      ')\g
INSERT INTO Diagnose VALUES ('13721','   Macrocystic-prolactic secreting      ')\g
INSERT INTO Diagnose VALUES ('13722','   Microcystic-prolactic secreting      ')\g
INSERT INTO Diagnose VALUES ('13723','   Macrocystic-growth hormone secreting      ')\g
INSERT INTO Diagnose VALUES ('13724','   Microcystic-growth hormone secreting      ')\g
INSERT INTO Diagnose VALUES ('13725','   Macrocystic ACTH secreting      ')\g
INSERT INTO Diagnose VALUES ('13726','   Microcystic ACTH secreting      ')\g
INSERT INTO Diagnose VALUES ('13727','   Other macrocystic adenoma      ')\g
INSERT INTO Diagnose VALUES ('13728','   Other microcystic adenoma      ')\g
INSERT INTO Diagnose VALUES ('13729','   Other      ')\g
INSERT INTO Diagnose VALUES ('1373','    Arachnoidal extension (empty sella" syndrome)"      ')\g
INSERT INTO Diagnose VALUES ('1379','    Other   include: carcinoma, metastasis, myoblastoma  exclude: craniopharyngioma (.3611)      ')\g
INSERT INTO Diagnose VALUES ('138','     INTRACRANIAL NEOPLASM-SECONDARY METASTATIC (for 5th number see box following .39)      ')\g
INSERT INTO Diagnose VALUES ('1381','    Carcinoma, solitary      ')\g
INSERT INTO Diagnose VALUES ('1382','    Carcinoma, multiple      ')\g
INSERT INTO Diagnose VALUES ('1383','    Sarcoma, solitary      ')\g
INSERT INTO Diagnose VALUES ('1384','    Sarcoma, multiple      ')\g
INSERT INTO Diagnose VALUES ('1389','    Other   exclude: leukemia, lymphoma (.34)      ')\g
INSERT INTO Diagnose VALUES ('139','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('14','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('141','     FRACTURE   include: suture diastasis (.413)      ')\g
INSERT INTO Diagnose VALUES ('1411','    Linear      ')\g
INSERT INTO Diagnose VALUES ('1412','    Depressed      ')\g
INSERT INTO Diagnose VALUES ('1413','    Diastatic      ')\g
INSERT INTO Diagnose VALUES ('1414','    Multiple      ')\g
INSERT INTO Diagnose VALUES ('1415','    Stellate      ')\g
INSERT INTO Diagnose VALUES ('1416','    Healed, healing, old      ')\g
INSERT INTO Diagnose VALUES ('1419','    Other   include: child abuse (battered child)      ')\g
INSERT INTO Diagnose VALUES ('142','     COMPLICATION OF FRACTURE      ')\g
INSERT INTO Diagnose VALUES ('1421','    Dural laceration      ')\g
INSERT INTO Diagnose VALUES ('14211','   Leptomeningeal cyst      ')\g
INSERT INTO Diagnose VALUES ('14212','   Herniated brain      ')\g
INSERT INTO Diagnose VALUES ('14219','   Other      ')\g
INSERT INTO Diagnose VALUES ('1422','    Traumatic bone cyst      ')\g
INSERT INTO Diagnose VALUES ('1423','    Cerebrospinal rhinorrhea, otorrhea      ')\g
INSERT INTO Diagnose VALUES ('1424','    Traumatic pneumocephalus      ')\g
INSERT INTO Diagnose VALUES ('1429','    Other      ')\g
INSERT INTO Diagnose VALUES ('143','     INTRACRANIAL EFFECT OF TRAUMA (for 5th number see box following .439)      ')\g
INSERT INTO Diagnose VALUES ('1431','    Vascular      ')\g
INSERT INTO Diagnose VALUES ('14311','   Stenosis      ')\g
INSERT INTO Diagnose VALUES ('14312','   Occlusion      ')\g
INSERT INTO Diagnose VALUES ('14313','   Laceration   include: extravasation of contrast medium      ')\g
INSERT INTO Diagnose VALUES ('14314','   Dissection      ')\g
INSERT INTO Diagnose VALUES ('14315','   Pseudoaneurysm      ')\g
INSERT INTO Diagnose VALUES ('14319','   Other   exclude: post-traumatic arteriovenous fistula (.494)      ')\g
INSERT INTO Diagnose VALUES ('1432','    Epidural hematoma      ')\g
INSERT INTO Diagnose VALUES ('1433','    Subdural hematoma, hygroma      ')\g
INSERT INTO Diagnose VALUES ('14331','   Acute      ')\g
INSERT INTO Diagnose VALUES ('14332','   Subacute      ')\g
INSERT INTO Diagnose VALUES ('14333','   Chronic      ')\g
INSERT INTO Diagnose VALUES ('1434','    Hematoma, brain   exclude: spontaneous (.367)      ')\g
INSERT INTO Diagnose VALUES ('1435','    Contusion, infarction      ')\g
INSERT INTO Diagnose VALUES ('14351','   Contusion      ')\g
INSERT INTO Diagnose VALUES ('14352','   Infarction      ')\g
INSERT INTO Diagnose VALUES ('1436','    Cerebral edema      ')\g
INSERT INTO Diagnose VALUES ('1437','    Post-traumatic atrophy, hydrocephalus      ')\g
INSERT INTO Diagnose VALUES ('14371','   Focal atrophy   include: porencephaly      ')\g
INSERT INTO Diagnose VALUES ('14372','   Obstructive hydrocephalus, post-traumatic  include: subarachnoid block      ')\g
INSERT INTO Diagnose VALUES ('14379','   Other  include: contracting skull      ')\g
INSERT INTO Diagnose VALUES ('1438','    More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('1439','    Other   include: laceration of brain, meninges  exclude: laceration of scalp (.491)  5th number for use with .43      ')\g
INSERT INTO Diagnose VALUES ('144','     COMPLICATION OF CATHETERIZATION, ANGIOGRAPHY      ')\g
INSERT INTO Diagnose VALUES ('1441','    Hematoma      ')\g
INSERT INTO Diagnose VALUES ('1442','    Thrombosis      ')\g
INSERT INTO Diagnose VALUES ('1443','    Embolus   include: cholesterol      ')\g
INSERT INTO Diagnose VALUES ('1444','    Intimal tear, intramural injection, mural dissection      ')\g
INSERT INTO Diagnose VALUES ('1445','    Spasm      ')\g
INSERT INTO Diagnose VALUES ('1446','    Transmural perforation      ')\g
INSERT INTO Diagnose VALUES ('1447','    Broken or knotted catheter      ')\g
INSERT INTO Diagnose VALUES ('1448','    Adverse reaction to contrast medium      ')\g
INSERT INTO Diagnose VALUES ('1449','    Other   exclude: arteriovenous fistula (.494)      ')\g
INSERT INTO Diagnose VALUES ('145','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('1451','    Cerebrospinal fluid (CSF) shunt      ')\g
INSERT INTO Diagnose VALUES ('14511','   Ventriculoatrial      ')\g
INSERT INTO Diagnose VALUES ('14512','   Ventriculopleural      ')\g
INSERT INTO Diagnose VALUES ('14513','   Ventriculoperitoneal      ')\g
INSERT INTO Diagnose VALUES ('14514','   Torkildsen      ')\g
INSERT INTO Diagnose VALUES ('14517','   Complication of shunt (see also .4379, .916)      ')\g
INSERT INTO Diagnose VALUES ('14519','   Other      ')\g
INSERT INTO Diagnose VALUES ('1452','    Implantation of radiation source      ')\g
INSERT INTO Diagnose VALUES ('1453','    Bone flap      ')\g
INSERT INTO Diagnose VALUES ('14531','   Elevated      ')\g
INSERT INTO Diagnose VALUES ('14532','   Depressed      ')\g
INSERT INTO Diagnose VALUES ('14533','   Infected      ')\g
INSERT INTO Diagnose VALUES ('14534','   Ischemic (asceptic) necrosis      ')\g
INSERT INTO Diagnose VALUES ('14539','   Other      ')\g
INSERT INTO Diagnose VALUES ('1454','    Burr hole      ')\g
INSERT INTO Diagnose VALUES ('1455','    Decompression procedure      ')\g
INSERT INTO Diagnose VALUES ('14551','   Strip craniectomy for premature suture closure      ')\g
INSERT INTO Diagnose VALUES ('14559','   Other      ')\g
INSERT INTO Diagnose VALUES ('1456','    Resection of brain tissue      ')\g
INSERT INTO Diagnose VALUES ('1457','    Aneurysm clip      ')\g
INSERT INTO Diagnose VALUES ('1458','    Complication of surgery or interventional procedure  include: of biopsy, of aspiration   exclude: of catheterization (.44), of angiography (.44)      ')\g
INSERT INTO Diagnose VALUES ('1459','    Other   exclude: hematoma (.433), arteriovenous fistula (.494)      ')\g
INSERT INTO Diagnose VALUES ('146','     FOREIGN BODY, MEDICATION      ')\g
INSERT INTO Diagnose VALUES ('1461','    Opaque      ')\g
INSERT INTO Diagnose VALUES ('14611','   Catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('14612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('14613','   Catheter (or other tube) in unsatisfactory position      ')\g
INSERT INTO Diagnose VALUES ('14617','   Complication of catheter (or other tube)      ')\g
INSERT INTO Diagnose VALUES ('14619','   Other  exclude: cerebrospinal fluid shunt tube (.451)      ')\g
INSERT INTO Diagnose VALUES ('1462','    Nonopaque      ')\g
INSERT INTO Diagnose VALUES ('1469','    Other      ')\g
INSERT INTO Diagnose VALUES ('147','     EFFECT OF RADIATION      ')\g
INSERT INTO Diagnose VALUES ('149','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('1491','    Scalp laceration or swelling, traumatic or postoperative      ')\g
INSERT INTO Diagnose VALUES ('1492','    Scalp avulsion      ')\g
INSERT INTO Diagnose VALUES ('1493','    Subgaleal hematoma      ')\g
INSERT INTO Diagnose VALUES ('1494','    Arteriovenous fistula (see also .75)  include: post-traumatic, postoperative, complication of angiography      ')\g
INSERT INTO Diagnose VALUES ('1495','    Cephalhematoma      ')\g
INSERT INTO Diagnose VALUES ('1499','    Other   include: soft tissue emphysema, intravascular gas  exclude: traumatic pseudoaneurysm (.4315)      ')\g
INSERT INTO Diagnose VALUES ('15','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('151','     PITUITARY DISORDER      ')\g
INSERT INTO Diagnose VALUES ('1511','    Gigantism (see also .372)   exclude: cerebral gigantism (.171)      ')\g
INSERT INTO Diagnose VALUES ('1512','    Acromegaly (see also .372)      ')\g
INSERT INTO Diagnose VALUES ('1513','    Pituitary dwarfism      ')\g
INSERT INTO Diagnose VALUES ('1519','    Other  include: psychosocial dwarfism (emotional deprivation)      ')\g
INSERT INTO Diagnose VALUES ('152','     THYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('1521','    Cretinism, hypothyroidism      ')\g
INSERT INTO Diagnose VALUES ('1529','    Other      ')\g
INSERT INTO Diagnose VALUES ('153','     PARATHYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('1531','    Primary hyperparathyroidism      ')\g
INSERT INTO Diagnose VALUES ('1532','    Secondary hyperparathyroidism, nonrenal  exclude: renal (.57)      ')\g
INSERT INTO Diagnose VALUES ('1533','    Hypoparathyroidism      ')\g
INSERT INTO Diagnose VALUES ('1534','    Albright hereditary osteodystrophy (pseudohypoparathyroidism)      ')\g
INSERT INTO Diagnose VALUES ('1539','    Other   include: congenital hyperparathyroidism  exclude: renal osteodystrophy (.573)      ')\g
INSERT INTO Diagnose VALUES ('154','     ADRENAL DISORDER   include: Cushing syndrome      ')\g
INSERT INTO Diagnose VALUES ('156','     OSTEOPOROSIS      ')\g
INSERT INTO Diagnose VALUES ('157','     OSTEOMALACIA, RENAL OSTEODYSTROPHY      ')\g
INSERT INTO Diagnose VALUES ('158','     INTOXICATION, POISONING      ')\g
INSERT INTO Diagnose VALUES ('1581','    Heavy metal      ')\g
INSERT INTO Diagnose VALUES ('15811','   Lead (see also causing cerebral edema 862)      ')\g
INSERT INTO Diagnose VALUES ('15819','   Other      ')\g
INSERT INTO Diagnose VALUES ('1582','    Fluorine      ')\g
INSERT INTO Diagnose VALUES ('1583','    Hypervitaminosis      ')\g
INSERT INTO Diagnose VALUES ('15831','   Vitamin A (see also causing cerebral edema .862, pseudotumor cerebri .369)      ')\g
INSERT INTO Diagnose VALUES ('15839','   Other      ')\g
INSERT INTO Diagnose VALUES ('1589','    Other   exclude: carbon monoxide encephalopathy (.5912)      ')\g
INSERT INTO Diagnose VALUES ('159','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('1591','    Anoxic encephalopathies (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('15911','   Anoxia      ')\g
INSERT INTO Diagnose VALUES ('15912','   Carbon monoxide      ')\g
INSERT INTO Diagnose VALUES ('15919','   Other   exclude: from drug overdose (.64)      ')\g
INSERT INTO Diagnose VALUES ('1592','    Lenticular degeneration in Wilson disease  exclude: bone changes (4.5729, 4.789)      ')\g
INSERT INTO Diagnose VALUES ('1593','    Hypophosphatasia      ')\g
INSERT INTO Diagnose VALUES ('1599','    Other   include: idiopathic hypercalcemia of infancy  exclude: hyperphosphatasemia (.1555)      ')\g
INSERT INTO Diagnose VALUES ('16','      OTHER GENERALIZED SYSTEMIC DISORDER      ')\g
INSERT INTO Diagnose VALUES ('161','     SYSTEMIC CONNECTIVE TISSUE DISORDER   include: lupus erythematosus      ')\g
INSERT INTO Diagnose VALUES ('162','     VASCULITIS  (for 5th number see box following .629)      ')\g
INSERT INTO Diagnose VALUES ('1621','    Polyarteritis (periarteritis) nodosa      ')\g
INSERT INTO Diagnose VALUES ('1625','    Takayasu arteritis and related states      ')\g
INSERT INTO Diagnose VALUES ('1626','    Drug abuse      ')\g
INSERT INTO Diagnose VALUES ('1629','    Other   include: idiopathic arteritis of childhood, mucocutaneous lymph node syndrome   exclude: infectious (.258), lupus arteritis (.61)      ')\g
INSERT INTO Diagnose VALUES ('164','     COMPLICATION OF DRUG THERAPY, DRUG USE   include: anoxic encephalopathy from drug overdose, calvarial thickening from phenytoin (Dilantin)      ')\g
INSERT INTO Diagnose VALUES ('165','     HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('1651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('1652','    Thalassemia or variant      ')\g
INSERT INTO Diagnose VALUES ('1653','    Other congenital (primary) anemia  include: spherocytosis      ')\g
INSERT INTO Diagnose VALUES ('1654','    Iron deficiency anemia      ')\g
INSERT INTO Diagnose VALUES ('1655','    Other acquired (secondary) anemia      ')\g
INSERT INTO Diagnose VALUES ('1657','    Myeloid metaplasia (myelosclerosis)      ')\g
INSERT INTO Diagnose VALUES ('1658','    Disseminated intravascular coagulation      ')\g
INSERT INTO Diagnose VALUES ('1659','    Other   include: polycythemia, complications of anticoagulant therapy, extramedullary  hematopoiesis   exclude: leukemia (.34)      ')\g
INSERT INTO Diagnose VALUES ('166','     HISTIOCYTOSIS (Langerhans cell histiocytosis)      ')\g
INSERT INTO Diagnose VALUES ('1661','    Letterer-Siwe disease      ')\g
INSERT INTO Diagnose VALUES ('1662','    Hand-Schuller-Christian disease      ')\g
INSERT INTO Diagnose VALUES ('1663','    Eosinophilic granuloma      ')\g
INSERT INTO Diagnose VALUES ('1669','    Other      ')\g
INSERT INTO Diagnose VALUES ('167','     SPHINGOLIPIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('1671','    Gaucher disease      ')\g
INSERT INTO Diagnose VALUES ('1672','    Niemann-Pick disease      ')\g
INSERT INTO Diagnose VALUES ('1673','    Tay-Sachs disease      ')\g
INSERT INTO Diagnose VALUES ('1679','    Other   include: Fabry disease      ')\g
INSERT INTO Diagnose VALUES ('168','     AMYLOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('169','     OTHER   include: mastocytosis (urticaria pigmentosa)      ')\g
INSERT INTO Diagnose VALUES ('17','      VASCULAR DISORDER      ')\g
INSERT INTO Diagnose VALUES ('172','     PRIMARY ARTERIAL DISORDER (for 5th number see box following .729)  exclude: aneurysm (.73), congenital arteriovenous  malformation or fistula (.75), vasculitis (.62)      ')\g
INSERT INTO Diagnose VALUES ('1721','    Atherosclerotic stenosis, hypertension      ')\g
INSERT INTO Diagnose VALUES ('17211','   Stenosis less than 50%      ')\g
INSERT INTO Diagnose VALUES ('17212','   Stenosis 50-80%      ')\g
INSERT INTO Diagnose VALUES ('17213','   Stenosis greater than 80%      ')\g
INSERT INTO Diagnose VALUES ('17214','   Occlusion      ')\g
INSERT INTO Diagnose VALUES ('17215','   Ulcerated plaque      ')\g
INSERT INTO Diagnose VALUES ('17216','   With hematoma   exclude: spontaneous (.367)      ')\g
INSERT INTO Diagnose VALUES ('17219','   Other      ')\g
INSERT INTO Diagnose VALUES ('1722','    Fibromuscular disorder      ')\g
INSERT INTO Diagnose VALUES ('17221','   Intimal fibroplasia      ')\g
INSERT INTO Diagnose VALUES ('17222','   Medial fibroplasia      ')\g
INSERT INTO Diagnose VALUES ('17223','   Subadventitial fibroplasia      ')\g
INSERT INTO Diagnose VALUES ('17224','   Fibromuscular hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('17229','   Other      ')\g
INSERT INTO Diagnose VALUES ('1729','    Other      ')\g
INSERT INTO Diagnose VALUES ('173','     ANEURYSM (for 5th number see box following .739)  exclude: traumatic pseudoaneurysm (.4315)      ')\g
INSERT INTO Diagnose VALUES ('1731','    Atherosclerotic      ')\g
INSERT INTO Diagnose VALUES ('1733','    Mycotic      ')\g
INSERT INTO Diagnose VALUES ('17331','   Multiple      ')\g
INSERT INTO Diagnose VALUES ('1734','    Syphilitic      ')\g
INSERT INTO Diagnose VALUES ('1735','    Generalized atherosclerotic ectasia      ')\g
INSERT INTO Diagnose VALUES ('1736','    Congenital      ')\g
INSERT INTO Diagnose VALUES ('17361','   Multiple      ')\g
INSERT INTO Diagnose VALUES ('17362','   Giant      ')\g
INSERT INTO Diagnose VALUES ('17363','   Fusiform      ')\g
INSERT INTO Diagnose VALUES ('17369','   Other      ')\g
INSERT INTO Diagnose VALUES ('1739','    Other   exclude: traumatic dissecting aneurysm (.4314), other intramural hematoma with dissection (dissecting aneurysm) (.74)      ')\g
INSERT INTO Diagnose VALUES ('174','     INTRAMURAL HEMATOMA WITH DISSECTION (DISSECTING ANEURYSM)   exclude: traumatic (.4314), complication of catheterization or angiography (.444)      ')\g
INSERT INTO Diagnose VALUES ('175','     ARTERIOVENOUS MALFORMATION OR FISTULA, ANGIOGRAPHIC MANIFESTATIONS   exclude: postoperative (.494) or post-traumatic arteriovenous fistula (.494)      ')\g
INSERT INTO Diagnose VALUES ('1751','    Partial thrombosis      ')\g
INSERT INTO Diagnose VALUES ('1752','    Total thrombosis      ')\g
INSERT INTO Diagnose VALUES ('1753','    Associated with aneurysm      ')\g
INSERT INTO Diagnose VALUES ('1754','    Associated with hematoma      ')\g
INSERT INTO Diagnose VALUES ('1755','    Associated with spasm      ')\g
INSERT INTO Diagnose VALUES ('1756','    Associated with a steal" syndrome"      ')\g
INSERT INTO Diagnose VALUES ('1757','    Status post-therapy   include: embolization      ')\g
INSERT INTO Diagnose VALUES ('1758','    More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('1759','    Other      ')\g
INSERT INTO Diagnose VALUES ('176','     LOW-FLOW STATE (for 5th number see box following .769)      ')\g
INSERT INTO Diagnose VALUES ('1761','    Increased intracranial pressure (angiographic manifestation)   include: slow or absent cerebral blood flow (brain death)      ')\g
INSERT INTO Diagnose VALUES ('1762','    Vasospasm (cause unknown)      ')\g
INSERT INTO Diagnose VALUES ('1767','    Steal" syndrome   include: subclavian steal (see also 9.767)"      ')\g
INSERT INTO Diagnose VALUES ('1769','    Other      ')\g
INSERT INTO Diagnose VALUES ('177','     EMBOLUS (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('1771','    With infarct      ')\g
INSERT INTO Diagnose VALUES ('1772','    Without infarct      ')\g
INSERT INTO Diagnose VALUES ('1779','    Other      ')\g
INSERT INTO Diagnose VALUES ('178','     INFARCT (see also .769, .771)      ')\g
INSERT INTO Diagnose VALUES ('1781','    Ischemic      ')\g
INSERT INTO Diagnose VALUES ('1782','    Hemorrhagic      ')\g
INSERT INTO Diagnose VALUES ('179','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('18','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('181','     CALCIFICATION, OSSIFICATION (also code cause)  include: abnormal intracranial calcification   exclude: physiological calcification (.134)      ')\g
INSERT INTO Diagnose VALUES ('182','     HYDROCEPHALUS (also code cause)  exclude: atrophic (.83), congenital (.145)      ')\g
INSERT INTO Diagnose VALUES ('1821','    Obstructive, noncommunicating      ')\g
INSERT INTO Diagnose VALUES ('1822','    Obstructive, communicating      ')\g
INSERT INTO Diagnose VALUES ('1823','    Normal pressure   include: low pressure" hydrocephalus"      ')\g
INSERT INTO Diagnose VALUES ('1828','    More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('1829','    Other      ')\g
INSERT INTO Diagnose VALUES ('183','     ATROPHY (also code cause)   include: Alzheimer disease, Pick disease      ')\g
INSERT INTO Diagnose VALUES ('1831','    Focal cortical      ')\g
INSERT INTO Diagnose VALUES ('1832','    Focal central   include: nuclei      ')\g
INSERT INTO Diagnose VALUES ('1833','    Diffuse cortical      ')\g
INSERT INTO Diagnose VALUES ('1834','    Diffuse central   include: nuclei      ')\g
INSERT INTO Diagnose VALUES ('1835','    Porencephaly      ')\g
INSERT INTO Diagnose VALUES ('18351','   Communicating      ')\g
INSERT INTO Diagnose VALUES ('18352','   Noncommunicating      ')\g
INSERT INTO Diagnose VALUES ('1836','    Jakob-Creutzfeldt disease      ')\g
INSERT INTO Diagnose VALUES ('1837','    Multi-infarct dementia      ')\g
INSERT INTO Diagnose VALUES ('1839','    Other      ')\g
INSERT INTO Diagnose VALUES ('184','     PAGET DISEASE      ')\g
INSERT INTO Diagnose VALUES ('185','     FIBROUS DYSPLASIA      ')\g
INSERT INTO Diagnose VALUES ('186','     CEREBRAL EDEMA (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('1861','    Focal      ')\g
INSERT INTO Diagnose VALUES ('1862','    Diffuse   exclude: pseudotumor cerebri (.368), secondary to trauma (.436)      ')\g
INSERT INTO Diagnose VALUES ('187','     DEGENERATIVE DISEASE OF WHITE MATTER      ')\g
INSERT INTO Diagnose VALUES ('1871','    Primary demyelinating disease  include:  multiple sclerosis      ')\g
INSERT INTO Diagnose VALUES ('1872','    Secondary demyelinating diseases      ')\g
INSERT INTO Diagnose VALUES ('18721','   Leukoencephalopathy secondary to an infectious agent  include: progressive multifocal leukoencephalopathy, subacute sclerosing, panencephalomyelitis, acute.  Disseminated encephalomyelitis      ')\g
INSERT INTO Diagnose VALUES ('1873','    Dysmyelinating diseases (leukodystrophy)      ')\g
INSERT INTO Diagnose VALUES ('18731','   Metachromatic leukodystrophy (arylsulfatase A deficiency)      ')\g
INSERT INTO Diagnose VALUES ('18732','   Krabbe disease (galactocerebrosidase B deficiency)      ')\g
INSERT INTO Diagnose VALUES ('18733','   Adrenoleukodystrophy (long-chain fatty acid metabolism)      ')\g
INSERT INTO Diagnose VALUES ('18734','   Canavan disease      ')\g
INSERT INTO Diagnose VALUES ('18735','   Maple syrup urine disease      ')\g
INSERT INTO Diagnose VALUES ('18736','   Leigh disease      ')\g
INSERT INTO Diagnose VALUES ('18737','   Alexander disease      ')\g
INSERT INTO Diagnose VALUES ('18739','   Other   include: Batten-Mayou disease,  Pelizaeus-Merzbacher disease      ')\g
INSERT INTO Diagnose VALUES ('1879','    Other      ')\g
INSERT INTO Diagnose VALUES ('188','     DEGENERATIVE DISEASE OF GRAY MATTER      ')\g
INSERT INTO Diagnose VALUES ('1881','    Parkinson disease      ')\g
INSERT INTO Diagnose VALUES ('1882','    Cerebellar degenerative disease, alcoholic      ')\g
INSERT INTO Diagnose VALUES ('1883','    Oligopontine cerebellar degeneration      ')\g
INSERT INTO Diagnose VALUES ('1884','    Huntington disease      ')\g
INSERT INTO Diagnose VALUES ('1889','    Other      ')\g
INSERT INTO Diagnose VALUES ('189','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('1891','    Degenerative disease of both gray and white matter  include: incontinentia pigmenta      ')\g
INSERT INTO Diagnose VALUES ('1899','    Other      ')\g
INSERT INTO Diagnose VALUES ('19','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('191','     FUNDAMENTAL OBSERVATION      ')\g
INSERT INTO Diagnose VALUES ('1911','    Widening of suture, increased digital markings      ')\g
INSERT INTO Diagnose VALUES ('1912','    Displacement of physiological calcification, pineal gland, midline of brain      ')\g
INSERT INTO Diagnose VALUES ('1913','    Enlarged vascular groove      ')\g
INSERT INTO Diagnose VALUES ('1914','    Secondary abnormality of sella turcica      ')\g
INSERT INTO Diagnose VALUES ('1915','    Erosion of adjacent bone      ')\g
INSERT INTO Diagnose VALUES ('1916','    Hyperostosis (see .4379)      ')\g
INSERT INTO Diagnose VALUES ('1918','    More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('1919','    Other   exclude: abnormal intracranial calcification (.81)      ')\g
INSERT INTO Diagnose VALUES ('192','     ANATOMICAL DETAIL      ')\g
INSERT INTO Diagnose VALUES ('193','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('194','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('199','     OTHER  include: phenomenon related to examination technique      ')\g
INSERT INTO Diagnose VALUES ('21','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('211','     NORMAL ROUTINE PLAIN FILM   exclude: normal variant (.13), dental film (.127)      ')\g
INSERT INTO Diagnose VALUES ('212','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION      ')\g
INSERT INTO Diagnose VALUES ('2121','    Digital radiographic techniques, tomography, MR, nuclear medicine  exclude: dental panoramic projection (.1274)      ')\g
INSERT INTO Diagnose VALUES ('21211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('212111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('212112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('212113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('212114','  Delayed scanning following enhancement      ')\g
INSERT INTO Diagnose VALUES ('212115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('212116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('212117','  Three-dimensional reconstruction      ')\g
INSERT INTO Diagnose VALUES ('212118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('212119','  Other, including serial enhancement studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('21214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('212141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('2121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('2121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('2121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('2121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('2121415',' Specific resonance suppressed; include:  fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('2121416',' High speed imaging   include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('2121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('2121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('212142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('212143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('212144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('212145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('212146','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('212147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('212149','  Other      ')\g
INSERT INTO Diagnose VALUES ('21215','   Digital radiography   exclude: with angiography .124-.3, 9.12-3      ')\g
INSERT INTO Diagnose VALUES ('21216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('212161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('212162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('212163','  PET      ')\g
INSERT INTO Diagnose VALUES ('212164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('212165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('212166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('212167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('212168','  Therapeutic procedure  include: use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('212169','  Other.  See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('21217','   Nuclear Medicine study-neck      ')\g
INSERT INTO Diagnose VALUES ('212171','  Thyroid uptake      ')\g
INSERT INTO Diagnose VALUES ('212172','  Thyroid imaging      ')\g
INSERT INTO Diagnose VALUES ('212173','  Metabolism study      ')\g
INSERT INTO Diagnose VALUES ('212174','  Thyroid carcinoma imaging      ')\g
INSERT INTO Diagnose VALUES ('212175','  Parathyroid imaging      ')\g
INSERT INTO Diagnose VALUES ('212176','  Salivary gland imaging      ')\g
INSERT INTO Diagnose VALUES ('212179','  Other.  See also 1.12177-Radionuclide dacrocystography      ')\g
INSERT INTO Diagnose VALUES ('21218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('21219','   Other      ')\g
INSERT INTO Diagnose VALUES ('2122','    Laryngography, sialography      ')\g
INSERT INTO Diagnose VALUES ('21221','   Laryngography      ')\g
INSERT INTO Diagnose VALUES ('21222','   Sialography      ')\g
INSERT INTO Diagnose VALUES ('2123','    Oropharyngeal function study (see .82)  include: swallowing, phonation      ')\g
INSERT INTO Diagnose VALUES ('2124','    Angiography (see also 9.)      ')\g
INSERT INTO Diagnose VALUES ('2125','    Special nonroutine projection      ')\g
INSERT INTO Diagnose VALUES ('2126','    Biopsy or interventional      ')\g
INSERT INTO Diagnose VALUES ('21261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('21262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('21263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('21264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('21265','   Thrombolytic infusion      ')\g
INSERT INTO Diagnose VALUES ('21266','   Chemotherapeutic infusion      ')\g
INSERT INTO Diagnose VALUES ('21267','   Operative procedure  include: stereotaxis      ')\g
INSERT INTO Diagnose VALUES ('21269','   Other      ')\g
INSERT INTO Diagnose VALUES ('2127','    Dental film, dental age determination      ')\g
INSERT INTO Diagnose VALUES ('21271','   Periapical      ')\g
INSERT INTO Diagnose VALUES ('21272','   Interproximal survey (bite-wing)      ')\g
INSERT INTO Diagnose VALUES ('21273','   Occlusal      ')\g
INSERT INTO Diagnose VALUES ('21274','   Panoramic      ')\g
INSERT INTO Diagnose VALUES ('21275','   Dental age determination      ')\g
INSERT INTO Diagnose VALUES ('21276','   Other      ')\g
INSERT INTO Diagnose VALUES ('2128','    Foreign body localization      ')\g
INSERT INTO Diagnose VALUES ('2129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('21291','   Therapy localization (port film)      ')\g
INSERT INTO Diagnose VALUES ('21292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('21293','   Orbitography      ')\g
INSERT INTO Diagnose VALUES ('21295','   Dacrocystography      ')\g
INSERT INTO Diagnose VALUES ('21298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('212981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('212982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('212983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('212984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('212985','  Guided procedure for diagnosis   include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('212986','  Guided procedure for therapy   include: drainage      ')\g
INSERT INTO Diagnose VALUES ('212987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('212988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('212989','  Special nonroutine projection, mode, or transducer type      ')\g
INSERT INTO Diagnose VALUES ('21299','   Other   include: xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('213','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('2131','    Unusually large or extensive paranasal sinus, mastoid   include: unusual region pneumatized by paranasal sinus, mastoid      ')\g
INSERT INTO Diagnose VALUES ('2132','    Hypoplasia or aplasia of paranasal sinus  exclude: poorly pneumatized or infantile mastoid, sclerotic mastoiditis (.262)      ')\g
INSERT INTO Diagnose VALUES ('2133','    Density variant   include: physiological osteosclerosis of newborn      ')\g
INSERT INTO Diagnose VALUES ('2134','    Dental normal variant      ')\g
INSERT INTO Diagnose VALUES ('21341','   Germinal center      ')\g
INSERT INTO Diagnose VALUES ('21342','   Pulp stone      ')\g
INSERT INTO Diagnose VALUES ('21349','   Other   exclude: ectopic eruption (.1473), delayed dentition (.551), precocious dentition (.552), supernumerary tooth (.1456)      ')\g
INSERT INTO Diagnose VALUES ('2135','    Torus      ')\g
INSERT INTO Diagnose VALUES ('21351','   Mandibularis      ')\g
INSERT INTO Diagnose VALUES ('21352','   Palatinus      ')\g
INSERT INTO Diagnose VALUES ('21353','   Alveolar crest      ')\g
INSERT INTO Diagnose VALUES ('21359','   Other   exclude: exostosis (.311)      ')\g
INSERT INTO Diagnose VALUES ('2136','    Enlarged nasopalatine canal      ')\g
INSERT INTO Diagnose VALUES ('2139','    Other      ')\g
INSERT INTO Diagnose VALUES ('214','     CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('2141','    Aplasia, agenesis   include: absent tooth, ectodermal dysplasia   exclude: of paranasal sinus (.132)      ')\g
INSERT INTO Diagnose VALUES ('2142','    Hypoplasia   include: microdontia  exclude: of enamel (.861), of paranasal sinus (.132), hypotelorism (.1432), dentinogenesis imperfecta (.1551), Treacher Collins syndrome (.1664), Pierre Robin syndrome (.1665), dysostosis (.166)      ')\g
INSERT INTO Diagnose VALUES ('21421','   Unilateral      ')\g
INSERT INTO Diagnose VALUES ('21422','   Bilateral      ')\g
INSERT INTO Diagnose VALUES ('2143','    Abnormal interorbital distance      ')\g
INSERT INTO Diagnose VALUES ('21431','   Hypertelorism (see .1485)      ')\g
INSERT INTO Diagnose VALUES ('21432','   Hypotelorism (see 1.1412)      ')\g
INSERT INTO Diagnose VALUES ('2144','    Hyperplasia, hypersegmentation   include: macrodontia, prognathism   exclude: normal variant (.131), supernumerary tooth (.1456)      ')\g
INSERT INTO Diagnose VALUES ('21441','   Unilateral      ')\g
INSERT INTO Diagnose VALUES ('21442','   Bilateral      ')\g
INSERT INTO Diagnose VALUES ('2145','    Impacted, unerupted, supernumerary tooth      ')\g
INSERT INTO Diagnose VALUES ('21451','   Impacted third molar      ')\g
INSERT INTO Diagnose VALUES ('21452','   Impacted cuspid      ')\g
INSERT INTO Diagnose VALUES ('21453','   Other impaction      ')\g
INSERT INTO Diagnose VALUES ('21454','   Unerupted tooth      ')\g
INSERT INTO Diagnose VALUES ('21455','   Ankylosed or submerged" tooth"      ')\g
INSERT INTO Diagnose VALUES ('21456','   Supernumerary tooth      ')\g
INSERT INTO Diagnose VALUES ('21459','   Other      ')\g
INSERT INTO Diagnose VALUES ('2146','    Miscellaneous dental anomaly      ')\g
INSERT INTO Diagnose VALUES ('21461','   Peg lateral incisor      ')\g
INSERT INTO Diagnose VALUES ('21462','   Dens in dente      ')\g
INSERT INTO Diagnose VALUES ('21463','   Ectopic eruption      ')\g
INSERT INTO Diagnose VALUES ('21464','   Taurodontia      ')\g
INSERT INTO Diagnose VALUES ('21469','   Other   exclude: fusion (.1491), microdontia (.142), macrodontia (.144), precocious dentition (.552), delayed dentition (.551)      ')\g
INSERT INTO Diagnose VALUES ('2147','    Branchial cleft anomaly      ')\g
INSERT INTO Diagnose VALUES ('21471','   Branchial cleft cyst      ')\g
INSERT INTO Diagnose VALUES ('21472','   Branchial cleft sinus or fistula      ')\g
INSERT INTO Diagnose VALUES ('21473','   Thyroglossal cyst      ')\g
INSERT INTO Diagnose VALUES ('21474','   Thyroglossal sinus or fistula      ')\g
INSERT INTO Diagnose VALUES ('21475','   Aberrant thyroid      ')\g
INSERT INTO Diagnose VALUES ('21476','   Aberrant tonsil      ')\g
INSERT INTO Diagnose VALUES ('21479','   Other  include: pyriform sinus fistula      ')\g
INSERT INTO Diagnose VALUES ('2148','    Cleft lip, palate, face (see also 1.1332, 1.1412)      ')\g
INSERT INTO Diagnose VALUES ('21481','   Cleft lip      ')\g
INSERT INTO Diagnose VALUES ('21482','   Cleft palate      ')\g
INSERT INTO Diagnose VALUES ('21483','   Unilateral cleft lip and palate      ')\g
INSERT INTO Diagnose VALUES ('21484','   Bilateral cleft lip and palate      ')\g
INSERT INTO Diagnose VALUES ('21485','   Median cleft face, frontonasal dysplasia  exclude: holoprosencephaly (1.1412), cranium bifidum occultum (1.1332)      ')\g
INSERT INTO Diagnose VALUES ('21489','   Other      ')\g
INSERT INTO Diagnose VALUES ('2149','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('21491','   Fusion      ')\g
INSERT INTO Diagnose VALUES ('21492','   Stenosis   include: laryngeal, choanal      ')\g
INSERT INTO Diagnose VALUES ('21493','   Atresia   include: choanal      ')\g
INSERT INTO Diagnose VALUES ('21494','   Congenital dislocation      ')\g
INSERT INTO Diagnose VALUES ('21495','   Congenital arteriovenous fistula      ')\g
INSERT INTO Diagnose VALUES ('21496','   Laryngomalacia, tracheomalacia      ')\g
INSERT INTO Diagnose VALUES ('21499','   Other   include: laryngeal cyst, cleft      ')\g
INSERT INTO Diagnose VALUES ('215','     DYSPLASIA (see 4.15 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('21524','   Cleidocranial dysplasia      ')\g
INSERT INTO Diagnose VALUES ('21545','   Cherubism   exclude: fibrous dysplasia (.85)      ')\g
INSERT INTO Diagnose VALUES ('21551','   Dentinogenesis imperfecta, osteogenesis imperfecta      ')\g
INSERT INTO Diagnose VALUES ('21557','   Oculo-dento-osseous dysplasia      ')\g
INSERT INTO Diagnose VALUES ('2159','    Other   include: amelogenesis imperfecta  exclude: ectodermal dysplasia (.141), frontonasal dysplasia (.1485)      ')\g
INSERT INTO Diagnose VALUES ('216','     DYSOSTOSIS (see 4.16 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('2161','    Basal cell nevus syndrome      ')\g
INSERT INTO Diagnose VALUES ('2166','    Facial dysostosis      ')\g
INSERT INTO Diagnose VALUES ('21661','   Oculomandibulofacial (Hallerman-Streiff) syndrome      ')\g
INSERT INTO Diagnose VALUES ('21662','   Goldenhar syndrome      ')\g
INSERT INTO Diagnose VALUES ('21663','   Hemifacial microsomia, other      ')\g
INSERT INTO Diagnose VALUES ('21664','   Mandibulofacial dysostosis (Treacher-Collins syndrome)      ')\g
INSERT INTO Diagnose VALUES ('21665','   Pierre Robin anomaly      ')\g
INSERT INTO Diagnose VALUES ('21669','   Other      ')\g
INSERT INTO Diagnose VALUES ('2167','    Dysostosis involving many systems      ')\g
INSERT INTO Diagnose VALUES ('21671','   Basal cell nevus syndrome (Gorlin syndrome)      ')\g
INSERT INTO Diagnose VALUES ('21679','   Other      ')\g
INSERT INTO Diagnose VALUES ('2169','    Other   exclude: hypoglossia-hypodactylia syndrome  (4.1632), oro-facial-digital syndrome (4.1631), whistling-face syndrome (4.1629), with craniosynostosis (1.143), oculo-dento-osseous dysplasia (4.1557)      ')\g
INSERT INTO Diagnose VALUES ('217','     PRIMARY DISTURBANCE OF GROWTH (see 4.17 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('218','     OTHER CONSTITUTIONAL DISORDER (see 4.18 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('2181','    Mucopolysaccharidosis      ')\g
INSERT INTO Diagnose VALUES ('2182','    Dysostosis multiplex, other cause (see 4.182 for details)      ')\g
INSERT INTO Diagnose VALUES ('2183','    Neurocutaneous syndrome (phacomatosis)      ')\g
INSERT INTO Diagnose VALUES ('21831','   Neurofibromatosis      ')\g
INSERT INTO Diagnose VALUES ('21839','   Other      ')\g
INSERT INTO Diagnose VALUES ('2184','    Autosomal aberration   include: trisomy 21 (Down syndrome)      ')\g
INSERT INTO Diagnose VALUES ('2185','    Sex chromosomal aberration      ')\g
INSERT INTO Diagnose VALUES ('2189','    Other      ')\g
INSERT INTO Diagnose VALUES ('219','     OTHER   exclude: bird-headed dwarfism (4.1729)      ')\g
INSERT INTO Diagnose VALUES ('22','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('220','     INFECTION CLASSIFIED BY ORGANISM (see 6.20 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('221','     OSTEOMYELITIS      ')\g
INSERT INTO Diagnose VALUES ('2211','    Typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('2212','    Unusual manifestation      ')\g
INSERT INTO Diagnose VALUES ('2214','    Associated with systemic disease  include: fungus, parasite, virus, spirochete      ')\g
INSERT INTO Diagnose VALUES ('2216','    Secondary to trauma   include: postoperative      ')\g
INSERT INTO Diagnose VALUES ('2217','    Opportunistic infection      ')\g
INSERT INTO Diagnose VALUES ('2218','    Bone reaction secondary to soft tissue infection      ')\g
INSERT INTO Diagnose VALUES ('2219','    Miscellaneous   exclude: tuberculosis (.23), radiation osteitis (.47), dental infection (.27), Wegener granulomatosis (.62)      ')\g
INSERT INTO Diagnose VALUES ('222','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('223','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('224','     SOFT TISSUE INFLAMMATION, SINUS TRACT, FISTULA      ')\g
INSERT INTO Diagnose VALUES ('2241','    Cellulitis, diffuse infection, myositis  include: granuloma   exclude: epiglottitis (2.248)      ')\g
INSERT INTO Diagnose VALUES ('2242','    Abscess, gas gangrene   include: retropharyngeal abscess      ')\g
INSERT INTO Diagnose VALUES ('2243','    Croup      ')\g
INSERT INTO Diagnose VALUES ('2244','    Sinus tract   exclude: congenital (.147)      ')\g
INSERT INTO Diagnose VALUES ('2245','    Fistula   include: laceration of salivary or lacrimal duct   exclude: congenital (.147)      ')\g
INSERT INTO Diagnose VALUES ('2246','    Lymphadenitis      ')\g
INSERT INTO Diagnose VALUES ('2247','    Sialectasis and related conditions      ')\g
INSERT INTO Diagnose VALUES ('22471','   Sialectasis      ')\g
INSERT INTO Diagnose VALUES ('22472','   Sialadenitis      ')\g
INSERT INTO Diagnose VALUES ('22473','   Sialodochitis      ')\g
INSERT INTO Diagnose VALUES ('22474','   Allergic parotitis      ')\g
INSERT INTO Diagnose VALUES ('22479','   Other      ')\g
INSERT INTO Diagnose VALUES ('2248','    Epiglottitis      ')\g
INSERT INTO Diagnose VALUES ('2249','    Other      ')\g
INSERT INTO Diagnose VALUES ('225','     SINUSITIS   (for 4th number see box following .269)      ')\g
INSERT INTO Diagnose VALUES ('2251','    Thickened mucosa      ')\g
INSERT INTO Diagnose VALUES ('2252','    Opaque sinus      ')\g
INSERT INTO Diagnose VALUES ('2253','    Fluid level      ')\g
INSERT INTO Diagnose VALUES ('2254','    Cyst (pseudopolyp)      ')\g
INSERT INTO Diagnose VALUES ('2255','    Mucocele      ')\g
INSERT INTO Diagnose VALUES ('2256','    Loss of mucoperiosteum      ')\g
INSERT INTO Diagnose VALUES ('2257','    Condensing osteitis      ')\g
INSERT INTO Diagnose VALUES ('2258','    Allergic      ')\g
INSERT INTO Diagnose VALUES ('2259','    Other   include: soft tissue swelling simulating sinusitis (see immotile cilia syndrome 6.266)      ')\g
INSERT INTO Diagnose VALUES ('226','     MASTOIDITIS (for 4th number see box following .269)      ')\g
INSERT INTO Diagnose VALUES ('22601','   Acute      ')\g
INSERT INTO Diagnose VALUES ('22602','   Subacute      ')\g
INSERT INTO Diagnose VALUES ('22603','   Chronic      ')\g
INSERT INTO Diagnose VALUES ('2261','    Opaque mastoid air cells and/or middle ear      ')\g
INSERT INTO Diagnose VALUES ('2262','    Poorly pneumatized or infantile mastoid, sclerotic mastoiditis      ')\g
INSERT INTO Diagnose VALUES ('2263','    Abscess, sequestrum      ')\g
INSERT INTO Diagnose VALUES ('2264','    Cholesteatoma      ')\g
INSERT INTO Diagnose VALUES ('2265','    Alteration in tegmen tympani      ')\g
INSERT INTO Diagnose VALUES ('2266','    Alteration of squamosa of temporal bone      ')\g
INSERT INTO Diagnose VALUES ('2267','    Alteration of ossicle(s)      ')\g
INSERT INTO Diagnose VALUES ('2268','    Alteration in sinodural plate      ')\g
INSERT INTO Diagnose VALUES ('2269','    Other      ')\g
INSERT INTO Diagnose VALUES ('227','     DENTAL INFECTION      ')\g
INSERT INTO Diagnose VALUES ('2271','    Caries      ')\g
INSERT INTO Diagnose VALUES ('22711','   Enamel caries      ')\g
INSERT INTO Diagnose VALUES ('22712','   Dentinoenamel junction caries      ')\g
INSERT INTO Diagnose VALUES ('22713','   Deep caries, pulp exposure      ')\g
INSERT INTO Diagnose VALUES ('22714','   Cemental caries      ')\g
INSERT INTO Diagnose VALUES ('22719','   Other      ')\g
INSERT INTO Diagnose VALUES ('2272','    Periodontal disease, pyorrhea""      ')\g
INSERT INTO Diagnose VALUES ('22721','   Localized periodontal disease, horizontal      ')\g
INSERT INTO Diagnose VALUES ('22722','   Localized periodontal disease, vertical      ')\g
INSERT INTO Diagnose VALUES ('22723','   Generalized peridontal disease, horizontal      ')\g
INSERT INTO Diagnose VALUES ('22724','   Generalized periodontal disease, vertical      ')\g
INSERT INTO Diagnose VALUES ('22729','   Other   exclude: dental calculus (.818)      ')\g
INSERT INTO Diagnose VALUES ('2273','    Periapical disease      ')\g
INSERT INTO Diagnose VALUES ('22731','   Radicular cyst      ')\g
INSERT INTO Diagnose VALUES ('22732','   Abscess      ')\g
INSERT INTO Diagnose VALUES ('22733','   Granuloma      ')\g
INSERT INTO Diagnose VALUES ('22739','   Other   exclude: periodontal fibroma or cyst"  (.313), radiolucent stage of cementoma (.382)"      ')\g
INSERT INTO Diagnose VALUES ('2274','    Giant cell reparative granuloma      ')\g
INSERT INTO Diagnose VALUES ('2279','    Other   exclude: internal resorption (.8621), osteomyelitis (.21), pulp calcification (.819), root resorption (.8622)      ')\g
INSERT INTO Diagnose VALUES ('228','     ENLARGED TONSILS OR ADENOIDS      ')\g
INSERT INTO Diagnose VALUES ('229','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('2291','    Infectious (suppurative) arthritis      ')\g
INSERT INTO Diagnose VALUES ('2292','    Thyroiditis      ')\g
INSERT INTO Diagnose VALUES ('2298','    Infectious complication of AIDS      ')\g
INSERT INTO Diagnose VALUES ('2299','    Other   exclude: sialectasis (.247)      ')\g
INSERT INTO Diagnose VALUES ('23','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('231','     BENIGN NEOPLASM OF BONE   exclude: dental neoplasm or cyst (.38)      ')\g
INSERT INTO Diagnose VALUES ('2311','    Cartilage origin   include: osteochondroma (osteocartilaginous  exostosis)   exclude: torus (.135)      ')\g
INSERT INTO Diagnose VALUES ('2312','    Osseous origin  include: osteoma      ')\g
INSERT INTO Diagnose VALUES ('2313','    Fibrous origin (see also .381)  include: periodontal fibroma or cyst""      ')\g
INSERT INTO Diagnose VALUES ('2314','    Vascular and/or lymphatic origin   include: angiofibroma      ')\g
INSERT INTO Diagnose VALUES ('2315','    Nerve tissue origin   include: melanotic progonoma      ')\g
INSERT INTO Diagnose VALUES ('2318','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('23181','   Solitary bone cyst      ')\g
INSERT INTO Diagnose VALUES ('23182','   Giant cell tumor (benign or malignant)      ')\g
INSERT INTO Diagnose VALUES ('23183','   Aneurysmal bone cyst      ')\g
INSERT INTO Diagnose VALUES ('23185','   Fibrous histiocytoma (benign) (see .3285)      ')\g
INSERT INTO Diagnose VALUES ('23189','   Other      ')\g
INSERT INTO Diagnose VALUES ('2319','    Other   exclude: ameloblastoma (adamantinoma) (.385)      ')\g
INSERT INTO Diagnose VALUES ('232','     MALIGNANT NEOPLASM OF BONE PRIMARY  exclude: dental neoplasm or cyst (.38)      ')\g
INSERT INTO Diagnose VALUES ('2321','    Cartilage origin   include: chondrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('2322','    Osseous origin   include: osteosarcoma      ')\g
INSERT INTO Diagnose VALUES ('2323','    Fibrous origin   include: fibrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('2324','    Vascular and/or lymphatic origin  include: angiosarcoma   exclude: lymphoma (.34), myeloma (.34)      ')\g
INSERT INTO Diagnose VALUES ('2325','    Nerve tissue origin   exclude: retinoblastoma (.37)      ')\g
INSERT INTO Diagnose VALUES ('2328','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('23281','   Ewing tumor      ')\g
INSERT INTO Diagnose VALUES ('23282','   Primary lymphoma of bone (reticulum cell sarcoma)      ')\g
INSERT INTO Diagnose VALUES ('23285','   Fibrous histiocytoma (malignant) (see .3185)      ')\g
INSERT INTO Diagnose VALUES ('23289','   Other      ')\g
INSERT INTO Diagnose VALUES ('2329','    Other  include: neoplasm associated with AIDS      ')\g
INSERT INTO Diagnose VALUES ('233','     MALIGNANT NEOPLASM OF BONE SECONDARY (METASTATIC)   exclude: invasion from adjoining lesion (.37)      ')\g
INSERT INTO Diagnose VALUES ('2331','    Osteolytic      ')\g
INSERT INTO Diagnose VALUES ('2332','    Osteoblastic      ')\g
INSERT INTO Diagnose VALUES ('2333','    Mixed osteolytic and osteoblastic  4th number for .33    For site of primary tumor add 4th number using anatomical field number of the primary site. Use  _ _ _9 for primary site unknown.      ')\g
INSERT INTO Diagnose VALUES ('234','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('2341','    Leukemia      ')\g
INSERT INTO Diagnose VALUES ('2342','    Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('2343','    Non-Hodgkin lymphoma (see p. 24)      ')\g
INSERT INTO Diagnose VALUES ('2345','    Myeloma      ')\g
INSERT INTO Diagnose VALUES ('23451','   Plasmacytoma      ')\g
INSERT INTO Diagnose VALUES ('23452','   Multiple myeloma      ')\g
INSERT INTO Diagnose VALUES ('23459','   Other      ')\g
INSERT INTO Diagnose VALUES ('2346','    Kaposi sarcoma      ')\g
INSERT INTO Diagnose VALUES ('234724','  Acute      ')\g
INSERT INTO Diagnose VALUES ('234725','  Acute      ')\g
INSERT INTO Diagnose VALUES ('234755','  Subacute      ')\g
INSERT INTO Diagnose VALUES ('234756','  Subacute      ')\g
INSERT INTO Diagnose VALUES ('234783','  Chronic      ')\g
INSERT INTO Diagnose VALUES ('234784','  Chronic      ')\g
INSERT INTO Diagnose VALUES ('2349','    Other   include: Waldenstrom macroglobulinemia      ')\g
INSERT INTO Diagnose VALUES ('235','     NEOPLASM ARISING IN PREEXISTING BENIGN DISORDER      ')\g
INSERT INTO Diagnose VALUES ('236','     BENIGN SOFT TISSUE NEOPLASM (WITH OR WITHOUT BONE INVOLVEMENT)      ')\g
INSERT INTO Diagnose VALUES ('2361','    Cyst  exclude: congenital laryngeal cyst (.1499)      ')\g
INSERT INTO Diagnose VALUES ('2362','    Hemangioma, lymphangioma, cystic hygroma      ')\g
INSERT INTO Diagnose VALUES ('2363','    Adenoma      ')\g
INSERT INTO Diagnose VALUES ('23631','   With cystic degeneration      ')\g
INSERT INTO Diagnose VALUES ('23632','   Without cystic degeneration      ')\g
INSERT INTO Diagnose VALUES ('2364','    Neurofibroma, neurilemmoma      ')\g
INSERT INTO Diagnose VALUES ('2365','    Pseudotumor      ')\g
INSERT INTO Diagnose VALUES ('2366','    Teratoma, dermoid (benign or malignant)      ')\g
INSERT INTO Diagnose VALUES ('2367','    Colloid nodule      ')\g
INSERT INTO Diagnose VALUES ('2368','    Goiter      ')\g
INSERT INTO Diagnose VALUES ('2369','    Other  include: papilloma, papillomatosis      ')\g
INSERT INTO Diagnose VALUES ('237','     MALIGNANT SOFT TISSUE NEOPLASM (WITH OR WITHOUT BONE INVOLVEMENT)      ')\g
INSERT INTO Diagnose VALUES ('2371','    Melanoma      ')\g
INSERT INTO Diagnose VALUES ('2372','    Retinoblastoma      ')\g
INSERT INTO Diagnose VALUES ('2373','    Primary carcinoma      ')\g
INSERT INTO Diagnose VALUES ('2374','    Sarcoma      ')\g
INSERT INTO Diagnose VALUES ('2375','    Metastatic neoplasm      ')\g
INSERT INTO Diagnose VALUES ('23751','   From adjoining lesion      ')\g
INSERT INTO Diagnose VALUES ('23752','   From distant side      ')\g
INSERT INTO Diagnose VALUES ('23759','   Other      ')\g
INSERT INTO Diagnose VALUES ('2379','    Other      ')\g
INSERT INTO Diagnose VALUES ('238','     DENTAL NEOPLASM, CYST (BENIGN OR MALIGNANT)      ')\g
INSERT INTO Diagnose VALUES ('2381','    Cyst (see also .1671, .313)      ')\g
INSERT INTO Diagnose VALUES ('23811','   Odontogenic   include: dentigerous (follicular)      ')\g
INSERT INTO Diagnose VALUES ('23812','   Median palatine, nasopalatine      ')\g
INSERT INTO Diagnose VALUES ('23813','   Globulomaxillary      ')\g
INSERT INTO Diagnose VALUES ('23814','   Solitary cyst of mandible      ')\g
INSERT INTO Diagnose VALUES ('23819','   Other   exclude: periodontal fibroma (.313), post-traumatic bone cyst (.43), basal cell nevus syndrome (.161)      ')\g
INSERT INTO Diagnose VALUES ('2382','    Cementoblastoma, cementoma      ')\g
INSERT INTO Diagnose VALUES ('2383','    Hypercementosis      ')\g
INSERT INTO Diagnose VALUES ('2384','    Odontoma      ')\g
INSERT INTO Diagnose VALUES ('2385','    Ameloblastoma (adamantinoma)      ')\g
INSERT INTO Diagnose VALUES ('2389','    Other   include: recurrent neoplasm or cyst, soft tissue mass (type unknown)   exclude: cherubism (.1545), exostosis (.311),  giant cell reparative granuloma (.274),  pseudopolyp of sinus (.254), radicular cyst (.2731)      ')\g
INSERT INTO Diagnose VALUES ('239','     OTHER   include: recurrent neoplasm (except dental, .389)   exclude: fibrous dysplasia (.85), laryngeal cyst (.1499)      ')\g
INSERT INTO Diagnose VALUES ('24','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('241','     FRACTURE      ')\g
INSERT INTO Diagnose VALUES ('2411','    Linear      ')\g
INSERT INTO Diagnose VALUES ('2412','    Depressed      ')\g
INSERT INTO Diagnose VALUES ('2413','    Comminuted      ')\g
INSERT INTO Diagnose VALUES ('2414','    Blowout" or "Blow-In""      ')\g
INSERT INTO Diagnose VALUES ('2415','    Fracture of tooth      ')\g
INSERT INTO Diagnose VALUES ('24151','   Fracture of crown without exposed pulp      ')\g
INSERT INTO Diagnose VALUES ('24152','   Fracture of crown with exposed pulp      ')\g
INSERT INTO Diagnose VALUES ('24153','   Fracture of root      ')\g
INSERT INTO Diagnose VALUES ('24154','   Fracture of alveolar process      ')\g
INSERT INTO Diagnose VALUES ('24159','   Other      ')\g
INSERT INTO Diagnose VALUES ('2416','    Pathological (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('2417','    Healed, old      ')\g
INSERT INTO Diagnose VALUES ('2418','    Multiple      ')\g
INSERT INTO Diagnose VALUES ('2419','    Other   exclude: complication of fracture (.43), soft tissue emphysema (.493)      ')\g
INSERT INTO Diagnose VALUES ('242','     DISLOCATION, SUBLUXATION      ')\g
INSERT INTO Diagnose VALUES ('2421','    Without fracture      ')\g
INSERT INTO Diagnose VALUES ('2422','    With fracture      ')\g
INSERT INTO Diagnose VALUES ('243','     COMPLICATION OF FRACTURE OR DISLOCATION  include: traumatic bone cyst   exclude: arteriovenous fistula, post-traumatic (.494)      ')\g
INSERT INTO Diagnose VALUES ('245','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('2452','    Implantation of radiation source      ')\g
INSERT INTO Diagnose VALUES ('2453','    Tooth extraction site      ')\g
INSERT INTO Diagnose VALUES ('2454','    Excision, other      ')\g
INSERT INTO Diagnose VALUES ('2455','    Dentin bridge      ')\g
INSERT INTO Diagnose VALUES ('2456','    Prosthesis   include: artificial eye      ')\g
INSERT INTO Diagnose VALUES ('2457','    Tracheostomy, endotracheal tube (see 6.457)      ')\g
INSERT INTO Diagnose VALUES ('2458','    Complication of surgery or interventional procedure   include: of biopsy, of aspiration, granuloma or stenosis complicating tracheostomy or  endotracheal tube (see 6.458)  exclude: of catheterization (9.44), of angiography (9.44)      ')\g
INSERT INTO Diagnose VALUES ('2459','    Other      ')\g
INSERT INTO Diagnose VALUES ('246','     FOREIGN BODY OR MEDICATION      ')\g
INSERT INTO Diagnose VALUES ('2461','    Opaque      ')\g
INSERT INTO Diagnose VALUES ('24611','   Catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('24612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('24613','   Catheter (or other tube) in unsatisfactory position      ')\g
INSERT INTO Diagnose VALUES ('24617','   Complication of catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('24619','   Other   exclude: dental filling (.48), prosthesis (.455)      ')\g
INSERT INTO Diagnose VALUES ('2462','    Nonopaque      ')\g
INSERT INTO Diagnose VALUES ('2469','    Other      ')\g
INSERT INTO Diagnose VALUES ('247','     EFFECT OF RADIATION      ')\g
INSERT INTO Diagnose VALUES ('248','     DENTAL FILLING, PULP CAP      ')\g
INSERT INTO Diagnose VALUES ('2481','    Dental filling, satisfactory   exclude: root canal filling (.483)      ')\g
INSERT INTO Diagnose VALUES ('2482','    Dental filling abnormality      ')\g
INSERT INTO Diagnose VALUES ('24821','   Overhang      ')\g
INSERT INTO Diagnose VALUES ('24822','   Open interproximal contact between fillings      ')\g
INSERT INTO Diagnose VALUES ('24823','   Poorly contoured      ')\g
INSERT INTO Diagnose VALUES ('24824','   Temporary filling      ')\g
INSERT INTO Diagnose VALUES ('24825','   Fractured filling      ')\g
INSERT INTO Diagnose VALUES ('24829','   Other      ')\g
INSERT INTO Diagnose VALUES ('2483','    Dental root canal filling      ')\g
INSERT INTO Diagnose VALUES ('24831','   Complete      ')\g
INSERT INTO Diagnose VALUES ('24832','   Incomplete      ')\g
INSERT INTO Diagnose VALUES ('24833','   Overfilled      ')\g
INSERT INTO Diagnose VALUES ('24834','   With apicalectomy      ')\g
INSERT INTO Diagnose VALUES ('24835','   With operative defect      ')\g
INSERT INTO Diagnose VALUES ('24836','   With hemisection      ')\g
INSERT INTO Diagnose VALUES ('24839','   Other      ')\g
INSERT INTO Diagnose VALUES ('2484','    Dental pulp cap, pulpotomy      ')\g
INSERT INTO Diagnose VALUES ('2485','    Dental filling material or impression material imbedded in jaw      ')\g
INSERT INTO Diagnose VALUES ('2489','    Other      ')\g
INSERT INTO Diagnose VALUES ('249','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('2491','    Laceration, soft tissue swelling secondary to trauma      ')\g
INSERT INTO Diagnose VALUES ('2492','    Hemorrhage   include: vitreous      ')\g
INSERT INTO Diagnose VALUES ('2493','    Soft tissue emphysema      ')\g
INSERT INTO Diagnose VALUES ('2494','    Arteriovenous fistula, post-traumatic      ')\g
INSERT INTO Diagnose VALUES ('2495','    Trauma to lens   include: subluxation      ')\g
INSERT INTO Diagnose VALUES ('2496','    Trauma to larynx   include: fracture      ')\g
INSERT INTO Diagnose VALUES ('2499','    Other   exclude: arteriovenous fistula, congenital (.1495), laceration of salivary duct (.245)      ')\g
INSERT INTO Diagnose VALUES ('25','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('251','     PITUITARY DISORDER      ')\g
INSERT INTO Diagnose VALUES ('2511','    Gigantism      ')\g
INSERT INTO Diagnose VALUES ('2512','    Acromegaly      ')\g
INSERT INTO Diagnose VALUES ('2519','    Other      ')\g
INSERT INTO Diagnose VALUES ('252','     THYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('2521','    Cretinism, hypothyroidism      ')\g
INSERT INTO Diagnose VALUES ('2522','    Hyperthyroidism   include: ocular effects      ')\g
INSERT INTO Diagnose VALUES ('2529','    Other      ')\g
INSERT INTO Diagnose VALUES ('253','     PARATHYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('2531','    Hyperparathyroidism   exclude: renal osteodystrophy (.573)      ')\g
INSERT INTO Diagnose VALUES ('2533','    Hypoparathyroidism      ')\g
INSERT INTO Diagnose VALUES ('2539','    Other      ')\g
INSERT INTO Diagnose VALUES ('254','     ADRENAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('255','     ABNORMAL DENTAL AGE      ')\g
INSERT INTO Diagnose VALUES ('2551','    Retarded dental age, delayed dentition      ')\g
INSERT INTO Diagnose VALUES ('2552','    Advanced dental age, precocious dentition      ')\g
INSERT INTO Diagnose VALUES ('256','     OSTEOPOROSIS      ')\g
INSERT INTO Diagnose VALUES ('257','     OSTEOMALACIA, RENAL OSTEODYSTROPHY      ')\g
INSERT INTO Diagnose VALUES ('2571','    Osteomalacia (rickets)-dietary      ')\g
INSERT INTO Diagnose VALUES ('2572','    Osteomalacia (rickets)-other      ')\g
INSERT INTO Diagnose VALUES ('2573','    Renal osteodystrophy      ')\g
INSERT INTO Diagnose VALUES ('2579','    Other      ')\g
INSERT INTO Diagnose VALUES ('258','     INTOXICATION, POISONING   include: fluorosis      ')\g
INSERT INTO Diagnose VALUES ('259','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('2595','    Alkaptonuria (ochronosis)      ')\g
INSERT INTO Diagnose VALUES ('26','      OTHER GENERALIZED SYSTEMIC DISORDER      ')\g
INSERT INTO Diagnose VALUES ('261','     CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('2612','    Lupus erythematosus      ')\g
INSERT INTO Diagnose VALUES ('2613','    Progressive systemic sclerosis (scleroderma)      ')\g
INSERT INTO Diagnose VALUES ('2614','    Dermatomyositis, polymyositis      ')\g
INSERT INTO Diagnose VALUES ('2619','    Other   exclude: Sjogren syndrome (.696)      ')\g
INSERT INTO Diagnose VALUES ('262','     VASCULITIS   include: polyarteritis (periarteritis) nodosa,  Wegener granulomatosis, mucocutaneous lymph node syndrome      ')\g
INSERT INTO Diagnose VALUES ('263','     ALLERGIC STATE      ')\g
INSERT INTO Diagnose VALUES ('264','     COMPLICATION OF DRUG THERAPY, DRUG USE      ')\g
INSERT INTO Diagnose VALUES ('265','     HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('2651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('2652','    Thalassemia or variant      ')\g
INSERT INTO Diagnose VALUES ('2656','    Hemophilia      ')\g
INSERT INTO Diagnose VALUES ('2659','    Other  include: extramedullary hemapoiesis   exclude: leukemia (.341)      ')\g
INSERT INTO Diagnose VALUES ('266','     HISTIOCYTOSIS      ')\g
INSERT INTO Diagnose VALUES ('267','     SPHINGOLIPIDOSIS   include: Gaucher disease, Niemann-Pick disease      ')\g
INSERT INTO Diagnose VALUES ('268','     OTHER INFILTRATIVE DISORDER   include: amyloid      ')\g
INSERT INTO Diagnose VALUES ('269','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('2695','    Infantile cortical hyperostosis (Caffey)      ')\g
INSERT INTO Diagnose VALUES ('2696','    Sjogren syndrome, Mikulicz syndrome      ')\g
INSERT INTO Diagnose VALUES ('2699','    Other      ')\g
INSERT INTO Diagnose VALUES ('27','      ARTHRITIS, OTHER JOINT DISORDER      ')\g
INSERT INTO Diagnose VALUES ('271','     RHEUMATOID ARTHRITIS      ')\g
INSERT INTO Diagnose VALUES ('277','     DEGENERATIVE JOINT DISORDER      ')\g
INSERT INTO Diagnose VALUES ('278','     MISCELLANEOUS TEMPOROMANDIBULAR JOINT ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('2781','    Ankylosis      ')\g
INSERT INTO Diagnose VALUES ('2782','    Motion abnormality      ')\g
INSERT INTO Diagnose VALUES ('2787','    Growth abnormality      ')\g
INSERT INTO Diagnose VALUES ('2789','    Other   include: joint effusion, etiology undetermined      ')\g
INSERT INTO Diagnose VALUES ('279','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('28','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('281','     CALCIFICATION OR OSSIFICATION      ')\g
INSERT INTO Diagnose VALUES ('2811','    Calcified hematoma      ')\g
INSERT INTO Diagnose VALUES ('2812','    Calcified blood vessel      ')\g
INSERT INTO Diagnose VALUES ('2813','    Interstitial calcification of metabolic origin (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('2816','    Calcified lymph node      ')\g
INSERT INTO Diagnose VALUES ('2817','    Calcification in neoplasm      ')\g
INSERT INTO Diagnose VALUES ('2818','    Calculus   include: salivary gland, dental, rhinolith  exclude: dentin bridge (.455), pulp stone (.1342)      ')\g
INSERT INTO Diagnose VALUES ('2819','    Other   include: calcified pulp, retrolental fibroplasia      ')\g
INSERT INTO Diagnose VALUES ('282','     SWALLOWING, PHONATION, OR SLEEP DISORDER      ')\g
INSERT INTO Diagnose VALUES ('2821','    Swallowing disorder without aspiration      ')\g
INSERT INTO Diagnose VALUES ('2822','    Swallowing disorder with aspiration      ')\g
INSERT INTO Diagnose VALUES ('2825','    Phonation disorder      ')\g
INSERT INTO Diagnose VALUES ('2827','    Sleep apnea      ')\g
INSERT INTO Diagnose VALUES ('2829','    Other   include: vocal cord palsy, paralysis      ')\g
INSERT INTO Diagnose VALUES ('284','     PAGET DISEASE      ')\g
INSERT INTO Diagnose VALUES ('285','     FIBROUS DYSPLASIA   include: leontiasis ossea  exclude: cherubism (.1545)      ')\g
INSERT INTO Diagnose VALUES ('286','     MISCELLANEOUS DENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('2861','    Enamel hypoplasia, hypomaturation      ')\g
INSERT INTO Diagnose VALUES ('2862','    Resorption, tooth      ')\g
INSERT INTO Diagnose VALUES ('28621','   Internal resorption      ')\g
INSERT INTO Diagnose VALUES ('28622','   Root resorption      ')\g
INSERT INTO Diagnose VALUES ('28629','   Other   include: crown resorption of impacted tooth      ')\g
INSERT INTO Diagnose VALUES ('2863','    Retained primary tooth      ')\g
INSERT INTO Diagnose VALUES ('2864','    Physiologically retained primary root      ')\g
INSERT INTO Diagnose VALUES ('2865','    Retained root fragment, other cause      ')\g
INSERT INTO Diagnose VALUES ('2866','    Hypereruption      ')\g
INSERT INTO Diagnose VALUES ('2867','    Erosion, abrasion      ')\g
INSERT INTO Diagnose VALUES ('2869','    Other   exclude: impacted tooth (.146), condensing osteitis (.873), enostosis (.873)      ')\g
INSERT INTO Diagnose VALUES ('287','     OSTEOSCLEROSIS, OTOSCLEROSIS      ')\g
INSERT INTO Diagnose VALUES ('2871','    Fenestral otosclerosis      ')\g
INSERT INTO Diagnose VALUES ('2872','    Cochlear otosclerosis      ')\g
INSERT INTO Diagnose VALUES ('2873','    Condensing osteitis, enostosis      ')\g
INSERT INTO Diagnose VALUES ('2879','    Other      ')\g
INSERT INTO Diagnose VALUES ('288','     HYPOPLASIA      ')\g
INSERT INTO Diagnose VALUES ('2881','    Of adenoids      ')\g
INSERT INTO Diagnose VALUES ('28811','   With dysgammaglobulinemia      ')\g
INSERT INTO Diagnose VALUES ('28812','   With cancer chemotherapy      ')\g
INSERT INTO Diagnose VALUES ('2889','    Other      ')\g
INSERT INTO Diagnose VALUES ('289','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('2891','    Macroglossia  exclude: cretinism (.521)      ')\g
INSERT INTO Diagnose VALUES ('2892','    Retinal detachment      ')\g
INSERT INTO Diagnose VALUES ('2893','    Optic neuropathy      ')\g
INSERT INTO Diagnose VALUES ('2899','    Other      ')\g
INSERT INTO Diagnose VALUES ('29','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('291','     FUNDAMENTAL OBSERVATION      ')\g
INSERT INTO Diagnose VALUES ('292','     ANATOMICAL DETAIL   include: normal sinus and mastoid development in children      ')\g
INSERT INTO Diagnose VALUES ('293','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('294','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('299','     OTHER   include: phenomenon related to technique of examination      ')\g
INSERT INTO Diagnose VALUES ('31','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('311','     NORMAL ROUTINE PLAIN FILM   exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('312','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION      ')\g
INSERT INTO Diagnose VALUES ('3121','    Digital radiographic techniques; tomography, MR, nuclear medicine      ')\g
INSERT INTO Diagnose VALUES ('31211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('312111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('312112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('312113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('312114','  Delayed scanning following      ')\g
INSERT INTO Diagnose VALUES ('312115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('312116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('312117','  Three-dimensional reconstruction      ')\g
INSERT INTO Diagnose VALUES ('312118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('312119','  Other, including serial enhancement studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('31214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('312141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('3121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('3121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('3121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('3121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('3121415',' Specific resonance suppressed include: fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('3121416',' High-speed imaging  include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('3121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('3121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('312142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('312143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('312144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('312145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('312146','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('312147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('312149','  Other      ')\g
INSERT INTO Diagnose VALUES ('31215','   Digital radiography   exclude: with angiography (.124-3)      ')\g
INSERT INTO Diagnose VALUES ('31216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('312161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('312162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('312163','  PET      ')\g
INSERT INTO Diagnose VALUES ('312164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('312165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('312166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('312167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('312168','  Therapeutic procedure include: use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('312169','  Other.  See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('31217','   Nuclear medicine study-spine      ')\g
INSERT INTO Diagnose VALUES ('312171','  Bone mineral measurement      ')\g
INSERT INTO Diagnose VALUES ('312172','  Bone scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('312174','  Bone marrow scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('312175','  Radionuclide myelography      ')\g
INSERT INTO Diagnose VALUES ('312179','  Other      ')\g
INSERT INTO Diagnose VALUES ('31218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('31219','   Other      ')\g
INSERT INTO Diagnose VALUES ('3122','    Myelography      ')\g
INSERT INTO Diagnose VALUES ('31221','   Positive contrast, not water-soluble (pantopaque)      ')\g
INSERT INTO Diagnose VALUES ('31222','   Positive contrast, water-soluble      ')\g
INSERT INTO Diagnose VALUES ('31223','   Negative contrast (gas)      ')\g
INSERT INTO Diagnose VALUES ('31224','   Negative contrast with tomography      ')\g
INSERT INTO Diagnose VALUES ('31229','   Other      ')\g
INSERT INTO Diagnose VALUES ('3123','    Diskography      ')\g
INSERT INTO Diagnose VALUES ('3124','    Angiography      ')\g
INSERT INTO Diagnose VALUES ('31241','   Aortography      ')\g
INSERT INTO Diagnose VALUES ('31242','   Selective angiography      ')\g
INSERT INTO Diagnose VALUES ('31243','   Venography, epidurography      ')\g
INSERT INTO Diagnose VALUES ('31249','   Other      ')\g
INSERT INTO Diagnose VALUES ('3125','    Special nonroutine projection      ')\g
INSERT INTO Diagnose VALUES ('31251','   Motion studies   include: flexion and extension projections, lateral bending projections      ')\g
INSERT INTO Diagnose VALUES ('31254','   Stereoscopic films      ')\g
INSERT INTO Diagnose VALUES ('31259','   Other      ')\g
INSERT INTO Diagnose VALUES ('3126','    Biopsy or interventional      ')\g
INSERT INTO Diagnose VALUES ('31261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('31262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('31263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('31264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('31267','   Operative procedure  include: stereotaxis      ')\g
INSERT INTO Diagnose VALUES ('31269','   Other      ')\g
INSERT INTO Diagnose VALUES ('3129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('31291','   Therapy localization      ')\g
INSERT INTO Diagnose VALUES ('31292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('31293','   Soft tissue technique      ')\g
INSERT INTO Diagnose VALUES ('31294','   Supervoltage roentgenography      ')\g
INSERT INTO Diagnose VALUES ('31295','   Bone mineral analysis  exclude: mineral analysis using radionuclides (.12171)      ')\g
INSERT INTO Diagnose VALUES ('31296','   Cine, video study      ')\g
INSERT INTO Diagnose VALUES ('31298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('312981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('312982','  A-mode      ')\g
INSERT INTO Diagnose VALUES ('312983','  B-scan      ')\g
INSERT INTO Diagnose VALUES ('312984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('312985','  Guided procedure for diagnosis   include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('312986','  Guided procedure for therapy   include: drainage      ')\g
INSERT INTO Diagnose VALUES ('312987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('312988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('312989','  Special nonroutine projection, mode, or transducer type      ')\g
INSERT INTO Diagnose VALUES ('31299','   Other   include: xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('313','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('3131','    Transitional vertebra   include: sacralization, lumbarization, 4 or 6 lumbar vertebrae      ')\g
INSERT INTO Diagnose VALUES ('3132','    Cervical or lumbar rib, elongated transverse process      ')\g
INSERT INTO Diagnose VALUES ('3133','    Spina bifida occulta (see also .1454, .1455)      ')\g
INSERT INTO Diagnose VALUES ('3134','    Cleft spinous process, accessory epiphysis, accessory spinous or transverse process      ')\g
INSERT INTO Diagnose VALUES ('3139','    Other   include: accessory ossification center, asymmetrical articular facets (tropism)   exclude: calcified iliolumbar ligament (.811), calcified sacrotuberous ligaments (.811), Schmorl node (.4961), limbus vertebra (.4962)      ')\g
INSERT INTO Diagnose VALUES ('314','     CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('3141','    Aplasia, agenesis   include: absent dens, sacral agenesis, caudal regression syndrome      ')\g
INSERT INTO Diagnose VALUES ('3142','    Hypoplasia   include: narrow spinal canal      ')\g
INSERT INTO Diagnose VALUES ('3143','    Spinal fusion      ')\g
INSERT INTO Diagnose VALUES ('31431','   Fused (block") vertebra"      ')\g
INSERT INTO Diagnose VALUES ('31432','   Klippel-Feil syndrome      ')\g
INSERT INTO Diagnose VALUES ('31439','   Other   include: bar formation  exclude: acquired spinal fusion (.451), atlantoaxial fusion (.1477)      ')\g
INSERT INTO Diagnose VALUES ('3144','    Hypersegmentation, hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('3145','    Failure of fusion deformity      ')\g
INSERT INTO Diagnose VALUES ('31451','   Cleft of vertebral body, hemivertebra  include: sagittal, coronal, butterfly      ')\g
INSERT INTO Diagnose VALUES ('31452','   Meningocele, meningomyelocele, spina bifida cystica      ')\g
INSERT INTO Diagnose VALUES ('31453','   Anterior or lateral meningomyelocele      ')\g
INSERT INTO Diagnose VALUES ('31454','   Diastematomyelia      ')\g
INSERT INTO Diagnose VALUES ('31455','   Vertebral anomaly associated with lipoma, cutaneous hemangioma, nevus or dimple (see also .1482)      ')\g
INSERT INTO Diagnose VALUES ('31459','   Other   exclude: spina bifida occulta (.133)      ')\g
INSERT INTO Diagnose VALUES ('3147','    Craniovertebral anomaly (see also 127.147)      ')\g
INSERT INTO Diagnose VALUES ('31471','   Ossiculum terminale      ')\g
INSERT INTO Diagnose VALUES ('31472','   Assimilation of atlas (atlanto-occipital)      ')\g
INSERT INTO Diagnose VALUES ('31474','   Occipital vertebra      ')\g
INSERT INTO Diagnose VALUES ('31475','   Basilar impression (invagination)      ')\g
INSERT INTO Diagnose VALUES ('31476','   Os odontoideum      ')\g
INSERT INTO Diagnose VALUES ('31477','   Atlantoaxial fusion      ')\g
INSERT INTO Diagnose VALUES ('31478','   Congenital atlantoaxial subluxation or dislocation      ')\g
INSERT INTO Diagnose VALUES ('31479','   Other   exclude: Klippel-Feil syndrome (.1432),  other odontoid malformation (.141, .142)      ')\g
INSERT INTO Diagnose VALUES ('3148','    Spinal cord or nerve root anomaly      ')\g
INSERT INTO Diagnose VALUES ('31481','   Tethered cord, isolated      ')\g
INSERT INTO Diagnose VALUES ('31482','   Tethered cord with congenital tumor, e.g., lipoma (see also .1455)      ')\g
INSERT INTO Diagnose VALUES ('31483','   Unusual length of cord or sac      ')\g
INSERT INTO Diagnose VALUES ('31484','   Root sheath cyst      ')\g
INSERT INTO Diagnose VALUES ('31485','   Root sheath segmentation anomaly      ')\g
INSERT INTO Diagnose VALUES ('31489','   Other   include: hydromyelia (syringomyelia) (.368)      ')\g
INSERT INTO Diagnose VALUES ('3149','    Other   include: congenital arteriovenous fistula, Vater anomaly    exclude: spondylolysis, spondylolisthesis (.42), congenital kyphosis  (.862), congenital scoliosis (.861)      ')\g
INSERT INTO Diagnose VALUES ('315','     DYSPLASIA (GENERALIZED CONSTITUTIONAL BONE DISORDER) (see 4.15 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('3151','    Osteochondrodysplasia-often lethal in neonate      ')\g
INSERT INTO Diagnose VALUES ('31512','   Asphyxiating thoracic dysplasia (Jeune)      ')\g
INSERT INTO Diagnose VALUES ('31513','   Thanatophoric dwarfism      ')\g
INSERT INTO Diagnose VALUES ('3152','    Osteochondrodysplasia-manifest in neonate      ')\g
INSERT INTO Diagnose VALUES ('31521','   Achondroplasia      ')\g
INSERT INTO Diagnose VALUES ('31522','   Chondrodysplasia punctata (stippled epiphyses)      ')\g
INSERT INTO Diagnose VALUES ('31525','   Diastrophic dwarfism      ')\g
INSERT INTO Diagnose VALUES ('31526','   Metatrophic dwarfism, Kneist syndrome      ')\g
INSERT INTO Diagnose VALUES ('31527','   Spondyloepiphyseal dysplasia congenita      ')\g
INSERT INTO Diagnose VALUES ('3153','    Osteochondrodysplasia-manifest in later life      ')\g
INSERT INTO Diagnose VALUES ('31530','   Hypochondroplasia      ')\g
INSERT INTO Diagnose VALUES ('31531','   Pseudoachondroplastic dysplasia      ')\g
INSERT INTO Diagnose VALUES ('31532','   Spondyloepiphyseal dysplasia tarda      ')\g
INSERT INTO Diagnose VALUES ('31533','   Spondyloepiphyseal dysplasia, other      ')\g
INSERT INTO Diagnose VALUES ('31534','   Spondyloepiphyseal dysplasia (Kozlowski)      ')\g
INSERT INTO Diagnose VALUES ('3154','    Dysplasia-cartilage and fibrous compo-nents      ')\g
INSERT INTO Diagnose VALUES ('3155','    Dysplasia with abnormal cortical formation, modeling, density      ')\g
INSERT INTO Diagnose VALUES ('31551','   Osteogenesis imperfecta      ')\g
INSERT INTO Diagnose VALUES ('31552','   Osteopetrosis      ')\g
INSERT INTO Diagnose VALUES ('3159','    Other      ')\g
INSERT INTO Diagnose VALUES ('316','     DYSOSTOSIS (see 4.16 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('3161','    Soft tissue dysostosis      ')\g
INSERT INTO Diagnose VALUES ('31611','   Fibrodysplasia (myositis) ossificans progressiva      ')\g
INSERT INTO Diagnose VALUES ('3165','    Spinal dysostosis      ')\g
INSERT INTO Diagnose VALUES ('31651','   Spondylocostal dysostosis  include: spondylothoracic dysplasia (Jarco Levine)      ')\g
INSERT INTO Diagnose VALUES ('31652','   Brachyrachia      ')\g
INSERT INTO Diagnose VALUES ('31659','   Other      ')\g
INSERT INTO Diagnose VALUES ('3166','    Facial dysostosis (see 2.166 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('31662','   Goldenhar syndrome      ')\g
INSERT INTO Diagnose VALUES ('317','     PRIMARY DISTURBANCE OF GROWTH      ')\g
INSERT INTO Diagnose VALUES ('3171','    Increased growth (see 4.171 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('31711','   Marfan syndrome      ')\g
INSERT INTO Diagnose VALUES ('31719','   Other   exclude: gigantism (.511), homocystinuria (.597)      ')\g
INSERT INTO Diagnose VALUES ('3172','    Decreased growth (see 4.172 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('318','     OTHER CONSTITUTIONAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3181','    Mucopolysaccharidosis (MPS)      ')\g
INSERT INTO Diagnose VALUES ('31811','   Hurler syndrome (MPS I-H)      ')\g
INSERT INTO Diagnose VALUES ('31812','   Hunter syndrome (MPS II)      ')\g
INSERT INTO Diagnose VALUES ('31813','   Sanfilippo syndrome (MPS  III)      ')\g
INSERT INTO Diagnose VALUES ('31814','   Morquio syndrome (MPS  IV)      ')\g
INSERT INTO Diagnose VALUES ('31815','   Maroteaux-Lamy syndrome (MPS  VI)      ')\g
INSERT INTO Diagnose VALUES ('31819','   Other   include: Scheie (MPS I-S), Hurler-Scheie compound (MPS I-H/S), beta-glucuronidase deficiency (MPS VII)      ')\g
INSERT INTO Diagnose VALUES ('3182','    Dysostosis multiplex, other cause (see 4.182 for details)      ')\g
INSERT INTO Diagnose VALUES ('3183','    Neurocutaneous syndrome (phacomatosis)      ')\g
INSERT INTO Diagnose VALUES ('31831','   Neurofibromatosis      ')\g
INSERT INTO Diagnose VALUES ('31832','   Tuberous sclerosis      ')\g
INSERT INTO Diagnose VALUES ('31839','   Other   exclude: fibrous dysplasia (.85), infantile  (juvenile) fibromatosis (.1544)      ')\g
INSERT INTO Diagnose VALUES ('3184','    Autosomal aberration      ')\g
INSERT INTO Diagnose VALUES ('31841','   Trisomy 21 (Down syndrome)      ')\g
INSERT INTO Diagnose VALUES ('31842','   Trisomy 18      ')\g
INSERT INTO Diagnose VALUES ('31843','   Trisomy 13      ')\g
INSERT INTO Diagnose VALUES ('31849','   Other   include: cri-du-chat syndrome      ')\g
INSERT INTO Diagnose VALUES ('3185','    Sex chromosomal aberration      ')\g
INSERT INTO Diagnose VALUES ('31851','   Turner syndrome      ')\g
INSERT INTO Diagnose VALUES ('31852','   Klinefelter syndrome      ')\g
INSERT INTO Diagnose VALUES ('31859','   Other      ')\g
INSERT INTO Diagnose VALUES ('3186','    Idiopathic osteolysis      ')\g
INSERT INTO Diagnose VALUES ('3189','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('31891','   Ehlers-Danlos syndrome      ')\g
INSERT INTO Diagnose VALUES ('31892','   Lipodystrophy      ')\g
INSERT INTO Diagnose VALUES ('31893','   Arthrogryposis (amyotonia congenita)      ')\g
INSERT INTO Diagnose VALUES ('31899','   Other   exclude: hypophosphatasia (.593),  idiopathic hypercalcemia of infancy (.599)      ')\g
INSERT INTO Diagnose VALUES ('319','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('32','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('320','     INFECTION CLASSIFIED BY ORGANISM (see 6.20 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('3201','    Gram-positive facultative bacterium      ')\g
INSERT INTO Diagnose VALUES ('32012','   Staphylococcus      ')\g
INSERT INTO Diagnose VALUES ('32013','   Streptococcus      ')\g
INSERT INTO Diagnose VALUES ('3202','    Gram-negative bacterium      ')\g
INSERT INTO Diagnose VALUES ('32025','   Salmonella      ')\g
INSERT INTO Diagnose VALUES ('3205','    Fungus      ')\g
INSERT INTO Diagnose VALUES ('3206','    Virus  include: AIDS      ')\g
INSERT INTO Diagnose VALUES ('3207','    Protozoa or spirochete      ')\g
INSERT INTO Diagnose VALUES ('32076','   Treponema pallidum (congenital syphilis)      ')\g
INSERT INTO Diagnose VALUES ('32077','   Treponema pallidum (acquired syphilis)      ')\g
INSERT INTO Diagnose VALUES ('321','     OSTEOMYELITIS      ')\g
INSERT INTO Diagnose VALUES ('3211','    Typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('3212','    Unusual manifestation      ')\g
INSERT INTO Diagnose VALUES ('3214','    Associated with systemic disease      ')\g
INSERT INTO Diagnose VALUES ('3215','    Unusual organism      ')\g
INSERT INTO Diagnose VALUES ('3216','    Secondary to trauma   include: postoperative      ')\g
INSERT INTO Diagnose VALUES ('3217','    Opportunistic infection      ')\g
INSERT INTO Diagnose VALUES ('3218','    Bone reaction secondary to soft tissue infection      ')\g
INSERT INTO Diagnose VALUES ('3219','    Other      ')\g
INSERT INTO Diagnose VALUES ('322','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('323','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('3231','    Typical manifestation   include: body, disk, paravertebral involvement      ')\g
INSERT INTO Diagnose VALUES ('3232','    Unusual manifestation   include: neural arch involvement      ')\g
INSERT INTO Diagnose VALUES ('3239','    Other      ')\g
INSERT INTO Diagnose VALUES ('324','     SOFT TISSUE INFLAMMATION, SINUS TRACT, FISTULA      ')\g
INSERT INTO Diagnose VALUES ('3241','    Cellulitis      ')\g
INSERT INTO Diagnose VALUES ('3242','    Abscess      ')\g
INSERT INTO Diagnose VALUES ('3244','    Sinus tract      ')\g
INSERT INTO Diagnose VALUES ('3245','    Fistula      ')\g
INSERT INTO Diagnose VALUES ('3246','    Lymphadenitis      ')\g
INSERT INTO Diagnose VALUES ('3249','    Other   include: myelitis   exclude: diskitis (.25), arachnoiditis (.27,  .446)      ')\g
INSERT INTO Diagnose VALUES ('325','     DISKITIS      ')\g
INSERT INTO Diagnose VALUES ('326','     INFECTIOUS ARTHRITIS (SUPPURATIVE)      ')\g
INSERT INTO Diagnose VALUES ('327','     ARACHNOIDITIS   exclude: complication of myelography (.446)      ')\g
INSERT INTO Diagnose VALUES ('329','     OTHER   exclude: syphilis (.2076, .2077)      ')\g
INSERT INTO Diagnose VALUES ('3298','    Infectious complications of AIDS      ')\g
INSERT INTO Diagnose VALUES ('33','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('331','     BENIGN NEOPLASM OF BONE   exclude: primary intraspinal lesion (.36)      ')\g
INSERT INTO Diagnose VALUES ('3311','    Cartilaginous origin      ')\g
INSERT INTO Diagnose VALUES ('33113','   Osteochondroma (osteocartilaginous exostosis)  exclude: multiple exostoses (.1542)      ')\g
INSERT INTO Diagnose VALUES ('33114','   Enchondroma   exclude: enchondromatosis (Ollier) (.1543)      ')\g
INSERT INTO Diagnose VALUES ('33119','   Other      ')\g
INSERT INTO Diagnose VALUES ('3312','    Osseous origin      ')\g
INSERT INTO Diagnose VALUES ('33121','   Osteoma      ')\g
INSERT INTO Diagnose VALUES ('33122','   Osteoid osteoma      ')\g
INSERT INTO Diagnose VALUES ('33123','   Osteoblastoma      ')\g
INSERT INTO Diagnose VALUES ('33129','   Other      ')\g
INSERT INTO Diagnose VALUES ('3313','    Fibrous origin   exclude: fibrous dysplasia (.85)      ')\g
INSERT INTO Diagnose VALUES ('3314','    Vascular, lymphatic origin      ')\g
INSERT INTO Diagnose VALUES ('33141','   Hemangioma      ')\g
INSERT INTO Diagnose VALUES ('33142','   Lymphangioma      ')\g
INSERT INTO Diagnose VALUES ('33143','   Skeletal angiomatosis   include: Gorham disease (vanishing bone")"      ')\g
INSERT INTO Diagnose VALUES ('33149','   Other   include: benign hemangiopericytoma      ')\g
INSERT INTO Diagnose VALUES ('3315','    Nerve tissue origin      ')\g
INSERT INTO Diagnose VALUES ('3318','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('33182','   Giant cell tumor (benign or malignant)      ')\g
INSERT INTO Diagnose VALUES ('33183','   Aneurysmal bone cyst      ')\g
INSERT INTO Diagnose VALUES ('33189','   Other      ')\g
INSERT INTO Diagnose VALUES ('3319','    Other benign neoplasms   include: lipoma, mesenchymal neoplasm   exclude: teratoma, dermoid (.362)      ')\g
INSERT INTO Diagnose VALUES ('332','     MALIGNANT NEOPLASM OF BONE-PRIMARY   exclude: spinal cord lesion (.36)      ')\g
INSERT INTO Diagnose VALUES ('3321','    Chondrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('3322','    Osteosarcoma      ')\g
INSERT INTO Diagnose VALUES ('3323','    Fibrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('3324','    Angiosarcoma, malignant hemangioendothelioma, malignant hemangiopericytoma      ')\g
INSERT INTO Diagnose VALUES ('3325','    Neurosarcoma      ')\g
INSERT INTO Diagnose VALUES ('3327','    Chordoma      ')\g
INSERT INTO Diagnose VALUES ('3328','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('33281','   Ewing tumor      ')\g
INSERT INTO Diagnose VALUES ('33282','   Primary lymphoma of bone      ')\g
INSERT INTO Diagnose VALUES ('33289','   Other      ')\g
INSERT INTO Diagnose VALUES ('3329','    Other  include: neoplasm associated with AIDS      ')\g
INSERT INTO Diagnose VALUES ('333','     MALIGNANT NEOPLASM OF BONE-SECONDARY (METASTATIC) (for 4th number see box following .339)      ')\g
INSERT INTO Diagnose VALUES ('3331','    Osteolytic      ')\g
INSERT INTO Diagnose VALUES ('3332','    Osteoblastic      ')\g
INSERT INTO Diagnose VALUES ('3333','    Mixed osteolytic and osteoblastic      ')\g
INSERT INTO Diagnose VALUES ('3339','    Other   4th number for use with .33 and .37  For site of primary add 4th number using the anatomical field number of the primary.  Use. _ _ _9 for primary site unknown      ')\g
INSERT INTO Diagnose VALUES ('334','     LEUKEMIA, LYMPHOMA, MYELOMA (for 5th number see box following .349)      ')\g
INSERT INTO Diagnose VALUES ('3341','    Leukemia      ')\g
INSERT INTO Diagnose VALUES ('3342','    Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('3343','    Non-Hodgkin lymphoma      ')\g
INSERT INTO Diagnose VALUES ('33431','   Lymphocytic, well differentiated      ')\g
INSERT INTO Diagnose VALUES ('33432','   Nodular      ')\g
INSERT INTO Diagnose VALUES ('33433','   Diffuse      ')\g
INSERT INTO Diagnose VALUES ('33434','   Histiocytic      ')\g
INSERT INTO Diagnose VALUES ('33435','   Burkitt      ')\g
INSERT INTO Diagnose VALUES ('33439','   Other      ')\g
INSERT INTO Diagnose VALUES ('3345','    Myeloma (see amyloidosis .68)      ')\g
INSERT INTO Diagnose VALUES ('33451','   Plasmacytoma      ')\g
INSERT INTO Diagnose VALUES ('33452','   Multiple myeloma      ')\g
INSERT INTO Diagnose VALUES ('33459','   Other      ')\g
INSERT INTO Diagnose VALUES ('3346','    Kaposi sarcoma      ')\g
INSERT INTO Diagnose VALUES ('334720','  Acute      ')\g
INSERT INTO Diagnose VALUES ('334751','  Subacute      ')\g
INSERT INTO Diagnose VALUES ('334779','  Chronic      ')\g
INSERT INTO Diagnose VALUES ('334810','  Single vertebra      ')\g
INSERT INTO Diagnose VALUES ('334840','  Multiple vertebrae      ')\g
INSERT INTO Diagnose VALUES ('334871','  Contiguous vertebrae      ')\g
INSERT INTO Diagnose VALUES ('3349','    Other      ')\g
INSERT INTO Diagnose VALUES ('334963','  Other      ')\g
INSERT INTO Diagnose VALUES ('335','     NEOPLASM ARISING IN PREEXISTING BENIGN DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3351','    Bone neoplasm (e.g., enchondroma, osteochondroma)      ')\g
INSERT INTO Diagnose VALUES ('3352','    Infection      ')\g
INSERT INTO Diagnose VALUES ('3353','    Paget disease      ')\g
INSERT INTO Diagnose VALUES ('3354','    Irradiated bone      ')\g
INSERT INTO Diagnose VALUES ('3355','    Fibrous dysplasia      ')\g
INSERT INTO Diagnose VALUES ('3356','    Bone infarction      ')\g
INSERT INTO Diagnose VALUES ('3359','    Other      ')\g
INSERT INTO Diagnose VALUES ('336','     INTRASPINAL NEOPLASM-PRIMARY, NEOPLASTIC-LIKE CONDITION (for 5th number see box following .369)   exclude: degenerative disk disease (.78), neoplasm arising in bone (.31, .32), inflammation (.2)      ')\g
INSERT INTO Diagnose VALUES ('3361','    Developmental origin      ')\g
INSERT INTO Diagnose VALUES ('33611','   Cyst (see also root sheath cyst .1484)      ')\g
INSERT INTO Diagnose VALUES ('3362','    Teratoma, dermoid   exclude: extraspinal teratoma or dermoid (.39), presacral teratoma or dermoid (8.313)      ')\g
INSERT INTO Diagnose VALUES ('3363','    Glial origin      ')\g
INSERT INTO Diagnose VALUES ('33631','   Astrocytoma grade 1      ')\g
INSERT INTO Diagnose VALUES ('33632','   Astrocytoma grade 2      ')\g
INSERT INTO Diagnose VALUES ('33633','   Astrocytoma grade 3      ')\g
INSERT INTO Diagnose VALUES ('33634','   Astrocytoma grade 4; glioblastoma multiforme      ')\g
INSERT INTO Diagnose VALUES ('33635','   Oligodendroglioma      ')\g
INSERT INTO Diagnose VALUES ('33636','   Ependymoma      ')\g
INSERT INTO Diagnose VALUES ('33637','   Medulloblastoma      ')\g
INSERT INTO Diagnose VALUES ('33638','   Mixed glial origin      ')\g
INSERT INTO Diagnose VALUES ('33639','   Other      ')\g
INSERT INTO Diagnose VALUES ('3364','    Nerve, nerve sheath origin      ')\g
INSERT INTO Diagnose VALUES ('3365','    Vascular origin   include: varix of cord      ')\g
INSERT INTO Diagnose VALUES ('3366','    Meningeal origin      ')\g
INSERT INTO Diagnose VALUES ('33661','   Meningioma      ')\g
INSERT INTO Diagnose VALUES ('33662','   Meningeal cyst or diverticulum   include: arachnoidal cyst, widened arachnoid space and dilated axillary pouch with rheumatoid arthritis   exclude: meningocele (.1452)      ')\g
INSERT INTO Diagnose VALUES ('33669','   Other      ')\g
INSERT INTO Diagnose VALUES ('3367','    Hemorrhage, hematoma      ')\g
INSERT INTO Diagnose VALUES ('33671','   Epidural hemorrhage, spontaneous, unknown origin      ')\g
INSERT INTO Diagnose VALUES ('33672','   Epidural hemorrhage, anticoagulant therapy      ')\g
INSERT INTO Diagnose VALUES ('33673','   Epidural hemorrhage, other cause   include: angioma, trauma      ')\g
INSERT INTO Diagnose VALUES ('33674','   Intramedullary hemorrhage, hematoma or contusion      ')\g
INSERT INTO Diagnose VALUES ('33679','   Other      ')\g
INSERT INTO Diagnose VALUES ('3368','    Syringomyelia, hydromyelia, hematomyelia      ')\g
INSERT INTO Diagnose VALUES ('3369','    Other   include: intraspinal mass lesion of unknown  etiology, myxoma, lipoma, extramedullary hematopoiesis  exclude: neurofibromatosis (.1831)      ')\g
INSERT INTO Diagnose VALUES ('337','     INTRASPINAL NEOPLASM-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('338','     INVASION OR EROSION FROM PARASPINAL LESION (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('339','     OTHER  include: paraspinal mass (also code cause) (see .38)      ')\g
INSERT INTO Diagnose VALUES ('34','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('341','     FRACTURE      ')\g
INSERT INTO Diagnose VALUES ('3411','    Simple compression fracture      ')\g
INSERT INTO Diagnose VALUES ('3412','    Hyperextension fracture      ')\g
INSERT INTO Diagnose VALUES ('3413','    Acute flexion fracture      ')\g
INSERT INTO Diagnose VALUES ('3414','    Epiphyseal fracture      ')\g
INSERT INTO Diagnose VALUES ('3415','    Stress fracture      ')\g
INSERT INTO Diagnose VALUES ('3416','    Pathological (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('3417','    Healed, old      ')\g
INSERT INTO Diagnose VALUES ('3419','    Other   include: child abuse (battered child), tetanus, seat belt (Chance) fracture   exclude: Cushing syndrome with fracture (.541)      ')\g
INSERT INTO Diagnose VALUES ('342','     DISLOCATION, SUBLUXATION, SPONDYLOLYSIS, SPONDYLOLISTHESIS      ')\g
INSERT INTO Diagnose VALUES ('3421','    Dislocation, subluxation      ')\g
INSERT INTO Diagnose VALUES ('34211','   Atlantoaxial   include: rotary subluxation  exclude: congenital (.1478)      ')\g
INSERT INTO Diagnose VALUES ('34219','   Other      ')\g
INSERT INTO Diagnose VALUES ('3422','    Fracture-dislocation      ')\g
INSERT INTO Diagnose VALUES ('3423','    Spondylolysis, spondylolisthesis      ')\g
INSERT INTO Diagnose VALUES ('34231','   Spondylolysis without spondylolisthesis      ')\g
INSERT INTO Diagnose VALUES ('34232','   Spondylolysis with spondylolisthesis      ')\g
INSERT INTO Diagnose VALUES ('34233','   Spondylolisthesis without spondylolysis (e.g., degenerative disease of small joints)      ')\g
INSERT INTO Diagnose VALUES ('34234','   Spondylolisthesis, unilateral (often with sclerotic pedicle)      ')\g
INSERT INTO Diagnose VALUES ('34239','   Other   include: retrolisthesis (reverse spondylolisthesis)      ')\g
INSERT INTO Diagnose VALUES ('3429','    Other   include: locked facets      ')\g
INSERT INTO Diagnose VALUES ('343','     COMPLICATION OF FRACTURE OR DISLOCATION   exclude: epidural hemorrhage (.367)      ')\g
INSERT INTO Diagnose VALUES ('344','     COMPLICATION OF MYELOGRAPHY      ')\g
INSERT INTO Diagnose VALUES ('3441','    Subdural injection      ')\g
INSERT INTO Diagnose VALUES ('3442','    Epidural injection      ')\g
INSERT INTO Diagnose VALUES ('3443','    Vascular injection      ')\g
INSERT INTO Diagnose VALUES ('3444','    Hemorrhage      ')\g
INSERT INTO Diagnose VALUES ('3445','    Spondylitis, diskitis (also code organism .20)      ')\g
INSERT INTO Diagnose VALUES ('3446','    Arachnoiditis      ')\g
INSERT INTO Diagnose VALUES ('3449','    Other      ')\g
INSERT INTO Diagnose VALUES ('345','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('3451','    Spinal fusion      ')\g
INSERT INTO Diagnose VALUES ('3452','    Laminectomy      ')\g
INSERT INTO Diagnose VALUES ('3453','    Disk surgery      ')\g
INSERT INTO Diagnose VALUES ('3458','    Complication of surgery or interventional procedure  include: of biopsy, of aspiration  exclude: of myelography (.44), of diskitis (.25)      ')\g
INSERT INTO Diagnose VALUES ('3459','    Other   include: traction      ')\g
INSERT INTO Diagnose VALUES ('346','     FOREIGN BODY OR MEDICATION      ')\g
INSERT INTO Diagnose VALUES ('3461','    Opaque      ')\g
INSERT INTO Diagnose VALUES ('34611','   Catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('34612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('34613','   Catheter (or other tube) in unsatisfactory position      ')\g
INSERT INTO Diagnose VALUES ('34617','   Complication of catheter (or other tube)      ')\g
INSERT INTO Diagnose VALUES ('34619','   Other      ')\g
INSERT INTO Diagnose VALUES ('3462','    Nonopaque      ')\g
INSERT INTO Diagnose VALUES ('3469','    Other      ')\g
INSERT INTO Diagnose VALUES ('347','     EFFECTS OF RADIATION      ')\g
INSERT INTO Diagnose VALUES ('349','     MISCELLANEOUS POST-TRAUMATIC CHANGES   exclude: iatrogenic intraspinal dermoid (.362)      ')\g
INSERT INTO Diagnose VALUES ('3491','    Postural change   include: loss of usual degree of spinal lordosis      ')\g
INSERT INTO Diagnose VALUES ('3492','    Avulsion of nerve root      ')\g
INSERT INTO Diagnose VALUES ('3493','    Soft tissue emphysema   exclude: vacuum disk (.782)      ')\g
INSERT INTO Diagnose VALUES ('3494','    Arteriovenous fistula, post-traumatic      ')\g
INSERT INTO Diagnose VALUES ('3496','    Juvenile diskogenic disorder      ')\g
INSERT INTO Diagnose VALUES ('34961','   Schmorl node      ')\g
INSERT INTO Diagnose VALUES ('34962','   Limbus vertebra      ')\g
INSERT INTO Diagnose VALUES ('34963','   Juvenile kyphosis (Scheuermann disease)      ')\g
INSERT INTO Diagnose VALUES ('34969','   Other      ')\g
INSERT INTO Diagnose VALUES ('3497','    Post-traumatic necrosis [ischemic (aseptic) necrosis] Kummel disease      ')\g
INSERT INTO Diagnose VALUES ('3498','    Cord atrophy      ')\g
INSERT INTO Diagnose VALUES ('3499','    Other      ')\g
INSERT INTO Diagnose VALUES ('35','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('351','     PITUITARY DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3511','    Gigantism      ')\g
INSERT INTO Diagnose VALUES ('3512','    Acromegaly      ')\g
INSERT INTO Diagnose VALUES ('3519','    Other      ')\g
INSERT INTO Diagnose VALUES ('352','     THYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3521','    Cretinism, hypothyroidism      ')\g
INSERT INTO Diagnose VALUES ('3529','    Other      ')\g
INSERT INTO Diagnose VALUES ('353','     PARATHYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3531','    Primary hyperparathyroidism      ')\g
INSERT INTO Diagnose VALUES ('3532','    Secondary hyperparathyroidism, nonrenal   include: erosive spondyloarthropathy  exclude: renal (.573)      ')\g
INSERT INTO Diagnose VALUES ('3533','    Hypoparathyroidism      ')\g
INSERT INTO Diagnose VALUES ('3539','    Other   exclude: renal osteodystrophy (.573)      ')\g
INSERT INTO Diagnose VALUES ('354','     ADRENAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3541','    Cushing syndrome, endogenous   include: fracture      ')\g
INSERT INTO Diagnose VALUES ('3542','    Cushing syndrome, exogenous   include: steroid therapy      ')\g
INSERT INTO Diagnose VALUES ('3549','    Other      ')\g
INSERT INTO Diagnose VALUES ('356','     OSTEOPOROSIS      ')\g
INSERT INTO Diagnose VALUES ('3561','    Scurvy      ')\g
INSERT INTO Diagnose VALUES ('3562','    Senile, postmenopausal      ')\g
INSERT INTO Diagnose VALUES ('3563','    Juvenile      ')\g
INSERT INTO Diagnose VALUES ('3564','    Disuse atrophy      ')\g
INSERT INTO Diagnose VALUES ('3569','    Other   exclude: Cushing syndrome (.54), homocystinuria (.597)      ')\g
INSERT INTO Diagnose VALUES ('357','     OSTEOMALACIA, RENAL OSTEODYSTROPHY      ')\g
INSERT INTO Diagnose VALUES ('3571','    Osteomalacia, rickets-dietary      ')\g
INSERT INTO Diagnose VALUES ('3572','    Osteomalacia, rickets-other      ')\g
INSERT INTO Diagnose VALUES ('35721','   Gastroenterogenous      ')\g
INSERT INTO Diagnose VALUES ('35722','   Organic renal disease (see also .573)      ')\g
INSERT INTO Diagnose VALUES ('35723','   Renal tubular dysfunction      ')\g
INSERT INTO Diagnose VALUES ('35729','   Other   include: atypical axial osteomalacia      ')\g
INSERT INTO Diagnose VALUES ('3573','    Renal osteodystrophy (osteomalacia plus secondary hyperparathyroidism)      ')\g
INSERT INTO Diagnose VALUES ('3579','    Other      ')\g
INSERT INTO Diagnose VALUES ('358','     INTOXICATION, POISONING      ')\g
INSERT INTO Diagnose VALUES ('3581','    Heavy metal      ')\g
INSERT INTO Diagnose VALUES ('3582','    Fluorine      ')\g
INSERT INTO Diagnose VALUES ('3583','    Hypervitaminosis D      ')\g
INSERT INTO Diagnose VALUES ('3589','    Other      ')\g
INSERT INTO Diagnose VALUES ('359','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('3593','    Hypophosphatasia      ')\g
INSERT INTO Diagnose VALUES ('3594','    Hemochromatosis      ')\g
INSERT INTO Diagnose VALUES ('3595','    Alkaptonuria (ochronosis)      ')\g
INSERT INTO Diagnose VALUES ('3597','    Homocystinuria      ')\g
INSERT INTO Diagnose VALUES ('3599','    Other   include: idiopathic hypercalcemia of infancy      ')\g
INSERT INTO Diagnose VALUES ('36','      OTHER GENERALIZED SYSTEMIC DISORDER   exclude: arthritis (.7)      ')\g
INSERT INTO Diagnose VALUES ('361','     SYSTEMIC CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3612','    Lupus erythematosus      ')\g
INSERT INTO Diagnose VALUES ('364','     COMPLICATION OF DRUG THERAPY, DRUG USE      ')\g
INSERT INTO Diagnose VALUES ('365','     HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('3652','    Thalassemia or variant      ')\g
INSERT INTO Diagnose VALUES ('3653','    Other congenital (primary) anemia  include: thrombocytopenia absent radius      ')\g
INSERT INTO Diagnose VALUES ('3656','    Hemophilia      ')\g
INSERT INTO Diagnose VALUES ('3657','    Myeloid metaphasia (myelosclerosis)      ')\g
INSERT INTO Diagnose VALUES ('3659','    Other   include: extramedullary hematopoiesis      ')\g
INSERT INTO Diagnose VALUES ('366','     HISTIOCYTOSIS (Langerhans cell histiocytosis)      ')\g
INSERT INTO Diagnose VALUES ('3661','    Letterer-Siwe disease      ')\g
INSERT INTO Diagnose VALUES ('3662','    Hand-Schuller-Christian disease      ')\g
INSERT INTO Diagnose VALUES ('3663','    Eosinophilic granuloma      ')\g
INSERT INTO Diagnose VALUES ('3669','    Other      ')\g
INSERT INTO Diagnose VALUES ('367','     SPHINGOLIPIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('3671','    Gaucher disease      ')\g
INSERT INTO Diagnose VALUES ('3672','    Niemann-Pick disease      ')\g
INSERT INTO Diagnose VALUES ('3679','    Other      ')\g
INSERT INTO Diagnose VALUES ('368','     OTHER INFILTRATIVE DISORDER   include: amyloid      ')\g
INSERT INTO Diagnose VALUES ('369','     OTHER   include: mastocytosis (urticaria pigmentosa)      ')\g
INSERT INTO Diagnose VALUES ('37','      ARTHRITIS   exclude: infectious arthritis (suppurative) (.26)      ')\g
INSERT INTO Diagnose VALUES ('371','     RHEUMATOID ARTHRITIS      ')\g
INSERT INTO Diagnose VALUES ('3711','    Typical manifestations      ')\g
INSERT INTO Diagnose VALUES ('3712','    Unusual manifestations      ')\g
INSERT INTO Diagnose VALUES ('3713','    Juvenile      ')\g
INSERT INTO Diagnose VALUES ('3719','    Other      ')\g
INSERT INTO Diagnose VALUES ('372','     PSORIATIC ARTHRITIS      ')\g
INSERT INTO Diagnose VALUES ('3721','    Typical psoriatic arthritis      ')\g
INSERT INTO Diagnose VALUES ('3722','    Psoriasis with features of psoriatic and rheumatoid arthritis      ')\g
INSERT INTO Diagnose VALUES ('3723','    Psoriasis with changes of classical rheumatoid arthritis      ')\g
INSERT INTO Diagnose VALUES ('3729','    Other      ')\g
INSERT INTO Diagnose VALUES ('373','     REITER SYNDROME      ')\g
INSERT INTO Diagnose VALUES ('374','     ANKYLOSING SPONDYLITIS      ')\g
INSERT INTO Diagnose VALUES ('375','     GOUT      ')\g
INSERT INTO Diagnose VALUES ('376','     CHONDROCALCINOSIS      ')\g
INSERT INTO Diagnose VALUES ('3761','    Calcium pyrophosphate deposition disease (pseudogout)      ')\g
INSERT INTO Diagnose VALUES ('3762','    Secondary to other disease (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('377','     DEGENERATIVE JOINT DISORDER, HYPERTROPHIC CHANGES, SPONDYLOSIS   exclude: degenerative disk disease (.78)      ')\g
INSERT INTO Diagnose VALUES ('3771','    Narrowed intervertebral foramen      ')\g
INSERT INTO Diagnose VALUES ('3772','    Spondylosis with narrow canal      ')\g
INSERT INTO Diagnose VALUES ('3773','    Spondylosis with ridge   include: ridge syndrome      ')\g
INSERT INTO Diagnose VALUES ('3774','    Spondylosis with hypertrophy of neural arches and dorsal indentations into vertebral canal      ')\g
INSERT INTO Diagnose VALUES ('3775','    Small joint involvement with productive changes  include: narrowing of the anteroposterior diameter of the spinal canal      ')\g
INSERT INTO Diagnose VALUES ('3776','    Small joint involvement with resulting vertebral malalignment      ')\g
INSERT INTO Diagnose VALUES ('3777','    Diffuse idiopathic skeletal hyperostosis (DISH, Forrestier disease)      ')\g
INSERT INTO Diagnose VALUES ('3779','    Other      ')\g
INSERT INTO Diagnose VALUES ('378','     DEGENERATIVE DISK DISORDER   exclude: spondylosis with ridge (.773), Schmorl node (.4961), Scheuermann disease (.4963)      ')\g
INSERT INTO Diagnose VALUES ('3781','    Narrowing of disk space (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('37811','   Degenerative      ')\g
INSERT INTO Diagnose VALUES ('37812','   Postspinal tap      ')\g
INSERT INTO Diagnose VALUES ('37813','   Infectious      ')\g
INSERT INTO Diagnose VALUES ('37814','   Postdiskography      ')\g
INSERT INTO Diagnose VALUES ('37819','   Other      ')\g
INSERT INTO Diagnose VALUES ('3782','    Vacuum intervertebral disk  include: post-traumatic      ')\g
INSERT INTO Diagnose VALUES ('3783','    Herniated or intervertebral protruded disk (for 5th number see box following .7839)      ')\g
INSERT INTO Diagnose VALUES ('37831','   Single herniated intervertebral disk, solitary, bulging      ')\g
INSERT INTO Diagnose VALUES ('37832','   Simple herniated intervertebral disk, multiple levels, bulging      ')\g
INSERT INTO Diagnose VALUES ('37833','   Extruded intervertebral disk      ')\g
INSERT INTO Diagnose VALUES ('37834','   Migrating intervertebral disk      ')\g
INSERT INTO Diagnose VALUES ('37839','   Other      ')\g
INSERT INTO Diagnose VALUES ('3789','    Other  exclude: erosive spondyloarthropathy (.532)      ')\g
INSERT INTO Diagnose VALUES ('379','     OTHER   include: ankylosis (also code cause)  exclude: hemochromatosis (.594), alkaptonuria (ochronosis) (.595)      ')\g
INSERT INTO Diagnose VALUES ('38','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('381','     CALCIFICATION, OSSIFICATION      ')\g
INSERT INTO Diagnose VALUES ('3811','    Spinal ligament   include: iliolumbar and sacrotuberous ligaments      ')\g
INSERT INTO Diagnose VALUES ('3812','    Intervertebral disk   exclude: alkaptonuria (ochronosis) (.595), chondrocalcinosis (.76), calcium pyrophosphate deposition disease (pseudogout) (.761)      ')\g
INSERT INTO Diagnose VALUES ('3813','    In neoplasm   include: in meningioma detected on plain films      ')\g
INSERT INTO Diagnose VALUES ('3814','    Inflammatory or granulomatous   include: calcified psoas abscess      ')\g
INSERT INTO Diagnose VALUES ('3815','    Vascular      ')\g
INSERT INTO Diagnose VALUES ('3819','    Other      ')\g
INSERT INTO Diagnose VALUES ('382','     NEUROGENIC, NEUROVASCULAR, PARALYTIC BONE AND/OR JOINT DISORDER      ')\g
INSERT INTO Diagnose VALUES ('3821','    Neuropathic skeletal disorder   include: neurosyphilis, leprosy, congenital indifference to pain      ')\g
INSERT INTO Diagnose VALUES ('3822','    Neurotrophic skeletal disorder  include: diabetes, thermal injury, vascular disease      ')\g
INSERT INTO Diagnose VALUES ('3829','    Other changes secondary to neurological disorder   include: cerebral palsy, birth injury      ')\g
INSERT INTO Diagnose VALUES ('384','     PAGET DISEASE      ')\g
INSERT INTO Diagnose VALUES ('3841','    Destructive phase      ')\g
INSERT INTO Diagnose VALUES ('3842','    Mixed phase      ')\g
INSERT INTO Diagnose VALUES ('3843','    Bone forming phase   include: ivory vertebra      ')\g
INSERT INTO Diagnose VALUES ('3849','    Other   exclude: pathological fracture (.416), other complication (code specifically)      ')\g
INSERT INTO Diagnose VALUES ('385','     FIBROUS DYSPLASIA      ')\g
INSERT INTO Diagnose VALUES ('386','     ABNORMAL ALIGNMENT OF SPINE      ')\g
INSERT INTO Diagnose VALUES ('3861','    Scoliosis      ')\g
INSERT INTO Diagnose VALUES ('38611','   Congenital malformation (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('38612','   Idiopathic infantile thoracolumbar      ')\g
INSERT INTO Diagnose VALUES ('38613','   Idiopathic, of childhood and adolescence      ')\g
INSERT INTO Diagnose VALUES ('38614','   Paralytic      ')\g
INSERT INTO Diagnose VALUES ('38615','   Incident to known disorder (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('38619','   Other      ')\g
INSERT INTO Diagnose VALUES ('3862','    Kyphosis, round back (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('3863','    Kyphoscoliosis      ')\g
INSERT INTO Diagnose VALUES ('3864','    Lordosis, swayback      ')\g
INSERT INTO Diagnose VALUES ('3865','    Increased lumbosacral angle      ')\g
INSERT INTO Diagnose VALUES ('3866','    Torticollis      ')\g
INSERT INTO Diagnose VALUES ('3867','    Straight back syndrome (see 5.8742)      ')\g
INSERT INTO Diagnose VALUES ('3869','    Other      ')\g
INSERT INTO Diagnose VALUES ('389','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('39','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('391','     FUNDAMENTAL OBSERVATION      ')\g
INSERT INTO Diagnose VALUES ('392','     ANATOMICAL DETAIL   include: interpediculate measurements      ')\g
INSERT INTO Diagnose VALUES ('393','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('394','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('399','     OTHER   include: phenomenon related to technique of examination      ')\g
INSERT INTO Diagnose VALUES ('41','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('411','     NORMAL ROUTINE PLAIN FILM   exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('412','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION      ')\g
INSERT INTO Diagnose VALUES ('4121','    Digital radiographic techniques, tomography, MR, nuclear medicine      ')\g
INSERT INTO Diagnose VALUES ('41211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('412111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('412112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('412113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('412114','  Delayed scanning following enhancement (e.g., for detection of liver lesions)      ')\g
INSERT INTO Diagnose VALUES ('412115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('412116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('412117','  Three-dimensional reconstruc-tion      ')\g
INSERT INTO Diagnose VALUES ('412118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('412119','  Other, including serial enhancement studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('41214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('412141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('4121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('4121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('4121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('4121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('4121415',' Specific resonance suppressed include: fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('4121416',' High-speed imaging   include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('4121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('4121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('412142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('412143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('412144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('412145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('412146','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('412147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('412149','  Other      ')\g
INSERT INTO Diagnose VALUES ('41215','   Digital radiography   exclude: with angiography (.124-3, 9.12-3)      ')\g
INSERT INTO Diagnose VALUES ('41216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('412161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('412162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('412163','  PET      ')\g
INSERT INTO Diagnose VALUES ('412164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('412165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('412166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('412167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('412168','  Therapeutic procedure    include: use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('412169','  Other.  See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('41217','   Nuclear medicine study-musculoskeletal      ')\g
INSERT INTO Diagnose VALUES ('412171','  Bone mineral measurement      ')\g
INSERT INTO Diagnose VALUES ('412172','  Bone scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('412173','  Metabolism study      ')\g
INSERT INTO Diagnose VALUES ('412174','  Bone marrow scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('412179','  Other      ')\g
INSERT INTO Diagnose VALUES ('41218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('41219','   Other      ')\g
INSERT INTO Diagnose VALUES ('4122','    Arthrography (see .48)      ')\g
INSERT INTO Diagnose VALUES ('4123','    Mensuration technique   include: leg length study, cortical thickness      ')\g
INSERT INTO Diagnose VALUES ('4124','    Angiography (see also 9.)      ')\g
INSERT INTO Diagnose VALUES ('41243','   Digital angiography      ')\g
INSERT INTO Diagnose VALUES ('4125','    Special nonroutine projection      ')\g
INSERT INTO Diagnose VALUES ('4126','    Biopsy or interventional      ')\g
INSERT INTO Diagnose VALUES ('41261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('41262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('41263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('41264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('41265','   Thrombolytic infusion      ')\g
INSERT INTO Diagnose VALUES ('41266','   Chemotherapeutic infusion      ')\g
INSERT INTO Diagnose VALUES ('41267','   Operative procedure  include: stereotaxis      ')\g
INSERT INTO Diagnose VALUES ('41269','   Other      ')\g
INSERT INTO Diagnose VALUES ('4127','    Bone age determination (see .55)      ')\g
INSERT INTO Diagnose VALUES ('4128','    Foreign body localization      ')\g
INSERT INTO Diagnose VALUES ('4129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('41291','   Therapy localization (port film)      ')\g
INSERT INTO Diagnose VALUES ('41292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('41293','   Soft tissue technique      ')\g
INSERT INTO Diagnose VALUES ('41294','   Supravoltage roentgenography      ')\g
INSERT INTO Diagnose VALUES ('41295','   Bone mineral analysis  exclude: analysis using radionuclides (.12171)      ')\g
INSERT INTO Diagnose VALUES ('41296','   Cine, video study      ')\g
INSERT INTO Diagnose VALUES ('41298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('412981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('412982','  A-mode      ')\g
INSERT INTO Diagnose VALUES ('412983','  B-scan      ')\g
INSERT INTO Diagnose VALUES ('412984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('412985','  Guided procedure for diagnosis   include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('412986','  Guided procedure for therapy   include: drainage      ')\g
INSERT INTO Diagnose VALUES ('412987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('412988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('412989','  Special nonroutine projection, mode, or transducer type      ')\g
INSERT INTO Diagnose VALUES ('41299','   Other   include: xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('413','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('4131','    Accessory center of ossification, unusual sesamoid, accessory ossicle   include: bipartite patella      ')\g
INSERT INTO Diagnose VALUES ('4132','    Developmental spur (e.g., supracondylar process)      ')\g
INSERT INTO Diagnose VALUES ('4133','    Density variant      ')\g
INSERT INTO Diagnose VALUES ('41331','   Physiological osteosclerosis of the newborn      ')\g
INSERT INTO Diagnose VALUES ('41332','   Transverse (growth) line      ')\g
INSERT INTO Diagnose VALUES ('41333','   Ivory epiphysis      ')\g
INSERT INTO Diagnose VALUES ('41334','   Bone island      ')\g
INSERT INTO Diagnose VALUES ('41339','   Other      ')\g
INSERT INTO Diagnose VALUES ('4134','    Irregular ossification  include: Meyer dysplasia      ')\g
INSERT INTO Diagnose VALUES ('4135','    Physiological bowing   exclude: Blount disease (.1481)      ')\g
INSERT INTO Diagnose VALUES ('4136','    Prematurity      ')\g
INSERT INTO Diagnose VALUES ('4137','    Vacuum joint, gas in symphysis in pregnancy      ')\g
INSERT INTO Diagnose VALUES ('4138','    Periosteal reaction of infancy      ')\g
INSERT INTO Diagnose VALUES ('4141','    Aplasia, agenesis   include: absent rib, amelia, hemimelia, phocomelia, absent pectoral muscle  exclude: Poland syndrome (.1621), amniotic band (.1498)      ')\g
INSERT INTO Diagnose VALUES ('4142','    Hypoplasia   include: hemiatrophy, brachydactyly    exclude: associated with hematological disorder (.653), Poland syndrome (.1621), amniotic band (.1498)      ')\g
INSERT INTO Diagnose VALUES ('4143','    Fusion, failure of segmentation   include: syndactyly, symphalangism, synostosis   exclude: polysyndactyly (.1441)      ')\g
INSERT INTO Diagnose VALUES ('4144','    Hypersegmentation, hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('41441','   Polydactyly, polysyndactyly      ')\g
INSERT INTO Diagnose VALUES ('41442','   Other supernumerary bone   include: accessory rib, os centrale      ')\g
INSERT INTO Diagnose VALUES ('41443','   Hemihypertrophy      ')\g
INSERT INTO Diagnose VALUES ('41444','   Localized gigantism" associated with congenital hemangioma, lymphangioma, arteriovenous fistula, neurofibroma"      ')\g
INSERT INTO Diagnose VALUES ('41449','   Other   include: congenital lymphedema   exclude: cerebral gigantism (.1712), pituitary gigantism (.511)      ')\g
INSERT INTO Diagnose VALUES ('4145','    Congenital bowing   exclude: camptomelic syndrome (.1624), physiological bowing (.135)      ')\g
INSERT INTO Diagnose VALUES ('4146','    Congenital dislocation   exclude: multiple congenital dislocations (Larsen syndrome) (.1626)      ')\g
INSERT INTO Diagnose VALUES ('4147','    Foot deformity      ')\g
INSERT INTO Diagnose VALUES ('41471','   Pes cavus   exclude: paralytic (.829)      ')\g
INSERT INTO Diagnose VALUES ('41472','   Flat foot-rigid (spastic)   include: vertical talus, intertarsal bar      ')\g
INSERT INTO Diagnose VALUES ('41473','   Flat foot-nonrigid (flexible)      ')\g
INSERT INTO Diagnose VALUES ('41474','   Metatarsus varus adductus      ')\g
INSERT INTO Diagnose VALUES ('41475','   Clubfoot (talipes equinovarus)      ')\g
INSERT INTO Diagnose VALUES ('41476','   Rocker-bottom foot      ')\g
INSERT INTO Diagnose VALUES ('41477','   Calcaneovarus, calcaneovalgus      ')\g
INSERT INTO Diagnose VALUES ('41479','   Other   exclude: arthrogryposis (.1893), paralytic deformity (.829)      ')\g
INSERT INTO Diagnose VALUES ('4148','    Developmental deformity      ')\g
INSERT INTO Diagnose VALUES ('41481','   Varus deformity   include: Blount disease   exclude: metatarsus varus adductus (.1474), physiological bowing (.135)      ')\g
INSERT INTO Diagnose VALUES ('41482','   Valgus deformity      ')\g
INSERT INTO Diagnose VALUES ('41483','   Anteversion      ')\g
INSERT INTO Diagnose VALUES ('41484','   Unequal leg lengths (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('41485','   Ectopia   include: Sprengel deformity      ')\g
INSERT INTO Diagnose VALUES ('41486','   Clinodactyly      ')\g
INSERT INTO Diagnose VALUES ('41487','   Camptodactyly      ')\g
INSERT INTO Diagnose VALUES ('41489','   Other   include: hammer toe, tibial torsion, Kirner deformity   exclude: genu recurvatum (.1493), paralytic deformity (.829), pes cavus (.1471), flat foot (.1472, .1473)      ')\g
INSERT INTO Diagnose VALUES ('4149','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('41491','   Madelung deformity, congenital or acquired   exclude: dyschondrosteosis (.1537)      ')\g
INSERT INTO Diagnose VALUES ('41492','   Congenital pseudarthrosis      ')\g
INSERT INTO Diagnose VALUES ('41493','   Genu recurvatum      ')\g
INSERT INTO Diagnose VALUES ('41494','   Congenital arteriovenous fistula   exclude: post-traumatic (.494)      ')\g
INSERT INTO Diagnose VALUES ('41495','   Discoid meniscus      ')\g
INSERT INTO Diagnose VALUES ('41496','   Pectus excavatum      ')\g
INSERT INTO Diagnose VALUES ('41497','   Pectus carinatum      ')\g
INSERT INTO Diagnose VALUES ('41498','   Effect of amniotic band      ')\g
INSERT INTO Diagnose VALUES ('41499','   Other   include: pterygium syndrome  exclude: abnormal fetus, monster (8.826), dysplasia (.15), dysostosis (.16); soft tissue edema (hydrops) (.833)      ')\g
INSERT INTO Diagnose VALUES ('415','     DYSPLASIA      ')\g
INSERT INTO Diagnose VALUES ('4151','    Osteochondrodysplasia-often lethal in neonate      ')\g
INSERT INTO Diagnose VALUES ('41511','   Achondrogenesis  exclude: Grebe syndrome (.1629)      ')\g
INSERT INTO Diagnose VALUES ('41512','   Asphyxiating thoracic dysplasia (Jeune)      ')\g
INSERT INTO Diagnose VALUES ('41513','   Thanatophoric dwarfism      ')\g
INSERT INTO Diagnose VALUES ('41514','   Short rib polydactyly      ')\g
INSERT INTO Diagnose VALUES ('41519','   Other   include: ateleosteogenesis, dyssegmental dwarfism   exclude: chondrodysplasia punctata (.1522), hypochondrogenesis  (.1528)      ')\g
INSERT INTO Diagnose VALUES ('4152','    Osteochondrodysplasia-manifest in neonate      ')\g
INSERT INTO Diagnose VALUES ('41521','   Achondroplasia      ')\g
INSERT INTO Diagnose VALUES ('41522','   Chondrodysplasia punctata (stippled epiphyses)   include: Zellweger (cerebrohepato�renal) syndrome      ')\g
INSERT INTO Diagnose VALUES ('41523','   Chondroectodermal dysplasia (Ellis van Creveld)  (see 5.1962 for cardiac lesion)      ')\g
INSERT INTO Diagnose VALUES ('41524','   Cleidocranial dysplasia (dysostosis)      ')\g
INSERT INTO Diagnose VALUES ('41525','   Diastrophic dwarfism      ')\g
INSERT INTO Diagnose VALUES ('41526','   Metatrophic dwarfism, Kneist syndrome      ')\g
INSERT INTO Diagnose VALUES ('41527','   Spondyloepiphyseal dysplasia congenita, Strudwick syndrome      ')\g
INSERT INTO Diagnose VALUES ('41528','   Spondyloepiphyseal dyplasia present at birth, other   include: hypochondrogenesis      ')\g
INSERT INTO Diagnose VALUES ('41529','   Other    include: mesomelic dwarfism (Nievergelt, Langer, Robinow), acromesomelic dwarfism, Dyggve-Melchior Clausen syndrome      ')\g
INSERT INTO Diagnose VALUES ('4153','    Osteochondrodysplasia-manifest in later life      ')\g
INSERT INTO Diagnose VALUES ('41530','   Hypochondroplasia      ')\g
INSERT INTO Diagnose VALUES ('41531','   Pseudoachondroplastic dysplasia      ')\g
INSERT INTO Diagnose VALUES ('41532','   Spondyloepiphyseal dysplasia tarda      ')\g
INSERT INTO Diagnose VALUES ('41533','   Spondyloepiphyseal dysplasia, other      ')\g
INSERT INTO Diagnose VALUES ('41534','   Spondylometaphyseal dysplasia   include: Kozlowski type      ')\g
INSERT INTO Diagnose VALUES ('41535','   Multiple epiphyseal dysplasia      ')\g
INSERT INTO Diagnose VALUES ('41536','   Metaphyseal chondrodysplasia (dysostosis)    include: Jansen, McKusick, or Schmid type, type with malabsorption and neutropenia, type with thymolymphopenia      ')\g
INSERT INTO Diagnose VALUES ('41537','   Dyschondrosteosis      ')\g
INSERT INTO Diagnose VALUES ('41538','   Acrodysplasia (types I and II)   include: trichorhinophalangeal syndrome (types I and II), epiphyseal (Thiemann), epiphysiometaphyseal (peripheral dysostosis)    exclude: acromesomelic dwarfism (.1529)      ')\g
INSERT INTO Diagnose VALUES ('41539','   Other    include: hereditary arthroophthalmopathy (Stickler), parastremmatic dwarfism, cranioectodermal dysplasia      ')\g
INSERT INTO Diagnose VALUES ('4154','    Dysplasia of cartilage, dysplasia of fibrous components      ')\g
INSERT INTO Diagnose VALUES ('41541','   Dysplasia epiphysealis hemimelica      ')\g
INSERT INTO Diagnose VALUES ('41542','   Multiple cartilaginous exostoses      ')\g
INSERT INTO Diagnose VALUES ('41543','   Enchondromatosis (Ollier)   include: with hemangiomata (Maffucci)      ')\g
INSERT INTO Diagnose VALUES ('41544','   Infantile (juvenile) fibromatosis      ')\g
INSERT INTO Diagnose VALUES ('41549','   Other   include: metachondromatosis exclude: cherubism (2.1545), fibrous dysplasia (.85)      ')\g
INSERT INTO Diagnose VALUES ('4155','    Dysplasia with abnormal cortical formation, modeling, density      ')\g
INSERT INTO Diagnose VALUES ('41551','   Osteogenesis imperfecta      ')\g
INSERT INTO Diagnose VALUES ('41552','   Osteopetrosis      ')\g
INSERT INTO Diagnose VALUES ('41553','   Pyknodysostosis      ')\g
INSERT INTO Diagnose VALUES ('41554','   Osteopoikilosis, melorheostosis, osteopathia striata      ')\g
INSERT INTO Diagnose VALUES ('41555','   Diaphyseal dysplasia    include: Engelmann disease,  craniodiaphyseal dysplasia, hyperphosphatasemia, Van Buchem  disease      ')\g
INSERT INTO Diagnose VALUES ('41556','   Metaphyseal dysplasia    include: Pyle disease,  craniometaphyseal dysplasia, frontometaphyseal dysplasia      ')\g
INSERT INTO Diagnose VALUES ('41557','   Oculodento-osseous dysplasia (oculodentodigital syndrome)      ')\g
INSERT INTO Diagnose VALUES ('41559','   Other    include: medullary stenosis,  osteodysplasty (Melnick Needle syndrome), pachydermoperiostosis, autosomal dominant osteosclerosis, osteomesopycnosis      ')\g
INSERT INTO Diagnose VALUES ('4159','    Other dysplasia      ')\g
INSERT INTO Diagnose VALUES ('416','     DYSOSTOSIS      ')\g
INSERT INTO Diagnose VALUES ('4161','    Soft tissue dysostosis      ')\g
INSERT INTO Diagnose VALUES ('41611','   Fibrodysplasia (myositis) ossificans progressiva      ')\g
INSERT INTO Diagnose VALUES ('41612','   Popliteal pterygium syndrome      ')\g
INSERT INTO Diagnose VALUES ('41619','   Other      ')\g
INSERT INTO Diagnose VALUES ('4162','    Limb dysostosis      ')\g
INSERT INTO Diagnose VALUES ('41621','   Poland syndrome (pectoral dysplasia-dysdactyly)      ')\g
INSERT INTO Diagnose VALUES ('41622','   Rubenstein-Taybi syndrome      ')\g
INSERT INTO Diagnose VALUES ('41623','   Nail-patella syndrome      ')\g
INSERT INTO Diagnose VALUES ('41624','   Camptomelic syndrome, familial congenital bowing      ')\g
INSERT INTO Diagnose VALUES ('41625','   Leri syndrome (pleonosteosis)      ')\g
INSERT INTO Diagnose VALUES ('41626','   Multiple congenital dislocations (Larsen syndrome)      ')\g
INSERT INTO Diagnose VALUES ('41629','   Other   include: whistling-face syndrome, Grebe syndrome   exclude: aplasia, agenesis (.141), hypoplasia (.142), fusion (.143), hypersegmentation (.144), hyperplasia (.144)      ')\g
INSERT INTO Diagnose VALUES ('4163','    Limb-facial dysostosis      ')\g
INSERT INTO Diagnose VALUES ('41631','   Orofacial digital syndrome      ')\g
INSERT INTO Diagnose VALUES ('41632','   Hypoglossia-hypodactylia syndrome      ')\g
INSERT INTO Diagnose VALUES ('41633','   Oto-palato-digital syndrome      ')\g
INSERT INTO Diagnose VALUES ('41639','   Other    include: Robert pseudo-thalidomide syndrome   exclude: oculodento-osseous dysplasia (.1557), craniosynostosis (1.143), facial dysostosis (2.16)      ')\g
INSERT INTO Diagnose VALUES ('4164','    Limb-visceral dysostosis      ')\g
INSERT INTO Diagnose VALUES ('41641','   Cardiomelic syndrome   include: Holt-Oram (see 5.1961 for cardiac lesion)      ')\g
INSERT INTO Diagnose VALUES ('41642','   Noonan syndrome (see 5.1964 for cardiac lesion)      ')\g
INSERT INTO Diagnose VALUES ('41643','   Hand-foot-uterus syndrome      ')\g
INSERT INTO Diagnose VALUES ('41649','   Other      ')\g
INSERT INTO Diagnose VALUES ('4165','    Spinal dysostosis (see 3.165 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('41651','   Spondylocostal dysostosis      ')\g
INSERT INTO Diagnose VALUES ('4166','    Facial dysostosis (see 2.166 for classification)      ')\g
INSERT INTO Diagnose VALUES ('4167','    Dysostosis involving multiple systems      ')\g
INSERT INTO Diagnose VALUES ('41671','   Basal cell nevus syndrome      ')\g
INSERT INTO Diagnose VALUES ('41679','   Other      ')\g
INSERT INTO Diagnose VALUES ('4169','    Other dysostosis   exclude: facial dysostosis (2.16),  craniosynostosis (1.143), cranial dysostosis (1.16), dysostosis with hematological disorder (4.653), Goldenhar syndrome (.1662)      ')\g
INSERT INTO Diagnose VALUES ('417','     PRIMARY DISTURBANCE OF GROWTH      ')\g
INSERT INTO Diagnose VALUES ('4171','    Increased growth      ')\g
INSERT INTO Diagnose VALUES ('41711','   Marfan syndrome (see 5.1971 for cardiac lesion)      ')\g
INSERT INTO Diagnose VALUES ('41712','   Cerebral gigantism    exclude: pituitary gigantism (.511)      ')\g
INSERT INTO Diagnose VALUES ('41719','   Other   include: Beckwith-Wiedemann syndrome, contractural arachnodactyly   exclude: homocystinuria (.597)      ')\g
INSERT INTO Diagnose VALUES ('4172','    Decreased growth      ')\g
INSERT INTO Diagnose VALUES ('41721','   Progeria  include: neonatal progeroid syndrome      ')\g
INSERT INTO Diagnose VALUES ('41722','   Weill-Marchesani syndrome      ')\g
INSERT INTO Diagnose VALUES ('41723','   Cornelia de Lange syndrome      ')\g
INSERT INTO Diagnose VALUES ('41724','   Russell-Silver dwarfism      ')\g
INSERT INTO Diagnose VALUES ('41725','   Primordial dwarfism      ')\g
INSERT INTO Diagnose VALUES ('41726','   Menkes (kinky hair) syndrome      ')\g
INSERT INTO Diagnose VALUES ('41727','   Intrauterine growth retardation (dwarfism), nonspecific      ')\g
INSERT INTO Diagnose VALUES ('41728','   Fetal alcohol syndrome, fetal Dilantin syndrome      ')\g
INSERT INTO Diagnose VALUES ('41729','   Other   include: bird-headed dwarfism, leprechaunism, Cockayne syndrome, Bloom syndrome, geroderma  osteodysplastica, Coffin-Lowery syndrome, Smith-Lemli-Opitz syndrome    exclude: dysplasia (.15), dysostosis (.16)      ')\g
INSERT INTO Diagnose VALUES ('418','     OTHER CONSTITUTIONAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4181','    Mucopolysaccharidosis (MPS)      ')\g
INSERT INTO Diagnose VALUES ('41811','   Hurler syndrome (MPS I-H)      ')\g
INSERT INTO Diagnose VALUES ('41812','   Hunter syndrome (MPS II)      ')\g
INSERT INTO Diagnose VALUES ('41813','   Sanfilippo syndrome (MPS III)      ')\g
INSERT INTO Diagnose VALUES ('41814','   Morquio syndrome (MPS IV)      ')\g
INSERT INTO Diagnose VALUES ('41815','   Maroteaux-Lamy syndrome (MPS VI)      ')\g
INSERT INTO Diagnose VALUES ('41819','   Other   include: Scheie (MPS I-S), Hurler-Scheie compound (MPS I-H/S), beta-glucuronidase deficiency (MPS VII)      ')\g
INSERT INTO Diagnose VALUES ('4182','    Dysostosis multiplex, other cause      ')\g
INSERT INTO Diagnose VALUES ('41821','   Lipomucopolysaccharidosis (ML I)      ')\g
INSERT INTO Diagnose VALUES ('41822','   I-cell disease (ML II)      ')\g
INSERT INTO Diagnose VALUES ('41823','   Pseudo-Hurler polydystrophy (ML III)      ')\g
INSERT INTO Diagnose VALUES ('41824','   GM1 gangliosidosis      ')\g
INSERT INTO Diagnose VALUES ('41825','   Glycoproteinosis   include: aspartylglycosaminuria, fucosidosis, mannosidosis, sialidosis      ')\g
INSERT INTO Diagnose VALUES ('41829','   Other    include: Austin type sulfatidosis, multiple sulfatase deficiency, Sandhoff disease   exclude: sphingolipidosis (.67), Farber disease (.679)      ')\g
INSERT INTO Diagnose VALUES ('4183','    Neurocutaneous syndrome (phacomatosis)      ')\g
INSERT INTO Diagnose VALUES ('41831','   Neurofibromatosis (see also .1444)      ')\g
INSERT INTO Diagnose VALUES ('41832','   Tuberous sclerosis      ')\g
INSERT INTO Diagnose VALUES ('41839','   Other    exclude: fibrous dysplasia (.85), infantile (juvenile) fibromatosis (.1544), Sturge-Weber (1.1833), Hippel-Lindau (1.1834)      ')\g
INSERT INTO Diagnose VALUES ('4184','    Autosomal aberration      ')\g
INSERT INTO Diagnose VALUES ('41841','   Trisomy 21 (Down syndrome)      ')\g
INSERT INTO Diagnose VALUES ('41842','   Trisomy 18      ')\g
INSERT INTO Diagnose VALUES ('41843','   Trisomy 13      ')\g
INSERT INTO Diagnose VALUES ('41849','   Other    include: cat-cry (cri-du-chat)      ')\g
INSERT INTO Diagnose VALUES ('4185','    Sex chromosomal aberration      ')\g
INSERT INTO Diagnose VALUES ('41851','   Turner syndrome      ')\g
INSERT INTO Diagnose VALUES ('41859','   Other    exclude: Noonan syndrome (.1642)      ')\g
INSERT INTO Diagnose VALUES ('4186','    Idiopathic osteolysis (see .3143)      ')\g
INSERT INTO Diagnose VALUES ('41861','   Hajdu-Chaney syndrome      ')\g
INSERT INTO Diagnose VALUES ('41862','   Other phalangeal acro-osteolysis      ')\g
INSERT INTO Diagnose VALUES ('41863','   Tarsal-carpal form of acro-osteolysis (multicentric)      ')\g
INSERT INTO Diagnose VALUES ('41869','   Other   include: Winchester syndrome      ')\g
INSERT INTO Diagnose VALUES ('4189','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('41891','   Ehlers-Danlos syndrome (see 5.1972 for cardiac lesion)      ')\g
INSERT INTO Diagnose VALUES ('41892','   Lipodystrophy      ')\g
INSERT INTO Diagnose VALUES ('41893','   Arthrogryposis multiplex congentia (amyotonia congenita)      ')\g
INSERT INTO Diagnose VALUES ('41894','   Lawrence-Moon-Bardot-Biedel syndrome      ')\g
INSERT INTO Diagnose VALUES ('41899','   Other   exclude: hypophosphatasia (.593), phenylketonuria (.596), sphingolipidosis (.67), idiopathic  hypercalcemia of infancy (.599)      ')\g
INSERT INTO Diagnose VALUES ('419','     OTHER   include: syndrome, unclassified      ')\g
INSERT INTO Diagnose VALUES ('42','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('420','     INFECTION CLASSIFIED BY ORGANISM (see 6.20 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('4201','    Gram-positive facultative bacterium      ')\g
INSERT INTO Diagnose VALUES ('42012','   Staphylococcus      ')\g
INSERT INTO Diagnose VALUES ('42013','   Streptococcus      ')\g
INSERT INTO Diagnose VALUES ('4202','    Gram-negative bacterium      ')\g
INSERT INTO Diagnose VALUES ('42025','   Salmonella      ')\g
INSERT INTO Diagnose VALUES ('42029','   Neisseria (gonococcus)      ')\g
INSERT INTO Diagnose VALUES ('4203','    Mycobacterium    exclude: tuberculosis (.23)      ')\g
INSERT INTO Diagnose VALUES ('42031','   Anonymous (atypical)      ')\g
INSERT INTO Diagnose VALUES ('42032','   Mycobacterium leprae (leprosy)      ')\g
INSERT INTO Diagnose VALUES ('4204','    Anaerobic bacterium      ')\g
INSERT INTO Diagnose VALUES ('42044','   Actinomycosis      ')\g
INSERT INTO Diagnose VALUES ('4205','    Fungus      ')\g
INSERT INTO Diagnose VALUES ('42051','   North American blastomycosis      ')\g
INSERT INTO Diagnose VALUES ('42052','   Coccidiodomycosis      ')\g
INSERT INTO Diagnose VALUES ('42053','   Histoplasmosis      ')\g
INSERT INTO Diagnose VALUES ('42054','   Cryptococcus (torulosis)      ')\g
INSERT INTO Diagnose VALUES ('42055','   Nocardiosis      ')\g
INSERT INTO Diagnose VALUES ('42056','   Aspergillosis      ')\g
INSERT INTO Diagnose VALUES ('42057','   Candidiasis (moniliasis)      ')\g
INSERT INTO Diagnose VALUES ('42059','   Other    include: mucormycosis, sporotrichosis, South American blastomycosis      ')\g
INSERT INTO Diagnose VALUES ('4206','    Virus or mycoplasma      ')\g
INSERT INTO Diagnose VALUES ('42063','   Measles      ')\g
INSERT INTO Diagnose VALUES ('42064','   Chickenpox      ')\g
INSERT INTO Diagnose VALUES ('42065','   Herpes      ')\g
INSERT INTO Diagnose VALUES ('42066','   Cytomegalovirus (cytomegalic inclusion disease)      ')\g
INSERT INTO Diagnose VALUES ('42068','   Human immunodeficiency virus infection (AIDS)      ')\g
INSERT INTO Diagnose VALUES ('4207','    Protozoa or spirochete      ')\g
INSERT INTO Diagnose VALUES ('42076','   Syphilis, congenital      ')\g
INSERT INTO Diagnose VALUES ('42077','   Syphilis, acquired      ')\g
INSERT INTO Diagnose VALUES ('42079','   Other  include: Lyme disease      ')\g
INSERT INTO Diagnose VALUES ('4208','    Helminth, roundworm, flatworm      ')\g
INSERT INTO Diagnose VALUES ('4209','    Other    include: rickettsia      ')\g
INSERT INTO Diagnose VALUES ('421','     OSTEOMYELITIS (for 4th number see box following .219)      ')\g
INSERT INTO Diagnose VALUES ('4211','    Typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('4212','    Unusual manifestation    include: epiphyseal infection      ')\g
INSERT INTO Diagnose VALUES ('4213','    Brodie abscess (cystic osteomyelitis)      ')\g
INSERT INTO Diagnose VALUES ('4214','    Associated with systemic disorder    include: sickle cell disease, granulomatous disease of childhood (Landing-Shirkey syndrome; neutrophil dysfunction) (see 7.296)      ')\g
INSERT INTO Diagnose VALUES ('4215','    Unusual organism   include: fungus, parasite, virus, spirochete  exclude: transplacental infection (.27)      ')\g
INSERT INTO Diagnose VALUES ('4216','    Secondary to trauma     include: postoperative      ')\g
INSERT INTO Diagnose VALUES ('4217','    Opportunistic infection      ')\g
INSERT INTO Diagnose VALUES ('4218','    Bone reaction secondary to soft tissue infection      ')\g
INSERT INTO Diagnose VALUES ('4219','    Other   include: narcotic abuse, chronic recurrent multifocal osteomyelitis   exclude: syphilis (.2076, .2077), leprosy (.2032), transplacental infection (.27)      ')\g
INSERT INTO Diagnose VALUES ('422','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('423','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('424','     SOFT TISSUE INFLAMMATION, SINUS TRACT, FISTULA      ')\g
INSERT INTO Diagnose VALUES ('4241','    Cellulitis      ')\g
INSERT INTO Diagnose VALUES ('4242','    Abscess, gas gangrene      ')\g
INSERT INTO Diagnose VALUES ('4243','    Ulcer (soft tissue)      ')\g
INSERT INTO Diagnose VALUES ('4244','    Sinus tract      ')\g
INSERT INTO Diagnose VALUES ('4245','    Fistula      ')\g
INSERT INTO Diagnose VALUES ('4246','    Lymphadenitis      ')\g
INSERT INTO Diagnose VALUES ('4247','    Maduromycosis (Madura foot)      ')\g
INSERT INTO Diagnose VALUES ('4249','    Other      ')\g
INSERT INTO Diagnose VALUES ('425','     PERIARTICULAR AND ARTICULAR SOFT TISSUE INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('4251','    Bursitis    include: calcific      ')\g
INSERT INTO Diagnose VALUES ('4252','    Synovitis    exclude: nonspecific (.785), pigmented villonodular (.784), tuberculous (.23)      ')\g
INSERT INTO Diagnose VALUES ('4253','    Tendinitis    include: calcified, Pellegrini-Stieda      ')\g
INSERT INTO Diagnose VALUES ('4254','    Joint effusion    exclude: traumatic (.492), suppurative arthritis (.26)      ')\g
INSERT INTO Diagnose VALUES ('4259','    Other    exclude: Baker cyst (.783), osteochondromatosis (.782), pigmented villonodular synovitis (.784), specific arthritis (.26, .7)      ')\g
INSERT INTO Diagnose VALUES ('426','     INFECTIOUS (SUPPURATIVE) ARTHRITIS    exclude: other arthritis (.7), e.g., rheumatoid (.71), Reiter syndrome (.73)      ')\g
INSERT INTO Diagnose VALUES ('427','     CHANGE SECONDARY TO TRANSPLACENTAL INFECTION    include: rubella, cytomegalovirus      ')\g
INSERT INTO Diagnose VALUES ('429','     OTHER    include: osteitis pubic, periosteal reaction associated with vascular insufficiency    exclude: condensans ilii (.863), soft tissue swelling of undetermined etiology (.833), leprosy (.2032), syphilis (.2076, .2077)      ')\g
INSERT INTO Diagnose VALUES ('4298','    Infectious complications of AIDS   include: bacillary angiomatosis      ')\g
INSERT INTO Diagnose VALUES ('43','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('431','     BENIGN NEOPLASM OF BONE      ')\g
INSERT INTO Diagnose VALUES ('4311','    Cartilaginous origin      ')\g
INSERT INTO Diagnose VALUES ('43111','   Chondroblastoma      ')\g
INSERT INTO Diagnose VALUES ('43112','   Chondromyxoid fibroma      ')\g
INSERT INTO Diagnose VALUES ('43113','   Osteochondroma (osteocartilaginous exostosis)   exclude: multiple exostoses (.1542)      ')\g
INSERT INTO Diagnose VALUES ('43114','   Enchondroma    exclude: enchondromatosis (Ollier) (.1543)      ')\g
INSERT INTO Diagnose VALUES ('43115','   Juxtacortical (parosteal) chondroma      ')\g
INSERT INTO Diagnose VALUES ('43119','   Other      ')\g
INSERT INTO Diagnose VALUES ('4312','    Osseous origin      ')\g
INSERT INTO Diagnose VALUES ('43121','   Osteoma      ')\g
INSERT INTO Diagnose VALUES ('43122','   Osteoid osteoma      ')\g
INSERT INTO Diagnose VALUES ('43123','   Osteoblastoma      ')\g
INSERT INTO Diagnose VALUES ('43129','   Other   include: Gardner syndrome (also code 7.3114)      ')\g
INSERT INTO Diagnose VALUES ('4313','    Fibrous origin      ')\g
INSERT INTO Diagnose VALUES ('43131','   Fibrous cortical defect, nonossifying fibroma      ')\g
INSERT INTO Diagnose VALUES ('43132','   Desmoplastic fibroma      ')\g
INSERT INTO Diagnose VALUES ('43139','   Other   include: ossifying fibroma   exclude: fibrous dysplasia (.85), infantile (juvenile) fibromatosis (.1544), parosteal desmoid (.434), fibrous histiocytoma (.3185)      ')\g
INSERT INTO Diagnose VALUES ('4314','    Vascular, lymphatic origin      ')\g
INSERT INTO Diagnose VALUES ('43141','   Hemangioma      ')\g
INSERT INTO Diagnose VALUES ('43142','   Lymphangioma      ')\g
INSERT INTO Diagnose VALUES ('43143','   Skeletal angiomatosis   include: Gorham disease (�vanishing bone�) (see also .186)      ')\g
INSERT INTO Diagnose VALUES ('43149','   Other   include: hemangiopericytoma (benign or malignant)      ')\g
INSERT INTO Diagnose VALUES ('4315','    Nerve tissue origin      ')\g
INSERT INTO Diagnose VALUES ('4318','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('43181','   Solitary bone cyst      ')\g
INSERT INTO Diagnose VALUES ('43182','   Giant cell tumor (benign or malignant)   exclude: of tendon sheath (.784)      ')\g
INSERT INTO Diagnose VALUES ('43183','   Aneurysmal bone cyst      ')\g
INSERT INTO Diagnose VALUES ('43184','   Adamantinoma      ')\g
INSERT INTO Diagnose VALUES ('43185','   Fibrous histiocytoma (benign) (see .3285, .376)      ')\g
INSERT INTO Diagnose VALUES ('43189','   Other      ')\g
INSERT INTO Diagnose VALUES ('4319','    Other   include: lipoma, mesenchymal neoplasm, intraosseous ganglion   exclude: epidermoid inclusion cyst (.369), pigmented villonodular synovitis (.784), soft tissue neoplasm (.36)      ')\g
INSERT INTO Diagnose VALUES ('432','     MALIGNANT NEOPLASM OF BONE-PRIMARY      ')\g
INSERT INTO Diagnose VALUES ('4321','    Cartilaginous origin      ')\g
INSERT INTO Diagnose VALUES ('43211','   Chondrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('43212','   Mesenchymal chondrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('43219','   Other      ')\g
INSERT INTO Diagnose VALUES ('4322','    Osseous origin      ')\g
INSERT INTO Diagnose VALUES ('43221','   Osteosarcoma      ')\g
INSERT INTO Diagnose VALUES ('43222','   Parosteal osteosarcoma      ')\g
INSERT INTO Diagnose VALUES ('43229','   Other      ')\g
INSERT INTO Diagnose VALUES ('4323','    Fibrous origin      ')\g
INSERT INTO Diagnose VALUES ('43231','   Fibrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('43239','   Other   exclude: fibrous histiocytoma (.3285)      ')\g
INSERT INTO Diagnose VALUES ('4324','    Vascular origin    include: angiosarcoma, hemangiosarcoma    exclude: hemangiopericytoma (.3149),  lymphoma (.34), myeloma (.345)      ')\g
INSERT INTO Diagnose VALUES ('4325','    Nerve tissue origin   include: neurosarcoma      ')\g
INSERT INTO Diagnose VALUES ('4328','    Unknown origin      ')\g
INSERT INTO Diagnose VALUES ('43281','   Ewing tumor      ')\g
INSERT INTO Diagnose VALUES ('43285','   Fibrous histiocytoma (malignant) (see .3185, .376)      ')\g
INSERT INTO Diagnose VALUES ('43289','   Other      ')\g
INSERT INTO Diagnose VALUES ('4329','    Other   include: neoplasm associated with AIDS      ')\g
INSERT INTO Diagnose VALUES ('433','     MALIGNANT NEOPLASM OF BONE-SECONDARY (METASTATIC) (for 4th number see box following .33)  exclude: invasion from adjoining lesion (.37).      ')\g
INSERT INTO Diagnose VALUES ('434','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('4341','    Leukemia      ')\g
INSERT INTO Diagnose VALUES ('4342','    Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('4343','    Non-Hodgkin lymphoma      ')\g
INSERT INTO Diagnose VALUES ('43431','   Lymphocytic well differentiated      ')\g
INSERT INTO Diagnose VALUES ('43432','   Nodular      ')\g
INSERT INTO Diagnose VALUES ('43433','   Diffuse      ')\g
INSERT INTO Diagnose VALUES ('43434','   Histiocytic      ')\g
INSERT INTO Diagnose VALUES ('43435','   Burkitt      ')\g
INSERT INTO Diagnose VALUES ('43439','   Other      ')\g
INSERT INTO Diagnose VALUES ('4345','    Myeloma      ')\g
INSERT INTO Diagnose VALUES ('43451','   Plasmacytoma      ')\g
INSERT INTO Diagnose VALUES ('43452','   Multiple myeloma      ')\g
INSERT INTO Diagnose VALUES ('43459','   Other      ')\g
INSERT INTO Diagnose VALUES ('4346','    Kaposi sarcoma      ')\g
INSERT INTO Diagnose VALUES ('434720','  Acute      ')\g
INSERT INTO Diagnose VALUES ('434751','  Subacute      ')\g
INSERT INTO Diagnose VALUES ('434779','  Chronic      ')\g
INSERT INTO Diagnose VALUES ('4349','    Other    include: Waldenstrom macroglobulinemia      ')\g
INSERT INTO Diagnose VALUES ('435','     NEOPLASM ARISING IN PREEXISTING BENIGN DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4351','    Bone neoplasm (e.g., enchondroma, osteochondroma)      ')\g
INSERT INTO Diagnose VALUES ('4352','    Infection      ')\g
INSERT INTO Diagnose VALUES ('4353','    Paget disease      ')\g
INSERT INTO Diagnose VALUES ('4354','    Irradiated bone      ')\g
INSERT INTO Diagnose VALUES ('4355','    Fibrous dysplasia      ')\g
INSERT INTO Diagnose VALUES ('4356','    Bone infarction      ')\g
INSERT INTO Diagnose VALUES ('4359','    Other      ')\g
INSERT INTO Diagnose VALUES ('436','     BENIGN SOFT TISSUE NEOPLASM (WITH OR WITHOUT BONE INVOLVEMENT)      ')\g
INSERT INTO Diagnose VALUES ('4361','    Ganglion   exclude: intraosseous ganglion (.319)      ')\g
INSERT INTO Diagnose VALUES ('4362','    Hemangioma, lymphangioma, cystic hygroma  exclude: with localized gigantism" (.1444)"      ')\g
INSERT INTO Diagnose VALUES ('4363','    Lipoma, lipoblastoma      ')\g
INSERT INTO Diagnose VALUES ('4364','    Neurofibroma, neurilemmoma   exclude: neurofibromatosis (.1831)      ')\g
INSERT INTO Diagnose VALUES ('4365','    Osteochondromatosis      ')\g
INSERT INTO Diagnose VALUES ('4366','    Teratoma, dermoid (benign or malignant)      ')\g
INSERT INTO Diagnose VALUES ('4369','    Other   include: epidermoid inclusion cyst, glomus tumor   exclude: Baker cyst (.783), pigmented villonodular synovitis (giant cell tumor of tendon sheath) (.784), infantile (juvenile) fibromatosis (.1544)      ')\g
INSERT INTO Diagnose VALUES ('437','     MALIGNANT SOFT TISSUE NEOPLASM (WITH OR WITHOUT BONE INVOLVEMENT)      ')\g
INSERT INTO Diagnose VALUES ('4371','    Liposarcoma      ')\g
INSERT INTO Diagnose VALUES ('4372','    Fibrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('4373','    Rhabdomyosarcoma      ')\g
INSERT INTO Diagnose VALUES ('4374','    Synovioma      ')\g
INSERT INTO Diagnose VALUES ('4375','    Osteosarcoma, chondrosarcoma      ')\g
INSERT INTO Diagnose VALUES ('4379','    Other  exclude: leukemia (.34), lymphoma (.34), myeloma (.34), arising in pre-existing benign disorder (.35)      ')\g
INSERT INTO Diagnose VALUES ('439','     OTHER   include: recurrent neoplasm, metastatic neoplasm in soft tissue   exclude: fibrous dysplasia (.85)      ')\g
INSERT INTO Diagnose VALUES ('44','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('441','     FRACTURE  (for 4th number see box following .4199)      ')\g
INSERT INTO Diagnose VALUES ('4411','    Linear or spiral      ')\g
INSERT INTO Diagnose VALUES ('44111','   Colles      ')\g
INSERT INTO Diagnose VALUES ('44112','   Smith      ')\g
INSERT INTO Diagnose VALUES ('44113','   Monteggia      ')\g
INSERT INTO Diagnose VALUES ('44114','   Galleazzi      ')\g
INSERT INTO Diagnose VALUES ('44115','   Bennett      ')\g
INSERT INTO Diagnose VALUES ('44116','   Other      ')\g
INSERT INTO Diagnose VALUES ('4412','    Impacted   include: torus      ')\g
INSERT INTO Diagnose VALUES ('44121','   Colles      ')\g
INSERT INTO Diagnose VALUES ('44122','   Smith      ')\g
INSERT INTO Diagnose VALUES ('44123','   Monteggia      ')\g
INSERT INTO Diagnose VALUES ('44124','   Galleazzi      ')\g
INSERT INTO Diagnose VALUES ('44125','   Bennett      ')\g
INSERT INTO Diagnose VALUES ('44126','   Other      ')\g
INSERT INTO Diagnose VALUES ('4413','    Comminuted      ')\g
INSERT INTO Diagnose VALUES ('44131','   Colles      ')\g
INSERT INTO Diagnose VALUES ('44132','   Smith      ')\g
INSERT INTO Diagnose VALUES ('44133','   Monteggia      ')\g
INSERT INTO Diagnose VALUES ('44134','   Galleazzi      ')\g
INSERT INTO Diagnose VALUES ('44135','   Bennett      ')\g
INSERT INTO Diagnose VALUES ('44136','   Other      ')\g
INSERT INTO Diagnose VALUES ('4414','    Epiphyseal      ')\g
INSERT INTO Diagnose VALUES ('44141','   Salter 1      ')\g
INSERT INTO Diagnose VALUES ('44142','   Salter 2      ')\g
INSERT INTO Diagnose VALUES ('44143','   Salter 3      ')\g
INSERT INTO Diagnose VALUES ('44144','   Salter 4      ')\g
INSERT INTO Diagnose VALUES ('44145','   Salter 5      ')\g
INSERT INTO Diagnose VALUES ('44146','   Epiphysiolysis    include: slipped femoral capital epiphysis      ')\g
INSERT INTO Diagnose VALUES ('44149','   Other      ')\g
INSERT INTO Diagnose VALUES ('4415','    Stress (march)      ')\g
INSERT INTO Diagnose VALUES ('44151','   Colles      ')\g
INSERT INTO Diagnose VALUES ('44152','   Smith      ')\g
INSERT INTO Diagnose VALUES ('44153','   Monteggia      ')\g
INSERT INTO Diagnose VALUES ('44154','   Galleazzi      ')\g
INSERT INTO Diagnose VALUES ('44155','   Bennett      ')\g
INSERT INTO Diagnose VALUES ('44156','   Other      ')\g
INSERT INTO Diagnose VALUES ('4416','    Pathological (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('44161','   Colles      ')\g
INSERT INTO Diagnose VALUES ('44162','   Smith      ')\g
INSERT INTO Diagnose VALUES ('44163','   Monteggia      ')\g
INSERT INTO Diagnose VALUES ('44164','   Galleazzi      ')\g
INSERT INTO Diagnose VALUES ('44165','   Bennett      ')\g
INSERT INTO Diagnose VALUES ('44166','   Other      ')\g
INSERT INTO Diagnose VALUES ('4417','    Healed, old      ')\g
INSERT INTO Diagnose VALUES ('44171','   Colles      ')\g
INSERT INTO Diagnose VALUES ('44172','   Smith      ')\g
INSERT INTO Diagnose VALUES ('44173','   Monteggia      ')\g
INSERT INTO Diagnose VALUES ('44174','   Galleazzi      ')\g
INSERT INTO Diagnose VALUES ('44175','   Bennett      ')\g
INSERT INTO Diagnose VALUES ('44176','   Other      ')\g
INSERT INTO Diagnose VALUES ('4418','    Multiple      ')\g
INSERT INTO Diagnose VALUES ('44181','   Colles      ')\g
INSERT INTO Diagnose VALUES ('44182','   Smith      ')\g
INSERT INTO Diagnose VALUES ('44183','   Monteggia      ')\g
INSERT INTO Diagnose VALUES ('44184','   Galleazzi      ')\g
INSERT INTO Diagnose VALUES ('44185','   Bennett      ')\g
INSERT INTO Diagnose VALUES ('44186','   Other      ')\g
INSERT INTO Diagnose VALUES ('4419','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('44191','   Avulsion, chip      ')\g
INSERT INTO Diagnose VALUES ('44192','   Cortical, greenstick,  subperiosteal      ')\g
INSERT INTO Diagnose VALUES ('44193','   Traumatic amputation exclude: self-mutilation (.496)      ')\g
INSERT INTO Diagnose VALUES ('44194','   Traumatic periostitis      ')\g
INSERT INTO Diagnose VALUES ('44195','   Child abuse (battered child)    exclude: sexual abuse (8.419)      ')\g
INSERT INTO Diagnose VALUES ('44199','   Other   include: pseudofracture (also code cause), refracture, fracture of spur or sesamoid, traumatic bowing   exclude: complication of fracture (.43), congenital indifference to pain (.821)      ')\g
INSERT INTO Diagnose VALUES ('4422','    With fracture      ')\g
INSERT INTO Diagnose VALUES ('443','     COMPLICATION OF FRACTURE OR DISLOCATION      ')\g
INSERT INTO Diagnose VALUES ('4431','    Malunion   include: altered mortise, altered weight  bearing line, loss of carrying angle      ')\g
INSERT INTO Diagnose VALUES ('4432','    Nonunion   include: delayed union, fibrous union, pseudarthrosis   exclude: congenital pseudarthrosis (.1492)      ')\g
INSERT INTO Diagnose VALUES ('4433','    Premature epiphyseal closure      ')\g
INSERT INTO Diagnose VALUES ('4434','    Parosteal (periosteal) desmoid      ')\g
INSERT INTO Diagnose VALUES ('4435','    Epidermoid inclusion cyst of bone      ')\g
INSERT INTO Diagnose VALUES ('4436','    Soft tissue change   include: contracture, atrophy      ')\g
INSERT INTO Diagnose VALUES ('4441','    Bone infarct (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('4442','    Subcortical (osteochondritis dissecans)      ')\g
INSERT INTO Diagnose VALUES ('4443','    Perthes disease      ')\g
INSERT INTO Diagnose VALUES ('4449','    Other   include: Osgood-Schlatter disease   exclude: Blount disease (.1481), loose body in joint (.48), epiphysiolysis (.4146), neuropathic skeletal disorder (.821),osteochondromatosis (.782)      ')\g
INSERT INTO Diagnose VALUES ('445','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('4451','    Fusion, arthrodesis, osteotomy      ')\g
INSERT INTO Diagnose VALUES ('4452','    Amputation      ')\g
INSERT INTO Diagnose VALUES ('4453','    Metallic fixation      ')\g
INSERT INTO Diagnose VALUES ('4454','    Prosthesis      ')\g
INSERT INTO Diagnose VALUES ('4455','    Transplant      ')\g
INSERT INTO Diagnose VALUES ('4456','    Bone graft (donor) site      ')\g
INSERT INTO Diagnose VALUES ('4458','    Complication of surgery or interventional procedure   include: of biopsy, of aspiration    exclude: of catheterization (9.44), of angiography (9.44)      ')\g
INSERT INTO Diagnose VALUES ('4459','    Other      ')\g
INSERT INTO Diagnose VALUES ('446','     FOREIGN BODY OR MEDICATION      ')\g
INSERT INTO Diagnose VALUES ('4461','    Opaque      ')\g
INSERT INTO Diagnose VALUES ('44611','   Catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('44612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('44613','   Catheter (or other tube) in unsatisfactory position      ')\g
INSERT INTO Diagnose VALUES ('44617','   Complication of catheter (or other tube)      ')\g
INSERT INTO Diagnose VALUES ('44619','   Other      ')\g
INSERT INTO Diagnose VALUES ('4462','    Nonopaque      ')\g
INSERT INTO Diagnose VALUES ('4469','    Other      ')\g
INSERT INTO Diagnose VALUES ('447','     EFFECT OF RADIATION   include: radium poisoning      ')\g
INSERT INTO Diagnose VALUES ('448','     CARTILAGE, LIGAMENT, MENISCUS, CAPSULAR INJURY      ')\g
INSERT INTO Diagnose VALUES ('4481','    Shoulder      ')\g
INSERT INTO Diagnose VALUES ('44811','   Anterior capsular dislocation      ')\g
INSERT INTO Diagnose VALUES ('44812','   Posterior capsular dislocation      ')\g
INSERT INTO Diagnose VALUES ('44813','   Rotator cuff tear      ')\g
INSERT INTO Diagnose VALUES ('44814','   Adhesive capsulitis      ')\g
INSERT INTO Diagnose VALUES ('44815','   Loose body      ')\g
INSERT INTO Diagnose VALUES ('44819','   Other      ')\g
INSERT INTO Diagnose VALUES ('4482','    Elbow      ')\g
INSERT INTO Diagnose VALUES ('4483','    Wrist      ')\g
INSERT INTO Diagnose VALUES ('4484','    Hip      ')\g
INSERT INTO Diagnose VALUES ('44841','   Articular cartilage degeneration (see also .77)      ')\g
INSERT INTO Diagnose VALUES ('44842','   Displaced labrum      ')\g
INSERT INTO Diagnose VALUES ('44843','   Communicating bursa      ')\g
INSERT INTO Diagnose VALUES ('44844','   Other loose body      ')\g
INSERT INTO Diagnose VALUES ('44849','   Other   exclude: synovitis (.785)      ')\g
INSERT INTO Diagnose VALUES ('4485','    Knee      ')\g
INSERT INTO Diagnose VALUES ('44851','   Articular cartilage degeneration      ')\g
INSERT INTO Diagnose VALUES ('44852','   Meniscal tear      ')\g
INSERT INTO Diagnose VALUES ('44853','   Peripheral meniscal separation      ')\g
INSERT INTO Diagnose VALUES ('44854','   Meniscal cyst      ')\g
INSERT INTO Diagnose VALUES ('44855','   Meniscal degeneration      ')\g
INSERT INTO Diagnose VALUES ('44856','   Postmeniscectomy      ')\g
INSERT INTO Diagnose VALUES ('44857','   Ligament tear      ')\g
INSERT INTO Diagnose VALUES ('44859','   Other   include: loose body   exclude: synovitis (.785), discoid meniscus (.1495)      ')\g
INSERT INTO Diagnose VALUES ('4486','    Ankle      ')\g
INSERT INTO Diagnose VALUES ('4489','    Other      ')\g
INSERT INTO Diagnose VALUES ('449','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('4491','    Laceration, soft tissue swelling secondary to trauma, wringer injury      ')\g
INSERT INTO Diagnose VALUES ('4492','    Traumatic hemarthrosis or hydrarthrosis      ')\g
INSERT INTO Diagnose VALUES ('4493','    Soft tissue emphysema      ')\g
INSERT INTO Diagnose VALUES ('4494','    Arteriovenous fistula, post-traumatic   include: pseudoaneurysm   exclude: congenital fistula (.1494)      ')\g
INSERT INTO Diagnose VALUES ('4495','    Fat-fluid level in joint      ')\g
INSERT INTO Diagnose VALUES ('4496','    Self-mutilation (see .599)   include: self-amputation      ')\g
INSERT INTO Diagnose VALUES ('4499','    Other   include: intravascular gas    exclude: calcified hematoma (.811), thermal injury (.822), �vanishing bone� (.3143), Sudeck atrophy (.565)      ')\g
INSERT INTO Diagnose VALUES ('45','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('451','     PITUITARY DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4511','    Gigantism    exclude: cerebral gigantism (.1712)      ')\g
INSERT INTO Diagnose VALUES ('4512','    Acromegaly      ')\g
INSERT INTO Diagnose VALUES ('4513','    Pituitary dwarfism      ')\g
INSERT INTO Diagnose VALUES ('4519','    Other  include: psychosocial dwarfism (emotional deprivation syndrome)      ')\g
INSERT INTO Diagnose VALUES ('452','     THYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4521','    Cretinism, hypothyroidism      ')\g
INSERT INTO Diagnose VALUES ('4522','    Hyperthyroidism (see also .523)      ')\g
INSERT INTO Diagnose VALUES ('4523','    Thyroid acropachy      ')\g
INSERT INTO Diagnose VALUES ('4529','    Other      ')\g
INSERT INTO Diagnose VALUES ('453','     PARATHYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4531','    Primary hyperparathyroidism      ')\g
INSERT INTO Diagnose VALUES ('45311','   Subperiosteal resorption      ')\g
INSERT INTO Diagnose VALUES ('45312','   Brown tumor      ')\g
INSERT INTO Diagnose VALUES ('45319','   Other  exclude: congenital (.539)      ')\g
INSERT INTO Diagnose VALUES ('4532','    Secondary hyperparathyroidism, nonrenal      ')\g
INSERT INTO Diagnose VALUES ('45321','   Subperiosteal resorption      ')\g
INSERT INTO Diagnose VALUES ('45322','   Osteosclerosis      ')\g
INSERT INTO Diagnose VALUES ('45323','   Brown tumor      ')\g
INSERT INTO Diagnose VALUES ('45329','   Other      ')\g
INSERT INTO Diagnose VALUES ('4533','    Hypoparathyroidism      ')\g
INSERT INTO Diagnose VALUES ('4534','    Albright hereditary osteodystrophy (pseudohypoparathyroidism)      ')\g
INSERT INTO Diagnose VALUES ('4539','    Other   include: congenital hyperparathyroidism   exclude: renal osteodystrophy (.573)      ')\g
INSERT INTO Diagnose VALUES ('454','     ADRENAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4541','    Cushing syndrome, endogenous      ')\g
INSERT INTO Diagnose VALUES ('4542','    Cushing syndrome, exogenous   include: steroid therapy      ')\g
INSERT INTO Diagnose VALUES ('4543','    Adrenogenital syndrome      ')\g
INSERT INTO Diagnose VALUES ('4549','    Other      ')\g
INSERT INTO Diagnose VALUES ('455','     ABNORMAL BONE AGE (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('4551','    Retarded bone age      ')\g
INSERT INTO Diagnose VALUES ('4552','    Advanced bone age      ')\g
INSERT INTO Diagnose VALUES ('4553','    Dysharmonic maturation      ')\g
INSERT INTO Diagnose VALUES ('4559','    Other      ')\g
INSERT INTO Diagnose VALUES ('456','     OSTEOPOROSIS      ')\g
INSERT INTO Diagnose VALUES ('4561','    Scurvy      ')\g
INSERT INTO Diagnose VALUES ('4562','    Senile, postmenopausal      ')\g
INSERT INTO Diagnose VALUES ('4563','    Idiopathic juvenile      ')\g
INSERT INTO Diagnose VALUES ('4564','    Disuse atrophy      ')\g
INSERT INTO Diagnose VALUES ('4565','    Sudeck atrophy (reflex sympathetic dystrophy syndrome)      ')\g
INSERT INTO Diagnose VALUES ('4569','    Other   include: malnutrition, trace element (e.g., copper) deficiency (see .579), transient osteoporosis     exclude: Cushing syndrome (.541, .542), hyperthyroidism (.522), Turner syndrome (.1851), paralytic (.82)      ')\g
INSERT INTO Diagnose VALUES ('457','     OSTEOMALACIA, RENAL OSTEODYSTROPHY      ')\g
INSERT INTO Diagnose VALUES ('4571','    Osteomalacia, rickets-dietary      ')\g
INSERT INTO Diagnose VALUES ('4572','    Osteomalacia, rickets-other      ')\g
INSERT INTO Diagnose VALUES ('45721','   Gastroenterogenous   include: biliary atresia, malabsorption      ')\g
INSERT INTO Diagnose VALUES ('45722','   Organic renal disease (see also .573)   include: chronic pyelonephritis, cystic disease      ')\g
INSERT INTO Diagnose VALUES ('45723','   Renal tubular dysfunction  include: cystinosis, Fanconi syndrome, Albright-Butler syndrome, vitamin D-resistant rickets, tyrosinosis      ')\g
INSERT INTO Diagnose VALUES ('45724','   Vitamin D-dependent rickets      ')\g
INSERT INTO Diagnose VALUES ('45729','   Other   include: Wilson disease, phenytoin (Dilantin) therapy, atypical axial osteomalacia (see .789, 1.592)      ')\g
INSERT INTO Diagnose VALUES ('4573','    Renal osteodystrophy (osteomalacia plus secondary hyperparathyroidism)      ')\g
INSERT INTO Diagnose VALUES ('4579','    Other   include: copper deficiency (see .569)   exclude: hypophosphatasia (.593), phenylketonuria (.596)      ')\g
INSERT INTO Diagnose VALUES ('458','     INTOXICATION, POISONING      ')\g
INSERT INTO Diagnose VALUES ('4581','    Heavy metal      ')\g
INSERT INTO Diagnose VALUES ('45811','   Lead      ')\g
INSERT INTO Diagnose VALUES ('45819','   Other      ')\g
INSERT INTO Diagnose VALUES ('4582','    Fluorine      ')\g
INSERT INTO Diagnose VALUES ('4583','    Hypervitaminosis      ')\g
INSERT INTO Diagnose VALUES ('45831','   Vitamin A      ')\g
INSERT INTO Diagnose VALUES ('45832','   Vitamin D      ')\g
INSERT INTO Diagnose VALUES ('45839','   Other      ')\g
INSERT INTO Diagnose VALUES ('4589','    Other      ')\g
INSERT INTO Diagnose VALUES ('459','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('4591','    Growth acceleration, secondary    exclude: conditions listed .171      ')\g
INSERT INTO Diagnose VALUES ('4592','    Growth retardation, secondary   exclude: conditions listed .172      ')\g
INSERT INTO Diagnose VALUES ('4593','    Hypophosphatasia      ')\g
INSERT INTO Diagnose VALUES ('4594','    Hemochromatosis      ')\g
INSERT INTO Diagnose VALUES ('4595','    Alkaptonuria (ochronosis)      ')\g
INSERT INTO Diagnose VALUES ('4596','    Phenylketonuria      ')\g
INSERT INTO Diagnose VALUES ('4597','    Homocystinuria (see also with cardiovascular abnormality 5.1973)      ')\g
INSERT INTO Diagnose VALUES ('4599','    Other   include: idiopathic hypercalcemia of infancy,  congenital hyperuricemia (Lesch-Nyhan syndrome) (see .496)      ')\g
INSERT INTO Diagnose VALUES ('46','      OTHER GENERALIZED SYSTEMIC DISORDER    exclude: arthritis (.7)      ')\g
INSERT INTO Diagnose VALUES ('461','     CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4612','    Lupus erythematosus      ')\g
INSERT INTO Diagnose VALUES ('4613','    Progressive systemic sclerosis (scleroderma)      ')\g
INSERT INTO Diagnose VALUES ('46131','   Osseous change      ')\g
INSERT INTO Diagnose VALUES ('46132','   Soft tissue change      ')\g
INSERT INTO Diagnose VALUES ('46139','   Other   include: diffuse fasciitis with eosinophilia      ')\g
INSERT INTO Diagnose VALUES ('4614','    Dermatomyositis, polymyositis      ')\g
INSERT INTO Diagnose VALUES ('4619','    Other   exclude: arthritis (.7)      ')\g
INSERT INTO Diagnose VALUES ('462','     VASCULITIS      ')\g
INSERT INTO Diagnose VALUES ('4621','    Polyarteritis (periarteritis) nodosa      ')\g
INSERT INTO Diagnose VALUES ('4629','    Other    include: mucocutaneous lymph node syndrome      ')\g
INSERT INTO Diagnose VALUES ('463','     ALLERGIC STATE      ')\g
INSERT INTO Diagnose VALUES ('464','     COMPLICATION OF DRUG THERAPY, DRUG USE  include: methotrexate, prostaglandin (periostitis)   exclude: hypervitaminosis (.583),phenytoin (Dilantin) therapy (.5729), steroid (.542)      ')\g
INSERT INTO Diagnose VALUES ('465','     HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('4652','    Thalassemia or variant      ')\g
INSERT INTO Diagnose VALUES ('4653','    Other congenital (primary) anemia   include: erythroblastosis, Fanconi, spherocytosis, thrombocytopenia absent radius, Diamond-Blackfan      ')\g
INSERT INTO Diagnose VALUES ('4654','    Iron deficiency anemia      ')\g
INSERT INTO Diagnose VALUES ('4655','    Other acquired (secondary) anemia      ')\g
INSERT INTO Diagnose VALUES ('4656','    Hemophilia      ')\g
INSERT INTO Diagnose VALUES ('46561','   Joint or soft tissue change      ')\g
INSERT INTO Diagnose VALUES ('46562','   Osseous change   exclude: pseudotumor (.6563)      ')\g
INSERT INTO Diagnose VALUES ('46563','   Pseudotumor      ')\g
INSERT INTO Diagnose VALUES ('46569','   Other      ')\g
INSERT INTO Diagnose VALUES ('4657','    Myeloproliferative disorders      ')\g
INSERT INTO Diagnose VALUES ('46571','   Polycythemia vera      ')\g
INSERT INTO Diagnose VALUES ('46572','   Idiopathic myelofibrosis      ')\g
INSERT INTO Diagnose VALUES ('46579','   Other      ')\g
INSERT INTO Diagnose VALUES ('4658','    Marrow Extinction disorder      ')\g
INSERT INTO Diagnose VALUES ('46581','   Aplastic anemia (primary or idiopathic)      ')\g
INSERT INTO Diagnose VALUES ('46582','   Drug-induced myelosuppression      ')\g
INSERT INTO Diagnose VALUES ('46583','   Radiation-induced fatty change (or conversion)      ')\g
INSERT INTO Diagnose VALUES ('46584','   Premature fatty change (or conversion)      ')\g
INSERT INTO Diagnose VALUES ('46589','   Other      ')\g
INSERT INTO Diagnose VALUES ('4659','    Other   include: extramedullary hematopoiesis  exclude: leukemia (.341)      ')\g
INSERT INTO Diagnose VALUES ('466','     HISTIOCYTOSIS (LANGERHANS CELL HISTIOCYTOSIS)      ')\g
INSERT INTO Diagnose VALUES ('4661','    Letterer-Siwe disease      ')\g
INSERT INTO Diagnose VALUES ('4662','    Hand-Schuller-Christian disease      ')\g
INSERT INTO Diagnose VALUES ('4663','    Eosinophilic granuloma      ')\g
INSERT INTO Diagnose VALUES ('4669','    Other      ')\g
INSERT INTO Diagnose VALUES ('467','     SPHINGOLIPIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('4671','    Gaucher disease      ')\g
INSERT INTO Diagnose VALUES ('4672','    Neimann-Pick disease      ')\g
INSERT INTO Diagnose VALUES ('4673','    Tay-Sachs disease      ')\g
INSERT INTO Diagnose VALUES ('4679','    Other   include: Fabry disease, Farber disease      ')\g
INSERT INTO Diagnose VALUES ('468','     OTHER INFILTRATIVE DISORDER   include: amyloid   exclude: Wilson disease (.5729)      ')\g
INSERT INTO Diagnose VALUES ('469','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('4695','    Infantile cortical hyperostosis (Caffey)      ')\g
INSERT INTO Diagnose VALUES ('4699','    Other    include: mastocytosis (urticaria pigmentosa), epidermolysis bullosa, pemphigus      ')\g
INSERT INTO Diagnose VALUES ('47','      ARTHRITIS, OTHER JOINT DISORDER      ')\g
INSERT INTO Diagnose VALUES ('471','     RHEUMATOID ARTHRITIS      ')\g
INSERT INTO Diagnose VALUES ('4711','    Typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('4712','    Unusual manifestation      ')\g
INSERT INTO Diagnose VALUES ('4713','    Juvenile      ')\g
INSERT INTO Diagnose VALUES ('4719','    Other      ')\g
INSERT INTO Diagnose VALUES ('472','     PSORIATIC ARTHRITIS      ')\g
INSERT INTO Diagnose VALUES ('4721','    Typical psoriatic arthritis      ')\g
INSERT INTO Diagnose VALUES ('4722','    Psoriasis with features of psoriatic and rheumatoid arthritis      ')\g
INSERT INTO Diagnose VALUES ('4723','    Psoriasis with changes of classical rheumatoid arthritis      ')\g
INSERT INTO Diagnose VALUES ('4729','    Other      ')\g
INSERT INTO Diagnose VALUES ('473','     REITER SYNDROME      ')\g
INSERT INTO Diagnose VALUES ('474','     ANKYLOSING SPONDYLITIS      ')\g
INSERT INTO Diagnose VALUES ('475','     GOUT      ')\g
INSERT INTO Diagnose VALUES ('476','     CHONDROCALCINOSIS      ')\g
INSERT INTO Diagnose VALUES ('4761','    Pseudogout      ')\g
INSERT INTO Diagnose VALUES ('4762','    Secondary to other disease (also code cause)   exclude: alkaptonuria (ochronosis) (.595)      ')\g
INSERT INTO Diagnose VALUES ('477','     DEGENERATIVE JOINT DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4771','    Typical      ')\g
INSERT INTO Diagnose VALUES ('4772','    Erosive osteoarthritis (acute)      ')\g
INSERT INTO Diagnose VALUES ('4773','    Unusual manifestation      ')\g
INSERT INTO Diagnose VALUES ('4777','    Diffuse idiopathic skeletal hyperostosis (DISH)      ')\g
INSERT INTO Diagnose VALUES ('4779','    Other    include: post-traumatic degenerative joint disorder   exclude: gout (.75), hemophilia (.656), nontraumatic hydrarthrosis (.254), loose body (.48)      ')\g
INSERT INTO Diagnose VALUES ('478','     MISCELLANEOUS JOINT DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4781','    Ankylosis (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('4782','    Osteochondromatosis      ')\g
INSERT INTO Diagnose VALUES ('4783','    Synovial herniation    include: Baker cyst      ')\g
INSERT INTO Diagnose VALUES ('4784','    Pigmented villonodular synovitis (giant cell tumor of tendon sheath)      ')\g
INSERT INTO Diagnose VALUES ('4785','    Synovitis, nonspecific      ')\g
INSERT INTO Diagnose VALUES ('4786','    Protrusio acetabuli (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('4787','    Relapsing polychondritis      ')\g
INSERT INTO Diagnose VALUES ('4789','    Other    include: contracture, Wilson disease (see .5729, 1.592)    exclude: traumatic hydrarthrosis or hemarthrosis (.492)      ')\g
INSERT INTO Diagnose VALUES ('479','     MISCELLANEOUS    include: Jacoud arthritis, arthritis with Crohn disease   exclude: hemochromatosis (.594), alkaptonuria  (ochronosis) (.595)      ')\g
INSERT INTO Diagnose VALUES ('48','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('481','     CALCIFICATION, OSSIFICATION      ')\g
INSERT INTO Diagnose VALUES ('4811','    Calcified hematoma, fibrodysplasia (myositis) ossificans    exclude: fibrodysplasia ossificans progressiva (.1611)      ')\g
INSERT INTO Diagnose VALUES ('4812','    Calcified blood vessel, phlebolith      ')\g
INSERT INTO Diagnose VALUES ('4813','    Interstitial calcification of metabolic origin     include: calcinosis, fat necrosis, milk-alkali syndrome      ')\g
INSERT INTO Diagnose VALUES ('4814','    Calcified articular cartilage    exclude: alkaptonuria (ochronosis) (.595), chondrocalcinosis (.76), pseudogout (.761)      ')\g
INSERT INTO Diagnose VALUES ('4815','    Calcification or ossification secondary to thermal injury, infection, venous stasis      ')\g
INSERT INTO Diagnose VALUES ('4816','    Calcified lymph node      ')\g
INSERT INTO Diagnose VALUES ('4817','    Calcification in neoplasm      ')\g
INSERT INTO Diagnose VALUES ('4818','    Tumoral calcinosis      ')\g
INSERT INTO Diagnose VALUES ('4821','    Neuropathic skeletal disorder    include: neurosyphilis, syringomyelia, leprosy, diabetes, congenital indifference to pain      ')\g
INSERT INTO Diagnose VALUES ('4822','    Neurotrophic skeletal disorder    include: thermal injury, diabetes, vascular disease, Werner syndrome (see .815)    exclude: dermatomyositis (.614), scleroderma (.613)      ')\g
INSERT INTO Diagnose VALUES ('4829','    Other change secondary to neurological disorder   include: cerebral palsy, birth injury, mental retardation, postparalytic change    exclude: muscular dystrophy (.831), arthrogryposis (.1893)      ')\g
INSERT INTO Diagnose VALUES ('483','     MUSCLE DISORDER, SOFT TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4831','    Muscular dystrophy      ')\g
INSERT INTO Diagnose VALUES ('4832','    Myopathy      ')\g
INSERT INTO Diagnose VALUES ('4833','    Edema, soft tissue swelling    include: anasarca, hydrops (soft tissue), lymphedema   exclude: inflammatory (.24), traumatic (.491)      ')\g
INSERT INTO Diagnose VALUES ('4837','    Obesity      ')\g
INSERT INTO Diagnose VALUES ('4838','    Emaciation      ')\g
INSERT INTO Diagnose VALUES ('4839','    Other    include: elephantiasis   exclude: arthrogryposis (.1893)      ')\g
INSERT INTO Diagnose VALUES ('484','     PAGET DISEASE      ')\g
INSERT INTO Diagnose VALUES ('4841','    Destructive phase      ')\g
INSERT INTO Diagnose VALUES ('4842','    Mixed phase      ')\g
INSERT INTO Diagnose VALUES ('4843','    Bone forming phase      ')\g
INSERT INTO Diagnose VALUES ('4849','    Other    exclude: pathological fracture (.416), other  complication (code specifically)      ')\g
INSERT INTO Diagnose VALUES ('485','     FIBROUS DYSPLASIA      ')\g
INSERT INTO Diagnose VALUES ('4851','    Monostotic      ')\g
INSERT INTO Diagnose VALUES ('4852','    Polyostotic    include: McCune-Albright syndrome      ')\g
INSERT INTO Diagnose VALUES ('4859','    Other    exclude: cherubism (243.1545)      ')\g
INSERT INTO Diagnose VALUES ('486','     MISCELLANEOUS SKELETAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('4861','    Hypertrophic osteoarthropathy      ')\g
INSERT INTO Diagnose VALUES ('4862','    Osteitis, periostitis or hyperostosis of unknown etiology      ')\g
INSERT INTO Diagnose VALUES ('4863','    Osteitis condensans ilii      ')\g
INSERT INTO Diagnose VALUES ('4869','    Other   include: ainhum   exclude: periosteal reaction associated with vascular insufficiency (.29)      ')\g
INSERT INTO Diagnose VALUES ('487','     RIB NOTCHING, EROSION      ')\g
INSERT INTO Diagnose VALUES ('4871','    Rib notching (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('4872','    Erosion of superior aspect of rib (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('4879','    Other      ')\g
INSERT INTO Diagnose VALUES ('489','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('49','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('491','     FUNDAMENTAL OBSERVATION (e.g., displaced fat pad)      ')\g
INSERT INTO Diagnose VALUES ('492','     ANATOMICAL DETAIL   include: normal bone growth and maturation      ')\g
INSERT INTO Diagnose VALUES ('493','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('494','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('499','     OTHER    include: phenomenon related to technique of examination    exclude: vacuum joint" (.137)"      ')\g
INSERT INTO Diagnose VALUES ('51','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('511','     NORMAL ROUTINE PLAIN FILM    exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('512','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION      ')\g
INSERT INTO Diagnose VALUES ('5121','    Digital radiographic techniques, tomography, MR, nuclear medicine      ')\g
INSERT INTO Diagnose VALUES ('51211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('512111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('512112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('512113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('512114','  Delayed scanning following enhancement (e.g., for detection of liver lesions)      ')\g
INSERT INTO Diagnose VALUES ('512115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('512116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('512117','  Three-dimensional reconstruc-tion      ')\g
INSERT INTO Diagnose VALUES ('512118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('512119','  Other, including serial enhancement studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('51214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('512141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('5121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('5121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('5121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('5121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('5121415',' Specific resonance suppressed    include: fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('5121416',' High-speed imaging    include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('5121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('5121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('512142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('512143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('512144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('512145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('512146','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('512147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('512149','  Other      ')\g
INSERT INTO Diagnose VALUES ('51215','   Digital radiography    exclude: angiography (.124-3)      ')\g
INSERT INTO Diagnose VALUES ('51216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('512161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('512162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('512163','  PET      ')\g
INSERT INTO Diagnose VALUES ('512164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('512165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('512166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('512167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('512168','  Therapeutic procedure, include use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('512169','  Other. See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('51217','   Nuclear medicine study-cardiac      ')\g
INSERT INTO Diagnose VALUES ('512171','  Myocardial perfusion imaging      ')\g
INSERT INTO Diagnose VALUES ('512172','  Myocardial infarct imaging (e.g., 99mTc-pyrophosphate)      ')\g
INSERT INTO Diagnose VALUES ('512173','  Metabolism study      ')\g
INSERT INTO Diagnose VALUES ('512174','  Gated cardiac blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('512175','  Radionuclide angiography (include first pass ejection fraction measurement)      ')\g
INSERT INTO Diagnose VALUES ('512176','  Cardiac shunt quantification      ')\g
INSERT INTO Diagnose VALUES ('512179','  Other      ')\g
INSERT INTO Diagnose VALUES ('51218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('51219','   Other      ')\g
INSERT INTO Diagnose VALUES ('5122','    Cardiac catheterization    exclude: complication (.44)      ')\g
INSERT INTO Diagnose VALUES ('5124','    Angiography      ')\g
INSERT INTO Diagnose VALUES ('51241','   Venous angiocardiography      ')\g
INSERT INTO Diagnose VALUES ('51242','   Selective angiocardiography      ')\g
INSERT INTO Diagnose VALUES ('51243','   Aortography      ')\g
INSERT INTO Diagnose VALUES ('51244','   Coronary arteriography      ')\g
INSERT INTO Diagnose VALUES ('51245','   Thoracic venography      ')\g
INSERT INTO Diagnose VALUES ('51249','   Other    include: azygography, CO2  angiography, transthoracic angiography, intraosseous venography      ')\g
INSERT INTO Diagnose VALUES ('5125','    Special nonroutine projection      ')\g
INSERT INTO Diagnose VALUES ('5126','    Biopsy or interventional      ')\g
INSERT INTO Diagnose VALUES ('51261','   Biopsy    include: myocardial      ')\g
INSERT INTO Diagnose VALUES ('51262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('51263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('51264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('51265','   Thrombolytic infusion      ')\g
INSERT INTO Diagnose VALUES ('51266','   Balloon angioplasty    include: laser assisted coronary angioplasty      ')\g
INSERT INTO Diagnose VALUES ('51267','   Use of atherectomy device      ')\g
INSERT INTO Diagnose VALUES ('51268','   Balloon valvuloplasty, balloon atrial septostomy, catheter closure of patent ductus arteriosus or atrial septal defect      ')\g
INSERT INTO Diagnose VALUES ('51269','   Other    include: catheter ablation of accessory electrical pathways      ')\g
INSERT INTO Diagnose VALUES ('5127','    Fluoroscopy    include: cineradiography      ')\g
INSERT INTO Diagnose VALUES ('5128','    Insertion of pacing electrode    exclude: complication (.43)      ')\g
INSERT INTO Diagnose VALUES ('5129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('51292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('51297','   Kymography      ')\g
INSERT INTO Diagnose VALUES ('51298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('512981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('512982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('512983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('512984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('512985','  Guided procedure for diagnosis    include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('512986','  Guided procedure for therapy    include: drainage      ')\g
INSERT INTO Diagnose VALUES ('512987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('512988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('512989','  Special nonroutine projection, mode, or transducer type   include: subxyphoid and suprasternal projection      ')\g
INSERT INTO Diagnose VALUES ('51299','   Other    include: xeroradiography, thermography, biomagnetism study      ')\g
INSERT INTO Diagnose VALUES ('513','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('5131','    Coronary artery      ')\g
INSERT INTO Diagnose VALUES ('51311','   Dominant left coronary artery      ')\g
INSERT INTO Diagnose VALUES ('51312','   Dominant right coronary artery      ')\g
INSERT INTO Diagnose VALUES ('51313','   Balanced coronary system      ')\g
INSERT INTO Diagnose VALUES ('51319','   Other      ')\g
INSERT INTO Diagnose VALUES ('5132','    Arterial, other      ')\g
INSERT INTO Diagnose VALUES ('51321','   Brachiocephalic artery (common origin of innominate and left common carotid arteries)      ')\g
INSERT INTO Diagnose VALUES ('51322','   Origin of left vertebral artery from aorta      ')\g
INSERT INTO Diagnose VALUES ('51329','   Other      ')\g
INSERT INTO Diagnose VALUES ('5133','    Pericardial fat pad      ')\g
INSERT INTO Diagnose VALUES ('51331','   Left      ')\g
INSERT INTO Diagnose VALUES ('51332','   Right      ')\g
INSERT INTO Diagnose VALUES ('51333','   Bilateral      ')\g
INSERT INTO Diagnose VALUES ('5139','    Other   exclude: myocardial sinusoids (.1937)      ')\g
INSERT INTO Diagnose VALUES ('514','     SEPTAL ANOMALY, PATENT DUCTUS (see also .776) (for 5th number see box following 149)      ')\g
INSERT INTO Diagnose VALUES ('5141','    Atrial septal defect    exclude: ostium primum (.1431)      ')\g
INSERT INTO Diagnose VALUES ('51411','   Patent foramen ovale      ')\g
INSERT INTO Diagnose VALUES ('51412','   Ostium secundum      ')\g
INSERT INTO Diagnose VALUES ('51413','   Sinus venosus      ')\g
INSERT INTO Diagnose VALUES ('51414','   Lutembacher syndrome (with congenital or acquired mitral stenosis)      ')\g
INSERT INTO Diagnose VALUES ('5142','    Ventricular septal defect or related disorder   exclude: acquired (.776, .41)      ')\g
INSERT INTO Diagnose VALUES ('51421','   Membranous      ')\g
INSERT INTO Diagnose VALUES ('51422','   Supracristal      ')\g
INSERT INTO Diagnose VALUES ('51423','   Muscular      ')\g
INSERT INTO Diagnose VALUES ('51424','   Left ventricle to right atrium shunt (all types)      ')\g
INSERT INTO Diagnose VALUES ('51425','   Ventricular septal aneurysm      ')\g
INSERT INTO Diagnose VALUES ('51426','   Spontaneous closure of ventricular septal defect      ')\g
INSERT INTO Diagnose VALUES ('51429','   Other      ')\g
INSERT INTO Diagnose VALUES ('5143','    Endocardial cushion defect      ')\g
INSERT INTO Diagnose VALUES ('51431','   Ostium primum with cleft mitral valve (partial atrioventricular canal)      ')\g
INSERT INTO Diagnose VALUES ('51432','   Common atrium with common atrioventricular valve      ')\g
INSERT INTO Diagnose VALUES ('51433','   Complete atrioventricular canal      ')\g
INSERT INTO Diagnose VALUES ('51434','   Intermediate atrioventricular canal      ')\g
INSERT INTO Diagnose VALUES ('51439','   Other      ')\g
INSERT INTO Diagnose VALUES ('5144','    Single ventricle (common ventricle; univentricular heart)      ')\g
INSERT INTO Diagnose VALUES ('51441','   Left ventricular type      ')\g
INSERT INTO Diagnose VALUES ('51446','   Malposition of great arteries without pulmonic stenosis      ')\g
INSERT INTO Diagnose VALUES ('514411','  With left-sided (l-loop) subaortic outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514412','  With other left-sided outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514413','  With right-sided (d-loop) subaortic outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514414','  With other right-sided outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514415','  With pulmonary outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514416','  Without pulmonary outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514417','  With aortic outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514418','  Without aortic outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514419','  Other    include: trabecular pouch; no outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('51442','   Right ventricular type      ')\g
INSERT INTO Diagnose VALUES ('51447','   Malposition of great arteries with pulmonic stenosis      ')\g
INSERT INTO Diagnose VALUES ('514421','  With left-sided (l-loop) subaortic outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514422','  With other left-sided outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514423','  With right sided (d-loop) subaortic outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514424','  With other right-sided outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514425','  With pulmonary outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514426','  Without pulmonary outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514427','  With aortic outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514428','  Without aortic outflow obstruction      ')\g
INSERT INTO Diagnose VALUES ('514429','  Other    include: trabecular pouch; no outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('51443','   Indeterminate ventricular type      ')\g
INSERT INTO Diagnose VALUES ('51448','   Normally related great arteries without pulmonic stenosis      ')\g
INSERT INTO Diagnose VALUES ('514431','  With outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('514432','  Without outlet chamber      ')\g
INSERT INTO Diagnose VALUES ('51444','   Normally related great arteries with pulmonic stenosis      ')\g
INSERT INTO Diagnose VALUES ('51445','   Double inlet left ventricle    include: straddling atrioventricular valves      ')\g
INSERT INTO Diagnose VALUES ('51449','   Other    include: double inlet right ventricle, absent ventricular septum      ')\g
INSERT INTO Diagnose VALUES ('5145','    Tetralogy of Fallot with      ')\g
INSERT INTO Diagnose VALUES ('51451','   Infundibular and valvular stenosis      ')\g
INSERT INTO Diagnose VALUES ('51452','   Infundibular stenosis      ')\g
INSERT INTO Diagnose VALUES ('51453','   Pulmonary atresia (pseudotruncus) (see also .1642)      ')\g
INSERT INTO Diagnose VALUES ('51459','   Other    include: with absent pulmonary valve   exclude: with anomalous muscle bundle (.1733)      ')\g
INSERT INTO Diagnose VALUES ('5146','    Patent ductus arteriosus      ')\g
INSERT INTO Diagnose VALUES ('51461','   Left ductus      ')\g
INSERT INTO Diagnose VALUES ('51462','   Right ductus      ')\g
INSERT INTO Diagnose VALUES ('51463','   Bilateral      ')\g
INSERT INTO Diagnose VALUES ('51464','   In premature infant      ')\g
INSERT INTO Diagnose VALUES ('51465','   Ductus sling      ')\g
INSERT INTO Diagnose VALUES ('51469','   Other      ')\g
INSERT INTO Diagnose VALUES ('5149','    Other   exclude: persistent fetal circulation (.789)      ')\g
INSERT INTO Diagnose VALUES ('515','     GREAT VESSEL ANOMALY (for 5th number see box following .1599)   exclude: transposition of great arteries (.144, .16)      ')\g
INSERT INTO Diagnose VALUES ('5151','    Aortic obstructive lesion      ')\g
INSERT INTO Diagnose VALUES ('51511','   Coarctation of aorta      ')\g
INSERT INTO Diagnose VALUES ('51512','   Tubular hypoplasia of aorta      ')\g
INSERT INTO Diagnose VALUES ('51513','   Pseudocoarctation      ')\g
INSERT INTO Diagnose VALUES ('51514','   Multiple coarctations, unusual site of      ')\g
INSERT INTO Diagnose VALUES ('51515','   Interruption of aortic arch, atresia      ')\g
INSERT INTO Diagnose VALUES ('51519','   Other   exclude: supravalvular aortic stenosis (.174)      ')\g
INSERT INTO Diagnose VALUES ('5152','    Left aortic arch with anomalous brachiocephalic vessel      ')\g
INSERT INTO Diagnose VALUES ('51521','   Aberrant right subclavian artery      ')\g
INSERT INTO Diagnose VALUES ('51522','   Isolation of right subclavian artery      ')\g
INSERT INTO Diagnose VALUES ('51523','   Cervical arch      ')\g
INSERT INTO Diagnose VALUES ('51528','   More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('51529','   Other    exclude: normal variations (.132)      ')\g
INSERT INTO Diagnose VALUES ('5153','    Right aortic arch (see box following .1599)   exclude: with symptomatic vascular ring (.1542)      ')\g
INSERT INTO Diagnose VALUES ('51531','   Mirror-image branching      ')\g
INSERT INTO Diagnose VALUES ('51532','   Aberrant left subclavian artery      ')\g
INSERT INTO Diagnose VALUES ('51533','   Isolation of left subclavian artery      ')\g
INSERT INTO Diagnose VALUES ('51534','   Cervical arch      ')\g
INSERT INTO Diagnose VALUES ('51539','   Other      ')\g
INSERT INTO Diagnose VALUES ('5154','    Vascular ring, related anomaly      ')\g
INSERT INTO Diagnose VALUES ('51541','   Double aortic arch      ')\g
INSERT INTO Diagnose VALUES ('51542','   Right aortic arch with left ductus      ')\g
INSERT INTO Diagnose VALUES ('51543','   Origin of left pulmonary artery from right pulmonary artery (pulmonary sling) (see 6.7523)      ')\g
INSERT INTO Diagnose VALUES ('51549','   Other   include: tracheal compression by carotid or innominate artery   exclude: ductus arteriosis sling (.1465)      ')\g
INSERT INTO Diagnose VALUES ('5155','    Pulmonary artery anomaly    exclude: pulmonary sling (.1543)      ')\g
INSERT INTO Diagnose VALUES ('51551','   Idiopathic dilatation      ')\g
INSERT INTO Diagnose VALUES ('51552','   Peripheral pulmonary artery stenosis (single or multiple)      ')\g
INSERT INTO Diagnose VALUES ('51553','   Aortic origin of right or left pulmonary artery   exclude: truncus arteriosus (.164)      ')\g
INSERT INTO Diagnose VALUES ('51554','   Unilateral hypoplasia or absence of pulmonary artery      ')\g
INSERT INTO Diagnose VALUES ('51555','   Absence of proximal pulmonary arteries   exclude: truncus (.164), pseudotruncus (.1453)      ')\g
INSERT INTO Diagnose VALUES ('51556','   Bronchial or other systemic supply to lungs (see 6.14, 943.15)      ')\g
INSERT INTO Diagnose VALUES ('51559','   Other    include: pulmonary arteriovenous fistula (see 6.1494)    exclude: pulmonary artery aneurysm (.73)      ')\g
INSERT INTO Diagnose VALUES ('5156','    Pulmonary venous anomaly with normal" drainage"      ')\g
INSERT INTO Diagnose VALUES ('51561','   Cor triatriatum      ')\g
INSERT INTO Diagnose VALUES ('51562','   Stenosis of pulmonary veins      ')\g
INSERT INTO Diagnose VALUES ('51569','   Other      ')\g
INSERT INTO Diagnose VALUES ('5157','    Total anomalous pulmonary venous connection      ')\g
INSERT INTO Diagnose VALUES ('51571','   Supracardiac      ')\g
INSERT INTO Diagnose VALUES ('51572','   Cardiac      ')\g
INSERT INTO Diagnose VALUES ('51573','   Infracardiac    include: atresia of common pulmonary vein      ')\g
INSERT INTO Diagnose VALUES ('51578','   More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('5158','    Partial anomalous pulmonary venous connection      ')\g
INSERT INTO Diagnose VALUES ('51581','   Supracardiac      ')\g
INSERT INTO Diagnose VALUES ('51582','   Cardiac      ')\g
INSERT INTO Diagnose VALUES ('51583','   Infracardiac    include: scimitar syndrome (see 6.142)      ')\g
INSERT INTO Diagnose VALUES ('51588','   More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('5159','    Anomaly of thoracic systemic vein      ')\g
INSERT INTO Diagnose VALUES ('51591','   Persistent left superior vena cava (left anterior cardinal vein) connected to coronary sinus   exclude: associated with anomalous pulmonary venous connection (.157, .158)      ')\g
INSERT INTO Diagnose VALUES ('51592','   Persistent left superior vena cava connected to left atrium with absent coronary sinus (see also .1864)      ')\g
INSERT INTO Diagnose VALUES ('51593','   Inferior vena cava connected to left atrium      ')\g
INSERT INTO Diagnose VALUES ('51594','   Azygos continuation of inferior vena cava (atresia or absence of hepatic portion of inferior vena cava)      ')\g
INSERT INTO Diagnose VALUES ('51599','   Other    include: hemiazygos continuation of inferior vena cava, bilateral inferior vena cava      ')\g
INSERT INTO Diagnose VALUES ('516','     TRANSPOSITION OF GREAT ARTERIES, RELATED ANOMALY, SITUS ANOMALY (for 5th number see box following .1599 or following .179)   exclude: with single ventricle (.144), valvular atresia (.1713, .1714)      ')\g
INSERT INTO Diagnose VALUES ('5161','    Complete transposition (situs solitus d-loop and d-transposition) (see also .168)      ')\g
INSERT INTO Diagnose VALUES ('51611','   Isolated (atrial septal defect only)      ')\g
INSERT INTO Diagnose VALUES ('51612','   With pulmonary stenosis      ')\g
INSERT INTO Diagnose VALUES ('51613','   With ventricular septal defect      ')\g
INSERT INTO Diagnose VALUES ('51614','   With pulmonary stenosis and ventricular septal defect      ')\g
INSERT INTO Diagnose VALUES ('51615','   With atrioventricular valve anomaly      ')\g
INSERT INTO Diagnose VALUES ('51619','   Other   exclude: with single ventricle (5.1441, 5.1442)      ')\g
INSERT INTO Diagnose VALUES ('5162','    Double outlet right ventricle (see also .168)      ')\g
INSERT INTO Diagnose VALUES ('51621','   Subaortic ventricular septal defect without pulmonary stenosis      ')\g
INSERT INTO Diagnose VALUES ('51622','   Subaortic ventricular septal defect with pulmonary stenosis (see also .145)      ')\g
INSERT INTO Diagnose VALUES ('51623','   Subpulmonic ventricular septal defect (Taussig-Bing anomaly)      ')\g
INSERT INTO Diagnose VALUES ('51629','   Other      ')\g
INSERT INTO Diagnose VALUES ('5163','    Corrected transposition (l-loop and l-transposition, inversion of ventricles) (see also .168)      ')\g
INSERT INTO Diagnose VALUES ('51631','   Isolated      ')\g
INSERT INTO Diagnose VALUES ('51632','   With ventricular septal defect      ')\g
INSERT INTO Diagnose VALUES ('51633','   With pulmonary stenosis      ')\g
INSERT INTO Diagnose VALUES ('51634','   With pulmonary atresia      ')\g
INSERT INTO Diagnose VALUES ('51635','   With ventricular septal defect and pulmonary stenosis or atresia      ')\g
INSERT INTO Diagnose VALUES ('51636','   With tricuspid valve incompetence    include: due to Ebstein anomaly (see also .1751)      ')\g
INSERT INTO Diagnose VALUES ('51639','   Other      ')\g
INSERT INTO Diagnose VALUES ('5164','    Truncus arteriosus      ')\g
INSERT INTO Diagnose VALUES ('51641','   Type I, II, and III (pulmonary artery arises from ascending aorta)      ')\g
INSERT INTO Diagnose VALUES ('51642','   Type IV    exclude: pseudotruncus (.1453)      ')\g
INSERT INTO Diagnose VALUES ('51643','   Aortic-pulmonic window      ')\g
INSERT INTO Diagnose VALUES ('51649','   Other      ')\g
INSERT INTO Diagnose VALUES ('5165','    Situs anomaly      ')\g
INSERT INTO Diagnose VALUES ('51651','   Situs solitus      ')\g
INSERT INTO Diagnose VALUES ('51652','   Situs inversus      ')\g
INSERT INTO Diagnose VALUES ('51653','   Situs ambiguous (visceral heterotaxy)    include: asplenia, polysplenia      ')\g
INSERT INTO Diagnose VALUES ('5166','    Dextrocardia      ')\g
INSERT INTO Diagnose VALUES ('51661','   Primary (use only with situs solitus or situs ambiguous)      ')\g
INSERT INTO Diagnose VALUES ('51662','   Secondary      ')\g
INSERT INTO Diagnose VALUES ('5167','    Levocardia (use only with situs inversus or ambiguous)      ')\g
INSERT INTO Diagnose VALUES ('51671','   Primary (use only with situs inversus)      ')\g
INSERT INTO Diagnose VALUES ('51672','   Secondary      ')\g
INSERT INTO Diagnose VALUES ('5168','    Anomaly of bulboventricular loop and transposition (see also .161, .162, .163, .169)      ')\g
INSERT INTO Diagnose VALUES ('51681','   d-loop and normally related great arteries      ')\g
INSERT INTO Diagnose VALUES ('51682','   d-loop and d-transposition      ')\g
INSERT INTO Diagnose VALUES ('51683','   l-loop and inversely related great arteries      ')\g
INSERT INTO Diagnose VALUES ('51684','   l-loop and l-transposition      ')\g
INSERT INTO Diagnose VALUES ('51685','   Indeterminate loop      ')\g
INSERT INTO Diagnose VALUES ('51689','   Other   include: discordant loop and transposition      ')\g
INSERT INTO Diagnose VALUES ('5169','    Other   include: other form of transposition (see also .168), double outlet left ventricle, malposition of the great arteries      ')\g
INSERT INTO Diagnose VALUES ('517','     VALVULAR ANOMALY (for 5th number see box following .179)   include: sinus of Valsalva anomaly (.177)      ')\g
INSERT INTO Diagnose VALUES ('5171','    Valvular atresia    exclude: pseudotruncus (.1453)      ')\g
INSERT INTO Diagnose VALUES ('51711','   Normally related great arteries with intact ventricular septum      ')\g
INSERT INTO Diagnose VALUES ('51712','   Normally related great arteries with ventricular septal defect      ')\g
INSERT INTO Diagnose VALUES ('51713','   Transposition of great arteries with intact ventricular septum      ')\g
INSERT INTO Diagnose VALUES ('51714','   Transposition of great arteries with ventricular septal defect      ')\g
INSERT INTO Diagnose VALUES ('51715','   Hypoplastic left heart syndrome      ')\g
INSERT INTO Diagnose VALUES ('51716','   Hypoplastic right heart syndrome      ')\g
INSERT INTO Diagnose VALUES ('51719','   Other      ')\g
INSERT INTO Diagnose VALUES ('5172','    Valvular stenosis, deformity      ')\g
INSERT INTO Diagnose VALUES ('51721','   Bicuspid or other leaflet deformity      ')\g
INSERT INTO Diagnose VALUES ('51722','   Hypoplastic annulus      ')\g
INSERT INTO Diagnose VALUES ('51729','   Other      ')\g
INSERT INTO Diagnose VALUES ('5173','    Subvalvular stenosis, deformity      ')\g
INSERT INTO Diagnose VALUES ('51731','   Infundibular      ')\g
INSERT INTO Diagnose VALUES ('51732','   Idiopathic hypertrophic subaortic stenosis (asymmetric septal hypertrophy)      ')\g
INSERT INTO Diagnose VALUES ('51733','   Accessory (anomalous) muscle bundle      ')\g
INSERT INTO Diagnose VALUES ('51734','   Discrete    include: membrane, ring      ')\g
INSERT INTO Diagnose VALUES ('51739','   Other    include: parachute mitral valve, cleft valve leaflet, prolapsed valve, extrinsic pressure      ')\g
INSERT INTO Diagnose VALUES ('5174','    Supravalvular stenosis, deformity      ')\g
INSERT INTO Diagnose VALUES ('51741','   Discrete      ')\g
INSERT INTO Diagnose VALUES ('51742','   Tubular hypoplasia      ')\g
INSERT INTO Diagnose VALUES ('51743','   Supravalvular stenosing ring of left atrium      ')\g
INSERT INTO Diagnose VALUES ('51749','   Other      ')\g
INSERT INTO Diagnose VALUES ('5175','    Valvular incompetence, prolapse      ')\g
INSERT INTO Diagnose VALUES ('51751','   Ebstein anomaly (see also .1636)      ')\g
INSERT INTO Diagnose VALUES ('51752','   Deficient valve tissue (absent, bicuspid, cleft or perforated leaflet)      ')\g
INSERT INTO Diagnose VALUES ('51753','   Prolapsed valve related to ventricular septal defect      ')\g
INSERT INTO Diagnose VALUES ('51754','   Prolapsed valve, other    include: midsystolic click syndrome, myxomatous degeneration of leaflet      ')\g
INSERT INTO Diagnose VALUES ('51755','   Anomalous chordae tendineae, papillary muscle      ')\g
INSERT INTO Diagnose VALUES ('51756','   Dilated annulus      ')\g
INSERT INTO Diagnose VALUES ('51759','   Other      ')\g
INSERT INTO Diagnose VALUES ('5176','    Aortico left ventricular tunnel      ')\g
INSERT INTO Diagnose VALUES ('5177','    Sinus of Valsalva anomaly      ')\g
INSERT INTO Diagnose VALUES ('51771','   Aneurysm of sinus of Valsalva, noncommunicating      ')\g
INSERT INTO Diagnose VALUES ('51772','   Aneurysm of sinus of Valsalva, communicating      ')\g
INSERT INTO Diagnose VALUES ('51779','   Other      ')\g
INSERT INTO Diagnose VALUES ('5179','    Other      ')\g
INSERT INTO Diagnose VALUES ('518','     CORONARY VESSEL ANOMALY (for 5th number see box above following .179 or following .199)      ')\g
INSERT INTO Diagnose VALUES ('5181','    Anomalous origin, minor    exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('51811','   Anterior descending from right coronary artery      ')\g
INSERT INTO Diagnose VALUES ('51812','   Circumflex origin from right coronary artery      ')\g
INSERT INTO Diagnose VALUES ('51819','   Other      ')\g
INSERT INTO Diagnose VALUES ('5182','    Anomalous origin, major      ')\g
INSERT INTO Diagnose VALUES ('51821','   Left coronary artery from pulmonary artery      ')\g
INSERT INTO Diagnose VALUES ('51822','   Right coronary artery from pulmonary artery      ')\g
INSERT INTO Diagnose VALUES ('51823','   Both coronary arteries from pulmonary artery      ')\g
INSERT INTO Diagnose VALUES ('51829','   Other      ')\g
INSERT INTO Diagnose VALUES ('5183','    Single coronary artery      ')\g
INSERT INTO Diagnose VALUES ('51831','   Right      ')\g
INSERT INTO Diagnose VALUES ('51832','   Left      ')\g
INSERT INTO Diagnose VALUES ('5184','    Coronary artery fistula communicating with      ')\g
INSERT INTO Diagnose VALUES ('51841','   Right atrium      ')\g
INSERT INTO Diagnose VALUES ('51842','   Right ventricle      ')\g
INSERT INTO Diagnose VALUES ('51843','   Pulmonary artery      ')\g
INSERT INTO Diagnose VALUES ('51849','   Other      ')\g
INSERT INTO Diagnose VALUES ('5185','    Systemic vessel supplying myocardium      ')\g
INSERT INTO Diagnose VALUES ('5186','    Coronary sinus anomaly      ')\g
INSERT INTO Diagnose VALUES ('51861','   Atresia of ostium in right atrium (no connection to left superior vena cava)      ')\g
INSERT INTO Diagnose VALUES ('51862','   Atresia of ostium in right atrium (with connection to left superior vena cava)      ')\g
INSERT INTO Diagnose VALUES ('51863','   Stenosis of coronary sinus ostium      ')\g
INSERT INTO Diagnose VALUES ('51864','   Absence of coronary sinus (see also .1592)      ')\g
INSERT INTO Diagnose VALUES ('51869','   Other      ')\g
INSERT INTO Diagnose VALUES ('5189','    Other    exclude: anomalous pulmonary venous connection to coronary vein (.1572, .1582), persistent fetal circulation (.789)      ')\g
INSERT INTO Diagnose VALUES ('519','     MISCELLANEOUS (for 5th number see box following .199)   exclude: chromosomal disorders (.195)      ')\g
INSERT INTO Diagnose VALUES ('5191','    Congenital heart disease, type undetermined      ')\g
INSERT INTO Diagnose VALUES ('51911','   With increased pulmonary blood flow      ')\g
INSERT INTO Diagnose VALUES ('51912','   With decreased pulmonary blood flow      ')\g
INSERT INTO Diagnose VALUES ('51913','   With normal pulmonary blood flow      ')\g
INSERT INTO Diagnose VALUES ('5192','    Chamber anomaly      ')\g
INSERT INTO Diagnose VALUES ('51921','   Juxtaposition of atrial appendages      ')\g
INSERT INTO Diagnose VALUES ('51922','   Idiopathic dilatation of atrial appendage      ')\g
INSERT INTO Diagnose VALUES ('51923','   Idiopathic dilatation of atrium      ')\g
INSERT INTO Diagnose VALUES ('51929','   Other    include: bifid apex, ectopia cordis   exclude: hypoplastic left heart syndrome (.1715), hypoplastic right heart syndrome (.1716)      ')\g
INSERT INTO Diagnose VALUES ('5193','    Myocardial or endocardial anomaly      ')\g
INSERT INTO Diagnose VALUES ('51931','   Endocardial fibroelastosis      ')\g
INSERT INTO Diagnose VALUES ('51932','   Congenital myocarditis      ')\g
INSERT INTO Diagnose VALUES ('51933','   Storage disorder   include: glycogen, mucopolysaccharidosis, with carnitine deficiency      ')\g
INSERT INTO Diagnose VALUES ('51934','   Cardiomyopathy in infant of diabetic mother      ')\g
INSERT INTO Diagnose VALUES ('51935','   Other cardiomyopathy    exclude: idiopathic hypertrophic subaortic stenosis (.1732), myxedema (.52), amyloidosis (.68)      ')\g
INSERT INTO Diagnose VALUES ('51936','   Uhl anomaly      ')\g
INSERT INTO Diagnose VALUES ('51937','   Myocardial sinusoids      ')\g
INSERT INTO Diagnose VALUES ('51938','   Heart block and other conduction disturbance (congenital)    exclude: acquired heart block (see box following .449)      ')\g
INSERT INTO Diagnose VALUES ('51939','   Other    include: transient myocardial ischemia or infarction of the newborn   exclude: mucopolysaccharidosis (4.181)      ')\g
INSERT INTO Diagnose VALUES ('5194','    Pericardial anomaly      ')\g
INSERT INTO Diagnose VALUES ('51941','   Parietal pericardial deficiency (partial or complete)      ')\g
INSERT INTO Diagnose VALUES ('51942','   Cyst, diverticulum      ')\g
INSERT INTO Diagnose VALUES ('51949','   Other      ')\g
INSERT INTO Diagnose VALUES ('5195','    Chromosomal disorder      ')\g
INSERT INTO Diagnose VALUES ('51951','   Trisomy 21 (Down syndrome)      ')\g
INSERT INTO Diagnose VALUES ('51952','   Trisomy 18      ')\g
INSERT INTO Diagnose VALUES ('51953','   Trisomy 13      ')\g
INSERT INTO Diagnose VALUES ('51954','   Turner syndrome      ')\g
INSERT INTO Diagnose VALUES ('51959','   Other      ')\g
INSERT INTO Diagnose VALUES ('5196','    Syndrome with associated cardiac malformation      ')\g
INSERT INTO Diagnose VALUES ('51961','   Cardiomelic syndrome (Holt-Oram) (see also 4.1641)      ')\g
INSERT INTO Diagnose VALUES ('51962','   Chondroectodermal dysplasia (Ellis van Creveld) (see also 4.1523)      ')\g
INSERT INTO Diagnose VALUES ('51963','   Idiopathic hypercalcemia of infancy (see also 4.599)      ')\g
INSERT INTO Diagnose VALUES ('51964','   Noonan syndrome (see also 4.1642)      ')\g
INSERT INTO Diagnose VALUES ('51965','   Friedreich ataxia      ')\g
INSERT INTO Diagnose VALUES ('51966','   DiGeorge syndrome (see 6.2514)      ')\g
INSERT INTO Diagnose VALUES ('51969','   Other    exclude: rubella syndrome (.27)      ')\g
INSERT INTO Diagnose VALUES ('5197','    Connective tissue disorder, congenital (see also valvular incompetence .842)      ')\g
INSERT INTO Diagnose VALUES ('51971','   Marfan syndrome (see also 4.1711)      ')\g
INSERT INTO Diagnose VALUES ('51972','   Ehlers-Danlos syndrome (see also 4.1891)      ')\g
INSERT INTO Diagnose VALUES ('51973','   Homocystinuria (see also 4.597)      ')\g
INSERT INTO Diagnose VALUES ('51974','   Relapsing polychondritis (see also 4.787)      ')\g
INSERT INTO Diagnose VALUES ('51975','   Pseudoxanthoma elasticum      ')\g
INSERT INTO Diagnose VALUES ('51979','   Other   exclude: acquired connective tissue disorder (.61)      ')\g
INSERT INTO Diagnose VALUES ('5199','    Other      ')\g
INSERT INTO Diagnose VALUES ('52','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('520','     INFECTION CLASSIFIED BY ORGANISM (see 6.20 for detailed classification)      ')\g
INSERT INTO Diagnose VALUES ('5201','    Gram-positive facultative bacterium      ')\g
INSERT INTO Diagnose VALUES ('5202','    Gram-negative bacterium      ')\g
INSERT INTO Diagnose VALUES ('5203','    Mycobacterium (see 6.20 for detailed classification)   exclude: tuberculosis (.23)      ')\g
INSERT INTO Diagnose VALUES ('5204','    Anaerobic bacterium      ')\g
INSERT INTO Diagnose VALUES ('5205','    Fungus      ')\g
INSERT INTO Diagnose VALUES ('5206','    Virus or mycoplasma   include: AIDS      ')\g
INSERT INTO Diagnose VALUES ('5207','    Protozoa or spirochete   include: Lyme disease      ')\g
INSERT INTO Diagnose VALUES ('52077','   Acquired syphilis      ')\g
INSERT INTO Diagnose VALUES ('5208','    Helminth      ')\g
INSERT INTO Diagnose VALUES ('5209','    Other      ')\g
INSERT INTO Diagnose VALUES ('5211','    Acute      ')\g
INSERT INTO Diagnose VALUES ('5212','    Subacute      ')\g
INSERT INTO Diagnose VALUES ('5213','    Chronic      ')\g
INSERT INTO Diagnose VALUES ('5214','    Secondary to surgical prosthesis, suture      ')\g
INSERT INTO Diagnose VALUES ('5219','    Other    exclude: abscess (.242), aneurysm (.733)      ')\g
INSERT INTO Diagnose VALUES ('522','     SARCOIDOSIS (for 5th number see box following .449)      ')\g
INSERT INTO Diagnose VALUES ('523','     TUBERCULOSIS (for 5th number see box following .449)      ')\g
INSERT INTO Diagnose VALUES ('527','     CHANGE SECONDARY TO TRANSPLACENTAL INFECTION (for 5th number see box following .449)   include: rubella,cytomegalovirus      ')\g
INSERT INTO Diagnose VALUES ('529','     OTHER (for 5th number see box following .449)      ')\g
INSERT INTO Diagnose VALUES ('53','      NEOPLASM, NEOPLASTIC-LIKE CONDITION (for 5th number see box following .449)      ')\g
INSERT INTO Diagnose VALUES ('531','     BENIGN NEOPLASM      ')\g
INSERT INTO Diagnose VALUES ('5311','    Myxoma (include all types)      ')\g
INSERT INTO Diagnose VALUES ('5312','    Lipoma      ')\g
INSERT INTO Diagnose VALUES ('5313','    Fibroma      ')\g
INSERT INTO Diagnose VALUES ('5314','    Vascular tumor      ')\g
INSERT INTO Diagnose VALUES ('53141','   Hemangioma      ')\g
INSERT INTO Diagnose VALUES ('53142','   Lymphangioma      ')\g
INSERT INTO Diagnose VALUES ('53149','   Other      ')\g
INSERT INTO Diagnose VALUES ('5315','    Nerve tissue origin      ')\g
INSERT INTO Diagnose VALUES ('5316','    Hamartoma, rhabdomyoma      ')\g
INSERT INTO Diagnose VALUES ('5319','    Other    include: mesothelioma, teratoma   exclude: congenital pericardial cyst (.1942)      ')\g
INSERT INTO Diagnose VALUES ('532','     MALIGNANT NEOPLASM-PRIMARY      ')\g
INSERT INTO Diagnose VALUES ('5321','    Rhabdomyosarcoma      ')\g
INSERT INTO Diagnose VALUES ('5324','    Angiosarcoma      ')\g
INSERT INTO Diagnose VALUES ('5329','    Other   include: neoplasm associated with AIDS      ')\g
INSERT INTO Diagnose VALUES ('533','     MALIGNANT NEOPLASM-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('534','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('536','     EXTRINSIC MASS AFFECTING CARDIOVASCULAR STRUCTURE      ')\g
INSERT INTO Diagnose VALUES ('539','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('54','      EFFECT OF TRAUMA (for 5th number see box following .449)      ')\g
INSERT INTO Diagnose VALUES ('5431','    Perforation      ')\g
INSERT INTO Diagnose VALUES ('5432','    Malposition      ')\g
INSERT INTO Diagnose VALUES ('5433','    Electrode wire break      ')\g
INSERT INTO Diagnose VALUES ('5434','    Battery failure      ')\g
INSERT INTO Diagnose VALUES ('5435','    Battery pack rotation      ')\g
INSERT INTO Diagnose VALUES ('5436','    Encasement of cardiac electrode      ')\g
INSERT INTO Diagnose VALUES ('5437','    Venous thrombosis, with or without infection      ')\g
INSERT INTO Diagnose VALUES ('5439','    Other      ')\g
INSERT INTO Diagnose VALUES ('544','     COMPLICATION OF CATHETERIZATION, ANGIOGRAPHY      ')\g
INSERT INTO Diagnose VALUES ('5441','    Hematoma      ')\g
INSERT INTO Diagnose VALUES ('5442','    Thrombosis      ')\g
INSERT INTO Diagnose VALUES ('5443','    Embolus    include: cholesterol      ')\g
INSERT INTO Diagnose VALUES ('5444','    Intimal tear, intramural injection, mural dissection      ')\g
INSERT INTO Diagnose VALUES ('5445','    Spasm      ')\g
INSERT INTO Diagnose VALUES ('5446','    Transmural perforation      ')\g
INSERT INTO Diagnose VALUES ('5447','    Broken or knotted catheter      ')\g
INSERT INTO Diagnose VALUES ('5448','    Adverse reaction to contrast medium      ')\g
INSERT INTO Diagnose VALUES ('5449','    Other    exclude: arteriovenous fistula, post-traumatic (.494)      ')\g
INSERT INTO Diagnose VALUES ('545','     POSTOPERATIVE (code also the specific abnormality)      ')\g
INSERT INTO Diagnose VALUES ('5451','    Palliation of congenital heart disease   exclude: valvular surgery (.453)      ')\g
INSERT INTO Diagnose VALUES ('54511','   Systemic to pulmonic shunt      ')\g
INSERT INTO Diagnose VALUES ('54512','   Enlargement of atrial septal opening      ')\g
INSERT INTO Diagnose VALUES ('54513','   Banding of pulmonary artery      ')\g
INSERT INTO Diagnose VALUES ('54518','   Surgery failure or complication (e.g., occluded shunt)      ')\g
INSERT INTO Diagnose VALUES ('54519','   Other      ')\g
INSERT INTO Diagnose VALUES ('5452','    Correction" of congenital heart disease   exclude: valvular surgery (.453), tumor resection (.4542)"      ')\g
INSERT INTO Diagnose VALUES ('54521','   Direct suture, ligation      ')\g
INSERT INTO Diagnose VALUES ('54522','   Prosthetic patch      ')\g
INSERT INTO Diagnose VALUES ('54523','   Homograft, heterograft      ')\g
INSERT INTO Diagnose VALUES ('54528','   Surgery failure or complication (e.g., persistent shunt)      ')\g
INSERT INTO Diagnose VALUES ('54529','   Other    include: catheter or drug closure of patent ductus arteriosus (e.g., with prostaglandin)      ')\g
INSERT INTO Diagnose VALUES ('5453','    Valvular surgery for congenital or acquired heart disease      ')\g
INSERT INTO Diagnose VALUES ('54531','   Open valvulotomy      ')\g
INSERT INTO Diagnose VALUES ('54532','   Closed valvulotomy      ')\g
INSERT INTO Diagnose VALUES ('54533','   Valvuloplasty (see also .1268)      ')\g
INSERT INTO Diagnose VALUES ('54534','   Replacement with prosthesis      ')\g
INSERT INTO Diagnose VALUES ('54535','   Homograft, heterograft      ')\g
INSERT INTO Diagnose VALUES ('54538','   Surgery failure or complication      ')\g
INSERT INTO Diagnose VALUES ('54539','   Other      ')\g
INSERT INTO Diagnose VALUES ('5454','    Surgery for acquired heart or vascular disease   exclude: valvular surgery (.453), coronary artery surgery (.455)      ')\g
INSERT INTO Diagnose VALUES ('54541','   Resection of aneurysm      ')\g
INSERT INTO Diagnose VALUES ('54542','   Resection of neoplasm    include: congenital      ')\g
INSERT INTO Diagnose VALUES ('54543','   Resection, other    include: of infarct      ')\g
INSERT INTO Diagnose VALUES ('54544','   Homograft or heterograft    exclude: valve (.4535)      ')\g
INSERT INTO Diagnose VALUES ('54545','   Plastic procedure, other    include: angioplasty      ')\g
INSERT INTO Diagnose VALUES ('54548','   Surgery failure or complication      ')\g
INSERT INTO Diagnose VALUES ('54549','   Other      ')\g
INSERT INTO Diagnose VALUES ('5455','    Coronary artery surgery      ')\g
INSERT INTO Diagnose VALUES ('54551','   Systemic vessel implant (e.g., internal mammary, left gastric)      ')\g
INSERT INTO Diagnose VALUES ('54552','   Aortic-coronary anastomosis      ')\g
INSERT INTO Diagnose VALUES ('54553','   Coronary-coronary anastomosis      ')\g
INSERT INTO Diagnose VALUES ('54554','   Endarterectomy      ')\g
INSERT INTO Diagnose VALUES ('54558','   Surgery failure or complication (e.g., occluded or stenosed graft)      ')\g
INSERT INTO Diagnose VALUES ('54559','   Other      ')\g
INSERT INTO Diagnose VALUES ('5456','    Pacing electrode      ')\g
INSERT INTO Diagnose VALUES ('54561','   Temporary transvenous      ')\g
INSERT INTO Diagnose VALUES ('54562','   Permanent transvenous      ')\g
INSERT INTO Diagnose VALUES ('54563','   Epicardial      ')\g
INSERT INTO Diagnose VALUES ('54564','   Electrophysiology studies      ')\g
INSERT INTO Diagnose VALUES ('54569','   Other    exclude: insertion (.128)      ')\g
INSERT INTO Diagnose VALUES ('5457','    Circulatory assistance device      ')\g
INSERT INTO Diagnose VALUES ('54571','   Counter pulsation      ')\g
INSERT INTO Diagnose VALUES ('54572','   Artificial heart      ')\g
INSERT INTO Diagnose VALUES ('54579','   Other      ')\g
INSERT INTO Diagnose VALUES ('5458','    Complication of interventional procedure   include: of biopsy, of aspiration   exclude: of pacing electrode (.43), of catheterization (.44), of angiography (.44)      ')\g
INSERT INTO Diagnose VALUES ('5459','    Other    include: cardiac transplantation      ')\g
INSERT INTO Diagnose VALUES ('546','     FOREIGN BODY      ')\g
INSERT INTO Diagnose VALUES ('5461','    Opaque      ')\g
INSERT INTO Diagnose VALUES ('54611','   Catheter or other tube   exclude: cardiac catheterization (.122)      ')\g
INSERT INTO Diagnose VALUES ('54612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('54613','   Catheter (or other tube) in unsatisfactory position    exclude: broken or knotted catheter (.447)      ')\g
INSERT INTO Diagnose VALUES ('54614','   Prosthetic cardiac valve      ')\g
INSERT INTO Diagnose VALUES ('54617','   Complication of catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('54619','   Other      ')\g
INSERT INTO Diagnose VALUES ('5462','    Nonopaque      ')\g
INSERT INTO Diagnose VALUES ('5469','    Other      ')\g
INSERT INTO Diagnose VALUES ('547','     EFFECT OF RADIATION      ')\g
INSERT INTO Diagnose VALUES ('5471','    Inflammation      ')\g
INSERT INTO Diagnose VALUES ('5472','    Effusion      ')\g
INSERT INTO Diagnose VALUES ('5473','    Fibrosis, constriction      ')\g
INSERT INTO Diagnose VALUES ('5479','    Other      ')\g
INSERT INTO Diagnose VALUES ('549','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('5491','    Abnormal gas collection    include: intracardial, intrapericardial, intravascular      ')\g
INSERT INTO Diagnose VALUES ('5494','    Arteriovenous fistula, post-traumatic   include: complication of angiography      ')\g
INSERT INTO Diagnose VALUES ('5499','    Other    exclude: aneurysm (.732)      ')\g
INSERT INTO Diagnose VALUES ('55','      METABOLIC, ENDOCRINE, TOXIC (for 5th number see box following .69)      ')\g
INSERT INTO Diagnose VALUES ('551','     PITUITARY DISORDER    include: acromegaly      ')\g
INSERT INTO Diagnose VALUES ('552','     THYROID DISORDER    include: hyperthyroidism, hypothyroidism      ')\g
INSERT INTO Diagnose VALUES ('553','     PARATHYROID DISORDER      ')\g
INSERT INTO Diagnose VALUES ('554','     ADRENAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('5541','    Hypercorticosteroidism      ')\g
INSERT INTO Diagnose VALUES ('5545','    Hypocorticosteroidism    include: Addison disease      ')\g
INSERT INTO Diagnose VALUES ('5549','    Other      ')\g
INSERT INTO Diagnose VALUES ('559','     OTHER    include: gout, cholesterol pericarditis, familial Mediterranean fever, lipomatosis cordis   exclude: glycogen storage disease (.1933)      ')\g
INSERT INTO Diagnose VALUES ('56','      OTHER GENERALIZED SYSTEMIC DISORDER (for 5th number see box following .69)      ')\g
INSERT INTO Diagnose VALUES ('561','     CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('5611','    Rheumatoid disorder    exclude: rheumatic heart disease (.831, .841, .861)      ')\g
INSERT INTO Diagnose VALUES ('5612','    Lupus erythematosis      ')\g
INSERT INTO Diagnose VALUES ('5613','    Progressive systemic sclerosis (scleroderma)      ')\g
INSERT INTO Diagnose VALUES ('5614','    Dermatomyositis, polymyositis      ')\g
INSERT INTO Diagnose VALUES ('5619','    Other    include: heart disease associated with rheumatoid arthritis    exclude: congenital connective tissue disorder (.197)      ')\g
INSERT INTO Diagnose VALUES ('562','     VASCULITIS (see also .21)      ')\g
INSERT INTO Diagnose VALUES ('5621','    Polyarteritis (periarteritis) nodosa      ')\g
INSERT INTO Diagnose VALUES ('5625','    Takayasu arteritis, related state      ')\g
INSERT INTO Diagnose VALUES ('5629','    Other    include: mucocutaneous lymph node syndrome (see aneurysm .739)      ')\g
INSERT INTO Diagnose VALUES ('563','     ALLERGIC STATE      ')\g
INSERT INTO Diagnose VALUES ('564','     COMPLICATION OF DRUG THERAPY, DRUG USE      ')\g
INSERT INTO Diagnose VALUES ('565','     HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('5651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('5652','    Thalassemia or variant      ')\g
INSERT INTO Diagnose VALUES ('5653','    Other congenital anemia      ')\g
INSERT INTO Diagnose VALUES ('5654','    Iron deficiency anemia      ')\g
INSERT INTO Diagnose VALUES ('5655','    Other acquired (secondary) anemia      ')\g
INSERT INTO Diagnose VALUES ('5659','    Other      ')\g
INSERT INTO Diagnose VALUES ('568','     OTHER INFILTRATIVE DISORDER    include: amyloid      ')\g
INSERT INTO Diagnose VALUES ('569','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('5697','    Nephritis      ')\g
INSERT INTO Diagnose VALUES ('5698','    Nephrosis      ')\g
INSERT INTO Diagnose VALUES ('5699','    Other      ')\g
INSERT INTO Diagnose VALUES ('57','      CARDIAC FAILURE, HYPERTENSION, ANEURYSM, ATHEROSCLEROSIS (for 5th number see box following .69)      ')\g
INSERT INTO Diagnose VALUES ('571','     CONGESTIVE HEART FAILURE (see pulmonary edema 6.71)      ')\g
INSERT INTO Diagnose VALUES ('5711','    With congenital heart disease (also code type)      ')\g
INSERT INTO Diagnose VALUES ('5712','    With acquired heart disease (also code type)      ')\g
INSERT INTO Diagnose VALUES ('5713','    With volume overload      ')\g
INSERT INTO Diagnose VALUES ('57131','   Iatrogenic    include: transfusion reaction      ')\g
INSERT INTO Diagnose VALUES ('57132','   Acute glomerulonephritis    exclude: chronic renal failure (6.7111), nephrosis (.698)      ')\g
INSERT INTO Diagnose VALUES ('57133','   Placental transfusion      ')\g
INSERT INTO Diagnose VALUES ('57139','   Other      ')\g
INSERT INTO Diagnose VALUES ('5714','    High output state    include: systemic arteriovenous shunt   exclude: hyperthyroidism (.52), anemia (.65)      ')\g
INSERT INTO Diagnose VALUES ('5719','    Other    include: with extrathoracic hypoxia (e.g., high altitude, upper airway obstruction) (see also .786)   exclude: drug induced (.64)      ')\g
INSERT INTO Diagnose VALUES ('572','     HYPERTENSIVE CARDIOVASCULAR DISORDER   exclude: pulmonary hypertension (.78)      ')\g
INSERT INTO Diagnose VALUES ('573','     ANEURYSM (see also Marfan syndrome .1971, Ehlers-Danlos  syndrome .1972)    exclude: myocardial (.1425, .773), dissecting aneurysm (.74), of sinus of Valsalva (.1771, .1772)      ')\g
INSERT INTO Diagnose VALUES ('5731','    Atherosclerotic      ')\g
INSERT INTO Diagnose VALUES ('5732','    Traumatic   include: surgical (e.g., ventricular aneurysm)      ')\g
INSERT INTO Diagnose VALUES ('5733','    Mycotic      ')\g
INSERT INTO Diagnose VALUES ('5734','    Syphilitic      ')\g
INSERT INTO Diagnose VALUES ('5739','    Other    include: with mucocutaneous lymph node syndrome (see .629)      ')\g
INSERT INTO Diagnose VALUES ('574','     INTRAMURAL HEMATOMA WITH DISSECTION (DISSECTING ANEURYSM)      ')\g
INSERT INTO Diagnose VALUES ('5741','    Aortic root to iliac vessels      ')\g
INSERT INTO Diagnose VALUES ('5742','    Limited to ascending aorta      ')\g
INSERT INTO Diagnose VALUES ('5743','    Distal to left subclavian artery      ')\g
INSERT INTO Diagnose VALUES ('5749','    Other      ')\g
INSERT INTO Diagnose VALUES ('575','     ATHEROSCLEROSIS      ')\g
INSERT INTO Diagnose VALUES ('5751','    Ectasia      ')\g
INSERT INTO Diagnose VALUES ('5752','    Tortuosity      ')\g
INSERT INTO Diagnose VALUES ('5753','    Ectasia and tortuosity      ')\g
INSERT INTO Diagnose VALUES ('5754','    Atherosclerotic plaque      ')\g
INSERT INTO Diagnose VALUES ('5759','    Other   exclude: aneurysm (.73), cardiac atherosclerosis (.76, .77)      ')\g
INSERT INTO Diagnose VALUES ('576','     CORONARY ATHEROSCLEROSIS      ')\g
INSERT INTO Diagnose VALUES ('5761','    Less than 50% narrowing of lumen      ')\g
INSERT INTO Diagnose VALUES ('5762','    50-80% narrowing of lumen      ')\g
INSERT INTO Diagnose VALUES ('5763','    More than 80% narrowing of lumen      ')\g
INSERT INTO Diagnose VALUES ('5764','    Occlusion of lumen      ')\g
INSERT INTO Diagnose VALUES ('5768','    More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('5769','    Other      ')\g
INSERT INTO Diagnose VALUES ('577','     CARDIAC EFFECT OF ATHEROSCLEROSIS      ')\g
INSERT INTO Diagnose VALUES ('5771','    Infarction   exclude: in newborn (.1939)      ')\g
INSERT INTO Diagnose VALUES ('5772','    Paradoxical motion, asynergy      ')\g
INSERT INTO Diagnose VALUES ('5773','    Aneurysm (myocardial)      ')\g
INSERT INTO Diagnose VALUES ('5774','    Papillary muscle dysfunction or rupture   exclude: chordae tendineae rupture (.843)      ')\g
INSERT INTO Diagnose VALUES ('5775','    Mural thrombus (see .813)      ')\g
INSERT INTO Diagnose VALUES ('5776','    Myocardial rupture   include: acquired septal defect      ')\g
INSERT INTO Diagnose VALUES ('5778','    More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('5779','    Other    include: valvular dysfunction (.83, .84)      ')\g
INSERT INTO Diagnose VALUES ('578','     PULMONARY HYPERTENSION, COR PULMONALE      ')\g
INSERT INTO Diagnose VALUES ('5781','    Primary (idiopathic) pulmonary hypertension      ')\g
INSERT INTO Diagnose VALUES ('5782','    With diffuse pulmonary parenchymal and airway disease      ')\g
INSERT INTO Diagnose VALUES ('5783','    With pulmonary embolus      ')\g
INSERT INTO Diagnose VALUES ('5784','    With acquired heart disease      ')\g
INSERT INTO Diagnose VALUES ('5785','    With congenital heart disease (see box following .199)   include: Eisenmenger complex   exclude: with persistent fetal circulation (.789)      ')\g
INSERT INTO Diagnose VALUES ('5786','    With extrathoracic hypoxia (see also .719)   include: high altitude, upper airway obstruction      ')\g
INSERT INTO Diagnose VALUES ('5787','    With thoracic deformity, obesity      ')\g
INSERT INTO Diagnose VALUES ('5788','    With pulmonary veno-occlusive disease      ')\g
INSERT INTO Diagnose VALUES ('5789','    Other   include: persistent fetal circulation, schistosomiasis      ')\g
INSERT INTO Diagnose VALUES ('579','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('58','      MISCELLANEOUS (for 5th number see box following .86)      ')\g
INSERT INTO Diagnose VALUES ('581','     CALCIFICATION, OSSIFICATION      ')\g
INSERT INTO Diagnose VALUES ('5811','    Dystrophic    exclude: ligamentum or ductus arteriosum (.1469)      ')\g
INSERT INTO Diagnose VALUES ('5812','    Atherosclerotic      ')\g
INSERT INTO Diagnose VALUES ('5813','    Thrombus      ')\g
INSERT INTO Diagnose VALUES ('5814','    Infarct      ')\g
INSERT INTO Diagnose VALUES ('5815','    Aneurysm      ')\g
INSERT INTO Diagnose VALUES ('5816','    Valve leaflet      ')\g
INSERT INTO Diagnose VALUES ('5817','    Valve annulus      ')\g
INSERT INTO Diagnose VALUES ('5818','    Neoplastic      ')\g
INSERT INTO Diagnose VALUES ('5819','    Other    include: homograft, heterograft  exclude: pericardial (.8242)      ')\g
INSERT INTO Diagnose VALUES ('582','     PERICARDIAL DISEASE      ')\g
INSERT INTO Diagnose VALUES ('5821','    Pericardial effusion, pyopericardium (see also .41)      ')\g
INSERT INTO Diagnose VALUES ('5822','    Hemopericardium, traumatic or other      ')\g
INSERT INTO Diagnose VALUES ('5823','    Pneumo-, hemopneumo-, or pyopneumo-pericardium, traumatic or other (see also 41)      ')\g
INSERT INTO Diagnose VALUES ('5824','    Constrictive, adhesive pericarditis      ')\g
INSERT INTO Diagnose VALUES ('58241','   Without calcification      ')\g
INSERT INTO Diagnose VALUES ('58242','   With calcification      ')\g
INSERT INTO Diagnose VALUES ('5829','    Other    exclude: postpericardiotomy syndrome (.85), postmyocardial infarction syndrome (.85), myxedema (.52)      ')\g
INSERT INTO Diagnose VALUES ('583','     VALVULAR AND PERIVALVULAR STENOSIS (see also congenital .172, .173, .174)      ')\g
INSERT INTO Diagnose VALUES ('5831','    Rheumatic heart disease      ')\g
INSERT INTO Diagnose VALUES ('5832','    Other collagen disorder      ')\g
INSERT INTO Diagnose VALUES ('5833','    Calcific aortic stenosis      ')\g
INSERT INTO Diagnose VALUES ('5834','    Carcinoid syndrome      ')\g
INSERT INTO Diagnose VALUES ('5835','    Postoperative    include: encroachment by prosthetic valve      ')\g
INSERT INTO Diagnose VALUES ('5836','    Subvalvular    exclude: idiopathic hypertrophic subaortic stenosis (.1732)      ')\g
INSERT INTO Diagnose VALUES ('5837','    Supravalvular      ')\g
INSERT INTO Diagnose VALUES ('5839','    Other      ')\g
INSERT INTO Diagnose VALUES ('584','     VALVULAR INCOMPETENCE    exclude: congenital (.175)      ')\g
INSERT INTO Diagnose VALUES ('5841','    Rheumatic heart disease      ')\g
INSERT INTO Diagnose VALUES ('5842','    Connective tissue disorder (see also .197)   include: Marfan syndrome, Ehlers-Danlos syndrome, myxomatous degeneration of mitral valve      ')\g
INSERT INTO Diagnose VALUES ('5843','    Papillary muscle dysfunction or rupture, chordae tendineae rupture      ')\g
INSERT INTO Diagnose VALUES ('5844','    Traumatic perforation or rupture of leaflet, valve dehiscence      ')\g
INSERT INTO Diagnose VALUES ('5845','    Postoperative    include: poppet dysfunction, paravalvular      ')\g
INSERT INTO Diagnose VALUES ('5846','    Dilated annulus, calcified annulus      ')\g
INSERT INTO Diagnose VALUES ('5847','    Metabolic, toxic    include: carcinoid syndrome, methysergide effect      ')\g
INSERT INTO Diagnose VALUES ('5848','    Ankylosing spondylitis      ')\g
INSERT INTO Diagnose VALUES ('5849','    Other      ')\g
INSERT INTO Diagnose VALUES ('585','     POSTPERICARDIOTOMY SYNDROME, POSTMYOCARDIAL INFARCTION SYNDROME      ')\g
INSERT INTO Diagnose VALUES ('586','     MYOCARDIOPATHY    exclude: inflammatory (.2), endocrine (.5), effect of radiation (.47), congenital (.193)      ')\g
INSERT INTO Diagnose VALUES ('5861','    Rheumatic    exclude: other collagen-vascular disorder (.61, .62)      ')\g
INSERT INTO Diagnose VALUES ('5862','    Uremia      ')\g
INSERT INTO Diagnose VALUES ('5863','    Hemochromatosis      ')\g
INSERT INTO Diagnose VALUES ('5864','    Nutritional   include: beriberi, kwashiokor, alcohol effect      ')\g
INSERT INTO Diagnose VALUES ('5869','    Other    exclude: nephrosis (.698), amyloidosis (.68), glycogen storage disease (.1933)      ')\g
INSERT INTO Diagnose VALUES ('587','     ABNORMAL CARDIAC CONFIGURATION, SIZE (see code cause)      ')\g
INSERT INTO Diagnose VALUES ('5871','    Nonspecific cardiomegaly   exclude: anemia (.65)      ')\g
INSERT INTO Diagnose VALUES ('5872','    Chamber enlargement      ')\g
INSERT INTO Diagnose VALUES ('5873','    Unusual or unexplained heart shape      ')\g
INSERT INTO Diagnose VALUES ('5874','    Related to skeletal deformity      ')\g
INSERT INTO Diagnose VALUES ('58741','   Pectus excavatum (also code 472.1496)      ')\g
INSERT INTO Diagnose VALUES ('58742','   Straight back syndrome (see 3.867)      ')\g
INSERT INTO Diagnose VALUES ('58743','   Kyphoscoliosis      ')\g
INSERT INTO Diagnose VALUES ('58749','   Other      ')\g
INSERT INTO Diagnose VALUES ('5875','    Postoperative      ')\g
INSERT INTO Diagnose VALUES ('5876','    Related to diaphragmatic abnormality      ')\g
INSERT INTO Diagnose VALUES ('5877','    Small heart    include: associated with dehydration   exclude: Addison disease (.545)      ')\g
INSERT INTO Diagnose VALUES ('5879','    Other    include: with obesity      ')\g
INSERT INTO Diagnose VALUES ('588','     FLUOROSCOPIC, CINERADIOGRAPHIC OBSERVATION      ')\g
INSERT INTO Diagnose VALUES ('5881','    Hilar dance""      ')\g
INSERT INTO Diagnose VALUES ('5882','    Abnormal cardiac pulsations      ')\g
INSERT INTO Diagnose VALUES ('58821','   Dyskinetic      ')\g
INSERT INTO Diagnose VALUES ('58822','   Akinetic      ')\g
INSERT INTO Diagnose VALUES ('58823','   Paradoxical      ')\g
INSERT INTO Diagnose VALUES ('58829','   Other      ')\g
INSERT INTO Diagnose VALUES ('5883','    Abnormal motion of cage or poppet  of prosthetic valve      ')\g
INSERT INTO Diagnose VALUES ('5889','    Other      ')\g
INSERT INTO Diagnose VALUES ('589','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('59','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('591','     FUNDAMENTAL OBSERVATION    include: collateral pulmonary circulation (e.g., enlarged bronchial arteries)      ')\g
INSERT INTO Diagnose VALUES ('592','     ANATOMICAL DETAIL      ')\g
INSERT INTO Diagnose VALUES ('593','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('594','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('599','     OTHER    include: phenomenon related to technique of examination      ')\g
INSERT INTO Diagnose VALUES ('61','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('611','     NORMAL ROUTINE PLAIN FILM    exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('612','     SPECIAL TECHNIQUE, PROCEDURE, PROJEC-TION      ')\g
INSERT INTO Diagnose VALUES ('6121','    Digital radiographic techniques, tomography, MR, nuclear medicine      ')\g
INSERT INTO Diagnose VALUES ('61211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('612111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('612112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('612113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('612114','  Delayed scanning following enhancement (e.g., for detection of liver lesions)      ')\g
INSERT INTO Diagnose VALUES ('612115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('612116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('612117','  Three-dimensional reconstruc-tion      ')\g
INSERT INTO Diagnose VALUES ('612118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('612119','  Other, including serial enhance-ment studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('61214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('612141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('6121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('6121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('6121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('6121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('6121415',' Specific resonance suppressed   include: fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('6121416',' High-speed imaging include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('6121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('6121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('612142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('612143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('612144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('612145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('612146','  Quantitative tissue characteriza-tion, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('612147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('612149','  Other      ')\g
INSERT INTO Diagnose VALUES ('61215','   Digital radiography    exclude: with angiography (.124-3, 94.12-3)      ')\g
INSERT INTO Diagnose VALUES ('61216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('612161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('612162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('612163','  PET      ')\g
INSERT INTO Diagnose VALUES ('612164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('612165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('612166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('612167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('612168','  Therapeutic procedure   include:use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('612169','  Other.  See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('61217','   Nuclear medicine study-lung medias-tinum, pleura      ')\g
INSERT INTO Diagnose VALUES ('612171','  Lung perfusion study      ')\g
INSERT INTO Diagnose VALUES ('612173','  Metabolism study      ')\g
INSERT INTO Diagnose VALUES ('612174','  Lung ventilation study with 133Xe or 127Xe      ')\g
INSERT INTO Diagnose VALUES ('612175','  Lung ventilation study with 81mKr      ')\g
INSERT INTO Diagnose VALUES ('612176','  Lung ventilation study with aerosol   include: 99mTc technegas      ')\g
INSERT INTO Diagnose VALUES ('612177','  Alveolar epithelial permeability study      ')\g
INSERT INTO Diagnose VALUES ('612179','  Other      ')\g
INSERT INTO Diagnose VALUES ('61218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('61219','   Other      ')\g
INSERT INTO Diagnose VALUES ('6122','    Bronchography      ')\g
INSERT INTO Diagnose VALUES ('6123','    Fluoroscopy      ')\g
INSERT INTO Diagnose VALUES ('6124','    Angiography (also see 94.)      ')\g
INSERT INTO Diagnose VALUES ('61241','   Pulmonary arteriography      ')\g
INSERT INTO Diagnose VALUES ('61242','   Aortography      ')\g
INSERT INTO Diagnose VALUES ('61243','   Bronchial arteriography      ')\g
INSERT INTO Diagnose VALUES ('61249','   Other    include: azygography, intraosseous venography      ')\g
INSERT INTO Diagnose VALUES ('6125','    Special nonroutine projection      ')\g
INSERT INTO Diagnose VALUES ('61251','   Lordotic      ')\g
INSERT INTO Diagnose VALUES ('61252','   Decubitus      ')\g
INSERT INTO Diagnose VALUES ('61253','   Inspiration-expiration study      ')\g
INSERT INTO Diagnose VALUES ('61254','   Stereoscopic films      ')\g
INSERT INTO Diagnose VALUES ('61259','   Other      ')\g
INSERT INTO Diagnose VALUES ('6126','    Lung biopsy      ')\g
INSERT INTO Diagnose VALUES ('61261','   Closed needle biopsy      ')\g
INSERT INTO Diagnose VALUES ('61262','   Transbronchial biopsy      ')\g
INSERT INTO Diagnose VALUES ('61263','   Needle biopsy of pleura      ')\g
INSERT INTO Diagnose VALUES ('61269','   Other      ')\g
INSERT INTO Diagnose VALUES ('6129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('61291','   Therapy localization      ')\g
INSERT INTO Diagnose VALUES ('61292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('61294','   Supervoltage roentgenography      ')\g
INSERT INTO Diagnose VALUES ('61295','   Emphysema measurements      ')\g
INSERT INTO Diagnose VALUES ('61296','   Cine, video study      ')\g
INSERT INTO Diagnose VALUES ('61297','   Diagnostic pneumothorax or pneumomediastinum      ')\g
INSERT INTO Diagnose VALUES ('61298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('612981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('612982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('612983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('612984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('612985','  Guided procedure for diagnosis    include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('612986','  Guided procedure for therapy    include: drainage      ')\g
INSERT INTO Diagnose VALUES ('612987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('612988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('612989','  Special nonroutine projection, mode, or transducer type      ')\g
INSERT INTO Diagnose VALUES ('61299','   Other    include: xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('613','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('6131','    Azygos lobe      ')\g
INSERT INTO Diagnose VALUES ('6132','    Superior accessory lobe      ')\g
INSERT INTO Diagnose VALUES ('6133','    Inferior accessory lobe      ')\g
INSERT INTO Diagnose VALUES ('6134','    Normal or prominent thymus      ')\g
INSERT INTO Diagnose VALUES ('6135','    Unusual calcification of tracheal cartilage      ')\g
INSERT INTO Diagnose VALUES ('6136','    Asymptomatic anterior tracheal vascular indentation      ')\g
INSERT INTO Diagnose VALUES ('6139','    Other    include: accessory bronchus from trachea, vertical fissure line      ')\g
INSERT INTO Diagnose VALUES ('614','     CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('6141','    Agenesis, aplasia, hypoplasia (lobe or entire lung)      ')\g
INSERT INTO Diagnose VALUES ('61411','   Agenesis, complete absence of bron-chus and lung tissue      ')\g
INSERT INTO Diagnose VALUES ('61412','   Atresia, congenital bronchial atresia    include: bronchial remnant, absent lung      ')\g
INSERT INTO Diagnose VALUES ('61413','   Hypoplasia, underdeveloped bronchus and lung      ')\g
INSERT INTO Diagnose VALUES ('61414','   Congenital tracheal stenosis without tracheoesophageal fistula      ')\g
INSERT INTO Diagnose VALUES ('61419','   Other    exclude: thymic (.2514)      ')\g
INSERT INTO Diagnose VALUES ('6142','    Hypogenetic lung syndrome, hypoplastic lung with abnormal bronchial tree, absent or hypoplastic pulmonary artery and anomalous venous drainage   include: scimitar syndrome      ')\g
INSERT INTO Diagnose VALUES ('61421','   With congenital heart disease      ')\g
INSERT INTO Diagnose VALUES ('61422','   With blood supply to lung from lower thoracic aorta or abdominal aorta      ')\g
INSERT INTO Diagnose VALUES ('61423','   With absent inferior vena cava      ')\g
INSERT INTO Diagnose VALUES ('61428','   With more than one of the above      ')\g
INSERT INTO Diagnose VALUES ('61429','   With other anomaly (see 5.1559)      ')\g
INSERT INTO Diagnose VALUES ('6144','    Duplication, bronchopulmonary foregut malformation      ')\g
INSERT INTO Diagnose VALUES ('61441','   Congenital bronchial cyst, bronchogenic cyst (see also .3156)      ')\g
INSERT INTO Diagnose VALUES ('61442','   Enteric cyst, gastroenteric cyst      ')\g
INSERT INTO Diagnose VALUES ('61443','   Neurenteric cyst      ')\g
INSERT INTO Diagnose VALUES ('61449','   Other    exclude: accessory diaphragm (.1424)      ')\g
INSERT INTO Diagnose VALUES ('6145','    Bronchopulmonary sequestration      ')\g
INSERT INTO Diagnose VALUES ('61451','   Intralobar      ')\g
INSERT INTO Diagnose VALUES ('61452','   Extralobar      ')\g
INSERT INTO Diagnose VALUES ('6146','    Pulmonary adenomatoid malformation      ')\g
INSERT INTO Diagnose VALUES ('6147','    Neurocutaneous disorder      ')\g
INSERT INTO Diagnose VALUES ('61471','   Tuberous sclerosis      ')\g
INSERT INTO Diagnose VALUES ('61472','   Neurofibromatosis      ')\g
INSERT INTO Diagnose VALUES ('61479','   Other      ')\g
INSERT INTO Diagnose VALUES ('6149','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('61491','   Lymphangiectasia      ')\g
INSERT INTO Diagnose VALUES ('61492','   Tracheomegaly, bronchomegaly, tracheal diverticulosis, saber sheath trachea      ')\g
INSERT INTO Diagnose VALUES ('61493','   Tracheomalacia, bronchomalacia      ')\g
INSERT INTO Diagnose VALUES ('61494','   Pulmonary vascular malformation, arteriovenous fistula or varix (see 5.1559)  include: hereditary hemorrhagic telangiectasia with arteriovenous fistula      ')\g
INSERT INTO Diagnose VALUES ('61495','   Absent pectoral muscle (see 474.141)   exclude: Poland syndrome (4.1621)      ')\g
INSERT INTO Diagnose VALUES ('61496','   Congenital lung hernia      ')\g
INSERT INTO Diagnose VALUES ('61499','   Other    exclude: alpha-1-antitrypsin deficiency (.7511), congenital lobar emphysema (.7531), congenital pneumonia (.786), cystic fibrosis (.252), diaphragmatic abnormality (7.15), pulmonary sling (.7523), tracheoesophageal fistula (7.142)      ')\g
INSERT INTO Diagnose VALUES ('619','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('62','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('620','     INFECTION CLASSIFIED BY ORGANISM (N.B: These numbers to be used in all 10 anatomical fields)      ')\g
INSERT INTO Diagnose VALUES ('6201','    Gram-positive facultative bacterium      ')\g
INSERT INTO Diagnose VALUES ('62011','   Pneumococcus      ')\g
INSERT INTO Diagnose VALUES ('62012','   Staphylococcus      ')\g
INSERT INTO Diagnose VALUES ('62013','   Streptococcus   include: Enterococcus      ')\g
INSERT INTO Diagnose VALUES ('62014','   Nocardia      ')\g
INSERT INTO Diagnose VALUES ('62019','   Other organism    include: Bacillus anthracis, Corynebac-terium diphtheriae, listeria monocytogenes      ')\g
INSERT INTO Diagnose VALUES ('6202','    Gram-negative bacterium      ')\g
INSERT INTO Diagnose VALUES ('62021','   Pseudomonas    include: meliodosis      ')\g
INSERT INTO Diagnose VALUES ('62022','   Klebsiella    include: Friedlander bacillus, Kleb-siella pneumoniae      ')\g
INSERT INTO Diagnose VALUES ('62023','   Escherichia coli      ')\g
INSERT INTO Diagnose VALUES ('62024','   Proteus species      ')\g
INSERT INTO Diagnose VALUES ('62025','   Salmonella, Shigella      ')\g
INSERT INTO Diagnose VALUES ('62026','   Hemophilus species   include: H. influenzae, Bacillus pertussis      ')\g
INSERT INTO Diagnose VALUES ('62027','   Yersinia (Pasteurella)    include: Y. enterocolitica, Y. pestis (plague), Y. tularensis (tularemia), Y. multocida      ')\g
INSERT INTO Diagnose VALUES ('62028','   Legionella    include: Pneumophila, micdadei (Pittsburgh pneumonia agent)      ')\g
INSERT INTO Diagnose VALUES ('62029','   Other    include: Enterobacter, Serratia, Edwardsiella, Arizona, Citrobacter, Brucella, Neisseria, Vibrio cholerae, Herellea, Mima      ')\g
INSERT INTO Diagnose VALUES ('6203','    Mycobacterium   exclude: tuberculosis (.23)      ')\g
INSERT INTO Diagnose VALUES ('62031','   Anonymous (atypical)      ')\g
INSERT INTO Diagnose VALUES ('62032','   M. leprae (leprosy)      ')\g
INSERT INTO Diagnose VALUES ('62039','   Other      ')\g
INSERT INTO Diagnose VALUES ('6204','    Anaerobic bacterium      ')\g
INSERT INTO Diagnose VALUES ('62041','   Bacteroides      ')\g
INSERT INTO Diagnose VALUES ('62042','   Anaerobic streptococcus      ')\g
INSERT INTO Diagnose VALUES ('62043','   Clostridia    include: C. tetani, C. botulinum, C. perfringens      ')\g
INSERT INTO Diagnose VALUES ('62044','   Actinomyces (actinomycosis)      ')\g
INSERT INTO Diagnose VALUES ('62049','   Other      ')\g
INSERT INTO Diagnose VALUES ('6205','    Fungus      ')\g
INSERT INTO Diagnose VALUES ('62051','   Blastomyces dermatitidis (North American blastomycosis)      ')\g
INSERT INTO Diagnose VALUES ('62052','   Coccidioides immitis (coccidioidomycosis)      ')\g
INSERT INTO Diagnose VALUES ('62053','   Histoplasma capsulatum, H. duBoisii (histoplasmosis)      ')\g
INSERT INTO Diagnose VALUES ('62054','   Cryptococcus neoformans (Cryptococcus, torulopsis)      ')\g
INSERT INTO Diagnose VALUES ('62056','   Aspergillus fumigatus (aspergillosis) (see .634)      ')\g
INSERT INTO Diagnose VALUES ('62057','   Candida group (candidiasis, moniliasis)      ')\g
INSERT INTO Diagnose VALUES ('62059','   Other   include: mucormycosis, sporotrichosis, South American blastomycosis    exclude: actinomyces (actinomycosis .2044)      ')\g
INSERT INTO Diagnose VALUES ('6206','    Virus, Mycoplasma      ')\g
INSERT INTO Diagnose VALUES ('62061','   Mycoplasma pneumoniae (primary atypical pneumonia)      ')\g
INSERT INTO Diagnose VALUES ('62062','   Influenza      ')\g
INSERT INTO Diagnose VALUES ('62063','   Measles    include: atypical measles infection, giant cell pneumonia      ')\g
INSERT INTO Diagnose VALUES ('62064','   Varicella (chickenpox)      ')\g
INSERT INTO Diagnose VALUES ('62065','   Herpes      ')\g
INSERT INTO Diagnose VALUES ('62066','   Cytomegalovirus (cytomegalic inclu-sion disease)      ')\g
INSERT INTO Diagnose VALUES ('62067','   Bedsoniae group (ornithosis, psittacosis)      ')\g
INSERT INTO Diagnose VALUES ('62068','   Human immunodeficiency virus infection (HIV) (see .2518)      ')\g
INSERT INTO Diagnose VALUES ('62069','   Other   include: respiratory syncytial virus, Epstein-Barr (infectious mononucleo-sis, see also .271), pertussis      ')\g
INSERT INTO Diagnose VALUES ('6207','    Protozoa, spirochete      ')\g
INSERT INTO Diagnose VALUES ('62071','   Entamoeba histolytica (amebiasis)      ')\g
INSERT INTO Diagnose VALUES ('62072','   Other intestinal protozoa    include: Giardia lamblia      ')\g
INSERT INTO Diagnose VALUES ('62073','   Blood protozoa    include: Leishmania, Trypanosoma  and Plasmodia (malaria)      ')\g
INSERT INTO Diagnose VALUES ('62074','   Toxoplasma group      ')\g
INSERT INTO Diagnose VALUES ('62075','   Pneumocystis carinii      ')\g
INSERT INTO Diagnose VALUES ('62076','   Treponema pallidum (congenital syphilis)      ')\g
INSERT INTO Diagnose VALUES ('62077','   Treponema pallidum (acquired syphilis)      ')\g
INSERT INTO Diagnose VALUES ('62078','   Leptospira group (leptospirosis)      ')\g
INSERT INTO Diagnose VALUES ('62079','   Other    include: Other treponemal species, (e.g., yaws)      ')\g
INSERT INTO Diagnose VALUES ('6208','    Helminth, roundworm, flatworm      ')\g
INSERT INTO Diagnose VALUES ('62081','   Intestinal roundworm    include: Enterobius vermicularis, Ascaris lumbricoides, Necator americanus, Strongyloides stercoralis, Toxocara, Trichinella spiralis      ')\g
INSERT INTO Diagnose VALUES ('62082','   Tissue roundworm    include: Wuchereria bancrofti, Dracunculus medinensis      ')\g
INSERT INTO Diagnose VALUES ('62083','   Tapeworm    include: Taenia saginata, Taenia solium (cysticercosis), Echinococcus      ')\g
INSERT INTO Diagnose VALUES ('62084','   Blood fluke    include: Schistosoma      ')\g
INSERT INTO Diagnose VALUES ('62085','   Intestinal, liver, or lung fluke    include: Paragonimus westermani, Clonorchis sinensis      ')\g
INSERT INTO Diagnose VALUES ('62089','   Other      ')\g
INSERT INTO Diagnose VALUES ('6209','    Other    include: Chlamydia, Rickettsia      ')\g
INSERT INTO Diagnose VALUES ('621','     PNEUMONIA, ABSCESS      ')\g
INSERT INTO Diagnose VALUES ('6211','    Lobar pneumonia      ')\g
INSERT INTO Diagnose VALUES ('6212','    Bronchopneumonia      ')\g
INSERT INTO Diagnose VALUES ('6213','    Interstitial      ')\g
INSERT INTO Diagnose VALUES ('6214','    Aspiration      ')\g
INSERT INTO Diagnose VALUES ('6215','    Suppurative pneumonia, pulmonary gangrene      ')\g
INSERT INTO Diagnose VALUES ('6216','    Abscess      ')\g
INSERT INTO Diagnose VALUES ('62161','   Acute      ')\g
INSERT INTO Diagnose VALUES ('62162','   Subacute or chronic      ')\g
INSERT INTO Diagnose VALUES ('62163','   Multiple abscesses    include: embolic      ')\g
INSERT INTO Diagnose VALUES ('62169','   Other    exclude: mediastinal (.272)      ')\g
INSERT INTO Diagnose VALUES ('6217','    Opportunistic infection      ')\g
INSERT INTO Diagnose VALUES ('62171','   With myeloproliferative or lymphoproliferative disorder      ')\g
INSERT INTO Diagnose VALUES ('62172','   Related to steroid therapy      ')\g
INSERT INTO Diagnose VALUES ('62173','   Related to immunosuppressive drugs      ')\g
INSERT INTO Diagnose VALUES ('62179','   Other (see also immunological defi-ciency state .251, fungus ball" .254)"      ')\g
INSERT INTO Diagnose VALUES ('6218','    Recurrent pneumonia, chronic pneumonia      ')\g
INSERT INTO Diagnose VALUES ('62181','   Recurrent pneumonia   exclude: immunological deficiency state  (.251)      ')\g
INSERT INTO Diagnose VALUES ('62182','   Chronic (unresolved) pneumonia      ')\g
INSERT INTO Diagnose VALUES ('62189','   Other      ')\g
INSERT INTO Diagnose VALUES ('6219','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('62191','   Acute bronchiolitis      ')\g
INSERT INTO Diagnose VALUES ('62192','   Miliary pneumonia      ')\g
INSERT INTO Diagnose VALUES ('62193','   Round pneumonia      ')\g
INSERT INTO Diagnose VALUES ('62199','   Other   exclude: Loffler pneumonia (.631)      ')\g
INSERT INTO Diagnose VALUES ('622','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('6221','    Adenopathy      ')\g
INSERT INTO Diagnose VALUES ('6222','    Alveolar" sarcoid, parenchymal sarcoid"      ')\g
INSERT INTO Diagnose VALUES ('6223','    Nodes and parenchymal disease      ')\g
INSERT INTO Diagnose VALUES ('6224','    Pleurodiaphragmatic adhesion      ')\g
INSERT INTO Diagnose VALUES ('6225','    Unusual manifestation    include: collapse, calcification, spontaneous pneumothorax      ')\g
INSERT INTO Diagnose VALUES ('6226','    End stage" fibrosis   include: multicystic, bullae"      ')\g
INSERT INTO Diagnose VALUES ('6229','    Other (see also sarcoid associated with tuberculosis .2371, intracavitary fungus ball .254)      ')\g
INSERT INTO Diagnose VALUES ('623','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('6231','    Primary (see also .233 to .239)      ')\g
INSERT INTO Diagnose VALUES ('62311','   Primary complex, active      ')\g
INSERT INTO Diagnose VALUES ('62312','   Lymphadenopathy, active      ')\g
INSERT INTO Diagnose VALUES ('62313','   Inactive (healed")"      ')\g
INSERT INTO Diagnose VALUES ('62319','   Other      ')\g
INSERT INTO Diagnose VALUES ('6232','    Reactivation (reinfection) (see also .233 to .239)      ')\g
INSERT INTO Diagnose VALUES ('62321','   Fibrocalcific, calcific, chronic fibroid contracting," "productive""      ')\g
INSERT INTO Diagnose VALUES ('62322','   Soft" lesions, caseous pneumonia, "exudative""      ')\g
INSERT INTO Diagnose VALUES ('62323','   Tuberculous lobar pneumonia      ')\g
INSERT INTO Diagnose VALUES ('62324','   Tuberculous cavitation      ')\g
INSERT INTO Diagnose VALUES ('62325','   Basal or other unusual location      ')\g
INSERT INTO Diagnose VALUES ('62329','   Other      ')\g
INSERT INTO Diagnose VALUES ('6233','    Collapse, bronchial tuberculosis      ')\g
INSERT INTO Diagnose VALUES ('6234','    Tuberculous pleuritis, pleural effusion      ')\g
INSERT INTO Diagnose VALUES ('6235','    Miliary      ')\g
INSERT INTO Diagnose VALUES ('6236','    Tuberculoma, solitary or multiple      ')\g
INSERT INTO Diagnose VALUES ('6237','    Tuberculosis associated with other disease      ')\g
INSERT INTO Diagnose VALUES ('62371','   Sarcoid      ')\g
INSERT INTO Diagnose VALUES ('62372','   Carcinoma      ')\g
INSERT INTO Diagnose VALUES ('62373','   Pneumoconiosis      ')\g
INSERT INTO Diagnose VALUES ('62374','   Bronchiectasis      ')\g
INSERT INTO Diagnose VALUES ('62375','   Diabetes      ')\g
INSERT INTO Diagnose VALUES ('62379','   Other   exclude: anonymous (atypical) mycobacterium (.2031)      ')\g
INSERT INTO Diagnose VALUES ('624','     SINUS TRACT, FISTULA    include: acquired tracheoesophageal, bronchopleural    exclude: congenital tracheoesophageal fistula (7.142)      ')\g
INSERT INTO Diagnose VALUES ('625','     OTHER PULMONARY INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('6251','    Immunological deficiency states and condi-tions associated with recurrent pulmonary infection      ')\g
INSERT INTO Diagnose VALUES ('62511','   Agammaglobulinemia, hypogammaglobulinemia      ')\g
INSERT INTO Diagnose VALUES ('62512','   Dysgammaglobulinemia or primary immunoglobulin aberration      ')\g
INSERT INTO Diagnose VALUES ('62513','   Immune deficiency with thrombocytopenia and eczema (Wiskott-Aldrich syndrome)      ')\g
INSERT INTO Diagnose VALUES ('62514','   Thymic aplasia (DiGeorge syndrome)      ')\g
INSERT INTO Diagnose VALUES ('62515','   Other immunological deficiency state    include: ataxia telangiectasia      ')\g
INSERT INTO Diagnose VALUES ('62516','   Chronic granulomatous disease of childhood (Landing-Shirkey syndrome)      ')\g
INSERT INTO Diagnose VALUES ('62517','   Familial dysautonomia (Riley-Day syndrome)      ')\g
INSERT INTO Diagnose VALUES ('62518','   Acquired immunodeficiency syn-drome (AIDS) (see .2068)      ')\g
INSERT INTO Diagnose VALUES ('62519','   Other    include: Chediak-Steinbrinck-Higashi syndrome      ')\g
INSERT INTO Diagnose VALUES ('6252','    Cystic fibrosis      ')\g
INSERT INTO Diagnose VALUES ('6253','    Cholesterol, lipid pneumonia      ')\g
INSERT INTO Diagnose VALUES ('6254','    Intracavitary fungus ball""      ')\g
INSERT INTO Diagnose VALUES ('6259','    Other    include: chronic aspiration from esophageal dysfunction      ')\g
INSERT INTO Diagnose VALUES ('626','     BRONCHIECTASIS (see also bronchitis .755)      ')\g
INSERT INTO Diagnose VALUES ('6261','    Fusiform or cylindrical      ')\g
INSERT INTO Diagnose VALUES ('6262','    Saccular      ')\g
INSERT INTO Diagnose VALUES ('6263','    Pseudo- or reversible      ')\g
INSERT INTO Diagnose VALUES ('6264','    Bronchiolectasis      ')\g
INSERT INTO Diagnose VALUES ('6265','    Mucoid impaction syndrome    include: bronchocele, bronchial mucocele   exclude: aspergillosis with mucoid impac-tion (.634), asthma with mucoid impaction (.7542), cystic fibrosis (.252)      ')\g
INSERT INTO Diagnose VALUES ('6266','    Immotile cilia syndrome (see also 2.25)   include: Kartegener syndrome (see 5.1652)      ')\g
INSERT INTO Diagnose VALUES ('6269','    Other    exclude: chronic bronchitis (.755), asthma (.754), tuberculosis with bronchiectasis (.2374)      ')\g
INSERT INTO Diagnose VALUES ('627','     MEDIASTINAL INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('6271','    Lymphadenitis    include: erythema nodosum, infectious mononucleosis (see .2069)    exclude: lymphadenopathy of unknown etiology (.391), sarcoid (.222), tuberculosis (.23)      ')\g
INSERT INTO Diagnose VALUES ('6272','    Mediastinal abscess, mediastinitis      ')\g
INSERT INTO Diagnose VALUES ('6273','    Mediastinal fibrosis      ')\g
INSERT INTO Diagnose VALUES ('6279','    Other      ')\g
INSERT INTO Diagnose VALUES ('628','     PULMONARY NODULE OR CAVITY      ')\g
INSERT INTO Diagnose VALUES ('6281','    Pulmonary nodule      ')\g
INSERT INTO Diagnose VALUES ('62811','   Calcified      ')\g
INSERT INTO Diagnose VALUES ('62812','   Cavitated      ')\g
INSERT INTO Diagnose VALUES ('6298','    Infectious complications of AIDS      ')\g
INSERT INTO Diagnose VALUES ('63','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('631','     NEOPLASM, CYST, OTHER NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('6311','    Bronchial neoplasms      ')\g
INSERT INTO Diagnose VALUES ('63111','   Carcinoid tumor      ')\g
INSERT INTO Diagnose VALUES ('63112','   Adenoid cystic carcinoma (cylindroma)      ')\g
INSERT INTO Diagnose VALUES ('63113','   Mucoepidermoid carcinoma      ')\g
INSERT INTO Diagnose VALUES ('63114','   Unusual histology,   include: mixed varieties      ')\g
INSERT INTO Diagnose VALUES ('63115','   Presentation as solitary nodule      ')\g
INSERT INTO Diagnose VALUES ('63116','   Presentation with obstructive pneumonitis or atelectasis      ')\g
INSERT INTO Diagnose VALUES ('63117','   Unusual clinical feature   include: Cushing syndrome, carcinoid syndrome, extrabronchial or mediasti-nal location      ')\g
INSERT INTO Diagnose VALUES ('63119','   Other      ')\g
INSERT INTO Diagnose VALUES ('6312','    Cystic disease of the lung      ')\g
INSERT INTO Diagnose VALUES ('63121','   Congenital cystic disease of the lung   exclude: bronchogenic cyst (.1441), adenomatoid malformation (.146), cystic bronchiectasis (.26), bronchopulmonary sequestration (.145)      ')\g
INSERT INTO Diagnose VALUES ('63122','   Bleb, bulla, pneumatocele    exclude: post-traumatic (.4124), em-physema (.751)      ')\g
INSERT INTO Diagnose VALUES ('63129','   Other      ')\g
INSERT INTO Diagnose VALUES ('6314','    Hamartoma      ')\g
INSERT INTO Diagnose VALUES ('6315','    Mediastinal neoplasm      ')\g
INSERT INTO Diagnose VALUES ('63151','   Aberrant thyroid      ')\g
INSERT INTO Diagnose VALUES ('63152','   Pericardial (celomic) cyst      ')\g
INSERT INTO Diagnose VALUES ('63153','   Germ cell tumor      ')\g
INSERT INTO Diagnose VALUES ('631531','  Benign teratoma      ')\g
INSERT INTO Diagnose VALUES ('631532','  Malignant teratoma      ')\g
INSERT INTO Diagnose VALUES ('631533','  Seminoma      ')\g
INSERT INTO Diagnose VALUES ('631534','  Choriocarcinoma      ')\g
INSERT INTO Diagnose VALUES ('631535','  Embryonal cell sarcoma      ')\g
INSERT INTO Diagnose VALUES ('631539','  Other      ')\g
INSERT INTO Diagnose VALUES ('63154','   Thymoma (benign or malignant), thymolipoma, without systemic manifestations; thymic cyst      ')\g
INSERT INTO Diagnose VALUES ('63155','   Thymoma with systemic manifesta-tions      ')\g
INSERT INTO Diagnose VALUES ('63156','   Mediastinal bronchogenic cyst    exclude: congenital intrapulmonary bronchogenic cyst (.1441)      ')\g
INSERT INTO Diagnose VALUES ('63157','   Hygroma, cystic hygroma, lymphangioma      ')\g
INSERT INTO Diagnose VALUES ('63159','   Other    include: lipoma, parathyroid adenoma   exclude: lipid deposition in Cushing disease or steroid therapy (.512)      ')\g
INSERT INTO Diagnose VALUES ('6316','    Benign neurogenic neoplasm      ')\g
INSERT INTO Diagnose VALUES ('63161','   Peripheral nerve neoplasm    include: neurofibroma, neurilemmoma, schwannoma      ')\g
INSERT INTO Diagnose VALUES ('63162','   Sympathetic ganglion neoplasm   include: ganglioneuroma   exclude: neuroblastoma (.3251)      ')\g
INSERT INTO Diagnose VALUES ('63163','   Paraganglionic neoplasm    include: chromaffinoma, pheochromocytoma, paraganglioma, chemodectoma      ')\g
INSERT INTO Diagnose VALUES ('63169','   Other      ')\g
INSERT INTO Diagnose VALUES ('6317','    Pleural neoplasm   include: mesothelioma, fibroma      ')\g
INSERT INTO Diagnose VALUES ('6318','    Pseudotumor      ')\g
INSERT INTO Diagnose VALUES ('63181','   Xanthomatous pseudotumor, postinflammatory pseudotumor, xanthogranuloma, sclerosing hemangioma, xanthoma, histiocytoma      ')\g
INSERT INTO Diagnose VALUES ('63182','   Pseudolymphoma, lymphocytic pseudotumor of lung      ')\g
INSERT INTO Diagnose VALUES ('63183','   Plasma cell granuloma      ')\g
INSERT INTO Diagnose VALUES ('63185','   Phantom tumor due to intrapleural fluid      ')\g
INSERT INTO Diagnose VALUES ('63189','   Other      ')\g
INSERT INTO Diagnose VALUES ('6319','    Other    include: papilloma, papillomatosis, leiomyoma, fibroma, hemangioma, hemangioendothelioma, hemangiopericytoma, granular cell myoblastoma, chrondroma, lipoma, endometrioma      ')\g
INSERT INTO Diagnose VALUES ('632','     MALIGNANT NEOPLASM-PRIMARY      ')\g
INSERT INTO Diagnose VALUES ('6321','    Carcinoma-type      ')\g
INSERT INTO Diagnose VALUES ('63211','   Squamous cell      ')\g
INSERT INTO Diagnose VALUES ('63212','   Adenocarcinoma      ')\g
INSERT INTO Diagnose VALUES ('63213','   Undifferentiated small cell (oat cell)      ')\g
INSERT INTO Diagnose VALUES ('63214','   Undifferentiated large cell      ')\g
INSERT INTO Diagnose VALUES ('63215','   Anaplastic      ')\g
INSERT INTO Diagnose VALUES ('63216','   Bronchoalveolar (alveolar cell)      ')\g
INSERT INTO Diagnose VALUES ('63217','   Carcinosarcoma      ')\g
INSERT INTO Diagnose VALUES ('63218','   Rare variety   include: giant cell carcinoma      ')\g
INSERT INTO Diagnose VALUES ('63219','   Other      ')\g
INSERT INTO Diagnose VALUES ('6322','    Carcinoma-appearance      ')\g
INSERT INTO Diagnose VALUES ('63221','   Localized mass, nodule      ')\g
INSERT INTO Diagnose VALUES ('63222','   Superior sulcus neoplasm (Pancoast tumor)      ')\g
INSERT INTO Diagnose VALUES ('63223','   Centrally infiltrating neoplasm with large or dense hilus      ')\g
INSERT INTO Diagnose VALUES ('63224','   Neoplasm with cavitation      ')\g
INSERT INTO Diagnose VALUES ('63225','   Linear strands extending from margin or lesion      ')\g
INSERT INTO Diagnose VALUES ('63226','   Calcification      ')\g
INSERT INTO Diagnose VALUES ('63227','   Alveolar infiltrate      ')\g
INSERT INTO Diagnose VALUES ('63228','   Subtle findings, not detected initially      ')\g
INSERT INTO Diagnose VALUES ('63229','   Other      ')\g
INSERT INTO Diagnose VALUES ('6323','    Carcinoma-secondary effects      ')\g
INSERT INTO Diagnose VALUES ('63231','   Obstructive emphysema      ')\g
INSERT INTO Diagnose VALUES ('63232','   Collapse      ')\g
INSERT INTO Diagnose VALUES ('63233','   Pneumonitis or abscess distal to neoplasm      ')\g
INSERT INTO Diagnose VALUES ('63234','   Bone destruction      ')\g
INSERT INTO Diagnose VALUES ('63235','   Lymph node involvement      ')\g
INSERT INTO Diagnose VALUES ('63236','   Paralyzed diaphragm      ')\g
INSERT INTO Diagnose VALUES ('63237','   Pleural involvement      ')\g
INSERT INTO Diagnose VALUES ('63239','   Other    exclude: tuberculosis with carcinoma (.2372)      ')\g
INSERT INTO Diagnose VALUES ('6325','    Other primary malignant neoplasm      ')\g
INSERT INTO Diagnose VALUES ('63251','   Neuroblastoma, ganglioneuroblastoma      ')\g
INSERT INTO Diagnose VALUES ('63252','   Other neurogenic      ')\g
INSERT INTO Diagnose VALUES ('63253','   Other mediastinal      ')\g
INSERT INTO Diagnose VALUES ('63254','   Pleural   include: mesothelioma      ')\g
INSERT INTO Diagnose VALUES ('63255','   Other sarcoma      ')\g
INSERT INTO Diagnose VALUES ('63259','   Other    exclude: teratoma (.3153), malignant bronchial adenoma (.311), malignant thymoma (.3154)      ')\g
INSERT INTO Diagnose VALUES ('6329','    Other   include: neoplasm associated with AIDS      ')\g
INSERT INTO Diagnose VALUES ('633','     MALIGNANT NEOPLASM-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('6331','    Lymphangitic      ')\g
INSERT INTO Diagnose VALUES ('6332','    Solitary pulmonary nodule      ')\g
INSERT INTO Diagnose VALUES ('6333','    Multiple pulmonary nodules      ')\g
INSERT INTO Diagnose VALUES ('6334','    Cavitated      ')\g
INSERT INTO Diagnose VALUES ('6335','    Pleural      ')\g
INSERT INTO Diagnose VALUES ('6336','    Invasion from adjoining neoplasm      ')\g
INSERT INTO Diagnose VALUES ('6337','    Mediastinal      ')\g
INSERT INTO Diagnose VALUES ('6339','    Other   include: metastasis to bronchus, calcified or ossified metastasis      ')\g
INSERT INTO Diagnose VALUES ('634','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('6341','    Leukemia      ')\g
INSERT INTO Diagnose VALUES ('63411','   Adenopathy      ')\g
INSERT INTO Diagnose VALUES ('63412','   Leukemic infiltration of lung      ')\g
INSERT INTO Diagnose VALUES ('63413','   Hemorrhage or infarction (see also effects of radiation .47, opportunistic infection .217)      ')\g
INSERT INTO Diagnose VALUES ('63419','   Other      ')\g
INSERT INTO Diagnose VALUES ('6342','    Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('63421','   Adenopathy      ')\g
INSERT INTO Diagnose VALUES ('63422','   Parenchymal infiltration      ')\g
INSERT INTO Diagnose VALUES ('63423','   Intrapulmonary mass      ')\g
INSERT INTO Diagnose VALUES ('63424','   Cavitation      ')\g
INSERT INTO Diagnose VALUES ('63425','   Endobronchial      ')\g
INSERT INTO Diagnose VALUES ('63426','   Pleural or diaphragmatic      ')\g
INSERT INTO Diagnose VALUES ('63428','   More than one of the above      ')\g
INSERT INTO Diagnose VALUES ('63429','   Other      ')\g
INSERT INTO Diagnose VALUES ('6343','    Non-Hodgkin lymphoma      ')\g
INSERT INTO Diagnose VALUES ('63431','   Lymphocytic well differentiated      ')\g
INSERT INTO Diagnose VALUES ('63432','   Nodular      ')\g
INSERT INTO Diagnose VALUES ('63433','   Diffuse      ')\g
INSERT INTO Diagnose VALUES ('63434','   Histiocytic      ')\g
INSERT INTO Diagnose VALUES ('63435','   Burkitt      ')\g
INSERT INTO Diagnose VALUES ('63439','   Other      ')\g
INSERT INTO Diagnose VALUES ('6345','    Myeloma      ')\g
INSERT INTO Diagnose VALUES ('6346','    Kaposi sarcoma      ')\g
INSERT INTO Diagnose VALUES ('6349','    Other   include: Waldenstrom macroglobulinemia      ')\g
INSERT INTO Diagnose VALUES ('635','     NEOPLASM ARISING IN PREEXISTING BENIGN DISORDER   include: scar carcinoma      ')\g
INSERT INTO Diagnose VALUES ('639','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('6391','    Mass of unknown etiology   include: lymphadenopathy of unknown etiology (e.g., Castleman disease)      ')\g
INSERT INTO Diagnose VALUES ('6399','    Other      ')\g
INSERT INTO Diagnose VALUES ('64','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('641','     PULMONARY AND MEDIASTINAL ALTER-ATIONS FOLLOWING TRAUMA AND SHOCK      ')\g
INSERT INTO Diagnose VALUES ('6411','    Penetrating injury (see also traumatic pneumothorax .732, traumatic pneumomediastinum .735)      ')\g
INSERT INTO Diagnose VALUES ('6412','    Nonpenetrating thoracic trauma      ')\g
INSERT INTO Diagnose VALUES ('64121','   Post-traumatic pulmonary edema      ')\g
INSERT INTO Diagnose VALUES ('64122','   Pulmonary contusion      ')\g
INSERT INTO Diagnose VALUES ('64123','   Intrapulmonary hemorrhage      ')\g
INSERT INTO Diagnose VALUES ('64124','   Pulmonary laceration, traumatic lung cyst, pulmonary hematoma, fracture of bronchus, fracture of trachea      ')\g
INSERT INTO Diagnose VALUES ('64125','   Hemothorax      ')\g
INSERT INTO Diagnose VALUES ('64126','   Traumatic pneumonitis      ')\g
INSERT INTO Diagnose VALUES ('64127','   Post-traumatic atelectasis      ')\g
INSERT INTO Diagnose VALUES ('64128','   Mediastinal hemorrhage, hematoma      ')\g
INSERT INTO Diagnose VALUES ('64129','   Other (see also traumatic pneumothorax .732, traumatic pneumomediastinum .735)      ')\g
INSERT INTO Diagnose VALUES ('6413','    Adult respiratory distress syndrome (shock lung")"      ')\g
INSERT INTO Diagnose VALUES ('64131','   Associated with shock      ')\g
INSERT INTO Diagnose VALUES ('64132','   Respirator therapy      ')\g
INSERT INTO Diagnose VALUES ('64133','   Oxygen toxicity      ')\g
INSERT INTO Diagnose VALUES ('64134','   Associated with infection      ')\g
INSERT INTO Diagnose VALUES ('64138','   Multiple factors      ')\g
INSERT INTO Diagnose VALUES ('64139','   Other    exclude: fat embolus (.724)      ')\g
INSERT INTO Diagnose VALUES ('6419','    Other      ')\g
INSERT INTO Diagnose VALUES ('643','     COMPLICATIONS OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('6431','    Traumatic aneurysm (see also 5.732)      ')\g
INSERT INTO Diagnose VALUES ('6432','    Traumatic hernia of lung      ')\g
INSERT INTO Diagnose VALUES ('6439','    Other    exclude: arteriovenous fistula, post-traumatic (.49), paralyzed diaphragm (.455 or 7.155)      ')\g
INSERT INTO Diagnose VALUES ('645','     POSTOPERATIVE (see also pneumomediastinum, pneumothorax .73, collapse [atelectasis] .74)      ')\g
INSERT INTO Diagnose VALUES ('6451','    Thoracoplasty      ')\g
INSERT INTO Diagnose VALUES ('6452','    Pneumonectomy      ')\g
INSERT INTO Diagnose VALUES ('6453','    Lobectomy   include: wedge resection      ')\g
INSERT INTO Diagnose VALUES ('6454','    Thoracotomy      ')\g
INSERT INTO Diagnose VALUES ('6455','    Phrenic nerve crush or section      ')\g
INSERT INTO Diagnose VALUES ('6456','    Tracheostomy      ')\g
INSERT INTO Diagnose VALUES ('6457','    Endotracheal tube (see 2.457)      ')\g
INSERT INTO Diagnose VALUES ('6458','    Complication of surgery or interventional procedure    include: of aspiration, of biopsy, of catheter or other tube, granuloma or stenosis com-plicating tracheotomy or endotracheal tube (see 2.458)   exclude: of angiography (9.44)      ')\g
INSERT INTO Diagnose VALUES ('6459','    Other     include: decortication, oleothorax, plombage, sympathectomy, ECMO (extracorporeal membrane oxygenation), tube track      ')\g
INSERT INTO Diagnose VALUES ('646','     FOREIGN BODY OR MEDICATION (for 5th number see box following .469)      ')\g
INSERT INTO Diagnose VALUES ('6461','    Opaque (see .743, .7521)      ')\g
INSERT INTO Diagnose VALUES ('64611','   Catheter or other tube    include: Swan Ganz catheter, central venous catheter      ')\g
INSERT INTO Diagnose VALUES ('64612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('64613','   Catheter (or other tube) in unsatisfac-tory position      ')\g
INSERT INTO Diagnose VALUES ('64619','   Other   include: barium aspiration   exclude: pulmonary oil emboli following lymphangiography (.7251)      ')\g
INSERT INTO Diagnose VALUES ('6462','    Nonopaque      ')\g
INSERT INTO Diagnose VALUES ('6469','    Other      ')\g
INSERT INTO Diagnose VALUES ('647','     EFFECT OF RADIATION   include: radiation pneumonitis, fibrosis      ')\g
INSERT INTO Diagnose VALUES ('649','     OTHER   include: arteriovenous fistula, post-traumatic      ')\g
INSERT INTO Diagnose VALUES ('65','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('651','     PULMONARY MANIFESTATION OF META-BOLIC OR ENDOCRINE DISTURBANCE      ')\g
INSERT INTO Diagnose VALUES ('6511','    Metastatic calcification in lung      ')\g
INSERT INTO Diagnose VALUES ('6512','    Mediastinal or subpleural fat deposition from steroid administration      ')\g
INSERT INTO Diagnose VALUES ('6519','    Other      ')\g
INSERT INTO Diagnose VALUES ('652','     RESPONSE TO INHALATION OF NOXIOUS GAS      ')\g
INSERT INTO Diagnose VALUES ('6521','    Silo-filler�s disease, nitrogen dioxide toxicity      ')\g
INSERT INTO Diagnose VALUES ('6522','    Sulfur dioxide inhalation      ')\g
INSERT INTO Diagnose VALUES ('6523','    Chlorine, phosgene, other noxious gases      ')\g
INSERT INTO Diagnose VALUES ('6529','    Other      ')\g
INSERT INTO Diagnose VALUES ('653','     RESPONSE TO INHALATION OF AEROSOL   include: thesaurosis      ')\g
INSERT INTO Diagnose VALUES ('654','     RESPONSE TO INHALATION OF METAL FUME OR VAPOR   include: Shaver�s disease (fumes of alumina and silica)      ')\g
INSERT INTO Diagnose VALUES ('655','     RESPONSE TO OTHER INHALED AGENT (see also .635)      ')\g
INSERT INTO Diagnose VALUES ('6551','    Farmer�s lung      ')\g
INSERT INTO Diagnose VALUES ('6552','    Bagassosis      ')\g
INSERT INTO Diagnose VALUES ('6553','    Byssinosis      ')\g
INSERT INTO Diagnose VALUES ('6559','    Other   include: mushroom-worker�s lung   exclude: pigeon-breeder�s disease (.635)      ')\g
INSERT INTO Diagnose VALUES ('656','     HYDROCARBON PNEUMONIA      ')\g
INSERT INTO Diagnose VALUES ('657','     RESPONSE TO INGESTION    exclude: hydrocarbon (.56), drug (.64)      ')\g
INSERT INTO Diagnose VALUES ('659','     OTHER    exclude: pulmonary complication of drug therapy (.64)      ')\g
INSERT INTO Diagnose VALUES ('66','      OTHER GENERALIZED SYSTEMIC DISORDER      ')\g
INSERT INTO Diagnose VALUES ('661','     SYSTEMIC CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('6611','    Rheumatoid disease      ')\g
INSERT INTO Diagnose VALUES ('66111','   Pleuritis, pleural effusion      ')\g
INSERT INTO Diagnose VALUES ('66112','   Rheumatoid nodule   include: cavitated nodule      ')\g
INSERT INTO Diagnose VALUES ('66113','   Interstitial fibrosis      ')\g
INSERT INTO Diagnose VALUES ('66114','   Caplan syndrome      ')\g
INSERT INTO Diagnose VALUES ('66119','   Other      ')\g
INSERT INTO Diagnose VALUES ('6612','    Lupus erythematosus      ')\g
INSERT INTO Diagnose VALUES ('66121','   Pleuritis, pleural effusion      ')\g
INSERT INTO Diagnose VALUES ('66122','   Parenchymal infiltrate      ')\g
INSERT INTO Diagnose VALUES ('66123','   Pulmonary vasculitis      ')\g
INSERT INTO Diagnose VALUES ('66129','   Other      ')\g
INSERT INTO Diagnose VALUES ('6613','    Progressive systemic sclerosis (scleroderma)      ')\g
INSERT INTO Diagnose VALUES ('6614','    Dermatomyositis, polymyositis      ')\g
INSERT INTO Diagnose VALUES ('6616','    Combined collagen disease      ')\g
INSERT INTO Diagnose VALUES ('6619','    Other      ')\g
INSERT INTO Diagnose VALUES ('662','     VASCULITIS      ')\g
INSERT INTO Diagnose VALUES ('6621','    Polyarteritis (periarteritis) nodosa      ')\g
INSERT INTO Diagnose VALUES ('6622','    Wegener granulomatosis    include: midline lethal granuloma      ')\g
INSERT INTO Diagnose VALUES ('6623','    Allergic granulomatous angiitis (Churg and Strauss)      ')\g
INSERT INTO Diagnose VALUES ('6624','    Hypersenitivity angiitis      ')\g
INSERT INTO Diagnose VALUES ('6628','    Hemolytic uremic syndrome      ')\g
INSERT INTO Diagnose VALUES ('6629','    Other    include: mucocutaneous lymph node syndrome    exclude: Goodpasture syndrome (.691), idiopathic pulmonary hemosiderosis (.692)      ')\g
INSERT INTO Diagnose VALUES ('663','     PULMONARY MANIFESTATION OF ALLERGIC STATE      ')\g
INSERT INTO Diagnose VALUES ('6631','    Loffler syndrome     include: visceral larva migrans      ')\g
INSERT INTO Diagnose VALUES ('6632','    Pulmonary infiltration with eosinophilia (PIE syndrome), eosinophilic pneumonia      ')\g
INSERT INTO Diagnose VALUES ('6633','    Tropical pulmonary eosinophilia      ')\g
INSERT INTO Diagnose VALUES ('6634','    Allergic bronchopulmonary aspergillosis   include: associated mucoid impaction (see also .2056)      ')\g
INSERT INTO Diagnose VALUES ('6635','    Extrinsic allergic alveolitis (inhalation)   include: bird sensitivity, pigeon-breeder disease      ')\g
INSERT INTO Diagnose VALUES ('6636','    Bronchocentric granulomatosis      ')\g
INSERT INTO Diagnose VALUES ('6639','    Other    include: milk allergy    exclude: asthma (.754), bronchiolitis (.2191)      ')\g
INSERT INTO Diagnose VALUES ('664','     COMPLICATION OF DRUG THERAPY, DRUG USE      ')\g
INSERT INTO Diagnose VALUES ('6641','    Antibiotics      ')\g
INSERT INTO Diagnose VALUES ('66411','   Nitrofurantoin (Furadantin)      ')\g
INSERT INTO Diagnose VALUES ('66412','   Sulfa drug      ')\g
INSERT INTO Diagnose VALUES ('66413','   Para-Aminosalicylic acid (PAS)      ')\g
INSERT INTO Diagnose VALUES ('66419','   Other      ')\g
INSERT INTO Diagnose VALUES ('6642','    Antihypertensive agent      ')\g
INSERT INTO Diagnose VALUES ('6643','    Antimetabolite      ')\g
INSERT INTO Diagnose VALUES ('66431','   Methotrexate      ')\g
INSERT INTO Diagnose VALUES ('66432','   Busulfan (Myleran)      ')\g
INSERT INTO Diagnose VALUES ('66433','   Bleomycin      ')\g
INSERT INTO Diagnose VALUES ('66434','   Cyclophosphamide      ')\g
INSERT INTO Diagnose VALUES ('66439','   Other      ')\g
INSERT INTO Diagnose VALUES ('6644','    Narcotic    include: heroin      ')\g
INSERT INTO Diagnose VALUES ('6645','    Phenytoin (Dilantin)      ')\g
INSERT INTO Diagnose VALUES ('6646','    Antiarrhythmics      ')\g
INSERT INTO Diagnose VALUES ('66461','   Procainamide (Pronestyl)      ')\g
INSERT INTO Diagnose VALUES ('66462','   Amiodarone      ')\g
INSERT INTO Diagnose VALUES ('6647','    Methysergide (Sansert)      ')\g
INSERT INTO Diagnose VALUES ('6649','    Other    exclude: mediastinal or subpleural lipid deposition from steroid administration (.512)      ')\g
INSERT INTO Diagnose VALUES ('665','     HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('6651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('6658','    Disseminated intravascular coagulation (see also adult respiratory distress syndrome 413, fat embolism .724)      ')\g
INSERT INTO Diagnose VALUES ('6659','    Other    include: extramedullary hematopoiesis      ')\g
INSERT INTO Diagnose VALUES ('666','     HISTIOCYTOSIS (LANGERHANS CELL HISTIOCYTOSIS)      ')\g
INSERT INTO Diagnose VALUES ('667','     SPHINGOLIPIDOSIS    include: Gaucher disease, Niemann-Pick disease      ')\g
INSERT INTO Diagnose VALUES ('668','     OTHER INFILTRATIVE DISORDER    include: amyloid      ')\g
INSERT INTO Diagnose VALUES ('669','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('6691','    Goodpasture syndrome      ')\g
INSERT INTO Diagnose VALUES ('6692','    Idiopathic pulmonary hemosiderosis      ')\g
INSERT INTO Diagnose VALUES ('6693','    Postcardiotomy syndrome    include: pleural reaction and pulmonary infiltrate following thoracotomy      ')\g
INSERT INTO Diagnose VALUES ('6694','    Rheumatic fever pneumonia      ')\g
INSERT INTO Diagnose VALUES ('6695','    Sjogren syndrome      ')\g
INSERT INTO Diagnose VALUES ('6697','    Nephritis      ')\g
INSERT INTO Diagnose VALUES ('6698','    Nephrosis      ')\g
INSERT INTO Diagnose VALUES ('6699','    Other    include: graft-versus-host reaction      ')\g
INSERT INTO Diagnose VALUES ('67','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('671','     PULMONARY EDEMA, LUNG CHANGES WITH HEART FAILURE (see also 5.71)      ')\g
INSERT INTO Diagnose VALUES ('6711','    Noncardiac pulmonary edema      ')\g
INSERT INTO Diagnose VALUES ('67111','   Azotemia, uremic lung      ')\g
INSERT INTO Diagnose VALUES ('67112','   Neurogenic    include: with convulsion, intracranial hemorrhage, central nervous system trauma or depression      ')\g
INSERT INTO Diagnose VALUES ('67113','   Circulatory overload   include: excess intravenous fluid      ')\g
INSERT INTO Diagnose VALUES ('67114','   Smoke inhalation      ')\g
INSERT INTO Diagnose VALUES ('67115','   Near drowning      ')\g
INSERT INTO Diagnose VALUES ('67116','   Transfusion reaction      ')\g
INSERT INTO Diagnose VALUES ('67121','   Prominent upper lobe vessels      ')\g
INSERT INTO Diagnose VALUES ('67122','   Pulmonary edema      ')\g
INSERT INTO Diagnose VALUES ('67129','   Other      ')\g
INSERT INTO Diagnose VALUES ('6713','    Lung changes with mitral stenosis      ')\g
INSERT INTO Diagnose VALUES ('67131','   Prominent upper lobe vessels      ')\g
INSERT INTO Diagnose VALUES ('67132','   Pulmonary edema      ')\g
INSERT INTO Diagnose VALUES ('67133','   Hemosiderosis      ')\g
INSERT INTO Diagnose VALUES ('67139','   Other   exclude: pulmonary calcification, ossification (.815)      ')\g
INSERT INTO Diagnose VALUES ('6719','    Other      ')\g
INSERT INTO Diagnose VALUES ('672','     INFARCT, EMBOLUS, THROMBUS      ')\g
INSERT INTO Diagnose VALUES ('6721','    Pulmonary embolization without infarction      ')\g
INSERT INTO Diagnose VALUES ('67211','   Normal chest roentgenogram      ')\g
INSERT INTO Diagnose VALUES ('67212','   Prominent main pulmonary artery      ')\g
INSERT INTO Diagnose VALUES ('67213','   Diminished pulmonary vascular markings (Westermark sign)      ')\g
INSERT INTO Diagnose VALUES ('67214','   Localized hemorrhagic edema      ')\g
INSERT INTO Diagnose VALUES ('67219','   Other      ')\g
INSERT INTO Diagnose VALUES ('6722','    Pulmonary infarction      ')\g
INSERT INTO Diagnose VALUES ('67221','   Hampton hump      ')\g
INSERT INTO Diagnose VALUES ('67222','   Pleural reaction      ')\g
INSERT INTO Diagnose VALUES ('67223','   Fleischner lines, patchy atelectasis      ')\g
INSERT INTO Diagnose VALUES ('67224','   Segmental infiltrate, hemorrhagic edema      ')\g
INSERT INTO Diagnose VALUES ('67225','   Cavitation      ')\g
INSERT INTO Diagnose VALUES ('67229','   Other    exclude: septic infarction (.2163)      ')\g
INSERT INTO Diagnose VALUES ('6723','    Main pulmonary artery thrombosis      ')\g
INSERT INTO Diagnose VALUES ('6724','    Fat embolism (see also adult respiratory distress syndrome .413, disseminated intra-vascular coagulation .658)      ')\g
INSERT INTO Diagnose VALUES ('6725','    Iatrogenic embolization    include: contrast material      ')\g
INSERT INTO Diagnose VALUES ('67251','   Following lymphangiography      ')\g
INSERT INTO Diagnose VALUES ('67252','   Foreign body    include: venous catheter      ')\g
INSERT INTO Diagnose VALUES ('67259','   Other      ')\g
INSERT INTO Diagnose VALUES ('6729','    Other   exclude: septic infarct (.216-3)      ')\g
INSERT INTO Diagnose VALUES ('673','     PNEUMOMEDIASTINUM, PNEUMOTHORAX (also code .76 if pleural fluid is present)   exclude: in newborn (for 4th number see box after .789)      ')\g
INSERT INTO Diagnose VALUES ('6731','    Spontaneous pneumothorax      ')\g
INSERT INTO Diagnose VALUES ('6732','    Traumatic pneumothorax      ')\g
INSERT INTO Diagnose VALUES ('6733','    Tension pneumothorax      ')\g
INSERT INTO Diagnose VALUES ('6734','    Pneumothorax secondary to lung disease   include: histiocytosis, sarcoidosis      ')\g
INSERT INTO Diagnose VALUES ('6735','    Pneumomediastinum, traumatic or spontaneous   exclude: diagnostic (.1297), pneumopericardium (5.491), subcutaneous (4.493), with asthma (.7541)      ')\g
INSERT INTO Diagnose VALUES ('6736','    Interstitial pulmonary emphysema      ')\g
INSERT INTO Diagnose VALUES ('6739','    Other   include: air embolism      ')\g
INSERT INTO Diagnose VALUES ('674','     COLLAPSE (ATELECTASIS)      ')\g
INSERT INTO Diagnose VALUES ('6741','    Bronchial stricture    include: middle lobe syndrome   exclude: broncholith (.811)      ')\g
INSERT INTO Diagnose VALUES ('6742','    Bronchiectasis (see also .26)      ')\g
INSERT INTO Diagnose VALUES ('6743','    Foreign body      ')\g
INSERT INTO Diagnose VALUES ('6744','    Postoperative    exclude: post-traumatic atelectasis (.4127)      ')\g
INSERT INTO Diagnose VALUES ('6745','    Extrinsic compression      ')\g
INSERT INTO Diagnose VALUES ('6746','    Discoid atelectasis (linear, plate-like)      ')\g
INSERT INTO Diagnose VALUES ('6747','    Collapse associated with pneumonia,asthma      ')\g
INSERT INTO Diagnose VALUES ('6748','    Etiology undetermined      ')\g
INSERT INTO Diagnose VALUES ('6749','    Other    include: round atelectasis   exclude: neoplasm (.3232), tuberculosis (.233), mucoid impaction (.265), allergic bronchopulmonary aspergillosis (.634), with asthma (.7545)      ')\g
INSERT INTO Diagnose VALUES ('675','     HYPERINFLATION, PULMONARY EMPHY-SEMA, BRONCHIAL ASTHMA, CHRONIC BRONCHITIS      ')\g
INSERT INTO Diagnose VALUES ('6751','    Pulmonary emphysema      ')\g
INSERT INTO Diagnose VALUES ('67511','   Alpha-1-antitrypsin deficiency      ')\g
INSERT INTO Diagnose VALUES ('67512','   Panacinar      ')\g
INSERT INTO Diagnose VALUES ('67513','   Centriacinar      ')\g
INSERT INTO Diagnose VALUES ('67514','   Paraseptal      ')\g
INSERT INTO Diagnose VALUES ('67519','   Other      ')\g
INSERT INTO Diagnose VALUES ('6752','    Hyperinflation due to physical obstruction of the airway      ')\g
INSERT INTO Diagnose VALUES ('67521','   Foreign body      ')\g
INSERT INTO Diagnose VALUES ('67522','   Obstruction of larynx or trachea      ')\g
INSERT INTO Diagnose VALUES ('67523','   Pulmonary sling (aberrant left pulmo-nary artery compressing right bron-chus) (see 5.1543)      ')\g
INSERT INTO Diagnose VALUES ('67529','   Other      ')\g
INSERT INTO Diagnose VALUES ('6753','    Other form of hyperinflation      ')\g
INSERT INTO Diagnose VALUES ('67531','   Congenital lobar emphysema      ')\g
INSERT INTO Diagnose VALUES ('67532','   Unilateral hyperlucent lung (Swyer-James syndrome)      ')\g
INSERT INTO Diagnose VALUES ('67533','   Compensatory emphysema    include: lung herniation      ')\g
INSERT INTO Diagnose VALUES ('67534','   Hyperventilation (e.g., aspirin intoxica-tion)      ')\g
INSERT INTO Diagnose VALUES ('67539','   Other      ')\g
INSERT INTO Diagnose VALUES ('6754','    Bronchial asthma      ')\g
INSERT INTO Diagnose VALUES ('67541','   With pneumomediastinum      ')\g
INSERT INTO Diagnose VALUES ('67542','   With mucoid impaction (see also .265, .7545)      ')\g
INSERT INTO Diagnose VALUES ('67543','   With allergic pneumonia    exclude: PIE syndrome (.632)      ')\g
INSERT INTO Diagnose VALUES ('67544','   With recurrent pneumonia (see also .218)      ')\g
INSERT INTO Diagnose VALUES ('67545','   With recurrent atelectasis (see also .7542)      ')\g
INSERT INTO Diagnose VALUES ('67549','   Other      ')\g
INSERT INTO Diagnose VALUES ('6755','    Chronic bronchitis      ')\g
INSERT INTO Diagnose VALUES ('6756','    Bulla, single or multiple      ')\g
INSERT INTO Diagnose VALUES ('6759','    Other    exclude: bronchiolitis (.2191), pneumatocele (.3122)      ')\g
INSERT INTO Diagnose VALUES ('676','     HYDROTHORAX, EMPYEMA, PLEURAL THICKENING (also code .73 if pneumothorax is associated) (for 4th number see box following .769)      ')\g
INSERT INTO Diagnose VALUES ('6761','    Free fluid      ')\g
INSERT INTO Diagnose VALUES ('6762','    Subpulmonary fluid (see also phantom tumor .3185)      ')\g
INSERT INTO Diagnose VALUES ('6763','    Interlobar fluid      ')\g
INSERT INTO Diagnose VALUES ('6764','    Other localized collection   include: encapsulated effusion      ')\g
INSERT INTO Diagnose VALUES ('6765','    Acute pleuritis without effusion      ')\g
INSERT INTO Diagnose VALUES ('6766','    Calcified pleura      ')\g
INSERT INTO Diagnose VALUES ('6767','    Pleural or pleuropericardial adhesion, old pleural thickening      ')\g
INSERT INTO Diagnose VALUES ('6768','    Extrapleural fluid      ')\g
INSERT INTO Diagnose VALUES ('6769','    Other    include: chylothorax, fibrin body, Meigs syndrome, postpericardiotomy syndrome (see 5.85)   exclude: pleural mesothelioma, fibroma (.317, .3254), tuberculosis (.234)      ')\g
INSERT INTO Diagnose VALUES ('6771','    Silicosis      ')\g
INSERT INTO Diagnose VALUES ('6772','    Coal worker pneumoconiosis      ')\g
INSERT INTO Diagnose VALUES ('6773','    Asbestosis without associated lung or pleural neoplasm      ')\g
INSERT INTO Diagnose VALUES ('6774','    Asbestosis with associated lung or pleural neoplasm      ')\g
INSERT INTO Diagnose VALUES ('6775','    Talcosis      ')\g
INSERT INTO Diagnose VALUES ('6776','    Siderosis      ')\g
INSERT INTO Diagnose VALUES ('6777','    Berylliosis      ')\g
INSERT INTO Diagnose VALUES ('6779','    Other   include: graphite, mica, kaolin, radiopaque dusts      ')\g
INSERT INTO Diagnose VALUES ('678','     NEONATAL RESPIRATORY ABNORMALITY (for 4th number see box following .789)      ')\g
INSERT INTO Diagnose VALUES ('6781','    Respiratory distress syndrome (hyaline membrane disease)      ')\g
INSERT INTO Diagnose VALUES ('67811','   Treated with surfactant      ')\g
INSERT INTO Diagnose VALUES ('67812','   Not treated with surfactant      ')\g
INSERT INTO Diagnose VALUES ('6782','    Meconium aspiration, amniotic fluid aspiration      ')\g
INSERT INTO Diagnose VALUES ('6783','    Bronchopulmonary dysplasia, oxygen toxicity      ')\g
INSERT INTO Diagnose VALUES ('6784','    Bubbly lung," other cause    include: Wilson-Mikity syndrome"      ')\g
INSERT INTO Diagnose VALUES ('6785','    Transient pulmonary edema (transient tachypnea) of newborn      ')\g
INSERT INTO Diagnose VALUES ('6786','    Congenital pneumonia      ')\g
INSERT INTO Diagnose VALUES ('6787','    Microatelectasis   exclude: respiratory distress syndrome (.781)      ')\g
INSERT INTO Diagnose VALUES ('6789','    Other      ')\g
INSERT INTO Diagnose VALUES ('679','     DISORDER OF UNKNOWN OR OBSCURE ETIOLOGY      ')\g
INSERT INTO Diagnose VALUES ('6791','    Pulmonary alveolar proteinosis      ')\g
INSERT INTO Diagnose VALUES ('6792','    Idiopathic interstitial fibrosis      ')\g
INSERT INTO Diagnose VALUES ('67921','   Acute or subacute      ')\g
INSERT INTO Diagnose VALUES ('67922','   Chronic      ')\g
INSERT INTO Diagnose VALUES ('6793','    Desquamative interstitial pneumonia      ')\g
INSERT INTO Diagnose VALUES ('68','      MISCELLANEOUS, OTHER      ')\g
INSERT INTO Diagnose VALUES ('681','     CALCIFICATION, OSSIFICATION      ')\g
INSERT INTO Diagnose VALUES ('6811','    Broncholith      ')\g
INSERT INTO Diagnose VALUES ('6812','    Calcification in neoplasm    include: metastatic   exclude: with bronchogenic carcinoma (.3266)      ')\g
INSERT INTO Diagnose VALUES ('6813','    Pulmonary alveolar microlithiasis      ')\g
INSERT INTO Diagnose VALUES ('6814','    Tracheopathic osteoplastica   include: without calcification      ')\g
INSERT INTO Diagnose VALUES ('6815','    Pulmonary calcification, ossification secondary to mitral stenosis      ')\g
INSERT INTO Diagnose VALUES ('6816','    Pulmonary calcification, ossification secondary to renal disease      ')\g
INSERT INTO Diagnose VALUES ('6819','    Other    exclude: calcified pleura (.766), metastatic calcification (.511) with pneumoconiosis (.77-7) and calcification in: tuberculosis (.2321), sarcoid (.225), primary carcinoma (.3226), pulmonary nodule (.2811), metastases (.339)      ')\g
INSERT INTO Diagnose VALUES ('689','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('69','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('691','     FUNDAMENTAL OBSERVATION (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('6911','    Air bronchogram      ')\g
INSERT INTO Diagnose VALUES ('6912','    Kerley lines      ')\g
INSERT INTO Diagnose VALUES ('6913','    Hilum overlay sign      ')\g
INSERT INTO Diagnose VALUES ('6914','    Silhouette sign      ')\g
INSERT INTO Diagnose VALUES ('6915','    Extrapleural sign      ')\g
INSERT INTO Diagnose VALUES ('6916','    Alveolar disease      ')\g
INSERT INTO Diagnose VALUES ('6917','    Interstitial disease      ')\g
INSERT INTO Diagnose VALUES ('6918','    Pulmonary oligemia      ')\g
INSERT INTO Diagnose VALUES ('6919','    Other   include: reduced pulmonary compliance      ')\g
INSERT INTO Diagnose VALUES ('692','     ANATOMICAL DETAIL      ')\g
INSERT INTO Diagnose VALUES ('693','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('694','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('699','     OTHER   include: phenomenon related to technique of examination      ')\g
INSERT INTO Diagnose VALUES ('71','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('711','     NORMAL ROUTINE PLAIN FILM   exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('712','     SPECIAL TECHNIQUE, PROCEDURE, PROJEC-TION      ')\g
INSERT INTO Diagnose VALUES ('7121','    Digital radiographic techniques, tomography, MR, nuclear medicine      ')\g
INSERT INTO Diagnose VALUES ('71211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('712111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('712112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('712113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('712114','  Delayed scanning following enhancement (e.g., for detection of liver lesions)      ')\g
INSERT INTO Diagnose VALUES ('712115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('712116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('712117','  Three-dimensional reconstruc-tion      ')\g
INSERT INTO Diagnose VALUES ('712118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('712119','  Other, including serial enhance-ment studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('71214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('712141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('7121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('7121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('7121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('7121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('7121415',' Specific resonance suppressed  include: fat  suppression, water  suppression      ')\g
INSERT INTO Diagnose VALUES ('7121416',' High-speed imaging  include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('7121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('7121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('712142','  MR angiography (morphologi-cal vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('712143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('712144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('712145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('712146','  Quantitative tissue characteriza-tion, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('712147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('712149','  Other      ')\g
INSERT INTO Diagnose VALUES ('71215','   Digital radiography    exclude: with angiography  (.124-3, 95.12-3)      ')\g
INSERT INTO Diagnose VALUES ('71216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('712161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('712162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('712163','  PET      ')\g
INSERT INTO Diagnose VALUES ('712164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('712165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('712166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('712167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('712168','  Therapeutic procedure    include: use of monoclonal  antibodies      ')\g
INSERT INTO Diagnose VALUES ('712169','  Other.  See also 1.12178 Receptor-avid radiopharma-ceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('71217','   Nuclear medicine study-gastrointesti-nal      ')\g
INSERT INTO Diagnose VALUES ('712171','  Liver-spleen imaging      ')\g
INSERT INTO Diagnose VALUES ('712172','  Hepatobiliary study (e.g., 99mTc-disofenin)      ')\g
INSERT INTO Diagnose VALUES ('712173','  Metabolism study      ')\g
INSERT INTO Diagnose VALUES ('712174','  Detection of ectopic gastric mucosa (e.g., 99mTc-pertechnetate)      ')\g
INSERT INTO Diagnose VALUES ('712175','  Esophageal motility imaging      ')\g
INSERT INTO Diagnose VALUES ('712176','  Gastroesophageal reflux study  include: detection of pulmo-nary aspiration      ')\g
INSERT INTO Diagnose VALUES ('712177','  Gastric emptying study      ')\g
INSERT INTO Diagnose VALUES ('712178','  Gastrointestinal bleeding study      ')\g
INSERT INTO Diagnose VALUES ('712179','  Other   include: splenic seques-tration study, spleen imaging with labeled heat-damaged red blood cells      ')\g
INSERT INTO Diagnose VALUES ('71218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('71219','   Other      ')\g
INSERT INTO Diagnose VALUES ('7122','    Biliary system      ')\g
INSERT INTO Diagnose VALUES ('71221','   Oral cholecystography      ')\g
INSERT INTO Diagnose VALUES ('71222','   ERCP-Endoscopic retrograde cholangiopancreatography      ')\g
INSERT INTO Diagnose VALUES ('71223','   ESWL-Biliary-Extracorporeal shock wave lithrotripsy      ')\g
INSERT INTO Diagnose VALUES ('71233','   Swallowing function study (see 2.123)      ')\g
INSERT INTO Diagnose VALUES ('71224','   Operative cholangiography      ')\g
INSERT INTO Diagnose VALUES ('71225','   Postoperative cholangiography      ')\g
INSERT INTO Diagnose VALUES ('71226','   Percutaneous cholangiography      ')\g
INSERT INTO Diagnose VALUES ('71227','   Transduodenal cholangiography      ')\g
INSERT INTO Diagnose VALUES ('71228','   Stone extraction, duct dilatation      ')\g
INSERT INTO Diagnose VALUES ('71229','   Other  include: use of pharmacological agent      ')\g
INSERT INTO Diagnose VALUES ('7123','    Upper GI series, esophagram      ')\g
INSERT INTO Diagnose VALUES ('71231','   Routine      ')\g
INSERT INTO Diagnose VALUES ('71232','   Special contrast medium  include: water-soluble agent      ')\g
INSERT INTO Diagnose VALUES ('71234','   Hypotonic duodenography      ')\g
INSERT INTO Diagnose VALUES ('71239','   Other  include: use of pharmacological agent      ')\g
INSERT INTO Diagnose VALUES ('7124','    Angiography (see also 95.)      ')\g
INSERT INTO Diagnose VALUES ('71241','   Celiac arteriography      ')\g
INSERT INTO Diagnose VALUES ('71242','   Hepatic arteriography      ')\g
INSERT INTO Diagnose VALUES ('71243','   Left gastric, inferior phrenic arteriography      ')\g
INSERT INTO Diagnose VALUES ('71244','   Splenic arteriography      ')\g
INSERT INTO Diagnose VALUES ('71245','   Superior mesenteric arteriography      ')\g
INSERT INTO Diagnose VALUES ('71246','   Inferior mesenteric arteriography      ')\g
INSERT INTO Diagnose VALUES ('71247','   Splenoportography      ')\g
INSERT INTO Diagnose VALUES ('71248','   More than one of the above, generalized      ')\g
INSERT INTO Diagnose VALUES ('71249','   Other      ')\g
INSERT INTO Diagnose VALUES ('7125','    Special nonroutine projection      ')\g
INSERT INTO Diagnose VALUES ('7126','    Biopsy-Interventional      ')\g
INSERT INTO Diagnose VALUES ('71261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('71262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('71263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('71264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('71265','   Thrombolytic infusion      ')\g
INSERT INTO Diagnose VALUES ('71266','   Chemotherapeutic infusion      ')\g
INSERT INTO Diagnose VALUES ('71267','   Operative procedure      ')\g
INSERT INTO Diagnose VALUES ('71269','   Other      ')\g
INSERT INTO Diagnose VALUES ('7127','    Small intestine      ')\g
INSERT INTO Diagnose VALUES ('71271','   Routine      ')\g
INSERT INTO Diagnose VALUES ('71272','   Intubation      ')\g
INSERT INTO Diagnose VALUES ('71273','   Special contrast medium      ')\g
INSERT INTO Diagnose VALUES ('71274','   Lactose study      ')\g
INSERT INTO Diagnose VALUES ('71275','   Retrograde or reflux study      ')\g
INSERT INTO Diagnose VALUES ('71279','   Other      ')\g
INSERT INTO Diagnose VALUES ('7128','    Colon      ')\g
INSERT INTO Diagnose VALUES ('71281','   Routine      ')\g
INSERT INTO Diagnose VALUES ('71282','   Air contrast      ')\g
INSERT INTO Diagnose VALUES ('71283','   Special contrast medium  include: water-soluble agent      ')\g
INSERT INTO Diagnose VALUES ('71288','   Defecography      ')\g
INSERT INTO Diagnose VALUES ('71289','   Other   include: use of pharmacological agent      ')\g
INSERT INTO Diagnose VALUES ('7129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('71291','   Therapy localization (port film)      ')\g
INSERT INTO Diagnose VALUES ('71296','   Cine, video study      ')\g
INSERT INTO Diagnose VALUES ('71297','   Peritoneography, parietography, herniography   include: air, positive contrast      ')\g
INSERT INTO Diagnose VALUES ('71298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('712981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('712982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('712983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('712984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('712985','  Guided procedure for diagnosis   include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('712986','  Guided procedure for therapy    include: drainage      ')\g
INSERT INTO Diagnose VALUES ('712987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('712988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('712989','  Special nonroutine projection, mode, or transducer type      ')\g
INSERT INTO Diagnose VALUES ('71299','   Other   include: xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('713','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('7131','    Unusual motor activity   include: tertiary contractions, pylorospasm      ')\g
INSERT INTO Diagnose VALUES ('7132','    Cascade or other variation of stomach      ')\g
INSERT INTO Diagnose VALUES ('7133','    Prolapse (gastric, ileal) (see .159)      ')\g
INSERT INTO Diagnose VALUES ('7134','    Unusual size, or shape of abdominal viscus, unusual course of bowel   include: absent right or left lobe of liver spleen: bilobed, accessory, prominent clefts    exclude: malrotation (.146)      ')\g
INSERT INTO Diagnose VALUES ('7135','    Prominent ileocecal valve, fat infiltration of valve      ')\g
INSERT INTO Diagnose VALUES ('7136','    Hepatodiaphragmatic interposition      ')\g
INSERT INTO Diagnose VALUES ('7137','    Phrygian cap or other variation of gall-bladder configuration      ')\g
INSERT INTO Diagnose VALUES ('7138','    Prominent mucosal fold      ')\g
INSERT INTO Diagnose VALUES ('7139','    Other    include: Riedel lobe, retrocecal appendix      ')\g
INSERT INTO Diagnose VALUES ('714','     CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('7142','    Congenital esophageal abnormality      ')\g
INSERT INTO Diagnose VALUES ('71421','   Esophageal atresia without tracheoesophageal fistula      ')\g
INSERT INTO Diagnose VALUES ('71422','   Esophageal atresia with tracheoesophageal fistula      ')\g
INSERT INTO Diagnose VALUES ('71423','   Tracheoesophageal fistula without esphageal atresia (H-type)      ')\g
INSERT INTO Diagnose VALUES ('71429','   Other     Include: web     exclude: acquired fistula (6.240), columnar lined esophagus (.291)      ')\g
INSERT INTO Diagnose VALUES ('7143','    Stenosis, atresia      ')\g
INSERT INTO Diagnose VALUES ('71431','   Hypertrophic pyloric stenosis      ')\g
INSERT INTO Diagnose VALUES ('71432','   Stenosis, other      ')\g
INSERT INTO Diagnose VALUES ('71433','   Anorectal malformation     Include: imperforate anus      ')\g
INSERT INTO Diagnose VALUES ('71434','   Biliary atresia      ')\g
INSERT INTO Diagnose VALUES ('71435','   Atresia, other      ')\g
INSERT INTO Diagnose VALUES ('71439','   Other    Include: congenital diaphragm    exclude: esophagus (.142), annular pancreas (.1491), acquired (.72)      ')\g
INSERT INTO Diagnose VALUES ('7144','    Meconium syndromes      ')\g
INSERT INTO Diagnose VALUES ('71441','   Meconium ileus (see .1496)      ')\g
INSERT INTO Diagnose VALUES ('71442','   Meconium peritonitis      ')\g
INSERT INTO Diagnose VALUES ('71443','   Meconium plug syndrome      ')\g
INSERT INTO Diagnose VALUES ('71449','   Other    Include: meconium cyst      ')\g
INSERT INTO Diagnose VALUES ('7145','    Aganglionosis (Hirschsprung disease)      ')\g
INSERT INTO Diagnose VALUES ('71451','   Without enterocolitis      ')\g
INSERT INTO Diagnose VALUES ('71452','   With enterocolitis      ')\g
INSERT INTO Diagnose VALUES ('71459','   Other    Include: neuronal dysplasia      ')\g
INSERT INTO Diagnose VALUES ('7146','    Malrotation     exclude: omphalocele (.1495)      ')\g
INSERT INTO Diagnose VALUES ('71461','   Without midgut volvulus      ')\g
INSERT INTO Diagnose VALUES ('71462','   With midgut volvulus      ')\g
INSERT INTO Diagnose VALUES ('7147','    Situs inversus      ')\g
INSERT INTO Diagnose VALUES ('7149','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('71491','   Annular pancreas    Include: pancreas divisum      ')\g
INSERT INTO Diagnose VALUES ('71492','   Choledochal cyst      ')\g
INSERT INTO Diagnose VALUES ('71493','   Meckel diverticulum, other vitelline duct anomaly      ')\g
INSERT INTO Diagnose VALUES ('71494','   Congenital arteriovenous fistula      ')\g
INSERT INTO Diagnose VALUES ('71495','   Omphalocele, gastroschisis      ')\g
INSERT INTO Diagnose VALUES ('71496','   Cystic fibrosis     exclude: meconium ileus (.1441)      ')\g
INSERT INTO Diagnose VALUES ('71497','   Ectopic gallbladder, anomalous or accessory bile ducts      ')\g
INSERT INTO Diagnose VALUES ('71499','   Other     Include: microgastria, segmental dilatation of bowel     exclude: gastroesophageal regurgita-tion (.151), diverticulum (.27), ectopic pancreas (.314), lymphangiectasia (.766)      ')\g
INSERT INTO Diagnose VALUES ('715','     HERNIA, EVENTRATION (CONGENITAL OR ACQUIRED)      ')\g
INSERT INTO Diagnose VALUES ('7151','    Gastroesophageal regurgitation (reflux)    Include: chalasia      ')\g
INSERT INTO Diagnose VALUES ('7152','    Esophageal hiatus hernia      ')\g
INSERT INTO Diagnose VALUES ('71521','   Sliding" hernia"      ')\g
INSERT INTO Diagnose VALUES ('71522','   Paraesophageal hernia      ')\g
INSERT INTO Diagnose VALUES ('7153','    Hernia through foramen of Morgagni      ')\g
INSERT INTO Diagnose VALUES ('7154','    Hernia through foramen of Bochdalek (pleuroperitoneal canal)     Include: absent diaphragm      ')\g
INSERT INTO Diagnose VALUES ('7155','    Eventration localized or complete, paralysis of diaphragm     exclude: secondary to carcinoma of lung (6.3236), secondary to phrenic nerve injury (6.455)      ')\g
INSERT INTO Diagnose VALUES ('7156','    Traumatic diaphragmatic hernia      ')\g
INSERT INTO Diagnose VALUES ('7157','    Abdominal wall hernia      ')\g
INSERT INTO Diagnose VALUES ('71571','   Inguinal      ')\g
INSERT INTO Diagnose VALUES ('71572','   Umbilical      ')\g
INSERT INTO Diagnose VALUES ('71579','   Other     Include: femoral, incisional      ')\g
INSERT INTO Diagnose VALUES ('7158','    Internal hernia     Include: paraduodenal, foramen of Winslow      ')\g
INSERT INTO Diagnose VALUES ('7159','    Other     Include: hernia of gallbladder, liver, spleen, prolapse (e.g., rectal) (see also .133)      ')\g
INSERT INTO Diagnose VALUES ('719','     OTHER     exclude: diverticulum (.27)      ')\g
INSERT INTO Diagnose VALUES ('72','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('720','     INFECTION CLASSIFIED BY ORGANISM (for detailed classification see 6.20)      ')\g
INSERT INTO Diagnose VALUES ('7201','    Gram-positive facultative bacterium      ')\g
INSERT INTO Diagnose VALUES ('7202','    Gram-negative bacterium      ')\g
INSERT INTO Diagnose VALUES ('72025','   Salmonella, Shigella      ')\g
INSERT INTO Diagnose VALUES ('72027','   Yersinia     Include: Y. enterocolitica      ')\g
INSERT INTO Diagnose VALUES ('7203','    Mycobacterium     exclude: tuberculosis (.23)      ')\g
INSERT INTO Diagnose VALUES ('7204','    Anaerobic bacterium      ')\g
INSERT INTO Diagnose VALUES ('7205','    Fungus      ')\g
INSERT INTO Diagnose VALUES ('72057','   Monilia      ')\g
INSERT INTO Diagnose VALUES ('7206','    Virus or Mycoplasma      ')\g
INSERT INTO Diagnose VALUES ('7207','    Protozoa or spirochete      ')\g
INSERT INTO Diagnose VALUES ('72071','   Amebiasis      ')\g
INSERT INTO Diagnose VALUES ('72072','   Other intestinal protozoa     Include: Giardia lamblia, cryptosporidiosis      ')\g
INSERT INTO Diagnose VALUES ('7208','    Helminth, roundworm, flatworm      ')\g
INSERT INTO Diagnose VALUES ('72081','   Intestinal roundworm     Include: Enterobius vermicularis, Ascaris lumbricoides, Necator americanus, Strongyloides stercoralis, Trichinella spiralis      ')\g
INSERT INTO Diagnose VALUES ('72082','   Tissue roundworm     Include: Wuchereria bancrofti, Dracunculus medinensis      ')\g
INSERT INTO Diagnose VALUES ('72083','   Tapeworm     Include: Taenia saginata, Taenia solium (cysticercosis), Echinococcus      ')\g
INSERT INTO Diagnose VALUES ('72084','   Blood fluke     Include: Schistosoma      ')\g
INSERT INTO Diagnose VALUES ('72085','   Intestinal, liver fluke     Include: Paragonimus westermani, Colonorchis sinensis      ')\g
INSERT INTO Diagnose VALUES ('72089','   Other      ')\g
INSERT INTO Diagnose VALUES ('7209','    Other      ')\g
INSERT INTO Diagnose VALUES ('721','     INTRA-ABDOMINAL ABSCESS,INFLAMMATORY MASS      ')\g
INSERT INTO Diagnose VALUES ('722','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('723','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('724','     SOFT TISSUE INFLAMMATION, SINUS TRACT, FISTULA      ')\g
INSERT INTO Diagnose VALUES ('7241','    Cellulitis      ')\g
INSERT INTO Diagnose VALUES ('7244','    Sinus tract      ')\g
INSERT INTO Diagnose VALUES ('7245','    Fistula     exclude: biliary fistula (.284), congenital tracheoesophageal fistula (.1422, .1423), acquired tracheoesophageal fistula (6.24), regional enteritis (.26)      ')\g
INSERT INTO Diagnose VALUES ('7246','    Lymphadenitis      ')\g
INSERT INTO Diagnose VALUES ('7249','    Other      ')\g
INSERT INTO Diagnose VALUES ('725','     ULCER      ')\g
INSERT INTO Diagnose VALUES ('7251','    Single niche      ')\g
INSERT INTO Diagnose VALUES ('7252','    Multiple niches      ')\g
INSERT INTO Diagnose VALUES ('7253','    Penetrating niche      ')\g
INSERT INTO Diagnose VALUES ('7254','    Epithelialized crater, scar, healed niche      ')\g
INSERT INTO Diagnose VALUES ('7255','    Deformity, pseudodiverticulum      ')\g
INSERT INTO Diagnose VALUES ('7256','    Stricture, obstruction      ')\g
INSERT INTO Diagnose VALUES ('7258','    Marginal      ')\g
INSERT INTO Diagnose VALUES ('7259','    Other     Include: stress or steroid induced, Zollinger-Ellison, giant" ulcer     exclude: perforated (.71), columnar lined esophagus (.291)"      ')\g
INSERT INTO Diagnose VALUES ('726','     ENTERITIS, COLITIS      ')\g
INSERT INTO Diagnose VALUES ('7261','    Ulcerative colitis      ')\g
INSERT INTO Diagnose VALUES ('7262','    Regional enteritis, granulomatous colitis (Crohn disease)      ')\g
INSERT INTO Diagnose VALUES ('7263','    Acute pseudomembranous      ')\g
INSERT INTO Diagnose VALUES ('7264','    Other bacterial (also code specific organism.20)      ')\g
INSERT INTO Diagnose VALUES ('7265','    Parasitic (also code specific organism .20)      ')\g
INSERT INTO Diagnose VALUES ('7266','    Ischemic      ')\g
INSERT INTO Diagnose VALUES ('7267','    Necrotizing enterocolitis (see also pneumatosis intestinalis .78)      ')\g
INSERT INTO Diagnose VALUES ('7268','    Segmental (etiology unknown)      ')\g
INSERT INTO Diagnose VALUES ('7269','    Other     Include: lymphopathia venereum, colitis cystica profunda, viral     exclude: eosinophilic gastroenteritis (.291)      ')\g
INSERT INTO Diagnose VALUES ('727','     DIVERTICULUM, DIVERTICULITIS      ')\g
INSERT INTO Diagnose VALUES ('7271','    Traction diverticulum (e.g., midesophagus), typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('7272','    Pulsion diverticulum, typical manifestation    Include: pharynx, Zenker, epiphrenic, stomach, duodenum, small bowel, colon      ')\g
INSERT INTO Diagnose VALUES ('7273','    Diverticulitis, typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('7274','    Unusual manifestation of diverticulum or diverticulitis      ')\g
INSERT INTO Diagnose VALUES ('7275','    Diverticulosis      ')\g
INSERT INTO Diagnose VALUES ('7279','    Other     Include: intramural diverticulum    exclude: congenital duplication (.141), Meckel diverticulum (.1493)      ')\g
INSERT INTO Diagnose VALUES ('728','     ABNORMAL BILIARY TRACT      ')\g
INSERT INTO Diagnose VALUES ('7281','    Normal opacification, satisfactory demon-stration (ultrasonography)      ')\g
INSERT INTO Diagnose VALUES ('7282','    Poor opacification, poor demonstration (ultrasonography)      ')\g
INSERT INTO Diagnose VALUES ('7283','    No opacification, no demonstration (ultrasonography)      ')\g
INSERT INTO Diagnose VALUES ('7284','    Internal biliary fistula (operative or other), emphysematous cholecystitis      ')\g
INSERT INTO Diagnose VALUES ('7285','    Hydrops, empyema, acute cholecystitis, intraperitoneal perforation of gallbladder      ')\g
INSERT INTO Diagnose VALUES ('7286','    Milk of calcium bile, calcified gallbladder wall      ')\g
INSERT INTO Diagnose VALUES ('7287','    Hyperplastic cholecystosis      ')\g
INSERT INTO Diagnose VALUES ('7288','    Sclerosing cholangitis, primary biliary cirrhosis, Caroli�s disease, other abnormality of biliary tract      ')\g
INSERT INTO Diagnose VALUES ('7289','    Other     Include: gallstone on plain film (76.2891), recurrent pyogenic cholangitis    exclude: papilloma or other attached nodule (.311)      ')\g
INSERT INTO Diagnose VALUES ('729','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('7293','    Adhesions causing deformity     Include: pericolic, perigastric      ')\g
INSERT INTO Diagnose VALUES ('7294','    Enlarged papilla of Vater (papillary" sign) (also code cause, e.g., pancreatitis .291, ulcer .25, common duct stone .2881, .2882)    exclude: hypertrophy of Brunner glands (.3195)"      ')\g
INSERT INTO Diagnose VALUES ('7295','    Peritonitis     exclude: intraperitoneal abscess (.21)      ')\g
INSERT INTO Diagnose VALUES ('7296','    Chronic granulomatous disease of child-hood (Landing-Shirkey syndrome)      ')\g
INSERT INTO Diagnose VALUES ('7297','    Postinflammatory stenosis or atresia      ')\g
INSERT INTO Diagnose VALUES ('7298','    Infectious complications of AIDS      ')\g
INSERT INTO Diagnose VALUES ('7299','    Other     exclude: chemical stricture (.744), meconium peritonitis (.1442), retractile mesenteritis (.768), Whipple disease (.764)      ')\g
INSERT INTO Diagnose VALUES ('73','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('731','     BENIGN NEOPLASM, CYST, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('7311','    Polyp      ')\g
INSERT INTO Diagnose VALUES ('73111','   Adenomatous polyp      ')\g
INSERT INTO Diagnose VALUES ('73112','   Villous tumor (benign and malignant)      ')\g
INSERT INTO Diagnose VALUES ('73113','   Hyperplastic polyp, juvenile polyp      ')\g
INSERT INTO Diagnose VALUES ('73114','   Polyposis     Include: familial colonic polyposis, generalized juvenile polyposis, Gardner syndrome, Turcot syndrome, Peutz-Jeghers syndrome, Cronkhite-Canada syndrome, nodular lymphoid hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('73119','   Other     Include: eosinophilic polyp (granu-loma)      ')\g
INSERT INTO Diagnose VALUES ('7312','    Cyst, pseudocyst, cystadenoma (benign or malignant)      ')\g
INSERT INTO Diagnose VALUES ('73121','   Cyst    Include: mesenteric cyst, omental cyst      ')\g
INSERT INTO Diagnose VALUES ('73122','   Cystadenoma      ')\g
INSERT INTO Diagnose VALUES ('731221','  Microcystic adenoma      ')\g
INSERT INTO Diagnose VALUES ('731222','  Macrocystic adenoma (mucinous cystic neoplasm)      ')\g
INSERT INTO Diagnose VALUES ('731229','  Other      ')\g
INSERT INTO Diagnose VALUES ('73123','   Pseudocyst      ')\g
INSERT INTO Diagnose VALUES ('73124','   Papillary and cystic neoplasm of pancreas      ')\g
INSERT INTO Diagnose VALUES ('73125','   Ductectatic pancreatic neoplasm      ')\g
INSERT INTO Diagnose VALUES ('73129','   Other     Include: polycystic disease    exclude: lymphangioma (.3194)      ')\g
INSERT INTO Diagnose VALUES ('7313','    Spindle cell tumor      ')\g
INSERT INTO Diagnose VALUES ('73131','   Leiomyoma      ')\g
INSERT INTO Diagnose VALUES ('73132','   Neurofibroma      ')\g
INSERT INTO Diagnose VALUES ('73133','   Fibroma      ')\g
INSERT INTO Diagnose VALUES ('73139','   Other      ')\g
INSERT INTO Diagnose VALUES ('7314','    Hamartoma, ectopic pancreas      ')\g
INSERT INTO Diagnose VALUES ('7315','    Lipoma      ')\g
INSERT INTO Diagnose VALUES ('7316','    Carcinoid      ')\g
INSERT INTO Diagnose VALUES ('7317','    Mucocele      ')\g
INSERT INTO Diagnose VALUES ('7318','    Endometriosis      ')\g
INSERT INTO Diagnose VALUES ('7319','    Other      ')\g
INSERT INTO Diagnose VALUES ('73191','   Islet cell tumor (benign and malignant)      ')\g
INSERT INTO Diagnose VALUES ('73192','   Adenoma     Include: liver      ')\g
INSERT INTO Diagnose VALUES ('73193','   Glomus tumor, hemangiopericytoma      ')\g
INSERT INTO Diagnose VALUES ('73194','   Hemangioma, lymphangioma      ')\g
INSERT INTO Diagnose VALUES ('73195','   Hypertrophy of Brunner glands      ')\g
INSERT INTO Diagnose VALUES ('73196','   Pseudomyxoma peritonei      ')\g
INSERT INTO Diagnose VALUES ('73197','   Pseudotumor     Include: pseudolymphoma    exclude: prominent mucosal fold (.138), Menetrier disease (.292)      ')\g
INSERT INTO Diagnose VALUES ('73198','   Other benign liver lesion    Include: focal nodular hyperplasia, regenerating nodule      ')\g
INSERT INTO Diagnose VALUES ('73199','   Other      ')\g
INSERT INTO Diagnose VALUES ('732','     MALIGNANT NEOPLASM-PRIMARY      ')\g
INSERT INTO Diagnose VALUES ('7321','    Carcinoma      ')\g
INSERT INTO Diagnose VALUES ('7322','    Sarcoma      ')\g
INSERT INTO Diagnose VALUES ('7323','    Hepatocellular carcinoma      ')\g
INSERT INTO Diagnose VALUES ('7324','    Fibrolamellar hepatoma      ')\g
INSERT INTO Diagnose VALUES ('7329','    Other     Include: neoplasm associated with AIDS    exclude: carcinoid (.316), lymphoma (.34), malignant islet cell tumor (.3191)      ')\g
INSERT INTO Diagnose VALUES ('733','     MALIGNANT NEOPLASM-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('7331','    From adjoining lesion      ')\g
INSERT INTO Diagnose VALUES ('7332','    From distant site      ')\g
INSERT INTO Diagnose VALUES ('7339','    Other      ')\g
INSERT INTO Diagnose VALUES ('734','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('7341','    Leukemia      ')\g
INSERT INTO Diagnose VALUES ('7342','    Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('7343','    Non-Hodgkin lymphoma      ')\g
INSERT INTO Diagnose VALUES ('73431','   Lymphocytic well differentiated      ')\g
INSERT INTO Diagnose VALUES ('73432','   Nodular      ')\g
INSERT INTO Diagnose VALUES ('73433','   Diffuse      ')\g
INSERT INTO Diagnose VALUES ('73434','   Histiocytic      ')\g
INSERT INTO Diagnose VALUES ('73435','   Burkitt      ')\g
INSERT INTO Diagnose VALUES ('73439','   Other      ')\g
INSERT INTO Diagnose VALUES ('7345','    Myeloma      ')\g
INSERT INTO Diagnose VALUES ('73451','   Plasmacytoma      ')\g
INSERT INTO Diagnose VALUES ('73452','   Multiple myeloma      ')\g
INSERT INTO Diagnose VALUES ('73459','   Other      ')\g
INSERT INTO Diagnose VALUES ('7346','    Kaposi syndrome      ')\g
INSERT INTO Diagnose VALUES ('734725','  Acute      ')\g
INSERT INTO Diagnose VALUES ('734727','  Opaque or partially opaque stone, typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('734728','  Acute      ')\g
INSERT INTO Diagnose VALUES ('734756','  Subacute      ')\g
INSERT INTO Diagnose VALUES ('734758','  Nonopaque stone, typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('734784','  Chronic      ')\g
INSERT INTO Diagnose VALUES ('734786','  Stone, unusual manifestation      ')\g
INSERT INTO Diagnose VALUES ('734787','  Chronic      ')\g
INSERT INTO Diagnose VALUES ('734815','  Chronic with acute exacerbation      ')\g
INSERT INTO Diagnose VALUES ('734817','  Obstructed cystic duct      ')\g
INSERT INTO Diagnose VALUES ('734818','  Chronic with acute exacerbation      ')\g
INSERT INTO Diagnose VALUES ('734845','  Chronic with superimposed carcinoma (see .321)      ')\g
INSERT INTO Diagnose VALUES ('734847','  Abnormality of cystic duct stump      ')\g
INSERT INTO Diagnose VALUES ('734848','  Chronic with superimposed carcinoma      ')\g
INSERT INTO Diagnose VALUES ('734876','  Toxic megacolon      ')\g
INSERT INTO Diagnose VALUES ('734878','  Obstructed common or hepatic ducts      ')\g
INSERT INTO Diagnose VALUES ('734879','  Toxic megacolon      ')\g
INSERT INTO Diagnose VALUES ('7349','    Other     Include: Waldenstrom macroglobulinemia, heavy or light chain disorder      ')\g
INSERT INTO Diagnose VALUES ('734906','  Fistula, sinus tract formation (see .244, .245)      ')\g
INSERT INTO Diagnose VALUES ('734908','  Stone with acoustical shadowing (ultrasonography)      ')\g
INSERT INTO Diagnose VALUES ('734909','  Fistula, sinus tract formation      ')\g
INSERT INTO Diagnose VALUES ('734937','  Complicated pancreatitis with phlegmon, abscess or necrosis      ')\g
INSERT INTO Diagnose VALUES ('734939','  Stone without acoustical shadowing (ultrasonography)      ')\g
INSERT INTO Diagnose VALUES ('734968','  Other      ')\g
INSERT INTO Diagnose VALUES ('734970','  Other   include: mucocutaneous lymph node syndrome, viscous bile (sludge) (ultrasonography)      ')\g
INSERT INTO Diagnose VALUES ('734971','  Other      ')\g
INSERT INTO Diagnose VALUES ('735','     NEOPLASM ARISING IN PREEXISTING BENIGN DISORDER      ')\g
INSERT INTO Diagnose VALUES ('736','     EXTRINSIC MASS AFFECTING GASTROINTESTINAL STRUCTURE, ARISING FROM      ')\g
INSERT INTO Diagnose VALUES ('7361','    Urinary tract      ')\g
INSERT INTO Diagnose VALUES ('7362','    Genital tract     exclude: endometriosis (.318)      ')\g
INSERT INTO Diagnose VALUES ('7363','    Liver, spleen (see also seen on plain film .37)      ')\g
INSERT INTO Diagnose VALUES ('7364','    Pancreas      ')\g
INSERT INTO Diagnose VALUES ('7365','    Bile ducts, gallbladder      ')\g
INSERT INTO Diagnose VALUES ('7366','    Thorax     Include: aneurysm, elongated aorta, medias-tinal nodes or masses      ')\g
INSERT INTO Diagnose VALUES ('7369','    Other     Include: cause unknown, mesenteric nodes, retroperitoneal soft tissues      ')\g
INSERT INTO Diagnose VALUES ('737','     HEPATOMEGALY, SPLENOMEGALY SEEN ON PLAIN FILM (see also .363)      ')\g
INSERT INTO Diagnose VALUES ('7371','    Hepatomegaly      ')\g
INSERT INTO Diagnose VALUES ('7372','    Splenomegaly      ')\g
INSERT INTO Diagnose VALUES ('7373','    Hepatosplenomegaly      ')\g
INSERT INTO Diagnose VALUES ('738','     ABDOMINAL MASS SEEN ON PLAIN FILM (see also .36)      ')\g
INSERT INTO Diagnose VALUES ('739','     OTHER     Include: recurrent neoplasm      ')\g
INSERT INTO Diagnose VALUES ('74','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('741','     ACUTE INJURY     exclude: perforated hollow viscus (.71)      ')\g
INSERT INTO Diagnose VALUES ('7411','    Laceration or rupture of solid organ      ')\g
INSERT INTO Diagnose VALUES ('7412','    Hematoma      ')\g
INSERT INTO Diagnose VALUES ('7419','    Other      ')\g
INSERT INTO Diagnose VALUES ('743','     COMPLICATION OF INJURY      ')\g
INSERT INTO Diagnose VALUES ('744','     COMPLICATION OF ANGIOGRAPHY (see 95.44)      ')\g
INSERT INTO Diagnose VALUES ('745','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('7451','    Resection, partial or total, or an organ    Include: inverted appendiceal stump, short gut syndrome""      ')\g
INSERT INTO Diagnose VALUES ('7452','    Implantation of radiation source      ')\g
INSERT INTO Diagnose VALUES ('7453','    Internal anastomosis      ')\g
INSERT INTO Diagnose VALUES ('7454','    Cutaneous stoma      ')\g
INSERT INTO Diagnose VALUES ('7455','    Pyloroplasty      ')\g
INSERT INTO Diagnose VALUES ('7456','    Vagotomy      ')\g
INSERT INTO Diagnose VALUES ('7458','    Complication of surgery or interventional procedure    Include: of biopsy, of aspiration    exclude: of angiography (.44)      ')\g
INSERT INTO Diagnose VALUES ('7461','    Opaque      ')\g
INSERT INTO Diagnose VALUES ('74611','   Catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('74612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('74613','   Catheter (or other tube) in unsatisfac-tory position      ')\g
INSERT INTO Diagnose VALUES ('74619','   Other     exclude: extravasation of contrast medium (.719), implantation of radia-tion source (.452), hepatic oil emboli following lymphangiography (.799)      ')\g
INSERT INTO Diagnose VALUES ('7462','    Nonopaque     Include: bezoar     exclude: worm (.208)      ')\g
INSERT INTO Diagnose VALUES ('7469','    Other      ')\g
INSERT INTO Diagnose VALUES ('747','     EFFECT OF RADIATION     Include: radium poisoning      ')\g
INSERT INTO Diagnose VALUES ('749','     OTHER     Include: arteriovenous fistula, intravascular gas, post-traumatic, gas in abdominal wall, gas in diaphragm      ')\g
INSERT INTO Diagnose VALUES ('75','      METABOLIC, ENDOCRINE, TOXIC    Include: fat infiltration of liver (see also .794), sequelae of thorotrast administration      ')\g
INSERT INTO Diagnose VALUES ('76','      OTHER GENERALIZED SYSTEMIC DISORDER      ')\g
INSERT INTO Diagnose VALUES ('761','     SYSTEMIC CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('7612','    Lupus erythematosus      ')\g
INSERT INTO Diagnose VALUES ('7613','    Progressive systemic sclerosis (scleroderma)      ')\g
INSERT INTO Diagnose VALUES ('7614','    Dermatomyositis, polymyositis      ')\g
INSERT INTO Diagnose VALUES ('7615','    Ehlers-Danlos syndrome, cutis laxa, pseudoxanthoma elasticum      ')\g
INSERT INTO Diagnose VALUES ('7619','    Other      ')\g
INSERT INTO Diagnose VALUES ('762','     VASCULITIS      ')\g
INSERT INTO Diagnose VALUES ('7621','    Polyarteritis (periarteritis) nodosa      ')\g
INSERT INTO Diagnose VALUES ('7627','    Henoch purpura      ')\g
INSERT INTO Diagnose VALUES ('7628','    Hemolytic-uremic syndrome      ')\g
INSERT INTO Diagnose VALUES ('7629','    Other     Include: mucocutaneous lymph node syndrome   exclude: lupus vasculitis (.612)      ')\g
INSERT INTO Diagnose VALUES ('764','     COMPLICATION OF DRUG THERAPY, DRUG USE      ')\g
INSERT INTO Diagnose VALUES ('765','     HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('7651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('7652','    Thalassemia or variant      ')\g
INSERT INTO Diagnose VALUES ('7655','    Pernicious anemia      ')\g
INSERT INTO Diagnose VALUES ('7657','    Myeloid metaplasia (myelosclerosis)      ')\g
INSERT INTO Diagnose VALUES ('7658','    Disseminated intravascular coagulation      ')\g
INSERT INTO Diagnose VALUES ('7659','    Other     Include: hemochromatosis, Budd-Chiari syndrome, complication of anticoagulant therapy     exclude: leukemia (.341), hemolytic-uremic syndrome (.628)      ')\g
INSERT INTO Diagnose VALUES ('766','     HISTIOCYTOSIS (LANGERHANS CELL HISTIOCYTOSIS)      ')\g
INSERT INTO Diagnose VALUES ('767','     SPHINGOLIPIDOSIS     Include: Gaucher disease, Niemann-Pick disease      ')\g
INSERT INTO Diagnose VALUES ('768','     OTHER INFILTRATIVE DISORDER     Include: amyloid, glycogen storage disease      ')\g
INSERT INTO Diagnose VALUES ('769','     OTHER     Include: graft versus host reaction, mastocytosis (urticaria pigmentosa), alpha-1-antitrypsin deficiency      ')\g
INSERT INTO Diagnose VALUES ('77','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('771','     PNEUMOPERITONEUM, PERFORATED HOL-LOW VISCUS      ')\g
INSERT INTO Diagnose VALUES ('7711','    Traumatic pneumoperitoneum (with or without perforated viscus) (see also .715)      ')\g
INSERT INTO Diagnose VALUES ('7712','    Spontaneous pneumoperitoneum     Include: perforated peptic ulcer      ')\g
INSERT INTO Diagnose VALUES ('7713','    Perforation of hollow viscus without free air (Boerhaave syndrome, Mallory-Weiss syndrome)      ')\g
INSERT INTO Diagnose VALUES ('7714','    Spontaneous pneumoperitoneum without perforated viscus or etiology unknown      ')\g
INSERT INTO Diagnose VALUES ('7715','    Perforation by instrumentation      ')\g
INSERT INTO Diagnose VALUES ('7716','    From pneumomediastinum      ')\g
INSERT INTO Diagnose VALUES ('7719','    Other     Include: extravasation of contrast medium    exclude: diagnostic (.1297) or postoperative pneumoperitoneum (.459)      ')\g
INSERT INTO Diagnose VALUES ('772','     ILEUS, OBSTRUCTIVE OR PARALYTIC      ')\g
INSERT INTO Diagnose VALUES ('7721','    Paralytic, adynamic or nonobstructive ileus,typical manifestation      ')\g
INSERT INTO Diagnose VALUES ('7722','    Paralytic, adynamic or nonobstructive ileus, unusual manifestation     Include: sentinel loop ileus      ')\g
INSERT INTO Diagnose VALUES ('7723','    Small bowel obstruction, typical manifesta-tion      ')\g
INSERT INTO Diagnose VALUES ('7724','    Small bowel obstruction, unusual manifes-tation    exclude: superior mesenteric artery syn-drome, cast syndrome (.729), milk curds (.769)      ')\g
INSERT INTO Diagnose VALUES ('7725','    Large bowel obstruction, typical manifesta-tion      ')\g
INSERT INTO Diagnose VALUES ('7726','    Large bowel obstruction, unusual manifes-tation      ')\g
INSERT INTO Diagnose VALUES ('7729','    Other     Include: superior mesenteric artery syn- drome, cast syndrome, chronic intestinal pseudo-obstruction     exclude: congenital (.14)      ')\g
INSERT INTO Diagnose VALUES ('773','     INTUSSUSCEPTION      ')\g
INSERT INTO Diagnose VALUES ('7731','    Without obstruction      ')\g
INSERT INTO Diagnose VALUES ('7732','    With obstruction      ')\g
INSERT INTO Diagnose VALUES ('774','     ESOPHAGEAL OBSTRUCTION, DYSKINESIA      ')\g
INSERT INTO Diagnose VALUES ('7741','    Dyskinesia (see also swallowing disorder 2.82)      ')\g
INSERT INTO Diagnose VALUES ('7742','    Web      ')\g
INSERT INTO Diagnose VALUES ('7744','    Stricture     Include: corrosives, alkali, acid      ')\g
INSERT INTO Diagnose VALUES ('7745','    Achalasia (cardiospasm)      ')\g
INSERT INTO Diagnose VALUES ('775','     VARICES (code under site)      ')\g
INSERT INTO Diagnose VALUES ('776','     SMALL BOWEL DISORDER     exclude: tuberculosis (.23), regional enteritis (.262), neoplasm (.3), systemic connective tissue disorder (.61)      ')\g
INSERT INTO Diagnose VALUES ('7761','    Malabsorption (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('77611','   Sprue, celiac disease      ')\g
INSERT INTO Diagnose VALUES ('77612','   Liver, biliary tract disease      ')\g
INSERT INTO Diagnose VALUES ('77619','   Other      ')\g
INSERT INTO Diagnose VALUES ('7762','    Maldigestion      ')\g
INSERT INTO Diagnose VALUES ('77621','   Zollinger-Ellison syndrome, Wermer syndrome      ')\g
INSERT INTO Diagnose VALUES ('77622','   Pancreatic insufficiency      ')\g
INSERT INTO Diagnose VALUES ('77623','   Disaccharidase deficiency      ')\g
INSERT INTO Diagnose VALUES ('77629','   Other      ')\g
INSERT INTO Diagnose VALUES ('7763','    Hypoproteinemia     Include: nutritional, nephrosis, cardiac failure    exclude: cirrhosis (.794)      ')\g
INSERT INTO Diagnose VALUES ('7764','    Whipple disease      ')\g
INSERT INTO Diagnose VALUES ('7765','    Nodular lymphoid hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('7766','    Lymphangiectasia      ')\g
INSERT INTO Diagnose VALUES ('7767','    Dysgammaglobulinemia (see also Waldenstrom macroglobulinemia .349,      ')\g
INSERT INTO Diagnose VALUES ('7768','    Retractile mesenteritis      ')\g
INSERT INTO Diagnose VALUES ('778','     UNUSUAL GAS COLLECTIONS (see also necrotizing enterocolitis .267)  exclude: biliary tract (.284, .285),gas in abscess (.21), in abdominal wall (.49), in diaphragm (.49)      ')\g
INSERT INTO Diagnose VALUES ('7781','    Meteorism (aerophagia)      ')\g
INSERT INTO Diagnose VALUES ('7782','    Pneumatosis intestinalis      ')\g
INSERT INTO Diagnose VALUES ('7783','    Gas in portal vein      ')\g
INSERT INTO Diagnose VALUES ('7789','    Other      ')\g
INSERT INTO Diagnose VALUES ('779','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('7791','    Constipation     Include: functional megacolon, fecal impaction      ')\g
INSERT INTO Diagnose VALUES ('7792','    Other functional colon disease      ')\g
INSERT INTO Diagnose VALUES ('7793','    Cathartic colon      ')\g
INSERT INTO Diagnose VALUES ('7794','    Cirrhosis      ')\g
INSERT INTO Diagnose VALUES ('77941','   With diffuse fatty infiltration      ')\g
INSERT INTO Diagnose VALUES ('77942','   With focal fatty infiltration      ')\g
INSERT INTO Diagnose VALUES ('7795','    Infarction      ')\g
INSERT INTO Diagnose VALUES ('7799','    Other     Include: hepatic oil emboli following lymphangiography      ')\g
INSERT INTO Diagnose VALUES ('78','      MISCELLANEOUS, OTHER      ')\g
INSERT INTO Diagnose VALUES ('781','     INTRA-ABDOMINAL CALCIFICATION, CALCULUS      ')\g
INSERT INTO Diagnose VALUES ('7811','    Enterolith     Include: appendicolith      ')\g
INSERT INTO Diagnose VALUES ('7812','    Lymph node      ')\g
INSERT INTO Diagnose VALUES ('7813','    Vascular calcification, intraperitoneal    Include: aneurysm      ')\g
INSERT INTO Diagnose VALUES ('7814','    Liver, spleen, pancreas      ')\g
INSERT INTO Diagnose VALUES ('7815','    Neoplasm      ')\g
INSERT INTO Diagnose VALUES ('7816','    Acoustic shadow     exclude: biliary tract (.28)      ')\g
INSERT INTO Diagnose VALUES ('7819','    Other     Include: in bowel lumen, in bowel wall    exclude: biliary tract (.28)      ')\g
INSERT INTO Diagnose VALUES ('785','     REFLUX   Include: gastroesophageal reflux (see .291)      ')\g
INSERT INTO Diagnose VALUES ('789','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('79','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('791','     FUNDAMENTAL OBSERVATION: FUNCTIONAL ABNORMALITIES      ')\g
INSERT INTO Diagnose VALUES ('792','     ANATOMICAL DETAIL      ')\g
INSERT INTO Diagnose VALUES ('7921','    Dilated pancreatic duct      ')\g
INSERT INTO Diagnose VALUES ('7929','    Other      ')\g
INSERT INTO Diagnose VALUES ('793','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('794','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('799','     OTHER     include: phenomenon related to technique of examination, perivascular or other accidental injection of contrast medium    exclude: perforation by instrumentation (.715), barium extravasation from hollow viscus (.719)      ')\g
INSERT INTO Diagnose VALUES ('81','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('811','     NORMAL ROUTINE PLAIN FILM    exclude: normal variant (.13)      ')\g
INSERT INTO Diagnose VALUES ('812','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION      ')\g
INSERT INTO Diagnose VALUES ('8121','    Digital radiographic techniques, tomography, MR, nuclear medicine      ')\g
INSERT INTO Diagnose VALUES ('81211','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('812111','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('812112','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('812113','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('812114','  Delayed scanning following enhancement (e.g., for detection of liver lesions)      ')\g
INSERT INTO Diagnose VALUES ('812115','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('812116','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('812117','  Three-dimensional reconstruction      ')\g
INSERT INTO Diagnose VALUES ('812118','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('812119','  Other, including serial enhancement studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('81212','   Nephrotomography      ')\g
INSERT INTO Diagnose VALUES ('81213','   Infusion pyelography with tomography      ')\g
INSERT INTO Diagnose VALUES ('81214','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('812141','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('8121411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('8121412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('8121413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('8121414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('8121415',' Specific resonance suppressed  include:  fat  suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('8121416',' High-speed imaging  include:  echo planar      ')\g
INSERT INTO Diagnose VALUES ('8121417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('8121419',' Other      ')\g
INSERT INTO Diagnose VALUES ('812142','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('812143','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('812144','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('812145','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('812146','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('812147','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('812149','  Other      ')\g
INSERT INTO Diagnose VALUES ('81215','   Digital radiography    exclude:  with angiography (.124-3, 9.12-3)      ')\g
INSERT INTO Diagnose VALUES ('81216','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('812161','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('812162','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('812163','  PET      ')\g
INSERT INTO Diagnose VALUES ('812164','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('812165','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('812166','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('812167','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('812168','  Therapeutic procedure, include use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('812169','  Other.  See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('81217','   Nuclear medicine study-genitourinary      ')\g
INSERT INTO Diagnose VALUES ('812171','  Renal cortex imaging (e.g., 99mTc-DMSA)      ')\g
INSERT INTO Diagnose VALUES ('812172','  Renal imaging (e.g., 99mTc-DTPA)      ')\g
INSERT INTO Diagnose VALUES ('812173','  Metabolism study      ')\g
INSERT INTO Diagnose VALUES ('812174','  Renal imaging with pharmacologic intervention include: imaging with use of diuretics, or with angiotensin-converting-enzyme inhibitors      ')\g
INSERT INTO Diagnose VALUES ('812175','  Adrenal imaging      ')\g
INSERT INTO Diagnose VALUES ('812176','  Testicular imaging      ')\g
INSERT INTO Diagnose VALUES ('812177','  Radionuclide cystography      ')\g
INSERT INTO Diagnose VALUES ('812179','  Other      ')\g
INSERT INTO Diagnose VALUES ('81218','   Conventional tomography      ')\g
INSERT INTO Diagnose VALUES ('81219','   Other      ')\g
INSERT INTO Diagnose VALUES ('8122','    Pyelography      ')\g
INSERT INTO Diagnose VALUES ('81221','   Intravenous      ')\g
INSERT INTO Diagnose VALUES ('81222','   Retrograde      ')\g
INSERT INTO Diagnose VALUES ('81223','   Drip infusion      ')\g
INSERT INTO Diagnose VALUES ('81224','   Special study for renovascular hypertension      ')\g
INSERT INTO Diagnose VALUES ('81229','   Other form of pyelography    include:  gas, intramuscular, antegrade pyelography      ')\g
INSERT INTO Diagnose VALUES ('8123','    Cystography, urethrography      ')\g
INSERT INTO Diagnose VALUES ('81231','   Cystography      ')\g
INSERT INTO Diagnose VALUES ('81232','   Cystography with voiding urethrography, male      ')\g
INSERT INTO Diagnose VALUES ('81233','   Cystography with voiding urethrography, female      ')\g
INSERT INTO Diagnose VALUES ('81234','   Retrograde urethrography, male      ')\g
INSERT INTO Diagnose VALUES ('81235','   Retrograde urethrography, female      ')\g
INSERT INTO Diagnose VALUES ('81236','   Cystourethrography, stress incontinence cystogram      ')\g
INSERT INTO Diagnose VALUES ('81237','   Air cystography, double contrast cystography      ')\g
INSERT INTO Diagnose VALUES ('81238','   Ascendent lipiodol      ')\g
INSERT INTO Diagnose VALUES ('81239','   Other    include:  urethrography for hermaphroditism      ')\g
INSERT INTO Diagnose VALUES ('8124','    Angiography (see also 9.)      ')\g
INSERT INTO Diagnose VALUES ('81241','   Abdominal aortography      ')\g
INSERT INTO Diagnose VALUES ('81242','   Intravenous angiography      ')\g
INSERT INTO Diagnose VALUES ('81243','   Selective renal angiography      ')\g
INSERT INTO Diagnose VALUES ('81244','   Selective renal venography      ')\g
INSERT INTO Diagnose VALUES ('81245','   Selective adrenal angiography      ')\g
INSERT INTO Diagnose VALUES ('81246','   Selective adrenal venography      ')\g
INSERT INTO Diagnose VALUES ('81247','   Pelvic angiography      ')\g
INSERT INTO Diagnose VALUES ('81248','   Vena cavography      ')\g
INSERT INTO Diagnose VALUES ('81249','   Other   include:  penile angiography      ')\g
INSERT INTO Diagnose VALUES ('8125','    Special, nonroutine projection      ')\g
INSERT INTO Diagnose VALUES ('8126','    Biopsy-interventional      ')\g
INSERT INTO Diagnose VALUES ('81261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('81262','   Aspiration    include:  cyst puncture      ')\g
INSERT INTO Diagnose VALUES ('81263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('81264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('81265','   Thrombolytic infusion      ')\g
INSERT INTO Diagnose VALUES ('81266','   Chemotherapeutic infusion      ')\g
INSERT INTO Diagnose VALUES ('81267','   Operative procedure      ')\g
INSERT INTO Diagnose VALUES ('81269','   Other      ')\g
INSERT INTO Diagnose VALUES ('8127','    Perirenal air insufflation, presacral or direct retroperitoneal pneumography, pelvic pneumography      ')\g
INSERT INTO Diagnose VALUES ('8128','    Special study of genitalia, fetus, placenta (see also .131)      ')\g
INSERT INTO Diagnose VALUES ('81281','   Pelvimetry, fetometry      ')\g
INSERT INTO Diagnose VALUES ('81282','   Hysterosalpingography      ')\g
INSERT INTO Diagnose VALUES ('81283','   Amniography      ')\g
INSERT INTO Diagnose VALUES ('81284','   Intrauterine transfusion      ')\g
INSERT INTO Diagnose VALUES ('81285','   Placentography      ')\g
INSERT INTO Diagnose VALUES ('81286','   Vaginography      ')\g
INSERT INTO Diagnose VALUES ('81287','   Epididymography, seminal vesiculography      ')\g
INSERT INTO Diagnose VALUES ('81288','   Corpus cavernosography, vasography      ')\g
INSERT INTO Diagnose VALUES ('81289','   Other      ')\g
INSERT INTO Diagnose VALUES ('8129','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('81291','   Therapy localization (port film)      ')\g
INSERT INTO Diagnose VALUES ('81292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('81293','   Soft tissue technique      ')\g
INSERT INTO Diagnose VALUES ('81296','   Cine, video study      ')\g
INSERT INTO Diagnose VALUES ('81298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('812981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('812982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('812983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('812984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('812985','  Guided procedure for diagnosis    include:  biopsy      ')\g
INSERT INTO Diagnose VALUES ('812986','  Guided procedure for therapy    include:  drainage      ')\g
INSERT INTO Diagnose VALUES ('812987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('812988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('812989','  Special nonroutine projection, mode, or transducer type   include:  transvaginal probe, transrectal probe      ')\g
INSERT INTO Diagnose VALUES ('81299','   Other    include:  xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('813','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('8131','    Pregnancy      ')\g
INSERT INTO Diagnose VALUES ('81311','   Single      ')\g
INSERT INTO Diagnose VALUES ('81312','   Multiple      ')\g
INSERT INTO Diagnose VALUES ('8132','    Renal backflow      ')\g
INSERT INTO Diagnose VALUES ('8133','    Ptosis    include:  mobile kidney      ')\g
INSERT INTO Diagnose VALUES ('8134','    Fetal lobulation      ')\g
INSERT INTO Diagnose VALUES ('8135','    Variation in contour (e.g., of pelvis, urethra)   include:  variation in number of calyces      ')\g
INSERT INTO Diagnose VALUES ('8136','    Transient extraperitoneal hernia of bladder (bladder ear")"      ')\g
INSERT INTO Diagnose VALUES ('8137','    Vascular impression      ')\g
INSERT INTO Diagnose VALUES ('8139','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('81391','   Extrarenal pelvis      ')\g
INSERT INTO Diagnose VALUES ('81392','   Jet" phenomenon"      ')\g
INSERT INTO Diagnose VALUES ('81393','   Interureteric ridge      ')\g
INSERT INTO Diagnose VALUES ('81394','   Placental calcification      ')\g
INSERT INTO Diagnose VALUES ('81395','   Ureteral septa      ')\g
INSERT INTO Diagnose VALUES ('81396','   Psoas impression   include:  ureteral deviation due to psoas      ')\g
INSERT INTO Diagnose VALUES ('81399','   Other    exclude:  prominent column of Bertin (.1492), compensatory hypertrophy (.892)      ')\g
INSERT INTO Diagnose VALUES ('814','     CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY   exclude:  of fetus (.825, .87)      ')\g
INSERT INTO Diagnose VALUES ('8141','    Abnormal number      ')\g
INSERT INTO Diagnose VALUES ('81411','   Agenesis, unilateral or of an unpaired organ (e.g., bladder)      ')\g
INSERT INTO Diagnose VALUES ('81412','   Agenesis, bilateral      ')\g
INSERT INTO Diagnose VALUES ('81413','   Duplication, unilateral or of an unpaired organ   include:  septate bladder      ')\g
INSERT INTO Diagnose VALUES ('81414','   Duplication, bilateral      ')\g
INSERT INTO Diagnose VALUES ('81419','   Other    include:  triplication, supernumerary kidney   exclude:  bicornuate uterus and variations (.1478), fusion (.142)      ')\g
INSERT INTO Diagnose VALUES ('8142','    Fusion      ')\g
INSERT INTO Diagnose VALUES ('81421','   Horseshoe kidney      ')\g
INSERT INTO Diagnose VALUES ('81422','   Crossed renal ectopia      ')\g
INSERT INTO Diagnose VALUES ('81423','   Other fused kidney    include:  pancake, sigmoid      ')\g
INSERT INTO Diagnose VALUES ('81424','   Fusion, other      ')\g
INSERT INTO Diagnose VALUES ('81429','   Other      ')\g
INSERT INTO Diagnose VALUES ('8143','    Abnormal position or rotation      ')\g
INSERT INTO Diagnose VALUES ('81431','   Anomalous origin    include:  of renal artery, high insertion of ureter into renal pelvis      ')\g
INSERT INTO Diagnose VALUES ('81432','   Anomalous insertion    exclude:  ureterocele (.1454, .1455)      ')\g
INSERT INTO Diagnose VALUES ('81433','   Superior ectopia    exclude:  epispadias (.1461)      ')\g
INSERT INTO Diagnose VALUES ('81434','   Medial or lateral ectopia      ')\g
INSERT INTO Diagnose VALUES ('81435','   Inferior ectopia    include:  pelvic kidney, hypospadias   exclude:  ptosis (.133), ureterocele (.1454, .1455)      ')\g
INSERT INTO Diagnose VALUES ('81436','   Herniation    exclude:  transient bladder herniation (.136), simple ureterocele (.1454)      ')\g
INSERT INTO Diagnose VALUES ('81437','   Malrotation    include:  nonrotation, hyper-rotation      ')\g
INSERT INTO Diagnose VALUES ('81439','   Other    include:  retrocaval ureter    exclude:  fused kidney (.142), crossed renal ectopia (.1422)      ')\g
INSERT INTO Diagnose VALUES ('8144','    Hypoplasia, dysplasia      ')\g
INSERT INTO Diagnose VALUES ('81441','   Hypoplasia    exclude:  agenesis (.1411), of uterus (.14781)      ')\g
INSERT INTO Diagnose VALUES ('81442','   Multicystic kidney      ')\g
INSERT INTO Diagnose VALUES ('81443','   Dysplasia, other    exclude:  polycystic kidney (.312)      ')\g
INSERT INTO Diagnose VALUES ('81444','   Hypoplasia of abdominal musculature (Eagle-Barrett syndrome, prune belly")"      ')\g
INSERT INTO Diagnose VALUES ('81445','   Megacystis-microcolon-intestinal hypoperistalsis syndrome      ')\g
INSERT INTO Diagnose VALUES ('81449','   Other    exclude:  megaloureter (.849)      ')\g
INSERT INTO Diagnose VALUES ('8145','    Obstruction      ')\g
INSERT INTO Diagnose VALUES ('81451','   Valve      ')\g
INSERT INTO Diagnose VALUES ('81452','   Stenosis    exclude:  simple ureterocele (.1454)      ')\g
INSERT INTO Diagnose VALUES ('81453','   Atresia    include:  blind ending ureter      ')\g
INSERT INTO Diagnose VALUES ('81454','   Simple ureterocele      ')\g
INSERT INTO Diagnose VALUES ('81455','   Ectopic ureterocele      ')\g
INSERT INTO Diagnose VALUES ('81459','   Other      ')\g
INSERT INTO Diagnose VALUES ('8146','    Anomalous ventral closure      ')\g
INSERT INTO Diagnose VALUES ('81461','   Epispadias      ')\g
INSERT INTO Diagnose VALUES ('81462','   Urachal extension of bladder      ')\g
INSERT INTO Diagnose VALUES ('81463','   Urachal anomaly, other    exclude:  omphalocele (7.1495)      ')\g
INSERT INTO Diagnose VALUES ('81464','   Exstrophy of bladder      ')\g
INSERT INTO Diagnose VALUES ('81465','   Intestinal-vesical fissure (exstrophy of cloaca")"      ')\g
INSERT INTO Diagnose VALUES ('81469','   Other      ')\g
INSERT INTO Diagnose VALUES ('8147','    Anomaly or developmental abnormality of the genital system    exclude:  of placenta (.82), of fetus (.825, .87)      ')\g
INSERT INTO Diagnose VALUES ('81471','   Sexual precocity (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('81472','   Delayed sexual maturation    include:  infantilism, Klinefelter syndrome, Turner syndrome      ')\g
INSERT INTO Diagnose VALUES ('81473','   Pseudohermaphroditism, male      ')\g
INSERT INTO Diagnose VALUES ('81474','   Pseudohermaphroditism, female, nonadrenal   exclude:  adrenogenital syndrome (.543)      ')\g
INSERT INTO Diagnose VALUES ('81476','   Urogenital sinus, cloaca      ')\g
INSERT INTO Diagnose VALUES ('81477','   Undescended testicle (cryptorchidism)      ')\g
INSERT INTO Diagnose VALUES ('81478','   Uterine anomaly, developmental abnormality      ')\g
INSERT INTO Diagnose VALUES ('814781','  Rudimentary uterus, infantile uterus      ')\g
INSERT INTO Diagnose VALUES ('814782','  Duplication (double uterus)      ')\g
INSERT INTO Diagnose VALUES ('814783','  Bicornuate uterus      ')\g
INSERT INTO Diagnose VALUES ('814784','  T-shaped uterus      ')\g
INSERT INTO Diagnose VALUES ('814785','  Incompetent cervix      ')\g
INSERT INTO Diagnose VALUES ('814786','  Hematometria, hydrometria      ')\g
INSERT INTO Diagnose VALUES ('814787','  Hematometrocolpos, hydrometrocolpos      ')\g
INSERT INTO Diagnose VALUES ('814788','  Hydrocolpos, hematocolpos      ')\g
INSERT INTO Diagnose VALUES ('814789','  Other    include:  septate uterus, rudimentary horn   exclude:  anomaly of placenta (.82), of fetus (.825, .87)      ')\g
INSERT INTO Diagnose VALUES ('81479','   Other    include:  true hermaphroditism, anomaly of seminal vesicle or vas deferens, fetal alcohol syndrome   exclude:  adrenogenital syndrome (.543), epispadias (.1461), hypospadias (.1435),  Mullerian duct cyst (.1493)      ')\g
INSERT INTO Diagnose VALUES ('8149','    Miscellaneous      ')\g
INSERT INTO Diagnose VALUES ('81491','   Diverticulum    include:  acquired    exclude:  calyceal diverticulum (.3115)      ')\g
INSERT INTO Diagnose VALUES ('81492','   Congenital pseudotumor (prominent column of Bertin)   exclude:  acquired pseudotumor (.891)      ')\g
INSERT INTO Diagnose VALUES ('81493','   Mullerian duct cyst      ')\g
INSERT INTO Diagnose VALUES ('81494','   Arteriovenous fistula    exclude:  aneurysm (.73)      ')\g
INSERT INTO Diagnose VALUES ('81495','   Hyperplasia, hypertrophy    exclude:  compensatory hypertrophy  (.892)      ')\g
INSERT INTO Diagnose VALUES ('81499','   Other      ')\g
INSERT INTO Diagnose VALUES ('818','     OTHER CONSTITUTIONAL DISORDER    include:  neurofibromatosis, Laurence-Moon-Bardot-Biedel syndrome, Turner syndrome      ')\g
INSERT INTO Diagnose VALUES ('819','     OTHER    exclude:  glycogen storage disease (.59)      ')\g
INSERT INTO Diagnose VALUES ('82','      INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('820','     INFECTION CLASSIFIED BY ORGANISM (for detailed classification see 6.20)      ')\g
INSERT INTO Diagnose VALUES ('8201','    Gram-positive facultative bacterium      ')\g
INSERT INTO Diagnose VALUES ('8202','    Gram-negative bacterium      ')\g
INSERT INTO Diagnose VALUES ('82023','   Escherichia coli      ')\g
INSERT INTO Diagnose VALUES ('8204','    Anaerobic bacterium      ')\g
INSERT INTO Diagnose VALUES ('8205','    Fungus      ')\g
INSERT INTO Diagnose VALUES ('8206','    Virus or Mycoplasma      ')\g
INSERT INTO Diagnose VALUES ('82068','   Human immunodeficiency virus infection (AIDS)      ')\g
INSERT INTO Diagnose VALUES ('8207','    Protozoa or spirochete      ')\g
INSERT INTO Diagnose VALUES ('8208','    Helminth, roundworm, flatworm      ')\g
INSERT INTO Diagnose VALUES ('82084','   Schistosoma      ')\g
INSERT INTO Diagnose VALUES ('8209','    Other      ')\g
INSERT INTO Diagnose VALUES ('821','     GENITOURINARY TRACT INFECTION (for 5th number see box following .219)      ')\g
INSERT INTO Diagnose VALUES ('8211','    Abscess, carbuncle      ')\g
INSERT INTO Diagnose VALUES ('82111','   Gas containing      ')\g
INSERT INTO Diagnose VALUES ('82112','   Communicating with calyceal system      ')\g
INSERT INTO Diagnose VALUES ('82119','   Other    exclude:  tubo-ovarian abscess (.2174)      ')\g
INSERT INTO Diagnose VALUES ('8212','    Pyelonephritis      ')\g
INSERT INTO Diagnose VALUES ('82121','   Acute      ')\g
INSERT INTO Diagnose VALUES ('82122','   Chronic      ')\g
INSERT INTO Diagnose VALUES ('82123','   Atrophic      ')\g
INSERT INTO Diagnose VALUES ('82124','   Calculous      ')\g
INSERT INTO Diagnose VALUES ('82125','   Xanthogranulomatous      ')\g
INSERT INTO Diagnose VALUES ('82129','   Other      ')\g
INSERT INTO Diagnose VALUES ('8213','    Pyonephrosis      ')\g
INSERT INTO Diagnose VALUES ('8214','    Pyelitis, ureteritis, cystitis, prostatitis,urethritis, epididymitis    exclude:  drug induced (.64)      ')\g
INSERT INTO Diagnose VALUES ('8215','    Pyelitis cystica, ureteritis cystica, cystitis cystica      ')\g
INSERT INTO Diagnose VALUES ('8216','    Renal papillitis (papillary, medullary necrosis)      ')\g
INSERT INTO Diagnose VALUES ('8217','    Pelvic inflammatory disease      ')\g
INSERT INTO Diagnose VALUES ('82171','   Salpingitis, endometritis      ')\g
INSERT INTO Diagnose VALUES ('82172','   Hydrosalpinx, hydometra      ')\g
INSERT INTO Diagnose VALUES ('82173','   Pyosalpinx, pyometra      ')\g
INSERT INTO Diagnose VALUES ('82174','   Tubo-ovarian abscess      ')\g
INSERT INTO Diagnose VALUES ('82175','   Blocked Fallopian tube      ')\g
INSERT INTO Diagnose VALUES ('82179','   Other    include:  associated with intrauterine device, tubal spasm      ')\g
INSERT INTO Diagnose VALUES ('8218','    Gas in urinary tract (e.g., emphysematous cystitis)   exclude:  gas-containing abscess (.2111), fistula (.245)      ')\g
INSERT INTO Diagnose VALUES ('8219','    Other    include:  amnionitis, uterine synechiae   exclude:  stricture (.233, .84)      ')\g
INSERT INTO Diagnose VALUES ('822','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('823','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('8231','    Abscess, cavity      ')\g
INSERT INTO Diagnose VALUES ('8233','    Stricture      ')\g
INSERT INTO Diagnose VALUES ('8234','    Autonephrectomy      ')\g
INSERT INTO Diagnose VALUES ('8236','    Tuberculoma      ')\g
INSERT INTO Diagnose VALUES ('8239','    Other   include:  with calcification of seminal vesicle or Fallopian tube      ')\g
INSERT INTO Diagnose VALUES ('824','     SOFT TISSUE INFLAMMATION, SINUS TRACT, FISTULA      ')\g
INSERT INTO Diagnose VALUES ('8241','    Cellulitis      ')\g
INSERT INTO Diagnose VALUES ('8244','    Sinus tract      ')\g
INSERT INTO Diagnose VALUES ('8245','    Fistula      ')\g
INSERT INTO Diagnose VALUES ('82451','   Ureterocolic      ')\g
INSERT INTO Diagnose VALUES ('82452','   Ureteroenteric      ')\g
INSERT INTO Diagnose VALUES ('82453','   Vesicovaginal      ')\g
INSERT INTO Diagnose VALUES ('82454','   Vesicocolic      ')\g
INSERT INTO Diagnose VALUES ('82455','   Vesicoenteric      ')\g
INSERT INTO Diagnose VALUES ('82459','   Other      ')\g
INSERT INTO Diagnose VALUES ('8246','    Lymphadenitis      ')\g
INSERT INTO Diagnose VALUES ('8249','    Other  exclude:  gas in urinary tract due to inflammation (.2111, .218), abscess (.211)      ')\g
INSERT INTO Diagnose VALUES ('829','     OTHER    include:  malakoplakia, Peyronie disease   exclude:  glomerulonephritis (.697)      ')\g
INSERT INTO Diagnose VALUES ('8298','    Infectious complications of AIDS      ')\g
INSERT INTO Diagnose VALUES ('83','      NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('831','     BENIGN NEOPLASM, CYST      ')\g
INSERT INTO Diagnose VALUES ('8311','    Cyst      ')\g
INSERT INTO Diagnose VALUES ('83111','   Simple cyst, solitary      ')\g
INSERT INTO Diagnose VALUES ('83112','   Simple cyst, multiple      ')\g
INSERT INTO Diagnose VALUES ('83113','   Multilocular cyst      ')\g
INSERT INTO Diagnose VALUES ('83114','   Parapelvic cyst      ')\g
INSERT INTO Diagnose VALUES ('83115','   Peripelvic cyst (pyelogenic cyst)    include:  calyceal diverticulum      ')\g
INSERT INTO Diagnose VALUES ('83116','   Paranephric cyst    include:  urinoma      ')\g
INSERT INTO Diagnose VALUES ('83117','   Functional ovarian cyst    include:  corpus luteum cyst, follicular cyst, theca luteum cyst      ')\g
INSERT INTO Diagnose VALUES ('831171','  With hemorrhage      ')\g
INSERT INTO Diagnose VALUES ('831172','  Multilocular      ')\g
INSERT INTO Diagnose VALUES ('831179','  Other      ')\g
INSERT INTO Diagnose VALUES ('83118','   Acquired cystic disease with renal failure      ')\g
INSERT INTO Diagnose VALUES ('83119','   Other   include:  septated cyst      ')\g
INSERT INTO Diagnose VALUES ('8312','    Polycystic, other congenital cystic disease      ')\g
INSERT INTO Diagnose VALUES ('83121','   Polycystic kidneys, dominantly inherited form (adult form)      ')\g
INSERT INTO Diagnose VALUES ('83122','   Polycystic kidneys, recessively inherited form (infantile form)   include:  familial bilateral cystic dysplasia,  microcystic kidney, renal tubular ectasia with hepatic fibrosis      ')\g
INSERT INTO Diagnose VALUES ('83123','   Renal tubular ectasia with hepatic fibrosis      ')\g
INSERT INTO Diagnose VALUES ('83124','   Medullary sponge kidney      ')\g
INSERT INTO Diagnose VALUES ('83125','   Medullary cystic disease, familial juvenile nephronophthisis      ')\g
INSERT INTO Diagnose VALUES ('83126','   Congenital cortical cysts    include:  with syndromes of multiple malformation, with trisomy syndromes, with tuberous sclerosis      ')\g
INSERT INTO Diagnose VALUES ('83127','   Segmental cystic disease      ')\g
INSERT INTO Diagnose VALUES ('83129','   Other    include:  polycystic ovary, multilocular cystic dysplasia    exclude:  multicystic (.1442)      ')\g
INSERT INTO Diagnose VALUES ('8313','    Teratoma, dermoid  include:  malignant teratoma, ovarian, presacral      ')\g
INSERT INTO Diagnose VALUES ('8314','    Hamartoma      ')\g
INSERT INTO Diagnose VALUES ('83141','   Angiomyolipoma      ')\g
INSERT INTO Diagnose VALUES ('83142','   Mesoblastic nephroma      ')\g
INSERT INTO Diagnose VALUES ('83149','   Other      ')\g
INSERT INTO Diagnose VALUES ('8315','    Leiomyoma (fibroid)      ')\g
INSERT INTO Diagnose VALUES ('83151','   With calcification      ')\g
INSERT INTO Diagnose VALUES ('83152','   Pedunculated      ')\g
INSERT INTO Diagnose VALUES ('83153','   Pedunculated with calcification      ')\g
INSERT INTO Diagnose VALUES ('83159','   Other      ')\g
INSERT INTO Diagnose VALUES ('8316','    Prostatic hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('8317','    Adenoma, cystadenoma      ')\g
INSERT INTO Diagnose VALUES ('83171','   Mucinous      ')\g
INSERT INTO Diagnose VALUES ('83172','   Serous      ')\g
INSERT INTO Diagnose VALUES ('83173','   Pseudomucinous      ')\g
INSERT INTO Diagnose VALUES ('83174','   Papillary      ')\g
INSERT INTO Diagnose VALUES ('83179','   Other    exclude:  pseudomyxoma peritonei (7.3196)      ')\g
INSERT INTO Diagnose VALUES ('8318','    Benign mesenchymal neoplasm    include:  hemangioma, lipoma, leiomyoma, fibroma      ')\g
INSERT INTO Diagnose VALUES ('8319','    Other      ')\g
INSERT INTO Diagnose VALUES ('83191','   Hydatidiform mole      ')\g
INSERT INTO Diagnose VALUES ('831911','  With fetus      ')\g
INSERT INTO Diagnose VALUES ('831912','  With theca-lutein cyst(s)      ')\g
INSERT INTO Diagnose VALUES ('831913','  Invasive      ')\g
INSERT INTO Diagnose VALUES ('831919','  Other      ')\g
INSERT INTO Diagnose VALUES ('83192','   Endometriosis      ')\g
INSERT INTO Diagnose VALUES ('83193','   Leukoplakia      ')\g
INSERT INTO Diagnose VALUES ('83199','   Other    include:  ganglioneuroma, lymphangioma, polyp   exclude:  papilloma (.321), benign pheochromocytoma (.328)      ')\g
INSERT INTO Diagnose VALUES ('832','     MALIGNANT NEOPLASM-PRIMARY      ')\g
INSERT INTO Diagnose VALUES ('8321','    Transitional cell carcinoma, papilloma      ')\g
INSERT INTO Diagnose VALUES ('8322','    Squamous cell carcinoma      ')\g
INSERT INTO Diagnose VALUES ('8323','    Cystadenocarcinoma      ')\g
INSERT INTO Diagnose VALUES ('83231','   Papillary      ')\g
INSERT INTO Diagnose VALUES ('83232','   Serous      ')\g
INSERT INTO Diagnose VALUES ('83233','   Mucinous      ')\g
INSERT INTO Diagnose VALUES ('83234','   Endometrioid      ')\g
INSERT INTO Diagnose VALUES ('83239','   Other      ')\g
INSERT INTO Diagnose VALUES ('8324','    Adenocarcinoma, renal cell carcinoma      ')\g
INSERT INTO Diagnose VALUES ('8325','    Neuroblastoma, ganglioneuroblastoma      ')\g
INSERT INTO Diagnose VALUES ('8326','    Wilms tumor      ')\g
INSERT INTO Diagnose VALUES ('8327','    Sarcoma, other    include:  rhabdomyosarcoma, sarcoma botryoides      ')\g
INSERT INTO Diagnose VALUES ('8328','    Pheochromocytoma    include:  benign pheochromocytoma, multiple endocrine neoplasis type II      ')\g
INSERT INTO Diagnose VALUES ('8329','    Other    include:  choriocarcinoma (chorioepithelioma),  seminoma, neoplasm associated with AIDS    exclude:  leukemia, lymphoma (.34), malignant teratoma (.313)      ')\g
INSERT INTO Diagnose VALUES ('833','     MALIGNANT NEOPLASM-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('8336','    Invasion from adjacent lesion      ')\g
INSERT INTO Diagnose VALUES ('8339','    Other    include:  Krukenberg tumor    exclude:  leukemia, lymphoma (.34)      ')\g
INSERT INTO Diagnose VALUES ('834','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('8341','    Leukemia   include:  chloroma      ')\g
INSERT INTO Diagnose VALUES ('8342','    Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('8343','    Non-Hodgkin lymphoma      ')\g
INSERT INTO Diagnose VALUES ('83431','   Lymphocytic well differentiated      ')\g
INSERT INTO Diagnose VALUES ('83432','   Nodular      ')\g
INSERT INTO Diagnose VALUES ('83433','   Diffuse      ')\g
INSERT INTO Diagnose VALUES ('83434','   Histiocytic      ')\g
INSERT INTO Diagnose VALUES ('83435','   Burkitt      ')\g
INSERT INTO Diagnose VALUES ('83439','   Other      ')\g
INSERT INTO Diagnose VALUES ('8345','    Myeloma      ')\g
INSERT INTO Diagnose VALUES ('8346','    Kaposi syndrome      ')\g
INSERT INTO Diagnose VALUES ('8349','    Other      ')\g
INSERT INTO Diagnose VALUES ('835','     NEOPLASM ARISING IN PREEXISTING BENIGN DISORDER      ')\g
INSERT INTO Diagnose VALUES ('836','     EXTRINSIC MASS AFFECTING GENITOURINARY STRUCTURE, ARISING IN      ')\g
INSERT INTO Diagnose VALUES ('8361','    Urinary tract      ')\g
INSERT INTO Diagnose VALUES ('8362','    Genital tract      ')\g
INSERT INTO Diagnose VALUES ('8363','    Gastrointestinal tract, hollow viscus      ')\g
INSERT INTO Diagnose VALUES ('8364','    Liver, spleen, pancreas      ')\g
INSERT INTO Diagnose VALUES ('8369','    Other    include:  retroperitoneal mass      ')\g
INSERT INTO Diagnose VALUES ('837','     RETROPERITONEAL, PELVIC MASS ON PLAIN FILM      ')\g
INSERT INTO Diagnose VALUES ('839','     OTHER    include:  recurrent neoplasm   exclude:  leukoplakia (.319)      ')\g
INSERT INTO Diagnose VALUES ('84','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('841','     ACUTE INJURY      ')\g
INSERT INTO Diagnose VALUES ('8411','    Fracture, laceration or contusion      ')\g
INSERT INTO Diagnose VALUES ('8412','    Extravasation of contrast media      ')\g
INSERT INTO Diagnose VALUES ('8413','    Hematoma      ')\g
INSERT INTO Diagnose VALUES ('84131','   Subcapsular      ')\g
INSERT INTO Diagnose VALUES ('84132','   Intrarenal      ')\g
INSERT INTO Diagnose VALUES ('84133','   Perinephric      ')\g
INSERT INTO Diagnose VALUES ('84134','   Retroperitoneal      ')\g
INSERT INTO Diagnose VALUES ('84139','   Other      ')\g
INSERT INTO Diagnose VALUES ('8414','    Blood clot      ')\g
INSERT INTO Diagnose VALUES ('8415','    Urinoma      ')\g
INSERT INTO Diagnose VALUES ('8419','    Other    include:  sexual abuse  exclude:  vascular trauma (.48), adrenal hemorrhage (.546)      ')\g
INSERT INTO Diagnose VALUES ('842','     COMPLICATION OF DIALYSIS      ')\g
INSERT INTO Diagnose VALUES ('843','     COMPLICATION OF INJURY    include:  fluid collection (e.g., lymphocyst, urinoma  except adjacent to transplanted kidney) (.4556)    exclude:  vascular (.48)      ')\g
INSERT INTO Diagnose VALUES ('844','     COMPLICATION OF ANGIOGRAPHY (see also 9.44)      ')\g
INSERT INTO Diagnose VALUES ('845','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('8451','    Resection, partial or total, of an organ      ')\g
INSERT INTO Diagnose VALUES ('8452','    Implantation of radiation source      ')\g
INSERT INTO Diagnose VALUES ('8453','    Anastomosis    include:  colon conduit, ileal conduit (ileal bladder)      ')\g
INSERT INTO Diagnose VALUES ('8454','    Cutaneous stoma    include:  cystostomy, nephrostomy, ureterostomy      ')\g
INSERT INTO Diagnose VALUES ('8455','    Renal transplant, complication of transplant      ')\g
INSERT INTO Diagnose VALUES ('84551','   Satisfactory transplant      ')\g
INSERT INTO Diagnose VALUES ('84552','   Rejection      ')\g
INSERT INTO Diagnose VALUES ('84553','   Tubular necrosis      ')\g
INSERT INTO Diagnose VALUES ('84554','   Urinary obstruction      ')\g
INSERT INTO Diagnose VALUES ('84555','   Infarct      ')\g
INSERT INTO Diagnose VALUES ('84556','   Perinephric fluid collection    include:  lymphocele      ')\g
INSERT INTO Diagnose VALUES ('84557','   Renal artery stenosis      ')\g
INSERT INTO Diagnose VALUES ('84559','   Other complication      ')\g
INSERT INTO Diagnose VALUES ('8456','    Endarterectomy      ')\g
INSERT INTO Diagnose VALUES ('8457','    Bypass graft      ')\g
INSERT INTO Diagnose VALUES ('84571','   Splenorenal shunt      ')\g
INSERT INTO Diagnose VALUES ('8458','    Complication of surgery or interventional procedure   include:  of biopsy, of aspiration   exclude:  of angiography (.44)      ')\g
INSERT INTO Diagnose VALUES ('8459','    Other    include:  sympathectomy, artifical sphincter,reimplantation of ureter    exclude:  arteriovenous fistula (.494)      ')\g
INSERT INTO Diagnose VALUES ('846','     FOREIGN BODY OR MEDICATION      ')\g
INSERT INTO Diagnose VALUES ('8461','    Opaque      ')\g
INSERT INTO Diagnose VALUES ('84611','   Catheter or other tube      ')\g
INSERT INTO Diagnose VALUES ('84612','   Catheter (or other tube) in satisfactory position      ')\g
INSERT INTO Diagnose VALUES ('84613','   Catheter (or other tube) in unsatisfactory position      ')\g
INSERT INTO Diagnose VALUES ('84617','   Complication  of catheter or tube      ')\g
INSERT INTO Diagnose VALUES ('84619','   Other  exclude:  intrauterine device  (.463)      ')\g
INSERT INTO Diagnose VALUES ('8462','    Nonopaque      ')\g
INSERT INTO Diagnose VALUES ('8463','    Intrauterine device (see .2179)      ')\g
INSERT INTO Diagnose VALUES ('84631','   Normally positioned      ')\g
INSERT INTO Diagnose VALUES ('84632','   Abnormally positioned in uterus      ')\g
INSERT INTO Diagnose VALUES ('84633','   Extrauterine      ')\g
INSERT INTO Diagnose VALUES ('8469','    Other      ')\g
INSERT INTO Diagnose VALUES ('847','     EFFECT OF RADIATION      ')\g
INSERT INTO Diagnose VALUES ('848','     VASCULAR TRAUMA (see also 9.41)      ')\g
INSERT INTO Diagnose VALUES ('8481','    Vascular occlusion    exclude:  in transplanted kidney (.4557)      ')\g
INSERT INTO Diagnose VALUES ('84811','   Partial (stenosis)      ')\g
INSERT INTO Diagnose VALUES ('84812','   Complete      ')\g
INSERT INTO Diagnose VALUES ('8482','    Laceration    include:  intimal tear, dissection      ')\g
INSERT INTO Diagnose VALUES ('8489','    Other    exclude:  complication of angiography (.44), aneurysm (.73, .74)      ')\g
INSERT INTO Diagnose VALUES ('849','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('8491','    Effects of lithotripsy      ')\g
INSERT INTO Diagnose VALUES ('8494','    Arteriovenous fistula, post-traumatic   include:  following biopsy, other postopera-tive      ')\g
INSERT INTO Diagnose VALUES ('8499','    Other  include:  intravascular gas      ')\g
INSERT INTO Diagnose VALUES ('85','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('853','     PARATHYROID DISORDER    include:  nephrocalcinosis, calculus      ')\g
INSERT INTO Diagnose VALUES ('854','     ADRENAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('8541','    Cushing syndrome, endogenous      ')\g
INSERT INTO Diagnose VALUES ('85411','   Adrenal neoplasm (see also .31, .32)      ')\g
INSERT INTO Diagnose VALUES ('85412','   Adrenal hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('8542','    Cushing syndrome, exogenous      ')\g
INSERT INTO Diagnose VALUES ('8543','    Adrenogenital syndrome      ')\g
INSERT INTO Diagnose VALUES ('8544','    Aldosteronism      ')\g
INSERT INTO Diagnose VALUES ('8545','    Adrenal atrophy    include:  Addison disease      ')\g
INSERT INTO Diagnose VALUES ('8546','    Adrenal hemorrhage      ')\g
INSERT INTO Diagnose VALUES ('8549','    Other    exclude:  neoplasm (.3)      ')\g
INSERT INTO Diagnose VALUES ('858','     INTOXICATION, POISONING      ')\g
INSERT INTO Diagnose VALUES ('8581','    Heavy metal intoxication    include:  lead nephropathy, mercury      ')\g
INSERT INTO Diagnose VALUES ('8583','    Hypervitaminosis D      ')\g
INSERT INTO Diagnose VALUES ('8584','    Other toxic nephropathy    include:  carbon tetrachloride      ')\g
INSERT INTO Diagnose VALUES ('8589','    Other      ')\g
INSERT INTO Diagnose VALUES ('859','     OTHER    include:  glycogen storage disease   exclude:  endometriosis (.319)      ')\g
INSERT INTO Diagnose VALUES ('86','      OTHER GENERALIZED SYSTEMIC DISORDER      ')\g
INSERT INTO Diagnose VALUES ('861','     CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('8612','    Lupus erythematosus    include:  lupus nephritis      ')\g
INSERT INTO Diagnose VALUES ('8613','    Progressive systemic sclerosis (scleroderma)      ')\g
INSERT INTO Diagnose VALUES ('8619','    Other      ')\g
INSERT INTO Diagnose VALUES ('862','     VASCULITIS      ')\g
INSERT INTO Diagnose VALUES ('8621','    Polyarteritis (periarteritis) nodosa      ')\g
INSERT INTO Diagnose VALUES ('8622','    Wegener granulomatosis      ')\g
INSERT INTO Diagnose VALUES ('8624','    Hypersensitivity angiitis      ')\g
INSERT INTO Diagnose VALUES ('8625','    Takayasu arteritis or related condition      ')\g
INSERT INTO Diagnose VALUES ('8626','    Vasculitis of drug abuse      ')\g
INSERT INTO Diagnose VALUES ('8629','    Other   include:  mucocutaneous lymph node syndrome      ')\g
INSERT INTO Diagnose VALUES ('863','     ALLERGIC STATE      ')\g
INSERT INTO Diagnose VALUES ('864','     COMPLICATION OF DRUG THERAPY, DRUG USE   include:  cyclophosphamide cystitis, renal tubular block from sulfa crystals    exclude:  effect of steroid therapy (.542), vasculitis of drug abuse (.626)      ')\g
INSERT INTO Diagnose VALUES ('865','     URINARY TRACT MANIFESTATION OF VASCULAR OR HEMATOLOGICAL DISORDER      ')\g
INSERT INTO Diagnose VALUES ('8651','    Sickle cell disease or variant      ')\g
INSERT INTO Diagnose VALUES ('8652','    Shock    include:  shock nephrogram      ')\g
INSERT INTO Diagnose VALUES ('8653','    Prerenal azotemia      ')\g
INSERT INTO Diagnose VALUES ('8654','    Cortical necrosis      ')\g
INSERT INTO Diagnose VALUES ('8655','    Renal tubular necrosis (see 9.765)  exclude:  in transplanted kidney (.4553)      ')\g
INSERT INTO Diagnose VALUES ('8656','    Hemophilia      ')\g
INSERT INTO Diagnose VALUES ('8657','    Stasis nephrogram with Tamm-Horsfall proteinuria      ')\g
INSERT INTO Diagnose VALUES ('8658','    Disseminated intravascular coagulation      ')\g
INSERT INTO Diagnose VALUES ('8659','    Other    include:  polycythemia      ')\g
INSERT INTO Diagnose VALUES ('868','     OTHER INFILTRATIVE DISORDER    include:  amyloid   exclude:  glycogen storage disease (.59)      ')\g
INSERT INTO Diagnose VALUES ('869','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('8697','    Glomerulonephritis      ')\g
INSERT INTO Diagnose VALUES ('8698','    Nephrosis, nephrotic syndrome    exclude:  toxic nephrosis (.584), heavy metal poisoning (.581)      ')\g
INSERT INTO Diagnose VALUES ('8699','    Other      ')\g
INSERT INTO Diagnose VALUES ('87','      NONOPACIFIED OR POORLY OPACIFIED KIDNEY, VASCULAR DISORDER If diagnosed by angiography, code under .9      ')\g
INSERT INTO Diagnose VALUES ('871','     NONOPACIFIED OR POORLY OPACIFIED KIDNEY      ')\g
INSERT INTO Diagnose VALUES ('8711','    Unilateral      ')\g
INSERT INTO Diagnose VALUES ('8712','    Bilateral      ')\g
INSERT INTO Diagnose VALUES ('872','     CIRCULATORY DISORDER (PLAIN FILM AND UROGRAPHIC MANIFESTATIONS)    include:  renovascular hypertension   exclude:  in transplanted kidney (.4553)      ')\g
INSERT INTO Diagnose VALUES ('873','     ANEURYSM, PLAIN FILM DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('874','     INTRAMURAL HEMATOMA WITH DISSECTION (DISSECTING ANEURYSM)      ')\g
INSERT INTO Diagnose VALUES ('875','     VENOUS DISORDER      ')\g
INSERT INTO Diagnose VALUES ('8751','    Renal vein thrombosis      ')\g
INSERT INTO Diagnose VALUES ('8755','    Varicosities    include:  ovarian vein syndrome      ')\g
INSERT INTO Diagnose VALUES ('8759','    Other      ')\g
INSERT INTO Diagnose VALUES ('877','     EMBOLUS, INFARCT    exclude:  in transplanted kidney (.4555)      ')\g
INSERT INTO Diagnose VALUES ('879','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('88','      MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('881','     CALCULUS, CALCIFICATION      ')\g
INSERT INTO Diagnose VALUES ('8811','    Calculus (opaque or nonopaque)    exclude:  with hyperparathyroidism (.53)      ')\g
INSERT INTO Diagnose VALUES ('8812','    Nephrocalcinosis   include:  oxalosis    exclude:  with hyperparathyroidism (.53)      ')\g
INSERT INTO Diagnose VALUES ('8813','    Vascular calcification, retroperitoneal   include:  aneurysm      ')\g
INSERT INTO Diagnose VALUES ('8814','    Calcification in neoplasm      ')\g
INSERT INTO Diagnose VALUES ('8815','    Calcification in infection      ')\g
INSERT INTO Diagnose VALUES ('8816','    Calcification in cyst      ')\g
INSERT INTO Diagnose VALUES ('8817','    Calcification of unknown etiology      ')\g
INSERT INTO Diagnose VALUES ('8819','    Other    include:  milk of calcium urine   exclude:  lithopedion (.823), placental (.1394), seminal vesicle or Fallopian tube (.239)      ')\g
INSERT INTO Diagnose VALUES ('882','     OBSTETRICAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('8821','    Dystocia, cephalopelvic disproportion      ')\g
INSERT INTO Diagnose VALUES ('8822','    Abnormal position or presentation      ')\g
INSERT INTO Diagnose VALUES ('88221','   Occiput posterior      ')\g
INSERT INTO Diagnose VALUES ('88222','   Brow, face, chin presentation      ')\g
INSERT INTO Diagnose VALUES ('88223','   Breech (all types)      ')\g
INSERT INTO Diagnose VALUES ('88224','   Transverse lie      ')\g
INSERT INTO Diagnose VALUES ('88229','   Other      ')\g
INSERT INTO Diagnose VALUES ('8823','    Ectopic pregnancy      ')\g
INSERT INTO Diagnose VALUES ('88231','   Abdominal      ')\g
INSERT INTO Diagnose VALUES ('88232','   Tubal      ')\g
INSERT INTO Diagnose VALUES ('88233','   Cornual      ')\g
INSERT INTO Diagnose VALUES ('88234','   Cervical      ')\g
INSERT INTO Diagnose VALUES ('88239','   Other    include:  lithopedion      ')\g
INSERT INTO Diagnose VALUES ('8824','    Placental abnormality      ')\g
INSERT INTO Diagnose VALUES ('88241','   Placenta praevia      ')\g
INSERT INTO Diagnose VALUES ('882411','  Central, complete      ')\g
INSERT INTO Diagnose VALUES ('882412','  Eccentric      ')\g
INSERT INTO Diagnose VALUES ('882413','  Marginal      ')\g
INSERT INTO Diagnose VALUES ('882419','  Other      ')\g
INSERT INTO Diagnose VALUES ('88242','   Abruptio placenta      ')\g
INSERT INTO Diagnose VALUES ('88243','   Postmature placenta, thickened placenta, placental calcification,placental cyst(s)      ')\g
INSERT INTO Diagnose VALUES ('88244','   Placental migration""      ')\g
INSERT INTO Diagnose VALUES ('88245','   Hydropic degeneration, partial mole      ')\g
INSERT INTO Diagnose VALUES ('88246','   Mole    exclude:  partial mole (.8245)      ')\g
INSERT INTO Diagnose VALUES ('88249','   Other      ')\g
INSERT INTO Diagnose VALUES ('8825','    Fetal death, blighted ovum      ')\g
INSERT INTO Diagnose VALUES ('88251','   Blighted ovum (empty gestational sac)      ')\g
INSERT INTO Diagnose VALUES ('88252','   Complete abortion      ')\g
INSERT INTO Diagnose VALUES ('88253','   Incomplete abortion      ')\g
INSERT INTO Diagnose VALUES ('88254','   Missed abortion      ')\g
INSERT INTO Diagnose VALUES ('88255','   Retained products of conception      ')\g
INSERT INTO Diagnose VALUES ('88256','   Nongrowth of gestational sac      ')\g
INSERT INTO Diagnose VALUES ('88258','   Late fetal death, stillborn infant [code also fetal abnormality, if present (.87)]      ')\g
INSERT INTO Diagnose VALUES ('88259','   Other    exclude:  fetal abnormality (.87)      ')\g
INSERT INTO Diagnose VALUES ('8826','    Pregnancy with abnormality of uterus or fetal membranes      ')\g
INSERT INTO Diagnose VALUES ('88261','   With congenital anomaly of uterus      ')\g
INSERT INTO Diagnose VALUES ('88262','   With uterine tumor    include:  leiomyoma      ')\g
INSERT INTO Diagnose VALUES ('88263','   With intrauterine device      ')\g
INSERT INTO Diagnose VALUES ('88264','   Low implantation of gestational sac      ')\g
INSERT INTO Diagnose VALUES ('88265','   Polyhydramnios      ')\g
INSERT INTO Diagnose VALUES ('88266','   Oligohydramnios      ')\g
INSERT INTO Diagnose VALUES ('88269','   Other      ')\g
INSERT INTO Diagnose VALUES ('8828','    Abnormality of pelvic contour      ')\g
INSERT INTO Diagnose VALUES ('88281','   Flat (platypelloid)      ')\g
INSERT INTO Diagnose VALUES ('88282','   Android      ')\g
INSERT INTO Diagnose VALUES ('88283','   Anthropoid      ')\g
INSERT INTO Diagnose VALUES ('88284','   Rachitic      ')\g
INSERT INTO Diagnose VALUES ('88285','   Generally contracted      ')\g
INSERT INTO Diagnose VALUES ('88286','   Sacral, lumbar deformity      ')\g
INSERT INTO Diagnose VALUES ('88289','   Other      ')\g
INSERT INTO Diagnose VALUES ('8829','    Other   exclude:  fetal abnormality (.87)      ')\g
INSERT INTO Diagnose VALUES ('883','     OTHER ABNORMALITY OF URINARY BLADDER      ')\g
INSERT INTO Diagnose VALUES ('8831','    Neurogenic      ')\g
INSERT INTO Diagnose VALUES ('88311','   Sensory      ')\g
INSERT INTO Diagnose VALUES ('88312','   Automatic      ')\g
INSERT INTO Diagnose VALUES ('88313','   Autonomous      ')\g
INSERT INTO Diagnose VALUES ('88319','   Other   include:  pseudoneurogenic (Hinmann) bladder (also code cause)      ')\g
INSERT INTO Diagnose VALUES ('8832','    Cystocele      ')\g
INSERT INTO Diagnose VALUES ('8833','    Bladder trabeculation      ')\g
INSERT INTO Diagnose VALUES ('8834','    Residual urine      ')\g
INSERT INTO Diagnose VALUES ('8835','    Stress incontinence      ')\g
INSERT INTO Diagnose VALUES ('8836','    Thickening of bladder wall      ')\g
INSERT INTO Diagnose VALUES ('8837','    Bullous edema (see .214)      ')\g
INSERT INTO Diagnose VALUES ('8839','    Other    include:  bladder-sphincter dyssynergia    exclude:  cystitis (.214), diverticulum (.1491)      ')\g
INSERT INTO Diagnose VALUES ('884','     URINARY TRACT OBSTRUCTION, DILATATION, HYDRONEPHROSIS, HYDROURETER (also code cause)   exclude:  in transplanted kidney (.4554)      ')\g
INSERT INTO Diagnose VALUES ('8841','    Calyceal      ')\g
INSERT INTO Diagnose VALUES ('8842','    Ureteropelvic (vessel, stricture, band, other)      ')\g
INSERT INTO Diagnose VALUES ('8843','    Ureteral obstruction, intrinsic    include:  stricture   exclude:  tuberculosis (.233), megaloureter (.849)      ')\g
INSERT INTO Diagnose VALUES ('8844','    Ureteral obstruction, extrinsic    include:  mass   exclude:  retroperitoneal fibrosis (.893)      ')\g
INSERT INTO Diagnose VALUES ('8845','    Ureterovesical      ')\g
INSERT INTO Diagnose VALUES ('8846','    Bladder neck, prostatic      ')\g
INSERT INTO Diagnose VALUES ('8847','    Urethral, other    exclude:  valve (.1451), meatal stenosis (.1452)      ')\g
INSERT INTO Diagnose VALUES ('8848','    Dilatation secondary to infection, reflux      ')\g
INSERT INTO Diagnose VALUES ('8849','    Other    include:  back pressure atrophy, infrequent voider, megaloureter      ')\g
INSERT INTO Diagnose VALUES ('885','     REFLUX FROM BLADDER      ')\g
INSERT INTO Diagnose VALUES ('886','     ABNORMAL FETAL MEASUREMENT      ')\g
INSERT INTO Diagnose VALUES ('8861','    Crown rump      ')\g
INSERT INTO Diagnose VALUES ('8862','    Skull      ')\g
INSERT INTO Diagnose VALUES ('8863','    Thorax      ')\g
INSERT INTO Diagnose VALUES ('8864','    Abdomen      ')\g
INSERT INTO Diagnose VALUES ('8869','    Other    exclude:  dwarfism (.871), short limbs (.8731, .8732)      ')\g
INSERT INTO Diagnose VALUES ('887','     FETAL ABNORMALITY (PRENATAL DIAGNOSIS)      ')\g
INSERT INTO Diagnose VALUES ('8871','    Dwarfism, intrauterine growth retardation   exclude:  osteogenesis imperfecta (.8733)      ')\g
INSERT INTO Diagnose VALUES ('88711','   Symmetrical      ')\g
INSERT INTO Diagnose VALUES ('88712','   Asymmetrical      ')\g
INSERT INTO Diagnose VALUES ('88713','   Disproportionately short limbs (see .8732)      ')\g
INSERT INTO Diagnose VALUES ('88714','   Disproportionately short trunk      ')\g
INSERT INTO Diagnose VALUES ('88719','   Other    exclude:  osteogenesis imperfecta (.8733)      ')\g
INSERT INTO Diagnose VALUES ('8872','    Excess fetal fluid collection      ')\g
INSERT INTO Diagnose VALUES ('88721','   Pleural effusion      ')\g
INSERT INTO Diagnose VALUES ('88722','   Ascites      ')\g
INSERT INTO Diagnose VALUES ('88723','   Hydrops    include:  erythroblastosis fetalis      ')\g
INSERT INTO Diagnose VALUES ('88724','   Cystic hygroma, lymphangioma      ')\g
INSERT INTO Diagnose VALUES ('88729','   Other    exclude:  polyhydramnios (.8265), hydronephrosis (.8772)      ')\g
INSERT INTO Diagnose VALUES ('8873','    Skeletal      ')\g
INSERT INTO Diagnose VALUES ('88731','   One limb hypoplasia or agenesis      ')\g
INSERT INTO Diagnose VALUES ('88732','   Multiple limb hypoplasia or agenesis (see .8713)      ')\g
INSERT INTO Diagnose VALUES ('88733','   Osteogenesis imperfecta      ')\g
INSERT INTO Diagnose VALUES ('88734','   Hypophosphatasia      ')\g
INSERT INTO Diagnose VALUES ('88739','   Other      ')\g
INSERT INTO Diagnose VALUES ('8874','    Neurological      ')\g
INSERT INTO Diagnose VALUES ('88741','   Anencephaly      ')\g
INSERT INTO Diagnose VALUES ('88742','   Microcephaly, micrencephaly      ')\g
INSERT INTO Diagnose VALUES ('88743','   Holoprosencephaly      ')\g
INSERT INTO Diagnose VALUES ('88744','   Hydrocephalus      ')\g
INSERT INTO Diagnose VALUES ('887441','  Hydranencephaly      ')\g
INSERT INTO Diagnose VALUES ('887442','  Porencephaly      ')\g
INSERT INTO Diagnose VALUES ('887443','  Dandy Walker cyst      ')\g
INSERT INTO Diagnose VALUES ('887449','  Other      ')\g
INSERT INTO Diagnose VALUES ('88745','   Encephalocele      ')\g
INSERT INTO Diagnose VALUES ('88746','   Meningomyelocele, meningocele      ')\g
INSERT INTO Diagnose VALUES ('88747','   Arnold-Chiari malformation      ')\g
INSERT INTO Diagnose VALUES ('88748','   Intracranial hemorrhage      ')\g
INSERT INTO Diagnose VALUES ('88749','   Other    include:  vein of Galen malformation      ')\g
INSERT INTO Diagnose VALUES ('8875','    Thoracic      ')\g
INSERT INTO Diagnose VALUES ('88751','   Congenital heart disease      ')\g
INSERT INTO Diagnose VALUES ('88752','   Dysrhythmia      ')\g
INSERT INTO Diagnose VALUES ('88753','   Heart, other      ')\g
INSERT INTO Diagnose VALUES ('88754','   Diaphragmatic hernia      ')\g
INSERT INTO Diagnose VALUES ('88755','   Pulmonary aplasia, hypoplasia      ')\g
INSERT INTO Diagnose VALUES ('88756','   Adenomatoid malformation      ')\g
INSERT INTO Diagnose VALUES ('88758','   Lung, other      ')\g
INSERT INTO Diagnose VALUES ('88759','   Other    include:  mediastinal    exclude:  pleural effusion (.8721)      ')\g
INSERT INTO Diagnose VALUES ('8876','    Gastrointestinal      ')\g
INSERT INTO Diagnose VALUES ('88761','   Omphalocele, gastroschisis      ')\g
INSERT INTO Diagnose VALUES ('88762','   Esophageal atresia      ')\g
INSERT INTO Diagnose VALUES ('88763','   Obstruction, other      ')\g
INSERT INTO Diagnose VALUES ('88764','   Meconium cyst      ')\g
INSERT INTO Diagnose VALUES ('88765','   Cyst, other    include:  duplication, choledochal      ')\g
INSERT INTO Diagnose VALUES ('88769','   Other    include:  meconium peritonitis      ')\g
INSERT INTO Diagnose VALUES ('8877','    Renal/ureteral      ')\g
INSERT INTO Diagnose VALUES ('88771','   Duplication      ')\g
INSERT INTO Diagnose VALUES ('88772','   Hydronephrosis, hydroureter      ')\g
INSERT INTO Diagnose VALUES ('88773','   Polycystic kidney, adult      ')\g
INSERT INTO Diagnose VALUES ('88774','   Polycystic kidney, infantile      ')\g
INSERT INTO Diagnose VALUES ('88775','   Multicystic kidney      ')\g
INSERT INTO Diagnose VALUES ('88776','   Tumor      ')\g
INSERT INTO Diagnose VALUES ('88779','   Other    include:  agenesis, hypoplasia      ')\g
INSERT INTO Diagnose VALUES ('8878','    Genitourinary, adrenal, other      ')\g
INSERT INTO Diagnose VALUES ('88781','   Urethral obstruction    include:  posterior urethral valve      ')\g
INSERT INTO Diagnose VALUES ('88782','   Hypoplasia of abdominal musculature (Eagle-Barrett syndrome, prune belly")"      ')\g
INSERT INTO Diagnose VALUES ('88783','   Bladder or urethra, other      ')\g
INSERT INTO Diagnose VALUES ('88784','   Adrenal hemorrhage or tumor      ')\g
INSERT INTO Diagnose VALUES ('88785','   Adrenal, other      ')\g
INSERT INTO Diagnose VALUES ('88789','   Other      ')\g
INSERT INTO Diagnose VALUES ('8879','    Other    include:  abdominal mass in fetus of undetermined location, conjoint twins      ')\g
INSERT INTO Diagnose VALUES ('888','     IMPOTENCE      ')\g
INSERT INTO Diagnose VALUES ('8881','    Secondary to arterial disease      ')\g
INSERT INTO Diagnose VALUES ('8882','    Secondary to venous incompetence      ')\g
INSERT INTO Diagnose VALUES ('8883','    Complication of trauma      ')\g
INSERT INTO Diagnose VALUES ('8889','    Other      ')\g
INSERT INTO Diagnose VALUES ('889','     OTHER URINARY TRACT ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('8891','    Pseudotumor, acquired (also code cause)   exclude:  congenital pseudotumor (.1492)      ')\g
INSERT INTO Diagnose VALUES ('8892','    Compensatory hypertrophy   include:  renal size      ')\g
INSERT INTO Diagnose VALUES ('8893','    Retroperitoneal fibrosis      ')\g
INSERT INTO Diagnose VALUES ('8894','    Renal sinus lipomatosis      ')\g
INSERT INTO Diagnose VALUES ('8895','    Pelvic lipomatosis      ')\g
INSERT INTO Diagnose VALUES ('8896','    Renal enlargement of undetermined etiology      ')\g
INSERT INTO Diagnose VALUES ('8897','    Small kidney of undetermined etiology      ')\g
INSERT INTO Diagnose VALUES ('8898','    Unusual gas collection    include:  retroperitoneal pneumatosis    exclude:  abscess (.211), gas in the urinary tract (.218), intravascular (.218)      ')\g
INSERT INTO Diagnose VALUES ('8899','    Other      ')\g
INSERT INTO Diagnose VALUES ('89','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('891','     FUNDAMENTAL OBSERVATION    include:  peristaltic activity, total body opacification      ')\g
INSERT INTO Diagnose VALUES ('8911','    Fetal sex determination      ')\g
INSERT INTO Diagnose VALUES ('8919','    Other      ')\g
INSERT INTO Diagnose VALUES ('892','     ANATOMICAL DETAIL      ')\g
INSERT INTO Diagnose VALUES ('893','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('894','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('899','     OTHER  include:  phenomenon related to technique of examination  exclude:  renal backflow (81.132), perforation by instrumentation (.458)      ')\g
INSERT INTO Diagnose VALUES ('91','      NORMAL, TECHNIQUE, ANOMALY      ')\g
INSERT INTO Diagnose VALUES ('912','     SPECIAL TECHNIQUE, PROCEDURE, PROJECTION      ')\g
INSERT INTO Diagnose VALUES ('9121','    Aortography      ')\g
INSERT INTO Diagnose VALUES ('91211','   Catheter      ')\g
INSERT INTO Diagnose VALUES ('91212','   Translumbar      ')\g
INSERT INTO Diagnose VALUES ('91213','   Intravenous      ')\g
INSERT INTO Diagnose VALUES ('9122','    Arteriography, catheter      ')\g
INSERT INTO Diagnose VALUES ('91221','   Nonselective      ')\g
INSERT INTO Diagnose VALUES ('91222','   Selective      ')\g
INSERT INTO Diagnose VALUES ('91223','   Subselective      ')\g
INSERT INTO Diagnose VALUES ('91224','   Umbilical      ')\g
INSERT INTO Diagnose VALUES ('91225','   Intravenous      ')\g
INSERT INTO Diagnose VALUES ('9123','    Direct needle puncture      ')\g
INSERT INTO Diagnose VALUES ('9124','    Venography      ')\g
INSERT INTO Diagnose VALUES ('91241','   Splenoportography      ')\g
INSERT INTO Diagnose VALUES ('91242','   Portography, arterial method      ')\g
INSERT INTO Diagnose VALUES ('91243','   Operative venography    include: umbilical venography      ')\g
INSERT INTO Diagnose VALUES ('91244','   Intraosseous venography      ')\g
INSERT INTO Diagnose VALUES ('91245','   Selective venography      ')\g
INSERT INTO Diagnose VALUES ('91246','   Umbilical      ')\g
INSERT INTO Diagnose VALUES ('91249','   Other  include: azygography      ')\g
INSERT INTO Diagnose VALUES ('9125','    Lymphangiography (see also .8)      ')\g
INSERT INTO Diagnose VALUES ('9126','    Biopsy or interventional   exclude: angioplasty (.128)      ')\g
INSERT INTO Diagnose VALUES ('91261','   Biopsy      ')\g
INSERT INTO Diagnose VALUES ('91262','   Aspiration      ')\g
INSERT INTO Diagnose VALUES ('91263','   Catheter drainage      ')\g
INSERT INTO Diagnose VALUES ('91264','   Embolization      ')\g
INSERT INTO Diagnose VALUES ('91265','   Thrombolytic infusion      ')\g
INSERT INTO Diagnose VALUES ('91266','   Chemotherapeutic infusion      ')\g
INSERT INTO Diagnose VALUES ('91267','   Insertion of venous filter      ')\g
INSERT INTO Diagnose VALUES ('91268','   Insertion of stent (see also angioplasty with stent .1286)      ')\g
INSERT INTO Diagnose VALUES ('91269','   Other   include: intravascular foreign body removal, venous sampling      ')\g
INSERT INTO Diagnose VALUES ('9127','    Pharmacological adjunct   exclude: chemotherapeutic infusion (.1266)      ')\g
INSERT INTO Diagnose VALUES ('91271','   Vasoconstrictor      ')\g
INSERT INTO Diagnose VALUES ('91272','   Vasodilator      ')\g
INSERT INTO Diagnose VALUES ('91273','   Anticoagulant      ')\g
INSERT INTO Diagnose VALUES ('91274','   Fibrinolytic agent      ')\g
INSERT INTO Diagnose VALUES ('91279','   Other      ')\g
INSERT INTO Diagnose VALUES ('9128','    Angioplasty      ')\g
INSERT INTO Diagnose VALUES ('91281','   Catheter (Dotter technique)      ')\g
INSERT INTO Diagnose VALUES ('91282','   Balloon catheter      ')\g
INSERT INTO Diagnose VALUES ('91283','   Use of atherectomy device      ')\g
INSERT INTO Diagnose VALUES ('91284','   Thermal laser      ')\g
INSERT INTO Diagnose VALUES ('91285','   Cold laser      ')\g
INSERT INTO Diagnose VALUES ('91286','   With stent      ')\g
INSERT INTO Diagnose VALUES ('91289','   Other      ')\g
INSERT INTO Diagnose VALUES ('9129','    Other    include: Computed tomography, MR, nuclear medicine, xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('91291','   Computed tomography      ')\g
INSERT INTO Diagnose VALUES ('912911','  Unenhanced      ')\g
INSERT INTO Diagnose VALUES ('912912','  Enhanced with intravenous drip or bolus      ')\g
INSERT INTO Diagnose VALUES ('912913','  Dynamic enhanced technique      ')\g
INSERT INTO Diagnose VALUES ('912914','  Delayed scanning following enhancement (e.g., for detection of liver lesions)      ')\g
INSERT INTO Diagnose VALUES ('912915','  Spiral scanning      ')\g
INSERT INTO Diagnose VALUES ('912916','  CT angiography      ')\g
INSERT INTO Diagnose VALUES ('912917','  Three-dimensional reconstruction      ')\g
INSERT INTO Diagnose VALUES ('912918','  High-resolution technique      ')\g
INSERT INTO Diagnose VALUES ('912919','  Other, including serial enhancement studies (e.g., for diagnosis of hemangioma)      ')\g
INSERT INTO Diagnose VALUES ('91292','   Magnification technique      ')\g
INSERT INTO Diagnose VALUES ('91294','   Magnetic resonance (MR)      ')\g
INSERT INTO Diagnose VALUES ('912941','  Morphologic magnetic resonance imaging      ')\g
INSERT INTO Diagnose VALUES ('9129411',' Spin echo      ')\g
INSERT INTO Diagnose VALUES ('9129412',' Gradient echo      ')\g
INSERT INTO Diagnose VALUES ('9129413',' Inversion recovery      ')\g
INSERT INTO Diagnose VALUES ('9129414',' Chemical shift imaging      ')\g
INSERT INTO Diagnose VALUES ('9129415',' Specific resonance suppressed  include: fat suppression, water suppression      ')\g
INSERT INTO Diagnose VALUES ('9129416',' High speed imaging     include: echo planar      ')\g
INSERT INTO Diagnose VALUES ('9129417',' Magnetization transfer      ')\g
INSERT INTO Diagnose VALUES ('9129419',' Other      ')\g
INSERT INTO Diagnose VALUES ('912942','  MR angiography (morphological vascular imaging)      ')\g
INSERT INTO Diagnose VALUES ('912943','  Contrast-enhanced studies, trace studies      ')\g
INSERT INTO Diagnose VALUES ('912944','  MR velocity measurements, flow volumetry studies, motion studies, diffusion studies      ')\g
INSERT INTO Diagnose VALUES ('912945','  MR spectroscopy, spectroscopic imaging      ')\g
INSERT INTO Diagnose VALUES ('912946','  Quantitative tissue characterization, relaxometry      ')\g
INSERT INTO Diagnose VALUES ('912947','  Imaging of nuclei other than hydrogen      ')\g
INSERT INTO Diagnose VALUES ('912949','  Other      ')\g
INSERT INTO Diagnose VALUES ('91296','   Nuclear medicine study      ')\g
INSERT INTO Diagnose VALUES ('912961','  Planar scintigraphy      ')\g
INSERT INTO Diagnose VALUES ('912962','  SPECT      ')\g
INSERT INTO Diagnose VALUES ('912963','  PET      ')\g
INSERT INTO Diagnose VALUES ('912964','  Blood pool imaging      ')\g
INSERT INTO Diagnose VALUES ('912965','  Inflammation-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('912966','  Tumor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('912967','  Thrombus-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('912968','  Therapeutic procedure, include use of monoclonal antibodies      ')\g
INSERT INTO Diagnose VALUES ('912969','  Other.  See also 1.12178 Receptor-avid radiopharmaceutical imaging      ')\g
INSERT INTO Diagnose VALUES ('91297','   Nuclear medicine study-CV system      ')\g
INSERT INTO Diagnose VALUES ('912971','  Radionuclide angiography      ')\g
INSERT INTO Diagnose VALUES ('912972','  Extremity blood flow distribution (e.g., 99mTc-microspheres, T1-201)      ')\g
INSERT INTO Diagnose VALUES ('912973','  Radionuclide venography      ')\g
INSERT INTO Diagnose VALUES ('912974','  Lymphoscintigraphy      ')\g
INSERT INTO Diagnose VALUES ('912979','  Other  include: evaluation of LeVeen shunt      ')\g
INSERT INTO Diagnose VALUES ('91298','   Ultrasonography      ')\g
INSERT INTO Diagnose VALUES ('912981','  Real time      ')\g
INSERT INTO Diagnose VALUES ('912982','  Intraoperative      ')\g
INSERT INTO Diagnose VALUES ('912983','  Color Doppler      ')\g
INSERT INTO Diagnose VALUES ('912984','  Doppler      ')\g
INSERT INTO Diagnose VALUES ('912985','  Guided procedure for diagnosis   include: biopsy      ')\g
INSERT INTO Diagnose VALUES ('912986','  Guided procedure for therapy   include: drainage      ')\g
INSERT INTO Diagnose VALUES ('912987','  Computerized reconstruction      ')\g
INSERT INTO Diagnose VALUES ('912988','  Contrast material added      ')\g
INSERT INTO Diagnose VALUES ('912989','  Special nonroutine projection, mode, or transducer type      ')\g
INSERT INTO Diagnose VALUES ('91299','   Other    include: xeroradiography, thermography      ')\g
INSERT INTO Diagnose VALUES ('913','     NORMAL VARIANT      ')\g
INSERT INTO Diagnose VALUES ('9131','    Variation in number of vessels      ')\g
INSERT INTO Diagnose VALUES ('91311','   Absent      ')\g
INSERT INTO Diagnose VALUES ('91312','   Multiple      ')\g
INSERT INTO Diagnose VALUES ('9132','    Variation in origin of vessel      ')\g
INSERT INTO Diagnose VALUES ('9133','    Variation in size of vessel      ')\g
INSERT INTO Diagnose VALUES ('9134','    Variation in course of vessel    include: circumaortic renal vein      ')\g
INSERT INTO Diagnose VALUES ('9135','    Intestinal arcade      ')\g
INSERT INTO Diagnose VALUES ('91351','   Arc of Buhler      ')\g
INSERT INTO Diagnose VALUES ('91352','   Arc of Riolan      ')\g
INSERT INTO Diagnose VALUES ('91353','   Arc of Burkow      ')\g
INSERT INTO Diagnose VALUES ('91359','   Other      ')\g
INSERT INTO Diagnose VALUES ('9139','    Other      ')\g
INSERT INTO Diagnose VALUES ('914','     CONGENITAL ANOMALY, VASCULAR (see also 5.15)      ')\g
INSERT INTO Diagnose VALUES ('9141','    Arterial malformation    include: coarctation of abdominal aorta      ')\g
INSERT INTO Diagnose VALUES ('9142','    Venous malformation   include: left inferior vena cava      ')\g
INSERT INTO Diagnose VALUES ('9149','    Other    include: congenital arteriovenous fistula (also code bleeding arteriovenous malformation .7171)      ')\g
INSERT INTO Diagnose VALUES ('915','     OTHER CONGENITAL ANOMALY    include: bronchopulmonary sequestration   exclude: coarctation of thoracic aorta (562.1511)      ')\g
INSERT INTO Diagnose VALUES ('919','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('9197','    Connective tissue disorder, neurocutaneous disorders, congenital      ')\g
INSERT INTO Diagnose VALUES ('91971','   Marfan syndrome      ')\g
INSERT INTO Diagnose VALUES ('91972','   Ehlers-Danlos syndrome      ')\g
INSERT INTO Diagnose VALUES ('91973','   Menkes (kinky hair) syndrome      ')\g
INSERT INTO Diagnose VALUES ('91975','   Pseudoxanthoma elasticum      ')\g
INSERT INTO Diagnose VALUES ('91976','   Neurofibromatosis      ')\g
INSERT INTO Diagnose VALUES ('91979','   Other    include: homocystinuria   exclude: acquired disorder (.61)      ')\g
INSERT INTO Diagnose VALUES ('9199','    Other      ')\g
INSERT INTO Diagnose VALUES ('92','      INFLAMMATION (see .714 for bleeding from inflammatory disease)      ')\g
INSERT INTO Diagnose VALUES ('921','     OSTEOMYELITIS      ')\g
INSERT INTO Diagnose VALUES ('922','     SARCOIDOSIS      ')\g
INSERT INTO Diagnose VALUES ('923','     TUBERCULOSIS      ')\g
INSERT INTO Diagnose VALUES ('924','     SOFT TISSUE INFLAMMATION, ABSCESS, INFLAMMATORY MASS      ')\g
INSERT INTO Diagnose VALUES ('925','     INFLAMMATION, GENITOURINARY SYSTEM   include: pyelonephritis      ')\g
INSERT INTO Diagnose VALUES ('926','     INFLAMMATION, GASTROINTESTINAL SYSTEM   include: enteritis, colitis    exclude: ischemic enteritis and colitis (.761)      ')\g
INSERT INTO Diagnose VALUES ('929','     OTHER    include: pancreatitis      ')\g
INSERT INTO Diagnose VALUES ('93','      NEOPLASM, NEOPLASTIC-LIKE CONDITION (see .715 for bleeding from neoplastic disease)      ')\g
INSERT INTO Diagnose VALUES ('931','     BENIGN NEOPLASM, CYST      ')\g
INSERT INTO Diagnose VALUES ('932','     MALIGNANT NEOPLASM-PRIMARY      ')\g
INSERT INTO Diagnose VALUES ('9321','    Carcinoma      ')\g
INSERT INTO Diagnose VALUES ('9322','    Sarcoma      ')\g
INSERT INTO Diagnose VALUES ('9329','    Other      ')\g
INSERT INTO Diagnose VALUES ('933','     MALIGNANT NEOPLASM-SECONDARY (METASTATIC)      ')\g
INSERT INTO Diagnose VALUES ('934','     LEUKEMIA, LYMPHOMA, MYELOMA      ')\g
INSERT INTO Diagnose VALUES ('939','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('94','      EFFECT OF TRAUMA      ')\g
INSERT INTO Diagnose VALUES ('941','     VASCULAR INJURY      ')\g
INSERT INTO Diagnose VALUES ('9411','    Occlusion      ')\g
INSERT INTO Diagnose VALUES ('9412','    Laceration, transection      ')\g
INSERT INTO Diagnose VALUES ('94124','   Intimal tear      ')\g
INSERT INTO Diagnose VALUES ('9413','    Hematoma      ')\g
INSERT INTO Diagnose VALUES ('9419','    Other    exclude: post-traumatic aneurysm (.732), arteriovenous fistula (.494)      ')\g
INSERT INTO Diagnose VALUES ('944','     COMPLICATION OF CATHETERIZATION OR ANGIOGRAPHY      ')\g
INSERT INTO Diagnose VALUES ('9441','    Hematoma      ')\g
INSERT INTO Diagnose VALUES ('9442','    Thrombosis      ')\g
INSERT INTO Diagnose VALUES ('9443','    Embolus    include: cholesterol, gas      ')\g
INSERT INTO Diagnose VALUES ('9444','    Intimal tear, intramural injection, mural dissection      ')\g
INSERT INTO Diagnose VALUES ('9445','    Spasm      ')\g
INSERT INTO Diagnose VALUES ('9446','    Transmural perforation      ')\g
INSERT INTO Diagnose VALUES ('9447','    Broken or knotted catheter      ')\g
INSERT INTO Diagnose VALUES ('9448','    Adverse reaction to contrast medium      ')\g
INSERT INTO Diagnose VALUES ('9449','    Other    exclude: arteriovenous fistula (.494)      ')\g
INSERT INTO Diagnose VALUES ('945','     POSTOPERATIVE      ')\g
INSERT INTO Diagnose VALUES ('9451','    Endarterectomy      ')\g
INSERT INTO Diagnose VALUES ('9452','    Bypass graft      ')\g
INSERT INTO Diagnose VALUES ('94521','   Saphenous vein      ')\g
INSERT INTO Diagnose VALUES ('94522','   Synthetic graft      ')\g
INSERT INTO Diagnose VALUES ('94529','   Other      ')\g
INSERT INTO Diagnose VALUES ('9453','    Portal-systemic shunt      ')\g
INSERT INTO Diagnose VALUES ('94531','   Mesocaval      ')\g
INSERT INTO Diagnose VALUES ('94532','   Portocaval      ')\g
INSERT INTO Diagnose VALUES ('94533','   Splenorenal      ')\g
INSERT INTO Diagnose VALUES ('94534','   Mesoatrial      ')\g
INSERT INTO Diagnose VALUES ('94539','   Other      ')\g
INSERT INTO Diagnose VALUES ('9454','    Transluminal angioplasty      ')\g
INSERT INTO Diagnose VALUES ('9455','    Sympathectomy      ')\g
INSERT INTO Diagnose VALUES ('9456','    Mechanical implant    include: venous umbrella      ')\g
INSERT INTO Diagnose VALUES ('9457','    Surgical arteriovenous fistula for dialysis-access      ')\g
INSERT INTO Diagnose VALUES ('9458','    Complication of surgery or interventional procedure    exclude: complication of catheterization (.44), of angiography (.44)      ')\g
INSERT INTO Diagnose VALUES ('9459','    Other    exclude: arteriovenous fistula (.494), vascular-enteric fistula (.7194)      ')\g
INSERT INTO Diagnose VALUES ('947','     EFFECT OF RADIATION      ')\g
INSERT INTO Diagnose VALUES ('949','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('9494','    Arteriovenous fistula (see also bleeding from arteriovenous fistula .717)    include: postoperative, complication of angiography, post-traumatic      ')\g
INSERT INTO Diagnose VALUES ('9499','    Other      ')\g
INSERT INTO Diagnose VALUES ('95','      METABOLIC, ENDOCRINE, TOXIC      ')\g
INSERT INTO Diagnose VALUES ('958','     INTOXICATION, POISONING    include: ergotism   exclude: drug abuse (.626), vascular manifestation of diabetes (.724)      ')\g
INSERT INTO Diagnose VALUES ('959','     OTHER      ')\g
INSERT INTO Diagnose VALUES ('96','      OTHER GENERALIZED SYSTEMIC DISORDER      ')\g
INSERT INTO Diagnose VALUES ('961','     CONNECTIVE TISSUE DISORDER      ')\g
INSERT INTO Diagnose VALUES ('9612','    Lupus erythematosus      ')\g
INSERT INTO Diagnose VALUES ('9613','    Progressive systemic sclerosis (scleroderma)      ')\g
INSERT INTO Diagnose VALUES ('96131','   With Raynaud phenomenon      ')\g
INSERT INTO Diagnose VALUES ('96132','   With renal cortical necrosis      ')\g
INSERT INTO Diagnose VALUES ('9619','    Other    exclude: congenital (.197), glomerulonephritis (.697)      ')\g
INSERT INTO Diagnose VALUES ('962','     VASCULITIS      ')\g
INSERT INTO Diagnose VALUES ('9621','    Polyarteritis (periarteritis) nodosa      ')\g
INSERT INTO Diagnose VALUES ('9625','    Takayashu arteritis      ')\g
INSERT INTO Diagnose VALUES ('9626','    Manifestation of drug abuse      ')\g
INSERT INTO Diagnose VALUES ('9629','    Other   include: midaortic syndrome, mucocutaneous lymph node syndrome (see .739), Behcet disease    exclude: lupus arteritis (.612), mycotic aneurysm (.733)      ')\g
INSERT INTO Diagnose VALUES ('963','     ALLERGIC STATE      ')\g
INSERT INTO Diagnose VALUES ('964','     COMPLICATION OF DRUG THERAPY, DRUG USE      ')\g
INSERT INTO Diagnose VALUES ('965','     HEMATOLOGICAL DISORDER    include: sickle cell disease or variant, disseminated intravascular coagulation      ')\g
INSERT INTO Diagnose VALUES ('966','     HISTIOCYTOSIS      ')\g
INSERT INTO Diagnose VALUES ('969','     MISCELLANEOUS      ')\g
INSERT INTO Diagnose VALUES ('9697','    Glomerulonephritis      ')\g
INSERT INTO Diagnose VALUES ('9698','    Nephrosis      ')\g
INSERT INTO Diagnose VALUES ('9699','    Other      ')\g
INSERT INTO Diagnose VALUES ('97','      GASTROINTESTINAL BLEEDING, OTHER VASCULAR DISORDER      ')\g
INSERT INTO Diagnose VALUES ('971','     GASTROINTESTINAL BLEEDING (code vessel as site, followed by etiology)      ')\g
INSERT INTO Diagnose VALUES ('9711','    Varices, portal hypertension      ')\g
INSERT INTO Diagnose VALUES ('9712','    Mallory-Weiss syndrome      ')\g
INSERT INTO Diagnose VALUES ('9713','    Benign peptic ulcer      ')\g
INSERT INTO Diagnose VALUES ('9714','    Inflammatory disease      ')\g
INSERT INTO Diagnose VALUES ('97141','   Gastritis      ')\g
INSERT INTO Diagnose VALUES ('97142','   Granulomatous enteritis, colitis      ')\g
INSERT INTO Diagnose VALUES ('97143','   Ulcerative colitis      ')\g
INSERT INTO Diagnose VALUES ('97144','   Infectious enteritis      ')\g
INSERT INTO Diagnose VALUES ('97145','   Ischemic colitis, enteritis      ')\g
INSERT INTO Diagnose VALUES ('97149','   Other      ')\g
INSERT INTO Diagnose VALUES ('9715','    Neoplasm      ')\g
INSERT INTO Diagnose VALUES ('97151','   Benign    include: leiomyoma, polyp, hemangioma, juvenile polyp, carcinoid      ')\g
INSERT INTO Diagnose VALUES ('97152','   Carcinoma      ')\g
INSERT INTO Diagnose VALUES ('97153','   Sarcoma      ')\g
INSERT INTO Diagnose VALUES ('97159','   Other      ')\g
INSERT INTO Diagnose VALUES ('9716','    Vascular malformation      ')\g
INSERT INTO Diagnose VALUES ('97161','   Peutz-Jeghers syndrome      ')\g
INSERT INTO Diagnose VALUES ('97162','   Vascular ectasia-angiodysplasia      ')\g
INSERT INTO Diagnose VALUES ('97169','   Other      ')\g
INSERT INTO Diagnose VALUES ('9717','    Arteriovenous fistula      ')\g
INSERT INTO Diagnose VALUES ('97171','   Congenital      ')\g
INSERT INTO Diagnose VALUES ('97172','   Post-traumatic      ')\g
INSERT INTO Diagnose VALUES ('97173','   Postoperative      ')\g
INSERT INTO Diagnose VALUES ('97179','   Other      ')\g
INSERT INTO Diagnose VALUES ('9718','    Diverticulum      ')\g
INSERT INTO Diagnose VALUES ('97181','   Meckel      ')\g
INSERT INTO Diagnose VALUES ('97189','   Other      ')\g
INSERT INTO Diagnose VALUES ('9719','    Vascular-enteric fistula      ')\g
INSERT INTO Diagnose VALUES ('97191','   Neoplastic      ')\g
INSERT INTO Diagnose VALUES ('97192','   Traumatic      ')\g
INSERT INTO Diagnose VALUES ('97193','   Complication of aneurysm      ')\g
INSERT INTO Diagnose VALUES ('97194','   Postoperative      ')\g
INSERT INTO Diagnose VALUES ('97195','   Postirradiation      ')\g
INSERT INTO Diagnose VALUES ('97199','   Other      ')\g
INSERT INTO Diagnose VALUES ('972','     PRIMARY ARTERIAL DISEASE    exclude: aneurysm (.73), vasculitis (.62)      ')\g
INSERT INTO Diagnose VALUES ('9721','    Atherosclerotic stenosis      ')\g
INSERT INTO Diagnose VALUES ('97211','   Less than 50%      ')\g
INSERT INTO Diagnose VALUES ('97212','   50-80%      ')\g
INSERT INTO Diagnose VALUES ('97213','   Greater than 80%      ')\g
INSERT INTO Diagnose VALUES ('97214','   Occlusion      ')\g
INSERT INTO Diagnose VALUES ('9722','    Fibromuscular disease      ')\g
INSERT INTO Diagnose VALUES ('97221','   Intimal fibroplasia      ')\g
INSERT INTO Diagnose VALUES ('97222','   Medial fibroplasia      ')\g
INSERT INTO Diagnose VALUES ('97223','   Subadventitial fibroplasia      ')\g
INSERT INTO Diagnose VALUES ('97224','   Fibromuscular hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('97229','   Other      ')\g
INSERT INTO Diagnose VALUES ('9723','    Vascular manifestation of hypertension      ')\g
INSERT INTO Diagnose VALUES ('97231','   Nephrosclerosis      ')\g
INSERT INTO Diagnose VALUES ('97232','   Malignant hypertension      ')\g
INSERT INTO Diagnose VALUES ('97239','   Other      ')\g
INSERT INTO Diagnose VALUES ('9724','    Vascular manifestation of diabetes      ')\g
INSERT INTO Diagnose VALUES ('9725','    Thromboangiitis obliterans (Buerger disease)      ')\g
INSERT INTO Diagnose VALUES ('9729','    Other    exclude: Takayasu arteritis (.625)      ')\g
INSERT INTO Diagnose VALUES ('973','     ANEURYSM    exclude: dissecting (.74)      ')\g
INSERT INTO Diagnose VALUES ('9731','    Atherosclerotic      ')\g
INSERT INTO Diagnose VALUES ('9732','    Post-traumatic      ')\g
INSERT INTO Diagnose VALUES ('9733','    Mycotic      ')\g
INSERT INTO Diagnose VALUES ('9734','    Syphilitic      ')\g
INSERT INTO Diagnose VALUES ('9735','    Generalized atherosclerotic ectasia      ')\g
INSERT INTO Diagnose VALUES ('9739','    Other   include: mucocutaneous lymph node syndrome (see .629)      ')\g
INSERT INTO Diagnose VALUES ('974','     INTRAMURAL HEMATOMA WITH DISSECTION (DISSECTING ANEURYSM) (see also Marfan syndrome .1971, Ehlers-Danlos syndrome .1972, homocystinuria .1979)      ')\g
INSERT INTO Diagnose VALUES ('9741','    Aortic root to iliac vessels      ')\g
INSERT INTO Diagnose VALUES ('9742','    Limited to ascending aorta      ')\g
INSERT INTO Diagnose VALUES ('9743','    Distal to left subclavian artery      ')\g
INSERT INTO Diagnose VALUES ('9749','    Other      ')\g
INSERT INTO Diagnose VALUES ('975','     VENOUS DISORDER      ')\g
INSERT INTO Diagnose VALUES ('9751','    Thrombosis   include: superior vena cava syndrome (see also complication of catheter .442)      ')\g
INSERT INTO Diagnose VALUES ('9752','    Recanalization, cavernous transformation      ')\g
INSERT INTO Diagnose VALUES ('9753','    Varicosity, incompetent perforating vessel      ')\g
INSERT INTO Diagnose VALUES ('9754','    Postphlebitic change      ')\g
INSERT INTO Diagnose VALUES ('9755','    Varicosities with compression of adjacent structure   include: ovarian vein syndrome      ')\g
INSERT INTO Diagnose VALUES ('9756','    Varicocele      ')\g
INSERT INTO Diagnose VALUES ('9759','    Other      ')\g
INSERT INTO Diagnose VALUES ('976','     LOW-FLOW STATE      ')\g
INSERT INTO Diagnose VALUES ('9761','    Gastrointestinal ischemia    include: ischemic enteritis, colitis, shock, digitalis, myocardial infarction      ')\g
INSERT INTO Diagnose VALUES ('9762','    Ureteral obstruction, hydronephrosis      ')\g
INSERT INTO Diagnose VALUES ('9763','    Pericapsular fibrosis (Page kidney")"      ')\g
INSERT INTO Diagnose VALUES ('9764','    Diffuse decrease in cortical perfusion      ')\g
INSERT INTO Diagnose VALUES ('9765','    Acute oliguria (tubular necrosis")"      ')\g
INSERT INTO Diagnose VALUES ('9766','    Hepatorenal syndrome      ')\g
INSERT INTO Diagnose VALUES ('9767','    Steal" syndrome    include: subclavian steal"      ')\g
INSERT INTO Diagnose VALUES ('9769','    Other      ')\g
INSERT INTO Diagnose VALUES ('977','     EMBOLUS (see also pulmonary hypertension 5.78)      ')\g
INSERT INTO Diagnose VALUES ('9771','    With infarction      ')\g
INSERT INTO Diagnose VALUES ('9772','    Without infarction      ')\g
INSERT INTO Diagnose VALUES ('9773','    Multiple cholesterol emboli    exclude: complication of angiography (.443)      ')\g
INSERT INTO Diagnose VALUES ('9779','    Other  include: air embolus      ')\g
INSERT INTO Diagnose VALUES ('978','     EXTRINSIC COMPRESSION OF VESSEL      ')\g
INSERT INTO Diagnose VALUES ('9781','    Thoracic outlet syndrome      ')\g
INSERT INTO Diagnose VALUES ('9782','    Median arcuate ligament syndrome      ')\g
INSERT INTO Diagnose VALUES ('9783','    Encasement, compression of unknown etiology      ')\g
INSERT INTO Diagnose VALUES ('9789','    Other      ')\g
INSERT INTO Diagnose VALUES ('979','     OTHER    include: cirrhosis      ')\g
INSERT INTO Diagnose VALUES ('98','      DISORDER OF LYMPHATIC SYSTEM Code normal lymphangiography (99.125)      ')\g
INSERT INTO Diagnose VALUES ('981','     NORMAL VARIANT, CONGENITAL ANOMALY, DEVELOPMENTAL ABNORMALITY      ')\g
INSERT INTO Diagnose VALUES ('9811','    Filling of thoracic nodes from pedal lymphangiography      ')\g
INSERT INTO Diagnose VALUES ('9812','    Filling of supraclavicular, cervical or axillary nodes from pedal      ')\g
INSERT INTO Diagnose VALUES ('9813','    Lymphatic-venous communication      ')\g
INSERT INTO Diagnose VALUES ('9814','    Duplicated major lymphatic vessels      ')\g
INSERT INTO Diagnose VALUES ('9815','    Poorly developed major lymphatic vessels      ')\g
INSERT INTO Diagnose VALUES ('9819','    Other      ')\g
INSERT INTO Diagnose VALUES ('982','     INFLAMMATION      ')\g
INSERT INTO Diagnose VALUES ('9821','    Involutive change, fibrolipomatosis      ')\g
INSERT INTO Diagnose VALUES ('9822','    Inflammatory change, hyperplastic enlarged nodes      ')\g
INSERT INTO Diagnose VALUES ('98221','   Infectious mononucleosis      ')\g
INSERT INTO Diagnose VALUES ('98222','   Measles      ')\g
INSERT INTO Diagnose VALUES ('98223','   With chronic fungus infection of feet      ')\g
INSERT INTO Diagnose VALUES ('98224','   With other infectious disease      ')\g
INSERT INTO Diagnose VALUES ('98225','   Rheumatoid arthritis      ')\g
INSERT INTO Diagnose VALUES ('98226','   Ankylosing spondylitis (Marie-Strumpel disease)      ')\g
INSERT INTO Diagnose VALUES ('98227','   With psoriatic arthritis      ')\g
INSERT INTO Diagnose VALUES ('98229','   Other      ')\g
INSERT INTO Diagnose VALUES ('9823','    Granulomatous disease      ')\g
INSERT INTO Diagnose VALUES ('98231','   Tuberculosis      ')\g
INSERT INTO Diagnose VALUES ('98232','   Sarcoid      ')\g
INSERT INTO Diagnose VALUES ('98233','   Toxoplasmosis, brucellosis, syphilis      ')\g
INSERT INTO Diagnose VALUES ('98234','   Histoplasmosis, coccidioidomycosis, blastomycosis, tularemia, leprosy      ')\g
INSERT INTO Diagnose VALUES ('98235','   Crohn disease, Whipple disease      ')\g
INSERT INTO Diagnose VALUES ('98239','   Other      ')\g
INSERT INTO Diagnose VALUES ('9824','    Parasites    include: filariasis      ')\g
INSERT INTO Diagnose VALUES ('9825','    Abscess    include: lymphopathia venereum      ')\g
INSERT INTO Diagnose VALUES ('9826','    Reactive hyperplasia      ')\g
INSERT INTO Diagnose VALUES ('9827','    Retroperitoneal fibrosis      ')\g
INSERT INTO Diagnose VALUES ('9829','    Other    include: lymphangiomyomatosis   exclude: radiation fibrosis (.844)      ')\g
INSERT INTO Diagnose VALUES ('983','     NEOPLASM, NEOPLASTIC-LIKE CONDITION      ')\g
INSERT INTO Diagnose VALUES ('9831','    Metastatic disease from genitourinary system      ')\g
INSERT INTO Diagnose VALUES ('98311','   Cervix      ')\g
INSERT INTO Diagnose VALUES ('98312','   Corpus uteri      ')\g
INSERT INTO Diagnose VALUES ('98313','   Ovary      ')\g
INSERT INTO Diagnose VALUES ('98314','   Vagina, vulva      ')\g
INSERT INTO Diagnose VALUES ('98315','   Bladder      ')\g
INSERT INTO Diagnose VALUES ('98316','   Testis      ')\g
INSERT INTO Diagnose VALUES ('98317','   Penis, prostate      ')\g
INSERT INTO Diagnose VALUES ('98318','   Kidney      ')\g
INSERT INTO Diagnose VALUES ('98319','   Adrenal    include: neuroblastoma      ')\g
INSERT INTO Diagnose VALUES ('9832','    Metastatic disease from gastrointestinal system      ')\g
INSERT INTO Diagnose VALUES ('98321','   Anus, rectum      ')\g
INSERT INTO Diagnose VALUES ('98322','   Colon      ')\g
INSERT INTO Diagnose VALUES ('98323','   Stomach, esophagus      ')\g
INSERT INTO Diagnose VALUES ('98324','   Pancreas      ')\g
INSERT INTO Diagnose VALUES ('98329','   Other      ')\g
INSERT INTO Diagnose VALUES ('9833','    Metastatic disease from other site      ')\g
INSERT INTO Diagnose VALUES ('98331','   Breast      ')\g
INSERT INTO Diagnose VALUES ('98332','   Malignant melanoma      ')\g
INSERT INTO Diagnose VALUES ('98333','   Skin (squamous cell carcinoma)      ')\g
INSERT INTO Diagnose VALUES ('98334','   Neoplasm of head, neck      ')\g
INSERT INTO Diagnose VALUES ('98335','   Soft tissue sarcoma      ')\g
INSERT INTO Diagnose VALUES ('98339','   Other    include: site unknown      ')\g
INSERT INTO Diagnose VALUES ('9834','    Leukemia, lymphoma      ')\g
INSERT INTO Diagnose VALUES ('98341','   Leukemia      ')\g
INSERT INTO Diagnose VALUES ('98342','   Hodgkin disease      ')\g
INSERT INTO Diagnose VALUES ('98343','   Non-Hodgkin lymphoma      ')\g
INSERT INTO Diagnose VALUES ('98346','   Mycosis fungoides      ')\g
INSERT INTO Diagnose VALUES ('98347','   Burkitt lymphoma      ')\g
INSERT INTO Diagnose VALUES ('98348','   Kaposi sarcoma      ')\g
INSERT INTO Diagnose VALUES ('98349','   Other      ')\g
INSERT INTO Diagnose VALUES ('9839','    Other      ')\g
INSERT INTO Diagnose VALUES ('9841','    Trauma      ')\g
INSERT INTO Diagnose VALUES ('9842','    Lymphocyst formation, postoperative      ')\g
INSERT INTO Diagnose VALUES ('9845','    Traumatic laceration of nodes or ducts      ')\g
INSERT INTO Diagnose VALUES ('9843','    Lymphatic obstruction, postoperative      ')\g
INSERT INTO Diagnose VALUES ('9844','    Radiation fibrosis      ')\g
INSERT INTO Diagnose VALUES ('9849','    Other      ')\g
INSERT INTO Diagnose VALUES ('985','     LYMPHEDEMA      ')\g
INSERT INTO Diagnose VALUES ('9851','    Primary lymphedema      ')\g
INSERT INTO Diagnose VALUES ('98511','   Milroy disease      ')\g
INSERT INTO Diagnose VALUES ('98512','   Aplasia, other      ')\g
INSERT INTO Diagnose VALUES ('98513','   Hypoplasia, other      ')\g
INSERT INTO Diagnose VALUES ('98519','   Other      ')\g
INSERT INTO Diagnose VALUES ('9852','    Acquired lymphedema      ')\g
INSERT INTO Diagnose VALUES ('98521','   Secondary to inflammatory disease      ')\g
INSERT INTO Diagnose VALUES ('98522','   Secondary to trauma      ')\g
INSERT INTO Diagnose VALUES ('98523','   Secondary to neoplasm      ')\g
INSERT INTO Diagnose VALUES ('98524','   Secondary to surgery, radiation      ')\g
INSERT INTO Diagnose VALUES ('98529','   Other      ')\g
INSERT INTO Diagnose VALUES ('987','     CHYLOTHORAX, CHYLOUS ASCITES      ')\g
INSERT INTO Diagnose VALUES ('989','     OTHER    exclude: pulmonary oil emboli (6.7251), hepatic oil emboli (7.799)      ')\g
INSERT INTO Diagnose VALUES ('99','      OTHER      ')\g
INSERT INTO Diagnose VALUES ('991','     FUNDAMENTAL OBSERVATION    include: standing waves, layering of contrast material, jet collapse      ')\g
INSERT INTO Diagnose VALUES ('992','     ANATOMICAL DETAIL      ')\g
INSERT INTO Diagnose VALUES ('993','     ARTIFACT      ')\g
INSERT INTO Diagnose VALUES ('994','     ERROR IN DIAGNOSIS      ')\g
INSERT INTO Diagnose VALUES ('999','     OTHER    include: phenomenon related to technique of examination      ')\g
