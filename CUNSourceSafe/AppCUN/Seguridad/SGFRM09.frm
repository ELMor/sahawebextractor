VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmCWProcessRol 
   Caption         =   "SEGURIDAD. Relaci�n entre Procesos y Roles"
   ClientHeight    =   5430
   ClientLeft      =   735
   ClientTop       =   2355
   ClientWidth     =   10425
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   24
   Icon            =   "SGFRM09.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5430
   ScaleWidth      =   10425
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   10425
      _ExtentX        =   18389
      _ExtentY        =   741
      ButtonWidth     =   635
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   327682
   End
   Begin SSDataWidgets_B.SSDBGrid grdGrid 
      Height          =   4290
      Left            =   3555
      TabIndex        =   1
      Top             =   675
      Width           =   6765
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      Col.Count       =   0
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      ForeColorEven   =   0
      BackColorEven   =   -2147483643
      BackColorOdd    =   -2147483643
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      _ExtentX        =   11933
      _ExtentY        =   7567
      _StockProps     =   79
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox picSplitter 
      BorderStyle     =   0  'None
      Height          =   4290
      Left            =   3330
      MousePointer    =   9  'Size W E
      ScaleHeight     =   4290
      ScaleWidth      =   45
      TabIndex        =   2
      Top             =   675
      Width           =   50
   End
   Begin ComctlLib.TreeView tvwTables 
      Height          =   4260
      Left            =   45
      TabIndex        =   0
      Top             =   675
      Width           =   3075
      _ExtentX        =   5424
      _ExtentY        =   7514
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   353
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin ComctlLib.StatusBar stbStatusBar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   5145
      Width           =   10425
      _ExtentX        =   18389
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuData 
      Caption         =   "&Datos"
      Begin VB.Menu optData 
         Caption         =   "&Guardar"
         Index           =   10
         Shortcut        =   ^G
      End
      Begin VB.Menu optData 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu optData 
         Caption         =   "&Imprimir"
         Index           =   30
         Shortcut        =   ^P
      End
      Begin VB.Menu optData 
         Caption         =   "-"
         Index           =   40
      End
      Begin VB.Menu optData 
         Caption         =   "&Salir"
         Index           =   50
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edici�n"
      Begin VB.Menu optEdit 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu optEdit 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu optEdit 
         Caption         =   "&Marcar todos"
         Index           =   30
         Shortcut        =   ^E
      End
      Begin VB.Menu optEdit 
         Caption         =   "&Desmarcar todos"
         Index           =   40
         Shortcut        =   ^D
      End
   End
   Begin VB.Menu mnuRegister 
      Caption         =   "&Registro"
      Begin VB.Menu optRegister 
         Caption         =   "&Primero"
         Index           =   10
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Anterior"
         Index           =   20
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Siguiente"
         Index           =   30
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Ultimo"
         Index           =   40
      End
      Begin VB.Menu optRegister 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu optRegister 
         Caption         =   "Lista de &Valores"
         Index           =   60
      End
      Begin VB.Menu optRegister 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu optRegister 
         Caption         =   "&Refrescar Registros"
         Index           =   80
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&?"
      Begin VB.Menu optHelp 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu optHelp 
         Caption         =   "&Acerca de..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmCWProcessRol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWProcessRol
' Coded by SYSECA Bilbao
' **********************************************************************************


Const cwProcessRolMsgNotSelect As String = "El Proceso '%1' tiene asignada una Funci�n pero no est� seleccionado"


Dim WithEvents objTree As clsCWTree
Attribute objTree.VB_VarHelpID = -1


Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
 ' KM: this approach needed a lot more modifications than expected:
 ' intKeyCode = objTree.TreeProcess(cwTreeProcessKeys, intKeyCode, intShift)
  If intKeyCode = vbKeyF12 And intShift = vbCtrlMask + vbAltMask + vbShiftMask Then
    Call objTree.TreeProcess(cwTreeProcessDoc, 0)
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objSource As New clsCWTreeSource
  Dim objLevel  As New clsCWTreeLevel
  
  On Error GoTo cwIntError
  
  Call objApp.SplashOn
  
  Set objTree = New clsCWTree
  
  With objTree
    Set .frmForm = Me
    Set .grdGrid = grdGrid
    Set .picSplitter = picSplitter
    Set .tvwTables = tvwTables
    Set .tlbToolbar = tlbToolbar
    Set .stbStatusBar = stbStatusBar
  
    ' datos del nivel 1
    With objLevel
      .intType = cwTreeLevelTypeNothing
      .strText = "ROLES"
      Set .objSource = Nothing
    End With
    Call objTree.TreeAddLevel(objLevel)
    Set objLevel = Nothing
    
    ' datos del nivel 2
    With objSource
      .strCrossSQL = "SELECT sg04cod, sg03cod, sg16cod FROM sg0700 WHERE sg03cod = ?"
      .strCrossTable = "SG0700"
      .strText = "sg0400"
      .intType = cwTreeSourceTypeSQL
      .strOrder = "ORDER BY sg04cod"
      Call .AddField("sg04cod", "C�digo", cwVariant, False, True, True, 7, "")
      Call .AddField("sg04des", "", cwVariant, False, True, True, 25, "")
      Call .AddField("", "Asignar", cwBoolean, False, False, True, 0, "")
      Call .AddField("", "Funci�n", cwString, False, False, True, 0, "SELECT sg16cod, sg16des FROM sg1600 ORDER BY sg16des")
      Call .CheckPositions(3)
      Call .LoadDictionary
    End With
    With objLevel
      .intType = cwTreeLevelTypeSQL
      .strText = "SELECT sg03cod, sg03des FROM sg0300 ORDER BY sg03des"
      Set .objSource = objSource
    End With
    Call objTree.TreeAddLevel(objLevel)
    Set objSource = Nothing
    Set objLevel = Nothing
    
    Call objTree.TreeCreateInfo
  End With
  
cwResError:
  Call objApp.SplashOff
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Load")
  Resume cwResError
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  Cancel = objTree.TreeExit
End Sub

Private Sub Form_Resize()
  Call objTree.TreeResize
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de clsCWTree
' -----------------------------------------------
Private Sub objTree_cwQueryUpdateRow(ByVal intLevel As Integer, ByVal cllGridValues As Collection, blnSave As Boolean, blnCancel As Boolean)
  If intLevel = 2 Then
    If cllGridValues(3) = -1 Then
      blnSave = True
    End If
    If cllGridValues(3) = 0 And Not objGen.IsStrEmpty(cllGridValues(4)) Then
      Call objError.SetError(cwCodeMsg, cwProcessRolMsgNotSelect, cllGridValues(2))
      Call objError.Raise
      blnCancel = True
    End If
  End If
End Sub

Private Sub objTree_cwReadCross(ByVal intLevel As Integer, aParameters() As String, ByVal cllParents As Collection)
  If intLevel = 2 Then
    aParameters(1) = cllParents(1)
  End If
End Sub

Private Sub objTree_cwReadRow(ByVal intLevel As Integer, aGridValues() As String, ByVal cllCross As Collection, ByVal cllCursor As Collection, ByVal blnExist As Boolean)
  If intLevel = 2 Then
    aGridValues(1) = cllCursor(1)
    aGridValues(2) = cllCursor(2)
    aGridValues(3) = IIf(blnExist, -1, 0)
    If blnExist Then
      aGridValues(4) = cllCross(3)
    Else
      aGridValues(4) = Empty
    End If
  End If
End Sub

Private Sub objTree_cwUpdateRow(ByVal intLevel As Integer, ByVal cllGridValues As Collection, ByVal cllParents As Collection, rdoCursor As RDO.rdoResultset)
  If intLevel = 2 Then
    rdoCursor(0) = cllGridValues(1)
    rdoCursor(1) = cllParents(1)
    rdoCursor(2) = cllGridValues(4)
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Grid
' -----------------------------------------------
Private Sub grdGrid_Change()
  Call objTree.TreeDataChanged
End Sub

Private Sub grdGrid_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
  Call objTree.TreeRowColChange(LastRow, LastCol)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del men�
' -----------------------------------------------
Private Sub optData_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessData, intIndex)
End Sub

Private Sub optEdit_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessEdit, intIndex)
End Sub

Private Sub optHelp_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessHelp, intIndex)
End Sub

Private Sub optRegister_Click(intIndex As Integer)
  Call objTree.TreeProcess(cwTreeProcessRegister, intIndex)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Splitter
' -----------------------------------------------
Private Sub picSplitter_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.TreeMoving(cwTreeMouseDown, Button, Shift, X, Y)
End Sub

Private Sub picSplitter_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.TreeMoving(cwTreeMouseUp, Button, Shift, X, Y)
End Sub

Private Sub picSplitter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objTree.TreeMoving(cwTreeMouseMove, Button, Shift, X, Y)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la ToolBar
' -----------------------------------------------
Private Sub tlbtoolbar_ButtonClick(ByVal btnButton As ComctlLib.Button)
  Call objTree.TreeProcess(cwTreeProcessToolBar, btnButton.Index)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la StatusBar
' -----------------------------------------------
Private Sub stbStatusBar_PanelDblClick(ByVal panPanel As ComctlLib.Panel)
  Call objTree.TreeProcess(cwTreeProcessStatusBar, panPanel.Index)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del TreeView
' -----------------------------------------------
Private Sub tvwTables_Collapse(ByVal nodNode As ComctlLib.Node)
  Call objTree.TreeSelected(cwTreeCollapse, nodNode)
End Sub

Private Sub tvwTables_Expand(ByVal nodNode As ComctlLib.Node)
  Call objTree.TreeSelected(cwTreeExpand, nodNode)
End Sub

Private Sub tvwTables_NodeClick(ByVal nodNode As ComctlLib.Node)
  Call objTree.TreeSelected(cwTreeSelect, nodNode)
End Sub

