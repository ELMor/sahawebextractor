VERSION 5.00
Begin VB.Form frmCWChangePW 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SEGURIDAD. Cambio de la Clave de Acceso"
   ClientHeight    =   2130
   ClientLeft      =   2835
   ClientTop       =   3480
   ClientWidth     =   6060
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   54
   Icon            =   "SGFRM02.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1258.475
   ScaleMode       =   0  'User
   ScaleWidth      =   5690.011
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraNew 
      Height          =   1185
      Left            =   90
      TabIndex        =   6
      Top             =   855
      Width           =   4335
      Begin VB.TextBox txtReply 
         Height          =   345
         IMEMode         =   3  'DISABLE
         Left            =   2385
         MaxLength       =   6
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   675
         Width           =   1560
      End
      Begin VB.TextBox txtNew 
         Height          =   345
         IMEMode         =   3  'DISABLE
         Left            =   2385
         MaxLength       =   6
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   225
         Width           =   1560
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Repetir Clave de Acceso"
         Height          =   195
         Index           =   2
         Left            =   405
         TabIndex        =   9
         Top             =   765
         Width           =   1770
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Clave de Acceso nueva"
         Height          =   195
         Index           =   1
         Left            =   405
         TabIndex        =   8
         Top             =   270
         Width           =   1710
      End
   End
   Begin VB.Frame fraOld 
      Height          =   780
      Left            =   90
      TabIndex        =   5
      Top             =   90
      Width           =   4335
      Begin VB.TextBox txtOld 
         Height          =   345
         IMEMode         =   3  'DISABLE
         Left            =   2385
         MaxLength       =   6
         PasswordChar    =   "*"
         TabIndex        =   0
         Top             =   270
         Width           =   1560
      End
      Begin VB.Label lblLabels 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Clave de Acceso Antigua"
         Height          =   195
         Index           =   0
         Left            =   405
         TabIndex        =   7
         Top             =   285
         Width           =   1800
      End
   End
   Begin VB.CommandButton cmdAccept 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   375
      Left            =   4545
      TabIndex        =   3
      Top             =   180
      Width           =   1400
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   4545
      TabIndex        =   4
      Top             =   675
      Width           =   1400
   End
End
Attribute VB_Name = "frmCWChangePW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


' **********************************************************************************
' Form frmCWChangePW
' Coded by SYSECA Bilbao
' **********************************************************************************


Const cwPwdMsgOk               As String = "La Clave de Acceso se cambi� con �xito"
Const cwPwdMsgNewNotValid      As String = "La Clave de Acceso nueva no es v�lida"
Const cwPwdMsgNewOldEqual      As String = "La Clave de Acceso antigua y la nueva son iguales"
Const cwPwdMsgNewReplyNotEqual As String = "La Clave de Acceso nueva y la copia no son iguales"
Const cwPwdMsgOldNotValid      As String = "La Clave de Acceso antigua no es v�lida"
Const cwPwdMsgErrorChanging    As String = "Se produjo un error no determinado modificando la Clave de Acceso"
Const cwPwdMsgErrorNotUser     As String = "El Usuario especificado no existe"

Dim StrUsuario As String
Dim mrdoCursor As rdoResultset
Dim mblnPriority As Boolean


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyF1 Then
     Call objApp.HelpContext
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call mrdoCursor.Close
  Set mrdoCursor = Nothing
End Sub

Public Function Prepare(ByVal STRUSER As String, _
                        ByVal blnPriority As Boolean) As Boolean
  Dim strSQL      As String
  Dim strFullName As String
  Dim rdoNombre   As rdoResultset
  
  On Error GoTo cwIntError
  
  mblnPriority = blnPriority
  StrUsuario = STRUSER
  strSQL = "SELECT sg02nom || ' ' || sg02ape1 || ' ' || sg02ape2 FROM sg0200 WHERE sg02cod = '" & STRUSER & "'"
  Set rdoNombre = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurReadOnly)
  If rdoNombre.RowCount > 0 Then
    strFullName = StrConv(rdoNombre(0).Value, vbProperCase)
  End If
  Call rdoNombre.Close
  Set rdoNombre = Nothing
  
  Me.Caption = Me.Caption & ", " & strFullName
  Call EnabledOk
  
  strSQL = "SELECT sg02passw FROM SG0200 WHERE sg02cod = '" & STRUSER & "'"
  Set mrdoCursor = objApp.rdoConnect.OpenResultset(strSQL, rdOpenKeyset, rdConcurRowVer)
  If mrdoCursor.RowCount = 1 Then
    If blnPriority Then
      txtOld.BackColor = vbButtonFace
      txtOld = mrdoCursor(0).Value
      fraOld.Enabled = False
    End If
    Prepare = True
  Else
    Call objError.SetError(cwCodeMsg, cwPwdMsgErrorNotUser)
    Call objError.Raise
  End If
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "Prepare")
End Function

Private Sub cmdAccept_Click()
  Dim objEncript As New clsCWMRES
  Dim blnOk As Boolean
  Dim SQL$, qry As rdoQuery
  Dim strSQL$, Qy As rdoQuery
  On Error GoTo cwIntError
  
  If mblnPriority Then
    blnOk = True
  Else
    blnOk = (mrdoCursor(0) = objEncript.Encript(txtOld)) _
    Or (mrdoCursor(0) = objEncript.EncriptOld(txtOld))
  End If
  
  If blnOk Then
    If UCase(txtNew) = UCase(txtReply) Then
      If objEncript.Encript(UCase(txtNew)) <> UCase(txtOld) Then
        If Not objGen.IsStrEmpty(txtNew) Then
        
          SQL = "UPDATE SG0200 SET SG02PASSW = ? WHERE SG02COD = ?"
          Set qry = objApp.rdoConnect.CreateQuery("", SQL)
          qry(0) = objEncript.Encript(UCase(txtNew))
          qry(1) = StrUsuario
          qry.Execute
          qry.Close
          'Para Picis
          strSQL = "UPDATE STAFF SET VIP = Null , VIPLASTUPDATE = Null WHERE " _
                    & "USERNAME = ?"
          Set Qy = objApp.rdoConnect.CreateQuery("", strSQL)
          Qy(0) = StrUsuario
          Qy.Execute
          Qy.Close
          'Call mrdoCursor.Edit
          'mrdoCursor(0) = objEncript.Encript(txtNew)
          'Call mrdoCursor.Update
          Call objError.SetError(cwCodeMsg, cwPwdMsgOk)
          Call objError.Raise
          Call Me.Hide
        Else
          Call objError.SetError(cwCodeMsg, cwPwdMsgNewNotValid)
          Call objError.Raise
          Call txtNew.SetFocus
        End If
      Else
        Call objError.SetError(cwCodeMsg, cwPwdMsgNewOldEqual)
        Call objError.Raise
        Call txtReply.SetFocus
      End If
    Else
      Call objError.SetError(cwCodeMsg, cwPwdMsgNewReplyNotEqual)
      Call objError.Raise
      Call txtNew.SetFocus
    End If
  Else
    Call objError.SetError(cwCodeMsg, cwPwdMsgOldNotValid)
    Call objError.Raise
  End If
  Exit Sub
  
cwIntError:
  Call objError.InternalError(Me, "Click")
End Sub

Private Sub cmdCancel_Click()
  Call Me.Hide
End Sub

Private Sub txtNew_Change()
  Call EnabledOk
End Sub

Private Sub txtOld_Change()
  Call EnabledOk
End Sub

Private Sub txtReply_Change()
  Call EnabledOk
End Sub

Private Sub EnabledOk()
  cmdAccept.Enabled = Not objGen.IsStrEmpty(txtOld) And _
                      Not objGen.IsStrEmpty(txtNew) And _
                      Not objGen.IsStrEmpty(txtReply)
End Sub
