VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWSecure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWSecure
' Coded by SYSECA Bilbao
' **********************************************************************************


' devuelve la versi�n de CodeWizardSecureDialogTitle
Public Function GetCWSecureVersion() As String
  On Error GoTo cwIntError
  
  With App
    GetCWSecureVersion = .FileDescription & " v" & _
                         Format(CStr(.Major), "###0") & "." & _
                         Format(CStr(.Minor), "###0") & "." & _
                         Format(CStr(.Revision), "###0")
  End With
  Exit Function
  
cwIntError:
  Call objError.InternalError(Me, "GetCWSecureVersion")
End Function



