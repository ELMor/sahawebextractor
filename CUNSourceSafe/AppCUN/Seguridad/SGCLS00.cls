VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


' **********************************************************************************
' Class clsCWLauncher
' Coded by SYSECA Bilbao
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
Const SGRepApps         As String = "SG0001"
Const SGRepGroups       As String = "SG0002"
Const SGRepRoles        As String = "SG0003"
Const SGRepRolesGroup   As String = "SG0004"
Const SGRepUsers        As String = "SG0005"
Const SGRepUsersGroup   As String = "SG0006"
Const SGRepUsersRol     As String = "SG0007"
Const SGRepProcessRol   As String = "SG0008"
Const SGRepProcessUser  As String = "SG0009"
Const SGRepProcessApp   As String = "SG0010"
Const SGRepTablesRol    As String = "SG0011"
Const SGRepTablesUser   As String = "SG0012"
Const SGRepColumnsRol   As String = "SG0013"
Const SGRepColumnsUser  As String = "SG0014"
Const SGRepAuditTables  As String = "SG0015"
Const SGRepLogTable     As String = "SG0016"
Const SGRepLogUser      As String = "SG0017"

' Ahora las ventanas. Continuan la numeraci�n a partir del SG1000
Const SGWinApps         As String = "SG1001"
Const SGWinGroups       As String = "SG1002"
Const SGWinUsers        As String = "SG1003"
Const SGWinRoles        As String = "SG1004"
Const SGWinRolesGroup   As String = "SG1005"
Const SGWinTypeProcess  As String = "SG1006"
Const SGWinProcess      As String = "SG1007"
Const SGWinFuncs        As String = "SG1008"
Const SGWinSegTabCol    As String = "SG1009"
Const SGWinChangePW     As String = "SG1010"
Const SGWinForzePW      As String = "SG1011"
Const SGWinProcessRol   As String = "SG1012"
Const SGWinDelega       As String = "SG1013"
Const SGWinPurgeDelega  As String = "SG1014"
Const SGWinQueryDelega  As String = "SG1015"
Const SGWinVenLis       As String = "SG1016"
Const SGWinPrnTabCol    As String = "SG1017"
Const SGWinEncript      As String = "SG1018"
Const SGWinAuditTables  As String = "SG1019"
Const SGWinViewAudit    As String = "SG1020"
Const SGWinMaintAudit   As String = "SG1021"
Const SGWinLoadProcess  As String = "SG1022"
Const SGWinDesEncript   As String = "SG1023"

' C�digos de ayuda
Const CWHelpIDApps          As Integer = 25
Const CWHelpIDGrupos        As Integer = 5
Const CWHelpIDRoles         As Integer = 14
Const CWHelpIDProcRol       As Integer = 24
Const CWHelpIDVentanas      As Integer = 37
Const CWHelpIDDelega        As Integer = 46
Const CWHelpIDNuevaClave    As Integer = 62
Const CWHelpIDTabAudit      As Integer = 87
Const CWHelpIDUsuarios      As Integer = 8
Const CWHelpIDGrupoRoles    As Integer = 17
Const CWHelpIDTabColFilters As Integer = 38
Const CWHelpIDElimDelega    As Integer = 53
Const CWHelpIDCargaProc     As Integer = 70
Const CWHelpIDLogAudit      As Integer = 98
Const CWHelpIDTiposProc     As Integer = 110
Const CWHelpIDMantFuncs     As Integer = 11
Const CWHelpIDSegTabCols    As Integer = 19
Const CWHelpIDMantProc      As Integer = 31
Const CWHelpIDMantDeleg     As Integer = 40
Const CWHelpIDCambioClave   As Integer = 54
Const CWHelpIDAlgEncrypt    As Integer = 86
' Const CWHelpIDConsultaLog   As Integer = 99




' Ahora las rutinas. Continuan la numeraci�n a partir del SG2000
' const SGRut... as string = "SG2001"

' las aplicaciones y listados externos no se meten por aqu�.


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objMouse = mobjCW.objMouse
  Set objSecurity = mobjCW.objSecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean

  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    Case SGWinApps
'      Load frmCWApps
'      Call objSecurity.AddHelpContext(CWHelpIDApps)
'      Call frmCWApps.Show(vbModal)
'      Call objSecurity.RemoveHelpContext
'      Unload frmCWApps
'      Set frmCWApps = Nothing
      Load frmApps
      Call objSecurity.AddHelpContext(CWHelpIDApps)
      Call frmApps.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmApps
      Set frmApps = Nothing
    Case SGWinGroups
      Load frmCWGroups
      Call objSecurity.AddHelpContext(CWHelpIDGrupos)
      Call frmCWGroups.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWGroups
      Set frmCWGroups = Nothing
    Case SGWinUsers
      Load frmCWUsers
      Call objSecurity.AddHelpContext(CWHelpIDUsuarios)
      Call frmCWUsers.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWUsers
      Set frmCWUsers = Nothing
    Case SGWinRoles
      Load frmCWRoles
      Call objSecurity.AddHelpContext(CWHelpIDRoles)
      Call frmCWRoles.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWRoles
      Set frmCWRoles = Nothing
    Case SGWinRolesGroup
      Load frmCWRolesGroup
      Call objSecurity.AddHelpContext(CWHelpIDGrupoRoles)
      Call frmCWRolesGroup.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWRolesGroup
      Set frmCWRolesGroup = Nothing
    Case SGWinTypeProcess
      Load frmCWTypeProcess
      Call objSecurity.AddHelpContext(CWHelpIDTiposProc)
      Call frmCWTypeProcess.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWTypeProcess
      Set frmCWTypeProcess = Nothing
    Case SGWinProcess
      Load frmCWProcess
      Call objSecurity.AddHelpContext(CWHelpIDMantProc)
      Call frmCWProcess.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWProcess
      Set frmCWProcess = Nothing
    Case SGWinFuncs
      Load frmCWFuncs
      Call objSecurity.AddHelpContext(CWHelpIDMantFuncs)
      Call frmCWFuncs.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWFuncs
      Set frmCWFuncs = Nothing
    Case SGWinSegTabCol
      Call objSecurity.AddHelpContext(CWHelpIDSegTabCols)
      Load frmCWSegTabCol
      Call frmCWSegTabCol.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWSegTabCol
      Set frmCWSegTabCol = Nothing
    Case SGWinChangePW
      Load frmCWChangePW
      Call objSecurity.AddHelpContext(CWHelpIDCambioClave)
      If frmCWChangePW.Prepare(objSecurity.STRUSER, False) Then
        Call frmCWChangePW.Show(vbModal)
      End If
      Call objSecurity.RemoveHelpContext
      Unload frmCWChangePW
      Set frmCWChangePW = Nothing
    Case SGWinForzePW
      Load frmCWChangePW
      Call objSecurity.AddHelpContext(CWHelpIDNuevaClave)
      If IsMissing(vntData) Then vntData = objSecurity.STRUSER
      If frmCWChangePW.Prepare(vntData, True) Then
        Call frmCWChangePW.Show(vbModal)
      End If
      Call objSecurity.RemoveHelpContext
      Unload frmCWChangePW
      Set frmCWChangePW = Nothing
    Case SGWinProcessRol
      Call objSecurity.AddHelpContext(CWHelpIDProcRol)
      Load frmCWProcessRol
      Call frmCWProcessRol.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWProcessRol
      Set frmCWProcessRol = Nothing
    Case SGWinDelega
      Call objSecurity.AddHelpContext(CWHelpIDMantDeleg)
      Load frmCWDelega
      Call frmCWDelega.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWDelega
      Set frmCWDelega = Nothing
    Case SGWinPurgeDelega
      Call objSecurity.AddHelpContext(CWHelpIDElimDelega)
      Load frmCWPurgeDelega
      Call frmCWPurgeDelega.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWPurgeDelega
      Set frmCWPurgeDelega = Nothing
    Case SGWinQueryDelega
      Call objSecurity.AddHelpContext(CWHelpIDDelega)
      Load frmCWQueryDelega
      Call frmCWQueryDelega.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWQueryDelega
      Set frmCWQueryDelega = Nothing
    Case SGWinVenLis
      MsgBox "Este proceso est� obsoleto"
      'Call objSecurity.AddHelpContext(CWHelpIDVentanas)
      'Load frmCWVenLis
      'Call frmCWVenLis.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frmCWVenLis
      'Set frmCWVenLis = Nothing
    Case SGWinPrnTabCol
      Call objSecurity.AddHelpContext(CWHelpIDTabColFilters)
      Load frmCWPrnTabCol
      Call frmCWPrnTabCol.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWPrnTabCol
      Set frmCWPrnTabCol = Nothing
    Case SGWinEncript
      Call objSecurity.AddHelpContext(CWHelpIDAlgEncrypt)
      Load frmCWEncript
      Call frmCWEncript.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWEncript
      Set frmCWEncript = Nothing
    Case SGWinDesEncript
      Call objSecurity.AddHelpContext(CWHelpIDAlgEncrypt)
      Load frmCWDesEncript
      Call frmCWDesEncript.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWDesEncript
      Set frmCWDesEncript = Nothing
    Case SGWinAuditTables
      Call objSecurity.AddHelpContext(CWHelpIDTabAudit)
      Load frmCWAuditTables
      Call frmCWAuditTables.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWAuditTables
      Set frmCWAuditTables = Nothing
    Case SGWinViewAudit
      Call objSecurity.AddHelpContext(CWHelpIDLogAudit)
      Load frmCWViewAudit
      If frmCWViewAudit.Prepare(False) Then
        Call frmCWViewAudit.Show(vbModal)
      End If
      Call objSecurity.RemoveHelpContext
      Unload frmCWViewAudit
      Set frmCWViewAudit = Nothing
    Case SGWinMaintAudit
      Load frmCWViewAudit
      Call objSecurity.AddHelpContext(CWHelpIDLogAudit)
      If frmCWViewAudit.Prepare(True) Then
        Call frmCWViewAudit.Show(vbModal)
      End If
      Call objSecurity.RemoveHelpContext
      Unload frmCWViewAudit
      Set frmCWViewAudit = Nothing
    Case SGWinLoadProcess
      Load frmCWLoadProcess
      Call objSecurity.AddHelpContext(CWHelpIDCargaProc)
      Call frmCWLoadProcess.Show(vbModal)
      Call objSecurity.RemoveHelpContext
      Unload frmCWLoadProcess
      Set frmCWLoadProcess = Nothing
    Case SGRepProcessUser
      Call objSecurity.AddHelpContext(CWHelpIDUsuarios)
      Call ReportProcessUser
      Call objSecurity.RemoveHelpContext
    Case SGRepLogTable
      Call objSecurity.AddHelpContext(CWHelpIDTabAudit)
      Call ReportAudit(True)
      Call objSecurity.RemoveHelpContext
    Case SGRepLogUser
      Call objSecurity.AddHelpContext(CWHelpIDLogAudit)
      Call ReportAudit(False)
      Call objSecurity.RemoveHelpContext
    Case Else
      ' el c�digo de proceso no es v�lido y se devuelve falso
      LaunchProcess = False
  End Select
  Call Err.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 39 procesos
  ReDim aProcess(1 To 38, 1 To 4) As Variant
      
  ' VENTANAS
  aProcess(1, 1) = SGWinApps
  aProcess(1, 2) = "Mantenimiento de Aplicaciones"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow
  
  aProcess(2, 1) = SGWinGroups
  aProcess(2, 2) = "Mantenimiento de Grupos"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
    
  aProcess(3, 1) = SGWinUsers
  aProcess(3, 2) = "Mantenimiento de Usuarios"
  aProcess(3, 3) = True
  aProcess(3, 4) = cwTypeWindow
      
  aProcess(4, 1) = SGWinRoles
  aProcess(4, 2) = "Mantenimiento de Roles"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow
      
  aProcess(5, 1) = SGWinRolesGroup
  aProcess(5, 2) = "Relaci�n entre Grupos y Roles"
  aProcess(5, 3) = True
  aProcess(5, 4) = cwTypeWindow
      
  aProcess(6, 1) = SGWinTypeProcess
  aProcess(6, 2) = "Consulta de Tipos de Procesos"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(7, 1) = SGWinProcess
  aProcess(7, 2) = "Mantenimiento de Procesos"
  aProcess(7, 3) = True
  aProcess(7, 4) = cwTypeWindow
      
  aProcess(8, 1) = SGWinFuncs
  aProcess(8, 2) = "Mantenimiento de Funciones"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow
      
  aProcess(9, 1) = SGWinSegTabCol
  aProcess(9, 2) = "Seguridad en Tablas y Columnas"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow
      
  aProcess(10, 1) = SGWinChangePW
  aProcess(10, 2) = "Cambio de la Clave de Acceso"
  aProcess(10, 3) = True
  aProcess(10, 4) = cwTypeWindow
      
  aProcess(11, 1) = SGWinForzePW
  aProcess(11, 2) = "Forzar nueva Clave de Acceso"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow
      
  aProcess(12, 1) = SGWinProcessRol
  aProcess(12, 2) = "Relaci�n entre Procesos y Roles"
  aProcess(12, 3) = True
  aProcess(12, 4) = cwTypeWindow
      
  aProcess(13, 1) = SGWinDelega
  aProcess(13, 2) = "Mantenimiento de Delegaciones"
  aProcess(13, 3) = True
  aProcess(13, 4) = cwTypeWindow
      
  aProcess(14, 1) = SGWinPurgeDelega
  aProcess(14, 2) = "Eliminaci�n de Delegaciones"
  aProcess(14, 3) = True
  aProcess(14, 4) = cwTypeWindow
      
  aProcess(15, 1) = SGWinQueryDelega
  aProcess(15, 2) = "Consulta de Delegaciones"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow
      
 ' aProcess(16, 1) = SGWinVenLis
 ' aProcess(16, 2) = "Relaci�n Ventanas y Listados"
 ' aProcess(16, 3) = True
 ' aProcess(16, 4) = cwTypeWindow
      
  aProcess(16, 1) = SGWinPrnTabCol
  aProcess(16, 2) = "Tablas y Columnas para Informes Externos"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow
      
  aProcess(17, 1) = SGWinEncript
  aProcess(17, 2) = "Algoritmo de Encriptaci�n"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow
      
  aProcess(18, 1) = SGWinAuditTables
  aProcess(18, 2) = "Gesti�n de Tablas a Auditar"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
      
  aProcess(19, 1) = SGWinViewAudit
  aProcess(19, 2) = "Consulta de Log de Auditor�a"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
      
  aProcess(20, 1) = SGWinMaintAudit
  aProcess(20, 2) = "Gesti�n del Log de Auditor�a"
  aProcess(20, 3) = True
  aProcess(20, 4) = cwTypeWindow
      
  aProcess(21, 1) = SGWinLoadProcess
  aProcess(21, 2) = "Carga Autom�tica de Procesos"
  aProcess(21, 3) = True
  aProcess(21, 4) = cwTypeWindow
      
  ' LISTADOS
  aProcess(22, 1) = SGRepApps
  aProcess(22, 2) = "Relaci�n de Aplicaciones"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeReport
      
  aProcess(23, 1) = SGRepGroups
  aProcess(23, 2) = "Relaci�n de Grupos de Usuarios"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeReport
  
  aProcess(24, 1) = SGRepRoles
  aProcess(24, 2) = "Relaci�n de Roles"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeReport
  
  aProcess(25, 1) = SGRepRolesGroup
  aProcess(25, 2) = "Relaci�n de Roles por Grupo"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeReport

  aProcess(26, 1) = SGRepUsers
  aProcess(26, 2) = "Relaci�n de Usuarios"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeReport
  
  aProcess(27, 1) = SGRepUsersGroup
  aProcess(27, 2) = "Relaci�n de Usuarios por Grupo"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeReport

  aProcess(28, 1) = SGRepUsersRol
  aProcess(28, 2) = "Relaci�n de Usuarios por Rol"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeReport

  aProcess(29, 1) = SGRepProcessRol
  aProcess(29, 2) = "Relaci�n de Procesos por Rol"
  aProcess(29, 3) = False
  aProcess(29, 4) = cwTypeReport

  aProcess(30, 1) = SGRepProcessUser
  aProcess(30, 2) = "Relaci�n de Procesos por Usuarios"
  aProcess(30, 3) = True
  aProcess(30, 4) = cwTypeReport

  aProcess(31, 1) = SGRepProcessApp
  aProcess(31, 2) = "Relaci�n de Procesos por Aplicaci�n"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeReport

  aProcess(32, 1) = SGRepTablesRol
  aProcess(32, 2) = "Restricciones en Tablas por Rol"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeReport

  aProcess(33, 1) = SGRepTablesUser
  aProcess(33, 2) = "Restricciones en Tablas por Usuario"
  aProcess(33, 3) = False
  aProcess(33, 4) = cwTypeReport

  aProcess(34, 1) = SGRepColumnsRol
  aProcess(34, 2) = "Restricciones en Columnas por Rol"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeReport

  aProcess(35, 1) = SGRepColumnsUser
  aProcess(35, 2) = "Restricciones en Columnas por Usuario"
  aProcess(35, 3) = False
  aProcess(35, 4) = cwTypeReport

  aProcess(36, 1) = SGRepAuditTables
  aProcess(36, 2) = "Relaci�n de Tablas a auditar"
  aProcess(36, 3) = False
  aProcess(36, 4) = cwTypeReport

  aProcess(37, 1) = SGRepLogTable
  aProcess(37, 2) = "Relaci�n del Log de Auditor�a por Tabla"
  aProcess(37, 3) = True
  aProcess(37, 4) = cwTypeReport

  aProcess(38, 1) = SGRepLogUser
  aProcess(38, 2) = "Relaci�n del Log de Auditor�a por Usuario"
  aProcess(38, 3) = True
  aProcess(38, 4) = cwTypeReport
End Sub
