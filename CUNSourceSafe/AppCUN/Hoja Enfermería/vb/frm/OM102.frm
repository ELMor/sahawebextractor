VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmProtocolos 
   Caption         =   "Suspender Protocolos"
   ClientHeight    =   4620
   ClientLeft      =   3555
   ClientTop       =   2775
   ClientWidth     =   9780
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4620
   ScaleWidth      =   9780
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtHistoria 
      Alignment       =   2  'Center
      BackColor       =   &H8000000B&
      DataField       =   "CI22NUMHISTORIA"
      Height          =   285
      Left            =   8280
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   360
      Width           =   975
   End
   Begin VB.TextBox txtCama 
      Alignment       =   2  'Center
      BackColor       =   &H8000000B&
      DataField       =   "AD15CODCAMA"
      Height          =   285
      Left            =   6840
      Locked          =   -1  'True
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   360
      Width           =   735
   End
   Begin VB.CommandButton cmdBorrar 
      Height          =   495
      Left            =   8640
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2280
      Width           =   975
   End
   Begin ComctlLib.ListView lstTrat 
      Height          =   3255
      Left            =   4560
      TabIndex        =   0
      Top             =   1080
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   5741
      View            =   3
      Arrange         =   1
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   ""
         Object.Width           =   7056
      EndProperty
   End
   Begin ComctlLib.ListView lstProtocolos 
      Height          =   3255
      Left            =   360
      TabIndex        =   2
      Top             =   1080
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   5741
      View            =   3
      Arrange         =   1
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      _Version        =   327682
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   1
      BeginProperty ColumnHeader(1) {0713E8C7-850A-101B-AFC0-4210102A8DA7} 
         Key             =   ""
         Object.Tag             =   ""
         Text            =   ""
         Object.Width           =   5644
      EndProperty
   End
   Begin VB.Label lblNombre 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1320
      TabIndex        =   7
      Top             =   360
      Width           =   75
   End
   Begin VB.Label Label5 
      Caption         =   "Historia"
      Height          =   255
      Left            =   7680
      TabIndex        =   6
      Top             =   360
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "Cama"
      Height          =   255
      Left            =   6360
      TabIndex        =   5
      Top             =   360
      Width           =   495
   End
   Begin VB.Image Image1 
      BorderStyle     =   1  'Fixed Single
      Height          =   735
      Left            =   360
      Stretch         =   -1  'True
      Top             =   120
      Width           =   855
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   360
      X2              =   9360
      Y1              =   960
      Y2              =   960
   End
   Begin ComctlLib.ImageList iml1 
      Left            =   120
      Top             =   4440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   25
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":0C68
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":0F82
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":129C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":15B6
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":1830
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":1E92
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":21AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":24C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":27E0
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":29BA
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":2CD4
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":2FEE
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":3308
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":3622
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":393C
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":3C56
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":3F70
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":428A
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":45A4
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":48BE
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OM102.frx":4BD8
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmProtocolos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim PicisData As String


Private Sub cmdBorrar_Click()
Dim Orderdboid As String, str$, str1$, strSql$, fechaINICIO$, strTodo$, strUp$, strTask$, strETASK$, strCont$
Dim rd As rdoResultset
Dim Qy As rdoQuery
Dim qy1 As rdoQuery
Dim rd1 As rdoResultset
Dim qyTodo As rdoQuery
Dim rdTodo As rdoResultset
Dim qyDoing As rdoQuery
Dim rdDoing As rdoResultset
Dim qyUp As rdoQuery
Dim qySql As rdoQuery
Dim rdSql As rdoResultset
Dim qyETASK As rdoQuery
Dim rdETASK As rdoResultset
Dim qytask As rdoQuery
Dim qyCont As rdoQuery
Dim rdCont As rdoResultset
Dim TaskDboid As String * 21
Dim StaffDboid As String
Dim i%
Dim blnFirst As Boolean


Me.MousePointer = vbHourglass
str = "SELECT STAFFDBOID FROM STAFF WHERE USERNAME = ? "
Set Qy = objApp.rdoConnect.CreateQuery("", str)
Qy(0) = objSecurity.strUser
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
StaffDboid = rd!StaffDboid
rd.Close
Qy.Close

Err = 0
For i = 1 To lstTrat.ListItems.COUNT '
  If lstTrat.ListItems.Item(i).Selected Then
    objApp.BeginTrans
    Orderdboid = lstTrat.ListItems.Item(i).Key   'OrderDboid de la orden seleccionada
        str = "SELECT * FROM ORDERS WHERE ORDERDBOID = ? "
        Set Qy = objApp.rdoConnect.CreateQuery("", str)
        Qy(0) = Orderdboid
        Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        
        'Obtener la ultima fecha sin validar
        strTodo = "SELECT TODODATE FROM TASKS WHERE ORDERDBOID=? AND TASKSTATUSDBOID=? " & _
        "ORDER BY TODODATE ASC"
        Set qyTodo = objApp.rdoConnect.CreateQuery("", strTodo)
            qyTodo(0) = Orderdboid
            qyTodo(1) = "170000000000000000000" 'TODO
        Set rdTodo = qyTodo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
           ' rdTodo.MoveFirst
            
        strETASK = "SELECT COUNT(*) FROM TASKS WHERE ORDERDBOID=? " & _
        "ORDER BY TODODATE ASC"
        Set qyETASK = objApp.rdoConnect.CreateQuery("", strTodo)
            qyETASK(0) = Orderdboid
        Set rdETASK = qyTodo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

                                                                 'hay Task
        If rd!ordertypedboid <> "169000000000001000000" And (rdETASK.RowCount > 0 And Not rdTodo.EOF) Then   'no Orden Continua
          'no orden continua
          If rdTodo.EOF Then 'todo
              MsgBox "Esta orden m�dica no se puede eliminar", vbCritical, "Aviso"
              rdTodo.Close
              qyTodo.Close
              Exit Sub
          Else
          'Comprobar si ser�a una orden cancelada o anulada. Para ello vemos
          'si la orden se est� haciendo
          strSql = "SELECT COUNT(*) FROM ORDERS WHERE ORDERDBOID=? " & _
          "AND ORDERSTATUSDBOID =?"
          Set qyDoing = objApp.rdoConnect.CreateQuery("", strSql)
              qyDoing(0) = Orderdboid
              qyDoing(1) = "170000000000006000000" 'doing
          Set rdDoing = qyDoing.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          End If
          'Modificar el registro para anular la OM
          strUp = "UPDATE ORDERS SET LASTDATE=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ORDERSTATUSDBOID=?"
          strUp = strUp & " WHERE ORDERDBOID =?"
           Set qyUp = objApp.rdoConnect.CreateQuery("", strUp)
           qyUp(0) = Format(rdTodo!TODODATE, "DD/MM/YYYY HH:MM:SS")
           If rdDoing.rdoColumns(0).Value >= 1 Then
              qyUp(1) = "170000000000010000000" 'Discontinued
           Else
              qyUp(1) = "170000000000008000000" 'Cancel
           End If
           qyUp(2) = Orderdboid
           qyUp.Execute
            qyUp.Close
          
          strSql = "SELECT TASKDBOID FROM TASKS WHERE ORDERDBOID=? " & _
          "AND TASKSTATUSDBOID NOT IN (?,?) "
          strSql = strSql & " ORDER BY TODODATE"
          Set qySql = objApp.rdoConnect.CreateQuery("", strSql)
              qySql(0) = Orderdboid
              qySql(1) = "170000000000001000000" 'done
              qySql(2) = "170000000000003000000" 'Hold
          Set rdSql = qySql.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
          rdSql.MoveFirst
          blnFirst = True
          Do While Not rdSql.EOF
              strTask = "UPDATE TASKS SET DONEDATE=SYSDATE, TASKSTATUSDBOID=? " & _
              "WHERE TASKDBOID= ?"
              Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
                  If blnFirst Then
                      qytask(0) = "170000000000009000000" 'firstDiscontinued (la primera linea tiene este status
                      blnFirst = False
                  Else
                      qytask(0) = "170000000000010000000" 'Discontinued
                  End If
                  qytask(1) = rdSql.rdoColumns(0).Value
              qytask.Execute
              rdSql.MoveNext
          Loop
          rdSql.Close
          qySql.Close
          qytask.Close
      Else 'Si es una orden continua
        'Comprobar si ser�a una orden cancelada o anulada. Para ello vemos
        'si la orden se est� haciendo
        str1 = "SELECT INITDATE FROM ORDERS WHERE ORDERDBOID=?"
        Set qy1 = objApp.rdoConnect.CreateQuery("", str1)
        qy1(0) = Orderdboid
        Set rd1 = qy1.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        fechaINICIO = Format(rd1!INITDATE, "DD/MM/YYYY HH:MM:SS")
        rd1.Close
        qy1.Close
        
        If fechaINICIO < strFechaHora_Sistema Then
            fechaINICIO = strFechaHora_Sistema
        End If
        
        'Comprobar si ser�a una orden cancelada o anulada. Para ello vemos
        'si la orden se est� haciendo
        strSql = "SELECT COUNT(*) FROM ORDERS WHERE ORDERDBOID=? " & _
        "AND ORDERSTATUSDBOID =?"
        Set qyDoing = objApp.rdoConnect.CreateQuery("", strSql)
            qyDoing(0) = Orderdboid
            qyDoing(1) = "170000000000006000000" 'doing
        Set rdDoing = qyDoing.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
        
        'Modificar el registro para anular la OM
        strUp = "UPDATE ORDERS SET LASTDATE=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), ORDERSTATUSDBOID=?"
        strUp = strUp & " WHERE ORDERDBOID =?"
         Set qyUp = objApp.rdoConnect.CreateQuery("", strUp)
         qyUp(0) = fechaINICIO
         If rdDoing.rdoColumns(0).Value >= 1 Then
            qyUp(1) = "170000000000010000000" 'Discontinued
         Else
            qyUp(1) = "170000000000008000000" 'Cancel
         End If
         qyUp(2) = Orderdboid
         qyUp.Execute
         qyUp.Close
         
         'Vemos si la orden es continua o no
        strCont = "SELECT  COUNT(*) " & _
        "FROM ORDERS, SCHEDULES, STDORDERORDERSET, STDORDERS " & _
        "WHERE ORDERS.ORDERSETDBOID = STDORDERORDERSET.ORDERSETDBOID AND " & _
        "STDORDERORDERSET.STDORDERDBOID = STDORDERS.STDORDERDBOID AND " & _
        "STDORDERS.SCHEDULEDBOID = SCHEDULES.SCHEDULEDBOID AND " & _
        "(ORDERS.ORDERDBOID=?) AND (SCHEDULES.FREQUENCY=?)"
        Set qyCont = objApp.rdoConnect.CreateQuery("", strCont)
          qyCont(0) = Orderdboid
          qyCont(1) = -1
        Set rdCont = qyCont.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

        
        If rdDoing.rdoColumns(0).Value >= 1 And rdCont.rdoColumns(0).Value = 0 Then  'Ya se esta haciendo: Discontinuar y no es continua
            strUp = "UPDATE TASKS SET TASKSTATUSDBOID= ?, TODODATE=TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), DONEDATE= TO_DATE(?,'DD/MM/YYYY HH24:MI:SS') " & _
            "WHERE ORDERDBOID= ? AND TASKSTATUSDBOID= ?"
            Set qyUp = objApp.rdoConnect.CreateQuery("", strUp)
                qyUp(0) = "170000000000001000000" 'Done
                qyUp(1) = fechaINICIO
                qyUp(2) = fechaINICIO
                qyUp(3) = Orderdboid
                qyUp(4) = "170000000000011000000" 'CurrentTime
            qyUp.Execute
            qyUp.Close
        Else 'Cancelar la orden y ademas es continua
            If Not LOG Then
              MsgBox "Estos cambios no han pasado a Picis" & Chr(13) & Chr(10) _
              & "Usuario desconocido en Picis", vbCritical, "Aviso"
              Exit Sub
            End If
            ierrcode = PcsTaskAlloc(hTask)
            ierrcode = PcsDbObjectGenerateIdentifier(hTask)
            ierrcode = PcsDbObjectGetIdentifier(hTask, TaskDboid, 21)
            ierrcode = PcsTaskFree(hTask)
            'log off de Picis
            ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
            ierrcode = PcsLogFree(hLog)
        
            strTask = "INSERT INTO TASKS (TASKDBOID,CREATIONDATE,TODODATE, DONEDATE, " & _
            "VALIDATEDDATE, DOSE, HASAMEMO, STAFFDBOID, " & _
            "ORDERDBOID, TASKSTATUSDBOID, GROUPDBOID, UNITDBOID) " & _
            "VALUES (?, SYSDATE, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), " & _
            "TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),SYSDATE, ?, ?, ?, ?, ?, ?, ?)"
            Set qytask = objApp.rdoConnect.CreateQuery("", strTask)
            qytask(0) = TaskDboid
            qytask(1) = fechaINICIO
            qytask(2) = fechaINICIO
            qytask(3) = 0
            qytask(4) = "F"
            qytask(5) = StaffDboid
            qytask(6) = Orderdboid
            If rd!ordertypedboid = "169000000000001000000" Then 'Frecuencia continua
              qytask(7) = "170000000000001000000" 'DONE
            Else
              qytask(7) = "170000000000009000000" 'first discontinued
            End If
            qytask(8) = "21000000000025000511" 'validaci�n COM�N
            qytask(9) = Null 'unitdboid
            qytask.Execute
            qytask.Close
            ierrcode = PcsLogDetachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
            ierrcode = PcsLogFree(hLog)
        End If
    End If
    rdDoing.Close
    qyDoing.Close
    rdTodo.Close
    qyTodo.Close
    If Err > 0 Then
        objApp.RollbackTrans
        MsgBox "ERROR: La cancelaci�n de orden m�dica no se ha pasado en Picis"
        Exit Sub
    Else: objApp.CommitTrans
    End If
  End If 'si esta selecionado el listitem
Next
Call lstProtocolos_ItemClick(lstProtocolos.SelectedItem)
Me.MousePointer = vbDefault
End Sub

Private Sub Form_Load()
Dim str$
Dim rd As rdoResultset
Dim Qy As rdoQuery
Dim itmX As ListItem
Dim rdAD As rdoResultset
Dim qyAD As rdoQuery
Dim strNombre
Dim qyNombre As rdoQuery
Dim rdNombre As rdoResultset

Set cmdBorrar.Picture = iml1.ListImages.Item(constTACHAR).Picture
Set Image1.Picture = iml1.ListImages.Item(constLIBROS).Picture
Set Me.Icon = iml1.ListImages.Item(constPROTOCOLOS).Picture


strNombre = "SELECT CI2200.CI22NOMBRE, CI2200.CI22PRIAPEL, " & _
"CI2200.CI22SEGAPEL, CI2200.CI22NUMHISTORIA, GCFN06(AD1500.AD15CODCAMA) AS AD15CODCAMA " & _
"FROM AD1500, CI2200, AD0100 WHERE AD0100.AD01CODASISTENCI= ? AND " & _
"AD0100.CI21CODPERSONA = CI2200.CI21CODPERSONA AND " & _
"AD1500.AD01CODASISTENCI=AD0100.AD01CODASISTENCI"
Set qyNombre = objApp.rdoConnect.CreateQuery("", strNombre)
    qyNombre(0) = codAsistencia
Set rdNombre = qyNombre.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

lblNombre.Caption = rdNombre!ci22nombre & " " & rdNombre!CI22PRIAPEL & _
" " & rdNombre!CI22SEGAPEL
txtHistoria = rdNombre!CI22NUMHISTORIA
txtCama = rdNombre!AD15CODCAMA

rdNombre.Close
qyNombre.Close

str = "SELECT PICISDATA.PICISDATADBOID " & _
"FROM ADMISSIONS, PICISDATA " & _
"WHERE ADMISSIONS.ADMISSIONDBOID = PICISDATA.ADMISSIONDBOID " & _
"AND (ADMISSIONS.ADMID1=?)"
Set qyAD = objApp.rdoConnect.CreateQuery("", str)
qyAD(0) = codAsistencia
Set rdAD = qyAD.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
PicisData = rdAD!PICISDATADBOID
str = "SELECT   /*+ RULE */ DISTINCT ORDERSETS.ORDERSETDBOID, ORDERSETS.ORDERSETDESC " & _
"FROM ORDERSETS ,STDORDERORDERSET ," & _
" STDORDERS, TREATMENTS,ORDERS " & _
"Where ORDERSETS.ORDERSETDBOID = STDORDERORDERSET.ORDERSETDBOID " & _
"AND STDORDERORDERSET.STDORDERDBOID = STDORDERS.STDORDERDBOID AND " & _
"STDORDERS.TREATMENTDBOID = TREATMENTS.TREATMENTDBOID AND " & _
"ORDERS.ORDERSETDBOID=ORDERSETS.ORDERSETDBOID AND " & _
"ORDERSETS.ISDELETED='F'AND " & _
"ORDERS.PICISDATADBOID=? AND " & _
"(ORDERS.LASTDATE > SYSDATE OR ORDERS.ORDERSTATUSDBOID= 170000000000006000000) AND " & _
"ORDERS.ORDERSTATUSDBOID not in (170000000000008000000,170000000000010000000 ," & _
"170000000000015000000,170000000000001000000) " & _
"ORDER BY ORDERSETDESC ASC"

'str = "SELECT DISTINCT ORDERS.ORDERDBOID, ORDERS.ORDERDESC," & _
'"FROM ORDERS, ORDERSETS,  TREATMENTS, FAMILIES " & _
'"(ORDERS.LASTDATE > SYSDATE OR ORDERS.ORDERSTATUSDBOID= 170000000000006000000) AND " & _
'"ORDERS.ORDERSTATUSDBOID not in (170000000000008000000,170000000000010000000 ," & _
'"170000000000015000000,170000000000001000000) " 'Cancelado, discontinuado, borrado,done
Set Qy = objApp.rdoConnect.CreateQuery("", str)
Qy(0) = PicisData
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Do While Not rd.EOF
  Set itmX = lstProtocolos.ListItems.Add(, rd!ORDERSETDBOID, rd!ORDERSETDESC)
  rd.MoveNext
Loop
End Sub


Private Sub lstProtocolos_ItemClick(ByVal Item As ComctlLib.ListItem)
Dim str$
Dim rd As rdoResultset
Dim Qy As rdoQuery
Dim itmX As ListItem
Dim icono%
lstTrat.ListItems.Clear

Set lstTrat.SmallIcons = iml1
str = "SELECT DISTINCT ORDERS.ORDERDBOID, ORDERS.ORDERDESC, FAMILIES.CATEGORYDBOID " & _
"FROM ORDERS, ORDERSETS,  TREATMENTS, FAMILIES " & _
"Where ORDERSETS.ORDERSETDBOID = ORDERS.ORDERSETDBOID And " & _
"TREATMENTS.TREATMENTDBOID = ORDERS.TREATMENTDBOID AND " & _
"FAMILIES.FAMILYDBOID = TREATMENTS.FAMILYDBOID AND " & _
"ORDERS.PICISDATADBOID = ? And ORDERSETS.ORDERSETDBOID = ? And " & _
"(ORDERS.LASTDATE > SYSDATE OR ORDERS.ORDERSTATUSDBOID= 170000000000006000000) AND " & _
"ORDERS.ORDERSTATUSDBOID not in (170000000000008000000,170000000000010000000 ," & _
"170000000000015000000,170000000000001000000) " 'Cancelado, discontinuado, borrado,done
Set Qy = objApp.rdoConnect.CreateQuery("", str)
Qy(0) = PicisData
Qy(1) = lstProtocolos.SelectedItem.Key
Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
Do While Not rd.EOF
  Select Case rd!CATEGORYDBOID
  Case "157000000000001000000" 'Medicaciones
    icono = constMEDICAMENTOS
  Case "157000000000002000000" 'Fluids in
    icono = constFLUIDOS
  Case "157000000000003000000" 'Nursing care
    icono = constCAMA
  Case "157000000000004000000" 'Respiratory
    icono = constRESPIRATORIO
  Case "157000000000006000000" 'Assessment
    icono = constVALORACIONES
  Case "157000000000007000000" 'Puntuaciones
    icono = constPUNTUACION
  Case "157000000000008000000" 'Fluidsout
    icono = constFLUIDSOUT
  Case "157000000000009000000" 'Equipamiento
    icono = constEQUIPAMIENTO
  End Select
  Set itmX = lstTrat.ListItems.Add(, rd!Orderdboid, rd!ORDERDESC, , icono)
  rd.MoveNext
Loop
End Sub

