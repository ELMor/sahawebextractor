Attribute VB_Name = "Funciones"

Public Function productos(Operacion As String, rdil As Long, _
rprod As Long, rprod2 As Long, codProducto1 As Long, codProducto2 As Long, _
codProducto3 As Long)

Select Case Operacion
Case "F"
    codProducto1 = rdil 'Importante el fluido
    If Not IsNull(rprod) Then
        codProducto2 = rprod
    Else: codProducto2 = 0
    End If
    If Not IsNull(rprod2) Then
        codProducto3 = rprod2
    Else: codProducto3 = 0
    End If

Case "M"
    codProducto1 = rdil 'Importante el fluido
    If Not IsNull(rprod) Then
        codProducto2 = rprod
    Else: codProducto2 = 0
    End If
    If Not IsNull(rprod2) Then
        codProducto3 = rprod2
    Else: codProducto3 = 0
    End If
Case "/"
        codProducto1 = rprod 'Importante el medicamento
        If Not IsNull(rdil) Then
           codProducto2 = rdil
        Else: codProducto2 = 0
        End If
Case "P"
    codProducto1 = rdil 'Importante el fluido
    If Not IsNull(rprod) Then
        codProducto2 = rprod
    Else: codProducto2 = 0
    End If
    If Not IsNull(rprod2) Then
        codProducto3 = rprod2
    Else: codProducto3 = 0
    End If
End Select
End Function

Public Function ObtenerDescripcionOrden(codProducto1 As Long, _
codProducto2 As Long, codProducto3 As Long, IntTipoOm As Integer)
'****************************************************************
'En esta funcion fabricamos el string de la orden medica
'****************************************************************
Dim DesOrdenMedica As String
Dim strProducto2 As String
Dim qyProducto2 As rdoQuery
Dim rdProducto2 As rdoResultset
Dim strProductoM As String
Dim qyProductoM As rdoQuery
Dim rdProductoM As rdoResultset
Dim strProducto As String
Dim qyProducto As rdoQuery
Dim rdProducto As rdoResultset
Dim longitud As Long

'Obtenemos el nombre del producto primario de la orden
strProducto = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
Set qyProducto = objApp.rdoConnect.CreateQuery("", strProducto)
    qyProducto(0) = codProducto1
Set rdProducto = qyProducto.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

'Obtenemos el nombre del producto secundario de la orden: sueros
'y aditivos
strProducto2 = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
Set qyProducto2 = objApp.rdoConnect.CreateQuery("", strProducto2)
    qyProducto2(0) = codProducto2
Set rdProducto2 = qyProducto2.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

If IntTipoOm <> 7 And IntTipoOm <> 8 Then
    longitud = Len(rdProducto!FR73DESPRODUCTO)
    DesOrdenMedica = UCase(Mid(rdProducto!FR73DESPRODUCTO, 1, 1)) & _
    LCase(Mid(rdProducto!FR73DESPRODUCTO, 2, longitud - 1))
    DesOrdenMedica = DesOrdenMedica
        If (IntTipoOm = 2 Or IntTipoOm = 4 Or IntTipoOm = 6) And codProducto2 <> 0 Then
            longitud = Len(rdProducto2!FR73DESPRODUCTO)
              DesOrdenMedica = DesOrdenMedica & " (+ " & _
                                UCase(Mid(rdProducto2!FR73DESPRODUCTO, 1, 1)) & _
                                LCase(Mid(rdProducto2!FR73DESPRODUCTO, 2, longitud - 1)) & ")"
        End If
ElseIf IntTipoOm = 7 And codProducto3 = 0 Then 'si es fluidoterapia : primero el fluido y despues el aditivo
    longitud = Len(rdProducto!FR73DESPRODUCTO)
    DesOrdenMedica = UCase(Mid(rdProducto!FR73DESPRODUCTO, 1, 1)) & _
    LCase(Mid(rdProducto!FR73DESPRODUCTO, 2, longitud - 1))
    DesOrdenMedica = DesOrdenMedica
    If rdProducto2.RowCount > 0 Then
        longitud = Len(rdProducto2!FR73DESPRODUCTO)
        If Not IsNull(rdProducto2!FR73DESPRODUCTO) And codProducto2 <> 0 Then
              DesOrdenMedica = DesOrdenMedica & " (+ " & _
                                UCase(Mid(rdProducto2!FR73DESPRODUCTO, 1, 1)) & _
                                LCase(Mid(rdProducto2!FR73DESPRODUCTO, 2, longitud - 1)) & ")"
        End If
    End If
Else 'Si es una mezcla: fluido + aditivos o es un fluido con dos aditivos
    strProductoM = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
    Set qyProductoM = objApp.rdoConnect.CreateQuery("", strProductoM)
        qyProductoM(0) = codProducto3
    Set rdProductoM = qyProductoM.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

    longitud = Len(rdProducto!FR73DESPRODUCTO)
    DesOrdenMedica = UCase(Mid(rdProducto!FR73DESPRODUCTO, 1, 1)) & _
    LCase(Mid(rdProducto!FR73DESPRODUCTO, 2, longitud - 1))
    DesOrdenMedica = DesOrdenMedica

        If Not IsNull(rdProducto2!FR73DESPRODUCTO) And codProducto2 <> 0 _
        And Not IsNull(rdProductoM!FR73DESPRODUCTO) And codProducto3 <> 0 Then
              longitud = Len(rdProducto2!FR73DESPRODUCTO)
              DesOrdenMedica = DesOrdenMedica & " (+ " & _
                                UCase(Mid(rdProducto2!FR73DESPRODUCTO, 1, 1)) & _
                                LCase(Mid(rdProducto2!FR73DESPRODUCTO, 2, longitud - 1))
              longitud = Len(rdProductoM!FR73DESPRODUCTO)
             DesOrdenMedica = DesOrdenMedica & " + " & _
                            UCase(Mid(rdProductoM!FR73DESPRODUCTO, 1, 1)) & _
                            LCase(Mid(rdProductoM!FR73DESPRODUCTO, 2, longitud - 1)) & ")"
    
        End If
    rdProductoM.Close
    qyProductoM.Close
End If
rdProducto.Close
qyProducto.Close
rdProducto2.Close
qyProducto2.Close
ObtenerDescripcionOrden = DesOrdenMedica
End Function

Public Function ObtenerTipoMedicamento(Operacion As String, _
codProducto1 As Long, _
codProducto2 As Long, codProducto3 As Long) As Integer
Dim strMedicamento As String
Dim strAditivo As String
Dim qyAditivo As rdoQuery
Dim strAditivo2 As String
Dim qyAditivo2 As rdoQuery
Dim strgrupo$
Dim qygrupo As rdoQuery
Dim rdgrupo As rdoResultset
Dim IntTipoOm As Integer    'intTipoOM =1 Medicamento como medicamento
                            'intTipoOM =2 Medicamento como perfusion
                            'intTipoOM =3 PRN como medicamento
                            'intTipoOM =4 PRN como perfusion
                            'intTipoOM =5 Estupefaciente como medicamento
                            'intTipoOM =6 Estupefaciente como perfusion
                            'intTipoOM =7 Fluido como fluidoterapia
                            'inttipoOM= 8 Mezcla

Select Case Operacion
    Case "/" 'Prescrito desde medicamento
    strgrupo = "SELECT FR7300.FR73CODPRODUCTO " & _
    "FROM FR7300, FR0900 WHERE FR0900.FR41CODGRUPPROD = '4000' AND " & _
    "FR7300.FR73CODPRODUCTO=FR0900.FR73CODPRODUCTO and " & _
    "FR7300.FR73CODPRODUCTO= ? " 'grupo perfusiones sin suero
    Set qygrupo = objApp.rdoConnect.CreateQuery("", strgrupo)
        qygrupo(0) = codProducto1
    Set rdgrupo = qygrupo.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
    If codProducto2 = 0 And rdgrupo.RowCount = 0 Then 'pasa el medicamento como una linea de medicamento
            IntTipoOm = 1
    Else
            IntTipoOm = 2
    End If
    rdgrupo.Close
    qygrupo.Close
    Case "M" 'Prescrito como mezcla MASIN: REVISAR CUANDO SE DECIDA
            IntTipoOm = 8
    Case "P"
    If codProducto2 = 0 Then 'pasa el medicamento como una linea de medicacion
            IntTipoOm = 3
    Else
            IntTipoOm = 4
    End If
    Case "F" 'prescrito como fluido
            IntTipoOm = 7
    Case "E" 'Prescrito como estupefaciente
    If codProducto2 = 0 Then 'pasa el medicamento como una linea de medicacion
            IntTipoOm = 5
    Else
            IntTipoOm = 6
    End If
    Case "L" 'Formula magistral
        
End Select
ObtenerTipoMedicamento = IntTipoOm
End Function

Public Function ObtenerDesProducto(codProducto As Long) As String
Dim strMedi As String
Dim rdMedi As rdoResultset
Dim qyMedi As rdoQuery
Dim longitud As Long
strMedi = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO= ?"
Set qyMedi = objApp.rdoConnect.CreateQuery("", strMedi)
    qyMedi(0) = codProducto
Set rdMedi = qyMedi.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

longitud = Len(rdMedi!FR73DESPRODUCTO)
ObtenerDesProducto = UCase(Mid(rdMedi!FR73DESPRODUCTO, 1, 1)) & _
LCase(Mid(rdMedi!FR73DESPRODUCTO, 2, longitud - 1))

rdMedi.Close
qyMedi.Close
End Function

Public Function HorasMinutos(Minutos As Long, _
horaInf As Integer, MinInf As Integer)
    horaInf = Int(Minutos / 60)
    MinInf = Minutos - horaInf * 60
End Function

Public Function strHora_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select " & _
"to_char(sysdate,'HH24:MI:SS') from dual")
  strHora_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function
Public Function strFecha_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select " & _
"to_char(sysdate,'DD/MM/YYYY') from dual")
  strFecha_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function
Public Function strFechaHora_Sistema() As String
  Dim Rs As rdoResultset
  Set Rs = objApp.rdoConnect.OpenResultset("select " & _
"to_char(sysdate,'DD/MM/YYYY HH24:MI:SS') from dual")
  strFechaHora_Sistema = Rs.rdoColumns(0)
  Rs.Close
  Set Rs = Nothing
End Function

Public Function fNextClaveSeq(campo$) As Long
'obtiene el siguiente c�digo a utilizar en un registro nuevo utilizando los sequences
    
    Dim SQL$, rsMaxClave As rdoResultset
    
    SQL = "SELECT " & campo & "_SEQUENCE.NEXTVAL FROM DUAL"
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(SQL)
    fNextClaveSeq = rsMaxClave(0)
    rsMaxClave.Close
    
End Function


Public Function LOG() As Boolean
Dim Rs As rdoResultset
Dim qy As rdoQuery
Dim strSql As String
Dim Password As String
Dim Passdes As String
Dim Passenc As String
Dim UsuarioDefecto As Long
Dim hParent As Long


'*******************************************************
'* CONECTARSE A LA BASE DE DATOS                       *
'*******************************************************
strSql = "SELECT SG02PASSW FROM SG0200 WHERE SG02COD=?"

Set qy = objApp.rdoConnect.CreateQuery("", strSql)
qy(0) = strCodUser
Set Rs = qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)

ierrcode = PcsLogAlloc(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error allocating Log", vbExclamation
        LOG = False
        Exit Function
End If
'Conexion a la base de datos
ierrcode = PcsLogAttachHDBC(hLog, objApp.rdoEnv.hEnv, objApp.rdoConnect.hDbc)
If ierrcode <> 0 Then
            MsgBox "Error conectandose a la base de datos", vbExclamation
            LOG = False
            Exit Function
End If
ierrcode = PcsLogSetVirtualUserName(hLog, strCodUser)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOG = False
        Exit Function
End If

Passdes = Desencriptar(Rs!SG02PASSW)

ierrcode = PcsLogSetVirtualPassword(hLog, Passdes) 'Rs!SG02PASSW)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOG = False
        Exit Function
End If
'conexion virtual
ierrcode = PcsLogUseDefaultUser(hLog, False)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error usando el logon virtual", vbExclamation
        LOG = False
        Exit Function
End If
ierrcode = PcsLogLogOn(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
       ' MsgBox "Usuario o contrase�a incorrectas para picis", vbExclamation
        LOG = False
        Exit Function
End If
ierrcode = PcsLogSetDefault(hLog)
If ierrcode <> ERRCODE_ALL_OK Then
        MsgBox "Error conectandose a la base de datos", vbExclamation
        LOG = False
        Exit Function
End If
Rs.Close
qy.Close
LOG = True
End Function

Public Sub UpdateFRP400(CodPeticion As Long, Velocidad As String, _
                        Observaciones As String, txtHora As String, _
                        txtfecFin As String)
Dim STR As String
Dim qy As rdoQuery

''''str = "UPDATE FR2800 SET FR28VELPERFUSION=? AND FR28INSTRADMI=?"
STR = "UPDATE FRP400 SET FRP4VELPERFUSION=?, FRP4INSTRADMIN=?, " & _
"FRP4HORAFIN=?, FRP4FECFIN=TO_DATE(?,'DD/MM/YYYY') where " & _
"FRP4CODPETICION=?"
Set qy = objApp.rdoConnect.CreateQuery("", STR)
qy(0) = Velocidad
qy(1) = Observaciones
qy(2) = txtHora
qy(3) = Format(txtfecFin, "dd/mm/yyyy")
qy(4) = CodPeticion
qy.Execute
qy.Close
End Sub

Public Sub UpdateFecFin(CodPeticion As Long, txtHora As String, _
                        txtfecFin As String)
Dim STR As String
Dim qy As rdoQuery

''''str = "UPDATE FR2800 SET FR28VELPERFUSION=? AND FR28INSTRADMI=?"
STR = "UPDATE FRP400 SET " & _
"FRP4HORAFIN=?, FRP4FECFIN=TO_DATE(?,'DD/MM/YYYY') where " & _
"FRP4CODPETICION=?"
Set qy = objApp.rdoConnect.CreateQuery("", STR)
qy(0) = txtHora
qy(1) = Format(txtfecFin, "dd/mm/yyyy")
qy(2) = CodPeticion
qy.Execute
qy.Close
End Sub

Public Function Desencriptar(texto As String) As String
Dim objRES As New clsCWMRES

    Desencriptar = objRES.DesEncript(texto)
End Function
Public Function Encriptar(texto As String) As String
Dim objRES As New clsCWMRES

    Encriptar = objRES.Encript(texto)
End Function


