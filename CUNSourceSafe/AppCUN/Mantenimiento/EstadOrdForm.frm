VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form EstadOrdForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Estadísticas"
   ClientHeight    =   8595
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   11220
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   11220
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command2 
      Caption         =   "&Calcular"
      Height          =   375
      Left            =   9840
      TabIndex        =   8
      Top             =   240
      Width           =   1335
   End
   Begin SSCalendarWidgets_A.SSDateCombo Fecha2Box 
      Height          =   255
      Left            =   7560
      TabIndex        =   7
      Top             =   240
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo Fecha1Box 
      Height          =   255
      Left            =   5280
      TabIndex        =   6
      Top             =   240
      Width           =   1575
      _Version        =   65537
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Data Data2 
      Caption         =   "Data2"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   0
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Salir"
      Height          =   495
      Left            =   6960
      TabIndex        =   0
      Top             =   10440
      Width           =   1335
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   360
      Visible         =   0   'False
      Width           =   2295
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7575
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   13361
      _Version        =   327681
      Tabs            =   6
      Tab             =   3
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Distribución de las O.T."
      TabPicture(0)   =   "EstadOrdForm.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "MSChart1"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "O.T.Pendientes"
      TabPicture(1)   =   "EstadOrdForm.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "MSChart2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "O.T.por Especialidades"
      TabPicture(2)   =   "EstadOrdForm.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "MSChart3"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Distribución de O.T. por Departamentos"
      TabPicture(3)   =   "EstadOrdForm.frx":0054
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).Control(0)=   "MSChart4"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Tiempos de Reparación por Especialidades"
      TabPicture(4)   =   "EstadOrdForm.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "MSChart5"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Tiempos de Reparación por Departamentos"
      TabPicture(5)   =   "EstadOrdForm.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "MSChart6"
      Tab(5).ControlCount=   1
      Begin MSChartLib.MSChart MSChart4 
         Height          =   6015
         Left            =   480
         OleObjectBlob   =   "EstadOrdForm.frx":00A8
         TabIndex        =   5
         Top             =   1080
         Width           =   10815
      End
      Begin MSChartLib.MSChart MSChart3 
         Height          =   6495
         Left            =   -74880
         OleObjectBlob   =   "EstadOrdForm.frx":23F8
         TabIndex        =   4
         Top             =   840
         Width           =   11175
      End
      Begin MSChartLib.MSChart MSChart2 
         Height          =   7935
         Left            =   -74880
         OleObjectBlob   =   "EstadOrdForm.frx":4A71
         TabIndex        =   3
         Top             =   960
         Width           =   11175
      End
      Begin MSChartLib.MSChart MSChart1 
         Height          =   7815
         Left            =   -74640
         OleObjectBlob   =   "EstadOrdForm.frx":6DD2
         TabIndex        =   2
         Top             =   1020
         Width           =   13935
      End
      Begin MSChartLib.MSChart MSChart5 
         Height          =   6255
         Left            =   -74640
         OleObjectBlob   =   "EstadOrdForm.frx":9133
         TabIndex        =   9
         Top             =   1200
         Width           =   10935
      End
      Begin MSChartLib.MSChart MSChart6 
         Height          =   6735
         Left            =   -74760
         OleObjectBlob   =   "EstadOrdForm.frx":B7AC
         TabIndex        =   10
         Top             =   840
         Width           =   10935
      End
   End
   Begin VB.Line Line1 
      X1              =   6960
      X2              =   7440
      Y1              =   360
      Y2              =   360
   End
End
Attribute VB_Name = "EstadOrdForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    Unload Me
End Sub

Private Sub Command2_Click()
    Dim nada As Integer
    nada = Calcula(Fecha1Box.Text, Fecha2Box.Text)
    
End Sub

Private Function Calcula(Fecha1 As String, Fecha2 As String)
    Dim i As Integer, j As Integer
    Dim X1(1 To 3, 1 To 4) As Variant
    Dim X2(1 To 2, 1 To 4) As Variant
    Dim X3(1 To 2, 1 To 11) As Variant
    Dim X4(1 To 2, 1 To 118) As Variant
    Dim col As Integer, Nombre As String, Col2 As Integer, Nombre2 As String
    Dim Espe As Integer
    
    On Error GoTo msgError
    
    'Preparación del primer chart
    
    MSChart1.chartType = VtChChartType2dPie
    
    X1(2, 1) = "Distribución de Órdenes"
    X1(3, 1) = "Distribución de horas de reparación"
    X1(1, 2) = "Reparaciones"
    X1(1, 3) = "Mant.Preventivo"
    X1(1, 4) = "Instalaciones"
    
    With Data1
        .RecordSource = "SELECT NOrden FROM Orden WHERE TipoOrden='Reparación' AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
        .Refresh
        X1(2, 2) = .Recordset.RecordCount
        .RecordSource = "SELECT NOrden FROM Orden WHERE TipoOrden='Mant.Preventivo' AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
        .Refresh
        X1(2, 3) = .Recordset.RecordCount
        .RecordSource = "SELECT NOrden FROM Orden WHERE TipoOrden='Instalación' AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
        .Refresh
        X1(2, 4) = .Recordset.RecordCount
        .RecordSource = "SELECT SUM(TiempoReparacion) FROM Orden WHERE Finalizada=True AND TipoOrden='Reparación' AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        .Refresh
        X1(3, 2) = .Recordset.Fields(0).Value
        .RecordSource = "SELECT SUM(TiempoReparacion) FROM Orden WHERE Finalizada=True AND TipoOrden='Mant.Preventivo' AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        .Refresh
        X1(3, 3) = .Recordset.Fields(0).Value
        .RecordSource = "SELECT SUM(TiempoReparacion) FROM Orden WHERE Finalizada=True AND TipoOrden='Instalación' AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        .Refresh
        X1(3, 4) = .Recordset.Fields(0).Value

    End With
    
    MSChart1 = X1
    
    'Preparación del segundo chart
    
    MSChart2.chartType = VtChChartType2dPie
    
    X2(2, 1) = "O.T. Pendientes"
    X2(1, 2) = "Reparaciones"
    X2(1, 3) = "Mant.Preventivo"
    X2(1, 4) = "Instalaciones"
    
    With Data1
        .RecordSource = "SELECT NOrden FROM Orden WHERE TipoOrden='Reparación' AND Finalizada=False AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
        .Refresh
        X2(2, 2) = .Recordset.RecordCount
        .RecordSource = "SELECT NOrden FROM Orden WHERE TipoOrden='Mant.Preventivo' AND Finalizada=False AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
        .Refresh
        X2(2, 3) = .Recordset.RecordCount
        .RecordSource = "SELECT NOrden FROM Orden WHERE TipoOrden='Instalación' AND Finalizada=False AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
        .Refresh
        X2(2, 4) = .Recordset.RecordCount
    End With
    
    MSChart2 = X2
    
    'Preparación del tercer chart
    
    MSChart3.chartType = VtChChartType2dBar
    
    With Data1
        .RecordSource = "SELECT T.CodigoEspecialidad, T.Especialidad FROM Orden O, Tarifas T WHERE O.CodigoEspecialidad=T.CodigoEspecialidad AND O.NOrden<>0 AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY T.CodigoEspecialidad, T.Especialidad"
        .Refresh
        .Recordset.MoveFirst
        Espe = .Recordset.RecordCount
        MSChart3.ColumnCount = Espe
        MSChart3.RowCount = 1
    End With
        
    For i = 1 To Data1.Recordset.RecordCount
        If Data1.Recordset.Fields("Especialidad").Value <> "" Then
            Nombre = Data1.Recordset.Fields("Especialidad").Value
            j = Data1.Recordset.Fields("CodigoEspecialidad").Value
            With Data2
                .RecordSource = "SELECT NOrden FROM Orden WHERE CodigoEspecialidad=" & j & " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
                .Refresh
                If .Recordset.RecordCount <> 0 Then
                    col = col + 1
                    MSChart3.Row = 1
                    MSChart3.Column = col
                    MSChart3.Data = .Recordset.RecordCount
                    MSChart3.ColumnLabel = Nombre
                End If
            End With
            Data1.Recordset.MoveNext
        End If
    Next i
    
    MSChart3.Title = "Distribución de O.T. por Especialidades"
    
    'Preparación del cuarto chart
    
    MSChart4.chartType = VtChChartType2dPie
    
    
    With Data1
        .RecordSource = "SELECT L.CodigoLugar FROM Lugar L, Orden O  WHERE O.CodigoLugar=L.CodigoLugar AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY L.CodigoLugar"
        .Refresh
        .Recordset.MoveFirst
        MSChart4.ColumnCount = .Recordset.RecordCount
        MSChart4.RowCount = 1
    End With
    
    For i = 1 To Data1.Recordset.RecordCount
        j = Data1.Recordset.Fields("CodigoLugar").Value
        With Data2
            .RecordSource = "SELECT O.NOrden, L.Localizacion FROM Orden O, Lugar L WHERE O.CodigoLugar=" & j & " AND O.CodigoLugar=L.CodigoLugar AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY NOrden"
            .Refresh
            Nombre2 = .Recordset.Fields("Localizacion").Value
            If .Recordset.RecordCount <> 0 Then
                Col2 = Col2 + 1
                MSChart4.Column = Col2
                MSChart4.Row = 1
                MSChart4.Data = .Recordset.RecordCount
                MSChart4.ColumnLabel = Nombre2
            End If
            
        End With
        Data1.Recordset.MoveNext
    
    Next i
        
    MSChart4.Title = "Distribución de O.T. por Departamentos"
    MSChart4.Title.TextLayout.Orientation = VtOrientationHorizontal
    MSChart4.Legend.TextLayout.Orientation = VtOrientationUp


    'Preparación del 5 chart
    
    MSChart5.chartType = VtChChartType2dBar
    
    MSChart5.ColumnCount = Espe
    MSChart5.RowCount = 1

    Data1.RecordSource = "SELECT SUM(O.TiempoReparacion), E.Especialidad  FROM Orden O, Tarifas E  WHERE O.CodigoEspecialidad=E.CodigoEspecialidad AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY O.CodigoEspecialidad, E.Especialidad"
    Data1.Refresh
        
    col = 0
        
    For i = 1 To Data1.Recordset.RecordCount
        
        Nombre = Data1.Recordset.Fields(1).Value
        j = Data1.Recordset.Fields(0).Value
        col = col + 1
        MSChart5.Row = 1
        MSChart5.Column = col
        MSChart5.Data = j
        MSChart5.ColumnLabel = Nombre
        Data1.Recordset.MoveNext

    Next i
    
    MSChart5.Title = "Distribución de Tiempos de Reparación por Especialidades"

    'Preparación del 6 chart
    
    MSChart6.chartType = VtChChartType2dPie
    
    With Data1
        .RecordSource = "SELECT SUM(O.TiempoReparacion), L.Localizacion  FROM Orden O, Lugar L  WHERE O.CodigoLugar=L.CodigoLugar AND O.FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY L.Localizacion"
        .Refresh
        .Recordset.MoveFirst
        MSChart6.ColumnCount = .Recordset.RecordCount
        MSChart6.RowCount = 1
    End With
    
    Col2 = 0
    
    For i = 1 To Data1.Recordset.RecordCount
        Nombre2 = Data1.Recordset.Fields("Localizacion").Value
        Col2 = Col2 + 1
        MSChart6.Column = Col2
        MSChart6.Row = 1
        MSChart6.Data = Data1.Recordset.Fields(0).Value
        MSChart6.ColumnLabel = Nombre2
        Data1.Recordset.MoveNext
    
    Next i
        
    MSChart6.Title = "Distribución de Tiempos de Reparación por Departamentos"
    MSChart6.Title.TextLayout.Orientation = VtOrientationHorizontal
    MSChart6.Legend.TextLayout.Orientation = VtOrientationUp

    Exit Function
msgError:
    MsgBox Err.Description, vbCritical


End Function

