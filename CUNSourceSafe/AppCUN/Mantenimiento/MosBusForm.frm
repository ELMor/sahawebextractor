VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form MosBusForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Resultado"
   ClientHeight    =   4560
   ClientLeft      =   1920
   ClientTop       =   1905
   ClientWidth     =   8715
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4560
   ScaleWidth      =   8715
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command1 
      Caption         =   "&Excel"
      Height          =   375
      Left            =   5640
      TabIndex        =   6
      Top             =   3960
      Width           =   1095
   End
   Begin VB.TextBox OpcionBox 
      Height          =   285
      Left            =   840
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.CommandButton ImpBoton 
      Caption         =   "&Imprimir"
      Height          =   375
      Left            =   3840
      TabIndex        =   2
      Top             =   3960
      Width           =   1095
   End
   Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
      Bindings        =   "MosBusForm.frx":0000
      Height          =   3255
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   8175
      _Version        =   131078
      AllowUpdate     =   0   'False
      SelectTypeRow   =   1
      SelectByCell    =   -1  'True
      MaxSelectedRows =   999
      RowHeight       =   423
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      UseDefaults     =   0   'False
      _ExtentX        =   14420
      _ExtentY        =   5741
      _StockProps     =   79
   End
   Begin VB.Data ResData 
      Caption         =   "Resultado"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   0
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   3840
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.CommandButton SalirBoton 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   2160
      TabIndex        =   0
      Top             =   3960
      Width           =   1095
   End
   Begin VB.Label RegBox 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   4080
      TabIndex        =   4
      Top             =   120
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "N� Registros:"
      Height          =   255
      Left            =   2880
      TabIndex        =   3
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "MosBusForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
  'pasa los datos a la hoja de calculo excel
   Dim Columnas As Integer, j As Integer, ColExcel As Integer, FilaExcel As Integer, Campo As Integer
   Dim Record As Recordset
   
   Set Record = ResData.Recordset
   If Display(4) = "CodigoMaquina (SECUENCIAL)" Then campos(4) = ""
   Columnas = ResData.Recordset.Fields.Count
   With Excel.Application
    .Workbooks.Add
    .Worksheets.Add
    .Visible = True
    'pone el nombre de los campos como cabeceras de las columnas
    FilaExcel = 1
    For ColExcel = 1 To Columnas
      Campo = ColExcel - 1
      .Cells(FilaExcel, ColExcel) = Record.Fields(Campo).Name
      .Cells(FilaExcel, ColExcel).Font.Bold = True
    Next
    'rellena el resto del la hoja de calculo con los los registros
    ResData.Recordset.MoveFirst
    FilaExcel = 2
      While Not Record.EOF
        For ColExcel = 1 To Columnas
          Campo = ColExcel - 1
          If Not IsNull(Record.Fields(Campo)) Then .Cells(FilaExcel, ColExcel) = Record.Fields(Campo)
        Next
        FilaExcel = FilaExcel + 1
        Record.MoveNext
      Wend
    End With
End Sub

Private Sub Form_Load()
  'Crea la SELECT completa que aparece en el grid
  
  With ResData
    .RecordSource = "SELECT " & SELE & " FROM " & FRO & IIf(WHER <> "", " WHERE " + WHER, "") & IIf(ORD <> "", " ORDER BY " & ORD, "")
    .Refresh
    RegBox.Caption = .Recordset.RecordCount
  End With
End Sub

Private Sub ImpBoton_Click()
  'Imprime el resultado en un documento word
  Dim wdDoc As Word.Document
  Dim wdApp As Word.Application
  Dim Filas As Long, Columnas As Long
  Dim Rango As Range
  Dim Col As Integer, Fil As Integer
                 
  Set wdApp = New Word.Application
  Set wdDoc = wdApp.Documents.Add("c:\archivos de programa\almacen\doc\listados.dot")
  wdDoc.Activate
  With ResData.Recordset
    Columnas = .Fields.Count
    Filas = .RecordCount
    Set Rango = wdDoc.Bookmarks("Listado").Range
    wdApp.ActiveDocument.Tables.Add Rango, Filas, Columnas
    wdApp.Selection.HomeKey Unit:=wdLine
    'pone el nombre del campo como cabeceras de las columnas
    For Col = 1 To Columnas
      wdApp.Selection.Font.Bold = True
      wdApp.Selection.TypeText Text:=.Fields(Col - 1).Name
      wdApp.Selection.MoveRight Unit:=wdCell
    Next
    wdApp.Selection.Font.Bold = False
    .MoveFirst
    Fil = 2
    While Not .EOF
      For Col = 1 To Columnas
        wdDoc.Tables(1).Cell(Fil, Col).Select
        If Not IsNull(.Fields(Col - 1)) Then wdApp.Selection.TypeText Text:=.Fields(Col - 1)
      Next
      Fil = Fil + 1
      .MoveNext
    Wend
  End With
  wdApp.Visible = True
  Set wdApp = Nothing
  Set wdDoc = Nothing
End Sub

Private Sub SalirBoton_Click()
  'sale de la pantalla de presentacion de resultados
    Unload Me
End Sub

Private Sub SSDBGrid1_DblClick()
    Dim Aal As Variant
    
    If OpcionBox.Text = "0" Then Exit Sub
    With ResData.Recordset
      Aal = .Fields(Display(0)).Value      'valor del primer campo del registro selecionado
      If Selct2 = 1 Then
        MantArt.GrArtCombo.Text = .Fields("GrupoArt").Value
        Selct2 = 0
      End If
    End With
    If Selct2 = 2 Then
      Objeto.Recordset.FindFirst (Display(0) & "='" & Aal & "'")
    Else
      Objeto.Recordset.FindFirst (Display(0) & "=" & Aal)
    End If
    If Selct = True Then
      If DataCarac2 = "" Then
        Objeto2.RecordSource = DataCarac & Aal
      Else
        Objeto2.RecordSource = DataCarac & Aal & DataCarac2
      End If
      Objeto2.Refresh
    End If
    Selct2 = 0
    Unload Me
End Sub
