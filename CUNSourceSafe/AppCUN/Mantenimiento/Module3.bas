Attribute VB_Name = "Informes"
Public Function GastosInf(Opcion As String, Codigo As Long, Fecha1 As String, Fecha2 As String)

    Dim wdApp As Word.Application
    Dim wdDoc As Word.Document
    Dim i As Long, Var As String, Busq(3) As String, j As Integer, Total(3) As Long
    Dim Ant As Long
    Dim nada As Integer
On Error GoTo msgError
    Set wdApp = New Word.Application
    Set wdDoc = wdApp.Documents.add("c:\archivos de programa\almacen\doc\inforgastos.dot")
    wdDoc.Activate
    
    Load DBase
    With DBase
        Select Case Opcion
        Case "Seccion"
            Var = "CodigoEspecialidad"
        Case "Departamento"
            Var = "CodigoDepartamento"
        Case "Area de Trabajo"
            Var = "CodigoMaquina"
        End Select
        
        'C�lculo de los gastos por pedidos exteriores
        
        If Var = "CodigoDepartamento" Then
            Var = "CodigoLugar"
        End If
        
        Busq(0) = "SELECT DISTINCT PP.NPedido,A.Descripcion, PP.Cantidad, A.PrecioUnit, AA.Descuento "
        Busq(1) = "FROM Pedido P, PPedido PP, Orden O, Articulos A, AAlbaran AA "
        Busq(2) = "WHERE O." & Var & "=" & Codigo & " AND P.NOrden=O.NOrden AND PP.NPedido=P.NPedido AND PP.CodigoArticulo=A.CodigoArticulo "
        Busq(3) = " AND FechaPedido BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# AND AA.NPedido=P.NPedido AND AA.CodigoArticulo=PP.CodigoArticulo"
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
        Ant = .PedidoData.Recordset.RecordCount
        
        For i = 1 To Ant
            wdDoc.Tables(1).Rows.add
            For j = 0 To 3
                wdDoc.Tables(1).Cell(i + 1, j + 1).Select
                Selection.Text = .PedidoData.Recordset.Fields(j).Value
                Selection.Font.Bold = False
            Next j
            wdDoc.Tables(1).Cell(i + 1, 5).Select
            Total(0) = (.PedidoData.Recordset.Fields("Cantidad").Value) * (.PedidoData.Recordset.Fields("PrecioUnit").Value) * (1 - (.PedidoData.Recordset.Fields("Descuento").Value / 100))
            Total(1) = Total(1) + Total(0)
            Selection.Font.Bold = False
            Selection.Text = Total(0)
            .PedidoData.Recordset.MoveNext
        Next i
                
        If Var = "CodigoLugar" Then
            Var = "CodigoDepartamento"
        End If
        
        Busq(1) = "FROM Pedido P, PPedido PP, Articulos A, AAlbaran AA "
        Busq(2) = "WHERE P." & Var & "=" & Codigo & " AND A.CodigoArticulo=PP.CodigoArticulo AND P.NPedido=PP.NPedido"
        Busq(3) = " AND FechaPedido BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# AND AA.NPedido=P.NPedido AND AA.CodigoArticulo=PP.CodigoArticulo"
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
    
        If .PedidoData.Recordset.RecordCount <> 0 Then
            .PedidoData.Recordset.MoveLast
            .PedidoData.Recordset.MoveFirst
            For i = Ant To (Ant + .PedidoData.Recordset.RecordCount - 1)
                wdDoc.Tables(1).Rows.add
                For j = 0 To 3
                    wdDoc.Tables(1).Cell(i + 1, j + 1).Select
                    Selection.Text = .PedidoData.Recordset.Fields(j).Value
                    Selection.Font.Bold = False
                Next j
                wdDoc.Tables(1).Cell(i + 1, 5).Select
                Total(0) = (.PedidoData.Recordset.Fields("Cantidad").Value) * (.PedidoData.Recordset.Fields("PrecioUnit").Value) * (1 - ((.PedidoData.Recordset.Fields("Descuento").Value) / 100))
                Total(1) = Total(1) + Total(0)
                Selection.Font.Bold = False
                Selection.Text = Total(0)
                .PedidoData.Recordset.MoveNext
            Next i
        End If
        wdDoc.Range(wdDoc.Bookmarks("TOTALPEDIDOS").Start, wdDoc.Bookmarks("TOTALPEDIDOS").End).Text = Total(1)
    
        'C�lculo de los gastos por S.A.T.
        
        Total(0) = 0
        Total(1) = 0
        
        Select Case Opcion
            
        Case "Seccion"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, F.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, SATFact F, SATFFactura FF, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoEspecialidad=" & Codigo
            Busq(3) = " AND P.NPetSAT=A.NPetSAT AND A.NAlbaranSAT=FF.NAlbaranSAT AND F.NFacturaSAT=FF.NFacturaSAT AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY P.FechaEnvio, P.SAT, F.Total"
        Case "Departamento"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, F.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, SATFact F, SATFFactura FF, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoLugar=" & Codigo
            Busq(3) = " AND P.NPetSAT=A.NPetSAT AND A.NAlbaranSAT=FF.NAlbaranSAT AND F.NFacturaSAT=FF.NFacturaSAT AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY P.FechaEnvio, P.SAT, F.Total"
        Case "Area de Trabajo"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, F.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, SATFact F, SATFFactura FF, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoMaquina=" & Codigo
            Busq(3) = " AND P.NPetSAT=A.NPetSAT AND A.NAlbaranSAT=FF.NAlbaranSAT AND F.NFacturaSAT=FF.NFacturaSAT AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY P.FechaEnvio, P.SAT, F.Total"
        
        End Select
        
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount <> 0 Then
            .PedidoData.Recordset.MoveLast
            .PedidoData.Recordset.MoveFirst
        End If
        
        For i = 1 To .PedidoData.Recordset.RecordCount
            wdDoc.Tables(2).Rows.add
            For j = 0 To 2
                wdDoc.Tables(2).Cell(i + 1, j + 1).Select
                Selection.Text = .PedidoData.Recordset.Fields(j).Value
                Selection.Font.Bold = False
            Next j
            Total(1) = Total(1) + .PedidoData.Recordset.Fields("Total").Value
            .PedidoData.Recordset.MoveNext
        Next i
        
        Ant = i
        
        Select Case Opcion
            
        Case "Seccion"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, A.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoEspecialidad=" & Codigo & " AND A.NAlbaranSAT<>ALL(SELECT NAlbaransat FROM SATFFactura)"
            Busq(3) = " AND P.NPetSAT=A.NPetSAT AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Case "Departamento"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, A.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoLugar=" & Codigo & " AND A.NAlbaranSAT<>ALL(SELECT NAlbaransat FROM SATFFactura)"
            Busq(3) = "AND P.NPetSAT=A.NPetSAT AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Case "Area de Trabajo"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, A.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoMaquina=" & Codigo & " AND A.NAlbaranSAT<>ALL(SELECT NAlbaransat FROM SATFFactura)"
            Busq(3) = "AND P.NPetSAT=A.NPetSAT AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        
        End Select
        
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount <> 0 Then
            .PedidoData.Recordset.MoveLast
            .PedidoData.Recordset.MoveFirst
        End If
        
        For i = Ant To Ant + (.PedidoData.Recordset.RecordCount) - 1
            wdDoc.Tables(2).Rows.add
            For j = 0 To 2
                wdDoc.Tables(2).Cell(i + 1, j + 1).Select
                Selection.Text = .PedidoData.Recordset.Fields(j).Value
                Selection.Font.Bold = False
            Next j
            Total(1) = Total(1) + .PedidoData.Recordset.Fields("Total").Value
            .PedidoData.Recordset.MoveNext
        Next i
        
        Ant = i
        
        Select Case Opcion
            
        Case "Seccion"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT "
            Busq(1) = "FROM PedidoSAT P,Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoEspecialidad=" & Codigo & " AND P.NPetSAT<>ALL(SELECT NPetSAT FROM SATAlbaran)"
            Busq(3) = " AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Case "Departamento"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT "
            Busq(1) = "FROM PedidoSAT P, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoLugar=" & Codigo & " AND P.NPetSAT<>ALL(SELECT NPetSAT FROM SATAlbaran)"
            Busq(3) = " AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Case "Area de Trabajo"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT "
            Busq(1) = "FROM PedidoSAT P, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoMaquina=" & Codigo & " AND P.NPetSAT<>ALL(SELECT NPetSAT FROM SATAlbaran)"
            Busq(3) = " AND P.FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        
        End Select
        
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount <> 0 Then
            .PedidoData.Recordset.MoveLast
            .PedidoData.Recordset.MoveFirst
        End If
        
        For i = Ant To Ant + (.PedidoData.Recordset.RecordCount) - 1
            wdDoc.Tables(2).Rows.add
            For j = 0 To 1
                wdDoc.Tables(2).Cell(i + 1, j + 1).Select
                Selection.Text = .PedidoData.Recordset.Fields(j).Value
                Selection.Font.Bold = False
            Next j
            .PedidoData.Recordset.MoveNext
        Next i
        
        wdDoc.Range(wdDoc.Bookmarks("TOTALSAT").Start, wdDoc.Bookmarks("TOTALSAT").End).Text = Total(1)
                    
        'C�lculo de tiempos
        
        Total(0) = 0
        Total(1) = 0
        
        If Var = "CodigoDepartamento" Then
            Var = "CodigoLugar"
        End If
        
        .PedidoData.RecordSource = "SELECT NOrden FROM Orden WHERE " & Var & "=" & Codigo
        .PedidoData.Refresh
        
        For i = 1 To .PedidoData.Recordset.RecordCount
            nada = TiempOrden(.PedidoData.Recordset.Fields("NOrden").Value)
            .PedidoData.Recordset.MoveNext
        Next i

        Busq(0) = "SELECT SUM(TiempoTotal),SUM(TiempoReparacion),SUM(TiempoEspera),SUM(TiempoReparacion),SUM(TiempoParo), SUM(EsperaMaterial) "
        Busq(1) = "FROM Orden "
        Busq(2) = "WHERE " & Var & "=" & Codigo
        Busq(3) = " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
                
        .PedidoData.Recordset.MoveFirst
               
        wdDoc.Range(wdDoc.Bookmarks("TIEMPOTOTAL").Start, wdDoc.Bookmarks("TIEMPOTOTAL").End).Text = .PedidoData.Recordset.Fields(0).Value
        wdDoc.Range(wdDoc.Bookmarks("TIEMPORE").Start, wdDoc.Bookmarks("TIEMPORE").End).Text = .PedidoData.Recordset.Fields(1).Value
        wdDoc.Range(wdDoc.Bookmarks("TIEMPOESRE").Start, wdDoc.Bookmarks("TIEMPOESRE").End).Text = .PedidoData.Recordset.Fields(2).Value
        wdDoc.Range(wdDoc.Bookmarks("TIEMPOPAIN").Start, wdDoc.Bookmarks("TIEMPOPAIN").End).Text = .PedidoData.Recordset.Fields(3).Value
        wdDoc.Range(wdDoc.Bookmarks("TIEMPOESMA").Start, wdDoc.Bookmarks("TIEMPOESMA").End).Text = .PedidoData.Recordset.Fields(4).Value
        
        
    End With
    
    
    Unload DBase
    
    wdApp.Application.Visible = True
    
    Set wdApp = Nothing
    Set wdDoc = Nothing
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
    
End Function

Public Function FacPend(Fecha1 As String, Fecha2 As String)
    
    Dim wdApp As Word.Application
    Dim wdDoc As Word.Document
    Dim fil As Long, col As Long
    Dim Rango As Range
    Dim i As Integer, j As Integer
    Dim NombreP As String
    Dim Total As Long
On Error GoTo msgError
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT P.Nombre, A.Total FROM Proveedores P, Albaran A, AAlbaran AA WHERE P.CodigoProveedor=A.CodigoProveedor AND A.NAlbaran=AA.NAlbaran AND AA.Facturado=False AND A.FechaAlbaran BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# GROUP BY P.Nombre, A.Total ORDER BY P.Nombre"
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount = 0 Then
            MsgBox ("No queda ninguna factura pendiente")
            Exit Function
        End If
         
        .PedidoData.Recordset.MoveFirst
    
    End With
        
    Set wdApp = New Word.Application
    Set wdDoc = wdApp.Documents.add("c:\archivos de programa\almacen\doc\listados.dot")
    wdDoc.Activate
    
    With DBase.PedidoData.Recordset
        
        fil = .RecordCount
            
        Set Rango = wdDoc.Bookmarks("listado").Range
        wdDoc.Tables.add Rango, 1, 2
        wdDoc.Tables(1).Cell(1, 1).Select
        Selection.Font.Bold = True
        Selection.Text = "PROVEEDOR"
        wdDoc.Tables(1).Cell(1, 2).Select
        Selection.Text = "CANTIDAD POR FACTURAR"
        i = 2
        While .EOF = False
        
            NombreP = .Fields("Nombre").Value
            While NombreP = .Fields("Nombre").Value And .EOF = False
                Total = Total + .Fields("Total").Value
                .MoveNext
                j = j + 1
                If j = .RecordCount Then
                    GoTo Escribir
                End If
            Wend
            
Escribir:
            wdDoc.Tables(1).Rows.add
            wdDoc.Tables(1).Cell(i, 1).Select
            Selection.Font.Bold = False
            Selection.Text = NombreP
            wdDoc.Tables(1).Cell(i, 2).Select
            Selection.Text = CStr(Total)
            Total = 0
            i = i + 1
            
        Wend
    
    End With
    
    DBase.PedidoData.RecordSource = "SELECT SUM(Total) FROM Proveedores P, Albaran A, AAlbaran AA WHERE P.CodigoProveedor=A.CodigoProveedor AND A.NAlbaran=AA.NAlbaran AND AA.Facturado=False AND A.FechaAlbaran BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
    DBase.PedidoData.Refresh
    
    wdDoc.Bookmarks("Final").Select
    Selection.Text = "TOTAL:  " & DBase.PedidoData.Recordset.Fields(0).Value
       
    wdApp.Application.Visible = True
    
    Set wdApp = Nothing
    Set wdDoc = Nothing
    
    Unload DBase
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Public Function FacPendD(Fecha1 As String, Fecha2 As String)
    
    Dim i As Integer, j As Integer
    Dim NombreP As String
    Dim Total As Long
    Dim fil As Long
    Dim Ve As Double, Ho As Double, Can As Double, Cun As Double
On Error GoTo msgError
    Ve = Printer.ScaleHeight
    Ho = Printer.ScaleWidth
    
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT P.Nombre, A.Total FROM Proveedores P, Albaran A, AAlbaran AA WHERE P.CodigoProveedor=A.CodigoProveedor AND A.NAlbaran=AA.NAlbaran AND AA.Facturado=False AND A.FechaAlbaran BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY P.Nombre"
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount = 0 Then
            MsgBox ("No queda ninguna factura pendiente")
            Exit Function
        End If
         
        .PedidoData.Recordset.MoveFirst
    
    End With
    Printer.Font.Bold = True
    Printer.Font.Size = 12
    Can = Printer.TextWidth("CANTIDADES PENDIENTES POR FACTURAR")
    Cun = Printer.TextHeight("CANTIDADES PENDIENTES POR FACTURAR")
    Printer.CurrentX = (Ho - Can) / 2
    Printer.CurrentY = 0
    Printer.Print "CANTIDADES PENDIENTES POR FACTURAR"
    Printer.Print
    Can = Printer.TextWidth("Desde el " & Fecha1 & " hasta el " & Fecha2)
    Printer.CurrentX = (Ho - Can) / 2
    Printer.Print "Desde el " & Fecha1 & " hasta el " & Fecha2
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print "Proveedores________________________________Cantidad"
    Printer.Font.Bold = False
    Printer.Print
    Printer.Print
    Printer.Print
    With DBase.PedidoData.Recordset
        
        fil = .RecordCount
            
        While .EOF = False
        
            NombreP = .Fields("Nombre").Value
            While NombreP = .Fields("Nombre").Value And .EOF = False
                Total = Total + .Fields("Total").Value
                .MoveNext
                j = j + 1
                If j = .RecordCount Then
                    GoTo Escribir
                End If
            Wend
            
Escribir:
            Cun = Printer.CurrentY
            Printer.Print NombreP
            Printer.CurrentY = Cun
            Printer.CurrentX = Ho / 2
            Printer.Print CStr(Total)
            Printer.Print
            Printer.Print
            Printer.Print
            Total = 0
            i = i + 1
            
        Wend
    
    End With
    
    DBase.PedidoData.RecordSource = "SELECT SUM(Total) FROM Proveedores P, Albaran A, AAlbaran AA WHERE P.CodigoProveedor=A.CodigoProveedor AND A.NAlbaran=AA.NAlbaran AND AA.Facturado=False AND A.FechaAlbaran BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
    DBase.PedidoData.Refresh

    Printer.Print "Total:  " & DBase.PedidoData.Recordset.Fields(0).Value
    
    Unload DBase
    
    Printer.EndDoc
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Public Function SATPend(Fecha1 As String, Fecha2 As String)
    
    Dim wdApp As Word.Application
    Dim wdDoc As Word.Document
    Dim fil As Long, col As Long
    Dim Rango As Range
    Dim i As Integer, j As Integer
    Dim NombreP As String
    Dim Total As Long
On Error GoTo msgError
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT SAT, Total FROM SATAlbaran WHERE Facturado=False AND FechaAlbaran BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY SAT"
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount = 0 Then
            MsgBox ("No queda ninguna factura pendiente")
            Exit Function
        End If
         
        .PedidoData.Recordset.MoveFirst
    
    End With
        
    Set wdApp = New Word.Application
    Set wdDoc = wdApp.Documents.add("c:\archivos de programa\almacen\doc\listados.dot")
    wdDoc.Activate
    
    With DBase.PedidoData.Recordset
        
        fil = .RecordCount
            
        Set Rango = wdDoc.Bookmarks("listado").Range
        wdDoc.Tables.add Rango, 1, 2
        wdDoc.Tables(1).Cell(1, 1).Select
        Selection.Font.Bold = True
        Selection.Text = "PROVEEDOR SAT"
        wdDoc.Tables(1).Cell(1, 2).Select
        Selection.Text = "CANTIDAD POR FACTURAR"
        i = 2
        While .EOF = False
        
            NombreP = .Fields("SAT").Value
            While NombreP = .Fields("SAT").Value And .EOF = False
                Total = Total + .Fields("Total").Value
                .MoveNext
                j = j + 1
                If j = .RecordCount Then
                    GoTo Escribir
                End If
            Wend
            
Escribir:
            wdDoc.Tables(1).Rows.add
            wdDoc.Tables(1).Cell(i, 1).Select
            Selection.Font.Bold = False
            Selection.Text = NombreP
            wdDoc.Tables(1).Cell(i, 2).Select
            Selection.Text = CStr(Total)
            Total = 0
            i = i + 1
            
        Wend
    
    End With
    wdApp.Application.Visible = True
       
    Set wdApp = Nothing
    Set wdDoc = Nothing
    
       
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Public Function SATPendD(Fecha1 As String, Fecha2 As String)
    
    Dim i As Integer, j As Integer
    Dim NombreP As String
    Dim Total As Long
    Dim fil As Long
    Dim Ve As Double, Ho As Double, Can As Double, Cun As Double
On Error GoTo msgError
    Ve = Printer.ScaleHeight
    Ho = Printer.ScaleWidth
    
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT SAT, Total FROM SATAlbaran WHERE Facturado=False AND FechaAlbaran BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# ORDER BY SAT"
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount = 0 Then
            MsgBox ("No queda ninguna factura pendiente")
            Exit Function
        End If
         
        .PedidoData.Recordset.MoveFirst
    
    End With
    Printer.Font.Bold = True
    Printer.Font.Size = 12
    Can = Printer.TextWidth("CANTIDADES PENDIENTES POR FACTURAR")
    Cun = Printer.TextHeight("CANTIDADES PENDIENTES POR FACTURAR")
    Printer.CurrentX = (Ho - Can) / 2
    Printer.CurrentY = 0
    Printer.Print "CANTIDADES PENDIENTES POR FACTURAR"
    Printer.Print
    Can = Printer.TextWidth("Desde el " & Fecha1 & " hasta el " & Fecha2)
    Printer.CurrentX = (Ho - Can) / 2
    Printer.Print "Desde el " & Fecha1 & " hasta el " & Fecha2
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print "Proveedores SAT____________________________Cantidad"
    Printer.Font.Bold = False
    Printer.Print
    Printer.Print
    Printer.Print
    With DBase.PedidoData.Recordset
        
        fil = .RecordCount
            
        While .EOF = False
        
            NombreP = .Fields("SAT").Value
            While NombreP = .Fields("SAT").Value And .EOF = False
                Total = Total + .Fields("Total").Value
                .MoveNext
                j = j + 1
                If j = .RecordCount Then
                    GoTo Escribir
                End If
            Wend
            
Escribir:
            Cun = Printer.CurrentY
            Printer.Print NombreP
            Printer.CurrentY = Cun
            Printer.CurrentX = Ho / 2
            Printer.Print CStr(Total)
            Printer.Print
            Printer.Print
            Printer.Print
            Total = 0
            i = i + 1
            
        Wend
    
    End With
    
    Printer.EndDoc
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Public Function GastosInfD(Opcion As String, Codigo As Long, Fecha1 As String, Fecha2 As String)

    Dim i As Long, Var As String, Busq(3) As String, j As Integer, Total(4) As Long
    Dim Ho As Double, Ve As Double, Can As Double, Cun As Double
    Dim Ant As Long, Con As Integer
On Error GoTo msgError
    Printer.Font.Bold = True
    Printer.Font.Size = 10
    Ve = Printer.ScaleHeight
    Ho = Printer.ScaleWidth
    Can = Printer.TextWidth("INFORME SOBRE " & Opcion & " N�MERO " & Codigo)
    Printer.CurrentX = (Ho - Can) / 2
    Printer.Print "INFORME SOBRE " & Opcion & " N�MERO " & Codigo
    Can = Printer.TextWidth("Desde " & Fecha1 & " hasta " & Fecha2)
    Printer.CurrentX = (Ho - Can) / 2
    Printer.Print "Desde " & Fecha1 & " hasta " & Fecha2
    Printer.Print
    Printer.Print
    Printer.Font.Bold = False
    
    Load DBase
    With DBase
        Select Case Opcion
        Case "Seccion"
            Var = "CodigoEspecialidad"
        Case "Departamento"
            Var = "CodigoLugar"
        Case "Area de Trabajo"
            Var = "CodigoMaquina"
        End Select
        
        'C�lculo de los gastos por pedidos exteriores
        
        Busq(0) = "SELECT PP.NPedido, PP.CodigoArticulo, A.Descripcion, PP.Cantidad, A.PrecioUnit, AA.Descuento "
        Busq(1) = "FROM Pedido P, PPedido PP, Orden O, Articulos A, AAlbaran AA "
        Busq(2) = "WHERE O." & Var & "=" & Codigo & " AND P.NOrden=O.NOrden AND PP.NPedido=P.NPedido AND PP.CodigoArticulo=A.CodigoArticulo "
        Busq(3) = " AND FechaPedido BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "# AND AA.NPedido=P.NPedido"
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
        Con = .PedidoData.Recordset.RecordCount
        
        Printer.Font.Underline = True
        Printer.Print "PEDIDOS EXTERIORES"
        Printer.Font.Underline = False
        
        For i = 1 To Con
            
            Ant = Printer.CurrentY
            
            For j = 0 To 4
                Printer.CurrentX = (Ho / 6) * j
                Printer.CurrentY = Ant
                Printer.Print .PedidoData.Recordset.Fields(j).Value
            Next j
            Printer.CurrentX = (Ho / 6) * 5
            Printer.CurrentY = Ant
            Total(0) = (.PedidoData.Recordset.Fields("Cantidad").Value) * (.PedidoData.Recordset.Fields("PrecioUnit").Value) * (1 - (.PedidoData.Recordset.Fields("Descuento").Value / 100))
            Total(1) = Total(1) + Total(0)
            Printer.Print Total(0)
            .PedidoData.Recordset.MoveNext
        Next i
                
        If Var = "CodigoLugar" Then
            Var = "CodigoDepartamento"
        End If
                
        Busq(1) = "FROM Pedido P, PPedido PP, Articulos A "
        Busq(2) = "WHERE P." & Var & "=" & Codigo & " AND A.CodigoArticulo=PP.CodigoArticulo AND P.NPedido=PP.NPedido"
        Busq(3) = " AND FechaPedido BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
    
        If .PedidoData.Recordset.RecordCount <> 0 Then
            For i = (1 + Cun) To Cun + .PedidoData.Recordset.RecordCount
                Ant = Printer.CurrentY
                For j = 0 To 4
                    Printer.CurrentX = (Ho / 6) * j
                    Printer.CurrentY = Ant
                    Printer.Print .PedidoData.Recordset.Fields(j).Value
                Next j
                Printer.CurrentX = (Ho / 6) * 5
                Printer.CurrentY = Ant
                Total(0) = (.PedidoData.Recordset.Fields("Cantidad").Value) * (.PedidoData.Recordset.Fields("PrecioUnit").Value)
                Total(1) = Total(1) + Total(0)
                Printer.Print Total(0)
                .PedidoData.Recordset.MoveNext
            Next i
        End If
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print "Total Pedidos:" & Total(1)
        Printer.Print
        Printer.Print
        Printer.Print
    
        'C�lculo de los gastos por S.A.T.
        
        Total(0) = 0
        Total(1) = 0
        
        Select Case Opcion
            
        Case "Seccion"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, F.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, SATFact F, SATFFactura FF "
            Busq(2) = "WHERE P.CodigoOperario=SOME (SELECT CodigoOperario FROM Operarios WHERE " & Var & "=" & Codigo & ") "
            Busq(3) = "AND P.NPetSAT=A.NPetSAT AND A.NAlbaranSAT=FF.NAlbaranSAT AND F.NFacturaSAT=FF.NFacturaSAT AND FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Case "Departamento"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, F.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, SATFact F, SATFFactura FF, Orden O "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoLugar=" & Codigo
            Busq(3) = "AND P.NPetSAT=A.NPetSAT AND A.NAlbaranSAT=FF.NAlbaranSAT AND F.NFacturaSAT=FF.NFacturaSAT AND FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        Case "Area de Trabajo"
            Busq(0) = "SELECT P.FechaEnvio, P.SAT, F.Total "
            Busq(1) = "FROM PedidoSAT P, SATAlbaran A, SATFact F, SATFFactura FF "
            Busq(2) = "WHERE P.NOrden=O.NOrden AND O.CodigoMaquina=" & Codigo
            Busq(3) = "AND P.NPetSAT=A.NPetSAT AND A.NAlbaranSAT=FF.NAlbaranSAT AND F.NFacturaSAT=FF.NFacturaSAT AND FechaEnvio BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        
        End Select
        
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
        
        Printer.Font.Underline = True
        Printer.Print "SERVICIOS DE ASISTENCIA T�CNICA"
        Printer.Font.Underline = False
        Printer.Print
        
        For i = 1 To .PedidoData.Recordset.RecordCount
            
            Ant = Printer.CurrentY
            
            For j = 0 To 2
                Printer.CurrentX = (Ho / 4) * j
                Printer.CurrentY = Ant
                Printer.Print .PedidoData.Recordset.Fields(j).Value
            Next j
            Total(1) = Total(1) + .PedidoData.Recordset.Fields("Total").Value
            .PedidoData.Recordset.MoveNext
        Next i
        
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print "Total S.A.T. :" & Total(1)
        Printer.Print
        Printer.Print
        Printer.Print

        'C�lculo de tiempos
        
        Total(0) = 0
        Total(1) = 0
        
        If Var = "CodigoDepartamento" Then
            Var = "CodigoLugar"
        End If
        
        Busq(0) = "SELECT NOrden, TiempoTotal, TiempoEspera, TiempoReparacion, TiempoParo, EsperaMaterial "
        Busq(1) = "FROM Orden "
        Busq(2) = "WHERE " & Var & "=" & Codigo
        Busq(3) = " AND FechaOrden BETWEEN #" & Format(CDate(Fecha1), "m/d/yyyy") & "# AND #" & Format(CDate(Fecha2), "m/d/yyyy") & "#"
        
        .PedidoData.RecordSource = Busq(0) & Busq(1) & Busq(2) & Busq(3)
        .PedidoData.Refresh
        .PedidoData.Recordset.MoveFirst
        
        For i = 1 To .PedidoData.Recordset.RecordCount
            nada = TiempOrden(CLng(.PedidoData.Recordset.Fields("NOrden").Value))
            .PedidoData.Recordset.MoveNext
        Next i
        
        .PedidoData.Recordset.MoveFirst
        
        For i = 1 To .PedidoData.Recordset.RecordCount
            Total(0) = Total(0) + .PedidoData.Recordset.Fields("TiempoTotal").Value
            Total(1) = Total(1) + .PedidoData.Recordset.Fields("TiempoEspera").Value
            Total(2) = Total(2) + .PedidoData.Recordset.Fields("TiempoParo").Value
            Total(3) = Total(3) + .PedidoData.Recordset.Fields("EsperaMaterial").Value
            Total(4) = Total(4) + .PedidoData.Recordset.Fields("TiempoReparacion").Value
            .PedidoData.Recordset.MoveNext
        Next i
        
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Font.Underline = True
        Printer.Print "CONTROL DE TIEMPOS"
        Printer.Font.Underline = False
        Printer.Print
        Printer.Print
                                
        
        Printer.Print "Tiempo Total: " & Total(0)
        Printer.Print
        Printer.Print
        Printer.Print "     Tiempo Espera Orden:" & Total(1)
        Printer.Print
        Printer.Print "     Tiempo Parada Instalaci�n: " & Total(2)
        Printer.Print
        Printer.Print "     Tiempo Espera Material: " & Total(3)
        Printer.Print
        Printer.Print "     Tiempo Reparacion: " & Total(4)
    End With
    
    
    Unload DBase
    
    Printer.EndDoc
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

