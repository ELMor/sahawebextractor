VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "Ssdatb32.ocx"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form OrdenForm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "�rdenes de Trabajo"
   ClientHeight    =   6735
   ClientLeft      =   2385
   ClientTop       =   1050
   ClientWidth     =   11190
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   11190
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11190
      _ExtentX        =   19738
      _ExtentY        =   1535
      ButtonWidth     =   1535
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   12
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "Pri"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Almac�n"
            Key             =   "Alm"
            Description     =   "PedAlBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "SAT"
            Key             =   "SAT"
            Description     =   "SATBoton"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Pedido Ext"
            Key             =   "PExt"
            Description     =   "PedExtBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
         BeginProperty Button9 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Imprimir"
            Key             =   "Imp"
            Description     =   "Imp"
            Object.Tag             =   ""
            ImageIndex      =   9
         EndProperty
         BeginProperty Button10 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   10
         EndProperty
         BeginProperty Button11 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Object.Tag             =   ""
            ImageIndex      =   11
         EndProperty
         BeginProperty Button12 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "Ult"
            Object.Tag             =   ""
            ImageIndex      =   12
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.TextBox NOrdenBox 
      Alignment       =   1  'Right Justify
      BackColor       =   &H80000018&
      DataField       =   "NOrden"
      DataSource      =   "OrdenData"
      Enabled         =   0   'False
      Height          =   285
      Left            =   4440
      TabIndex        =   73
      Top             =   960
      Width           =   1575
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5175
      Left            =   240
      TabIndex        =   13
      Top             =   1440
      Width           =   10575
      _ExtentX        =   18653
      _ExtentY        =   9128
      _Version        =   327681
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Datos Principales"
      TabPicture(0)   =   "OrdenForm.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Check1"
      Tab(0).Control(1)=   "LugarBox"
      Tab(0).Control(2)=   "PlantaBox"
      Tab(0).Control(3)=   "PeticionarioBox"
      Tab(0).Control(4)=   "ExtTelBox"
      Tab(0).Control(5)=   "DefBox"
      Tab(0).Control(6)=   "FinalBoton"
      Tab(0).Control(7)=   "Command2"
      Tab(0).Control(8)=   "TipoOrdenBox"
      Tab(0).Control(9)=   "FechaOrdenBox"
      Tab(0).Control(10)=   "CoDeparBox"
      Tab(0).Control(11)=   "EspecBox"
      Tab(0).Control(12)=   "HoraAvisoBox"
      Tab(0).Control(13)=   "AreaTBox"
      Tab(0).Control(14)=   "Label2"
      Tab(0).Control(15)=   "Label3"
      Tab(0).Control(16)=   "Label4"
      Tab(0).Control(17)=   "Label5"
      Tab(0).Control(18)=   "Label6"
      Tab(0).Control(19)=   "Label7"
      Tab(0).Control(20)=   "Label8"
      Tab(0).Control(21)=   "Label9"
      Tab(0).Control(22)=   "Label10"
      Tab(0).Control(23)=   "Label16"
      Tab(0).Control(24)=   "Label30"
      Tab(0).Control(25)=   "Line1"
      Tab(0).Control(26)=   "EsppBox"
      Tab(0).Control(27)=   "Line2"
      Tab(0).Control(28)=   "Line3"
      Tab(0).Control(29)=   "Label11"
      Tab(0).Control(30)=   "Label13"
      Tab(0).Control(31)=   "Label22"
      Tab(0).Control(32)=   "Label32"
      Tab(0).Control(33)=   "Line4"
      Tab(0).Control(34)=   "ContManBox"
      Tab(0).Control(35)=   "Line5"
      Tab(0).Control(36)=   "Label33"
      Tab(0).ControlCount=   37
      TabCaption(1)   =   "Control Ordenes"
      TabPicture(1)   =   "OrdenForm.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.CheckBox Check1 
         Alignment       =   1  'Right Justify
         Caption         =   "Listada"
         DataField       =   "Listada"
         DataSource      =   "OrdenData"
         Height          =   375
         Left            =   -68400
         TabIndex        =   75
         Top             =   2760
         Width           =   1215
      End
      Begin VB.TextBox LugarBox 
         DataField       =   "Lugar"
         DataSource      =   "OrdenData"
         Height          =   285
         Left            =   -69000
         TabIndex        =   4
         Top             =   960
         Width           =   1815
      End
      Begin VB.TextBox PlantaBox 
         DataField       =   "Planta"
         DataSource      =   "OrdenData"
         Height          =   285
         Left            =   -65880
         TabIndex        =   5
         Top             =   960
         Width           =   1095
      End
      Begin VB.TextBox PeticionarioBox 
         DataField       =   "Peticionario"
         DataSource      =   "OrdenData"
         Height          =   285
         Left            =   -73200
         TabIndex        =   6
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox ExtTelBox 
         DataField       =   "ExtTelf"
         DataSource      =   "OrdenData"
         Height          =   285
         Left            =   -69720
         TabIndex        =   7
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox DefBox 
         DataField       =   "Definicion"
         DataSource      =   "OrdenData"
         Height          =   735
         Left            =   -73200
         MultiLine       =   -1  'True
         TabIndex        =   8
         Top             =   1680
         Width           =   8415
      End
      Begin VB.CommandButton FinalBoton 
         Caption         =   "&Finalizar"
         Height          =   375
         Left            =   -66600
         TabIndex        =   54
         Top             =   2760
         Width           =   1215
      End
      Begin VB.CommandButton Command2 
         Caption         =   "&Li&sta"
         Height          =   375
         Left            =   -66240
         TabIndex        =   53
         Top             =   4440
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.ComboBox TipoOrdenBox 
         DataField       =   "TipoOrden"
         DataSource      =   "OrdenData"
         Height          =   315
         ItemData        =   "OrdenForm.frx":0038
         Left            =   -73560
         List            =   "OrdenForm.frx":0048
         TabIndex        =   10
         Top             =   3000
         Width           =   1695
      End
      Begin VB.Frame Frame1 
         Caption         =   "Datos Control Orden"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Left            =   120
         TabIndex        =   12
         Top             =   720
         Width           =   10215
         Begin VB.TextBox Text15 
            DataField       =   "Diagn�stico"
            DataSource      =   "OrdenData"
            Height          =   285
            Left            =   1320
            TabIndex        =   44
            Top             =   3120
            Width           =   8655
         End
         Begin VB.TextBox Text16 
            DataField       =   "Causa"
            DataSource      =   "OrdenData"
            Height          =   285
            Left            =   1320
            TabIndex        =   43
            Top             =   3480
            Width           =   8655
         End
         Begin VB.TextBox Text17 
            DataField       =   "Accion"
            DataSource      =   "OrdenData"
            Height          =   285
            Left            =   1320
            TabIndex        =   42
            Top             =   3840
            Width           =   8655
         End
         Begin VB.CommandButton Command1 
            Caption         =   "&Control"
            Height          =   375
            Left            =   4440
            TabIndex        =   19
            Top             =   1440
            Width           =   1215
         End
         Begin VB.TextBox Text13 
            DataField       =   "TiempoParo"
            DataSource      =   "OrdenData"
            Height          =   285
            Left            =   2160
            TabIndex        =   18
            Top             =   1440
            Width           =   1455
         End
         Begin VB.TextBox Text14 
            DataField       =   "TiempoReparacion"
            DataSource      =   "OrdenData"
            Height          =   285
            Left            =   2160
            TabIndex        =   17
            Top             =   1920
            Width           =   1455
         End
         Begin VB.TextBox TTotalBoxMP 
            DataField       =   "TiempoTotalMP"
            DataSource      =   "OrdenData"
            Height          =   285
            Left            =   2160
            TabIndex        =   15
            Top             =   1440
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.TextBox TRepBoxMP 
            DataField       =   "TiempoReparacionMP"
            DataSource      =   "OrdenData"
            Height          =   285
            Left            =   2160
            TabIndex        =   14
            Top             =   1920
            Visible         =   0   'False
            Width           =   1455
         End
         Begin SSCalendarWidgets_A.SSDateCombo FechaFinalBox 
            DataField       =   "FechaFinalizacion"
            DataSource      =   "OrdenData"
            Height          =   255
            Left            =   1920
            TabIndex        =   16
            Top             =   480
            Width           =   1575
            _Version        =   65537
            _ExtentX        =   2778
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin TabDlg.SSTab SSTab2 
            Height          =   2655
            Left            =   5760
            TabIndex        =   20
            Top             =   360
            Width           =   3735
            _ExtentX        =   6588
            _ExtentY        =   4683
            _Version        =   327681
            Tabs            =   2
            Tab             =   1
            TabHeight       =   520
            TabCaption(0)   =   "Tiempos"
            TabPicture(0)   =   "OrdenForm.frx":0085
            Tab(0).ControlEnabled=   0   'False
            Tab(0).Control(0)=   "Frame3"
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Costes"
            TabPicture(1)   =   "OrdenForm.frx":00A1
            Tab(1).ControlEnabled=   -1  'True
            Tab(1).Control(0)=   "Frame2"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).ControlCount=   1
            Begin VB.Frame Frame3 
               Caption         =   "Control Tiempos"
               Height          =   2055
               Left            =   -74880
               TabIndex        =   30
               Top             =   480
               Width           =   3495
               Begin VB.Label TiempoRepMP 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "TiempoReparacionMP"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   77
                  Top             =   1320
                  Width           =   1455
               End
               Begin VB.Label TiempoTotalMP 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "TiempoTotalMP"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   76
                  Top             =   960
                  Width           =   1455
               End
               Begin VB.Label TEMaterBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "EsperaMaterial"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   41
                  Top             =   1680
                  Width           =   1455
               End
               Begin VB.Label EsSaBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "TiempoEspera"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   40
                  Top             =   600
                  Width           =   1455
               End
               Begin VB.Label TTotalBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "TiempoTotal"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1800
                  TabIndex        =   39
                  Top             =   240
                  Width           =   1575
               End
               Begin VB.Label Label21 
                  Caption         =   "Espera material:"
                  Height          =   255
                  Left            =   360
                  TabIndex        =   38
                  Top             =   1680
                  Width           =   1455
               End
               Begin VB.Label Label20 
                  Caption         =   "Reparaci�n:"
                  Height          =   255
                  Left            =   360
                  TabIndex        =   37
                  Top             =   1320
                  Width           =   1455
               End
               Begin VB.Label Label19 
                  Caption         =   "Tiempo de Paro:"
                  Height          =   255
                  Left            =   360
                  TabIndex        =   36
                  Top             =   960
                  Width           =   1455
               End
               Begin VB.Label Label18 
                  Caption         =   "Espera Salida Vale:"
                  Height          =   255
                  Left            =   360
                  TabIndex        =   35
                  Top             =   600
                  Width           =   1455
               End
               Begin VB.Label Label17 
                  Caption         =   "Tiempo total:"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   34
                  Top             =   240
                  Width           =   1575
               End
               Begin VB.Label TiPaBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "TiempoParo"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   33
                  Top             =   960
                  Width           =   1455
               End
               Begin VB.Label TiReBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "TiempoReparacion"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   32
                  Top             =   1320
                  Width           =   1455
               End
               Begin VB.Label EstimBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "TiempoEstimadoMP"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1920
                  TabIndex        =   31
                  Top             =   1680
                  Visible         =   0   'False
                  Width           =   1455
               End
            End
            Begin VB.Frame Frame2 
               Caption         =   "Control Costes"
               Height          =   2055
               Left            =   360
               TabIndex        =   21
               Top             =   480
               Width           =   3135
               Begin VB.Label CSATBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "CosteSAT"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1680
                  TabIndex        =   29
                  Top             =   1560
                  Width           =   1335
               End
               Begin VB.Label CMaterialBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "CosteMaterial"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1680
                  TabIndex        =   28
                  Top             =   1200
                  Width           =   1335
               End
               Begin VB.Label CMObraBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "CosteManoDO"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1680
                  TabIndex        =   27
                  Top             =   840
                  Width           =   1335
               End
               Begin VB.Label CTotalBox 
                  BorderStyle     =   1  'Fixed Single
                  DataField       =   "CosteTotal"
                  DataSource      =   "OrdenData"
                  Height          =   255
                  Left            =   1680
                  TabIndex        =   26
                  Top             =   360
                  Width           =   1335
               End
               Begin VB.Label Label26 
                  Caption         =   "SAT:"
                  Height          =   255
                  Left            =   480
                  TabIndex        =   25
                  Top             =   1560
                  Width           =   855
               End
               Begin VB.Label Label25 
                  Caption         =   "Material:"
                  Height          =   255
                  Left            =   480
                  TabIndex        =   24
                  Top             =   1200
                  Width           =   975
               End
               Begin VB.Label Label24 
                  Caption         =   "Mano de obra:"
                  Height          =   255
                  Left            =   480
                  TabIndex        =   23
                  Top             =   840
                  Width           =   1095
               End
               Begin VB.Label Label23 
                  Caption         =   "Coste Total:"
                  Height          =   255
                  Left            =   240
                  TabIndex        =   22
                  Top             =   360
                  Width           =   975
               End
            End
         End
         Begin MSMask.MaskEdBox HoraComplBox 
            DataField       =   "HoraCompletado"
            DataSource      =   "OrdenData"
            Height          =   255
            Left            =   2040
            TabIndex        =   45
            Top             =   960
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   450
            _Version        =   327681
            PromptChar      =   "_"
         End
         Begin VB.Label Label14 
            Caption         =   "Fecha Finalizaci�n:"
            Height          =   255
            Left            =   480
            TabIndex        =   52
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label Label15 
            Caption         =   "Hora finalizaci�n:"
            Height          =   255
            Left            =   600
            TabIndex        =   51
            Top             =   960
            Width           =   1335
         End
         Begin VB.Label Label27 
            Caption         =   "Diagn�stico:"
            Height          =   255
            Left            =   120
            TabIndex        =   50
            Top             =   3120
            Width           =   1095
         End
         Begin VB.Label Label28 
            Caption         =   "Causa:"
            Height          =   255
            Left            =   120
            TabIndex        =   49
            Top             =   3480
            Width           =   1095
         End
         Begin VB.Label Label29 
            Caption         =   "Acci�n:"
            Height          =   255
            Left            =   120
            TabIndex        =   48
            Top             =   3840
            Width           =   1095
         End
         Begin VB.Label Label12 
            Caption         =   "Tiempo Reparaci�n:"
            Height          =   255
            Left            =   600
            TabIndex        =   47
            Top             =   1920
            Width           =   1455
         End
         Begin VB.Label Label31 
            Caption         =   "Tiempo de Paro:"
            Height          =   255
            Left            =   600
            TabIndex        =   46
            Top             =   1440
            Width           =   1455
         End
      End
      Begin SSCalendarWidgets_A.SSDateCombo FechaOrdenBox 
         DataField       =   "FechaOrden"
         DataSource      =   "OrdenData"
         Height          =   255
         Left            =   -69480
         TabIndex        =   1
         Top             =   600
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBCombo CoDeparBox 
         Bindings        =   "OrdenForm.frx":00BD
         DataField       =   "CodigoLugar"
         DataSource      =   "OrdenData"
         Height          =   255
         Left            =   -73200
         TabIndex        =   3
         Top             =   960
         Width           =   855
         DataFieldList   =   "CodigoLugar"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�d.Departamento"
         Columns(0).Name =   "CodigoLugar"
         Columns(0).Alignment=   1
         Columns(0).CaptionAlignment=   1
         Columns(0).DataField=   "CodigoLugar"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Departamento"
         Columns(1).Name =   "Localizacion"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Localizacion"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1508
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo EspecBox 
         Bindings        =   "OrdenForm.frx":00D1
         DataField       =   "CodigoEspecialidad"
         DataSource      =   "OrdenData"
         Height          =   255
         Left            =   -73560
         TabIndex        =   9
         Top             =   2640
         Width           =   975
         DataFieldList   =   "CodigoEspecialidad"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "CodigoEspecialidad"
         Columns(0).Name =   "CodigoEspecialidad"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoEspecialidad"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Especialidad"
         Columns(1).Name =   "Especialidad"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Especialidad"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1720
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSMask.MaskEdBox HoraAvisoBox 
         DataField       =   "HoraOrden"
         DataSource      =   "OrdenData"
         Height          =   255
         Left            =   -66120
         TabIndex        =   2
         Top             =   600
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   327681
         PromptChar      =   "_"
      End
      Begin SSDataWidgets_B.SSDBCombo AreaTBox 
         Bindings        =   "OrdenForm.frx":00E5
         DataField       =   "CodigoMaquina"
         DataSource      =   "OrdenData"
         Height          =   255
         Left            =   -73560
         TabIndex        =   11
         Top             =   3480
         Width           =   1215
         DataFieldList   =   "CodigoMaquina"
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   1376
         Columns(0).Caption=   "�rea T."
         Columns(0).Name =   "CodigoMaquina"
         Columns(0).CaptionAlignment=   0
         Columns(0).DataField=   "CodigoMaquina"
         Columns(0).DataType=   3
         Columns(0).FieldLen=   256
         Columns(1).Width=   2434
         Columns(1).Caption=   "Modelo"
         Columns(1).Name =   "Modelo"
         Columns(1).CaptionAlignment=   0
         Columns(1).DataField=   "Modelo"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   3413
         Columns(2).Caption=   "Instalaci�n"
         Columns(2).Name =   "Instalacion"
         Columns(2).CaptionAlignment=   0
         Columns(2).DataField=   "Instalacion"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   5556
         Columns(3).Caption=   "Descripci�n"
         Columns(3).Name =   "Descripcion"
         Columns(3).CaptionAlignment=   0
         Columns(3).DataField=   "Descripcion"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         _ExtentX        =   2143
         _ExtentY        =   450
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "Codigo Departamento:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   72
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Lugar:"
         Height          =   255
         Left            =   -69720
         TabIndex        =   71
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label4 
         Caption         =   "Planta:"
         Height          =   255
         Left            =   -66600
         TabIndex        =   70
         Top             =   960
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Peticionario:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   69
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Fecha de Orden:"
         Height          =   255
         Left            =   -70920
         TabIndex        =   68
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "Hora de aviso:"
         Height          =   255
         Left            =   -67440
         TabIndex        =   67
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label8 
         Caption         =   "Extensi�n Tel�fono:"
         Height          =   255
         Left            =   -71280
         TabIndex        =   66
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label Label9 
         Caption         =   "Definici�n:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   65
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label Label10 
         Caption         =   "Cod.Especialidad:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   64
         Top             =   2640
         Width           =   1335
      End
      Begin VB.Label Label16 
         Caption         =   "Tipo de orden:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   63
         Top             =   3000
         Width           =   1095
      End
      Begin VB.Label Label30 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Localizacion"
         DataSource      =   "LugarData"
         Height          =   255
         Left            =   -72120
         TabIndex        =   62
         Top             =   960
         Width           =   1935
      End
      Begin VB.Line Line1 
         X1              =   -72360
         X2              =   -72120
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label EsppBox 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Especialidad"
         DataSource      =   "EspecData"
         Height          =   255
         Left            =   -72240
         TabIndex        =   61
         Top             =   2640
         Width           =   2415
      End
      Begin VB.Line Line2 
         X1              =   -72600
         X2              =   -72240
         Y1              =   2760
         Y2              =   2760
      End
      Begin VB.Line Line3 
         X1              =   -72360
         X2              =   -72000
         Y1              =   3600
         Y2              =   3600
      End
      Begin VB.Label Label11 
         Caption         =   "Area de trabajo:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   60
         Top             =   3480
         Width           =   1215
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Descripcion"
         DataSource      =   "MaquinaData"
         Height          =   255
         Left            =   -72000
         TabIndex        =   59
         Top             =   3480
         Width           =   5655
      End
      Begin VB.Label Label22 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         DataField       =   "Criticidad"
         DataSource      =   "MaquinaData"
         Height          =   255
         Left            =   -65280
         TabIndex        =   58
         Top             =   3480
         Width           =   495
      End
      Begin VB.Label Label32 
         Caption         =   "Criticidad:"
         Height          =   255
         Left            =   -66000
         TabIndex        =   57
         Top             =   3480
         Width           =   735
      End
      Begin VB.Line Line4 
         X1              =   -66360
         X2              =   -65280
         Y1              =   3720
         Y2              =   3720
      End
      Begin VB.Label ContManBox 
         BorderStyle     =   1  'Fixed Single
         DataField       =   "ContratoMantenimiento"
         DataSource      =   "MaquinaData"
         Height          =   255
         Left            =   -71280
         TabIndex        =   56
         Top             =   3960
         Width           =   1935
      End
      Begin VB.Line Line5 
         X1              =   -70320
         X2              =   -70320
         Y1              =   3720
         Y2              =   3960
      End
      Begin VB.Label Label33 
         Caption         =   "Contrato Mantenimiento:"
         Height          =   255
         Left            =   -73200
         TabIndex        =   55
         Top             =   3960
         Width           =   1815
      End
   End
   Begin VB.Data LugarData 
      Caption         =   "Lugares"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   4320
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT * FROM Lugar ORDER BY Localizacion"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data Datos 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   7800
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data EspecData 
      Caption         =   "Especialidades"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   5640
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   "SELECT * FROM Tarifas ORDER BY CodigoEspecialidad"
      Top             =   0
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Data MaquinaData 
      Caption         =   "Maquinas"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   1800
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   $"OrdenForm.frx":00FB
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data OrdenData 
      Caption         =   "Ordenes"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "SELECT * FROM Orden  WHERE NOrden<>0 ORDER BY NOrden"
      Top             =   0
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Data Data1 
      Caption         =   "Data1"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   840
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   2  'Snapshot
      RecordSource    =   ""
      Top             =   480
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "N� Orden:"
      Height          =   255
      Left            =   3360
      TabIndex        =   74
      Top             =   960
      Width           =   975
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   12
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":0192
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":04AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":07C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":0AE0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":0DFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":1114
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":142E
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":1748
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":1A62
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":1D7C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":2096
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "OrdenForm.frx":23B0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu Prg 
      Caption         =   "&Programa"
      Begin VB.Menu Exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu Reg 
      Caption         =   "&Registro"
      Begin VB.Menu goto 
         Caption         =   "Ir a "
         Shortcut        =   {F1}
      End
      Begin VB.Menu FinO 
         Caption         =   "Finalizar Orden"
         Shortcut        =   {F12}
      End
      Begin VB.Menu Prim 
         Caption         =   "Primero"
      End
      Begin VB.Menu Ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu Sig 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu Ult 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu A�a 
         Caption         =   "A�adir"
         Shortcut        =   ^I
      End
      Begin VB.Menu Erase 
         Caption         =   "Borrar"
         Shortcut        =   ^B
      End
   End
   Begin VB.Menu Alma 
      Caption         =   "&Almac�n"
      Begin VB.Menu Sal 
         Caption         =   "Salidas Almac�n"
      End
   End
   Begin VB.Menu SAT 
      Caption         =   "&S.A.T."
      Begin VB.Menu PetSAT 
         Caption         =   "Petici�n Reparaci�n Ext."
      End
      Begin VB.Menu PT 
         Caption         =   "Parte de Trabajo"
      End
      Begin VB.Menu FactSAT 
         Caption         =   "Factura S.A.T."
      End
      Begin VB.Menu PTPend 
         Caption         =   "Partes de Trabajo Pendientes"
      End
   End
   Begin VB.Menu Pedid 
      Caption         =   "&Pedidos"
      Begin VB.Menu Pedi 
         Caption         =   "Pedidos"
      End
      Begin VB.Menu Alba 
         Caption         =   "Albaranes"
      End
      Begin VB.Menu Fact 
         Caption         =   "Facturas"
      End
   End
   Begin VB.Menu Li 
      Caption         =   "&Listados"
      Begin VB.Menu ActCost 
         Caption         =   "Actualizar costes"
         Index           =   30
      End
      Begin VB.Menu List 
         Caption         =   "Listados"
      End
   End
   Begin VB.Menu Imp 
      Caption         =   "&Imprimir"
      Begin VB.Menu OA 
         Caption         =   "Orden Actual"
      End
      Begin VB.Menu OP 
         Caption         =   "�rdenes Pendientes"
      End
   End
End
Attribute VB_Name = "OrdenForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub ActCost_Click(Index As Integer)
    
    Dim i As Integer
    
    With OrdenData.Recordset
    
        .MoveFirst
        For i = 0 To .RecordCount
            Call Control(.Fields("NOrden").Value)
        Next i
    
    End With
    
End Sub

Private Sub Alba_Click()
    AlbaranForm.Show modal
    
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
    
End Sub

Private Sub A�a_Click()
    Dim nada As Integer
    nada = OtraOrden()
    
End Sub

Private Sub AreaTBox_Change()
On Error GoTo msgError
    If AreaTBox.Text <> "" Then
        MaquinaData.Recordset.FindFirst ("CodigoMaquina = " & AreaTBox.Text)
        If MaquinaData.Recordset.NoMatch = True Then
            Label13.Caption = ""
        End If
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub AreaTBox_CloseUp()
On Error GoTo msgError
        MaquinaData.Recordset.FindFirst ("CodigoMaquina = " & AreaTBox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CoDeparBox_Change()
On Error GoTo msgError
    With LugarData
        If CoDeparBox.Text <> "" Then
            .Recordset.FindFirst ("CodigoLugar=" & CoDeparBox.Text)
            If .Recordset.NoMatch = True Then
                EsppBox.Caption = "No Existente"
            End If
        End If
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub CoDeparBox_CloseUp()
On Error GoTo msgError
    With LugarData
        .Recordset.FindFirst ("CodigoLugar=" & CoDeparBox.Text)
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub Command1_Click()
On Error GoTo msgError
    Dim nada As Integer
    nada = Control(CLng(NOrdenBox.Text))
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub Command2_Click()
On Error GoTo msgError
    With ListaMPForm
        .Show modal
        .ListaData.RecordSource = "SELECT M.CodigoMaquina, M.Descripcion FROM ListaMP L, InventMaquinas M WHERE L.CodigoMaquina=M.CodigoMaquina AND L.NOrden=" & NOrdenBox.Text
        .ListaData.Refresh
        .HechoData.RecordSource = "SELECT Hecho FROM ListaMP WHERE NOrden=" & NOrdenBox.Text
        .HechoData.Refresh
        If .ListaData.Recordset.RecordCount = 0 Then
            .ListaData.RecordSource = "SELECT M.CodigoMaquina, M.Descripcion FROM ListaMP2 L, InventMaquinas M WHERE L.CodigoMaquina=M.CodigoMaquina AND L.NOrden=" & NOrdenBox.Text
            .ListaData.Refresh
            .HechoData.RecordSource = "SELECT HechoS1,HechoS2,HechoS3,HechoS4,HechoM FROM ListaMP2 WHERE NOrden=" & NOrdenBox.Text
            .HechoData.Refresh
            If .ListaData.Recordset.RecordCount = 0 Then
                MsgBox ("No hay ninguna lista asociada a la Orden de Mant.Preventivo")
                Unload ListaMPForm
                Exit Sub
            Else
                .SSDBGrid2.Width = (.SSDBGrid2.Columns(0).Width) * 5
                .SSDBGrid2.Columns(0).Style = ssStyleCheckBox
                .SSDBGrid2.Columns(1).Style = ssStyleCheckBox
                .SSDBGrid2.Columns(2).Style = ssStyleCheckBox
                .SSDBGrid2.Columns(3).Style = ssStyleCheckBox
                .SSDBGrid2.Columns(4).Style = ssStyleCheckBox
                .Width = 10000
            End If
        Else
            .SSDBGrid2.Width = 855
            .SSDBGrid2.Columns(0).Style = ssStyleCheckBox
        End If
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub Erase_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar la orden?", vbYesNo)
    If nada = vbYes Then
        OrdenData.Recordset.Delete
        nada = Anterior()
    End If

End Sub

Private Sub EspecBox_Change()
On Error GoTo msgError
    If EspecBox.Text <> "" Then
        EspecData.Recordset.FindFirst ("CodigoEspecialidad=" & EspecBox.Text)
        If EspecData.Recordset.NoMatch = True Then
            EsppBox.Caption = "No Existe"
        End If
    Else
        EspecBox.Text = "0"
        
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub EspecBox_CloseUp()
On Error GoTo msgError
    EspecData.Recordset.FindFirst ("CodigoEspecialidad=" & EspecBox.Text)
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub exit_Click()
    Unload Me
    
End Sub

Private Sub Fact_Click()
    FacturaForm.Show modal

End Sub

Private Sub FactSAT_Click()
    SATFForm.Show modal
End Sub

Private Sub FinalBoton_Click()
    Dim nada As Integer
    nada = Final()
End Sub

Private Sub FinO_Click()
    Dim nada As Integer
    nada = Final()

End Sub



Private Sub Form_Load()
    
    Flag = False
    Flag2 = False
    
End Sub


Private Sub goto_Click()
    Set Objeto = OrdenForm.OrdenData
    Display(0) = "NOrden"
    Selct = False
    IraForm.Show modal
End Sub

Private Sub List_Click()
    Dim nada As Integer
    nada = Listar()
    
End Sub

Private Sub NOrdenBox_Change()
    
    Dim nada As Integer
    
    nada = Check()
    
End Sub


Private Sub OA_Click()
On Error GoTo msgError
    Dim nada As Integer
    nada = ImpOrden(Val(NOrdenBox.Text))
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub



Private Sub Pedi_Click()
    Dim nada As Integer
    nada = PExt
End Sub

Private Sub PetSAT_Click()
On Error GoTo msgError
    PedSATForm.Show modal
    PedSATForm.AreaT.Text = AreaTBox.Text
    PedSATForm.AreaTBox.Caption = Label13
    PedSATForm.Aparato.Text = MaquinaData.Recordset.Fields("Descripcion").Value
    PedSATForm.Marca.Text = MaquinaData.Recordset.Fields("Modelo").Value
    PedSATForm.NSerie.Text = MaquinaData.Recordset.Fields("NSerie").Value

    Orden(0) = CLng(NOrdenBox.Text)
    Orden(1) = CLng(AreaTBox.Text)
    Orden(2) = 0
    Orden(3) = CLng(CoDeparBox.Text)
    
    With PedSATForm
            .PedSATData.RecordSource = "SELECT * FROM PedidoSAT WHERE NOrden=" & NOrdenBox.Text
            .PedSATData.Refresh
    End With
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub prim_Click()
    OrdenData.Recordset.MoveFirst
End Sub


Private Sub PT_Click()
On Error GoTo msgError
    Dim res As Integer
    
    SATAlbForm.Show modal
    
    Load DBase
    With DBase
        .PedidoData.RecordSource = "SELECT NPetSAT FROM PedidoSAT WHERE NOrden=" & NOrdenBox.Text
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount <> 0 Then
            res = .PedidoData.Recordset.Fields("NPetSAT").Value
        End If
    End With
    Unload DBase
    
    If res <> 0 Then
        With SATAlbForm
            .SATAlbData.RecordSource = "SELECT * FROM SATAlbaran WHERE NPetSAT=" & res
            .SATAlbData.Refresh
            
            If .SATAlbData.Recordset.RecordCount <> 0 Then
                res = .SATAlbData.Recordset.Fields("NAlbaranSAT").Value
                .SATAAlb2Data.RecordSource = "SELECT * FROM SATAAlbaran WHERE NAlbaranSAT=" & res & " ORDER BY Linea"
                .SATAAlb2Data.Refresh
            End If
        End With
    End If
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical
End Sub

Private Sub PTPend_Click()
    Dim nada
    
    nada = NuevaBusq()
    SELE = "NPetSAT, FechaEnvio, SAT, CodigoDpto"
    FRO = "PedidoSAT"
    WHER = "NPetSAT<>ALL (SELECT NPetSAT FROM SATAlbaran WHERE NPetSAT<>0) AND NPetSAT<>0"
    MosBusForm.Show modal
    MosBusForm.SSDBGrid1.Enabled = False
    Display(0) = "NPetSAT"
    Display(1) = "FechaEnvio"
    Display(2) = "SAT"
    Display(3) = "CodigoDpto"
    For nada = 0 To 3
        campos(nada) = Display(nada)
    Next
    
End Sub

Private Sub Sal_Click()

    Dim nada As Integer

    nada = Almacen()

End Sub

Private Sub sig_Click()
    Dim nada As Integer
    nada = Siguiente()
End Sub





Private Sub TipoOrdenBox_Change()
    If TipoOrdenBox.Text = "Mant.Preventivo" Then
        Command2.Visible = True
        Label31.Caption = "Tiempo Total:"
        Label19.Caption = "Tiempo Total:"
        Label12.Caption = "Reparaciones :"
        Label20.Caption = "Reparaciones :"
        Label21.Caption = "Tiempo Estimado: "
        Text13.Visible = False
        TTotalBoxMP.Visible = True
        Text14.Visible = False
        TRepBoxMP.Visible = True
        TEMaterBox.Visible = False
        EstimBox.Visible = True
        TTotalBox.Visible = False
        Label17.Visible = False
        TiempoTotalMP.Visible = True
        TiReBox.Visible = False
        TiempoRepMP.Visible = True
    Else
        If Text13.Visible = False Then
            Command2.Visible = False
            Label31.Caption = "Tiempo de Paro:"
            Label19.Caption = "Tiempo de Paro:"
            Label12.Caption = "Tiempo Reparaci�n:"
            Label20.Caption = "Reparaci�n:"
            Label21.Caption = "Espera material: "
            Text13.Visible = True
            TTotalBoxMP.Visible = False
            Text14.Visible = True
            TRepBoxMP.Visible = False
            TEMaterBox.Visible = True
            EstimBox.Visible = False
            TTotalBox.Visible = True
            Label17.Visible = True
            TiempoTotalMP.Visible = False
            TiReBox.Visible = True
            TiempoRepMP.Visible = False
        End If

    End If
End Sub

Private Sub TipoOrdenBox_LostFocus()
    If TipoOrdenBox.Text = "Mant.Preventivo" Then
        Command2.Visible = True
        Label31.Caption = "Tiempo Total:"
        Label19.Caption = "Tiempo Total:"
        Label12.Caption = "Reparaciones :"
        Label20.Caption = "Reparaciones :"
        Label21.Caption = "Tiempo Estimado: "
        Text13.Visible = False
        TTotalBoxMP.Visible = True
        Text14.Visible = False
        TRepBoxMP.Visible = True
        TEMaterBox.Visible = False
        EstimBox.Visible = True
    Else
        If Text13.Visible = False Then
            Command2.Visible = False
            Label31.Caption = "Tiempo de Paro:"
            Label19.Caption = "Tiempo de Paro:"
            Label12.Caption = "Tiempo Reparaci�n:"
            Label20.Caption = "Reparaci�n:"
            Label21.Caption = "Espera material: "
            Text13.Visible = True
            TTotalBoxMP.Visible = False
            Text14.Visible = True
            TRepBoxMP.Visible = False
            TEMaterBox.Visible = True
            EstimBox.Visible = False
        End If

    End If
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
On Error GoTo msgError
    Select Case Button.Key
    
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Ult"
        OrdenData.Recordset.MoveLast
    Case "Pri"
        OrdenData.Recordset.MoveFirst
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtraOrden()
   
    Case "Lista"
    
        nada = Listar()
                   
    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar la orden?", vbYesNo)
        If nada = vbYes Then
            OrdenData.Recordset.Delete
            nada = Anterior()
        End If
        
    Case "PExt"
        nada = PExt()
    Case "SAT"
        PedSATForm.Show modal
        PedSATForm.AreaT.Text = AreaTBox.Text
        PedSATForm.AreaTBox.Caption = Label13
        PedSATForm.Aparato.Text = MaquinaData.Recordset.Fields("Descripcion").Value
        If IsNull(MaquinaData.Recordset.Fields("Modelo").Value) = False Then
            PedSATForm.Marca.Text = MaquinaData.Recordset.Fields("Modelo").Value
        End If
        If IsNull(MaquinaData.Recordset.Fields("NSerie").Value) = False Then
            PedSATForm.NSerie.Text = MaquinaData.Recordset.Fields("NSerie").Value
        End If
        Orden(0) = CLng(NOrdenBox.Text)
        Orden(1) = CLng(AreaTBox.Text)
        Orden(2) = 0
        Orden(3) = CLng(CoDeparBox.Text)
    
        With PedSATForm
            .PedSATData.RecordSource = "SELECT * FROM PedidoSAT WHERE NOrden=" & NOrdenBox.Text
            .PedSATData.Refresh
        End With
    
    Case "Imp"
        nada = ImpOrden()
        
    Case "Alm"
    
        nada = Almacen()
    
    End Select
    Exit Sub
msgError:
    MsgBox Err.Description, vbCritical

End Sub

Private Function Anterior()

    If Flag = True Then
        Flag = False
    End If
    
    With OrdenData
    
        If .Recordset.RecordCount = 0 Then
            Exit Function
        End If
                
        .Recordset.MovePrevious
        
        If .Recordset.BOF = True Then
            .Recordset.MoveNext
        End If
    End With
    
End Function


Private Function Siguiente()
        
    If Flag = False Then
    
        With OrdenData.Recordset
                                  
            If .RecordCount = 0 Then
                Exit Function
            End If
            
            
            .MoveNext
        
            If .EOF = True Then
                .MovePrevious
            End If
        
        End With
    
    End If
    
End Function

Private Function OtraOrden()
    
    Dim last As Long
On Error GoTo msgError
    With OrdenData.Recordset
        .MoveLast
        last = .Fields("NOrden") + 1
        .AddNew
        NOrdenBox.Text = last
        CoDeparBox.Text = "0"
        AreaTBox.Text = "0"
        LugarBox.Text = "."
        PlantaBox.Text = "."
        .Fields("FechaOrden").Value = Date
        HoraAvisoBox.SelText = Format(Time, "hh.mm.ss")
        .Fields("Finalizada") = False
        .Update
        .MoveLast
        Flag = True
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Check()

    If OrdenData.Recordset.EOF = False And OrdenData.Recordset.BOF = False Then
    
        Select Case OrdenData.Recordset.Fields("Finalizada").Value
            Case True
                Frame1.Enabled = True
            Case False
                Frame1.Enabled = False
            Case Null
                Frame1.Enabled = False
        End Select
    
    End If
    
End Function


Public Function ImpOrden()
    
    Dim He As Double, Wi As Double, ScaH As Double
    Dim ScaW As Double, Opcion As Boolean
    Dim HoOr As Date, Seg As Double, Ant As Double
    Dim i As Integer, j As Integer, Aescribir As String, k As Integer
    Dim Cond As Boolean, Tit As String, Tit2 As String
On Error GoTo msgError
    If IMPRESORA = 1 Then
        
    Select Case TipoOrdenBox.Text
    Case "Reparaci�n"
        Opcion = True
        Tit = "AVISO DE REPARACI�N"
        Tit2 = "    Descripci�n de la aver�a: "
    Case "Instalaci�n"
        Opcion = True
        Tit = "SOLICITUD DE REFORMA-INSTALACI�N"
        Tit2 = "    Reforma solicitada: "
    Case "Rep.Anomal�as"
        Opcion = True
        Tit = "REPARACI�N DE ANOMAL�A"
        Tit2 = "    Descripci�n de la aver�a: "
    Case "Mant.Preventivo"
        Opcion = False
    Case ""
        MsgBox ("Necesita completar el tipo de orden")
        Exit Function
    End Select
    
    Select Case Opcion
    Case True
        Printer.PaperBin = 1
        Printer.Orientation = 2
        Printer.FontName = "Times New Roman"

        ScaH = Printer.ScaleHeight / 2
        ScaW = Printer.ScaleWidth / 2
        Seg = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXX")
    
        Printer.Font.Size = 18
        Printer.Font.Bold = True
        He = Printer.TextWidth("C.U.N. MANTENIMIENTO")
        Printer.CurrentX = ScaW + ((ScaW - He) / 2)
        Printer.CurrentY = 0
        Printer.Print "C.U.N. MANTENIMIENTO"

        He = Printer.TextWidth(Tit)
        Printer.CurrentX = ScaW + ((ScaW - He) / 2)
        Printer.Print Tit
        Printer.Font.Bold = False
    
        Printer.Font.Size = 11
        Printer.Print
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     N�Orden: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print NOrdenBox.Text
        Ant = Printer.CurrentY
        Printer.CurrentX = ScaW
        Printer.Font.Bold = True
        Printer.Print "     Fecha Solicitud: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print FechaOrdenBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Departamento: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print ; CoDeparBox.Text; "__"; Label30.Caption
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Peticionario: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print PeticionarioBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Ext.Tel�fono: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print ExtTelBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Lugar: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print LugarBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Planta: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print PlantaBox.Text
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Asignado a "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print EsppBox.Caption
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Font.Bold = True
        Printer.Print Tit2
        Printer.Font.Bold = False
        
            For j = 1 To Len(DefBox.Text)
                
                If Printer.TextWidth(Aescribir) >= (ScaW - Printer.TextWidth("****")) Or Mid(DefBox.Text, j, 1) = Chr(13) Then
                    If Mid(DefBox.Text, j, 1) = Chr(13) Then
                        j = j + 1
                    End If
                    Printer.CurrentX = ScaW
                    Printer.Print " " & Aescribir
                    Aescribir = ""
                Else
                    Aescribir = Aescribir & Mid(DefBox.Text, j, 1)
                
                End If
            
            Next j
            
            If Aescribir <> "" Then
                Printer.CurrentX = ScaW
                Printer.Print " " & Aescribir
            End If
        
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "____________________________________________________________"
        Printer.Print
        Printer.Print
        Printer.Font.Bold = True
        Printer.CurrentX = ScaW
        Printer.Print "     Area de Trabajo:             / "
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Fecha Finalizaci�n: "
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Tiempo de Paro:"
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Tiempo de Reparaci�n: "
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Diagn�stico: "
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Causa: "
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Acci�n: "
    
        Printer.EndDoc
    
    Case False
        If Mid(DefBox.Text, 1, 1) = "*" Then
            Cond = True
        Else
            Cond = False
        End If
        With Datos
            .RecordSource = "SELECT M.CodigoMaquina, M.Descripcion FROM ListaMP L, InventMaquinas M WHERE L.CodigoMaquina=M.CodigoMaquina AND L.NOrden=" & NOrdenBox.Text & " ORDER BY M.CodigoMaquina"
            .Refresh
            
            If .Recordset.RecordCount = 0 Then
                .RecordSource = "SELECT M.CodigoMaquina, M.Descripcion FROM ListaMP2 L, InventMaquinas M WHERE L.CodigoMaquina=M.CodigoMaquina AND L.NOrden=" & NOrdenBox.Text & " ORDER BY M.CodigoMaquina"
                .Refresh
                If .Recordset.RecordCount <> 0 Then
                    .Recordset.MoveLast
                    .Recordset.MoveFirst
                End If
            Else
                .Recordset.MoveLast
                .Recordset.MoveFirst
            End If
            
        End With
        
        If Datos.Recordset.RecordCount < 11 Then
        
            Printer.PaperBin = 1
            Printer.Orientation = 2
            Printer.FontName = "Times New Roman"

            ScaH = Printer.ScaleHeight / 2
            ScaW = Printer.ScaleWidth / 2
            Seg = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXX")
        
            Printer.Font.Size = 18
            Printer.Font.Bold = True
            He = Printer.TextWidth("C.U.N. MANTENIMIENTO")
            Printer.CurrentX = ScaW + ((ScaW - He) / 2)
            Printer.CurrentY = 0
            Printer.Print "C.U.N. MANTENIMIENTO"
    
            He = Printer.TextWidth("MANTENIMIENTO PREVENTIVO")
            Printer.CurrentX = ScaW + ((ScaW - He) / 2)
            Printer.Print "MANTENIMIENTO PREVENTIVO"
            Printer.Font.Bold = False
        
            Printer.Font.Size = 11
            Printer.Print
            Printer.CurrentX = ScaW
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     N�Orden: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print NOrdenBox.Text
            Ant = Printer.CurrentY
            Printer.CurrentX = ScaW
            Printer.Font.Bold = True
            Printer.Print "     Fecha Solicitud: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print FechaOrdenBox.Text
            Printer.CurrentX = ScaW
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     Asignado a "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print EsppBox.Caption & Mid$(BuscaOperario(OrdenData.Recordset.Fields("CodigoOperario").Value), 1, 30)
            Printer.Print
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Font.Bold = True
            Printer.Print "     Area de Trabajo: "
            Printer.Font.Bold = False
            Printer.CurrentX = ScaW
            Printer.Print "   " & AreaTBox.Text & "_" & Label13.Caption
            Printer.Print
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Font.Bold = True
            Printer.Print "     Efectuar revisi�n: "
            Printer.Font.Bold = False
        
            For j = 1 To Len(DefBox.Text)
                
                If Printer.TextWidth(Aescribir) >= (ScaW - Printer.TextWidth("****")) Or Mid(DefBox.Text, j, 1) = Chr(13) Then
                    If Mid(DefBox.Text, j, 1) = Chr(13) Then
                        j = j + 1
                    End If
                    Printer.CurrentX = ScaW
                    Printer.Print " " & Aescribir
                    Aescribir = ""
                Else
                    Aescribir = Aescribir & Mid(DefBox.Text, j, 1)
                
                End If
            
            Next j
            
            If Aescribir <> "" Then
                Printer.CurrentX = ScaW
                Printer.Print " " & Aescribir
            End If
            
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.CurrentX = ScaW
            Printer.Print "     Aplicar a :"
            Printer.Font.Bold = False
            If Cond = False Then
                Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "Hecho"
            Else
                Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "  S1     S2      S3     S4  "
            End If
            
            For i = 1 To Datos.Recordset.RecordCount
                Printer.CurrentX = ScaW + Printer.TextWidth("XXXX")
                Ant = Printer.CurrentY
                Printer.Print Datos.Recordset.Fields("CodigoMaquina").Value & " , " & Datos.Recordset.Fields("Descripcion").Value
                Printer.CurrentY = Ant
                If Cond = False Then
                    Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___]"
                Else
                    Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___] [___] [___] [___]"
                End If
                Datos.Recordset.MoveNext
            Next i

            
            Printer.Print
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Print "____________________________________________________________"
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.CurrentX = ScaW
            Printer.Print "     Fecha Finalizaci�n: "
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Print "     Tiempo Total (Estimado " & EstimBox.Caption & "): "
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Print "     Tiempo Reparaciones Adicionales: "
            Printer.Font.Bold = False
            
            Printer.EndDoc
        
        
        
        Else
                
            Printer.PaperBin = 2
            Printer.Orientation = 1
            Printer.FontName = "Times New Roman"

            ScaH = Printer.ScaleHeight
            ScaW = Printer.ScaleWidth
            Seg = Printer.TextWidth("XXXXXXXXXXXXXXXXX")
    
            Printer.Font.Size = 18
            Printer.Font.Bold = True
            He = Printer.TextWidth("C.U.N. MANTENIMIENTO")
            Printer.CurrentX = ((ScaW - He) / 2)
            Printer.CurrentY = 0
            Printer.Print "C.U.N. MANTENIMIENTO"
            He = Printer.TextWidth("MANTENIMIENTO PREVENTIVO")
            Printer.CurrentX = ((ScaW - He) / 2)
            Printer.Print "MANTENIMIENTO PREVENTIVO"
            Printer.Font.Bold = False
    
            Printer.Font.Size = 11
            Printer.Print
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     N�Orden: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print NOrdenBox.Text
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     Fecha Solicitud: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print FechaOrdenBox.Text
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     Asignado a "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print EsppBox.Caption & Mid$(BuscaOperario(OrdenData.Recordset.Fields("CodigoOperario").Value), 1, 30)
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.Print "     Area de Trabajo: "
            Printer.Font.Bold = False
            Printer.Print "   " & AreaTBox.Text & "_" & Label13.Caption
            Printer.Print
            Printer.Print
    
            Printer.Font.Bold = True
            Printer.Print "     Efectuar revisi�n: "
            Printer.Font.Bold = False
            
            'Printer.Print DefBox.Text
            
            i = Len(DefBox.Text)
            j = 1
            Do While j <= i
                For k = 1 To 110
                    If Asc(CStr(Mid(DefBox.Text, j, 1))) <> 13 And j < i Then
                        Aescribir = Aescribir & Mid(DefBox.Text, j, 1)
                        j = j + 1
                    Else
                        j = j + 2
                        Exit For
                    End If
                Next
                Printer.Print "  " & Aescribir
                Aescribir = ""
            Loop
            
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.Print " Aplicar a :"
            Printer.Font.Bold = False
            If Cond = False Then
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "Hecho"
            Else
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "  S1    S2    S3    S4   "
            End If
            
            For i = 1 To Datos.Recordset.RecordCount
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXX")
                Ant = Printer.CurrentY
                Printer.Print Datos.Recordset.Fields("CodigoMaquina").Value & " , " & Datos.Recordset.Fields("Descripcion").Value
                Printer.CurrentY = Ant
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                If Cond = False Then
                    Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___]"
                Else
                    Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___] [___] [___] [___] "
                End If
                Datos.Recordset.MoveNext
            Next i
        
            Printer.Print "______________________________________________________________________________________"
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.Print
            Printer.Print "     Fecha Finalizaci�n: "
            Printer.Print
            Printer.Print "     Tiempo Total (Estimado " & EstimBox.Caption & "): "
            Printer.Print
            Printer.Print "     Tiempo Reparaciones Adicionales: "
            Printer.Print
            Printer.Print
        
            Printer.EndDoc
    
        End If
    
    End Select
        
    HoOr = OrdenData.Recordset.Fields("HoraOrden").Value
    OrdenData.Recordset.Edit
    OrdenData.Recordset.Fields("Listada").Value = True
    OrdenData.Recordset.Fields("TiempoEspera").Value = DateDiff("h", HoOr, Time)
    OrdenData.Recordset.Update
    
    Else
        
'Impresora con una sola bandeja
        
    Select Case TipoOrdenBox.Text
    Case "Reparaci�n"
        Opcion = True
        Tit = "AVISO DE REPARACI�N"
        Tit2 = "    Descripci�n de la aver�a: "
    Case "Instalaci�n"
        Opcion = True
        Tit = "SOLICITUD DE REFORMA-INSTALACI�N"
        Tit2 = "    Reforma solicitada: "
    Case "Rep.Anomal�as"
        Opcion = True
        Tit = "REPARACI�N DE ANOMAL�A"
        Tit2 = "    Descripci�n de la aver�a: "
    Case "Mant.Preventivo"
        Opcion = False
    Case ""
        MsgBox ("Necesita completar el tipo de orden")
        Exit Function
    End Select
    
    Select Case Opcion
    Case True
        Printer.Orientation = 2
        Printer.FontName = "Times New Roman"

        ScaH = Printer.ScaleHeight / 2
        ScaW = Printer.ScaleWidth / 2 + Printer.TextWidth("XX")
        Seg = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXX")
    
        Printer.Font.Size = 18
        Printer.Font.Bold = True
        He = Printer.TextWidth("C.U.N. MANTENIMIENTO")
        Printer.CurrentX = ScaW + ((ScaW - He) / 2)
        Printer.CurrentY = 0
        Printer.Print "C.U.N. MANTENIMIENTO"

        He = Printer.TextWidth(Tit)
        Printer.CurrentX = ScaW + ((ScaW - He) / 2)
        Printer.Print Tit
        Printer.Font.Bold = False
    
        Printer.Font.Size = 11
        Printer.Print
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     N�Orden: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print NOrdenBox.Text
        Ant = Printer.CurrentY
        Printer.CurrentX = ScaW
        Printer.Font.Bold = True
        Printer.Print "     Fecha Solicitud: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print FechaOrdenBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Departamento: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print ; CoDeparBox.Text; "__"; Label30.Caption
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Peticionario: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print PeticionarioBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Ext.Tel�fono: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print ExtTelBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Lugar: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print LugarBox.Text
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Planta: "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print PlantaBox.Text
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Ant = Printer.CurrentY
        Printer.Font.Bold = True
        Printer.Print "     Asignado a "
        Printer.Font.Bold = False
        Printer.CurrentY = Ant
        Printer.CurrentX = Seg
        Printer.Print EsppBox.Caption
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Font.Bold = True
        Printer.Print Tit2
        Printer.Font.Bold = False
        
            For j = 1 To Len(DefBox.Text)
                
                If Printer.TextWidth(Aescribir) >= (ScaW - Printer.TextWidth("******")) Or Mid(DefBox.Text, j, 1) = Chr(13) Then
                    If Mid(DefBox.Text, j, 1) = Chr(13) Then
                        j = j + 1
                    End If
                    Printer.CurrentX = ScaW + Printer.TextWidth("XX")
                    Printer.Print " " & Aescribir
                    Aescribir = ""
                Else
                    Aescribir = Aescribir & Mid(DefBox.Text, j, 1)
                
                End If
            
            Next j
            
            If Aescribir <> "" Then
                Printer.CurrentX = ScaW + Printer.TextWidth("XX")
                Printer.Print " " & Aescribir
            End If
        
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "____________________________________________________________"
        Printer.Print
        Printer.Font.Bold = True
        Printer.CurrentX = ScaW
        Printer.Print "     Area de Trabajo:             / "
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Fecha Finalizaci�n: "
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Tiempo de Paro:"
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Tiempo de Reparaci�n: "
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Diagn�stico: "
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Causa: "
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.CurrentX = ScaW
        Printer.Print "     Acci�n: "
    
        Printer.EndDoc
    
    Case False
        If Mid(DefBox.Text, 1, 1) = "*" Then
            Cond = True
        Else
            Cond = False
        End If
        With Datos
            .RecordSource = "SELECT M.CodigoMaquina, M.Descripcion FROM ListaMP L, InventMaquinas M WHERE L.CodigoMaquina=M.CodigoMaquina AND L.NOrden=" & NOrdenBox.Text & " ORDER BY M.CodigoMaquina"
            .Refresh
            
            If .Recordset.RecordCount = 0 Then
                .RecordSource = "SELECT M.CodigoMaquina, M.Descripcion FROM ListaMP2 L, InventMaquinas M WHERE L.CodigoMaquina=M.CodigoMaquina AND L.NOrden=" & NOrdenBox.Text & " ORDER BY M.CodigoMaquina"
                .Refresh
                If .Recordset.RecordCount <> 0 Then
                    .Recordset.MoveLast
                    .Recordset.MoveFirst
                End If
            Else
                .Recordset.MoveLast
                .Recordset.MoveFirst
            End If
            
        End With
        
        If Datos.Recordset.RecordCount < 11 Then
        
            Printer.Orientation = 2
            Printer.FontName = "Times New Roman"

            ScaH = Printer.ScaleHeight / 2
            ScaW = Printer.ScaleWidth / 2 + Printer.TextWidth("XX")
            Seg = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXX")
        
            Printer.Font.Size = 18
            Printer.Font.Bold = True
            He = Printer.TextWidth("C.U.N. MANTENIMIENTO")
            Printer.CurrentX = ScaW + ((ScaW - He) / 2)
            Printer.CurrentY = 0
            Printer.Print "C.U.N. MANTENIMIENTO"
    
            He = Printer.TextWidth("MANTENIMIENTO PREVENTIVO")
            Printer.CurrentX = ScaW + ((ScaW - He) / 2)
            Printer.Print "MANTENIMIENTO PREVENTIVO"
            Printer.Font.Bold = False
        
            Printer.Font.Size = 11
            Printer.Print
            Printer.CurrentX = ScaW
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     N�Orden: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print NOrdenBox.Text
            Ant = Printer.CurrentY
            Printer.CurrentX = ScaW
            Printer.Font.Bold = True
            Printer.Print "     Fecha Solicitud: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print FechaOrdenBox.Text
            Printer.CurrentX = ScaW
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     Asignado a "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print EsppBox.Caption & Mid$(BuscaOperario(OrdenData.Recordset.Fields("CodigoOperario").Value), 1, 30)
            Printer.Print
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Font.Bold = True
            Printer.Print "     Area de Trabajo: "
            Printer.Font.Bold = False
            Printer.CurrentX = ScaW
            Printer.Print AreaTBox.Text & "_" & Label13.Caption
            Printer.Print
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Font.Bold = True
            Printer.Print "     Efectuar revisi�n: "
            Printer.Font.Bold = False
        
            For j = 1 To Len(DefBox.Text)
                
                If Printer.TextWidth(Aescribir) >= (ScaW - Printer.TextWidth("****")) Or Mid(DefBox.Text, j, 1) = Chr(13) Then
                    If Mid(DefBox.Text, j, 1) = Chr(13) Then
                        j = j + 1
                    End If
                    Printer.CurrentX = ScaW + Printer.TextWidth("XX")
                    Printer.Print " " & Aescribir
                    Aescribir = ""
                Else
                    Aescribir = Aescribir & Mid(DefBox.Text, j, 1)
                
                End If
            
            Next j
            
            If Aescribir <> "" Then
                Printer.CurrentX = ScaW + Printer.TextWidth("XX")
                Printer.Print " " & Aescribir
            End If
            
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.CurrentX = ScaW
            Printer.Print "     Aplicar a :"
            Printer.Font.Bold = False
            If Cond = False Then
                Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "Hecho"
            Else
                Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "  S1     S2      S3     S4   "
            End If
            
            For i = 1 To Datos.Recordset.RecordCount
                Printer.CurrentX = ScaW + Printer.TextWidth("XXXX")
                Ant = Printer.CurrentY
                Printer.Print Datos.Recordset.Fields("CodigoMaquina").Value & " , " & Datos.Recordset.Fields("Descripcion").Value
                Printer.CurrentY = Ant
                If Cond = False Then
                    Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___]"
                Else
                    Printer.CurrentX = ScaW + Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___] [___] [___] [___] "
                End If
                Datos.Recordset.MoveNext
            Next i

            
            Printer.Print
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Print "____________________________________________________________"
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.CurrentX = ScaW
            Printer.Print "     Fecha Finalizaci�n: "
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Print "     Tiempo Total (Estimado " & EstimBox.Caption & "): "
            Printer.Print
            Printer.CurrentX = ScaW
            Printer.Print "     Tiempo Reparaciones Adicionales: "
            Printer.Font.Bold = False
            
            Printer.EndDoc
        
        
        
        Else
                
            Printer.Orientation = 1
            Printer.FontName = "Times New Roman"

            ScaH = Printer.ScaleHeight
            ScaW = Printer.ScaleWidth
            Seg = Printer.TextWidth("XXXXXXXXXXXXXXXXX")
    
            Printer.Font.Size = 18
            Printer.Font.Bold = True
            He = Printer.TextWidth("C.U.N. MANTENIMIENTO")
            Printer.CurrentX = ((ScaW - He) / 2)
            Printer.CurrentY = 0
            Printer.Print "C.U.N. MANTENIMIENTO"
            He = Printer.TextWidth("MANTENIMIENTO PREVENTIVO")
            Printer.CurrentX = ((ScaW - He) / 2)
            Printer.Print "MANTENIMIENTO PREVENTIVO"
            Printer.Font.Bold = False
    
            Printer.Font.Size = 11
            Printer.Print
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     N�Orden: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print NOrdenBox.Text
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     Fecha Solicitud: "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print FechaOrdenBox.Text
            Ant = Printer.CurrentY
            Printer.Font.Bold = True
            Printer.Print "     Asignado a "
            Printer.Font.Bold = False
            Printer.CurrentY = Ant
            Printer.CurrentX = Seg
            Printer.Print EsppBox.Caption & Mid$(BuscaOperario(OrdenData.Recordset.Fields("CodigoOperario").Value), 1, 30)
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.Print "     Area de Trabajo: "
            Printer.Font.Bold = False
            Printer.Print AreaTBox.Text & "_" & Label13.Caption
            Printer.Print
            Printer.Print
    
            Printer.Font.Bold = True
            Printer.Print "     Efectuar revisi�n: "
            Printer.Font.Bold = False
            
            'Printer.Print DefBox.Text
            
            i = Len(DefBox.Text)
            j = 1
            Do While j <= i
                For k = 1 To 110
                    If Asc(CStr(Mid(DefBox.Text, j, 1))) <> 13 Then
                        Aescribir = Aescribir & Mid(DefBox.Text, j, 1)
                        j = j + 1
                    Else
                        j = j + 2
                        Exit For
                    End If
                Next
                Printer.Print "  " & Aescribir
                Aescribir = ""
            Loop
            
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.Print " Aplicar a :"
            Printer.Font.Bold = False
            If Cond = False Then
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "Hecho"
            Else
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                Printer.Print "  S1    S2    S3    S4  "
            End If
            
            For i = 1 To Datos.Recordset.RecordCount
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXX")
                Ant = Printer.CurrentY
                Printer.Print Datos.Recordset.Fields("CodigoMaquina").Value & " , " & Datos.Recordset.Fields("Descripcion").Value
                Printer.CurrentY = Ant
                Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                If Cond = False Then
                    Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___]"
                Else
                    Printer.CurrentX = Printer.TextWidth("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                    Printer.Print "[___] [___] [___] [___]"
                End If
                Datos.Recordset.MoveNext
            Next i
        
            Printer.Print "______________________________________________________________________________________"
            Printer.Print
            Printer.Print
            Printer.Font.Bold = True
            Printer.Print
            Printer.Print "     Fecha Finalizaci�n: "
            Printer.Print
            Printer.Print "     Tiempo Total (Estimado " & EstimBox.Caption & "): "
            Printer.Print
            Printer.Print "     Tiempo Reparaciones Adicionales: "
            Printer.Print
            Printer.Print
        
            Printer.EndDoc
    
        End If
    
    End Select
        
    HoOr = OrdenData.Recordset.Fields("HoraOrden").Value
    OrdenData.Recordset.Edit
    OrdenData.Recordset.Fields("Listada").Value = True
    OrdenData.Recordset.Fields("TiempoEspera").Value = DateDiff("h", HoOr, Time)
    OrdenData.Recordset.Update
    
    End If
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Sub Ult_Click()
    OrdenData.Recordset.MoveLast
End Sub


Private Function Almacen()
        
    Dim nada As Long
On Error GoTo msgError
    Orden(0) = Val(NOrdenBox.Text)
    With PedidoAlForm
        .Show modal
        .PedidoData.RecordSource = "SELECT * FROM PedidoAl WHERE NOrden=" & NOrdenBox.Text
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount <> 0 Then
            nada = .PedidoData.Recordset.Fields("NPedidoA").Value
            .PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedidoAl PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedidoA=" & nada & " ORDER BY PP.Linea"
            .PPedidoData.Refresh
        End If

    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical

End Function


Private Function Listar()
        
    Dim nada As Integer
           
    nada = NuevaBusq()
    Display(0) = "NOrden"
    Display(1) = "FechaOrden"
    Display(2) = "CodigoLugar"
    Display(3) = "CodigoEspecialidad"
    Display(4) = "CodigoMaquina"
        
    TypeCampos(0) = 1
    TypeCampos(1) = 2
    TypeCampos(2) = 3
    TypeCampos(3) = 3
    TypeCampos(4) = 3
    TypeCampos(5) = 0
    TypeCampos(6) = 0
    TypeCampos(7) = 0
        
    For nada = 0 To 4
        campos(nada) = "O." & Display(nada)
    Next
        
    Lists(2, 0) = "CodigoLugar, Localizacion"
    Lists(2, 1) = "CodigoLugar"
    Lists(2, 2) = "Lugar"
    Lists(3, 0) = "CodigoEspecialidad, Especialidad"
    Lists(3, 1) = "CodigoEspecialidad"
    Lists(3, 2) = "Tarifas"
    Lists(4, 0) = "CodigoMaquina, Descripcion"
    Lists(4, 1) = "CodigoMaquina"
    Lists(4, 2) = "InventMaquinas"
        
    FRO = "Orden O, Lugar L, InventMaquinas I"
    SELE = "O.* "
    'SELE = campos(0) & "," & campos(1) & "," & campos(2) & "," & campos(3) & "," & campos(4) & ", L.Localizacion, O.TiempoReparacion "
    WHER = "O.CodigoLugar=L.CodigoLugar AND O.CodigoMaquina=I.CodigoMaquina"
    ORD = "O.NOrden"
        
    Set Objeto = OrdenForm.OrdenData
    
    Selct = False
        
    nada = Listado()

End Function

Private Function PExt()
    
    Dim nada As Long
On Error GoTo msgError
    With PedidoForm
        .Show modal
        .PedidoData.RecordSource = "SELECT * FROM Pedido WHERE NOrden=" & NOrdenBox.Text
        .PedidoData.Refresh
        If .PedidoData.Recordset.RecordCount <> 0 Then
            nada = .PedidoData.Recordset.Fields("NPedido").Value
            .PPedidoData.RecordSource = "SELECT PP.CodigoArticulo, AR.Descripcion, PP.Cantidad,PP.Linea,PP.SuCodigo, AR.PrecioUnit FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo=AR.CodigoArticulo AND PP.NPedido=" & nada & " ORDER BY PP.Linea"
            .PPedidoData.Refresh
        End If

    End With
    
    Orden(0) = Val(NOrdenBox.Text)
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function

Private Function Final()
    
    Frame1.Enabled = True
    With OrdenData.Recordset
        .Edit
        .Fields("Finalizada").Value = True
        .Fields("FechaFinalizacion").Value = Date
        HoraComplBox.Text = "19.00.00"
        .Fields("HoraCompletado").Value = Format("19.00.00", "hh.mm.ss")
        If TipoOrdenBox.Text = "Mant.Preventivo" Then
            .Fields("TiempoTotalMP").Value = .Fields("TiempoEstimadoMP").Value
        End If
        .Update
    End With

End Function

Public Function Control(Nor As Long)
    
    Dim i As Integer, j As Integer
    Dim PedTotal As Long, SATTotal As Long
    Dim Fecha1 As Date, Fecha2 As Date, pH As Long
On Error GoTo msgError
    With Datos
        .RecordSource = "SELECT PP.CodigoArticulo, PP.Cantidad, AR.PrecioUnit, P.NOrden, P.NPedido FROM PPedido PP, Pedido P, Articulos AR WHERE P.NOrden=" & Nor & " AND PP.NPedido=P.NPedido AND PP.CodigoArticulo=AR.CodigoArticulo"
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveLast
            .Recordset.MoveFirst
            i = .Recordset.RecordCount
            For j = 0 To i - 1
                PedTotal = PedTotal + (.Recordset.Fields("Cantidad").Value) * (.Recordset.Fields("PrecioUnit").Value)
                .Recordset.MoveNext
            Next
        End If
    End With
    
    With Datos
        .RecordSource = "SELECT F.Total FROM SATFact F, SATFFactura FF,SATAlbaran A,PedidoSAT P WHERE P.NOrden=" & Nor & " AND P.NPetSAT=A.NPetSAT AND A.NAlbaranSAT=FF.NAlbaranSAT AND F.NFacturaSAT=FF.NFacturaSAT"
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveLast
            .Recordset.MoveFirst
            i = .Recordset.RecordCount
            For j = 0 To i - 1
                SATTotal = SATTotal + (.Recordset.Fields("Total").Value)
                .Recordset.MoveNext
            Next
        End If
    End With

    With Datos
        .RecordSource = "SELECT P.FechaPedido, A.FechaAlbaran, P.NPedido, A.NAlbaran FROM Orden O, Pedido P, Albaran A, AAlbaran AA WHERE O.NOrden=" & Nor & " AND P.NOrden=O.NOrden AND P.NPedido=AA.NPedido AND AA.NAlbaran=A.NAlbaran GROUP BY P.NPedido, A.NAlbaran, P.FechaPedido, A.FechaAlbaran ORDER BY P.FechaPedido"
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveLast
            .Recordset.MoveFirst
            Fecha1 = .Recordset.Fields("FechaPedido").Value
        End If
            
        .RecordSource = "SELECT P.FechaPedido, A.FechaAlbaran, P.NPedido, A.NAlbaran FROM Orden O, Pedido P, Albaran A, AAlbaran AA WHERE O.NOrden=" & Nor & " AND P.NOrden=O.NOrden AND P.NPedido=AA.NPedido AND AA.NAlbaran=A.NAlbaran GROUP BY P.NPedido, A.NAlbaran, P.FechaPedido, A.FechaAlbaran ORDER BY A.FechaAlbaran"
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            .Recordset.MoveLast
            Fecha2 = .Recordset.Fields("FechaAlbaran").Value
        End If
        .Recordset.Close
                
    End With
    
    With EspecData.Recordset
        .FindFirst ("CodigoEspecialidad=" & EspecBox.Text)
        pH = .Fields("PrecioHora").Value
    End With
    
    With OrdenData.Recordset
      .FindFirst ("NOrden=" & Nor)
      If .Fields("Finalizada").Value = True Then
       .Edit
       .Fields("CosteMaterial").Value = PedTotal
       .Fields("CosteSAT").Value = SATTotal
       
    If TipoOrdenBox.Text = "Mant.Preventivo" Then

       '.Fields("TiempoTotalMP").Value = CDbl(TTotalBoxMP.Text)
       .Fields("CosteManoDO").Value = pH * (CDbl(TTotalBoxMP.Text) + CDbl(TRepBoxMP.Text))
       .Fields("CosteTotal").Value = PedTotal + SATTotal + (pH * (CDbl(TTotalBoxMP.Text) + CDbl(TRepBoxMP.Text)))
    Else
       .Fields("CosteManoDO").Value = pH * CDbl(Text14.Text)
       .Fields("CosteTotal").Value = PedTotal + SATTotal + (pH * CDbl(Text14.Text))
    End If
             
       .Fields("EsperaMaterial").Value = DateDiff("h", Fecha1, Fecha2)
       
       Select Case TipoOrdenBox.Text
       Case "Mant.Preventivo"
'         .Fields("TiempoTotalMP").Value = CDbl(TTotalBoxMP.Text) + CDbl(TRepBoxMP.Text)
       Case Else
         .Fields("TiempoTotal").Value = DateDiff("h", CDate(HoraAvisoBox.Text), CDate(HoraComplBox.Text)) + DateDiff("h", CDate(FechaOrdenBox.Text), CDate(FechaFinalBox.Text))
       End Select
       
       .Update
      End If
    End With
    Exit Function
msgError:
    MsgBox Err.Description, vbCritical
End Function


Private Function BuscaOperario(CodOper As Integer) As String

On Error GoTo msgError
    With Data1
        .RecordSource = "SELECT Nombre FROM Operarios WHERE CodigoOperario=" & CodOper
        .Refresh
        If .Recordset.RecordCount <> 0 Then
            BuscaOperario = .Recordset.Fields("Nombre").Value
        Else
            BuscaOperario = ""
        End If
        .Recordset.Close
    End With
    Exit Function
msgError:
    BuscaOperario = ""
    MsgBox Err.Description, vbCritical
End Function
