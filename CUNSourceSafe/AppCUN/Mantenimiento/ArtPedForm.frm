VERSION 5.00
Object = "{00028C01-0000-0000-0000-000000000046}#1.0#0"; "DBGRID32.OCX"
Object = "{FAEEE763-117E-101B-8933-08002B2F4F5A}#1.1#0"; "DBLIST32.OCX"
Begin VB.Form ArtPedForm 
   Caption         =   "Art�culos"
   ClientHeight    =   5940
   ClientLeft      =   1545
   ClientTop       =   1140
   ClientWidth     =   9255
   LinkTopic       =   "Form1"
   ScaleHeight     =   5940
   ScaleWidth      =   9255
   Begin VB.Data PPedidoData2 
      Caption         =   "PPedido2"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   5640
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   240
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Data ArtData 
      Caption         =   "Articulo"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   600
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Articulos"
      Top             =   240
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton A�adirBoton 
      Caption         =   "&A�adir"
      Height          =   375
      Left            =   7800
      TabIndex        =   12
      Top             =   4680
      Width           =   1215
   End
   Begin VB.TextBox CantBox 
      Height          =   285
      Left            =   6360
      TabIndex        =   11
      Top             =   4680
      Width           =   1095
   End
   Begin MSDBCtls.DBCombo DBCombo1 
      Bindings        =   "ArtPedForm.frx":0000
      Height          =   315
      Left            =   2160
      TabIndex        =   9
      Top             =   4680
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   556
      _Version        =   327680
      ListField       =   "Descripcion"
      Text            =   "DBCombo1"
   End
   Begin VB.TextBox CABox 
      Height          =   285
      Left            =   360
      TabIndex        =   8
      Top             =   4680
      Width           =   1215
   End
   Begin VB.Data PPedidoData 
      Caption         =   "PPedido"
      Connect         =   "Access"
      DatabaseName    =   "C:\Proyecto\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   375
      Left            =   3120
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   240
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.CommandButton SalirBOTON 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   5040
      TabIndex        =   0
      Top             =   5400
      Width           =   1215
   End
   Begin MSDBGrid.DBGrid DBGrid1 
      Bindings        =   "ArtPedForm.frx":0012
      Height          =   1935
      Left            =   360
      OleObjectBlob   =   "ArtPedForm.frx":0029
      TabIndex        =   5
      Top             =   2280
      Width           =   7575
   End
   Begin VB.Label CanLabel 
      Caption         =   "Cantidad"
      Height          =   255
      Left            =   6360
      TabIndex        =   10
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label DesLabel 
      Caption         =   "Descripci�n"
      Height          =   255
      Left            =   2160
      TabIndex        =   7
      Top             =   4320
      Width           =   1095
   End
   Begin VB.Label CALabel 
      Caption         =   "C�digo Art�culo"
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   4320
      Width           =   1215
   End
   Begin VB.Line Line2 
      X1              =   120
      X2              =   8160
      Y1              =   840
      Y2              =   840
   End
   Begin VB.Label ProvBox 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   5160
      TabIndex        =   4
      Top             =   1320
      Width           =   2055
   End
   Begin VB.Label Label2 
      Caption         =   "Proveedor:"
      Height          =   255
      Left            =   4200
      TabIndex        =   3
      Top             =   1320
      Width           =   855
   End
   Begin VB.Label NPedidoBox 
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   2640
      TabIndex        =   2
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "N�Pedido:"
      Height          =   255
      Left            =   1800
      TabIndex        =   1
      Top             =   1320
      Width           =   735
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   8160
      Y1              =   2040
      Y2              =   2040
   End
End
Attribute VB_Name = "ArtPedForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub A�adirBoton_Click()
    Static id As Integer
    
    id = id + 1
    
    With PPedidoData
        .Recordset.AddNew
        .Recordset.Fields("Linea").Value = id
        .Recordset.Fields("NPedido").Value = nped(0)
        .Recordset.Fields("CodigoArticulo").Value = CABox.Text
        .Recordset.Fields("Cantidad").Value = CantBox.Text
        .Recordset.Update
    End With
        
        
    PPedidoData2.UpdateRecord
    
    DBGrid1.Refresh

End Sub

Private Sub CABox_Change()
    If CABox.Text <> "" Then
        ArtData.Recordset.FindFirst ("CodigoArticulo=" & CABox.Text)
        DBCombo1.Text = ArtData.Recordset.Fields("Descripcion").Value
    Else
        DBCombo1.Text = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        DBCombo1.Text = ""
    End If
    

End Sub

Private Sub DBCombo1_Click(Area As Integer)
    If DBCombo1.Text <> "" Then
        ArtData.Recordset.FindFirst ("Descripcion='" & DBCombo1.Text & "'")
        CABox.Text = ArtData.Recordset.Fields("CodigoArticulo").Value
    Else
        CABox.Text = ""
    End If
    
    If ArtData.Recordset.NoMatch = True Then
        CABox.Text = ""
    End If
    
End Sub

Private Sub Form_Load()
    NPedidoBox.Caption = nped(0)
    ProvBox.Caption = nped(1)
    
    PPedidoData.RecordSource = "SELECT * FROM PPedido WHERE NPedido = " & nped(0)
    PPedidoData2.RecordSource = "SELECT PP.Cantidad, AR.Descripcion, PP.CodigoArticulo FROM PPedido PP, Articulos AR WHERE PP.CodigoArticulo = AR.CodigoArticulo AND PP.NPedido=" & nped(0)
    
End Sub


Private Sub SalirBoton_Click()
    Unload Me
    
End Sub
