VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.Form MantProv 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Proveedores"
   ClientHeight    =   4710
   ClientLeft      =   150
   ClientTop       =   675
   ClientWidth     =   6195
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   6195
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   20
      Top             =   0
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "first"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "last"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
   End
   Begin VB.TextBox Text1 
      DataField       =   "Provincia"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   19
      Top             =   2520
      Width           =   1935
   End
   Begin VB.Data ProvData 
      Caption         =   "Proveedor"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   2640
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Proveedores"
      Top             =   120
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.TextBox Text9 
      DataField       =   "NIF"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   17
      Top             =   4320
      Width           =   1455
   End
   Begin VB.TextBox Text8 
      DataField       =   "Fax"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   16
      Top             =   3960
      Width           =   1455
   End
   Begin VB.TextBox Text7 
      DataField       =   "Telefono"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   15
      Top             =   3600
      Width           =   1455
   End
   Begin VB.TextBox Text6 
      DataField       =   "CP"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   14
      Top             =   3240
      Width           =   1455
   End
   Begin VB.TextBox Text5 
      DataField       =   "Pais"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   13
      Top             =   2880
      Width           =   1935
   End
   Begin VB.TextBox Text4 
      DataField       =   "Poblacion"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   12
      Top             =   2160
      Width           =   1935
   End
   Begin VB.TextBox Text3 
      DataField       =   "Direccion"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   11
      Top             =   1800
      Width           =   4095
   End
   Begin VB.TextBox Text2 
      DataField       =   "Nombre"
      DataSource      =   "ProvData"
      Height          =   285
      Left            =   1920
      TabIndex        =   10
      Top             =   1440
      Width           =   4095
   End
   Begin VB.TextBox ProvBox 
      BackColor       =   &H80000018&
      DataField       =   "CodigoProveedor"
      DataSource      =   "ProvData"
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      TabIndex        =   9
      Top             =   1080
      Width           =   1335
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":0C68
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":0F82
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":129C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MantProv.frx":15B6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Label10 
      Caption         =   "Provincia:"
      Height          =   255
      Left            =   480
      TabIndex        =   18
      Top             =   2520
      Width           =   735
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   6840
      Y1              =   600
      Y2              =   600
   End
   Begin VB.Label Label9 
      Caption         =   "N.I.F."
      Height          =   255
      Left            =   480
      TabIndex        =   8
      Top             =   4320
      Width           =   615
   End
   Begin VB.Label Label8 
      Caption         =   "Fax:"
      Height          =   255
      Left            =   480
      TabIndex        =   7
      Top             =   3960
      Width           =   615
   End
   Begin VB.Label Label7 
      Caption         =   "Tel�fono:"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   3600
      Width           =   735
   End
   Begin VB.Label Label6 
      Caption         =   "C�digo Postal:"
      Height          =   255
      Left            =   480
      TabIndex        =   5
      Top             =   3240
      Width           =   1095
   End
   Begin VB.Label Label5 
      Caption         =   "Pa�s:"
      Height          =   255
      Left            =   480
      TabIndex        =   4
      Top             =   2880
      Width           =   495
   End
   Begin VB.Label Label4 
      Caption         =   "Poblaci�n:"
      Height          =   255
      Left            =   480
      TabIndex        =   3
      Top             =   2160
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Direcci�n:"
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   1800
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Nombre:"
      Height          =   255
      Left            =   480
      TabIndex        =   1
      Top             =   1440
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Codigo Proveedor:"
      Height          =   255
      Left            =   480
      TabIndex        =   0
      Top             =   1080
      Width           =   1335
   End
   Begin VB.Menu prg 
      Caption         =   "&Programa"
      Begin VB.Menu mnuSalir 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnu 
         Caption         =   "Primero"
         Index           =   3
      End
      Begin VB.Menu mnu 
         Caption         =   "Anterior"
         Index           =   4
      End
      Begin VB.Menu mnu 
         Caption         =   "Siguiente"
         Index           =   5
      End
      Begin VB.Menu mnu 
         Caption         =   "Ultimo"
         Index           =   6
      End
      Begin VB.Menu mnu 
         Caption         =   "A�adir"
         Index           =   7
      End
      Begin VB.Menu mnu 
         Caption         =   "Borrar"
         Index           =   8
      End
   End
   Begin VB.Menu listados 
      Caption         =   "&Listados"
      Begin VB.Menu all 
         Caption         =   "Todos"
      End
   End
End
Attribute VB_Name = "MantProv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub all_Click()
  Call ListaProv
End Sub


Private Sub mnu_Click(Index As Integer)
  'menu de registros
  With ProvData.Recordset
    Select Case Index
      Case ctemnuPrimero
        .MoveFirst
      Case ctemnuAnterior
        If .RecordCount = 0 Then Exit Sub
        .MovePrevious
        If .BOF = True Then .MoveNext
      Case ctemnuSiguiente
        If .RecordCount = 0 Then Exit Sub
        .MoveNext
        If .EOF = True Then .MovePrevious
      Case ctemnuUltimo
      .MoveLast
      Case ctemnuA�adir
        If .RecordCount <> 0 Then .MoveLast
        .AddNew
        ProvBox.Text = UltimoProveedor()
        ProvData.UpdateRecord
        .MoveLast
      Case ctemnuBorrar
        If MsgBox("�Est� usted seguro de querer borrar el proveedor?", vbYesNo) = vbYes Then
          .Delete
          If .RecordCount = 0 Then Exit Sub     'anterior
          .MovePrevious
          If .BOF = True Then .MoveNext
        End If
    End Select
  End With
End Sub


Private Sub mnuSalir_Click()
   Unload Me
'    If Pedidoform.Enabled = True Then Pedidoform.ProvData.Refresh
    If AlbaranForm.Enabled = True Then AlbaranForm.ProvData.Refresh
    If FacturaForm.Enabled = True Then FacturaForm.ProvData.Refresh
End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
  'barra de herramientas
  Select Case Button.Key
    Case "Ant":     mnu_Click (ctemnuAnterior)
    Case "Sig":     mnu_Click (ctemnuSiguiente)
    Case "Salir":   mnuSalir_Click
    Case "Add":     mnu_Click (ctemnuA�adir)
    Case "Lista":   Call ListaProv
    Case "Borrar":  mnu_Click (ctemnuBorrar)
    Case "first":   mnu_Click (ctemnuPrimero)
    Case "last":    mnu_Click (ctemnuUltimo)
  End Select
End Sub

Private Function ListaProv()
  Dim i As Integer
  Call NuevaBusq
  Display(0) = "CodigoProveedor"
  Display(1) = "Nombre"
  Display(2) = "Poblacion"
  Display(3) = "Telefono"
  Display(4) = "Fax"
  For i = 0 To 4
    campos(i) = Display(i)
  Next
  SELE = "CodigoProveedor, Nombre, Poblacion, Telefono, Fax"
  FRO = "Proveedores"
  ORD = "Nombre"
  Set Objeto = MantProv.ProvData
  Selct = False
  MosBusForm.Show modal
End Function
