VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form ManTarifasForm 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Mantenimiento Secciones"
   ClientHeight    =   2250
   ClientLeft      =   150
   ClientTop       =   675
   ClientWidth     =   6150
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2250
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   870
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   6150
      _ExtentX        =   10848
      _ExtentY        =   1535
      ButtonWidth     =   1349
      ButtonHeight    =   1376
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   8
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Primero"
            Key             =   "first"
            Description     =   "PrimBoton"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Anterior"
            Key             =   "Ant"
            Description     =   "AntBoton"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "A�adir"
            Key             =   "Add"
            Description     =   "A�adirBoton"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Listado"
            Key             =   "Lista"
            Description     =   "ListadoBoton"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Borrar"
            Key             =   "Borrar"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Salir"
            Key             =   "Salir"
            Description     =   "SalirBoton"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "Siguiente"
            Key             =   "Sig"
            Description     =   "SigBoton"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
         BeginProperty Button8 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Caption         =   "�ltimo"
            Key             =   "last"
            Description     =   "UltBoton"
            Object.Tag             =   ""
            ImageIndex      =   8
         EndProperty
      EndProperty
   End
   Begin VB.TextBox PHBox 
      DataField       =   "PrecioHora"
      DataSource      =   "TarData"
      Height          =   285
      Left            =   2280
      TabIndex        =   4
      Top             =   1800
      Width           =   1455
   End
   Begin VB.TextBox EspBox 
      DataField       =   "Especialidad"
      DataSource      =   "TarData"
      Height          =   285
      Left            =   2280
      TabIndex        =   2
      Top             =   1440
      Width           =   3135
   End
   Begin VB.Data TarData 
      Caption         =   "Tarifas"
      Connect         =   "Access"
      DatabaseName    =   "D:\mantcun.mdb"
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   300
      Left            =   1320
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   "Tarifas"
      Top             =   0
      Visible         =   0   'False
      Width           =   2055
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   8
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":031A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":0634
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":094E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":0C68
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":0F82
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":129C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "ManTarifasForm.frx":15B6
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label CodEspBox 
      BorderStyle     =   1  'Fixed Single
      DataField       =   "CodigoEspecialidad"
      DataSource      =   "TarData"
      Height          =   255
      Left            =   2280
      TabIndex        =   5
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "Precio Hora:"
      Height          =   255
      Left            =   600
      TabIndex        =   3
      Top             =   1800
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Secci�n:"
      Height          =   255
      Left            =   600
      TabIndex        =   1
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Codigo Especialidad:"
      Height          =   255
      Left            =   600
      TabIndex        =   0
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Menu Prg 
      Caption         =   "&Programa"
      Begin VB.Menu Exit 
         Caption         =   "Salir"
         Shortcut        =   ^X
      End
   End
   Begin VB.Menu Reg 
      Caption         =   "&Registro"
      Begin VB.Menu First 
         Caption         =   "Primero"
      End
      Begin VB.Menu Ante 
         Caption         =   "Anterior"
         Shortcut        =   ^A
      End
      Begin VB.Menu Sigui 
         Caption         =   "Siguiente"
         Shortcut        =   ^S
      End
      Begin VB.Menu Last 
         Caption         =   "�ltimo"
      End
      Begin VB.Menu ADd 
         Caption         =   "A�adir"
      End
      Begin VB.Menu Eraser 
         Caption         =   "Borrar"
      End
   End
   Begin VB.Menu Lista 
      Caption         =   "&Listado"
      Begin VB.Menu all 
         Caption         =   "Todas"
      End
   End
End
Attribute VB_Name = "ManTarifasForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function Anterior()
    TarData.Recordset.MovePrevious
 
    If TarData.Recordset.BOF = True Then
        TarData.Recordset.MoveNext
    End If
    
End Function

Private Function OtroEsp()
    
    Dim i As Long
    
    TarData.Recordset.MoveLast
    i = TarData.Recordset.Fields("CodigoEspecialidad").Value
    TarData.Recordset.AddNew
    TarData.Recordset.Fields("CodigoEspecialidad").Value = i + 1
    TarData.Recordset.Update
    TarData.Recordset.MoveLast
    
End Function

Private Function Siguiente()
    
    TarData.Recordset.MoveNext
    
    If TarData.Recordset.EOF = True Then
        TarData.Recordset.MovePrevious
    End If

End Function


Private Sub add_Click()
    Dim nada As Integer
    nada = OtroEsp()
End Sub

Private Sub all_Click()
    Dim nada As Integer
    nada = ListSecciones()
End Sub

Private Sub Ante_Click()
    Dim nada As Integer
    nada = Anterior()
End Sub

Private Sub eraser_Click()
    Dim nada As Integer
    nada = MsgBox("�Est� usted seguro de querer borrar la secci�n?", vbYesNo)
    If nada = vbYes Then
        TarData.Recordset.Delete
        nada = Anterior()
    End If

End Sub

Private Sub exit_Click()
    Unload Me
End Sub

Private Sub first_Click()
    TarData.Recordset.MoveFirst
End Sub

Private Sub last_Click()
    TarData.Recordset.MoveLast
End Sub

Private Sub sigui_Click()
    Dim nada As Integer
    nada = Siguiente()

End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)
    
    Dim nada As Variant
            
    Select Case Button.Key
    Case "Ant"
        nada = Anterior()
    Case "Sig"
        nada = Siguiente()
    Case "Salir"
        Unload Me
    Case "Add"
        nada = OtroEsp()
    
    Case "Lista"
        nada = ListSecciones()

    Case "Borrar"
        nada = MsgBox("�Est� usted seguro de querer borrar la secci�n?", vbYesNo)
        If nada = vbYes Then
            TarData.Recordset.Delete
            nada = Anterior()
        End If
    Case "first"
        TarData.Recordset.MoveFirst
    Case "last"
        TarData.Recordset.MoveLast
    
    End Select
    

End Sub

Private Function ListSecciones()
        
    Dim nada As Integer
        
    nada = NuevaBusq()
    campos(0) = "CodigoEspecialidad"
    campos(1) = "Especialidad"
    campos(2) = "PrecioHora"
    For nada = 0 To 2
        Display(nada) = campos(nada)
    Next
        
    SELE = "CodigoEspecialidad, Especialidad, PrecioHora"
    FRO = "Tarifas"
        
    Set Objeto = ManTarifasForm.TarData
    
    Selct = False

    MosBusForm.Show modal
        

End Function
