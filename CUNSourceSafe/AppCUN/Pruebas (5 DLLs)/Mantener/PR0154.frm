VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmSelPrincipales 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Selecci�n de Actuaciones. Principales"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   10680
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0154.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   10680
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   11
      Top             =   0
      Width           =   10680
      _ExtentX        =   18838
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdDescrAct 
      Caption         =   "Descripci�n Actuaci�n"
      Height          =   375
      Index           =   1
      Left            =   7200
      TabIndex        =   20
      Top             =   7680
      Width           =   2175
   End
   Begin VB.CommandButton cmdAceptarPrinc 
      Caption         =   "Aceptar"
      Height          =   375
      Index           =   0
      Left            =   2760
      TabIndex        =   0
      Top             =   7680
      Width           =   2175
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   1
      Left            =   360
      TabIndex        =   12
      Top             =   480
      Width           =   10380
      Begin VB.TextBox txtdeptext1 
         BackColor       =   &H00808080&
         Height          =   330
         Index           =   1
         Left            =   1560
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Tag             =   "Descripci�n Dpto. Peticionario"
         Top             =   720
         Width           =   5400
      End
      Begin VB.TextBox txtdeptext1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   720
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Tag             =   "C�d.Dpto. Peticionario"
         Top             =   720
         Width           =   372
      End
      Begin VB.Label lbllabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento Peticionario"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   720
         TabIndex        =   13
         Top             =   480
         Width           =   2265
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Principales"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Index           =   0
      Left            =   360
      TabIndex        =   9
      Top             =   1800
      Width           =   10380
      Begin TabDlg.SSTab tabTab1 
         Height          =   5220
         Index           =   0
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   10110
         _ExtentX        =   17833
         _ExtentY        =   9208
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0154.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txttext1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txttext1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txttext1(13)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txttext1(12)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txttext1(11)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txttext1(10)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lbllabel1(12)"
         Tab(0).Control(7)=   "lbllabel1(11)"
         Tab(0).Control(8)=   "lbllabel1(10)"
         Tab(0).Control(9)=   "lbllabel1(9)"
         Tab(0).ControlCount=   10
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0154.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR44NUMCUENTA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   -67560
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   360
            Visible         =   0   'False
            Width           =   372
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   -66960
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   360
            Visible         =   0   'False
            Width           =   372
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   13
            Left            =   -71880
            MultiLine       =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n|Descripci�n"
            Top             =   1410
            Width           =   5400
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   12
            Left            =   -74160
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "C�digo Actuaci�n|C�digo"
            Top             =   1410
            Width           =   1092
         End
         Begin VB.TextBox txttext1 
            BackColor       =   &H00808080&
            Height          =   330
            Index           =   11
            Left            =   -71880
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Descripci�n Dpto. Peticionario"
            Top             =   2520
            Width           =   5400
         End
         Begin VB.TextBox txttext1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "AD02CODDPTO_REA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   10
            Left            =   -74160
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "C�d.Dpto. Realizador"
            Top             =   2520
            Width           =   372
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4785
            Index           =   0
            Left            =   120
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   240
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   3
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   8440
            _StockProps     =   79
            Caption         =   "ACTUACIONES PRINCIPALES"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Descripci�n Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   -71880
            TabIndex        =   17
            Top             =   1200
            Width           =   1935
         End
         Begin VB.Label lbllabel1 
            Caption         =   "C�digo Actuaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   -74160
            TabIndex        =   16
            Top             =   1200
            Width           =   1575
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Descripci�n Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   -71880
            TabIndex        =   15
            Top             =   2280
            Width           =   2415
         End
         Begin VB.Label lbllabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dpto. Realizador"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   -74160
            TabIndex        =   14
            Top             =   2280
            Width           =   1440
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   10
      Top             =   8055
      Width           =   10680
      _ExtentX        =   18838
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSelPrincipales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR001.FRM                                                    *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 8 DE OCTUBRE DE 1997                                          *
'* DESCRIPCI�N: Seleccionar actuaciones por principales                 *
'* ARGUMENTOS:  PR01CODACTUACION, PR01DESCORTA, PR05NUMFASE,            *
'*              PR05DESFASE (por valor)                                 *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gblnselec As Boolean

Private Sub Comprobar_Repetidas(ByVal lngcodact As Long, ByVal lngcoddpto As Long)
    Dim mintI As Integer
    Dim strrespuesta As String
    Dim cont As Integer
    
    frmSeleccionarAct.grdDBGrid1(0).MoveFirst
    cont = 0
    Do While cont < frmSeleccionarAct.grdDBGrid1(0).Rows
        If (lngcodact = frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value) And _
           (lngcoddpto = frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value) Then
           strrespuesta = MsgBox("La actuaci�n '" _
                        & frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value & "'" _
                        & " del Departamento '" & _
                        frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value & "'" _
                        & Chr(13) & " ya ha sido seleccionada." & Chr(13) & Chr(13) & _
                        "                      �Desea seleccionarla?", vbYesNo, "SELECCION DE ACTUACIONES")
            If strrespuesta <> vbYes Then
                gblnselec = False
            End If
           Exit Do
        End If
        If cont = frmSeleccionarAct.grdDBGrid1(0).Rows - 1 Then
            Exit Do
        Else
            frmSeleccionarAct.grdDBGrid1(0).MoveNext
            cont = cont + 1
        End If
    Loop
   frmSeleccionarAct.grdDBGrid1(0).MoveFirst
End Sub
Private Sub seleccionar_actuacionsel(ByVal codact As Long, ByVal codpadre As Long, ByVal mvarBkmrk As Variant, ByVal coddpto As Long)
    Dim rsta As rdoResultset
    Dim sqlstr As String
    Dim rstAcod As rdoResultset
    Dim sqlstrcod As String
    Dim rstAcoddpto As rdoResultset
    Dim sqlstrcoddpto As String
    Dim muactaso As Long
    Dim mudptoaso As Long
    
    Call Comprobar_Repetidas(codact, coddpto)
    If gblnselec = True Then
        sqlstrcod = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & codact
        Set rstAcod = objApp.rdoConnect.OpenResultset(sqlstrcod)
        
        sqlstrcoddpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO=" & coddpto
        Set rstAcoddpto = objApp.rdoConnect.OpenResultset(sqlstrcoddpto)
        
        frmSeleccionarAct.grdDBGrid1(0).AddNew
        frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value = codact
        frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value = rstAcod.rdoColumns(0).Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value = grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk)
        frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value = rstAcoddpto.rdoColumns("AD02DESDPTO").Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(0).Value = True
        frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = False
        glngSelCond = glngSelCond + 1
        frmSeleccionarAct.grdDBGrid1(0).Columns(9).Value = codpadre
        frmSeleccionarAct.grdDBGrid1(0).Update
        
        sqlstr = "SELECT * FROM PR3100 WHERE PR01CODACTUACION=" & codact
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        With rsta
            Do Until rsta.EOF
                muactaso = .rdoColumns("PR01CODACTUACION_ASO")
                mudptoaso = .rdoColumns("AD02CODDPTO")
                Call seleccionar_actuacionsel(muactaso, codact, mvarBkmrk, mudptoaso)
            rsta.MoveNext
            Loop
       End With
       rsta.Close
       rstAcod.Close
       rstAcoddpto.Close
        Set rsta = Nothing
        Set rstAcod = Nothing
        Set rstAcoddpto = Nothing
    Else
        gblnselec = True
    End If
End Sub
Private Sub seleccionar_actuacion(ByVal codact As Long, ByVal codpadre As Long, ByVal coddpto As Long)
    Dim rsta As rdoResultset
    Dim sqlstr As String
    Dim rstAcod As rdoResultset
    Dim sqlstrcod As String
    Dim rstAcoddpto As rdoResultset
    Dim sqlstrcodcond As String
    Dim rstAcodcond As rdoResultset
    Dim sqlstrcoddpto As String
    Dim muactaso As Long
    Dim mudptoaso As Long
    
    Call Comprobar_Repetidas(codact, coddpto)
    If gblnselec = True Then
        sqlstrcod = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & codact
        Set rstAcod = objApp.rdoConnect.OpenResultset(sqlstrcod)
        
        sqlstrcoddpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO=" & coddpto
        Set rstAcoddpto = objApp.rdoConnect.OpenResultset(sqlstrcoddpto)
        
        frmSeleccionarAct.grdDBGrid1(0).AddNew
        frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value = codact
        frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value = rstAcod.rdoColumns("PR01DESCORTA").Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value = coddpto
        frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value = rstAcoddpto.rdoColumns("AD02DESDPTO").Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(0).Value = True
        frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = False
        sqlstrcodcond = "SELECT * FROM PR2300 WHERE PR01CODACTUACION=" & codact
        Set rstAcodcond = objApp.rdoConnect.OpenResultset(sqlstrcodcond)
        
        If rstAcodcond.EOF = False Then
            glngSelCond = glngSelCond + 1
        End If
        
        frmSeleccionarAct.grdDBGrid1(0).Columns(9).Value = codpadre
        frmSeleccionarAct.grdDBGrid1(0).Update
        sqlstr = "SELECT * FROM PR3100 WHERE PR01CODACTUACION=" & codact
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        With rsta
            Do Until rsta.EOF
                muactaso = .rdoColumns("PR01CODACTUACION_ASO")
                mudptoaso = .rdoColumns("AD02CODDPTO")
                Call seleccionar_actuacion(muactaso, codact, mudptoaso)
            rsta.MoveNext
            Loop
       End With
       rsta.Close
       rstAcod.Close
       rstAcoddpto.Close
       rstAcodcond.Close
        Set rsta = Nothing
        Set rstAcod = Nothing
        Set rstAcoddpto = Nothing
        Set rstAcodcond = Nothing
    Else
        gblnselec = True
    End If
End Sub
Private Sub cmdAceptarPrinc_Click(Index As Integer)
    Dim mintisel As Integer
    Dim mintNTotalSelRows As Integer
    Dim mvarBkmrk As Variant
    
    If txtText1(0).Text <> "" Then
        gblnselec = True
        If tabTab1(0).Tab = 0 Then
            Call seleccionar_actuacion(txtText1(12).Text, 0, txtText1(10).Text)
        Else
        
            'Guardamos el n�mero de filas seleccionadas
            mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
            'minti = 1
            For mintisel = 0 To mintNTotalSelRows - 1
                'Guardamos el n�mero de fila que est� seleccionada
                mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
                Call seleccionar_actuacionsel(grdDBGrid1(0).Columns(1).CellValue(mvarBkmrk), 0, mvarBkmrk, grdDBGrid1(0).Columns(3).CellValue(mvarBkmrk))
            Next mintisel
        End If
        If glngSelCond = 0 Then
            frmSeleccionarAct.cmdpeticion.Enabled = True
        Else
            frmSeleccionarAct.cmdpeticion.Enabled = False
        End If
    End If
    Unload Me
End Sub

Private Sub cmdDescrAct_Click(Index As Integer)

Dim sqlstrdesl As String
Dim rstdesl As rdoResultset
Dim codact As Long

'JRC 28/4/98
If grdDBGrid1(0).Rows > 0 Then
  codact = grdDBGrid1(0).Columns(1).Value
  If IsNull(codact) = False Then
    sqlstrdesl = "SELECT PR01DESCOMPLETA FROM PR0100 WHERE PR01CODACTUACION=" & codact
    Set rstdesl = objApp.rdoConnect.OpenResultset(sqlstrdesl)
    If Not rstdesl.EOF Then
        If IsNull(rstdesl.rdoColumns("PR01DESCOMPLETA").Value) = False Then
            Call MsgBox("Descripci�n completa actuaci�n " & codact & " : " & Chr(13) & rstdesl.rdoColumns("PR01DESCOMPLETA").Value, vbInformation)
        Else
            Call MsgBox("La actuaci�n " & codact & " no tiene descripci�n completa.", vbExclamation)
        End If
    End If
    rstdesl.Close
    Set rstdesl = Nothing
  Else
    Call MsgBox("No hay ninguna actuaci�n seleccionada.", vbExclamation)
  End If
Else
    Call MsgBox("No hay ninguna actuaci�n.", vbExclamation)
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------


Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
 
   With objDetailInfo
    .strName = "Actuaciones mas solicitadas"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR4400"
    .strWhere = "AD02CODDPTO=" & frmpedir.txtText1(1).Text & " AND " & _
            "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion))) AND " _
            & "pr01codactuacion not in (select pr01codactuacion from PR0100 " _
            & " where pr01fecfin < (select sysdate from dual))"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("PR44NUMCUENTA", cwDescending)
    Call .FormAddOrderField("PR01CODACTUACION", cwAscending)
    Call .FormAddOrderField("AD02CODDPTO_REA", cwAscending)
    
    'Call .objPrinter.Add("PR001241", "Listado de Tipos de Recurso y Recurso para cada Fase")
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Actuaciones mas solicitadas")
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "Actuaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD02CODDPTO_REA", "Dpto Realizador", cwNumeric)
    'Call .FormAddFilterWhere(strKey2, "PR13NUMUNIREC", "Unidades", cwNumeric)
    'Call .FormAddFilterWhere(strKey2, "PR13NUMTIEMPREC", "Tiempo", cwNumeric)
    'Call .FormAddFilterWhere(strKey2, "PR13NUMMINDESF", "M�nimo Desfase", cwNumeric)
    'Call .FormAddFilterWhere(strKey2, "PR13INDPREFEREN", "�Preferente?", cwBoolean)
    'Call .FormAddFilterWhere(strKey2, "PR13INDPLANIF", "�Planificable?", cwBoolean)
    
    
    Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "AD02CODDPTO_REA", "Dpto Realizador")
    'Call .FormAddFilterOrder(strKey,"PR13NUMUNIREC", "Unidades")
    'Call .FormAddFilterOrder(strKey,"PR13NUMTIEMPREC", "Tiempo")
    'Call .FormAddFilterOrder(strKey,"PR13NUMMINDESF", "M�nimo Desfase")
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo)
    
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    .CtrlGetInfo(txtText1(1)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    '.CtrlGetInfo(txttext1(2)).blnInFind = True
    '.CtrlGetInfo(txttext1(5)).blnInFind = True
    '.CtrlGetInfo(txttext1(4)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(1)).blnInFind = True
    '.CtrlGetInfo(chkCheck1(2)).blnInFind = True
    
    .CtrlGetInfo(txtText1(12)).blnMandatory = True
    .CtrlGetInfo(txtText1(10)).blnMandatory = True
     
    '.CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT AD02CODDPTO,AD02DESDPTO" & _
    '" FROM AD0200 order by AD02CODDPTO asc"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(12)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(12)), txtText1(13), "PR01DESCORTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(10)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(10)), txtText1(11), "AD02DESDPTO")
    
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
  'si al cargarse la pantalla no hay ning�n tipo de recurso el bot�n Recursos se deshabilita
  'If txtdeptext1(0).Text = "" Then
  '  cmdrecursos.Enabled = False
  'End If

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
   ' Dim sqlstr1 As String
   ' Dim rstA1 As rdoResultset
   ' mira si la actuaci�n tiene alg�n departamento realizador
   '     sqlstr1 = "SELECT * " _
   '        & "FROM PR0200 " _
   '        & "WHERE pr01codactuacion = " & frmdefactuacionesCUN.txttext1(0)
   '    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
   '    If rstA1.EOF Then
   '         frmdefactuacionesCUN.cmdinteracciones.Enabled = False
   '    Else
   '         frmdefactuacionesCUN.cmdinteracciones.Enabled = True
   '    End If
   '    rstA1.close
   '    Set rstA1 = Nothing
  'intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub lblactLabel1_Click(Index As Integer)
'txtdeptext1(0).SetFocus
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
'If intNewStatus = cwModeSingleAddKey Then
' se deshabilita el bot�n <Recursos>
'      cmdrecursos.Enabled = False
'End If
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
 txtText1(0).Text = txtdeptext1(0).Text
   'txtdeptext1(1).Text = txtactText1(2).Text
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
   'deshabilitar el bot�n Recursos si se pulsa Abrir
    'Dim sqlstring As String
    'Dim rstA As rdoResultset
    'If (txtdeptext1(0).Text <> "" And intabrir = 1) Then
    'intabrir = 0
        ' cu�ntos tipos de recurso
    '    sqlstring = "select count(*) from PR1300 " & _
    '         "where PR05NUMFASE=" & frmdeffases.txttext1(3).Text & _
    '         " and PR01CODACTUACION=" & frmdefactuacionesCUN.txttext1(0).Text & _
    '         " and PR13NUMNECESID=" & txtdeptext1(0).Text
    '    Set rstA = objApp.rdoConnect.OpenResultset(sqlstring)
    '    If rstA.rdoColumns(0).Value = 1 Then
    '        cmdrecursos.Enabled = True
    '    End If
    '    rstA.close
    '    Set rstA = Nothing
    'End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
     ' cmdrecursos.Enabled = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Actuaciones mas solicitadas" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  'Dim sqlstr As String
  'Dim rstA As rdoResultset
  'Dim sqlstr1 As String
  'Dim rstA1 As rdoResultset
  
  ' controla que el Tipo de Recurso nuevo existe antes de hacer Guardar
   'If (btnButton.Index = 4) Then
   '   If (txttext1(6).Text <> "") Then
   '     sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
   '          " where AG14CODTIPRECU=" & txttext1(6).Text
   '     Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
   '     If (rstA.rdoColumns(0).Value = 0) Then
   '       strmensaje = MsgBox("El Tipo de Recurso " & txttext1(6) & " no existe. " _
   '        & "Elija otro, por favor.", vbCritical, "Tipos de Recurso")
   '       txttext1(6).SetFocus
   '       objWinInfo.objWinActiveForm.blnChanged = False
   '       rstA.close
   '       Set rstA = Nothing
   '       Exit Sub
    '    Else
    '    cmdrecursos.Enabled = True
    '   End If
    '  End If
  'End If
   ' controla que el nuevo tipo de recurso existe antes de pulsar un bot�n
  ' distinto de imprimir,borrar,anterior,siguiente,guardar.
  'If (btnButton.Index <> 6 And btnButton.Index <> 8 And btnButton.Index <> 4) Then ' And
       'btnButton.Index <> 23 And btnButton.Index <> 23) Then
  '     If (txttext1(6).Text <> "") Then
  '     sqlstr = "select count(AG14CODTIPRECU) from AG1400" & _
  '           " where AG14CODTIPRECU=" & txttext1(6).Text
  '     Set rstA = objApp.rdoConnect.OpenResultset(sqlstr)
  '     If (rstA.rdoColumns(0).Value = 0) Then
  '     objWinInfo.objWinActiveForm.blnChanged = False
  '     End If
  '     End If
  'End If
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
  'If btnButton.Index = 2 Then
  '    On Error GoTo Err_Ejecutar
  '    ' generaci�n autom�tica del c�digo
  '    sqlstr1 = "SELECT PR1300_SEQ.nextval FROM dual"
  '    Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
  '    txtdeptext1(0) = rstA1.rdoColumns(0).Value
  '    txtdeptext1(0).Locked = True
  '    txttext1(6).SetFocus
  'End If
  'objWinInfo.objWinActiveForm.blnChanged = False
  'If (txtdeptext1(0).Text = "" Or txttext1(6).Text = "") Then
  '  cmdrecursos = False
  'Else
  '  cmdrecursos.Enabled = True
  'End If
  '' Nuevo
  'If btnButton.Index = 2 Then
     ' se deshabilita el bot�n <Recursos>
  '    cmdrecursos.Enabled = False
  'End If
  'Abrir Registro
  'If btnButton.Index = 3 Then
     ' se deshabilita el bot�n <Recursos>
  '    cmdrecursos.Enabled = False
  '    intabrir = 1
  'End If
  'rstA.close
  'Set rstA = Nothing
  'rstA1.close
  'Set rstA1 = Nothing
 'Exit Sub
 
'Err_Ejecutar:
'  MsgBox "Error: " & Err.Number & " " & Err.Description
 ' Exit Sub
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
   Call objWinInfo.CtrlDataChange
End Sub




Private Sub txtactText1_GotFocus(Index As Integer)
txtText1(12).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Call objWinInfo.CtrlDataChange
End Sub

