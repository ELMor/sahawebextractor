VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmSelDptosP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Selecci�n de Actuaciones. Protocolos"
   ClientHeight    =   8220
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0157.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8220
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdDescrAct 
      Caption         =   "Descripci�n Actuaci�n"
      Height          =   375
      Index           =   1
      Left            =   7200
      TabIndex        =   12
      Top             =   7560
      Width           =   2175
   End
   Begin VB.CommandButton cmdAceptardpto 
      Caption         =   "A�adir selecci�n"
      Height          =   375
      Index           =   0
      Left            =   2640
      TabIndex        =   0
      Top             =   7560
      Width           =   1935
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones del Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6975
      Index           =   2
      Left            =   5760
      TabIndex        =   8
      Top             =   480
      Width           =   6015
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6495
         Index           =   1
         Left            =   120
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   360
         Width           =   5730
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   10107
         _ExtentY        =   11456
         _StockProps     =   79
         Caption         =   "ACTUACIONES DEL DEPARTAMENTO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Departamento"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6975
      HelpContextID   =   2
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   5415
      Begin TabDlg.SSTab tabTab1 
         Height          =   6495
         HelpContextID   =   90001
         Index           =   1
         Left            =   120
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   360
         Width           =   5130
         _ExtentX        =   9049
         _ExtentY        =   11456
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0157.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0157.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   -74760
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "C�digo|C�digo Departamento"
            Top             =   690
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02DESDPTO"
            Height          =   330
            Index           =   1
            Left            =   -74760
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Descripci�n|Descripci�n Departamento"
            Top             =   1410
            Width           =   4800
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5850
            Index           =   2
            Left            =   120
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   450
            Width           =   4935
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   8705
            _ExtentY        =   10319
            _StockProps     =   79
            Caption         =   "DEPARTAMENTOS"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   -74760
            TabIndex        =   11
            Top             =   1170
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   -74760
            TabIndex        =   10
            Top             =   450
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   4
      Top             =   7935
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSelDptosP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim gblnselec As Boolean

Private Sub Comprobar_Repetidas(ByVal lngcodact As Long, ByVal lngcoddpto As Long)
    Dim mintI As Integer
    Dim strrespuesta As String
    Dim cont As Integer
    
    
    
    frmSeleccionarAct.grdDBGrid1(0).MoveFirst
    cont = 0
    Do While cont < frmSeleccionarAct.grdDBGrid1(0).Rows
        If (lngcodact = frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value) And _
           (lngcoddpto = frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value) Then
           strrespuesta = MsgBox("La actuaci�n '" _
                        & frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value & "'" _
                        & " del Departamento '" & _
                        frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value & "'" _
                        & Chr(13) & " ya ha sido seleccionada." & Chr(13) & Chr(13) & _
                        "                      �Desea seleccionarla?", vbYesNo, "SELECCION DE ACTUACIONES")
            If strrespuesta <> vbYes Then
                gblnselec = False
            End If
           Exit Do
        End If
        If cont = frmSeleccionarAct.grdDBGrid1(0).Rows - 1 Then
            Exit Do
        Else
            frmSeleccionarAct.grdDBGrid1(0).MoveNext
            cont = cont + 1
        End If
    Loop
   frmSeleccionarAct.grdDBGrid1(0).MoveFirst
End Sub
Private Sub seleccionar_actuacion(ByVal codact As Long, ByVal codpadre As Long, ByVal mvarBkmrk As Variant, ByVal coddpto As Long)
    Dim rsta As rdoResultset
    Dim sqlstr As String
    Dim rstAcod As rdoResultset
    Dim sqlstrcod As String
    Dim rstAcoddpto As rdoResultset
    Dim sqlstrcodcond As String
    Dim rstAcodcond As rdoResultset
    Dim sqlstrcoddpto As String
    Dim muactaso As Long
    Dim mudptoaso As Long
    
    Call Comprobar_Repetidas(codact, coddpto)
    If gblnselec = True Then
        sqlstrcod = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION=" & codact
        Set rstAcod = objApp.rdoConnect.OpenResultset(sqlstrcod)
        
        sqlstrcoddpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO=" & coddpto
        Set rstAcoddpto = objApp.rdoConnect.OpenResultset(sqlstrcoddpto)
        
        frmSeleccionarAct.grdDBGrid1(0).AddNew
        frmSeleccionarAct.grdDBGrid1(0).Columns(2).Value = codact
        frmSeleccionarAct.grdDBGrid1(0).Columns(3).Value = rstAcod.rdoColumns(0).Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(4).Value = coddpto
        frmSeleccionarAct.grdDBGrid1(0).Columns(5).Value = rstAcoddpto.rdoColumns("AD02DESDPTO").Value
        frmSeleccionarAct.grdDBGrid1(0).Columns(0).Value = True
        frmSeleccionarAct.grdDBGrid1(0).Columns(1).Value = False
        sqlstrcodcond = "SELECT * FROM PR2300 WHERE PR01CODACTUACION=" & codact
        Set rstAcodcond = objApp.rdoConnect.OpenResultset(sqlstrcodcond)
        
        If rstAcodcond.EOF = False Then
            glngSelCond = glngSelCond + 1
        End If
        
        frmSeleccionarAct.grdDBGrid1(0).Columns(9).Value = codpadre
        frmSeleccionarAct.grdDBGrid1(0).Update
        
        sqlstr = "SELECT * FROM PR3100 WHERE PR01CODACTUACION=" & codact
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        With rsta
            Do Until rsta.EOF
                muactaso = .rdoColumns("PR01CODACTUACION_ASO")
                mudptoaso = .rdoColumns("AD02CODDPTO")
                Call seleccionar_actuacion(muactaso, codact, mvarBkmrk, mudptoaso)
            rsta.MoveNext
            Loop
        End With
        rsta.Close
        Set rsta = Nothing
        rstAcod.Close
        Set rstAcod = Nothing
        rstAcoddpto.Close
        Set rstAcoddpto = Nothing
        rstAcodcond.Close
        Set rstAcodcond = Nothing
    Else
        gblnselec = True
    End If

End Sub
Private Sub cmdAceptardpto_Click(Index As Integer)
    Dim mintisel As Integer
    Dim mintNTotalSelRows As Integer
    Dim mvarBkmrk As Variant
    
        'Guardamos el n�mero de filas seleccionadas
        mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
        'minti = 1
        gblnselec = True
        For mintisel = 0 To mintNTotalSelRows - 1
            'Guardamos el n�mero de fila que est� seleccionada
            mvarBkmrk = grdDBGrid1(1).SelBookmarks(mintisel)
            Call seleccionar_actuacion(grdDBGrid1(1).Columns(3).CellValue(mvarBkmrk), 0, mvarBkmrk, grdDBGrid1(1).Columns(5).CellValue(mvarBkmrk))
        Next mintisel
        If glngSelCond = 0 Then
            frmSeleccionarAct.cmdpeticion.Enabled = True
        Else
            frmSeleccionarAct.cmdpeticion.Enabled = False
        End If
        grdDBGrid1(1).SelBookmarks.RemoveAll
    'Unload Me
End Sub

Private Sub cmdDescrAct_Click(Index As Integer)

Dim sqlstrdesl As String
Dim rstdesl As rdoResultset
Dim codact As Long

'JRC 28/4/98
If grdDBGrid1(1).Rows > 0 Then
  codact = grdDBGrid1(1).Columns(3).Value
  If IsNull(codact) = False Then
    sqlstrdesl = "SELECT PR01DESCOMPLETA FROM PR0100 WHERE PR01CODACTUACION=" & codact
    Set rstdesl = objApp.rdoConnect.OpenResultset(sqlstrdesl)
    If Not rstdesl.EOF Then
        If IsNull(rstdesl.rdoColumns("PR01DESCOMPLETA").Value) = False Then
            Call MsgBox("Descripci�n completa actuaci�n " & codact & " : " & Chr(13) & rstdesl.rdoColumns("PR01DESCOMPLETA").Value, vbInformation)
        Else
            Call MsgBox("La actuaci�n " & codact & " no tiene descripci�n completa.", vbExclamation)
        End If
    End If
    rstdesl.Close
    Set rstdesl = Nothing
  Else
    Call MsgBox("No hay ninguna actuaci�n seleccionada.", vbExclamation)
  End If
Else
    Call MsgBox("No hay ninguna actuaci�n.", vbExclamation)
End If

End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    
    Dim strKey As String
    
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
    With objMasterInfo
        .strName = "Departamentos"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(1)
        Set .grdGrid = grdDBGrid1(2)
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "AD0200"
        .intAllowance = cwAllowReadOnly
        .strWhere = "AD02INDCAMA=-1 OR AD02INDRESPONPROC=-1"
        
        strKey = .strDataBase & .strTable
                           
        Call .FormAddOrderField("AD02CODDPTO", cwAscending)
        
        'Call .objPrinter.Add("PR001051", "Listado de protocolos")
        'Call .objPrinter.Add("PR001052", "Listado de actuaciones de un protocolo")
        
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Departamentos")
        Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "AD02DESDPTO", "Descripci�n", cwString)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo")
        Call .FormAddFilterOrder(strKey, "AD02DESDPTO", "Descripci�n")
  
    End With
  
  
    With objMultiInfo
        .strName = "Actuaciones del departamento"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = fraframe1(0)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .intAllowance = cwAllowReadOnly
        
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR0201J"
        ' s�lo las actuaciones con alguna fase (y la fase con al menos un tipo de recurso)
        .strWhere = "(pr01codactuacion in (select pr01codactuacion from PR0500 " _
            & "where pr05numfase in (select pr05numfase from PR1300 " _
            & "where PR0500.pr05numfase=PR1300.pr05numfase AND " _
            & "PR0500.pr01codactuacion=PR1300.pr01codactuacion))) AND " _
            & "pr01codactuacion  not in (select pr01codactuacion from PR0100 " _
            & " where pr01fecfin < (select sysdate from dual))"
        .intCursorSize = 0
        Call .FormAddOrderField("PR01DESCORTA", cwAscending)
        
        Call .FormAddRelation("AD02CODDPTO", txtText1(0))
 
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Actuaciones del Departamento")
        'Call .FormAddFilterWhere(strKey, "AD02CODDPTO", "C�digo Departamento", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Descripci�n Actuaci�n", cwString)
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        'Call .FormAddFilterOrder(strKey, "AD02CODDPTO", "C�digo Departamento")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
        Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Descripci�n Actuaci�n")
    End With

    With objWinInfo

        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del protocolo
        Call .GridAddColumn(objMultiInfo, "C�d. Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Actuaci�n", "PR01DESCORTA", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d.Dpto.", "AD02CODDPTO", cwNumeric, 3)
        'Call .GridAddColumn(objMultiInfo, "Departamento", "", cwString, 30)
    
    
        Call .FormCreateInfo(objMasterInfo)
    
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True

        'Se indica que campos son obligatorios y cuales son clave primaria
        'en el grid que contiene las actuaciones del protocolo
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
 
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True

        'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
        'contiene las actuaciones del protocolo
        'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "AD02DESDPTO")

        'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
        'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(3)), grdDBGrid1(1).Columns(4), "PR01DESCORTA")
    

        Call .WinRegister
        Call .WinStabilize
    End With

    grdDBGrid1(1).Columns(3).Width = 7 * 200
    grdDBGrid1(1).Columns(5).Visible = False

    'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


