VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmDefUrgencias 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Mantenimiento de Urgencias"
   ClientHeight    =   7065
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11655
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0151.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7065
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Urgencias"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6015
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Width           =   11085
      Begin TabDlg.SSTab tabTab1 
         Height          =   5460
         Index           =   0
         Left            =   240
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   360
         Width           =   10695
         _ExtentX        =   18865
         _ExtentY        =   9631
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0151.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "txtText1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0151.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR48CODURGENCIA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   -74640
            TabIndex        =   0
            Tag             =   "C�digo Urgencia"
            Top             =   1065
            Width           =   300
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR48DESURGENCIA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   -73800
            TabIndex        =   1
            Tag             =   "Urgencia"
            Top             =   1080
            Width           =   5400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5025
            Index           =   0
            Left            =   120
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   240
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   8864
            _StockProps     =   79
            Caption         =   "URGENCIAS"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   -74640
            TabIndex        =   7
            Top             =   840
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Urgencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   -73800
            TabIndex        =   6
            Top             =   840
            Width           =   780
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   6780
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefUrgencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Urgencias"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR4800"
    
    Call .FormAddOrderField("PR48CODURGENCIA", cwAscending)
    Call .objPrinter.Add("PR1511", "Listado de Tipos de Urgencias")
    
    .blnHasMaint = True
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Urgencias")
    Call .FormAddFilterWhere(strKey, "PR48CODURGENCIA", "C�d. Urgencia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR48DESURGENCIA", "Urgencia", cwString)
    
    Call .FormAddFilterOrder(strKey, "PR48CODURGENCIA", "C�d. Urgencia")
    Call .FormAddFilterOrder(strKey, "PR48DESURGENCIA", "Urgencia")
    
  End With
   
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
  
    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Urgencias" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


