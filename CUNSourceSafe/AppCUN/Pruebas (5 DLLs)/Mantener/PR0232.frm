VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmTerminarPrueba 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Terminar Actuaci�n"
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0232.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   240
      TabIndex        =   5
      Top             =   480
      Width           =   11175
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   6
         Left            =   6600
         Locked          =   -1  'True
         TabIndex        =   32
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   1800
         Width           =   3600
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   5
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   31
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   1800
         Width           =   3600
      End
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   3
         Left            =   1080
         TabIndex        =   28
         TabStop         =   0   'False
         Tag             =   "Actuaci�n Planificada"
         Top             =   1200
         Width           =   1092
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   2
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   27
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   1200
         Width           =   3600
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30104
         Index           =   1
         Left            =   2520
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   600
         Width           =   5400
      End
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   1080
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Actuaci�n Planificada"
         Top             =   600
         Width           =   1092
      End
      Begin VB.TextBox txtdptoText1 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   1
         Left            =   8160
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "Nombre Departamento Realizador"
         Top             =   1080
         Visible         =   0   'False
         Width           =   5400
      End
      Begin VB.TextBox txtdptoText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   30101
         Index           =   0
         Left            =   8280
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "C�digo del Departamento"
         Top             =   600
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Apellido 2� "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   6600
         TabIndex        =   34
         Top             =   1560
         Width           =   990
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Apellido 1�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   2520
         TabIndex        =   33
         Top             =   1560
         Width           =   930
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Nombre Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   2520
         TabIndex        =   30
         Top             =   960
         Width           =   1470
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "N�Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   1080
         TabIndex        =   29
         Top             =   960
         Width           =   870
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Actuaci�n "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   1080
         TabIndex        =   11
         Top             =   360
         Width           =   930
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Departamento Realizador"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   8280
         TabIndex        =   10
         Top             =   360
         Visible         =   0   'False
         Width           =   2160
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Recursos Consumidos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5055
      Index           =   0
      Left            =   240
      TabIndex        =   2
      Top             =   2880
      Width           =   11100
      Begin TabDlg.SSTab tabTab1 
         Height          =   4620
         Index           =   0
         Left            =   240
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   360
         Width           =   10695
         _ExtentX        =   18865
         _ExtentY        =   8149
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0232.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblactLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblactLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblactLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(14)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(12)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(10)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(11)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(13)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(15)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(16)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(17)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(6)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(12)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(13)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(1)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(4)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(2)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(3)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(5)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(0)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(7)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "chkCheck1(0)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "chkCheck1(1)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(8)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(9)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(10)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(11)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).ControlCount=   31
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0232.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   11
            Left            =   4680
            TabIndex        =   48
            Tag             =   "Descripci�n Recurso"
            Top             =   3960
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   10
            Left            =   4680
            TabIndex        =   47
            Tag             =   "Descripci�n Recurso"
            Top             =   3240
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD31CODPUESTO"
            Height          =   330
            Index           =   9
            Left            =   3240
            TabIndex        =   44
            Tag             =   "C�d Puesto"
            Top             =   3960
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD30CODCATEGORIA"
            Height          =   330
            Index           =   8
            Left            =   3240
            TabIndex        =   43
            Tag             =   "C�d Categoria"
            Top             =   3240
            Width           =   372
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Firmado Informe?"
            DataField       =   "PR10INDFIRMA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   600
            TabIndex        =   42
            Tag             =   "�Firmado Informe?"
            Top             =   3960
            Width           =   2235
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�D�ctor Responsable?"
            DataField       =   "PR10INDDOCTRESP"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   600
            TabIndex        =   41
            Tag             =   "�D�ctor Responsable?"
            Top             =   3240
            Width           =   2595
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            Height          =   330
            Index           =   7
            Left            =   600
            TabIndex        =   39
            Tag             =   "C�digo Recurso"
            Top             =   1080
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   0
            Left            =   2400
            TabIndex        =   37
            Tag             =   "Descripci�n Recurso"
            Top             =   1080
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR04NUMACTPLAN"
            Height          =   330
            HelpContextID   =   30101
            Index           =   5
            Left            =   8160
            TabIndex        =   35
            TabStop         =   0   'False
            Tag             =   "Actuaci�n Planificada"
            Top             =   120
            Visible         =   0   'False
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR10NUMMINOCUREC"
            Height          =   330
            Index           =   3
            Left            =   4080
            TabIndex        =   23
            Tag             =   "Tiempo Ocupaci�n"
            Top             =   2520
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR10NUMUNIREC"
            Height          =   330
            Index           =   2
            Left            =   600
            TabIndex        =   22
            Tag             =   "N� de unidades"
            Top             =   2520
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            Index           =   4
            Left            =   600
            TabIndex        =   19
            Tag             =   "C�digo Recurso"
            Top             =   1800
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   1
            Left            =   2400
            TabIndex        =   18
            Tag             =   "Descripci�n Recurso"
            Top             =   1800
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   13
            Left            =   2400
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "Descripci�n de la Fase"
            Top             =   360
            Width           =   5040
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR07NUMFASE"
            Height          =   330
            Index           =   12
            Left            =   600
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "C�digo de la Fase"
            Top             =   360
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR10NUMNECESID"
            Height          =   330
            Index           =   6
            Left            =   6480
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "N� Necesidad"
            Top             =   120
            Visible         =   0   'False
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4395
            Index           =   0
            Left            =   -74760
            TabIndex        =   1
            TabStop         =   0   'False
            Top             =   120
            Width           =   9975
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17595
            _ExtentY        =   7752
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Puesto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   4680
            TabIndex        =   50
            Top             =   3720
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Categoria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   4680
            TabIndex        =   49
            Top             =   3000
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d Puesto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   3240
            TabIndex        =   46
            Top             =   3720
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d Categoria"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   3240
            TabIndex        =   45
            Top             =   3000
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d Tipo Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   11
            Left            =   600
            TabIndex        =   40
            Top             =   840
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo de Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   10
            Left            =   2400
            TabIndex        =   38
            Top             =   840
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   7320
            TabIndex        =   36
            Top             =   120
            Visible         =   0   'False
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   4800
            TabIndex        =   26
            Top             =   2640
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo de Ocupaci�n "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   4080
            TabIndex        =   25
            Top             =   2280
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Unidades Recurso Consumidas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   600
            TabIndex        =   24
            Top             =   2280
            Width           =   2775
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   600
            TabIndex        =   21
            Top             =   1560
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   2400
            TabIndex        =   20
            Top             =   1560
            Width           =   1815
         End
         Begin VB.Label lblactLabel1 
            Caption         =   "Descripci�n Fase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   2400
            TabIndex        =   17
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label lblactLabel1 
            Caption         =   "N�mero Fase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   600
            TabIndex        =   16
            Top             =   120
            Width           =   1695
         End
         Begin VB.Label lblactLabel1 
            Caption         =   "Necesidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   5400
            TabIndex        =   15
            Top             =   120
            Visible         =   0   'False
            Width           =   1095
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmTerminarPrueba"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Recursos Consumidos"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR1000"
    
    'Ordenamos por el c�digo del recurso
    Call .FormAddOrderField("PR10NUMNECESID", cwAscending)
    
    
    .blnHasMaint = True
    'Pasamos el n�mero de actuaci�n planificada
    .strWhere = "pr04numactplan=" & frmpruebasadicionales.txtText1(0).Text
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Recursos")
    Call .FormAddFilterWhere(strKey, "AG11CODRECURSO", "C�digo Recurso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AG11DESRECURSO", "Descripci�n Recurso", cwString)
    Call .FormAddFilterWhere(strKey, "PR10NUMUNIREC", "Unidades de Recurso", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR10NUMMINOCUREC", "Tiempo M�nimo Ocupaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD30CODCATEGORIA", "C�digo Categor�a", cwNumeric)
    Call .FormAddFilterWhere(strKey, "AD31CODPUESTO", "C�digo Puesto", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "AG11CODRECURSO", "C�digo Recurso")
    Call .FormAddFilterOrder(strKey, "AG11DESRECURSO", "Descripci�n Recurso")
    Call .FormAddFilterOrder(strKey, "PR10NUMUNIREC", "Unidades de Recurso")
    Call .FormAddFilterOrder(strKey, "PR10NUMMINOCUREC", "Tiempo M�nimo Ocupaci�n")
    Call .FormAddFilterOrder(strKey, "AD30CODCATEGORIA", "C�digo Categor�a")
    Call .FormAddFilterOrder(strKey, "AD31CODPUESTO", "C�digo Puesto")
  End With
  
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
        
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    
    '.CtrlGetInfo(txtText1(12)).blnForeign = True
    '.CtrlGetInfo(txtText1(6)).blnForeign = True
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(9)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(12)), "PR05NUMFASE", "SELECT PR05DESFASE FROM PR0500 WHERE PR05NUMFASE=? AND PR01CODACTUACION=" & frmpruebasadicionales.txtText1(28).Text)
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(12)), txtText1(13), "PR05DESFASE")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "AG11CODRECURSO", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(1), "AG11DESRECURSO")
  
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "AG11CODRECURSO", "SELECT AG11DESRECURSO FROM AG1100 WHERE AG11CODRECURSO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(1), "AG11DESRECURSO")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "PR14NUMNECESID", "SELECT AG14CODTIPRECU FROM PR1400 WHERE PR10NUMNECESID = ? AND PR03NUMACTPEDI=" & frmpruebasadicionales.txtText1(1).Text & " AND PR06NUMFASE=" & txtText1(12).Text)
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "AG14CODTIPRECU")
    'JRC 14-9-1998 linked clave varios campos
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "PR14NUMNECESID", "SELECT AG14CODTIPRECU FROM PR1400 WHERE PR14NUMNECESID =? AND PR03NUMACTPEDI=? AND PR06NUMFASE=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(7), "AG14CODTIPRECU")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "AG14CODTIPRECU", "SELECT AG14DESTIPRECU FROM AG1400 WHERE AG14CODTIPRECU=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(0), "AG14DESTIPRECU")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "AD30CODCATEGORIA", "SELECT AD30DESCATEGORIA FROM AD3000 WHERE AD30CODCATEGORIA =?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(10), "AD30DESCATEGORIA")

    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(9)), "AD31CODPUESTO", "SELECT AD31DESPUESTO FROM AD3100 WHERE AD31CODPUESTO=? AND AD30CODCATEGORIA=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(9)), txtText1(11), "AD31DESPUESTO")

    Call .WinRegister
    Call .WinStabilize
  End With

  objWinInfo.DataRefresh

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
'Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Empleados" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
'End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
    
    'para comprobar que se trata del control en cuestion
    If strFormName = "Recursos Consumidos" And strCtrlName = "txtText1(6)" Then
        aValues(2) = frmpruebasadicionales.txtText1(1)
        aValues(3) = txtText1(12)
    End If
    If strFormName = "Recursos Consumidos" And strCtrlName = "txtText1(9)" Then
        aValues(2) = txtText1(8)
    End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
    Dim objField As clsCWFieldSearch
    
    'If strFormName = "Recursos Consumidos" And strCtrl = "txttext1(12)" Then
    '    Set objSearch = New clsCWSearch
    '    With objSearch
    '        .strTable = "pr0700"
    '        .strWhere = "where PR04NUMACTPLAN=" & txtactText1(0).Text
    '        .strOrder = "ORDER BY PR07NUMFASE ASC"
    '
    '        Set objField = .AddField("PR07NUMFASE")
    '        objField.strSmallDesc = "PR07DESFASE"
    '
    '        If .Search Then
    '            Call MsgBox("Fase: " & .cllValues("PR07NUMFASE") & " - " & .cllValues("PR07DESFASE"))
    '        End If
    '    End With
    '    Set objSearch = Nothing
    'End If
    'If strFormName = "Recursos Consumidos" And strCtrl = "txttext1(6)" Then
    '    Set objSearch = New clsCWSearch
    '    With objSearch
    '        .strTable = "pr1100"
    '        .strWhere = "where PR01CODACTUACION=" & frmpruebasadicionales.txtText1(18).Text _
    '                    & "PR05NUMFASE=" & txtText1(12).Text
    '        .strOrder = "ORDER BY PR13NUMNECESID ASC"
    '
    '        Set objField = .AddField("PR13NUMNECESID")
    '
    '        If .Search Then
    '            Call MsgBox("Necesidad: " & .cllValues("PR13NUMNECESID"))
    '        End If
    '    End With
    '    Set objSearch = Nothing
    'End If
    If strFormName = "Recursos Consumidos" And strCtrl = "txtText1(4)" Then
        Set objSearch = New clsCWSearch
        With objSearch
            .strTable = "AG1100"
            '.strWhere = "where AG11CODRECURSO IN (SELECT AG11CODRECURSO FROM PR1100 WHERE PR01CODACTUACION=" & frmpruebasadicionales.txtText1(28).Text _
            '            & " AND PR05NUMFASE=" & txtText1(12).Text & " AND PR13NUMNECESID=" & txtText1(6).Text & ")"
            .strOrder = "ORDER BY AG11CODRECURSO ASC"
            
            Set objField = .AddField("AG11CODRECURSO")
            objField.strSmallDesc = "C�digo del Recurso"
         
            Set objField = .AddField("ag11desrecurso")
            objField.strSmallDesc = "Descripci�n del Recurso"
         
            If .Search Then
             'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(4)), .cllValues("ag11codrecurso"))
             Call objWinInfo.CtrlSet(txtText1(4), .cllValues("ag11codrecurso"))
            End If
        End With
        Set objSearch = Nothing
    End If
    If strFormName = "Recursos Consumidos" And strCtrl = "txtText1(8)" Then
        Set objSearch = New clsCWSearch
        With objSearch
            .strTable = "AD3000"
            '.strWhere = "where AG11CODRECURSO IN (SELECT AG11CODRECURSO FROM PR1100 WHERE PR01CODACTUACION=" & frmpruebasadicionales.txtText1(28).Text _
            '            & " AND PR05NUMFASE=" & txtText1(12).Text & " AND PR13NUMNECESID=" & txtText1(6).Text & ")"
            .strOrder = "ORDER BY AD30CODCATEGORIA ASC"
            
            Set objField = .AddField("AD30CODCATEGORIA")
            objField.strSmallDesc = "C�digo de Categor�a"
         
            Set objField = .AddField("AD30DESCATEGORIA")
            objField.strSmallDesc = "Descripci�n de Categor�a"
         
            If .Search Then
             'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(4)), .cllValues("ag11codrecurso"))
             Call objWinInfo.CtrlSet(txtText1(8), .cllValues("AD30CODCATEGORIA"))
            End If
        End With
        Set objSearch = Nothing
    End If
    If strFormName = "Recursos Consumidos" And strCtrl = "txtText1(9)" Then
        Set objSearch = New clsCWSearch
        With objSearch
            .strTable = "AD3100"
            '.strWhere = "where AG11CODRECURSO IN (SELECT AG11CODRECURSO FROM PR1100 WHERE PR01CODACTUACION=" & frmpruebasadicionales.txtText1(28).Text _
            '            & " AND PR05NUMFASE=" & txtText1(12).Text & " AND PR13NUMNECESID=" & txtText1(6).Text & ")"
            If txtText1(8).Text <> "" Then
                .strWhere = "WHERE AD30CODCATEGORIA=" & txtText1(8).Text
            End If
            .strOrder = "ORDER BY AD31CODPUESTO ASC"
            
            Set objField = .AddField("AD31CODPUESTO")
            objField.strSmallDesc = "C�digo de Puesto"
         
            Set objField = .AddField("AD31DESPUESTO")
            objField.strSmallDesc = "Descripci�n de Puesto"
         
            If .Search Then
             'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(4)), .cllValues("ag11codrecurso"))
             Call objWinInfo.CtrlSet(txtText1(9), .cllValues("AD31CODPUESTO"))
            End If
        End With
        Set objSearch = Nothing
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim sqlstr1 As String
  Dim rstA1 As rdoResultset
  Dim lngFase As Long
  
  lngFase = txtText1(12).Text
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
  If btnButton.Index = 2 Then
      'On Error GoTo Err_Ejecutar
      ' generaci�n autom�tica del c�digo
      sqlstr1 = "SELECT PR13NUMNECESID_SEQUENCE.nextval FROM dual"
      Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
      txtText1(6) = rstA1.rdoColumns(0).Value
      txtText1(6).Locked = True
      txtText1(12).Text = lngFase
      txtText1(12).Locked = True
      txtText1(5).Text = txtactText1(0).Text
      txtText1(5).Locked = True
      rstA1.Close
      Set rstA1 = Nothing
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


