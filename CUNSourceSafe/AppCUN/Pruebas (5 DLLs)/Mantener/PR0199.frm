VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmPlanifActuaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Petici�n de Actuaciones. Situaci�n de las Actuaciones Pedidas."
   ClientHeight    =   7065
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11640
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0199.frx":0000
   KeyPreview      =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7065
   ScaleWidth      =   11640
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11640
      _ExtentX        =   20532
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdPlanificar 
      Caption         =   "Planificar"
      Height          =   375
      Left            =   5040
      TabIndex        =   19
      Top             =   7560
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Datos del Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   11340
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   2
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "D.N.I."
         Top             =   720
         Width           =   2000
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   1
         Left            =   2040
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "N�mero de Historia"
         Top             =   720
         Width           =   852
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   5
         Left            =   7080
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Segundo Apellido del Paciente"
         Top             =   1440
         Width           =   3200
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   4
         Left            =   3720
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "Primer Apellido del Paciente"
         Top             =   1440
         Width           =   3200
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   3
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "Nombre del Paciente"
         Top             =   1440
         Width           =   3200
      End
      Begin VB.TextBox txtText1 
         Alignment       =   1  'Right Justify
         Height          =   330
         Index           =   0
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "C�digo del Paciente"
         Top             =   720
         Width           =   852
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 2�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   18
         Left            =   7080
         TabIndex        =   16
         Top             =   1200
         Width           =   1575
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Apellido 1�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   17
         Left            =   3720
         TabIndex        =   15
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   16
         Left            =   360
         TabIndex        =   14
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         Caption         =   "D.N.I"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   15
         Left            =   3720
         TabIndex        =   13
         Top             =   480
         Width           =   615
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   14
         Left            =   2040
         TabIndex        =   12
         Top             =   480
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         Caption         =   "C�digo Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   11
         Top             =   480
         Width           =   1695
      End
   End
   Begin VB.Frame fraFrame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Situaci�n de las Actuaciones Pedidas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   4920
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   2520
      Width           =   11415
      Begin VB.CheckBox chkCheck1 
         DataField       =   "PR01INDCONSFDO"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   9960
         TabIndex        =   17
         Tag             =   "Consentimiento Firmado|�Se necesita consentimiento firmado?"
         Top             =   240
         Width           =   255
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4185
         Index           =   0
         Left            =   240
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   600
         Width           =   10935
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   9
         stylesets.count =   1
         stylesets(0).Name=   "seleccionar"
         stylesets(0).Picture=   "PR0199.frx":000C
         AllowUpdate     =   0   'False
         SelectTypeRow   =   3
         RowNavigation   =   1
         CellNavigation  =   1
         StyleSet        =   "seleccionar"
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   9
         Columns(0).Width=   1667
         Columns(0).Caption=   "Planificada"
         Columns(0).Name =   "Planificada"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   11
         Columns(0).FieldLen=   256
         Columns(0).Style=   2
         Columns(1).Width=   2196
         Columns(1).Caption=   "C�d. Actuaci�n"
         Columns(1).Name =   "C�d. Actuaci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   3
         Columns(1).FieldLen=   256
         Columns(2).Width=   3969
         Columns(2).Caption=   "Actuaci�n"
         Columns(2).Name =   "Actuaci�n"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3519
         Columns(3).Caption=   "Dpto Realizador"
         Columns(3).Name =   "Dpto Realizador"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   2778
         Columns(4).Caption=   "Fecha Planificaci�n"
         Columns(4).Name =   "Fecha Planificaci�n"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   7
         Columns(4).FieldLen=   256
         Columns(5).Width=   1323
         Columns(5).Caption=   "Iterativa"
         Columns(5).Name =   "Iterativa"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   11
         Columns(5).FieldLen=   256
         Columns(5).Style=   2
         Columns(6).Width=   1138
         Columns(6).Caption=   "Citable"
         Columns(6).Name =   "Citable"
         Columns(6).DataField=   "Column 6"
         Columns(6).DataType=   11
         Columns(6).FieldLen=   256
         Columns(6).Style=   2
         Columns(7).Width=   1773
         Columns(7).Caption=   "Cuestionario"
         Columns(7).Name =   "Cuestionario"
         Columns(7).DataField=   "Column 7"
         Columns(7).DataType=   11
         Columns(7).FieldLen=   256
         Columns(7).Style=   2
         Columns(8).Width=   3200
         Columns(8).Visible=   0   'False
         Columns(8).Caption=   "Numero pedida"
         Columns(8).Name =   "Numero pedida"
         Columns(8).DataField=   "Column 8"
         Columns(8).DataType=   8
         Columns(8).FieldLen=   256
         _ExtentX        =   19288
         _ExtentY        =   7382
         _StockProps     =   79
         Caption         =   "ACTUACIONES PEDIDAS"
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Ver todas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   12
         Left            =   10200
         TabIndex        =   18
         Tag             =   "�Se necesita consentimiento firmado?"
         Top             =   260
         Width           =   975
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   6780
      Width           =   11640
      _ExtentX        =   20532
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPlanifActuaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
    Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuRegistroOpcion(72).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub

Private Sub chkCheck1_Click(Index As Integer)
  If chkCheck1(0).Value = 1 Then
    Call Cargar_Planificacion(True)
  Else
    Call Cargar_Planificacion(False)
  End If
End Sub

Private Sub cmdPlanificar_Click()
    Dim mintisel As Integer
    Dim mintNTotalSelRows As Integer
    Dim mvarBkmrk As Variant
    Dim mblnLlamar As Boolean
    
    mblnLlamar = False
    gstrplanif = ""
    'Guardamos el n�mero de filas seleccionadas
    mintNTotalSelRows = grdDBGrid1(0).SelBookmarks.Count
    
    For mintisel = 0 To mintNTotalSelRows - 1
        'Guardamos el n�mero de fila que est� seleccionada
        mvarBkmrk = grdDBGrid1(0).SelBookmarks(mintisel)
        If gstrplanif = "" Then
          gstrplanif = gstrplanif & grdDBGrid1(0).Columns(8).CellValue(mvarBkmrk)
        Else
          gstrplanif = gstrplanif & "," & grdDBGrid1(0).Columns(8).CellValue(mvarBkmrk)
        End If
        mblnLlamar = True
    Next mintisel
    gstrplanif = "(" & gstrplanif & ")"
    If mblnLlamar = True Then
      Call objsecurity.LaunchProcess("PR0149")
    Else
      Call MsgBox("No ha seleccionado ninguna Actuaci�n", vbExclamation)
    End If
End Sub

Private Sub Form_Activate()
  If chkCheck1(0).Value = 1 Then
    Call Cargar_Planificacion(True)
  Else
    Call Cargar_Planificacion(False)
  End If
  Call Form_Paint
End Sub

Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  'With objMultiInfo
    'Set .objFormContainer = fraframe1(0)
    'Set .objFatherContainer = Nothing
    'Set .tabMainTab = Nothing
    'Set .grdGrid = Nothing
    '.intFormModel = cwWithoutGrid + cwWithoutTab + cwWithoutKeys
    '.intAllowance = cwAllowReadOnly
    '.strDataBase = objEnv.GetValue("Main")
    '.strTable = "PR0300"

    'Call .FormAddOrderField("PR03NUMACTPEDI", cwAscending)
  
    'strKey = .strDataBase & .strTable
    'Call .FormCreateFilterWhere(strKey, "Actuaciones Seleccionadas")
    'Call .FormAddFilterWhere(strKey, "PR03NUMACTPEDI", "N�m.Actuaci�n", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�d.Actuaci�n", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "PR02CODDPTO", "C�d.Departamento", cwNumeric)
    
    'Call .FormAddFilterOrder(strKey,"PR03NUMACTPEDI", "N�m.Actuaci�n")
    'Call .FormAddFilterOrder(strKey,"PR01CODACTUACION", "C�d.Actuaci�n")
    'Call .FormAddFilterOrder(strKey,"PR02CODDPTO", "C�d.Departamento")
  'End With
  
  'With objWinInfo
    'Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
    'Call .GridAddColumn(objMultiInfo, "Condiciones", "", cwBoolean, 1)
    'Call .GridAddColumn(objMultiInfo, "Seleccionada", "", cwBoolean, 1)
    'Call .GridAddColumn(objMultiInfo, "N�mero", "PR03NUMACTPEDI", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "C�d.Act", "PR01CODACTUACION", cwNumeric, 9)
    'Call .GridAddColumn(objMultiInfo, "Descripci�n Actuaci�n", "", cwString, 30)
    'Call .GridAddColumn(objMultiInfo, "C�d.Dpto", "AD02CODDPTO", cwNumeric, 3)
    'Call .GridAddColumn(objMultiInfo, "Descrici�n Dpto", "", cwString, 30)
    ''Call .GridAddColumn("Situaci�n", "", cwString, 20)
    ''Call .GridAddColumn("Comisi�n", "Comm", cwDecimal, 1)
    'Call .FormCreateInfo(objMultiInfo)
    '.CtrlGetInfo(grdDBGrid1(0).Columns).blnNegotiated = False
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    '.CtrlGetInfo(grdDBGrid1(0).Columns(5)).intKeyNo = 1
    '.CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnMandatory = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnMandatory = True
    'Call .FormChangeColor(objMultiInfo)
  
    '.CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
    '.CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
  
    '.CtrlGetInfo(grdDBGrid1(0).Columns(8)).strSQL = "SELECT deptno, dname FROM " & objEnv.GetValue("Main") & "dept ORDER BY deptno"
  
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(6)), grdDBGrid1(0).Columns(7), "PR01DESCORTA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO = ?")
    'Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(8)), grdDBGrid1(0).Columns(9), "AD02DESDPTO")
    
    'Call .WinRegister
    'Call .WinStabilize
  'End With
   Inicializar_Toolbar
  'Call objApp.SplashOff
End Sub

Private Sub Form_Paint()
  grdDBGrid1(0).Refresh
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
  Select Case Button.Index
    Case 21:
      grdDBGrid1(0).MoveFirst
    Case 22:
      grdDBGrid1(0).MovePrevious
    Case 23:
      grdDBGrid1(0).MoveNext
    Case 24:
      grdDBGrid1(0).MoveLast
    Case 26:
      grdDBGrid1(0).Refresh
    Case 30:
      Unload Me
  End Select
End Sub
Private Sub Cargar_Planificacion(ByVal Todos As Boolean)
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim strPed As String
  Dim rstPed As rdoResultset
  Dim strSelectDesc As String
  Dim rstA1 As rdoResultset
  Dim strSelect1 As String
  Dim rstDesc As rdoResultset
  Dim strSelectDescDpto As String
  Dim rstDescDpto As rdoResultset
  Dim strSelectPlanif As String
  Dim rstPlanif As rdoResultset
  Dim strSelectPlanif2 As String
  Dim rstPlanif2 As rdoResultset
  Dim mblnver As Boolean
  Dim strSelectCuest As String
  Dim rstCuest As rdoResultset
  Dim rstACuest As rdoResultset
  Dim mblnCuest As Boolean
  Dim mintPlanificada As Integer
  
  grdDBGrid1(0).Redraw = False
  grdDBGrid1(0).RemoveAll
  strSelect = "SELECT * FROM PR0300 WHERE PR09NUMPETICION = " & frmpedir.txtText1(0).Text
  Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
  While rsta.EOF = False
    mblnver = True
    'C�digo de la Actuaci�n= rsta("pr01codactuacion").value
    'C�digo del Dpto=rsta("ad02coddpto").value
    'Citable=rsta("pr03indcitable").value
    'Sacar Desc.Actuaci�n
    strSelectDesc = "SELECT PR01DESCORTA FROM PR0100 WHERE PR01CODACTUACION = " & rsta("PR01CODACTUACION").Value
    Set rstDesc = objApp.rdoConnect.OpenResultset(strSelectDesc)
    'Desc Actuaci�n=rstdesc(0).value
    
    'Sacar Desc.Dpto
    strSelectDescDpto = "SELECT AD02DESDPTO FROM AD0200 WHERE AD02CODDPTO = " & rsta("AD02CODDPTO").Value
    Set rstDescDpto = objApp.rdoConnect.OpenResultset(strSelectDescDpto)
    'Desc Dpto=rstdescdpto(0).value
    
    'Sacar Planif
    strSelectPlanif = "SELECT * FROM PR0400 WHERE PR03NUMACTPEDI = " & rsta("PR03NUMACTPEDI").Value & " order by PR04FECPLANIFIC"
    Set rstPlanif = objApp.rdoConnect.OpenResultset(strSelectPlanif)
    
    If rstPlanif.EOF = False Then
      mintPlanificada = 1
      'Fecha Planificaci�n=rstplanif("pr04fecplanific").value
      If Todos = False Or (rstPlanif("PR37CODESTADO").Value <> 1) Then
        mblnver = False
      End If
      strSelectPlanif2 = "SELECT count(*) FROM PR0400 WHERE PR03NUMACTPEDI = " & rsta("PR03NUMACTPEDI").Value
      Set rstPlanif2 = objApp.rdoConnect.OpenResultset(strSelectPlanif2)
      If rstPlanif2(0).Value > 1 Then
        mintPlanificada = 2
      End If
      rstPlanif2.Close
      Set rstPlanif2 = Nothing
    Else
      mintPlanificada = 0
      strPed = "SELECT count(*) FROM PR0600 WHERE PR03NUMACTPEDI = " & rsta("PR03NUMACTPEDI").Value
      Set rstPed = objApp.rdoConnect.OpenResultset(strPed)
      If rstPed(0).Value = 0 Then
        mblnver = False
      End If
      'Fecha Planificaci�n=""
      rstPed.Close
      Set rstPed = Nothing
    End If
    'Sacar Cuestionario
    strSelectCuest = "SELECT COUNT(*) FROM PR2900 WHERE PR01CODACTUACION=" & rsta("pr01codactuacion").Value
    Set rstACuest = objApp.rdoConnect.OpenResultset(strSelectCuest)
    If rstACuest(0).Value = 0 Then
      strSelect1 = "SELECT COUNT(*) FROM PR3000 WHERE PR16CODGRUPO IN (SELECT PR16CODGRUPO FROM PR1700 WHERE PR01CODACTUACION=" & rsta("pr01codactuacion").Value & ")"
      Set rstA1 = objApp.rdoConnect.OpenResultset(strSelect1)
      If rstA1(0).Value = 0 Then
        strSelect1 = "SELECT COUNT(*) FROM PR4700,AG1500,PR1400 WHERE " & _
                      "PR4700.PR09NUMPETICION=" & frmpedir.txtText1(0).Text & _
                      " AND PR4700.AG16CODTIPREST = AG1500.AG16CODTIPREST" & _
                      " AND AG1500.AG14CODTIPRECU = PR1400.AG14CODTIPRECU" & _
                      " AND PR1400.PR03NUMACTPEDI=" & rsta("PR03NUMACTPEDI").Value
        Set rstA1 = objApp.rdoConnect.OpenResultset(strSelect1)
        If rstA1(0).Value = 0 Then
          mblnCuest = False
        Else
          mblnCuest = True
        End If
      Else
        mblnCuest = True
      End If
    Else
      mblnCuest = True
    End If
    If mblnver = True Then
      grdDBGrid1(0).AddNew
      grdDBGrid1(0).Columns(0).Value = mintPlanificada
      grdDBGrid1(0).Columns(1).Value = rsta("pr01codactuacion").Value
      grdDBGrid1(0).Columns(2).Value = rstDesc(0).Value
      'grdDBGrid1(0).Columns(3).Value = rsta("ad02coddpto").Value
      grdDBGrid1(0).Columns(3).Value = rstDescDpto(0).Value
      If mintPlanificada > 0 Then
        If IsNull(rstPlanif("pr04fecplanific").Value) Then
          grdDBGrid1(0).Columns(4).Value = ""
        Else
          grdDBGrid1(0).Columns(4).Value = rstPlanif("pr04fecplanific").Value
        End If
      Else
        grdDBGrid1(0).Columns(4).Value = ""
      End If
      If mintPlanificada = 2 Then
        grdDBGrid1(0).Columns(5).Value = 1
      Else
        grdDBGrid1(0).Columns(5).Value = 0
      End If
      If IsNull(rsta("pr03indcitaant").Value) Then
        grdDBGrid1(0).Columns(6).Value = 0
      Else
        If rsta("pr03indcitaant").Value = -1 Then
          grdDBGrid1(0).Columns(6).Value = 1
        Else
          grdDBGrid1(0).Columns(6).Value = 0
        End If
      End If
      If mblnCuest = True Then
        grdDBGrid1(0).Columns(7).Value = 1
      Else
        grdDBGrid1(0).Columns(7).Value = 0
      End If
      grdDBGrid1(0).Columns(8).Value = rsta("PR03NUMACTPEDI").Value
      grdDBGrid1(0).Update
    End If
    rsta.MoveNext
    rstDesc.Close
    Set rstDesc = Nothing
    rstDescDpto.Close
    Set rstDescDpto = Nothing
    rstPlanif.Close
    Set rstPlanif = Nothing
    rstACuest.Close
    Set rstACuest = Nothing
    
  Wend
  grdDBGrid1(0).MoveFirst
  rsta.Close
  Set rsta = Nothing
  grdDBGrid1(0).Redraw = True
End Sub
