Attribute VB_Name = "PED002"

'blncuestionario2 para saber si el bot�n "Siguiente" nos lleva al
'cuestionario de las muestras o no
Public gblncuestionario2 As Integer
Public gblncuestionario3 As Integer
Public gblncuest3volver As Integer
Public gblnmuestraactivate As Integer
Public gblnmuestravolver As Integer
Public gdesde As Integer
Public gsalirag17 As Integer
Public gtabla As String
Public gcolumna As String
Public gcoldesc As String
Public gtipodato As Integer
Public gindice As Integer
'cuest3 ser� 1 si voy de frmcuestionario3 a los buscadores frmag16 o frmag17
'cuest3 ser� 2 si voy de frmcuestionario3SegundaParte a los buscadores frmag16 o frmag17
Public cuest3 As Integer
Public numelem As Integer
Public gintEstadoPeticion As Integer
Public glngnumerogrupo As Long
Public glngnumeropeticion As Long
Public glngcodigopersona As Long
Public datfechprop As Date
Public blnprimprop As Boolean
Public primerallamada As Boolean
'gintsinfases controla si al volver de la pantalla Fase �nica la fase se ha guardado o no.
Public gintactsinfases As Integer
Public gpasardpto As Long
Public gstrplanif As String
Public gstractcuest As String
Public gblnunloadrellenardatosmuestra As Boolean
Public gblnactivarbotones As Boolean
Public gstrLlamadorPed As String 'Aplicacion que llama a la pantalla de pedir



Public Sub Seleccionar_Dpto()
    Dim sqlstr As String
    Dim rsta As rdoResultset
    
    sqlstr = "SELECT * FROM AD0300 WHERE SG02COD ='" & objsecurity.strUser & "'"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta.EOF = False Then
        glngdptologin = rsta("AD02CODDPTO").Value
        rsta.MoveNext
        If rsta.EOF = False Then
            'Load frmEscogerDpto
            'frmEscogerDpto.Show (vbModal)
            'Unload frmEscogerDpto
            'Set frmEscogerDpto = Nothing
            Call objsecurity.LaunchProcess("PR0194")
        End If
    End If
    rsta.Close
    Set rsta = Nothing
End Sub
Public Function Fecha_Mayor(ByVal strFec1 As String, ByVal strFec2 As String, _
                            ByVal strHora As String, ByVal strMin As String, _
                            ByVal blnHora As Boolean, blnMin As Boolean) As Boolean
'Compara las fechas strFec1 y strFec2, devolviendo TRUE su strFec1 es mayor que strFec2
'Si blnHora = TRUE hay comparar con precisi�n de Horas
'Si blnMin = TRUE hay que comparar con precisi�n de minutos

  
  Dim dtcFechaSel As Date
  Dim strAnyoSelAux As String
  Dim strMesSelAux As String
  Dim strDiaSelAux As String
  Dim intAnyoSelAux As Integer
  Dim intMesSelAux  As Integer
  Dim intDiaSelAux As Integer
  
  Dim dtcFechaSys As Date
  Dim strAnyoSysAux As String
  Dim strMesSysAux As String
  Dim strDiaSysAux As String
  Dim intAnyoSysAux As Integer
  Dim intMesSysAux  As Integer
  Dim intDiaSysAux As Integer
  Dim blnFechaValida As Boolean
  
  
  
  
  strAnyoSelAux = Format(strFec1, "yyyy")
  If Len(strAnyoSelAux) = 2 Then
    strAnyoSelAux = "19" & strAnyoSelAux
  End If
  intAnyoSelAux = strAnyoSelAux
    
  strAnyoSysAux = Format(strFec2, "yyyy")
  If Len(strAnyoSysAux) = 2 Then
     strAnyoSysAux = "19" & strAnyoSysAux
  End If
  intAnyoSysAux = strAnyoSysAux
    
  strMesSelAux = Format(strFec1, "mm")
  intMesSelAux = strMesSelAux
    
  strMesSysAux = Format(strFec2, "mm")
  intMesSysAux = strMesSysAux
      
  strDiaSelAux = Format(strFec1, "dd")
  intDiaSelAux = strDiaSelAux
    
  strDiaSysAux = Format(strFec2, "dd")
  intDiaSysAux = strDiaSysAux
        
  If intAnyoSelAux < intAnyoSysAux Then
     blnFechaValida = False
  Else
    If intAnyoSelAux = intAnyoSysAux Then
      If intMesSelAux < intMesSysAux Then
        blnFechaValida = False
      Else
        If intMesSelAux = intMesSysAux Then
          If intDiaSelAux < intDiaSysAux Then
            blnFechaValida = False
          Else
            If (intDiaSelAux = intDiaSysAux) And (blnHora = True) And (strHora <> "") Then
              If CDbl(Format(strHora, "hh")) < CDbl(Format(strFec2, "hh")) Then
                blnFechaValida = False
              Else
                If CDbl(Format(strHora, "hh")) = CDbl(Format(strFec2, "hh")) _
                  And (blnMin = True) And (strMin <> "") Then
                   If CDbl(Format(strMin, "nn")) <= CDbl(Format(strFec2, "nn")) Then
                      blnFechaValida = False
                   End If
                Else
                  If CDbl(Format(strHora, "hh")) = CDbl(Format(strFec2, "hh")) _
                    And (blnMin = False) And (strHora <> "") Then
                    blnFechaValida = False
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
  End If
   
  Fecha_Mayor = blnFechaValida
End Function

Public Sub LlenarTabla(ByVal act As Variant, actpedi As Variant)
' procedimiento que va llenando un string con las interacciones que
' ya estaban generadas

Dim sqlstr As String
Dim rsta As rdoResultset
Dim desc As String
Dim sqlstr1 As String
'***********************************
'BORRAR la tabla PR0800


    sqlstr1 = "DELETE FROM PR0800 " _
             & "WHERE (pr09numpeticion = " & frmpedir.txtText1(0).Text _
             & " and pr03numactpedi= " & actpedi & ")"
    On Error GoTo Err_Ejecutar
    objApp.rdoConnect.Execute sqlstr1, 64
    objApp.rdoConnect.Execute "Commit", 64
    'frmSeleccionarAct.grdDBGrid1(0).Columns(7).Value = ""
    'frmSeleccionarAct.grdDBGrid1(0).Columns(8).Value = ""
    
    'BORRAR la tabla PR3800
    sqlstr1 = "DELETE FROM PR3800 " _
           & "WHERE (pr01codactuacion = " & act & " and pr03numactpedi= " & actpedi & ")"
    On Error GoTo Err_Ejecutar
    objApp.rdoConnect.Execute sqlstr1, 64
    objApp.rdoConnect.Execute "Commit", 64
    
    'BORRAR la tabla PR0300
    If glngactpedida <> "" Then
        sqlstr1 = "DELETE FROM PR0300 " _
                & "WHERE (pr01codactuacion = " & act & " and pr03numactpedi= " & actpedi & ")"
        On Error GoTo Err_Ejecutar
        objApp.rdoConnect.Execute sqlstr1, 64
        objApp.rdoConnect.Execute "Commit", 64
        'frmSeleccionarAct.grdDBGrid1(0).Columns(6).Value = ""
    End If
'**********************************
  
  
      sqlstr = "SELECT pr01descorta " _
             & "FROM pr0100 " _
             & "WHERE pr01codactuacion = " & act
      On Error GoTo Err_Ejecutar
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      desc = rsta.rdoColumns(0).Value
      ' se guarda en el grid el c�digo y descripci�n de la actuaci�n que no se seleccionar�
      frmactnosel.grdDBGrid1(0).Columns(0).Text = act
      frmactnosel.grdDBGrid1(0).Columns(1).Text = desc
    
      frmactnosel.grdDBGrid1(0).AddNew
      ' si hay m�s registros que el tama�o del grid que haga autom�tiacmente Scroll
      'If (frmactnosel.grdDBGrid1(0).Row >= frmactnosel.grdDBGrid1(0).VisibleRows - 2) Then
      '      frmactnosel.grdDBGrid1(0).Scroll 0, 1
      '      frmactnosel.grdDBGrid1(0).AddItem ""
      'End If
      'frmactnosel.grdDBGrid1(0).Row = frmactnosel.grdDBGrid1(0).Row + 1
      
      rsta.Close
      Set rsta = Nothing
      Exit Sub
Err_Ejecutar:
    MsgBox "Error: " & Err.Number & " " & Err.Description
    Exit Sub
End Sub


