VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmActuacionesRealizadas1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ADMISION.Admitir Paciente"
   ClientHeight    =   8235
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8235
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdImpActReali 
      Caption         =   "Imp. Act. Realizadas"
      Height          =   375
      Left            =   6480
      TabIndex        =   24
      Top             =   7680
      Width           =   2175
   End
   Begin VB.OptionButton optReali 
      Caption         =   "Realizadas"
      Height          =   255
      Left            =   9120
      TabIndex        =   22
      Top             =   1200
      Value           =   -1  'True
      Width           =   1335
   End
   Begin VB.OptionButton optCancel 
      Caption         =   "Canceladas"
      Height          =   255
      Left            =   10200
      TabIndex        =   21
      Top             =   840
      Width           =   1215
   End
   Begin VB.OptionButton optTodas 
      Caption         =   "Todas"
      Height          =   255
      Left            =   9120
      TabIndex        =   20
      Top             =   840
      Width           =   855
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   9240
      TabIndex        =   18
      Top             =   7680
      Width           =   1455
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Index           =   1
      Left            =   10800
      TabIndex        =   16
      Top             =   240
      Width           =   1035
   End
   Begin VB.CommandButton cmdImpPet 
      Caption         =   "Imp. Petici�n"
      Height          =   375
      Left            =   4680
      TabIndex        =   15
      Top             =   7680
      Width           =   1515
   End
   Begin VB.CommandButton cmdObser 
      Caption         =   "Datos de la Actuacion"
      Height          =   375
      Left            =   120
      TabIndex        =   14
      Top             =   7680
      Width           =   2235
   End
   Begin VB.CommandButton cmdRecConsumidos 
      Caption         =   "Recursos consumidos"
      Height          =   375
      Left            =   2640
      TabIndex        =   13
      Top             =   7680
      Width           =   1755
   End
   Begin VB.Frame Frame1 
      Caption         =   "Fechas"
      ForeColor       =   &H00800000&
      Height          =   615
      Left            =   6000
      TabIndex        =   10
      Top             =   120
      Width           =   4575
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   1
         Left            =   720
         TabIndex        =   4
         Tag             =   "Fecha Desde"
         ToolTipText     =   "Fecha Desde"
         Top             =   165
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   330
         Index           =   2
         Left            =   2880
         TabIndex        =   5
         Tag             =   "Fecha Hasta"
         ToolTipText     =   "Fecha Hasta"
         Top             =   165
         Width           =   1455
         _Version        =   65537
         _ExtentX        =   2566
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         Height          =   195
         Left            =   2280
         TabIndex        =   12
         Top             =   240
         Width           =   420
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   465
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Paciente"
      ForeColor       =   &H00800000&
      Height          =   1110
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   5820
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar"
         Height          =   375
         Left            =   4575
         TabIndex        =   3
         Top             =   225
         Width           =   990
      End
      Begin VB.TextBox txtHistoria 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   960
         TabIndex        =   0
         Top             =   240
         Width           =   825
      End
      Begin VB.TextBox txtPaciente 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   975
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   675
         Width           =   4575
      End
      Begin VB.TextBox txtFNacim 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   2775
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   225
         Width           =   1200
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Historia:"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   9
         Top             =   315
         Width           =   570
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Paciente:"
         Height          =   195
         Left            =   225
         TabIndex        =   8
         Top             =   750
         Width           =   675
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "F. Nacim:"
         Height          =   195
         Index           =   1
         Left            =   2025
         TabIndex        =   7
         Top             =   300
         Width           =   675
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
      Height          =   5955
      HelpContextID   =   2
      Index           =   1
      Left            =   120
      TabIndex        =   19
      Top             =   1560
      Width           =   11655
      _Version        =   131078
      DataMode        =   2
      Col.Count       =   0
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeRow   =   3
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorEven   =   12632256
      BackColorOdd    =   12632256
      RowHeight       =   423
      CaptionAlignment=   0
      SplitterVisible =   -1  'True
      Columns(0).Width=   3200
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      _ExtentX        =   20558
      _ExtentY        =   10504
      _StockProps     =   79
      Caption         =   "Actuaciones Relizadas"
      ForeColor       =   8388608
   End
   Begin SSDataWidgets_B.SSDBCombo cboDBcombo1 
      Height          =   285
      Index           =   1
      Left            =   7200
      TabIndex        =   23
      Tag             =   "Departamento|Departamento Peticionario"
      Top             =   840
      Width           =   1875
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Descripcion"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3307
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   16777215
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Departamento"
      Height          =   195
      Left            =   6000
      TabIndex        =   17
      Top             =   840
      Width           =   1005
   End
End
Attribute VB_Name = "frmActuacionesRealizadas1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim nPers As Long
Dim strHistoria_Ant As String
Dim strdptos As String






Private Sub cboDBCombo1_Click(Index As Integer)
grdDBGrid1(1).RemoveAll

End Sub

Private Sub cmdConsultar_Click(Index As Integer)
If txtHistoria.Text = "" Then
   MsgBox "Tiene que introducir un Paciente", vbExclamation + vbOKOnly
   txtHistoria.SetFocus
   Exit Sub
End If
Call pActualizar(-1)
End Sub
Private Sub pActualizar(intCol As Integer)
Dim strWhere As String
Dim strSelect As String
Dim strsql As String
Dim strfrom As String
Dim rst As rdoResultset

Dim sqlOrder As String
Screen.MousePointer = vbHourglass

 If intCol = -1 Then
    If sqlOrder = "" Then
      If optTodas.Value = True Then
        sqlOrder = " ORDER BY PR0400.PR04FECCANCEL,PR0400.PR04FECFINACT"
      ElseIf optCancel.Value = True Then
        sqlOrder = " ORDER BY PR0400.PR04FECCANCEL"
      ElseIf optReali.Value Then
        sqlOrder = " ORDER BY PR0400.PR04FECFINACT"
      End If
     End If
Else
    Select Case grdDBGrid1(1).Columns(intCol).Caption
    Case "Fecha Realizacion": sqlOrder = " ORDER BY PR0400.PR04FECFINACT"
    Case "Fecha Cancelacion": sqlOrder = " ORDER BY PR0400.PR04FECCANCEL"
    Case "Fecha": sqlOrder = " ORDER BY PR0400.PR04FECCANCEL,PR0400.PR04FECFINACT"
    Case "Departamento Responsable": sqlOrder = " ORDER BY AD0200.AD02DESDPTO"
    Case "Doctor Responsable": sqlOrder = " ORDER BY SG02FIRMA.SG02TXTFIRMA"
    Case "Actuaci�n": sqlOrder = " ORDER BY PR0100.PR01DESCORTA"
    Case "A�adido": sqlOrder = " ORDER BY SG02UPD.SG02APE2,SG02UPD.SG02APE1"
    Case Else: Exit Sub
    End Select
  End If
grdDBGrid1(1).RemoveAll
If optCancel.Value = True Then
    grdDBGrid1(1).Columns(0).Caption = "Fecha Cancelacion"
    grdDBGrid1(1).Refresh
    
    strSelect = "SELECT PR0400.PR04FECCANCEL"
    strSelect = strSelect & ",PR0100.PR01DESCORTA,"
    strSelect = strSelect & "SG02FIRMA.SG02TXTFIRMA,PR3700.PR37DESESTADO,PR0400.AD02CODDPTO,AD0200.AD02DESDPTO,"
    strSelect = strSelect & "PR0400.PR04NUMACTPLAN,"
    strSelect = strSelect & "PR0400.PR04CANTIDAD,SG02UPD.SG02NOM||' '||SG02UPD.SG02APE1||' '||SG02UPD.SG02APE2 USUARIO"
    strfrom = " FROM    PR0400,PR3700,PR0100,AD0200,SG0200 SG02FIRMA,AD0500,SG0200 SG02UPD"
    strWhere = " WHERE "
    strWhere = strWhere & "PR0400.PR37CODESTADO=PR3700.PR37CODESTADO AND"
    strWhere = strWhere & " PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION AND PR0400.AD02CODDPTO=AD0200.AD02CODDPTO"
    strWhere = strWhere & " AND PR0400.AD01CODASISTENCI=AD0500.AD01CODASISTENCI AND PR0400.AD07CODPROCESO=AD0500.AD07CODPROCESO"
    strWhere = strWhere & " AND AD0500.SG02COD=SG02FIRMA.SG02COD"
    strWhere = strWhere & " AND PR0400.SG02COD_UPD=SG02UPD.SG02COD"
    strWhere = strWhere & " AND PR0400.PR37CODESTADO=6"
    If cboDBcombo1(1).Columns(0).Value = 999 Then
         strWhere = strWhere & " AND PR0400.PR04FECCANCEL >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECCANCEL < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY')" & " AND PR0400.CI21CODPERSONA=" & nPers & _
        " AND PR0400.AD02CODDPTO IN (" & strdptos & ")"
        
        
        
    Else
        strWhere = strWhere & " AND PR0400.PR04FECCANCEL >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECCANCEL < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY') AND " & _
        "PR0400.AD02CODDPTO = " & cboDBcombo1(1).Columns(0).Value & " AND PR0400.CI21CODPERSONA=" & nPers
    End If
ElseIf optReali.Value = True Then
    grdDBGrid1(1).Columns(0).Caption = "Fecha Realizacion"
    grdDBGrid1(1).Refresh
     strSelect = "SELECT PR0400.PR04FECFINACT"
    strSelect = strSelect & ",PR0100.PR01DESCORTA,"
    strSelect = strSelect & "SG02FIRMA.SG02TXTFIRMA,PR3700.PR37DESESTADO,PR0400.AD02CODDPTO,AD0200.AD02DESDPTO,"
    strSelect = strSelect & "PR0400.PR04NUMACTPLAN,"
    strSelect = strSelect & "PR0400.PR04CANTIDAD,SG02UPD.SG02NOM||' '||SG02UPD.SG02APE1||' '||SG02UPD.SG02APE2 USUARIO"
    strfrom = " FROM    PR0400,PR3700,PR0100,AD0200,SG0200 SG02FIRMA,AD0500,SG0200 SG02UPD"
    strWhere = " WHERE"
    strWhere = strWhere & " PR0400.PR37CODESTADO=PR3700.PR37CODESTADO AND"
    strWhere = strWhere & " PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION AND PR0400.AD02CODDPTO=AD0200.AD02CODDPTO"
    strWhere = strWhere & " AND PR0400.AD01CODASISTENCI=AD0500.AD01CODASISTENCI AND PR0400.AD07CODPROCESO=AD0500.AD07CODPROCESO"
    strWhere = strWhere & " AND AD0500.SG02COD=SG02FIRMA.SG02COD"
    strWhere = strWhere & " AND PR0400.SG02COD_UPD=SG02UPD.SG02COD"
    strWhere = strWhere & " AND PR0400.PR04FECENTRCOLA IS NOT NULL AND PR0400.PR04FECFINACT IS NOT NULL"
    strWhere = strWhere & " AND PR0400.PR37CODESTADO=4"

    If cboDBcombo1(1).Columns(0).Value = 999 Then
         strWhere = strWhere & " AND PR0400.PR04FECFINACT >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECFINACT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY')" & " AND PR0400.CI21CODPERSONA=" & nPers & _
        " AND PR0400.AD02CODDPTO IN (" & strdptos & ")"
        
        
    Else
        strWhere = strWhere & " AND PR0400.PR04FECFINACT >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECFINACT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY') AND " & _
        "PR0400.AD02CODDPTO = " & cboDBcombo1(1).Columns(0).Value & " AND PR0400.CI21CODPERSONA=" & nPers
        
        
    End If
ElseIf optTodas.Value = True Then
    grdDBGrid1(1).Columns(0).Caption = "Fecha"
    grdDBGrid1(1).Refresh
     strSelect = "SELECT PR0400.PR04FECFINACT"
    strSelect = strSelect & ",PR0100.PR01DESCORTA,"
    strSelect = strSelect & "SG02FIRMA.SG02TXTFIRMA,PR3700.PR37DESESTADO,PR0400.AD02CODDPTO,AD0200.AD02DESDPTO,"
    strSelect = strSelect & "PR0400.PR04NUMACTPLAN,"
    strSelect = strSelect & "PR0400.PR04CANTIDAD,SG02UPD.SG02NOM||' '||SG02UPD.SG02APE1||' '||SG02UPD.SG02APE2 USUARIO"
    strfrom = " FROM    PR0400,PR3700,PR0100,AD0200,SG0200 SG02FIRMA,AD0500,SG0200 SG02UPD"
    strWhere = " WHERE "
    strWhere = strWhere & "PR0400.PR37CODESTADO=PR3700.PR37CODESTADO AND"
    strWhere = strWhere & " PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION AND PR0400.AD02CODDPTO=AD0200.AD02CODDPTO"
    strWhere = strWhere & " AND PR0400.AD01CODASISTENCI=AD0500.AD01CODASISTENCI AND PR0400.AD07CODPROCESO=AD0500.AD07CODPROCESO"
    strWhere = strWhere & " AND AD0500.SG02COD=SG02FIRMA.SG02COD"
    strWhere = strWhere & " AND PR0400.SG02COD_UPD=SG02UPD.SG02COD"
    strWhere = strWhere & " AND PR0400.PR04FECENTRCOLA IS NOT NULL AND PR0400.PR04FECFINACT IS NOT NULL"
    strWhere = strWhere & " AND PR0400.PR37CODESTADO=4"
    
    If cboDBcombo1(1).Columns(0).Value = 999 Then
         strWhere = strWhere & " AND PR0400.PR04FECFINACT >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECFINACT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY')" & " AND PR0400.CI21CODPERSONA=" & nPers & _
        " AND PR0400.AD02CODDPTO IN (" & strdptos & ")"
    Else
        strWhere = strWhere & " AND PR0400.PR04FECFINACT >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECFINACT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY') AND " & _
        "PR0400.AD02CODDPTO = " & cboDBcombo1(1).Columns(0).Value & " AND PR0400.CI21CODPERSONA=" & nPers
    End If
 End If
 




strsql = strSelect & strfrom & strWhere & sqlOrder

Set rst = objApp.rdoConnect.OpenResultset(strsql)
Do While Not rst.EOF

 grdDBGrid1(1).AddItem (rst(0) & Chr$(9) & _
                     rst(1) & Chr$(9) & _
                     rst(4) & Chr$(9) & _
                     rst(6) & Chr$(9) & _
                     rst(5) & Chr$(9) & _
                     rst(2) & Chr$(9) & _
                     rst!USUARIO & Chr$(9) & _
                     rst(3) & Chr$(9) & _
                     rst(7))
                 

 
 rst.MoveNext
 
 Loop
 rst.Close
 Set rst = Nothing
 
 If optTodas.Value = True Then
    grdDBGrid1(1).Columns(0).Caption = "Fecha"
    grdDBGrid1(1).Refresh
    strSelect = "SELECT PR0400.PR04FECCANCEL"
    strSelect = strSelect & ",PR0100.PR01DESCORTA,"
    strSelect = strSelect & "SG02FIRMA.SG02TXTFIRMA,PR3700.PR37DESESTADO,PR0400.AD02CODDPTO,AD0200.AD02DESDPTO,"
    strSelect = strSelect & "PR0400.PR04NUMACTPLAN,"
    strSelect = strSelect & "PR0400.PR04CANTIDAD,SG02UPD.SG02NOM||' '||SG02UPD.SG02APE1||' '||SG02UPD.SG02APE2 USUARIO"
    strfrom = " FROM    PR0400,PR3700,PR0100,AD0200,SG0200 SG02FIRMA,AD0500,SG0200 SG02UPD"
    strWhere = " WHERE "
    strWhere = strWhere & "PR0400.PR37CODESTADO=PR3700.PR37CODESTADO AND"
    strWhere = strWhere & " PR0400.PR01CODACTUACION=PR0100.PR01CODACTUACION AND PR0400.AD02CODDPTO=AD0200.AD02CODDPTO"
    strWhere = strWhere & " AND PR0400.AD01CODASISTENCI=AD0500.AD01CODASISTENCI AND PR0400.AD07CODPROCESO=AD0500.AD07CODPROCESO"
    strWhere = strWhere & " AND AD0500.SG02COD=SG02FIRMA.SG02COD"
    strWhere = strWhere & " AND PR0400.SG02COD_UPD=SG02UPD.SG02COD"
    strWhere = strWhere & " AND PR0400.PR37CODESTADO=6"
    If cboDBcombo1(1).Columns(0).Value = 999 Then
         strWhere = strWhere & " AND PR0400.PR04FECCANCEL >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECCANCEL < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY')" & " AND PR0400.CI21CODPERSONA=" & nPers & _
        " AND PR0400.AD02CODDPTO IN (" & strdptos & ")"
        
        
    Else
        strWhere = strWhere & " AND PR0400.PR04FECCANCEL >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECCANCEL < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY') AND " & _
        "PR0400.AD02CODDPTO = " & cboDBcombo1(1).Columns(0).Value & " AND PR0400.CI21CODPERSONA=" & nPers
    End If
 strsql = strSelect & strfrom & strWhere & sqlOrder

Set rst = objApp.rdoConnect.OpenResultset(strsql)
Do While Not rst.EOF

 grdDBGrid1(1).AddItem (rst(0) & Chr$(9) & _
                     rst(1) & Chr$(9) & _
                     rst(4) & Chr$(9) & _
                     rst(6) & Chr$(9) & _
                     rst(5) & Chr$(9) & _
                     rst(2) & Chr$(9) & _
                     rst!USUARIO & Chr$(9) & _
                     rst(3) & Chr$(9) & _
                     rst(7))
                 

 
 rst.MoveNext
 
 Loop
 rst.Close
 Set rst = Nothing
 End If
 




 Screen.MousePointer = vbDefault
 
End Sub

Private Sub cmdImpActReali_Click()
Dim strwhererep As String
If optTodas.Value = True Or optReali.Value = True Then
      strwhererep = "PR0400.PR37CODESTADO=4"
End If
If cboDBcombo1(1).Columns(0).Value = 999 Then
         strwhererep = strwhererep & " AND PR0400.PR04FECFINACT >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECFINACT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY')" & " AND PR0400.CI21CODPERSONA=" & nPers & _
        " AND PR0400.AD02CODDPTO IN (" & strdptos & ")"
        
    Else
        strwhererep = strwhererep & " AND PR0400.PR04FECFINACT >= TO_DATE('" & dtcDateCombo1(1).Text & "','DD/MM/YYYY')" & _
        " AND PR0400.PR04FECFINACT < TO_DATE('" & DateAdd("d", 1, dtcDateCombo1(2).Text) & "','DD/MM/YYYY') AND " & _
        "PR0400.AD02CODDPTO = " & cboDBcombo1(1).Columns(0).Value & " AND PR0400.CI21CODPERSONA=" & nPers
    End If
Call Imprimir_API(strwhererep, "PR0273.rpt")
End Sub

Private Sub cmdImpPet_Click()
Dim rsgrid As SSDBGrid
Set rsgrid = grdDBGrid1(1)
If rsgrid.SelBookmarks.Count = 0 Then
  MsgBox "Debe seleccionar una Actuaci�n", vbExclamation + vbOKOnly
  Exit Sub
End If

Call pImprimir(rsgrid)
End Sub

Private Sub cmdObser_Click()
Dim lngActPlan&
Dim rsgrid As SSDBGrid
Set rsgrid = grdDBGrid1(1)


  If rsgrid Is Nothing Then
    Exit Sub
  End If
  If rsgrid.SelBookmarks.Count = 0 Then
    MsgBox "No se ha seleccionado ninguna actuacion", vbExclamation, Me.Caption
    
    Exit Sub
  End If
  If rsgrid.SelBookmarks.Count = 1 Then
    lngActPlan = rsgrid.Columns("Actuaci�n Planificada").CellValue(rsgrid.SelBookmarks(0))
  Else
    MsgBox "Debe Ud. seleccionar una �nica Actuaci�n.", vbExclamation, Me.Caption
    
    Exit Sub
  End If
  lngActPlan = rsgrid.Columns("Actuaci�n Planificada").CellValue(rsgrid.SelBookmarks(0)) & ", "
  Call objsecurity.LaunchProcess("PR0510", lngActPlan)


End Sub

Private Sub cmdRecConsumidos_Click()
If grdDBGrid1(1).SelBookmarks.Count = 0 Then
  MsgBox "Debe seleccionar una Actuaci�n", vbExclamation + vbOKOnly
  Exit Sub
End If

Call objPipe.PipeSet("PR_NActPlan", grdDBGrid1(1).Columns("Actuaci�n Planificada").Value)
Call objPipe.PipeSet("AD_CodDpto", grdDBGrid1(1).Columns("Cod.Dpto").Value)
frmRecConsumidos.Show vbModal
Set frmRecConsumidos = Nothing
End Sub

Private Sub cmdSalir_Click()
Unload Me
Set frmActuacionesRealizadas1 = Nothing


End Sub

Private Sub dtcDateCombo1_Click(Index As Integer)
grdDBGrid1(1).RemoveAll

End Sub

Private Sub Form_Load()
Call Cargar_Dptos
dtcDateCombo1(1).Text = strFecha_Sistema()
dtcDateCombo1(2).Text = strFecha_Sistema()
dtcDateCombo1(1).Date = strFecha_Sistema()
dtcDateCombo1(2).Date = strFecha_Sistema()
Call Formatear_grid
End Sub
Private Sub Cargar_Dptos()
Dim sqlstr As String
    Dim rsta As rdoResultset
    sqlstr = "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM " & _
             "AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objsecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL ORDER BY AD0200.AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    While rsta.EOF = False
      If strdptos = "" Then
        strdptos = rsta("AD02CODDPTO").Value
      Else
        strdptos = strdptos & "," & rsta("AD02CODDPTO").Value
      End If
        cboDBcombo1(1).AddItem rsta("AD02CODDPTO").Value & Chr$(9) & rsta("AD02DESDPTO").Value
        cboDBcombo1(1).Text = rsta("AD02DESDPTO").Value
        cboDBcombo1(1).MoveNext
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
    sqlstr = "SELECT COUNT(AD0200.AD02CODDPTO) FROM " & _
             "AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objsecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL ORDER BY AD0200.AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If rsta(0) > 1 Then
    cboDBcombo1(1).AddItem "999" & Chr$(9) & "TODOS"
    End If
     rsta.Close
    Set rsta = Nothing
   
    cboDBcombo1(1).Enabled = True
End Sub

Private Sub grdDBGrid1_Click(Index As Integer)
If grdDBGrid1(1).SelBookmarks.Count = 1 Then
  If grdDBGrid1(1).Columns("Estado").Text = "Cancelada" Then
    cmdRecConsumidos.Enabled = False
    cmdImpPet.Enabled = False
  Else
    cmdRecConsumidos.Enabled = True
    cmdImpPet.Enabled = True
  End If
Else
 Exit Sub
End If

End Sub

Private Sub grdDBGrid1_HeadClick(Index As Integer, ByVal ColIndex As Integer)
If grdDBGrid1(1).Rows > 0 Then Call pActualizar(ColIndex)
End Sub

Private Sub optCancel_Click()
grdDBGrid1(1).RemoveAll
cmdRecConsumidos.Enabled = False
cmdImpPet.Enabled = False
cmdImpActReali.Enabled = False

End Sub

Private Sub optReali_Click()
grdDBGrid1(1).RemoveAll
cmdRecConsumidos.Enabled = True
cmdImpPet.Enabled = True
cmdImpActReali.Enabled = True
End Sub

Private Sub optTodas_Click()
grdDBGrid1(1).RemoveAll
cmdRecConsumidos.Enabled = True
cmdImpPet.Enabled = True
cmdImpActReali.Enabled = True

End Sub

Private Sub txtHistoria_Change()
  If txtHistoria.Text <> strHistoria_Ant Then
    Call pLimpiar
  End If
End Sub

Private Sub txtHistoria_GotFocus()
  txtHistoria.SelStart = 0
  txtHistoria.SelLength = Len(Trim(txtHistoria.Text))

End Sub

Private Sub txtHistoria_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    cmdBuscar.SetFocus
  End If
End Sub

Private Sub txthistoria_LostFocus()
If txtHistoria = "" Then
  Exit Sub
End If

  Call pIniciar
'Call pMensajes
End Sub

Private Sub txtPaciente_GotFocus()
txtPaciente.SelStart = 0
txtPaciente.SelLength = Len(Trim(txtPaciente.Text))

End Sub
Public Sub pIniciar()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

  If txtHistoria.Text = "" And nPers = 0 Then
    Exit Sub
  End If
  
  If txtHistoria.Text <> "" Then
    sql = "SELECT CI21CODPERSONA,"
    sql = sql & "CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PACIENTE, "
    sql = sql & "CI22FECNACIM FECHANACIM "
    sql = sql & "FROM CI2200 WHERE CI22NUMHISTORIA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = Val(txtHistoria.Text)
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
      nPers = rs!CI21CODPERSONA
      txtPaciente.Text = rs!PACIENTE
      txtFNacim.Text = Format$(rs!FECHANACIM, "dd/mm/yyyy")
      strHistoria_Ant = txtHistoria.Text
    Else
    
      MsgBox "No existe nadie con ese n�mero de historia. Utilice el buscador", vbExclamation, Me.Caption
      Exit Sub
    End If
  ElseIf nPers <> 0 Then
    sql = "SELECT CI22PRIAPEL||' '||NVL(CI22SEGAPEL,' ')||', '||CI22NOMBRE PACIENTE, "
    sql = sql & "CI22FECNACIM FECHANACIM "
    sql = sql & "FROM CI2200 WHERE CI21CODPERSONA=?"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = nPers
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
      txtPaciente.Text = rs!PACIENTE
      txtFNacim.Text = Format$(rs!FECHANACIM, "dd/mm/yyyy")
      strHistoria_Ant = txtHistoria.Text
    End If
  End If
End Sub
Private Sub cmdBuscar_Click()
 Call objsecurity.LaunchProcess("AD2999")
  If objPipe.PipeExist("AD_CodPersona") Then
    nPers = objPipe.PipeGet("AD_CodPersona")
    objPipe.PipeRemove ("AD_CodPersona")
  End If
  If objPipe.PipeExist("AD_NumHistoria") Then
    txtHistoria.Text = objPipe.PipeGet("AD_NumHistoria")
    objPipe.PipeRemove ("AD_NumHistoria")
  End If
  Call pIniciar
End Sub

Private Sub Formatear_grid()
With grdDBGrid1(1)
    
    .Columns(0).Caption = "Fecha Realizacion"
    .Columns(0).Width = 1800
    .Columns(1).Caption = "Actuaci�n"
    .Columns(1).Width = 1800
    .Columns(2).Caption = "Cod.Dpto"
    .Columns(2).Visible = False
    .Columns(2).Width = 800
    .Columns(3).Caption = "Actuaci�n Planificada"
    .Columns(3).Visible = False
    .Columns(3).Width = 1900
    .Columns(4).Caption = "Departamento Responsable"
    .Columns(4).Width = 1700
    .Columns(5).Caption = "Doctor Responsable"
    .Columns(5).Width = 2000
    .Columns(6).Width = 2300
    .Columns(6).Caption = "A�adido"
    .Columns(7).Caption = "Estado"
    .Columns(7).Width = 1000
    .Columns(8).Caption = "Cantidad"
    .Columns(8).Width = 1000
  End With
End Sub
Private Sub pLimpiar()
'txtHistoria.Text = ""
txtPaciente.Text = ""
txtFNacim.Text = ""

grdDBGrid1(1).RemoveAll

End Sub
Private Sub pImprimir(ssgrid1 As SSDBGrid)
Dim strWhere As String
Dim i As Integer

    For i = 0 To ssgrid1.SelBookmarks.Count - 1
        strWhere = strWhere & "PR04NUMACTPLAN= " & Val(ssgrid1.Columns("Actuaci�n Planificada").CellText(ssgrid1.SelBookmarks(i))) & " OR "
    Next i
    strWhere = Left$(strWhere, Len(strWhere) - 4)
    If ssgrid1.SelBookmarks.Count > 0 Then
        Call Imprimir_API(strWhere, "Peticion.rpt")
    End If
    
'Dim vntdata(1) As Variant
'Dim i As Integer
'Dim sql As String
'
'  For i = 0 To ssgrid1.SelBookmarks.Count - 1
'    sql = sql & "PR0400.PR04NUMACTPLAN= " & Val(ssgrid1.Columns("Actuaci�n Planificada").CellText(ssgrid1.SelBookmarks(i))) & " OR "
'  Next i
'  sql = Left$(sql, Len(sql) - 4)
'  vntdata(1) = sql
'  Call objsecurity.LaunchProcess("PR2400", vntdata(1))

End Sub
