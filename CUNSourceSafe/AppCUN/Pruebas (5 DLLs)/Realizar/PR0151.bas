Attribute VB_Name = "modConstantes"
Option Explicit
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long 'para cargar los TreeViews a gran velocidad

'Constantes de los Tipos de Asistencia
Public Const constASIST_HOSP = 1
Public Const constASIST_AMBUL = 2

'Constantes de los Estados de las Camas
Public Const constCAMA_LIBRE = 1
Public Const constCAMA_OCUPADA = 2
Public Const constCAMA_RESERVADA = 6

'Constantes de los Departamentos
Public Const constDPTO_QUIROFANO = 213
Public Const constDPTO_ANESTESIA = 223
Public Const constDPTO_URGENCIAS = 216
Public Const constDPTO_ONCOLOGIA = 109
Public Const constDPTO_RADIOTERAPIA = 210
Public Const constDPTO_ENDOSCOPIAS = 218
Public Const constDPTO_ANATOMIA = 204


'Constantes de los estados de las actuaciones
Public Const constESTACT_PLANIFICADA = 1
Public Const constESTACT_CITADA = 2
Public Const constESTACT_REALIZANDOSE = 3
Public Const constESTACT_REALIZADA = 4
Public Const constESTACT_INFORMADA = 5
Public Const constESTACT_CANCELADA = 6

'Constantes de los estados de las citas
Public Const constESTCITA_CITADA = 1
Public Const constESTCITA_ANULADA = 2
Public Const constESTCITA_RECITADA = 3
Public Const constESTCITA_PENDRECITAR = 4
Public Const constESTCITA_RESERVADA = 5

'Constantes para los tipos de actividades
Public Const constACTIV_CONSULTA = 201
Public Const constACTIV_PRUEBA = 203
Public Const constACTIV_INTERVENCION = 207
Public Const constACTIV_TRATAMIENTO = 208
Public Const constACTIV_HOSPITALIZACION = 209
Public Const constACTIV_INFORME = 215

'Constantes para las preguntas de los cuestionarios
Public Const constPREG_DURACION = 1041
Public Const constPREG_ANESTESIA = 1042
Public Const constPREG_QUIROFANO = 1043
Public Const constPREG_ORDEN = 1044

'Constantes de los Quir�fanos
Public Const constTIPORECQUIROFANO = 33
Public Const constQUIROFANO_01 = 577
Public Const constQUIROFANO_02 = 578
Public Const constQUIROFANO_03 = 579
Public Const constQUIROFANO_04 = 580
Public Const constQUIROFANO_05 = 581
Public Const constQUIROFANO_06 = 582
Public Const constQUIROFANO_07 = 583
Public Const constQUIROFANO_08 = 584
Public Const constQUIROFANO_10 = 585
Public Const constQUIROFANO_11 = 586

'Constantes de las categor�as o puestos de los usuarios
Public Const constPUESTO_CONSULTOR = 1
Public Const constPUESTO_ENFERMERA = 2
Public Const constPUESTO_COLABORADOR = 5
Public Const constPUESTO_RESIDENTE = 6

'Constantes de las categorias de los recursos
Public Const constCATEGORIA_MEDICO = 1


'Constantes de los estados de la Hoja de Quir�fano
Public Const constESTADOHQ_ENPROCESO = 0
Public Const constESTADOHQ_FINALIZADA = 1

'Constantes para los tipos de Anestesia
Public Const constANESTESIA_LOCAL = "L"

'Revision y nuevo de Oncologia
Public Const const_NUEVOONCOLOGIA = 3238
Public Const const_REVONCOLOGIA = 3239

'Constantes para la citaci�n de consultas de Urgencias
Public Const constURGENCIAS_PR_CONSULTA = 4293
Public Const constURGENCIAS_PR_VISITA = 4298
Public Const constURGENCIAS_REC_PASTRANA = 650
Public Const constURGENCIAS_DR_PASTRANA = "HPA"
Public Const constURGENCIAS_DR_PASTRANA_NAME = "Dr. Pastrana"
Public Const constURGENCIAS_REC_CONSCOLURG = 311
Public Const constURGENCIAS_DR_CONSCOLURG = "CCU"

' Estados de las camas
Global Const constCAMALIBRE = 1
Global Const constCAMAOCUPADA = 2
Global Const constCAMABAJA = 3
Global Const constCAMAATENCIONSOCIAL = 4
Global Const constCAMADESINFECCION = 5
Global Const constCAMARESERVADA = 6
Global Const constCAMAORDENFAC = 7
Global Const constCAMAPACQUIROFANO = 8
Global Const constCAMAMANTENIMIENTO = 9
Global Const constCAMAOCUPADALIBREHOY = 10

' Tipos de camas
Global Const constCAMAQUIROFANO = 16
Global Const constCAMASALADESPERTAR = 17
Global Const constCAMAPREANESTESIA = 15

' C�digos de actuaci�n
Global Const constCPRBIOPSIA = 3809

' Preguntas
Global Const constCPREGMOTIVO = 378

' C�digo de categor�a de doctor
Global Const constCODDOCTOR = 1

' Categor�as (AD3100)
Global Const constCATDOCTOR = "(1, 5, 9)"

' Tipo de recurso colaborador-consultor de endoscopias
Global Const constTIRECDRENDOSCOPIAS = 11

' Cod de los tipos econ�micos y entidad de investigaci�n
Global Const constTIECOINVESTIGACION = "H"
Global Const constENTIDADINVESTIGACION = "H"

' Formas de mostrar Visi�n global
Global Const constMOSTRARASIST = 1
Global Const constMOSTRARRESP = 2

' Constantes para imprimir la pet. de biopsia
Public Const interlinea1 = 500
Public Const interlinea2 = 300
Public Const margensuperior = 1300
Public Const margenizquierdo = 1000


' Tipos de paciente
Global Const constTIPACNUEVO = 1
Global Const constTIPACNUEVOREVISION = 2
Global Const constTIPACREVISION = 3
Global Const constTIPACEXPLORACIONES = 4
Global Const constTIPACSEGCONSULTA = 5
Global Const constTIPACDONANTE = 6
Global Const constTIPACPRODUCTOS = 7

' Tipos de asistencia
Global Const constTIASISTHOSPITALIZACION = 1
Global Const constTIASISTAMBULATORIA = 2
Global Const constTIASISTEXPLORACION = 3
Global Const constTIASISTSEGOPINION = 4
Global Const constTIASISTDONANTE = 5
Global Const constTIASISTPRODUCTOS = 6


' Colores
Enum reColor
  constAzulClaro = 16776960
  constNegro = 0
  constBlanco = 16777215
  constAmarilloClaro = 12648447
  constAmarilloOscuro = 65535
  constVerde = 9895830
  constVerdeOscuro = 46080
  constRojo = 255
  constRojoClaro = 45055
  constAzul = 16711680
  constCyan = &HFFFF80
  constGris = &HC0C0C0
  constMorado = 9830600
  constLila = 16711935
End Enum


