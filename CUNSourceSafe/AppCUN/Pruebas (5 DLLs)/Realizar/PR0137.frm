VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{4407CEBF-F3CC-11D2-84F3-00C04FA79FD2}#1.0#0"; "IdPerson.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form frmPeticPend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Realizaci�n de Actuaciones. Listar Actuaciones Pendientes"
   ClientHeight    =   8325
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11655
   ControlBox      =   0   'False
   Icon            =   "PR0137.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8325
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   5040
      Top             =   3840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Consultar"
      Height          =   375
      Left            =   4800
      TabIndex        =   25
      Top             =   690
      Width           =   1575
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   0
      Top             =   1080
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   1
      Left            =   240
      TabIndex        =   2
      Top             =   1080
      Width           =   11340
      Begin idperson.IdPersona IdPersona1 
         Height          =   1335
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   2355
         BackColor       =   12648384
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Datafield       =   "CI21CodPersona"
         MaxLength       =   7
         blnAvisos       =   0   'False
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8040
      Width           =   11655
      _ExtentX        =   20558
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin TabDlg.SSTab tabTab1 
      Height          =   5175
      HelpContextID   =   90001
      Index           =   0
      Left            =   240
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2880
      Width           =   11340
      _ExtentX        =   20003
      _ExtentY        =   9128
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   529
      WordWrap        =   0   'False
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Peticiones Pendientes"
      TabPicture(0)   =   "PR0137.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdConsEstPac"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdRecibirPaciente"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "cmdCancelarPrueba"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "cmdrecact(0)"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "chksinfecha"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdinfadi(2)"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Frame1"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Frame2"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "cmdcamasis(0)"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).ControlCount=   10
      TabCaption(1)   =   "Actuaciones en Espera"
      TabPicture(1)   =   "PR0137.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdrecact(1)"
      Tab(1).Control(1)=   "cmdEliminarCola"
      Tab(1).Control(2)=   "fraFrame1(2)"
      Tab(1).ControlCount=   3
      Begin VB.CommandButton cmdcamasis 
         Caption         =   "Cambios Asistencias"
         Height          =   375
         Index           =   0
         Left            =   9360
         TabIndex        =   32
         Top             =   3600
         Width           =   1815
      End
      Begin VB.Frame Frame2 
         Caption         =   "Listado"
         Height          =   735
         Left            =   5160
         TabIndex        =   28
         Top             =   4200
         Width           =   5895
         Begin VB.CommandButton CmdConsulta 
            Caption         =   "Imprimir"
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   4440
            TabIndex        =   31
            Top             =   240
            Width           =   1095
         End
         Begin SSDataWidgets_B.SSDBCombo SSDBConsulta 
            Height          =   330
            Index           =   0
            Left            =   840
            TabIndex        =   29
            Top             =   240
            Width           =   3375
            DataFieldList   =   "Column 1"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            FieldSeparator  =   ";"
            DefColWidth     =   2
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3598
            Columns(0).Caption=   "COD. DOCTOR"
            Columns(0).Name =   "COD. DPTO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   5927
            Columns(1).Caption=   "DESCRIPCI�N"
            Columns(1).Name =   "DESCRIPCI�N"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5953
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label Label1 
            Caption         =   "Doctor:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   30
            Top             =   360
            Width           =   735
         End
      End
      Begin VB.Frame Frame1 
         Height          =   735
         Left            =   120
         TabIndex        =   18
         Top             =   4200
         Width           =   4815
         Begin VB.CommandButton Command6 
            Caption         =   "Hoja de Datos"
            Height          =   375
            Left            =   3360
            TabIndex        =   33
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton cmdPeticion 
            Caption         =   "Petici�n"
            Height          =   375
            Left            =   1920
            TabIndex        =   27
            Top             =   240
            Width           =   1095
         End
         Begin VB.CommandButton cmdhojas 
            Caption         =   "Hojas Afiliaci�n"
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   26
            Top             =   240
            Width           =   1575
         End
      End
      Begin VB.CommandButton cmdinfadi 
         Caption         =   "Informaci�n Actuaci�n"
         Height          =   375
         Index           =   2
         Left            =   7320
         TabIndex        =   17
         Top             =   3600
         Width           =   1935
      End
      Begin VB.CheckBox chksinfecha 
         Caption         =   "Ver actuaciones sin fecha"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   7680
         TabIndex        =   16
         Top             =   360
         Width           =   2775
      End
      Begin VB.CommandButton cmdrecact 
         Caption         =   "Recursos Actuaci�n"
         Height          =   375
         Index           =   1
         Left            =   -68760
         TabIndex        =   15
         Top             =   4680
         Width           =   1935
      End
      Begin VB.CommandButton cmdEliminarCola 
         Caption         =   "Deshacer Entrada"
         Height          =   375
         Left            =   -72360
         TabIndex        =   14
         Top             =   4680
         Width           =   1935
      End
      Begin VB.CommandButton cmdrecact 
         Caption         =   "Recursos Actuaci�n"
         Height          =   375
         Index           =   0
         Left            =   5520
         TabIndex        =   13
         Top             =   3600
         Width           =   1695
      End
      Begin VB.CommandButton cmdCancelarPrueba 
         Caption         =   "Cancelar Prueba"
         Height          =   375
         Left            =   3720
         TabIndex        =   12
         Top             =   3600
         Width           =   1695
      End
      Begin VB.CommandButton cmdRecibirPaciente 
         Caption         =   "Recibir Paciente"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   3600
         Width           =   1695
      End
      Begin VB.CommandButton cmdConsEstPac 
         Caption         =   "Estado Paciente"
         Height          =   375
         Left            =   1920
         TabIndex        =   10
         Top             =   3600
         Width           =   1695
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Actuaciones en Espera"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4080
         Index           =   2
         Left            =   -74880
         TabIndex        =   8
         Top             =   480
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3585
            Index           =   1
            Left            =   120
            TabIndex        =   9
            TabStop         =   0   'False
            Top             =   360
            Width           =   10680
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18838
            _ExtentY        =   6324
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Actuaciones Pendientes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3000
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   11055
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2505
            Index           =   0
            Left            =   120
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   360
            Width           =   10800
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            SelectTypeRow   =   1
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   -2147483643
            BackColorOdd    =   -2147483643
            RowHeight       =   423
            SplitterPos     =   1
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19050
            _ExtentY        =   4419
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2130
         Index           =   2
         Left            =   -74880
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   90
         Width           =   10005
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         SelectTypeRow   =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   17648
         _ExtentY        =   3757
         _StockProps     =   79
         Caption         =   "PACIENTES"
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
      Height          =   330
      Index           =   0
      Left            =   8160
      TabIndex        =   19
      Top             =   690
      Width           =   3135
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      DefColWidth     =   2
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   2540
      Columns(0).Caption=   "COD. DPTO"
      Columns(0).Name =   "COD. DPTO"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   5927
      Columns(1).Caption=   "DESCRIPCI�N"
      Columns(1).Name =   "DESCRIPCI�N"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   0
      Left            =   360
      TabIndex        =   21
      Tag             =   "Fecha Desde"
      ToolTipText     =   "Fecha Desde"
      Top             =   690
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
      Height          =   330
      Index           =   1
      Left            =   2640
      TabIndex        =   22
      Tag             =   "Fecha Hasta"
      ToolTipText     =   "Fecha Hasta"
      Top             =   720
      Width           =   1935
      _Version        =   65537
      _ExtentX        =   3413
      _ExtentY        =   582
      _StockProps     =   93
      BackColor       =   16776960
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MinDate         =   "1997/1/1"
      MaxDate         =   "2050/12/31"
      Format          =   "DD/MM/YYYY"
      AllowNullDate   =   -1  'True
      BackColorSelected=   8388608
      BevelColorFace  =   12632256
      AutoSelect      =   0   'False
      ShowCentury     =   -1  'True
      Mask            =   2
      NullDateLabel   =   "__/__/____"
      StartofWeek     =   2
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Desde"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   360
      TabIndex        =   24
      Top             =   480
      Width           =   1140
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Hasta"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   2685
      TabIndex        =   23
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Selecci�n del   Departamento:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   6720
      TabIndex        =   20
      Top             =   690
      Width           =   1335
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPeticPend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Constante para Botones 'Mariajo 14/6/99
Const iBUTLISTA = 0
'Constante para combos 'Mariajo 14/6/99
Const iCBODOCTOR = 0

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm



'MIRAMOS SI VAMOS A SACAR LAS ACTUACIONES CON O SIN FECHA
Private Sub chksinfecha_Click()
If chksinfecha.Value = 0 Then
objMultiInfo1.strWhere = "PR0409J.AD07CODPROCESO IS NOT NULL " & _
                 " AND PR0409J.AD01CODASISTENCI IS NOT NULL" & _
                 " AND FECHA < TO_DATE('" & _
                 dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI') " & _
                 "AND FECHA > = TO_DATE('" & Trim(dtcDateCombo1(0).Text) & _
                 "','DD/MM/YYYY') AND PR0409J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value
     
End If
If chksinfecha.Value = 1 Then
  objMultiInfo1.strWhere = "PR0409J.AD07CODPROCESO IS NOT NULL " & _
                                         " AND PR0409J.AD01CODASISTENCI IS NOT NULL" & _
                                         " AND PR0409J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value & _
                                         " AND PR0409J.FECHA is null"
 
End If
objWinInfo.DataRefresh
End Sub

Private Sub cmdcamasis_Click(Index As Integer)
    ReDim vntdata(2) As Variant
    'Pasa el c�digo de persona para mostrarla despu�s
    vntdata(1) = IdPersona1.Text
    vntdata(2) = grdDBGrid1(0).Columns(15).Value
'''    vntData(2) = 0
'''    vntData(3) = IdPersona1.Text
'''    vntData(4) = "PETICIONESPENDIENTES"
    Call objsecurity.LaunchProcess("AD0115", vntdata)
    objWinInfo.DataRefresh
    
End Sub

Private Sub cmdCancelarPrueba_Click()
'Llama a la pantalla de cancelar actuaciones o anular las cancelaciones
    'Pasa el n�mero de actuaci�n planificada
    glngnumactplancan = grdDBGrid1(0).Columns(9).Value
    'Load frmcancelar
    'Call frmcancelar.Show(vbModal)
    Call objsecurity.LaunchProcess("PR0132")
    'Unload frmcancelar
    'Set frmcancelar = Nothing
    'Regresca la pantalla
    objWinInfo.DataRefresh
End Sub

Private Sub cmdConsEstPac_Click()
'Llama a la pantalla de consultar el estado del paciente

    'Pasa el c�digo del paciente
    'glngcodpaciente = IdPersona1.Text
    frmsituacionpaciente.IdPersona1.Text = IdPersona1.Text
    'Load frmsituacionpaciente
    'Call frmsituacionpaciente.Show(vbModal)
    Call objsecurity.LaunchProcess("PR0173")
    'Unload frmsituacionpaciente
    'Set frmsituacionpaciente = Nothing

End Sub

Private Sub CmdConsulta_Click(Index As Integer)
    
    Dim strWhereTotal As String
    Dim strTipoHoja As String
    Dim intTipoImpresora As Integer
    Dim intCont As Integer
    Dim strsql As String
    Dim rdprocAsis As rdoResultset
    
    If SSDBCombo1(0).Text = "" Then
        MsgBox "Por favor, seleccione un departamento", vbOKOnly
        Exit Sub
    End If
    If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
           If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
             Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
             Call objError.Raise
             Exit Sub
           End If
    End If
    'Si no han introducido fechas, error
    If Not IsDate(dtcDateCombo1(0).Date) Then                   'Or Not IsDate(dtcDateCombo1(1).Date) Then
        MsgBox "Seleccione la Fecha Desde para obtener el Reporte", vbOKOnly
        Exit Sub
    End If
    strWhereTotal = " {PR0409J.AD02CODDPTO}= " & SSDBCombo1(0).Columns(0).Text & " "
    'Si han seleccionado un doctor, error
    If SSDBConsulta(iCBODOCTOR).Text <> "" Then
        strWhereTotal = strWhereTotal + " AND {AG1100.AG11CODRECURSO}= " & SSDBConsulta(iCBODOCTOR).Columns(0).Text & " "
    End If
        
    With crtCrystalReport1
        'date(1999,6,14)
        .Formulas(1) = "fecha = date(" & Right(dtcDateCombo1(0).Text, 4) & "," & Trim(Mid(dtcDateCombo1(0).Text, 4, 2)) & "," & Trim(Left(dtcDateCombo1(0).Text, 2)) & ")"
        .ReportFileName = objApp.strReportsPath & "PR0137.rpt"
        .PrinterCopies = 1
        '.Destination = crptToPrinter '21/6/1999  Cambiamos a Pantalla el reporte
        .Destination = crptToWindow
        .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
         Me.MousePointer = vbHourglass
        .Action = 1
         Me.MousePointer = vbDefault
    End With
    'SSDBConsulta(iCBODOCTOR).SetFocus
End Sub

Private Sub cmdEliminarCola_Click()
    Dim strupdate As String
    'Introducimos la fecha de la entrada en la cola
    strupdate = "UPDATE PR0400 SET PR04FECENTRCOLA = null" & _
       " WHERE pr04numactplan=" & grdDBGrid1(1).Columns(10).Value
    objApp.rdoConnect.Execute strupdate, 64
    objWinInfo.DataRefresh
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End Sub

Private Sub cmdhojas_Click(Index As Integer)
   
    Dim strWhereTotal As String
    Dim strTipoHoja As String
    Dim intTipoImpresora As Integer
    Dim intCont As Integer
    Dim strsql As String
    Dim rdprocAsis As rdoResultset
    
    If (InStr(Printer.DeviceName, "HP LaserJet 4Si") <> 0) Then
        intTipoImpresora = 1
    ElseIf (InStr(Printer.DeviceName, "HP LaserJet 6P/6MP") <> 0) Then
        intTipoImpresora = 2
    Else
        intTipoImpresora = 2
    End If
    If grdDBGrid1(0).Columns(9).Value = "" Then
        MsgBox "Seleccione una peticion", vbOKOnly
        Exit Sub
    End If
    strsql = "SELECT AD01CODASISTENCI, AD07CODPROCESO FROM PR0400 WHERE " & _
    "PR04NUMACTPLAN=" & grdDBGrid1(0).Columns(9).Value
    Set rdprocAsis = objApp.rdoConnect.OpenResultset(strsql)
    If rdprocAsis.EOF Then
        MsgBox "Asocie proceso asistencia", vbOKOnly
    Else
        strWhereTotal = "({Ad0823j.AD01CODASISTENCI}=" & rdprocAsis.rdoColumns(0) & _
        "AND {Ad0823j.AD07CODPROCESO}=" & rdprocAsis.rdoColumns(1) & ")"
                
        For intCont = 0 To 2
            Select Case intCont
                Case 0:
                    crtCrystalReport1.ReportFileName = objApp.strReportsPath & "filiacion" & _
                    intTipoImpresora & ".rpt"
                 Case 1:
                    crtCrystalReport1.ReportFileName = objApp.strReportsPath & "hojaverde" & _
                    intTipoImpresora & ".rpt"
                 Case 2:
                    crtCrystalReport1.ReportFileName = objApp.strReportsPath & "hojablanca" & _
                    intTipoImpresora & ".rpt"
            End Select
            With crtCrystalReport1
                .PrinterCopies = 1
                .Destination = crptToPrinter
                .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
                .Destination = crptToPrinter
            '        .Destination = crptToWindow
                .Connect = objApp.rdoConnect.Connect
                .DiscardSavedData = True
                Me.MousePointer = vbHourglass
                .Action = 1
            '        .PrintReport
                Me.MousePointer = vbDefault
            End With
        Next intCont
    End If
End Sub

Private Sub cmdinfadi_Click(Index As Integer)
   
   Call objsecurity.LaunchProcess("PR0229")

End Sub

Private Sub cmdPeticion_Click()
  Dim strWhereTotal As String
  Dim vntActPlan(25) As Variant
  Dim fecha As String
  Dim Hora As String
  Dim strReportName As String
  Dim lngPersona As Long
  Dim i As Integer
  Dim intA As Integer
  
  If grdDBGrid1(0).Columns(9).Value = "" Then
    MsgBox "Seleccione una peticion", vbOKOnly
    Exit Sub
  Else
    vntActPlan(0) = grdDBGrid1(0).Columns(9).Value
    fecha = grdDBGrid1(0).Columns("Fecha").Value
    Hora = grdDBGrid1(0).Columns("Hora").Value
    lngPersona = grdDBGrid1(0).Columns("Cod paciente").Value
  End If
  intA = 1
  strReportName = "Peticion.rpt"
  grdDBGrid1(0).MoveFirst
  For i = 1 To grdDBGrid1(0).Rows
    If grdDBGrid1(0).Columns("Fecha").Value = fecha And _
    grdDBGrid1(0).Columns("Cod paciente").Value = lngPersona And vntActPlan(0) <> grdDBGrid1(0).Columns(9).Value Then
        vntActPlan(intA) = grdDBGrid1(0).Columns(9).Value
        intA = intA + 1
    End If
    grdDBGrid1(0).MoveNext
  Next i
  If intA > 1 Then
    i = MsgBox("Desea unificar en la misma peticion las Actuaciones con la misma hora?", vbYesNo)
    If i = vbNo Then
        intA = 1
    Else
     strReportName = "PetAgrup.rpt"
    End If
  End If
  strWhereTotal = "({PR0457J.PR04NUMACTPLAN}=" & vntActPlan(0) & ")"
  For i = 1 To intA - 1
    strWhereTotal = strWhereTotal & " OR ({PR0457J.PR04NUMACTPLAN}=" & vntActPlan(i) & ")"
  Next i
  crtCrystalReport1.ReportFileName = objApp.strReportsPath & strReportName
  With crtCrystalReport1
    .PrinterCopies = 1
    .Destination = crptToPrinter
    .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
    .Destination = crptToPrinter
    .Connect = objApp.rdoConnect.Connect
    .DiscardSavedData = True
     Me.MousePointer = vbHourglass
    .Action = 1
     Me.MousePointer = vbDefault
  End With
End Sub

Private Sub cmdrecact_Click(Index As Integer)
   
   gstrLlamadorSel = ""
   If Index = 0 Then
    gstrLlamadorSel = "Peticiones Pendientes 0"
    'gstrLlamadorSel = grdDBGrid1(0).Columns(9).Value
    frmRecPacRecur.IdPersona1 = IdPersona1
    frmRecPacRecur.txtacttext1(0).Text = grdDBGrid1(0).Columns(9).Value
    frmRecPacRecur.txtacttext1(1).Text = grdDBGrid1(0).Columns(8).Value
   Else
    gstrLlamadorSel = "Peticiones Pendientes 1"
    'gstrLlamadorSel = grdDBGrid1(1).Columns(10).Value
    frmRecPacRecur.IdPersona1 = IdPersona1
    frmRecPacRecur.txtacttext1(0).Text = grdDBGrid1(1).Columns(10).Value
    frmRecPacRecur.txtacttext1(1).Text = grdDBGrid1(1).Columns(8).Value
   End If
   Call objsecurity.LaunchProcess("PR0197")
   gstrLlamadorSel = ""

End Sub

Private Sub Command5_Click()
If SSDBCombo1(0).Text = "" Then
    MsgBox "Por favor, seleccione un departamento", vbOKOnly
    Exit Sub
End If
If IsDate(dtcDateCombo1(1).Date) And IsDate(dtcDateCombo1(0).Date) Then
        If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          Call objError.SetError(cwCodeMsg, "La Fecha Hasta es  menor que Fecha Desde")
          Call objError.Raise
          Exit Sub
        End If
End If
Me.MousePointer = vbHourglass
objMultiInfo1.strWhere = "PR0409J.AD07CODPROCESO IS NOT NULL " & _
                 " AND PR0409J.AD01CODASISTENCI IS NOT NULL" & _
                 " AND FECHA < TO_DATE('" & _
                 dtcDateCombo1(1).Text & " 23:59','DD/MM/YYYY HH24:MI') " & _
                 "AND FECHA > = TO_DATE(' " & dtcDateCombo1(0).Text & _
                 "','DD/MM/YYYY') AND PR0409J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value
objMultiInfo2.strWhere = "PR0419J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value
objWinInfo.DataRefresh
Me.MousePointer = vbDefault
End Sub


Private Sub Command6_Click()
    Dim strsql As String
    Dim rstProcAsis As rdoResultset
    Dim qryProcAsis As rdoQuery
   ' Dim rstTipPacien As rdoResultset
   ' Dim qryTipPacien As rdoQuery
    Dim strWhereTotal As String

  If grdDBGrid1(0).Columns(9).Value = "" Then
    MsgBox "Seleccione una peticion", vbOKOnly
    Exit Sub
  End If
  strsql = "SELECT AD01CODASISTENCI, AD07CODPROCESO FROM PR0400 WHERE " & _
           "PR04NUMACTPLAN= ?"
  Set qryProcAsis = objApp.rdoConnect.CreateQuery("", strsql)
      qryProcAsis(0) = grdDBGrid1(0).Columns(9).Value
  Set rstProcAsis = qryProcAsis.OpenResultset()
  If rstProcAsis.EOF Then
    MsgBox "Asocie proceso asistencia", vbOKOnly
  Else
    'strsql = "SELECT AD10CODTIPPACIEN FROM AD0800 WHERE " & _
    '         "AD07CODPROCESO= ? AND AD01CODASISTENCI= ?"
    'Set qryTipPacien = objApp.rdoConnect.CreateQuery("", strsql)
    '    qryTipPacien(0) = rstProcAsis(1)
    '    qryTipPacien(1) = rstProcAsis(0)
    'Set rstTipPacien = qryTipPacien.OpenResultset()
    'If rstTipPacien(0) <> "1" Then ' no nuevo
    '   MsgBox "Este paciente no es nuevo", vbOKOnly
    '   Exit Sub
    'End If

    strWhereTotal = "({Ad0823j.AD01CODASISTENCI}=" & rstProcAsis(0) & _
               " AND {Ad0823j.AD07CODPROCESO}=" & rstProcAsis(1) & ")"
                

    crtCrystalReport1.ReportFileName = objApp.strReportsPath & "ConfirmarFiliacion.rpt"
    With crtCrystalReport1
      .PrinterCopies = 1
      .Destination = crptToPrinter
      .SelectionFormula = objGen.ReplaceStr(strWhereTotal, "#", Chr(34), 0)
      .Destination = crptToPrinter
      .Connect = objApp.rdoConnect.Connect
      .DiscardSavedData = True
       Me.MousePointer = vbHourglass
      .Action = 1
       Me.MousePointer = vbDefault
    End With
   ' rstTipPacien.Close
   ' qryTipPacien.Close
  End If
  rstProcAsis.Close
  qryProcAsis.Close
End Sub

Private Sub Form_Activate()
  objWinInfo.DataRefresh
    
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
      
End Sub
Private Sub Form_Load()
  Dim strKey As String
  
  Call objApp.AddCtrl(TypeName(IdPersona1))
  Call IdPersona1.BeginControl(objApp, objGen)
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
    
  With objMultiInfo1
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "PR0409J"
    .strWhere = "PR0409J.AD07CODPROCESO IS NOT NULL " & _
                 " AND PR0409J.AD01CODASISTENCI IS NOT NULL" & _
                 " AND FECHA < TO_DATE('" & _
                 strFecha_Sistema & " 23:59','DD/MM/YYYY HH24:MI') " & _
                 " AND FECHA > = SYSDATE AND PR0409J.AD02CODDPTO='999'"

    Call .objPrinter.Add("PR0137", "Listado de Peticiones Pendientes")
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0
    'El grid estar� ordenado por la fecha de la consulta
    
    Call .FormAddOrderField("CI22PRIAPEL", cwAscending)
    Call .FormAddOrderField("CI22SEGAPEL", cwAscending)
    Call .FormAddOrderField("CI22NOMBRE", cwAscending)
    Call .FormAddOrderField("FECHA", cwAscending)
  
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Pendientes")
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "PR37DESESTADO", "Estado", cwString)
    
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "PR37DESESTADO", "Estado")
  End With
  
  With objMultiInfo2
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "PR0419J"
    .strWhere = "AD02CODDPTO='999'"
    
    .intAllowance = cwAllowReadOnly
    .intCursorSize = 0

    'Se ordena el grid por la fecha de la citaci�n
    Call .FormAddOrderField("FECHA", cwAscending)
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Peticiones Pendientes")
    Call .FormAddFilterWhere(strKey, "FECHA", "Fecha", cwDate)
    Call .FormAddFilterWhere(strKey, "HORA", "Hora Cita", cwString)
    Call .FormAddFilterWhere(strKey, "CI22NOMBRE", "Nombre", cwString)
    Call .FormAddFilterWhere(strKey, "CI22PRIAPEL", "Primer Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "CI22SEGAPEL", "Segundo Apellido", cwString)
    Call .FormAddFilterWhere(strKey, "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere(strKey, "HORACOLA", "Hora entrada cola", cwString)
    
    Call .FormAddFilterOrder(strKey, "FECHA", "Fecha")
    Call .FormAddFilterOrder(strKey, "HORA", "Hora Cita")
    Call .FormAddFilterOrder(strKey, "CI22NOMBRE", "Nombre")
    Call .FormAddFilterOrder(strKey, "CI22PRIAPEL", "Primer Apellido")
    Call .FormAddFilterOrder(strKey, "CI22SEGAPEL", "Segundo Apellido")
    Call .FormAddFilterOrder(strKey, "PR01DESCORTA", "Actuaci�n")
    Call .FormAddFilterOrder(strKey, "HORACOLA", "Hora entrada cola")
    
  End With
  
  With objWinInfo
  
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo1, "Fecha", "FECHA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Hora", "HORA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo1, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo1, "Cod act pedi", "PR04numactplan", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Cod paciente", "CI21CODPERSONA", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "Estado", "PR37DESESTADO", cwString, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado", "PR37CODESTADO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Estado Muestra", "PR56CODESTMUES", cwNumeric, 10)
    'Proceso y asistencia
    Call .GridAddColumn(objMultiInfo1, "C�d.Proceso", "AD07CODPROCESO", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo1, "C�d.Asistencia", "AD01CODASISTENCI", cwNumeric, 10)
    'CAMA
    Call .GridAddColumn(objMultiInfo1, "Cama", "CAMA", cwString, 6)
    Call .GridAddColumn(objMultiInfo1, "Act.Ped", "PR03NUMACTPEDI", cwNumeric, 10)
    Call .FormCreateInfo(objMultiInfo1)
    
    'La primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    
    Call .FormChangeColor(objMultiInfo1)
  
    .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnInFind = True
    
    'Los campos no estar�n visibles en el grid
    grdDBGrid1(0).Columns(9).Visible = False
    grdDBGrid1(0).Columns(10).Visible = False
    grdDBGrid1(0).Columns(12).Visible = False
    grdDBGrid1(0).Columns(13).Visible = False
    
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo2, "Fecha", "FECHA", cwDate, 8)
    Call .GridAddColumn(objMultiInfo2, "Hora", "HORA", cwString, 8)
    Call .GridAddColumn(objMultiInfo2, "Nombre", "CI22NOMBRE", cwString, 25)
    Call .GridAddColumn(objMultiInfo2, "Primer Apellido", "CI22PRIAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo2, "Segundo Apellido", "CI22SEGAPEL", cwString, 25)
    Call .GridAddColumn(objMultiInfo2, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo2, "Hora entrada cola", "HORACOLA", cwString, 10)
    Call .GridAddColumn(objMultiInfo2, "Cod act pedi", "PR04numactplan", cwString, 10)
    Call .GridAddColumn(objMultiInfo2, "Cod paciente", "CI21CODPERSONA", cwNumeric, 10)
    Call .GridAddColumn(objMultiInfo2, "Estado", "PR37DESESTADO", cwString, 10)
    
    Call .FormCreateInfo(objMultiInfo2)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).intKeyNo = 1
    Call .FormChangeColor(objMultiInfo2)

    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(7)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnInFind = True
    
    'Hacemos invisibles la columnas del grid
    grdDBGrid1(1).Columns(10).Visible = False
    grdDBGrid1(1).Columns(11).Visible = False
    IdPersona1.blnSearchButton = False
      '**************MARIA
  Dim sqlstr As String
    Dim rsta As rdoResultset
    Dim sqlstrdpto As String
    Dim rstadpto As rdoResultset

    sqlstr = "SELECT AD0200.AD02CODDPTO,AD0200.AD02DESDPTO FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objsecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL ORDER BY AD0200.AD02DESDPTO"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    While rsta.EOF = False
        SSDBCombo1(0).AddItem rsta("AD02CODDPTO").Value & ";" & rsta("AD02DESDPTO").Value
        SSDBCombo1(0).Text = rsta("AD02DESDPTO").Value
        SSDBCombo1(0).MoveNext
        rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
     '**************MARIA
    SSDBCombo1(0).Enabled = True
    SSDBCombo1(0).Text = ""
    Call .WinRegister
    Call .WinStabilize
  End With
  
  'Damos el ancho de las columnas del grid
  grdDBGrid1(0).Columns(3).Width = 1000
  grdDBGrid1(0).Columns(4).Width = 600
  grdDBGrid1(0).Columns(5).Width = 1400
  grdDBGrid1(0).Columns(6).Width = 1500
  grdDBGrid1(0).Columns(7).Width = 1500
  grdDBGrid1(0).Columns(8).Width = 2800
  grdDBGrid1(0).Columns(11).Width = 1400
  
    'Establecemos el ancho de las columnas del grid
    grdDBGrid1(1).Columns(3).Width = 1000
    grdDBGrid1(1).Columns(4).Width = 600
    grdDBGrid1(1).Columns(5).Width = 1600
    grdDBGrid1(1).Columns(6).Width = 1600
    grdDBGrid1(1).Columns(7).Width = 1600
    grdDBGrid1(1).Columns(8).Width = 2100
    grdDBGrid1(1).Columns(9).Width = 1600
    
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
    Call objWinInfo.FormChangeActive(fraFrame1(3), False, True)
    
    'PARA QUE NO SALGA EL BOTON DE BUSQUEDA DEL ID PERSONA
    IdPersona1.blnSearchButton = False
    

End Sub



Private Sub grdDBGrid1_Click(Index As Integer)
Call CtrBotonDeshacer
End Sub

'Private Sub SSDBCombo1_Click(Index As Integer)
'
'If Index = 0 And SSDBCombo1(0).Text <> "" Then
'******************************************************************
'   Cambio de departamento
'******************************************************************
'   Modifico la strWhere para obtener los nuevos registro del
'   departamento elegido y refresco el cursor
'    With objWinInfo
'      .objWinActiveForm.strInitialWhere = "AG0419J.AD02CODDPTO=" & SSDBCombo1(0).Value
'      .objWinActiveForm.strWhere = "AG0419J.AD02CODDPTO=" & SSDBCombo1(0).Value
'      .DataRefresh
'    End With
'
'   Si hay alg�n recurso en ese departamento habilito los botones
'
'    If objGen.GetRowCount(objWinInfo.objWinActiveForm.rdoCursor) > 0 Then
'      fraFrame1(1).Enabled = True
'    Else
'      fraFrame1(1).Enabled = False
'    End If
'  End If
'
'
'End Sub
Private Sub SSDBCombo1_KeyDown(intIndex As Integer, KeyCode As Integer, Shift As Integer)
  If intIndex = 0 And KeyCode = 46 Then
    SSDBCombo1(intIndex).Text = ""
    objWinInfo.objWinActiveForm.strWhere = ""
    objWinInfo.DataRefresh
  End If

End Sub

Private Sub SSDBCombo1_CloseUp(Index As Integer)
    
    SSDBCombo1(0).Text = SSDBCombo1(0).Columns(1).Value
    If tabTab1(0).Tab = 0 Then
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    ElseIf tabTab1(0).Tab = 1 Then
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
    End If
    dtcDateCombo1(0).Text = strFecha_Sistema()
    dtcDateCombo1(1).Text = strFecha_Sistema()
    Call chksinfecha_Click
    objMultiInfo2.strWhere = "PR0419J.AD02CODDPTO=" & SSDBCombo1(0).Columns(0).Value
    objWinInfo.DataRefresh
    IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
''    If tabTab1(0).Tab = 0 Then
''        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
''    ElseIf tabTab1(0).Tab = 1 Then
''        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
''    End If
'    IdPersona1.ReadPersona

   ' With objWinInfo
      'Text1.Text = SSDBCombo1(0).Columns(0).Value
    'End If
    'End With

 '   objWinInfo.CtrlDataChange
   ' LMF imprimir Petici�n de actuaciones pendientes en funci�n del Departamento
   'If SSDBCombo1(0).Columns(0).Value <> 209 Then ' departamento realizador
   '   cmdPeticion.Visible = False
   'Else
   '   cmdPeticion.Visible = True ' opci�n a imprimir petici�n
   'End If
   
    'Si han seleccionado un Departamento
    'Cargamos la combo de Doctores.
    If SSDBCombo1(0).Columns(0).Value <> "" Then
        pCargaCombo (iCBODOCTOR)
        CmdConsulta(iBUTLISTA).Enabled = True
    Else
        CmdConsulta(iBUTLISTA).Enabled = False
    End If
   
End Sub

'Private Sub SSDBCombo1_GotFocus(Index As Integer)
'Call objWinInfo.CtrlGotFocus
'End Sub

Private Sub SSDBCombo1_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = 0
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
If Not gblnCancelar Then
  intCancel = objWinInfo.WinExit
End If
End Sub

Private Sub Form_Unload(intCancel As Integer)
If Not gblnCancelar Then
  Call IdPersona1.EndControl
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End If
End Sub


Private Sub cmdRecibirPaciente_Click()
'Tratamos la llegada de un paciente a la cola de espera
Dim strupdate As String
Dim sacarmensaje As String
Dim mensaje As String
Dim strNumActPedi   As String

cmdRecibirPaciente.Enabled = False
  If grdDBGrid1(0).Columns(13).Value = "" Then
    grdDBGrid1(0).Columns(13).Value = 0
  End If
  If grdDBGrid1(0).Columns(12).Value = 1 And grdDBGrid1(0).Columns(13).Value = 1 Then
        mensaje = MsgBox(" El paciente tiene muestras pendientes. " & Chr(13) & _
                       " �Desea recibirle?", vbYesNo + vbExclamation, "Aviso")
    If mensaje = vbNo Then
            cmdRecibirPaciente.Enabled = True
            Exit Sub
    Else
        'Introducimos la fecha de la entrada en la cola
        strupdate = "UPDATE PR0400 SET PR04FECENTRCOLA=(SELECT SYSDATE FROM DUAL)" & _
           " WHERE pr04numactplan=" & grdDBGrid1(0).Columns(9).Value
        objApp.rdoConnect.Execute strupdate, 64
        Call pArrancarAsociadas(grdDBGrid1(0).Columns("Act.Ped").Value)
        objWinInfo.DataRefresh
        'Activamos y desactivamos los botones
        If grdDBGrid1(0).Rows = 0 Then
            cmdCancelarPrueba.Enabled = False
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            If grdDBGrid1(0).Columns(12).Value = 6 Then
                cmdRecibirPaciente.Enabled = False
                cmdConsEstPac.Enabled = False
                cmdCancelarPrueba.Enabled = True
                cmdrecact(0).Enabled = False
                cmdinfadi(2).Enabled = False
                cmdcamasis(0).Enabled = False
            Else
                cmdRecibirPaciente.Enabled = True
                cmdConsEstPac.Enabled = True
                cmdCancelarPrueba.Enabled = True
                cmdrecact(0).Enabled = True
                cmdinfadi(2).Enabled = True
                cmdcamasis(0).Enabled = True
            End If
            IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
            IdPersona1.ReadPersona
        End If
        If grdDBGrid1(1).Rows = 0 Then
            cmdEliminarCola.Enabled = False
            cmdrecact(1).Enabled = False
        Else
            cmdEliminarCola.Enabled = True
            cmdrecact(1).Enabled = True
        End If
    End If
Else
 'Introducimos la fecha de la entrada en la cola
    strupdate = "UPDATE PR0400 SET PR04FECENTRCOLA=(SELECT SYSDATE FROM DUAL)" & _
                " WHERE pr04numactplan=" & grdDBGrid1(0).Columns(9).Value
    objApp.rdoConnect.Execute strupdate, 64
    Call pArrancarAsociadas(grdDBGrid1(0).Columns("Act.Ped").Value)
    objWinInfo.DataRefresh
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End If


End Sub
Private Sub SSDBConsulta_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If iCBODOCTOR = 0 And KeyCode = 46 Then
        SSDBConsulta(Index).Text = ""
    End If
End Sub

Private Sub SSDBConsulta_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyAscii = 0
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
  End If
  objWinInfo.DataRefresh
  If grdDBGrid1(1).Rows = 0 Then
      cmdEliminarCola.Enabled = False
      cmdrecact(1).Enabled = False
  Else
      cmdEliminarCola.Enabled = True
      cmdrecact(1).Enabled = True
  End If
  If tabTab1(0).Tab = 1 Then
    Call CtrBotonDeshacer
  End If
End Sub

Private Sub tabTab1_DblClick(Index As Integer)
  If tabTab1(0).Tab = 0 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
  End If
  objWinInfo.DataRefresh
  If grdDBGrid1(1).Rows = 0 Then
      cmdEliminarCola.Enabled = False
      cmdrecact(1).Enabled = False
  Else
      cmdEliminarCola.Enabled = True
      cmdrecact(1).Enabled = True
  End If
End Sub

Private Sub Timer1_Timer()
Dim cont As Integer

cont = cont + 1
If cont = 5 Then
    cont = 0
    objWinInfo.DataRefresh
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    
If btnButton.Index = 30 Then
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Exit Sub
End If
    
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    'Activamos y desactivamos los botones
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
''''  If intIndex = 1 Then
''''    Call CtrBotonDeshacer
''''  End If
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    'Activamos y desactivamos los botones
    'If grdDBGrid1(0).Rows = 0 Or grdDBGrid1(0).Columns(12).Value = 5 Then
    '    cmdRecibirPaciente.Enabled = False
    '    cmdConsEstPac.Enabled = False
    'Else
    '    cmdRecibirPaciente.Enabled = True
    '    cmdConsEstPac.Enabled = True
    'End If
    'If grdDBGrid1(0).Rows = 0 Then
    '   cmdCancelarPrueba.Enabled = False
    'Else
    '    cmdCancelarPrueba.Enabled = True
    '    IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
    '    IdPersona1.ReadPersona
    'End If
    If grdDBGrid1(0).Rows = 0 Then
        cmdCancelarPrueba.Enabled = False
        cmdRecibirPaciente.Enabled = False
        cmdConsEstPac.Enabled = False
        cmdrecact(0).Enabled = False
        cmdinfadi(2).Enabled = False
        cmdcamasis(0).Enabled = False
    Else
        If grdDBGrid1(0).Columns(12).Value = 6 Then
            cmdRecibirPaciente.Enabled = False
            cmdConsEstPac.Enabled = False
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = False
            cmdinfadi(2).Enabled = False
            cmdcamasis(0).Enabled = False
        Else
            cmdRecibirPaciente.Enabled = True
            cmdConsEstPac.Enabled = True
            cmdCancelarPrueba.Enabled = True
            cmdrecact(0).Enabled = True
            cmdinfadi(2).Enabled = True
            cmdcamasis(0).Enabled = True
        End If
        IdPersona1.Text = grdDBGrid1(0).Columns(10).Value
        IdPersona1.ReadPersona
    End If
    If grdDBGrid1(1).Rows = 0 Then
        cmdEliminarCola.Enabled = False
        cmdrecact(1).Enabled = False
    Else
        cmdEliminarCola.Enabled = True
        cmdrecact(1).Enabled = True
    End If
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
  If intIndex = 3 Then
  SSDBCombo1(0).Enabled = True
  End If
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
'  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange

End Sub

Private Sub CtrBotonDeshacer()
    If grdDBGrid1(1).Columns(12).Value = "Realiz�ndose" Then
     cmdEliminarCola.Enabled = False
    Else
     cmdEliminarCola.Enabled = True
    End If
End Sub
Private Sub pCargaCombo(Index As Integer)
   
    Dim SQL As String
    Dim rs  As rdoResultset
    Dim qry As rdoQuery
    
    Select Case Index
        Case iCBODOCTOR
            SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO "
            SQL = SQL & "FROM AG1100 "
            SQL = SQL & "WHERE AG1100.AD02CODDPTO =  " & Trim(SSDBCombo1(0).Columns(0).Value)
            SQL = SQL & "AND (AG1100.AG11FECFINVREC > SYSDATE "
            SQL = SQL & "OR AG1100.AG11FECFINVREC IS NULL) "
            SSDBConsulta(iCBODOCTOR).RemoveAll
            Set rs = objApp.rdoConnect.OpenResultset(SQL)
            While rs.EOF = False
                SSDBConsulta(iCBODOCTOR).AddItem rs("AG11CODRECURSO").Value & ";" & rs("AG11DESRECURSO").Value
                SSDBConsulta(iCBODOCTOR).Text = rs("AG11DESRECURSO").Value
                SSDBConsulta(iCBODOCTOR).MoveNext
                rs.MoveNext
            Wend
            rs.Close
            Set rs = Nothing
    End Select
    SSDBConsulta(iCBODOCTOR).Text = ""
End Sub
Private Sub pArrancarAsociadas(strNumActPedi As String)
'Chequeo de las pruebas asociadas de anestesia para
'la prueba recibida
'La recibiremos automaticamente
    
  Dim sStmSql     As String
  Dim rstAsocia   As rdoResultset
  Dim sStmUpd     As String
      
    Screen.MousePointer = vbHourglass
    sStmSql = "SELECT PR03NUMACTPEDI FROM PR6100 "
    sStmSql = sStmSql & "WHERE PR03NUMACTPEDI_ASO = " & strNumActPedi
    Set rstAsocia = objApp.rdoConnect.OpenResultset(sStmSql)
    While Not rstAsocia.EOF
        sStmUpd = "UPDATE PR0400 SET PR04FECENTRCOLA=(SELECT SYSDATE FROM DUAL) "
        sStmUpd = sStmUpd & "WHERE PR03NUMACTPEDI = " & rstAsocia.rdoColumns(0).Value
        objApp.rdoConnect.Execute sStmUpd
        rstAsocia.MoveNext
    Wend
    Screen.MousePointer = vbDefault
End Sub
