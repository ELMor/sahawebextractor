VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmActuacionesSinRecurso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Actuaciones finalizadas sin recurso consumido"
   ClientHeight    =   8040
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11565
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8040
   ScaleWidth      =   11565
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Caption         =   "Actividad"
      ForeColor       =   &H00C00000&
      Height          =   1395
      Left            =   3120
      TabIndex        =   11
      Top             =   60
      Width           =   2415
      Begin VB.OptionButton optActividad 
         Caption         =   "Intervenciones"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   13
         Top             =   900
         Width           =   1695
      End
      Begin VB.OptionButton optActividad 
         Caption         =   "Consultas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   12
         Top             =   420
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Fechas"
      ForeColor       =   &H00C00000&
      Height          =   1395
      Left            =   60
      TabIndex        =   6
      Top             =   60
      Width           =   2955
      Begin SSCalendarWidgets_A.SSDateCombo dcboDesde 
         Height          =   315
         Left            =   1080
         TabIndex        =   7
         Top             =   300
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboHasta 
         Height          =   315
         Left            =   1080
         TabIndex        =   8
         Top             =   900
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   60
         TabIndex        =   9
         Top             =   960
         Width           =   915
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Actuaciones finalizadas sin recurso consumido"
      ForeColor       =   &H00C00000&
      Height          =   6375
      Left            =   60
      TabIndex        =   2
      Top             =   1560
      Width           =   11475
      Begin VB.CommandButton cmdRecCons 
         Caption         =   "&Rec. Cons."
         Height          =   375
         Left            =   10260
         TabIndex        =   5
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton cmdRecPrev 
         Caption         =   "&Rec. Prev."
         Height          =   375
         Left            =   10260
         TabIndex        =   4
         Top             =   780
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBGrid grdActuaciones 
         Height          =   6030
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   10065
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   17754
         _ExtentY        =   10636
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   5640
      TabIndex        =   1
      Top             =   1080
      Width           =   1095
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   5640
      TabIndex        =   0
      Top             =   180
      Width           =   1095
   End
End
Attribute VB_Name = "frmActuacionesSinRecurso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub pActualizar()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strFecDesde$, strFecHasta$
    Dim lngActividad&
    
    Screen.MousePointer = vbHourglass
    
    'Fechas
    strFecDesde = dcboDesde.Date
    strFecHasta = dcboHasta.Date
    'Actividad
    If optActividad(0).Value = True Then 'Consulta
        lngActividad = 201
    ElseIf optActividad(1).Value = True Then 'Intervenci�n
        lngActividad = 207
    End If

    grdActuaciones.RemoveAll
    
    'se cambia el scrollbar para evitar el efecto de llenado del grid dejando la �ltima _
    fila vacia durante un cierto rato
    grdActuaciones.ScrollBars = ssScrollBarsNone

    SQL = "SELECT /*+ ORDERED */ DISTINCT PR0400.PR04NUMACTPLAN, PR01DESCORTA,"
'    SQL = "SELECT DISTINCT PR0400.PR04NUMACTPLAN, PR01DESCORTA,"
    SQL = SQL & " PR04FECENTRCOLA, PR04FECINIACT, PR04FECFINACT,"
    SQL = SQL & " PR0400.CI21CODPERSONA, CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE"
    SQL = SQL & " FROM PR0400, PR0100, PR1000, CI2200"
    SQL = SQL & " WHERE PR37CODESTADO = 4" 'finalizadas
    SQL = SQL & " AND PR04FECENTRCOLA >= TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & " AND PR04FECENTRCOLA < TO_DATE(?,'DD/MM/YYYY')"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR0100.PR12CODACTIVIDAD = ?" 'consultas
    SQL = SQL & " AND PR1000.PR04NUMACTPLAN (+)= PR0400.PR04NUMACTPLAN"
    SQL = SQL & " AND PR1000.PR04NUMACTPLAN IS NULL"
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " ORDER BY PR04FECENTRCOLA"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Format(strFecDesde, "dd/mm/yyyy")
    qry(1) = Format(DateAdd("d", 1, strFecHasta), "dd/mm/yyyy")
    qry(2) = lngActividad
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        grdActuaciones.AddItem Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & Format(rs!PR04FECINIACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & Format(rs!PR04FECFINACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) _
            & rs!PACIENTE & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!PR04NUMACTPLAN & Chr$(9) _
            & rs!CI21CODPERSONA
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'se pone el scrollbar en su estado normal
    grdActuaciones.ScrollBars = ssScrollBarsAutomatic

    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatearGrids()
    With grdActuaciones
        .Columns(0).Caption = "Entrada Cola"
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Fecha Inicio"
        .Columns(1).Width = 1300
        .Columns(2).Caption = "Fecha Fin"
        .Columns(2).Width = 1300
        .Columns(3).Caption = "N� Hist."
        .Columns(3).Alignment = ssCaptionAlignmentRight
        .Columns(3).Width = 700
        .Columns(4).Caption = "Paciente"
        .Columns(4).Width = 3100
        .Columns(5).Caption = "Actuaci�n"
        .Columns(5).Width = 2000
        .Columns(6).Caption = "Num. Act."
        .Columns(6).Alignment = ssCaptionAlignmentRight
        .Columns(6).Visible = False
        .Columns(7).Caption = "Persona"
        .Columns(7).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub cmdRecCons_Click()
    Dim msg$, lngNumActPlan&
    
    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
    Case 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        
        'Acceso a Recursos Consumidos
        Call pVerRecursosConsumidos(lngNumActPlan)
'    Case Else
'        msg = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdRecPrev_Click()
    Dim msg$, lngNumActPlan&
    
    Select Case grdActuaciones.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Actuaci�n."
    Case 1
        lngNumActPlan = grdActuaciones.Columns("Num. Act.").CellText(grdActuaciones.SelBookmarks(0))
        
        'Acceso a Recursos Consumidos
        Call pVerRecursos(lngNumActPlan)
'    Case Else
'        msg = "Debe Ud. seleccionar una �nica Actuaci�n."
    End Select
    If msg <> "" Then MsgBox msg, vbExclamation, Me.Caption
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub dcboDesde_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboHasta.Date = dcboDesde.Date
    End If
End Sub

Private Sub dcboHasta_Change()
    If CDate(dcboHasta.Date) < CDate(dcboDesde.Date) Then
        dcboDesde.Date = dcboHasta.Date
    End If
End Sub

Private Sub cmdConsultar_Click()
    Call pActualizar
End Sub

Private Sub Form_Load()
    Dim strHoy$
    
    Call pFormatearGrids
    strHoy = strFecha_Sistema
    dcboDesde.Date = DateAdd("ww", -1, strHoy)
    dcboHasta.Date = strHoy
    dcboDesde.BackColor = objApp.objUserColor.lngMandatory
    dcboHasta.BackColor = objApp.objUserColor.lngMandatory
End Sub

Private Sub grdActuaciones_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub grdActuaciones_Click()
    Call pSeleccionar
End Sub

Private Sub grdActuaciones_DblClick()
    Dim lngNumActPlan&
    On Error Resume Next
    lngNumActPlan = grdActuaciones.Columns("Num. Act.").Text
    On Error GoTo 0
    If lngNumActPlan > 0 Then
        Call pSeleccionar
        Call pVerRecursosConsumidos(lngNumActPlan)
    End If
End Sub

Private Sub pVerRecursos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmConsultaRecursos.Show vbModal
    Set frmConsultaRecursos = Nothing
End Sub

Private Sub pVerRecursosConsumidos(lngNumActPlan&)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT PR04NUMACTPLAN FROM PR0700"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset()
    If rs.EOF Then
        SQL = "INSERT INTO PR0700 (PR04NUMACTPLAN, PR07NUMFASE, PR07DESFASE, PR07NUMMINOCUPAC)"
        SQL = SQL & " SELECT PR04NUMACTPLAN, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC"
        SQL = SQL & " FROM PR0400, PR0600"
        SQL = SQL & " WHERE PR04NUMACTPLAN =?"
        SQL = SQL & " AND PR0600.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPlan
        On Error Resume Next
        qry.Execute
        On Error GoTo 0
    End If
    rs.Close
    qry.Close
    
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmRecConsumidos.Show vbModal
    Set frmRecConsumidos = Nothing
    grdActuaciones.DeleteSelected
 End Sub

Private Sub pSeleccionar()
    'deselecci�n
    If grdActuaciones.SelBookmarks.Count > 0 Then
        grdActuaciones.SelBookmarks.Remove (0)
    End If
    'selecci�n
    grdActuaciones.SelBookmarks.Add (grdActuaciones.RowBookmark(grdActuaciones.Row))
End Sub
