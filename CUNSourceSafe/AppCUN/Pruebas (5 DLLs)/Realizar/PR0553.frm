VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmRegistroQuirofano 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registro de Quir�fano"
   ClientHeight    =   8280
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11490
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8280
   ScaleWidth      =   11490
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10440
      TabIndex        =   78
      Top             =   720
      Width           =   975
   End
   Begin VB.Frame fraFacturado 
      BorderStyle     =   0  'None
      Height          =   8235
      Left            =   0
      TabIndex        =   25
      Top             =   0
      Width           =   10335
      Begin VB.TextBox txtOrden 
         BackColor       =   &H00C0C0C0&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   7260
         Locked          =   -1  'True
         TabIndex        =   80
         TabStop         =   0   'False
         Top             =   180
         Width           =   495
      End
      Begin VB.TextBox txtQuir 
         BackColor       =   &H00C0C0C0&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   6060
         Locked          =   -1  'True
         TabIndex        =   74
         TabStop         =   0   'False
         Top             =   180
         Width           =   435
      End
      Begin VB.Frame Frame1 
         Caption         =   "Equipo Quir�rgico"
         ForeColor       =   &H00C00000&
         Height          =   4215
         Left            =   60
         TabIndex        =   64
         Top             =   2700
         Width           =   4995
         Begin SSDataWidgets_B.SSDBCombo cboEQCir 
            Height          =   315
            Left            =   1440
            TabIndex        =   0
            Top             =   780
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQAy1 
            Height          =   315
            Left            =   1440
            TabIndex        =   1
            Top             =   1200
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQAy2 
            Height          =   315
            Left            =   1440
            TabIndex        =   2
            Top             =   1620
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQAnes 
            Height          =   315
            Left            =   1440
            TabIndex        =   3
            Top             =   2040
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQIns1 
            Height          =   315
            Left            =   1440
            TabIndex        =   4
            Top             =   2460
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQEnf1 
            Height          =   315
            Left            =   1440
            TabIndex        =   6
            Top             =   3300
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQIns2 
            Height          =   315
            Left            =   1440
            TabIndex        =   5
            Top             =   2880
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQEnf2 
            Height          =   315
            Left            =   1440
            TabIndex        =   7
            Top             =   3720
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboEQDpto 
            Height          =   315
            Left            =   1440
            TabIndex        =   88
            Top             =   360
            Width           =   3435
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6059
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Cirujano:"
            Height          =   255
            Index           =   2
            Left            =   420
            TabIndex        =   73
            Top             =   840
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "1� Ayudante:"
            Height          =   255
            Index           =   3
            Left            =   420
            TabIndex        =   72
            Top             =   1260
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "2� Ayudante:"
            Height          =   255
            Index           =   4
            Left            =   420
            TabIndex        =   71
            Top             =   1680
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Anestesi�logo:"
            Height          =   255
            Index           =   5
            Left            =   180
            TabIndex        =   70
            Top             =   2100
            Width           =   1155
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "1� Instrumentista:"
            Height          =   255
            Index           =   6
            Left            =   120
            TabIndex        =   69
            Top             =   2520
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "1� Enfermera:"
            Height          =   255
            Index           =   7
            Left            =   120
            TabIndex        =   68
            Top             =   3360
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Dpto:"
            Height          =   255
            Index           =   8
            Left            =   420
            TabIndex        =   67
            Top             =   420
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "2� Instrumentista:"
            Height          =   255
            Index           =   20
            Left            =   120
            TabIndex        =   66
            Top             =   2940
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "2� Enfermera:"
            Height          =   255
            Index           =   21
            Left            =   120
            TabIndex        =   65
            Top             =   3780
            Width           =   1215
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Equipo Colaborador"
         ForeColor       =   &H00C00000&
         Height          =   1935
         Left            =   5160
         TabIndex        =   58
         Top             =   5040
         Width           =   5175
         Begin SSDataWidgets_B.SSDBCombo cboECCir 
            Height          =   315
            Left            =   1200
            TabIndex        =   17
            Top             =   660
            Width           =   3795
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6694
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboECAy 
            Height          =   315
            Left            =   1200
            TabIndex        =   18
            Top             =   1080
            Width           =   3795
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6694
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboECDpto 
            Height          =   315
            Left            =   1200
            TabIndex        =   16
            Top             =   240
            Width           =   3795
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6694
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin MSMask.MaskEdBox mskHoraIEC 
            Height          =   315
            Left            =   1200
            TabIndex        =   19
            Top             =   1500
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   327681
            BackColor       =   16777215
            MaxLength       =   5
            Mask            =   "##:##"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskHoraFEC 
            Height          =   315
            Left            =   2940
            TabIndex        =   20
            Top             =   1500
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   327681
            BackColor       =   16777215
            MaxLength       =   5
            Mask            =   "##:##"
            PromptChar      =   "_"
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Dpto:"
            Height          =   255
            Index           =   9
            Left            =   180
            TabIndex        =   63
            Top             =   300
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Ayudante:"
            Height          =   255
            Index           =   14
            Left            =   180
            TabIndex        =   62
            Top             =   1140
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Cirujano:"
            Height          =   255
            Index           =   15
            Left            =   180
            TabIndex        =   61
            Top             =   720
            Width           =   915
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "H. Inicio:"
            Height          =   255
            Index           =   10
            Left            =   120
            TabIndex        =   60
            Top             =   1560
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "H. Fin:"
            Height          =   255
            Index           =   11
            Left            =   2280
            TabIndex        =   59
            Top             =   1560
            Width           =   615
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Intervenci�n"
         ForeColor       =   &H00C00000&
         Height          =   4395
         Left            =   5160
         TabIndex        =   44
         Top             =   600
         Width           =   5175
         Begin VB.TextBox txtTOcupQuirof 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   4260
            Locked          =   -1  'True
            TabIndex        =   86
            TabStop         =   0   'False
            Top             =   2220
            Width           =   495
         End
         Begin VB.TextBox txtTEstimado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1380
            Locked          =   -1  'True
            TabIndex        =   83
            TabStop         =   0   'False
            Top             =   2040
            Width           =   495
         End
         Begin VB.TextBox txtTDif 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   4260
            Locked          =   -1  'True
            TabIndex        =   82
            TabStop         =   0   'False
            Top             =   1860
            Width           =   495
         End
         Begin VB.CheckBox chkProg 
            Caption         =   "Programada"
            Height          =   195
            Left            =   540
            TabIndex        =   48
            Top             =   240
            Width           =   1275
         End
         Begin VB.CheckBox chkImprev 
            Caption         =   "Imprevista"
            Height          =   195
            Left            =   2280
            TabIndex        =   47
            Top             =   240
            Width           =   1095
         End
         Begin VB.CheckBox chkUrg 
            Caption         =   "Urgente"
            Height          =   195
            Left            =   3900
            TabIndex        =   46
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox txtObserv 
            Height          =   615
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   15
            Top             =   3660
            Width           =   4935
         End
         Begin VB.TextBox txtDuracion 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   4260
            TabIndex        =   45
            Top             =   1500
            Width           =   495
         End
         Begin MSMask.MaskEdBox mskHoraIAnest 
            Height          =   315
            Left            =   1440
            TabIndex        =   8
            Top             =   540
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   327681
            BackColor       =   16777215
            MaxLength       =   5
            Mask            =   "##:##"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskHoraFAnest 
            Height          =   315
            Left            =   3240
            TabIndex        =   9
            Top             =   540
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   327681
            BackColor       =   16777215
            MaxLength       =   5
            Mask            =   "##:##"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskHoraIInt 
            Height          =   315
            Left            =   1380
            TabIndex        =   11
            Top             =   1500
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   327681
            BackColor       =   16777215
            MaxLength       =   5
            Mask            =   "##:##"
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox mskHoraFInt 
            Height          =   315
            Left            =   3180
            TabIndex        =   12
            Top             =   1500
            Width           =   615
            _ExtentX        =   1085
            _ExtentY        =   556
            _Version        =   327681
            BackColor       =   16777215
            MaxLength       =   5
            Mask            =   "##:##"
            PromptChar      =   "_"
         End
         Begin SSDataWidgets_B.SSDBCombo cboTipoAnest 
            Height          =   315
            Left            =   1440
            TabIndex        =   10
            Top             =   960
            Width           =   3075
            DataFieldList   =   "Column 0"
            MaxDropDownItems=   9
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboPosicion 
            Height          =   315
            Left            =   1440
            TabIndex        =   13
            Top             =   2640
            Width           =   3075
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboLocPlBis 
            Height          =   315
            Left            =   1440
            TabIndex        =   14
            Top             =   3060
            Width           =   3075
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            DividerType     =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "Cod"
            Columns(0).Name =   "Cod"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   4974
            Columns(1).Caption=   "Desig"
            Columns(1).Name =   "Desig"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   5424
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "T. ocupaci�n quir�fano:             min"
            Height          =   255
            Index           =   32
            Left            =   1980
            TabIndex        =   87
            Top             =   2280
            Width           =   3075
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "T. Estimado:             min"
            Height          =   255
            Index           =   30
            Left            =   180
            TabIndex        =   85
            Top             =   2100
            Width           =   1995
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "T(real-estimado):             min"
            Height          =   255
            Index           =   31
            Left            =   2460
            TabIndex        =   84
            Top             =   1920
            Width           =   2595
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "H. Fin Anest.:"
            Height          =   255
            Index           =   12
            Left            =   2100
            TabIndex        =   57
            Top             =   600
            Width           =   1095
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "H. Ini. Anest.:"
            Height          =   255
            Index           =   13
            Left            =   360
            TabIndex        =   56
            Top             =   600
            Width           =   1035
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "H. Fin Interv.:"
            Height          =   255
            Index           =   16
            Left            =   2040
            TabIndex        =   55
            Top             =   1560
            Width           =   1095
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "H. Ini Interv.:"
            Height          =   255
            Index           =   17
            Left            =   360
            TabIndex        =   54
            Top             =   1560
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Tipo Anest.:"
            Height          =   255
            Index           =   18
            Left            =   420
            TabIndex        =   53
            Top             =   1020
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Observaciones:"
            Height          =   255
            Index           =   19
            Left            =   120
            TabIndex        =   52
            Top             =   3420
            Width           =   1275
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "T:             min"
            Height          =   255
            Index           =   22
            Left            =   3960
            TabIndex        =   51
            Top             =   1560
            Width           =   1095
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Posici�n:"
            Height          =   255
            Index           =   23
            Left            =   420
            TabIndex        =   50
            Top             =   2700
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Loc. placa bistur�:"
            Height          =   255
            Index           =   24
            Left            =   120
            TabIndex        =   49
            Top             =   3120
            Width           =   1275
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Paciente"
         ForeColor       =   &H00C00000&
         Height          =   795
         Left            =   60
         TabIndex        =   41
         Top             =   60
         Width           =   4995
         Begin VB.TextBox txtNH 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   43
            TabStop         =   0   'False
            Top             =   300
            Width           =   855
         End
         Begin VB.TextBox txtPac 
            BackColor       =   &H00C0C0C0&
            Height          =   315
            Left            =   1020
            Locked          =   -1  'True
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   300
            Width           =   3855
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Intervenciones realizadas"
         ForeColor       =   &H00C00000&
         Height          =   1755
         Left            =   60
         TabIndex        =   39
         Top             =   900
         Width           =   4995
         Begin SSDataWidgets_B.SSDBGrid grdInterv 
            Height          =   1395
            Left            =   120
            TabIndex        =   40
            Top             =   240
            Width           =   4755
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            ColumnHeaders   =   0   'False
            AllowUpdate     =   0   'False
            AllowColumnMoving=   0
            AllowColumnSwapping=   0
            SelectTypeCol   =   0
            RowNavigation   =   1
            CellNavigation  =   1
            ForeColorEven   =   0
            BackColorEven   =   12632256
            BackColorOdd    =   12632256
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   7911
            Columns(0).Caption=   "Act"
            Columns(0).Name =   "Act"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            _ExtentX        =   8387
            _ExtentY        =   2461
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Estudios solicitados"
         ForeColor       =   &H00C00000&
         Height          =   1215
         Left            =   60
         TabIndex        =   26
         Top             =   6960
         Width           =   10275
         Begin VB.CheckBox chkRadioterapia 
            Caption         =   "Radioterapia"
            Height          =   195
            Left            =   2040
            TabIndex        =   34
            Top             =   300
            Width           =   1275
         End
         Begin VB.CheckBox chkEndoscopia 
            Caption         =   "Ecograf�a"
            Height          =   195
            Left            =   2040
            TabIndex        =   33
            Top             =   600
            Width           =   1215
         End
         Begin VB.CheckBox chkRx 
            Caption         =   "Rx"
            Height          =   195
            Left            =   2040
            TabIndex        =   32
            Top             =   900
            Width           =   795
         End
         Begin VB.CheckBox chkCEC 
            Caption         =   "C.E.C."
            Height          =   195
            Left            =   3420
            TabIndex        =   31
            Top             =   300
            Width           =   915
         End
         Begin VB.CheckBox chkLaser 
            Caption         =   "Laser"
            Height          =   195
            Left            =   3420
            TabIndex        =   30
            Top             =   600
            Width           =   915
         End
         Begin VB.CheckBox chkIsquemia 
            Caption         =   "Isquemia"
            Height          =   195
            Left            =   3420
            TabIndex        =   29
            Top             =   900
            Width           =   975
         End
         Begin VB.CheckBox chkInstrumental 
            Caption         =   "Cta. instrumental correcta"
            Height          =   195
            Left            =   4560
            TabIndex        =   28
            Top             =   300
            Width           =   2115
         End
         Begin VB.CheckBox chkCtaGasas 
            Caption         =   "Cta. gasas correcta"
            Height          =   195
            Left            =   4560
            TabIndex        =   27
            Top             =   600
            Width           =   2055
         End
         Begin VB.TextBox txtAPIntra 
            Height          =   285
            Left            =   1200
            MaxLength       =   2
            TabIndex        =   21
            Top             =   240
            Width           =   615
         End
         Begin VB.TextBox txtAPNoIntra 
            Height          =   285
            Left            =   1200
            MaxLength       =   2
            TabIndex        =   22
            Top             =   540
            Width           =   615
         End
         Begin VB.TextBox txtMB 
            Height          =   285
            Left            =   1200
            MaxLength       =   2
            TabIndex        =   23
            Top             =   840
            Width           =   615
         End
         Begin VB.TextBox txtOtrosEstudios 
            Height          =   675
            Left            =   6780
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   24
            Top             =   420
            Width           =   3375
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "AP (intra):"
            Height          =   255
            Index           =   25
            Left            =   180
            TabIndex        =   38
            Top             =   300
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "AP (NO intra):"
            Height          =   255
            Index           =   26
            Left            =   120
            TabIndex        =   37
            Top             =   600
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Microb.:"
            Height          =   255
            Index           =   27
            Left            =   180
            TabIndex        =   36
            Top             =   840
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Otros:"
            Height          =   255
            Index           =   28
            Left            =   6780
            TabIndex        =   35
            Top             =   180
            Width           =   975
         End
      End
      Begin SSCalendarWidgets_A.SSDateCombo dcboFecha 
         Height          =   315
         Left            =   8580
         TabIndex        =   75
         Top             =   180
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowCentury     =   -1  'True
         StartofWeek     =   2
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Orden:"
         Height          =   255
         Index           =   29
         Left            =   6660
         TabIndex        =   81
         Top             =   240
         Width           =   555
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         Height          =   255
         Index           =   0
         Left            =   7980
         TabIndex        =   77
         Top             =   240
         Width           =   555
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Quir�fano:"
         Height          =   255
         Index           =   1
         Left            =   5220
         TabIndex        =   76
         Top             =   240
         Width           =   795
      End
   End
   Begin VB.Label lblFacturado 
      Caption         =   "F A C T U R A D O"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   5115
      Left            =   10740
      TabIndex        =   79
      Top             =   1380
      Visible         =   0   'False
      Width           =   495
   End
End
Attribute VB_Name = "frmRegistroQuirofano"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intEstado%, strNAPlan$, lngEQDpto&, strAhora$
Dim strNumHQ$
Dim blnFacturado As Boolean, blnReadOnly As Boolean

Private Sub cboECAy_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboECAy.Text = ""
End Sub
Private Sub cboECCir_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboECCir.Text = ""
End Sub
Private Sub cboECDpto_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboECDpto.Text = ""
        cboECCir.Text = "": cboECCir.RemoveAll: cboECCir.BackColor = objApp.objUserColor.lngNormal
        cboECAy.Text = "": cboECAy.RemoveAll
        mskHoraIEC.Text = "__:__": mskHoraIEC.BackColor = objApp.objUserColor.lngNormal
        mskHoraFEC.Text = "__:__": mskHoraFEC.BackColor = objApp.objUserColor.lngNormal
    End If
End Sub
Private Sub cboEQAnes_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then
        cboEQAnes.Text = ""
        mskHoraIAnest.BackColor = objApp.objUserColor.lngNormal
        mskHoraFAnest.BackColor = objApp.objUserColor.lngNormal
        cboTipoAnest.BackColor = objApp.objUserColor.lngNormal
        mskHoraIAnest.Text = "__:__"
        mskHoraFAnest.Text = "__:__"
        cboTipoAnest.Text = ""
    End If
End Sub
Private Sub cboEQAy1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboEQAy1.Text = ""
End Sub
Private Sub cboEQAy2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboEQAy2.Text = ""
End Sub
Private Sub cboEQCir_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboEQCir.Text = ""
End Sub

Private Sub cboEQDpto_Click()
    If lngEQDpto <> cboEQDpto.Columns(0).Text Then
        lngEQDpto = cboEQDpto.Columns(0).Text
        Call pDatosEquipoQuirurgico
    End If
End Sub

Private Sub cboEQEnf1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboEQEnf1.Text = ""
End Sub
Private Sub cboEQEnf2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboEQEnf2.Text = ""
End Sub
Private Sub cboEQIns1_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboEQIns1.Text = ""
End Sub
Private Sub cboEQIns2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboEQIns2.Text = ""
End Sub
Private Sub cboTipoAnest_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboTipoAnest.Text = ""
End Sub
Private Sub cboLocPlBis_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboLocPlBis.Text = ""
End Sub
Private Sub cboPosicion_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then cboPosicion.Text = ""
End Sub

Private Sub mskHoraFAnest_GotFocus()
    mskHoraFAnest.SelStart = 0
    mskHoraFAnest.SelLength = Len(mskHoraFAnest.Text)
End Sub
Private Sub mskHoraFEC_GotFocus()
    mskHoraFEC.SelStart = 0
    mskHoraFEC.SelLength = Len(mskHoraFEC.Text)
End Sub
Private Sub mskHoraFInt_GotFocus()
    mskHoraFInt.SelStart = 0
    mskHoraFInt.SelLength = Len(mskHoraFInt.Text)
End Sub
Private Sub mskHoraIAnest_GotFocus()
    mskHoraIAnest.SelStart = 0
    mskHoraIAnest.SelLength = Len(mskHoraIAnest.Text)
End Sub
Private Sub mskHoraIEC_GotFocus()
    mskHoraIEC.SelStart = 0
    mskHoraIEC.SelLength = Len(mskHoraIEC.Text)
End Sub
Private Sub mskHoraIInt_GotFocus()
    mskHoraIInt.SelStart = 0
    mskHoraIInt.SelLength = Len(mskHoraIInt.Text)
End Sub
Private Sub txtAPIntra_GotFocus()
    txtAPIntra.SelStart = 0
    txtAPIntra.SelLength = Len(txtAPIntra.Text)
End Sub
Private Sub txtAPNoIntra_GotFocus()
    txtAPNoIntra.SelStart = 0
    txtAPNoIntra.SelLength = Len(txtAPNoIntra.Text)
End Sub

Private Sub txtDuracion_Change()
    If txtDuracion.Text <> "" And txtTEstimado.Text <> "" Then
        txtTDif.Text = txtDuracion.Text - txtTEstimado.Text
    Else
        txtTDif.Text = ""
    End If
End Sub

Private Sub txtMB_GotFocus()
    txtMB.SelStart = 0
    txtMB.SelLength = Len(txtMB.Text)
End Sub

Private Sub txtAPIntra_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case 8, 13
    Case Is < 48, Is > 57 'n�
        KeyAscii = 0
    End Select
End Sub
Private Sub txtAPNoIntra_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case 8, 13
    Case Is < 48, Is > 57 'n�
        KeyAscii = 0
    End Select
End Sub
Private Sub txtMB_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case 8, 13
    Case Is < 48, Is > 57 'n�
        KeyAscii = 0
    End Select
End Sub

Private Sub mskHoraIAnest_LostFocus()
    If Not mskHoraIAnest.Text = "__:__" Then
        mskHoraIAnest.Text = objGen.ReplaceStr(mskHoraIAnest.Text, "_", "0", 0)
        If Not IsDate(mskHoraIAnest.Text) Then mskHoraIAnest.Text = "__:__"
    End If
    Call pTOcupQuirof
End Sub
Private Sub mskHoraIEC_LostFocus()
    If Not mskHoraIEC.Text = "__:__" Then
        mskHoraIEC.Text = objGen.ReplaceStr(mskHoraIEC.Text, "_", "0", 0)
        If Not IsDate(mskHoraIEC.Text) Then mskHoraIEC.Text = "__:__"
    End If
End Sub
Private Sub mskHoraIInt_LostFocus()
    Dim lngMin&, msg$
    If Not mskHoraIInt.Text = "__:__" Then
        mskHoraIInt.Text = objGen.ReplaceStr(mskHoraIInt.Text, "_", "0", 0)
        If Not IsDate(mskHoraIInt.Text) Then
            mskHoraIInt.Text = "__:__"
        Else
            If mskHoraFInt.Text <> "__:__" Then
                mskHoraFInt.Text = objGen.ReplaceStr(mskHoraFInt.Text, "_", "0", 0)
                If IsDate(mskHoraFInt.Text) Then
                    lngMin = DateDiff("n", CDate(mskHoraIInt.Text), CDate(mskHoraFInt.Text))
                    If lngMin < 0 Then lngMin = 1440 + lngMin
                    txtDuracion.Text = lngMin
                    Call pTOcupQuirof
                    If lngMin >= 480 Then
                        msg = "La duraci�n de la intervenci�n es superior a 8 horas."
                        msg = msg & Chr$(13)
                        msg = msg & " Aseg�rese que la informaci�n introducida es correcta."
                        MsgBox msg, vbInformation, Me.Caption
                    End If
                    Exit Sub
                End If
            End If
        End If
    End If
    txtDuracion.Text = ""
    Call pTOcupQuirof
End Sub

Private Sub mskHoraFAnest_LostFocus()
    If mskHoraFAnest.Text <> "__:__" Then
        mskHoraFAnest.Text = objGen.ReplaceStr(mskHoraFAnest.Text, "_", "0", 0)
        If Not IsDate(mskHoraFAnest.Text) Then mskHoraFAnest.Text = "__:__"
    End If
    Call pTOcupQuirof
End Sub
Private Sub mskHoraFEC_LostFocus()
    If mskHoraFEC.Text <> "__:__" Then
        mskHoraFEC.Text = objGen.ReplaceStr(mskHoraFEC.Text, "_", "0", 0)
        If Not IsDate(mskHoraFEC.Text) Then mskHoraFEC.Text = "__:__"
    End If
End Sub
Private Sub mskHoraFInt_LostFocus()
    Dim lngMin&, msg$
    If mskHoraFInt.Text <> "__:__" Then
        mskHoraFInt.Text = objGen.ReplaceStr(mskHoraFInt.Text, "_", "0", 0)
        If Not IsDate(mskHoraFInt.Text) Then
            mskHoraFInt.Text = "__:__"
        Else
            If mskHoraIInt.Text <> "__:__" Then
                mskHoraIInt.Text = objGen.ReplaceStr(mskHoraIInt.Text, "_", "0", 0)
                If IsDate(mskHoraIInt.Text) Then
                    lngMin = DateDiff("n", CDate(mskHoraIInt.Text), CDate(mskHoraFInt.Text))
                    If lngMin < 0 Then lngMin = 1440 + lngMin
                    txtDuracion.Text = lngMin
                    Call pTOcupQuirof
                    If lngMin >= 480 Then
                        msg = "La duraci�n de la intervenci�n es superior a 8 horas."
                        msg = msg & Chr$(13)
                        msg = msg & " Aseg�rese que la informaci�n introducida es correcta."
                        MsgBox msg, vbInformation, Me.Caption
                    End If
                    Exit Sub
                End If
            End If
        End If
    End If
    txtDuracion.Text = ""
    Call pTOcupQuirof
End Sub

Private Sub cmdSalir_Click()
    If blnReadOnly Then
        Unload Me 'si est� facturado no se guarda ning�n cambio
    Else
        If fComprobaciones Then
            Screen.MousePointer = vbHourglass
            If strNumHQ = "" Then Call pInsertarHoja
            Call pGuardarHoja
            Screen.MousePointer = vbDefault
            Unload Me
        End If
    End If
End Sub

Private Sub Form_Load()
        
    Screen.MousePointer = vbHourglass
    
    'se mira si la HQ es ReadOnly
    If objPipe.PipeExist("PR_NumHQ") Then 'Presentar la hoja com ReadOnly
        blnReadOnly = True
        strNumHQ = objPipe.PipeGet("PR_NumHQ")
        objPipe.PipeRemove ("PR_NumHQ")
    Else
        'se leen las intervenciones que componen el 'acto m�dico'
        strNAPlan = objPipe.PipeGet("PR_IntervActoMedico")
        objPipe.PipeRemove ("PR_IntervActoMedico")
        'se mira el estado del acceso a la Hoja de Quir�fano (intermedio o para finalizar)
        intEstado = objPipe.PipeGet("PR_EstadoHojaQuir") '0 - en proceso; 1 - para finalizar
        objPipe.PipeRemove ("PR_EstadoHojaQuir")
    End If
    Call pCargarIntervActoMedico 'intervenciones, dpto. quir�rgico
    Call pCargarDatosFijos 'del paciente, quir�fano...
    
    'se mira si la Hoja est� ya facturada
    blnFacturado = fActFacturada(strNAPlan)
    If blnFacturado Then blnReadOnly = True
    
    If blnReadOnly Then
        Call pCargarHQReadOnly 'datos ya existentes de la Hoja
    Else
        Call pDatosAsociadosInterv 'tipo anest., posici�n pac., localizaci�n placa bistur�...
        Call pDatosEquipoQuirurgicoFijo 'equipo quir�rgico (anestesistas y enfermeras)
        Call pDatosDptosColaboradores 'departamentos colaboradores
        Call pCargarHQ 'datos ya existentes de la Hoja
    End If
    Call pFormatoControles
        
    Screen.MousePointer = vbDefault
End Sub

Private Sub pFormatoControles()
    If blnReadOnly Then
        If blnFacturado Then lblFacturado.Visible = True
        fraFacturado.Enabled = False
        Dim c As Control
        On Error Resume Next
        For Each c In Me
            c.BackColor = objApp.objUserColor.lngReadOnly
        Next
        On Error GoTo 0
        Exit Sub
    End If

    dcboFecha.BackColor = objApp.objUserColor.lngMandatory
    txtNH.BackColor = objApp.objUserColor.lngReadOnly
    txtPac.BackColor = objApp.objUserColor.lngReadOnly
    txtQuir.BackColor = objApp.objUserColor.lngReadOnly
    txtDuracion.BackColor = objApp.objUserColor.lngReadOnly
    grdInterv.BackColorOdd = objApp.objUserColor.lngReadOnly
    grdInterv.BackColorEven = objApp.objUserColor.lngReadOnly
    
    If intEstado = constESTADOHQ_FINALIZADA Then
        cboEQDpto.BackColor = objApp.objUserColor.lngMandatory
        cboEQCir.BackColor = objApp.objUserColor.lngMandatory
        Select Case Val(txtQuir.Text)
        Case 10, 11 'quir�fanos 10 y 11: no es obligatorio ni instrumentista ni enfermera
        Case Else
            cboEQIns1.BackColor = objApp.objUserColor.lngMandatory
            cboEQEnf1.BackColor = objApp.objUserColor.lngMandatory
        End Select
        mskHoraIInt.BackColor = objApp.objUserColor.lngMandatory
        mskHoraFInt.BackColor = objApp.objUserColor.lngMandatory
        If cboEQAnes.Text <> "" Then
            mskHoraIAnest.BackColor = objApp.objUserColor.lngMandatory
            mskHoraFAnest.BackColor = objApp.objUserColor.lngMandatory
            cboTipoAnest.BackColor = objApp.objUserColor.lngMandatory
        End If
        If cboECDpto.Text <> "" Then
            cboECCir.BackColor = objApp.objUserColor.lngMandatory
            mskHoraIEC.BackColor = objApp.objUserColor.lngMandatory
            mskHoraFEC.BackColor = objApp.objUserColor.lngMandatory
        End If
    End If
End Sub

Private Sub pCargarHQ()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim i%

    strAhora = strFechaHora_Sistema
    dcboFecha.Date = Format(strAhora, "dd/mm/yyyy") 'fecha (se moficar� con la Hora Ini Interv.)
    
    If strNumHQ = "" Then Exit Sub

    'Datos existentes de la Hoja de Quir�fano
    SQL = "SELECT * FROM PR6200"
    SQL = SQL & " WHERE PR62CODHOJAQUIR = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumHQ
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        'departamento
        If Not IsNull(rs!AD02CODDPTO) Then
            For i = 1 To cboEQDpto.Rows
                If i = 1 Then cboEQDpto.MoveFirst Else cboEQDpto.MoveNext
                If cboEQDpto.Columns(0).Text = rs!AD02CODDPTO Then
                    cboEQDpto.Text = cboEQDpto.Columns(1).Text
                    Exit For
                End If
            Next i
        End If
        If cboEQDpto.Text <> "" Then
            lngEQDpto = rs!AD02CODDPTO
            Call pDatosEquipoQuirurgico
            'cirujano
            If Not IsNull(rs!SG02COD_CIR) Then
                For i = 1 To cboEQCir.Rows
                    If i = 1 Then cboEQCir.MoveFirst Else cboEQCir.MoveNext
                    If cboEQCir.Columns(0).Text = rs!SG02COD_CIR Then
                        cboEQCir.Text = cboEQCir.Columns(1).Text
                        Exit For
                    End If
                Next i
                If i > cboEQCir.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_CIR, cboEQCir)
            End If
            'primer ayudante
            If Not IsNull(rs!SG02COD_PAY) Then
                For i = 1 To cboEQAy1.Rows
                    If i = 1 Then cboEQAy1.MoveFirst Else cboEQAy1.MoveNext
                    If cboEQAy1.Columns(0).Text = rs!SG02COD_PAY Then
                        cboEQAy1.Text = cboEQAy1.Columns(1).Text
                        Exit For
                    End If
                Next i
                If i > cboEQAy1.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_PAY, cboEQAy1)
            End If
            'segundo ayudante
            If Not IsNull(rs!SG02COD_SAY) Then
                For i = 1 To cboEQAy2.Rows
                    If i = 1 Then cboEQAy2.MoveFirst Else cboEQAy2.MoveNext
                    If cboEQAy2.Columns(0).Text = rs!SG02COD_SAY Then
                        cboEQAy2.Text = cboEQAy2.Columns(1).Text
                        Exit For
                    End If
                Next i
                If i > cboEQAy2.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_SAY, cboEQAy2)
            End If
        End If
        'primera instrumentista
        If Not IsNull(rs!SG02COD_PINS) Then
            For i = 1 To cboEQIns1.Rows
                If i = 1 Then cboEQIns1.MoveFirst Else cboEQIns1.MoveNext
                If cboEQIns1.Columns(0).Text = rs!SG02COD_PINS Then
                    cboEQIns1.Text = cboEQIns1.Columns(1).Text
                    Exit For
                End If
            Next i
            If i > cboEQIns1.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_PINS, cboEQIns1)
        End If
        'segunda instrumentista
        If Not IsNull(rs!SG02COD_SINS) Then
            For i = 1 To cboEQIns2.Rows
                If i = 1 Then cboEQIns2.MoveFirst Else cboEQIns2.MoveNext
                If cboEQIns2.Columns(0).Text = rs!SG02COD_SINS Then
                    cboEQIns2.Text = cboEQIns2.Columns(1).Text
                    Exit For
                End If
            Next i
            If i > cboEQIns2.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_SINS, cboEQIns2)
        End If
        'primera enfermera
        If Not IsNull(rs!SG02COD_PENF) Then
            For i = 1 To cboEQEnf1.Rows
                If i = 1 Then cboEQEnf1.MoveFirst Else cboEQEnf1.MoveNext
                If cboEQEnf1.Columns(0).Text = rs!SG02COD_PENF Then
                    cboEQEnf1.Text = cboEQEnf1.Columns(1).Text
                    Exit For
                End If
            Next i
            If i > cboEQEnf1.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_PENF, cboEQEnf1)
        End If
        'segunda enfermera
        If Not IsNull(rs!SG02COD_SENF) Then
            For i = 1 To cboEQEnf2.Rows
                If i = 1 Then cboEQEnf2.MoveFirst Else cboEQEnf2.MoveNext
                If cboEQEnf2.Columns(0).Text = rs!SG02COD_SENF Then
                    cboEQEnf2.Text = cboEQEnf2.Columns(1).Text
                    Exit For
                End If
            Next i
            If i > cboEQEnf2.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_SENF, cboEQEnf2)
        End If
        'anestesi�logo
        If Not IsNull(rs!SG02COD_ANE) Then
            For i = 1 To cboEQAnes.Rows
                If i = 1 Then cboEQAnes.MoveFirst Else cboEQAnes.MoveNext
                If cboEQAnes.Columns(0).Text = rs!SG02COD_ANE Then
                    cboEQAnes.Text = cboEQAnes.Columns(1).Text
                    Exit For
                End If
            Next i
            If i > cboEQAnes.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_ANE, cboEQAnes)
        End If
        'tipo de anestesia
        If Not IsNull(rs!PR63CODTIPOANEST) Then
            For i = 1 To cboTipoAnest.Rows
                If i = 1 Then cboTipoAnest.MoveFirst Else cboTipoAnest.MoveNext
                If cboTipoAnest.Columns(0).Text = rs!PR63CODTIPOANEST Then
                    cboTipoAnest.Text = cboTipoAnest.Columns(1).Text
                    Exit For
                End If
            Next i
        End If
        'posici�n paciente
        If Not IsNull(rs!PR64CODPOSICION) Then
            For i = 1 To cboPosicion.Rows
                If i = 1 Then cboPosicion.MoveFirst Else cboPosicion.MoveNext
                If cboPosicion.Columns(0).Text = rs!PR64CODPOSICION Then
                    cboPosicion.Text = cboPosicion.Columns(1).Text
                    Exit For
                End If
            Next i
        End If
        'localizaci�n placa bistur�
        If Not IsNull(rs!PR65CODLOCPLACABIS) Then
            For i = 1 To cboLocPlBis.Rows
                If i = 1 Then cboLocPlBis.MoveFirst Else cboLocPlBis.MoveNext
                If cboLocPlBis.Columns(0).Text = rs!PR65CODLOCPLACABIS Then
                    cboLocPlBis.Text = cboLocPlBis.Columns(1).Text
                    Exit For
                End If
            Next i
        End If
        'hora inicio intervencion
        If Not IsNull(rs!PR62FECINI_INTERV) Then
            dcboFecha.Date = Format(rs!PR62FECINI_INTERV, "dd/mm/yyyy") 'fecha
            mskHoraIInt.Text = Format(rs!PR62FECINI_INTERV, "hh:mm")
        End If
        'hora fin intervenci�n
        If Not IsNull(rs!PR62FECFIN_INTERV) Then
            mskHoraFInt.Text = Format(rs!PR62FECFIN_INTERV, "hh:mm")
        End If
        'duraci�n de la intervenci�n
'        If Not IsNull(rs!PR62FECINI_INTERV) And Not IsNull(rs!PR62FECFIN_INTERV) Then
'            txtDuracion.Text = DateDiff("n", rs!PR62FECINI_INTERV, rs!PR62FECFIN_INTERV)
'        End If
        If Not IsNull(rs!PR62NUMMININT) Then
            txtDuracion.Text = rs!PR62NUMMININT
        End If
        'hora inicio anestesia
        If Not IsNull(rs!PR62FECINI_ANES) Then
            mskHoraIAnest.Text = Format(rs!PR62FECINI_ANES, "hh:mm")
        End If
        'hora fin anestesia
        If Not IsNull(rs!PR62FECFIN_ANES) Then
            mskHoraFAnest.Text = Format(rs!PR62FECFIN_ANES, "hh:mm")
        End If
        'hora inicio dpto colaborador
        If Not IsNull(rs!PR62FECINI_COL) Then
            mskHoraIEC.Text = Format(rs!PR62FECINI_COL, "hh:mm")
        End If
        'hora fin dpto colaborador
        If Not IsNull(rs!PR62FECFIN_COL) Then
            mskHoraFEC.Text = Format(rs!PR62FECFIN_COL, "hh:mm")
        End If
        'indicadores del caracter de la intervenci�n
        If Not IsNull(rs!PR62INDPROGRAMADA) Then chkProg.Value = Abs(rs!PR62INDPROGRAMADA)
        If Not IsNull(rs!PR62INDIMPREVISTA) Then chkImprev.Value = Abs(rs!PR62INDIMPREVISTA)
        If Not IsNull(rs!PR62INDURGENTE) Then chkUrg.Value = Abs(rs!PR62INDURGENTE)
        'estudios solicitados
        If Not IsNull(rs!PR62NUMAP_INTRA) Then txtAPIntra.Text = rs!PR62NUMAP_INTRA
        If Not IsNull(rs!PR62NUMAP_NOINTRA) Then txtAPNoIntra.Text = rs!PR62NUMAP_NOINTRA
        If Not IsNull(rs!PR62NUMMB) Then txtMB.Text = rs!PR62NUMMB
        If Not IsNull(rs!PR62INDRADIOTERAP) Then chkRadioterapia.Value = Abs(rs!PR62INDRADIOTERAP)
        If Not IsNull(rs!PR62INDECOGRAFIA) Then chkEndoscopia.Value = Abs(rs!PR62INDECOGRAFIA)
        If Not IsNull(rs!PR62INDRX) Then chkRx.Value = Abs(rs!PR62INDRX)
        If Not IsNull(rs!PR62INDCEC) Then chkCEC.Value = Abs(rs!PR62INDCEC)
        If Not IsNull(rs!PR62INDLASER) Then chkLaser.Value = Abs(rs!PR62INDLASER)
        If Not IsNull(rs!PR62INDINSTRUMENTAL) Then chkInstrumental.Value = Abs(rs!PR62INDINSTRUMENTAL)
        If Not IsNull(rs!PR62INDISQUEMIA) Then chkIsquemia.Value = Abs(rs!PR62INDISQUEMIA)
        If Not IsNull(rs!PR62INDCUANTAGASAS) Then chkCtaGasas.Value = Abs(rs!PR62INDCUANTAGASAS)
        If Not IsNull(rs!PR62DESOTROS) Then txtOtrosEstudios.Text = rs!PR62DESOTROS
        'observaciones
        If Not IsNull(rs!PR62OBSERV) Then txtObserv.Text = rs!PR62OBSERV
        'colaboradores
        If Not IsNull(rs!AD02CODDPTO_COL) Then
            For i = 1 To cboECDpto.Rows
                If i = 1 Then cboECDpto.MoveFirst Else cboECDpto.MoveNext
                If cboECDpto.Columns(0).Text = rs!AD02CODDPTO_COL Then
                    cboECDpto.Text = cboECDpto.Columns(1).Text
                    Exit For
                End If
            Next i
        End If
        If cboECDpto.Text <> "" Then
            Call pDatosEquipoColaborador(rs!AD02CODDPTO_COL)
            'cirujano colaborador
            If Not IsNull(rs!SG02COD_COLCIR) Then
                For i = 1 To cboECCir.Rows
                    If i = 1 Then cboECCir.MoveFirst Else cboECCir.MoveNext
                    If cboECCir.Columns(0).Text = rs!SG02COD_COLCIR Then
                        cboECCir.Text = cboECCir.Columns(1).Text
                        Exit For
                    End If
                Next i
                If i > cboECCir.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_COLCIR, cboECCir)
            End If
            'ayudante colaborador
            If Not IsNull(rs!SG02COD_COLAY) Then
                For i = 1 To cboECAy.Rows
                    If i = 1 Then cboECAy.MoveFirst Else cboECAy.MoveNext
                    If cboECAy.Columns(0).Text = rs!SG02COD_COLAY Then
                        cboECAy.Text = cboECAy.Columns(1).Text
                        Exit For
                    End If
                Next i
                If i > cboECAy.Rows Then Call pCargarUsuarioInactivo(rs!SG02COD_COLAY, cboECAy)
            End If
        End If
    End If
    rs.Close
    qry.Close
    If cboEQDpto.Text = "" Then
        cboEQDpto.MoveFirst
        cboEQDpto.Text = cboEQDpto.Columns(1).Text
        lngEQDpto = cboEQDpto.Columns(0).Text
        Call pDatosEquipoQuirurgico
    End If
    
    'tiempo de ocupaci�n del Quir�fano
    Call pTOcupQuirof
End Sub

Private Sub pCargarHQReadOnly()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    If strNumHQ = "" Then Exit Sub

    SQL = "SELECT AD02Q.AD02DESDPTO AD02DESDPTO_QUIR,"
    SQL = SQL & " SG02COD_CIR, NVL(SG02CIR.SG02TXTFIRMA, SG02CIR.SG02APE1||' '||SG02CIR.SG02APE2||', '||SG02CIR.SG02NOM) CIR,"
    SQL = SQL & " SG02COD_PAY, NVL(SG02PAY.SG02TXTFIRMA, SG02PAY.SG02APE1||' '||SG02PAY.SG02APE2||', '||SG02PAY.SG02NOM) PAY,"
    SQL = SQL & " SG02COD_SAY, NVL(SG02SAY.SG02TXTFIRMA, SG02SAY.SG02APE1||' '||SG02SAY.SG02APE2||', '||SG02SAY.SG02NOM) SAY,"
    SQL = SQL & " SG02COD_PINS, NVL(SG02PINS.SG02TXTFIRMA, SG02PINS.SG02APE1||' '||SG02PINS.SG02APE2||', '||SG02PINS.SG02NOM) PINS,"
    SQL = SQL & " SG02COD_SINS, NVL(SG02SINS.SG02TXTFIRMA, SG02SINS.SG02APE1||' '||SG02SINS.SG02APE2||', '||SG02SINS.SG02NOM) SINS,"
    SQL = SQL & " SG02COD_PENF, NVL(SG02PENF.SG02TXTFIRMA, SG02PENF.SG02APE1||' '||SG02PENF.SG02APE2||', '||SG02PENF.SG02NOM) PENF,"
    SQL = SQL & " SG02COD_SENF, NVL(SG02SENF.SG02TXTFIRMA, SG02SENF.SG02APE1||' '||SG02SENF.SG02APE2||', '||SG02SENF.SG02NOM) SENF,"
    SQL = SQL & " SG02COD_ANE, NVL(SG02ANE.SG02TXTFIRMA, SG02ANE.SG02APE1||' '||SG02ANE.SG02APE2||', '||SG02ANE.SG02NOM) ANE,"
    SQL = SQL & " AD02C.AD02DESDPTO AD02DESDPTO_COL,"
    SQL = SQL & " SG02COD_COLCIR, NVL(SG02COLCIR.SG02TXTFIRMA, SG02COLCIR.SG02APE1||' '||SG02COLCIR.SG02APE2||', '||SG02COLCIR.SG02NOM) COLCIR,"
    SQL = SQL & " SG02COD_COLAY, NVL(SG02COLAY.SG02TXTFIRMA, SG02COLAY.SG02APE1||' '||SG02COLAY.SG02APE2||', '||SG02COLAY.SG02NOM) COLAY,"
    SQL = SQL & " PR62FECINI_INTERV, PR62FECFIN_INTERV, PR62NUMMININT,"
    SQL = SQL & " PR62FECINI_ANES, PR62FECFIN_ANES,"
    SQL = SQL & " PR62FECINI_COL, PR62FECFIN_COL,"
    SQL = SQL & " PR63DESTIPOANEST, PR64DESPOSICION, PR65DESLOCPLACABIS,"
    SQL = SQL & " PR62INDPROGRAMADA, PR62INDIMPREVISTA, PR62INDURGENTE,"
    SQL = SQL & " PR62NUMAP_INTRA, PR62NUMAP_NOINTRA, PR62NUMMB,"
    SQL = SQL & " PR62INDRADIOTERAP, PR62INDECOGRAFIA, PR62INDRX, PR62INDCEC, PR62INDLASER,"
    SQL = SQL & " PR62INDINSTRUMENTAL, PR62INDISQUEMIA, PR62INDCUANTAGASAS,"
    SQL = SQL & " PR62DESOTROS, PR62OBSERV"
    SQL = SQL & " FROM PR6200, AD0200 AD02Q, SG0200 SG02CIR, SG0200 SG02PAY, SG0200 SG02SAY, SG0200 SG02PINS,"
    SQL = SQL & " SG0200 SG02SINS, SG0200 SG02PENF, SG0200 SG02SENF, SG0200 SG02ANE,"
    SQL = SQL & " AD0200 AD02C, SG0200 SG02COLCIR, SG0200 SG02COLAY, PR6300, PR6400, PR6500"
    SQL = SQL & " WHERE PR62CODHOJAQUIR = ?"
    SQL = SQL & " AND AD02Q.AD02CODDPTO (+)= PR6200.AD02CODDPTO"
    SQL = SQL & " AND SG02CIR.SG02COD (+)= SG02COD_CIR"
    SQL = SQL & " AND SG02PAY.SG02COD (+)= SG02COD_PAY"
    SQL = SQL & " AND SG02SAY.SG02COD (+)= SG02COD_SAY"
    SQL = SQL & " AND SG02PINS.SG02COD (+)= SG02COD_PINS"
    SQL = SQL & " AND SG02SINS.SG02COD (+)= SG02COD_SINS"
    SQL = SQL & " AND SG02PENF.SG02COD (+)= SG02COD_PENF"
    SQL = SQL & " AND SG02SENF.SG02COD (+)= SG02COD_SENF"
    SQL = SQL & " AND SG02ANE.SG02COD (+)= SG02COD_ANE"
    SQL = SQL & " AND AD02C.AD02CODDPTO (+)= AD02CODDPTO_COL"
    SQL = SQL & " AND SG02COLCIR.SG02COD (+)= SG02COD_COLCIR"
    SQL = SQL & " AND SG02COLAY.SG02COD (+)= SG02COD_COLAY"
    SQL = SQL & " AND PR6300.PR63CODTIPOANEST (+)= PR6200.PR63CODTIPOANEST"
    SQL = SQL & " AND PR6400.PR64CODPOSICION (+)= PR6200.PR64CODPOSICION"
    SQL = SQL & " AND PR6500.PR65CODLOCPLACABIS (+)= PR6200.PR65CODLOCPLACABIS"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumHQ
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        'equipos
        If Not IsNull(rs!AD02DESDPTO_QUIR) Then cboEQDpto.Text = rs!AD02DESDPTO_QUIR
        If Not IsNull(rs!SG02COD_CIR) Then cboEQCir.Text = rs!CIR
        If Not IsNull(rs!SG02COD_PAY) Then cboEQAy1.Text = rs!PAY
        If Not IsNull(rs!SG02COD_SAY) Then cboEQAy2.Text = rs!SAY
        If Not IsNull(rs!SG02COD_PINS) Then cboEQIns1.Text = rs!PINS
        If Not IsNull(rs!SG02COD_SINS) Then cboEQIns2.Text = rs!SINS
        If Not IsNull(rs!SG02COD_PENF) Then cboEQEnf1.Text = rs!PENF
        If Not IsNull(rs!SG02COD_SENF) Then cboEQEnf2.Text = rs!SENF
        If Not IsNull(rs!SG02COD_ANE) Then cboEQAnes.Text = rs!ANE
        If Not IsNull(rs!AD02DESDPTO_COL) Then cboECDpto.Text = rs!AD02DESDPTO_COL
        If Not IsNull(rs!SG02COD_COLCIR) Then cboECCir.Text = rs!COLCIR
        If Not IsNull(rs!SG02COD_COLAY) Then cboECAy.Text = rs!COLAY
        
        'hora inicio intervencion
        If Not IsNull(rs!PR62FECINI_INTERV) Then
            dcboFecha.Date = Format(rs!PR62FECINI_INTERV, "dd/mm/yyyy") 'fecha
            mskHoraIInt.Text = Format(rs!PR62FECINI_INTERV, "hh:mm")
        End If
        'hora fin intervenci�n
        If Not IsNull(rs!PR62FECFIN_INTERV) Then
            mskHoraFInt.Text = Format(rs!PR62FECFIN_INTERV, "hh:mm")
        End If
        'duraci�n de la intervenci�n
        If Not IsNull(rs!PR62NUMMININT) Then txtDuracion.Text = rs!PR62NUMMININT
        'hora inicio anestesia
        If Not IsNull(rs!PR62FECINI_ANES) Then
            dcboFecha.Date = Format(rs!PR62FECINI_ANES, "dd/mm/yyyy") 'fecha
            mskHoraIAnest.Text = Format(rs!PR62FECINI_ANES, "hh:mm")
        End If
        'hora fin anestesia
        If Not IsNull(rs!PR62FECFIN_ANES) Then
            mskHoraFAnest.Text = Format(rs!PR62FECFIN_ANES, "hh:mm")
        End If
        'hora inicio dpto colaborador
        If Not IsNull(rs!PR62FECINI_COL) Then
            mskHoraIEC.Text = Format(rs!PR62FECINI_COL, "hh:mm")
        End If
        'hora fin dpto colaborador
        If Not IsNull(rs!PR62FECFIN_COL) Then
            mskHoraFEC.Text = Format(rs!PR62FECFIN_COL, "hh:mm")
        End If
        
        'otros datos de la intervenci�n
        If Not IsNull(rs!PR63DESTIPOANEST) Then cboTipoAnest.Text = rs!PR63DESTIPOANEST
        If Not IsNull(rs!PR64DESPOSICION) Then cboPosicion.Text = rs!PR64DESPOSICION
        If Not IsNull(rs!PR65DESLOCPLACABIS) Then cboLocPlBis.Text = rs!PR65DESLOCPLACABIS

        'indicadores del caracter de la intervenci�n
        If Not IsNull(rs!PR62INDPROGRAMADA) Then chkProg.Value = Abs(rs!PR62INDPROGRAMADA)
        If Not IsNull(rs!PR62INDIMPREVISTA) Then chkImprev.Value = Abs(rs!PR62INDIMPREVISTA)
        If Not IsNull(rs!PR62INDURGENTE) Then chkUrg.Value = Abs(rs!PR62INDURGENTE)
        'estudios solicitados
        If Not IsNull(rs!PR62NUMAP_INTRA) Then txtAPIntra.Text = rs!PR62NUMAP_INTRA
        If Not IsNull(rs!PR62NUMAP_NOINTRA) Then txtAPNoIntra.Text = rs!PR62NUMAP_NOINTRA
        If Not IsNull(rs!PR62NUMMB) Then txtMB.Text = rs!PR62NUMMB
        If Not IsNull(rs!PR62INDRADIOTERAP) Then chkRadioterapia.Value = Abs(rs!PR62INDRADIOTERAP)
        If Not IsNull(rs!PR62INDECOGRAFIA) Then chkEndoscopia.Value = Abs(rs!PR62INDECOGRAFIA)
        If Not IsNull(rs!PR62INDRX) Then chkRx.Value = Abs(rs!PR62INDRX)
        If Not IsNull(rs!PR62INDCEC) Then chkCEC.Value = Abs(rs!PR62INDCEC)
        If Not IsNull(rs!PR62INDLASER) Then chkLaser.Value = Abs(rs!PR62INDLASER)
        If Not IsNull(rs!PR62INDINSTRUMENTAL) Then chkInstrumental.Value = Abs(rs!PR62INDINSTRUMENTAL)
        If Not IsNull(rs!PR62INDISQUEMIA) Then chkIsquemia.Value = Abs(rs!PR62INDISQUEMIA)
        If Not IsNull(rs!PR62INDCUANTAGASAS) Then chkCtaGasas.Value = Abs(rs!PR62INDCUANTAGASAS)
        If Not IsNull(rs!PR62DESOTROS) Then txtOtrosEstudios.Text = rs!PR62DESOTROS
        'observaciones
        If Not IsNull(rs!PR62OBSERV) Then txtObserv.Text = rs!PR62OBSERV
    End If
    rs.Close
    qry.Close
        
    'tiempo de ocupaci�n del Quir�fano
    Call pTOcupQuirof
End Sub

Private Sub pDatosAsociadosInterv()
    Dim SQL$, rs As rdoResultset

    'Tipos de anestesia
    SQL = "SELECT PR63CODTIPOANEST, PR63DESTIPOANEST"
    SQL = SQL & " FROM PR6300"
    SQL = SQL & " ORDER BY PR63DESTIPOANEST"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboTipoAnest.AddItem rs!PR63CODTIPOANEST & Chr$(9) & rs!PR63DESTIPOANEST
        rs.MoveNext
    Loop
    rs.Close

    'Posiciones paciente
    SQL = "SELECT PR64CODPOSICION, PR64DESPOSICION"
    SQL = SQL & " FROM PR6400"
    SQL = SQL & " ORDER BY PR64DESPOSICION"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboPosicion.AddItem rs!PR64CODPOSICION & Chr$(9) & rs!PR64DESPOSICION
        rs.MoveNext
    Loop
    rs.Close

    'Localizaci�n placa bistur�
    SQL = "SELECT PR65CODLOCPLACABIS, PR65DESLOCPLACABIS"
    SQL = SQL & " FROM PR6500"
    SQL = SQL & " ORDER BY PR65DESLOCPLACABIS"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboLocPlBis.AddItem rs!PR65CODLOCPLACABIS & Chr$(9) & rs!PR65DESLOCPLACABIS
        rs.MoveNext
    Loop
    rs.Close
End Sub

Private Sub pCargarDatosFijos()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT PR0400.AD02CODDPTO,"
    SQL = SQL & " CI22NUMHISTORIA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
    SQL = SQL & " LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') QUIROFANO"
    SQL = SQL & " FROM PR0400, CI2200, CI0100, AG1100"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND CI0100.PR04NUMACTPLAN = PR0400.PR04NUMACTPLAN"
    SQL = SQL & " AND CI01SITCITA = '" & constESTCITA_CITADA & "'"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Val(strNAPlan)
    Set rs = qry.OpenResultset()
    txtNH.Text = rs!CI22NUMHISTORIA
    txtPac.Text = rs!PACIENTE
    txtQuir.Text = rs!QUIROFANO
    rs.Close
    qry.Close
    
    'NOTA: la duraci�n estimada del 'Acto M�dico' se obtiene como la duraci�n de cualquiera
    'de las intervenciones que lo componen ya que todas ellas van a tener la misma duraci�n
    'se�alada en el cuestionario (se arrastra tanto al pedir varias intervenciones conjuntas
    'como al realizar cambios de intervenciones). El �nico caso en el que se pueden dar
    'problemas es cuando se piden varias intervenciones por separado para el mismo paciente
    'y a cada una de ellas se le se�ala una duraci�n concreta que en realidad habr�a que
    'sumar
    SQL = "SELECT PR41DUR.PR41RESPUESTA, PR41ORD.PR41RESPUESTA"
    SQL = SQL & " FROM PR0400, PR4100 PR41DUR, PR4100 PR41ORD"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND PR41DUR.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41DUR.PR40CODPREGUNTA (+)= " & constPREG_DURACION
    SQL = SQL & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = Val(strNAPlan)
    Set rs = qry.OpenResultset()
    If Not IsNull(rs(1)) Then txtOrden.Text = rs(1)
    If Not IsNull(rs(0)) Then
        If IsNumeric(rs(0)) Then txtTEstimado.Text = rs(0)
    End If
    rs.Close
    qry.Close
        
''    Dim strOrden$, lngTEstimado&
''    SQL = "SELECT PR41DUR.PR41RESPUESTA, PR41ORD.PR41RESPUESTA"
''    SQL = SQL & " FROM PR0400, PR4100 PR41DUR, PR4100 PR41ORD"
''    SQL = SQL & " WHERE PR04NUMACTPLAN IN (" & strNAPlan & ")"
''    SQL = SQL & " AND PR41DUR.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
''    SQL = SQL & " AND PR41DUR.PR40CODPREGUNTA (+)= " & constPREG_DURACION
''    SQL = SQL & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
''    SQL = SQL & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
''    Set rs = objApp.rdoConnect.OpenResultset(SQL)
''    Do While Not rs.EOF
''        If strOrden = "" Then
''            If Not IsNull(rs(1)) Then strOrden = rs(1)
''        End If
''        If Not IsNull(rs(0)) Then
''            If IsNumeric(rs(0)) Then lngTEstimado = lngTEstimado + rs(0)
''        End If
''        rs.MoveNext
''    Loop
''    rs.Close
''    txtOrden.Text = strOrden
''    txtTEstimado.Text = lngTEstimado
End Sub

Private Sub pDatosEquipoQuirurgico()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset

    'Cirujanos
    cboEQCir.RemoveAll: cboEQCir.Text = ""
    SQL = "SELECT SG0200.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) NOMBRE"
    SQL = SQL & " FROM SG0200, AD0300"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AD31CODPUESTO IN (" & constPUESTO_CONSULTOR & "," & constPUESTO_COLABORADOR & ")"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0300.SG02COD"
    SQL = SQL & " AND SG02FECDES IS NULL"
    SQL = SQL & " AND SG0200.SG02COD <> 'SDR'"
    SQL = SQL & " ORDER BY SG02APE1||' '||SG02APE2||', '||SG02NOM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngEQDpto
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboEQCir.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'Ayudantes
    cboEQAy1.RemoveAll: cboEQAy1.Text = ""
    cboEQAy2.RemoveAll: cboEQAy2.Text = ""
    SQL = "SELECT SG0200.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) NOMBRE"
    SQL = SQL & " FROM SG0200, AD0300"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AD31CODPUESTO IN (" & constPUESTO_RESIDENTE & "," & constPUESTO_CONSULTOR & "," & constPUESTO_COLABORADOR & ")"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0300.SG02COD"
    SQL = SQL & " AND SG02FECDES IS NULL"
    SQL = SQL & " AND SG0200.SG02COD <> 'SDR'"
    SQL = SQL & " ORDER BY SG02APE1||' '||SG02APE2||', '||SG02NOM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngEQDpto
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboEQAy1.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        cboEQAy2.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pDatosEquipoQuirurgicoFijo()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
     
    'Anestesi�logos
    SQL = "SELECT SG0200.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) NOMBRE"
    SQL = SQL & " FROM SG0200, AD0300"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AD31CODPUESTO IN (" & constPUESTO_CONSULTOR & "," & constPUESTO_COLABORADOR & ")"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0300.SG02COD"
    SQL = SQL & " AND SG02FECDES IS NULL"
    SQL = SQL & " AND SG0200.SG02COD <> 'SA'"
    SQL = SQL & " AND SG0200.SG02COD <> 'SDR'"
    SQL = SQL & " ORDER BY SG02APE1||' '||SG02APE2||', '||SG02NOM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = constDPTO_ANESTESIA
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboEQAnes.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'Instrumentistas y Enfermeras
    SQL = "SELECT SG0200.SG02COD, SG02APE1||' '||SG02APE2||', '||SG02NOM NOMBRE"
    SQL = SQL & " FROM SG0200, AD0300"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AD31CODPUESTO = ?"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0300.SG02COD"
    SQL = SQL & " AND SG02FECDES IS NULL"
    SQL = SQL & " ORDER BY SG02APE1||' '||SG02APE2||', '||SG02NOM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = constDPTO_QUIROFANO
    qry(1) = constPUESTO_ENFERMERA
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboEQIns1.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        cboEQIns2.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        cboEQEnf1.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        cboEQEnf2.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub pDatosDptosColaboradores()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset

    SQL = "SELECT DISTINCT AD0200.AD02CODDPTO, AD02DESDPTO"
    SQL = SQL & " FROM AD0200, PR0200, PR0100"
    SQL = SQL & " WHERE PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    SQL = SQL & " AND PR0100.PR01FECFIN IS NULL"
    SQL = SQL & " AND PR0200.PR01CODACTUACION = PR0100.PR01CODACTUACION"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0200.AD02CODDPTO"
    SQL = SQL & " AND AD0200.AD02FECFIN IS NULL"
    SQL = SQL & " ORDER BY AD02DESDPTO"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboECDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
        rs.MoveNext
    Loop
    rs.Close
End Sub

Private Sub cboECDpto_Click()
    If cboECDpto.Text <> cboECDpto.Columns(0).Text Then
        cboECCir.Text = "": cboECCir.RemoveAll
        cboECAy.Text = "": cboECAy.RemoveAll
        Call pDatosEquipoColaborador(cboECDpto.Columns(0).Text)
    End If
    If intEstado = constESTADOHQ_FINALIZADA Then
        cboECCir.BackColor = objApp.objUserColor.lngMandatory
        mskHoraIEC.BackColor = objApp.objUserColor.lngMandatory
        mskHoraFEC.BackColor = objApp.objUserColor.lngMandatory
    End If
End Sub

Private Sub pDatosEquipoColaborador(lngECDpto&)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset

    'Cirujanos
    SQL = "SELECT SG0200.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) NOMBRE"
    SQL = SQL & " FROM SG0200, AD0300"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AD31CODPUESTO IN (" & constPUESTO_CONSULTOR & "," & constPUESTO_COLABORADOR & ")"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0300.SG02COD"
    SQL = SQL & " AND SG02FECDES IS NULL"
    SQL = SQL & " AND SG0200.SG02COD <> 'SDR'"
    SQL = SQL & " ORDER BY SG02APE1||' '||SG02APE2||', '||SG02NOM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngECDpto
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboECCir.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    'Ayudantes
    SQL = "SELECT SG0200.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) NOMBRE"
    SQL = SQL & " FROM SG0200, AD0300"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND AD31CODPUESTO IN (" & constPUESTO_RESIDENTE & "," & constPUESTO_CONSULTOR & "," & constPUESTO_COLABORADOR & ")"
    SQL = SQL & " AND AD03FECFIN IS NULL"
    SQL = SQL & " AND SG0200.SG02COD = AD0300.SG02COD"
    SQL = SQL & " AND SG02FECDES IS NULL"
    SQL = SQL & " AND SG0200.SG02COD <> 'SDR'"
    SQL = SQL & " ORDER BY SG02APE1||' '||SG02APE2||', '||SG02NOM"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngECDpto
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        cboECAy.AddItem rs!SG02COD & Chr$(9) & rs!Nombre
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Sub cboEQAnes_Click()
    If intEstado = constESTADOHQ_FINALIZADA Then
        mskHoraIAnest.BackColor = objApp.objUserColor.lngMandatory
        mskHoraFAnest.BackColor = objApp.objUserColor.lngMandatory
        cboTipoAnest.BackColor = objApp.objUserColor.lngMandatory
    End If
End Sub

Private Function fComprobaciones() As Boolean
    Dim c As Control, strColor$, msg$
    
    'campos obligatorios
    If intEstado = constESTADOHQ_FINALIZADA Then
        On Error Resume Next
        For Each c In Me
            strColor = c.BackColor
            If strColor = objApp.objUserColor.lngMandatory Then
                If c.Text = "" Or c.Text = "__:__" Then
                    msg = "Existen datos obligatorios sin rellenar."
                    MsgBox msg, vbExclamation, Me.Caption
                    On Error GoTo 0
                    Exit Function
                End If
            End If
        Next
        On Error GoTo 0
    End If
    
    'comprobaci�n concordancia anestesi�logo-anestesia
    If cboTipoAnest.Text <> "" And cboEQAnes.Text = "" Then
        If cboTipoAnest.Columns(0).Text <> constANESTESIA_LOCAL Then
            msg = "Se ha indicado el uso de anestes�a pero no se ha se�alado ning�n anestesi�logo."
            msg = msg & Chr$(13)
            msg = msg & "�Est� Ud. de acuerdo con estos datos?"
            If MsgBox(msg, vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Function
        End If
    End If
    
    'comprobaci�n de las horas de inicio y fin
    Dim strHII$, strHFI$, strHIA$, strHFA$, strHIC$, strHFC$
    Dim strHRef$
    Dim msg1$

    If mskHoraIInt.Text <> "__:__" Then strHII = mskHoraIInt.Text
    If mskHoraFInt.Text <> "__:__" Then strHFI = mskHoraFInt.Text
    If mskHoraIAnest.Text <> "__:__" Then strHIA = mskHoraIAnest.Text
    If mskHoraFAnest.Text <> "__:__" Then strHFA = mskHoraFAnest.Text
    If mskHoraIEC.Text <> "__:__" Then strHIC = mskHoraIEC.Text
    If mskHoraFEC.Text <> "__:__" Then strHFC = mskHoraFEC.Text

    'Fecha
    If strHIA <> "" Then
        strHRef = strHIA
    ElseIf strHII <> "" Then
        strHRef = strHII
    ElseIf strHIC <> "" Then
        strHRef = strHIC
    End If
    If strHRef <> "" Then
        If CDate(dcboFecha & " " & strHRef) > CDate(strAhora) Then
            msg = "La fecha-hora de inicio se�alada como referencia es posterior a la hora actual."
            MsgBox msg, vbExclamation, Me.Caption
            Exit Function
        End If
    End If

    'Horas de anestesia
    If strHIA <> "" And strHFA <> "" Then
        If CDate(strHIA) >= CDate(strHFA) Then msg1 = msg1 & "- Hora de inicio y finalizaci�n de la Anestesia" & Chr$(13)
    End If
    'Horas de intervenci�n
    If strHII <> "" And strHFI <> "" Then
        If CDate(strHII) >= CDate(strHFI) Then msg1 = msg1 & "- Hora de inicio y finalizaci�n de la Intervenci�n" & Chr$(13)
    End If
    'Horas de colaborador
    If strHIC <> "" And strHFC <> "" Then
        If CDate(strHIC) >= CDate(strHFC) Then msg1 = msg1 & "- Hora de inicio y finalizaci�n del Dpto. Colaborador" & Chr$(13)
    End If
    'Horas de anestesia-intervenci�n
    If strHII <> "" And strHIA <> "" Then
        If CDate(strHIA) > CDate(strHII) Then msg1 = msg1 & "- Relaci�n de las horas de inicio de la Anestesia y la Intervenci�n" & Chr$(13)
    End If
    If strHFI <> "" And strHFA <> "" Then
        If CDate(strHFI) > CDate(strHFA) Then msg1 = msg1 & "- Relaci�n de las horas de finalizaci�n de la Anestesia y la Intervenci�n" & Chr$(13)
    End If
    'Horas de intervenci�n-colaborador
    If strHII <> "" And strHIC <> "" Then
        If CDate(strHII) > CDate(strHIC) Then msg1 = msg1 & "- Relaci�n de las horas de inicio la Intervenci�n y el Dpto. Colaborador" & Chr$(13)
    End If
    If strHFI <> "" And strHFC <> "" Then
        If CDate(strHFC) > CDate(strHFI) Then msg1 = msg1 & "- Relaci�n de las horas de finalizaci�n la Intervenci�n y el Dpto. Colaborador" & Chr$(13)
    End If
    
    If msg1 <> "" Then
        msg = "Se han detectado posibles discrepancias en las siguientes horas marcadas:"
        msg = msg & Chr$(13) & Chr$(13) & msg1 & Chr$(13)
        msg = msg & "�Desea Ud. realizar una comprobaci�n antes de Salir?"
        If MsgBox(msg, vbQuestion + vbYesNo, Me.Caption) = vbYes Then Exit Function
    End If
    
    fComprobaciones = True
End Function

Private Sub pInsertarHoja()
    Dim SQL$, qry As rdoQuery
    
    strNumHQ = fNextClave("PR62CODHOJAQUIR")
    
    objApp.BeginTrans
    
    SQL = "INSERT INTO PR6200 (PR62CODHOJAQUIR, PR62CODESTADO_FAR) VALUES (?,?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumHQ
    qry(1) = 1
    qry.Execute
    qry.Close

    SQL = "UPDATE PR0400"
    SQL = SQL & " SET PR62CODHOJAQUIR = ?"
    SQL = SQL & " WHERE PR04NUMACTPLAN IN (" & strNAPlan & ")"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumHQ
    qry.Execute
    qry.Close
    
    objApp.CommitTrans
End Sub

Private Sub pGuardarHoja()
'****************************************************************************************
'*  Se guardan de nuevo todos los datos de la Hoja
'*  Se podr�a controlar cuales han cambiado desde que se accedi� a la hoja y montar los
'*  UPDATE incluyendo s�lo esos campos que han cambiado pero es mucho m�s costoso
'*
'*  Para las fechas, se toma como referencia la Hora de Inicio de la Anestesia y si
'*  Las horas menores que la hora de referencia se tomar�n como horas del del d�a siguiente
'****************************************************************************************
    Dim SQL$, qry As rdoQuery
    Dim strHoraRef$
    
    If mskHoraIAnest.Text <> "__:__" Then
        strHoraRef = mskHoraIAnest.Text
    ElseIf mskHoraIInt.Text <> "__:__" Then
        strHoraRef = mskHoraIInt.Text
    Else
        strHoraRef = "00:00"
    End If

    SQL = "UPDATE PR6200 SET"
    SQL = SQL & " SG02COD_CIR = ?,"
    SQL = SQL & " SG02COD_PAY = ?,"
    SQL = SQL & " SG02COD_SAY = ?,"
    SQL = SQL & " SG02COD_PINS = ?,"
    SQL = SQL & " SG02COD_SINS = ?,"
    SQL = SQL & " SG02COD_PENF = ?,"
    SQL = SQL & " SG02COD_SENF = ?,"
    SQL = SQL & " PR62FECINI_INTERV = TO_DATE(?,'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & " PR62FECFIN_INTERV = TO_DATE(?,'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & " PR62NUMMININT = ?,"
    SQL = SQL & " SG02COD_ANE = ?,"
    SQL = SQL & " PR62FECINI_ANES = TO_DATE(?,'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & " PR62FECFIN_ANES = TO_DATE(?,'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & " PR63CODTIPOANEST = ?,"
    SQL = SQL & " PR64CODPOSICION = ?,"
    SQL = SQL & " PR65CODLOCPLACABIS = ?,"
    SQL = SQL & " AD02CODDPTO_COL = ?,"
    SQL = SQL & " SG02COD_COLCIR = ?,"
    SQL = SQL & " SG02COD_COLAY = ?,"
    SQL = SQL & " PR62FECINI_COL = TO_DATE(?,'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & " PR62FECFIN_COL = TO_DATE(?,'DD/MM/YYYY HH24:MI'),"
    SQL = SQL & " PR62INDPROGRAMADA = ?,"
    SQL = SQL & " PR62INDIMPREVISTA = ?,"
    SQL = SQL & " PR62INDURGENTE = ?,"
    SQL = SQL & " PR62NUMAP_INTRA = ?,"
    SQL = SQL & " PR62NUMAP_NOINTRA = ?,"
    SQL = SQL & " PR62NUMMB = ?,"
    SQL = SQL & " PR62INDRADIOTERAP = ?,"
    SQL = SQL & " PR62INDECOGRAFIA = ?,"
    SQL = SQL & " PR62INDRX = ?,"
    SQL = SQL & " PR62INDCEC = ?,"
    SQL = SQL & " PR62INDLASER = ?,"
    SQL = SQL & " PR62INDINSTRUMENTAL = ?,"
    SQL = SQL & " PR62INDISQUEMIA = ?,"
    SQL = SQL & " PR62INDCUANTAGASAS = ?,"
    SQL = SQL & " PR62DESOTROS = ?,"
    SQL = SQL & " PR62OBSERV = ?,"
    SQL = SQL & " PR62CODESTADO = ?,"
    SQL = SQL & " AD02CODDPTO = ?"
    SQL = SQL & " WHERE PR62CODHOJAQUIR = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    If cboEQCir.Text <> "" Then qry(0) = cboEQCir.Columns(0).Text Else qry(0) = Null
    If cboEQAy1.Text <> "" Then qry(1) = cboEQAy1.Columns(0).Text Else qry(1) = Null
    If cboEQAy2.Text <> "" Then qry(2) = cboEQAy2.Columns(0).Text Else qry(2) = Null
    If cboEQIns1.Text <> "" Then qry(3) = cboEQIns1.Columns(0).Text Else qry(3) = Null
    If cboEQIns2.Text <> "" Then qry(4) = cboEQIns2.Columns(0).Text Else qry(4) = Null
    If cboEQEnf1.Text <> "" Then qry(5) = cboEQEnf1.Columns(0).Text Else qry(5) = Null
    If cboEQEnf2.Text <> "" Then qry(6) = cboEQEnf2.Columns(0).Text Else qry(6) = Null
    If mskHoraIInt.Text <> "__:__" Then
        If CDate(mskHoraIInt.Text) >= CDate(strHoraRef) Then
            qry(7) = Format(dcboFecha.Date & " " & mskHoraIInt.Text, "dd/mm/yyyy hh:mm")
        Else
            qry(7) = Format(DateAdd("d", 1, dcboFecha.Date) & " " & mskHoraIInt.Text, "dd/mm/yyyy hh:mm")
        End If
        If mskHoraFInt.Text <> "__:__" Then 's�lo si guarda si existe hora inicio
            If CDate(mskHoraFInt.Text) >= CDate(strHoraRef) Then
                qry(8) = Format(dcboFecha.Date & " " & mskHoraFInt.Text, "dd/mm/yyyy hh:mm")
            Else
                qry(8) = Format(DateAdd("d", 1, dcboFecha.Date) & " " & mskHoraFInt.Text, "dd/mm/yyyy hh:mm")
            End If
        Else
            qry(8) = Null
        End If
    Else
        qry(7) = Null
        qry(8) = Null
    End If
    If txtDuracion.Text <> "" Then qry(9) = txtDuracion.Text Else qry(9) = Null
    If cboEQAnes.Text <> "" Then qry(10) = cboEQAnes.Columns(0).Text Else qry(10) = Null
    If mskHoraIAnest.Text <> "__:__" Then
        If CDate(mskHoraIAnest.Text) >= CDate(strHoraRef) Then
            qry(11) = Format(dcboFecha.Date & " " & mskHoraIAnest.Text, "dd/mm/yyyy hh:mm")
        Else
            qry(11) = Format(DateAdd("d", 1, dcboFecha.Date) & " " & mskHoraIAnest.Text, "dd/mm/yyyy hh:mm")
        End If
        If mskHoraFAnest.Text <> "__:__" Then 's�lo si guarda si existe hora inicio
            If CDate(mskHoraFAnest.Text) >= CDate(strHoraRef) Then
                qry(12) = Format(dcboFecha.Date & " " & mskHoraFAnest.Text, "dd/mm/yyyy hh:mm")
            Else
                qry(12) = Format(DateAdd("d", 1, dcboFecha.Date) & " " & mskHoraFAnest.Text, "dd/mm/yyyy hh:mm")
            End If
        Else
            qry(12) = Null
        End If
    Else
        qry(11) = Null
        qry(12) = Null
    End If
    If cboTipoAnest.Text <> "" Then qry(13) = cboTipoAnest.Columns(0).Text Else qry(13) = Null
    If cboPosicion.Text <> "" Then qry(14) = cboPosicion.Columns(0).Text Else qry(14) = Null
    If cboLocPlBis.Text <> "" Then qry(15) = cboLocPlBis.Columns(0).Text Else qry(15) = Null
    If cboECDpto.Text <> "" Then qry(16) = cboECDpto.Columns(0).Text Else qry(16) = Null
    If cboECCir.Text <> "" Then qry(17) = cboECCir.Columns(0).Text Else qry(17) = Null
    If cboECAy.Text <> "" Then qry(18) = cboECAy.Columns(0).Text Else qry(18) = Null
    If mskHoraIEC.Text <> "__:__" Then
        If CDate(mskHoraIEC.Text) >= CDate(strHoraRef) Then
            qry(19) = Format(dcboFecha.Date & " " & mskHoraIEC.Text, "dd/mm/yyyy hh:mm")
        Else
            qry(19) = Format(DateAdd("d", 1, dcboFecha.Date) & " " & mskHoraIEC.Text, "dd/mm/yyyy hh:mm")
        End If
        If mskHoraFEC.Text <> "__:__" Then 's�lo si guarda si existe hora inicio
            If CDate(mskHoraFEC.Text) >= CDate(strHoraRef) Then
                qry(20) = Format(dcboFecha.Date & " " & mskHoraFEC.Text, "dd/mm/yyyy hh:mm")
            Else
                qry(20) = Format(DateAdd("d", 1, dcboFecha.Date) & " " & mskHoraFEC.Text, "dd/mm/yyyy hh:mm")
            End If
        Else
            qry(20) = Null
        End If
    Else
        qry(19) = Null
        qry(20) = Null
    End If
    qry(21) = -chkProg.Value
    qry(22) = -chkImprev.Value
    qry(23) = -chkUrg.Value
    If txtAPIntra.Text <> "" Then qry(24) = txtAPIntra.Text Else qry(24) = Null
    If txtAPNoIntra.Text <> "" Then qry(25) = txtAPNoIntra.Text Else qry(25) = Null
    If txtMB.Text <> "" Then qry(26) = txtMB.Text Else qry(26) = Null
    qry(27) = -chkRadioterapia.Value
    qry(28) = -chkEndoscopia.Value
    qry(29) = -chkRx.Value
    qry(30) = -chkCEC.Value
    qry(31) = -chkLaser.Value
    qry(32) = -chkInstrumental.Value
    qry(33) = -chkIsquemia.Value
    qry(34) = -chkCtaGasas.Value
    If txtOtrosEstudios.Text <> "" Then qry(35) = txtOtrosEstudios.Text Else qry(35) = Null
    If txtObserv.Text <> "" Then qry(36) = txtObserv.Text Else qry(36) = Null
    qry(37) = intEstado
    If cboEQDpto.Text <> "" Then qry(38) = cboEQDpto.Columns(0).Text Else qry(38) = Null
    qry(39) = strNumHQ
    qry.Execute
    qry.Close
End Sub

Private Sub pTOcupQuirof()
    Dim strHI$, strHF$, lngMin&
    
    If mskHoraIAnest.Text <> "__:__" Then
        strHI = mskHoraIAnest.Text
    ElseIf mskHoraIInt.Text <> "__:__" Then
        strHI = mskHoraIInt.Text
    End If
    If mskHoraFAnest.Text <> "__:__" Then
        strHF = mskHoraFAnest.Text
    ElseIf mskHoraFInt.Text <> "__:__" Then
        strHF = mskHoraFInt.Text
    End If
    If strHI <> "" And strHF <> "" Then
        lngMin = DateDiff("n", CDate(strHI), CDate(strHF))
        If lngMin < 0 Then lngMin = 1440 + lngMin
        txtTOcupQuirof.Text = lngMin
    Else
        txtTOcupQuirof.Text = ""
    End If
End Sub

Private Sub pCargarIntervActoMedico()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset

    If strNumHQ <> "" Then 'Entrando por Codigo Hoja
        'Intervenciones del 'Acto M�dico'
        SQL = "SELECT PR01DESCORTA, PR04NUMACTPLAN"
        SQL = SQL & " FROM PR0400, PR0100"
        SQL = SQL & " WHERE PR0400.PR62CODHOJAQUIR = ?"
        SQL = SQL & " AND PR37CODESTADO <> " & constESTACT_CANCELADA
        SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        SQL = SQL & " ORDER BY PR01DESCORTA"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strNumHQ
        Set rs = qry.OpenResultset()
        Do While Not rs.EOF
            grdInterv.AddItem rs!PR01DESCORTA
            strNAPlan = strNAPlan & "," & rs!PR04NUMACTPLAN
            rs.MoveNext
        Loop
        rs.Close
        qry.Close
        strNAPlan = Mid$(strNAPlan, 2)
        
        'Dpto quir�rgico
        SQL = "SELECT DISTINCT PR0400.AD02CODDPTO, AD02DESDPTO"
        SQL = SQL & " FROM PR0400, AD0200"
        SQL = SQL & " WHERE PR0400.PR62CODHOJAQUIR = ?"
        SQL = SQL & " AND PR37CODESTADO <> " & constESTACT_CANCELADA
        SQL = SQL & " AND AD0200.AD02CODDPTO = PR0400.AD02CODDPTO"
        SQL = SQL & " ORDER BY AD02DESDPTO"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strNumHQ
        Set rs = qry.OpenResultset()
        Do While Not rs.EOF
            cboEQDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
            rs.MoveNext
        Loop
        rs.Close
        qry.Close
    Else 'Entrando por N� Act. Planificada
        'Intervenciones del 'Acto M�dico'
        SQL = "SELECT PR01DESCORTA, PR62CODHOJAQUIR"
        SQL = SQL & " FROM PR0400, PR0100"
        SQL = SQL & " WHERE PR04NUMACTPLAN IN (" & strNAPlan & ")"
        SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
        SQL = SQL & " ORDER BY PR62CODHOJAQUIR, PR01DESCORTA"
        Set rs = objApp.rdoConnect.OpenResultset(SQL)
        If Not rs.EOF Then
            If Not IsNull(rs!PR62CODHOJAQUIR) Then strNumHQ = rs!PR62CODHOJAQUIR
            Do While Not rs.EOF
                grdInterv.AddItem rs!PR01DESCORTA
                rs.MoveNext
            Loop
        End If
        rs.Close
        
        'Dpto quir�rgico
        SQL = "SELECT DISTINCT PR0400.AD02CODDPTO, AD02DESDPTO"
        SQL = SQL & " FROM PR0400, AD0200"
        SQL = SQL & " WHERE PR04NUMACTPLAN IN (" & strNAPlan & ")"
        SQL = SQL & " AND AD0200.AD02CODDPTO = PR0400.AD02CODDPTO"
        SQL = SQL & " ORDER BY AD02DESDPTO"
        Set rs = objApp.rdoConnect.OpenResultset(SQL)
        If Not rs.EOF Then
            Do While Not rs.EOF
                cboEQDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
                rs.MoveNext
            Loop
        End If
        rs.Close
    End If
End Sub

Private Sub pCargarUsuarioInactivo(strCodUser$, cboUser As SSDBCombo)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM)"
    SQL = SQL & " FROM SG0200"
    SQL = SQL & " WHERE SG02COD = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodUser
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then
        cboUser.Text = rs(0)
        cboUser.AddItem strCodUser & Chr$(9) & rs(0)
        cboUser.MoveLast
    End If
    rs.Close
    qry.Close
End Sub

