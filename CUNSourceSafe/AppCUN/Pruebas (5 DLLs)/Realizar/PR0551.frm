VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmOrdenQuirofano 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Quir�fano / Orden"
   ClientHeight    =   1125
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3660
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1125
   ScaleWidth      =   3660
   StartUpPosition =   2  'CenterScreen
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   0
      Top             =   300
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   2520
      TabIndex        =   3
      Top             =   600
      Width           =   975
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   2520
      TabIndex        =   1
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txtOrden 
      Height          =   315
      Left            =   1260
      TabIndex        =   0
      Top             =   660
      Width           =   1095
   End
   Begin VB.TextBox txtQuirofano 
      Height          =   315
      Left            =   1260
      Locked          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Quir�fano:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   5
      Top             =   180
      Width           =   915
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Orden:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   4
      Top             =   720
      Width           =   915
   End
End
Attribute VB_Name = "frmOrdenQuirofano"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
    Call objPipe.PipeSet("PR_QuirOrd", txtQuirofano.Text & "|" & txtOrden.Text)
    Unload Me
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim strQuirOrdIni$
    
    txtQuirofano.BackColor = objApp.objUserColor.lngReadOnly
    If objPipe.PipeExist("PR_QuirOrd") Then
        awk1.FS = "|"
        awk1 = objPipe.PipeGet("PR_QuirOrd")
        txtQuirofano.Text = awk1.F(1)
        txtOrden.Text = awk1.F(2)
    End If
End Sub

Private Sub txtOrden_GotFocus()
    txtOrden.SelStart = 0
    txtOrden.SelLength = Len(txtOrden.Text)
End Sub

Private Sub txtOrden_KeyPress(KeyAscii As Integer)
    'If KeyAscii = 13 Then SendKeys "{TAB}"
    If KeyAscii = 13 Then cmdAceptar_Click
End Sub

''Private Sub txtQuirofano_GotFocus()
''    txtQuirofano.SelStart = 0
''    txtQuirofano.SelLength = Len(txtQuirofano.Text)
''End Sub
''
''Private Sub txtQuirofano_KeyPress(KeyAscii As Integer)
''    If KeyAscii = 13 Then SendKeys "{TAB}"
''End Sub

