VERSION 5.00
Begin VB.Form frmmeterinstreal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTIÓN DE ACTUACIONES. Realización de Actuaciones. Instrucciones de Realización"
   ClientHeight    =   2955
   ClientLeft      =   1545
   ClientTop       =   2190
   ClientWidth     =   8445
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2955
   ScaleWidth      =   8445
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtInstrucciones 
      BackColor       =   &H00FFFFFF&
      Height          =   1890
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   3
      Tag             =   "Descripción|Descripción del Tipo de Muestra"
      Top             =   360
      Width           =   8400
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   6960
      TabIndex        =   2
      Top             =   2400
      Width           =   1455
   End
   Begin VB.CommandButton cmdGuardar 
      Caption         =   "Guardar"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   2400
      Width           =   1455
   End
   Begin VB.Label lblLabel1 
      Caption         =   "Instrucciones de Realización"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   2655
   End
End
Attribute VB_Name = "frmmeterinstreal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmdGuardar_Click()
Dim sql  As String
Dim qry  As rdoQuery
Dim rs   As rdoResultset
Dim qry1 As rdoQuery
Dim resp As Integer


sql = "UPDATE PR0400 SET PR04DESINSTREA = ? WHERE PR04NUMACTPLAN = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
If Trim(txtInstrucciones) = "" Then
    qry(0) = Null
    qry(1) = objPipe.PipeGet("PR04NUMACTPLAN")
    qry.Execute
    qry.Close
    sql = "UPDATE PR0300 SET PR03INDCITABLE = 0 WHERE PR03NUMACTPEDI = "
    sql = sql & " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = objPipe.PipeGet("PR04NUMACTPLAN")
    qry.Execute
    qry.Close
    Unload Me
Else
   qry(0) = txtInstrucciones.Text
   qry(1) = objPipe.PipeGet("PR04NUMACTPLAN")
   qry.Execute
   sql = "SELECT COUNT(*) FROM PR1300 WHERE  PR13INDPREFEREN = -1"
   sql = sql & " AND  PR13INDPLANIF= -1 AND PR01CODACTUACION = "
   sql = sql & "(SELECT PR01CODACTUACION FROM PR0400  WHERE PR04NUMACTPLAN = ?)"
   Set qry = objApp.rdoConnect.CreateQuery("", sql)
        qry(0) = objPipe.PipeGet("PR04NUMACTPLAN")
   Set rs = qry.OpenResultset()
   If rs(0) > 0 Then
        sql = "UPDATE PR0300 SET PR03INDCITABLE = -1 WHERE PR03NUMACTPEDI = "
        sql = sql & " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = objPipe.PipeGet("PR04NUMACTPLAN")
        qry.Execute
        qry.Close
   Else
        sql = "UPDATE PR0300 SET PR03INDCITABLE = 0 WHERE PR03NUMACTPEDI = "
        sql = sql & " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACPLAN = ?)"
        Set qry = objApp.rdoConnect.CreateQuery("", sql)
            qry(0) = objPipe.PipeGet("PR04NUMACTPLAN")
        qry.Execute
        qry.Close
    End If
    Unload Me
End If
End Sub

Private Sub cmdSalir_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "SELECT PR04DESINSTREA FROM PR0400 WHERE PR04NUMACTPLAN = ?"
Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = objPipe.PipeGet("PR04NUMACTPLAN")
Set rs = qry.OpenResultset
If Not IsNull(rs(0)) Then
    txtInstrucciones.Text = rs!PR04DESINSTREA
End If
rs.Close
qry.Close
End Sub
