VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmRecConsumidos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recursos consumidos"
   ClientHeight    =   7290
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11865
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7290
   ScaleWidth      =   11865
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraRec 
      Caption         =   "Recursos consumidos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Index           =   0
      Left            =   120
      TabIndex        =   4
      Top             =   2340
      Width           =   11685
      Begin SSDataWidgets_B.SSDBGrid ssgrdRec 
         Height          =   4290
         Index           =   0
         Left            =   180
         TabIndex        =   5
         Top             =   300
         Width           =   11445
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20188
         _ExtentY        =   7567
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraActuacion 
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1695
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   10860
      Begin VB.TextBox txtActuacion 
         DataField       =   "PR04OBSERV"
         Height          =   345
         Index           =   9
         Left            =   1200
         Locked          =   -1  'True
         MaxLength       =   256
         TabIndex        =   23
         TabStop         =   0   'False
         Tag             =   "Actuaci�n|Designaci�n de la actuaci�n"
         Top             =   1200
         Width           =   5950
      End
      Begin VB.TextBox txtActuacion 
         DataField       =   "PR04ASA"
         Height          =   345
         Index           =   8
         Left            =   10320
         Locked          =   -1  'True
         MaxLength       =   1
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   405
         Visible         =   0   'False
         Width           =   315
      End
      Begin VB.TextBox txtActuacion 
         Height          =   195
         Index           =   7
         Left            =   3480
         Locked          =   -1  'True
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox txtActuacion 
         DataField       =   "PR04CANTIDAD"
         Height          =   345
         Index           =   6
         Left            =   7380
         Locked          =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   1200
         Visible         =   0   'False
         Width           =   1635
      End
      Begin VB.TextBox txtActuacion 
         DataField       =   "AD02CODDPTO"
         Height          =   195
         Index           =   5
         Left            =   2400
         Locked          =   -1  'True
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox txtActuacion 
         DataField       =   "PR01CODACTUACION"
         Height          =   195
         Index           =   4
         Left            =   1380
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   180
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.TextBox txtActuacion 
         Height          =   345
         Index           =   3
         Left            =   2100
         Locked          =   -1  'True
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "Paciente|Nombre del paciente"
         Top             =   420
         Width           =   5055
      End
      Begin VB.TextBox txtActuacion 
         Height          =   345
         Index           =   2
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "N� Hist.|N� de Historia"
         Top             =   420
         Width           =   855
      End
      Begin VB.TextBox txtActuacion 
         DataField       =   "PR04NUMACTPLAN"
         Height          =   345
         Index           =   0
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "N� Act. Planif.|N� de Actuaci�n Planificaca"
         Top             =   840
         Width           =   855
      End
      Begin VB.TextBox txtActuacion 
         Height          =   345
         Index           =   1
         Left            =   2100
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "Actuaci�n|Designaci�n de la actuaci�n"
         Top             =   840
         Width           =   5055
      End
      Begin TabDlg.SSTab tabActuacion 
         Height          =   1290
         Index           =   0
         Left            =   -5000
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   540
         Width           =   2310
         _ExtentX        =   4075
         _ExtentY        =   2275
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0503.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).ControlCount=   0
         TabCaption(1)   =   "Tab 1"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "ssgrdActuacion(0)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid ssgrdActuacion 
            Height          =   315
            Index           =   0
            Left            =   -74940
            TabIndex        =   6
            Top             =   60
            Width           =   1875
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowHeight       =   423
            Columns(0).Width=   3200
            _ExtentX        =   3307
            _ExtentY        =   556
            _StockProps     =   79
            Caption         =   "SSDBGrid1"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
         DataField       =   "PR63CODTIPOANEST"
         Height          =   330
         Index           =   0
         Left            =   8880
         TabIndex        =   19
         Top             =   360
         Width           =   855
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         DefColWidth     =   2
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "COD. DPTO"
         Columns(0).Name =   "COD. DPTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5927
         Columns(1).Caption=   "DESCRIPCI�N"
         Columns(1).Name =   "DESCRIPCI�N"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   1508
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Incidencias"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   22
         Top             =   1320
         Width           =   1035
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Asa"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   9960
         TabIndex        =   20
         Top             =   480
         Width           =   330
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Anestesia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   7320
         TabIndex        =   18
         Top             =   480
         Width           =   1545
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cantidad a facturar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   7320
         TabIndex        =   17
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   180
         TabIndex        =   12
         Top             =   900
         Width           =   915
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Paciente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   8
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   915
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   7005
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmRecConsumidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objDetailInfo As New clsCWForm
Dim objMultiInfo As New clsCWForm
Dim lngNumActPlan&
Dim intCodDpto%
Dim blnFacturada As Boolean
Dim blnConsultaUrg As Boolean

Private Sub Form_Activate()
  tlbToolbar1.Buttons(21).Visible = False
  tlbToolbar1.Buttons(22).Visible = False
  tlbToolbar1.Buttons(23).Visible = False
  tlbToolbar1.Buttons(24).Visible = False
  mnuRegistroOpcion(40).Visible = False
  mnuRegistroOpcion(50).Visible = False
  mnuRegistroOpcion(60).Visible = False
  mnuRegistroOpcion(70).Visible = False
End Sub

Private Sub Form_Load()
    Dim SQL$, rs As rdoResultset, qry As rdoQuery
    Dim i%

  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn

  Set objWinInfo = New clsCWWin

  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

  'se obtiene el n� de actuaci�n planificada
  If objPipe.PipeExist("PR_NActPlan") Then
      lngNumActPlan = objPipe.PipeGet("PR_NActPlan")
      Call objPipe.PipeRemove("PR_NActPlan")
  End If
  If objPipe.PipeExist("AD_CodDpto") Then
    intCodDpto = objPipe.PipeGet("AD_CodDpto")
    Call objPipe.PipeRemove("AD_CodDpto")
  End If
  
  SQL = "SELECT PR0400.AD02CODDPTO, PR0100.PR12CODACTIVIDAD"
  SQL = SQL & " FROM PR0100, PR0400"
  SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
  SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
  Set qry = objApp.rdoConnect.CreateQuery("", SQL)
  qry(0) = lngNumActPlan
  Set rs = qry.OpenResultset()
  If rs!AD02CODDPTO = constDPTO_URGENCIAS And rs!PR12CODACTIVIDAD = constACTIV_CONSULTA Then
      blnConsultaUrg = True
  End If
  rs.Close
  qry.Close
  
  With objDetailInfo
    .strName = "Actuacion"
    Set .objFormContainer = fraActuacion(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabActuacion(0)
    Set .grdGrid = ssgrdActuacion(0)
    .strTable = "PR0400"
    .strWhere = "PR04NUMACTPLAN = " & lngNumActPlan
    blnFacturada = fActFacturada(CStr(lngNumActPlan))
    If blnFacturada Then
        .intAllowance = cwAllowReadOnly
    Else
        .intAllowance = cwAllowModify
    End If
    .intCursorSize = 0
    .blnHasMaint = True
  End With

  With objMultiInfo
    .strName = "Recursos"
    Set .objFormContainer = fraRec(0)
    Set .objFatherContainer = fraActuacion(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = ssgrdRec(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .intCursorSize = 0
    .strTable = "PR1000"
    .blnHasMaint = True
    Call .FormAddOrderField("PR07NUMFASE", cwAscending)
    Call .FormAddRelation("PR04NUMACTPLAN", txtActuacion(0))
    If blnFacturada Then
        .intAllowance = cwAllowReadOnly
    Else
        If blnConsultaUrg Then
            .intAllowance = cwAllowModify
        Else
            .intAllowance = cwAllowAll
        End If
    End If
  End With
  
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)

    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .GridAddColumn(objMultiInfo, "N� Act.", "PR04NUMACTPLAN", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Fase", "PR07NUMFASE", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Tipo Recurso", "", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Recurso", "AG11CODRECURSO", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "PR10NUMUNIREC", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Tiempo (min.)", "PR10NUMMINOCUREC", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "�Dr. Resp.?", "PR10INDDOCTRESP", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "�Informe Firmado?", "PR10INDFIRMA", cwBoolean)
    Call .GridAddColumn(objMultiInfo, "Categor�a", "AD30CODCATEGORIA", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "Puesto", "AD31CODPUESTO", cwNumeric)
    Call .GridAddColumn(objMultiInfo, "N� Nec.", "PR10NUMNECESID", cwNumeric)
   
    Call .FormCreateInfo(objDetailInfo)
    
    .CtrlGetInfo(txtActuacion(0)).blnReadOnly = True
    .CtrlGetInfo(txtActuacion(1)).blnReadOnly = True
    .CtrlGetInfo(txtActuacion(2)).blnReadOnly = True
    .CtrlGetInfo(txtActuacion(3)).blnReadOnly = True
    .CtrlGetInfo(SSDBCombo1(0)).strSql = "SELECT PR63CODTIPOANEST,PR63DESTIPOANEST FROM PR6300"
    Call .FormChangeColor(objMultiInfo)
    ssgrdRec(0).Columns(3).Visible = False
    ssgrdRec(0).Columns(13).Visible = False
    .CtrlGetInfo(ssgrdRec(0).Columns(13)).blnValidate = False

    SQL = "SELECT CI22NUMHISTORIA, CI22PRIAPEL||' '||CI22SEGAPEL||', '||CI22NOMBRE PACIENTE,"
    SQL = SQL & " PR01DESCORTA, NVL(PR01INDFACTCANT,0) PR01INDFACTCANT"
    SQL = SQL & " FROM PR0100, PR0300, CI2200, PR0400"
    SQL = SQL & " WHERE CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0300.PR01CODACTUACION"
    SQL = SQL & " AND PR0400.PR04NUMACTPLAN = ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(txtActuacion(0)), "PR04NUMACTPLAN", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtActuacion(0)), txtActuacion(1), "PR01DESCORTA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtActuacion(0)), txtActuacion(2), "CI22NUMHISTORIA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtActuacion(0)), txtActuacion(3), "PACIENTE")
    Call .CtrlAddLinked(.CtrlGetInfo(txtActuacion(0)), txtActuacion(7), "PR01INDFACTCANT")

    'Se llenan los combos y DropDowns
    SQL = "SELECT PR05NUMFASE, PR05DESFASE"
    SQL = SQL & " FROM PR0500, PR0400"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = " & lngNumActPlan
    SQL = SQL & " AND PR0500.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " ORDER BY PR05NUMFASE"
    .CtrlGetInfo(ssgrdRec(0).Columns(4)).strSql = SQL
    
    SQL = "SELECT AG14DESTIPRECU"
    SQL = SQL & " FROM AG1400, PR0400, PR1400"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND PR1400.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR1400.PR06NUMFASE = ?"
    SQL = SQL & " AND PR1400.PR14NUMNECESID = ?"
    SQL = SQL & " AND AG1400.AG14CODTIPRECU = PR1400.AG14CODTIPRECU"
    Call .CtrlCreateLinked(.CtrlGetInfo(ssgrdRec(0).Columns(3)), "PR04NUMACTPLAN", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(ssgrdRec(0).Columns(3)), ssgrdRec(0).Columns(5), "AG14DESTIPRECU")
    
    If blnConsultaUrg Then
        'NOTA (18/8/2000): se incluyen tambi�n los recursos de QUIMIO y RADIOTERAPIA a
        'pesar de que est�n dados de baja (EFS)
        SQL = "SELECT MIN(AG1100.AG11CODRECURSO), NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM) DR,"
        SQL = SQL & " SG0200.SG02COD, SG02APE1, SG02APE2, SG02NOM"
        SQL = SQL & " FROM SG0200, AG1100"
        SQL = SQL & " WHERE SG0200.SG02INDGUAR = -1"
        SQL = SQL & " AND SYSDATE <= NVL(SG0200.SG02FECDES,TO_DATE('31/12/9999','DD/MM/YYYY'))"
        SQL = SQL & " AND AG1100.SG02COD = SG0200.SG02COD"
        SQL = SQL & " AND (SYSDATE <= NVL(AG1100.AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
        SQL = SQL & " OR AG1100.AD02CODDPTO = " & constDPTO_ONCOLOGIA
        SQL = SQL & " OR AG1100.AD02CODDPTO = " & constDPTO_RADIOTERAPIA & ")"
        SQL = SQL & " GROUP BY SG0200.SG02COD, NVL(SG02TXTFIRMA, SG02APE1||' '||SG02APE2||', '||SG02NOM),"
        SQL = SQL & " SG02APE1, SG02APE2, SG02NOM"
        SQL = SQL & " ORDER BY SG02APE1, SG02APE2, SG02NOM"
    Else
        SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO"
        SQL = SQL & " FROM AG1100, PR0400"
        SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = " & lngNumActPlan
        SQL = SQL & " AND AG1100.AD02CODDPTO = PR0400.AD02CODDPTO"
        SQL = SQL & " ORDER BY AG11DESRECURSO"
    End If
    .CtrlGetInfo(ssgrdRec(0).Columns(6)).strSql = SQL
    
    SQL = "SELECT AD30CODCATEGORIA, AD30DESCATEGORIA FROM AD3000"
    SQL = SQL & " WHERE SYSDATE BETWEEN AD30FECINICIO AND NVL(AD30FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    SQL = SQL & " ORDER BY AD30DESCATEGORIA"
    .CtrlGetInfo(ssgrdRec(0).Columns(11)).strSql = SQL
    
    SQL = "SELECT AD31CODPUESTO, AD31DESPUESTO FROM AD3100"
    SQL = SQL & " WHERE SYSDATE BETWEEN AD31FECINICIO AND NVL(AD31FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    SQL = SQL & " ORDER BY AD31DESPUESTO"
    
    .CtrlGetInfo(ssgrdRec(0).Columns(12)).strSql = SQL
    Call .WinRegister
    Call .WinStabilize
  End With
  
  txtActuacion(0).BackColor = objApp.objUserColor.lngReadOnly
  txtActuacion(1).BackColor = objApp.objUserColor.lngReadOnly
  txtActuacion(2).BackColor = objApp.objUserColor.lngReadOnly
  txtActuacion(3).BackColor = objApp.objUserColor.lngReadOnly
  
  ssgrdRec(0).Columns(4).Width = 2000
  ssgrdRec(0).Columns(5).Width = 2000
  ssgrdRec(0).Columns(6).Width = 3000
  ssgrdRec(0).Columns(7).Width = 800
  ssgrdRec(0).Columns(8).Width = 700
  ssgrdRec(0).Columns(9).Width = 1000
  ssgrdRec(0).Columns(10).Width = 1500
  
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraActuacion_Click(Index As Integer)
  Call objWinInfo.FormChangeActive(fraActuacion(Index), False, True)
End Sub

Private Sub Label1_Click(Index As Integer)
  Call objWinInfo.FormChangeActive(fraActuacion(0), False, True)
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
    If strFormName = objMultiInfo.strName And strCtrlName = "ssgrdRec(0).N� Act." Then
        If Not objMultiInfo.rdoCursor.EOF Then
            aValues(2) = objMultiInfo.rdoCursor!pr07numfase
            aValues(3) = objMultiInfo.rdoCursor!pr10numnecesid
        Else
            aValues(2) = 0
            aValues(3) = 0
        End If
    End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    Call pVerRecursos(lngNumActPlan)
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If strFormName = objDetailInfo.strName Then
    If txtActuacion(7).Text = "0" Then
      Label1(0).Visible = False
      txtActuacion(6).Visible = False
        If intCodDpto = constDPTO_ANESTESIA Then
            Label1(1).Visible = True
            Label1(2).Visible = True
            txtActuacion(8).Visible = True
            fraActuacion(0).Width = 10860
        Else
            Label1(1).Visible = False
            Label1(2).Visible = False
            txtActuacion(8).Visible = False
            fraActuacion(0).Width = 7320
        End If
    Else
      Label1(1).Visible = False
      Label1(2).Visible = False
      txtActuacion(8).Visible = False
      SSDBCombo1(0).Visible = False
      Label1(0).Visible = True
      txtActuacion(6).Visible = True
      fraActuacion(0).Width = 9200
    End If
  End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'cambio de responsable
    'urgencias
    'If blnConsultaUrg Then Call pResponsableUrg(lngNumActPlan)
    'oncolog�a
    If txtActuacion(4).Text = const_NUEVOONCOLOGIA Or txtActuacion(4).Text = const_REVONCOLOGIA Then
        Call pResponsableOnco(lngNumActPlan)
    End If
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    If ssgrdRec(0).Columns(13).Text = "" Then
        ssgrdRec(0).Columns(13).Text = fNextClave
        objMultiInfo.rdoCursor("PR10NUMNECESID") = ssgrdRec(0).Columns(13).Text
    End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean

  Select Case strFormName
    Case objDetailInfo.strName, objMultiInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing
  End Select
End Sub

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    If btnButton.Index = 30 Then
        If fblnSalir Then Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    If intIndex = 100 Then
         If fblnSalir Then Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

Private Sub fraRec_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraRec(0), False, True)
End Sub

Private Sub ssgrdRec_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub ssgrdRec_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub ssgrdRec_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub ssgrdRec_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Function fNextClave() As Long
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT MIN(PR10NUMNECESID)-1 FROM PR1000 WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            fNextClave = -1
        Else
            If rs(0) > -1 Then fNextClave = -1 Else fNextClave = rs(0)
        End If
    Else
        fNextClave = -1
    End If
    rs.Close
    qry.Close
End Function

Private Function fblnSalir() As Boolean
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    objWinInfo.DataRefresh
    
    If blnConsultaUrg Then
        
        
        'CAMBIAR LA SELECT DEL DROPDOWN DE URGENCIAS
        
        
        
        SQL = "SELECT AG11CODRECURSO"
        SQL = SQL & " FROM PR1000"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        SQL = SQL & " AND AG11CODRECURSO <> " & constURGENCIAS_REC_CONSCOLURG
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPlan
        Set rs = qry.OpenResultset()
        If rs.EOF Then
            MsgBox "Debe seleccionar un Doctor como recurso consumido.", vbExclamation, "Recursos consumidos"
            Exit Function
        End If
        
        Call pResponsableUrg(lngNumActPlan)
        
        'se anota de nuevo el recurso consumido como si fuera el Consultor-Colaborador
        SQL = "UPDATE PR1000 SET AG11CODRECURSO = ?,"
        SQL = SQL & " AD31CODPUESTO=?,"
        SQL = SQL & " AD30CODCATEGORIA=?"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = constURGENCIAS_REC_CONSCOLURG
        qry(1) = constPUESTO_CONSULTOR
        qry(2) = constCATEGORIA_MEDICO
        qry(3) = lngNumActPlan
        qry.Execute
        qry.Close
    End If
    
    If objMultiInfo.rdoCursor.RowCount = 0 Then
        MsgBox "Es necesario indicar al menos un recurso consumido.", vbExclamation, Me.Caption
        fblnSalir = False
    Else
        fblnSalir = True
    End If
End Function

Private Sub txtActuacion_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtActuacion_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtActuacion_KeyPress(Index As Integer, KeyAscii As Integer)
 If Index = 8 Then
    If KeyAscii < 49 Or KeyAscii > 53 Then
        txtActuacion(Index).Text = ""
        KeyAscii = 8
    End If
 End If
End Sub

Private Sub txtActuacion_LostFocus(Index As Integer)
 Call objWinInfo.CtrlLostFocus
End Sub

Private Sub pVerRecursos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmConsultaRecursos.Show vbModal
    Set frmConsultaRecursos = Nothing
End Sub
