VERSION 5.00
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form frmMensajeCorreo 
   Caption         =   "GESTI�N DE ACTUACIONES. Controlar Peticiones. Enviar Mensaje Correo"
   ClientHeight    =   7530
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11625
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   11625
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   840
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "Salir"
      Height          =   375
      Left            =   6360
      TabIndex        =   25
      Top             =   8040
      Width           =   1935
   End
   Begin VB.CommandButton cmdEnviarMensaje 
      Caption         =   "Enviar Correo"
      Height          =   375
      Left            =   3360
      TabIndex        =   12
      Top             =   8040
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Mensaje de Correo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   7080
      Index           =   0
      Left            =   480
      TabIndex        =   0
      Top             =   720
      Width           =   10335
      Begin VB.CommandButton cmdDestCorreo 
         Caption         =   "..."
         Height          =   330
         Left            =   4560
         TabIndex        =   26
         Top             =   720
         Width           =   330
      End
      Begin VB.TextBox txtText1 
         Height          =   2490
         Index           =   9
         Left            =   240
         TabIndex        =   11
         Tag             =   "Notas Remitente"
         ToolTipText     =   "Notas Remitente"
         Top             =   4320
         Width           =   9855
      End
      Begin VB.Frame fraFrame1 
         Caption         =   "Referencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2640
         Index           =   1
         Left            =   240
         TabIndex        =   15
         Top             =   1200
         Width           =   9855
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   8
            Left            =   360
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Descripci�n de la Actuaci�n"
            ToolTipText     =   "Descripci�n de la Actuaci�n"
            Top             =   2040
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22DNI"
            Height          =   330
            Index           =   4
            Left            =   3240
            TabIndex        =   5
            Tag             =   "D.N.I  del Paciente"
            ToolTipText     =   "D.N.I  del Paciente"
            Top             =   600
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22SEGAPEL"
            Height          =   330
            Index           =   7
            Left            =   6120
            TabIndex        =   8
            Tag             =   "Segundo Apellido del Paciente"
            ToolTipText     =   "Segundo Apellido del Paciente"
            Top             =   1320
            Width           =   2535
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22PRIAPEL"
            Height          =   330
            Index           =   6
            Left            =   3240
            TabIndex        =   7
            Tag             =   "Primer Apellido del Paciente"
            ToolTipText     =   "Primer Apellido del Paciente"
            Top             =   1320
            Width           =   2535
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NOMBRE"
            Height          =   330
            Index           =   5
            Left            =   360
            TabIndex        =   6
            Tag             =   "Nombre del Paciente"
            ToolTipText     =   "Nombre del Paciente"
            Top             =   1320
            Width           =   2535
         End
         Begin VB.TextBox txtText1 
            DataField       =   "CI21CODPERSONA"
            Height          =   330
            Index           =   2
            Left            =   360
            TabIndex        =   3
            Tag             =   "C�digo del Paciente"
            ToolTipText     =   "C�digo del Paciente"
            Top             =   600
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CI22NUMHISTORIA"
            Height          =   330
            Index           =   3
            Left            =   2040
            TabIndex        =   4
            Tag             =   "N�mero de Historia"
            ToolTipText     =   "N�mero de Historia"
            Top             =   600
            Width           =   900
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "PR09FECPETICION"
            Height          =   330
            Index           =   0
            Left            =   6120
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Fecha de Petici�n|Fecha de la Petici�n"
            ToolTipText     =   "Fecha de Petici�n|Fecha de la Petici�n"
            Top             =   2040
            Width           =   1800
            _Version        =   65537
            _ExtentX        =   3175
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Actuaci�n "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   360
            TabIndex        =   23
            Top             =   1800
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   6120
            TabIndex        =   22
            Top             =   1800
            Width           =   1290
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 2�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   6120
            TabIndex        =   21
            Top             =   1080
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Apellido 1�"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   17
            Left            =   3240
            TabIndex        =   20
            Top             =   1080
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   16
            Left            =   360
            TabIndex        =   19
            Top             =   1080
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "D.N.I"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   15
            Left            =   3240
            TabIndex        =   18
            Top             =   360
            Width           =   615
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   2040
            TabIndex        =   17
            Top             =   360
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�d.Persona"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   28
            Left            =   360
            TabIndex        =   16
            Top             =   360
            Width           =   1215
         End
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   1
         Left            =   5400
         TabIndex        =   2
         Tag             =   "Remitente"
         ToolTipText     =   "Remitente"
         Top             =   720
         Width           =   4575
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   1
         Tag             =   "Destinatario"
         ToolTipText     =   "Destinatario"
         Top             =   720
         Width           =   4215
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Notas del Remitente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   24
         Top             =   4080
         Width           =   2895
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Remitente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   5400
         TabIndex        =   14
         Top             =   480
         Width           =   870
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Destinatario"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   13
         Top             =   480
         Width           =   1035
      End
   End
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   120
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
End
Attribute VB_Name = "frmMensajeCorreo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim mblnunload As Boolean

Private Sub cmdDestCorreo_Click()
  On Error GoTo Err_Ejecutar
    MAPIMessages1.Show
    'MAPIMessages1.MsgIndex = 1
    txtText1(0).Text = MAPIMessages1.RecipDisplayName
   Exit Sub
Err_Ejecutar:
  MsgBox (Error)
End Sub

Private Sub cmdEnviarMensaje_Click()
  On Error GoTo Err_Ejecutar
    MAPIMessages1.AddressCaption = txtText1(1).Text
    MAPIMessages1.RecipDisplayName = txtText1(0).Text
    MAPIMessages1.MsgNoteText = txtText1(9).Text
    MAPIMessages1.Send (True)
    'MAPISession1.SignOn
    Exit Sub
Err_Ejecutar:
  Call MsgBox("No ha indicado el Destinatario del Correo", vbExclamation)
End Sub

Private Sub cmdsalir_Click()
  Unload Me
End Sub

Private Sub Form_Activate()
  If mblnunload = True Then
    Unload Me
  End If
End Sub

Private Sub Form_Load()
On Error GoTo Err_Ejecutar
    MAPISession1.SignOn
    MAPIMessages1.MsgIndex = -1
    MAPIMessages1.SessionID = MAPISession1.SessionID
    mblnunload = False
Exit Sub
Err_Ejecutar:
  mblnunload = True
End Sub

