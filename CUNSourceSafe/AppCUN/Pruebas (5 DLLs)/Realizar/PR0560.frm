VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "crystl32.ocx"
Begin VB.Form frmRealizacionIntervenciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Realizaci�n de Intervenciones"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdComun 
      Caption         =   "Petici�&n de Pruebas"
      Height          =   375
      Index           =   7
      Left            =   5100
      TabIndex        =   34
      Top             =   8220
      Width           =   1815
   End
   Begin VB.CommandButton cmdConsultar 
      Caption         =   "&Consultar"
      Height          =   375
      Left            =   3600
      TabIndex        =   31
      Top             =   540
      Width           =   1035
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "&Farmacia"
      Height          =   375
      Index           =   6
      Left            =   10080
      TabIndex        =   21
      Top             =   8220
      Width           =   1815
   End
   Begin Crystal.CrystalReport crtCrystalReport1 
      Left            =   11475
      Top             =   5520
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "Imprimir P&etici�n"
      Height          =   375
      Index           =   5
      Left            =   7590
      TabIndex        =   20
      Top             =   8220
      Width           =   1815
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "&Rec. Previstos"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   18
      Top             =   8220
      Width           =   1815
   End
   Begin VB.CommandButton cmdComun 
      Caption         =   "O&bserv. / Cuestionario"
      Height          =   375
      Index           =   1
      Left            =   2610
      TabIndex        =   17
      Top             =   8220
      Width           =   1815
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   10560
      TabIndex        =   9
      Top             =   540
      Width           =   1155
   End
   Begin VB.Frame Frame4 
      Caption         =   "Intervenciones Realizadas"
      ForeColor       =   &H00C00000&
      Height          =   2685
      Left            =   120
      TabIndex        =   3
      Top             =   5460
      Width           =   11715
      Begin VB.CommandButton cmdCambiar 
         Caption         =   "&Cambiar"
         Height          =   375
         Index           =   2
         Left            =   10440
         TabIndex        =   36
         Top             =   540
         Width           =   1155
      End
      Begin VB.CommandButton cmdUrpa 
         Caption         =   "Volver &URPA"
         Height          =   375
         Left            =   10425
         TabIndex        =   35
         Top             =   2220
         Width           =   1155
      End
      Begin VB.CommandButton cmdConsuRealizadas 
         Caption         =   "C&onsultar"
         Height          =   315
         Left            =   8400
         TabIndex        =   27
         Top             =   180
         Width           =   1035
      End
      Begin VB.CommandButton cmdHojaQuirofano 
         Caption         =   "Registro &Quir."
         Height          =   375
         Index           =   1
         Left            =   10440
         TabIndex        =   22
         Top             =   1800
         Width           =   1155
      End
      Begin VB.CommandButton cmdVolverFinRea 
         Caption         =   "Volver Rea&liz."
         Height          =   375
         Left            =   10440
         TabIndex        =   16
         Top             =   1380
         Width           =   1155
      End
      Begin VB.CommandButton cmdVolverFinPend 
         Caption         =   "Volver Pen&d."
         Height          =   375
         Left            =   10440
         TabIndex        =   15
         Top             =   960
         Width           =   1155
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdFinalizadas 
         Height          =   2085
         Left            =   120
         TabIndex        =   6
         Top             =   540
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   3678
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   285
         Index           =   0
         Left            =   3360
         TabIndex        =   25
         Tag             =   "Fecha Desde"
         ToolTipText     =   "Fecha Desde"
         Top             =   180
         Width           =   1695
         _Version        =   65537
         _ExtentX        =   2990
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
         Height          =   285
         Index           =   1
         Left            =   6480
         TabIndex        =   26
         Tag             =   "Fecha Hasta"
         ToolTipText     =   "Fecha Hasta"
         Top             =   180
         Width           =   1575
         _Version        =   65537
         _ExtentX        =   2778
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MinDate         =   "1997/1/1"
         MaxDate         =   "2050/12/31"
         Format          =   "DD/MM/YYYY"
         AllowNullDate   =   -1  'True
         BackColorSelected=   8388608
         BevelColorFace  =   12632256
         AutoSelect      =   0   'False
         ShowCentury     =   -1  'True
         Mask            =   2
         NullDateLabel   =   "__/__/____"
         StartofWeek     =   2
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   5910
         TabIndex        =   24
         Top             =   240
         Width           =   510
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   2730
         TabIndex        =   23
         Top             =   240
         Width           =   555
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Intervenciones Realiz�ndose"
      ForeColor       =   &H00C00000&
      Height          =   2310
      Left            =   120
      TabIndex        =   2
      Top             =   3120
      Width           =   11715
      Begin VB.CommandButton cmdCambiar 
         Caption         =   "&Cambiar"
         Height          =   375
         Index           =   1
         Left            =   10440
         TabIndex        =   33
         Top             =   600
         Width           =   1155
      End
      Begin VB.CommandButton cmdHojaQuirofano 
         Caption         =   "Re&gistro Quir."
         Height          =   375
         Index           =   0
         Left            =   10440
         TabIndex        =   32
         Top             =   1440
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnularRea 
         Caption         =   "An&ular"
         Height          =   375
         Left            =   10440
         TabIndex        =   14
         Top             =   1860
         Width           =   1155
      End
      Begin VB.CommandButton cmdVolverReaPend 
         Caption         =   "&Volver Pend."
         Height          =   375
         Left            =   10440
         TabIndex        =   13
         Top             =   1020
         Width           =   1155
      End
      Begin VB.CommandButton cmdTerminar 
         Caption         =   "&Terminar"
         Height          =   375
         Left            =   10440
         TabIndex        =   8
         Top             =   180
         Width           =   1155
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdRealizandose 
         Height          =   2010
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   3545
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Intervenciones Pendientes"
      ForeColor       =   &H00C00000&
      Height          =   2130
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   11715
      Begin VB.CommandButton cmdCambiar 
         Caption         =   "&Cambiar"
         Height          =   375
         Index           =   0
         Left            =   10440
         TabIndex        =   28
         Top             =   660
         Width           =   1155
      End
      Begin VB.CommandButton cmdConsSI 
         Caption         =   "C&onsent.: SI"
         Height          =   375
         Left            =   10440
         TabIndex        =   12
         Top             =   1500
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdConsNO 
         Caption         =   "Co&nsent.: NO"
         Height          =   375
         Left            =   10440
         TabIndex        =   11
         Top             =   1740
         Visible         =   0   'False
         Width           =   1155
      End
      Begin VB.CommandButton cmdAnularPend 
         Caption         =   "&Anular"
         Height          =   375
         Left            =   10440
         TabIndex        =   10
         Top             =   1080
         Width           =   1155
      End
      Begin VB.CommandButton cmdIniciar 
         Caption         =   "&Iniciar"
         Height          =   375
         Left            =   10440
         TabIndex        =   7
         Top             =   240
         Width           =   1155
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdPendientes 
         Height          =   1830
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   10245
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         AllowColumnMoving=   0
         AllowColumnSwapping=   0
         SelectTypeCol   =   0
         SelectTypeRow   =   3
         SelectByCell    =   -1  'True
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   18071
         _ExtentY        =   3228
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Recurso"
      ForeColor       =   &H00C00000&
      Height          =   915
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   3375
      Begin ComctlLib.ListView lvwRec 
         Height          =   675
         Left            =   120
         TabIndex        =   19
         Top             =   180
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   1191
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         HideColumnHeaders=   -1  'True
         _Version        =   327682
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin SSDataWidgets_B.SSDBCombo sscboDpto 
      Height          =   315
      Left            =   6660
      TabIndex        =   29
      Top             =   120
      Visible         =   0   'False
      Width           =   3135
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      AllowNull       =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   5530
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Dpto."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   5820
      TabIndex        =   30
      Top             =   120
      Visible         =   0   'False
      Width           =   795
   End
End
Attribute VB_Name = "frmRealizacionIntervenciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim intDptoSel%, strRecSel$
Dim strMeCaption$

Private Sub cmdAnularPend_Click()
    Call pAnularAct(ssgrdPendientes)
    ssgrdPendientes.SetFocus
End Sub

Private Sub cmdAnularRea_Click()
    Call pAnularAct(ssgrdRealizandose)
    ssgrdRealizandose.SetFocus
End Sub

Private Sub cmdCambiar_Click(Index As Integer)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim lngNumActPlan&, lngDpto&
    Dim strActSel$, i%
    Dim grdGrid As SSDBGrid
    
    Select Case Index
    Case 0: Set grdGrid = ssgrdPendientes
    Case 1: Set grdGrid = ssgrdRealizandose
    Case 2
        Set grdGrid = ssgrdFinalizadas
        'si alguna de las intervenciones est� terminada y facturada, no se deja volverla atr�s
        If fFacturada(ssgrdFinalizadas) Then Exit Sub
    End Select
    
    Select Case grdGrid.SelBookmarks.Count
    Case 0
        SQL = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox SQL, vbExclamation, strMeCaption
        Exit Sub
    Case Else
        lngNumActPlan = grdGrid.Columns("Num. Act.").CellText(grdGrid.SelBookmarks(0))
        For i = 0 To grdGrid.SelBookmarks.Count - 1
            strActSel = strActSel & grdGrid.Columns("Num. Act.").CellText(grdGrid.SelBookmarks(i)) & ","
        Next i
    End Select

    'se busca el Dpto. realizador de la intervenci�n
    SQL = "SELECT AD02CODDPTO"
    SQL = SQL & " FROM PR0400"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset()
    lngDpto = rs(0)
    rs.Close
    qry.Close
    
    Call objPipe.PipeSet("PR_DPTO", lngDpto)
    Call objPipe.PipeSet("PR_ACTIV", constACTIV_INTERVENCION)
    Call objPipe.PipeSet("PR_ACTPLAN", strActSel)
    frmCambioInterv.Show vbModal
    Set frmCambioInterv = Nothing
    DoEvents
    
    If objPipe.PipeExist("PR_Cambio") Then
        objPipe.PipeRemove ("PR_Cambio")
        Select Case Index
        Case 0: Call pCargarActPendientes(-1)
        Case 1: Call pCargarActRealizandose(-1)
        Case 2
            Call pCargarActFinalizadas(-1)
        End Select
    End If
End Sub

Private Sub cmdComun_Click(Index As Integer)
    Dim ssgrid As SSDBGrid, lngNumActPlan&, strHistoria$
    
    If ssgrdPendientes.SelBookmarks.Count > 0 Then
        Set ssgrid = ssgrdPendientes
    ElseIf ssgrdRealizandose.SelBookmarks.Count > 0 Then
        Set ssgrid = ssgrdRealizandose
    ElseIf ssgrdFinalizadas.SelBookmarks.Count > 0 Then
        Set ssgrid = ssgrdFinalizadas
    Else
        MsgBox "No se ha seleccionado ninguna Intervenci�n.", vbExclamation, strMeCaption
        Exit Sub
    End If
    Select Case Index
    Case 0, 1, 2, 3, 4, 7
      If ssgrid.SelBookmarks.Count = 1 Then
          lngNumActPlan = ssgrid.Columns("Num. Act.").CellText(ssgrid.SelBookmarks(0))
      Else
          MsgBox "Debe Ud. seleccionar una �nica Intervenci�n.", vbExclamation, strMeCaption
          Exit Sub
      End If
    Case 6
      'puede haber varias actuaciones seleccionadas del mismo paciente, pero lo que interesa _
      es el paciente, no la actuaci�n
      strHistoria = ssgrid.Columns("N� Hist.").CellText(ssgrid.SelBookmarks(0))
    End Select
    
    Select Case Index
    Case 0 'Ver recursos previstos
        Call pVerRecursos(lngNumActPlan)
    Case 1 'Ver datos / cuestionario
        Call pVerDatosActuacion(lngNumActPlan)
    Case 5 'Imprimir petici�n
        Call pImprimirPeticion(ssgrid)
    Case 6 'Farmacia
        Call pFarmacia(strHistoria)
    Case 7
        Dim arData(1 To 1) As Variant
        If fAsociarAsist Then
            arData(1) = CVar(lngNumActPlan)
            Call objsecurity.LaunchProcess("PRA104", arData())
        Else
            Dim SQL$, qry As rdoQuery, rs As rdoResultset
            Dim varCodProc As Variant
            SQL = "SELECT AD07CODPROCESO"
            SQL = SQL & " FROM PR0400"
            SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = lngNumActPlan
            Set rs = qry.OpenResultset()
            If Not IsNull(rs(0)) Then varCodProc = rs(0) Else varCodProc = 0
            rs.Close: qry.Close
            If varCodProc > 0 Then
                arData(1) = varCodProc
                Call objsecurity.LaunchProcess("PRA102", arData())
            Else
                MsgBox "No se ha encontrado el proceso.", vbExclamation, strMeCaption
                Exit Sub
            End If
        End If
    End Select
End Sub

Private Sub cmdConsNO_Click()
    Call pConsentimiento(False)
End Sub

Private Sub cmdConsSI_Click()
    Call pConsentimiento(True)
End Sub

Private Sub cmdConsultar_Click()
    Dim i%
    
    strRecSel = ""
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected Then
            strRecSel = strRecSel & lvwRec.ListItems(i).Tag & ","
            If i = 1 Then Exit For 'TODOS
        End If
    Next i
    If strRecSel <> "" Then
        strRecSel = Left$(strRecSel, Len(strRecSel) - 1)
        Call pCargarActPendientes(-1)
        Call pCargarActRealizandose(-1)
        Call pCargarActFinalizadas(-1)
    Else
        MsgBox "No se ha seleccionado ning�n recurso.", vbExclamation, strMeCaption
    End If
End Sub

Private Sub cmdConsuRealizadas_Click()
    Dim i%
    
    strRecSel = ""
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected Then
            strRecSel = strRecSel & lvwRec.ListItems(i).Tag & ","
            If i = 1 Then Exit For 'TODOS
        End If
    Next i
    If strRecSel <> "" Then
        strRecSel = Left$(strRecSel, Len(strRecSel) - 1)
        Call pCargarActFinalizadas(-1)
    Else
        MsgBox "No se ha seleccionado ning�n recurso.", vbExclamation, strMeCaption
    End If
End Sub

Private Sub cmdHojaQuirofano_Click(Index As Integer)
    Dim strNAPlan$, msg$, grdGrid As SSDBGrid, i%
    
    Select Case Index
    Case 0: Set grdGrid = ssgrdRealizandose
    Case 1: Set grdGrid = ssgrdFinalizadas
    End Select
    
    Select Case grdGrid.SelBookmarks.Count
    Case 0
        msg = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox msg, vbExclamation, strMeCaption
        Exit Sub
    Case Else
        Call pSeleccionarConjunto(grdGrid)
        For i = 0 To grdGrid.SelBookmarks.Count - 1
            strNAPlan = strNAPlan & "," & grdGrid.Columns("Num. Act.").CellText(grdGrid.SelBookmarks(i))
        Next i
        strNAPlan = Mid$(strNAPlan, 2)
        Call objPipe.PipeSet("PR_IntervActoMedico", strNAPlan)
        Call objPipe.PipeSet("PR_EstadoHojaQuir", Index)
        frmRegistroQuirofano.Show vbModal
        Set frmRegistroQuirofano = Nothing
    End Select
End Sub

Private Sub cmdIniciar_Click()
    Call pIniciar
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cmdTerminar_Click()
    Call pTerminar
End Sub

Private Sub cmdUrpa_Click()
Dim nPrPlan As Long, nAsist As Long, nProc As Long
Dim SQL As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  If ssgrdFinalizadas.SelBookmarks.Count = 0 Then
    MsgBox "Debe seleccionar alguna intervenci�n finalizada para volver al paciente a la URPA.", vbExclamation, Me.Caption
  Else
    nPrPlan = ssgrdFinalizadas.Columns("Num. Act.").CellText(ssgrdFinalizadas.SelBookmarks(0))
    SQL = "SELECT AD01CodAsistenci, AD07CodProceso FROM PR0400 WHERE PR04NumActPlan = ?"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", SQL)
    rdoQ(0) = nPrPlan
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo.EOF = False Then
      nAsist = rdo(0)
      nProc = rdo(1)
      Call VolverURPA(nAsist, nProc)
    End If
  End If
End Sub

Private Sub cmdVolverFinPend_Click()
    Call pVolverAPendientes(ssgrdFinalizadas)
End Sub

Private Sub cmdVolverFinRea_Click()
    Call pVolverARealizandose
End Sub

Private Sub cmdVolverReaPend_Click()
    Call pVolverAPendientes(ssgrdRealizandose)
End Sub

Private Sub dtcDateCombo1_Change(Index As Integer)
    If Index = 0 Then
        If CDate(dtcDateCombo1(1).Date) < CDate(dtcDateCombo1(0).Date) Then
            dtcDateCombo1(1).Date = dtcDateCombo1(0).Date
        End If
    ElseIf Index = 1 Then
        If CDate(dtcDateCombo1(1).Date) < CDate(dtcDateCombo1(0).Date) Then
            dtcDateCombo1(0).Date = dtcDateCombo1(1).Date
        End If
    End If
End Sub

Private Sub Form_Load()
    Dim intDpto As Integer
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    strMeCaption = "Realizaci�n de Intervenciones"

    If objPipe.PipeExist("PR_dpto") Then
        intDpto = objPipe.PipeGet("PR_dpto")
        Call objPipe.PipeRemove("PR_dpto")
    End If

    'se cargan los departamentos realizadores a los que tiene acceso el usuario
    SQL = "SELECT AD02CODDPTO, AD02DESDPTO"
    SQL = SQL & " FROM AD0200"
    SQL = SQL & " WHERE AD02CODDPTO IN ("
    SQL = SQL & " SELECT AD02CODDPTO FROM AD0300"
    SQL = SQL & " WHERE SG02COD = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AD03FECINICIO AND NVL(AD03FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY')))"
    SQL = SQL & " AND SYSDATE BETWEEN AD02FECINICIO AND NVL(AD02FECFIN,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    SQL = SQL & " AND AD02CODDPTO = " & constDPTO_QUIROFANO
    SQL = SQL & " ORDER BY AD02DESDPTO"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = objsecurity.strUser
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        intDptoSel = rs!AD02CODDPTO
        sscboDpto.Text = rs!AD02DESDPTO
        Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
        Do While Not rs.EOF
            If intDpto = rs!AD02CODDPTO Then
                intDptoSel = rs!AD02CODDPTO
                sscboDpto.Text = rs!AD02DESDPTO
            End If
            sscboDpto.AddItem rs!AD02CODDPTO & Chr$(9) & rs!AD02DESDPTO
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ColumnHeaders.Add , , , 2000
    Call pCargarRecursos(intDptoSel)
    
    'se formatean los grids
    Call pFormatearGrids
    
    'Inicializamos las fechas a la fecha actual
    dtcDateCombo1(0).Text = strFecha_Sistema
    dtcDateCombo1(1).Text = strFecha_Sistema
End Sub

Private Sub lvwRec_Click()
    Dim i%, msg$
    
    Call pVaciarGrids
 
    'Si se ha seleccionado TODOS, se deselecciona el resto de recursos
    If lvwRec.ListItems(1).Selected Then
        For i = 2 To lvwRec.ListItems.Count
            lvwRec.ListItems(i).Selected = False
        Next i
    End If
    'Se muestra en el Caption los recursos seleccionados
    Call pCaption
End Sub

Private Sub sscboDpto_Click()
    If intDptoSel <> sscboDpto.Columns(0).Value Then Call pVaciarGrids
    intDptoSel = sscboDpto.Columns(0).Value
    Me.Caption = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: TODOS"
    Call pCargarRecursos(intDptoSel)
End Sub

Private Sub pCargarRecursos(intDptoSel%)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim item As ListItem
    
    'se cargan los recursos del Dpto. seleccionado
    lvwRec.ListItems.Clear
    Set item = lvwRec.ListItems.Add(, , "TODOS")
    item.Tag = 0
    item.Selected = True

    SQL = "SELECT AG11CODRECURSO, AG11DESRECURSO"
    SQL = SQL & " FROM AG1100"
    SQL = SQL & " WHERE AD02CODDPTO = ?"
    SQL = SQL & " AND SYSDATE BETWEEN AG11FECINIVREC AND NVL(AG11FECFINVREC,TO_DATE('31/12/9999','DD/MM/YYYY'))"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = intDptoSel
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        Set item = lvwRec.ListItems.Add(, , rs!AG11DESRECURSO)
        item.Tag = rs!AG11CODRECURSO
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
End Sub

Private Function fFechaActual()
'Devuelve la fecha y hora actual obtenida del servidor
    Dim SQL$, rs As rdoResultset
    
    SQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY HH24:MI:SS') FROM DUAL"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    fFechaActual = rs(0)
    rs.Close
End Function

Private Sub pFormatearGrids()
    'grid de actuaciones pendientes
    With ssgrdPendientes
        .Columns(0).Caption = "Fecha Prog."
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Quir."
        .Columns(1).Width = 500
        .Columns(2).Caption = "Ord."
        .Columns(2).Width = 500
        .Columns(3).Caption = "Intervenci�n"
        .Columns(3).Width = 3300
        .Columns(4).Caption = "N� Hist."
        .Columns(4).Alignment = ssCaptionAlignmentRight
        .Columns(4).Width = 700
        .Columns(5).Caption = "Paciente"
        .Columns(5).Width = 3000
        .Columns(6).Caption = "Edad"
        .Columns(6).Width = 600
        .Columns(6).Alignment = ssCaptionAlignmentCenter
        .Columns(7).Caption = "Entrada cola"
        .Columns(7).Width = 1300
        .Columns(7).Visible = False
        .Columns(8).Caption = "Cons."
        .Columns(8).Width = 600
        .Columns(8).Visible = False
        .Columns(9).Caption = "Num. Act."
        .Columns(9).Alignment = ssCaptionAlignmentRight
        .Columns(9).Width = 900
        .Columns(9).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
        
    'grid de actuaciones realiz�ndose
    With ssgrdRealizandose
        .Columns(0).Caption = "Fecha Inicio"
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Quir."
        .Columns(1).Width = 500
        .Columns(2).Caption = "Ord."
        .Columns(2).Width = 500
        .Columns(3).Caption = "Intervenci�n"
        .Columns(3).Width = 3300
        .Columns(4).Caption = "N� Hist."
        .Columns(4).Alignment = ssCaptionAlignmentRight
        .Columns(4).Width = 700
        .Columns(5).Caption = "Paciente"
        .Columns(5).Width = 3000
        .Columns(6).Caption = "Edad"
        .Columns(6).Width = 600
        .Columns(6).Alignment = ssCaptionAlignmentCenter
        .Columns(7).Caption = "Entrada cola"
        .Columns(7).Width = 1300
        .Columns(7).Visible = False
        .Columns(8).Caption = "Cons."
        .Columns(8).Width = 600
        .Columns(8).Visible = False
        .Columns(9).Caption = "Num. Act."
        .Columns(9).Width = 900
        .Columns(9).Alignment = ssCaptionAlignmentRight
        .Columns(9).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
    
    'grid de actuaciones finalizadas
    With ssgrdFinalizadas
        .Columns(0).Caption = "Fecha Inicio"
        .Columns(0).Width = 1300
        .Columns(1).Caption = "Fecha Fin"
        .Columns(1).Width = 1300
        .Columns(2).Caption = "Quir."
        .Columns(2).Width = 500
        .Columns(3).Caption = "Ord."
        .Columns(3).Width = 500
        .Columns(4).Caption = "Intervenci�n"
        .Columns(4).Width = 2500
        .Columns(5).Caption = "N� Hist."
        .Columns(5).Alignment = ssCaptionAlignmentRight
        .Columns(5).Width = 700
        .Columns(6).Caption = "Paciente"
        .Columns(6).Width = 2500
        .Columns(7).Caption = "Edad"
        .Columns(7).Width = 600
        .Columns(7).Alignment = ssCaptionAlignmentCenter
        .Columns(8).Caption = "Entrada cola"
        .Columns(8).Width = 0 '1300
        .Columns(8).Visible = False
        .Columns(9).Caption = "Cons."
        .Columns(9).Width = 600
        .Columns(9).Visible = False
        .Columns(10).Caption = "Num. Act."
        .Columns(10).Alignment = ssCaptionAlignmentRight
        .Columns(10).Width = 900
        .Columns(10).Visible = False
        .BackColorEven = objApp.objUserColor.lngReadOnly
        .BackColorOdd = objApp.objUserColor.lngReadOnly
    End With
End Sub

Private Sub ssgrdFinalizadas_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdFinalizadas_Click()
    ssgrdPendientes.SelBookmarks.RemoveAll
    ssgrdRealizandose.SelBookmarks.RemoveAll
    
    If ssgrdFinalizadas.SelBookmarks.Count > 1 Then
        If ssgrdFinalizadas.Columns("N� Hist.").CellText(ssgrdFinalizadas.SelBookmarks(0)) <> ssgrdFinalizadas.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdFinalizadas.SelBookmarks.Remove ssgrdFinalizadas.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdFinalizadas_HeadClick(ByVal ColIndex As Integer)
    If ssgrdFinalizadas.Rows > 0 Then Call pCargarActFinalizadas(ColIndex)
End Sub

Private Sub ssgrdPendientes_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdPendientes_Click()
    ssgrdFinalizadas.SelBookmarks.RemoveAll
    ssgrdRealizandose.SelBookmarks.RemoveAll
    
    If ssgrdPendientes.SelBookmarks.Count > 1 Then
        If ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(0)) <> ssgrdPendientes.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdPendientes.SelBookmarks.Remove ssgrdPendientes.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdPendientes_HeadClick(ByVal ColIndex As Integer)
    If ssgrdPendientes.Rows > 0 Then Call pCargarActPendientes(ColIndex)
End Sub

Private Sub ssgrdRealizandose_BeforeDelete(Cancel As Integer, DispPromptMsg As Integer)
    DispPromptMsg = False
End Sub

Private Sub ssgrdRealizandose_Click()
    ssgrdFinalizadas.SelBookmarks.RemoveAll
    ssgrdPendientes.SelBookmarks.RemoveAll
    
    If ssgrdRealizandose.SelBookmarks.Count > 1 Then
        If ssgrdRealizandose.Columns("N� Hist.").CellText(ssgrdRealizandose.SelBookmarks(0)) <> ssgrdRealizandose.Columns("N� Hist.").Text Then
            MsgBox "La selecci�n m�ltiple s�lo es posible para actuaciones del mismo paciente.", vbExclamation, strMeCaption
            ssgrdRealizandose.SelBookmarks.Remove ssgrdRealizandose.SelBookmarks.Count - 1
            Exit Sub
        End If
    End If
End Sub

Private Sub ssgrdRealizandose_HeadClick(ByVal ColIndex As Integer)
    If ssgrdRealizandose.Rows > 0 Then Call pCargarActRealizandose(ColIndex)
End Sub

Private Sub pVerRecursos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmConsultaRecursos.Show vbModal
    Set frmConsultaRecursos = Nothing
End Sub

Private Sub pConsentimiento(blnValor As Boolean)
    Dim lngNumActPlan&
    Dim SQL$, qry As rdoQuery
    
    Select Case ssgrdPendientes.SelBookmarks.Count
    Case 0
        SQL = "No se ha seleccionado ninguna Intervenci�n."
    Case 1
        Select Case ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(0))
        Case "SI"
            If blnValor = True Then Exit Sub
        Case "NO"
            If blnValor = False Then Exit Sub
        Case ""
            SQL = "La Intervenci�n no necesita consentimiento del paciente."
        End Select
    Case Is > 1
        SQL = "Debe Ud. seleccionar una �nica Intervenci�n."
    End Select
    If SQL <> "" Then MsgBox SQL, vbExclamation, strMeCaption: Exit Sub
    
    'esto s�lo es v�lido si s�lo hay un registro seleccionado
    lngNumActPlan = ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(0))
    
    SQL = "UPDATE PR0300 SET PR03INDCONSFIRM = ?"
    SQL = SQL & " WHERE PR03NUMACTPEDI IN"
    SQL = SQL & " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    If blnValor = 0 Then qry(0) = 0 Else qry(0) = -1
    qry(1) = lngNumActPlan
    On Error Resume Next
    qry.Execute
    If Err = 0 Then
        'Esta asignaci�n s�lo es v�lido si el Click sobre el Grid supone la selcci�n del _
        registro
        If blnValor = 0 Then
            ssgrdPendientes.Columns("Cons.").Text = "NO"
        Else
            ssgrdPendientes.Columns("Cons.").Text = "SI"
        End If
        ssgrdPendientes.Update
    Else
        MsgBox Error
    End If
End Sub

Private Sub pIniciar()
'**************************************************************************************
'*  Inicia la realizaci�n de la actuaci�n seleccionada cambiando su estado a realiz�ndose
'*  Muestra la pantalla de observ., indic. y cuestionarios de la actuaci�n si las hubiera
'**************************************************************************************
    Dim SQL$, qry As rdoQuery, strAhora$
    Dim lngNumActPlan&, i%

    'se comprueba que se ha seleccionado alguna actuaci�n de las pendientes
    If ssgrdPendientes.SelBookmarks.Count = 0 Then
        SQL = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox SQL, vbExclamation, strMeCaption
        Exit Sub
    End If
    Call pSeleccionarConjunto(ssgrdPendientes)

''    'se comprueba si todas las actuaciones tienen el consentimiento firmado. Si alguna de _
''    ellas no lo tiene no se puede seguir adelante
''    For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
''        If ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(i)) = "NO" Then
''           SQL = "El paciente no ha firmado el consentimiento necesario." & Chr$(13)
''           MsgBox SQL, vbInformation, strMeCaption
''           Exit Sub
''        End If
''    Next i
    
    strAhora = fFechaActual
    For i = 0 To ssgrdPendientes.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i))
        
        'se cumplimenta el consentimiento ya que el paciente ha firmado un papel antes de _
        entrar en el quir�fano y es repetitivo tener que volver a hacerlo
        If ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(i)) = "NO" Then
            SQL = "UPDATE PR0300 SET PR03INDCONSFIRM = ?"
            SQL = SQL & " WHERE PR03NUMACTPEDI IN"
            SQL = SQL & " (SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?)"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = -1
            qry(1) = lngNumActPlan
            qry.Execute
        End If
        
        'se anota la fecha-hora de inicio en la BD
        SQL = "UPDATE PR0400 SET PR04FECINIACT = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),"
        SQL = SQL & " PR37CODESTADO = ?"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strAhora
        qry(1) = constESTACT_REALIZANDOSE
        qry(2) = lngNumActPlan
        qry.Execute
        
        'se a�ade la actuaci�n al grid de actuaciones realiz�ndose
        ssgrdRealizandose.AddItem Format(strAhora, "dd/mm/yy hh:mm") & Chr$(9) _
                    & ssgrdPendientes.Columns("Quir.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Ord.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Intervenci�n").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("N� Hist.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Paciente").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Edad").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Entrada cola").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Cons.").CellText(ssgrdPendientes.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdPendientes.Columns("Num. Act.").CellText(ssgrdPendientes.SelBookmarks(i)), 0
    
        'se muestran los datos de la actuaci�n y el cuestionario
'        Call pVerDatosActuacion(lngNumActPlan, True)
    Next i
    
    'se quitan las actuaciones del grid de las actuaciones pendientes
    ssgrdPendientes.DeleteSelected
End Sub

Private Sub pTerminar()
'**************************************************************************************
'*  Termina la realizaci�n de las intervenciones seleccionadas cambiando su estado a finalizada
'*  Muestra la Hoja de Quir�fano para terminar de cumplimentarla
'**************************************************************************************
    Dim SQL$, qry As rdoQuery, strAhora$
    Dim lngNumActPlan&, strNAPlan, i%

     'se comprueba que se ha seleccionado alguna actuaci�n de las pendientes
    If ssgrdRealizandose.SelBookmarks.Count = 0 Then
        SQL = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox SQL, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    Screen.MousePointer = vbHourglass
    
    strAhora = fFechaActual
        
    Call pSeleccionarConjunto(ssgrdRealizandose)
    For i = 0 To ssgrdRealizandose.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdRealizandose.Columns("Num. Act.").CellText(ssgrdRealizandose.SelBookmarks(i))
        strNAPlan = strNAPlan & "," & lngNumActPlan
    
        'se anota la fecha-hora de fin en la BD
        SQL = "UPDATE PR0400 SET PR04FECFINACT = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),"
        SQL = SQL & " PR37CODESTADO = ?"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = strAhora
        qry(1) = constESTACT_REALIZADA
        qry(2) = lngNumActPlan
        qry.Execute
        qry.Close
        
        'se a�ade la actuaci�n al grid de actuaciones realiz�ndose
        ssgrdFinalizadas.AddItem ssgrdRealizandose.Columns("Fecha Inicio").Text & Chr$(9) _
                    & Format(strAhora, "dd/mm/yy hh:mm") & Chr$(9) _
                    & ssgrdRealizandose.Columns("Quir.").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("Ord.").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("Intervenci�n").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("N� Hist.").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("Paciente").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("Edad").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("Entrada cola").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("Cons.").CellText(ssgrdRealizandose.SelBookmarks(i)) & Chr$(9) _
                    & ssgrdRealizandose.Columns("Num. Act.").CellText(ssgrdRealizandose.SelBookmarks(i)), 0
    Next i
    
    'se quita la actuaci�n del grid de las actuaciones pendientes
    ssgrdRealizandose.DeleteSelected
    
    strNAPlan = Mid$(strNAPlan, 2)
    Call objPipe.PipeSet("PR_IntervActoMedico", strNAPlan)
    Call objPipe.PipeSet("PR_EstadoHojaQuir", constESTADOHQ_FINALIZADA)
    Screen.MousePointer = vbDefault
    frmRegistroQuirofano.Show vbModal
    Set frmRegistroQuirofano = Nothing
End Sub

Private Sub pVolverAPendientes(ssgrid As SSDBGrid)
'**************************************************************************************
'*  Se deja la intervenci�n seleccionada (realiz�ndose o finalizada) en estado pendiente
'*  Si la intervenci�n estaba terminada, se cambia el estado de la Hoja de Quir�fano
'**************************************************************************************
    Dim lngNumActPlan&
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim intEstado%, strFechaProg$
    Dim i%
    Dim strHoy$
    
    Select Case ssgrid.SelBookmarks.Count
    Case 0
        SQL = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox SQL, vbExclamation, strMeCaption
        Exit Sub
    Case Else
        If ssgrid.Name = ssgrdFinalizadas.Name Then
            strHoy = DateAdd("d", -2, strFecha_Sistema)
            If CDate(ssgrdFinalizadas.Columns("Fecha Inicio").CellText(ssgrdFinalizadas.SelBookmarks(0))) < CDate(strHoy) Then
                SQL = "No se pueden volver a Pendientes intervenciones realizadas hace m�s de 2 d�as."
                MsgBox SQL, vbExclamation, strMeCaption
                Exit Sub
            End If
        End If
        SQL = "�Desea Ud. volver a Pendientes las intervenciones seleccionadas?"
        If MsgBox(SQL, vbQuestion + vbYesNo, strMeCaption) = vbNo Then Exit Sub
    End Select
        
    'se selecciona el conjunto de intervenciones del paciente
    Call pSeleccionarConjunto(ssgrid)
    
    If ssgrid.Name = ssgrdFinalizadas.Name Then
        'si alguna de las intervenciones est� terminada y facturada, no se deja volverla atr�s
        If fFacturada(ssgrdFinalizadas) Then Exit Sub
        
        'se cambia el estado de la Hoja de Quir�fano
        Call pEstadoHojaQuirofano(ssgrid.Columns("Num. Act.").CellText(ssgrid.SelBookmarks(0)))
    End If
    
    For i = 0 To ssgrid.SelBookmarks.Count - 1
        lngNumActPlan = ssgrid.Columns("Num. Act.").CellText(ssgrid.SelBookmarks(i))
        
        'se busca la fecha para la que se hab�a programado la actuaci�n dependiendo de si fue _
        citada o no
        SQL = "SELECT CI01FECCONCERT FROM CI0100 WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPlan
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If Not rs.EOF Then
            intEstado = 2 'citada
            strFechaProg = rs!CI01FECCONCERT
        Else
            SQL = "SELECT PR04FECPLANIFIC FROM PR0400 WHERE PR04NUMACTPLAN = ?"
            Set qry = objApp.rdoConnect.CreateQuery("", SQL)
            qry(0) = lngNumActPlan
            Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            intEstado = 1 'planificada
            If Not IsNull(rs(0)) Then strFechaProg = rs(0) Else strFechaProg = ""
        End If
        rs.Close
        qry.Close
            
        'se elimina la fecha-hora de inicio y de fin de la actuaci�n y se deja en su _
        estado inicial (planificada o citada)
        SQL = "UPDATE PR0400 SET PR04FECINIACT = ?, PR04FECFINACT = ?,"
        SQL = SQL & " PR37CODESTADO = ?"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = Null
        qry(1) = Null
        qry(2) = intEstado
        qry(3) = lngNumActPlan
        qry.Execute
        
        'se a�ade la actuaci�n al grid de actuaciones pendientes
        ssgrdPendientes.AddItem Format(strFechaProg, "dd/mm/yy hh:mm") & Chr$(9) _
                & ssgrid.Columns("Quir.").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("Ord.").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("Intervenci�n").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("N� Hist.").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("Paciente").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("Edad").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("Entrada Cola").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("Cons.").CellText(ssgrid.SelBookmarks(i)) & Chr$(9) _
                & ssgrid.Columns("Num. Act.").CellText(ssgrid.SelBookmarks(i)), 0
    Next i
    
    'se quitan las actuaciones del grid (realiz�ndose o pendientes)
    ssgrid.DeleteSelected
End Sub

Private Sub pVolverARealizandose()
'**************************************************************************************
'*  Se deja la intervenci�n seleccionada (finalizada) en estado realiz�ndose
'*  Se cambia el estado de la Hoja de Quir�fano
'**************************************************************************************
    Dim lngNumActPlan&
    Dim SQL$, qry As rdoQuery, i%
    Dim strHoy$
    
    Select Case ssgrdFinalizadas.SelBookmarks.Count
    Case 0
        SQL = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox SQL, vbExclamation, strMeCaption
        Exit Sub
    Case Else
        strHoy = DateAdd("d", -2, strFecha_Sistema)
        If CDate(ssgrdFinalizadas.Columns("Fecha Inicio").CellText(ssgrdFinalizadas.SelBookmarks(0))) < CDate(strHoy) Then
            SQL = "No se pueden volver a Realiz�ndose intervenciones realizadas hace m�s de 2 d�as."
            MsgBox SQL, vbExclamation, strMeCaption
            Exit Sub
        End If
        SQL = "�Desea Ud. volver a Realiz�ndose las intervenciones seleccionadas?"
        If MsgBox(SQL, vbQuestion + vbYesNo, strMeCaption) = vbNo Then Exit Sub
    End Select
    
    'se selecciona el conjunto de intervenciones del paciente
    Call pSeleccionarConjunto(ssgrdFinalizadas)
        
    'si alguna de las intervenciones est� terminada y facturada, no se deja volverla atr�s
    If fFacturada(ssgrdFinalizadas) Then Exit Sub
    
    'se cambia el estado de la Hoja de Quir�fano
    Call pEstadoHojaQuirofano(ssgrdFinalizadas.Columns("Num. Act.").CellText(ssgrdFinalizadas.SelBookmarks(0)))
    
    For i = 0 To ssgrdFinalizadas.SelBookmarks.Count - 1
        lngNumActPlan = ssgrdFinalizadas.Columns("Num. Act.").CellText(ssgrdFinalizadas.SelBookmarks(i))
    
        'se elimina la fecha-hora de inicio y de fin de la actuaci�n y se deja en su _
        estado inicial (planificada o citada)
        SQL = "UPDATE PR0400 SET PR04FECFINACT = ?,"
        SQL = SQL & " PR37CODESTADO = ?"
        SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = Null
        qry(1) = constESTACT_REALIZANDOSE 'realiz�ndose
        qry(2) = lngNumActPlan
        qry.Execute
        qry.Close
        
        'se a�ade la actuaci�n al grid de actuaciones realiz�ndose
        ssgrdRealizandose.AddItem ssgrdFinalizadas.Columns("Fecha Inicio").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Quir.").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Ord.").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Intervenci�n").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("N� Hist.").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Paciente").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Edad").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Entrada Cola").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Cons.").CellText(ssgrdFinalizadas.SelBookmarks(i)) & Chr$(9) _
            & ssgrdFinalizadas.Columns("Num. Act.").CellText(ssgrdFinalizadas.SelBookmarks(i)), 0
    Next i

    'se quitan las actuaciones del grid de actuaciones finalizadas
    ssgrdFinalizadas.DeleteSelected
End Sub

Private Sub pVerDatosActuacion(lngNumActPlan&, Optional blnMostrarSoloSiHayDatos As Boolean)
'**************************************************************************************
'*  Llama a la pantalla que muestra los datos (observ., indic., intruccines...) de la
'*  actuaci�n y los cuestionarios (cuestionarios + restricciones)
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = True significa que s�lo hay que mostrar la
'*  pantalla en el caso de que exista alguna observ., indic. o pregunta en el cuestionario.
'*  En este caso se hace previamente el Load de la pantalla, la cual devolver� en un Pipe
'*  la informaci�n necesaria para saber si hay que hacer el Show o el Unload
'*
'*  Si la opci�n blnMostrarSoloSiHayDatos = False se muestra la pantalla directamente
'**************************************************************************************

    'se establece el Pipe con el n� de actuaci�n planificada
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    
    'se mira la opci�n seleccionada: cargar pantalla previamente o mostrar directamente
    If blnMostrarSoloSiHayDatos Then 'cargar pantalla previamente
        Call objPipe.PipeSet("PR_VerSoloSiHayDatos", True)
        'se elimina el posible Pipe devuelto por frmObservAct en operaciones previas
        If objPipe.PipeExist("PR_frmObservAct") Then Call objPipe.PipeRemove("PR_frmObservAct")
        'se carga en memoria la pantalla
        Load frmObservAct
        'se mira si hay que mostrarla o descargarla
        If objPipe.PipeExist("PR_frmObservAct") Then
            If objPipe.PipeGet("PR_frmObservAct") = True Then
                frmObservAct.Show vbModal
            Else
                Unload frmObservAct
            End If
            Call objPipe.PipeRemove("PR_frmObservAct")
        Else
            Unload frmObservAct
        End If
        Call objPipe.PipeRemove("PR_VerSoloSiHayDatos")
    Else 'mostrar pantalla directamente
        frmObservAct.Show vbModal
        Set frmObservAct = Nothing
    End If
End Sub

Private Sub pAnularAct(ssgrd As SSDBGrid)
'**************************************************************************************
'*  Anula una actuaci�n planificada cambiando su estado a anulada
'**************************************************************************************
    Dim strCancel$, SQL$, qryPR04 As rdoQuery, qryCI01 As rdoQuery, i%, lngNumActPlan&
    
    Select Case ssgrd.SelBookmarks.Count
    Case 0
        SQL = "No se ha seleccionado ninguna Intervenci�n."
        MsgBox SQL, vbExclamation, strMeCaption
        Exit Sub
    Case 1
        SQL = "Si Ud. anula la intervenci�n ya no estar� disponible para ser realizada." & Chr$(13)
        SQL = SQL & "Indique el motivo por el que se va a anular la intervenci�n:"
    Case Is > 1
        SQL = "Si Ud. anula las intervenciones ya no estar�n disponibles para ser realizadas." & Chr$(13)
        SQL = SQL & "Indique el motivo por el que se van a anular las intervenciones:"
    End Select
    strCancel = Trim$(InputBox(SQL, "Anular Intervenci�n"))
    If strCancel = "" Then
        SQL = "La intervenci�n NO ha sido anulada: hace falta indicar un motivo."
        MsgBox SQL, vbExclamation, strMeCaption
        Exit Sub
    End If
    
    'Querys para la anulaci�n de la intervenci�n y la cita
    SQL = "UPDATE PR0400 SET PR04DESMOTCAN = ?,"
    SQL = SQL & " PR37CODESTADO = " & constESTACT_CANCELADA & ","
    SQL = SQL & " PR04FECCANCEL = SYSDATE,"
    SQL = SQL & " PR04FECENTRCOLA = NULL,"
    SQL = SQL & " PR04FECINIACT = NULL,"
    SQL = SQL & " PR04FECFINACT = NULL"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR04 = objApp.rdoConnect.CreateQuery("", SQL)
    
    SQL = "UPDATE CI0100 SET CI01SITCITA = '" & constESTCITA_ANULADA & "'"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA IN ('" & constESTCITA_CITADA & "','" & constESTCITA_PENDRECITAR & "')"
    Set qryCI01 = objApp.rdoConnect.CreateQuery("", SQL)

    For i = 0 To ssgrd.SelBookmarks.Count - 1
        lngNumActPlan = ssgrd.Columns("Num. Act.").CellText(ssgrd.SelBookmarks(i))
        
        'anular la intervenci�n
        If Len(strCancel) > 50 Then qryPR04(0) = Left$(strCancel, 50) Else qryPR04(0) = strCancel
        qryPR04(1) = lngNumActPlan
        qryPR04.Execute
        
        'anular la cita
        qryCI01(0) = lngNumActPlan
        qryCI01.Execute
        
        'anular las intervenciones y citas asociadas
        Call pAnularAsociadas(lngNumActPlan)
    Next i
    qryPR04.Close
    qryCI01.Close
    
    'se elimina la actuaci�n del Grid
    ssgrd.DeleteSelected
End Sub

Private Sub pVaciarGrids()
    ssgrdPendientes.RemoveAll
    ssgrdRealizandose.RemoveAll
    ssgrdFinalizadas.RemoveAll
End Sub



Private Sub pVerRecursosConsumidos(lngNumActPlan&)
    Call objPipe.PipeSet("PR_NActPlan", lngNumActPlan)
    frmRecConsumidos.Show vbModal
    Set frmRecConsumidos = Nothing
 End Sub

Private Sub pCaption()
Dim i%, msg$

'Se muestra en el Caption del formulario los recursos seleccionados
    For i = 1 To lvwRec.ListItems.Count
        If lvwRec.ListItems(i).Selected = True Then
            msg = msg & lvwRec.ListItems(i).Text & ", "
            If i = 1 Then Exit For
        End If
    Next i
    If msg <> "" Then
        msg = Left$(msg, Len(msg) - 2)
        msg = strMeCaption & ". Dpto: " & sscboDpto.Text & ". Recursos: " & msg
    Else
        msg = strMeCaption & ". Dpto: " & sscboDpto.Text
    End If
    Me.Caption = msg
End Sub

Private Sub pImprimirPeticion(ssgrid As SSDBGrid)
'Dim vntdata(1) As Variant, i As Integer, sql As String
'
'  For i = 0 To ssGrid.SelBookmarks.Count - 1
'    sql = sql & "PR0400.PR04NUMACTPLAN= " & ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(i)) & " OR "
'  Next i
'  sql = Left$(sql, Len(sql) - 4)
'  vntdata(1) = sql
'  Call objsecurity.LaunchProcess("PR2400", vntdata(1))
    



    Dim i%, SQL$

    If ssgrid.SelBookmarks.Count = 1 Then
      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
    Else
      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
    End If
    With crtCrystalReport1
        .PrinterCopies = 1
        .Destination = crptToPrinter
        For i = 0 To ssgrid.SelBookmarks.Count - 1
            SQL = SQL & "{PR0457J.PR04NUMACTPLAN}= " & ssgrid.Columns("Num. Act.").CellText(ssgrid.SelBookmarks(i)) & " OR "
        Next i
        SQL = Left$(SQL, Len(SQL) - 4)
        .SelectionFormula = "(" & SQL & ")"
        .Connect = objApp.rdoConnect.Connect
        .DiscardSavedData = True
        Screen.MousePointer = vbHourglass
        .Action = 1
        Screen.MousePointer = vbDefault
    End With
End Sub

Private Sub pFarmacia(strHistoria$)
'****************************************************************************************
'*  Accede a farmacia
'****************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim vntdata(1 To 1)
    
    'se busca el c�digo de persona necesario para pasarlo como par�metro al Launcher
    SQL = "SELECT CI21CODPERSONA" _
      & " FROM CI2200" _
      & " WHERE CI22NUMHISTORIA = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strHistoria
    Set rs = qry.OpenResultset()
    If Not rs.EOF Then vntdata(1) = rs(0)
    rs.Close
    qry.Close
    
    Call objsecurity.LaunchProcess("FR0171", vntdata)
End Sub

Private Sub pCargarActFinalizadas(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones ya realizadas hoy en funci�n del Dpto y rescurso o recursos
'*  seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$, strEdad$
    Static sqlOrder$
    
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PR04FECFINACT DESC, PACIENTE" 'Opci�n por defecto: Fecha Fin
    Else
        Select Case UCase(ssgrdFinalizadas.Columns(intCol).Caption)
        Case UCase("Fecha Fin"): sqlOrder = " ORDER BY PR04FECFINACT DESC, PACIENTE"
        Case UCase("Fecha Inicio"): sqlOrder = " ORDER BY PR04FECINIACT, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, PR04FECFINACT DESC"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, PR04FECFINACT DESC"
        Case UCase("Intervenci�n"): sqlOrder = " ORDER BY PR01DESCORTA, PR04FECFINACT DESC"
        Case UCase("Ord."), UCase("Quir."): sqlOrder = " ORDER BY TRUNC(PR04FECFINACT) DESC, LPAD(QUIROFANO,2,'0'), LPAD(ORDEN,2,'0'), PACIENTE"
        Case Else: Exit Sub
        End Select
    End If
    
    ssgrdFinalizadas.ScrollBars = ssScrollBarsNone
    ssgrdFinalizadas.RemoveAll
    
    'NOTA: la siguiente SELECT funciona mucho mejor sin Bind Variables para las fechas
    SQL = "SELECT /*+ ORDERED INDEX(CI0100 CI0103) */"
    SQL = SQL & " PR0400.PR04FECINIACT, PR0400.PR04FECFINACT,"
    SQL = SQL & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA, (SYSDATE-NVL(CI22FECNACIM,SYSDATE))/365 EDAD,"
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    SQL = SQL & " PR0400.PR04FECENTRCOLA,"
    SQL = SQL & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    SQL = SQL & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    SQL = SQL & " LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') QUIROFANO,"
    SQL = SQL & " PR41ORD.PR41RESPUESTA ORDEN"
    SQL = SQL & " FROM CI0100, PR0400, PR0100, PR0300, CI2200, AG1100, PR4100 PR41ORD"
    SQL = SQL & " WHERE CI0100.CI01SITCITA = '1'"
    If strRecSel = "0" Then
        SQL = SQL & " AND CI0100.AG11CODRECURSO IN ("
        SQL = SQL & " SELECT AG11CODRECURSO"
        SQL = SQL & " FROM AG1100"
        SQL = SQL & " WHERE AG14CODTIPRECU = " & constTIPORECQUIROFANO & ")"
    Else
        SQL = SQL & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    SQL = SQL & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    SQL = SQL & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZADA 'finalizadas
    SQL = SQL & " AND PR04FECFINACT >= TO_DATE ('" & Format(dtcDateCombo1(0).Date, "dd/mm/yyyy") & "','DD/MM/YYYY')"
    SQL = SQL & " AND PR04FECFINACT < TO_DATE ('" & Format(DateAdd("d", 1, dtcDateCombo1(1).Date), "dd/mm/yyyy") & "','DD/MM/YYYY')"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0300.PR03INDCITABLE = -1" 'citable
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    SQL = SQL & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
    SQL = SQL & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        Select Case Val(rs!EDAD)
        Case 0: strEdad = "?"
        Case Is < 1: strEdad = "<1"
        Case Else: strEdad = Val(rs!EDAD)
        End Select
        ssgrdFinalizadas.AddItem Format(rs!PR04FECINIACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & Format(rs!PR04FECFINACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!QUIROFANO & Chr$(9) _
            & rs!orden & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) _
            & rs!PACIENTE & Chr$(9) _
            & strEdad & Chr$(9) _
            & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & strConsent & Chr$(9) _
            & rs!PR04NUMACTPLAN
        rs.MoveNext
    Loop
    rs.Close
    qry.Close

    ssgrdFinalizadas.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActPendientes(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones pendientes hasta hoy inclusive en funci�n del Dpto y rescurso
'*  o recursos seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$, strEdad$
    Static sqlOrder$
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY FECHA, PACIENTE" 'Opci�n por defecto: Fecha Prog.
    Else
        Select Case UCase(ssgrdPendientes.Columns(intCol).Caption)
        Case UCase("Fecha Prog."): sqlOrder = " ORDER BY FECHA, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, FECHA"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, FECHA"
        Case UCase("Intervenci�n"): sqlOrder = " ORDER BY PR01DESCORTA, FECHA"
        Case UCase("Entrada cola"): sqlOrder = " ORDER BY PR04FECENTRCOLA, PACIENTE"
        Case UCase("Ord."), UCase("Quir."): sqlOrder = " ORDER BY TRUNC(FECHA), LPAD(QUIROFANO,2,'0'), LPAD(ORDEN,2,'0'), PACIENTE"
        Case Else: Exit Sub
        End Select
    End If
    
    ssgrdPendientes.ScrollBars = ssScrollBarsNone
    ssgrdPendientes.RemoveAll
    
    SQL = "SELECT /*+ ORDERED INDEX(CI0100 CI0103) */"
    SQL = SQL & " CI0100.CI01FECCONCERT FECHA,"
    SQL = SQL & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA, (SYSDATE-NVL(CI22FECNACIM,SYSDATE))/365 EDAD,"
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    SQL = SQL & " PR0400.PR04FECENTRCOLA,"
    SQL = SQL & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    SQL = SQL & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    SQL = SQL & " LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') QUIROFANO,"
    SQL = SQL & " PR41ORD.PR41RESPUESTA ORDEN"
    SQL = SQL & " FROM CI0100, PR0400, PR0100, PR0300, CI2200, AG1100, PR4100 PR41ORD"
    SQL = SQL & " WHERE CI0100.CI01SITCITA = '1'"
    If strRecSel = "0" Then
        SQL = SQL & " AND CI0100.AG11CODRECURSO IN ("
        SQL = SQL & " SELECT AG11CODRECURSO"
        SQL = SQL & " FROM AG1100"
        SQL = SQL & " WHERE AG14CODTIPRECU = " & constTIPORECQUIROFANO & ")"
    Else
        SQL = SQL & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    SQL = SQL & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    SQL = SQL & " AND PR0400.PR37CODESTADO = " & constESTACT_CITADA
    SQL = SQL & " AND PR0400.PR04FECENTRCOLA IS NOT NULL"
' Alberto: se buscan las intervenciones citadas entre el dia anterior y el mismo dia
    SQL = SQL & " AND CI0100.CI01FecConcert BETWEEN TRUNC(SYSDATE) - 2 AND SYSDATE + 1"
' ***********************************************************************************
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0300.PR03INDCITABLE = -1"
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    SQL = SQL & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
    SQL = SQL & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
        Select Case Val(rs!EDAD)
        Case 0: strEdad = "?"
        Case Is < 1: strEdad = "<1"
        Case Else: strEdad = Val(rs!EDAD)
        End Select
        ssgrdPendientes.AddItem Format(rs!fecha, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!QUIROFANO & Chr$(9) _
            & rs!orden & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) _
            & rs!PACIENTE & Chr$(9) _
            & strEdad & Chr$(9) _
            & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & strConsent & Chr$(9) _
            & rs!PR04NUMACTPLAN
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
        
    ssgrdPendientes.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActRealizandose(intCol%)
'**************************************************************************************
'*  Muestra las actuaciones que se est�n realizando hasta hoy inclusive en funci�n del
'*  Dpto y rescurso o recursos seleccionados ordenadas seg�n criterio del usuario (intCol)
'**************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim strConsent$, strEdad$
    Static sqlOrder$
    
    Screen.MousePointer = vbHourglass
    
    If intCol = -1 Then
        If sqlOrder = "" Then sqlOrder = " ORDER BY PR04FECINIACT, PACIENTE" 'Opci�n por defecto: Fecha Inicio
    Else
        Select Case UCase(ssgrdRealizandose.Columns(intCol).Caption)
        Case UCase("Fecha Inicio"): sqlOrder = " ORDER BY PR04FECINIACT, PACIENTE"
        Case UCase("Paciente"): sqlOrder = " ORDER BY PACIENTE, PR04FECINIACT"
        Case UCase("N� Hist."): sqlOrder = " ORDER BY CI22NUMHISTORIA, PR04FECINIACT DESC"
        Case UCase("Intervenci�n"): sqlOrder = " ORDER BY PR01DESCORTA, PR04FECINIACT"
        Case UCase("Entrada cola"): sqlOrder = " ORDER BY PR04FECENTRCOLA, PACIENTE"
        Case UCase("Ord."), UCase("Quir."): sqlOrder = " ORDER BY TRUNC(PR04FECINIACT), LPAD(QUIROFANO,2,'0'), LPAD(ORDEN,2,'0'), PACIENTE"
        Case Else: Exit Sub
        End Select
    End If

    ssgrdRealizandose.ScrollBars = ssScrollBarsNone
    ssgrdRealizandose.RemoveAll

'    sql = "SELECT /*+ ORDERED INDEX(CI0100 CI0103) */"
    SQL = "SELECT " ' No hace falta indicar �ndice
    SQL = SQL & " PR0400.PR04FECINIACT,"
    SQL = SQL & " PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE,"
    SQL = SQL & " PR0400.PR04FECENTRCOLA, (SYSDATE-NVL(CI22FECNACIM,SYSDATE))/365 EDAD,"
    SQL = SQL & " NVL(PR0100.PR01INDCONSFDO,0) PR01INDCONSFDO,"
    SQL = SQL & " NVL(PR0300.PR03INDCONSFIRM,0) PR03INDCONSFIRM,"
    SQL = SQL & " LTRIM(AG1100.AG11DESRECURSO,'Quir�fano ') QUIROFANO,"
    SQL = SQL & " PR41ORD.PR41RESPUESTA ORDEN"
    SQL = SQL & " FROM CI0100, PR0400, PR0100, PR0300, CI2200, AG1100, PR4100 PR41ORD"
    SQL = SQL & " WHERE CI0100.CI01SITCITA = '1'"
    If strRecSel = "0" Then
        SQL = SQL & " AND CI0100.AG11CODRECURSO IN ("
        SQL = SQL & " SELECT AG11CODRECURSO"
        SQL = SQL & " FROM AG1100"
        SQL = SQL & " WHERE AG14CODTIPRECU = " & constTIPORECQUIROFANO & ")"
    Else
        SQL = SQL & " AND CI0100.AG11CODRECURSO IN (" & strRecSel & ")"
    End If
    SQL = SQL & " AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN"
    SQL = SQL & " AND PR0400.PR37CODESTADO = " & constESTACT_REALIZANDOSE 'realiz�ndose
' Alberto: se buscan las intervenciones realiz�ndose entre el dia anterior y el mismo dia
    SQL = SQL & " AND PR0400.PR04FecIniAct BETWEEN TRUNC(SYSDATE) - 2 AND SYSDATE + 1"
' ***********************************************************************************
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " AND PR0100.PR12CODACTIVIDAD = " & constACTIV_INTERVENCION
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0300.PR03INDCITABLE = -1" 'citable
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND AG1100.AG11CODRECURSO = CI0100.AG11CODRECURSO"
    SQL = SQL & " AND PR41ORD.PR03NUMACTPEDI (+)= PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR41ORD.PR40CODPREGUNTA (+)= " & constPREG_ORDEN
    SQL = SQL & sqlOrder
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rs.EOF
        If rs!PR01INDCONSFDO = 0 Then
            strConsent = ""
        Else
            If rs!PR03INDCONSFIRM = 0 Then strConsent = "NO" Else strConsent = "SI"
        End If
            Select Case Val(rs!EDAD)
        Case 0: strEdad = "?"
        Case Is < 1: strEdad = "<1"
        Case Else: strEdad = Val(rs!EDAD)
        End Select
        ssgrdRealizandose.AddItem Format(rs!PR04FECINIACT, "dd/mm/yy hh:mm") & Chr$(9) _
            & rs!QUIROFANO & Chr$(9) _
            & rs!orden & Chr$(9) _
            & rs!PR01DESCORTA & Chr$(9) _
            & rs!CI22NUMHISTORIA & Chr$(9) _
            & rs!PACIENTE & Chr$(9) _
            & strEdad & Chr$(9) _
            & Format(rs!PR04FECENTRCOLA, "dd/mm/yy hh:mm") & Chr$(9) _
            & strConsent & Chr$(9) _
            & rs!PR04NUMACTPLAN
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdRealizandose.ScrollBars = ssScrollBarsAutomatic
    Screen.MousePointer = vbDefault
End Sub

Private Sub pSeleccionarConjunto(grdGrid As SSDBGrid)
    Dim strNH$, strFecIni$, strNAPlan$, i%, pos%
    
    strNH = grdGrid.Columns("N� Hist.").CellText(grdGrid.SelBookmarks(0))
    LockWindowUpdate Me.hWnd
    Select Case grdGrid.Name
    Case ssgrdPendientes.Name
        'grdGrid.SelBookmarks.RemoveAll
        For i = 1 To grdGrid.Rows
            If i = 1 Then grdGrid.MoveFirst Else grdGrid.MoveNext
            If grdGrid.Columns("N� Hist.").Text = strNH Then
                If pos = 0 Then pos = i
                grdGrid.SelBookmarks.Add (grdGrid.RowBookmark(grdGrid.Row))
            End If
        Next i
    Case Else
        strFecIni = grdGrid.Columns("Fecha Inicio").CellText(grdGrid.SelBookmarks(0))
        'grdGrid.SelBookmarks.RemoveAll
        For i = 1 To grdGrid.Rows
            If i = 1 Then grdGrid.MoveFirst Else grdGrid.MoveNext
            If grdGrid.Columns("N� Hist.").Text = strNH And grdGrid.Columns("Fecha Inicio").Text = strFecIni Then
                If pos = 0 Then pos = i
                grdGrid.SelBookmarks.Add (grdGrid.RowBookmark(grdGrid.Row))
            End If
        Next i
    End Select
    grdGrid.MoveFirst
    grdGrid.MoveRecords pos - 1
    LockWindowUpdate 0&
    DoEvents
End Sub

Private Sub pEstadoHojaQuirofano(lngNAPlan&)
    Dim SQL$, qry As rdoQuery
    
    SQL = "UPDATE PR6200"
    SQL = SQL & " SET PR62CODESTADO = " & constESTADOHQ_ENPROCESO
    SQL = SQL & " WHERE PR62CODHOJAQUIR = "
    SQL = SQL & " (SELECT PR62CODHOJAQUIR"
    SQL = SQL & " FROM PR0400"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?)"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNAPlan
    qry.Execute
    qry.Close
End Sub

Private Function fFacturada(grdGrid As SSDBGrid) As Boolean
    Dim strNAPlan$, i%, msg$
    
    For i = 0 To grdGrid.SelBookmarks.Count - 1
        strNAPlan = strNAPlan & "," & grdGrid.Columns("Num. Act.").CellText(grdGrid.SelBookmarks(i))
    Next i
    strNAPlan = Mid$(strNAPlan, 2)
    
    'se comprueba si alguna de las actuaciones est� ya facturada
    If fActFacturada(strNAPlan) Then
        msg = "Alguna de las intervenciones seleccionadas est� facturada."
        msg = msg & " No se puede realizar la operaci�n."
        msg = msg & Chr$(13) & Chr$(13)
        msg = msg & "Consulte con Administraci�n."
        MsgBox msg, vbExclamation, strMeCaption
        fFacturada = True
    End If
End Function

Sub VolverURPA(nAsist As Long, nProc As Long)
Dim SQL As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim res As Integer

  SQL = "SELECT AD15CodCama, AD02CodDpto, GCFN06(AD15CodCama), AD14CodEstCama " _
      & "FROM AD1500 WHERE " _
      & "AD01CodAsistenci = ? AND " _
      & "AD07CodProceso = ? " _
      & "ORDER BY AD14CodEstCama"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", SQL)
  rdoQ(0) = nAsist
  rdoQ(1) = nProc
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = False Then
    If rdo(3) = constCAMA_OCUPADA Then
      MsgBox "El paciente ya est� ocupando la cama " & rdo(2) & ". No puede volver a ser trasladao a la URPA", vbExclamation, Me.Caption
      Exit Sub
    End If
  End If
  
  ' Se busca en el hist�rico
  SQL = "SELECT AD1600.AD15CodCama, GCFN06(AD1600.AD15CodCama), " _
      & "TO_CHAR(AD1600.AD16FecCambio, 'DD/MM/YYYY HH24:MI:SS'), AD1500.AD14CodEstCama " _
      & "FROM AD1600, AD1500 WHERE " _
      & "AD1600.AD15CodCama = AD1500.AD15CodCama AND " _
      & "AD1600.AD01CodAsistenci = ? AND " _
      & "AD1600.AD07CodProceso = ? AND " _
      & "TRUNC(AD1600.AD16FecFin) = TRUNC(SYSDATE) AND " _
      & "AD1500.AD02CodDpto = ? AND " _
      & "AD1500.AD13CodTipoCama = ? " _
      & "ORDER BY AD1600.AD16FecCambio DESC"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", SQL)
  rdoQ(0) = nAsist
  rdoQ(1) = nProc
  rdoQ(2) = constDPTO_QUIROFANO
  rdoQ(3) = constCAMASALADESPERTAR
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  If rdo.EOF = True Then
    MsgBox "No se tiene registrado que el paciente haya ocupado una cama en la URPA hoy.", vbExclamation, Me.Caption
  Else
    If rdo(3) = constCAMA_OCUPADA Then
      MsgBox "La �ltima cama ocupada por el paciente es la " & rdo(1) & ", pero en este " _
          & "momento est� ocupada por otro paciente. Debe liberar la cama antes de " _
          & "reingresar al paciente.", vbExclamation, Me.Caption
      Exit Sub
    End If
    res = MsgBox("El paciente va a quedar ingresado en la cama " & rdo(1) & ". �Desea confirmar la operaci�n?", vbQuestion + vbYesNo, Me.Caption)
    If res = vbYes Then
      On Error Resume Next
      objApp.BeginTrans
      SQL = "UPDATE AD1500 SET AD14CodEstCama = ?, AD01CodAsistenci = ?, AD07CodProceso = ? " _
          & "WHERE AD15CodCama = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", SQL)
      rdoQ(0) = constCAMA_OCUPADA
      rdoQ(1) = nAsist
      rdoQ(2) = nProc
      rdoQ(3) = rdo(0)
      rdoQ.Execute
      If rdoQ.RowsAffected = 0 Then
        objApp.RollbackTrans
        MsgBox "Error al efectuar el reingreso del paciente. Error: " & Error, vbExclamation, Me.Caption
        Exit Sub
      End If
    
      SQL = "UPDATE AD1600 SET AD16FecFin = NULL WHERE " _
          & "AD15CodCama = ? AND " _
          & "AD16FecCambio = TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS')"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", SQL)
      rdoQ(0) = rdo(0)
      rdoQ(1) = rdo(2)
      rdoQ.Execute
      If rdoQ.RowsAffected = 0 Then
        objApp.RollbackTrans
        MsgBox "Error al efectuar el reingreso del paciente. Error: " & Error, vbExclamation, Me.Caption
        Exit Sub
      End If
      objApp.CommitTrans
    
    End If
  End If
End Sub
