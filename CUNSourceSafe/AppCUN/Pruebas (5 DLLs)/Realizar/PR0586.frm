VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmDatosBiopsia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datos de la biopsia"
   ClientHeight    =   5310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7935
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5310
   ScaleWidth      =   7935
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Datos para Anatomía Patológica"
      ForeColor       =   &H00800000&
      Height          =   3690
      Left            =   75
      TabIndex        =   4
      Top             =   1500
      Width           =   7665
      Begin VB.TextBox txtDiag 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1575
         TabIndex        =   18
         Top             =   1650
         Width           =   5940
      End
      Begin VB.TextBox txtMuestras 
         BackColor       =   &H00FFFFFF&
         Height          =   1290
         Left            =   1575
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   16
         Top             =   2025
         Width           =   5940
      End
      Begin VB.TextBox txtResumen 
         BackColor       =   &H00FFFFFF&
         Height          =   540
         Left            =   1575
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   13
         Top             =   1050
         Width           =   5940
      End
      Begin VB.CheckBox chkInvestigacion 
         Alignment       =   1  'Right Justify
         Caption         =   "Investigación"
         Height          =   240
         Left            =   6225
         TabIndex        =   10
         Top             =   3375
         Width           =   1290
      End
      Begin VB.TextBox txtObserPet 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1575
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   675
         Width           =   5940
      End
      Begin SSDataWidgets_B.SSDBCombo cboDr 
         Height          =   315
         Left            =   5325
         TabIndex        =   15
         Top             =   300
         Width           =   2160
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   3
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "Cod"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "cRecurso"
         Columns(1).Name =   "cRecurso"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   4974
         Columns(2).Caption=   "Desig"
         Columns(2).Name =   "Desig"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   3810
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 2"
      End
      Begin SSDataWidgets_B.SSDBCombo cboPrAP 
         Height          =   315
         Left            =   1575
         TabIndex        =   21
         Top             =   300
         Width           =   2760
         DataFieldList   =   "Column 0"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         DividerType     =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "Cod"
         Columns(0).Name =   "cPr"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5371
         Columns(1).Caption=   "Desig"
         Columns(1).Name =   "Pr"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   4868
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         DataFieldToDisplay=   "Column 1"
      End
      Begin VB.Label Label2 
         Caption         =   "Prueba:"
         Height          =   240
         Index           =   7
         Left            =   825
         TabIndex        =   20
         Top             =   375
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Diag. endoscópico:"
         Height          =   240
         Index           =   6
         Left            =   150
         TabIndex        =   19
         Top             =   1650
         Width           =   1440
      End
      Begin VB.Label Label2 
         Caption         =   "Muestras:"
         Height          =   240
         Index           =   4
         Left            =   750
         TabIndex        =   17
         Top             =   2025
         Width           =   840
      End
      Begin VB.Label Label2 
         Caption         =   "Dr. Realiza:"
         Height          =   240
         Index           =   5
         Left            =   4425
         TabIndex        =   14
         Top             =   375
         Width           =   915
      End
      Begin VB.Label Label2 
         Caption         =   "Resumen:"
         Height          =   240
         Index           =   1
         Left            =   750
         TabIndex        =   7
         Top             =   1050
         Width           =   840
      End
      Begin VB.Label Label2 
         Caption         =   "Observ. petición:"
         Height          =   240
         Index           =   0
         Left            =   225
         TabIndex        =   6
         Top             =   750
         Width           =   1290
      End
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   390
      Left            =   6675
      TabIndex        =   12
      Top             =   1050
      Width           =   1065
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   390
      Left            =   6675
      TabIndex        =   11
      Top             =   150
      Width           =   1065
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datos paciente"
      ForeColor       =   &H00800000&
      Height          =   1365
      Left            =   75
      TabIndex        =   0
      Top             =   75
      Width           =   6390
      Begin VB.TextBox txtPrueba 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   825
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   825
         Width           =   5340
      End
      Begin VB.TextBox txtNH 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   825
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   300
         Width           =   765
      End
      Begin VB.TextBox txtPac 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1600
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   300
         Width           =   4590
      End
      Begin VB.Label Label2 
         Caption         =   "Prueba:"
         Height          =   240
         Index           =   3
         Left            =   225
         TabIndex        =   9
         Top             =   900
         Width           =   615
      End
      Begin VB.Label Label2 
         Caption         =   "Paciente:"
         Height          =   240
         Index           =   2
         Left            =   75
         TabIndex        =   3
         Top             =   375
         Width           =   765
      End
   End
End
Attribute VB_Name = "frmDatosBiopsia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public seguir As Integer, cRec As Integer, cPrAP As Long

Private Sub cboDr_Click()
  cRec = Val(cboDr.Columns(1).Text)
End Sub

Private Sub cboPrAP_Click()
  cPrAP = Val(cboPrAP.Columns(0).Text)
End Sub

Private Sub cmdAceptar_Click()
  seguir = True
  Me.Visible = False
End Sub

Private Sub cmdCancelar_Click()
  seguir = False
  Me.Visible = False
End Sub

Private Sub Form_Load()
  Call CargarDrEndoscopias
  Call CargarPrAnatomia
End Sub

Sub CargarDrEndoscopias()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  sql = "SELECT SG0200.SG02Cod, AG1100.AG11CodRecurso, " _
      & "NVL(SG0200.SG02TxtFirma, SG0200.SG02Ape1||' '||SG0200.SG02Ape2||', '||SG0200.SG02Nom) " _
      & "FROM SG0200, AD0300, AG1100 WHERE " _
      & "SG0200.SG02Cod = AD0300.SG02Cod AND " _
      & "AD0300.AD02CodDpto = AG1100.AD02CodDpto AND " _
      & "SG0200.SG02Cod = AG1100.SG02Cod AND " _
      & "SG0200.AD30CodCategoria = " & constCODDOCTOR & " AND " _
      & "AD0300.AD31CodPuesto IN " & constCATDOCTOR & " AND " _
      & "AD0300.AD02CodDpto = ? AND " _
      & "(SG0200.SG02FecDes IS NULL OR SG0200.SG02FecDes > SYSDATE) AND " _
      & "SG0200.SG02FecAct < SYSDATE"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = constDPTO_ENDOSCOPIAS
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    cboDr.AddItem rdo(0) & Chr$(9) & rdo(1) & Chr$(9) & rdo(2)
    rdo.MoveNext
  Wend

End Sub

Sub CargarPrAnatomia()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
  
  sql = "SELECT PR0100.PR01CodActuacion, PR0100.PR01DesCorta " _
      & "FROM PR0100, PR0200 WHERE " _
      & "PR0100.PR01CodActuacion = PR0200.PR01CodActuacion AND " _
      & "PR0200.AD02CodDpto = ? AND " _
      & "PR0100.PR01FecFin IS NULL " _
      & "ORDER BY PR0100.PR01DesCorta"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = constDPTO_ANATOMIA
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  While rdo.EOF = False
    cboPrAP.AddItem rdo(0) & Chr$(9) & rdo(1)
    rdo.MoveNext
  Wend
  cboPrAP.Text = "Biopsia"
  cPrAP = constCPRBIOPSIA
End Sub


