VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "sscala32.ocx"
Begin VB.Form frmAvisos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CITAS. Recoger y Comunicar Avisos"
   ClientHeight    =   7485
   ClientLeft      =   345
   ClientTop       =   1350
   ClientWidth     =   11190
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7485
   ScaleWidth      =   11190
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   11190
      _ExtentX        =   19738
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Avisos"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6675
      Index           =   0
      Left            =   120
      TabIndex        =   14
      Top             =   425
      Width           =   10995
      Begin VB.TextBox txtIdenti 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   40102
         Index           =   0
         Left            =   480
         MaxLength       =   7
         TabIndex        =   12
         TabStop         =   0   'False
         Tag             =   "C�digo de Persona del Paciente"
         ToolTipText     =   "C�digo Persona"
         Top             =   600
         Width           =   1525
      End
      Begin VB.TextBox txtIdenti 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   40102
         Index           =   2
         Left            =   3840
         Locked          =   -1  'True
         MaxLength       =   12
         TabIndex        =   21
         TabStop         =   0   'False
         Tag             =   "D.N.I. del Paciente"
         ToolTipText     =   "D.N.I. Persona F�sica"
         Top             =   600
         Width           =   2325
      End
      Begin VB.TextBox txtIdenti 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   40102
         Index           =   3
         Left            =   480
         Locked          =   -1  'True
         MaxLength       =   25
         TabIndex        =   20
         TabStop         =   0   'False
         Tag             =   "Nombre del Paciente"
         ToolTipText     =   "Nombre y Apellidos del Paciente"
         Top             =   1320
         Width           =   3200
      End
      Begin VB.TextBox txtIdenti 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   40102
         Index           =   1
         Left            =   2160
         Locked          =   -1  'True
         MaxLength       =   7
         TabIndex        =   19
         TabStop         =   0   'False
         Tag             =   "Historia del paciente"
         ToolTipText     =   "Historia Cl�nica"
         Top             =   600
         Width           =   1525
      End
      Begin VB.TextBox txtIdenti 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   40102
         Index           =   4
         Left            =   3840
         Locked          =   -1  'True
         MaxLength       =   25
         TabIndex        =   18
         TabStop         =   0   'False
         Tag             =   "Primer Apellido del Paciente"
         ToolTipText     =   "Nombre y Apellidos del Paciente"
         Top             =   1320
         Width           =   3200
      End
      Begin VB.TextBox txtIdenti 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   40102
         Index           =   5
         Left            =   7200
         Locked          =   -1  'True
         MaxLength       =   25
         TabIndex        =   17
         TabStop         =   0   'False
         Tag             =   "Segundo Apellido del Paciente"
         ToolTipText     =   "Nombre y Apellidos del Paciente"
         Top             =   1320
         Width           =   3200
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   4770
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   1800
         Width           =   10530
         _ExtentX        =   18574
         _ExtentY        =   8414
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraFrame1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "fraFrame1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).ControlCount=   2
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.Frame fraFrame1 
            Caption         =   "Aviso"
            Height          =   2580
            Index           =   2
            Left            =   120
            TabIndex        =   33
            Top             =   360
            Width           =   10215
            Begin VB.TextBox txtText1 
               BackColor       =   &H00E0E0E0&
               DataField       =   "CI21CodPersona"
               Height          =   330
               HelpContextID   =   40102
               Index           =   6
               Left            =   2760
               MaxLength       =   7
               TabIndex        =   41
               TabStop         =   0   'False
               Tag             =   "C�digo de Persona del Paciente"
               ToolTipText     =   "C�digo Persona"
               Top             =   480
               Visible         =   0   'False
               Width           =   570
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI04USUGENMENS"
               Height          =   330
               HelpContextID   =   30104
               Index           =   1
               Left            =   3480
               TabIndex        =   4
               Tag             =   "Emisor"
               Top             =   1080
               Width           =   3200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI04DESMENSAJE"
               Height          =   795
               HelpContextID   =   30104
               Index           =   12
               Left            =   120
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   6
               Tag             =   "Texto"
               Top             =   1680
               Width           =   9900
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "ci04SecAviso"
               Height          =   330
               HelpContextID   =   40102
               Index           =   2
               Left            =   120
               MaxLength       =   7
               TabIndex        =   0
               Tag             =   "N�mero de Aviso"
               Top             =   480
               Width           =   1890
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI04FECENTMENS"
               Height          =   330
               Index           =   0
               Left            =   3480
               TabIndex        =   1
               TabStop         =   0   'False
               Tag             =   "Fecha de Emisi�n"
               Top             =   480
               Width           =   1905
               _Version        =   65537
               _ExtentX        =   3360
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI04TIPMENSAJE"
               Height          =   330
               Index           =   0
               Left            =   120
               TabIndex        =   3
               Tag             =   "Tipo Mensaje"
               Top             =   1080
               Width           =   3195
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               FieldDelimiter  =   "'"
               FieldSeparator  =   ","
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   5636
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5644
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "AD02CODDPTO"
               Height          =   330
               Index           =   1
               Left            =   6840
               TabIndex        =   5
               Tag             =   "Destinatario"
               Top             =   1080
               Width           =   3195
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ColumnHeaders   =   0   'False
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   5636
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5644
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI04FECCADMENS"
               Height          =   330
               Index           =   1
               Left            =   6840
               TabIndex        =   2
               Tag             =   "Fecha de Caducidad"
               Top             =   480
               Width           =   1905
               _Version        =   65537
               _ExtentX        =   3360
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Caducidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   6840
               TabIndex        =   40
               Top             =   240
               Width           =   1500
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Texto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   165
               TabIndex        =   39
               Top             =   1440
               Width           =   495
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Destinatario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   6840
               TabIndex        =   38
               Top             =   840
               Width           =   1035
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Emisor"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   3480
               TabIndex        =   37
               Top             =   840
               Width           =   570
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tipo Mensaje"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   120
               TabIndex        =   36
               Top             =   840
               Width           =   1155
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha Emisi�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   0
               Left            =   3480
               TabIndex        =   35
               Top             =   240
               Width           =   1245
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "N�mero Aviso"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   120
               TabIndex        =   34
               Top             =   240
               Width           =   1185
            End
         End
         Begin VB.Frame fraFrame1 
            Caption         =   "Comunicaci�n"
            Height          =   1575
            Index           =   1
            Left            =   120
            TabIndex        =   28
            Top             =   3000
            Width           =   10215
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI04USUCOMMENS"
               Height          =   330
               HelpContextID   =   30104
               Index           =   10
               Left            =   120
               TabIndex        =   8
               Tag             =   "Usuario Comunicaci�n"
               Top             =   1080
               Width           =   3200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "CI04PERRECMENS"
               Height          =   330
               HelpContextID   =   30104
               Index           =   0
               Left            =   3480
               TabIndex        =   9
               Tag             =   "Receptor del Mensaje"
               Top             =   1080
               Width           =   3200
            End
            Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
               DataField       =   "CI04FORCOMMENS"
               Height          =   330
               Index           =   2
               Left            =   6840
               TabIndex        =   10
               Tag             =   "Modo comunicaci�n"
               Top             =   1080
               Width           =   3195
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldDelimiter  =   "'"
               FieldSeparator  =   ","
               ForeColorEven   =   0
               BackColorOdd    =   16777215
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   3200
               Columns(0).Visible=   0   'False
               Columns(0).Caption=   "C�digo"
               Columns(0).Name =   "C�digo"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3387
               Columns(1).Caption=   "Descripci�n"
               Columns(1).Name =   "Descripci�n"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   5644
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               DataFieldToDisplay=   "Column 1"
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "CI04FECCOMMENS"
               Height          =   330
               Index           =   2
               Left            =   120
               TabIndex        =   7
               Tag             =   "Fecha de Caducidad"
               Top             =   480
               Width           =   1905
               _Version        =   65537
               _ExtentX        =   3360
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MinDate         =   "1900/1/1"
               MaxDate         =   "2050/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               NullDateLabel   =   "__/__/____"
               StartofWeek     =   2
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Usuario"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   32
               Top             =   840
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fecha comunicaci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   31
               Top             =   240
               Width           =   1770
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Modo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   6825
               TabIndex        =   30
               Top             =   840
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Receptor del Mensaje"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   3480
               TabIndex        =   29
               Top             =   840
               Width           =   1875
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4005
            Index           =   0
            Left            =   -74760
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   600
            Width           =   10065
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17754
            _ExtentY        =   7064
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Label lblIdenti 
         AutoSize        =   -1  'True
         Caption         =   "Apellido 1�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   3840
         TabIndex        =   27
         Top             =   1080
         Width           =   930
      End
      Begin VB.Label lblIdenti 
         AutoSize        =   -1  'True
         Caption         =   "Persona"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   480
         TabIndex        =   26
         Top             =   360
         Width           =   705
      End
      Begin VB.Label lblIdenti 
         AutoSize        =   -1  'True
         Caption         =   "Nombre "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   480
         TabIndex        =   25
         Top             =   1080
         Width           =   720
      End
      Begin VB.Label lblIdenti 
         AutoSize        =   -1  'True
         Caption         =   "D.N.I"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   3840
         TabIndex        =   24
         Top             =   360
         Width           =   465
      End
      Begin VB.Label lblIdenti 
         AutoSize        =   -1  'True
         Caption         =   "Historia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   2160
         TabIndex        =   23
         Top             =   360
         Width           =   660
      End
      Begin VB.Label lblIdenti 
         AutoSize        =   -1  'True
         Caption         =   "Apellido 2�"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   7200
         TabIndex        =   22
         Top             =   1080
         Width           =   930
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   7200
      Width           =   11190
      _ExtentX        =   19738
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAvisos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim lngPersonaAviso As Long
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Call objApp.SplashOn
  Set objWinInfo = New clsCWWin
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objWinInfo.objDoc
    .cwPRJ = "Citas"
    .cwMOD = "Mantenimiento de Avisos"
    .cwDAT = "12-08-97"
    .cwAUT = "I�aki Gabiola"
    
    .cwDES = "Esta ventana permite mantener la recogida y comunicaci�n de los mensajes que se producen en la CUN"
    
    .cwUPD = "12-08-97 - I�aki Gabiola - Creaci�n del m�dulo"
    
    .cwEVT = "En el evento de PreValidate se realizar�..."
  End With
  
  lngPersonaAviso = objPipe.PipeGet("CI21CODPERSONA")
  
  Call pCargarPersona(lngPersonaAviso)


  With objDetailInfo
 
    .strName = "Avisos"
    Set .objFormContainer = tabTab1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .blnAskPrimary = False
    .strTable = "CI0400"
    .strWhere = "CI21CODPERSONA = " & lngPersonaAviso & " and (ci04FecCadMens >= " & objGen.ValueToSQL(Date, cwDate) & " or ci04FecCadMens is null) "
    Call .FormAddOrderField("CI04SecAviso", cwAscending)
    strKey = .strDataBase & .strTable
  End With
    
  
  With objWinInfo
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo)
    
    .CtrlGetInfo(txtText1(2)).blnValidate = False
    
    .CtrlGetInfo(txtText1(6)).vntDefaultValue = lngPersonaAviso
    
    .CtrlGetInfo(dtcDateCombo1(0)).vntDefaultValue = CDate(objGen.GetDBDateTime)
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True
                
    .CtrlGetInfo(cboSSDBCombo1(1)).strSql = "SELECT AD02CODDPTO, AD02DESDPTO FROM " & objDetailInfo.strDataBase & "AD0200 ORDER BY AD02DESDPTO"
    cboSSDBCombo1(0).AddItem ("'1','Para el Paciente'")
    cboSSDBCombo1(0).AddItem ("'2','Para los Usuarios'")
    cboSSDBCombo1(2).AddItem ("'1','Hablado'")
    cboSSDBCombo1(2).AddItem ("'2','Escrito'")


    Call .WinRegister

    Call .WinStabilize
                            
    'If objWinInfo.intWinStatus <> cwModeSingleEdit Then
    '   objWinInfo.DataNew
    '   objWinInfo.WinPrepareScr
    'End If
  End With
  Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
  'Call ExitApp
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
  
  'txtText1(1) =????
  txtText1(6) = lngPersonaAviso
  
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)

If blnError Then
   objApp.rdoConnect.RollbackTrans
Else
   objApp.rdoConnect.CommitTrans
End If
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
   objApp.rdoConnect.BeginTrans
   If objWinInfo.intWinStatus <> cwModeSingleEdit Then
      Call objWinInfo.CtrlStabilize(txtText1(2), Nuevo_codigo())
   End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Call MsgBox("Se ha generado el evento de mantenimiento sobre el control " & strCtrl)
End Sub
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub pCargarPersona(lngP As Long)
Dim sql As String
Dim qry As rdoQuery
Dim rs As rdoResultset

  
  txtIdenti(0).Locked = True
  txtIdenti(1).Locked = True
  txtIdenti(2).Locked = True
  txtIdenti(3).Locked = True
  txtIdenti(4).Locked = True
  txtIdenti(5).Locked = True
  
  sql = "SELECT CI22NUMHISTORIA, CI22DNI, CI22NOMBRE, CI22PRIAPEL, CI22SEGAPEL "
  sql = sql & " FROM CI2200 WHERE CI21CODPERSONA = ?"
  Set qry = objApp.rdoConnect.CreateQuery("", sql)
    qry(0) = lngP
  Set rs = qry.OpenResultset()
  If Not rs.EOF Then
    txtIdenti(0) = lngP
    If Not IsNull(rs!CI22NUMHISTORIA) Then txtIdenti(1) = rs!CI22NUMHISTORIA
    If Not IsNull(rs!CI22DNI) Then txtIdenti(2) = rs!CI22DNI
    txtIdenti(3) = rs!CI22NOMBRE
    txtIdenti(4) = rs!CI22PRIAPEL
    If Not IsNull(rs!CI22SEGAPEL) Then txtIdenti(5) = rs!CI22SEGAPEL
  End If
    
  txtIdenti(0).BackColor = objApp.objUserColor.lngReadOnly
  txtIdenti(1).BackColor = objApp.objUserColor.lngReadOnly
  txtIdenti(2).BackColor = objApp.objUserColor.lngReadOnly
  txtIdenti(3).BackColor = objApp.objUserColor.lngReadOnly
  txtIdenti(4).BackColor = objApp.objUserColor.lngReadOnly
  txtIdenti(5).BackColor = objApp.objUserColor.lngReadOnly
End Sub
Private Function Nuevo_codigo() As String
Dim sql As String
Dim rs As rdoResultset
Dim qry As rdoQuery

sql = "Select max(CI04SecAviso) from CI0400"
Set rs = objApp.rdoConnect.OpenResultset(sql)
Nuevo_codigo = rs(0) + 1
    
End Function
