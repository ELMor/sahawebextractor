VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "ssdatb32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmCambioActuaciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cambio de Actuaciones"
   ClientHeight    =   6645
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6645
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VsOcxLib.VideoSoftAwk awk1 
      Left            =   10980
      Top             =   1560
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
      FS              =   "|"
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   10800
      TabIndex        =   1
      Top             =   600
      Width           =   1035
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   375
      Left            =   10800
      TabIndex        =   0
      Top             =   60
      Width           =   1035
   End
   Begin SSDataWidgets_B.SSDBGrid grdAct 
      Height          =   6030
      Left            =   5400
      TabIndex        =   2
      Top             =   540
      Width           =   5295
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   2
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8864
      Columns(1).Caption=   "Actuaci�n"
      Columns(1).Name =   "Actuaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9340
      _ExtentY        =   10636
      _StockProps     =   79
      Caption         =   "Actuaciones"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdActFin 
      Height          =   3210
      Left            =   60
      TabIndex        =   3
      Top             =   3360
      Width           =   5235
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   2
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8784
      Columns(1).Caption=   "Actuaci�n"
      Columns(1).Name =   "Actuaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9234
      _ExtentY        =   5662
      _StockProps     =   79
      Caption         =   "por las actuaciones..."
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBGrid grdActIni 
      Height          =   3210
      Left            =   60
      TabIndex        =   4
      Top             =   60
      Width           =   5235
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   2
      AllowUpdate     =   0   'False
      AllowColumnMoving=   0
      AllowColumnSwapping=   0
      SelectTypeCol   =   0
      RowNavigation   =   1
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      SplitterVisible =   -1  'True
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   8784
      Columns(1).Caption=   "Actuaci�n"
      Columns(1).Name =   "Actuaci�n"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   9234
      _ExtentY        =   5662
      _StockProps     =   79
      Caption         =   "Cambiar las actuaciones..."
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBCombo cboActiv 
      Height          =   315
      Left            =   6360
      TabIndex        =   5
      Top             =   120
      Width           =   3495
      DataFieldList   =   "Column 0"
      AllowInput      =   0   'False
      _Version        =   131078
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      DividerType     =   0
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   3200
      Columns(0).Visible=   0   'False
      Columns(0).Caption=   "Cod"
      Columns(0).Name =   "Cod"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   4974
      Columns(1).Caption=   "Desig"
      Columns(1).Name =   "Desig"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   6165
      _ExtentY        =   556
      _StockProps     =   93
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DataFieldToDisplay=   "Column 1"
   End
   Begin VB.Label lblActiv 
      Alignment       =   1  'Right Justify
      Caption         =   "Actividad:"
      Height          =   255
      Left            =   5400
      TabIndex        =   6
      Top             =   180
      Width           =   855
   End
End
Attribute VB_Name = "frmCambioActuaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngDpto&, lngActiv&, strActIni$, strCodActIni$, strNumActPediIni$, strNumActPlanIni$
Dim cllActSel As New Collection, cllActIni As New Collection
Dim blnCambio As Boolean

Private Sub cboActiv_Click()
    Call pCargarActuaciones(cboActiv.Columns(0).Text)
End Sub

Private Sub cmdAceptar_Click()
    Dim msg$
    
    If grdActFin.Rows = 0 Then
        msg = "No se ha se�alado ninguna actuaci�n nueva para realizar el cambio."
        MsgBox msg, vbExclamation, Me.Caption
        Exit Sub
    End If
    
    msg = "�Desea Ud. cambiar las actuaciones?"
    If MsgBox(msg, vbQuestion + vbYesNo, Me.Caption) = vbNo Then Exit Sub
    
    Call pCambiar
    
    If blnCambio Then
        Call objPipe.PipeSet("PR_Cambio", blnCambio)
        Unload Me
    End If
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    grdAct.SetFocus
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbHourglass
    
    Call pFormatearControles

    'departamento
    If objPipe.PipeExist("PR_DPTO") Then
        lngDpto = objPipe.PipeGet("PR_DPTO")
        Call objPipe.PipeRemove("PR_DPTO")
    End If
    'actividad
    If objPipe.PipeExist("PR_ACTIV") Then
        lngActiv = objPipe.PipeGet("PR_ACTIV")
        Call objPipe.PipeRemove("PR_ACTIV")
    End If
    'actuaciones a cambiar
    If objPipe.PipeExist("PR_ACTPLAN") Then
        strActIni = objPipe.PipeGet("PR_ACTPLAN")
        Call objPipe.PipeRemove("PR_ACTPLAN")
    End If
    
    If strActIni <> "" Then Call pCargarActuacionesIni
    Select Case lngActiv
    Case constACTIV_PRUEBA, constACTIV_TRATAMIENTO
        Call pCargarActiv
    Case Else
        lblActiv.Visible = False: cboActiv.Visible = False
        grdAct.Top = grdActIni.Top: grdAct.Height = 6510
    End Select
    If lngDpto > 0 Then Call pCargarActuaciones(lngActiv)
    
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActuaciones(lngActivSel&)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    Dim i%, SQL1$, cll1 As New Collection

    Screen.MousePointer = vbHourglass
    grdAct.RemoveAll
    
    's�lo se pueden cambiar actuaciones por otras que tengan los mismos tipos de _
    recursos preferentes
    SQL = "SELECT AG14CODTIPRECU"
    SQL = SQL & " FROM PR1300"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    SQL = SQL & " AND PR13INDPREFEREN = -1"
    SQL = SQL & " AND PR13INDPLANIF = -1"
    SQL = SQL & " ORDER BY AG14CODTIPRECU"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strCodActIni
    Set rs = qry.OpenResultset()
    If rs.EOF Then 'Actuaci�n sin recursos preferentes: se puede cambiar por cualquiera
        SQL1 = "SELECT PR01CODACTUACION FROM PR0200 WHERE AD02CODDPTO = ?"
        cll1.Add lngDpto 'par�metros
    Else 'Actuaci�n con recursos preferentes: se puede cambiar por actuaciones con los _
        mismos recursos preferentes
        SQL1 = "": i = 0
        Do While Not rs.EOF
            i = i + 1
            If i > 1 Then SQL1 = SQL1 & " INTERSECT "
            SQL1 = SQL1 & "SELECT PR01CODACTUACION"
            SQL1 = SQL1 & " FROM PR1300"
            SQL1 = SQL1 & " WHERE AD02CODDPTO = ?"
            SQL1 = SQL1 & " AND AG14CODTIPRECU = ?"
            cll1.Add lngDpto 'par�metros
            cll1.Add CStr(rs(0)) 'par�metros
            rs.MoveNext
        Loop
    End If
    rs.Close
    qry.Close

    SQL = "SELECT PR01CODACTUACION, PR01DESCORTA"
    SQL = SQL & " FROM PR0100"
    SQL = SQL & " WHERE PR12CODACTIVIDAD = ?"
    SQL = SQL & " AND PR01CODACTUACION IN (" & SQL1 & ")"
    SQL = SQL & " AND SYSDATE BETWEEN NVL(PR0100.PR01FECINICO, TO_DATE('1/1/1900','DD/MM/YYYY'))"
    SQL = SQL & " AND NVL(PR0100.PR01FECFIN, TO_DATE('31/12/9999','DD/MM/YYYY'))"
    SQL = SQL & " ORDER BY PR01DESCORTA"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngActivSel
    For i = 1 To cll1.Count: qry(i) = cll1.item(i): Next i
    Set rs = qry.OpenResultset()
    Do While Not rs.EOF
        grdAct.AddItem rs(0) & Chr$(9) & rs(1)
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCargarActuacionesIni()
    Dim SQL$, rs As rdoResultset
    
    If Right$(strActIni, 1) = "," Then
        strActIni = Left$(strActIni, Len(strActIni) - 1)
    End If
        
    SQL = "SELECT PR0400.PR01CODACTUACION, PR01DESCORTA, PR04NUMACTPLAN, PR03NUMACTPEDI"
    SQL = SQL & " FROM PR0400, PR0100"
    SQL = SQL & " WHERE PR04NUMACTPLAN IN (" & strActIni & ")"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    SQL = SQL & " ORDER BY PR04NUMACTPLAN"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    strCodActIni = rs(0) 'para tomarlo como referencia
    strNumActPlanIni = rs(2) 'para tomarlo como referencia
    strNumActPediIni = rs(3) 'para tomarlo como referencia
    Do While Not rs.EOF
        grdActIni.AddItem rs(0) & Chr$(9) & rs(1)
        cllActIni.Add CStr(rs(0))
        rs.MoveNext
    Loop
    rs.Close
End Sub

Private Sub grdAct_DblClick()
    Dim item As Variant
    Screen.MousePointer = vbHourglass
    'si la actuaci�n ya est� seleccionada no se vuelve a a�adir
    For Each item In cllActSel
        If item = grdAct.Columns(0).Text Then
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
    Next
    'se a�ade la actuaci�n seleccionada...
    cllActSel.Add grdAct.Columns(0).Text '... a la colecci�n
    grdActFin.AddItem grdAct.Columns(0).Text & Chr$(9) & grdAct.Columns(1).Text '... al grig
    Screen.MousePointer = vbDefault
End Sub

Private Sub grdActFin_DblClick()
    Dim i%
    If grdActFin.Rows = 0 Then Exit Sub
    'se quita la actuaci�n seleccionada...
    Screen.MousePointer = vbHourglass
    For i = 1 To cllActSel.Count
        If cllActSel.item(i) = grdActFin.Columns(0).Text Then
            cllActSel.Remove i '... de la colecci�n
            grdActFin.RemoveItem (grdActFin.Row) '... del grid
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
    Next i
    Screen.MousePointer = vbDefault
End Sub

Private Sub pCambiar()
'*****************************************************************************************
'*  Cambia un grupo de actuaciones (strActIni) por otro (cllActSel)
'*  Puede tratarse simplemente de un a�adido de actuaciones
'*****************************************************************************************
    Dim SQL$, i%
    Dim qryPR03 As rdoQuery, qryPR08 As rdoQuery, qryPR04 As rdoQuery
    Dim qryPR06 As rdoQuery, qryPR14 As rdoQuery, qryCI01 As rdoQuery
    Dim qryCI15 As rdoQuery, qryCI27 As rdoQuery, qryPR41 As rdoQuery
    Dim qryCI01b As rdoQuery, qryPR04b As rdoQuery
    Dim item As Variant
    Dim strNumActPedi$, strNumActPlan$, strCodAct$, strNumSolicit$, strFecCita$, strNumCita$
    Dim strNumCitaIni$
  
    
    Screen.MousePointer = vbHourglass
    blnCambio = False
    
    'A�adir la nueva actuaci�n pedida
    SQL = "INSERT INTO PR0300 (PR03NUMACTPEDI, PR01CODACTUACION, AD02CODDPTO, CI21CODPERSONA,"
    SQL = SQL & " CI32CODTIPECON, CI13CODENTIDAD, PR09NUMPETICION, PR03INDCONSFIRM,"
    SQL = SQL & " PR03INDCITABLE, PR48CODURGENCIA, PR03INDINTCIENTIF, AD02CODDPTO_CRG)"
    SQL = SQL & " SELECT ?, ?, AD02CODDPTO, CI21CODPERSONA,"
    SQL = SQL & " CI32CODTIPECON, CI13CODENTIDAD, PR09NUMPETICION, PR03INDCONSFIRM,"
    SQL = SQL & " PR03INDCITABLE, PR48CODURGENCIA, PR03INDINTCIENTIF, AD02CODDPTO_CRG"
    SQL = SQL & " FROM PR0300"
    SQL = SQL & " WHERE PR03NUMACTPEDI = ?"
    Set qryPR03 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'A�adir la nueva actuaci�n pedida a la petici�n
    SQL = "INSERT INTO PR0800 (PR03NUMACTPEDI, PR09NUMPETICION, PR08NUMSECUENCIA,"
    SQL = SQL & " AD07CODPROCESO, AD01CODASISTENCI, PR08DESINDICAC, PR08DESOBSERV,"
    SQL = SQL & " PR08DESMOTPET)"
    SQL = SQL & " SELECT ?, PR09NUMPETICION, PR08NUMSECUENCIA,"
    SQL = SQL & " AD07CODPROCESO, AD01CODASISTENCI, PR08DESINDICAC, PR08DESOBSERV,"
    SQL = SQL & " PR08DESMOTPET"
    SQL = SQL & " FROM PR0800"
    SQL = SQL & " WHERE PR03NUMACTPEDI = ?"
    Set qryPR08 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'A�adir la nueva actuaci�n planificada
    SQL = "INSERT INTO PR0400 (PR04NUMACTPLAN, PR03NUMACTPEDI, PR01CODACTUACION,"
    SQL = SQL & " AD01CODASISTENCI, AD07CODPROCESO, AD02CODDPTO, CI21CODPERSONA,"
    SQL = SQL & " CI32CODTIPECON, CI13CODENTIDAD, PR37CODESTADO, PR04FECPLANIFIC,"
    SQL = SQL & " PR04FECENTRCOLA, PR04FECINIACT, PR48CODURGENCIA, PR04INDINTCIENTIF, AD02CODDPTO_CRG,"
    SQL = SQL & " PR04INDREQDOC, PR62CODHOJAQUIR, PR63CODTIPOANEST, PR04ASA, PR04OBSERV)"
    SQL = SQL & " SELECT ?, ?, ?,"
    SQL = SQL & " AD01CODASISTENCI, AD07CODPROCESO, AD02CODDPTO, CI21CODPERSONA,"
    SQL = SQL & " CI32CODTIPECON, CI13CODENTIDAD, PR37CODESTADO, PR04FECPLANIFIC,"
    SQL = SQL & " PR04FECENTRCOLA, PR04FECINIACT, PR48CODURGENCIA, PR04INDINTCIENTIF, AD02CODDPTO_CRG,"
    SQL = SQL & " PR04INDREQDOC, PR62CODHOJAQUIR, PR63CODTIPOANEST, PR04ASA, PR04OBSERV"
    SQL = SQL & " FROM PR0400"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR04 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'A�adir las fases pedidas de la nueva actuaci�n
    SQL = "INSERT INTO PR0600 (PR03NUMACTPEDI, PR06NUMFASE, PR06DESFASE, PR06NUMMINOCUPAC,"
    SQL = SQL & " PR06NUMFASE_PRE, PR06NUMMINFPRE, PR06NUMMAXFPRE, PR06INDHABNATU,"
    SQL = SQL & " PR06INDINIFIN)"
    SQL = SQL & " SELECT ?, PR05NUMFASE, PR05DESFASE, PR05NUMOCUPACI,"
    SQL = SQL & " PR05NUMFASE_PRE, PR05NUMTMINFPRE, PR05NUMTMAXFPRE, PR05INDHABILNATU,"
    SQL = SQL & " PR05INDINICIOFIN"
    SQL = SQL & " FROM PR0500"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    Set qryPR06 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'A�adir los recursos pedidos de la nueva actuaci�n
    SQL = "INSERT INTO PR1400 (PR03NUMACTPEDI, PR06NUMFASE, PR14NUMNECESID, AG14CODTIPRECU,"
    SQL = SQL & " AD02CODDPTO, PR14NUMUNIREC, PR14NUMMINOCU, PR14NUMMINDESREC,"
    SQL = SQL & " PR14INDRECPREFE, PR14INDPLANIF)"
    SQL = SQL & " SELECT ?, PR05NUMFASE, PR13NUMNECESID, AG14CODTIPRECU,"
    SQL = SQL & " AD02CODDPTO, PR13NUMUNIREC, PR13NUMTIEMPREC, PR13NUMMINDESF,"
    SQL = SQL & " PR13INDPREFEREN, PR13INDPLANIF"
    SQL = SQL & " FROM PR1300"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    Set qryPR14 = objApp.rdoConnect.CreateQuery("", SQL)

    'A�adir la cita de la nueva actuaci�n
    SQL = "INSERT INTO CI0100 (PR04NUMACTPLAN, CI01NUMCITA, CI31NUMSOLICIT, AG05NUMINCIDEN,"
    SQL = SQL & " AG11CODRECURSO, CI01FECCONCERT, CI01SITCITA, CI01PERSCONFIR, CI01INDRECORDA,"
    SQL = SQL & " CI01FECEMIRECO, CI01TIPRECORDA, CI01NUMENVRECO, CI01FECCOMRECI,"
    SQL = SQL & " CI01INDLISESPE, CI01FECCONFIR, CI01INDASIG, AD15CODCAMA, AD13CODTIPOCAMA)"
    SQL = SQL & " SELECT ?, ?, CI31NUMSOLICIT, AG05NUMINCIDEN,"
    SQL = SQL & " AG11CODRECURSO, CI01FECCONCERT, CI01SITCITA, CI01PERSCONFIR, CI01INDRECORDA,"
    SQL = SQL & " CI01FECEMIRECO, CI01TIPRECORDA, CI01NUMENVRECO, CI01FECCOMRECI,"
    SQL = SQL & " CI01INDLISESPE, CI01FECCONFIR, CI01INDASIG, AD15CODCAMA, AD13CODTIPOCAMA"
    SQL = SQL & " FROM CI0100"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA = '" & constESTCITA_CITADA & "'"
    Set qryCI01 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'A�adir las fases citadas de la nueva actuaci�n
    SQL = "INSERT INTO CI1500 (CI31NUMSOLICIT, CI01NUMCITA, CI15FECCONCPAC, CI15NUMFASECITA,"
    SQL = SQL & " CI15DESFASECITA, CI15NUMDIASPAC, CI15NUMHORAPAC,"
    SQL = SQL & " CI15NUMMINUPAC)"
    SQL = SQL & " SELECT ?, ?, TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), PR05NUMFASE,"
    SQL = SQL & " PR05DESFASE, FLOOR(PR05NUMOCUPACI/1440), FLOOR(MOD(PR05NUMOCUPACI,1440)/60),"
    SQL = SQL & " MOD(MOD(PR05NUMOCUPACI,1440),60)"
    SQL = SQL & " FROM PR0500"
    SQL = SQL & " WHERE PR01CODACTUACION = ?"
    Set qryCI15 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'A�adir los recursos citados de la nueva actuaci�n
    SQL = "INSERT INTO CI2700 (CI01NUMCITA, CI31NUMSOLICIT, CI15NUMFASECITA, AG11CODRECURSO,"
    SQL = SQL & " CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC)"
    SQL = SQL & " SELECT ?, CI31NUMSOLICIT, CI15NUMFASECITA, AG11CODRECURSO,"
    SQL = SQL & " CI27FECOCUPREC, CI27NUMDIASREC, CI27NUMHORAREC, CI27NUMMINUREC"
    SQL = SQL & " FROM CI2700"
    SQL = SQL & " WHERE CI31NUMSOLICIT = ?"
    SQL = SQL & " AND CI01NUMCITA = ?"
    Set qryCI27 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'A�adir cuestionario
    SQL = "INSERT INTO PR4100 (PR03NUMACTPEDI, PR40CODPREGUNTA, PR27CODTIPRESPU, PR41INDROBLIG,"
    SQL = SQL & " PR41RESPUESTA, PR28NUMRESPUESTA, PR46CODLISTRESP, PR41INDAGRUPA)"
    SQL = SQL & " SELECT ?, PR40CODPREGUNTA, PR27CODTIPRESPU, PR41INDROBLIG,"
    SQL = SQL & " PR41RESPUESTA, PR28NUMRESPUESTA, PR46CODLISTRESP, PR41INDAGRUPA"
    SQL = SQL & " FROM PR4100"
    SQL = SQL & " WHERE PR03NUMACTPEDI = ?"
    Set qryPR41 = objApp.rdoConnect.CreateQuery("", SQL)
    
    'Anular la cita de la actuaci�n anterior
    SQL = "UPDATE CI0100"
    SQL = SQL & " SET CI01SITCITA = '" & constESTCITA_ANULADA & "'"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA IN ('" & constESTCITA_CITADA & "','" & constESTCITA_PENDRECITAR & "')"
    Set qryCI01b = objApp.rdoConnect.CreateQuery("", SQL)
    
    'Anular la actuaci�n anterior
    SQL = "UPDATE PR0400"
    SQL = SQL & " SET PR04DESMOTCAN = 'Cambio actuaci�n',"
    SQL = SQL & " PR37CODESTADO = " & constESTACT_CANCELADA & ","
    SQL = SQL & " PR04FECCANCEL = SYSDATE"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    Set qryPR04b = objApp.rdoConnect.CreateQuery("", SQL)
    
    objApp.BeginTrans
    On Error Resume Next
    
    For Each item In cllActSel
        If Not fIsItemInCll(item, cllActIni) Then 'si no est� entre las iniciales (a�adido de actuaciones)
            'datos
            strNumActPedi = fNextClave("PR03NUMACTPEDI")
            strNumActPlan = fNextClave("PR04NUMACTPLAN")
            strCodAct = item
            Call pCita(strNumActPlanIni, strNumSolicit, strFecCita, strNumCitaIni)
            
            'A�adir los registros de la nueva actuaci�n
            'actuaci�n pedida
            qryPR03(0) = strNumActPedi
            qryPR03(1) = strCodAct
            qryPR03(2) = strNumActPediIni
            qryPR03.Execute
            
            'petici�n
            qryPR08(0) = strNumActPedi
            qryPR08(1) = strNumActPediIni
            qryPR08.Execute
            
            'actuaci�n planificada
            qryPR04(0) = strNumActPlan
            qryPR04(1) = strNumActPedi
            qryPR04(2) = strCodAct
            qryPR04(3) = strNumActPlanIni
            qryPR04.Execute
            
            'fases pedidas
            qryPR06(0) = strNumActPedi
            qryPR06(1) = strCodAct
            qryPR06.Execute
            
            'recursos pedidos
            qryPR14(0) = strNumActPedi
            qryPR14(1) = strCodAct
            qryPR14.Execute
            
            If strNumSolicit <> "" Then 'Si la actuaci�n a cambiar era citable; la nueva tambi�n ser�
                'actuaci�n citada
                qryCI01(0) = strNumActPlan
                strNumCita = fNextNumCita(strNumSolicit)
                qryCI01(1) = strNumCita
                qryCI01(2) = strNumActPlanIni
                qryCI01.Execute
                
                'fases citadas
                qryCI15(0) = strNumSolicit
                qryCI15(1) = strNumCita
                qryCI15(2) = Format(strFecCita, "dd/mm/yyyy hh:mm:ss")
                qryCI15(3) = strCodAct
                qryCI15.Execute
                
                'recursos citados
                qryCI27(0) = strNumCita
                qryCI27(1) = strNumSolicit
                qryCI27(2) = strNumCitaIni
                qryCI27.Execute
            End If
            
            'cuestionario
            qryPR41(0) = strNumActPedi
            qryPR41(1) = strNumActPediIni
            qryPR41.Execute
            
            'se a�aden las actuaciones asociadas
            Call pInsertarActAsocidas(CLng(strNumActPlan))
        End If
    Next
    
    awk1 = strActIni
    awk1.FS = ","
    For i = 1 To awk1.NF
        If Not fIsItemInCll(cllActIni.item(i), cllActSel) Then 'si no est� entre las finales (a�adido de actuaciones)
            'Anular de la actuaci�n a cambiar...
            '...la cita
            qryCI01b(0) = awk1.F(i)
            qryCI01b.Execute
            
            '...la actuaci�n
            qryPR04b(0) = awk1.F(i)
            qryPR04b.Execute
            
            '...las actuaciones asociadas
            Call pAnularAsociadas(awk1.F(i))
        End If
    Next i
    
    If Err = 0 Then
        objApp.CommitTrans
        blnCambio = True
    Else
        objApp.RollbackTrans
        blnCambio = False
        SQL = "Se ha producido un error al realizar el cambio de actuaciones."
        SQL = SQL & Chr$(13)
        SQL = SQL & "Avise al Serv. de Inform�tica."
        SQL = SQL & Chr$(13) & Chr$(13)
        SQL = SQL & Error
        MsgBox SQL, vbExclamation, Me.Caption
    End If
    
    Screen.MousePointer = vbDefault
End Sub

Private Function pCita(strNumActPlan$, strNumSolicit$, strFecCita$, strNumCitaIni$)
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT DISTINCT CI31NUMSOLICIT, CI01FECCONCERT, CI01NUMCITA"
    SQL = SQL & " FROM CI0100"
    SQL = SQL & " WHERE PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI01SITCITA = '" & constESTCITA_CITADA & "'"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumActPlan
    Set rs = qry.OpenResultset(SQL)
    If Not rs.EOF Then 'la actuaci�n es citable
        strNumSolicit = rs(0)
        strFecCita = rs(1)
        strNumCitaIni = rs(2)
    End If
    rs.Close
    qry.Close
End Function

Private Function fNextNumCita(strNumSolicit$) As String
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT MAX(CI01NUMCITA)"
    SQL = SQL & " FROM CI0100"
    SQL = SQL & " WHERE CI31NUMSOLICIT = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = strNumSolicit
    Set rs = qry.OpenResultset(SQL)
    If Not IsNull(rs(0)) Then fNextNumCita = rs(0) + 1 Else fNextNumCita = 1
    rs.Close
    qry.Close
End Function

Private Function fIsItemInCll(itemB, cllB As Collection) As Boolean
    Dim item
    For Each item In cllB
        If item = itemB Then fIsItemInCll = True: Exit Function
    Next
End Function

Private Sub pFormatearControles()
    grdAct.BackColorEven = objApp.objUserColor.lngReadOnly
    grdAct.BackColorOdd = objApp.objUserColor.lngReadOnly
    grdActIni.BackColorEven = objApp.objUserColor.lngReadOnly
    grdActIni.BackColorOdd = objApp.objUserColor.lngReadOnly
    grdActFin.BackColorEven = objApp.objUserColor.lngMandatory
    grdActFin.BackColorOdd = objApp.objUserColor.lngMandatory
End Sub

Private Sub pCargarActiv()
    Dim SQL$, rs As rdoResultset

    SQL = "SELECT PR12CODACTIVIDAD, PR12DESACTIVIDAD"
    SQL = SQL & " FROM PR1200"
    SQL = SQL & " WHERE PR12CODACTIVIDAD IN (" & constACTIV_PRUEBA & "," & constACTIV_TRATAMIENTO & ")"
    SQL = SQL & " ORDER BY PR12DESACTIVIDAD"
    Set rs = objApp.rdoConnect.OpenResultset(SQL)
    Do While Not rs.EOF
        cboActiv.AddItem rs!PR12CODACTIVIDAD & Chr$(9) & rs!PR12DESACTIVIDAD
        If lngActiv > 0 Then
            If rs!PR12CODACTIVIDAD = lngActiv Then cboActiv.Text = rs!PR12DESACTIVIDAD
        End If
        rs.MoveNext
    Loop
    rs.Close
End Sub

