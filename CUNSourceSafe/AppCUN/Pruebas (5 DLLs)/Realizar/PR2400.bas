Attribute VB_Name = "modListadoPeticiones"
Public Function ImprimirPeticiones(NumAct As Variant)
Dim vsPet As vsPrinter
Dim str$, CodAct&, Observaciones$
Dim rd As rdoResultset
Dim Qy As rdoQuery

  Screen.MousePointer = 11
  str = "SELECT CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE," & _
        "CI2200.CI22NUMHISTORIA, SUBSTR(AD08NUMCASO,7,4) CASO,CI2200.CI22FECNACIM, " & _
        "AG1100.AG11DESRECURSO, PR0400.PR04FECENTRCOLA, CI0100.CI01FECCONCERT, " & _
        "GCFN06(AD1500.AD15CODCAMA) CAMA, " & _
        "PR0900.PR09FECPETICION, " & _
        "PR0900.PR09DESOBSERVAC ||'. '||AG1600.AG16DESTIPREST OBSERVACIONES, " & _
        "AD0200.AD02DESDPTO, PR0100.PR01DESCORTA,PR0800.PR08DESMOTPET,PR0400.PR04DESINSTREA, " & _
        "NVL(SG0200.SG02TXTFIRMA,SG0200.SG02APE1|| SG0200.SG02APE2||', '||SG0200.SG02NOM) DOCTOR, " & _
        " PR4000.PR40DESPREGUNTA, PR4100.PR41RESPUESTA, PR0400.PR04NUMACTPLAN, PR0800.PR08DESINDICAC "
  str = str & _
      "FROM PR0400,PR0100, AD0800, CI2200, PR0800, PR0900, " & _
      "AD0200, SG0200, CI0100, AG1100, AD1500, PR4100, PR4000, " & _
      "PR4700,AG1600 " & _
      "WHERE  PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION " & _
      "AND PR0400.AD01CODASISTENCI = AD0800.AD01CODASISTENCI " & _
      "AND PR0400.AD07CODPROCESO = AD0800.AD07CODPROCESO " & _
      "AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
      "AND PR0400.PR03NUMACTPEDI=PR0800.PR03NUMACTPEDI " & _
      "AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO " & _
      "AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI (+) " & _
      "AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO (+) " & _
      "AND PR0400.PR03NUMACTPEDI = PR4100.PR03NUMACTPEDI (+) " & _
      "AND PR4100.PR40CODPREGUNTA = PR4000.PR40CODPREGUNTA (+) " & _
      "AND PR0900.SG02COD = SG0200.SG02COD  " & _
      "AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO (+)  " & _
      "AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN (+) " & _
      "AND PR0800.PR09NUMPETICION = PR0900.PR09NUMPETICION " & _
      "AND PR0900.PR09NUMPETICION = PR4700.PR09NUMPETICION (+) " & _
      "AND PR4700.AG16CODTIPREST = AG1600.AG16CODTIPREST (+) " & _
      "AND CI0100.CI01SITCITA (+)='1' " & _
      "AND (" & NumAct & ") " & _
      "ORDER BY PR0400.PR04NUMACTPLAN"
  Set rd = objApp.rdoConnect.OpenResultset(str, rdOpenKeyset)
  Load frmPet
  With frmPet.vsPet
    .Action = 3 ' StartDoc
    .CurrentX = 8700
    .CurrentY = 200
    .FontSize = 8
    .Text = "Fecha: " & Format$(Now, "DD/MM/YY") '& "  Hora: " & Format$(Now, "HH:MM")
    
    .CurrentX = 200
    .CurrentY = 100
    .FontName = "Arial"
    .FontSize = 10
    .FontBold = True
    .Text = rd!PACIENTE
    
    .FontName = "Arial"
    .FontSize = 10
    .CurrentX = 200
    .CurrentY = 350
    .FontBold = True
    .Text = CStr(rd!CI22NUMHISTORIA) + " / " + CStr(rd!CASO)
    
    
    .CurrentX = 3500
    .CurrentY = 350
    .FontBold = True
    .Text = CStr("Fecha de Nacimiento: ")
    
    .CurrentX = 5600
    .CurrentY = 350
    .FontBold = False
    .Text = CStr(rd!CI22FECNACIM)
    
    If Not IsNull(rd!Cama) Then
      .CurrentX = 7200
      .CurrentY = 350
      .FontBold = True
      .Text = "Cama: "
      
      .CurrentX = 7900
      .CurrentY = 350
      .FontBold = False
      .Text = CStr(rd!Cama)
    End If
    
    .CurrentX = 200
    .CurrentY = 600
    .FontBold = True
    .Text = CStr("F. Realización:")
    
    .CurrentX = 1700
    .CurrentY = 600
    .FontBold = False
    If IsNull(rd!CI01FECCONCERT) Then
      .Text = CStr(Format(rd!PR04FECENTRCOLA, "dd/MM/YY HH:MM"))
    Else
      .Text = CStr(Format(rd!CI01FECCONCERT, "dd/MM/YY HH:MM"))
    End If

    If Not IsNull(rd!AG11DESRECURSO) Then
      .CurrentX = 3500
      .CurrentY = 600
      .FontBold = True
      .Text = CStr("Citado para: ")
      
      .CurrentX = 4700
      .CurrentY = 600
      .FontBold = False
      .Text = CStr(rd!AG11DESRECURSO)
    End If
    .PenWidth = 50
    .DrawLine 200, 1000, 11000, 1000

    .CurrentX = 300
    .CurrentY = 1200
    .FontBold = True
    .Text = "Fecha Petición: "
    
    .CurrentX = 1800
    .CurrentY = 1200
    .FontBold = False
    .Text = Format$(rd!pr09fecpeticion, "dd/mm/yy hh:mm")
    
    .CurrentX = 3500
    .CurrentY = 1200
    .FontBold = True
    .Text = "Dpto. Solicitante: "
    
    .CurrentX = 5200
    .CurrentY = 1200
    .FontBold = False
    .Text = CStr(rd!AD02DESDPTO)
    
    .CurrentX = 7200
    .CurrentY = 1200
    .FontBold = True
    .Text = "Doctor: "
    
    .CurrentX = 8000
    .CurrentY = 1200
    .FontBold = False
    .Paragraph = CStr(rd!DOCTOR)
    
    '.Paragraph = ""
    .CurrentX = 300
    .FontBold = True
    .Text = "Observaciones Petición:"

    Dim pos%
    Observaciones = Trim(CStr(rd!Observaciones))
    CodAct = rd!PR04NUMACTPLAN
    rd.MoveNext
    Do While Not rd.EOF
      If CodAct <> rd!PR04NUMACTPLAN Then
        If Observaciones <> Trim(rd!Observaciones) Then
          If Observaciones <> "" And Observaciones = "." Then
            Observaciones = Observaciones & ". " & Trim(CStr(rd!Observaciones))
          Else
            Observaciones = Trim(CStr(rd!Observaciones))
          End If
        End If
      End If
      CodAct = rd!PR04NUMACTPLAN
      rd.MoveNext
    Loop
    rd.MoveFirst
    .FontBold = False
    .Paragraph = Observaciones

    .CurrentX = 300
    .FontBold = True
    .FontSize = 14
    .FontUnderline = True
    .Paragraph = CStr(rd!PR01DESCORTA)
    
    .FontSize = 6
    .FontUnderline = False
    .Paragraph = ""
    
    .CurrentX = 300
    .FontBold = True
    .FontSize = 10
    .Text = "Motivo Petición: "

    If Not IsNull(rd!PR08DESMOTPET) Then
      .FontBold = False
      .Paragraph = Trim(CStr(rd!PR08DESMOTPET))
    Else
      .Paragraph = ""
    End If
    .CurrentX = 300
    .FontBold = True
    .Text = "Instrucciones Realización: "
    
    If Not IsNull(rd!PR04DESINSTREA) Then
      .FontBold = False
      .Text = Trim(CStr(rd!PR04DESINSTREA))
    End If
    
    .Paragraph = ""
    .CurrentX = 300
    .FontBold = True
    .Text = "Indicaciones: "

    If Not IsNull(rd!PR08DESINDICAC) Then
      .CurrentX = 1700
      .MarginLeft = 1700
      .FontBold = False
      .Text = Trim(CStr(rd!PR08DESINDICAC))
    End If

    .MarginLeft = 300
    .Paragraph = ""
    .FontBold = True
    .FontUnderline = True
    .Paragraph = "CUESTIONARIO"
    
    .FontSize = 6
    .FontUnderline = False
    .Paragraph = ""
    
    .FontSize = 10
    If Not IsNull(rd!PR40DESPREGUNTA) Then
      .CurrentX = 300
      .FontBold = True
      .Text = CStr(rd!PR40DESPREGUNTA) + ": "
    End If

    If Not IsNull(rd!PR41RESPUESTA) Then
      .FontBold = False
      .Paragraph = CStr(rd!PR41RESPUESTA)
    End If
    CodAct = rd!PR04NUMACTPLAN
    rd.MoveNext
    Do While Not rd.EOF
'*************
      If CodAct <> rd!PR04NUMACTPLAN Then
        .Paragraph = ""
        .CurrentX = 300
        .FontBold = True
        .FontSize = 16
        .FontUnderline = True
        .Paragraph = CStr(rd!PR01DESCORTA)
        
        .FontSize = 6
        .FontUnderline = False
        .Paragraph = ""
        
        .CurrentX = 300
        .FontBold = True
        .FontSize = 10
        .Text = "Motivo Petición: "
        
        If Not IsNull(rd!PR08DESMOTPET) Then
          .FontBold = False
          .Paragraph = Trim(CStr(rd!PR08DESMOTPET))
        Else
          .Paragraph = ""
        End If
        .CurrentX = 300
        .FontBold = True
        .Text = "Instrucciones Realización: "
        
        If Not IsNull(rd!PR04DESINSTREA) Then
          .FontBold = False
          .Paragraph = Trim(CStr(rd!PR04DESINSTREA))
        Else
          .Paragraph = ""
        End If
        .CurrentX = 300
        .FontBold = True
        .Text = "Indicaciones: "
        If Not IsNull(rd!PR08DESINDICAC) Then
          .MarginLeft = 1700
          .FontBold = False
          .Text = Trim(CStr(rd!PR08DESINDICAC))
        End If
        '.Paragraph = ""
        .Paragraph = ""
        .CurrentX = 300
        .FontBold = True
        .FontUnderline = True
        .Paragraph = "CUESTIONARIO"
        .FontSize = 6
        .FontUnderline = False
        .Paragraph = ""
      End If
      
      CodAct = rd!PR04NUMACTPLAN
    
      .FontSize = 10
      If Not IsNull(rd!PR40DESPREGUNTA) Then
        .CurrentX = 300
        .FontBold = True
        .Text = CStr(rd!PR40DESPREGUNTA) + ": "
      End If
    
      If Not IsNull(rd!PR41RESPUESTA) Then
        .FontBold = False
        .Paragraph = CStr(rd!PR41RESPUESTA)
      End If
      rd.MoveNext
    Loop
    
    .Action = 6 'End Document
    DoEvents
    .PrintDoc  ' Se imprime
    DoEvents
    .KillDoc

  End With
  Screen.MousePointer = 0

End Function




'Public Function ImprimirPeticiones(NumAct As Variant)
'Dim vsPet As vsPrinter
'Dim str$
'Dim rd As rdoResultset
'Dim Qy As rdoQuery
'
'str = "SELECT CI2200.CI22SEGAPEL||' '||CI2200.CI22PRIAPEL||', '||CI2200.CI22NOMBRE PACIENTE," & _
'"CI2200.CI22NUMHISTORIA, SUBSTR(AD08NUMCASO,7,4) CASO,CI2200.CI22FECNACIM, " & _
'"AG1100.AG11DESRECURSO, PR0400.PR04FECENTRCOLA, CI0100.CI01FECCONCERT, " & _
'"GCFN06(AD1500.AD15CODCAMA) CAMA, " & _
'"PR0900.PR09FECPETICION, " & _
'"PR0900.PR09DESOBSERVAC ||' '||PR0900.PR09DESINDICACIO||' '||AG1600.AG16DESTIPREST OBSERVACIONES, " & _
'"AD0200.AD02DESDPTO, PR0100.PR01DESCORTA,PR0800.PR08DESMOTPET,PR0400.PR04DESINSTREA, " & _
'"NVL(SG0200.SG02TXTFIRMA,SG0200.SG02APE1|| SG0200.SG02APE2||', '||SG0200.SG02NOM) DOCTOR, PR4000.PR40DESPREGUNTA, PR4100.PR41RESPUESTA "
'str = str & _
'"FROM PR0400,PR0100, AD0800, CI2200, PR0800, PR0900, " & _
'"AD0200, SG0200, CI0100, AG1100, AD1500, PR4100, PR4000, " & _
'"PR4700,AG1600 " & _
'"WHERE  PR0400.PR01CODACTUACION = PR0100.PR01CODACTUACION " & _
'"AND PR0400.AD01CODASISTENCI = AD0800.AD01CODASISTENCI " & _
'"AND PR0400.AD07CODPROCESO = AD0800.AD07CODPROCESO " & _
'"AND PR0400.CI21CODPERSONA = CI2200.CI21CODPERSONA " & _
'"AND PR0400.PR03NUMACTPEDI=PR0800.PR03NUMACTPEDI " & _
'"AND PR0900.AD02CODDPTO = AD0200.AD02CODDPTO " & _
'"AND PR0400.AD01CODASISTENCI = AD1500.AD01CODASISTENCI (+) " & _
'"AND PR0400.AD07CODPROCESO = AD1500.AD07CODPROCESO (+) " & _
'"AND PR0400.PR03NUMACTPEDI = PR4100.PR03NUMACTPEDI (+) " & _
'"AND PR4100.PR40CODPREGUNTA = PR4000.PR40CODPREGUNTA (+) " & _
'"AND PR0900.SG02COD = SG0200.SG02COD  " & _
'"AND CI0100.AG11CODRECURSO = AG1100.AG11CODRECURSO (+)  " & _
'"AND PR0400.PR04NUMACTPLAN = CI0100.PR04NUMACTPLAN (+) " & _
'"AND PR0800.PR09NUMPETICION = PR0900.PR09NUMPETICION " & _
'"AND PR0900.PR09NUMPETICION = PR4700.PR09NUMPETICION (+) " & _
'"AND PR4700.AG16CODTIPREST = AG1600.AG16CODTIPREST (+) " & _
'"AND CI0100.CI01SITCITA (+)='1' " & _
'"AND PR0400.PR04NUMACTPLAN = (?)"
'Set Qy = objApp.rdoConnect.CreateQuery("", str)
'Qy(0) = NumAct
'Set rd = Qy.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
'Load frmPet
'With frmPet.vsPet
'.Action = 3 ' StartDoc
'.CurrentX = 8700
'.CurrentY = 200
'.FontSize = 8
'.Text = "Fecha: " & Format$(Now, "DD/MM/YY") & "  Hora: " & Format$(Now, "HH:MM")
'
'.CurrentX = 200
'.CurrentY = 100
'.FontName = "Arial"
'.FontSize = 10
'.FontBold = True
'.Text = rd!PACIENTE
'
'.FontName = "Arial"
'.FontSize = 10
'.CurrentX = 200
'.CurrentY = 350
'.FontBold = True
'.Text = CStr(rd!CI22NUMHISTORIA) + " / " + CStr(rd!CASO)
'
'
'.CurrentX = 3500
'.CurrentY = 350
'.FontBold = True
'.Text = CStr("Fecha de Nacimiento: ")
'
'.CurrentX = 5600
'.CurrentY = 350
'.FontBold = False
'.Text = CStr(rd!CI22FECNACIM)
'
'If Not IsNull(rd!Cama) Then
' .CurrentX = 7200
' .CurrentY = 350
'  .FontBold = True
' .Text = "Cama: "
'
' .CurrentX = 7900
' .CurrentY = 350
'  .FontBold = False
' .Text = CStr(rd!Cama)
'End If
'
'.CurrentX = 200
'.CurrentY = 600
'.FontBold = True
'.Text = CStr("F. Realización:")
'
'.CurrentX = 1700
'.CurrentY = 600
'.FontBold = False
'If IsNull(rd!CI01FECCONCERT) Then
'   .Text = CStr(Format(rd!PR04FECENTRCOLA, "dd/MM/YY HH:MM"))
'Else
'   .Text = CStr(Format(rd!CI01FECCONCERT, "dd/MM/YY HH:MM"))
'End If
'
'If Not IsNull(rd!AG11DESRECURSO) Then
'.CurrentX = 3500
'.CurrentY = 600
'.FontBold = True
'.Text = CStr("Citado para: ")
'
' .CurrentX = 4700
'.CurrentY = 600
'.FontBold = False
'.Text = CStr(rd!AG11DESRECURSO)
'
'End If
'.PenWidth = 50
'.DrawLine 200, 1000, 11000, 1000
'
'
'.CurrentX = 300
'.CurrentY = 1200
'.FontBold = True
'.Text = "Fecha Petición: "
'
'.CurrentX = 1800
'.CurrentY = 1200
'.FontBold = False
'.Text = Format$(rd!pr09fecpeticion, "dd/mm/yy hh:mm")
'
'
'.CurrentX = 3500
'.CurrentY = 1200
'.FontBold = True
'.Text = "Dpto. Solicitante: "
'
'.CurrentX = 5200
'.CurrentY = 1200
'.FontBold = False
'.Text = CStr(rd!AD02DESDPTO)
'
'.CurrentX = 7200
'.CurrentY = 1200
'.FontBold = True
'.Text = "Doctor: "
'
'.CurrentX = 8000
'.CurrentY = 1200
'.FontBold = False
'.Paragraph = CStr(rd!DOCTOR)
' .Paragraph = ""
'.CurrentX = 300
'.FontBold = True
'.Text = "Observaciones Petición:"
'
'.FontBold = False
'.Paragraph = Trim(CStr(rd!Observaciones))
' .Paragraph = ""
' .Paragraph = ""
'
' .CurrentX = 300
'.FontBold = True
'.FontSize = 14
'.FontUnderline = True
'.Paragraph = CStr(rd!PR01DESCORTA)
'
'.FontSize = 6
'.FontUnderline = False
'.Paragraph = ""
'
'.CurrentX = 300
'.FontBold = True
'.FontSize = 10
'.Text = "Motivo Petición: "
'
' If Not IsNull(rd!PR08DESMOTPET) Then
' .FontBold = False
' .Paragraph = Trim(CStr(rd!PR08DESMOTPET))
'Else
' .Paragraph = ""
'End If
' .Paragraph = ""
'.CurrentX = 300
'.FontBold = True
'.Text = "Instrucciones Realización: "
'
' If Not IsNull(rd!PR04DESINSTREA) Then
' .FontBold = False
' .Paragraph = Trim(CStr(rd!PR04DESINSTREA))
' Else
' .Paragraph = ""
' End If
' .Paragraph = ""
'.CurrentX = 300
'.FontBold = True
' .FontUnderline = True
'.Paragraph = "CUESTIONARIO"
'
'.FontSize = 6
'.FontUnderline = False
'.Paragraph = ""
'
'.FontSize = 10
'If Not IsNull(rd!PR40DESPREGUNTA) Then
' .CurrentX = 300
' .FontBold = True
' .Text = CStr(rd!PR40DESPREGUNTA) + ": "
'End If
'
'If Not IsNull(rd!PR41RESPUESTA) Then
' .FontBold = False
' .Paragraph = CStr(rd!PR41RESPUESTA)
'End If
'
'.Action = 6 'End Document
'DoEvents
'.PrintDoc  ' Se imprime
'DoEvents
'.KillDoc
'
'End With
'
'End Function
'


