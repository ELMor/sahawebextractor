VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmObservAct 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datos / Cuestionario de la Actuaci�n"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9825
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   9825
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtPac 
      Height          =   345
      Left            =   2100
      Locked          =   -1  'True
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   60
      Width           =   4215
   End
   Begin VB.TextBox txtNumActPlan 
      Height          =   345
      Left            =   1140
      Locked          =   -1  'True
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   480
      Width           =   855
   End
   Begin VB.TextBox txtAct 
      Height          =   345
      Left            =   2100
      Locked          =   -1  'True
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   480
      Width           =   4215
   End
   Begin VB.TextBox txtNH 
      Height          =   345
      Left            =   1140
      Locked          =   -1  'True
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   60
      Width           =   855
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7635
      Left            =   120
      TabIndex        =   1
      Top             =   900
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   13467
      _Version        =   327681
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Datos"
      TabPicture(0)   =   "PR0502.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1(3)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1(2)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1(1)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame1(0)"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Cuestionario"
      TabPicture(1)   =   "PR0502.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "ssgrdCuest"
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame1 
         Caption         =   "Solicitante"
         ForeColor       =   &H00C00000&
         Height          =   675
         Index           =   0
         Left            =   180
         TabIndex        =   16
         Top             =   360
         Width           =   9315
         Begin VB.TextBox txtDpto 
            Height          =   315
            Left            =   900
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   240
            Width           =   3735
         End
         Begin VB.TextBox txtDr 
            Height          =   315
            Left            =   5700
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   240
            Width           =   3435
         End
         Begin VB.Label Label1 
            Caption         =   "Dpto."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   300
            TabIndex        =   20
            Top             =   300
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "Dr."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   5400
            TabIndex        =   19
            Top             =   300
            Width           =   375
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Petici�n"
         ForeColor       =   &H00C00000&
         Height          =   2115
         Index           =   1
         Left            =   180
         TabIndex        =   11
         Top             =   1080
         Width           =   9315
         Begin VB.TextBox txtObservPet 
            BackColor       =   &H00FFFFFF&
            Height          =   870
            HelpContextID   =   30104
            Left            =   900
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   240
            Width           =   8340
         End
         Begin VB.TextBox txtIndicPet 
            BackColor       =   &H00FFFFFF&
            Height          =   870
            HelpContextID   =   30104
            Left            =   900
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   1140
            Width           =   8340
         End
         Begin VB.Label Label1 
            Caption         =   "Observ."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   180
            TabIndex        =   15
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Indic."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   180
            TabIndex        =   14
            Top             =   1140
            Width           =   675
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Actuaci�n"
         ForeColor       =   &H00C00000&
         Height          =   3015
         Index           =   2
         Left            =   180
         TabIndex        =   4
         Top             =   3240
         Width           =   9315
         Begin VB.TextBox txtMotivoAct 
            BackColor       =   &H00FFFFFF&
            Height          =   870
            HelpContextID   =   30104
            Left            =   900
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   2040
            Width           =   8340
         End
         Begin VB.TextBox txtIndicAct 
            BackColor       =   &H00FFFFFF&
            Height          =   870
            HelpContextID   =   30104
            Left            =   900
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   6
            TabStop         =   0   'False
            Top             =   1140
            Width           =   8340
         End
         Begin VB.TextBox txtObservAct 
            BackColor       =   &H00FFFFFF&
            Height          =   870
            HelpContextID   =   30104
            Left            =   900
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   240
            Width           =   8340
         End
         Begin VB.Label Label1 
            Caption         =   "Observ."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   180
            TabIndex        =   10
            Top             =   240
            Width           =   735
         End
         Begin VB.Label Label1 
            Caption         =   "Indic."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   240
            TabIndex        =   9
            Top             =   1140
            Width           =   675
         End
         Begin VB.Label Label1 
            Caption         =   "Motivo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   8
            Top             =   2040
            Width           =   675
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Instrucciones Realizaci�n"
         ForeColor       =   &H00C00000&
         Height          =   1215
         Index           =   3
         Left            =   180
         TabIndex        =   2
         Top             =   6300
         Width           =   9315
         Begin VB.TextBox txtReali 
            BackColor       =   &H00FFFFFF&
            Height          =   870
            HelpContextID   =   30104
            Left            =   120
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            TabStop         =   0   'False
            Top             =   240
            Width           =   9105
         End
      End
      Begin SSDataWidgets_B.SSDBGrid ssgrdCuest 
         Height          =   7155
         Left            =   -74880
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   9435
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   2
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   2
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   2
         Columns(0).Width=   8308
         Columns(0).Caption=   "Pregunta"
         Columns(0).Name =   "Pregunta"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   25532
         Columns(1).Caption=   "Respuesta"
         Columns(1).Name =   "Respuesta"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   16642
         _ExtentY        =   12621
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   435
      Left            =   6480
      TabIndex        =   0
      Top             =   360
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Paciente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   8
      Left            =   120
      TabIndex        =   27
      Top             =   120
      Width           =   915
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   7
      Left            =   180
      TabIndex        =   26
      Top             =   540
      Width           =   915
   End
End
Attribute VB_Name = "frmObservAct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim lngNumActPlan&
Dim blnMostrarTab0, blnMostrarTab1 As Boolean 'para mostrar seleccionada una pesta�a en el Tab u otra
Dim blnVerSoloSiHayDatos As Boolean

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    'inicializaci�n de variables ya que a veces no se hace el unload
    blnMostrarTab0 = False
    blnMostrarTab1 = False
    
    'se formatean los controles del formulario
    Call pFormatearControles
    
    'se obtiene el n� de actuaci�n planificada
    If objPipe.PipeExist("PR_NActPlan") Then
        lngNumActPlan = objPipe.PipeGet("PR_NActPlan")
        Call objPipe.PipeRemove("PR_NActPlan")
    End If
    
    'se mira el tipo de acceso al Load de la pantalla
    'NOTA: se utiliza la variable blnVerSoloSiHayDatos para evitar hacer alguna SELECT _
    innecesaria en la funci�n pCargarCuestionario ya que si el formulario s�lo se debe _
    mostrar cuando hay datos hay que distinguir si se trata de una actuaci�n de consulta _
    para no tener en cuenta como "datos" las restricciones de usuario
    blnVerSoloSiHayDatos = False
    If objPipe.PipeExist("PR_VerSoloSiHayDatos") Then blnVerSoloSiHayDatos = True
    
    'se muestran los datos
    If lngNumActPlan > 0 Then
        Call pCargarCabecera
        Call pCargarDatos
        Call pCargarCuestionario
    End If
    
    'se establece un Pipe que indica que hay datos (observ., indic., o cuestionario) _
    para mostrar en el formulario
    'se utilizar� en los procesos que realizan el Load de este formulario para decidir _
    si se hace el Show o el Unload (si no hay datos)
    If blnMostrarTab0 Or blnMostrarTab1 Then Call objPipe.PipeSet("PR_frmObservAct", True)
    
    'se selecciona la pesta�a: si no hay datos en la pesta�a 0 y s� los hay en la 1 _
    se selecciona la 1. En el resto de los casos se selecciona la 0
    If Not blnMostrarTab0 And blnMostrarTab1 Then SSTab1.Tab = 1 Else SSTab1.Tab = 0
End Sub

Private Sub pFormatearControles()
    txtNH.BackColor = objApp.objUserColor.lngReadOnly
    txtPac.BackColor = objApp.objUserColor.lngReadOnly
    txtNumActPlan.BackColor = objApp.objUserColor.lngReadOnly
    txtAct.BackColor = objApp.objUserColor.lngReadOnly
    
    txtDpto.BackColor = objApp.objUserColor.lngReadOnly
    txtDr.BackColor = objApp.objUserColor.lngReadOnly
    txtObservPet.BackColor = objApp.objUserColor.lngReadOnly
    txtIndicPet.BackColor = objApp.objUserColor.lngReadOnly
    txtObservAct.BackColor = objApp.objUserColor.lngReadOnly
    txtIndicAct.BackColor = objApp.objUserColor.lngReadOnly
    txtMotivoAct.BackColor = objApp.objUserColor.lngReadOnly
    txtReali.BackColor = objApp.objUserColor.lngReadOnly
    
    ssgrdCuest.BackColorEven = objApp.objUserColor.lngReadOnly
    ssgrdCuest.BackColorOdd = objApp.objUserColor.lngReadOnly
End Sub

Private Sub pCargarDatos()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    'se traen los datos del solicitante, de la petici�n y de la actuaci�n
    SQL = "SELECT AD02DESDPTO, NVL(SG02TXTFIRMA,SG02APE1||' '||SG02APE2||', '||SG02NOM) SG02DESDR,"
    SQL = SQL & " PR09DESOBSERVAC, PR09DESINDICACIO, PR04DESINSTREA,"
    SQL = SQL & " PR08DESOBSERV, PR08DESINDICAC, PR08DESMOTPET"
    SQL = SQL & " FROM AD0200, SG0200, PR0900, PR0800, PR0300, PR0400"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND PR0300.PR03NUMACTPEDI = PR0400.PR03NUMACTPEDI"
    SQL = SQL & " AND PR0900.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    SQL = SQL & " AND AD0200.AD02CODDPTO = PR0900.AD02CODDPTO"
    SQL = SQL & " AND SG0200.SG02COD = PR0900.SG02COD"
    SQL = SQL & " AND PR0800.PR09NUMPETICION = PR0300.PR09NUMPETICION"
    SQL = SQL & " AND PR0800.PR03NUMACTPEDI = PR0300.PR03NUMACTPEDI"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then
        If Not IsNull(rs!AD02DESDPTO) Then txtDpto = rs!AD02DESDPTO
        If Not IsNull(rs!SG02DESDR) Then txtDr = rs!SG02DESDR
        If Not IsNull(rs!PR09DESOBSERVAC) Then txtObservPet = rs!PR09DESOBSERVAC: blnMostrarTab0 = True
        If Not IsNull(rs!PR09DESINDICACIO) Then txtIndicPet = rs!PR09DESINDICACIO: blnMostrarTab0 = True
        If Not IsNull(rs!PR08DESOBSERV) Then txtObservAct = rs!PR08DESOBSERV: blnMostrarTab0 = True
        If Not IsNull(rs!PR08DESINDICAC) Then txtIndicAct = rs!PR08DESINDICAC: blnMostrarTab0 = True
        If Not IsNull(rs!PR08DESMOTPET) Then txtMotivoAct = rs!PR08DESMOTPET: blnMostrarTab0 = True
        If Not IsNull(rs!PR04DESINSTREA) Then txtReali = rs!PR04DESINSTREA: blnMostrarTab0 = True
    End If
    rs.Close
    qry.Close
End Sub

Private Sub pCargarCuestionario()
    Dim SQL$, qry As rdoQuery, rs As rdoResultset, qry1 As rdoQuery, rs1 As rdoResultset
    Dim strResp$, strPreg$
    Dim lngNumActPedi&
    
    ssgrdCuest.ScrollBars = ssScrollBarsNone

    'se busca el n� de actuaci�n pedida PR03NUMACTPEDI para que luego entre por �ndice _
    en las pr�ximas SELECT
    SQL = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN = ?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    lngNumActPedi = rs!PR03NUMACTPEDI

    'preguntas y respuestas
    SQL = "SELECT PR40DESPREGUNTA, PR27CODTIPRESPU, NVL(PR41RESPUESTA,' ') PR41RESPUESTA"
    SQL = SQL & " FROM PR4000, PR4100"
    SQL = SQL & " WHERE PR03NUMACTPEDI =?"
    SQL = SQL & " AND PR4000.PR40CODPREGUNTA = PR4100.PR40CODPREGUNTA"
    SQL = SQL & " ORDER BY PR27CODTIPRESPU"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPedi
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then blnMostrarTab1 = True
    Do While Not rs.EOF
        If rs!PR27CODTIPRESPU = 3 Then
            If rs!PR41RESPUESTA = -1 Then strResp = "S�" Else strResp = "No"
        Else
            strResp = rs!PR41RESPUESTA
        End If
        ssgrdCuest.AddItem rs!PR40DESPREGUNTA & Chr$(9) & strResp
        rs.MoveNext
    Loop
    rs.Close
    qry.Close

    'se mira si es necesario cargar las restricciones del paciente en funci�n de: _
    - si se ha accedido a la pantalla al Iniciar una prueba o directamente _
    - si se trata de una actuaci�n de consulta u otra cualquiera
    If blnVerSoloSiHayDatos Then
        SQL = "SELECT PR12CODACTIVIDAD"
        SQL = SQL & " FROM PR0100, PR0300"
        SQL = SQL & " WHERE PR03NUMACTPEDI = ?"
        SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0300.PR01CODACTUACION"
        Set qry = objApp.rdoConnect.CreateQuery("", SQL)
        qry(0) = lngNumActPedi
        Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
        If rs(0) = 201 Then 'Actuaci�n de Consulta
            ssgrdCuest.ScrollBars = ssScrollBarsAutomatic
            'no hace falta ver si tiene restricciones ya que para las consultas no intersa _
            mostrarlas
            Exit Sub
        End If
    End If
           
    'se cargan las restricciones del paciente (tipo movilidad, tipo asistencia...)
    SQL = "SELECT AG16DESTIPREST, AG18CODTIPDATO, NVL(AG16TABLAVALOR,' ') AG16TABLAVALOR,"
    SQL = SQL & " AG16COLCODVALO, AG16COLDESVALO, NVL(VALOR,' ') VALOR"
    SQL = SQL & " FROM PR0301J"
    SQL = SQL & " WHERE PR03NUMACTPEDI =?"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPedi
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not rs.EOF Then blnMostrarTab1 = True
    Do While Not rs.EOF
        'pregunta
        strPreg = rs!AG16DESTIPREST
        'respuesta
        If Trim(rs!VALOR) = "" Then
            strResp = ""
        Else
            Select Case rs!AG18CODTIPDATO
            Case 1, 2 'respuesta num�rica o texto
                If Trim(rs!AG16TABLAVALOR) = "" Then
                    strResp = Trim(rs!VALOR)
                Else
                    SQL = "SELECT " & rs!AG16COLDESVALO
                    SQL = SQL & " FROM " & rs!AG16TABLAVALOR
                    SQL = SQL & " WHERE " & rs!AG16COLCODVALO & " = ?"
                    Set qry1 = objApp.rdoConnect.CreateQuery("", SQL)
                    qry1(0) = rs!VALOR
                    Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
                    strResp = rs1(0)
                    rs1.Close
                    qry1.Close
                End If
            Case 3 'respuesta tipo fecha
                strResp = Format(rs!VALOR, "dd/mm/yyyy")
            End Select
        End If
        
        ssgrdCuest.AddItem strPreg & Chr$(9) & strResp
        
        rs.MoveNext
    Loop
    rs.Close
    qry.Close
    
    ssgrdCuest.ScrollBars = ssScrollBarsAutomatic
End Sub

Private Sub pCargarCabecera()
'*************************************************************************************
'*  Muestra los datos del paciente al que se realiza la actuaci�n seleccionada
'*************************************************************************************
    Dim SQL$, qry As rdoQuery, rs As rdoResultset
    
    SQL = "SELECT PR0400.PR04NUMACTPLAN, PR0100.PR01DESCORTA,"
    SQL = SQL & " CI2200.CI22NUMHISTORIA,"
    SQL = SQL & " CI2200.CI22PRIAPEL||' '||CI2200.CI22SEGAPEL||', '||CI2200.CI22NOMBRE PACIENTE"
    SQL = SQL & " FROM CI2200, PR0100, PR0400"
    SQL = SQL & " WHERE PR0400.PR04NUMACTPLAN = ?"
    SQL = SQL & " AND CI2200.CI21CODPERSONA = PR0400.CI21CODPERSONA"
    SQL = SQL & " AND PR0100.PR01CODACTUACION = PR0400.PR01CODACTUACION"
    Set qry = objApp.rdoConnect.CreateQuery("", SQL)
    qry(0) = lngNumActPlan
    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not IsNull(rs!CI22NUMHISTORIA) Then
        txtNH.Text = rs!CI22NUMHISTORIA
    Else
        txtNH.Text = ""
    End If
    txtPac.Text = rs!PACIENTE
    txtNumActPlan.Text = rs!PR04NUMACTPLAN
    txtAct.Text = rs!PR01DESCORTA
    rs.Close
    qry.Close
End Sub

