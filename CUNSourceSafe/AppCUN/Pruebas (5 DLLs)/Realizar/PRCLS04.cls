VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher DLL Realizar
' Coded by SIC Donosti
' **********************************************************************************

' Primero los listados. Un proceso por listado.
' Los valores deben coincidir con el nombre del archivo en disco
'REALIZAR
Const PRWinEstadoPruebas           As String = "PR0134"
Const PRWinPeticPendNW             As String = "PR1137"
Const PRWinSituacionPaciente       As String = "PR0173"
Const PRWinControlarPet            As String = "PR0182"
Const PRWinEstadoPeticion          As String = "PR0183"
Const PRWinMensajeCorreo           As String = "PR0184"
Const PRWINEscogerDpto             As String = "PR0194"
Const PRWinrellenarinstreal        As String = "PR0195"
Const PRWinmeterinstreal           As String = "PR0196"
Const PRWincuestrealizar           As String = "PR0260"
Const PRWActuacionesRealizadas     As String = "PR0272"
Const PRWinPetSanitarios           As String = "PR0275"
Const PRWinListadoPread            As String = "PR0248"
Const PRWinActRealiNuevo           As String = "PR0273"
Const PRWRealizarActuacion         As String = "PR0500"
Const PRWObservaActuacion          As String = "PR0502"
Const PRWinCitasControles          As String = "PR0504"
Const PRWinRecConsumidos           As String = "PR0503"
Const PRWinActuacionesSinRecurso   As String = "PR0505"
Const PRWinActPlanAnestesia        As String = "PR0506"
Const PRWinActReaAnestesia         As String = "PR0509"
Const PRWinCambioActuaciones       As String = "PR0507"
Const PRWinCambioIntervenciones    As String = "PR0508"
Const PRWinDatosActuacion          As String = "PR0510"

Const PRWinAsociarAnestesia        As String = "pr0600"
Const PRWinIntervencionesPendientes         As String = "PR0550"
Const PRWinRegistroQuirofano       As String = "PR0553"
Const PRWinRealizaInterven         As String = "PR0560"
Const PRWinMensajes                As String = "PR0580"
Const PRWinImprimirEtiquetas       As String = "PR4000"
Const PRRepPeticionesPendientes    As String = "PR2400"

Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
  Set objCW = mobjCW
  objCW.blnAutoDisconnect = False
  objCW.SetClockEnable (False)
End Sub

' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntdata As Variant) As Boolean
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
    'REALIZAR

    Case PRWinSituacionPaciente
        Load frmsituacionpaciente
        Call objsecurity.AddHelpContext(553)
        Call frmsituacionpaciente.Show(vbModal)
        Unload frmsituacionpaciente
        Call objsecurity.RemoveHelpContext
        Set frmsituacionpaciente = Nothing

    Case PRWinEstadoPruebas
        primerallamada = True
        Load frmestadopruebas
        Call objsecurity.AddHelpContext(549)
        Call frmestadopruebas.Show(vbModal)
        Unload frmestadopruebas
        Call objsecurity.RemoveHelpContext
        Set frmestadopruebas = Nothing
    Case PRWinrellenarinstreal
        primerallamada = True
        Load frmrellenarinstreal
        'Call objsecurity.AddHelpContext(559)
        Call frmrellenarinstreal.Show(vbModal)
        Unload frmrellenarinstreal
        'Call objsecurity.RemoveHelpContext
        Set frmrellenarinstreal = Nothing
    Case PRWinmeterinstreal
        Load frmmeterinstreal
        'Call objsecurity.AddHelpContext(559)
        Call frmmeterinstreal.Show(vbModal)
        Unload frmmeterinstreal
        'Call objsecurity.RemoveHelpContext
        Set frmmeterinstreal = Nothing
    Case PRWinControlarPet
        Load frmControlarPet
        Call objsecurity.AddHelpContext(558)
        Call frmControlarPet.Show(vbModal)
        Unload frmControlarPet
        Call objsecurity.RemoveHelpContext
        Set frmControlarPet = Nothing
        'Call objsecurity.LaunchProcess("PR0182")
    Case PRWinEstadoPeticion
        Load frmEstadoPeticion
        Call objsecurity.AddHelpContext(549)
        Call frmEstadoPeticion.Show(vbModal)
        Unload frmEstadoPeticion
        Call objsecurity.RemoveHelpContext
        Set frmEstadoPeticion = Nothing
    Case PRWinMensajeCorreo
        Load frmMensajeCorreo
        Call objsecurity.AddHelpContext(561)
        Call frmMensajeCorreo.Show(vbModal)
        Unload frmMensajeCorreo
        Call objsecurity.RemoveHelpContext
        Set frmMensajeCorreo = Nothing
    Case PRWincuestrealizar
        'vntdata(1)--> Numero de peticon
        'vntdata(2)--> actuacion pedida
        glngnumPeticion = vntdata(1)
        glngactpedida = vntdata(2)
          Load frmcuestrealizar
          'Call objsecurity.AddHelpContext(561)
          Call frmcuestrealizar.Show(vbModal)
          Unload frmcuestrealizar
          'Call objsecurity.RemoveHelpContext
          Set frmcuestrealizar = Nothing
    Case PRWINEscogerDpto
        Load frmEscogerDpto
        Call objsecurity.AddHelpContext(538)
        Call frmEscogerDpto.Show(vbModal)
        Unload frmEscogerDpto
        Call objsecurity.RemoveHelpContext
        Set frmEscogerDpto = Nothing
    Case PRWActuacionesRealizadas
        Load frmActuacionesRealizadas
        Call objsecurity.AddHelpContext(538)
        Call frmActuacionesRealizadas.Show(vbModal)
        Unload frmActuacionesRealizadas
        Call objsecurity.RemoveHelpContext
        Set frmActuacionesRealizadas = Nothing
    Case PRWinActRealiNuevo
    
        Load frmActuacionesRealizadas1
        Call frmActuacionesRealizadas1.Show(vbModal)
        Unload frmActuacionesRealizadas1
        Set frmActuacionesRealizadas1 = Nothing
    Case PRWObservaActuacion
        Load frmObservAct
        Call objsecurity.AddHelpContext(538)
        Call frmObservAct.Show(vbModal)
        Unload frmObservAct
        Call objsecurity.RemoveHelpContext
        Set frmObservAct = Nothing
    Case PRWRealizarActuacion
    Dim a As Variant
        On Error Resume Next
        a = vntdata(1)
        If Err = 0 Then
        Call objPipe.PipeSet("PR_dpto", a)
        Call objPipe.PipeSet("PR_REC", vntdata(2))
        End If
        On Error GoTo 0
        frmRealizacionActuaciones.Show vbModal
        Set frmRealizacionActuaciones = Nothing
    Case PRWinPetSanitarios
        frmPeticionesSanitarios.Show vbModal
        Set frmPeticionesSanitarios = Nothing
    Case PRWinCitasControles
        frmCitaControles.Show vbModal
        Set frmCitaControles = Nothing
    Case PRWinIntervencionesPendientes
        frmIntervencionesPendientes.Show vbModal
        Set frmIntervencionesPendientes = Nothing
    Case PRWinRegistroQuirofano
        If Not IsMissing(vntdata) Then Call objPipe.PipeSet("PR_NumHQ", vntdata)
        frmRegistroQuirofano.Show vbModal
        Set frmRegistroQuirofano = Nothing
    Case PRWinRealizaInterven
        frmRealizacionIntervenciones.Show vbModal
        Set frmRealizacionIntervenciones = Nothing
    Case PRWinPeticPendNW
        On Error Resume Next
        a = vntdata(1)
        If Err = 0 Then Call objPipe.PipeSet("PR_dpto", a)
        On Error GoTo 0
        frmPeticionesPendientesNW.Show vbModal
        Set frmPeticionesPendientesNW = Nothing
    Case PRWinMensajes
        Call objPipe.PipeSet("CI21CODPERSONA", vntdata(1))
        frmAvisos.Show vbModal
        Set frmAvisos = Nothing
    Case PRWinActuacionesSinRecurso
        frmActuacionesSinRecurso.Show vbModal
        Set frmActuacionesSinRecurso = Nothing
    Case PRWinActPlanAnestesia
        frmConsultaActPlanAnestesia.Show vbModal
        Set frmConsultaActPlanAnestesia = Nothing
    Case PRWinActReaAnestesia
        frmConsultaActReaAnestesia.Show vbModal
        Set frmConsultaActReaAnestesia = Nothing
    Case PRWinCambioActuaciones
        Call objPipe.PipeSet("PR_DPTO", vntdata(1))
        Call objPipe.PipeSet("PR_ACTIV", vntdata(2))
        Call objPipe.PipeSet("PR_ACTPLAN", vntdata(3))
        frmCambioActuaciones.Show vbModal
        Set frmCambioActuaciones = Nothing
    Case PRWinCambioIntervenciones
'        Call objPipe.PipeSet("PR_DPTO", vntdata(1))
'        Call objPipe.PipeSet("PR_ACTIV", vntdata(2))
'        Call objPipe.PipeSet("PR_ACTPLAN", vntdata(3))
        frmCambioInterv.Show vbModal
        Set frmCambioInterv = Nothing
        'devuelve un Pipe llamado "PR_Cambio" con las actuaciones nuevas
    Case PRWinAsociarAnestesia
        Call objPipe.PipeSet("PR_NUMPETI", vntdata(1))
        Call objPipe.PipeSet("PR_CODPER", vntdata(2))
        Call objPipe.PipeSet("PR_NUMACTP", vntdata(3))
        Call objPipe.PipeSet("PR_NUMACTPLAN", vntdata(4))
          Load frmAsociarAnestesia
          Call objsecurity.AddHelpContext(561)
          Call frmAsociarAnestesia.Show(vbModal)
          Unload frmAsociarAnestesia
          Call objsecurity.RemoveHelpContext
          Set frmAsociarAnestesia = Nothing
    Case PRWinRecConsumidos
        Call objPipe.PipeSet("PR_NActPlan", vntdata(1))
        Call objPipe.PipeSet("AD_CodDpto", vntdata(2))
        frmRecConsumidos.Show vbModal
        Set frmRecConsumidos = Nothing
    Case PRWinImprimirEtiquetas
        Call objPipe.PipeSet("CI_CODPER", vntdata(1))
        frmImprimirEtiquetas.Show vbModal
        Set frmImprimirEtiquetas = Nothing
    Case PRWinDatosActuacion
        Call objPipe.PipeSet("PR0510_PR04NUMACTPLAN", vntdata)
        frmInformacionAct.Show vbModal
        Set frmInformacionAct = Nothing
    
    Case PRRepPeticionesPendientes
        Call ImprimirPeticiones(vntdata)
  
  End Select
  Call Err.Clear
End Function

Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz a 11 procesos
  ReDim aProcess(1 To 29, 1 To 4) As Variant
      
  ' VENTANAS
 'REALIZAR
  aProcess(1, 1) = PRWinActReaAnestesia
  aProcess(1, 2) = "Actuaciones Realizadas de Anestesia"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = PRWinEstadoPruebas
  aProcess(2, 2) = "Estados de las Pruebas"
  aProcess(2, 3) = True
  aProcess(2, 4) = cwTypeWindow
      
  aProcess(6, 1) = PRWinSituacionPaciente
  aProcess(6, 2) = "Informar Situaci�n Paciente"
  aProcess(6, 3) = False
  aProcess(6, 4) = cwTypeWindow
      
  aProcess(8, 1) = PRWinControlarPet
  aProcess(8, 2) = "Controlar Peticiones"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow

  aProcess(9, 1) = PRWinEstadoPeticion
  aProcess(9, 2) = "Estado de la Petici�n"
  aProcess(9, 3) = False
  aProcess(9, 4) = cwTypeWindow

  aProcess(10, 1) = PRWinMensajeCorreo
  aProcess(10, 2) = "Mensaje de Correo"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow

  aProcess(11, 1) = PRWinrellenarinstreal
  aProcess(11, 2) = "Rellenar Instrucciones de Realizaci�n"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow

  aProcess(12, 1) = PRWinmeterinstreal
  aProcess(12, 2) = "Meter Instrucciones de Realizaci�n"
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeWindow
  
  aProcess(15, 1) = PRWincuestrealizar
  aProcess(15, 2) = "Cuestionario en la realizaci�n"
  aProcess(15, 3) = False
  aProcess(15, 4) = cwTypeWindow
  
  aProcess(16, 1) = PRWINEscogerDpto
  aProcess(16, 2) = "Selecci�n de Dptos"
  aProcess(16, 3) = False
  aProcess(16, 4) = cwTypeWindow

  aProcess(18, 1) = PRWRealizarActuacion
  aProcess(18, 2) = "Realizaci�n de Actuaciones"
  aProcess(18, 3) = True
  aProcess(18, 4) = cwTypeWindow
  
  aProcess(19, 1) = PRWinListadoPread
  aProcess(19, 2) = "Listado de Preadmisiones"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow
  
  aProcess(20, 1) = PRWinPeticPendNW
  aProcess(20, 2) = "Peticiones Pendientes Nueva"
  aProcess(20, 3) = True
  aProcess(20, 4) = cwTypeWindow
  
  aProcess(21, 1) = PRWinMensajes
  aProcess(21, 2) = "Avisos"
  aProcess(21, 3) = False
  aProcess(21, 4) = cwTypeWindow
  
  aProcess(22, 1) = PRWinActPlanAnestesia
  aProcess(22, 2) = "Actuaciones Planificadas Anestesia"
  aProcess(22, 3) = True
  aProcess(22, 4) = cwTypeWindow
  
  aProcess(23, 1) = PRWinAsociarAnestesia
  aProcess(23, 2) = "Asociar Anestesia"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeWindow
  
  aProcess(24, 1) = PRWinRecConsumidos
  aProcess(24, 2) = "Recursos Consumidos"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeWindow
  
  aProcess(25, 1) = PRWinRegistroQuirofano
  aProcess(25, 2) = "Registro de Quir�fano"
  aProcess(25, 3) = False
  aProcess(25, 4) = cwTypeWindow
  
  aProcess(26, 1) = PRWinImprimirEtiquetas
  aProcess(26, 2) = "Imprimir Etiquetas"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow
      
  aProcess(27, 1) = PRWinDatosActuacion
  aProcess(27, 2) = "Informaci�n de la Actuaci�n"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeWindow
      
  aProcess(28, 1) = PRWinCambioIntervenciones
  aProcess(28, 2) = "Cambio de intervenciones"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow

  aProcess(29, 1) = PRWinActRealiNuevo
  aProcess(29, 2) = "Actuaciones Realizadas por Paciente (Nueva)"
  aProcess(29, 3) = True
  aProcess(29, 4) = cwTypeWindow

  aProcess(30, 1) = PRRepPeticionesPendientes
  aProcess(30, 2) = "Listado de Peticiones Pendientes"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeReport

End Sub
