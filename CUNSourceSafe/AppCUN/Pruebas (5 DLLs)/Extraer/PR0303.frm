VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmcuestionarioMuestra 
   Caption         =   "GESTI�N DE ACTUACIONES. Extracciones. Cuestionario de la muestra asignada"
   ClientHeight    =   3495
   ClientLeft      =   1335
   ClientTop       =   2910
   ClientWidth     =   5580
   HelpContextID   =   2016145
   Icon            =   "PR0303.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4.18757e8
   ScaleMode       =   0  'User
   ScaleWidth      =   1.03298e8
   ShowInTaskbar   =   0   'False
   Tag             =   "Recordset"
   WindowState     =   2  'Maximized
   Begin VB.CommandButton cmdfin 
      Caption         =   "Fin del Cuestionario"
      Height          =   255
      Left            =   5280
      TabIndex        =   18
      Top             =   5880
      Width           =   2055
   End
   Begin VB.Frame Frame1 
      Caption         =   "Cuestionario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5655
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   11655
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   4815
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   11415
         Begin VB.PictureBox picFields 
            Appearance      =   0  'Flat
            AutoRedraw      =   -1  'True
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   4815
            Left            =   120
            ScaleHeight     =   4776.48
            ScaleMode       =   0  'User
            ScaleWidth      =   10568.41
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   0
            Width           =   10575
            Begin VB.Frame frametapar 
               BorderStyle     =   0  'None
               Height          =   495
               Index           =   0
               Left            =   8760
               TabIndex        =   20
               Top             =   2280
               Visible         =   0   'False
               Width           =   855
            End
            Begin VB.TextBox Text1 
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   0  'None
               Height          =   285
               Index           =   0
               Left            =   8880
               TabIndex        =   15
               Top             =   360
               Visible         =   0   'False
               Width           =   615
            End
            Begin VB.CheckBox chkCheck2 
               Caption         =   "NO"
               Height          =   255
               Index           =   0
               Left            =   960
               TabIndex        =   11
               TabStop         =   0   'False
               Top             =   2640
               Visible         =   0   'False
               Width           =   615
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "SI"
               Height          =   255
               Index           =   0
               Left            =   0
               TabIndex        =   10
               Top             =   2640
               Visible         =   0   'False
               Width           =   855
            End
            Begin SSCalendarWidgets_A.SSDateCombo SSDateCombo1 
               Height          =   285
               Index           =   0
               Left            =   0
               TabIndex        =   9
               Top             =   2040
               Visible         =   0   'False
               Width           =   1815
               _Version        =   65537
               _ExtentX        =   3201
               _ExtentY        =   508
               _StockProps     =   93
               BackColor       =   16777215
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
            End
            Begin VB.CommandButton cmdbuscar 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "Arial Black"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Index           =   0
               Left            =   3480
               TabIndex        =   8
               TabStop         =   0   'False
               Top             =   2520
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataSource      =   "Data1"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   288
               Index           =   0
               Left            =   600
               TabIndex        =   7
               Top             =   480
               Visible         =   0   'False
               Width           =   5403
            End
            Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
               Height          =   285
               Index           =   0
               Left            =   600
               TabIndex        =   6
               TabStop         =   0   'False
               Top             =   1200
               Visible         =   0   'False
               Width           =   5659
               DataFieldList   =   "Column 0"
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               FieldSeparator  =   ";"
               RowHeight       =   423
               Columns.Count   =   2
               Columns(0).Width=   9525
               Columns(0).Caption=   "RESPUESTA"
               Columns(0).Name =   "RESPUESTA"
               Columns(0).DataField=   "Column 0"
               Columns(0).DataType=   8
               Columns(0).FieldLen=   256
               Columns(1).Width=   3200
               Columns(1).Visible=   0   'False
               Columns(1).Caption=   "CODIGO"
               Columns(1).Name =   "CODIGO"
               Columns(1).DataField=   "Column 1"
               Columns(1).DataType=   8
               Columns(1).FieldLen=   256
               _ExtentX        =   9982
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DataFieldToDisplay=   "Column 0"
            End
            Begin VB.Label lblLabel6 
               Caption         =   "Label6"
               Height          =   255
               Index           =   0
               Left            =   9360
               TabIndex        =   19
               Top             =   1680
               Visible         =   0   'False
               Width           =   735
            End
            Begin VB.Label lblLabel5 
               Caption         =   "Label5"
               Height          =   255
               Index           =   0
               Left            =   9360
               TabIndex        =   17
               Top             =   960
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.Label lblLabel4 
               Caption         =   "Label4"
               Height          =   255
               Index           =   0
               Left            =   9600
               TabIndex        =   16
               Top             =   360
               Visible         =   0   'False
               Width           =   615
            End
            Begin VB.Label lblLabel3 
               Caption         =   "Label3"
               Height          =   375
               Index           =   0
               Left            =   7920
               TabIndex        =   14
               Top             =   360
               Visible         =   0   'False
               Width           =   1095
            End
            Begin VB.Label lblLabel2 
               Caption         =   "Label2"
               Height          =   375
               Index           =   0
               Left            =   600
               TabIndex        =   13
               Top             =   960
               Visible         =   0   'False
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Label1"
               Height          =   255
               Index           =   0
               Left            =   360
               TabIndex        =   12
               Top             =   0
               Visible         =   0   'False
               Width           =   5655
            End
         End
         Begin VB.VScrollBar vsbScrollBar 
            Height          =   4800
            LargeChange     =   3000
            Left            =   11040
            SmallChange     =   300
            TabIndex        =   4
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   252
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Actuaciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2295
      Left            =   120
      TabIndex        =   0
      Top             =   6240
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Height          =   1815
         Left            =   240
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11175
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ","
         Col.Count       =   2
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Caption=   "C�D. ACTUACI�N"
         Columns(0).Name =   "C�DIGO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   13097
         Columns(1).Caption=   "ACTUACI�N"
         Columns(1).Name =   "PREGUNTA"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   19711
         _ExtentY        =   3201
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmcuestionarioMuestra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR0303.FRM                                                   *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: DICIEMBRE DE 1998                                             *
'* DESCRIPCI�N: Se muestra el cuestionario asociado a la muestra        *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
'>>>>>>>>>>>>>>>>>>>>>>>>
Const gnCTLARRAYHEIGHT = 830&          '
Dim intfocus As Integer
Dim intcamp As Integer
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim nocheckclic As Integer
Dim numactuaciones As Integer
Dim texto() As Integer
Dim casilla() As Integer
Dim lista() As Integer
Dim existe_indice() As Integer
Dim desplazamiento
Dim valoranterior As Integer
Dim inthay As Integer
Dim stractpedi As String
Dim rstactpedi As rdoResultset

Private Sub chkCheck1_Click(Index As Integer)
If nocheckclic = 0 Then
  If chkCheck1(Index).Value = 1 Then
    chkCheck2(Index).Value = 0
  End If
  If chkCheck1(Index).Value = 0 Then
    chkCheck2(Index).Value = 1
  End If
End If
End Sub
Private Sub chkCheck2_Click(Index As Integer)
If nocheckclic = 0 Then
  If chkCheck2(Index).Value = 1 Then
    chkCheck1(Index).Value = 0
  End If
  If chkCheck2(Index).Value = 0 Then
    chkCheck1(Index).Value = 1
  End If
End If
End Sub
Private Sub chkCheck1_GotFocus(Index As Integer)
Dim stract As String
Dim rstact As rdoResultset
Dim rstdes As rdoResultset
Dim strdes As String

SSDBGrid1.RemoveAll
If lblLabel6(Index) = -1 Then 'la pregunta  es agrupable
    stract = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN IN (" & _
        "SELECT PR04NUMACTPLAN FROM pr4300 WHERE PR40CODPREGUNTA=" & lblLabel4(Index) & _
        " AND PR43INDAGRUPA=-1" & _
        " AND PR04NUMACTPLAN IN " & gstractcuest & _
        ")"
    Set rstact = objApp.rdoConnect.OpenResultset(stract)
    While Not rstact.EOF
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstact.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
    rstact.MoveNext
    Wend
    rstact.Close
    Set rstact = Nothing
    rstdes.Close
    Set rstdes = Nothing
End If
If lblLabel6(Index) = 0 Then 'la pregunta NO es agrupable
      stractpedi = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN=" & _
                    lblLabel5(Index)
      Set rstactpedi = objApp.rdoConnect.OpenResultset(stractpedi)
      
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstactpedi.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
      rstdes.Close
      Set rstdes = Nothing
      rstactpedi.Close
      Set rstactpedi = Nothing
End If

'vsbScrollBar.Value = -(lblLabel2(Index) * vsbScrollBar.SmallChange)
End Sub





Private Sub chkCheck2_GotFocus(Index As Integer)
Dim stract As String
Dim rstact As rdoResultset
Dim rstdes As rdoResultset
Dim strdes As String

SSDBGrid1.RemoveAll
If lblLabel6(Index) = -1 Then 'la pregunta  es agrupable
    stract = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN IN (" & _
        "SELECT PR04NUMACTPLAN FROM pr4300 WHERE PR40CODPREGUNTA=" & lblLabel4(Index) & _
        " AND PR43INDAGRUPA=-1" & _
        " AND PR04NUMACTPLAN IN " & gstractcuest & _
        ")"
    Set rstact = objApp.rdoConnect.OpenResultset(stract)
    While Not rstact.EOF
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstact.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
    rstact.MoveNext
    Wend
    rstact.Close
    Set rstact = Nothing
    rstdes.Close
    Set rstdes = Nothing
End If
If lblLabel6(Index) = 0 Then 'la pregunta NO es agrupable
      stractpedi = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN=" & _
                    lblLabel5(Index)
      Set rstactpedi = objApp.rdoConnect.OpenResultset(stractpedi)
      
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstactpedi.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
      rstdes.Close
      Set rstdes = Nothing
      rstactpedi.Close
      Set rstactpedi = Nothing
End If
'vsbScrollBar.Value = -(lblLabel2(Index) * vsbScrollBar.SmallChange)
End Sub



Private Sub cmdfin_Click()
Unload Me
End Sub

Private Sub Form_Activate()
vsbScrollBar.Value = vsbScrollBar.min
  
'actuaciones pedidas de la peticion
Dim strsql As String
Dim rsta As rdoResultset
Dim strsql4 As String
Dim strsql5 As String
Dim rstA5 As rdoResultset
Dim intcontador As Integer
Dim intnumact As Integer
Dim i As Integer
Dim strsql1 As String
Dim rstA1 As rdoResultset
Dim blncargado As Integer
Dim intsobran As Integer
Dim j As Integer
Dim preg
Dim resp
Dim k As Integer
Dim existe As Integer


inthay = 0
numactuaciones = 0
cmdfin.TabStop = False
SSDBGrid1.TabStop = False

strsql = "select count(*) from PR4300 where PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
         " AND pr04numactplan in " & gstractcuest
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
numactuaciones = rsta.rdoColumns(0).Value
strsql = "select * from PR4300 where PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
         " AND pr04numactplan in " & gstractcuest
Set rsta = objApp.rdoConnect.OpenResultset(strsql)
ReDim texto(numactuaciones)
ReDim casilla(numactuaciones)
ReDim lista(numactuaciones)
ReDim existe_indice(numactuaciones)
preg = -710 - 140
resp = -476 - 140

    For i = 1 To numactuaciones
    '*******************************************************************
     For k = 1 To numactuaciones
       If existe_indice(k) = 1 Then
        If rsta.rdoColumns("PR40CODPREGUNTA").Value = lblLabel4(k) _
                And lblLabel6(k) = -1 Then
            existe = 1
            Exit For
        End If
       End If
      Next k
      If existe = 1 And rsta.rdoColumns("PR43INDAGRUPA").Value = -1 Then
        existe = 0
        existe_indice(i) = 0
        If Not rsta.EOF Then
            rsta.MoveNext
        End If
      Else
       existe = 0
       existe_indice(i) = 1
    
       picFields.Height = picFields.Height + gnCTLARRAYHEIGHT
       Load lblLabel1(i)
       lblLabel1(i).Visible = True
       Load lblLabel2(i)
       lblLabel2(i).Visible = True
       Load lblLabel3(i)
       'lblLabel3(i).Visible = True
       Load lblLabel4(i)
       'lblLabel4(i).Visible = True
       Load lblLabel5(i)
       'lblLabel5(i).Visible = True
       Load lblLabel6(i)
       'lblLabel6(i).Visible = True
       Load Text1(i)
       Text1(i).Visible = True
       Load frametapar(i)
       frametapar(i).Visible = True
       
       strsql1 = "select pr40despregunta from PR4000 " _
                & "where pr40codpregunta=" & rsta.rdoColumns("pr40codpregunta").Value
       Set rstA1 = objApp.rdoConnect.OpenResultset(strsql1)
       lblLabel1(i) = rstA1.rdoColumns("PR40DESPREGUNTA").Value
       lblLabel4(i) = rsta.rdoColumns("pr40codpregunta").Value
       lblLabel2(i) = inthay + 1
       
       lblLabel1(i).Left = 600
       lblLabel1(i).Top = preg + 830
       lblLabel2(i).Left = 120
       lblLabel2(i).Top = preg + 830
       lblLabel3(i).Left = 7900
       lblLabel3(i).Top = preg + 830
       lblLabel4(i).Left = 9550
       lblLabel4(i).Top = preg + 830
       lblLabel5(i).Left = 10000
       lblLabel5(i).Top = preg + 830
       lblLabel6(i).Left = 10100
       lblLabel6(i).Top = preg + 830
       Text1(i).Left = 8600
       Text1(i).Top = preg + 830
       Text1(i).TabStop = False
       frametapar(i).Left = 8600
       frametapar(i).Top = preg + 830
       frametapar(i).Caption = ""
       frametapar(i).BorderStyle = 0
       frametapar(i).ZOrder (0)
       preg = preg + 830
       
       'PR27CODTIPRESPU
       Select Case (rsta.rdoColumns("pr27codtiprespu").Value)
        Case 1
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               txtText1(i).Text = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel5(i) = rsta.rdoColumns("PR04NUMACTPLAN").Value
            lblLabel6(i) = rsta.rdoColumns("PR43INDAGRUPA").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("pr43indoblig").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 600
            txtText1(i).Top = resp + 833
        Case 2
            Load txtText1(i)
            texto(i) = 1
            txtText1(i).Visible = True
            txtText1(i).Locked = False
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
                txtText1(i).Text = ""
            Else
                txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel5(i) = rsta.rdoColumns("PR04NUMACTPLAN").Value
            lblLabel6(i) = rsta.rdoColumns("PR43INDAGRUPA").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("pr43indoblig").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            txtText1(i).Left = 600
            txtText1(i).Top = resp + 833
        Case 3
            Load chkCheck1(i)
            Load chkCheck2(i)
            casilla(i) = 1
            chkCheck1(i).Visible = True
            chkCheck2(i).Visible = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               nocheckclic = 1
               chkCheck1(i).Value = 0
               chkCheck2(i).Value = 0
               nocheckclic = 0
            Else
               If rsta.rdoColumns("PR43RESPUESTA").Value = "-1" Then
                chkCheck1(i).Value = 1
               End If
               If rsta.rdoColumns("PR43RESPUESTA").Value = "0" Then
                chkCheck2(i).Value = 1
              End If
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel5(i) = rsta.rdoColumns("PR04NUMACTPLAN").Value
            lblLabel6(i) = rsta.rdoColumns("PR43INDAGRUPA").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("pr43indroblig").Value = -1) Then
                chkCheck1(i).BackColor = &HFFFF00
                chkCheck2(i).BackColor = &HFFFF00
            End If
            chkCheck1(i).Left = 600
            chkCheck2(i).Left = 1460
            chkCheck1(i).Top = resp + 833
            chkCheck2(i).Top = resp + 833
        Case 4
            Load SSDBCombo1(i)
            SSDBCombo1(i).RemoveAll
            Load txtText1(i)
            txtText1(i).ZOrder (0)
            lista(i) = 1
            texto(i) = 1
            SSDBCombo1(i).Visible = True
            txtText1(i).Visible = True
            txtText1(i).Locked = True
            If IsNull(rsta.rdoColumns("PR43RESPUESTA").Value) Then
               txtText1(i) = ""
            Else
               txtText1(i).Text = rsta.rdoColumns("PR43RESPUESTA").Value
            End If
            If IsNull(rsta.rdoColumns("pr28numrespuesta").Value) Then
               Text1(i).Text = ""
            Else
               Text1(i).Text = rsta.rdoColumns("pr28numrespuesta").Value
            End If
            lblLabel3(i) = rsta.rdoColumns("pr27codtiprespu").Value
            lblLabel5(i) = rsta.rdoColumns("PR04NUMACTPLAN").Value
            lblLabel6(i) = rsta.rdoColumns("PR43INDAGRUPA").Value
            'Respuesta Obligatoria (PR43INDOBLIG)
            If (rsta.rdoColumns("pr43indroblig").Value = -1) Then
                txtText1(i).BackColor = &HFFFF00
            End If
            SSDBCombo1(i).Left = 600
            txtText1(i).Left = 600
            SSDBCombo1(i).Top = resp + 833
            txtText1(i).Top = resp + 833
            txtText1(i).ZOrder (0)
             ' se rellena la Combo
            'CODLISTRESP
            strsql5 = "select pr28desrespuesta,pr28numrespuesta from pr2800 " _
                  & "where pr46codlistresp=" & rsta.rdoColumns("pr46codlistresp").Value _
                  & " order by pr28numrespuesta"
            Set rstA5 = objApp.rdoConnect.OpenResultset(strsql5)
            While (Not rstA5.EOF)
                Call SSDBCombo1(i).AddItem(rstA5.rdoColumns("pr28desrespuesta").Value & ";" & rstA5.rdoColumns("pr28numrespuesta").Value)
                rstA5.MoveNext
            Wend
            rstA5.Close
            Set rstA5 = Nothing
       End Select
       resp = resp + 833
       
       If (rsta.EOF) Then
          Exit For
       Else
          inthay = inthay + 1
          rsta.MoveNext
          If rsta.EOF Then
             blncargado = 1
             Exit For
          End If
       End If
    rstA1.Close
    Set rstA1 = Nothing
    '*******************************
    'Next i
    End If
    Next i
    '*******************************

  vsbScrollBar.Visible = True
  vsbScrollBar.min = 0
  vsbScrollBar.max = -32600
  vsbScrollBar.SmallChange = 32200 / inthay
  vsbScrollBar.LargeChange = 32200 / inthay
  vsbScrollBar.Value = 0
  Text1(1).SetFocus

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



Private Sub Form_Unload(Cancel As Integer)
Dim strsql5 As String
Dim rstA5 As rdoResultset
Dim strmodif As String
Dim rstA6 As rdoResultset
Dim strsql6 As String
Dim i As Integer
Dim rstactuacion As rdoResultset
Dim stractuacion As String
Dim mensaje As String

gnotodas = False
'Unload de los controles de la pantalla y UPDATE en la BD
For i = 1 To numactuaciones
If existe_indice(i) = 1 Then
    If texto(i) = 1 Then
    If txtText1(i).BackColor = &HFFFF00 And txtText1(i).Text = "" Then
        gnotodas = True
    End If
       txtText1(i).Locked = False
       strsql6 = "select pr40codpregunta from PR4000 " _
                & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       On Error GoTo Err_Ejecutar
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If lblLabel6(i) = 0 Then
        If Text1(i).Text <> "" Then
           strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
             & ",pr28numrespuesta=" & Text1(i).Text _
             & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
               " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        Else
           strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
             & ",pr28numrespuesta=null" _
             & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
               " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
               " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
        End If
        objApp.rdoConnect.Execute strmodif, 64
        objApp.rdoConnect.Execute "Commit", 64
       End If
       If lblLabel6(i) = -1 Then
        stractuacion = "SELECT PR04NUMACTPLAN FROM pr4300 WHERE " & _
                     "PR40CODPREGUNTA=" & rstA6.rdoColumns("pr40codpregunta").Value & _
                     " AND " & _
                     "PR43INDAGRUPA=-1" & " AND " & _
                     "PR04NUMACTPLAN IN " & gstractcuest
        Set rstactuacion = objApp.rdoConnect.OpenResultset(stractuacion)
        While Not rstactuacion.EOF
                If Text1(i).Text <> "" Then
                   strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
                     & ",pr28numrespuesta=" & Text1(i).Text _
                     & " WHERE PR04NUMACTPLAN=" & rstactuacion.rdoColumns(0).Value & _
                       " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                       " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                Else
                   strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & txtText1(i) & "'" _
                     & ",pr28numrespuesta=null" _
                     & " WHERE PR04NUMACTPLAN=" & rstactuacion.rdoColumns(0).Value & _
                       " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                       " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                End If
            objApp.rdoConnect.Execute strmodif, 64
            objApp.rdoConnect.Execute "Commit", 64
        rstactuacion.MoveNext
        Wend
        rstactuacion.Close
        Set rstactuacion = Nothing
       End If
       On Error GoTo Err_Ejecutar
       Unload txtText1(i)
       texto(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
       '***************
       Unload lblLabel1(i)
       Unload lblLabel2(i)
       Unload lblLabel3(i)
       Unload lblLabel4(i)
       Unload lblLabel5(i)
       Unload lblLabel6(i)
       Unload Text1(i)
       Unload frametapar(i)
       '*************
    End If
    
    
    If casilla(i) = 1 Then
    If chkCheck1(i).BackColor = &HFFFF00 And chkCheck1(i).Value = 0 And chkCheck2(i).Value = 0 Then
        gnotodas = True
    End If
       strsql6 = "select pr40codpregunta from PR4000 " _
                 & "where pr40despregunta LIKE " & "'" & lblLabel1(i).Caption & "'"
       Set rstA6 = objApp.rdoConnect.OpenResultset(strsql6)
       If lblLabel6(i) = -1 Then
            If chkCheck1(i).Value = 1 Then
                 If Text1(i).Text <> "" Then
                     strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
                          & ",pr28numrespuesta=" & Text1(i).Text _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                 Else
                     strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
                          & ",pr28numrespuesta=null" _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                 End If
            objApp.rdoConnect.Execute strmodif, 64
            objApp.rdoConnect.Execute "Commit", 64
            End If
            If chkCheck2(i).Value = 1 Then
                     If Text1(i).Text <> "" Then
                       strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
                          & ",pr28numrespuesta=" & Text1(i).Text _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                     Else
                       strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
                          & ",pr28numrespuesta=null" _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                     End If
            objApp.rdoConnect.Execute strmodif, 64
            objApp.rdoConnect.Execute "Commit", 64
            End If
       End If
       If lblLabel6(i) = 0 Then
            stractuacion = "SELECT PR04NUMACTPLAN FROM pr4300 WHERE " & _
                          "PR40CODPREGUNTA=" & rstA6.rdoColumns("pr40codpregunta").Value & _
                          " AND " & _
                          "PR43INDAGRUPA=-1" & " AND " & _
                          "PR04NUMACTPLAN IN " & gstractcuest
             Set rstactuacion = objApp.rdoConnect.OpenResultset(stractuacion)
             While Not rstactuacion.EOF
                If chkCheck1(i).Value = 1 Then
                 If Text1(i).Text <> "" Then
                     strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
                          & ",pr28numrespuesta=" & Text1(i).Text _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                 Else
                     strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & -1 & "'" _
                          & ",pr28numrespuesta=null" _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                 End If
            objApp.rdoConnect.Execute strmodif, 64
            objApp.rdoConnect.Execute "Commit", 64
            End If
            If chkCheck2(i).Value = 1 Then
                     If Text1(i).Text <> "" Then
                       strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
                          & ",pr28numrespuesta=" & Text1(i).Text _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                     Else
                       strmodif = "UPDATE  pr4300 SET PR43RESPUESTA= " & "'" & 0 & "'" _
                          & ",pr28numrespuesta=null" _
                          & " WHERE PR04NUMACTPLAN=" & lblLabel5(i) & _
                            " AND PR52NUMMUESTRA=" & frmAsigMuestras.grdDBGrid1(1).Columns(3).Value & _
                            " AND pr40codpregunta=" & rstA6.rdoColumns("pr40codpregunta").Value
                     End If
            objApp.rdoConnect.Execute strmodif, 64
            objApp.rdoConnect.Execute "Commit", 64
            End If
             rstactuacion.MoveNext
             Wend
             rstactuacion.Close
             Set rstactuacion = Nothing
        End If
       Unload chkCheck1(i)
       Unload chkCheck2(i)
       casilla(i) = 0
       Text1(i).Text = ""
       rstA6.Close
       Set rstA6 = Nothing
       '***************
       Unload lblLabel1(i)
       Unload lblLabel2(i)
       Unload lblLabel3(i)
       Unload lblLabel4(i)
       Unload lblLabel5(i)
       Unload lblLabel6(i)
       Unload Text1(i)
       Unload frametapar(i)
       '*************
    End If
    If lista(i) = 1 Then
         Unload SSDBCombo1(i)
        lista(i) = 0
        'Text1(i).Text = ""
    End If
End If
Next i

If gnotodas = True Then
   mensaje = MsgBox("No ha contestado a todas las preguntas obligatorias." & Chr(13) & _
               "�Desea salir?", vbYesNo, "Aviso")
   If mensaje = 6 Then 'si
        Cancel = 0
   End If
   If mensaje = 7 Then 'no
        Cancel = 1
        Call Form_Activate
   End If
Else
   Cancel = 0
End If

Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub SSDBCombo1_Click(Index As Integer)
    txtText1(Index).Text = SSDBCombo1(Index).Columns(0).Text
    Text1(Index).Text = SSDBCombo1(Index).Columns(1).Text
End Sub

Private Sub SSDBCombo1_GotFocus(Index As Integer)
Dim stract As String
Dim rstact As rdoResultset
Dim rstdes As rdoResultset
Dim strdes As String

SSDBGrid1.RemoveAll
If lblLabel6(Index) = -1 Then 'la pregunta  es agrupable
    stract = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN IN (" & _
        "SELECT PR04NUMACTPLAN FROM pr4300 WHERE PR40CODPREGUNTA=" & lblLabel4(Index) & _
        " AND PR43INDAGRUPA=-1" & _
        " AND PR04NUMACTPLAN IN " & gstractcuest & _
        ")"
    Set rstact = objApp.rdoConnect.OpenResultset(stract)
    While Not rstact.EOF
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstact.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
    rstact.MoveNext
    Wend
    rstact.Close
    Set rstact = Nothing
    rstdes.Close
    Set rstdes = Nothing
End If
If lblLabel6(Index) = 0 Then 'la pregunta NO es agrupable
      stractpedi = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN=" & _
                    lblLabel5(Index)
      Set rstactpedi = objApp.rdoConnect.OpenResultset(stractpedi)
      
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstactpedi.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
      rstdes.Close
      Set rstdes = Nothing
      rstactpedi.Close
      Set rstactpedi = Nothing
End If
'vsbScrollBar.Value = -(lblLabel2(Index) * vsbScrollBar.SmallChange)
End Sub






Private Sub Text1_GotFocus(Index As Integer)
'vsbScrollBar.Value = -(lblLabel2(Index) * vsbScrollBar.SmallChange)
End Sub

Private Sub txtText1_GotFocus(Index As Integer)
Dim stract As String
Dim rstact As rdoResultset
Dim rstdes As rdoResultset
Dim strdes As String


SSDBGrid1.RemoveAll
If lblLabel6(Index) = -1 Then 'la pregunta  es agrupable
    stract = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN IN (" & _
        "SELECT PR04NUMACTPLAN FROM pr4300 WHERE PR40CODPREGUNTA=" & lblLabel4(Index) & _
        " AND PR43INDAGRUPA=-1" & _
        " AND PR04NUMACTPLAN IN " & gstractcuest & _
        ")"
    Set rstact = objApp.rdoConnect.OpenResultset(stract)
    While Not rstact.EOF
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstact.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
    rstdes.Close
    Set rstdes = Nothing
    rstact.MoveNext
    Wend
    rstact.Close
    Set rstact = Nothing
End If
If lblLabel6(Index) = 0 Then 'la pregunta NO es agrupable
      stractpedi = "SELECT PR03NUMACTPEDI FROM PR0400 WHERE PR04NUMACTPLAN=" & _
                    lblLabel5(Index)
      Set rstactpedi = objApp.rdoConnect.OpenResultset(stractpedi)
      
      strdes = "SELECT PR0100.PR01CODACTUACION,PR0100.PR01DESCORTA FROM PR0300,PR0100 " & _
               "WHERE " & _
               "PR0100.PR01CODACTUACION=PR0300.PR01CODACTUACION" & _
               " AND PR0300.PR03NUMACTPEDI=" & rstactpedi.rdoColumns(0).Value
      Set rstdes = objApp.rdoConnect.OpenResultset(strdes)
      
      SSDBGrid1.AddNew
      SSDBGrid1.Columns(0).Value = rstdes.rdoColumns(0).Value
      SSDBGrid1.Columns(0).Locked = True
      SSDBGrid1.Columns(1).Value = rstdes.rdoColumns(1).Value
      SSDBGrid1.Columns(1).Locked = True
      SSDBGrid1.Update
      rstdes.Close
      Set rstdes = Nothing
      rstactpedi.Close
      Set rstactpedi = Nothing
End If

'vsbScrollBar.Value = -(lblLabel2(Index) * vsbScrollBar.SmallChange)

End Sub



Private Sub vsbScrollBar_Change()
Dim i As Integer
Dim incremento As Integer

  If vsbScrollBar.Value = -vsbScrollBar.SmallChange Then
    picFields.Top = 120
  ElseIf vsbScrollBar.Value = vsbScrollBar.max Then
    picFields.Top = -(inthay * gnCTLARRAYHEIGHT)
  Else
    incremento = Abs((vsbScrollBar.Value - valoranterior) / vsbScrollBar.SmallChange)
    If vsbScrollBar.Value < valoranterior Then
      For i = 1 To incremento
        picFields.Top = picFields.Top - gnCTLARRAYHEIGHT
      Next i
    Else
      For i = 1 To incremento
        picFields.Top = picFields.Top + gnCTLARRAYHEIGHT
      Next i
    End If
  End If

  valoranterior = vsbScrollBar.Value

End Sub

