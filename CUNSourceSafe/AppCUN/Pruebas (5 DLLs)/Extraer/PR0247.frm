VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmEscogerDptoM 
   Caption         =   "Departamentos"
   ClientHeight    =   2310
   ClientLeft      =   4110
   ClientTop       =   2955
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2310
   ScaleWidth      =   4680
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "Aceptar"
      Height          =   375
      Left            =   1440
      TabIndex        =   2
      Top             =   1800
      Width           =   1815
   End
   Begin VB.Frame fraframe1 
      Caption         =   "DEPARTAMENTO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1365
      Index           =   1
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   4335
      Begin SSDataWidgets_B.SSDBCombo SSDBCombo1 
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   3
         Top             =   720
         Width           =   3735
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         FieldSeparator  =   ";"
         DefColWidth     =   2
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   2540
         Columns(0).Caption=   "COD. DPTO"
         Columns(0).Name =   "COD. DPTO"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   5927
         Columns(1).Caption=   "DESCRIPCIÓN"
         Columns(1).Name =   "DESCRIPCIÓN"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6588
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblaCT 
         Caption         =   "Departamento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   1
         Top             =   480
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frmEscogerDptoM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdaceptar_Click()
    glngdptologin = SSDBCombo1(0).Columns(0).Value
    Unload Me
End Sub

Private Sub Form_Load()
    Dim sqlstr As String
    Dim rsta As rdoResultset
    Dim sqlstrdpto As String
    Dim rstadpto As rdoResultset
    
    sqlstr = "SELECT * FROM AD0300,AD0200 WHERE AD0200.AD02CODDPTO=AD0300.AD02CODDPTO" & _
             " AND AD0300.SG02COD ='" & objsecurity.strUser & "'" & _
             " AND AD0300.AD03FECFIN IS NULL AND AD0200.AD02FECFIN IS NULL"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    While rsta.EOF = False
        'SSDBCombo1(0).Columns(0).Value = rsta("AD02CODDPTO").Value
        sqlstrdpto = "SELECT * FROM AD0200 WHERE AD02CODDPTO = " & rsta("AD02CODDPTO").Value
        Set rstadpto = objApp.rdoConnect.OpenResultset(sqlstrdpto)
        SSDBCombo1(0).AddItem rsta("AD02CODDPTO").Value & ";" & rstadpto("AD02DESDPTO").Value
        SSDBCombo1(0).Text = rstadpto("AD02DESDPTO").Value
        'SSDBCombo1(0).Columns("DESCRIPCIÓN").Value = rstadpto("AD02DESDPTO").Value
        SSDBCombo1(0).MoveNext
        rsta.MoveNext
        rstadpto.Close
        Set rstadpto = Nothing
    Wend
    rsta.Close
    Set rsta = Nothing
    
End Sub

Private Sub SSDBCombo1_CloseUp(Index As Integer)
    SSDBCombo1(0).Text = SSDBCombo1(0).Columns(1).Value
End Sub

