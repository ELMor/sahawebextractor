VERSION 5.00
Begin VB.Form frmmenuinterac2 
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Interacciones"
   ClientHeight    =   7530
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   8175
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7530
   ScaleWidth      =   8175
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipos de Interacci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5175
      Left            =   1560
      TabIndex        =   0
      Top             =   840
      Width           =   8655
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   480
         TabIndex        =   3
         Top             =   4080
         Width           =   2175
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Actuaci�n - Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   480
         TabIndex        =   2
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Actuaci�n - Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   480
         TabIndex        =   1
         Top             =   1440
         Width           =   2175
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   3240
         TabIndex        =   6
         Top             =   4080
         Width           =   855
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Interacci�n entre Actuaci�n - Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   3240
         TabIndex        =   5
         Top             =   2520
         Width           =   5055
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Interacci�n entre Actuaciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   3240
         TabIndex        =   4
         Top             =   1440
         Width           =   4335
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Actuaci�n-Actuaci�n"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "A&ctuaci�n-Grupo"
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   60
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda   F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmmenuinterac2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS C.U.N.                                             *
'* NOMBRE: PR00111.FRM                                                  *
'* AUTOR: JAVIER OSTOLAZA LASA                                          *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: men� de tipos de interacci�n para una actuaci�n         *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************



Private Sub cmdCommand1_Click(intIndex As Integer)
    cmdCommand1(intIndex).Enabled = False
    Select Case intIndex
        Case 0
            'Load frminterconact
            'frminterconact!tabTab1(0).Tab = 0 'para mostrar el detalle del Tab
            'Call frminterconact.Show(vbModal)
            Call objsecurity.LaunchProcess("PR0112")
            'Unload frminterconact
            'Set frminterconact = Nothing
        Case 1
            'Load frmintercongrp
            'frmintercongrp!tabTab1(0).Tab = 0
            'Call frmintercongrp.Show(vbModal)
            Call objsecurity.LaunchProcess("PR0113")
            'Unload frmintercongrp
            'Set frmintercongrp = Nothing
        Case 2
            Unload Me
            frmdefactuacionesCUN.cmdcondiciones.Enabled = True
            frmdefactuacionesCUN.cmdmuestras.Enabled = True
            frmdefactuacionesCUN.cmdCuestionario.Enabled = True
            frmdefactuacionesCUN.cmdactprev.Enabled = True
            frmdefactuacionesCUN.cmdactasoc.Enabled = True
            frmdefactuacionesCUN.cmddptos.Enabled = True
            frmdefactuacionesCUN.cmdfases.Enabled = True
            frmdefactuacionesCUN.cmdinteracciones.Enabled = True
    End Select
    cmdCommand1(intIndex).Enabled = True
End Sub




Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objApp.HelpContext
    Case 20
      Call objApp.About
  End Select
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
        'Load frminterconact
        'frminterconact!tabTab1(0).Tab = 0 'para mostrar el detalle del Tab
        'Call frminterconact.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0112")
        'Unload frminterconact
        'Set frminterconact = Nothing
    Case 20
        'Load frmintercongrp
        'frmintercongrp!tabTab1(0).Tab = 0
        'Call frmintercongrp.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0113")
        'Unload frmintercongrp
        'Set frmintercongrp = Nothing
    Case 60
      Unload Me
         frmdefactuacionesCUN.cmdcondiciones.Enabled = True
         frmdefactuacionesCUN.cmdmuestras.Enabled = True
         frmdefactuacionesCUN.cmdCuestionario.Enabled = True
         frmdefactuacionesCUN.cmdactprev.Enabled = True
         frmdefactuacionesCUN.cmdactasoc.Enabled = True
         frmdefactuacionesCUN.cmddptos.Enabled = True
         frmdefactuacionesCUN.cmdfases.Enabled = True
         frmdefactuacionesCUN.cmdinteracciones.Enabled = True
  End Select
End Sub
