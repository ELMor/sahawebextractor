VERSION 5.00
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmSeleccionarActGrupo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Selecci�n de Actuaciones."
   ClientHeight    =   4485
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   9690
   ControlBox      =   0   'False
   Icon            =   "PR0169.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9690
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Height          =   3615
      Index           =   1
      Left            =   9600
      TabIndex        =   2
      Top             =   2400
      Width           =   2175
      Begin VB.CommandButton cmdpaquetes 
         Caption         =   "Paquetes"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   600
         Width           =   1695
      End
      Begin VB.CommandButton cmdprotocolos 
         Caption         =   "Protocolos"
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   1200
         Width           =   1695
      End
      Begin VB.CommandButton cmddepartamentos 
         Caption         =   "Departamentos"
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   1800
         Width           =   1695
      End
      Begin VB.CommandButton cmdActgrupo 
         Caption         =   "Grupos"
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   2400
         Width           =   1695
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaciones Seleccionadas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7080
      Index           =   0
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   9135
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6585
         Index           =   0
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   8895
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         stylesets.count =   1
         stylesets(0).Name=   "seleccionar"
         stylesets(0).Picture=   "PR0169.frx":000C
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         StyleSet        =   "seleccionar"
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   15690
         _ExtentY        =   11615
         _StockProps     =   79
         Caption         =   "ACTUACIONES SELECCIONADAS"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   7
      Top             =   4200
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmSeleccionarActGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00169.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 30 DE OCTUBRE DE 1997                                         *
'* DESCRIPCION: PERMITE SELECCIONAR ACTUACIONES DEL GRUPO               *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit


Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim numero_pedida As Long  'cmdcondiciones
Attribute numero_pedida.VB_VarHelpID = -1
Dim numero_secuencia As Long 'cmdcondiciones
Dim pedida As Boolean   'cmdcondiciones
Dim gblnactivar As Boolean


Private Sub cmdActgrupo_Click()
   'Load frmSelGrupos
   'frmSelGrupos.Show (vbModal)
   'Unload frmSelGrupos
   'Set frmSelGrupos = Nothing
   Call objsecurity.LaunchProcess("PR0160")
   Call Insertar_Act_Paq
End Sub

Private Sub cmddepartamentos_Click()
   'Load frmSelDptos
   'frmSelDptos.Show (vbModal)
   'Unload frmSelDptos
   'Set frmSelDptos = Nothing
   Call objsecurity.LaunchProcess("PR0165")
   Call Insertar_Act_Paq
End Sub
Private Sub Insertar_Act_Paq()
    Dim mintindice As Integer
    Dim sqlstrins As String
    Dim strSelect As String
    Dim rsta As rdoResultset
    Dim intCont As Integer
    
    For mintindice = 0 To gintIndice - 1
        'objWinInfo.DataNew
        'grdDBGrid1(0).Columns(3).Value = Gtblactsel(0, mintindice)
        'grdDBGrid1(0).Columns(5).Value = frmDefGrupos.txtText1(0).Text
        strSelect = "SELECT COUNT(*) FROM PR1700 " _
                  & "WHERE PR01CODACTUACION = " & gtblactsel(0, mintindice) _
                  & "  AND PR16CODGRUPO = " & frmDefGrupos.txtText1(0).Text
        Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
        intCont = 1
        intCont = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
        If intCont = 0 Then
          sqlstrins = "INSERT INTO PR1700 (PR16CODGRUPO,pr01codactuacion) VALUES " _
                  & "(" & frmDefGrupos.txtText1(0).Text & "," & gtblactsel(0, mintindice) & ")"
                        
          objApp.rdoConnect.Execute sqlstrins, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
    
          'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(grdDBGrid1(0).Columns(3)), Gtblactsel(0, mintindice))
          'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(grdDBGrid1(0).Columns(5)), Gtblactsel(1, mintindice))
          'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(grdDBGrid1(0).Columns(7)), frmDefProtocolos.txtText1(0).Text)
    Next mintindice
    objWinInfo.DataRefresh
    gintIndice = 0
End Sub
Private Sub cmdpaquetes_Click()
   'Load frmSelPaquetes
   'frmSelPaquetes.Show (vbModal)
   'Unload frmSelPaquetes
   'Set frmSelPaquetes = Nothing
   Call objsecurity.LaunchProcess("PR0163")
   Call Insertar_Act_Paq
End Sub

'Private Sub cmdprincipales_Click()
'   Load frmSelPrincipales
'        frmSelPrincipales.txtText1(8).Text = frmpedir.txtText1(1).Text
 '       frmSelPrincipales.txtText1(9).Text = frmpedir.txtText1(5).Text
'   frmSelPrincipales.Show (vbModal)
'   Unload frmSelPrincipales
'   Set frmSelPrincipales = Nothing
'End Sub

Private Sub cmdprotocolos_Click()
   'Load frmSelProtocolos
   'frmSelProtocolos.Show (vbModal)
   'Unload frmSelProtocolos
   'Set frmSelProtocolos = Nothing
   Call objsecurity.LaunchProcess("PR0164")
   Call Insertar_Act_Paq
End Sub



Private Sub Form_Load()
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)

    With objMultiInfo
        .strName = "Actuaciones del grupo"
        Set .objFormContainer = fraFrame1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR1700"
        .intAllowance = cwAllowReadOnly
        .strWhere = "PR16CODGRUPO=" & frmDefGrupos.txtText1(0).Text
        .intCursorSize = 0
        Call .FormAddOrderField("PR16codgrupo", cwAscending)
 
        strKey = .strDataBase & .strTable
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Actuaciones del Grupo")
        Call .FormAddFilterWhere(strKey, "PR16CODGRUPO", "C�digo Grupo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR35CODGRUPO", "C�digo Grupo")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    End With
  
    With objWinInfo


        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
        
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del grupo
        Call .GridAddColumn(objMultiInfo, "C�d. Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Actuaci�n", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d. Grupo", "PR16CODGRUPO", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Grupo", "", cwString, 30)

        Call .FormCreateInfo(objMultiInfo)
        
        
        'Se indica que campos son obligatorios y cuales son clave primaria
        'en el grid que contiene las actuaciones del grupo
        .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnMandatory = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(5)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(0).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(5)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnInFind = True
    
        'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
        'contiene las actuaciones del grupo
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(4), "PR01DESCORTA")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), "PR16CODGRUPO", "SELECT * FROM PR1600 WHERE PR16CODGRUPO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(5)), grdDBGrid1(0).Columns(6), "PR16DESGRUPO")
    
        Call .WinRegister
        Call .WinStabilize
    End With
    
  'Call objApp.SplashOff
End Sub


Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub




Private Sub Form_Paint()
    grdDBGrid1(0).Refresh
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub


Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange

End Sub



Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


