VERSION 5.00
Begin VB.Form frmmenuinterac 
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Interacciones"
   ClientHeight    =   3195
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Frame fraFrame1 
      Caption         =   "Tipos de Interacci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5535
      Left            =   1680
      TabIndex        =   0
      Top             =   840
      Width           =   8895
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   4
         Left            =   720
         TabIndex        =   5
         Top             =   4440
         Width           =   2175
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Grupo - Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   720
         TabIndex        =   4
         Top             =   3120
         Width           =   2175
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Grupo - Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   720
         TabIndex        =   3
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Actuaci�n - Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   720
         TabIndex        =   2
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CommandButton cmdCommand1 
         Caption         =   "Actuaci�n - Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   720
         TabIndex        =   1
         Top             =   960
         Width           =   2175
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   3120
         TabIndex        =   10
         Top             =   4560
         Width           =   1575
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Interacci�n entre Grupos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   3120
         TabIndex        =   9
         Top             =   3240
         Width           =   3255
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Interacci�n entre Grupo - Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   3120
         TabIndex        =   8
         Top             =   2520
         Width           =   4575
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Interacci�n entre Actuaci�n - Grupo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   3120
         TabIndex        =   7
         Top             =   1800
         Width           =   4695
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Interacci�n entre Actuaciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   3120
         TabIndex        =   6
         Top             =   1080
         Width           =   3855
      End
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Actuaci�n-Actuaci�n"
         Index           =   10
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "A&ctuaci�n-Grupo"
         Index           =   20
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "G&rupo-Actuaci�n"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Grupo-Grupo"
         Index           =   40
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   60
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda   F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de..."
         Index           =   20
      End
   End
End
Attribute VB_Name = "frmmenuinterac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS C.U.N.                                             *
'* NOMBRE: PR00114.FRM                                                  *
'* AUTOR: JAVIER OSTOLAZA LASA                                          *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: men� de tipos de interacci�n                            *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Private Sub cmdCommand1_Click(intIndex As Integer)
    cmdCommand1(intIndex).Enabled = False
    Select Case intIndex
        Case 0
          'Load frminteractact
          'frminteractact!tabTab1(0).Tab = 0 'para mostrar el detalle del Tab
          'Call frminteractact.Show(vbModal)
          Call objsecurity.LaunchProcess("PR0115")
          'Unload frminteractact
          'Set frminteractact = Nothing
        Case 1
          'Load frminteractgrp
          'frminteractgrp!tabTab1(0).Tab = 0
          'Call frminteractgrp.Show(vbModal)
          Call objsecurity.LaunchProcess("PR0116")
          'Unload frminteractgrp
          'Set frminteractgrp = Nothing
        Case 2
          'Load frmintergrpact
          'frmintergrpact!tabTab1(0).Tab = 0
          'Call frmintergrpact.Show(vbModal)
          Call objsecurity.LaunchProcess("PR0117")
          'Unload frmintergrpact
          'Set frmintergrpact = Nothing
        Case 3
          'Load frmintergrpgrp
          'frmintergrpgrp!tabTab1(0).Tab = 0
          'Call frmintergrpgrp.Show(vbModal)
          Call objsecurity.LaunchProcess("PR0118")
          'Unload frmintergrpgrp
          'Set frmintergrpgrp = Nothing
        Case 4
          Unload Me
    End Select
    cmdCommand1(intIndex).Enabled = True
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
      Call objApp.HelpContext
    Case 20
      Call objApp.About
  End Select
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Select Case intIndex
    Case 10
        'Load frminteractact
        'frminteractact!tabTab1(0).Tab = 0 'para mostrar el detalle del Tab
        'Call frminteractact.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0115")
        'Unload frminteractact
        'Set frminteractact = Nothing
    Case 20
        'Load frminteractgrp
        'frminteractgrp!tabTab1(0).Tab = 0
        'Call frminteractgrp.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0116")
        'Unload frminteractgrp
        'Set frminteractgrp = Nothing
    Case 30
        'Load frmintergrpact
        'frmintergrpact!tabTab1(0).Tab = 0
        'Call frmintergrpact.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0117")
        'Unload frmintergrpact
        'Set frmintergrpact = Nothing
    Case 40
        'Load frmintergrpgrp
        'frmintergrpgrp!tabTab1(0).Tab = 0
        'Call frmintergrpgrp.Show(vbModal)
        Call objsecurity.LaunchProcess("PR0118")
        'Unload frmintergrpgrp
        'Set frmintergrpgrp = Nothing
    Case 60
      Unload Me
  End Select
End Sub
