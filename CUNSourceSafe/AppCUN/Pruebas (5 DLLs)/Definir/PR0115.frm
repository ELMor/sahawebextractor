VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frminteractact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Interacciones. Actuaci�n - Actuaci�n"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Interacci�n Actuaci�n - Actuaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6855
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Top             =   720
      Width           =   10845
      Begin TabDlg.SSTab tabTab1 
         Height          =   6060
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   480
         Width           =   10215
         _ExtentX        =   18018
         _ExtentY        =   10689
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0115.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(4)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(6)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(5)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(9)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(7)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "cboSSDBCombo1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(2)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(3)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(4)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(5)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(6)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(8)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtminuto"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txthora"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtdia"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).ControlCount=   23
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0115.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtdia 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   720
            MaxLength       =   4
            TabIndex        =   5
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txthora 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   2040
            MaxLength       =   2
            TabIndex        =   6
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txtminuto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            Height          =   330
            Left            =   3480
            MaxLength       =   2
            TabIndex        =   7
            Top             =   4320
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   8
            Left            =   4200
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n Origen"
            Top             =   720
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   7
            Left            =   3360
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n Origen"
            Top             =   1920
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   3360
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Descripci�n Actuaci�n Destino"
            Top             =   3120
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR18NUMTIEMPOINT"
            Height          =   330
            Index           =   5
            Left            =   720
            TabIndex        =   8
            Tag             =   "Tiempo|Tiempo de la Interacci�n"
            Top             =   5040
            Visible         =   0   'False
            Width           =   852
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR16CODGRUPO_DES"
            Height          =   330
            Index           =   4
            Left            =   7200
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "C�d.Grp_Dest.|C�digo del Grupo Destino"
            Top             =   4800
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            DataField       =   "PR16CODGRUPO"
            Height          =   330
            Index           =   3
            Left            =   7200
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "C�d.Grp_Orig.|C�digo del Grupo Origen"
            Top             =   4320
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR01CODACTUACION_DES"
            Height          =   330
            Index           =   2
            Left            =   720
            TabIndex        =   4
            Tag             =   "C�d.Act_Dest.|C�digo de la Actuaci�n Destino"
            Top             =   3120
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   1
            Left            =   720
            TabIndex        =   3
            Tag             =   "C�d.Act_Orig.|C�digo de la Actuaci�n Origen"
            Top             =   1920
            Width           =   1092
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR18NUMDEFINTER"
            Height          =   330
            Index           =   0
            Left            =   720
            TabIndex        =   1
            Tag             =   "N�mero|N�mero de Interacci�n"
            Top             =   720
            Width           =   852
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5625
            Index           =   0
            Left            =   -74760
            TabIndex        =   16
            Top             =   240
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            RowHeight       =   423
            Columns(0).Width=   3200
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   9922
            _StockProps     =   79
            Caption         =   "INTERACCIONES ACTUACI�N - ACTUACI�N"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR19CODTIPINTERAC"
            Height          =   330
            Index           =   0
            Left            =   3360
            TabIndex        =   2
            Tag             =   "Tipo|Tipo de Interacci�n"
            Top             =   720
            Width           =   750
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1402
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3519
            Columns(1).Caption=   "Descripci�n"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1323
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "d�as"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1440
            TabIndex        =   27
            Top             =   4440
            Width           =   390
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "horas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   2760
            TabIndex        =   26
            Top             =   4440
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "minutos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   4200
            TabIndex        =   25
            Top             =   4440
            Width           =   660
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Actuaci�n Origen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   3360
            TabIndex        =   23
            Top             =   1680
            Width           =   2550
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n Actuaci�n Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   3360
            TabIndex        =   22
            Top             =   2880
            Width           =   2640
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Actuaci�n Destino"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   720
            TabIndex        =   21
            Top             =   2880
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Actuaci�n Origen"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   720
            TabIndex        =   20
            Top             =   1680
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tiempo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   720
            TabIndex        =   19
            Top             =   4080
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Tipo Interacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   3360
            TabIndex        =   18
            Top             =   480
            Width           =   1575
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�mero de Interacci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   720
            TabIndex        =   17
            Top             =   480
            Width           =   2055
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   14
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Alta Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frminteractact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS C.U.N.                                             *
'* NOMBRE: PR00115.FRM                                                  *
'* AUTOR: JAVIER OSTOLAZA LASA                                          *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: permite definir interacciones entre dos actuaciones     *
'*              diferentes                                              *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim tipo As Variant
Dim tiempo As Variant
Dim origen As Variant
Dim destino As Variant
'intabrir controla si se ha pulsado Abrir Registro
Dim intabrir As Integer
'intnuevo controla si se ha pulsado Nuevo
Dim intnuevo As Integer
Dim blnAltaMasiva As Boolean
Dim TiempoTotal As Variant
'intcambioalgo detecta si se cambia algo: si se pulsa una tecla en una caja de texto,
'o se cambia la combo
Dim intcambioalgo As Integer
Private Function CrearFiltroInforme(strEntrada As String, strPrefijo As String, strBuscar As String)
'Toma como entrada una cadena de caracteres (strEntrada). En esta cadena de entrada busca la
'palabra (strBuscar) y cuando la encuentra le a�ade el prefijo (strPrefijo)

Dim strSalida As String
Dim strPalabra1 As String

    Do While Len(strEntrada) > 0
       strPalabra1 = Left(strEntrada, Len(strBuscar))
       If strPalabra1 = strBuscar Then
          strSalida = strSalida & strPrefijo & strPalabra1
          strEntrada = Right(strEntrada, Len(strEntrada) - Len(strBuscar))
       Else
          strSalida = strSalida & Left(strEntrada, 1)
          strEntrada = Right(strEntrada, Len(strEntrada) - 1)
       End If
    Loop
    CrearFiltroInforme = strSalida
End Function



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Activate()
  origen = txtText1(1)
  destino = txtText1(2)
  tiempo = txtText1(5)
  tipo = cboSSDBCombo1(0).Text
  blnAltaMasiva = False
End Sub


Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  Dim i As Integer
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Interacci�n Actuaci�n-Actuaci�n"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR1800" 'Definici�n de Interacciones
    .strWhere = "(PR01CODACTUACION is NOT NULL) and (PR01CODACTUACION_DES is NOT NULL) "
    
    '.blnMasive = False
    
    Call .FormAddOrderField("PR18NUMDEFINTER", cwAscending)
    
    .blnHasMaint = True
    
    Call .objPrinter.Add("PR1151", "Listado de Interacciones Actuaci�n-Actuaci�n")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Interacci�n Actuaci�n-Actuaci�n")
    Call .FormAddFilterWhere(strKey, "PR18NUMDEFINTER", "N�mero", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "Actuaci�n Origen", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR01CODACTUACION_DES", "Actuaci�n Destino", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR18NUMTIEMPOINT", "Tiempo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR19CODTIPINTERAC", "Tipo", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "PR18NUMDEFINTER", "N�mero")
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    
    'tratamiento especial para los campos de grupo
    .CtrlGetInfo(txtText1(3)).blnInGrid = False
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(1)).blnMandatory = True
    .CtrlGetInfo(txtText1(2)).blnMandatory = True
    .CtrlGetInfo(txtText1(5)).blnMandatory = True
    .CtrlGetInfo(cboSSDBCombo1(0)).blnMandatory = True
    .CtrlGetInfo(txtText1(3)).blnMandatory = False
    .CtrlGetInfo(txtText1(4)).blnMandatory = False
    
    .CtrlGetInfo(txtdia).blnNegotiated = False
    .CtrlGetInfo(txthora).blnNegotiated = False
    .CtrlGetInfo(txtminuto).blnNegotiated = False
    
    'Tipos de Interacci�n
    .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR19CODTIPINTERAC, PR19DESTIPINTERAC FROM PR1900"
    
    'Creamos las busquedas por el Search
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(7), "PR01DESCORTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(6), "PR01DESCORTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR19CODTIPINTERAC", "SELECT * FROM PR1900 WHERE PR19CODTIPINTERAC=?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtText1(8), "PR19DESTIPINTERAC")
    
    Call .WinRegister
    Call .WinStabilize
    
  End With

  'tratamiento especial para los campos de grupo
  txtText1(3) = ""
  txtText1(4) = ""
  txtText1(3).Visible = False
  txtText1(4).Visible = False

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

  'NOTA: el objeto que contiene la base de datos abierta (el Database de VB)
  '      es aqu� el objeto objApp.rdoConnect

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'inserto los valores en la tabla PR2000
Dim sqlstr As String

intcambioalgo = 0

  If Not blnError Then
    If objWinInfo.intWinStatus = cwModeSingleAddRest Then   'NUEVO
     ' insertar en la tabla interna de interacciones
      sqlstr = "INSERT INTO PR2000 " _
                  & "(PR01CODACTUACION,PR01CODACTUACION_DES,PR19CODTIPINTERAC," _
                  & "PR20NUMTIEMPOINT,PR18NUMDEFINTER) " _
                  & "VALUES (" _
                  & txtText1(1).Text & "," _
                  & txtText1(2).Text & "," _
                  & cboSSDBCombo1(0).Text & "," _
                  & txtText1(5).Text & "," _
                  & txtText1(0).Text & ")"
      On Error GoTo Err_Ejecutar
      objApp.rdoConnect.Execute sqlstr, 64
      objApp.rdoConnect.Execute "Commit", 64 'para asegurarnos
    ElseIf objWinInfo.intWinStatus = cwModeSingleEdit Then  'ACTUALIZAR
      sqlstr = "UPDATE PR2000 " _
             & "SET pr01codactuacion = " & txtText1(1).Text & "," _
             & "pr01codactuacion_des = " & txtText1(2).Text & "," _
             & "pr19codtipinterac = " & cboSSDBCombo1(0).Text & "," _
             & "pr20numtiempoint = " & txtText1(5).Text & " " _
             & "WHERE pr18numdefinter = " & txtText1(0).Text
      On Error GoTo Err_Ejecutar
      objApp.rdoConnect.Execute sqlstr, 64
      objApp.rdoConnect.Execute "Commit", 64
    End If
  End If
  Exit Sub
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub



Private Sub objWinInfo_cwPreDelete(ByVal strFormName As String, ByRef blnCancel As Boolean)
'borro la entrada de la tabla PR2000
  Dim sqlstr As String
  sqlstr = "DELETE FROM PR2000 " _
           & "WHERE (pr18numdefinter = " & txtText1(0) & ")"
  On Error GoTo Err_Ejecutar
  objApp.rdoConnect.Execute sqlstr, 64
  objApp.rdoConnect.Execute "Commit", 64
  Exit Sub
Err_Ejecutar:
  blnCancel = True
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strFiltro As String
  Dim strOrden As String
    
  If strFormName = "Interacci�n Actuaci�n-Actuaci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      
      If blnHasFilter = False Then
          strFiltro = CrearFiltroInforme(objWinInfo.DataGetWhere(blnHasFilter), "PR2001J.", "PR01CODACTUACION")
      Else
          strFiltro = CrearFiltroInforme(objWinInfo.DataGetWhere(blnHasFilter), "PR2001J.", "PR01CODACTUACION")
          strFiltro = CrearFiltroInforme(strFiltro, "PR2001J.", "PR18NUMDEFINTER")
          'strFiltro = CrearFiltroInforme(strFiltro, "PR2001J.", "PR01CODACTUACION")
          strFiltro = CrearFiltroInforme(strFiltro, "PR2001J.", "PR01CODACTUCION_DES")
          strFiltro = CrearFiltroInforme(strFiltro, "PR2001J.", "PR18NUMTIEMPOINT")
      End If
      
      strOrden = CrearFiltroInforme(objWinInfo.DataGetOrder(blnHasFilter, True), "PR2001J.", "PR18NUMDEFINTER")
      
      Call objPrinter.ShowReport(strFiltro, strOrden)
      
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Interacci�n Actuaci�n-Actuaci�n" Then
    Set objSearch = New clsCWSearch
    With objSearch
      .strTable = "PR0101J"
      .strOrder = "ORDER BY PR01CODACTUACION ASC"
      
      Set objField = .AddField("PR01CODACTUACION")
      objField.strSmallDesc = "C�d. Actuaci�n"
      
      Set objField = .AddField("PR01DESCORTA")
      objField.strSmallDesc = "Actuaci�n"
            
      If .Search Then
        If (strCtrl = "txtText1(1)") Then
          'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(1)), .cllValues("PR01CODACTUACION"))
          Call objWinInfo.CtrlSet(txtText1(1), .cllValues("PR01CODACTUACION"))
          'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(7)), .cllValues("PR01DESCORTA"))
          Call objWinInfo.CtrlSet(txtText1(7), .cllValues("PR01DESCORTA"))
        Else
          If (strCtrl = "txtText1(2)") Then
            'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("PR01CODACTUACION"))
            Call objWinInfo.CtrlSet(txtText1(2), .cllValues("PR01CODACTUACION"))
            'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(6)), .cllValues("PR01DESCORTA"))
            Call objWinInfo.CtrlSet(txtText1(6), .cllValues("PR01DESCORTA"))
          End If
        End If
      End If
    End With
    Set objSearch = Nothing
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim mensaje As String
Dim rsta As rdoResultset
Dim sqlstr As String

'*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" Then
  'se mira si la interacci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  Set rstTiempo = Nothing
End If
'*********************************************************************

If (btnButton.Index = 4) And (mnuOpcionesOpcion.item(50).Checked = True) Then
  blnAltaMasiva = True
End If

If btnButton.Index = 3 Then
    intabrir = 1
End If
If btnButton.Index = 2 Then
    intnuevo = 1
End If
If (btnButton.Index = 4) And (intnuevo = 1) Then
    intnuevo = 0
    tipo = cboSSDBCombo1(0).Value
    tiempo = txtText1(5)
    origen = txtText1(1)
    destino = txtText1(2)
End If

  'bot�n GRABAR
  If btnButton.Index = 4 Then
    If btnButton.Index = 4 And txtText1(1) <> "" And txtText1(2) <> "" And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Una actuaci�n no puede interaccionar consigo misma.", vbOKOnly + vbExclamation, _
         "Interacci�n Actuaci�n - Actuaci�n")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    Exit Sub
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(1) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. " _
           & "Elija otra, por favor.", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
             & "La actuaci�n " & txtText1(2) & " no existe " _
             & "o no tiene asociado ning�n departamento realizador. " _
           & "Elija otra, por favor.", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
    
    ' Se controla que no se guarde una interacci�n entre dos actuaciones
    ' entre las que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR2000 " _
          & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe interacci�n entre las dos actuaciones
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
            objWinInfo.objWinActiveForm.blnChanged = False
            rsta.Close
            Set rsta = Nothing
            Exit Sub
        End If
      End If
      Else 'la interacci�n est� guardada
       sqlstr = "SELECT pr19codtipinterac,pr20numtiempoint,pr01codactuacion," _
                & "pr01codactuacion_des FROM PR2000 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
        txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de actuaci�n no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            rsta.Close
            Set rsta = Nothing
            If btnButton.Index = 30 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Exit Sub
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
                objWinInfo.objWinActiveForm.blnChanged = False
                rsta.Close
                Set rsta = Nothing
               Exit Sub
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
  
  If (btnButton.Index <> 6 And btnButton.Index <> 8 And btnButton.Index <> 4) Then
    objWinInfo.objWinActiveForm.blnChanged = False
    If intcambioalgo = 1 Then
       objWinInfo.objWinActiveForm.blnChanged = True
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  End If
  

  '****************************************
  If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  '******************************************
  If txtText1(0).Text <> "" Then
  'bot�ndistinto de grabar
  If btnButton.Index <> 4 Then
    If btnButton.Index = 4 And txtText1(1) <> "" And txtText1(2) <> "" And _
                            (txtText1(1) = txtText1(2)) Then
    mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
        & "Una actuaci�n no puede interaccionar consigo misma.", vbOKOnly + vbExclamation, _
         "Interacci�n Actuaci�n - Actuaci�n")
    txtText1(2).SetFocus
    objWinInfo.objWinActiveForm.blnChanged = False
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(1) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador.", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
             & "La actuaci�n " & txtText1(2) & " no existe " _
             & "o no tiene asociado ning�n departamento realizador. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    
    ' Se controla que no se guarde una interacci�n entre dos actuaciones
    ' entre las que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR2000 " _
          & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe interacci�n entre las dos actuaciones
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
       sqlstr = "SELECT pr19codtipinterac,pr20numtiempoint,pr01codactuacion," _
                & "pr01codactuacion_des FROM PR2000 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
        txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de actuaci�n no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            If btnButton.Index = 30 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
End If
'*****************************************
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  If btnButton.Index = 30 Then
    Exit Sub
  End If
  'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If btnButton.Index = 16 Then
    Call txtText1_Change(5)
  End If
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  'bot�n NUEVO
  If btnButton.Index = 2 Then
    'ponemos autom�ticamente el c�digo (PK es sequence)
    On Error GoTo Err_Ejecutar
    sqlstr = "SELECT PR18NUMDEFINTER_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    txtText1(0).Locked = True
    ' se activa el bot�n Guardar
    txtText1(1).SetFocus
    txtText1(0).SetFocus
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
  End If

 
  Exit Sub


Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim mensaje As String
  
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

'**********************************************************************
Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And _
    txtText1(2).Text <> "" And intIndex = 40 Then
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  Set rstTiempo = Nothing
End If
'*********************************************************************
  
  If (intIndex = 40) And (mnuOpcionesOpcion.item(50).Checked = True) Then
    blnAltaMasiva = True
  End If

  If intIndex = 20 Then
    intabrir = 1
  End If
  If intIndex = 10 Then
    intnuevo = 1
  End If
  If (intIndex = 40) And (intnuevo = 1) Then
    intnuevo = 0
    tipo = cboSSDBCombo1(0).Value
    tiempo = txtText1(5)
    origen = txtText1(1)
    destino = txtText1(2)
  End If
  
  'bot�n GRABAR
  If (intIndex = 40) Then
    'si quiere grabar orig. y dest. iguales evitamos que grabe
    If txtText1(1) <> "" And txtText1(2) <> "" And _
                              (txtText1(1) = txtText1(2)) Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
          & "Una actuaci�n no puede interaccionar consigo misma.", vbOKOnly + vbExclamation, _
          "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      Exit Sub
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(1) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. " _
           & "Elija otra, por favor. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
    If txtText1(2) <> "" Then
    ' Actuaciones del departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(2) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. " _
           & "Elija otra, por favor. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      rsta.Close
      Set rsta = Nothing
      Exit Sub
    End If
    End If
     ' Se controla que no se guarde una interacci�n entre dos actuaciones
    ' entre las que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR2000 " _
           & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe interacci�n entre las dos actuaciones
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
            objWinInfo.objWinActiveForm.blnChanged = False
            rsta.Close
            Set rsta = Nothing
            Exit Sub
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr20numtiempoint,pr01codactuacion," _
                & "pr01codactuacion_des FROM PR2000 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de actuaci�n no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
            rsta.Close
            Set rsta = Nothing
            If intIndex = 100 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Exit Sub
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
                objWinInfo.objWinActiveForm.blnChanged = False
                rsta.Close
                Set rsta = Nothing
               Exit Sub
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If

' control si la opci�n es distinta de Eliminar y De Imprimir
If (intIndex <> 40 And intIndex <> 60 And intIndex <> 80) Then
     objWinInfo.objWinActiveForm.blnChanged = False
      If (txtText1(0) <> "") Then
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  End If
  End If
  
  
  
  '****************************************
  If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  '******************************************
  If txtText1(0).Text <> "" Then
  'bot�n distinto de grabar
  If (intIndex <> 40) Then
    'si quiere grabar orig. y dest. iguales evitamos que grabe
    If txtText1(1) <> "" And txtText1(2) <> "" And _
                              (txtText1(1) = txtText1(2)) Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
          & "Una actuaci�n no puede interaccionar consigo misma.", vbOKOnly + vbExclamation, _
          "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(1) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    ' Actuaciones del departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(2) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
     ' Se controla que no se guarde una interacci�n entre dos actuaciones
    ' entre las que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR2000 " _
           & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe interacci�n entre las dos actuaciones
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr20numtiempoint,pr01codactuacion," _
                & "pr01codactuacion_des FROM PR2000 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de actuaci�n no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
            If intIndex = 100 Then
              Exit Sub
            End If
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
End If
  '***************************************
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  If intIndex = 100 Then
    Exit Sub
  End If
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  'NUEVO
  If (intIndex = 10) Then
    'ponemos autom�ticamente el c�digo (PK es sequence)
    On Error GoTo Err_Ejecutar
    sqlstr = "SELECT PR18NUMDEFINTER_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
    txtText1(0).Locked = True
    ' se activa el bot�n Guardar
    txtText1(1).SetFocus
    txtText1(0).SetFocus
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
  End If
  
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim mensaje As String

'*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" Then
  'se mira si la interacci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  Set rstTiempo = Nothing
End If
'*********************************************************************

  
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  
  ' control al pulsar Filtro
  If (intIndex = 10) Then
  objWinInfo.objWinActiveForm.blnChanged = False
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  End If
  
  If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
 
  '******************************************
  If txtText1(0).Text <> "" Then
    'si quiere grabar orig. y dest. iguales evitamos que grabe
    If txtText1(1) <> "" And txtText1(2) <> "" And _
                              (txtText1(1) = txtText1(2)) Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
          & "Una actuaci�n no puede interaccionar consigo misma.", vbOKOnly + vbExclamation, _
          "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(1) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    ' Actuaciones del departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(2) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
     ' Se controla que no se guarde una interacci�n entre dos actuaciones
    ' entre las que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR2000 " _
           & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe interacci�n entre las dos actuaciones
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr20numtiempoint,pr01codactuacion," _
                & "pr01codactuacion_des FROM PR2000 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de actuaci�n no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If
  '***************************************
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim mensaje As String
  
'*************************************************************************
Dim rstTiempo As rdoResultset
Dim strTiempo As String
Dim detectarcambios As Integer

Call Calcular_Tiempo(txtdia.Text, txthora.Text, txtminuto.Text)
If txtText1(0).Text <> "" And txtText1(1).Text <> "" And _
    txtText1(2).Text <> "" Then
  'se mira si la interacci�n est� guardada para que al movernos por los botones
  'o men�s detecte los cambios o no
  strTiempo = "SELECT count(*) FROM PR1800 where pr18numdefinter=" & txtText1(0).Text & _
         " AND pr01codactuacion=" & txtText1(1).Text & _
         " AND pr01codactuacion_des=" & txtText1(2).Text & _
         " AND pr18numtiempoint=" & TiempoTotal
  Set rstTiempo = objApp.rdoConnect.OpenResultset(strTiempo)
  If (rstTiempo.rdoColumns(0).Value) = 0 Then
    detectarcambios = 0
    Call objWinInfo.CtrlSet(txtText1(5), TiempoTotal)
    objWinInfo.objWinActiveForm.blnChanged = True
    detectarcambios = 1
  'Else
  '  objWinInfo.objWinActiveForm.blnChanged = False
  End If
  Set rstTiempo = Nothing
End If
'*********************************************************************
  
  tipo = cboSSDBCombo1(0).Value
  tiempo = txtText1(5)
  origen = txtText1(1)
  destino = txtText1(2)
  
   objWinInfo.objWinActiveForm.blnChanged = False
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
    If txtText1(2) <> "" Then
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    rsta.Close
    Set rsta = Nothing
    End If
  
   If detectarcambios = 1 Then
    detectarcambios = 0
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
  If intcambioalgo = 1 Then
    objWinInfo.objWinActiveForm.blnChanged = True
  End If
    
  '******************************************
  If txtText1(0).Text <> "" Then
  'si quiere grabar orig. y dest. iguales evitamos que grabe
    If txtText1(1) <> "" And txtText1(2) <> "" And _
                              (txtText1(1) = txtText1(2)) Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
          & "Una actuaci�n no puede interaccionar consigo misma.", vbOKOnly + vbExclamation, _
          "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    '  Para la integridad referencial se debe comprobar que los c�digos de actuaci�n
    'tecleados est�n en la tabla PR0200 (donde est�n las actuaciones con departamento
    'realizador, es decir, las utilizables) y no en la tabla de actuaciones PR0100 (donde
    'todas las actuaciones, incluidas las que est�n en fase de definici�n).
    If txtText1(1) <> "" Then
    ' Actuaciones del Departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(1)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n origen
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(1) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(1).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
    If txtText1(2) <> "" Then
    ' Actuaciones del departamento
    sqlstr = "SELECT * " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & txtText1(2)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    'actuaci�n destino
    If rsta.EOF Then
      mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
           & "La actuaci�n " & txtText1(2) & " no existe " _
           & "o no tiene asociado ning�n departamento realizador. ", vbCritical, _
           "Interacci�n Actuaci�n - Actuaci�n")
      txtText1(2).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
    End If
    End If
     ' Se controla que no se guarde una interacci�n entre dos actuaciones
    ' entre las que ya existe una interacci�n
    sqlstr = "SELECT * " _
           & "FROM PR2000 " _
           & "WHERE pr18numdefinter=" & txtText1(0)
    On Error GoTo Err_Ejecutar
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    ' la interacci�n no est� guardada
    If (rsta.EOF) Then
      If (txtText1(1) <> "" And txtText1(2) <> "") Then
        sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
        On Error GoTo Err_Ejecutar
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        ' ya existe interacci�n entre las dos actuaciones
        If Not rsta.EOF Then
           mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
            objWinInfo.objWinActiveForm.blnChanged = False
        End If
      End If
      Else 'la interacci�n est� guardada
      sqlstr = "SELECT pr19codtipinterac,pr20numtiempoint,pr01codactuacion," _
                & "pr01codactuacion_des FROM PR2000 " _
               & " WHERE pr18numdefinter=" & txtText1(0)
       On Error GoTo Err_Ejecutar
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       tipo = rsta.rdoColumns(0).Value
       tiempo = rsta.rdoColumns(1).Value
       origen = rsta.rdoColumns(2).Value
       destino = rsta.rdoColumns(3).Value
      
       If (txtText1(1) <> "" And txtText1(2) <> "" And _
           txtText1(5) <> "" And cboSSDBCombo1(0) <> "") Then
        ' se hacen cambios en el tipo de interacci�n o en el tiempo de interacci�n y
        ' los c�digos de actuaci�n no cambian
        If (tiempo <> txtText1(5) Or tipo <> cboSSDBCombo1(0).Value) And _
             (origen = txtText1(1) And destino = txtText1(2)) Then
            Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
            tipo = cboSSDBCombo1(0).Value
            tiempo = txtText1(5)
            origen = txtText1(1)
            destino = txtText1(2)
            Else
            If (origen <> txtText1(1) Or destino <> txtText1(2)) Then
                sqlstr = "SELECT * " _
               & "FROM PR2000 " _
               & "WHERE pr01codactuacion=" & txtText1(1) _
               & " AND pr01codactuacion_des=" & txtText1(2)
               On Error GoTo Err_Ejecutar
               Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
               If Not rsta.EOF Then
                mensaje = MsgBox("La interacci�n no se guardar�." & Chr(13) _
                 & "Ya existe la interacci�n entre las actuaciones " & txtText1(1) & " y " _
                 & txtText1(2), vbCritical, _
                 "Interacci�n Actuaci�n - Actuaci�n")
                objWinInfo.objWinActiveForm.blnChanged = False
              End If
           End If
        End If
    End If
End If
rsta.Close
Set rsta = Nothing
End If

  '***************************************
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
   'como Localizar no activa los Change de las cajas de texto no actualiza bien
  'los d�as-horas-minutos por eso hay que forzar al Change de la caja oculta
  If intIndex = 10 Then
    Call txtText1_Change(5)
  End If
  Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


Private Sub txtdia_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja d�a vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtdia.Text) = False Then
    Beep
    txtdia.Text = ""
    txtdia.SetFocus
  End If
  If txtdia.Text <> "" Then
     If txtdia.Text > 6500 Then
      txtdia.Text = 6500
     End If
  End If
End Sub

Private Sub txtdia_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txthora_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja hora vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txthora.Text) = False Then
    Beep
    txthora.Text = ""
    txthora.SetFocus
  End If
  If txthora.Text <> "" Then
     If txthora.Text > 23 Then
      txthora.Text = 23
     End If
  End If
End Sub

Private Sub txthora_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtminuto_Change()
Dim rsta As rdoResultset
Dim strsql As String
Dim tipo As Integer

'al borrar en la caja minuto vac�a la combo del tipo,por eso se llena otra vez
If txtText1(0).Text <> "" Then
   strsql = "SELECT count(pr19codtipinterac) FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
   Set rsta = objApp.rdoConnect.OpenResultset(strsql)
   If rsta.rdoColumns(0).Value > 0 Then
      strsql = "SELECT pr19codtipinterac FROM PR1800 " & _
        "WHERE pr18numdefinter=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsql)
      tipo = rsta.rdoColumns(0).Value
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), tipo)
   End If
   rsta.Close
   Set rsta = Nothing
End If

  'objWinInfo.objWinActiveForm.blnChanged = True
  tlbToolbar1.Buttons(4).Enabled = True
  If IsNumeric(txtminuto.Text) = False Then
    Beep
    txtminuto.Text = ""
    txtminuto.SetFocus
  End If
  If txtminuto.Text <> "" Then
     If txtminuto.Text > 59 Then
      txtminuto.Text = 59
     End If
  End If
End Sub

Private Sub txtminuto_KeyPress(KeyAscii As Integer)
  intcambioalgo = 1
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)
  intcambioalgo = 1
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Dim mensaje As String
  Dim rsta As rdoResultset
  Dim sqlstr As String

  If intIndex = 0 Then
    intcambioalgo = 0
  End If

  If (mnuOpcionesOpcion.item(50).Checked = True) And (blnAltaMasiva = True) And (intIndex = 0) Then
    blnAltaMasiva = False
    sqlstr = "SELECT PR18NUMDEFINTER_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0) = rsta.rdoColumns(0).Value
    txtText1(0).Locked = True
    txtdia.Text = 0
    txthora.Text = 0
    txtminuto.Text = 0
   ' se activa el bot�n Guardar
    cboSSDBCombo1(0).SetFocus
    txtText1(0).SetFocus
    rsta.Close
    Set rsta = Nothing
  End If


  If intIndex = 0 Then
    origen = txtText1(1)
    destino = txtText1(2)
    tiempo = txtText1(5)
    tipo = cboSSDBCombo1(0).Value
  End If
  Call objWinInfo.CtrlDataChange
  
  '****************************************************************************
  'cuando cambie la columna invisible Tiempo que se actualice dias,horas,minutos
  If intIndex = 5 Then
    If txtText1(5).Text = "" Then
      txtdia.Text = 0
      txthora.Text = 0
      txtminuto.Text = 0
    Else
      txtdia.Text = txtText1(5).Text \ 1440                 'd�as
      txthora.Text = (txtText1(5).Text Mod 1440) \ 60       'horas
      txtminuto.Text = (txtText1(5).Text Mod 1440) Mod 60   'minutos
    End If
  End If
  '****************************************************************************
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  intcambioalgo = 1
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Calcular_Tiempo(d, h, m)
'procedimiento que transforma los d�as,horas y minutos a Minutos

If d = "" Or IsNumeric(d) = False Then
  d = 0
Else
  If d > 6500 Then
    d = 6500
  End If
End If
If h = "" Or IsNumeric(h) = False Then
  h = 0
Else
  If h > 23 Then
    h = 23
  End If
End If
If m = "" Or IsNumeric(m) = False Then
  m = 0
Else
  If m > 59 Then
    m = 59
  End If
End If

TiempoTotal = (d * 1440) + (h * 60) + m
  
End Sub

