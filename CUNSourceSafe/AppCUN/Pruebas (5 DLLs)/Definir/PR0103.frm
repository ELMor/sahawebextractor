VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Begin VB.Form frmcuestionariomuestra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaci�n. Muestras.Cuestionario"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0103.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraRespuestas1 
      Caption         =   "Respuestas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   240
      TabIndex        =   7
      Tag             =   "Respuestas del Cuestionario"
      Top             =   5520
      Width           =   11415
      Begin SSDataWidgets_B.SSDBGrid Grid1 
         Height          =   2055
         Index           =   1
         Left            =   240
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   360
         Width           =   11055
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   3
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns.Count   =   3
         Columns(0).Width=   2381
         Columns(0).Caption=   "Lista Respuestas"
         Columns(0).Name =   "Lista Respuestas"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   1984
         Columns(1).Caption=   "N� Respuesta"
         Columns(1).Name =   "N� Respuesta"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   13732
         Columns(2).Caption=   "Respuesta"
         Columns(2).Name =   "Respuesta"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         _ExtentX        =   19500
         _ExtentY        =   3625
         _StockProps     =   79
         Caption         =   "RESPUESTAS"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Cuestionario"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2865
      Index           =   1
      Left            =   240
      TabIndex        =   0
      Tag             =   "Preguntas del Cuestionario"
      Top             =   2640
      Width           =   11385
      Begin TabDlg.SSTab tabTab1 
         Height          =   2385
         Index           =   0
         Left            =   240
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   4207
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0103.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lbllabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lbllabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboSSDBCombo1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "chkCheck1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtTipResp"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtListResp"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(3)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(5)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "chkCheck1(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0103.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkCheck1 
            Caption         =   "�Pregunta Agrupable?"
            DataField       =   "PR26INDAGRUPA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1560
            TabIndex        =   32
            Tag             =   "�Pregunta Agrupable?"
            Top             =   360
            Width           =   2295
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR46CODLISTRESP"
            Height          =   330
            Index           =   5
            Left            =   240
            TabIndex        =   6
            Tag             =   "Lista de Respuestas|C�digo de la Lista de Respuestas"
            Top             =   1800
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR25TIEMPO"
            Height          =   285
            Index           =   3
            Left            =   6960
            TabIndex        =   29
            Tag             =   "C�digo actuaci�n"
            Top             =   720
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.TextBox txtListResp 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Left            =   1560
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Descripci�n de la Lista de Respuestas"
            Top             =   1800
            Width           =   5400
         End
         Begin VB.TextBox txtTipResp 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Left            =   4920
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Descripci�n del Tipo de Respuesta"
            Top             =   360
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR01CODACTUACION"
            Height          =   285
            Index           =   0
            Left            =   5040
            TabIndex        =   28
            Tag             =   "C�digo actuaci�n"
            Top             =   720
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "PR24CODMUESTRA"
            Height          =   285
            Index           =   4
            Left            =   3120
            TabIndex        =   27
            Tag             =   "C�digo Muestra"
            Top             =   720
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Respuesta Obligatoria (si/no)"
            DataField       =   "PR26INDROBLIG"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   7320
            TabIndex        =   9
            Tag             =   "� La respuesta es Obligatoria ?"
            Top             =   1800
            Width           =   2895
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFF00&
            Height          =   330
            Index           =   1
            Left            =   240
            Locked          =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   5
            Tag             =   "Texto de la Pregunta"
            Top             =   1080
            Width           =   9000
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "PR40CODPREGUNTA"
            Height          =   330
            Index           =   2
            Left            =   240
            TabIndex        =   2
            Tag             =   "C�digo de la Pregunta"
            Top             =   360
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2160
            Index           =   0
            Left            =   -74760
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   120
            Width           =   10335
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   18230
            _ExtentY        =   3810
            _StockProps     =   79
            Caption         =   "CUESTIONARIO"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR27CODTIPRESPU"
            Height          =   330
            Index           =   0
            Left            =   4080
            TabIndex        =   3
            Tag             =   "Tipo de la Respuesta"
            Top             =   360
            Width           =   675
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Caption=   "N�MERO"
            Columns(0).Name =   "N�MERO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "DESCRIPCI�N"
            Columns(1).Name =   "DESCRIPCI�N"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1199
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16776960
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Tipo de Respuesta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   4080
            TabIndex        =   18
            Top             =   120
            Width           =   1695
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Lista de Respuestas"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   240
            TabIndex        =   17
            Top             =   1560
            Width           =   1935
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Texto de la Pregunta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   16
            Top             =   840
            Width           =   2055
         End
         Begin VB.Label lbllabel1 
            Caption         =   "C�d Pregunta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   15
            Top             =   120
            Width           =   1215
         End
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Muestra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2085
      Index           =   0
      Left            =   240
      TabIndex        =   26
      Tag             =   "Actuaci�n-Muestra"
      Top             =   480
      Width           =   11385
      Begin VB.TextBox txtCuestext1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   4
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   30
         TabStop         =   0   'False
         Tag             =   "Tiempo en minutos respecto a la primera"
         ToolTipText     =   "Tiempo en minutos respecto a la primera"
         Top             =   1680
         Width           =   1092
      End
      Begin VB.TextBox txtCuestext1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   3
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         ToolTipText     =   "Descripci�n de la Actuaci�n"
         Top             =   1080
         Width           =   5400
      End
      Begin VB.TextBox txtCuestext1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   2
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Tag             =   "C�digo de la Actuaci�n"
         ToolTipText     =   "C�digo de la Actuaci�n"
         Top             =   1080
         Width           =   1092
      End
      Begin VB.TextBox txtCuestext1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   0
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Tag             =   "C�digo de la Muestra"
         ToolTipText     =   "C�digo de la Muestra"
         Top             =   480
         Width           =   612
      End
      Begin VB.TextBox txtCuestext1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   1
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Muestra"
         ToolTipText     =   "Descripci�n de la Muestra"
         Top             =   480
         Width           =   5400
      End
      Begin VB.Label lblCodActu 
         Caption         =   "Tiempo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   360
         TabIndex        =   31
         Top             =   1440
         Width           =   1815
      End
      Begin VB.Label lblDesActu 
         Caption         =   "Descripci�n Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   2640
         TabIndex        =   20
         Top             =   840
         Width           =   2655
      End
      Begin VB.Label lblCodActu 
         Caption         =   "C�digo Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   7
         Left            =   360
         TabIndex        =   19
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label lbllCodMuestra 
         Caption         =   "C�digo Muestra"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   360
         TabIndex        =   14
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label lblDesMuestra 
         Caption         =   "Descripci�n Muestra"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   2640
         TabIndex        =   13
         Top             =   240
         Width           =   2655
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   11
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmcuestionariomuestra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00103.FRM                                                  *
'* AUTOR: JESUS MARIA RODILLA LARA                                      *
'* FECHA: 6 DE AGOSTO DE 1997                                           *
'* DESCRIPCION: permite definir condiciones para una nuestra, de una    *
'*              actuaci�n.                                              *
'* ARGUMENTOS:  PR01CODACTUACION, PR01DESACTUACION, PR25CODMUESTRA,     *
'*              PR25DESMUESTRA
'* ACTUALIZACIONES:                                                     *
'************************************************************************




Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim gintIndice As Integer 'Para saber que control tiene el foco para el mantenimiento asociado

Private Sub Mostrar_Respuestas(strCodLista As String)
  'Este procedimiento sirve para llenar el grid que muestra las respuestas
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim intResp As Integer

  If strCodLista <> "" Then
    'Hay que llenar el grid que contiene la respuestas
    strSelect = "SELECT PR46CODLISTRESP,PR28NUMRESPUESTA, PR28DESRESPUESTA " _
             & " FROM PR2800 " _
             & " WHERE PR46CODLISTRESP = " & strCodLista _
             & " ORDER BY PR28NUMRESPUESTA"
             
    Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
    Grid1(1).RemoveAll
    Do While Not rsta.EOF
      Grid1(1).AddNew
      Grid1(1).Columns(0).Value = rsta.rdoColumns(0).Value
      Grid1(1).Columns(1).Value = rsta.rdoColumns(1).Value
      Grid1(1).Columns(2).Value = rsta.rdoColumns(2).Value
      Grid1(1).Update
      rsta.MoveNext
    Loop
    Grid1(1).MoveFirst
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    ''objWinInfo.DataSave
    rsta.Close
    Set rsta = Nothing
  Else
    'Hay que limpiar el grid que contiene las respuestas
    Grid1(1).RemoveAll
  End If
  
End Sub


Private Sub Form_Activate()
  
  txtCuestext1(0).Text = frmmuestra.txtText1(5).Text 'C�d. de la muestra
  txtCuestext1(1).Text = frmmuestra.txtAct(2).Text 'Descripci�n de la muestra
  txtCuestext1(2).Text = frmmuestra.txtText1(2).Text 'C�d. de la actuaci�n
  txtCuestext1(3).Text = frmmuestra.txtAct(1).Text   'Descripci�n de la actuaci�n
  txtCuestext1(4).Text = frmmuestra.txtText1(0).Text   'Tiempo en minutos respecto a la primera
        
  Grid1(1).ZOrder (0)
  
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
  Else
    txtText1(5).Enabled = True
  End If
    
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm

  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    .strName = "Cuestionario_Muestra"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)

    .strTable = "PR2600"
    .strWhere = "PR24CODMUESTRA=" & frmmuestra.txtText1(5).Text & " AND PR01CODACTUACION = " & frmmuestra.txtText1(2).Text _
    & " AND PR25TIEMPO = " & frmmuestra.txtText1(0).Text
    
    Call .FormAddOrderField("PR40CODPREGUNTA", cwAscending)
        
    .blnHasMaint = True
    .blnMasive = False
    
    strKey = .strDataBase & .strTable
     
    Call .FormCreateFilterWhere(strKey, "Cuestionario de la Muestra")
    Call .FormAddFilterWhere(strKey, "PR40CODPREGUNTA", "C�digo Pregunta ", cwNumeric)
    Call .FormAddFilterWhere(strKey, "PR26INDOBLIG", "�Obligatoria?", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "PR26CODPREGUNTA", "C�digo Pregunta")
    Call .FormAddFilterOrder(strKey, "PR26INDOBLIG", "�Obligatoria?")
   
    
  End With
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
   
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    
    
    'Se establecen los buscadores
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(cboSSDBCombo1(0)).blnForeign = True
    .CtrlGetInfo(txtText1(5)).blnForeign = True
    .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR27CODTIPRESPU,PR27DESTIPRESPU FROM PR2700 order by PR27CODTIPRESPU asc"
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "PR40CODPREGUNTA", "SELECT * FROM PR4000 WHERE PR40CODPREGUNTA=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(1), "PR40DESPREGUNTA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(cboSSDBCombo1(0)), "PR27CODTIPRESPU", "SELECT PR27CODTIPRESPU, PR27DESTIPRESPU FROM PR2700 WHERE PR27CODTIPRESPU = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(cboSSDBCombo1(0)), txtTipResp, "PR27DESTIPRESPU")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "PR46CODLISTRESP", "SELECT PR46CODLISTRESP,PR46DESLISTRESP FROM PR4600 WHERE PR46CODLISTRESP = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtListResp, "PR46DESLISTRESP")
 
    Call .WinRegister
    Call .WinStabilize
  End With
  
End Sub

Private Sub lblCodActu_Click(Index As Integer)
   txtText1(2).SetFocus
End Sub

Private Sub lblDesActu_Click(Index As Integer)
   txtText1(2).SetFocus
End Sub

Private Sub lblDesMuestra_Click(Index As Integer)
   txtText1(2).SetFocus
End Sub

Private Sub lbllCodMuestra_Click(Index As Integer)
   txtText1(2).SetFocus
End Sub

Private Sub objWinInfo_cwPostChangeStatus(ByVal strFormName As String, ByVal intNewStatus As CodeWizard.cwFormStatus, ByVal intOldStatus As CodeWizard.cwFormStatus)
If intNewStatus = cwModeSingleAddRest And intOldStatus = cwModeSingleAddKey Then
    chkCheck1(1).Value = 1
End If
End Sub

Private Sub objWinInfo_cwPostDefault(ByVal strFormName As String)
    txtText1(4).Text = txtCuestext1(0).Text 'PR24CODMUESTRA
    txtText1(0).Text = txtCuestext1(2).Text 'PR01CODACTUACION
    txtText1(3).Text = txtCuestext1(4).Text 'PR25TIEMPO
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  txtText1(5).Enabled = False
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
  'cmdpreguntas.Enabled = False
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Cuestionario_Muestra" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  If gintIndice = 0 Then
    Call objsecurity.LaunchProcess("PR0207")
  Else
    If gintIndice = 1 Then
      Call objsecurity.LaunchProcess("PR0135")
    Else
      If gintIndice = 2 Then
        Call objsecurity.LaunchProcess("PR0131")
      End If
    End If
  End If
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  
  If strFormName = "Cuestionario_Muestra" And strCtrl = "cboSSDBCombo1(0)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR2700"
     .strOrder = "ORDER BY PR27CODTIPRESPU ASC"
         
     Set objField = .AddField("PR27CODTIPRESPU")
     objField.strSmallDesc = "C�digo del Tipo de Respuestas"
         
     Set objField = .AddField("PR27DESTIPRESPU")
     objField.strSmallDesc = "Descripci�n del Tipo de Respuestas"
         
     If .Search Then
      Call objWinInfo.CtrlSet(cboSSDBCombo1(0), .cllValues("PR27CODTIPRESPU"))
     End If
   End With

    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    ''objWinInfo.DataSave
    Call cboSSDBCombo1_Click(0)
   Set objSearch = Nothing
 End If
 
  If strFormName = "Cuestionario_Muestra" And strCtrl = "txtText1(5)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "PR4600"
     .strOrder = "ORDER BY PR46CODLISTRESP ASC"
         
     Set objField = .AddField("PR46CODLISTRESP")
     objField.strSmallDesc = "C�digo de la Lista de Respuestas"
         
     Set objField = .AddField("PR46DESLISTRESP")
     objField.strSmallDesc = "Descripci�n de la Lista de Respuestas"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(5), .cllValues("PR46CODLISTRESP"))
     End If
   End With

  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  ''objWinInfo.DataSave
  Call cboSSDBCombo1_Click(1)
  Set objSearch = Nothing
 End If
 
  If strFormName = "Cuestionario_Muestra" And strCtrl = "txtText1(2)" Then
     Set objSearch = New clsCWSearch
    With objSearch
  
     .strTable = "PR4000"
     .strOrder = "ORDER BY PR40CODPREGUNTA ASC"
  
     Set objField = .AddField("PR40CODPREGUNTA")
     objField.strSmallDesc = "C�digo de la Pregunta"
  
     Set objField = .AddField("PR40DESPREGUNTA")
     objField.strSmallDesc = "Descripci�n de la Pregunta "
  
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("PR40CODPREGUNTA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

If btnButton.Index = 30 Then
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  Exit Sub
End If
  
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
  If (btnButton.Index = 8) Or (btnButton.Index = 4) Then
    If mnuOpcionesOpcion.Item(50).Checked = True Then
      objWinInfo.DataNew
    End If
  End If
      
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
    txtText1(5).Text = ""
    txtListResp.Text = ""
  Else
    txtText1(5).Enabled = True
  End If
        
  If btnButton.Index = 3 Then
    'Ha pulsado el bot�n abrir
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.DataRefresh
  End If
  Call Mostrar_Respuestas(txtText1(5).Text)


End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
If intIndex = 100 Then
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  Exit Sub
End If
  
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
 
  If (intIndex = 60) Or (intIndex = 40) Then
    If mnuOpcionesOpcion.Item(50).Checked = True Then
      objWinInfo.DataNew
    End If
  End If
  
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
    txtText1(5).Text = ""
    txtListResp.Text = ""
  Else
    txtText1(5).Enabled = True
  End If
  Call Mostrar_Respuestas(txtText1(5).Text)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
  
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
    txtText1(5).Text = ""
    txtListResp.Text = ""
  Else
    txtText1(5).Enabled = True
  End If
  Call Mostrar_Respuestas(txtText1(5).Text)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
    txtText1(5).Text = ""
    txtListResp.Text = ""
  Else
    txtText1(5).Enabled = True
  End If
  Call Mostrar_Respuestas(txtText1(5).Text)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
    txtText1(5).Text = ""
    txtListResp.Text = ""
  Else
    txtText1(5).Enabled = True
  End If
  Call Mostrar_Respuestas(txtText1(5).Text)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
    txtText1(5).Text = ""
    txtListResp.Text = ""
  Else
    txtText1(5).Enabled = True
  End If
  Call Mostrar_Respuestas(txtText1(5).Text)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
  If (cboSSDBCombo1(0).Text <> "4") Then
    txtText1(5).Enabled = False
    txtText1(5).Text = ""
    txtListResp.Text = ""
  Else
    txtText1(5).Enabled = True
  End If
  Call Mostrar_Respuestas(txtText1(5).Text)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
 
    Call objWinInfo.FormChangeActive(lbllabel1(intIndex).Container, False, True)
 
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  gintIndice = intIndex
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  Call Mostrar_Respuestas(txtText1(5).Text)
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim intResp As Integer
  Dim intCodLis As Integer
  
  Call objWinInfo.CtrlDataChange
  
  
  If Index = 0 Then
     'Se est� manipulando el combo que determina el tipo de respuesta
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         txtText1(5).Text = ""
         txtText1(5).Enabled = False
         txtListResp.Text = ""
     Else
         'La respuesta ser� una lista de valores
         txtText1(5).Enabled = True
         If txtText1(5).Text = "" Then
           strSelect = "SELECT COUNT(*) FROM PR2800"
           Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
           If rsta.rdoColumns(0).Value = 0 Then
             intResp = MsgBox("No existe ninguna lista de respuestas con respuestas, Defina una", vbInformation, "Importante")
             Call objWinInfo.CtrlSet(txtText1(5), 1)
             rsta.Close
             Set rsta = Nothing
           Else
             strSelect = "select min(pr46codlistresp) from pr2800"
             Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
             intCodLis = rsta.rdoColumns(0).Value
             Call objWinInfo.CtrlSet(txtText1(5), intCodLis)
             rsta.Close
             Set rsta = Nothing
           End If
         End If
     End If
  End If
  
  If (Index = 1) Then
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         txtText1(5).Text = ""
         txtText1(5).Enabled = False
         txtListResp.Text = ""
     Else
         'La respuesta ser� una lista de valores
         txtText1(5).Enabled = True
     End If
  End If
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
  Dim strSelect As String
  Dim rsta As rdoResultset
  Dim intResp As Integer
  Dim intCodLis As Integer

    If Index = 0 Then
     'Se est� manipulando el combo que determina el tipo de respuesta
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         txtText1(5).Text = ""
         txtText1(5).Enabled = False
         txtListResp.Text = ""
     Else
         'La respuesta ser� una lista de valores
         txtText1(5).Enabled = True
         If txtText1(5).Text = "" Then
           strSelect = "SELECT COUNT(*) FROM PR2800"
           Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
           If rsta.rdoColumns(0).Value = 0 Then
             intResp = MsgBox("No existe ninguna lista de respuestas con respuestas, Defina una", vbInformation, "Importante")
             Call objWinInfo.CtrlSet(txtText1(5), 1)
             rsta.Close
             Set rsta = Nothing
           Else
             strSelect = "select min(pr46codlistresp) from pr2800"
             Set rsta = objApp.rdoConnect.OpenResultset(strSelect)
             intCodLis = rsta.rdoColumns(0).Value
             Call objWinInfo.CtrlSet(txtText1(5), intCodLis)
             rsta.Close
             Set rsta = Nothing
           End If
         End If
     End If
    End If
  
    If (Index = 1) Then
     If (cboSSDBCombo1(0).Text <> "4") Then
         'La respuesta no ser� de tipo lista
         txtText1(5).Text = ""
         txtText1(5).Enabled = False
         txtListResp.Text = ""
     Else
         'La respuesta ser� una lista de valores
         txtText1(5).Enabled = True
     End If
    End If
    Call Mostrar_Respuestas(txtText1(5).Text)

End Sub
Private Sub txtCuestext1_GotFocus(Index As Integer)
   txtText1(2).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  gintIndice = intIndex
End Sub

Private Sub txtText1_KeyPress(intIndex As Integer, intKeyAscii As Integer)
  If (intIndex = 2) Then
    cboSSDBCombo1(0).Enabled = True
  End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim rstB As rdoResultset
  Dim strsqlA As String
  Dim strsqlB As String
  Dim strPregunta As String
  Dim intCont As Integer
  Dim strMensage As String
  Dim intRespuesta As String
  
  Call objWinInfo.CtrlLostFocus
  
  intCont = 0
  If (txtText1(1).Text = "") And (intIndex = 2) And (txtText1(2).Text <> "") Then
    strsqlB = "SELECT COUNT(*) " _
             & "FROM PR4000 " _
             & "WHERE PR40CODPREGUNTA =" & txtText1(2).Text
    Set rstB = objApp.rdoConnect.OpenResultset(strsqlB)
    intCont = rstB.rdoColumns(0).Value
    rstB.Close
    Set rstB = Nothing
    If intCont > 0 Then
      strsqlA = "SELECT PR40DESPREGUNTA " _
               & "FROM PR4000 " _
               & "WHERE PR40CODPREGUNTA =" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
      txtText1(1).Text = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
    Else
      strMensage = "No existe ninguna pregunta con el c�digo " & txtText1(2).Text
      intRespuesta = MsgBox(strMensage, vbInformation, "Aviso")
      objWinInfo.objWinActiveForm.blnChanged = False
      objWinInfo.DataNew
    End If
  End If
  If intIndex = 5 And txtText1(5).Text <> "" Then
    strsqlB = "SELECT COUNT(*) " _
             & "FROM PR4600 " _
             & "WHERE PR46CODLISTRESP =" & txtText1(5).Text
    Set rstB = objApp.rdoConnect.OpenResultset(strsqlB)
    intCont = rstB.rdoColumns(0).Value
    rstB.Close
    Set rstB = Nothing
    If intCont > 0 Then
      strsqlA = "SELECT PR46DESLISTRESP " _
               & "FROM PR4600 " _
               & "WHERE PR46CODLISTRESP =" & txtText1(5).Text
      Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
      txtListResp.Text = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
    Else
      strMensage = "No existe ninguna Lista de respuestas con el c�digo " & txtText1(5).Text
      intRespuesta = MsgBox(strMensage, vbInformation, "Aviso")
      txtText1(5).Text = ""
      txtListResp.Text = ""
      txtText1(5).SetFocus
    End If
  End If

End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 5 Then
    Call Mostrar_Respuestas(txtText1(intIndex).Text)
  End If
End Sub

