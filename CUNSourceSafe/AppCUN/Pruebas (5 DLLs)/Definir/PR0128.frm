VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmdefdptos 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Departamentos Realizadores"
   ClientHeight    =   7785
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0128.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7785
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Departamentos Realizadores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5895
      Index           =   1
      Left            =   360
      TabIndex        =   3
      Top             =   2040
      Width           =   11025
      Begin TabDlg.SSTab tabTab1 
         Height          =   5220
         Index           =   1
         Left            =   240
         TabIndex        =   13
         TabStop         =   0   'False
         Tag             =   "Fecha de Fin"
         Top             =   480
         Width           =   10695
         _ExtentX        =   18865
         _ExtentY        =   9208
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0128.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(10)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(14)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(16)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(18)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(6)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(7)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcDateCombo1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "dtcDateCombo1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(2)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(3)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(4)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(0)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(1)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(5)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(6)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(7)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(8)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).ControlCount=   23
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0128.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   8
            Left            =   4440
            TabIndex        =   33
            Top             =   3960
            Width           =   3975
         End
         Begin VB.TextBox txtText1 
            DataField       =   "GC01FPCLAV_SIB"
            Height          =   330
            Index           =   7
            Left            =   2400
            TabIndex        =   32
            Top             =   3960
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            DataField       =   "GC01FCTREG_SIB"
            Height          =   330
            Index           =   6
            Left            =   720
            TabIndex        =   31
            Top             =   3960
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   5
            Left            =   4440
            TabIndex        =   26
            Top             =   3120
            Width           =   3975
         End
         Begin VB.TextBox txtText1 
            DataField       =   "GC01FPCLAV_IBS"
            Height          =   330
            Index           =   1
            Left            =   2400
            TabIndex        =   25
            Top             =   3120
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            DataField       =   "GC01FCTREG_IBS"
            Height          =   330
            Index           =   0
            Left            =   720
            TabIndex        =   24
            Top             =   3120
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   4
            Left            =   6480
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   1320
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   3
            Left            =   720
            TabIndex        =   5
            Tag             =   "Descripci�n Departamento"
            Top             =   1320
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   2
            Left            =   720
            TabIndex        =   4
            Tag             =   "C�digo Departamento"
            Top             =   600
            Width           =   372
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   1
            Left            =   -74910
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4665
            Index           =   2
            Left            =   -74880
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   240
            Width           =   10095
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17806
            _ExtentY        =   8229
            _StockProps     =   79
            Caption         =   "DEPARTAMENTOS REALIZADORES"
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   0
            Left            =   720
            TabIndex        =   6
            Tag             =   "Fecha de Inicio de Vigencia"
            Top             =   2040
            Width           =   1800
            _Version        =   65537
            _ExtentX        =   3175
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   1
            Left            =   4440
            TabIndex        =   7
            Tag             =   "Fecha de Fin de Vigencia"
            Top             =   2040
            Width           =   1800
            _Version        =   65537
            _ExtentX        =   3175
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            AutoSelect      =   0   'False
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   4440
            TabIndex        =   30
            Top             =   3720
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod.  IBM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   2400
            TabIndex        =   29
            Top             =   3720
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Clase (6,7)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   720
            TabIndex        =   28
            Top             =   3720
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Correspondencia  Nuevo Sistema -- IBM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   720
            TabIndex        =   27
            Top             =   3480
            Width           =   3855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   4440
            TabIndex        =   23
            Top             =   2880
            Width           =   2175
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Clase (6,7)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   720
            TabIndex        =   22
            Top             =   2880
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Cod.  IBM"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2400
            TabIndex        =   21
            Top             =   2880
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Correspondencia IBM - Nuevo Sistema"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   720
            TabIndex        =   20
            Top             =   2640
            Width           =   3855
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha de Inicio de Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   720
            TabIndex        =   19
            Top             =   1800
            Width           =   2535
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo del Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   720
            TabIndex        =   17
            Top             =   360
            Width           =   2160
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n del Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   720
            TabIndex        =   16
            Top             =   1080
            Width           =   2580
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha de Fin de Vigencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   4440
            TabIndex        =   15
            Top             =   1800
            Width           =   2190
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaci�n "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Index           =   2
      Left            =   360
      TabIndex        =   0
      Top             =   600
      Width           =   11055
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR01CODACTUACION"
         Height          =   330
         Index           =   0
         Left            =   360
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Tag             =   "C�digo de la Actuaci�n"
         Top             =   600
         Width           =   1092
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00C0C0C0&
         DataField       =   "PR01DESCORTA"
         Height          =   330
         Index           =   1
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   600
         Width           =   5400
      End
      Begin VB.Label lblAct1 
         Caption         =   "C�digo Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   360
         TabIndex        =   12
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label lblAct1 
         Caption         =   "Descripci�n Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   2640
         TabIndex        =   11
         Top             =   360
         Width           =   2535
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   7500
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmdefdptos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00128.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: Definici�n de los departamentos realizadores de una     *
'*              actuaci�n                                               *
'* ARGUMENTOS:  Par�metros por valor : PR01CODACTUACION y PR01DESCORTA  *                                            *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
'strmensaje guarda el string de los mensajes por pantalla
Dim strmensaje As String
'undpto sirve para controlar si la actuacion est� asociada s�lo a un departamento
'con lo cual si se quiere borrar no debe dejar
Dim undpto As Integer
'posibleborrar controla si la funcion es_posible_borrar da true o false
Dim posibleborrar As Integer



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Activate()
    ' para que se active Guardar cuando se hace Nuevo
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    txtText1(2).SetFocus
    txtText1(3).SetFocus
    txtText1(2).SetFocus
End Sub


Private Sub Form_Load()
  Dim objDetailInfo1 As New clsCWForm
  Dim strKey As String
  Dim objDetailInfo2 As New clsCWForm
  Dim strKey2 As String
  Dim SQL As String
  
  'Call objApp.SplashOn

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo2
    .strName = "Departamentos Realizadores"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(2)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR0200" 'Depatamentos Realizadores
    .strWhere = "PR01CODACTUACION=" & frmdefactuacionesCUN.txtText1(0).Text
    
    Call .FormAddOrderField("AD02CODDPTO", cwAscending)
    Call .FormAddOrderField("PR01CODACTUACION", cwAscending)
    
    Call .objPrinter.Add("PR1281", "Listado por Departamentos con sus Actuaciones")
    
    .blnHasMaint = True
 
    strKey2 = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey2, "Departamentos Realizadores")
    Call .FormAddFilterWhere(strKey2, "AD02CODDPTO", "C�digo Departamento", cwNumeric)
    
    Call .FormAddFilterOrder(strKey2, "AD02CODDPTO", "C�digo Departamento")
End With
   
   
   With objWinInfo
   
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo2)
    
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    
    .CtrlGetInfo(txtText1(4)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(3)).blnReadOnly = True
  
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(txtText1(1)).blnForeign = True
    .CtrlGetInfo(txtText1(7)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), dtcDateCombo1(0), "AD02FECINICIO")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), dtcDateCombo1(1), "AD02FECFIN")
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "AD02DESDPTO")
    SQL = "SELECT GC01FPCLAV,GC01FCDESCA FROM GC0100 WHERE  GC01FPCLAV= ? AND GC01FCTREG= ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "GC01FPCLAV", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(5), "GC01FCDESCA")
'    SQL = "SELECT GC01FPCLAV,GC01FCDESCA FROM GC0100 WHERE  GC01FPCLAV= ? AND GC01FCTREG= ?"
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(7)), "GC01FPCLAV", SQL)
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(7)), txtText1(8), "GC01FCDESCA")
    Call .WinRegister
    Call .WinStabilize
  End With
 
  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
Dim sqlstr1 As String
Dim rstA1 As rdoResultset
Dim sqlstr2 As String
Dim rstA2 As rdoResultset

        intCancel = objWinInfo.WinExit
        ' mira si la actuaci�n tiene alg�n departamento realizador
        sqlstr1 = "SELECT count(*) " _
           & "FROM PR0200 " _
           & "WHERE pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0)
       Set rstA1 = objApp.rdoConnect.OpenResultset(sqlstr1)
       'mira si la actuacion tiene alguna fase
       sqlstr2 = "SELECT count(*) " _
           & "FROM PR0500 " _
           & "WHERE pr01codactuacion = " & frmdefactuacionesCUN.txtText1(0)
       Set rstA2 = objApp.rdoConnect.OpenResultset(sqlstr2)
       If rstA2.rdoColumns(0).Value > 0 Then 'existen fases
       
         If (rstA1.rdoColumns(0).Value = 0) Then
            frmdefactuacionesCUN.cmdinteracciones.Enabled = False
         Else
            frmdefactuacionesCUN.cmdinteracciones.Enabled = True
         End If
         frmdefactuacionesCUN.cmdcondiciones.Enabled = True
         frmdefactuacionesCUN.cmdmuestras.Enabled = True
         frmdefactuacionesCUN.cmdCuestionario.Enabled = True
         frmdefactuacionesCUN.cmdactprev.Enabled = True
         frmdefactuacionesCUN.cmdactasoc.Enabled = True
         frmdefactuacionesCUN.cmddptos.Enabled = True
         frmdefactuacionesCUN.cmdfases.Enabled = True
      
       End If '********************
  rstA1.Close
  Set rstA1 = Nothing
  rstA2.Close
  Set rstA2 = Nothing
  
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub lblAct1_Click(Index As Integer)
    txtText1(2).SetFocus
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
If strCtrlName = "txtText1(1)" Then
  aValues(2) = txtText1(0).Text
  'objWinInfo.objWinMainForm.rdoCursor!CI32CODTIPECON
End If
If strCtrlName = "txtText1(7)" Then
  aValues(2) = txtText1(6).Text
  'objWinInfo.objWinMainForm.rdoCursor!CI32CODTIPECON
End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
    Dim blnErrores As Boolean
    Dim strDepart As String
    Dim vntA As Variant
    
    If (txtText1(0).Text <> "" And txtText1(1).Text = "") Or (txtText1(1).Text <> "" And txtText1(0).Text = "") Then
        blnErrores = True
    End If
    
    If (txtText1(6).Text <> "" And txtText1(7).Text = "") Or (txtText1(7).Text <> "" And txtText1(6).Text = "") Then
        blnErrores = True
    End If
    If blnErrores Then
          Call objError.SetError(cwCodeMsg, "Por favor indique el codigo de clase y el de actuacion", vntA)
          vntA = objError.Raise
          blnCancel = True
          Exit Sub
    End If
    strDepart = Left(txtText1(1).Text, 3)
    If strDepart <> txtText1(2).Text And strDepart <> "" Then
    blnErrores = True
    End If
    strDepart = Left(txtText1(7).Text, 3)
    If strDepart <> txtText1(2).Text And strDepart <> "" Then
    blnErrores = True
    End If
    If blnErrores Then
          Call objError.SetError(cwCodeMsg, "Uno de los codigos de actuacion no pertenece al departamento", vntA)
          vntA = objError.Raise
          blnCancel = True
          Exit Sub
    End If
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Departamentos Realizadores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  If strFormName = "Departamentos Realizadores" And strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02FECFIN IS NULL"
     .strOrder = "ORDER BY ad02coddpto ASC"
         
     Set objField = .AddField("ad02coddpto")
     objField.strSmallDesc = "C�digo del Departamento"
         
     Set objField = .AddField("ad02desdpto")
     objField.strSmallDesc = "Descripci�n del Departamento "
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("ad02coddpto"))
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("ad02coddpto"))
     End If
   End With
   Set objSearch = Nothing
   txtText1(3).SetFocus
 End If
 If strFormName = "Departamentos Realizadores" And (strCtrl = "txtText1(1)" Or strCtrl = "txtText1(7)") Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "GC0100"
     .strOrder = "ORDER BY GC01FPCLAV ASC"
     .strWhere = "WHERE GC01FPCLAV LIKE '" & txtText1(2).Text & _
     "%'"
      Set objField = .AddField("GC01FCTREG")
     objField.strSmallDesc = "TIPO DE ACTUACION"
         
     Set objField = .AddField("GC01FPCLAV")
     objField.strSmallDesc = "CODIGO DE LA ACTUACION"
     
     Set objField = .AddField("GC01FCDESCA")
     objField.strSmallDesc = "DESCRIPCION DE LA ACTUACION"
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(2)), .cllValues("ad02coddpto"))
      If strCtrl = "txtText1(1)" Then
        Call objWinInfo.CtrlSet(txtText1(0), .cllValues("GC01FCTREG"))
        Call objWinInfo.CtrlSet(txtText1(1), .cllValues("GC01FPCLAV"))
      Else
        Call objWinInfo.CtrlSet(txtText1(6), .cllValues("GC01FCTREG"))
        Call objWinInfo.CtrlSet(txtText1(7), .cllValues("GC01FPCLAV"))
      End If
     End If
   End With
 End If
 
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim rstD As rdoResultset
  Dim strSelect As String
  Dim strmensaje As String
  Dim intResp As Integer
  Dim strDpto As String
  Dim rstDpto As rdoResultset
  
  ' controla que el departamento Nuevo existe antes de hacer Guardar
   If (btnButton.Index = 4) Then
      If (txtText1(2).Text <> "") Then
      sqlstr = "select count(AD02CODDPTO) from AD0200" & _
             " where AD02CODDPTO=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value = 0) Then
        strmensaje = MsgBox("El Departamento " & txtText1(2) & " no existe. " _
           & "Elija otro, por favor.", vbCritical, "Departamentos Realizadores")
        txtText1(2).SetFocus
        objWinInfo.objWinActiveForm.blnChanged = False
        Call objWinInfo.WinProcess(6, 2, 0)
        rsta.Close
        Set rsta = Nothing
        Exit Sub
      End If
      End If
  End If
  ' controla que el departamento Nuevo existe antes de pulsar un bot�n
  ' distinto de imprimir,borrar,anterior,siguiente,guardar.
  If (btnButton.Index <> 6 And btnButton.Index <> 8 And btnButton.Index <> 22 And _
       btnButton.Index <> 23 And btnButton.Index <> 4) Then
       If (txtText1(2).Text <> "") Then
       sqlstr = "select count(AD02CODDPTO) from AD0200" & _
             " where AD02CODDPTO=" & txtText1(2).Text
       Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
       If (rsta.rdoColumns(0).Value = 0) Then
           objWinInfo.objWinActiveForm.blnChanged = False
       End If
       rsta.Close
       Set rsta = Nothing
       End If
  End If
  'Borrar
  If btnButton.Index = 8 Then
   If Es_Posible_Borrar() Then
    posibleborrar = 1
    'Comprobamos que el departamento que queremos borrar no es el �ltimo
    strSelect = "SELECT COUNT(*) FROM PR0200 WHERE PR01CODACTUACION = " & txtactText1(0).Text
    Set rstD = objApp.rdoConnect.OpenResultset(strSelect)
    If (rstD.rdoColumns(0).Value = 1) Then
      'se mira si la actuaci�n tiene alguna fase o no
      strDpto = "SELECT count(*) FROM PR0500 WHERE pr01codactuacion=" & txtactText1(0).Text
      Set rstDpto = objApp.rdoConnect.OpenResultset(strDpto)
      If (rstDpto.rdoColumns(0).Value > 0) Then 'la actuacion  tiene fases
        strmensaje = "� Para borrar la actuaci�n PRIMERO borre las FASES y LUEGO el DEPARTAMENTO REALIZADOR"
        intResp = MsgBox(strmensaje, vbInformation, "Importante")
        undpto = 1
      End If
      rstDpto.Close
      Set rstDpto = Nothing
    End If
    rstD.Close
    Set rstD = Nothing
  End If
 End If
  'JCR 13/3/98 para activar la lista en la status bar
  If btnButton.Index = 2 Then
    txtText1(3).SetFocus
    txtText1(2).SetFocus
  End If
  
  'si la actuacion tiene fases y se pulsa borrar que no deje
  If (btnButton.Index = 8 And undpto = 1) Then
   'no hace nada
  Else
  If btnButton.Index = 8 And posibleborrar = 1 Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      posibleborrar = 0
  End If
  End If
  
  If btnButton.Index <> 8 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim rstD As rdoResultset
  Dim strSelect As String
  Dim strmensaje As String
  Dim intResp As Integer
  Dim contadpto As Integer
  Dim rstDpto As rdoResultset
  Dim strDpto As String
  
  ' controla que el departamento Nuevo existe antes de hacer Guardar
  If (intIndex = 40) Then
      If (txtText1(2).Text <> "") Then
      sqlstr = "select count(AD02CODDPTO) from AD0200" & _
             " where AD02CODDPTO=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      contadpto = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
      If (contadpto = 0) Then
        strmensaje = MsgBox("El Departamento " & txtText1(2) & " no existe. " _
        & "Elija otro, por favor.", vbCritical, "Departamentos Realizadores")
        txtText1(2).SetFocus
        objWinInfo.objWinActiveForm.blnChanged = False
        Call objWinInfo.WinProcess(6, 2, 0)
        Exit Sub
      End If
      End If
  End If
  ' controla que el departamento Nuevo existe antes de hacer la opci�n de men�.
  ' control si la opci�n es distinta de Eliminar y De Imprimir
  If (intIndex <> 60 And intIndex <> 80) Then
      If (txtText1(2).Text <> "") Then
      sqlstr = "select count(AD02CODDPTO) from AD0200" & _
             " where AD02CODDPTO=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value = 0) Then
        objWinInfo.objWinActiveForm.blnChanged = False
      End If
      rsta.Close
      Set rsta = Nothing
      End If
  End If
  
  'Borrar
  If intIndex = 60 Then
    If Es_Posible_Borrar() Then
      posibleborrar = 1
      'Comprobamos que el departamento que queremos borrar no es el �ltimo
      strSelect = "SELECT COUNT(*) FROM PR0200 WHERE PR01CODACTUACION = " & txtactText1(0).Text
      Set rstD = objApp.rdoConnect.OpenResultset(strSelect)
      If (rstD.rdoColumns(0).Value = 1) Then
        'se mira si la actuaci�n tiene alguna fase o no
        strDpto = "SELECT count(*) FROM PR0500 WHERE pr01codactuacion=" & txtactText1(0).Text
        Set rstDpto = objApp.rdoConnect.OpenResultset(strDpto)
        If (rstDpto.rdoColumns(0).Value > 0) Then 'la actuacion  tiene fases
          strmensaje = ". Para borrar la actuaci�n PRIMERO borre las FASES y LUEGO el DEPARTAMENTO REALIZADOR"
          intResp = MsgBox(strmensaje, vbInformation, "Importante")
          undpto = 1
        End If
        rstDpto.Close
        Set rstDpto = Nothing
      End If
  rstD.Close
  Set rstD = Nothing
  End If
  End If
    
    
 'si la actuacion tiene fases y se pulsa borrar que no deje
  If (intIndex = 60 And undpto = 1) Then
   'no hace nada
  Else
  If posibleborrar = 1 And intIndex = 60 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    posibleborrar = 0
  End If
  End If
  
  If intIndex <> 60 Then
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If
  
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   ' controla que el departamento Nuevo existe antes de hacer la opci�n de men�.
   ' control al pulsar Filtro
   If (intIndex = 10) Then
      If (txtText1(2).Text <> "") Then
      sqlstr = "select count(AD02CODDPTO) from AD0200" & _
             " where AD02CODDPTO=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value = 0) Then
        objWinInfo.objWinActiveForm.blnChanged = False
      End If
      rsta.Close
      Set rsta = Nothing
      End If
  End If
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   ' controla que el departamento Nuevo existe antes de hacer la opci�n de men�.
   ' control si es distinto de Anterior y de Siguiente
   If (intIndex <> 50 And intIndex <> 60) Then
      If (txtText1(2).Text <> "") Then
      sqlstr = "select count(AD02CODDPTO) from AD0200" & _
             " where AD02CODDPTO=" & txtText1(2).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value = 0) Then
        objWinInfo.objWinActiveForm.blnChanged = False
      End If
      rsta.Close
      Set rsta = Nothing
      End If
  End If
  
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              x As Single, _
                              y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtactText1_GotFocus(Index As Integer)
    txtText1(2).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(intIndex As Integer, KeyAscii As Integer)
If intIndex = 0 Or intIndex = 6 Then
 If KeyAscii <> 54 And KeyAscii <> 55 Then
  KeyAscii = 8
 End If
End If
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  If (intIndex = 2) Then
    txtText1(4).Text = txtactText1(0).Text
  End If
  Call objWinInfo.CtrlDataChange
End Sub


Private Function Es_Posible_Borrar() As Boolean
'JMRL 3/12/97
'Esta funci�n se crea para evitar el error 4002 Imposible borrar registros que da la
'CodeWizard cuado se intenta borrar un registro referenciado en otras tablas.
'
'Es_Posible_Borrar devuelve la lista de tablas en las que se hace referencia al registro
'que queremos borrar. Si NO se hace referencia en ninguna tabla al registro que se quiere
'borrar se devuelve TRUE
'Es_Posible_Borrar se usa en tlbToolBar1 y en mnuDatosOpcion
  
  Dim rstH As rdoResultset
  Dim strSelect As String
  Dim strmensaje As String
  Dim blnTieneHijos As Boolean
  Dim intResp As Integer
  
  If txtText1(2).Text <> "" Then
  
  strmensaje = "El departamento NO puede se borrado por: " & Chr(13) & Chr(13)
  blnTieneHijos = False
   
  'Se comprueba si se hace referencia en la tabla PR0300 ACTUACIONES PEDIDAS
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR0300 " & _
               "WHERE PR01CODACTUACION = " & txtactText1(0).Text & _
               "  AND AD02CODDPTO = " & txtText1(2).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If rstH.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� La actuaci�n ESTA PEDIDA." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
    
  'Se comprueba si se hace referencia en la tabla PR3100 ACTUACIONES ASOCIADAS
  'PR01CODACTUACION y PR01CODACTUACION_ASO
  strSelect = "SELECT COUNT(*) FROM PR3100 " & _
              "WHERE PR01CODACTUACION_ASO = " & txtactText1(0).Text & _
              "  AND AD02CODDPTO = " & txtText1(2).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� Otras actuaciones INCLUYEN ESTA ACTUACION COMO ASOCIADA." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3200 ACTUACIONES PREVISIBLES
  'PR01CODACTUACION y PR01CODACTUACION_PRE
  strSelect = "SELECT COUNT(*) FROM PR3200 " & _
              "WHERE PR32CODACTUACION_PRE = " & txtactText1(0).Text & _
              "  AND AD02CODDPTO = " & txtText1(2).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� Otras actuaciones INCLUYEN ESTA ACTUACION COMO PREVISIBLE." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3400 ACTUACIONES DE UN PAQUETE
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR3400 " & _
              "WHERE PR01CODACTUACION = " & txtactText1(0).Text & _
              "  AND AD02CODDPTO = " & txtText1(2).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN PAQUETE." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  'Se comprueba si se hace referencia en la tabla PR3600 ACTUACIONES DE UN PROTOCOLO
  'PR01CODACTUACION
  strSelect = "SELECT COUNT(*) FROM PR3600 " & _
              "WHERE PR01CODACTUACION = " & txtactText1(0).Text & _
              "  AND AD02CODDPTO = " & txtText1(2).Text
  Set rstH = objApp.rdoConnect.OpenResultset(strSelect)
  If (rstH.rdoColumns(0).Value > 0) Then
    strmensaje = strmensaje & "� La actuaci�n ESTA EN ALGUN PROTOCOLO." & Chr(13)
    blnTieneHijos = True
  End If
  rstH.Close
  Set rstH = Nothing
  
  
  If blnTieneHijos = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Es_Posible_Borrar = False
  Else
    Es_Posible_Borrar = True
  End If
  
End If

End Function
