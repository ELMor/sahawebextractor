VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "Comctl32.ocx"
Begin VB.Form frmPregPR 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cuestionarios asociados a las actuaciones"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11430
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8205
   ScaleWidth      =   11430
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Datos de la prueba"
      ForeColor       =   &H00800000&
      Height          =   2040
      Left            =   225
      TabIndex        =   2
      Top             =   0
      Width           =   5490
      Begin VB.CommandButton cmdSalir 
         Caption         =   "&Salir"
         Height          =   390
         Left            =   4050
         TabIndex        =   13
         Top             =   1500
         Width           =   1290
      End
      Begin VB.Frame Frame2 
         Caption         =   "Cuestionario asociado a:"
         ForeColor       =   &H00800000&
         Height          =   540
         Left            =   75
         TabIndex        =   9
         Top             =   1425
         Width           =   3840
         Begin VB.OptionButton optLab 
            Caption         =   "Laboratorio"
            Height          =   240
            Left            =   2550
            TabIndex        =   12
            Top             =   225
            Width           =   1215
         End
         Begin VB.OptionButton optDpt 
            Caption         =   "Departamento"
            Height          =   240
            Left            =   1125
            TabIndex        =   11
            Top             =   225
            Width           =   1515
         End
         Begin VB.OptionButton optPr 
            Caption         =   "Prueba"
            Height          =   240
            Left            =   150
            TabIndex        =   10
            Top             =   225
            Width           =   990
         End
      End
      Begin VB.TextBox txtPr 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   1050
         Width           =   3990
      End
      Begin VB.TextBox txtDpt 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   675
         Width           =   3990
      End
      Begin VB.TextBox txtPaquete 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   300
         Width           =   3990
      End
      Begin VB.Label Label1 
         Caption         =   "Actuaci�n:"
         Height          =   240
         Index           =   2
         Left            =   450
         TabIndex        =   8
         Top             =   1050
         Width           =   840
      End
      Begin VB.Label Label1 
         Caption         =   "Departamento:"
         Height          =   240
         Index           =   1
         Left            =   150
         TabIndex        =   6
         Top             =   675
         Width           =   1140
      End
      Begin VB.Label lblConcepto 
         Alignment       =   1  'Right Justify
         Caption         =   "Paquete:"
         Height          =   240
         Left            =   75
         TabIndex        =   3
         Top             =   300
         Width           =   1140
      End
   End
   Begin ComctlLib.TreeView tvwPreg 
      Height          =   8040
      Left            =   5775
      TabIndex        =   1
      Top             =   75
      Width           =   5640
      _ExtentX        =   9948
      _ExtentY        =   14182
      _Version        =   327682
      Indentation     =   706
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "imgIcos"
      Appearance      =   1
   End
   Begin ComctlLib.TreeView tvwPr 
      Height          =   6015
      Left            =   225
      TabIndex        =   0
      Top             =   2100
      Width           =   5490
      _ExtentX        =   9684
      _ExtentY        =   10610
      _Version        =   327682
      HideSelection   =   0   'False
      Indentation     =   706
      LabelEdit       =   1
      Style           =   7
      ImageList       =   "imgIcos"
      Appearance      =   1
   End
   Begin ComctlLib.ImageList imgIcos 
      Left            =   5400
      Top             =   3525
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   19
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":01DA
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":04F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":080E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":0B28
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":0E42
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":115C
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":1476
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":1790
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":1AAA
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":1DC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":20DE
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":23F8
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":2712
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":2A2C
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":2D46
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":2F20
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":30FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "PR0701.frx":32D4
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPregPR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public cPaq As Long, cProt As Long
Public cPr As Long
Public Orden As Integer, cDptPaq As Long, cDptReali As Long
Public Ventana As String ' Desde donde se le va a llamar
Dim nodoNuevo As Node
Dim OrdenGrupo As Integer, OrdenPreg As Integer
Dim pedir As Integer ' 1: Tipo de preguntas, 2: Pregunta, 3: Lista de respuestas
Dim tipo As Long ' Cuestionario asociado a -1: Laboratorio, 0: Dpt, Otro: n� cod. actuaci�n
Dim cod As Variant, cPrLab As Long

' Key de los grupos: "G" + C�digo del grupo (no su orden)
' Key de las preguntas: "P" + C�digo de la pregunta + "G" + C�digo del grupo (no su orden)

Private Sub cmdSalir_Click()
  Unload Me
End Sub

Private Sub Form_Activate()
  Call CargarCuestionarioPr
End Sub

Private Sub Form_Load()

  Screen.MousePointer = 11
  Call CargarPreguntas
  Screen.MousePointer = 0
    
'  optPr.Value = True
End Sub

Sub CargarCuestionarioPr()

  If Ventana = "Paquete" Then
    Call CargarCuestionarioPrPaq
  ElseIf Ventana = "Protocolo" Then
    Call CargarCuestionarioPrProt
  End If
End Sub

Sub CargarCuestionarioPrPaq()
Dim sql As String, texto As String, clave As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim cOrdenGrupo  As Integer, cOrdenPreg As Integer, ctrl As Integer
Dim nodo As Node

  tvwPr.Nodes.Clear
  tvwPr.Nodes.Add , , "R", txtPr.Text, hcICONPR
  sql = "SELECT UI0300.UI03Orden, UI0200.UI02Desig, UI0400.UI04Orden, " _
      & "PR4000.PR40DesPregunta, PR2700.PR27DesTipRespu, PR2800.PR28NumRespuesta, " _
      & "PR2800.PR28DesRespuesta, UI0300.UI03CodGrupo, PR4000.PR40CodPregunta, " _
      & "PR4600.PR46DesListResp, PR2700.PR27CodTipRespu, PR.cPrueba, " _
      & "PR.cResultado, PR.Designacion, UI0400.Operacion, PR1.cResultado, PR1.Designacion " _
      & "FROM UI0300, UI0200, UI0400, PR4000, PR2700, PR4600, PR2800, " _
      & "PruebasResultados PR, PruebasResultados PR1 WHERE " _
      & "UI0300.UI02CodTiPregunta = UI0200.UI02CodTiPregunta AND " _
      & "UI0300.UI03CodGrupo = UI0400.UI03CodGrupo (+) AND " _
      & "UI0400.PR40CodPregunta = PR4000.PR40CodPregunta (+) AND " _
      & "UI0400.UI04CodTipRespu = PR2700.PR27CodTipRespu (+) AND " _
      & "UI0400.PR46CodListResp = PR4600.PR46CodListResp (+) AND " _
      & "PR4600.PR46CodListResp = PR2800.PR46CodListResp (+) AND " _
      & "UI0400.cPrueba = PR.cPrueba (+) AND " _
      & "UI0400.cResultado = PR.cResultado (+) AND " _
      & "UI0400.cPrueba1 = PR1.cPrueba (+) AND " _
      & "UI0400.cResultado1 = PR1.cResultado (+) AND " _
      & "UI0300.AD02CODDPTO = ? AND " _
      & "UI0300.PR33CODESTANDARD = ? AND " _
      & "UI0300.UI03COD = ? AND " _
      & "UI0300.AD02CODDPTO_REA = ? " _
      & "ORDER BY UI0300.UI03Orden, UI0400.UI04Orden, PR2800.PR28NumRespuesta"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cDptPaq
  rdoQ(1) = cPaq
  
' Primero se comprueba si existe cuestionario asociado a la actuacion
  rdoQ(2) = cPr
  rdoQ(3) = cDptReali
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  
  If rdo.EOF = True Then ' Se comprueba si tiene cuestionario asociado al dpt.
    rdoQ(2) = 0
    rdoQ(3) = cDptReali
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo.EOF = True Then ' Se comprueba si tiene cuestionario asociado a laboratorio
      rdoQ(2) = -1
      rdoQ(3) = -1
      Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
      If rdo.EOF = True Then
        optPr.Value = True
        Exit Sub
      Else
        optLab.Value = True
      End If
    Else
      optDpt.Value = True
    End If
  Else
    optPr.Value = True
  End If

' Se insertan los nodos en el treeview
  cOrdenGrupo = 0
  cOrdenPreg = 0
  ctrl = False
  While rdo.EOF = False
    If cOrdenGrupo <> rdo(0) Then
      Set nodo = tvwPr.Nodes.Add("R", tvwChild, "G" & rdo(7), rdo(0) & ".- " & rdo(1), hcICONTIPOPREG)
      nodo.Tag = rdo(7)
    End If
    If Not IsNull(rdo(2)) Then
      Set nodo = tvwPr.Nodes.Add("G" & rdo(7), tvwChild, "P" & rdo(8) & "G" & rdo(7), rdo(2) & ".- " & rdo(3), hcICONPREG)
      nodo.Tag = rdo(8)
      If Not IsNull(rdo(4)) Or Not IsNull(rdo(11)) Then
        If rdo(10) = hcCODLISTA Then
          Set nodo = tvwPr.Nodes.Add(nodo.Key, tvwChild, , "Respuesta: " & rdo(9), hcICONLISTAS)
          cOrdenPreg = rdo(2)
          Do While cOrdenPreg = rdo(2)
            If IsNull(rdo(2)) Then Exit Do
            tvwPr.Nodes.Add nodo.Index, tvwChild, , rdo(5) & ".- " & rdo(6), hcICONLISTA
            ctrl = True
            cOrdenGrupo = rdo(0)
            rdo.MoveNext
            If rdo.EOF = True Then
              If tvwPr.Nodes("R").Children > 0 Then tvwPr.Nodes("R").Child.EnsureVisible
              Exit Sub
            End If
          Loop
        Else
          If Not IsNull(rdo(11)) Then
            texto = rdo(13)
            clave = "Z" & rdo(11) & "R" & rdo(12)
            If Not IsNull(rdo(14)) Then
              texto = texto & " " & rdo(14)
              clave = "W" & rdo(11) & "R" & rdo(12) & "O"
            End If
            If Not IsNull(rdo(16)) Then
              texto = texto & " " & rdo(16)
              clave = "X" & rdo(11) & "R" & rdo(12) & "O" & "R" & rdo(15)
            End If
            tvwPr.Nodes.Add nodo.Key, tvwChild, clave, texto, hcICONRESLAB
          Else
            tvwPr.Nodes.Add nodo.Key, tvwChild, , "Respuesta: " & rdo(4), hcICONLISTA
          End If
        End If
      End If
    End If
    
    If ctrl = True Then
      ctrl = False
    Else
      cOrdenGrupo = rdo(0)
      rdo.MoveNext
    End If
  Wend
  If tvwPr.Nodes("R").Children > 0 Then tvwPr.Nodes("R").Child.EnsureVisible
End Sub

Sub CargarCuestionarioPrProt()
Dim sql As String, texto As String, clave As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim cOrdenGrupo  As Integer, cOrdenPreg As Integer, ctrl As Integer
Dim nodo As Node

  tvwPr.Nodes.Clear
  tvwPr.Nodes.Add , , "R", txtPr.Text, hcICONPR
  sql = "SELECT UI0600.UI06Orden, UI0200.UI02Desig, UI0400.UI04Orden, " _
      & "PR4000.PR40DesPregunta, PR2700.PR27DesTipRespu, PR2800.PR28NumRespuesta, " _
      & "PR2800.PR28DesRespuesta, UI0600.UI06CodGrupo, PR4000.PR40CodPregunta, " _
      & "PR4600.PR46DesListResp, PR2700.PR27CodTipRespu, PR.cPrueba, " _
      & "PR.cResultado, PR.Designacion, UI0400.Operacion, PR1.cResultado, PR1.Designacion " _
      & "FROM UI0600, UI0200, UI0400, PR4000, PR2700, PR4600, PR2800, " _
      & "PruebasResultados PR, PruebasResultados PR1 WHERE " _
      & "UI0600.UI02CodTiPregunta = UI0200.UI02CodTiPregunta AND " _
      & "UI0600.UI06CodGrupo = UI0400.UI03CodGrupo (+) AND " _
      & "UI0400.PR40CodPregunta = PR4000.PR40CodPregunta (+) AND " _
      & "UI0400.UI04CodTipRespu = PR2700.PR27CodTipRespu (+) AND " _
      & "UI0400.PR46CodListResp = PR4600.PR46CodListResp (+) AND " _
      & "PR4600.PR46CodListResp = PR2800.PR46CodListResp (+) AND " _
      & "UI0400.cPrueba = PR.cPrueba (+) AND " _
      & "UI0400.cResultado = PR.cResultado (+) AND " _
      & "UI0400.cPrueba1 = PR1.cPrueba (+) AND " _
      & "UI0400.cResultado1 = PR1.cResultado (+) AND " _
      & "UI0600.PR35CODProtocolo = ? AND " _
      & "UI0600.UI06COD = ? AND " _
      & "UI0600.AD02CODDPTO_REA = ? " _
      & "ORDER BY UI0600.UI06Orden, UI0400.UI04Orden, PR2800.PR28NumRespuesta"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cProt
  
' Primero se comprueba si existe cuestionario asociado a la actuacion
  rdoQ(1) = cPr
  rdoQ(2) = cDptReali
  Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
  
  If rdo.EOF = True Then ' Se comprueba si tiene cuestionario asociado al dpt.
    rdoQ(1) = 0
    rdoQ(2) = cDptReali
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    If rdo.EOF = True Then ' Se comprueba si tiene cuestionario asociado a laboratorio
      rdoQ(1) = -1
      rdoQ(2) = -1
      Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
      If rdo.EOF = True Then
        optPr.Value = True
        Exit Sub
      Else
        optLab.Value = True
      End If
    Else
      optDpt.Value = True
    End If
  Else
    optPr.Value = True
  End If

' Se insertan los nodos en el treeview
  cOrdenGrupo = 0
  cOrdenPreg = 0
  ctrl = False
  While rdo.EOF = False
    If cOrdenGrupo <> rdo(0) Then
      Set nodo = tvwPr.Nodes.Add("R", tvwChild, "G" & rdo(7), rdo(0) & ".- " & rdo(1), hcICONTIPOPREG)
      nodo.Tag = rdo(7)
    End If
    If Not IsNull(rdo(2)) Then
      Set nodo = tvwPr.Nodes.Add("G" & rdo(7), tvwChild, "P" & rdo(8) & "G" & rdo(7), rdo(2) & ".- " & rdo(3), hcICONPREG)
      nodo.Tag = rdo(8)
      If Not IsNull(rdo(4)) Or Not IsNull(rdo(11)) Then
        If rdo(10) = hcCODLISTA Then
          Set nodo = tvwPr.Nodes.Add(nodo.Key, tvwChild, , "Respuesta: " & rdo(9), hcICONLISTAS)
          cOrdenPreg = rdo(2)
          Do While cOrdenPreg = rdo(2)
            If IsNull(rdo(2)) Then Exit Do
            tvwPr.Nodes.Add nodo.Index, tvwChild, , rdo(5) & ".- " & rdo(6), hcICONLISTA
            ctrl = True
            cOrdenGrupo = rdo(0)
            rdo.MoveNext
            If rdo.EOF = True Then
              If tvwPr.Nodes("R").Children > 0 Then tvwPr.Nodes("R").Child.EnsureVisible
              Exit Sub
            End If
          Loop
        Else
          If Not IsNull(rdo(11)) Then
            texto = rdo(13)
            clave = "Z" & rdo(11) & "R" & rdo(12)
            If Not IsNull(rdo(14)) Then
              texto = texto & " " & rdo(14)
              clave = "W" & rdo(11) & "R" & rdo(12) & "O"
            End If
            If Not IsNull(rdo(16)) Then
              texto = texto & " " & rdo(16)
              clave = "X" & rdo(11) & "R" & rdo(12) & "O" & "R" & rdo(15)
            End If
            tvwPr.Nodes.Add nodo.Key, tvwChild, clave, texto, hcICONRESLAB
          Else
            tvwPr.Nodes.Add nodo.Key, tvwChild, , "Respuesta: " & rdo(4), hcICONLISTA
          End If
        End If
      End If
    End If
    
    If ctrl = True Then
      ctrl = False
    Else
      cOrdenGrupo = rdo(0)
      rdo.MoveNext
    End If
  Wend
  If tvwPr.Nodes("R").Children > 0 Then tvwPr.Nodes("R").Child.EnsureVisible
End Sub



Sub CargarPreguntas()
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo As Node, nodo1 As Node
Dim cLista As Long, cDpt As Integer

  tvwPreg.Nodes.Clear
  tvwPreg.Nodes.Add , , "T", "Grupos de preguntas", hcICONTIPOSPREG
  tvwPreg.Nodes.Add , , "P", "Preguntas", hcICONPREGS
  tvwPreg.Nodes.Add , , "R", "Tipos de respuestas", hcICONLISTAS
  sql = "SELECT UI02CodTiPregunta, UI02Desig FROM UI0200 WHERE UI02IndActiva = -1 ORDER BY UI02Desig"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set nodo = tvwPreg.Nodes.Add("T", tvwChild, "T" & rdo(0), rdo(1), hcICONTIPOPREG)
    nodo.Tag = rdo(0)
    rdo.MoveNext
  Wend

  sql = "SELECT PR40CodPregunta, PR40DesPregunta FROM PR4000 ORDER BY PR40DesPregunta"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set nodo = tvwPreg.Nodes.Add("P", tvwChild, "P" & rdo(0), rdo(1), hcICONPREG)
    nodo.Tag = rdo(0)
    rdo.MoveNext
  Wend

  sql = "SELECT PR27CodTipRespu, PR27DesTipRespu FROM PR2700 ORDER BY PR27CodTipRespu"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  While rdo.EOF = False
    Set nodo = tvwPreg.Nodes.Add("R", tvwChild, "R" & rdo(0), rdo(1), hcICONLISTA)
    nodo.Tag = rdo(0)
    rdo.MoveNext
  Wend
  Set nodo1 = nodo
  Set nodo = tvwPreg.Nodes.Add("R", tvwChild, "R-1", "Laboratorio", hcICONLISTAS)
  nodo.Tag = -1
  nodo.Image = hcICONLISTAS
  
  sql = "SELECT PR4600.PR46CodListResp, PR4600.PR46DesListResp," _
      & "PR2800.PR28NumRespuesta, PR2800.PR28DesRespuesta " _
      & "FROM PR4600, PR2800 WHERE " _
      & "PR4600.PR46CodListResp = PR2800.PR46CodListResp " _
      & "ORDER BY PR4600.PR46DesListResp, PR2800.PR28NumRespuesta"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  cLista = 0
  While rdo.EOF = False
    If cLista <> rdo(0) Then
      Set nodo = tvwPreg.Nodes.Add(nodo1.Key, tvwChild, "A" & rdo(0), rdo(1), hcICONLISTAS)
      nodo.Tag = rdo(0)
    End If
    Set nodo = tvwPreg.Nodes.Add("A" & rdo(0), tvwChild, , rdo(2) & ".- " & rdo(3), hcICONLISTA)
    cLista = rdo(0)
    rdo.MoveNext
  Wend
  
  sql = "SELECT AD0200.AD02CodDpto, NVL(AD0200.AD02DesCompleta, AD0200.AD02DesDpto), " _
      & "P.cPrueba, P.Descripcion " _
      & "FROM Pruebas P, PR0200, AD0200 WHERE " _
      & "AD0200.AD02CodDpto = PR0200.AD02CodDpto AND " _
      & "PR0200.PR01CodActuacion = P.cPrueba " _
      & "ORDER BY AD0200.AD02CodDpto, P.Descripcion"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  cDpt = 0
  While rdo.EOF = False
    If cDpt <> rdo(0) Then
      Set nodo = tvwPreg.Nodes.Add("R-1", tvwChild, "L" & rdo(0), rdo(1), hcICONDPT)
      nodo.Tag = rdo(0)
    End If
    Set nodo = tvwPreg.Nodes.Add("L" & rdo(0), tvwChild, "Z" & rdo(2), rdo(3), hcICONPRUEBALAB)
    nodo.Tag = rdo(2)
    cDpt = rdo(0)
    rdo.MoveNext
  Wend
  
  Set nodo = tvwPreg.Nodes.Add("R-1", tvwChild, "H", "Operaciones entre resultados", hcICONOPERACIONES)
  tvwPreg.Nodes.Add nodo.Key, tvwChild, "H+", "Sumar", hcICONMAS
  tvwPreg.Nodes.Add nodo.Key, tvwChild, "H-", "Restar", hcICONMENOS
  tvwPreg.Nodes.Add nodo.Key, tvwChild, "H*", "Multiplicar", hcICONPOR
  tvwPreg.Nodes.Add nodo.Key, tvwChild, "H/", "Dividir", hcICONDIVIDIR
End Sub

Private Sub optDpt_Click()
  If optDpt.Value = True Then
    tvwPr.Nodes("R").Text = txtDpt.Text
    tipo = 0
  End If

End Sub

Private Sub optLab_Click()
  If optLab.Value = True Then
    tvwPr.Nodes("R").Text = "Laboratorio"
    tipo = -1
  End If

End Sub

Private Sub optPr_Click()
  If optPr.Value = True Then
    tvwPr.Nodes("R").Text = txtPr.Text
    tipo = cPr
  End If
End Sub

Private Sub tvwPr_Click()
  tvwPr.DropHighlight = Nothing
End Sub

Private Sub tvwPr_DragDrop(Source As Control, X As Single, Y As Single)
Dim nodo As Node, clase As String
      
  On Error Resume Next
  Set nodo = tvwPr.DropHighlight
  If Err <> 0 Then Exit Sub
  clase = Left(nodo.Key, 1)
  
  Select Case pedir
    Case 1 ' se ha arrastrado un grupo de preguntas
      If clase = "R" Then ' Se ha arrastrado sobre el nodo raiz. Se inserta al final
        If InsertarGrupoFinal() = False Then
          MsgBox "Error al a�adir el grupo de preguntas. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      ElseIf clase = "G" Then ' Se ha arrastrado sobre un grupo. Se a�ade antes
        If InsertarGrupoMedio(nodo) = False Then
          MsgBox "Error al a�adir el grupo de preguntas. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
    Case 2 ' Se ha arrastrado la pregunta
      If clase = "G" Then ' Se ha arrastrado sobre el grupo. Se inserta al final
        If InsertarPregFinal(nodo) = False Then
          MsgBox "Error al a�adir la pregunta. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      ElseIf clase = "P" Then ' Se ha arrastrado sobre la pregunta. Se a�ade antes
        If InsertarPregMedio(nodo) = False Then
          MsgBox "Error al a�adir la pregunta. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
      
    Case 3 ' Se ha arrastrado el tipo de respuesta
      If clase = "P" Then ' Se ha arrastrado sobre la prueba
        If InsertarResp(nodo) = False Then
          MsgBox "Error al a�adir la respuesta. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
    
    Case 4  ' Se ha arrastrado la lista de respuestas
      If clase = "P" Then ' Se ha arrastrado sobre la prueba
        If InsertarTipoResp(nodo) = False Then
          MsgBox "Error al a�adir la lista de respuestas. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
  
    Case 5  ' Se ha arrastrado un resultado de laboratorio
      If clase = "P" Then ' Se ha arrastrado sobre la prueba
        If InsertarResLab(nodo) = False Then
          MsgBox "Error al a�adir el resultado de laboratorio. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      ElseIf clase = "W" Then ' Se ha arrastrado sobre el operador
        If InsertarRes1Lab(nodo) = False Then
          MsgBox "Error al a�adir el resultado de laboratorio. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
  
    Case 6  ' Se ha arrastrado un operador
      If clase = "Z" Then ' Se ha arrastrado sobre el resultado
        If InsertarOperador(nodo) = False Then
          MsgBox "Error al a�adir el operador. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
  
    Case 10  ' Se ha arrastrado la lista de respuestas
      If clase = "G" And nodo.Tag <> cod Then ' Se ha arrastrado sobre otro grupo
        If CambiarOrdenGrupo(nodo) = False Then
          MsgBox "Error al a�adir la lista de respuestas. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
  
    Case 11  ' Se ha arrastrado la lista de respuestas
      If clase = "P" And nodo.Tag <> cod Then ' Se ha arrastrado sobre otra prueba
        If CambiarOrdenPreg(nodo) = False Then
          MsgBox "Error al a�adir la lista de respuestas. " & Chr$(13) & Error, vbExclamation, "Cuestionario"
        End If
      End If
  
  End Select
    
End Sub

Private Sub tvwPr_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
Dim valido As Boolean
  
  Set tvwPr.DropHighlight = tvwPr.HitTest(X, Y)
  Select Case State
    Case 1
      tvwPreg.DragIcon = imgIcos.ListImages(hcICONPROHIBIDO).Picture
    Case 0
      tvwPreg.DragIcon = imgIcos.ListImages(Source.SelectedItem.Image).Picture
  End Select
    
On Error Resume Next

  If Err = 0 Then
    valido = ArrastreValido(Source)
    If valido = True Then
      Source.DragIcon = imgIcos.ListImages(Source.SelectedItem.Image).Picture
    Else
      Source.DragIcon = imgIcos.ListImages(hcICONPROHIBIDO).Picture
    End If
  Else
    valido = False
    Source.DragIcon = imgIcos.ListImages(hcICONPROHIBIDO).Picture
  End If

End Sub

Private Function ArrastreValido(Source As Control) As Boolean
Dim nodo As Node
Dim clase As String

  Set nodo = tvwPr.DropHighlight
  clase = Left(nodo.Key, 1)
  
  Select Case pedir
    Case 1 ' se ha arrastrado un grupo de preguntas
      If clase = "R" Or clase = "G" Then ' Se ha arrastrado sobre la prueba (Root) o grupo
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
    Case 2 ' Se ha arrastrado la pregunta
      If clase = "G" Or clase = "P" Then ' Se ha arrastrado sobre el grupo o pregunta
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
      
    Case 3 ' Se ha arrastrado el tipo de respuesta
      If clase = "P" Then ' Se ha arrastrado sobre la pregunta
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
    
    Case 4  ' Se ha arrastrado la lista de respuestas
      If clase = "P" Then ' Se ha arrastrado sobre la pregunta
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
  
    Case 5  ' Se ha arrastrado el resultado de laboratorio
      If clase = "P" Or clase = "W" Then ' Se ha arrastrado sobre la pregunta o el operador
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
  
    Case 6  ' Se ha arrastrado el operador
      If clase = "Z" Then ' Se ha arrastrado sobre el resultado de laboratorio
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
  
    Case 10  ' Se ha arrastrado el grupo para cambiarlo de orden
      If clase = "G" Then ' Se ha arrastrado sobre el grupo
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
  
    Case 11  ' Se ha arrastrado la pregunta para cambiarla de orden
      If clase = "P" Then ' Se ha arrastrado sobre el grupo
        ArrastreValido = True
      Else
        ArrastreValido = False
      End If
  
  End Select
End Function

Private Sub tvwPr_KeyDown(KeyCode As Integer, Shift As Integer)
Dim nodo As Node, nodo1 As Node
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer, res As Integer, pos As Integer

  If KeyCode = 46 Then    ' Tecla Suprimir
    Set nodo = tvwPr.SelectedItem
    Select Case Left(nodo.Key, 1)
      Case "G"
        res = MsgBox("�Est� seguro de que desea borrar el grupo '" & nodo.Text & "'?. Se renumerar�n los grupos posteriores.", vbQuestion + vbYesNo, "Cuestionario")
        If res = vbYes Then Call BorrarGrupo(nodo)
      Case "P"
        res = MsgBox("�Est� seguro de que desea borrar la pregunta '" & nodo.Text & "'?. Se renumerar�n las preguntas posteriores.", vbQuestion + vbYesNo, "Cuestionario")
        If res = vbYes Then Call BorrarPreg(nodo)
    End Select
  End If
End Sub

Private Sub tvwPr_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Key As String, nodo As Node
  
  On Error Resume Next
  Set tvwPr.DropHighlight = Nothing
  Set nodo = tvwPr.HitTest(X, Y)
  If Err <> 0 Then Exit Sub
  tvwPr.SelectedItem = nodo
  Key = nodo.Key
  If Len(nodo) > 1 Then
    Select Case Left(Key, 1)
      Case "G" ' Se arrastra el tipo de pregunta
        pedir = 10
        tvwPr.Drag 1 'inicia el arrastre
        Set tvwPr.DragIcon = imgIcos.ListImages(tvwPr.SelectedItem.Image).Picture
        cod = nodo.Tag
        OrdenGrupo = Val(nodo.Text)
        Set nodoNuevo = nodo
        
      Case "P" ' Se arrastra el tipo de pregunta
        pedir = 11
        tvwPr.Drag 1 'inicia el arrastre
        Set tvwPr.DragIcon = imgIcos.ListImages(tvwPr.SelectedItem.Image).Picture
        cod = nodo.Tag
        OrdenPreg = Val(nodo.Text)
        Set nodoNuevo = nodo
  
    End Select
  End If
End Sub

Private Sub tvwPreg_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
  Select Case State
    Case 1
      tvwPreg.DragIcon = imgIcos.ListImages(hcICONPROHIBIDO).Picture
    Case 0
      tvwPreg.DragIcon = imgIcos.ListImages(Source.SelectedItem.Image).Picture
  End Select
End Sub

Private Sub tvwPreg_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Dim Key As String, nodo As Node
  
  On Error Resume Next
  Set tvwPreg.DropHighlight = Nothing
  Set nodo = tvwPreg.HitTest(X, Y)
  If Err <> 0 Then Exit Sub
  tvwPreg.SelectedItem = nodo
  Key = nodo.Key
  If Len(nodo) > 1 Then
   Select Case Left(Key, 1)
     Case "T" ' Se arrastra el tipo de pregunta
       pedir = 1
       tvwPreg.Drag 1 'inicia el arrastre
       Set tvwPreg.DragIcon = imgIcos.ListImages(tvwPreg.SelectedItem.Image).Picture
       cod = nodo.Tag
  
     Case "P" ' Se arrastra la pregunta
       pedir = 2
       tvwPreg.Drag 1 'inicia el arrastre
       Set tvwPreg.DragIcon = imgIcos.ListImages(tvwPreg.SelectedItem.Image).Picture
       cod = nodo.Tag
     
     Case "R" ' Se arrastra el tipo de respuesta
       If Left(Key, 2) <> "R4" Then ' No se puede arrastrar "Lista de respuestas"
         pedir = 3
         tvwPreg.Drag 1 'inicia el arrastre
         Set tvwPreg.DragIcon = imgIcos.ListImages(tvwPreg.SelectedItem.Image).Picture
         cod = nodo.Tag
       End If
     
     Case "A"  ' Se arrastra la lista de respuestas
       pedir = 4
       tvwPreg.Drag 1 'inicia el arrastre
       Set tvwPreg.DragIcon = imgIcos.ListImages(tvwPreg.SelectedItem.Image).Picture
       cod = nodo.Tag
     
     Case "Y"  ' Se arrastra el resultado de laboratorio
       pedir = 5
       tvwPreg.Drag 1 'inicia el arrastre
       Set tvwPreg.DragIcon = imgIcos.ListImages(tvwPreg.SelectedItem.Image).Picture
       cod = nodo.Tag
       cPrLab = nodo.Parent.Tag
       
     Case "H"  ' Se arrastra el operador entre resultados de laboratorio
       pedir = 6
       tvwPreg.Drag 1 'inicia el arrastre
       Set tvwPreg.DragIcon = imgIcos.ListImages(tvwPreg.SelectedItem.Image).Picture
       cod = Right$(nodo.Key, 1)
       
    End Select
  End If
End Sub

Function InsertarGrupoFinal() As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim nodo As Node, cGrupo As Long

  If Ventana = "Paquete" Then ' Si se ha llamado desde la ventana de paquetes
    sql = "INSERT INTO UI0300 (UI03CODGRUPO, AD02CODDPTO, PR33CODESTANDARD, UI03COD ," _
        & "AD02CODDPTO_REA, UI02CODTIPREGUNTA, UI03ORDEN, PR34ORDEN) VALUES " _
        & "(?, ?, ?, ?, ?, ?, ?, ?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    cGrupo = NuevoCod("UI03CodGrupo")
    rdoQ(0) = cGrupo
    rdoQ(1) = cDptPaq
    rdoQ(2) = cPaq
    rdoQ(3) = tipo
    If tipo = -1 Then rdoQ(4) = -1 Else rdoQ(4) = cDptReali
    rdoQ(5) = cod
    If tvwPr.Nodes("R").Children = 0 Then
      rdoQ(6) = 1
    Else
      rdoQ(6) = tvwPr.Nodes("R").Children + 1
    End If
    rdoQ(7) = Orden
    rdoQ.Execute
  ElseIf Ventana = "Protocolo" Then ' Si se ha llamado desde la ventana de protocolos
    sql = "INSERT INTO UI0600 (UI06CODGRUPO, PR35CODProtocolo, UI06COD ," _
        & "AD02CODDPTO_REA, UI02CODTIPREGUNTA, UI06ORDEN, PR36ORDEN) VALUES " _
        & "(?, ?, ?, ?, ?, ?, ?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    cGrupo = NuevoCod("UI03CodGrupo")
    rdoQ(0) = cGrupo
    rdoQ(1) = cProt
    rdoQ(2) = tipo
    If tipo = -1 Then rdoQ(3) = -1 Else rdoQ(3) = cDptReali
    rdoQ(4) = cod
    If tvwPr.Nodes("R").Children = 0 Then
      rdoQ(5) = 1
    Else
      rdoQ(5) = tvwPr.Nodes("R").Children + 1
    End If
    rdoQ(6) = Orden
    rdoQ.Execute
  End If
  
  If rdoQ.RowsAffected = 1 Then
    InsertarGrupoFinal = True
    Set nodo = tvwPr.Nodes.Add("R", tvwChild, "G" & cGrupo, rdoQ(5) & ".- " & tvwPreg.SelectedItem.Text, hcICONTIPOPREG)
    nodo.Tag = cGrupo
    tvwPr.Nodes("R").Child.EnsureVisible
  Else
    InsertarGrupoFinal = False
  End If

End Function

Function InsertarGrupoMedio(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim pos As Integer, i As Integer
Dim nodo1 As Node, cGrupo As Long

  pos = Val(nodo.Text)
  Set nodo1 = nodo
  InsertarGrupoMedio = True
  
  If Ventana = "Paquete" Then ' Si se ha llamado desde la ventana de paquetes
    sql = "INSERT INTO UI0300 (UI03CODGRUPO, AD02CODDPTO, PR33CODESTANDARD, UI03COD ," _
        & "AD02CODDPTO_REA, UI02CODTIPREGUNTA, UI03ORDEN, PR34ORDEN) VALUES " _
        & "(?, ?, ?, ?, ?, ?, ?, ?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    cGrupo = NuevoCod("UI03CodGrupo")
    rdoQ(0) = cGrupo
    rdoQ(1) = cDptPaq
    rdoQ(2) = cPaq
    rdoQ(3) = tipo
    If tipo = -1 Then rdoQ(4) = -1 Else rdoQ(4) = cDptReali
    rdoQ(5) = cod
    rdoQ(6) = pos
    rdoQ(7) = Orden
    objApp.BeginTrans
    rdoQ.Execute
  ElseIf Ventana = "Protocolo" Then ' Si se ha llamado desde la ventana de protocolos
    sql = "INSERT INTO UI0600 (UI06CODGRUPO, PR35CODProtocolo, UI06COD ," _
        & "AD02CODDPTO_REA, UI02CODTIPREGUNTA, UI06ORDEN, PR36ORDEN) VALUES " _
        & "(?, ?, ?, ?, ?, ?, ?)"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    cGrupo = NuevoCod("UI03CodGrupo")
    rdoQ(0) = cGrupo
    rdoQ(1) = cProt
    rdoQ(2) = tipo
    If tipo = -1 Then rdoQ(3) = -1 Else rdoQ(3) = cDptReali
    rdoQ(4) = cod
    rdoQ(5) = pos
    rdoQ(6) = Orden
    objApp.BeginTrans
    rdoQ.Execute
  End If
  If rdoQ.RowsAffected = 1 Then
    For i = pos To tvwPr.Nodes("R").Children
      If Ventana = "Paquete" Then ' Si se ha llamado desde la ventana de paquetes
        sql = "UPDATE UI0300 SET UI03ORDEN = ? WHERE UI03CODGRUPO = ?"
      ElseIf Ventana = "Protocolo" Then ' Si se ha llamado desde la ventana de protocolos
        sql = "UPDATE UI0600 SET UI06ORDEN = ? WHERE UI06CODGRUPO = ?"
      End If
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = i + 1
      rdoQ(1) = nodo.Tag
      rdoQ.Execute
      If rdoQ.RowsAffected = 0 Then
        objApp.RollbackTrans
        InsertarGrupoMedio = False
        Exit Function
      End If
      nodo.Text = i + 1 & Right(nodo.Text, Len(nodo.Text) - 1)
      nodo.Key = "G" & nodo.Tag
      Set nodo = nodo.Next
    Next i
    InsertarGrupoMedio = True
    Set nodo = tvwPr.Nodes.Add(nodo1.Key, tvwPrevious, "G" & cGrupo, pos & ".- " & tvwPreg.SelectedItem.Text, hcICONTIPOPREG)
    nodo.Tag = cGrupo
    tvwPr.Nodes("R").Child.EnsureVisible
    objApp.CommitTrans
  Else
    InsertarGrupoMedio = False
  End If
End Function


Function InsertarPregFinal(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim cGrupo As Long

  sql = "INSERT INTO UI0400 (UI03CODGRUPO, PR40CODPREGUNTA, UI04ORDEN) VALUES (?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  cGrupo = nodo.Tag
  rdoQ(0) = cGrupo
  rdoQ(1) = cod
  If nodo.Children = 0 Then
    rdoQ(2) = 1
  Else
    rdoQ(2) = nodo.Children + 1
  End If
  rdoQ.Execute
  
  If rdoQ.RowsAffected = 1 Then
    InsertarPregFinal = True
    Set nodo = tvwPr.Nodes.Add(nodo.Key, tvwChild, "P" & cod & "G" & cGrupo, rdoQ(2) & ".- " & tvwPreg.SelectedItem.Text, hcICONPREG)
    nodo.Tag = cod
    tvwPr.Nodes("R").Child.EnsureVisible
  Else
    InsertarPregFinal = False
  End If
End Function

Function InsertarPregMedio(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim pos As Integer, i As Integer
Dim nodo1 As Node, cGrupo As Long

  pos = Val(nodo.Text)
  Set nodo1 = nodo
  InsertarPregMedio = True
  sql = "INSERT INTO UI0400 (UI03CODGRUPO, PR40CODPREGUNTA, UI04ORDEN) VALUES (?, ?, ?)"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  cGrupo = nodo.Parent.Tag
  rdoQ(0) = cGrupo
  rdoQ(1) = cod
  rdoQ(2) = pos
  objApp.BeginTrans
  rdoQ.Execute
  If rdoQ.RowsAffected = 1 Then
    For i = pos To nodo.Parent.Children
      sql = "UPDATE UI0400 SET UI04ORDEN = ? WHERE UI03CODGRUPO = ? AND PR40CODPREGUNTA = ?"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
      rdoQ(0) = i + 1
      rdoQ(1) = cGrupo
      rdoQ(2) = nodo.Tag
      rdoQ.Execute
      If rdoQ.RowsAffected = 0 Then
        objApp.RollbackTrans
        InsertarPregMedio = False
        Exit Function
      End If
      nodo.Text = i + 1 & Right(nodo.Text, Len(nodo.Text) - 1)
      nodo.Key = "P" & nodo.Tag & "G" & cGrupo
      Set nodo = nodo.Next
    Next i
    InsertarPregMedio = True
    Set nodo = tvwPr.Nodes.Add(nodo1.Key, tvwPrevious, "P" & cod & "G" & cGrupo, pos & ".- " & tvwPreg.SelectedItem.Text, hcICONPREG)
    nodo.Tag = cod
    tvwPr.Nodes("R").Child.EnsureVisible
    objApp.CommitTrans
  Else
    InsertarPregMedio = False
  End If
End Function



Function NuevoCod(campo As String) As Long
Dim sql As String
Dim rdo As rdoResultset
  
  campo = campo & "_SEQUENCE"
  sql = "SELECT " & campo & ".NEXTVAL FROM DUAL"
  Set rdo = objApp.rdoConnect.OpenResultset(sql, rdOpenForwardOnly)
  NuevoCod = rdo(0)
  rdo.Close

End Function


Sub BorrarGrupo(nodo As Node)
Dim nodo1 As Node, nodoPreg As Node
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer, pos As Integer

  On Error Resume Next
  If nodo.Children > 0 Then
    Set nodoPreg = nodo.Child
    For i = 1 To nodo.Children
      Call BorrarPreg(nodoPreg)
      Set nodoPreg = nodoPreg.Next
    Next i
  End If
  If Ventana = "Paquete" Then
    sql = "DELETE FROM UI0300 WHERE UI03CodGrupo = ?"
  ElseIf Ventana = "Protocolo" Then
    sql = "DELETE FROM UI0600 WHERE UI06CodGrupo = ?"
  End If
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = nodo.Tag
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    MsgBox "No se ha conseguido borrar el grupo de preguntas.", vbExclamation, "Cuestionario"
    Exit Sub
  End If
  Set nodo1 = nodo.Next
  tvwPr.Nodes.Remove nodo.Key
  If nodo1 Is Nothing Then Exit Sub
  pos = Val(nodo.Text)
  If Ventana = "Paquete" Then
    sql = "UPDATE UI0300 SET UI03Orden = ? WHERE UI03CodGrupo = ?"
  ElseIf Ventana = "Protocolo" Then
    sql = "UPDATE UI0600 SET UI06Orden = ? WHERE UI06CodGrupo = ?"
  End If
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  For i = pos To nodo1.Parent.Children
    rdoQ(0) = i
    rdoQ(1) = nodo1.Tag
    rdoQ.Execute
    nodo1.Text = i & Right$(nodo1.Text, Len(nodo1.Text) - 1)
    Set nodo1 = nodo1.Next
  Next i

End Sub


Sub BorrarPreg(nodo As Node)
Dim nodo1 As Node
Dim sql As String
Dim rdoQ As rdoQuery
Dim i As Integer, pos As Integer, cGrupo As Long


  sql = "DELETE FROM UI0400 WHERE UI03CodGrupo = ? AND PR40CodPregunta = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  cGrupo = nodo.Parent.Tag
  rdoQ(0) = cGrupo
  rdoQ(1) = nodo.Tag
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    MsgBox "No se ha conseguido borrar la pregunta.", vbExclamation, "Cuestionario"
    Exit Sub
  End If
  Set nodo1 = nodo.Next
  tvwPr.Nodes.Remove nodo.Key
  If nodo1 Is Nothing Then
    Exit Sub
  End If
  pos = Val(nodo1.Text)
  sql = "UPDATE UI0400 SET UI04Orden = ? WHERE UI03CodGrupo = ? AND PR40CodPregunta = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  For i = pos - 1 To nodo1.Parent.Children
    rdoQ(0) = i
    rdoQ(1) = cGrupo
    rdoQ(2) = nodo1.Tag
    rdoQ.Execute
    nodo1.Text = i & Right$(nodo1.Text, Len(nodo1.Text) - 1)
    Set nodo1 = nodo1.Next
  Next i

End Sub

Function InsertarResp(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim cGrupo As Long, cPreg As Long

  cGrupo = nodo.Parent.Tag
  cPreg = nodo.Tag
    
' Se borran los hijos de la pregunta, si existen
  If nodo.Children > 0 Then tvwPr.Nodes.Remove nodo.Child.Index
    
  sql = "UPDATE UI0400 SET UI04CODTIPRESPU = ?, cPrueba = NULL, cResultado = NULL WHERE " _
      & "UI03CODGRUPO = ? AND PR40CODPREGUNTA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cod
  rdoQ(1) = cGrupo
  rdoQ(2) = cPreg
  rdoQ.Execute
  
  If rdoQ.RowsAffected = 1 Then
    InsertarResp = True
    Set nodo = tvwPr.Nodes.Add(nodo.Key, tvwChild, , "Respuesta: " & tvwPreg.SelectedItem.Text, hcICONLISTA)
  Else
    InsertarResp = False
  End If
End Function

Function InsertarTipoResp(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim cGrupo As Long, cPreg As Long, i As Integer, numResp As Integer
Dim nodoresp As Node, nodoLista As Node

  cGrupo = nodo.Parent.Tag
  cPreg = nodo.Tag
  
' Se borran los hijos de la pregunta, si existen
  If nodo.Children > 0 Then tvwPr.Nodes.Remove nodo.Child.Index
  
  sql = "UPDATE UI0400 SET UI04CODTIPRESPU = ?, PR46CODLISTRESP = ? WHERE " _
      & "UI03CODGRUPO = ? AND PR40CODPREGUNTA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = hcCODLISTA
  rdoQ(1) = cod
  rdoQ(2) = cGrupo
  rdoQ(3) = cPreg
  rdoQ.Execute
  
  If rdoQ.RowsAffected = 1 Then
    InsertarTipoResp = True
    Set nodoresp = tvwPr.Nodes.Add(nodo.Key, tvwChild, , "Respuesta: " & tvwPreg.SelectedItem.Text, hcICONLISTAS)
    numResp = tvwPreg.SelectedItem.Children
    Set nodoLista = tvwPreg.SelectedItem.Child
    For i = 1 To numResp
      tvwPr.Nodes.Add nodoresp.Index, tvwChild, , nodoLista.Text, hcICONLISTA
      Set nodoLista = nodoLista.Next
    Next i
  Else
    InsertarTipoResp = False
  End If
End Function

Function InsertarResLab(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim cGrupo As Long, cPreg As Long, i As Integer, numResp As Integer
Dim nodoresp As Node, nodoLista As Node

  cGrupo = nodo.Parent.Tag
  cPreg = nodo.Tag
  
' Se borran los hijos de la pregunta, si existen
  If nodo.Children > 0 Then tvwPr.Nodes.Remove nodo.Child.Index
  
  sql = "UPDATE UI0400 SET UI04CODTIPRESPU = -1, PR46CODLISTRESP = NULL, " _
      & "cPrueba1 = NULL, cResultado1 = NULL, Operacion = NULL, " _
      & "cPrueba = ?, cResultado = ? " _
      & "WHERE UI03CODGRUPO = ? AND PR40CODPREGUNTA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cPrLab
  rdoQ(1) = cod
  rdoQ(2) = cGrupo
  rdoQ(3) = cPreg
  rdoQ.Execute
  
  If rdoQ.RowsAffected = 1 Then
    InsertarResLab = True
    Set nodoresp = tvwPr.Nodes.Add(nodo.Key, tvwChild, "Z" & cod & "P" & cPreg, tvwPreg.SelectedItem.Text, hcICONRESLAB)
    nodoresp.Tag = "R"
  Else
    InsertarResLab = False
  End If
End Function

Function InsertarRes1Lab(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim cGrupo As Long, cPreg As Long, i As Integer, numResp As Integer
Dim nodoresp As Node, nodoLista As Node

  cGrupo = nodo.Parent.Parent.Tag
  cPreg = nodo.Parent.Tag
  
' Se borran los hijos de la pregunta, si existen
  If nodo.Children > 0 Then tvwPr.Nodes.Remove nodo.Child.Index
  
  sql = "UPDATE UI0400 SET cPrueba1 = ?, cResultado1 = ? " _
      & "WHERE UI03CODGRUPO = ? AND PR40CODPREGUNTA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cPrLab
  rdoQ(1) = cod
  rdoQ(2) = cGrupo
  rdoQ(3) = cPreg
  rdoQ.Execute
  
  If rdoQ.RowsAffected = 1 Then
    InsertarRes1Lab = True
    nodo.Text = nodo.Text & " " & tvwPreg.SelectedItem.Text
    nodo.Key = "X" & cod & "P" & cPreg
  Else
    InsertarRes1Lab = False
  End If
End Function


Function InsertarOperador(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim cGrupo As Long, cPreg As Long, i As Integer, numResp As Integer
Dim nodoresp As Node, nodoLista As Node

  cGrupo = nodo.Parent.Parent.Tag
  cPreg = nodo.Parent.Tag
  
' Se borran los hijos de la pregunta, si existen
  If nodo.Children > 0 Then tvwPr.Nodes.Remove nodo.Child.Index
  
  sql = "UPDATE UI0400 SET Operacion = ? WHERE UI03CODGRUPO = ? AND PR40CODPREGUNTA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  rdoQ(0) = cod
  rdoQ(1) = cGrupo
  rdoQ(2) = cPreg
  rdoQ.Execute
  
  If rdoQ.RowsAffected = 1 Then
    InsertarOperador = True
    nodo.Text = nodo.Text & " " & cod
    nodo.Key = "W" & cod & "P" & cPreg
  Else
    InsertarOperador = False
  End If
End Function


Function CambiarOrdenGrupo(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim pos As Integer
Dim OrdenNuevo As Integer

  pos = Val(nodo.Text)
  CambiarOrdenGrupo = True
  OrdenNuevo = Val(nodo.Text)
  
  objApp.BeginTrans
  sql = "UPDATE UI0300 SET UI03ORDEN = ? WHERE UI03CODGRUPO = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  
' Se cambia primero el orden al nodo al que se arrastra
  rdoQ(0) = OrdenNuevo
  rdoQ(1) = cod
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    objApp.RollbackTrans
    CambiarOrdenGrupo = False
    Exit Function
  End If
  
' Se cambia el orden del nodo del que se parte
  rdoQ(0) = OrdenGrupo
  rdoQ(1) = nodo.Tag
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    objApp.RollbackTrans
    CambiarOrdenGrupo = False
    Exit Function
  End If
   
' Se reconstruye el TreeView
  Call CargarCuestionarioPr
  
  objApp.CommitTrans

End Function

Function CambiarOrdenPreg(nodo As Node) As Integer
Dim sql As String
Dim rdoQ As rdoQuery
Dim pos As Integer
Dim OrdenNuevo As Integer, cGrupo As Long

  pos = Val(nodo.Text)
  CambiarOrdenPreg = True
  OrdenNuevo = Val(nodo.Text)
  cGrupo = nodo.Parent.Tag
  
  objApp.BeginTrans
  sql = "UPDATE UI0400 SET UI04ORDEN = ? WHERE UI03CODGRUPO = ? AND PR40CODPREGUNTA = ?"
  Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
  
' Se cambia primero el orden al nodo al que se arrastra
  rdoQ(0) = OrdenNuevo
  rdoQ(1) = cGrupo
  rdoQ(2) = cod
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    objApp.RollbackTrans
    CambiarOrdenPreg = False
    Exit Function
  End If
  
' Se cambia el orden del nodo del que se parte
  rdoQ(0) = OrdenPreg
  rdoQ(1) = cGrupo
  rdoQ(2) = nodo.Tag
  rdoQ.Execute
  If rdoQ.RowsAffected = 0 Then
    objApp.RollbackTrans
    CambiarOrdenPreg = False
    Exit Function
  End If
   
' Se reconstruye el TreeView
  Call CargarCuestionarioPr
  
  tvwPr.Nodes("P" & rdoQ(2) & "G" & cGrupo).EnsureVisible
  objApp.CommitTrans

End Function

Private Sub tvwPreg_NodeClick(ByVal Node As ComctlLib.Node)
Dim sql As String
Dim rdo As rdoResultset
Dim rdoQ As rdoQuery
Dim nodo As Node

  If Node.Children = 0 Then
    Screen.MousePointer = 11
    sql = "SELECT PR.cResultado, PR.Designacion " _
        & "FROM PruebasResultados PR WHERE " _
        & "PR.cPrueba = ? " _
        & "ORDER BY PR.cResultado"
    Set rdoQ = objApp.rdoConnect.CreateQuery("", sql)
    rdoQ(0) = Node.Tag
    Set rdo = rdoQ.OpenResultset(rdOpenForwardOnly)
    While rdo.EOF = False
      Set nodo = tvwPreg.Nodes.Add(Node.Key, tvwChild, "Y" & rdo(0) & Node.Key, rdo(1), hcICONRESLAB)
      nodo.Tag = rdo(0)
      rdo.MoveNext
    Wend
    Screen.MousePointer = 0
  End If
End Sub
