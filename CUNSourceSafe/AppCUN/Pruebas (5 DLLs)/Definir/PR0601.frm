VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form FrmCuestNoResp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PRUEBAS. Pacientes con Cuestionario Pendiente de Responder"
   ClientHeight    =   8340
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   11910
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "PR0601.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdcuest 
      Caption         =   "Preguntas"
      Height          =   375
      Left            =   10920
      TabIndex        =   12
      Top             =   2160
      Width           =   975
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Unidades Interdepartamentales / Paquetes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3495
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   10695
      Begin TabDlg.SSTab tabTab1 
         Height          =   3015
         HelpContextID   =   90001
         Index           =   0
         Left            =   240
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   10410
         _ExtentX        =   18362
         _ExtentY        =   5318
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0601.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "txtText1(2)"
         Tab(0).Control(1)=   "txtText1(1)"
         Tab(0).Control(2)=   "txtText1(0)"
         Tab(0).Control(3)=   "lbllabel1(1)"
         Tab(0).Control(4)=   "lbllabel1(2)"
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0601.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR33CODESTANDARD"
            Height          =   330
            Index           =   2
            Left            =   -74760
            MultiLine       =   -1  'True
            TabIndex        =   6
            Tag             =   "Paquete | Paquete"
            Top             =   1320
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR33DESESTANDARD"
            Height          =   330
            Index           =   1
            Left            =   -73440
            MultiLine       =   -1  'True
            TabIndex        =   7
            Tag             =   "Paquete | Paquete"
            Top             =   1320
            Width           =   7800
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "UI01DESIG"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   -74760
            TabIndex        =   5
            Tag             =   "Unidad Interdepartamental | U.I."
            Top             =   600
            Width           =   9120
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2565
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   120
            Width           =   9735
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17171
            _ExtentY        =   4524
            _StockProps     =   79
            Caption         =   "Unidades Interdepartamentales / Paquetes"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Unidad Interdepartamental"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   -74760
            TabIndex        =   11
            Top             =   360
            Width           =   2895
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Paquete"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   -74760
            TabIndex        =   9
            Top             =   1080
            Width           =   2775
         End
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones con Cuestionario sin Responder"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3960
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   4080
      Width           =   11670
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3345
         Index           =   1
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   11400
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   20108
         _ExtentY        =   5900
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmCuestNoResp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR0601.FRM                                                   *
'* AUTOR: JESUS MARIA RODILLA LARA                                      *
'* FECHA: 23 DE DICIEMBRE DE 1998                                       *
'* DESCRIPCION: Para cada unidad interdepartamental muestra todos los   *
'*              paquetes pedidos que tienen actuaciones con preguntas   *
'*              del cuestionario (interdepatamental) no respondidas.    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1



Private Sub cmdcuest_Click()
Dim rsta As rdoResultset
Dim stra As String

cmdcuest.Enabled = False
If grdDBGrid1(1).Columns(7).Value = "" Then
    Call MsgBox("No hay ninguna actuaci�n seleccionada.", vbInformation, "Aviso")
Else
    stra = "SELECT COUNT(*) FROM UI0400 WHERE PR04NUMACTPLAN=" & grdDBGrid1(1).Columns(7).Value
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.rdoColumns(0).Value > 0 Then
        Call objsecurity.LaunchProcess("PR0602")
    Else
        Call MsgBox("La actuaci�n no tiene cuestionario asociado.", vbInformation, "Aviso")
    End If
End If
cmdcuest.Enabled = True
End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  'If gblnPrimeraLlamada = True Then
    Call Seleccionar_Dpto
    If gblnCancelar Then
        Unload Me
        Exit Sub
    End If
   ' gblnPrimeraLlamada = False
  'End If
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objMasterInfo
    .strName = "Unidades Interdepartamentales"
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithTab + cwWithKeys
    .strTable = "UI0410J"
    .intAllowance = cwAllowReadOnly
    strKey = .strDataBase & .strTable
    
    Call .FormAddOrderField("UI01DESIG ", cwAscending)
     
    Call .FormCreateFilterWhere("UI0410J", "Unidades Interdepartamentales")
    Call .FormAddFilterWhere("UI0410J", "UI01DESIG", "Unidad Interdepartamental", cwString)
    Call .FormAddFilterWhere("UI0410J", "PR33DESESTANDARD", "Paquete", cwNumeric)
    
    Call .FormAddFilterOrder("UI0410J", "UI01DESIG", "Unidad Interdepartamental")
    Call .FormAddFilterOrder("UI0410J", "PR33DESESTANDARD", "Paquete")
    
  End With
  
  With objMultiInfo
    .strName = "Actuaciones"
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = fraframe1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strTable = "PR0430J"
    .intAllowance = cwAllowReadOnly
    .strWhere = "AD02CODDPTO = " & glngdptologin
    Call .FormAddOrderField("PACIENTE", cwAscending)
    Call .FormAddRelation("PR33CODESTANDARD", txtText1(2))
    
    strKey = .strDataBase & .strTable
       
    'Se establecen los campos por los que se puede filtrar
    Call .FormCreateFilterWhere("PR0430J", "Actuaciones Sin Contestar")
    Call .FormAddFilterWhere("PR0430J", "PACIENTE", "Paciente", cwString)
    Call .FormAddFilterWhere("PR0430J", "CI22NUMHISTORIA", "N� Historia", cwNumeric)
    Call .FormAddFilterWhere("PR0430J", "PR01DESCORTA", "Actuaci�n", cwString)
    Call .FormAddFilterWhere("PR0430J", "PR04FECFINACT", "Fecha", cwDate)
        
    Call .FormAddFilterOrder("PR0430J", "PACIENTE", cwAscending)
    Call .FormAddFilterOrder("PR0430J", "CI22NUMHISTORIA", cwAscending)
    Call .FormAddFilterOrder("PR0430J", "PR01DESCORTA", cwAscending)
    Call .FormAddFilterOrder("PR0430J", "PR04FECFINACT", cwAscending)
        
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
           
    Call .GridAddColumn(objMultiInfo, "Paciente", "PACIENTE", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "N� Historia", "CI22NUMHISTORIA", cwNumeric, 7)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n", "PR01DESCORTA", cwString, 30)
    Call .GridAddColumn(objMultiInfo, "Fecha", "PR04FECFINACT", cwDate, 10)
    Call .GridAddColumn(objMultiInfo, "Actuaci�n Planificada", "PR04NUMACTPLAN", cwNumeric, 9)
     
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(txtText1(2)).blnInGrid = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
   
  grdDBGrid1(0).Columns(1).Width = 5000
  grdDBGrid1(0).Columns(2).Width = 5000
  
  grdDBGrid1(1).Columns(3).Width = 3000
  grdDBGrid1(1).Columns(4).Width = 1000
  grdDBGrid1(1).Columns(5).Width = 5500
  grdDBGrid1(1).Columns(6).Width = 900
  
  grdDBGrid1(1).Columns(7).Visible = False
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
On Error GoTo SalirCuestNoResp
  intCancel = objWinInfo.WinExit
SalirCuestNoResp:
  'Se sale del form
End Sub

Private Sub Form_Unload(intCancel As Integer)
On Error GoTo SalirCuestNoResp
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
SalirCuestNoResp:
  'Se sale del form
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer, _
                            Value As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub




