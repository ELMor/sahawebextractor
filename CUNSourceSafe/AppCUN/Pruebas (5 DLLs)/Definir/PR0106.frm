VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmDefGrupos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Grupos"
   ClientHeight    =   5145
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0106.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5145
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdSeleccAct 
      Caption         =   "Seleccionar"
      DragIcon        =   "PR0106.frx":000C
      Height          =   375
      Left            =   6120
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   7680
      Width           =   1935
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Actuaciones del Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Index           =   2
      Left            =   360
      TabIndex        =   4
      Top             =   2760
      Width           =   10935
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   4215
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   10650
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         UseDefaults     =   0   'False
         _ExtentX        =   18785
         _ExtentY        =   7435
         _StockProps     =   79
         Caption         =   "ACTUACIONES DEL GRUPO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   0
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   10935
      Begin TabDlg.SSTab tabTab1 
         Height          =   1695
         HelpContextID   =   90001
         Index           =   0
         Left            =   240
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   360
         Width           =   10410
         _ExtentX        =   18362
         _ExtentY        =   2990
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0106.frx":044E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lbllabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lbllabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lbllabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblDesTipGrupo"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "cboSSDBCombo1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtDesTipGrupo"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).ControlCount=   8
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0106.frx":046A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtDesTipGrupo 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   3720
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Descripci�n Grupo"
            Top             =   1200
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR16CODGRUPO"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   1200
            TabIndex        =   1
            Tag             =   "C�digo Grupo"
            Top             =   480
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "PR16DESGRUPO"
            Height          =   330
            Index           =   1
            Left            =   3720
            MultiLine       =   -1  'True
            TabIndex        =   2
            Tag             =   "Descripci�n Grupo"
            Top             =   480
            Width           =   5400
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1485
            Index           =   0
            Left            =   -74880
            TabIndex        =   11
            TabStop         =   0   'False
            Top             =   120
            Width           =   9735
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17171
            _ExtentY        =   2619
            _StockProps     =   79
            Caption         =   "GRUPOS"
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "PR15CODTIPGRUPO"
            Height          =   330
            Index           =   0
            Left            =   1200
            TabIndex        =   3
            Tag             =   "Tipo de Grupo"
            Top             =   1200
            Width           =   660
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   820
            Columns(0).Caption=   "COD"
            Columns(0).Name =   "CODIGO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   9525
            Columns(1).Caption=   "DESCRIPCION"
            Columns(1).Name =   "DESCRIPCION"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   1164
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblDesTipGrupo 
            Caption         =   "Descripci�n Tipo de Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   3720
            TabIndex        =   16
            Top             =   960
            Width           =   3015
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Descripci�n Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   3720
            TabIndex        =   14
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label lbllabel1 
            Caption         =   "C�digo Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1200
            TabIndex        =   13
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label lbllabel1 
            Caption         =   "Tipo Grupo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   1200
            TabIndex        =   12
            Top             =   960
            Width           =   1095
         End
      End
   End
   Begin VB.CommandButton cmdcuestionario 
      Caption         =   "Cuestionario"
      Height          =   375
      Left            =   3960
      TabIndex        =   5
      Top             =   7680
      Width           =   1935
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   9
      Top             =   4860
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   "Tipo Grupo"
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00106.FRM                                                  *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: 14 DE AGOSTO DE 1997                                          *
'* DESCRIPCION: permite crear nuevos grupos de actuaciones. Tambi�n     *
'*              se puede modificar un grupo                             *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim refrescar As Boolean


'INTERACCIONES CON UNA ACTUACION (como origen)
'la act.origen ser� la reci�n a�adida al grupo y
'las actuaciones destino ser�n aquellas con las que interacciona el grupo
Private Sub grpO_act()
    Dim sqlstrX As String
    Dim sqlstrins As String
    Dim sqlstrsel As String
    Dim codgrp As Long
    Dim codgrp_des As Long
    Dim codact As Long
    Dim codact_des As Long
    Dim numint As Long
    Dim codtipint As Integer
    Dim tiempo As Long
    Dim rsta As rdoResultset
    Dim rstB As rdoResultset
    Dim rstX As rdoResultset
    codgrp = txtText1(0)
    'codact = txtText1(2)
    codact = grdDBGrid1(0).Columns(3).Value
    sqlstrsel = "SELECT pr18numdefinter,pr01codactuacion_des,pr19codtipinterac,pr18numtiempoint " _
              & "FROM pr1800 " _
              & "WHERE pr16codgrupo = " & codgrp _
              & " and pr01codactuacion_des is not NULL"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstrsel)
    With rsta
        Do Until rsta.EOF
            numint = .rdoColumns(0)
            codact_des = .rdoColumns(1)
            codtipint = .rdoColumns(2)
            tiempo = .rdoColumns(3)
            'a�ado la entrada correspondiente
            sqlstrins = "INSERT INTO PR2000 (PR01CODACTUACION,PR01CODACTUACION_DES," _
                        & "PR19CODTIPINTERAC,PR20NUMTIEMPOINT,PR18NUMDEFINTER) VALUES " _
                          & "(" & codact & "," & codact_des & "," & codtipint & "," _
                          & tiempo & "," & numint & ")"
            sqlstrX = "SELECT * " _
                    & "FROM PR2000 " _
                    & "WHERE pr18numdefinter = " & numint _
                     & " AND pr01codactuacion = " & codact _
                     & " AND pr01codactuacion_des = " & codact_des
            Set rstX = objApp.rdoConnect.OpenResultset(sqlstrX)
            'Antes de insertar el registro verifico si ya existe, ya que
            'al borrar una actuaci�n de un grupo no se borran sus interacciones.
            If rstX.EOF Then
                objApp.rdoConnect.Execute sqlstrins, 64
            End If
            rstX.Close
            Set rstX = Nothing
            rsta.MoveNext
        Loop
    End With
    rsta.Close
    Set rsta = Nothing
End Sub

'INTERACCIONES CON UNA ACTUACION (como destino)
'las actuaciones origen ser�n aquellas con las que interacciona el grupo y
'la act.destino ser� la reci�n a�adida al grupo
Private Sub act_grpD()
    Dim sqlstrX As String
    Dim sqlstrins As String
    Dim sqlstrsel As String
    Dim codgrp As Long
    Dim codgrp_des As Long
    Dim codact As Long
    Dim codact_des As Long
    Dim numint As Long
    Dim codtipint As Integer
    Dim tiempo As Long
    Dim rsta As rdoResultset
    Dim rstB As rdoResultset
    Dim rstX As rdoResultset
    codgrp_des = txtText1(0)
    codact_des = grdDBGrid1(0).Columns(3).Value
    sqlstrsel = "SELECT pr18numdefinter,pr01codactuacion,pr19codtipinterac,pr18numtiempoint " _
              & "FROM pr1800 " _
              & "WHERE pr16codgrupo_des = " & codgrp_des _
                   & " and pr01codactuacion is not NULL"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstrsel)
    With rsta
        Do Until rsta.EOF
            numint = .rdoColumns(0)
            codact = .rdoColumns(1)
            codtipint = .rdoColumns(2)
            tiempo = .rdoColumns(3)
            'a�ado la entrada correspondiente
            sqlstrins = "INSERT INTO PR2000 (PR01CODACTUACION,PR01CODACTUACION_DES," _
                        & "PR19CODTIPINTERAC,PR20NUMTIEMPOINT,PR18NUMDEFINTER) VALUES " _
                      & "(" & codact & "," & codact_des & "," & codtipint & "," _
                            & tiempo & "," & numint & ")"
            sqlstrX = "SELECT * " _
                    & "FROM PR2000 " _
                    & "WHERE pr18numdefinter = " & numint _
                     & " AND pr01codactuacion = " & codact _
                     & " AND pr01codactuacion_des= " & codact_des
            Set rstX = objApp.rdoConnect.OpenResultset(sqlstrX)
            'Antes de insertar el registro verifico si ya existe, ya que
            'al borrar una actuaci�n de un grupo no se borran sus interacciones.
            If rstX.EOF Then
                objApp.rdoConnect.Execute sqlstrins, 64
            End If
            rstX.Close
            Set rstX = Nothing
            rsta.MoveNext
        Loop
    End With
    rsta.Close
    Set rsta = Nothing
End Sub

'INTERACCIONES CON UN GRUPO (como origen)
'la act.origen ser� la reci�n a�adida al grupo y
'las actuaciones destino ser�n las de los grupos con los que interacciona el grupo
Private Sub grpO_grp()
    Dim sqlstrX As String
    Dim sqlstrins As String
    Dim sqlstrsel As String
    Dim codgrp As Long
    Dim codgrp_des As Long
    Dim codact As Long
    Dim codact_des As Long
    Dim numint As Long
    Dim codtipint As Integer
    Dim tiempo As Long
    Dim rsta As rdoResultset
    Dim rstB As rdoResultset
    Dim rstX As rdoResultset
    codgrp = txtText1(0)
    codact = grdDBGrid1(0).Columns(3).Value
    sqlstrsel = "SELECT pr18numdefinter,pr16codgrupo_des,pr19codtipinterac,pr18numtiempoint " _
              & "FROM pr1800 " _
              & "WHERE pr16codgrupo = " & codgrp _
                    & " and pr16codgrupo_des is not NULL"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstrsel)
    With rsta
        Do Until rsta.EOF
            numint = .rdoColumns(0)
            codtipint = .rdoColumns(2)
            tiempo = .rdoColumns(3)
            codgrp_des = .rdoColumns(1)
           'a�ado las entradas correspondientes
            sqlstrsel = "SELECT pr01codactuacion " _
                      & "FROM pr1700 " _
                      & "WHERE pr16codgrupo = " & codgrp_des
            Set rstB = objApp.rdoConnect.OpenResultset(sqlstrsel)
            Do Until rstB.EOF
                codact_des = rstB.rdoColumns(0)
                sqlstrins = "INSERT INTO PR2000 (PR01CODACTUACION,PR01CODACTUACION_DES," _
                        & "PR19CODTIPINTERAC,PR20NUMTIEMPOINT,PR18NUMDEFINTER) VALUES " _
                          & "(" & codact & "," & codact_des & "," & codtipint & "," _
                                & tiempo & "," & numint & ")"
                sqlstrX = "SELECT * " _
                    & "FROM PR2000 " _
                    & "WHERE pr18numdefinter = " & numint _
                     & " AND pr01codactuacion = " & codact _
                     & " AND pr01codactuacion_des = " & codact_des
                Set rstX = objApp.rdoConnect.OpenResultset(sqlstrX)
                'Antes de insertar el registro verifico si ya existe, ya que
                'al borrar una actuaci�n de un grupo no se borran sus interacciones.
                If rstX.EOF Then
                    objApp.rdoConnect.Execute sqlstrins, 64
                End If
                rstX.Close
                Set rstX = Nothing
                rstB.MoveNext
            Loop
            rstB.Close
            Set rstB = Nothing
            rsta.MoveNext
        Loop
    End With
    rsta.Close
    Set rsta = Nothing
End Sub

'INTERACCIONES CON UN GRUPO (como destino)
'las actuaciones origen ser�n las de los grupos con los que interacciona el grupo y
'la act.destino ser� la reci�n a�adida al grupo
Private Sub grp_grpD()
    Dim sqlstrX As String
    Dim sqlstrins As String
    Dim sqlstrsel As String
    Dim codgrp As Long
    Dim codgrp_des As Long
    Dim codact As Long
    Dim codact_des As Long
    Dim numint As Long
    Dim codtipint As Integer
    Dim tiempo As Long
    Dim rsta As rdoResultset
    Dim rstB As rdoResultset
    Dim rstX As rdoResultset
    codgrp_des = txtText1(0)
    codact_des = grdDBGrid1(0).Columns(3).Value
    sqlstrsel = "SELECT pr18numdefinter,pr16codgrupo,pr19codtipinterac,pr18numtiempoint " _
              & "FROM pr1800 " _
              & "WHERE pr16codgrupo_des = " & codgrp_des _
                    & " and pr16codgrupo is not NULL"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstrsel)
    With rsta
        Do Until rsta.EOF
            numint = .rdoColumns(0)
            codtipint = .rdoColumns(2)
            tiempo = .rdoColumns(3)
            codgrp = .rdoColumns(1)
            'a�ado las entradas correspondientes
            sqlstrsel = "SELECT pr01codactuacion " _
                      & "FROM pr1700 " _
                      & "WHERE pr16codgrupo = " & codgrp
            Set rstB = objApp.rdoConnect.OpenResultset(sqlstrsel)
            Do Until rstB.EOF
                codact = rstB.rdoColumns(0)
                sqlstrins = "INSERT INTO PR2000 (PR01CODACTUACION,PR01CODACTUACION_DES," _
                        & "PR19CODTIPINTERAC,PR20NUMTIEMPOINT,PR18NUMDEFINTER) VALUES " _
                          & "(" & codact & "," & codact_des & "," & codtipint & "," _
                                & tiempo & "," & numint & ")"
                sqlstrX = "SELECT * " _
                        & "FROM PR2000 " _
                        & "WHERE pr18numdefinter = " & numint _
                         & " AND pr01codactuacion = " & codact _
                         & " AND pr01codactuacion_des = " & codact_des
                Set rstX = objApp.rdoConnect.OpenResultset(sqlstrX)
                'Antes de insertar el registro verifico si ya existe, ya que
                'al borrar una actuaci�n de un grupo no se borran sus interacciones.
                If rstX.EOF Then
                    objApp.rdoConnect.Execute sqlstrins, 64
                End If
                rstX.Close
                Set rstX = Nothing
                rstB.MoveNext
            Loop
            rstB.Close
            Set rstB = Nothing
            rsta.MoveNext
        Loop
    End With
    rsta.Close
    Set rsta = Nothing
End Sub


Private Sub cmdcuestionario_Click()
    cmdcuestionario.Enabled = False
    objWinInfo.DataSave
    'Pasamos los campos necesarios en la pantalla del cuestionario
    frmCuestGrupo.txtCuestext1(0).Text = txtText1(0).Text
    frmCuestGrupo.txtCuestext1(1).Text = txtText1(1).Text
    'Cargamos la pantalla del cuestionario
    'Load frmCuestGrupo
    'frmCuestGrupo.Show (vbModal)
    'Descargamos la pantalla del cuestionario
    'Unload frmCuestGrupo
    'Set frmCuestGrupo = Nothing
    Call objsecurity.LaunchProcess("PR0110")
    cmdcuestionario.Enabled = True
End Sub

Private Sub cmdSeleccAct_Click()
  Dim vResp As Variant
  
  If txtText1(0).Text <> "" Then
   objWinInfo.DataSave
   'Load frmSeleccionarActGrupo
   'frmSeleccionarActGrupo.Show (vbModal)
   'Unload frmSeleccionarActGrupo
   'Set frmSeleccionarActGrupo = Nothing
   Call objsecurity.LaunchProcess("PR0169")
   objWinInfo.DataRefresh
  Else
    vResp = MsgBox("Primero debe seleccionar un grupo", vbInformation)
  End If
   
End Sub

Private Sub Form_Activate()
    txtText1(0).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  
  
    With objMasterInfo
        .strName = "Grupos"
        Set .objFormContainer = fraframe1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(0)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR1600"
        strKey = .strDataBase & .strTable
        
        Call .objPrinter.Add("PR1061", "Listado de grupos y actuaciones")
        Call .objPrinter.Add("PR1062", "Listado de cuestionario de un grupo")
        
        'Se establecen los campos por los que se puede filtrar
        Call .FormAddOrderField("PR16codgrupo", cwAscending)
        .blnHasMaint = True
        Call .FormCreateFilterWhere(strKey, "Grupos")
        Call .FormAddFilterWhere(strKey, "PR16CODGRUPO", "C�digo Grupo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR16DESGRUPO", "Descripci�n Grupo", cwString)
        Call .FormAddFilterWhere(strKey, "PR15CODTIPGRUPO", "C�digo Tipo Grupo", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR16CODGRUPO", "C�digo Grupo")
        Call .FormAddFilterOrder(strKey, "PR16DESGRUPO", "Descripci�n")
        Call .FormAddFilterOrder(strKey, "PR15CODTIPGRUPO", "C�digo Tipo Grupo")
    End With
  
  
    With objMultiInfo
        .strName = "Actuaciones del grupo"
        Set .objFormContainer = fraframe1(2)
        Set .objFatherContainer = fraframe1(0)
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(2)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
  
        '.strDataBase = objEnv.GetValue("Main")
        .strTable = "PR1700"
        .intAllowance = cwAllowAll
        .intCursorSize = 0
         
        Call .FormAddOrderField("PR16codgrupo", cwAscending)
        Call .FormAddRelation("PR16codgrupo", txtText1(0))
 
        strKey = .strDataBase & .strTable
        'Se establecen los campos por los que se puede filtrar
        Call .FormCreateFilterWhere(strKey, "Actuaciones del Grupo")
        Call .FormAddFilterWhere(strKey, "PR16CODGRUPO", "C�digo Grupo", cwNumeric)
        Call .FormAddFilterWhere(strKey, "PR01CODACTUACION", "C�digo Actuaci�n", cwNumeric)
        
        'Se establecen los campos por los que se puede ordenar con el filtro
        Call .FormAddFilterOrder(strKey, "PR35CODGRUPO", "C�digo Grupo")
        Call .FormAddFilterOrder(strKey, "PR01CODACTUACION", "C�digo Actuaci�n")
    End With

    With objWinInfo

        

        Call .FormAddInfo(objMasterInfo, cwFormDetail)
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
        
        'Se indican las columnas que aparecer�n en el grid que contiene las actuaciones del grupo
        Call .GridAddColumn(objMultiInfo, "C�d. Actuaci�n", "PR01CODACTUACION", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "Actuaci�n", "", cwString, 30)
        Call .GridAddColumn(objMultiInfo, "C�d. Grupo", "PR16CODGRUPO", cwNumeric, 5)
        Call .GridAddColumn(objMultiInfo, "Grupo", "", cwString, 30)

        Call .FormCreateInfo(objMasterInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(txtText1(0)).blnInFind = True
        .CtrlGetInfo(txtText1(1)).blnInFind = True
        '.CtrlGetInfo(cboSSDBCombo1(0)).blnInFind = True '20/11 da problemas
        
        'Se indica que campos son obligatorios y cuales son clave primaria
        'en el grid que contiene las actuaciones del grupo
        .CtrlGetInfo(grdDBGrid1(2).Columns(3)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnMandatory = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(5)).intKeyNo = 1
        .CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnMandatory = True
       
        Call .FormChangeColor(objMultiInfo)
        
        'Se indican los campos por los que se desea buscar
        .CtrlGetInfo(grdDBGrid1(2).Columns(3)).blnInFind = True
        .CtrlGetInfo(grdDBGrid1(2).Columns(4)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(2).Columns(5)).blnInFind = True
        '.CtrlGetInfo(grdDBGrid1(2).Columns(6)).blnInFind = True
        

        'A�adinos las columnas que tienen las descripciones de los c�digos en el grid que
        'contiene las actuaciones del grupo
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), "PR01CODACTUACION", "SELECT * FROM PR0100 WHERE PR01CODACTUACION = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(3)), grdDBGrid1(2).Columns(4), "PR01DESCORTA")
    
        Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), "PR16CODGRUPO", "SELECT * FROM PR1600 WHERE PR16CODGRUPO = ?")
        Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(2).Columns(5)), grdDBGrid1(2).Columns(6), "PR16DESGRUPO")
    
        
        'Para sacar el Search del tipo de grupo
        '.CtrlGetInfo(cboSSDBCombo1(0)).blnForeign = True
        .CtrlGetInfo(cboSSDBCombo1(0)).strsql = "SELECT PR15CODTIPGRUPO,PR15DESTIPGRUPO FROM PR1500 ORDER BY PR15CODTIPGRUPO ASC"
        
        Call .WinRegister
        Call .WinStabilize
    End With
    gstrLlamadorSel = "Grupos"
    'Call objApp.SplashOff
    'Call objWinInfo.WinProcess(6, 24, 0)

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    Call objsecurity.LaunchProcess("PR0202")
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  Dim rsta As rdoResultset
  Dim strsqlA As String
  
  If (strFormName = "Grupos") And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
    strsqlA = "SELECT PR15DESTIPGRUPO " _
            & "  FROM PR1500 " _
            & " WHERE PR15CODTIPGRUPO = " & cboSSDBCombo1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
    txtDesTipGrupo.Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If
  txtDesTipGrupo.Locked = True
  
  If txtText1(0).Text = "" Then
    cmdcuestionario.Enabled = False
  Else
    cmdcuestionario.Enabled = True
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Grupos" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)

    'Para cada interacci�n en que aparezca el grupo (como origen o como destino),
    'a�ado en PR2000 la(s) entrada(s) correspondiente(s)
    Dim sqlstrX As String
    Dim sqlstrins As String
    Dim sqlstrsel As String
    Dim codgrp As Long
    Dim codgrp_des As Long
    Dim codact As Long
    Dim codact_des As Long
    Dim numint As Long
    Dim codtipint As Integer
    Dim tiempo As Long
    Dim rsta As rdoResultset
    Dim rstB As rdoResultset
    Dim rstX As rdoResultset

    If Not blnError Then
        On Error GoTo Err_Ejecutar
        If objWinInfo.objWinActiveForm.strName = "Actuaciones" Then
            grpO_act

            act_grpD

            grpO_grp

            grp_grpD

            'Valido los cambios
            objApp.rdoConnect.Execute "Commit", 64
        End If
        cmdcuestionario.Enabled = True
    End If
    Exit Sub

Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub

End Sub

'Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'  If (strFormName = "Grupos") And (strCtrl = "cboSSDBCombo1(0)") Then
'    frmtipodegrupo.cmdaceptar.Visible = True
'    'Load frmtipodegrupo
'    'Call frmtipodegrupo.Show(vbModal)
'    Call objsecurity.LaunchProcess("PR0202")
'    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)), guCodigo)
'    Call objWinInfo.CtrlSet(cboSSDBCombo1(0), guCodigo)
'    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtAct(2)), gstrdescripcion)
'    'Unload frmtipodegrupo
'    'Set frmtipodegrupo = Nothing
'    Call objCW.objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDBCombo1(0)))
'  End If
' guCodigo = ""
' gstrdescripcion = ""
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_GotFocus(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim strsqlA As String
  
  If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
    strsqlA = "SELECT PR15DESTIPGRUPO " _
            & "  FROM PR1500 " _
            & " WHERE PR15CODTIPGRUPO = " & cboSSDBCombo1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
    txtDesTipGrupo.Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If
  txtDesTipGrupo.Locked = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  
  If (btnButton.Index = 8) And (objWinInfo.objWinActiveForm.strName = "Grupos") Then
    If (Tiene_Cuestionario = False) Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
  Else
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
  
    
    If btnButton.Index = 2 Then
        cmdcuestionario.Enabled = False
        On Error GoTo Err_Ejecutar
        ' generaci�n autom�tica del c�digo
        sqlstr = "SELECT PR16CODGRUPO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0) = rsta.rdoColumns(0).Value
        txtText1(0).Locked = True
        'txtText1(1).SetFocus
        rsta.Close
        Set rsta = Nothing
  End If
 Exit Sub
 
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  
  If (intIndex = 60) And (objWinInfo.objWinActiveForm.strName = "Grupos") Then
    If (Tiene_Cuestionario = False) Then
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End If
  Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End If
    
    
    If intIndex = 10 Then
        cmdcuestionario.Enabled = False
        On Error GoTo Err_Ejecutar
        ' generaci�n autom�tica del c�digo
        sqlstr = "SELECT PR16CODGRUPO_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0) = rsta.rdoColumns(0).Value
        txtText1(0).Locked = True
        'txtText1(1).SetFocus
        rsta.Close
        Set rsta = Nothing
    End If
 Exit Sub
 
Err_Ejecutar:
  MsgBox "Error: " & Err.Number & " " & Err.Description
  Exit Sub
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)

    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(lbllabel1(intIndex).Container, False, True)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim strsqlA As String
  
  Call objWinInfo.CtrlDataChange
      
  If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
    strsqlA = "SELECT PR15DESTIPGRUPO " _
            & "  FROM PR1500 " _
            & " WHERE PR15CODTIPGRUPO = " & cboSSDBCombo1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
    txtDesTipGrupo.Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If
  txtDesTipGrupo.Locked = True

End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(intIndex As Integer)

  Dim rsta As rdoResultset
  Dim strsqlA As String
  
  Call objWinInfo.CtrlDataChange
      
  If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
    strsqlA = "SELECT PR15DESTIPGRUPO " _
            & "  FROM PR1500 " _
            & " WHERE PR15CODTIPGRUPO = " & cboSSDBCombo1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
    txtDesTipGrupo.Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If
  txtDesTipGrupo.Locked = True

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Dim rsta As rdoResultset
  Dim strsqlA As String
  
  Call objWinInfo.CtrlGotFocus
      
  If (intIndex = 0) And (Not IsNull(cboSSDBCombo1(0).Text)) And (cboSSDBCombo1(0).Text <> "") Then
    strsqlA = "SELECT PR15DESTIPGRUPO " _
            & "  FROM PR1500 " _
            & " WHERE PR15CODTIPGRUPO = " & cboSSDBCombo1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(strsqlA)
    txtDesTipGrupo.Text = rsta.rdoColumns(0).Value
    rsta.Close
    Set rsta = Nothing
  End If
  txtDesTipGrupo.Locked = True

End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Public Function Tiene_Cuestionario() As Boolean
'JMRL 4/12/97
'Tiene_Cuestionario es una funci�n que devuelve TRUE si hay ning�n problema a la hora de
'borrar un grupo.

  Dim rstF As rdoResultset
  Dim strSelect As String
  Dim blnSacarMensaje As Boolean
  Dim intResp As Integer
  Dim strmensaje As String
  
  
  strmensaje = "El grupo NO puede se borrado por: " & Chr(13) & Chr(13)
  blnSacarMensaje = False
    
  'Se comprueba que el grupo no tiene CUESTIONARIO
  strSelect = "SELECT COUNT(*) FROM PR3000" & _
              " WHERE PR16CODGRUPO=" & txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� El grupo TIENE CUESTIONARIO." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
      
  'Se comprueba que el grupo no tiene ACTUACIONES
  strSelect = "SELECT COUNT(*) FROM PR1700" & _
              " WHERE PR16CODGRUPO=" & txtText1(0).Text
  Set rstF = objApp.rdoConnect.OpenResultset(strSelect)
  If rstF.rdoColumns(0).Value > 0 Then
    strmensaje = strmensaje & "� El grupo TIENE ACTUACIONES." & Chr(13)
    blnSacarMensaje = True
  End If
  rstF.Close
  Set rstF = Nothing
  
  If blnSacarMensaje = True Then
    intResp = MsgBox(strmensaje, vbInformation, "Importante")
    Tiene_Cuestionario = True
  Else
    Tiene_Cuestionario = False
  End If
  

End Function

