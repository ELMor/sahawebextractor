Attribute VB_Name = "modUIConstantes"
Option Explicit

' Constantes para los iconos de preguntas-respuestas
Global Const hcICONPR = 1 ' Pruebas
Global Const hcICONTIPOSPREG = 2 ' Tipos de preguntas
Global Const hcICONTIPOPREG = 3 ' Tipo de preguntas
Global Const hcICONPREGS = 4 ' Preguntas
Global Const hcICONPREG = 5 ' Pregunta
Global Const hcICONLISTAS = 6 ' Listas de respuestas
Global Const hcICONLISTA = 7 ' Lista de respuestas
Global Const hcICONRESS = 8 ' Respuestas (elementos de la lista de respuestas)
Global Const hcICONRES = 9 ' Respuesta (elementos de la lista de respuestas)
Global Const hcICONPROHIBIDO = 10 ' Prohibido
Global Const hcICONPRUEBALAB = 11 ' Pruebas de laboratorio
Global Const hcICONRESLAB = 12 ' Resultados de laboratorio
Global Const hcICONDPT = 13 ' Departamentos
Global Const hcICONOPERACIONES = 14 ' Operaciones
Global Const hcICONOPERACION = 15 ' Operaci�n
Global Const hcICONMAS = 16 ' Mas
Global Const hcICONMENOS = 17 ' Menos
Global Const hcICONPOR = 18 ' Por
Global Const hcICONDIVIDIR = 19 ' Dividir
Global Const hcICONACTIVIDAD = 20 ' Tipo de actuaci�n
Global Const hcICONPADRE = 21 ' Nodos padre
Global Const hcICONPAQUETE = 22 ' Paquetes
Global Const hcICONGRUPO = 23 ' Grupos
Global Const hcICONPROTOCOLO = 24 ' Protocolo

' Constante para identificar el c�digo del tipo de respuesta 'Lista de respuestas'
Global Const hcCODLISTA = 4

' Listas de c�digos de departamentos de laboratorio
Global Const uiCDPTSLAB = "201, 202, 203, 205"

' Estructura que guarda desde donde se va a llamar para el cuestionario
Type TypeTipoLlamada
  cPr As Long
  Pr As String
  Orden As Integer ' Orden de la actuacion dentro del paquete o protocolo
  cPaq As Long
  Paquete As String
  cDptPaq As Long ' C�digo del departamento propietario del paquete
  DptPaq As String ' Departamento propietario del paquete
  cDptPr As Long ' C�digo del departamento realizador de la actuaci�n
  DptPr As String ' Departamento realizador de la actuaci�n
  cProt As Long
  Protocolo As String
End Type
