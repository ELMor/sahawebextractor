VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Begin VB.Form frmdefrecursovalido 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "GESTI�N DE ACTUACIONES. Definici�n de Actuaciones. Recursos"
   ClientHeight    =   4485
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   9645
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "PR0126.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9645
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Recursos Seleccionados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5175
      Index           =   1
      Left            =   240
      TabIndex        =   0
      Top             =   2880
      Width           =   10980
      Begin TabDlg.SSTab tabTab1 
         Height          =   4500
         Index           =   1
         Left            =   240
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   480
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   7938
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "PR0126.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(12)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(13)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(14)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(5)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(6)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(7)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(5)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(6)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(7)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(8)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(9)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(0)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "PR0126.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   1
            Left            =   3000
            TabIndex        =   34
            Tag             =   "Descripci�n Departamento"
            Top             =   2160
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   0
            Left            =   1800
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   "C�digo del Departamento"
            Top             =   2160
            Visible         =   0   'False
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR13NUMNECESID"
            Height          =   330
            Index           =   9
            Left            =   5880
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Necesidad"
            Top             =   2880
            Visible         =   0   'False
            Width           =   612
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR05NUMFASE"
            Height          =   330
            Index           =   8
            Left            =   5880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   2520
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "PR01CODACTUACION"
            Height          =   330
            Index           =   7
            Left            =   5880
            TabIndex        =   13
            TabStop         =   0   'False
            Top             =   3240
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AD02CODDPTO"
            Height          =   330
            Index           =   6
            Left            =   360
            TabIndex        =   3
            Tag             =   "C�digo del Departamento"
            Top             =   2160
            Width           =   372
         End
         Begin VB.TextBox txtText1 
            Height          =   330
            Index           =   5
            Left            =   3000
            TabIndex        =   2
            Tag             =   "Descripci�n Recurso"
            Top             =   960
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "AG11CODRECURSO"
            Height          =   330
            Index           =   4
            Left            =   360
            TabIndex        =   1
            Tag             =   "C�digo Recurso"
            Top             =   960
            Width           =   612
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2865
            Index           =   1
            Left            =   -74910
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   90
            Width           =   8655
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15266
            _ExtentY        =   5054
            _StockProps     =   79
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4185
            Index           =   2
            Left            =   -74760
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   120
            Width           =   9855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17383
            _ExtentY        =   7382
            _StockProps     =   79
            Caption         =   "RECURSOS SELECCIONADOS"
            BackColor       =   -2147483633
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   3000
            TabIndex        =   35
            Top             =   1920
            Width           =   2895
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Necesidad"
            Height          =   255
            Index           =   7
            Left            =   6480
            TabIndex        =   32
            Top             =   2880
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�mero Fase"
            Height          =   255
            Index           =   6
            Left            =   7080
            TabIndex        =   31
            Top             =   2520
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Actuaci�n"
            Height          =   255
            Index           =   5
            Left            =   7080
            TabIndex        =   30
            Top             =   3240
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   14
            Left            =   3000
            TabIndex        =   28
            Top             =   720
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Departamento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   13
            Left            =   360
            TabIndex        =   27
            Top             =   1920
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Recurso"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   12
            Left            =   360
            TabIndex        =   26
            Top             =   720
            Width           =   1575
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Actuaci�n - Fase - Tipo de Recurso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Index           =   2
      Left            =   240
      TabIndex        =   4
      Top             =   480
      Width           =   10935
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   6
         Left            =   9360
         TabIndex        =   11
         TabStop         =   0   'False
         Tag             =   "N� Necesidad"
         Top             =   1800
         Width           =   612
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   5
         Left            =   3120
         TabIndex        =   10
         TabStop         =   0   'False
         Tag             =   "Descripci�n del Tipo de Recurso"
         Top             =   1800
         Width           =   5400
      End
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   4
         Left            =   240
         TabIndex        =   9
         TabStop         =   0   'False
         Tag             =   "C�digo del Tipo de Recurso"
         Top             =   1800
         Width           =   372
      End
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   5
         TabStop         =   0   'False
         Tag             =   "C�digo de la Actuaci�n"
         Top             =   600
         Width           =   1092
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   1
         Left            =   3120
         Locked          =   -1  'True
         TabIndex        =   6
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Actuaci�n"
         Top             =   600
         Width           =   5400
      End
      Begin VB.TextBox txtactText1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   2
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "C�digo de la Fase"
         Top             =   1200
         Width           =   612
      End
      Begin VB.TextBox txtactText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   3
         Left            =   3120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   8
         TabStop         =   0   'False
         Tag             =   "Descripci�n de la Fase"
         Top             =   1200
         Width           =   5400
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "Necesidad"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   9360
         TabIndex        =   29
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "Descripci�n Tipo Recurso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   11
         Left            =   3120
         TabIndex        =   25
         Top             =   1560
         Width           =   2295
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "C�digo Tipo Recurso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   10
         Left            =   240
         TabIndex        =   24
         Top             =   1560
         Width           =   2055
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "C�digo Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   240
         TabIndex        =   20
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "Descripci�n Actuaci�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   3120
         TabIndex        =   19
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "N�mero Fase"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   18
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label lblactLabel1 
         Caption         =   "Descripci�n Fase"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   3120
         TabIndex        =   17
         Top             =   960
         Width           =   1815
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   4200
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmdefrecursovalido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: PRUEBAS                                                    *
'* NOMBRE: PR00126.FRM                                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: 22 DE AGOSTO DE 1997                                          *
'* DESCRIPCI�N: Descripci�n de los recursos seleccionados para un       *
'*              tipo de recurso                                         *
'* ARGUMENTOS:  PR01CODACTUACION, PR01DESCORTA, PR05NUMFASE,            *
'*              PR05DESFASE, PR13NUMNECESID, AG14CODTIPRECU,            *
'*              AG14DESTIPRECU (por valor)                              *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
' strmensaje para sacar mensajes por pantalla
Dim strmensaje As String

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Activate()
'Ajo ********************* 09/07/1999 No hace falta,
'lo pasa desde tipos de recursos todo la informaci�n.
'Ajo ********* Comentado
'  txtactText1(0).Text = frmfasesimple.txtactText1(0).Text 'C�d. Actuaci�n
'  txtactText1(1).Text = frmfasesimple.txtactText1(1).Text 'Des. Actuaci�n
'  txtactText1(2).Text = frmfasesimple.txtactText1(2).Text 'C�d. Fase
'  txtactText1(3).Text = frmfasesimple.txtactText1(3).Text 'Des. Fase
'  txtactText1(4).Text = frmfasesimple.txtText1(6).Text 'C�d. Tipo Recurso
'  txtactText1(5).Text = frmfasesimple.txtText1(10).Text 'Des. Tipo Recurso
'  txtactText1(6).Text = frmfasesimple.txtText1(8).Text 'Necesidad

  'para activar Guardar cuando se hace Nuevo
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  txtText1(4).SetFocus
  txtText1(5).SetFocus
  txtText1(4).SetFocus
End Sub


Private Sub Form_Load()
  Dim objDetailInfo1 As New clsCWForm
  Dim strKey As String
  Dim objDetailInfo2 As New clsCWForm
  Dim strKey2 As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  

   With objDetailInfo2
    .strName = "Recurso_Valido_Fase"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(2)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "PR1100" 'Recurso V�lido Fase
    .strWhere = "pr01codactuacion=" & frmdefactuacionesCUN.txtText1(0).Text & _
    " and pr05numfase=" & frmdeffases.txtText1(3).Text & _
    " and pr13numnecesid=" & frmdeftiposrecurso.txtText1(8).Text
    
    Call .FormAddOrderField("PR13NUMNECESID", cwAscending)
    Call .FormAddOrderField("AG11CODRECURSO", cwAscending)
    
    'Call .objPrinter.Add("PR001241", "Listado de Tipos de Recurso y Recurso para cada Fase")
    
    .blnHasMaint = True
  
    strKey2 = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey2, "Recursos Seleccionados")
    Call .FormAddFilterWhere(strKey2, "AG11CODRECURSO", "C�digo Recurso", cwNumeric)
    Call .FormAddFilterOrder(strKey2, "AG11CODRECURSO", "C�digo Recurso")
    
  End With
   
  With objWinInfo
    
    Call .FormAddInfo(objDetailInfo2, cwFormDetail)
    Call .FormCreateInfo(objDetailInfo2)
    
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    
    .CtrlGetInfo(txtText1(7)).blnInGrid = False
    .CtrlGetInfo(txtText1(8)).blnInGrid = False
    .CtrlGetInfo(txtText1(9)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(5)).blnInGrid = False
    .CtrlGetInfo(txtText1(6)).blnInGrid = False
    
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True
    '.CtrlGetInfo(txtText1(6)).blnReadOnly = True
    .CtrlGetInfo(txtText1(9)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(4)).blnForeign = True
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "AG11CODRECURSO", "SELECT * FROM AG1100 WHERE AG11CODRECURSO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "AG11DESRECURSO")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(6), "AD02CODDPTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(6)), "AD02CODDPTO", "SELECT * FROM AD0200 WHERE AD02CODDPTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(6)), txtText1(1), "AD02DESDPTO")
    
    Call .WinRegister
    Call .WinStabilize
  End With

  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub lblactLabel1_Click(Index As Integer)
txtText1(4).SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Recurso_Valido_Fase" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub


Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  If strFormName = "Recurso_Valido_Fase" And strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AG1100"
     .strWhere = "WHERE ag14codtiprecu=" & frmdeftiposrecurso.txtText1(6)
     .strOrder = "ORDER BY ag11codrecurso ASC"
         
     Set objField = .AddField("ag11codrecurso")
     objField.strSmallDesc = "C�digo del Recurso"
         
     Set objField = .AddField("ag11desrecurso")
     objField.strSmallDesc = "Descripci�n del Recurso"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(4)), .cllValues("ag11codrecurso"))
      Call objWinInfo.CtrlSet(txtText1(4), .cllValues("ag11codrecurso"))
     End If
   End With
   Set objSearch = Nothing
 End If
  If strFormName = "Recurso_Valido_Fase" And strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "AD0200"
     .strWhere = "WHERE AD02CODDPTO IN (SELECT AD02CODDPTO FROM PR0200 WHERE PR01CODACTUACION=" & txtactText1(0).Text & ") AND AD02FECFIN IS NULL"
     .strOrder = "ORDER BY AD02CODDPTO ASC"
         
     Set objField = .AddField("AD02CODDPTO")
     objField.strSmallDesc = "C�digo del Departamento"
         
     Set objField = .AddField("AD02DESDPTO")
     objField.strSmallDesc = "Descripci�n del Departamento"
         
     If .Search Then
      'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(4)), .cllValues("ag11codrecurso"))
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("AD02CODDPTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim contador As Integer
  
  ' controla que el departamento Nuevo existe antes de hacer Guardar
   If (btnButton.Index = 4) Then
      If (txtText1(4).Text <> "") Then
      sqlstr = "select count(AG11CODRECURSO) from AG1100" & _
       " where (AG11CODRECURSO=" & txtText1(4).Text & _
       " and AG14CODTIPRECU=" & frmdeftiposrecurso.txtText1(6).Text & ")"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      contador = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
      If (contador = 0) Then
      strmensaje = MsgBox("El Recurso " & txtText1(4) & " no existe o no pertenece al " _
            & "tipo de recurso " & frmdeftiposrecurso.txtText1(6).Text _
           & ". Elija otro, por favor.", vbCritical, "Recursos Seleccionados")
      txtText1(4).SetFocus
      objWinInfo.objWinActiveForm.blnChanged = False
      Call objWinInfo.WinProcess(6, 2, 0)
      Exit Sub
      End If
      End If
  End If
  ' controla que el departamento Nuevo existe antes de pulsar un bot�n
  ' distinto de imprimir,borrar,anterior,siguiente,guardar.
  If (btnButton.Index <> 6 And btnButton.Index <> 8 And btnButton.Index <> 22 And _
       btnButton.Index <> 23 And btnButton.Index <> 4) Then
       If (txtText1(4).Text <> "") Then
         sqlstr = "select count(AG11CODRECURSO) from AG1100" & _
            " where (AG11CODRECURSO=" & txtText1(4).Text & _
            " and AG14CODTIPRECU=" & frmdeftiposrecurso.txtText1(6).Text & ")"
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         If (rsta.rdoColumns(0).Value = 0) Then
           objWinInfo.objWinActiveForm.blnChanged = False
         End If
         rsta.Close
         Set rsta = Nothing
       End If
  End If
  If btnButton.Index = 2 Then
    txtText1(6).SetFocus
    txtText1(4).SetFocus
  End If

 Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim contador As Integer
  
  ' controla que el departamento Nuevo existe antes de hacer Guardar
  If (intIndex = 40) Then
      If (txtText1(4).Text <> "") Then
      sqlstr = "select count(AG11CODRECURSO) from AG1100" & _
          " where (AG11CODRECURSO=" & txtText1(4).Text & _
          " and AG14CODTIPRECU=" & frmdeftiposrecurso.txtText1(6).Text & ")"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      contador = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
      If (contador = 0) Then
        strmensaje = MsgBox("El Recurso " & txtText1(4) & " no existe o no pertenece al " _
            & "tipo de recurso " & frmdeftiposrecurso.txtText1(6).Text _
           & ". Elija otro, por favor.", vbCritical, "Recursos Seleccionados")
        txtText1(4).SetFocus
        objWinInfo.objWinActiveForm.blnChanged = False
        Call objWinInfo.WinProcess(6, 2, 0)
        Exit Sub
      End If
      End If
  End If
  ' controla que el departamento Nuevo existe antes de hacer la opci�n de men�.
  ' control si la opci�n es distinta de Eliminar y De Imprimir
  If (intIndex <> 60 And intIndex <> 80) Then
      If (txtText1(4).Text <> "") Then
      sqlstr = "select count(AG11CODRECURSO) from AG1100" & _
         " where (AG11CODRECURSO=" & txtText1(4).Text & _
         " and AG14CODTIPRECU=" & frmdeftiposrecurso.txtText1(6).Text & ")"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value = 0) Then
        objWinInfo.objWinActiveForm.blnChanged = False
      End If
      rsta.Close
      Set rsta = Nothing
      End If
  End If
  
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   
   ' controla que el departamento Nuevo existe antes de hacer la opci�n de men�.
   ' control al pulsar Filtro
   If (intIndex = 10) Then
      If (txtText1(4).Text <> "") Then
      sqlstr = "select count(AG11CODRECURSO) from AG1100" & _
        " where (AG11CODRECURSO=" & txtText1(4).Text & _
        " and AG14CODTIPRECU=" & frmdeftiposrecurso.txtText1(6).Text & ")"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value = 0) Then
        objWinInfo.objWinActiveForm.blnChanged = False
      End If
      rsta.Close
      Set rsta = Nothing
      End If
  End If
  
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   
   ' controla que el departamento Nuevo existe antes de hacer la opci�n de men�.
   ' control si es distinto de Anterior y de Siguiente
   If (intIndex <> 50 And intIndex <> 60) Then
      If (txtText1(4).Text <> "") Then
      sqlstr = "select count(AG11CODRECURSO) from AG1100" & _
         " where AG11CODRECURSO=" & txtText1(4).Text & _
         " and AG14CODTIPRECU=" & frmdeftiposrecurso.txtText1(6).Text & ")"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If (rsta.rdoColumns(0).Value = 0) Then
        objWinInfo.objWinActiveForm.blnChanged = False
      End If
      rsta.Close
      Set rsta = Nothing
      End If
  End If
  
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
 Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtactText1_GotFocus(Index As Integer)
    txtText1(4).SetFocus
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Command Button
' -----------------------------------------------

'Private Sub cmdbuscar_Click()
'   cmdbuscar.Enabled = False
'   Call objsecurity.LaunchProcess("PR0127")
'   'Load frmdefrecursovalidoaux
'   'frmdefrecursovalidoaux!tabTab1(0).Tab = 0 'para mostrar el detalle del Tab
'   'frmdefrecursovalidoaux.Show (vbModal)
'   'Unload frmdefrecursovalidoaux
'   'Set frmdefrecursovalidoaux = Nothing
'   cmdbuscar.Enabled = True
'End Sub

Private Sub txtText1_Change(intIndex As Integer)
  If (intIndex = 4) Then
    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(7)), txtactText1(0).Text)
    Call objWinInfo.CtrlSet(txtText1(7), txtactText1(0).Text)
    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(8)), txtactText1(2).Text)
    Call objWinInfo.CtrlSet(txtText1(8), txtactText1(2).Text)
    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(9)), txtactText1(6).Text)
    Call objWinInfo.CtrlSet(txtText1(9), txtactText1(6).Text)
  End If
  
  Call objWinInfo.CtrlDataChange
  
  If (intIndex = 6) Then
    'Call objWinInfo.CtrlSet(objWinInfo.CtrlGetInfo(txtText1(0)), txtText1(6).Text)
    Call objWinInfo.CtrlSet(txtText1(0), txtText1(6).Text)
  End If
End Sub


