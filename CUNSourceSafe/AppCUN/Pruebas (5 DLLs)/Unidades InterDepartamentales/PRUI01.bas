Attribute VB_Name = "modConstantes"
Option Explicit

Global Const uiCDPTSLAB = "201, 202, 203, 205"

Global Const uiTIPREG_TOP = 150 ' Top del tipo de pregunta inicial
Global Const uiDIF_LINEA = 375  ' Diferencia en el Top entre el tipo de preg y la l�nea
Global Const uiDIF_PREG = 525   ' Diferencia en el Top entre el tipo de preg y la pregunta
Global Const uiDIF_RESP = 450   ' Diferencia en el Top entre el tipo de preg y la respuesta
Global Const uiDIF_RESP_RESP = 350   ' Diferencia en el Top entre respuestas

Global Const uiPASO_SCROLL = 600   ' Paso del scroll en twips

' Constantes para los iconos
Global Const hcICONDOC = 1 ' Documentos
Global Const hcICONPAQUETE = 2 ' Paquetes (UI)
Global Const hcICONUI = 3 ' Unidades Interdepartamentales

Global Const hcICONPR = 1 ' Pruebas
Global Const hcICONTIPREG = 2 ' Tipos de preguntas
Global Const hcICONPREG = 3 ' Preguntas
Global Const hcICONLISTA = 4 ' Lista de respuestas
Global Const hcICONRES = 5 ' Respuestas (elementos de la lista de respuestas)


