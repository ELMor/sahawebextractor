Attribute VB_Name = "Constants"
Option Explicit


' **********************************************************************************
' Module Constants
' Coded by SYSECA Bilbao
' **********************************************************************************

Global Const IconExit                As String = "i1"
Global Const IconPrint               As String = "i2"
Global Const IconProgram             As String = "i3"
Global Const IconStop                As String = "i4"
Global Const IconResults             As String = "i5"
Global Const IconRefresh             As String = "i6"
Global Const IconComprobar           As String = "i7"

Global Const ButtonProgram           As String = "b1"
Global Const ButtonResults           As String = "b2"
Global Const ButtonPrint             As String = "b3"
Global Const ButtonExit              As String = "b4"
Global Const ButtonRefresh           As String = "b5"
Global Const ButtonComprobar         As String = "b6"

' constantes para la ToolBar del formulario
Public Const cwToolBarButtonNew       As String = "b0"
Public Const cwToolBarButtonOpen      As String = "b1"
Public Const cwToolBarButtonSave      As String = "b2"
Public Const cwToolBarButtonPrint     As String = "b3"
Public Const cwToolBarButtonDelete    As String = "b4"
Public Const cwToolBarButtonCut       As String = "b5"
Public Const cwToolBarButtonCopy      As String = "b6"
Public Const cwToolBarButtonPaste     As String = "b7"
Public Const cwToolBarButtonUndo      As String = "b8"
Public Const cwToolBarButtonFind      As String = "b9"
Public Const cwToolBarButtonFilterOn  As String = "b10"
Public Const cwToolBarButtonFilterOff As String = "b11"
Public Const cwToolBarButtonFirst     As String = "b12"
Public Const cwToolBarButtonPrevious  As String = "b13"
Public Const cwToolBarButtonNext      As String = "b14"
Public Const cwToolBarButtonLast      As String = "b15"
Public Const cwToolBarButtonRefresh   As String = "b16"
Public Const cwToolBarButtonMaint     As String = "b17"
Public Const cwToolBarButtonExit      As String = "b18"

' Valores para los tabs del formulario
Public Const cwTabDetail As Byte = 0
Public Const cwTabGrid   As Byte = 1

' posibles tipos de controles soportados
Public Const cwCheckBox    As String = "CheckBox"
Public Const cwComboBox    As String = "ComboBox"
Public Const cwFrame       As String = "Frame"
Public Const cwListBox     As String = "ListBox"
Public Const cwPicture     As String = "Picture"
Public Const cwTextBox     As String = "TextBox"
Public Const cwRichText    As String = "RichTextBox"
Public Const cwToolBar     As String = "Toolbar"
Public Const cwSSCheck     As String = "SSCheck"
Public Const cwSSDateCombo As String = "SSDateCombo"
Public Const cwSSDBCombo   As String = "SSDBCombo"
Public Const cwSSDBGrid    As String = "SSDBGrid"
Public Const cwSSDBOptSet  As String = "SSDBOptSet"
Public Const cwSSFrame     As String = "SSFrame"
Public Const cwSSPanel     As String = "SSPanel"
Public Const cwSSTab       As String = "SSTab"
Public Const cwMaskBox     As String = "MaskEdBox"

' ancho de un caracter en twips
Public Const cwCharWidth As Byte = 130


' constantes para el uso del registro de windows
Public Const cwWinRegStringZ                As Long = 1
Public Const cwWinRegDWord                  As Long = 4
Public Const cwWinRegHKeyClassesRoot        As Long = &H80000000
Public Const cwWinRegHKeyCurrentUser        As Long = &H80000001
Public Const cwWinRegHKeyLocalMachine       As Long = &H80000002
Public Const cwWinRegHKeyUsers              As Long = &H80000003
Public Const cwWinRegErrorNone              As Integer = 0
Public Const cwWinRegErrorBadDB             As Integer = 1
Public Const cwWinRegErrorBadKey            As Integer = 2
Public Const cwWinRegErrorCantOpen          As Integer = 3
Public Const cwWinRegErrorCantRead          As Integer = 4
Public Const cwWinRegErrorCantWrite         As Integer = 5
Public Const cwWinRegErrorOutOfMemory       As Integer = 6
Public Const cwWinRegErrorInvalidParameter  As Integer = 7
Public Const cwWinRegErrorAccessDenied      As Integer = 8
Public Const cwWinRegErrorInvalidParameters As Integer = 87
Public Const cwWinRegErrorNoMoreItems       As Integer = 259
Public Const cwWinRegKeyAllAccess           As Integer = &H3F
Public Const cwWinRegOptionNonVolatile      As Integer = 0


' constantes para las im�genes
Public Const cwImageNew         As String = "i1"
Public Const cwImageOpen        As String = "i2"
Public Const cwImageSave        As String = "i3"
Public Const cwImagePrint       As String = "i4"
Public Const cwImageDelete      As String = "i5"
Public Const cwImageCut         As String = "i6"
Public Const cwImageCopy        As String = "i7"
Public Const cwImagePaste       As String = "i8"
Public Const cwImageUndo        As String = "i9"
Public Const cwImageFind        As String = "i10"
Public Const cwImageFindNext    As String = "i11"
Public Const cwImageFilterOn    As String = "i12"
Public Const cwImageFilterOff   As String = "i13"
Public Const cwImageExit        As String = "i14"
Public Const cwImageFirst       As String = "i15"
Public Const cwImagePrevious    As String = "i16"
Public Const cwImageNext        As String = "i17"
Public Const cwImageLast        As String = "i18"
Public Const cwImageRefresh     As String = "i19"
Public Const cwImageEdit        As String = "i20"
Public Const cwImageOrder       As String = "i21"
Public Const cwImageExecute     As String = "i22"
Public Const cwImageQuery       As String = "i23"
Public Const cwImageViewSQL     As String = "i24"
Public Const cwImageForeign     As String = "i25"
Public Const cwImageMaint       As String = "i26"
Public Const cwImageValueOn     As String = "i27"
Public Const cwImageValueOff    As String = "i28"
Public Const cwImageFileOpen    As String = "i29"
Public Const cwImageFileClose   As String = "i30"
Public Const cwImageInformation As String = "i31"
Public Const cwImageExclamation As String = "i32"
Public Const cwImageQuestion    As String = "i33"
Public Const cwImageCritical    As String = "i34"
Public Const cwImageForeignBMP  As String = "i35"
Public Const cwImageEditBMP     As String = "i36"
Public Const cwImageNewBMP      As String = "i37"
Public Const cwImageOpenBMP     As String = "i38"
Public Const cwImageZoom        As String = "i39"
Public Const cwImageMail        As String = "i40"
Public Const cwImageCheckOn     As String = "i41"
Public Const cwImageCheckOff    As String = "i42"


' constante para el tama�o de bloque de I/O en columnas LONG
Public Const cwLongDataBlockSize As Integer = 16384


' constantes para el tama�o del cursor
Public Const cwCursorSizeDef    As Integer = 40
Public Const cwCursorSizeMax    As Integer = 500
Public Const cwCursorSizeFilter As Integer = 250
Public Const cwCursorSizeView   As Integer = 1000

' constante para la conversi�n de n�meros
Public Const cwMaskNumber As String = "##,###,###,###,##0"


' constantes para los menues
Public Const cwMenuNew    As Integer = 10
Public Const cwMenuOpen   As Integer = 20
Public Const cwMenuSave   As Integer = 40
Public Const cwMenuDelete As Integer = 60
Public Const cwMenuPrint  As Integer = 80
Public Const cwMenuExit   As Integer = 100

Public Const cwMenuUndo      As Integer = 10
Public Const cwMenuCut       As Integer = 30
Public Const cwMenuCopy      As Integer = 40
Public Const cwMenuPaste     As Integer = 50
Public Const cwMenuDel       As Integer = 60
Public Const cwMenuClipAdv   As Integer = 62
Public Const cwMenuBlock     As Integer = 80
Public Const cwMenuLastValue As Integer = 90

Public Const cwMenuShowFilter As Integer = 10
Public Const cwMenuHideFilter As Integer = 20

Public Const cwMenuSearch   As Integer = 10
Public Const cwMenuRestore  As Integer = 20
Public Const cwMenuFirst    As Integer = 40
Public Const cwMenuPrevious As Integer = 50
Public Const cwMenuNext     As Integer = 60
Public Const cwMenuLast     As Integer = 70
Public Const cwMenuForeign  As Integer = 72

Public Const cwMenuRefresh As Integer = 10
Public Const cwMenuMaint   As Integer = 20
Public Const cwMenuColors  As Integer = 40
Public Const cwMenuMasive  As Integer = 50

Public Const cwMenuHelp  As Integer = 10
Public Const cwMenuAbout As Integer = 30

Public Const SQL_FETCH_NEXT As Long = 1
Public Const SQL_NO_DATA_FOUND As Long = 100


Global Const Separador = "***********************************************"
