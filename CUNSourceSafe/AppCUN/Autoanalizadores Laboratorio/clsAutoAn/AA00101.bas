Attribute VB_Name = "Module1"
Option Explicit

'Constantes de acceso a Oracle: DEJAR�N DE USARSE CUANDO SE LEEAN LOS PAR�METROS DESDE EL INI
'Global Const ctePropietario = "Cunlab"        ' Propietario de las tablas
'Global Const ctePwd = "esii"                  ' Password de las tablas
'Global Const cteDatabase = "Oracle73"      ' Nombre del driver que se utiliza
'Global Const cteServerNamePrep = "prep.cun"    ' Nombre del servidor
'Global Const cteServerNameProd = "prod.cun"    ' Nombre del servidor
'Global cteServerName As String

Global Const strPanelLista = "Lista N�:  "
Public Formulario As Form

Declare Function SQLDataSources Lib "odbc32.dll" (ByVal henv&, ByVal fDirection%, ByVal szDSN$, ByVal cbDSNMax%, pcbDSN%, ByVal szDescription$, ByVal cbDescriptionMax%, pcbDescription%) As Integer
Declare Function WritePrivateProfileString Lib "kernel32" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFileName As String) As Integer
Declare Function GetTickCount Lib "kernel32" () As Long
Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long
Declare Function WNetGetUser Lib "mpr" _
        Alias "WNetGetUserA" _
        (ByVal lpName As String, _
         ByVal lpUserName As String, _
         lpnLength As Long) _
         As Long
Declare Function GetPrivateProfileString Lib "kernel32" _
        Alias "GetPrivateProfileStringA" _
        (ByVal lpApplicationName As String, _
         ByVal lpKeyName As Any, _
         ByVal lpDefault As String, _
         ByVal lpReturnedString As String, _
         ByVal nSize As Long, _
         ByVal lpFileName As String) As Long

Declare Function RegCreateKeyEx Lib "advapi32.dll" _
        Alias "RegCreateKeyExA" (ByVal hKey As Long, _
                                 ByVal lpSubKey As String, _
                                 ByVal Reserved As Long, _
                                 ByVal lpClass As String, _
                                 ByVal dwOptions As Long, _
                                 ByVal samDesired As Long, _
                                 ByVal lpSecurityAttributes As Long, _
                                 phkResult As Long, _
                                 lpdwDisposition As Long) As Long
Declare Function RegSetValueExString Lib "advapi32.dll" _
        Alias "RegSetValueExA" (ByVal hKey As Long, _
                                ByVal lpValueName As String, _
                                ByVal Reserved As Long, _
                                ByVal dwType As Long, _
                                ByVal lpValue As String, _
                                ByVal cbData As Long) As Long

Declare Function RegOpenKeyEx Lib "advapi32.dll" _
        Alias "RegOpenKeyExA" (ByVal hKey As Long, _
                               ByVal lpSubKey As String, _
                               ByVal ulOptions As Long, _
                               ByVal samDesired As Long, _
                               phkResult As Long) As Long
Declare Function RegQueryValueExString Lib "advapi32.dll" _
        Alias "RegQueryValueExA" (ByVal hKey As Long, _
                                  ByVal lpValueName As String, _
                                  ByVal lpReserved As Long, _
                                  lpType As Long, _
                                  ByVal lpData As String, _
                                  lpcbData As Long) As Long
                                  
Declare Function RegCloseKey Lib "advapi32.dll" _
        (ByVal hKey As Long) As Long
        
Declare Function RegQueryValueExNULL Lib "advapi32.dll" _
        Alias "RegQueryValueExA" (ByVal hKey As Long, _
                                  ByVal lpValueName As String, _
                                  ByVal lpReserved As Long, _
                                  lpType As Long, _
                                  ByVal lpData As Long, _
                                  lpcbData As Long) As Long
         
Global blnBloqueoChangeRow As Boolean
Global TextoLog As String
Global objError As New clsErrores
Global objAutoan As clsAutoAnalizador


Public Sub Main()

End Sub

Public Function TransfAsciiString(strAscii As String) As String
Dim i As Integer
Dim caracter As String
On Error GoTo Autoanerror

  For i = 1 To Len(strAscii)
    caracter = Mid$(strAscii, i, 1)
    Select Case Asc(caracter)
      Case 1: caracter = "<SOH>"
      Case 2: caracter = "<STX>"
      Case 3: caracter = "<ETX>"
      Case 4: caracter = "<EOT>"
      Case 5: caracter = "<ENQ>"
      Case 6: caracter = "<ACK>"
      Case 10: caracter = "<LF>"
      Case 13: caracter = "<CR>"
      Case 16: caracter = "<DLE>"
      Case 17: caracter = "<XON>"
      Case 19: caracter = "<XOFF>"
      Case 21: caracter = "<NAK>"
      Case 23: caracter = "<ETB>"
      Case Is < 23: caracter = "chr$(" & Asc(caracter) & ")"
    End Select
    TransfAsciiString = TransfAsciiString & caracter
  Next i
  Exit Function
    
Autoanerror:
    Call objError.InternalError(App, "Asignaci�n de Control", "prueba") 'cwMsgWrite & strFileName)
End Function

Public Function strSistema(val As constSistema) As String
  Select Case val
    Case cteMASTER: strSistema = "MASTER"
    Case cteESPERA: strSistema = "ESPERA"
    Case cteESCLAVO: strSistema = "ESCLAVO"
    Case cteSOLICITUD: strSistema = "SOLICITUD"
    Case cteTRANSMISION: strSistema = "TRANSMISION"
    Case cteCONFIRMACION: strSistema = "CONFIRMACION"
  End Select
End Function

Public Function strDescEstado(val As constEstado) As String
  Select Case val
    Case cteLEIDA: strDescEstado = "Leida"
    Case cteBORRADA: strDescEstado = "Borrada"
    Case cteACEPTADA: strDescEstado = "Aceptada"
    Case cteEXTRAIDA: strDescEstado = "Extraida"
    Case cteAREPETIR: strDescEstado = "A Repetir"
    Case cteNOBORRADA: strDescEstado = "No Borrada"
    Case cteNOACEPTADA: strDescEstado = "No Acept."
    Case ctePREBORRADA: strDescEstado = "Para Borrar"
    Case cteSINPROGRAMAR: strDescEstado = "No Progr."
    Case cteREALIZADA: strDescEstado = "...Results..."
    Case cteFUERADERANGOFIN: strDescEstado = "F.Rango"
    Case ctePREPROGRAMACION: strDescEstado = "PreProgr."
    Case cteAREPETIRFIN: strDescEstado = "A Repetir Fin"
    Case cteFUERADERANGO: strDescEstado = "...F.Rango..."
    Case cteINTENTOBORRADO: strDescEstado = "Int.Borrado"
    Case cteINTENTOPROGRAMAR: strDescEstado = "Int.Progr."
    Case cteREALIZACIONCOMPLETA: strDescEstado = "Resultados"
    Case cteRECIBIENDORESULTADOS: strDescEstado = "Rec.Result."
    Case cteFALLODEAUTOANALIZADOR: strDescEstado = "Fallo Autoan"
    Case cteFALLODEAUTOANALIZADORFIN: strDescEstado = "Fallo Autoan Fin"
  End Select
End Function

Public Function strEstadoDeDesc(val As String) As constEstado
  Select Case val
    Case "Leida": strEstadoDeDesc = cteLEIDA
    Case "Borrada": strEstadoDeDesc = cteBORRADA
    Case "Aceptada": strEstadoDeDesc = cteACEPTADA
    Case "Extraida": strEstadoDeDesc = cteEXTRAIDA
    Case "A Repetir": strEstadoDeDesc = cteAREPETIR
    Case "No Borrada": strEstadoDeDesc = cteNOBORRADA
    Case "No Acept.": strEstadoDeDesc = cteNOACEPTADA
    Case "No Progr.": strEstadoDeDesc = cteSINPROGRAMAR
    Case "Para Borrar": strEstadoDeDesc = ctePREBORRADA
    Case "...Results...": strEstadoDeDesc = cteREALIZADA
    Case "F.Rango": strEstadoDeDesc = cteFUERADERANGOFIN
    Case "A Repetir Fin": strEstadoDeDesc = cteAREPETIRFIN
    Case "PreProgr.": strEstadoDeDesc = ctePREPROGRAMACION
    Case "...F.Rango...": strEstadoDeDesc = cteFUERADERANGO
    Case "Int.Borrado": strEstadoDeDesc = cteINTENTOBORRADO
    Case "Int.Progr.": strEstadoDeDesc = cteINTENTOPROGRAMAR
    Case "Resultados": strEstadoDeDesc = cteREALIZACIONCOMPLETA
    Case "Rec.Result.": strEstadoDeDesc = cteRECIBIENDORESULTADOS
    Case "Fallo Autoan": strEstadoDeDesc = cteFALLODEAUTOANALIZADOR
    Case "Fallo Autoan Fin": strEstadoDeDesc = cteFALLODEAUTOANALIZADORFIN
    Case Else: strEstadoDeDesc = cteSINPROGRAMAR
  End Select
End Function

Public Function IsStrEmpty(ByVal strString As Variant) As Boolean
  IsStrEmpty = IIf(strString > "", (Trim(strString) = ""), True)
End Function
