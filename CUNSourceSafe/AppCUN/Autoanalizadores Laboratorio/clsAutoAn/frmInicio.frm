VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "Mscomm32.ocx"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmComunicacion 
   Caption         =   "Container"
   ClientHeight    =   900
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   1830
   LinkTopic       =   "Form1"
   ScaleHeight     =   900
   ScaleWidth      =   1830
   StartUpPosition =   3  'Windows Default
   Begin MSCommLib.MSComm Comm1 
      Left            =   1260
      Top             =   330
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      DTREnable       =   -1  'True
      Handshaking     =   1
      InBufferSize    =   10240
      InputLen        =   1
      ParityReplace   =   0
      RThreshold      =   1
      RTSEnable       =   -1  'True
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Timer TimerEsperaREPLY 
      Enabled         =   0   'False
      Interval        =   15000
      Left            =   0
      Top             =   480
   End
   Begin VB.Timer TimerTrasMENSAJE 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   420
      Top             =   480
   End
   Begin VB.Timer TimerAUTO 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   840
      Top             =   480
   End
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   960
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk3 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmComunicacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Estado As Integer                   'Estado del sistema
Dim res As Boolean
Dim strLog As String
Dim strArchivoLog As String

Dim blnAutoEsperaRespuesta As Integer   'Indica si el autoanalizador espera un mensaje de respuesta
Dim blnUltimoRepetido As Integer      'Indica si es el �ltimo frame a enviar
Dim intNumFrameTrans As Integer    'Indica el n�mero de frame que hay que trasmitir
Dim blnFrameRepetido As Integer    'Indica si el frame es repetido
Dim arFrames() As String          'Contiene los frames que forman el mensaje a transmitir completo
Dim strFrameTotal As String        'Recoge los distintos frames libres de caracteres de protocolo
Dim strCodMuestraQuery As String     'C�digo de la muestra enviado por el Electra en un Query
Dim strRackQuery As String         'Posicion de la muestra enviada por el Electra en un Query

Private Sub Comm1_OnComm()
Dim ERMsg$
Dim caracter As String * 1
Dim strCharLog As String 'para el Log

    'Se selecciona el evento que ocurre en el puerto
    Select Case Comm1.CommEvent
        'Si se produce una entrada se procede a su lectura
        'Se lee cada caracter por separado
        Case comEvReceive
            Comm1.RThreshold = 0
            Do While Comm1.InBufferCount
                DoEvents
                caracter = Comm1.Input
                strCharLog = caracter
                Select Case Asc(strCharLog)
                Case 1
                    strCharLog = "<SOH>"
                Case 2
                    strCharLog = "<STX>"
                Case 3
                    strCharLog = "<ETX>"
                Case 4
                    strCharLog = "<EOT>"
                Case 5
                    strCharLog = "<ENQ>"
                Case 6
                    strCharLog = "<ACK>"
                Case 10
                    strCharLog = "<LF>"
                Case 13
                    strCharLog = "<CR>"
                Case 16
                    strCharLog = "<DLE>"
                Case 17
                    strCharLog = "<XON>"
                Case 19
                    strCharLog = "<XOFF>"
                Case 21
                    strCharLog = "<NAK>"
                Case 23
                    strCharLog = "<ETB>"
                End Select
                strLog = strLog & strCharLog
                Call pLecturaProtocolo(caracter)
            Loop
            Comm1.RThreshold = 1
        Case comEventRxOver
            ERMsg$ = "Buffer de entada de datos lleno."
        Case comEventTxFull
            ERMsg$ = "Buffer de salida de datos lleno."
    End Select
    
    If Len(ERMsg$) Then
        MsgBox ERMsg$, vbExclamation, Me.Caption
        ERMsg$ = ""
    End If

End Sub

Private Sub Electra_Confirmacion(Mensaje As String)
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo _
    (si le ha llegado con errores).Si el mensaje es rechazado habr� que volver a enviarlo.
    Dim contador1 As Integer
    Dim objactuacion As clsActuacion

    On Error Resume Next
        
    TimerEsperaREPLY.Enabled = False

    If Right$(Mensaje, 1) = Chr$(6) Or Right$(Mensaje, 1) = Chr$(4) Then '<ACK> o <EOT> - El autoanalizador ha aceptado el bloque
        '***** HABR�A QUE ESTUDIAR M�S EN PROFUNDIDAD QUE OCURRE EN EL CASO DE QUE EL   *****
        '***** AUTOANALIZADOR RESPONDA CON UN <EOT>                                     *****
        blnFrameRepetido = False
        Mensaje = ""
        If blnUltimoRepetido = True Then
            Comm1.Output = Chr$(4) '<EOT> - Se cierra la comunicaci�n
            blnAutoEsperaRespuesta = False
            Estado = objConst.constESTADOSISTESPERA
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
            Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
            'Se actualiza el grid
'            For contador1 = 1 To UBound(arActuaciones)
'                If arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR Then
'                    arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOACEPTADA
'                    ssdgPruebas.MoveFirst
'                    ssdgPruebas.MoveRecords contador1 - 1
'                    ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
'                End If
'            Next contador1
            For Each objactuacion In objAutoAn.ColActuaciones
                If objactuacion.strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR Then
                    Call objAutoAn.pModificarRegistro(1, objactuacion.strCodMuestra, objactuacion.strCodAct, objConst.constESTADOPRAUTOACEPTADA)
                End If
            Next
        Else
            Estado = objConst.constESTADOSISTMASTER
            Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
            strLog = strLog & " intNumFrameTrans= " & intNumFrameTrans & Chr$(13) & Chr$(10)
            intNumFrameTrans = intNumFrameTrans + 1
            Call pTransmisionFrames
        End If
        Exit Sub
    End If
    
    If Right$(Mensaje, 1) = Chr$(21) Then  '<NAK> - El autoanalizador no ha aceptado el bloque
        blnFrameRepetido = True
        Estado = objConst.constESTADOSISTMASTER
        Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
        Mensaje = ""
        Call pEnvioMensajes  'Se llama a esta subrutina y no directamene a pTransmisionFrames para
                        'poder controlar el n�mero de repeticiones que se hacen
    End If

End Sub

Private Sub Electra_Esclavo(Mensaje As String)
'Identifica el final del mensaje que se est� recibiendo y procede a su lectura si es le caso.
'El mensaje puede ser enviado en uno o varios frames dependiendo de su longitud
    Static intNumFrame As Integer 'Guarda el anterior n�mero de frame
    Dim intActualNumFrame As Integer 'Recoge el actal n�mero de frame
    Dim strFrame As String 'variable de paso de Mensaje a strFrameTotal
    Dim filenum As Integer
    
    TimerAUTO.Enabled = False 'Al llegar datos del autoanalizador, se desactiva el Timer

    If Right$(Mensaje, 1) = Chr$(4) Then '<EOT> - Final de la transmisi�n por parte del autoanalizador
        Mensaje = ""
        Estado = objConst.constESTADOSISTESPERA
        Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
        strLog = strLog & "ESTADO ESPERA" & Chr$(13) & Chr$(10)
        strLog = strLog & "Fin de Transmision" & Chr$(13) & Chr$(10)
        'Si el autoanalizador est� esperando un respuesta se contesta al Query
        If blnAutoEsperaRespuesta = True Then
            Call pEnvioMensajes 'Se env�a una nueva prueba a programar
        End If
        Exit Sub
    End If

    If Right$(Mensaje, 2) = Chr$(13) & Chr$(10) Then '<CR><LF> - Fin del frame.
        'Se elimina el comienzo del mensaje hasta el <STX> inclusive
        strLog = strLog & "Fin de Frame" & Chr$(13) & Chr$(10)
        strFrame = Mid$(Mensaje, InStr(1, Mensaje, Chr$(2)) + 1)
        Mensaje = ""
        'Se lee el n�mero de Frame
        intActualNumFrame = Val(Left$(strFrame, 1))
        'Se comprueba el n�mero frame
        Select Case intActualNumFrame
        Case intNumFrame      'Frame repetido
        Case intNumFrame + 1  'Frame correcto
            Comm1.Output = Chr$(6) '<ACK> - Respuesta afirmativa
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ACK>" & Chr$(13) & Chr$(10)
            'Se comprueba si es el �ltimo frame
            If InStr(1, strFrame, Chr$(3)) Then
                'Se actualiza el n�mero del frame
                intNumFrame = 0
                'Se elimina el n�mero de frame y el final del mensaje desde <ETX>
                strFrame = Mid$(strFrame, 2, InStr(1, strFrame, Chr$(3)) - 2)
                strFrameTotal = strFrameTotal & strFrame
                Call pLecturaDatos(strFrameTotal)
                strFrameTotal = ""
            ElseIf InStr(1, strFrame, Chr$(23)) Then
                'Se actualiza el n�mero del frame
                Select Case intActualNumFrame
                Case 7
                    intNumFrame = -1
                Case Else
                    intNumFrame = intActualNumFrame
                End Select
                'Se elimina el n�mero de frame y el final del mensaje desde <ETB>
                strFrame = Mid$(strFrame, 2, InStr(1, strFrame, Chr$(23)) - 2)
                strFrameTotal = strFrameTotal & strFrame
            End If
        Case Else          'Frame incorrecto
            Comm1.Output = Chr$(21) '<NAK> - Respuesta negativa
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <NAK>" & Chr$(13) & Chr$(10)
        End Select
        TimerAUTO.Enabled = True
        
        objAutoAn.EscribirLog strLog
        strLog = ""
        Exit Sub
    End If

End Sub

Private Sub Electra_Espera(Mensaje As String)
'Identifica la llegada de los caracteres de solicitud de comunicaci�n, la acepta y pasa _
    al estado de Esclavo.
    
    If Right$(Mensaje, 1) = Chr$(5) Then '<ENQ> - Solicitud de comunicaci�n por parte del autoanalizador
        Comm1.Output = Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
        Estado = objConst.constESTADOSISTESCLAVO
        strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ACK> ESTADO ESCLAVO" & Chr$(13) & Chr$(10)
        Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
        Me.Refresh
        Mensaje = ""
        TimerAUTO.Enabled = True
    End If

End Sub

Private Sub Electra_Solicitud(Mensaje As String)
'Respuesta del autoanalizador a la solicitud para establecer una comunicaci�n.
    TimerEsperaREPLY.Enabled = False

    If Right$(Mensaje, 1) = Chr$(6) Then '<ACK> - El autoanalizador ha aceptado la comunicaci�n
        Estado = objConst.constESTADOSISTMASTER 'El sistema se convierte en master
        Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
        strLog = strLog & "ESTADO MASTER"
        Mensaje = ""
        Call pEnvioMensajes 'Se env�a el bloque de datos por parte del sistema al autoanalizador
        Exit Sub
    End If
    
    If Right$(Mensaje, 1) = Chr$(21) Then '<NAK> - El autoanalizador no ha aceptado la comunicaci�n
        Estado = objConst.constESTADOSISTESPERA 'El sistema vuelve a estado de espera
        Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
        strLog = strLog & "ESTADO ESPERA"
        Mensaje = ""
        TimerTrasMENSAJE.Enabled = True 'Se espera 10 segundos para intentar una nueva comunicaci�n
        Exit Sub
    End If
    
    If Right$(Mensaje, 1) = Chr$(5) Then '<ENQ> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
        Estado = objConst.constESTADOSISTESPERA 'El sistema da paso a la preferencia del autoanalizador
        Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado) & "  (P)")
        strLog = strLog & "ESTADO ESPERA"
        Mensaje = ""
    End If

End Sub
Private Sub pEnvioMensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar _
    el caracter <ENQ> y esperar a que el autoanalizador responda afirmativamente. Luego _
    se manda el mensaje propiamente dicho hasta que sea recibido correctamente por el _
    autoanalizador. Finalmente se cierra la comunicaci�n enviando <EOT>.
    Static intContIntentosComunic As Integer 'cuenta el n� de veces que se intenta comunicar con el autoanalizador
    Static intContIntentosTrans As Integer 'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
    Dim T As Long
    Dim res As Integer
    Dim msg As String
    Dim contador1 As Integer
    Dim objMuestra As clsMuestra
    
    On Error Resume Next

    Select Case Estado
    Case objConst.constESTADOSISTESPERA 'Se intenta establecer la comunicaci�n
        intContIntentosTrans = 0 'Se inicializa el intContIntentosTrans por si se realiza una nueva comunicaci�n
        intContIntentosComunic = intContIntentosComunic + 1
        If intContIntentosComunic = 7 Then 'Fallo en la comunicaci�n despu�s de 6 intentos
            intContIntentosComunic = 0
            msg = "No se puede establecer comunicaci�n."
            Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
            Case vbRetry
                T = Timer + 5
                Do While T > Timer
                    res = DoEvents()
                Loop
                Sistema_Espera
                intContIntentosComunic = intContIntentosComunic + 1
                Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado) & "  " & intContIntentosComunic)
                Exit Sub
            Case vbCancel
                Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
                Estado = objConst.constESTADOSISTESPERA 'Se pasa al estado de espera
                strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
                Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
                Exit Sub
            End Select
        Else
            Sistema_Espera
            Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado) & "  " & intContIntentosComunic)
            Exit Sub
        End If

    Case objConst.constESTADOSISTMASTER 'Se intenta transmitir los datos
        intContIntentosComunic = 0 'Se inicializa el intContIntentosComunic por si se realiza una nueva comunicaci�n
        intContIntentosTrans = intContIntentosTrans + 1
        If intContIntentosTrans = 7 Then 'Fallo en la transmisi�n despu�s de 6 intentos
            intContIntentosTrans = 0
            Estado = objConst.constESTADOSISTESPERA
            Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
            strLog = strLog & "ESTADO ESPERA"
            msg = "No se puede transmitir informaci�n."
            Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
            Case vbRetry
                T = Timer + 5
                Do While T > Timer
                    res = DoEvents()
                Loop
                Sistema_Espera
                intContIntentosComunic = intContIntentosComunic + 1
                Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado) & "  " & intContIntentosComunic)
                Exit Sub
            Case vbCancel
                Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
                strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> " & Chr$(13) & Chr$(10)
                'Se actualiza el grid
'                For contador1 = 1 To UBound(arActuaciones)
'                    If arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR Then
'                        arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOSINPROGRAMAR
'                        arActuaciones(contador1).strPosicion = ""
'                        ssdgPruebas.MoveFirst
'                        ssdgPruebas.MoveRecords contador1 - 1
'                        ssdgPruebas.Columns("Copa").Text = ""
'                        ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
'                    End If
'                Next contador1
                
                For Each objMuestra In objAutoAn.colmuestras
                    If objactuacion.strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR Then
                        Call objAutoAn.pModificarMuestra(objConst.ConstCABESTADO, objMuestra.strCodMuestra, objConst.constESTADOPRAUTOACEPTADA)
                        Call objAutoAn.pModificarMuestra(objConst.ConstCABCOPA, objMuestra.strCodMuestra, "")
                    End If
                Next
                
                Estado = objConst.constESTADOSISTESPERA 'Se pasa al estado de espera
                Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
                strLog = strLog & "ESTADO ESPERA"
                Exit Sub
            End Select
        Else
            If blnFrameRepetido = False Then
                Sistema_Master
            Else
                Call pTransmisionFrames
            End If
            Call objAutoAn.PanelCaption("Comunicacion", "CONFIRMACI�N " & intContIntentosTrans)
            Exit Sub
        End If
    End Select

End Sub

Private Sub pEstablecerCheck(Mensaje As String)
'Subrutina que establece el check del mensaje que ser� enviado al autoanalizador.
    Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
    Dim suma As Integer         'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal

    suma = 0
    For contador = 1 To Len(Mensaje)
        suma = suma + Asc(Mid$(Mensaje, contador, 1))
        If suma >= 256 Then
            suma = suma - 256
        End If
    Next contador
    If Len(Hex$(suma)) = 1 Then
        Mensaje = Mensaje & "0" & Hex$(suma)
    Else
        Mensaje = Mensaje & Hex$(suma)
    End If

End Sub
Private Sub pLecturaProtocolo(caracter As String)
    Static Mensaje As String   'Contiene el mensaje que env�a el autoanalizador
    Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es _
    la que se env�a a otras subrutinas pudiendo ser modificada en �stas. Al ser modificada _
    la copia, tambi�n deber� modificarse el original. El hecho de utilizar esta copia _
    se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas _
    por estar declarada como Static en esta subrutina
    
    'Se va componiendo el mensaje
    Mensaje = Mensaje & caracter
    CopiaMensaje = Mensaje
    
    'Se acude a la subrutina correspondiente para la lectura del mensaje seg�n el estado.
    Select Case Estado
    Case objConst.constESTADOSISTESPERA
        Electra_Espera CopiaMensaje
        Mensaje = CopiaMensaje
    Case objConst.constESTADOSISTSOLICITUD
        Electra_Solicitud CopiaMensaje
        Mensaje = CopiaMensaje
    Case objConst.constESTADOSISTESCLAVO
        Electra_Esclavo CopiaMensaje
        Mensaje = CopiaMensaje
    Case objConst.constESTADOSISTCONFIRMACION
        Electra_Confirmacion CopiaMensaje
        Mensaje = CopiaMensaje
    End Select

End Sub
Public Sub pLecturaDatos(datos As String)
'Se leen los mensajes que env�a el autoanalizador. Solo se tendr�n en cuenta los mensajes _
    Query y los de Resultados.
Dim contador1 As Integer
Dim resultado As String
Dim i As Integer
Dim filenum As Integer
Dim strIDMuestra As String
Dim strCodActAuto As String
Dim dimension As Integer
Dim objResultado As clsValorResultado
Dim colResultados As New Collection
Dim CodMuestra As String
    
    
    
objAutoAn.colmuestras("AAY-6455").strurgente = IIf(objAutoAn.colmuestras("AAY-6455").strurgente = "NO", "SI", "NO")

Exit Sub

datos = "H|\^&" & Chr$(13)
datos = datos & "P|1|0007276422" & Chr$(13)
'Call objAutoAn.pModificarRegistro(objConst.ConstCABESTADO, "AAY-6455", "1085", objConst.constESTADOPRAUTOACEPTADA)


datos = datos & "O|1||A02|^^^001^S|||||||||||||||||199809210849|199809210849||F|F" & Chr$(13)
datos = datos & "R|1|^^^001^0||||||F^3^0" & Chr$(13)
datos = datos & "R|2|^^^001^1|4.9|||||F^1^0" & Chr$(13)
datos = datos & "R|3|^^^001^2|4.7|||||F^1^0" & Chr$(13)
datos = datos & "R|4|^^^001^3|4.8|||||F^1^0" & Chr$(13)
datos = datos & "R|5|^^^001^4|366|2||||F^1^0" & Chr$(13)
datos = datos & "C|1|I|1^005" & Chr$(13)
datos = datos & "C|2|I|2" & Chr$(13)
datos = datos & "L|1" & Chr$(13)


    'Se separan los distintos tipos de registros del mensaje
    Awk1 = datos
    Awk1.FS = Chr$(13)
    strLog = strLog & "Awk1=Datos=" & datos & Chr$(13) & Chr$(10)
    'El awk1.F(1) contiene la cabecera (registro tipo H) y el awk1.F(awk1.NF) el terminador
    '(registro tipo L) que no sirven para nada.
    'El awk1.F(2) nos indica que tipo de mensaje es: registro tipo P (resultados) o registro tipo
    'Q (query)
    Select Case UCase$(Left$(Awk1.F(2), 1))
    Case "P"
        blnAutoEsperaRespuesta = False
        Awk2.FS = "|"
        
        'Se obtiene la ID de la muestra
        Awk2 = Awk1.F(2)
        strIDMuestra = Awk2.F(3)
        If Len(strIDMuestra) = 8 And Val(strIDMuestra) = 0 Then
            strIDMuestra = objAutoAn.fCodigoANumero(strIDMuestra)
        End If
        CodMuestra = objAutoAn.fNumeroACodigo(strIDMuestra)
        'Se obtiene el c�digo de la prueba
        Awk2 = Awk1.F(3)
        Awk3.FS = "^"
        Awk3 = Awk2.F(5)
        strCodActAuto = Awk3.F(4)
        
        'Se recogen los resultados. Los comentarios no se tienen en cuenta por ahora
        For contador1 = 4 To Awk1.NF
            If UCase$(Left$((Awk1.F(contador1)), 1)) = "R" Then
                Awk2 = Awk1.F(contador1)
                If Awk2.F(4) <> "" Then 'si el resultado no es null
                    Awk3.FS = "^"
                    Awk3 = Awk2.F(3)
                    Set objResultado = New clsValorResultado
                    With objResultado
                        .strCodResultAuto = Awk3.F(5) 'awk2.F(2)   'codigo
                        .strCodActAuto = strCodActAuto
                        .strCodMuestra = CodMuestra
                        resultado = Awk2.F(4)
                        i = objAutoAn.fFormatearNumero(resultado, ".", ",")
                        .strResultado = resultado   'resultado
                    End With
                    colResultados.Add objResultado
                    Set objResultado = Nothing
                End If
            End If
        Next contador1
        'Se actualiza la base de datos
        Call objAutoAn.pActualizarDB(colResultados)
    Case "Q"
        Awk2.FS = "|"
        Awk2 = Awk1.F(2)
        
        'Solo nos interesa el c�digo de la muestra y la posicion de esta en el rack
        Awk3.FS = "^"
        Awk3 = Awk2.F(3)
        
        'Se guarda el c�digo de la muestra en formato enviado por el autoanalizador
        strCodMuestraQuery = Awk3.F(1)
        strRackQuery = Awk3.F(2)
        
        'El autoanalizador estar� esperando una respuesta a su Query
        blnAutoEsperaRespuesta = True
    End Select

    objAutoAn.EscribirLog strLog
    strLog = ""

End Sub

Public Sub Salir()
Dim contador As Integer
Dim resp As Integer
    
    strLog = strLog & "Pulsado salir" & Chr$(13) & Chr$(10)

    'Se comprueba si existe alguna prueba que no tiene resultados y en ese caso se _
    cambia su estado a Prueba Extra�da.
'    For contador = 1 To UBound(arActuaciones)
'        If arActuaciones(contador).strEstado <> objConst.constESTADOPRAUTOREALIZADA Then
'            resp = objAutoAn.fPreguntarVolverPruebasAExtraidas()
'            If resp = vbCancel Then Exit Sub
'            Exit For
'        End If
'    Next contador
'
    'El form_unload s�lo se ejecuta si no se ha cancelado
    'Form_Unload (False)
End
End Sub
Public Sub Descargar()
    Comm1.PortOpen = False
    objAutoAn.EscribirLog strLog & Chr$(13) & Chr$(10) & "Fin de Ejecucion"
    Set objAutoAn = Nothing
    End
End Sub
Private Sub Sistema_Espera()
'Se env�a el caracter de solicitud de comunicaci�n y se pasa a estado de Solicitud
    Comm1.Output = Chr$(5) 'Env�o de <ENQ>, se "solicita l�nea"
    Estado = objConst.constESTADOSISTSOLICITUD
    'panEstado.Caption = objAutoAn.fLeerEstado(estado)  'Especificado en pEnvioMensajes
    TimerEsperaREPLY.Enabled = True
    strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ENQ> ESTADO SOLICITUD" & Chr$(13) & Chr$(10)
End Sub

Private Sub Sistema_Master()
'Se forma el mensaje de respuesta al Query.
    Dim strRegistro_H As String
    Dim strRegistro_P As String
    Dim arRegistro_O() As String
    Dim dimension As Integer
    Dim strRegistro_L As String
    Dim intNumeroDeFrames As Integer
    Dim contador1 As Integer
    Dim contador2 As Integer
    Dim msg As String
    Dim objactuacion As clsActuacion
    
    'Registro cabecera tipo H
    strRegistro_H = "H|\^&" & Chr$(13)

    'Registro de informaci�n del paciente tipo P
    strRegistro_P = "P|1|" & strCodMuestraQuery & Chr$(13)

    'Registros de ordenes de pruebas tipo O
    'Se crear�n tantos registros de orden tipo O como pruebas existan para la muestra
    dimension = 0
'    For contador1 = 1 To UBound(arActuaciones)
'        If arActuaciones(contador1).strIDMuestra = strCodMuestraQuery Then
'            dimension = dimension + 1
'            ReDim Preserve arRegistro_O(1 To dimension)
'            msg = "O|" & dimension & "||" & strRackQuery & "|^^^" & arActuaciones(contador1).strCodActAuto & "|"
'            If arActuaciones(contador1).blnUrgente = True Then
'                msg = msg & "S|"
'            Else
'                msg = msg & "|"
'            End If
'            msg = msg & "|||||P||||||||||||||Q" & Chr$(13)
'            arRegistro_O(dimension) = msg
'            'se actualiza el arActuaciones y el grid
'            arActuaciones(contador1).strPosicion = strRackQuery
'            arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR
'            ssdgPruebas.MoveFirst
'            ssdgPruebas.MoveRecords contador1 - 1
'            ssdgPruebas.Columns("Copa").Text = arActuaciones(contador1).strPosicion
'            ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
'        End If
'    Next contador1
        
    For Each objactuacion In objAutoAn.ColActuaciones
        If objactuacion.strIDMuestra = strCodMuestraQuery Then
            dimension = dimension + 1
            ReDim Preserve arRegistro_O(1 To dimension)
            msg = "O|" & dimension & "||" & strRackQuery & "|^^^" & arActuaciones(contador1).strCodActAuto & "|"
            If arActuaciones(contador1).blnUrgente = True Then
                msg = msg & "S|"
            Else
                msg = msg & "|"
            End If
            msg = msg & "|||||P||||||||||||||Q" & Chr$(13)
            arRegistro_O(dimension) = msg
            'se actualiza el arActuaciones y el grid
            Call objAutoAn.pModificarRegistro(objConst.ConstCABCOPA, objactuacion.strCodMuestra, objactuacion.strCodAct, strRackQuery)
            Call objAutoAn.pModificarRegistro(objConst.ConstCABCOPA, objactuacion.strCodMuestra, objactuacion.strCodAct, objConst.constESTADOPRAUTOINTENTOPROGRAMAR)
        End If
    Next
    
    If dimension = 0 Then 'no existe nada pedido para la muestra
        dimension = dimension + 1
        ReDim Preserve arRegistro_O(1 To dimension)
        arRegistro_O(dimension) = "O|1||" & strRackQuery & "|^^^000|||||||P||||||||||||||Z" & Chr$(13)
    End If

    'Registro terminador tipo L
    strRegistro_L = "L|1|I" & Chr$(13)

    'Se juntan todos los registros en uno solo (se emplea la variable strRegistro_H)
    strRegistro_H = strRegistro_H & strRegistro_P
    For contador1 = 1 To UBound(arRegistro_O)
        strRegistro_H = strRegistro_H & arRegistro_O(contador1)
    Next contador1
    strRegistro_H = strRegistro_H & strRegistro_L

    'Se descompone el mensaje en frames
    If Len(strRegistro_H) / 240 = Int(Len(strRegistro_H) / 240) Then
        intNumeroDeFrames = Int(Len(strRegistro_H) / 240)
    Else
        intNumeroDeFrames = Int(Len(strRegistro_H) / 240) + 1
    End If
    ReDim arFrames(1 To intNumeroDeFrames)
    contador2 = 0
    For contador1 = 1 To intNumeroDeFrames
        arFrames(contador1) = Mid$(strRegistro_H, 1 + (contador1 - 1) * 240, 240)
        'Se completan los frames con los caracteres de transmision
        contador2 = contador2 + 1
        If contador2 = 8 Then
            contador2 = 0
        End If
        If contador1 = UBound(arFrames) Then
            arFrames(contador1) = contador2 & arFrames(contador1) & Chr$(3)
        Else
            arFrames(contador1) = contador2 & arFrames(contador1) & Chr$(23)
        End If
        Call pEstablecerCheck(arFrames(contador1))
        arFrames(contador1) = Chr$(2) & arFrames(contador1) & Chr$(13) & Chr$(10)
    Next contador1

    'Se transmiten los frames
    intNumFrameTrans = 1
    strLog = strLog & " intNumFrameTrans= 1" & Chr$(13) & Chr$(10)
    Call pTransmisionFrames

End Sub

Private Sub Form_Load()
Estado = constESTADOSISTESPERA
End Sub

Private Sub Form_Unload(Cancel As Integer)
'fff
End Sub

Private Sub TimerAUTO_Timer()
'Controla si se ha producido un TimeOut por parte del autoanalizador. Esto ocurre cuando _
    estando el ordenador en estado de esclavo pasan 30 seg. sin recibir ning�n dato.
    Dim msg As String
    Dim resp As Integer

    TimerAUTO.Enabled = False
    If Estado = objConst.constESTADOSISTESCLAVO Then
        msg = "Se ha producido un Timeout durante la comunicaci�n por parte del autoanalizador."
        msg = msg & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
        resp = MsgBox(msg, vbExclamation + vbYesNo, Me.Caption)
        Select Case resp
        Case vbYes
            TimerAUTO.Enabled = True
        Case vbNo
            strFrameTotal = ""
            Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
            Estado = objConst.constESTADOSISTESPERA 'Se pasa al estado de espera
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
            Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
            Exit Sub
        End Select
    End If

End Sub

Private Sub TimerEsperaREPLY_Timer()
'Controla los TimeOut que produce el autoanalizador cuando el ordenador est� a la espera _
    de una respuesta
        
    Dim msg As String
    Dim resp As Integer
    
    TimerEsperaREPLY.Enabled = False
    Select Case Estado
    Case objConst.constESTADOSISTSOLICITUD, objConst.constESTADOSISTCONFIRMACION
        msg = "Se ha producido un Timeout durante la comunicaci�n por parte del autoanalizador."
        msg = msg & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
        resp = MsgBox(msg, vbExclamation + vbYesNo, Me.Caption)
        Select Case resp
        Case vbYes
            TimerEsperaREPLY.Enabled = True
        Case vbNo
            Comm1.Output = Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
            Estado = objConst.constESTADOSISTESPERA 'Se pasa al estado de espera
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
            Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
            Exit Sub
        End Select
    Case Else
        msg = "Error de programaci�n. Revisar el c�digo. El TimeoOut que se ha producido solo "
        msg = msg & "deb�a producirse en el estado de SOLICITUD o CONFIRMACION"
        MsgBox msg
    End Select

End Sub


Private Sub TimerTrasMENSAJE_Timer()
'Controla los tiempos entre env�os de solicitudes
    TimerTrasMENSAJE.Enabled = False
    Call pEnvioMensajes

End Sub

Private Sub pTransmisionFrames()
'Se transmiten los frames teniendo en cuenta las respuestas del autoanalizador.
    Dim contador1 As Integer
    Dim SQL As String

    blnUltimoRepetido = (intNumFrameTrans = UBound(arFrames))
    Comm1.Output = arFrames(intNumFrameTrans)
    Estado = objConst.constESTADOSISTCONFIRMACION
    strLog = strLog & Chr$(13) & Chr$(10) & " ESTADO CONFIRMACION" & Chr$(13) & Chr$(10)
    strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: " & arFrames(intNumFrameTrans) & Chr$(13) & Chr$(10)
    TimerEsperaREPLY.Enabled = True

End Sub


