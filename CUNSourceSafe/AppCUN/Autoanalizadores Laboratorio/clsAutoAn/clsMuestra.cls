VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsMuestra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"

Public intFilaPrRe As String
Private strMiCodMuestra As String '2
Private strMiNuevaMuestra As String '2
Private strMiIDMuestra As String '3
Private strMiEstado As constEstado  'String '9
Private strMiPosicion As String '10
Private strMiSeccion As String '12
Private strMiDisco As String '13
Private strMiCopa As String '14
Private strMiBandeja As String '?
Private strMiProperty1 As String
Private strMiProperty2 As String
Private strMiProperty3 As String
Private strMiEspecimen As String
Private strMiEspecimenAuto As String
Private blnMiRepetida As Boolean
Private blnMiUrgente As Boolean '11
Private intMiFila As Integer
Private intMiDilucion As Double '15
Private intMiTiempo As Integer 'copia local
Private strMiCarpeta As String

Public lngHistoria As Long
Public ColMuestrasPruebas As Collection
'variables locales que contienen valores de propiedad

Public Sub QuitarMuestra()
Dim objActuacion As clsActuacion
Dim ret As Boolean
  ret = LockWindowUpdate(frmAutoan.hWnd)
  For Each objActuacion In ColMuestrasPruebas
    objActuacion.QuitarPrueba
  Next
  ret = LockWindowUpdate(0&)
End Sub

Public Sub EliminarMuestra()
  With objAutoan.ColMuestras
    .Remove strCodMuestra
    If .Count = 0 Then objAutoan.frmFormulario.descargar
  End With
End Sub

Public Property Let strCarpeta(val As String)
If strMiCarpeta <> val Then
  strMiCarpeta = val
  Call EscribirEnColumna(cteMuestraCARPETA, val)
End If
End Property

Public Property Get strCarpeta() As String
  strCarpeta = strMiCarpeta
End Property

Public Property Let intTiempo(ByVal vData As Integer)
'se usa cuando se asigna un valor a una propiedad, en el lado izquierdo de la asignaci�n.
'Syntax: X.intTiempo = 5
    intMiTiempo = vData
End Property

Public Property Get intTiempo() As Integer
'se usa cuando se asigna un valor a una propiedad, en el lado derecho de la asignaci�n.
'Syntax: Debug.Print X.intTiempo
    intTiempo = intMiTiempo
End Property

Public Sub ColMuestrasPruebas_Add(objActuacion As clsActuacion, strKey As String)
  ColMuestrasPruebas.Add objActuacion, strKey
  objActuacion.strCodMuestra = strCodMuestra
End Sub
    
Private Sub EscribirEnColumna(code As constColumnaGridMuestras, valor As String)
Dim columna As Column

  With frmAutoan.ssdgMuestras
    For Each columna In .Columns
      If columna.TagVariant = code Then
        blnBloqueoChangeRow = True
'        .MoveFirst
'        .MoveRecords intFila
        .Bookmark = intFila
        columna.Text = valor
        blnBloqueoChangeRow = False
        Exit For
      End If
    Next
  End With
End Sub
        
Public Property Let strEspecimen(val As String)   '9
  If val <> strMiEspecimen Then
    strMiEspecimen = val
    Call EscribirEnColumna(cteMuestraESPECIMEN, val)
  End If
End Property
                
Public Property Let strEspecimenAuto(val As String)   '9
  If val <> strMiEspecimenAuto Then
    strMiEspecimenAuto = val
    Call EscribirEnColumna(cteMuestraESPECIMENAUTO, val)
  End If
End Property
                
Public Property Let strProperty1(val As String)   '9
  If val <> strMiProperty1 Then
    strMiProperty1 = val
    Call EscribirEnColumna(cteMuestraPROPERTY1, val)
  End If
End Property

Public Property Let strProperty2(val As String)   '9
  If val <> strMiProperty2 Then
    strMiProperty2 = val
    Call EscribirEnColumna(cteMuestraPROPERTY2, val)
  End If
End Property

Public Property Let strProperty3(val As String)   '9
  If val <> strMiProperty3 Then
    strMiProperty3 = val
    Call EscribirEnColumna(cteMuestraPROPERTY3, val)
  End If
End Property

Public Property Let strCodMuestra(val As String)
Dim Sql$
Dim Qry As rdoQuery

  If strMiCodMuestra <> val Then
    strMiNuevaMuestra = val
    strNuevaMuestra = val
'    With objAutoan
'      .ColMuestras.Add Me, val
'      If .blnExistMuestra(strMiCodMuestra) Then
'        .ColMuestras.Remove strMiCodMuestra
'      End If
'    End With
    Call EscribirEnColumna(cteMuestraCODMUESTRA, val)
    Call EscribirEnColumna(cteMuestraNewSample, val)
'    sql = "select cmuestra from muestraasistencia where cmuestra=?"
'    Set qry = objAutoan.rdoConnect.CreateQuery("", sql)
'    qry(0) = val
'    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'    If rs.EOF Then
'      sql = "insert into MuestraAsistencia " _
'      & "(cMuestra, Estado) Values (?,?)"
'      Set qry = objAutoan.rdoConnect.CreateQuery("", sql)
'      qry(0) = val
'      qry(1) = 2
'      qry.Execute
'      qry.Close
'    End If
'    sql = "update MuestraPrueba Set cMuestra=? where cMuestra=?"
'    Set qry = objAutoan.rdoConnect.CreateQuery("", sql)
'    qry(0) = val
'    qry(1) = strMiCodMuestra
'    qry.Execute
'    qry.Close
    strMiCodMuestra = val
  End If
End Property

Public Property Let strNuevaMuestra(val As String)
Dim Sql$
Dim Qry As rdoQuery

  If strMiNuevaMuestra <> val Then
'    With objAutoan
'      .ColMuestras.Add Me, val
'      If .blnExistMuestra(strMiCodMuestra) Then
'        .ColMuestras.Remove strMiCodMuestra
'      End If
'    End With
    Call EscribirEnColumna(cteMuestraNewSample, val)
'    sql = "select cmuestra from muestraasistencia where cmuestra=?"
'    Set qry = objAutoan.rdoConnect.CreateQuery("", sql)
'    qry(0) = val
'    Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'    If rs.EOF Then
'      sql = "insert into MuestraAsistencia " _
'      & "(cMuestra, Estado) Values (?,?)"
'      Set qry = objAutoan.rdoConnect.CreateQuery("", sql)
'      qry(0) = val
'      qry(1) = 2
'      qry.Execute
'      qry.Close
'    End If
'    sql = "update MuestraPrueba Set cMuestra=? where cMuestra=?"
'    Set qry = objAutoan.rdoConnect.CreateQuery("", sql)
'    qry(0) = val
'    qry(1) = strMiCodMuestra
'    qry.Execute
'    qry.Close
    strMiNuevaMuestra = val
  End If
End Property

Public Property Let strIDMuestra(val As String)   '3
  strMiIDMuestra = val
  Call EscribirEnColumna(cteMuestraIDMUESTRA, val)
End Property

Public Property Let strEstado(val As constEstado)    '9
Dim objActuacion As clsActuacion
Dim blnIncompleta As Boolean
Dim DescEstado As String
Dim EstadoActuacion As constEstado

  If val <> strMiEstado Then
    DescEstado = strDescEstado(val)
    'para que una muestra marcada para repetir no cambie de estado
    'aunque tenga resultados correctos
    If Not (strMiEstado = cteAREPETIR And (val = cteREALIZADA Or val = cteFUERADERANGO)) Then
      strMiEstado = val
      Call EscribirEnColumna(cteMuestraESTADO, DescEstado)
    End If
    objAutoan.EscribirLog "EstadoMuestra (" & strCodMuestra & ") = " & DescEstado
    If val <> cteAREPETIR And val <> cteAREPETIRFIN Then
      objAutoan.blnRepetirPendiente = objAutoan.blnExistMuestraConEstado(cteAREPETIR) _
      Or objAutoan.blnExistMuestraConEstado(cteAREPETIRFIN)
    Else
      objAutoan.blnRepetirPendiente = True
    End If
  End If
  
  Select Case val
    Case cteSINPROGRAMAR
'Se comentariza porque si no cuando una muestra est� programada
'con varias pruebas, y dandole al boton de refrescar hay nuevas
'pruebas pendientes para esa muestra se ponian todas como no programadas
'y se volvian a programar al darle al boton de programar.
'      For Each objActuacion In ColMuestrasPruebas
'        If objActuacion.strEstado = cteACEPTADA Then
'         objActuacion.strEstado = cteSINPROGRAMAR
'        End If
'      Next
    Case cteACEPTADA
'      For Each objActuacion In ColMuestrasPruebas
'        If objActuacion.strEstado = cteSINPROGRAMAR Then
'          objActuacion.strEstado = cteACEPTADA
'        End If
'      Next
    Case cteREALIZADA
      If ColMuestrasPruebas.Count > 0 Then
        For Each objActuacion In ColMuestrasPruebas
          EstadoActuacion = objActuacion.strEstado
          If EstadoActuacion <> cteREALIZACIONCOMPLETA _
          And EstadoActuacion <> cteFUERADERANGOFIN _
          And EstadoActuacion <> cteAREPETIRFIN Then
            blnIncompleta = True: Exit For
          End If
        Next
        If Not blnIncompleta Then
          strMiEstado = cteREALIZACIONCOMPLETA
          Call EscribirEnColumna(cteMuestraESTADO, strDescEstado(strMiEstado))
          objAutoan.IsFinished 'en esta subrutina vemos si todas las muestras ya estan completas
          If objAutoan.blnAutoDescarga Then QuitarMuestra: objAutoan.pRefreshGrids
        End If
      End If
    Case cteFUERADERANGO
      If ColMuestrasPruebas.Count > 0 Then
        For Each objActuacion In ColMuestrasPruebas
          EstadoActuacion = objActuacion.strEstado
          If EstadoActuacion <> cteREALIZACIONCOMPLETA _
          And EstadoActuacion <> cteFUERADERANGOFIN _
          And EstadoActuacion <> cteAREPETIRFIN Then
            blnIncompleta = True: Exit For
          End If
        Next
        If Not blnIncompleta Then
          strMiEstado = cteFUERADERANGOFIN
          Call EscribirEnColumna(cteMuestraESTADO, strDescEstado(strMiEstado))
          objAutoan.IsFinished 'en esta subrutina vemos si todas las muestras ya estan completas
          If objAutoan.blnAutoDescarga Then QuitarMuestra: objAutoan.pRefreshGrids
        End If
      End If
    Case cteAREPETIR
      If ColMuestrasPruebas.Count > 0 Then
        For Each objActuacion In ColMuestrasPruebas
          EstadoActuacion = objActuacion.strEstado
          If EstadoActuacion <> cteREALIZACIONCOMPLETA _
          And EstadoActuacion <> cteFUERADERANGOFIN _
          And EstadoActuacion <> cteAREPETIRFIN Then
            blnIncompleta = True: Exit For
          End If
        Next
        If Not blnIncompleta Then
          strMiEstado = cteAREPETIRFIN
          Call EscribirEnColumna(cteMuestraESTADO, strDescEstado(strMiEstado))
          objAutoan.IsFinished 'en esta subrutina vemos si todas las muestras ya estan completas
        End If
      End If
  End Select
  
End Property

Public Property Let strPosicion(val As String)   '10
  strMiPosicion = val
  Call EscribirEnColumna(cteMuestraPOSICION, val)
End Property

Public Property Let blnUrgente(val As Boolean)   '11
  blnMiUrgente = val
  Call EscribirEnColumna(cteMuestraURGENTE, IIf(val, "SI", "NO"))
End Property

Public Property Let strSeccion(val As String)   '12
  strMiSeccion = val
  Call EscribirEnColumna(cteMuestraSECCION, val)
End Property

Public Property Let strDisco(val As String)   '13
  strMiDisco = val
  Call EscribirEnColumna(cteMuestraDISCO, val)
End Property

Public Property Let strCopa(val As String)   '14
  strMiCopa = val
  Call EscribirEnColumna(cteMuestraCOPA, val)
End Property

Public Property Let strBandeja(val As String)   '14
  strMiBandeja = val
  Call EscribirEnColumna(cteMuestraBANDEJA, val)
End Property

Public Property Let intDilucion(val As Double)  '15
  intMiDilucion = Format(val, "0.0")
  Call EscribirEnColumna(cteMuestraDILUCION, CStr(intMiDilucion))
End Property

Public Property Get intDilucion() As Double  '15
  intDilucion = intMiDilucion
End Property

Public Property Let blnRepetida(val As Boolean)
  blnMiRepetida = val
  Call EscribirEnColumna(cteMuestraNUMREPETICION, IIf(val, "SI", "NO"))
  'frmAutoan.ssdgMuestras.Refresh
End Property

Public Property Let intFila(val As Integer)   '14
  intMiFila = val
  Call EscribirEnColumna(cteMuestraFILA, CStr(val))
End Property


Public Property Get strCodMuestra() As String
  strCodMuestra = strMiCodMuestra
End Property

Public Property Get strNuevaMuestra() As String
  strNuevaMuestra = strMiNuevaMuestra
End Property

Public Property Get strIDMuestra() As String   '3
  strIDMuestra = strMiIDMuestra
End Property

Public Property Get strEstado() As constEstado   '9
  strEstado = strMiEstado
End Property

Public Property Get strPosicion() As String   '10
  strPosicion = strMiPosicion
End Property

Public Property Get blnUrgente() As Boolean   '11
  blnUrgente = blnMiUrgente
End Property

Public Property Get strSeccion() As String   '12
  strSeccion = strMiSeccion
End Property

Public Property Get strDisco() As String   '13
  strDisco = strMiDisco
End Property

Public Property Get strCopa() As String   '14
  strCopa = strMiCopa
End Property

Public Property Get strBandeja() As String   '24
  strBandeja = strMiBandeja
End Property

Public Property Get blnRepetida() As Boolean
  blnRepetida = blnMiRepetida
  'frmAutoan.ssdgMuestras.Refresh
End Property

Public Property Get intFila() As Integer   '14
  intFila = intMiFila
End Property

Public Property Get strProperty1() As String   '11
  strProperty1 = strMiProperty1
End Property

Public Property Get strProperty2() As String   '11
  strProperty2 = strMiProperty2
End Property

Public Property Get strProperty3() As String   '11
  strProperty3 = strMiProperty3
End Property

Public Property Get strEspecimen() As String   '11
  strEspecimen = strMiEspecimen
End Property

Public Property Get strEspecimenAuto() As String   '11
  strEspecimenAuto = strMiEspecimenAuto
End Property

Public Sub CambioManualEstado(val As constEstado)
Dim objActuacion As clsActuacion

  strEstado = val
  For Each objActuacion In ColMuestrasPruebas
    objActuacion.strEstado = val
  Next
End Sub

Public Sub CambioCarpeta(cCarpeta As Integer)
Dim objActuacion As clsActuacion

  For Each objActuacion In ColMuestrasPruebas
    objActuacion.CambioCarpeta (cCarpeta)
  Next
End Sub
