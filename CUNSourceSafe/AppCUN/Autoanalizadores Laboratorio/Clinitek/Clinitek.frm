VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Clinitek 200"
   ClientHeight    =   600
   ClientLeft      =   765
   ClientTop       =   1905
   ClientWidth     =   1605
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Clinitek.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   600
   ScaleWidth      =   1605
   Begin MSMask.MaskEdBox Mask 
      Height          =   315
      Left            =   720
      TabIndex        =   0
      Tag             =   "N� Sec Inicial"
      Top             =   240
      Width           =   795
      _ExtentX        =   1402
      _ExtentY        =   556
      _Version        =   327681
      BackColor       =   16777215
      MaxLength       =   5
      Mask            =   "#-###"
      PromptChar      =   " "
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   0
      Top             =   180
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "N� Sec. Inicial:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   -30
      Width           =   1995
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim res As Boolean
Dim strLog As String
Dim DemasiadosResultados As Integer 'Indica si se han recibido m�s resultados de los esperados y por tanto hay un error
Dim strFirstNumSec As String             'Primer n� de secuencia a partir del cual se comienza a leer
                                    'los mensajes que llegan
Dim blnLeerResultados As Integer       'True: los resultados que llegan son v�lidos
                                    'False: los resultados que llegan son antiguos
Dim strNumSec As String                  'N� de sucuencia del resultado que llega

Private Sub Clinitek_Espera(Mensaje As String)
'Identifica la llegada de los caracteres de comienzo de transmisi�n (comienzo del mensaje) _
y pasa al estado de transmisi�n.
  If Right$(Mensaje, 3) = Chr$(2) & Chr$(13) & Chr$(10) Then '<STX><CR><LF> - LLegada de los caracteres de comienzo del mensaje
    objAutoAn.Estado = cteTRANSMISION
    'Me.Refresh
    Mensaje = ""
  End If
End Sub

Private Sub Clinitek_Transmision(Mensaje As String)
'Identifica el final del mensaje.
Dim filenum 'Para el Log
'Si el autoanalizador est� configurado con la opci�n Computer Handshake ser� necesario _
enviarle una respuesta afirmativa <ACK><XON> para que transmita el pr�ximo resultado. _
Si no tiene esta opci�n el autoanalizador env�a continuamente resultados sin esperar _
ninguna respuesta hacieno caso omiso de los mensajes que le llegan excepto si se _
trata de un Xoff que interrumpe el paso de resultados.
  If Right$(Mensaje, 1) = Chr$(3) Then '<ETX> - LLegada del caracter de finalizaci�n del mensaje
    objAutoAn.Enviar Chr$(6) & Chr$(17) '<ACK> <XON> - Respuesta afirmativa y nuevo prompt
    'Si han llegado ya todos los resultados esperados y continuan llegando m�s, no se leer�n estos nuevos resultados y
    'cuando terminen de llegar se emitir� un mensaje diciendo que se ha producido un error en la recepci�n de resultados
    If DemasiadosResultados = False Then
      Call pLecturaDatos(Mensaje)
    Else
      Timer1.Enabled = True
    End If
    objAutoAn.Estado = cteESPERA
    Mensaje = ""
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim T As Long
Dim res As Integer
Dim SQL As String
Dim filenum

  On Error Resume Next
  'Para que deje de transmitir resultados
  'Comm1.Output = Chr$(19)         'Xoff
  objAutoAn.Enviar Chr$(19)
  'Para que se vac�e el buffer de salida y el Xoff sea enviado
  'If Comm1.PortOpen = True Then
  If objAutoAn.PortOpen Then
    T = Timer + 5
    Do While objAutoAn.OutBufferCount > 0
      res = DoEvents()
      If Timer > T Then
        SQL = "El buffer de salida no se ha vac�ado."
        Select Case MsgBox(SQL, vbExclamation + vbRetryCancel, Me.Caption)
          Case vbCancel: 'Exit Do
          Case vbRetry: T = Timer + 5
        End Select
      End If
    Loop
    'Comm1.PortOpen = False
    objAutoAn.pAbrirCerrarPuerto False
  End If
  DoEvents
End Sub

Private Sub pLecturaDatos(strDatos As String)
'Se leen los datos seg�n lo explicado en el manual. La 1� l�nea contiene la fecha _
    (que no se leer� por que no suele coincidir con la fecha real). Las 10 l�neas restantes _
    contienen los resultados de la prueba, que siempre aparecen en el mismo orden.
'Se buscan los resultados de la prueba y se a�aden al array de resultados. Cada uno de _
    los mensajes que llega contiene los 10 resultados de la prueba. Estos mensajes llegan _
    de forma ordenada seg�n lo dispuesto en el arActuaciones si las muestras se han ido _
    analizando en el orden en que aparecen en la lista de trabajo.
Dim contador As Integer
Dim Resultado As String
Dim unidad As String
Dim objMuestra As clsMuestra
Dim codMuestra As String
Dim codActuacion As String
Dim codResultado As Integer
Dim CodResultadoAuto As String
Dim blnIntroducido As Boolean
Static NumMuestra As Integer
On Error GoTo AutoAnError

  strNumSec = Mid$(strDatos, 2, 5)
  'para comprobar que el resultado que llega no _
  ha sido enviado ya antes por el autoanalizador
  For Each objMuestra In objAutoAn.ColMuestras
    If objMuestra.strPosicion = strNumSec Then Exit Sub
  Next
  If strFirstNumSec = strNumSec Or blnLeerResultados Then
    blnLeerResultados = True
    For contador = 1 To 10
      Resultado = Trim$(Mid$(strDatos, 22 * contador + 5, 16))
      CodResultadoAuto = Trim$(Mid$(strDatos, 22 * contador + 1, 3))
      Select Case CodResultadoAuto
        Case "PRO", "GLU"
          unidad = " mg/dL"
          If Right$(Resultado, Len(unidad)) = unidad Then
            Resultado = Left$(Resultado, Len(Resultado) - Len(unidad))
          End If
        Case "CET"
          Select Case Resultado
            Case "15 mg/dL": Resultado = "+"
            Case "40 mg/dL": Resultado = "++"
            Case "<=80 mg/dL": Resultado = "+++"
          End Select
        Case "BIL"
          Select Case Resultado
            Case "BAJO": Resultado = "Negativo"
            Case "MODERADO": Resultado = "+"
            Case "ALTO": Resultado = "++"
          End Select
        Case "SAN"
          Select Case Resultado
            Case "Apro 10 ERI/uL", "Apro 25 ERI/uL": Resultado = "+"
            Case "Apro 80 ERI/uL": Resultado = "++"
            Case "Apro 200 ERI/uL": Resultado = "+++"
          End Select
        Case "URO"
          Select Case Resultado
            Case "0.2 E.U./dL": Resultado = "Negativo"
            Case "1.0 E.U./dL", "2.0 E.U./dL": Resultado = "Negativo"
            Case "4.0 E.U./dL": Resultado = "+"
            Case ">=8.0 E.U./dL": Resultado = "++"
          End Select
        Case "LEU"
          Select Case Resultado
            Case "Apro 15 cel/uL", "Apro 70 cel/uL": Resultado = "+"
            Case "Apro 125 cel/uL": Resultado = "++"
            Case "Apro 500 cel/uL": Resultado = "+++"
          End Select
      End Select
'      If IsNumeric(Resultado) Then res = objAutoAn.fFormatearNumero(Resultado, ".", ",")
      Resultado = UCase$(Left$(Resultado, 1)) & LCase$(Mid$(Resultado, 2))
      If NumMuestra <= objAutoAn.ColMuestras.Count - 1 Then
        codMuestra = objAutoAn.ColMuestras(NumMuestra + 1).strCodMuestra
        Call objAutoAn.pIntroResultConCResAuto(codMuestra, CodResultadoAuto, Resultado, True)
        objAutoAn.ColMuestras(codMuestra).strPosicion = strNumSec
        blnIntroducido = True
      End If
    Next contador
    If blnIntroducido Then NumMuestra = NumMuestra + 1
  End If
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "pLecturaDatos", "strDatos" & strDatos)
End Sub

Public Sub Lectura_Protocolo(caracter As String)
Static Mensaje As String 'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es _
la que se env�a a otras subrutinas pudiendo ser modificada en �stas. Al ser modificada _
la copia, tambi�n deber� modificarse el original. El hecho de utilizar esta copia _
se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas _
por estar declarada como Static en esta subrutina
  Mensaje = Mensaje & caracter 'Se va componiendo el mensaje
  CopiaMensaje = Mensaje
  Select Case objAutoAn.Estado
    Case cteESPERA
      Clinitek_Espera CopiaMensaje
      Mensaje = CopiaMensaje
    Case cteTRANSMISION
      Clinitek_Transmision CopiaMensaje
      Mensaje = CopiaMensaje
  End Select
End Sub

Private Sub Label1_Click()

End Sub

Private Sub Mask_Change()
  objAutoAn.MaskChange
End Sub

Private Sub Timer1_Timer()
'Se esperan 30 segundos despu�s de la recepci�n del �ltimo resultado esperado para ver _
si llega alg�n resultado de m�s. Si es as�, se ha debido producir un error bien _
porque se hayan colocado m�s muestras que las que se indica en la lista de trabajo o _
bien porque al principio estuvieron llegando resultados de otras muestras anteriores _
a esta lista de trabajo que por cualquier causa no llegaron en su momento. En este caso _
se mostrar� un mensaje de error. En caso contrario se saldr� directamente de la ejecuci�n.
Dim msg As String
  If DemasiadosResultados = True Then
    msg = "Se han recibido resultados de un n�mero de muestras mayor que el especificado en la lista de trabajo."
    msg = msg & Chr$(10) & "Todos los resultados pueden ser incorrectos y se recomienda "
    msg = msg & "repetir completamente la lista de trabajo al realizar la validaci�n t�cnica."
    MsgBox msg, vbExclamation, Me.Caption
  End If
  Descargar
End Sub

Public Sub mask_KeyPress(KeyAscii As Integer)
Dim SQL As String
Dim resp As Integer

  If KeyAscii = 13 Then
    blnLeerResultados = False 'Por si se cambia el n� sec. inicial durante la llegada de resultados
    resp = Val(Mid$(Mask.Text, 3))
    Mask.Text = Val(Left$(Mask.Text, 1)) & "-" & Left$("000", 3 - Len(CStr(resp))) & resp
    SQL = "El n�mero de secuencia pr�ximo es el " & Mask.Text & Chr$(10)
    SQL = SQL & "�Est� Ud. de acuerdo?"
    resp = MsgBox(SQL, vbQuestion + vbYesNo, Me.Caption)
    Select Case resp
      Case vbYes
        strFirstNumSec = Mask.Text
        SendKeys "{TAB}"
        'Para que comience a recibir resultados
        'Comm1.Output = Chr$(17)    'Xon
        objAutoAn.Enviar Chr$(17)
      Case vbNo
        Mask.Text = " -   "
    End Select
  End If
End Sub

Public Sub Probar()
  objAutoAn.AutoRecepcion Chr$(2) & vbCrLf & "#1-579      08-06-80" & vbCrLf & "GLU         NEGATIVO" & vbCrLf & "BIL         NEGATIVO" & vbCrLf & "CET         NEGATIVO" & vbCrLf & "DEN            1.020" & vbCrLf & "SAN         NEGATIVO" & vbCrLf & "pH               5.5" & vbCrLf & "PRO         NEGATIVO" & vbCrLf & "URO      0.2 E.U./dL" & vbCrLf & "NIT         NEGATIVO" & vbCrLf & "LEU         NEGATIVO" & vbCrLf & Chr$(3)
End Sub

Public Sub Descargar()
  End
End Sub
