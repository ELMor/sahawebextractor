VERSION 5.00
Begin VB.Form frmGrupos 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   0  'None
   Caption         =   "Tablas de Grupos de Medicamentos"
   ClientHeight    =   8235
   ClientLeft      =   75
   ClientTop       =   525
   ClientWidth     =   12000
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8235
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin VB.PictureBox tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   390
      Left            =   0
      ScaleHeight     =   360
      ScaleWidth      =   11970
      TabIndex        =   7
      Top             =   0
      Width           =   12000
   End
   Begin VB.PictureBox tabSSTab1 
      Height          =   2985
      Left            =   60
      ScaleHeight     =   2955
      ScaleWidth      =   11865
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   930
      Width           =   11895
      Begin VB.PictureBox SSFrame1 
         Height          =   1845
         Left            =   240
         ScaleHeight     =   1815
         ScaleWidth      =   11355
         TabIndex        =   3
         Top             =   720
         Width           =   11385
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFF00&
            DataField       =   "FR02_GRUPO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   1
            Left            =   1590
            MaxLength       =   36
            TabIndex        =   1
            Tag             =   "Grupo | Nombre Grupo"
            Top             =   840
            Width           =   5025
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR02_CGRUPO"
            Height          =   285
            HelpContextID   =   30101
            Index           =   0
            Left            =   360
            MaxLength       =   6
            TabIndex        =   0
            Tag             =   "C�digo|C�digo Grupo"
            Top             =   840
            Width           =   975
         End
         Begin VB.Label Label1 
            Caption         =   "Descripci�n de Grupo"
            Height          =   255
            Index           =   1
            Left            =   1590
            TabIndex        =   5
            Top             =   540
            Width           =   2745
         End
         Begin VB.Label Label1 
            Caption         =   "C�digo"
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   4
            Top             =   540
            Width           =   735
         End
      End
      Begin VB.PictureBox grdSSDBGrid1 
         BackColor       =   &H00FFFFFF&
         Height          =   5175
         Left            =   -74880
         ScaleHeight     =   5145
         ScaleWidth      =   11625
         TabIndex        =   6
         Top             =   720
         Width           =   11655
      End
   End
   Begin VB.PictureBox stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   750
      Left            =   0
      ScaleHeight     =   720
      ScaleWidth      =   11970
      TabIndex        =   8
      Top             =   7485
      Width           =   12000
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edicion"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener"
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero                              CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior                              Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente                           Av Pag"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo                                CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&RefrescarRegistros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &Masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de ayuda                              F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Public objWinInfo As New clsCWWin ' realizar una instancia de la clase ventana de la libreria
Private Sub Form_Load()
    ' crear una instancia de la clase clsCWForm que contendr� informaci�n sobre el formulario
    Dim objDetailInfo As New clsCWForm
    
    'crear informaci�n de la ventana
    Call objWinInfo.WinCreateInfo(cwFormDetail, _
                                      cwModeSingleEdit, _
                                      Me, _
                                      tlbToolbar1, _
                                      stbStatusBar1, _
                                      cwWithAll)
   
    'definir las propiedades de la clase formulario
  With objDetailInfo
    Set .objFormContainer = Me 'contenedor principal
    Set .objFatherContainer = Nothing 'contenedor padre
    Set .tabMainTab = tabSSTab1
    Set .grdGrid = grdSSDBGrid1
    .strDataBase = objData.GetValue("main")
    .strTable = "FR0200" ' nombre de la tabla principal a la que se vincular�n los controles del formulario
'    .strInitialWhere = ""
    'ordenaci�n de la consulta . columna de la tabla y tipo de ordenaci�n (ascendente)
    Call .FormAddOrderField("FR02_CGRUPO", False)
  End With
  
  Call CreateFilterInfo(objDetailInfo)
  
  
  'Call objWinInfo.FormCreateFilterWhere(objDetailInfo, "FR0200", "Grupos", "", _
                                Array(Array("FR02_CGRUPO", "C�digo", vbString, ""), _
                                   Array("FR02_GRUPO", "Descripci�n", vbString, "")), "", "")
                                   
  'Call objWinInfo.FormCreateFilterOrder(objDetailInfo, _
                               Array(Array("FR02_CGRUPO", "C�digo"), _
                                     Array("FR02_GRUPO", "Descripci�n")))
  
  With objWinInfo
   Call .FormAddInfo(objDetailInfo) 'a�adir el formulario a la colecci�n de formularios
   'obtener toda la informaci�n de los controles del formulario
   Call .FormCreateInfo(objDetailInfo)
'   Para utilizar el formulario est�ndar de localizaci�n de registros ser� necesario indicar
'   que controles linkados del formulario van a intervenir en la opci�n de b�squedas
     .CtrlGetInfo(txtText1(0)).blnInFind = True
     .CtrlGetInfo(txtText1(1)).blnInFind = True
'
'  'campos char en la tabla pero con edicion num�rica
'    .stdCtrlGetInfo(txtText1(5)).intMask = cwMaskInteger
'    .stdCtrlGetInfo(SSDBCombo1(0)).intMask = cwMaskInteger
'
'  'cargar ssdbcombo de provincias
'    Call LoadProvincias
'
   Call .WinRegister 'registrar la ventana en el sistema
   Call .WinStabilize 'estabilizar la ventana con todos sus formularios
  End With
   
End Sub
Private Sub Form_QueryUnload(intCancel As Integer, intUnloadMode As Integer)
    ' salida controlada de la ventana activa
    intCancel = objWinInfo.WinExit
End Sub
Private Sub Form_Unload(intCancel As Integer)
    'eliminar la ventana de la colecci�n de ventanas de la aplicaci�n
    Call objWinInfo.WinDeRegister
    'liberar de la memoria toda la informaci�n referente a la ventana eliminada y a todos sus
    'objetos dependientes
    Call objWinInfo.WinRemoveInfo
End Sub
Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
    ' analiza las teclas pulsadas sobre el formulario activo y procesa aquellas que tengan
    ' alguna funcionalidad asociada
    Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub
Private Sub grdSSDBGrid1_DblClick()
    'cambia a la solapa DETALLE actualizando los datos del formulario con la informaci�n de
    'la fila seleccionada
    Call objWinInfo.GridDblClick
End Sub
Private Sub grdSSDBGrid1_UnboundReadData(ByVal objRowBuf As ssRowBuffer, _
                                        vntStartLocation As Variant, _
                                        ByVal blnReadPriorRows As Boolean)
    'alimenta la informaci�n del GRID en modo UNBOUND durante la navegaci�n.
    Call objWinInfo.GridUnboundRead(objRowBuf, vntStartLocation, blnReadPriorRows)
End Sub
Private Sub grdSSDBGrid1_RowColChange(ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
    'controla el movimiento de los registrosen el Grid.
'    Call objWinInfo.DataChangeGridRowCol(LastRow, LastCol)
End Sub
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    ' esta funci�n analiza el bot�n seleccionado y ejecuta la acci�n asociada
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.index, 0)
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    ' analiza el bot�n seleccionado y ejecuta la acci�n asociada
    Select Case btnButton.Key
    Case cwToolBarButtonOpen
        Call objWinInfo.WinProcess(cwProcessRegister, 10, 0)
    Case Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.index, 0)
    End Select
End Sub
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    'en los eventos Click de todas las opciones del men� est�ndar
    'evalua la opci�n de men� seleccionada y ejecuta la acci�n correspondiente
    Select Case intIndex
    Case 20
        Call objWinInfo.WinProcess(cwProcessRegister, 10, 0)
    Case Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
    End Select
End Sub
Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub
Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub
Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub
Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub
Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub
'Private Sub tabSSTab1_Click(intPreviousTab As Integer)
'    'avisa al formulario que se ha producido un cambio de solapa y realiza las acciones
'    'derivadas de dicho cambio
'    Call objWinInfo.FormChangeTab(intPreviousTab)
'End Sub
Private Sub txtText1_Change(intIndex As Integer)
    'comunicar al formulario que se han realizado cambios en el control y habilitar
    'la opci�n Grabar
    Call objWinInfo.CtrlDataChange
End Sub
Private Sub txtText1_GotFocus(intIndex As Integer)
    'comunicar al formulario cual es el control que recibe el foco y realizar las acciones
    'pertinentes,tales como cambiar el control activo,actualizar los paneles de la barra
    'de estado,etc.
    Call objWinInfo.CtrlGotFocus
End Sub
Private Sub txtText1_KeyPress(intIndex As Integer, intKeyAscii As Integer)
    'controlar la pulsaci�n de teclas del control activo.
    intKeyAscii = objWinInfo.CtrlKeyPress(txtText1(intIndex), intKeyAscii)
End Sub
Private Sub txtText1_LostFocus(intIndex As Integer)
    'controlar p�rdida del foco en los controles.Las acciones a ejecutar ser�n diferentes
    'en funci�n del modo del formulario y la informaci�n interna de los controles.
    'este m�todo podr� realizar desde validaciones sencillas sobre la informaci�n de los
    'controles hasta forzar lecturas diversas dela base de datos
    Call objWinInfo.CtrlLostFocus
 '   If Index = 5 Then
 '       Call RevisarCodigoPostal
 '   End If
End Sub
Private Sub CreateFilterInfo(ByVal objFormInfo As clsCWForm)
  Call objWinInfo.FormCreateFilterWhere(objFormInfo, objData.GetValue("Main") & "FR0200", "Grupos", "", _
                                Array(Array("FR02_CGRUPO", "C�digo", cwString, ""), _
                                   Array("FR02_GRUPO", "Descripci�n", cwString, "")), "", "")
  Call objWinInfo.FormCreateFilterOrder(objFormInfo, _
                               Array(Array("FR02_CGRUPO", "C�digo"), _
                                     Array("FR02_GRUPO", "Descripci�n")))

End Sub
Public Function cwEventHandler(ByVal intEvento As Integer, _
                              ByVal vntData1 As Variant, _
                              ByVal vntData2 As Variant) As Boolean
'    Dim rdoContaContactos As rdoResultset
'    Dim strSQL As String
       Select Case intEvento
        Case cwEventPreRead
        Case cwEventPostRead
        Case cwEventPreWrite
        Case cwEventPostWrite
        Case cwEventPreDelete
        Case cwEventPostDelete
        Case cwEventPreRestore
        Case cwEventPostRestore
        Case cwEventPreValidate
        Case cwEventPostValidate
        Case cwEventPreDefault
        Case cwEventPostDefault
        Case cwEventPreChangeStatus
        Case cwEventPostChangeStatus
        Case cwEventPreChanged
        Case cwEventPostChanged
    End Select
End Function

