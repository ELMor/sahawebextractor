VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPruebasResultados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento del Archivo"
   ClientHeight    =   7050
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7050
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VsOcxLib.VideoSoftAwk Awk 
      Left            =   240
      Top             =   780
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Frame fraFicha 
      Caption         =   "Ficha"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2835
      Left            =   0
      TabIndex        =   15
      Top             =   3780
      Width           =   11790
      Begin TabDlg.SSTab tabFicha 
         Height          =   2385
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   300
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   4207
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "frmPruebasResultados.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(2)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "dtcDateCombo1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "cboSSTecnica"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtFicha(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtFicha(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtFicha(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "frmPruebasResultados.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssFicha"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtFicha 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX01HISTORIA"
            Height          =   285
            Index           =   9
            Left            =   9540
            MaxLength       =   7
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "N� Historia"
            Top             =   960
            Visible         =   0   'False
            Width           =   1230
         End
         Begin VB.TextBox txtFicha 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX02NUMFICHA"
            Height          =   285
            Index           =   1
            Left            =   6360
            MaxLength       =   5
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "N� Ficha"
            Top             =   480
            Width           =   1230
         End
         Begin VB.TextBox txtFicha 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX02DESORGANO"
            Height          =   645
            Index           =   0
            Left            =   2220
            MaxLength       =   300
            TabIndex        =   8
            Tag             =   "Descripci�n de T�cnica"
            Top             =   1260
            Width           =   8550
         End
         Begin SSDataWidgets_B.SSDBGrid grdssFicha 
            Height          =   2265
            Left            =   -74940
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   60
            Width           =   11050
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19491
            _ExtentY        =   3995
            _StockProps     =   79
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSTecnica 
            DataField       =   "RX05CODTECNICA"
            Height          =   285
            Left            =   2220
            TabIndex        =   7
            Tag             =   "T�cnica de Estudio"
            Top             =   900
            Width           =   3495
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "T�cnica de Estudio"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   6165
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "RX02FECEXPLORA"
            Height          =   330
            Index           =   0
            Left            =   2220
            TabIndex        =   6
            Tag             =   "Fecha de Exploraci�n"
            Top             =   480
            Width           =   1560
            _Version        =   65537
            _ExtentX        =   2752
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de Exploraci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   25
            Top             =   540
            Width           =   2055
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Ficha:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   5460
            TabIndex        =   24
            Top             =   540
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Organo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   1440
            TabIndex        =   23
            Top             =   1260
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            Caption         =   "T�cnica de Estudio:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   420
            TabIndex        =   22
            Top             =   900
            Width           =   1755
         End
      End
   End
   Begin VB.Frame fraHistoria 
      Caption         =   "Historia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3240
      Left            =   60
      TabIndex        =   12
      Top             =   420
      Width           =   11790
      Begin TabDlg.SSTab tabHistoria 
         Height          =   2880
         HelpContextID   =   90001
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   300
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   5080
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "frmPruebasResultados.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(22)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(21)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(20)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(9)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtHistoria(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtHistoria(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtHistoria(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtHistoria(1)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtHistoria(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtHistoria(5)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "frmPruebasResultados.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssHistoria"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtHistoria 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX01NUMDISCO"
            Height          =   285
            Index           =   5
            Left            =   7920
            MaxLength       =   10
            TabIndex        =   5
            Tag             =   "N� Sobre"
            Top             =   480
            Width           =   1470
         End
         Begin VB.TextBox txtHistoria 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX01NUMSOBRE"
            Height          =   285
            Index           =   0
            Left            =   7920
            MaxLength       =   7
            TabIndex        =   4
            Tag             =   "N� Sobre"
            Top             =   120
            Width           =   1230
         End
         Begin VB.TextBox txtHistoria 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX01HISTORIA"
            Height          =   285
            Index           =   1
            Left            =   2220
            MaxLength       =   7
            TabIndex        =   0
            Tag             =   "Historia"
            Top             =   120
            Width           =   1230
         End
         Begin VB.TextBox txtHistoria 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX01SEGAPEL"
            Height          =   285
            Index           =   4
            Left            =   2220
            MaxLength       =   35
            TabIndex        =   3
            Tag             =   "Apellido 2"
            Top             =   1080
            Width           =   5370
         End
         Begin VB.TextBox txtHistoria 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX01PRIAPEL"
            Height          =   285
            Index           =   3
            Left            =   2220
            MaxLength       =   35
            TabIndex        =   2
            Tag             =   "Apellido 1"
            Top             =   780
            Width           =   5370
         End
         Begin VB.TextBox txtHistoria 
            BackColor       =   &H00FFFFFF&
            DataField       =   "RX01NOMBRE"
            Height          =   285
            Index           =   2
            Left            =   2220
            MaxLength       =   15
            TabIndex        =   1
            Tag             =   "Nombre"
            Top             =   480
            Width           =   2790
         End
         Begin SSDataWidgets_B.SSDBGrid grdssHistoria 
            Height          =   1470
            Left            =   -74940
            TabIndex        =   11
            Top             =   60
            Width           =   11055
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19500
            _ExtentY        =   2593
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Disco:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   9
            Left            =   6960
            TabIndex        =   28
            Top             =   540
            Width           =   1110
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N� Sobre:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   6960
            TabIndex        =   27
            Top             =   180
            Width           =   1110
         End
         Begin VB.Label lblLabel1 
            Caption         =   "2� Apellido:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   1140
            TabIndex        =   21
            Top             =   1140
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "1� Apellido:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   21
            Left            =   1140
            TabIndex        =   20
            Top             =   840
            Width           =   1005
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   1380
            TabIndex        =   19
            Top             =   540
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Historia:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   1380
            TabIndex        =   18
            Top             =   180
            Width           =   1110
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   16
      Top             =   6765
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
      Begin VB.Menu mnubuscar 
         Caption         =   "&Buscar"
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmPruebasResultados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Public WithEvents objWin As clsCWWin
Attribute objWin.VB_VarHelpID = -1
Public objHistoria As New clsCWForm
Public objFicha As New clsCWForm
Public objDiagnostico As New clsCWForm
Dim objSearch As New clsCWSearch
Dim objField As New clsCWFieldSearch
 
Dim Modif1, Modif2
Dim AntSqlArea$
Dim AntSqlDiagnostico$

Const constComboArea = 1
Const constComboDiagnostico = 1
Const constTextoArea = 2
Const constTextoDiagnostico = 2

Public Sub ContarPacientes()
Dim sqlcount
Dim rs As rdoResultset

    On Error Resume Next
    sqlcount = "SELECT COUNT(*) from RX0100" & IIf(frmA_Archivo.objHistoria.STRWHERE <> "", " WHERE " & frmA_Archivo.objHistoria.STRWHERE, "")
    Set rs = objApp.rdoConnect.OpenResultset(sqlcount, rdOpenKeyset, _
    rdConcurReadOnly, rdAsyncEnable + rdExecDirect)
    While frmA_Archivo.stbStatusBar1.Panels(6).Text <> rs(0) & " Pacientes"
    frmA_Archivo.stbStatusBar1.Panels(6).Text = rs(0) & " Pacientes"
    Wend
    'frmA_Archivo.stbStatusBar1.Panels(6).Text = rs(0) & " Pacientes"
    
End Sub

Sub Impresion()
  Dim StrSql$
  Dim printjob%
  Dim lngText As Long
  Dim inttext%
  Dim errortext$
  Dim Leftw%
  Dim Topw%
  
  Call PELogOnSQLServerWithPrivateInfo("pdsodbc.dll", objApp.rdoConnect.hDbc)
  printjob = PEOpenPrintJob("c:\rayos\rx001.rpt")
    
    StrSql = "SELECT" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01NUMSOBRE" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX04CODAREA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX04DESAREA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX05CODTECNICA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX05DESTECNICA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX06CODDIAGNOSTICO" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX06DESDIAGNOSTICO" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX03INDCONFIRMADO" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX03OBSERVAC" & Chr$(34) & "," & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01NOMBRE" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01PRIAPEL" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01SEGAPEL" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0100." & Chr$(34) & "RX01NUMDISCO" & Chr$(34) & "," & vbCrLf
    StrSql = StrSql & "RX0200." & Chr$(34) & "RX02FECEXPLORA" & Chr$(34) & ", " & vbCrLf
    StrSql = StrSql & "RX0200." & Chr$(34) & "RX02DESORGANO" & Chr$(34) & vbCrLf
    StrSql = StrSql & "From" & vbCrLf
    StrSql = StrSql & "RX0101J, RX0100, RX0200" & vbCrLf
    StrSql = StrSql & "Where" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " = RX0100." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " AND" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " = RX0200." & Chr$(34) & "RX01HISTORIA" & Chr$(34) & " AND" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & " = RX0200." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & " AND" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01NUMSOBRE" & Chr$(34) & " = 1" & vbCrLf
    StrSql = StrSql & "Order By" & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX01NUMSOBRE" & Chr$(34) & " ASC," & vbCrLf
    StrSql = StrSql & "RX0101J." & Chr$(34) & "RX02NUMFICHA" & Chr$(34) & " ASC" & vbCrLf
    
  Call PESetSQLQuery(printjob, StrSql)
  'Debemos refrescar los datos
  Call PEDiscardSavedData(printjob)
  
'  If blnExtraInfo Then
'    On Error Resume Next
''    .Formulas(0) = "CODIGO_INFORME='" & .ReportFileName & "'"
'    Call PESetFormula(printjob, "CODIGO_INFORME", "RX01NUMSOBRE = 1 ")
'    On Error GoTo cwIntError
'  End If
  
'  If mintDestination = 0 Then
    Leftw = (Screen.Width \ Screen.TwipsPerPixelX) \ 16
    Topw = (Screen.Height \ Screen.TwipsPerPixelY) \ 16
    Call PEOutputToWindow(printjob, "", Leftw, Topw, Leftw * 14, Topw * 14, 0, 0)
'  Else
'    Call PEOutputToPrinter(printJob, 1)
'  End If
  
  Call PEStartPrintJob(printjob, True)
  
  If PEGetErrorText(printjob, lngText, inttext) <> 0 Then
    errortext = String(inttext, 0)
    If PEGetHandleString(lngText, errortext, inttext) <> 1 Then
'        Call objError.SetError(cwCodeMsg, msgPrintError, _
'                           mcllDescriptions(mintSelected), vbCrLf, 1, ErrorText)
'        Call objError.Raise
'        ShowReport = False
'    Else
'        ShowReport = True
    End If
  End If
'
  Call PEClosePrintJob(printjob)
  End Sub

Function fNextLinea() As String
    Dim SQL$, rsMaxClave As rdoResultset
    
    SQL = "SELECT max(RX03CODIDENTDIAG) + 1 FROM RX0300 WHERE RX01HISTORIA = " & txtHistoria(1).Text
    SQL = SQL & " and RX02NUMFICHA = " & txtFicha(1).Text
    
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(SQL)
    
    If Not rsMaxClave.EOF And Not IsNull(rsMaxClave(0)) Then
        fNextLinea = rsMaxClave(0)
    Else
        fNextLinea = 1
    End If
    
    rsMaxClave.Close
    
End Function

Function fNextSobre() As String
    Dim SQL$, rsMaxClave As rdoResultset
    
    SQL = "SELECT max(RX01NUMSOBRE) + 1 FROM RX0100 "
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(SQL)
    If Not rsMaxClave.EOF And Not IsNull(rsMaxClave(0)) Then
        fNextSobre = rsMaxClave(0)
    Else
        fNextSobre = 1
    End If
    
    rsMaxClave.Close
    
End Function

Function fNextFicha(Hist As Long) As String
    Dim SQL$, rsMaxClave As rdoResultset
    
    SQL = "SELECT max(RX02NUMFICHA) + 1 FROM RX0200 WHERE RX01HISTORIA = " & Hist
        
    Set rsMaxClave = objApp.rdoConnect.OpenResultset(SQL)
    
    If Not rsMaxClave.EOF And Not IsNull(rsMaxClave(0)) Then
        fNextFicha = rsMaxClave(0)
    Else
        fNextFicha = 1
    End If
    
    rsMaxClave.Close
    
End Function

Sub HabilitarCampos()
'  Select Case txtTipo(0)
'  Case 1
'    txtPatologo(1).Enabled = True
'    cboSSPersonal.Enabled = False
'  Case 2
'    txtPatologo(1).Enabled = False
'    cboSSPersonal.Enabled = True
'  Case Else
'    cboSSPersonal.Enabled = False
'    txtPatologo(1).Enabled = False
'  End Select

End Sub

Private Sub cbossPersonal_Click()
  cbossPersonal_LostFocus
End Sub

Private Sub cmdClave_Click()
'Dim nombre As String
'
'    If Trim(txtPatologo(6).Text) = "" Then
'      nombre = txtPatologo(3).Text & " " & txtPatologo(4).Text & ", " & txtPatologo(2).Text
'    Else
'      nombre = txtPatologo(6).Text
'    End If
'    Load frmIDDr
'    With frmIDDr
'      .Height = 2200
'      If objWinInfo.intWinStatus = 3 Then
'        .fraNuevo.Visible = True
'        .txtNuevoPwd(0).Text = ""
'        .fraPwd.Visible = False
'      Else
'        .fraNuevo.Visible = False
'        .fraPwd.Visible = True
'      End If
'      .cmdNueva.Visible = True
'      .txtPat.Text = nombre
'      .txtPwd.Tag = txtPatologo(0).Text
'      .Show vbModal
'    End With
'    Unload frmIDDr
End Sub

Private Sub cboSSArea_Change()
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSArea_Click()
    txtDiagnostico(0).Text = cboSSArea.Columns(0).Text
End Sub

Private Sub cboSSArea_DropDown()
    Dim Words() As String, i%, SQL$
    
    If Trim(txtDiagnostico(0).Text) <> "" And (Modif1 = constTextoArea Or (cboSSArea.Text = "" And Modif1 = constComboArea)) And Not (cboSSArea.Text <> "" And Modif1 = constComboArea) Then
        'SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM " _
                & objEnv.GetValue("Main") & "RX0400 where "
        SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM RX0400 where "
        SQL = SQL & " upper(RX04CODAREA) LIKE '" & UCase(txtDiagnostico(0).Text) & "%'"
        SQL = SQL & "ORDER BY RX04CODAREA"
    ElseIf cboSSArea.Text <> "" And (Modif1 = constComboArea Or (Trim(txtDiagnostico(0).Text) = "" And Modif1 = constTextoArea)) Then
        Awk = cboSSArea.Text
        Awk.FS = " "
        For i = 1 To Awk.NF
            If Not IsNull(Trim(Awk.F(i))) Then
                ReDim Preserve Words(1 To i)
                Words(i) = Trim(Awk.F(i))
            End If
        Next i
        'SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM " _
                & objEnv.GetValue("Main") & "RX0400 where "
        SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM RX0400 where "
        For i = 1 To UBound(Words)
            SQL = SQL & " upper(RX04DESAREA) LIKE '%" & UCase(Words(i)) & "%' AND"
        Next i
        SQL = Left$(SQL, Len(SQL) - 3) & "ORDER BY RX04CODAREA"
    ElseIf txtDiagnostico(0).Text <> "" Then
        'SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM " _
                & objEnv.GetValue("Main") & "RX0400 where "
        SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM RX0400 where "
        SQL = SQL & " upper(RX04CODAREA) LIKE '" & UCase(txtDiagnostico(0).Text) & "%'"
        SQL = SQL & "ORDER BY RX04CODAREA"
    Else
        'SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM " _
                & objEnv.GetValue("Main") & "RX0400 "
        SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM RX0400 "
        SQL = SQL & "ORDER BY RX04CODAREA"
        If SQL <> AntSqlArea Then objApp.SplashOn ("Cargando Combo..."): DoEvents
    End If
    'solo ejecutamos la carga del combo si realmente ha cambiado la seleccion de registros
    If SQL <> AntSqlArea Then
        AntSqlArea = SQL
        objWinInfo.CtrlGetInfo(cboSSArea).StrSql = SQL
        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSArea))
        objApp.SplashOff
    End If
    
End Sub

Private Sub cboSSArea_GotFocus()
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSArea_KeyDown(KeyCode As Integer, Shift As Integer)
Modif1 = constComboArea
End Sub

Private Sub cboSSArea_LostFocus()
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDiagnostico_Change()
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDiagnostico_Click()
    txtDiagnostico(1).Text = cboSSDiagnostico.Columns(0).Text
End Sub

Private Sub cboSSDiagnostico_DropDown()

Dim Words() As String, i%, SQL$
Static AntSQL$

    If Trim(txtDiagnostico(1).Text) <> "" And (Modif2 = constTextoDiagnostico Or (cboSSDiagnostico.Text = "" And Modif2 = constComboDiagnostico)) And Not (cboSSDiagnostico.Text <> "" And Modif2 = constComboDiagnostico) Then
        'SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM " _
                & objEnv.GetValue("Main") & "RX0600 where "
        SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM RX0600 where "
        SQL = SQL & " upper(RX06CODDIAGNOSTICO) LIKE '" & UCase(txtDiagnostico(1).Text) & "%'"
        SQL = SQL & "ORDER BY RX06CODDIAGNOSTICO"
    ElseIf cboSSDiagnostico.Text <> "" And (Modif2 = constComboDiagnostico Or (Trim(txtDiagnostico(0).Text) = "" And Modif2 = constTextoDiagnostico)) Then
        Awk = cboSSDiagnostico.Text
        Awk.FS = " "
        For i = 1 To Awk.NF
            If Not (Trim(Awk.F(i)) = "") Then
                ReDim Preserve Words(1 To i)
                Words(i) = Trim(Awk.F(i))
            End If
        Next i
        'SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM " _
                & objEnv.GetValue("Main") & "RX0600 where "
        SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM RX0600 where "
        For i = 1 To UBound(Words)
            SQL = SQL & " upper(RX06DESDIAGNOSTICO) LIKE '%" & UCase(Words(i)) & "%' AND"
        Next i
        SQL = Left$(SQL, Len(SQL) - 3) & "ORDER BY RX06CODDIAGNOSTICO"
    ElseIf txtDiagnostico(1).Text <> "" Then
        'SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM " _
                & objEnv.GetValue("Main") & "RX0600 where "
        SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM RX0600 where "
        SQL = SQL & " upper(RX06CODDIAGNOSTICO) LIKE '" & UCase(txtDiagnostico(1).Text) & "%'"
        SQL = SQL & "ORDER BY RX06CODDIAGNOSTICO"
    Else
        'SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM " _
                & objEnv.GetValue("Main") & "RX0600 "
        SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM RX0600 "
        SQL = SQL & "ORDER BY RX06CODDIAGNOSTICO"
        If SQL <> AntSQL Then objApp.SplashOn ("Cargando Combo..."): DoEvents
    End If
    'solo ejecutamos la carga del combo si realmente ha cambiado la seleccion de registros
    If SQL <> AntSqlDiagnostico Then
        AntSqlDiagnostico = SQL
        objWinInfo.CtrlGetInfo(cboSSDiagnostico).StrSql = SQL
        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDiagnostico))
        objApp.SplashOff
    End If
    
End Sub

Private Sub cboSSDiagnostico_GotFocus()
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDiagnostico_KeyDown(KeyCode As Integer, Shift As Integer)
Modif2 = constComboDiagnostico
End Sub

Private Sub cboSSDiagnostico_LostFocus()
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSTecnica_GotFocus()
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub Check1_Click()

End Sub

Private Sub chkConfirmado_Click()
Call objWinInfo.CtrlDataChange
End Sub

Private Sub chkConfirmado_GotFocus()
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkConfirmado_LostFocus()
Call objWinInfo.CtrlLostFocus
End Sub

Private Sub Form_DblClick()
Stop
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  'stbStatusBar1.Panels.Add 1, "e", "hola"
  
  stbStatusBar1.Panels(1).Width = 4500
  stbStatusBar1.Panels(2).Visible = False
  stbStatusBar1.Panels.Add 6, ""
  stbStatusBar1.Panels(6).Width = 2900
  stbStatusBar1.Panels(6).Alignment = sbrRight
  
  With objHistoria
    .strName = "Historia"
    Set .objFormContainer = fraHistoria
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabHistoria
    Set .grdGrid = grdssHistoria
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "RX0100"
    .intAllowance = cwAllowAll
    .blnAskPrimary = True
    Call .FormAddOrderField("RX01NUMSOBRE", cwAscending)
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "RX0100")
    Call .FormAddFilterWhere(strKey, "RX01HISTORIA", "Historia", cwNumeric)
    Call .FormAddFilterWhere(strKey, "RX01NUMSOBRE", "N� Sobre", cwNumeric)
'    Call .FormCreateFilterWhere(strKey, "RX0300")
'    Call .FormAddFilterWhere(strKey, "RX03DESDIAGNOSTICO", "Diagn�stico", cwString)
    Call .objPrinter.Add("Rx001", "Archivo Diagn�stico")
  End With
  
  With objFicha
    .strName = "Ficha"
    Set .objFormContainer = fraFicha
    Set .objFatherContainer = fraHistoria
    Set .tabMainTab = tabFicha
    Set .grdGrid = grdssFicha
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "RX0200"
    .blnAskPrimary = False
    Call .FormAddOrderField("RX02NUMFICHA", cwAscending)
    Call .FormAddRelation("RX01HISTORIA", txtHistoria(1))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "RX0200")
    Call .FormAddFilterWhere(strKey, "RX05CODTECNICA", "C�digo de T�cnica", cwNumeric)
    Call .objPrinter.Add("Rx001", "Archivo Diagn�stico")
  End With
  
  With objDiagnostico
    .strName = "Diagn�stico"
    Set .objFormContainer = fraDiagnostico
    Set .objFatherContainer = fraFicha
    Set .tabMainTab = TabDiagnostico
    Set .grdGrid = grdssDiagnostico
    '.intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "RX0300"
    .intAllowance = cwAllowAll
    .blnAskPrimary = False
    
    Call .FormAddOrderField("RX03CODIDENTDIAG", cwAscending)
    Call .FormAddRelation("RX02NUMFICHA", txtFicha(1))
    Call .FormAddRelation("RX01HISTORIA", txtHistoria(1))

    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "RX0300")
    Call .FormAddFilterWhere(strKey, "RX06CODDIAGNOSTICO", "C�digo de Diagn�stico", cwNumeric)
    Call .FormAddFilterWhere(strKey, "RX04CODAREA", "C�digo de Localizaci�n", cwNumeric)
    Call .FormAddFilterWhere(strKey, "RX03OBSERVAC", "Observaciones", cwString)
    'Call .FormAddFilterWhere(strKey, "RX03DESDIAGNOSTICO", "Diagn�stico", cwString)
    Call .objPrinter.Add("Rx001", "Archivo Diagn�stico")
  End With
      
  With objWinInfo
    Call .FormAddInfo(objHistoria, cwFormDetail)
    Call .FormAddInfo(objFicha, cwFormDetail)
    Call .FormAddInfo(objDiagnostico, cwFormDetail)

'    Call .GridAddColumn(objCarpetas, "Personal", "AP08_CODPers", cwNumeric, 4)
'    Call .GridAddColumn(objCarpetas, "Carpeta", "AP06_CODCarp", cwString, 50)

'    Call .FormAddInfo(objOrganos, cwFormMultiLine)
'    Call .GridAddColumn(objOrganos, "Personal", "AP08_CODPers", cwNumeric, 4)
'    Call .GridAddColumn(objOrganos, "Organo", "AP23_CODOrg", cwString, 50)
'
    Call .FormCreateInfo(objHistoria)
''    .CtrlGetInfo(txtTipo(0)).blnValidate = False
    .CtrlGetInfo(txtFicha(1)).blnValidate = False
    .CtrlGetInfo(txtDiagnostico(3)).blnValidate = False
    
'
    '.CtrlGetInfo(cboSSTecnica).StrSql = "SELECT RX05CODTECNICA ,RX05DESTECNICA FROM " _
        & objEnv.GetValue("Main") & "RX0500 ORDER BY RX05CODTECNICA"
        .CtrlGetInfo(cboSSTecnica).StrSql = "SELECT RX05CODTECNICA ,RX05DESTECNICA FROM " _
         & "RX0500 ORDER BY RX05CODTECNICA"
'    .CtrlGetInfo(cboSSArea).strSQL = "SELECT RX04CODAREA ,RX04DESAREA FROM " _
'        & objEnv.GetValue("Main") & "RX0400 ORDER BY RX04CODAREA"
'    .CtrlGetInfo(cboSSDiagnostico).strSQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM " _
'        & objEnv.GetValue("Main") & "RX0600 ORDER BY RX06CODDIAGNOSTICO"

   .CtrlGetInfo(txtHistoria(0)).blnInFind = True
   .CtrlGetInfo(txtHistoria(1)).blnInFind = True
   .CtrlGetInfo(txtFicha(1)).blnInFind = True
   .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
   .CtrlGetInfo(cboSSArea).blnInFind = True
   .CtrlGetInfo(cboSSTecnica).blnInFind = True
   .CtrlGetInfo(cboSSDiagnostico).blnInFind = True
   .CtrlGetInfo(txtDiagnostico(1)).blnInFind = True
   .CtrlGetInfo(txtHistoria(2)).blnInFind = True
   .CtrlGetInfo(txtHistoria(3)).blnInFind = True
   .CtrlGetInfo(txtHistoria(4)).blnInFind = True
'   .CtrlGetInfo(cboSSArea).blnForeign = True
'   .CtrlGetInfo(cboSSDiagnostico).blnForeign = True
   
'   .CtrlGetInfo(txtHistoria(0)).blnForeign = True
'   .CtrlGetInfo(txtHistoria(1)).blnForeign = True
'   .CtrlGetInfo(txtHistoria(2)).blnForeign = True
'   .CtrlGetInfo(txtHistoria(3)).blnForeign = True
'   .CtrlGetInfo(txtHistoria(4)).blnForeign = True
'   .CtrlGetInfo(txtHistoria(5)).blnForeign = True
'   .CtrlGetInfo(txtFicha(0)).blnForeign = True
'   .CtrlGetInfo(txtFicha(1)).blnForeign = True
'   .CtrlGetInfo(dtcDateCombo1(0)).blnForeign = True
'   .CtrlGetInfo(cboSSTecnica).blnForeign = True
'   .CtrlGetInfo(txtDiagnostico(0)).blnForeign = True
'   .CtrlGetInfo(txtDiagnostico(1)).blnForeign = True
'   .CtrlGetInfo(txtDiagnostico(2)).blnForeign = True
'   .CtrlGetInfo(chkConfirmado).blnForeign = True
   
   
   .CtrlGetInfo(txtFicha(9)).blnInGrid = False
   .CtrlGetInfo(txtDiagnostico(3)).blnInGrid = False
   .CtrlGetInfo(txtDiagnostico(5)).blnInGrid = False
   .CtrlGetInfo(txtDiagnostico(11)).blnInGrid = False
   
    Call .WinRegister
    Call .WinStabilize
  End With
  tlbToolbar1.Buttons(6).Enabled = True
  ContarPacientes
  Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdssDiagnostico_Change()
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub grdssDiagnostico_DblClick()
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdssDiagnostico_GotFocus()
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssDiagnostico_RowColChange(ByVal vntLastRow As Variant, ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'prueba

'Set objSearch = New clsCWSearch
'With objSearch
'    .strTable = "RX0101J"
'    .STRWHERE = ""
'    .strOrder = " ORDER BY RX01HISTORIA,RX02NUMFICHA,RX03CODIDENTDIAG"
'    Set objField = .AddField("RX01HISTORIA")
'    objField.strSmallDesc = "Historia"
'    objField.blnInDialog = False
'    objField.blnInGrid = True
'    Set objField = .AddField("RX01NUMSOBRE")
'    objField.strSmallDesc = "N� Sobre"
'    objField.blnInDialog = False
'    objField.blnInGrid = True
'    Set objField = .AddField("RX02NUMFICHA")
'    objField.strSmallDesc = "N� Ficha"
'    objField.blnInDialog = False
'    objField.blnInGrid = False
'    Set objField = .AddField("RX03CODIDENTDIAG")
'    objField.strSmallDesc = "C�digo de Linea"
'    objField.blnInDialog = False
'    objField.blnInGrid = False
'    Set objField = .AddField("RX05CODTECNICA")
'    objField.strSmallDesc = "C�digo de T�cnica"
'    objField.blnInDialog = False
'    objField.blnInGrid = False
'    Set objField = .AddField("RX05DESTECNICA")
'    objField.strSmallDesc = "T�cnica"
'    objField.intSize = 15
'    Set objField = .AddField("RX04CODAREA")
'    objField.strSmallDesc = "C�digo de Localizaci�n"
'    objField.intSize = 4
'    Set objField = .AddField("RX04DESAREA")
'    objField.strSmallDesc = "Localizaci�n"
'    objField.intSize = 15
'    Set objField = .AddField("RX06CODDIAGNOSTICO")
'    objField.strSmallDesc = "C�digo de Diagn�stico"
'    objField.intSize = 7
'    Set objField = .AddField("RX06DESDIAGNOSTICO")
'    objField.strSmallDesc = "Diagn�stico"
'    objField.intSize = 15
'    Set objField = .AddField("RX03INDCONFIRMADO")
'    objField.strSmallDesc = "Confirmado"
'    objField.intType = cwBoolean
'    Set objField = .AddField("RX03OBSERVAC")
'    objField.strSmallDesc = "Observaciones"
'    objField.intSize = 15
'
'    If .Search Then
'        objPipe.PipeSet "flthistoria", .cllValues(1)
'        objPipe.PipeSet "fltsobre", .cllValues(2)
'        objPipe.PipeSet "fltficha", .cllValues(3)
'        objPipe.PipeSet "fltcodtecnica", .cllValues(5)
'        objPipe.PipeSet "fltdestecnica", .cllValues(6)
'        objPipe.PipeSet "fltcodarea", .cllValues(7)
'        objPipe.PipeSet "fltdesarea", .cllValues(8)
'        objPipe.PipeSet "fltcoddiagnoctico", .cllValues(9)
'        objPipe.PipeSet "fltdesdiagnostico", .cllValues(10)
'        objPipe.PipeSet "fltindconfirmado", .cllValues(11)
'        objPipe.PipeSet "fltobservac", .cllValues(12)
'
'        txtHistoria(1).SetFocus
'        Call txtHistoria_GotFocus(1)
'        objWinInfo.DataOpen
'        Call objWinInfo.CtrlSet(txtHistoria(1), .cllValues(1))
'        objWinInfo.CtrlLostFocus
''        Call objWinInfo.CtrlSet(txtFicha(9), .cllValues(1))
''        Call objWinInfo.CtrlSet(txtDiagnostico(11), .cllValues(1))
'        txtFicha(1).SetFocus
'        Call txtFicha_GotFocus(1)
'        objWinInfo.DataOpen
'        Call objWinInfo.CtrlSet(txtFicha(9), .cllValues(1))
'        Call objWinInfo.CtrlSet(txtFicha(1), .cllValues(3))
'        objWinInfo.CtrlLostFocus
''        Call objWinInfo.CtrlSet(txtDiagnostico(5), .cllValues(2))
'        txtDiagnostico(3).SetFocus
'        Call txtDiagnostico_GotFocus(3)
'        objWinInfo.DataOpen
'        Call objWinInfo.CtrlSet(txtDiagnostico(3), .cllValues(4))
'        Call objWinInfo.CtrlSet(txtDiagnostico(11), .cllValues(1))
'        Call objWinInfo.CtrlSet(txtDiagnostico(5), .cllValues(3))
'        objWinInfo.CtrlLostFocus
'    End If
'End With
'Set objSearch = Nothing
End Sub

Private Sub SSTab1_DblClick()

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Static AntSQL$
Dim SQL$
 tlbToolbar1.Buttons(6).Enabled = True
'  If strFormName = "Tipo" Then
'    With objWinInfo
'      If txtTipo(0).Text > 2 Then
'        .CtrlGetInfo(txtTipo(1)).blnReadOnly = False
'        .CtrlGetInfo(txtTipo(2)).blnReadOnly = False
'      Else
'        .CtrlGetInfo(txtTipo(1)).blnReadOnly = True
'        .CtrlGetInfo(txtTipo(2)).blnReadOnly = True
'      End If
'    End With
'  End If
'If Trim$(txtDiagnostico(0)) <> "" Then
'    SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM " _
'                & objEnv.GetValue("Main") & "RX0400 where "
'        SQL = SQL & " upper(RX04CODAREA) LIKE '" & UCase(txtDiagnostico(0).Text) & "%'"
'        SQL = SQL & "ORDER BY RX04CODAREA"
'    If SQL <> AntSQL Then
'        AntSQL = SQL
'        objWinInfo.CtrlGetInfo(cboSSArea).strSQL = SQL
'        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSArea))
'    End If
'End If
'If Trim$(txtDiagnostico(1)) <> "" Then
'    SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM " _
'                    & objEnv.GetValue("Main") & "RX0600 where "
'            SQL = SQL & " upper(RX06CODDIAGNOSTICO) LIKE '" & UCase(txtDiagnostico(1).Text) & "%'"
'            SQL = SQL & "ORDER BY RX06CODDIAGNOSTICO"
'    If SQL <> AntSQL Then
'        AntSQL = SQL
'        objWinInfo.CtrlGetInfo(cboSSDiagnostico).strSQL = SQL
'        Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDiagnostico))
'    End If
'End If
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
'    Select Case strFormName
'        Case objTipo.strName
'           If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'                txtTipo(0).Text = fNextLinea("AP07_CODTiPers")
'                objTipo.rdoCursor("AP07_CODTiPers") = txtTipo(0).Text
'            End If
'        Case objPersonal.strName
'           If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'                txtPatologo(0).Text = fNextLinea("AP08_CODPers")
'                objPersonal.rdoCursor("AP08_CODPers") = txtPatologo(0).Text
'            End If
'            If cboSSPersonal.Text = "" Then
'
'            End If
'    End Select
    Select Case strFormName
        Case objHistoria.strName
            If Trim$(txtHistoria(0).Text) = "" Then
                objHistoria.rdoCursor("RX01NUMSOBRE") = fNextSobre()
            End If
        Case objFicha.strName
            If Trim$(txtFicha(1).Text) = "" Then
            objFicha.rdoCursor("RX02NUMFICHA") = fNextFicha(txtHistoria(1).Text)
            End If
        Case objDiagnostico.strName
'            objDiagnostico.rdoCursor("RX01HISTORIA") = txtHistoria(1).Text
'            objDiagnostico.rdoCursor("RX02NUMFICHA") = txtFicha(1).Text
            objDiagnostico.rdoCursor("RX03CODIDENTDIAG") = fNextLinea()
    End Select
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTab1_DblClick(Index As Integer)

End Sub

Private Sub TabDiagnostico_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(TabDiagnostico, False, True)
  Call HabilitarCampos
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
    If btnButton.Key = cwToolBarButtonFind Then
        frmImpresion.Caption = "Filtro de Consulta"
        frmImpresion.cmdAccion.Caption = "&Consultar"
        frmImpresion.Show vbModal
    ElseIf btnButton.Key = cwToolBarButtonPrint Then
        frmImpresion.Caption = "Filtro de Impresi�n"
        frmImpresion.cmdAccion.Caption = "&Imprimir"
        frmImpresion.Show vbModal
    Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        If btnButton.Key = cwToolBarButtonSave Or btnButton.Key = cwToolBarButtonDelete Then ContarPacientes
    End If
'  If objWinInfo.objWinActiveForm.strName = "Personal" Then
'    MsgBox "FORM ACTIVO: Personal"
'  '******* SOLUCIONADO ********
'
'  End If
'  If btnButton.Index = 2 Then txtPatologo(8) = txtTipo(0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssHistoria_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssHistoria_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssHistoria_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssHistoria_Change()
  Call objWinInfo.CtrlDataChange
End Sub

'=======================================================
Private Sub grdssFicha_GotFocus()
  Call objWinInfo.CtrlGotFocus
  Call HabilitarCampos
End Sub

Private Sub grdssFicha_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssFicha_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssFicha_Change()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabHistoria_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabHistoria, False, True)
End Sub

'================================================================
Private Sub tabficha_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabFicha, False, True)
  Call HabilitarCampos
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraHistoria_Click()
  Call objWinInfo.FormChangeActive(fraHistoria, False, True)
End Sub

'==================================================================
Private Sub fraFicha_Click()
  Call objWinInfo.FormChangeActive(fraFicha, False, True)
  Call HabilitarCampos
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
 Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkTipo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkTipo_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTipo_Click()
  Call objWinInfo.CtrlDataChange
End Sub

'=================================================================
Private Sub chkPatologo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkPatologo_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkPatologo_Click()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cbossPersonal_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossPersonal_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossPersonal_CloseUp()
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTipo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTipo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTipo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

'==================================================================
Private Sub txtPatologo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  Call HabilitarCampos
End Sub

Private Sub txtPatologo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtPatologo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtDiagnostico_Change(Index As Integer)

Dim SQL$

Call objWinInfo.CtrlDataChange

If Index = 0 Then
    If Trim$(txtDiagnostico(0)) <> "" Then
        'SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM " _
                    & objEnv.GetValue("Main") & "RX0400 where "
            SQL = "SELECT RX04CODAREA ,RX04DESAREA FROM RX0400 where "
            SQL = SQL & " upper(RX04CODAREA) LIKE '" & UCase(txtDiagnostico(0).Text) & "%'"
            SQL = SQL & "ORDER BY RX04CODAREA"
        If SQL <> AntSqlArea Then
            AntSqlArea = SQL
            objWinInfo.CtrlGetInfo(cboSSArea).StrSql = SQL
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSArea))
        End If
    End If
ElseIf Index = 1 Then
    If Trim$(txtDiagnostico(1)) <> "" Then
        'SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM " _
                    & objEnv.GetValue("Main") & "RX0600 where "
        SQL = "SELECT RX06CODDIAGNOSTICO ,RX06DESDIAGNOSTICO FROM RX0600 where "
            SQL = SQL & " upper(RX06CODDIAGNOSTICO) LIKE '" & UCase(txtDiagnostico(1).Text) & "%'"
            SQL = SQL & "ORDER BY RX06CODDIAGNOSTICO"
        If SQL <> AntSqlDiagnostico Then
            AntSqlDiagnostico = SQL
            objWinInfo.CtrlGetInfo(cboSSDiagnostico).StrSql = SQL
            Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cboSSDiagnostico))
        End If
    End If
End If

End Sub

Private Sub txtDiagnostico_GotFocus(Index As Integer)
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtDiagnostico_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If Index = 0 Then Modif1 = constTextoArea
If Index = 1 Then Modif2 = constTextoDiagnostico
End Sub

Private Sub txtDiagnostico_LostFocus(Index As Integer)
Call objWinInfo.CtrlLostFocus
If Index = 0 Then
    cboSSArea.FirstRow = "2"
End If
End Sub

Private Sub txtFicha_Change(Index As Integer)
Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtFicha_GotFocus(Index As Integer)
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtFicha_LostFocus(Index As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtHistoria_Change(Index As Integer)
Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtHistoria_GotFocus(Index As Integer)
Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtHistoria_LostFocus(Index As Integer)
Dim SQL$, rsPaciente As rdoResultset
Dim a%

    If Index = 1 And Trim$(txtHistoria(1).Text) <> "" And (objWinInfo.intWinStatus = cwModeSingleAddKey Or objWinInfo.intWinStatus = cwModeSingleAddRest) Then
        SQL = "SELECT nom,ap1,ap2 FROM pac WHERE nh= " & txtHistoria(1).Text
        Set rsPaciente = objApp.rdoConnect.OpenResultset(SQL)
        If Not rsPaciente.EOF And Not IsNull(rsPaciente(1)) Then
            txtHistoria(2) = rsPaciente(0)
            txtHistoria(3) = rsPaciente(1)
            txtHistoria(4) = rsPaciente(2)
        End If
        rsPaciente.Close
    ElseIf Index = 0 And Trim$(txtHistoria(0).Text) <> "" Then
        SQL = "select rx01numsobre from rx0100 where rx01numsobre=" & txtHistoria(0).Text
        Set rsPaciente = objApp.rdoConnect.OpenResultset(SQL)
        If Not rsPaciente.EOF Then
            a = MsgBox("Escoja Otro N� de Sobre Por Favor", vbCritical + vbOKOnly, "�N� Sobre Duplicado!")
            txtHistoria(0).SelStart = 0
            txtHistoria(0).SelLength = Len(txtHistoria(0).Text)
            txtHistoria(0).SetFocus
        End If
    End If
    Call objWinInfo.CtrlLostFocus
End Sub
