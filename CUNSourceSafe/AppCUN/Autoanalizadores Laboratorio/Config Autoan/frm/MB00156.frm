VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmG_Almacenamiento 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Almacenamiento de Colonias"
   ClientHeight    =   2985
   ClientLeft      =   75
   ClientTop       =   360
   ClientWidth     =   7020
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   7020
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancelar"
      Height          =   390
      Left            =   5625
      TabIndex        =   1
      Top             =   2400
      Width           =   1140
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "Aceptar"
      Default         =   -1  'True
      Height          =   390
      Left            =   4020
      TabIndex        =   0
      Top             =   2400
      Width           =   1140
   End
   Begin Threed.SSPanel sspanel1 
      Height          =   975
      Index           =   1
      Left            =   180
      TabIndex        =   2
      Top             =   1320
      Width           =   6615
      _Version        =   65536
      _ExtentX        =   11668
      _ExtentY        =   1720
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin SSDataWidgets_B.SSDBCombo cbossMotivo 
         Height          =   315
         Left            =   2280
         TabIndex        =   11
         Tag             =   "Problema|Tipo de Problema"
         Top             =   360
         Width           =   2115
         DataFieldList   =   "Column 1"
         AllowInput      =   0   'False
         AllowNull       =   0   'False
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Caption=   "Designaci�n"
         Columns(1).Name =   "Designaci�n"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   3731
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16776960
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "Motivo de Almacenamiento:"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   3
         Top             =   420
         Width           =   2055
      End
   End
   Begin Threed.SSPanel sspanel1 
      Height          =   1035
      Index           =   0
      Left            =   180
      TabIndex        =   4
      Top             =   240
      Width           =   6615
      _Version        =   65536
      _ExtentX        =   11668
      _ExtentY        =   1826
      _StockProps     =   15
      Caption         =   "Identificaci�n"
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin VB.TextBox txtIdentificacion 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   0
         Left            =   1260
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtIdentificacion 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   1
         Left            =   5400
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   420
         Width           =   1035
      End
      Begin VB.TextBox txtIdentificacion 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   2
         Left            =   3480
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   420
         Width           =   795
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "N� Referencia:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "N� Muestra:"
         Height          =   255
         Index           =   1
         Left            =   4500
         TabIndex        =   9
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label lblIdentificacion 
         Caption         =   "N� Colonia:"
         Height          =   255
         Index           =   2
         Left            =   2580
         TabIndex        =   8
         Top             =   480
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmG_Almacenamiento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim nRef As String
Dim cMuestra As String
Dim CodCol As String
Dim cMotivo As Integer
Private Sub pNombreMotivo()
    Dim rsMotivos As rdoResultset, qryMotivos As rdoQuery
    Dim SQL As String

    ' se mira si la colonia ya fue almacenada anteriormente
    SQL = "SELECT MB1900.MB19_codMotivo, MB19_Desig FROM MB2700,MB1900"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    SQL = SQL & " AND MB1900.MB19_codMotivo = MB2700.MB19_codMotivo"
    SQL = SQL & " AND MB19_INDActiva=-1"
    Set qryMotivos = objApp.rdoConnect.CreateQuery("", SQL)
    qryMotivos(0) = nRef
    qryMotivos(1) = CodCol
    Set rsMotivos = qryMotivos.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsMotivos.EOF = False Then
        cMotivo = rsMotivos(0)
        cbossMotivo.Text = rsMotivos(1)
    End If
    rsMotivos.Close
    qryMotivos.Close
    
End Sub

Private Sub cbossMotivo_Click()
cMotivo = cbossMotivo.Columns(0).Text
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOK_Click()
    Dim res As Boolean
    
    res = fGuardarMotivo()
    If res = True Then
        Unload Me
    End If
End Sub

Private Function fGuardarMotivo() As Boolean
    Dim SQL As String
    Dim qryMotivo As rdoQuery
    Dim rsMotivo As rdoResultset
    Dim ahora As String
    
    On Error Resume Next
    If cMotivo > 0 Then
        ahora = fFechaHoraActual
        'cMotivo = cbossMotivo.Columns(0).Text
        
        Err = 0
        SQL = "UPDATE MB2700 SET"
        SQL = SQL & " MB19_CODMotivo = ?"
        SQL = SQL & ",MB27_FECALM = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
        SQL = SQL & " WHERE NREF = ?"
        SQL = SQL & " AND MB27_CODCOL = ?"
        Set qryMotivo = objApp.rdoConnect.CreateQuery("Almacenar", SQL)
        
        qryMotivo(0) = cMotivo
        qryMotivo(1) = ahora
        qryMotivo(2) = nRef
        qryMotivo(3) = CodCol
        Set rsMotivo = qryMotivo.OpenResultset(rdOpenKeyset)
        If Err <> 0 Then
            MsgBox "Imposible guardar los datos.", vbError, "Almacenamiento"
        Else
            fGuardarMotivo = True
        End If
        rsMotivo.Close
        qryMotivo.Close
    Else
        MsgBox "Debe especificar el motivo de almacenamiento.", vbExclamation, "Almacenamiento"
    End If
End Function


Private Sub Form_Load()
    nRef = objPipe.PipeGet("nRef")
    cMuestra = objPipe.PipeGet("cMuestra")
    CodCol = objPipe.PipeGet("codCol")
    cMotivo = 0
    
    pPrepararVentana
    pCargarComboMotivos
    pNombreMotivo
End Sub


Private Sub pCargarComboMotivos()
    ' Llena el combo con los diversos motivos de almacenamiento definidos
    Dim rsMotivos As rdoResultset
    Dim SQL As String

    
    ' Se recogen los tipos de motivos
    SQL = "SELECT MB19_CODMotivo,MB19_Desig FROM MB1900"
    SQL = SQL & " WHERE MB19_INDActiva=-1"
    SQL = SQL & " ORDER BY MB19_Desig"
    Set rsMotivos = objApp.rdoConnect.OpenResultset(SQL, rdOpenForwardOnly, rdConcurReadOnly)
    Do While Not rsMotivos.EOF
        cbossMotivo.AddItem rsMotivos(0) & Chr$(9) & rsMotivos(1)
        rsMotivos.MoveNext
    Loop
    rsMotivos.Close
    
    ' Se actualiza el color del combo a obligatorio
    cbossMotivo.BackColor = objApp.objColor.lngMandatory
End Sub

Private Sub pPrepararVentana()
    
    txtIdentificacion(0).Text = Val(Mid$(nRef, 6))
    
    If cMuestra <> "" Then
        txtIdentificacion(1).Text = cMuestra
        txtIdentificacion(1).Visible = True
        lblIdentificacion(1).Visible = True
    Else
        txtIdentificacion(1).Visible = False
        lblIdentificacion(1).Visible = False
    End If
    If CodCol <> "" Then
        txtIdentificacion(2).Text = CodCol
        txtIdentificacion(2).Visible = True
        lblIdentificacion(2).Visible = True
    Else
        txtIdentificacion(2).Visible = False
        lblIdentificacion(2).Visible = False
    End If

End Sub

