VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmA_Personal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenimiento del Archivo"
   ClientHeight    =   7770
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11175
   ClipControls    =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7770
   ScaleWidth      =   11175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraDetalle 
      Caption         =   "Diagn�sticos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   2445
      Left            =   75
      TabIndex        =   35
      Top             =   4875
      Width           =   10890
      Begin TabDlg.SSTab tabDatos 
         Height          =   2040
         Left            =   75
         TabIndex        =   36
         Top             =   300
         Width           =   10590
         _ExtentX        =   18680
         _ExtentY        =   3598
         _Version        =   327681
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   520
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         TabCaption(0)   =   "Carpeta"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "fraCarpeta"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Organo"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "fraOrgano"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.Frame fraOrgano 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   1590
            Left            =   -74850
            TabIndex        =   39
            Top             =   375
            Width           =   10290
            Begin SSDataWidgets_B.SSDBGrid grdssOrgano 
               Height          =   1530
               Index           =   0
               Left            =   0
               TabIndex        =   40
               Top             =   0
               Width           =   10245
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   18071
               _ExtentY        =   2699
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame fraCarpeta 
            BorderStyle     =   0  'None
            Caption         =   "Frame1"
            Height          =   1590
            Left            =   75
            TabIndex        =   37
            Top             =   375
            Width           =   10440
            Begin SSDataWidgets_B.SSDBGrid grdssCarpeta 
               Height          =   1530
               Index           =   0
               Left            =   75
               TabIndex        =   38
               Top             =   0
               Width           =   10245
               _Version        =   131078
               DataMode        =   2
               BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Col.Count       =   0
               RowNavigation   =   1
               CellNavigation  =   1
               ForeColorEven   =   0
               BackColorEven   =   16776960
               RowHeight       =   423
               SplitterPos     =   1
               SplitterVisible =   -1  'True
               Columns(0).Width=   3200
               Columns(0).DataType=   8
               Columns(0).FieldLen=   4096
               _ExtentX        =   18071
               _ExtentY        =   2699
               _StockProps     =   79
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
      End
   End
   Begin VB.Frame fraPersonal 
      Caption         =   "Ficha"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2295
      Left            =   75
      TabIndex        =   17
      Top             =   2475
      Width           =   10890
      Begin TabDlg.SSTab tabPersonal 
         Height          =   1905
         Left            =   150
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   300
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   3360
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(0)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(18)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(19)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(20)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(21)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(22)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(26)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "cboSSPersonal"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "cmdClave"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtPatologo(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtPatologo(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtPatologo(5)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtPatologo(4)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtPatologo(3)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtPatologo(2)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "chkPatologo"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtPatologo(0)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtPatologo(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtPatologo(8)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).ControlCount=   20
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssPersonal"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP07_CODTiPers"
            Height          =   285
            Index           =   8
            Left            =   1950
            MaxLength       =   50
            TabIndex        =   33
            Tag             =   "Referencia|Texto de Referencia"
            Top             =   225
            Visible         =   0   'False
            Width           =   480
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP08_Clave"
            Height          =   285
            Index           =   7
            Left            =   5625
            MaxLength       =   50
            TabIndex        =   32
            Tag             =   "Password"
            Top             =   1200
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP08_CODPers"
            Height          =   285
            Index           =   0
            Left            =   1275
            TabIndex        =   4
            Tag             =   "Personal|C�digo del Personal"
            Top             =   300
            Width           =   570
         End
         Begin VB.CheckBox chkPatologo 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo:"
            DataField       =   "AP08_INDActiva"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   6675
            TabIndex        =   10
            Tag             =   "Activo|Registro Activo"
            Top             =   1200
            Width           =   990
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP08_Nom"
            Height          =   285
            Index           =   2
            Left            =   1275
            MaxLength       =   50
            TabIndex        =   5
            Tag             =   "Nobre"
            Top             =   600
            Width           =   4230
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP08_Ap1"
            Height          =   285
            Index           =   3
            Left            =   1275
            MaxLength       =   50
            TabIndex        =   6
            Tag             =   "Apellido 1"
            Top             =   900
            Width           =   4230
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP08_Ap2"
            Height          =   285
            Index           =   4
            Left            =   1275
            MaxLength       =   50
            TabIndex        =   7
            Tag             =   "Apellido 2"
            Top             =   1200
            Width           =   4230
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP08_Ref"
            Height          =   285
            Index           =   5
            Left            =   7425
            MaxLength       =   50
            TabIndex        =   8
            Tag             =   "Referencia|Texto de Referencia"
            Top             =   600
            Width           =   1005
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "AP08_Firma"
            Height          =   285
            Index           =   6
            Left            =   7425
            MaxLength       =   50
            TabIndex        =   9
            Tag             =   "Firma|Texto de Firma"
            Top             =   900
            Width           =   2655
         End
         Begin VB.TextBox txtPatologo 
            BackColor       =   &H00FFFFFF&
            DataField       =   "CDr"
            Height          =   285
            Index           =   1
            Left            =   4500
            MaxLength       =   50
            TabIndex        =   23
            Tag             =   "C�digo|C�digo del Doctor en IBM"
            Top             =   300
            Width           =   1005
         End
         Begin VB.CommandButton cmdClave 
            Caption         =   "&Contrase�a"
            Height          =   390
            Left            =   8850
            TabIndex        =   11
            Top             =   1350
            Width           =   1290
         End
         Begin SSDataWidgets_B.SSDBGrid grdssPersonal 
            Height          =   1740
            Left            =   -74850
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   75
            Width           =   10005
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   17648
            _ExtentY        =   3069
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSPersonal 
            DataField       =   "AP08_CodPers_Aso"
            Height          =   285
            Left            =   7425
            TabIndex        =   34
            Tag             =   "Pat�logo asociado"
            Top             =   300
            Width           =   2655
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   1508
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3387
            Columns(1).Caption=   "Pat�logo"
            Columns(1).Name =   "Descripci�n"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4683
            _ExtentY        =   503
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   26
            Left            =   525
            TabIndex        =   31
            Top             =   300
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Nombre:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   22
            Left            =   375
            TabIndex        =   30
            Top             =   600
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            Caption         =   "1� Apellido:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   21
            Left            =   75
            TabIndex        =   29
            Top             =   900
            Width           =   1005
         End
         Begin VB.Label lblLabel1 
            Caption         =   "2� Apellido:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   20
            Left            =   75
            TabIndex        =   28
            Top             =   1200
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Referencia:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   19
            Left            =   6150
            TabIndex        =   27
            Top             =   600
            Width           =   1230
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Texto de firma:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   18
            Left            =   5925
            TabIndex        =   26
            Top             =   900
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo IBM (dr):"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   2850
            TabIndex        =   25
            Top             =   300
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Pat�logo Asociado:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   5625
            TabIndex        =   24
            Top             =   300
            Width           =   1755
         End
      End
   End
   Begin VB.Frame fraTipo 
      Caption         =   "Historia"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1920
      Left            =   90
      TabIndex        =   14
      Top             =   450
      Width           =   10890
      Begin TabDlg.SSTab tabTipo 
         Height          =   1575
         HelpContextID   =   90001
         Left            =   135
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   300
         Width           =   10560
         _ExtentX        =   18627
         _ExtentY        =   2778
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "ry00100.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtTipo(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtTipo(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtTipo(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "chkTipo"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).ControlCount=   7
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdssTipo"
         Tab(1).ControlCount=   1
         Begin VB.CheckBox chkTipo 
            Alignment       =   1  'Right Justify
            Caption         =   "Activo"
            DataField       =   "AP07_INDActiva"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   8250
            TabIndex        =   3
            Tag             =   "Activo|Registro activo"
            Top             =   300
            Width           =   990
         End
         Begin VB.TextBox txtTipo 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP07_Descrip"
            Height          =   285
            Index           =   2
            Left            =   1500
            MaxLength       =   50
            TabIndex        =   2
            Tag             =   "Descripci�n|Descripci�n del Tipo de Personal"
            Top             =   825
            Width           =   7755
         End
         Begin VB.TextBox txtTipo 
            BackColor       =   &H0000FFFF&
            DataField       =   "AP07_CODTiPers"
            Height          =   285
            Index           =   0
            Left            =   1500
            TabIndex        =   0
            Tag             =   "Tipo de Personal|C�digo del Tipo de Personal"
            Top             =   375
            Width           =   495
         End
         Begin VB.TextBox txtTipo 
            BackColor       =   &H00FFFF00&
            DataField       =   "AP07_Desig"
            Height          =   285
            Index           =   1
            Left            =   3750
            MaxLength       =   50
            TabIndex        =   1
            Tag             =   "Designaci�n|Designaci�n del Tipo de Personal"
            Top             =   375
            Width           =   4125
         End
         Begin SSDataWidgets_B.SSDBGrid grdssTipo 
            Height          =   1395
            Left            =   -74850
            TabIndex        =   13
            Top             =   75
            Width           =   9900
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   17462
            _ExtentY        =   2461
            _StockProps     =   79
            ForeColor       =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   300
            TabIndex        =   22
            Top             =   825
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   750
            TabIndex        =   21
            Top             =   375
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Designaci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2475
            TabIndex        =   20
            Top             =   375
            Width           =   1110
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   18
      Top             =   7485
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Personal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
  Dim objTipo As New clsCWForm
  Dim objCarpetas As New clsCWForm
  Dim objOrganos As New clsCWForm
  Dim objPersonal As New clsCWForm



Sub HabilitarCampos()
  Select Case txtTipo(0)
  Case 1
    txtPatologo(1).Enabled = True
    cboSSPersonal.Enabled = False
  Case 2
    txtPatologo(1).Enabled = False
    cboSSPersonal.Enabled = True
  Case Else
    cboSSPersonal.Enabled = False
    txtPatologo(1).Enabled = False
  End Select

End Sub

Private Sub cbossPersonal_Click()
  cbossPersonal_LostFocus
End Sub

Private Sub cmdClave_Click()
Dim nombre As String
    
    If Trim(txtPatologo(6).Text) = "" Then
      nombre = txtPatologo(3).Text & " " & txtPatologo(4).Text & ", " & txtPatologo(2).Text
    Else
      nombre = txtPatologo(6).Text
    End If
    Load frmIDDr
    With frmIDDr
      .Height = 2200
      If objWinInfo.intWinStatus = 3 Then
        .fraNuevo.Visible = True
        .txtNuevoPwd(0).Text = ""
        .fraPwd.Visible = False
      Else
        .fraNuevo.Visible = False
        .fraPwd.Visible = True
      End If
      .cmdNueva.Visible = True
      .txtPat.Text = nombre
      .txtPwd.Tag = txtPatologo(0).Text
      .Show vbModal
    End With
    Unload frmIDDr
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim strKey As String
  
  Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objTipo
    .strName = "Tipo"
    Set .objFormContainer = fraTipo
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTipo
    Set .grdGrid = grdssTipo
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "AP0700"
    .intAllowance = cwAllowAll
    .blnAskPrimary = False
    Call .FormAddOrderField("AP07_CODTiPers", cwAscending)
  End With
  
  With objPersonal
    .strName = "Personal"
    Set .objFormContainer = fraPersonal
    Set .objFatherContainer = fraTipo
    Set .tabMainTab = tabPersonal
    Set .grdGrid = grdssPersonal
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "AP0800"
    .blnAskPrimary = False
    Call .FormAddOrderField("AP08_CODPers", cwAscending)
    Call .FormAddRelation("AP07_CODTiPers", txtTipo(0))
  
  End With
  
  With objCarpetas
    .strName = "Carpetas"
    Set .objFormContainer = fraCarpeta
    Set .objFatherContainer = fraPersonal
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssCarpeta(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "AP1300"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("AP06_CODCarp", cwAscending)
    Call .FormAddRelation("AP08_CODPers", txtPatologo(0))
  
  End With
   
  With objOrganos
    .strName = "Organos"
    Set .objFormContainer = fraOrgano
    Set .objFatherContainer = fraPersonal
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssOrgano(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
    .strDataBase = objEnv.GetValue("Main")
    .strTable = "AP2400"
    .intAllowance = cwAllowAll
    Call .FormAddOrderField("AP23_CODOrg", cwAscending)
    Call .FormAddRelation("AP08_CODPers", txtPatologo(0))
  
  End With
   
  With objWinInfo
    Call .FormAddInfo(objTipo, cwFormDetail)
    Call .FormAddInfo(objPersonal, cwFormDetail)
    
    Call .FormAddInfo(objCarpetas, cwFormMultiLine)
    Call .GridAddColumn(objCarpetas, "Personal", "AP08_CODPers", cwNumeric, 4)
    Call .GridAddColumn(objCarpetas, "Carpeta", "AP06_CODCarp", cwString, 50)
   
    Call .FormAddInfo(objOrganos, cwFormMultiLine)
    Call .GridAddColumn(objOrganos, "Personal", "AP08_CODPers", cwNumeric, 4)
    Call .GridAddColumn(objOrganos, "Organo", "AP23_CODOrg", cwString, 50)
   
    Call .FormCreateInfo(objTipo)
'    .CtrlGetInfo(txtTipo(0)).blnValidate = False
    .CtrlGetInfo(txtPatologo(0)).blnValidate = False
        
    .CtrlGetInfo(cboSSPersonal).strSQL = "SELECT AP08_CODPers, AP08_Firma FROM " _
        & objEnv.GetValue("Main") & "AP0800 WHERE AP07_CODTiPers=1 ORDER BY AP08_CODPers"
    .CtrlGetInfo(grdssCarpeta(0).Columns(3)).strSQL = "SELECT AP06_CODCarp, AP06_Desig FROM AP0600 ORDER BY AP06_CODCarp"
'    .CtrlGetInfo(grdssOrgano(0).Columns(3)).strSQL = "SELECT AP23_CODOrg, AP23_Desig FROM AP2300 ORDER BY AP06_CODOrg"
  
    Call .WinRegister
    Call .WinStabilize
  End With
  
  Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If strFormName = "Tipo" Then
    With objWinInfo
      If txtTipo(0).Text > 2 Then
        .CtrlGetInfo(txtTipo(1)).blnReadOnly = False
        .CtrlGetInfo(txtTipo(2)).blnReadOnly = False
      Else
        .CtrlGetInfo(txtTipo(1)).blnReadOnly = True
        .CtrlGetInfo(txtTipo(2)).blnReadOnly = True
      End If
    End With
  End If
End Sub

Private Sub SSTab1_DblClick()

End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    Select Case strFormName
        Case objTipo.strName
           If objWinInfo.intWinStatus = cwModeSingleAddRest Then
                txtTipo(0).Text = fNextClave("AP07_CODTiPers")
                objTipo.rdoCursor("AP07_CODTiPers") = txtTipo(0).Text
            End If
        Case objPersonal.strName
           If objWinInfo.intWinStatus = cwModeSingleAddRest Then
                txtPatologo(0).Text = fNextClave("AP08_CODPers")
                objPersonal.rdoCursor("AP08_CODPers") = txtPatologo(0).Text
            End If
            If cboSSPersonal.Text = "" Then
              
            End If
    End Select

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTab1_DblClick(Index As Integer)

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'  If objWinInfo.objWinActiveForm.strName = "Personal" Then
'    MsgBox "FORM ACTIVO: Personal"
'  '******* SOLUCIONADO ********
'
'  End If
'  If btnButton.Index = 2 Then txtPatologo(8) = txtTipo(0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdssTipo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdssTipo_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssTipo_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssTipo_Change()
  Call objWinInfo.CtrlDataChange
End Sub

'=======================================================
Private Sub grdssPersonal_GotFocus()
  Call objWinInfo.CtrlGotFocus
  Call HabilitarCampos
End Sub

Private Sub grdssPersonal_DblClick()
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdssPersonal_RowColChange(ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdssPersonal_Change()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTipo_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTipo, False, True)
End Sub

'================================================================
Private Sub tabPersonal_MouseDown(Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabPersonal, False, True)
  Call HabilitarCampos
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraTipo_Click()
  Call objWinInfo.FormChangeActive(fraTipo, False, True)
End Sub

'==================================================================
Private Sub fraPersonal_Click()
  Call objWinInfo.FormChangeActive(fraPersonal, False, True)
  Call HabilitarCampos
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
 Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkTipo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkTipo_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkTipo_Click()
  Call objWinInfo.CtrlDataChange
End Sub

'=================================================================
Private Sub chkPatologo_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkPatologo_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkPatologo_Click()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cbossPersonal_GotFocus()
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cbossPersonal_LostFocus()
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cbossPersonal_CloseUp()
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtTipo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtTipo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtTipo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

'==================================================================
Private Sub txtPatologo_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  Call HabilitarCampos
End Sub

Private Sub txtPatologo_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtPatologo_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

