VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Begin VB.Form frmA_Medicamento 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Medicamentos "
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "mb00145.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Fotos de Medicamentos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7575
      Index           =   0
      Left            =   0
      TabIndex        =   2
      Top             =   450
      Width           =   11880
      Begin ComctlLib.Toolbar tlbToolbar2 
         Height          =   420
         Left            =   0
         TabIndex        =   24
         Top             =   0
         Visible         =   0   'False
         Width           =   11910
         _ExtentX        =   21008
         _ExtentY        =   741
         AllowCustomize  =   0   'False
         Appearance      =   1
         _Version        =   327682
         BorderStyle     =   1
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   7200
         Index           =   0
         Left            =   120
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   300
         Width           =   11730
         _ExtentX        =   20690
         _ExtentY        =   12700
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "mb00145.frx":030A
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "Image1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "Label1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "Label1(4)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "Label1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(1)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "Label1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "Label1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "cboSSResultAuto(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "cboSSResultAuto(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "CmDialog1"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "Command1(0)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtresultauto(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtresultauto(12)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtresultauto(11)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtresultauto(10)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtresultauto(0)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtresultauto(1)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtresultauto(2)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).ControlCount=   23
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "mb00145.frx":0326
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Fotos"
         TabPicture(2)   =   "mb00145.frx":0342
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "SSPanel1"
         Tab(2).ControlCount=   1
         Begin VB.TextBox txtresultauto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01NUMVVENT"
            Height          =   285
            HelpContextID   =   30101
            Index           =   2
            Left            =   1620
            MaxLength       =   12
            TabIndex        =   27
            Tag             =   "Vvent | Vvent del Medicamento"
            Top             =   4680
            Width           =   1245
         End
         Begin VB.TextBox txtresultauto 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01DESMED"
            Height          =   1005
            Index           =   1
            Left            =   1620
            MaxLength       =   50
            MultiLine       =   -1  'True
            TabIndex        =   13
            Tag             =   "Descripci�n|Descripci�n del Medicamento"
            Top             =   1440
            Width           =   3795
         End
         Begin VB.TextBox txtresultauto 
            BackColor       =   &H0000FFFF&
            DataField       =   "FR01CODMED"
            Height          =   315
            Index           =   0
            Left            =   1620
            MaxLength       =   3
            TabIndex        =   12
            Tag             =   "C�digo|C�digo de Resultado Autom�tico"
            Top             =   540
            Width           =   1035
         End
         Begin VB.TextBox txtresultauto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01REFMED"
            Height          =   285
            HelpContextID   =   30101
            Index           =   10
            Left            =   1620
            MaxLength       =   15
            TabIndex        =   11
            Tag             =   "Referencia|Referencia del Medicamento"
            Top             =   1080
            Width           =   2175
         End
         Begin VB.TextBox txtresultauto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01UBICACION"
            Height          =   285
            HelpContextID   =   30101
            Index           =   11
            Left            =   1620
            MaxLength       =   8
            TabIndex        =   10
            Tag             =   "Ubicaci�n|Ubicaci�n del Medicamento"
            Top             =   3960
            Width           =   1245
         End
         Begin VB.TextBox txtresultauto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01NUMENVASE"
            Height          =   285
            HelpContextID   =   30101
            Index           =   12
            Left            =   1620
            MaxLength       =   5
            TabIndex        =   9
            Tag             =   "Envase | Envase del Medicamento"
            Top             =   4320
            Width           =   1245
         End
         Begin VB.TextBox txtresultauto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR01NOMBREFOTO"
            Height          =   345
            HelpContextID   =   30101
            Index           =   13
            Left            =   5520
            MaxLength       =   40
            TabIndex        =   6
            Tag             =   "Archivo Foto | Foto del Medicamento"
            Top             =   6660
            Width           =   5265
         End
         Begin VB.CommandButton Command1 
            Caption         =   "..."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   10860
            TabIndex        =   5
            Top             =   6660
            Width           =   345
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   7005
            Index           =   0
            Left            =   -75210
            TabIndex        =   1
            TabStop         =   0   'False
            Top             =   90
            Width           =   11535
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   20346
            _ExtentY        =   12356
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSComDlg.CommonDialog CmDialog1 
            Left            =   4560
            Top             =   4500
            _ExtentX        =   847
            _ExtentY        =   847
            _Version        =   327681
            Filter          =   "Texto (*.txt)|*.txt|Im�genes (*.bmp;*.ico)|*.bmp;*.ico"
         End
         Begin Threed.SSPanel SSPanel1 
            Height          =   7035
            Left            =   -74940
            TabIndex        =   7
            Top             =   60
            Visible         =   0   'False
            Width           =   11295
            _Version        =   65536
            _ExtentX        =   19923
            _ExtentY        =   12409
            _StockProps     =   15
            BackColor       =   12632256
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BevelOuter      =   0
            Begin VB.TextBox Text1 
               Appearance      =   0  'Flat
               BackColor       =   &H00C0C0C0&
               BorderStyle     =   0  'None
               Height          =   300
               Index           =   0
               Left            =   300
               TabIndex        =   8
               Top             =   3840
               Visible         =   0   'False
               Width           =   3315
            End
            Begin VB.Image FotoGrafia 
               BorderStyle     =   1  'Fixed Single
               Height          =   3045
               Index           =   0
               Left            =   240
               Stretch         =   -1  'True
               Top             =   240
               Visible         =   0   'False
               Width           =   2715
            End
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSResultAuto 
            DataField       =   "FR03CODPROVEEDOR"
            Height          =   315
            Index           =   1
            Left            =   1620
            TabIndex        =   14
            Tag             =   "Proveedor|Proveedor"
            Top             =   3600
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSResultAuto 
            DataField       =   "FR02CODGRUPO"
            Height          =   315
            Index           =   0
            Left            =   1620
            TabIndex        =   15
            Tag             =   "Grupo|Grupo"
            Top             =   3240
            Width           =   2490
            DataFieldList   =   "Column 0"
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnHeaders   =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   3200
            Columns(0).Visible=   0   'False
            Columns(0).Caption=   "C�digo"
            Columns(0).Name =   "C�digo"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   3200
            Columns(1).Caption=   "Nombre"
            Columns(1).Name =   "Nombre"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   4392
            _ExtentY        =   556
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DataFieldToDisplay=   "Column 1"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR01FECALTA"
            Height          =   330
            Index           =   0
            Left            =   1620
            TabIndex        =   26
            Tag             =   "Inicio Vigencia|Fecha de inicio de vigencia de la actuaci�n"
            Top             =   2880
            Width           =   1560
            _Version        =   65537
            _ExtentX        =   2752
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label Label1 
            Caption         =   "VVent"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   240
            TabIndex        =   28
            Top             =   4740
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Fecha de Alta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   25
            Top             =   2940
            Width           =   1455
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Archivo Foto:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   4020
            TabIndex        =   23
            Top             =   6720
            Width           =   1440
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   300
            TabIndex        =   22
            Top             =   600
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   300
            TabIndex        =   21
            Top             =   1500
            Width           =   1080
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Proveedor:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   20
            Top             =   3660
            Width           =   1140
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Grupo:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   19
            Top             =   3300
            Width           =   1140
         End
         Begin VB.Label Label1 
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   300
            TabIndex        =   18
            Top             =   1140
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Ubicaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   17
            Top             =   4020
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Envase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   240
            TabIndex        =   16
            Top             =   4380
            Width           =   1455
         End
         Begin VB.Image Image1 
            BorderStyle     =   1  'Fixed Single
            Height          =   6405
            Left            =   5520
            Stretch         =   -1  'True
            Top             =   120
            Width           =   5775
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   3
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Image Image3 
      BorderStyle     =   1  'Fixed Single
      Height          =   8325
      Index           =   0
      Left            =   2640
      Stretch         =   -1  'True
      Top             =   0
      Visible         =   0   'False
      Width           =   6915
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
      Begin VB.Menu s 
         Caption         =   "-"
      End
      Begin VB.Menu mnuConfiguracion 
         Caption         =   "&Configuraci�n de Pagina"
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmA_Medicamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objDetailInfo As New clsCWForm
Dim DesigPrueba As String
Dim Nreg As Integer, NumPag As Integer
Dim Pagina() As Variant
Dim NVer As Integer
Dim NHor As Integer
Dim Anch As Double
Dim Alt As Double
Dim Sepve As Double
Dim Sepho As Double
Dim NFotosPagina As Byte
Dim PaginActual As Integer
Dim Primera As Integer
Dim Posic As Integer 'posicion del cursor :inicio 0 o final de pagina 1
Dim Navegacion As Boolean 'indica si se esta navegando
'desde la 1� pag (true) o desde la ultima (false)
Dim NFot As Integer
Dim EofBof As Boolean
Dim Ref As Variant
Dim Aviso As Boolean, Aviso2 As Boolean
Dim INI As Boolean, FIN As Boolean
Dim UnidadFotos$

Sub LeeUnidadFotos()

Dim unit As String * 20
Dim res As Long
Dim a%

res = GetPrivateProfileString("config", "unidad", "6", unit, 20, "C:\DIMFOTOS.INI")
UnidadFotos = fQuitaChr(unit)
If unit = "" Then
    a = MsgBox("Falta el Archivo 'c:\dimfotos.ini'", vbOKOnly, "No Se Ha Encontrado la Unidad de Fotos")
'    Call objWinInfo.WinDeRegister
'    Call objWinInfo.WinRemoveInfo
    End
End If
End Sub
Sub QuitaMarcosTextos()
Dim n%

For n = 1 To NFotosPagina
    Unload FotoGrafia(n)
    Unload Text1(n)
Next n
    
End Sub
Private Function fQuitaChr(texto As String)
    fQuitaChr = Left$(texto, InStr(1, texto, Chr$(0)) - 1)
End Function
Sub LeeDimensiones()
    Dim nv As String * 5
    Dim nh As String * 5
    Dim sepver As String * 5
    Dim sephor As String * 5
    Dim res As Long
    
    res = GetPrivateProfileString("config", "anchura", "6", nh, 5, "C:\FARMACIA\DIMFOTOS.INI")
    If res = 1 Then Call objPipe.PipeSet("nfotosh", fQuitaChr(nh)) Else Call objPipe.PipeSet("nfotosh", "4")
    res = GetPrivateProfileString("config", "altura", "6", nv, 5, "C:\FARMACIA\DIMFOTOS.INI")
    If res = 1 Then Call objPipe.PipeSet("nfotosv", fQuitaChr(nv)) Else Call objPipe.PipeSet("nfotosv", "2")
'    res = GetPrivateProfileString("config", "sephoriz", "1", sephor, 5, "C:\FARMACIA\DIMFOTOS.INI")
'    If res = 1 Then Call objPipe.PipeSet("seph", fQuitaChr(sephor)) 'Else Call objPipe.PipeSet("seph", "1")
'    res = GetPrivateProfileString("config", "sepverti", "1", sepver, 5, "C:\FARMACIA\DIMFOTOS.INI")
'    If res = 1 Then Call objPipe.PipeSet("sepv", fQuitaChr(sepver)) 'Else Call objPipe.PipeSet("sepv", "1")
    
    LeerDimPagina
    
End Sub
Sub CargaMarcosTextos()

Dim n%

For n = 1 To NFotosPagina
    Load FotoGrafia(n)
    Load Text1(n)
Next n
    
End Sub
Sub CalculoNumPaginas()

Dim i As Integer, j%

Nreg = objDetailInfo.rdoCursor.RowCount
If Nreg / NFotosPagina > Int(Nreg / NFotosPagina) Then
    NumPag = Int(Nreg / NFotosPagina) + 1
Else
    NumPag = Int(Nreg / NFotosPagina)
End If

ReDim Preserve Pagina(1 To NumPag)

Call objWinInfo.WinProcess(cwProcessToolBar, 21, 0)
Pagina(1) = objDetailInfo.rdoCursor.Bookmark

End Sub
Sub MoverRegistro(HaciaDelante As Boolean)

Dim code As String
'    stbStatusBar1.Panels(2).Text = "Cursor en: " & objDetailInfo.rdoCursor("fr01_CODIGO") & " nfot: " & nfot   '& " Aviso: " & aviso & " Aviso2: " & aviso2
        

    code = objDetailInfo.rdoCursor("fr01codmed")
    If HaciaDelante Eqv objDetailInfo.blnDirection Then
        objDetailInfo.rdoCursor.MoveNext
    Else
        objDetailInfo.rdoCursor.MovePrevious
    End If
    If objDetailInfo.rdoCursor.EOF Or objDetailInfo.rdoCursor.BOF Then
        If HaciaDelante Then
            If objDetailInfo.blnDirection Then objDetailInfo.rdoCursor.MovePrevious Else objDetailInfo.rdoCursor.MoveNext
            objWinInfo.DataMoveNext
        Else
            If objDetailInfo.blnDirection Then objDetailInfo.rdoCursor.MoveNext Else objDetailInfo.rdoCursor.MovePrevious
            objWinInfo.DataMovePrevious
        End If
        EofBof = True
        Ref = objDetailInfo.rdoCursor.Bookmark
    End If
    'estamos en el 1er registro o en el ultimo
    If objDetailInfo.rdoCursor("fr01codmed") = code Then
        If HaciaDelante Then FIN = True Else INI = True
    End If

End Sub

Sub CreatePage()

    Dim i%, j%, q%, X%, txt$
    
    Posic = 1
    
    
    q = 1
    
    Ocultar
    For i = 1 To NVer
        For j = 1 To NHor
            If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")) Then
                On Error Resume Next
                FotoGrafia(q).Picture = LoadPicture(UnidadFotos & "\" & Trim$(objDetailInfo.rdoCursor("fr01nombrefoto")))
                Err = 0
            End If
            txt = objDetailInfo.rdoCursor.rdoColumns("fr01codmed") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01refmed")
            q = q + 1
            MoverRegistro True
            Text1(q - 1) = txt
            
            If Aviso2 Then
                NFot = q + 1
                Mostrar True, q + 1
                Screen.MousePointer = 0
                Exit Sub
            End If
            
            If objDetailInfo.rdoCursor.rdoColumns("fr01codmed") = code2 Then
                Aviso2 = True
            End If
        Next j
    Next i
    
    Mostrar True, NFotosPagina
    SSPanel1.Visible = True
    NFot = NFotosPagina
    Screen.MousePointer = 0

End Sub
Sub LeerDimPagina()

    Dim res%, i%, j%
    Dim alto As String * 5
    Dim ancho As String * 5
    Dim sepver As String * 5
    Dim sephor As String * 5
    
'    anch = 4
'    alt = 5
'    sepve = 0.5
'    sepho = 0.5
'
'    nhor = Int((SSPanel1.Width - 567 * sepho) / (anch * 567 + sepho * 567))
'    nver = Int((SSPanel1.Height - 465 - 567 * sepve) / (alt * 567 + sepve * 567))
    
    NHor = objPipe.PipeGet("nfotosh")
    NVer = objPipe.PipeGet("nfotosv")
    Sepho = 0.5
    Sepve = 0.2
'    sepho = objPipe.PipeGet("seph")
'    sepve = objPipe.PipeGet("sepv")
    
'    If ((SSPanel1.Width / 567) - anch * nhor) / (nhor + 1) > 0.5 Then
'        sepho = ((SSPanel1.Width / 567) - anch * nhor) / (nhor + 1)
'    Else
'        sepho = 0.5
'        anch = ((SSPanel1.Width / 567) - sepho * (nhor + 1)) / nhor
'    End If
'
'    If ((SSPanel1.Height / 567) - alt * nver) / (nver + 1) > 0.5 Then
'        sepve = ((SSPanel1.Height / 567) - alt * nver) / (nver + 1)
'    Else
'        sepve = 0.5
'        anch = ((SSPanel1.Height / 567) - sepve * (nver + 1)) / nver
'    End If
    
    NFotosPagina = NHor * NVer
    
End Sub
Sub Mostrar(inicio As Boolean, NumFotos As Byte)

Dim q As Byte
Dim inicial%, final%, step%

inicial = NFotosPagina * Abs(Not inicio) + 1 * Abs(inicio)

If inicial > NumFotos Then
    final = inicial
    inicial = NumFotos
Else
    final = NumFotos
End If

For q = inicial To final
    FotoGrafia(q).Visible = True
    Text1(q).Visible = True
Next q
SSPanel1.Visible = True
    
End Sub
Sub Ocultar()
Dim q As Byte
    SSPanel1.Visible = False
    For q = 1 To NFotosPagina
        FotoGrafia(q).Visible = False
        Text1(q).Visible = False
        FotoGrafia(q).Picture = LoadPicture()
    Next q
End Sub
Sub PageFirst()

    Dim i%, j%, q%, X%, txt$
    
    Navegacion = True
    q = 0
    FIN = False: INI = False: EofBof = False
    'Al cambiarnos de pagina borramos lo que habia
    SSPanel1.Visible = False
    Ocultar
    objWinInfo.DataMoveFirst
    
    Posic = 1
    q = 1
    
    For i = 1 To NVer
        For j = 1 To NHor
             If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")) Then
                 On Error Resume Next
                 FotoGrafia(q).Picture = LoadPicture(UnidadFotos & "\" & Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")))
                 Err = 0
             End If
             txt = objDetailInfo.rdoCursor.rdoColumns("fr01codmed") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01refmed")
             q = q + 1
             If Not FIN Then MoverRegistro True
             Text1(q - 1).Text = txt
             If FIN Then
                NFot = q - 1
                Mostrar True, q - 1
                SSPanel1.Visible = True
                Screen.MousePointer = 0
                Exit Sub
            End If
        Next j
    Next i
    
    Mostrar True, NFotosPagina
    NFot = NFotosPagina + 1
    Screen.MousePointer = 0

End Sub
Sub PageLast()
    
    Dim i%, j%, q%, X%, txt
    
    FIN = False: INI = False: EofBof = False
    Navegacion = False
    q = 0
    'Al cambiarnos de pagina borramos lo que habia
    Ocultar
    Posic = 0
    objWinInfo.DataMoveLast
    'code2 = objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")
    q = NFotosPagina
    SSPanel1.Visible = False
    For i = NVer To 1 Step -1
        For j = NHor To 1 Step -1
            If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")) Then
                On Error Resume Next
                FotoGrafia(q).Picture = LoadPicture(UnidadFotos & "\" & Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")))
                Err = 0
            End If
            Text1(q).Text = objDetailInfo.rdoCursor.rdoColumns("fr01codmed") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01refmed")
            q = q - 1
            If Not INI Then MoverRegistro False
            Text1(q).Text = txt
            If INI Then
                NFot = NFotosPagina - q
                Mostrar False, q + 1
                Screen.MousePointer = 0
                Exit Sub
            End If
        Next j
    Next i
    Mostrar True, NFotosPagina
    NFot = NFotosPagina + 1
    Screen.MousePointer = 0

End Sub
Sub PageNext()

    Dim i%, j%, q%, X%, txt$
    
    FIN = False: INI = False
    
    If EofBof Then objDetailInfo.rdoCursor.Bookmark = Ref
    EofBof = False
    If Posic = 0 Then
        For i = 1 To NFot: MoverRegistro True: Next i
        Posic = 1
    End If
    If EofBof Then NFot = NFotosPagina
    If FIN Then Beep: Exit Sub
    'Al cambiarnos de pagina borramos lo que habia
    Ocultar
    q = 1
    With objDetailInfo.rdoCursor
        For i = 1 To NVer
            For j = 1 To NHor
                If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")) Then
                     On Error Resume Next
                     FotoGrafia(q).Picture = LoadPicture(UnidadFotos & "\" & Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")))
                     Err = 0
                End If
                txt = objDetailInfo.rdoCursor.rdoColumns("fr01codmed") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01refmed")
                q = q + 1
                If Not FIN Then MoverRegistro True
                Text1(q - 1) = txt
                If FIN Then
                    NFot = q - 1
                    Mostrar True, q - 1
                    SSPanel1.Visible = True
                    Screen.MousePointer = 0
                    Exit Sub
                End If
                'If (objDetailInfo.rdoCursor.rdoColumns("fr01_codigo")) = code2 Then aviso2 = True
            Next j
        Next i
        If EofBof Then Ref = objDetailInfo.rdoCursor.Bookmark
        Mostrar True, NFotosPagina
        NFot = NFotosPagina + 1
        Screen.MousePointer = 0
    End With
End Sub
Sub PagePrevious()
    Dim i%, j%, q%, X%, txt$
    
    FIN = False: INI = False
    
    If EofBof Then objDetailInfo.rdoCursor.Bookmark = Ref
    EofBof = False
    If Posic = 1 Then
        For i = 1 To NFot: MoverRegistro False: Next i
        Posic = 0
    End If
    If EofBof Then NFot = NFotosPagina
    If INI Then Beep: Exit Sub
     
    'Al cambiarnos de pagina borramos lo que habia
    Ocultar
    q = NFotosPagina
    For i = NVer To 1 Step -1
        For j = NHor To 1 Step -1
            If Not IsNull(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")) Then
                On Error Resume Next
                FotoGrafia(q).Picture = LoadPicture(UnidadFotos & "\" & Trim$(objDetailInfo.rdoCursor.rdoColumns("fr01nombrefoto")))
                Err = 0
            End If
            txt = objDetailInfo.rdoCursor.rdoColumns("fr01codmed") & " - " & objDetailInfo.rdoCursor.rdoColumns("fr01refmed")
            q = q - 1
            If Not INI Then MoverRegistro False
            Text1(q + 1).Text = txt
            If INI Then
                NFot = NFotosPagina - q
                Mostrar False, q + 1
                Screen.MousePointer = 0
                Exit Sub
            End If
        Next j
    Next i
    
    If EofBof Then Ref = objDetailInfo.rdoCursor.Bookmark
    Mostrar True, NFotosPagina
    NFot = NFotosPagina + 1
    Screen.MousePointer = 0

End Sub
Sub PosicionaFotos()
Dim i As Byte, j As Byte, n As Integer
'segun el numero de fotos por pagina creamos los images y los ponemos
'para la 3� pesta�a
    'LeerDimPagina
    
    CargaMarcosTextos
    
    n = 0
    For i = 1 To NVer
        For j = 1 To NHor
            n = n + 1
            Sepho = 0.5
            Sepve = 0.5
            FotoGrafia(n).Width = (SSPanel1.Width - (NHor + 1) * Sepho * 567) / NHor
            FotoGrafia(n).Height = (SSPanel1.Height - (NVer + 1) * Sepve * 567) / NVer - 325 'se resta 325 para que entre el texto
            FotoGrafia(n).Left = j * Sepho * 567 + FotoGrafia(n).Width * (j - 1)
            FotoGrafia(n).Top = i * Sepve * 567 + (FotoGrafia(n).Height + 325) * (i - 1)
            FotoGrafia(n).Visible = True
            Text1(n).Left = FotoGrafia(n).Left
            Text1(n).Width = FotoGrafia(n).Width
            Text1(n).Top = FotoGrafia(n).Top + FotoGrafia(n).Height + 25
            Text1(n).Height = 300
            Text1(n).BackColor = &HC0C0C0
            Text1(n).Visible = True
        Next j
    Next i
End Sub

Public Sub pComboTiposMuestraCargar(objWinInfo As clsCWWin, cbossResult As SSDBCombo, cPrueba$)
'Para actualizar la lista del combo de los resultados
        Dim SQL As String
    
'    SQL = "SELECT cTipoMuestra, Designacion FROM TiposMuestras"
'    SQL = SQL & " WHERE cTipoMuestra IN "
'    SQL = SQL & "       (SELECT cTipoMuestra FROM pruebasExtracciones pE, pruebasCarpetas pC"
'    SQL = SQL & "        WHERE pE.cPrueba = pC.cPrueba"
'    SQL = SQL & "        AND pC.cCarpeta IN"
'    SQL = SQL & "           (SELECT cCarpeta FROM carpetas WHERE cDptoSecc = " & departamento & ")"
'    If cPrueba <> "" Then
'        SQL = SQL & "        AND pC.cPrueba = " & cPrueba
'    End If
'    SQL = SQL & "        )"
'    SQL = SQL & " ORDER BY Designacion"
'    cbossResult.RemoveAll
'    objWinInfo.CtrlGetInfo(cbossResult).strSQL = SQL
'    Call objGen.LoadCombo(objWinInfo, objWinInfo.CtrlGetInfo(cbossResult))
        
End Sub


Private Sub cboSSResultAuto_Click(Index As Integer)
    'Se llena el combo de los resultados seg�n la prueba seleccionada
    Call objWinInfo.CtrlDataChange
    If Index = 0 Then
        Call pComboTiposMuestraCargar(objWinInfo, cboSSResultAuto(1), cboSSResultAuto(0).Columns(0).Text)
        'cboSSResultAuto(1).Text = ""
        If cboSSResultAuto(1).Text <> "" Then
            cboSSResultAuto(1).Text = cboSSResultAuto(1).Columns(1).Text
        End If
    End If
End Sub

Private Sub cboSSResultAuto_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
    'Se llena el combo de los resultados seg�n la prueba seleccionada
    If Index = 0 Then
        If cboSSResultAuto(0).Text = "" Then
            Call pComboTiposMuestraCargar(objWinInfo, cboSSResultAuto(1), "")
        End If
    End If
    
End Sub

Private Sub cboSSResultAuto_GotFocus(Index As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSResultAuto_LostFocus(Index As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub Command1_Click(Index As Integer)

Dim q$, extension$
Dim X, fd

    Screen.MousePointer = 11
    CmDialog1.DialogTitle = "Insertar el Archivo Foto"
    CmDialog1.DefaultExt = "*.bmp;*.jpg;*.gif"
    CmDialog1.InitDir = UnidadFotos
    CmDialog1.Filter = "Im�genes JPEG (*.jpg)|*.jpg|Im�genes GIF (*.gif)|*.gif|BitMaps/Iconos (*.bmp;*.ico)|*.bmp;*.ico"
    CmDialog1.Action = 1
    q = CmDialog1.filename
    If q <> "" Then
        txtresultauto(13).SetFocus
        If InStr(1, UCase(q), UCase(UnidadFotos)) > 0 Then
            txtresultauto(13).Text = Right$(q, Len(q) - Len(UnidadFotos) - 1)
        Else
            'aviso de que la foto no cuelga del directorio compartido
        End If
        On Error Resume Next
        fd = FileDateTime(q)
        If Err <> 0 Then  'no existe el archivo bmp
            Err = 0
        Else
            Image1.Picture = LoadPicture(q)
            'Exit Sub
        End If
    End If
    Screen.MousePointer = 0
End Sub

Private Sub Form_Load()

Dim strKey As String
Dim SQL As String
  
    Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                  Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
    With objDetailInfo
        .strName = "resultAuto"
        Set .objFormContainer = fraFrame1(0)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = tabTab1(0)
        Set .grdGrid = grdDBGrid1(0)
        .strTable = "FR0100"
        '.strWhere = "cDptoSecc = " & departamento
        Call .FormAddOrderField("FR01codmed", cwAscending)
        .blnHasMaint = False
        .blnAskPrimary = True
        
        strKey = .strDataBase & .strTable
        Call .FormCreateFilterWhere(strKey, "FR0100")
                
        Call .FormAddFilterWhere(strKey, "FR01codmed", "C�digo", cwString)
        Call .FormAddFilterWhere(strKey, "FR01refmed", "Referencia", cwString)
        Call .FormAddFilterWhere(strKey, "FR01desmed", "Descripci�n", cwString)
        Call .FormAddFilterWhere(strKey, "FR01NOMBREFOTO", "Archivo Foto", cwString)
        Call .FormAddFilterWhere(strKey, "FR01FECALTA", "Fecha de Alta", cwDate)
        Call .FormAddFilterWhere(strKey, "FR02codGRUPO", "Grupo", cwString)
        Call .FormAddFilterWhere(strKey, "FR03codPROVEEDOR", "Proveedor", cwString)
        Call .FormAddFilterWhere(strKey, "FR01UBICACION", "Ubicaci�n", cwString)
        Call .FormAddFilterWhere(strKey, "FR01numENVASE", "Envase", cwNumeric)
        
        Call .FormAddFilterOrder(strKey, "FR01CODmed", "Codigo de Medicamento") 'nombre tabla para report
        Call .FormAddFilterOrder(strKey, "FR01REFmed", "Referencia") 'nombre tabla para report
        Call .FormAddFilterOrder(strKey, "FR01FECALTA", "Fecha de Alta")
        Call .FormAddFilterOrder(strKey, "FR02codGRUPO", "Grupo")
        Call .FormAddFilterOrder(strKey, "FR03codPROVEEDOR", "Proveedor")
        Call .FormAddFilterOrder(strKey, "FR01UBICACION", "Ubicaci�n")
        Call .FormAddFilterOrder(strKey, "FR01numENVASE", "Envase")
        
        'Call .objPrinter.Add("MB00145", "Resultados Autom�ticos")
    
    End With
       
    With objWinInfo
        Call .FormAddInfo(objDetailInfo, cwFormDetail)
        Call .FormCreateInfo(objDetailInfo)
           
        .CtrlGetInfo(txtresultauto(0)).blnValidate = False
        .CtrlGetInfo(cboSSResultAuto(0)).blnInFind = True
        .CtrlGetInfo(cboSSResultAuto(1)).blnInFind = True
        
        .CtrlGetInfo(txtresultauto(0)).blnInFind = True
        .CtrlGetInfo(txtresultauto(1)).blnInFind = True
        .CtrlGetInfo(txtresultauto(10)).blnInFind = True
        .CtrlGetInfo(txtresultauto(11)).blnInFind = True
        .CtrlGetInfo(txtresultauto(12)).blnInFind = True
        .CtrlGetInfo(txtresultauto(13)).blnInFind = True
           
        SQL = "SELECT FR02CodGRUPO,FR02desGRUPO FROM FR0200"
        SQL = SQL & "    ORDER BY FR02desGRUPO"
        .CtrlGetInfo(cboSSResultAuto(0)).strsql = SQL
    
        SQL = "SELECT FR03codPROVEEDOR,FR03desPROVEEDOR FROM FR0300"
        SQL = SQL & "    ORDER BY FR03desPROVEEDOR"
        .CtrlGetInfo(cboSSResultAuto(1)).strsql = SQL
            
        Call .WinRegister
        Call .WinStabilize
    End With

    LeeUnidadFotos
    LeeDimensiones
    PosicionaFotos
    NFot = NFotosPagina - 2
    Screen.MousePointer = vbDefault 'Call objApp.SplashOff
    'tlbToolbar2.ImageList = tlbToolbar1.ImageList
    'tlbToolbar2.Buttons.Add 1, , , , tlbToolbar1.Buttons(18).Image
    'tlbToolbar2.Buttons.Add 2, , , , tlbToolbar1.Buttons(19).Image
    'tlbToolbar2.Buttons.Add 3, , , , tlbToolbar1.Buttons(21).Image
    'tlbToolbar2.Buttons.Add 4, , , , tlbToolbar1.Buttons(22).Image
    'tlbToolbar2.Buttons.Add 5, , , , tlbToolbar1.Buttons(23).Image
    'tlbToolbar2.Buttons.Add 6, , , , tlbToolbar1.Buttons(24).Image
    'tlbToolbar2.Buttons.Add 7, , , , tlbToolbar1.Buttons(30).Image
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub

Private Sub FotoGrafia_Click(Index As Integer)
    Image3(0).Picture = FotoGrafia(Index).Picture
    fraFrame1(0).Visible = False
    frmA_Medicamento.Caption = Text1(Index).Text
    stbStatusBar1.Visible = False
    tlbToolbar1.Visible = False
    Image3(0).Visible = True
End Sub

Private Sub Image3_Click(Index As Integer)
    Image3(Index).Visible = False
    fraFrame1(0).Visible = True
    frmA_Medicamento.Caption = "Medicamentos"
    stbStatusBar1.Visible = True
    tlbToolbar1.Visible = True
End Sub

Private Sub mnuConfiguracion_Click()
    frmConfiguracion.Show vbModal
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
'    Dim codPrueba As String
'
'    On Error Resume Next
'    If cboSSResultAuto(0).Text = "" Then
'        codPrueba = ""
'    Else
'        codPrueba = cboSSResultAuto(0).Columns(0).Text
'    End If
'    Call pComboTiposMuestraCargar(objWinInfo, cboSSResultAuto(1), codPrueba)
'
Image1.Picture = LoadPicture()
If tabTab1.Item(0).Tab = 0 Then
        On Error Resume Next
        Image1.Picture = LoadPicture(UnidadFotos & "\" & objDetailInfo.rdoCursor("fr01nombrefoto"))
    End If
End Sub

Private Sub objWinInfo_cwPreWrite(ByVal strFormName As String, ByVal blnError As Boolean)
    
'    If strFormName = objDetailInfo.strName Then
'        If objWinInfo.intWinStatus = cwModeSingleAddRest Then
'            txtResultAuto(0).Text = fNextClave("MB45_codResultAuto", "MB4500")
'            objDetailInfo.rdoCursor("MB45_codResultAuto") = txtResultAuto(0).Text
'            txtResultAuto(2).Text = departamento
'            objDetailInfo.rdoCursor("cDptoSecc") = txtResultAuto(2).Text
'        End If
'    End If
'
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean

  Select Case strFormName
    Case objDetailInfo.strName
        Call objWinInfo.FormPrinterDialog(True, "")
        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
        intReport = objPrinter.Selected
        If intReport > 0 Then
          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                     objWinInfo.DataGetOrder(blnHasFilter, True))
        End If
        Set objPrinter = Nothing

  End Select

End Sub

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim res
    ' analiza el bot�n seleccionado y ejecuta la acci�n asociada
    Image1.Picture = LoadPicture()
    If tabTab1.Item(0).Tab = 2 Then
        Select Case btnButton.Index
            Case 21
                If INI Then Beep Else PageFirst: DoEvents
            Case 22
                If INI Then Beep Else PagePrevious: DoEvents
            Case 23
                If FIN Then Beep Else PageNext: DoEvents
            Case 24
                If FIN Then Beep Else PageLast: DoEvents
            Case 18
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                PageFirst
            Case 19
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                PageFirst
        End Select
        HabilitarToolbar False
'    Else
'
'        If btnButton.Index = 19 Then 'Or btnButton.Index = 19 Or btnButton.Index = 1 Then
'            'calculonumpag
'            'Movepage (1)
'            PageFirst
'        End If
    End If
    
'    If btnButton.Index = 8 Then
'        If Not objDetailInfo.rdoCursor.EOF And Not objDetailInfo.rdoCursor.BOF Then
'            stbStatusBar1.Panels(1).Text = "Cursor en: " & objDetailInfo.rdoCursor("fr01_CODIGO") & " nfot: " & nfot '& " Aviso: " & aviso & " Aviso2: " & aviso2
'        Else
'            stbStatusBar1.Panels(1).Text = "EOF BOF"
'        End If
'        Exit Sub
'    End If
        
    If tabTab1.Item(0).Tab <> 2 Or btnButton.Index = 30 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    Else
        'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End If
    
'    If tabTab1.Item(0).Tab = 0 Then
'        On Error Resume Next
'        Image1.Picture = LoadPicture(objDetailInfo.rdoCursor("fr01_nombrefoto"))
'    End If
    
'    If Not objDetailInfo.rdoCursor.EOF And Not objDetailInfo.rdoCursor.BOF Then
'        stbStatusBar1.Panels(1).Text = "Cursor en: " & objDetailInfo.rdoCursor("fr01_CODIGO") & " nfot: " & nfot '& " Aviso: " & aviso & " Aviso2: " & aviso2
'    Else
'        stbStatusBar1.Panels(1).Text = "EOF BOF"
'    End If
    
    
    'stbStatusBar1.Panels(1).Text = objDetailInfo.rdoCursor("fr01_CODIGO")
    'DoEvents
End Sub

Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
    
    If tabTab1.Item(0).Tab <> 2 Then
        HabilitarToolbar True
        SSPanel1.Visible = False
        objWinInfo.FormChangeActive tabTab1(intIndex), False, True
        If EofBof Then objDetailInfo.rdoCursor.Bookmark = Ref: EofBof = False
        objWinInfo.DataRead
    Else
        PageNext
        SSPanel1.Visible = True
        HabilitarToolbar False
    End If
    
End Sub
Private Sub HabilitarToolbar(hbl As Boolean)
Dim i%

    For i = 2 To 4
        tlbToolbar1.Buttons(i).Enabled = hbl
    Next i
    
    tlbToolbar1.Buttons(11).Enabled = hbl
    tlbToolbar1.Buttons(26).Enabled = hbl
    
    For i = 6 To 16 Step 2
        tlbToolbar1.Buttons(i).Enabled = hbl
    Next i
    
End Sub
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub

Private Sub txtResultAuto_GotFocus(intIndex As Integer)
'If intIndex = 13 Then
txtresultauto(intIndex).SelStart = 0
txtresultauto(intIndex).SelLength = Len(txtresultauto(intIndex).Text)
'End If
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtResultAuto_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtResultAuto_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


