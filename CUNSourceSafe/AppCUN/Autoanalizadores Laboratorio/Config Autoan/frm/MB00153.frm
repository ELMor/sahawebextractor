VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Begin VB.Form frmP_MontajeMesa 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Montaje de la mesa"
   ClientHeight    =   8565
   ClientLeft      =   285
   ClientTop       =   660
   ClientWidth     =   11430
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8565
   ScaleWidth      =   11430
   ShowInTaskbar   =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   14
      Top             =   0
      WhatsThisHelpID =   10131
      Width           =   11430
      _ExtentX        =   20161
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraCultivos 
      Caption         =   "Cultivos"
      Height          =   4815
      Index           =   0
      Left            =   60
      TabIndex        =   16
      Top             =   1380
      WhatsThisHelpID =   10130
      Width           =   11235
      Begin SSDataWidgets_B.SSDBGrid grdssCultivos 
         Height          =   4395
         Index           =   0
         Left            =   180
         TabIndex        =   17
         TabStop         =   0   'False
         Tag             =   "Pendientes|Cultivos Pendientes de Colocar"
         Top             =   300
         Width           =   5415
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   6
         BeveColorScheme =   1
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns.Count   =   6
         Columns(0).Width=   1323
         Columns(0).Caption=   "N� Micro"
         Columns(0).Name =   "NRef"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "codTecAsist"
         Columns(1).Name =   "codTecAsist"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1270
         Columns(2).Caption=   "N� Placa"
         Columns(2).Name =   "codPlaca"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3016
         Columns(3).Caption=   "T�cnica/Cultivo"
         Columns(3).Name =   "desigTecnica"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3678
         Columns(4).Caption=   "Condici�n"
         Columns(4).Name =   "desigCondicion"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "Numero"
         Columns(5).Name =   "Numero"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   9551
         _ExtentY        =   7752
         _StockProps     =   79
         Caption         =   "Cultivos pendientes de colocar"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdssCultivos 
         Height          =   4395
         Index           =   1
         Left            =   5700
         TabIndex        =   18
         TabStop         =   0   'False
         Tag             =   "Colocados|Cultivos Colocados"
         Top             =   300
         Width           =   5415
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         Col.Count       =   6
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   3
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns.Count   =   6
         Columns(0).Width=   1323
         Columns(0).Caption=   "N� Micro"
         Columns(0).Name =   "NRef"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "codTecAsist"
         Columns(1).Name =   "codTecAsist"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         Columns(2).Width=   1270
         Columns(2).Caption=   "N� Placa"
         Columns(2).Name =   "codPlaca"
         Columns(2).DataField=   "Column 2"
         Columns(2).DataType=   8
         Columns(2).FieldLen=   256
         Columns(3).Width=   3016
         Columns(3).Caption=   "T�cnica/Cultivo"
         Columns(3).Name =   "desigTecnica"
         Columns(3).DataField=   "Column 3"
         Columns(3).DataType=   8
         Columns(3).FieldLen=   256
         Columns(4).Width=   3440
         Columns(4).Caption=   "Condici�n"
         Columns(4).Name =   "desigCondicion"
         Columns(4).DataField=   "Column 4"
         Columns(4).DataType=   8
         Columns(4).FieldLen=   256
         Columns(5).Width=   3200
         Columns(5).Visible=   0   'False
         Columns(5).Caption=   "Numero"
         Columns(5).Name =   "Numero"
         Columns(5).DataField=   "Column 5"
         Columns(5).DataType=   8
         Columns(5).FieldLen=   256
         UseDefaults     =   0   'False
         _ExtentX        =   9551
         _ExtentY        =   7752
         _StockProps     =   79
         Caption         =   "Cultivos colocados"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame fraCarpetas 
      Caption         =   "Carpetas"
      Height          =   915
      Index           =   0
      Left            =   60
      TabIndex        =   12
      Top             =   420
      WhatsThisHelpID =   10129
      Width           =   3615
      Begin SSDataWidgets_B.SSDBGrid grdssCarpetas 
         Height          =   675
         Index           =   0
         Left            =   120
         TabIndex        =   15
         TabStop         =   0   'False
         Tag             =   "Carpetas|Carpetas"
         Top             =   180
         Width           =   3420
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RecordSelectors =   0   'False
         ColumnHeaders   =   0   'False
         Col.Count       =   0
         AllowUpdate     =   0   'False
         RowSelectionStyle=   2
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowGroupSwapping=   0   'False
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   6032
         _ExtentY        =   1191
         _StockProps     =   79
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.CommandButton cmdMontaje 
      BackColor       =   &H000000FF&
      Caption         =   "Pruebas"
      Enabled         =   0   'False
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   11
      Top             =   7680
      WhatsThisHelpID =   10126
      Width           =   1215
   End
   Begin VB.CommandButton cmdMontaje 
      BackColor       =   &H000000FF&
      Caption         =   "Problema"
      Enabled         =   0   'False
      Height          =   375
      Index           =   1
      Left            =   1440
      TabIndex        =   9
      Top             =   7680
      Width           =   1215
   End
   Begin VB.CommandButton cmdMontaje 
      Caption         =   "&Limpiar"
      Height          =   375
      Index           =   2
      Left            =   10080
      TabIndex        =   7
      Top             =   7740
      Width           =   1215
   End
   Begin VB.Frame fraDatos 
      Caption         =   "Datos"
      Height          =   915
      Left            =   3720
      TabIndex        =   4
      Top             =   420
      WhatsThisHelpID =   10125
      Width           =   7575
      Begin VB.TextBox txtCodigoBarras 
         Height          =   285
         Left            =   780
         TabIndex        =   0
         Top             =   180
         Width           =   2415
      End
      Begin VB.CommandButton cmdColocar 
         BackColor       =   &H000000FF&
         Caption         =   "&Colocar"
         Enabled         =   0   'False
         Height          =   375
         Index           =   0
         Left            =   6180
         TabIndex        =   10
         Top             =   300
         Width           =   1215
      End
      Begin MSMask.MaskEdBox mskMontaje 
         Height          =   315
         Index           =   0
         Left            =   780
         TabIndex        =   1
         Tag             =   "N� Ref|N�mero de Referencia"
         Top             =   540
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   556
         _Version        =   327681
         PromptInclude   =   0   'False
         AutoTab         =   -1  'True
         MaxLength       =   6
         Mask            =   "######"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox mskMontaje 
         Height          =   315
         Index           =   1
         Left            =   2580
         TabIndex        =   2
         Tag             =   "N� Placa|N�mero de Placa"
         Top             =   540
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   556
         _Version        =   327681
         PromptInclude   =   0   'False
         AutoTab         =   -1  'True
         MaxLength       =   3
         Mask            =   "###"
         PromptChar      =   " "
      End
      Begin VB.Label lblMontaje 
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Index           =   0
         Left            =   3900
         TabIndex        =   19
         Tag             =   "Cultivo|Tipo de Cultivo"
         Top             =   360
         Width           =   2115
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Cultivo"
         Height          =   255
         Index           =   3
         Left            =   3360
         TabIndex        =   8
         Top             =   420
         Width           =   555
      End
      Begin VB.Label lblLabel1 
         Caption         =   "N� Placa"
         Height          =   255
         Index           =   1
         Left            =   1860
         TabIndex        =   6
         Top             =   600
         Width           =   645
      End
      Begin VB.Label lblLabel1 
         Caption         =   "N� Micro"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   600
         Width           =   645
      End
   End
   Begin SSDataWidgets_B.SSDBGrid grdSSMesa 
      Height          =   1335
      Index           =   0
      Left            =   60
      TabIndex        =   3
      TabStop         =   0   'False
      Tag             =   "Mesa|Mesa de Colocaci�n de Cultivos"
      Top             =   6300
      WhatsThisHelpID =   10124
      Width           =   11640
      ScrollBars      =   0
      _Version        =   131078
      DataMode        =   2
      Cols            =   28
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RecordSelectors =   0   'False
      ColumnHeaders   =   0   'False
      Col.Count       =   28
      stylesets.count =   5
      stylesets(0).Name=   "Incompleta"
      stylesets(0).ForeColor=   0
      stylesets(0).BackColor=   65535
      stylesets(0).Picture=   "MB00153.frx":0000
      stylesets(0).AlignmentText=   7
      stylesets(0).AlignmentPicture=   1
      stylesets(1).Name=   "Falta"
      stylesets(1).BackColor=   255
      stylesets(1).Picture=   "MB00153.frx":001C
      stylesets(2).Name=   "Localizacion"
      stylesets(2).ForeColor=   65535
      stylesets(2).BackColor=   16711680
      stylesets(2).Picture=   "MB00153.frx":0038
      stylesets(3).Name=   "Vacia"
      stylesets(3).Picture=   "MB00153.frx":0054
      stylesets(4).Name=   "Colocado"
      stylesets(4).BackColor=   65280
      stylesets(4).Picture=   "MB00153.frx":0070
      DefColWidth     =   706
      MultiLine       =   0   'False
      ActiveCellStyleSet=   "Localizacion"
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowColumnSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      SelectByCell    =   -1  'True
      RowNavigation   =   3
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      CaptionAlignment=   0
      Columns(0).Width=   706
      Columns(0).DataType=   8
      Columns(0).FieldLen=   4096
      UseDefaults     =   0   'False
      _ExtentX        =   20532
      _ExtentY        =   2355
      _StockProps     =   79
      Caption         =   "MESA 1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   8280
      WhatsThisHelpID =   10123
      Width           =   11430
      _ExtentX        =   20161
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin ComctlLib.ImageList imlIconos 
      Left            =   480
      Top             =   300
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
End
Attribute VB_Name = "frmP_MontajeMesa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objCarpetaInfo As New clsCWForm
Dim arTecPend() As typeTecMesa      ' Array de t�cnicas pednientes de colocar
Dim arMesa() As typeMesa              ' Array de la mesa

' Constantes que definen las propiedades de la mesa
Dim intMesaCols     ' Ya que no se permite cambiar en runtime el n� de columnas,
                    ' se pone en design time y se recoge en la variable
Private Const constMesaRows = 3

Private Const constLocalizarPrimero = 1
Private Const constLocalizarUltimo = 2
Private Function fEstadoColumnaMesa(posEnArray%, posEnMesa%) As Integer
    ' Recoge la posici�n de la t�cnica en el array de t�cnicas
    ' para colocar y la posici�n del n� de referencia en el
    ' array de la mesa.
    ' Devuelve el estado de la columna.
    
    Dim i As Integer
    Dim j As Integer
    Dim localizado As Integer
    Dim comienzoNRef As Integer
    
    
    ' Si est�n todas las placas colocadas -> estado=completo
    If arMesa(posEnMesa).nTecCol = arMesa(posEnMesa).nTec Then
        fEstadoColumnaMesa = constColColocado
    ElseIf arMesa(posEnMesa).nTecCol = 0 Then
        fEstadoColumnaMesa = constColVacia
    Else
        ' Se mira hacia atr�s en el array arTecPend para ver si se ha saltado alguna placa del NRef
        If posEnArray = 1 Then
            comienzoNRef = 1
        Else
            For i = posEnArray To 1 Step -1
                If arTecPend(i).nRef <> arTecPend(posEnArray).nRef Then
                    comienzoNRef = i + 1
                    Exit For
                End If
            Next i
        End If
        
        'Se mira si hay alguna sin colocar
        If comienzoNRef = 0 Then comienzoNRef = 1
        For i = comienzoNRef To UBound(arTecPend)
            'If arTecPend(i).nRef <> arTecPend(posEnArray).nRef Then
            '    Exit For
            'ElseIf arTecPend(i).colocado = False Then
            
            If arTecPend(i).colocado = False Then
                ' Se ha localizado uno sin colocar
                ' Se mira si de ah� en adelante hay alguno colocado
                ' Si hay alguno colocado -> estado=Falta
                '           si no        -> estado=incompleta
                For j = i + 1 To UBound(arTecPend)
                    If arTecPend(j).nRef <> arTecPend(posEnArray).nRef Then
                        Exit For
                    ElseIf arTecPend(i).colocado = True Then
                        localizado = True
                        Exit For
                    End If
                Next j
                If localizado = True Then ' Hab�a una sin poner, y por detr�s hab�a otras puestas
                    fEstadoColumnaMesa = constColFalta
                Else
                    If arMesa(posEnMesa).nTecCol = 0 Then
                        fEstadoColumnaMesa = constColVacia
                    Else
                        fEstadoColumnaMesa = constColIncompleta
                    End If
                End If
                Exit For
            End If
        Next i
        
    End If
End Function

Private Function fLocalizarPrimeraTecnica(nRef$, colocado As Boolean, posicion%) As Integer
    ' recoge:
    '       NRef: N�mero de referencia a localizar
    '       estado:     true=colocado
    '                   false=nocolocado
    '       posicion:   1=primero (constLocalizarPrimero)
    '                   2=�ltimo  (constLocalizarUltimo)
    
    Dim i As Integer
    For i = 1 To UBound(arTecPend)
        If nRef = arTecPend(i).nRef And arTecPend(i).colocado = colocado Then
            fLocalizarPrimeraTecnica = i
            If posicion = constLocalizarPrimero Then Exit For
        ElseIf arTecPend(i).nRef > nRef Then
            Exit For
        End If
    Next i
    
End Function

Private Function fLugarEnMesa(posEnArray%, Optional fila%, Optional columna%) As Boolean
    ' Localiza el lugar que le corresponde en la mesa
    ' Lo se�ala en el grid y devuelve la fila y la columna
    
    Dim dimension As Integer
    Dim i As Integer
    Dim posEnMesa As Integer
    
    On Error Resume Next
    ' Se indica el lugar que le corresponde en la mesa
    dimension = UBound(arMesa)
    On Error GoTo 0
    For i = 1 To dimension
        If arMesa(i).nRef = arTecPend(posEnArray).nRef Then
            posEnMesa = i
            fLugarEnMesa = True
            Exit For
        End If
    Next i
    
    ' Se se�ala el lugar en el grid
    grdSSMesa(0).Col = (posEnMesa - 1) Mod intMesaCols
    grdSSMesa(0).Row = Int((posEnMesa - 1) / intMesaCols)

    fila = grdSSMesa(0).Row
    columna = grdSSMesa(0).Col
    
End Function

Private Sub pActivarBotones(estado As Boolean)
    cmdColocar(0).Enabled = estado
    cmdMontaje(0).Enabled = estado
    cmdMontaje(1).Enabled = estado
End Sub

Private Sub pActivarCodigoBarras()
    On Error Resume Next
    DoEvents
    txtCodigoBarras.SetFocus
End Sub

Private Sub pCambioPlaca()
    ' Se mira si la placa est� pendiente o colocada.
    ' Seg�n d�nde est�, se pone una etiqueta u otra en el
    ' bot�n cmdColocar.
    
    Dim dimension As Integer
    Dim i As Integer
    Dim blnLocalizado As Boolean
    Dim res As Boolean
    
    
    lblMontaje(0).Caption = ""
    If mskMontaje(0).Text <> "" And mskMontaje(1).Text <> "" Then
        On Error Resume Next
        dimension = UBound(arTecPend)
        On Error GoTo 0
        blnLocalizado = False
        
        For i = 1 To dimension
            If mskMontaje(0).Tag = arTecPend(i).nRef And Val(mskMontaje(1).Text) = arTecPend(i).codPlaca Then
                blnLocalizado = True
                Exit For
            End If
        Next i
        If blnLocalizado = True Then
            Call objPipe.PipeSet("nRef", arTecPend(i).nRef)
            Call objPipe.PipeSet("codTecAsist", arTecPend(i).codTecAsist)
            Call objPipe.PipeSet("codPlaca", arTecPend(i).codPlaca)
            
            pActivarBotones (True)
            lblMontaje(0).Caption = arTecPend(i).desigTecnica
            Select Case arTecPend(i).colocado
                Case False
                    cmdColocar(0).Caption = "&Colocar"
                Case True
                    cmdColocar(0).Caption = "&Quitar"
            End Select
            
            res = fLugarEnMesa(i)
        Else
            pActivarBotones (False)
        End If
    Else
        pActivarBotones (False)
    End If


End Sub

Private Function fCambioPlacaCodigoBarras() As Boolean
    ' Se mira si la placa est� pendiente o colocada.
    ' Seg�n d�nde est�, se pone una etiqueta u otra en el
    ' bot�n cmdColocar.
    
    Dim dimension As Integer
    Dim i As Integer
    Dim blnLocalizado As Boolean
    Dim res As Boolean
    Dim strNRef As String
    Dim strCodPlaca As String
    
    
    lblMontaje(0).Caption = ""
    If txtCodigoBarras.Text <> "" Then
        On Error Resume Next
        dimension = UBound(arTecPend)
        On Error GoTo 0
        blnLocalizado = False
        
        strNRef = Left(txtCodigoBarras, 12)
        strCodPlaca = Right(txtCodigoBarras, 3)
        For i = 1 To dimension
            If strNRef = arTecPend(i).nRef And Val(strCodPlaca) = arTecPend(i).codPlaca Then
                blnLocalizado = True
                Exit For
            End If
        Next i
        If blnLocalizado = True Then
            Call objPipe.PipeSet("nRef", arTecPend(i).nRef)
            Call objPipe.PipeSet("codTecAsist", arTecPend(i).codTecAsist)
            Call objPipe.PipeSet("codPlaca", arTecPend(i).codPlaca)
            
            pActivarBotones (True)
            lblMontaje(0).Caption = arTecPend(i).desigTecnica
            Select Case arTecPend(i).colocado
                Case False
                    cmdColocar(0).Caption = "&Colocar"
                Case True
                    cmdColocar(0).Caption = "&Quitar"
            End Select
            
            res = fLugarEnMesa(i)
            fCambioPlacaCodigoBarras = True
        Else
            pActivarBotones (False)
            fCambioPlacaCodigoBarras = False
        End If
    Else
        pActivarBotones (False)
    End If


End Function

Private Sub pCargarMesa()
    ' A cada celda del grid se le asigna un n�mero de referencia
    
    Dim nRef As String
    Dim dimension1 As Integer
    Dim dimension As Integer
    Dim i As Integer
    
    On Error Resume Next
    dimension = UBound(arTecPend)
    On Error GoTo 0
    Erase arMesa
    
    For i = 1 To dimension
        If nRef <> arTecPend(i).nRef Then
            nRef = arTecPend(i).nRef
                
            On Error Resume Next
            dimension1 = UBound(arMesa)
            dimension1 = dimension1 + 1
            ReDim Preserve arMesa(1 To dimension1)
            arMesa(dimension1).nRef = nRef
            arMesa(dimension1).nTec = 1
            arMesa(dimension1).nTecCol = 0
            arMesa(dimension1).estado = constColVacia
        Else
            arMesa(dimension1).nTec = arMesa(dimension1).nTec + 1
        End If
    Next i
    
End Sub

Private Sub pColocarEnMesa(posEnArray%)
    ' Mira en qu� posici�n de la mesa se tiene que colocar
    ' Se�ala mediante un color donde se coloca
    ' Actualiza el array de la mesa
    '           - N� de placas colocadas
    '           - Estado de la posici�n
    
    Dim nRef As String
    Dim dimension As Integer
    Dim i As Integer
    Dim posEnMesa As Integer    ' Posici�n del n� de referencia en el array arMesa
    
    nRef = arTecPend(posEnArray).nRef
    
    ' Se localiza en la mesa d�nde se coloca
    On Error Resume Next
    dimension = UBound(arMesa)
    On Error GoTo 0
    For i = 1 To dimension
        If arMesa(i).nRef = arTecPend(posEnArray).nRef Then
            posEnMesa = i
            Exit For
        End If
    Next i
    
    ' Se se�ala el lugar en el grid
    grdSSMesa(0).Col = (posEnMesa - 1) Mod intMesaCols
    grdSSMesa(0).Row = Int((posEnMesa - 1) / intMesaCols)
        
    ' Se actualiza el dato de placas colocadas
    arMesa(posEnMesa).nTecCol = arMesa(posEnMesa).nTecCol + 1
    
    ' Se cambia el estado de la columna
    
    arMesa(posEnMesa).estado = fEstadoColumnaMesa(posEnArray, posEnMesa)
    
    ' Se actualiza el color de la celda
    Call pColorMesa(grdSSMesa(0).Col, arMesa(posEnMesa).estado, posEnMesa)
    
    If arMesa(posEnMesa).estado <> constColVacia Then
        ' Se mira si en las posiciones anteriores a quedado alguna sin colocar
        For i = 1 To posEnMesa - 1
            If arMesa(i).estado <> constColColocado Then
                arMesa(i).estado = constColFalta
                ' Se actualiza el color de la celda
                Call pColorMesa(grdSSMesa(0).Col, arMesa(i).estado, i)
            End If
        Next i
    End If
    If arMesa(posEnMesa).estado <> constColColocado Then
        ' Se mira si en las posiciones posteriores hay alguna colocada
        For i = posEnMesa + 1 To UBound(arMesa)
            If arMesa(i).estado <> constColVacia Then
                arMesa(posEnMesa).estado = constColFalta
                ' Se actualiza el color de la celda
                Call pColorMesa(grdSSMesa(0).Col, arMesa(posEnMesa).estado, posEnMesa)
                Exit For
            End If
        Next i
    End If
    
End Sub

Private Sub pColorMesa(ByVal columna%, ByVal estado%, ByVal posicion%)
    Dim linea As Integer
    'If posicion > grdSSMesa(0).Columns.Count - 1 Then
        linea = Int(posicion / grdSSMesa(0).Columns.Count)
        columna = (posicion Mod grdSSMesa(0).Columns.Count) - 1
    'End If
    grdSSMesa(0).Row = linea
    Select Case estado
        Case constColVacia
            grdSSMesa(0).Columns(columna).CellStyleSet "Vacia", 0
        Case constColIncompleta
            grdSSMesa(0).Columns(columna).CellStyleSet "Incompleta", 0
        Case constColFalta
            grdSSMesa(0).Columns(columna).CellStyleSet "Falta", 0
        Case constColColocado
            grdSSMesa(0).Columns(columna).CellStyleSet "Colocado", 0
    End Select

End Sub

Private Sub pConfigurarGrids()
  pFormatearGrid grdssCarpetas(0)
  pFormatearGrid grdssCultivos(0)
  pFormatearGrid grdssCultivos(1)
End Sub

Private Sub pFormatearGridMesa()
    Dim i As Variant
    For Each i In grdSSMesa(0).Columns
        i.Locked = True
    Next i
End Sub

Private Sub pLimpiar()
    ' Da como colocadas las placas de la parte derecha y pone la mesa en blanco
    ' para empezar desde cero.
    
    Dim arTemp() As typeTecMesa
    Dim i As Integer
    Dim dimension As Integer
    Dim dimension1 As Integer
    
    On Error Resume Next
    dimension = UBound(arTecPend)
    For i = 1 To dimension
        If arTecPend(i).colocado = False Then
            dimension1 = UBound(arTemp)
            dimension1 = dimension1 + 1
            ReDim Preserve arTemp(1 To dimension1)
            arTemp(dimension1).nRef = arTecPend(i).nRef
            arTemp(dimension1).codTecAsist = arTecPend(i).codTecAsist
            arTemp(dimension1).codPlaca = arTecPend(i).codPlaca
            arTemp(dimension1).desigCondicion = arTecPend(i).desigCondicion
            arTemp(dimension1).desigTecnica = arTecPend(i).desigTecnica
        End If
    Next i
    
    Erase arTecPend
    ReDim arTecPend(1 To dimension1)
    For i = 1 To dimension1
        arTecPend(i).nRef = arTemp(i).nRef
        arTecPend(i).codTecAsist = arTemp(i).codTecAsist
        arTecPend(i).codPlaca = arTemp(i).codPlaca
        arTecPend(i).desigCondicion = arTemp(i).desigCondicion
        arTecPend(i).desigTecnica = arTemp(i).desigTecnica
        arTecPend(i).colocado = False
    Next i
    
    ' Se muestran en pantalla las t�cnicas cargadas
    Call pMostrarTecnicasEnPantalla(arTecPend())
    
    ' Se dibuja la mesa en el grid inferior
    Call pCargarMesa

    ' Se vac�a la mesa
    Call pVaciarMesa
    
    ' Se muestra la Mesa
    Call pMostrarMesa

End Sub

Private Sub pMostrarMesa()
    ' Se rellena el grid de Mesa
    
    Dim dimension As Integer
    Dim i As Integer
    Dim linea As Integer
    Dim columna As Integer
    Dim locGrid As Integer
    Dim lineaAnterior As Integer
    Dim texto As String
        
    On Error Resume Next
    dimension = UBound(arMesa)
    For i = 1 To dimension
        locGrid = i - 1
        
        columna = locGrid Mod intMesaCols
        linea = Int(locGrid / intMesaCols)

        If columna = 0 Then
            texto = ""
        Else
            texto = texto & Chr$(9)
        End If
        
    
        grdSSMesa(0).Row = linea
        grdSSMesa(0).Columns(columna).Text = arMesa(i).nRef
            
        ' Se inserta como texto el n� de referencia
        'grdSSMesa(0).Columns(columna).Text = arMesa(i).nRef
        texto = texto & arMesa(i).nRef
        
        If i = dimension Or columna = grdSSMesa(0).Cols - 1 Then
            grdSSMesa(0).AddItem texto
        End If
    Next i
    
    grdSSMesa(0).Col = 0
    grdSSMesa(0).Row = 0
End Sub
Private Sub pQuitarDeMesa(posEnArray%)
    ' Mira de qu� posici�n de la mesa se tiene que quitar
    ' Actualiza el array de la mesa
    '           - N� de placas colocadas
    '           - Estado de la posici�n
    
    Dim nRef As String
    Dim dimension As Integer
    Dim i As Integer
    Dim j As Integer
    Dim posEnMesa As Integer    ' Posici�n del n� de referencia en el array arMesa
    Dim localizado As Integer
    
    nRef = arTecPend(posEnArray).nRef
    
    ' Se localiza en la mesa d�nde se coloca
    On Error Resume Next
    dimension = UBound(arMesa)
    On Error GoTo 0
    For i = 1 To dimension
        If arMesa(i).nRef = arTecPend(posEnArray).nRef Then
            posEnMesa = i
            Exit For
        End If
    Next i
    
    ' Se se�ala el lugar en el grid
    grdSSMesa(0).Col = (posEnMesa - 1) Mod intMesaCols
    grdSSMesa(0).Row = Int((posEnMesa - 1) / intMesaCols)
        
    ' Se actualiza el dato de placas colocadas
    arMesa(posEnMesa).nTecCol = arMesa(posEnMesa).nTecCol - 1
    
    ' Se cambia el estado de la columna
    arMesa(posEnMesa).estado = fEstadoColumnaMesa(posEnArray, posEnMesa)
    
    ' Se actualiza el color de la celda
    Call pColorMesa(grdSSMesa(0).Col, arMesa(posEnMesa).estado, posEnMesa)


    If arMesa(posEnMesa).estado = constColVacia Then
        ' Se mira si en las posiciones anteriores a quedado alguna sin colocar
        For i = posEnMesa - 1 To 1 Step -1
            ' Si se encuentra alguno colocado, se actualiza su estado, y a en los anteriores se mira
            ' si alguno se ha quedado sin colocar
            If arMesa(i).nTecCol > 0 Then
                arMesa(i).estado = fEstadoColumnaMesa(posEnArray, i)
                Call pColorMesa(grdSSMesa(0).Col, arMesa(i).estado, i)
                For j = 1 To i - 1
                    If arMesa(j).estado <> constColColocado Then
                        arMesa(j).estado = constColFalta
                        ' Se actualiza el color de la celda
                        Call pColorMesa(grdSSMesa(0).Col, arMesa(j).estado, j)
                    End If
                Next j
                Exit For
            Else
                arMesa(i).estado = constColVacia
                ' Se actualiza el color de la celda
                Call pColorMesa(grdSSMesa(0).Col, arMesa(i).estado, i)
            End If
        Next i
    End If
    If arMesa(posEnMesa).estado <> constColColocado Then
        ' Se mira si en las posiciones posteriores hay alguna colocada
        For i = posEnMesa + 1 To UBound(arMesa)
            If arMesa(i).estado <> constColVacia Then
                arMesa(posEnMesa).estado = constColFalta
                ' Se actualiza el color de la celda
                Call pColorMesa(grdSSMesa(0).Col, arMesa(posEnMesa).estado, posEnMesa)
                Exit For
            End If
        Next i
    End If
End Sub
Private Sub pQuitarPlaca(nRef$, cPlaca$)
    ' Se quita de como colocada la placa
    ' Se cambia el estado 'No colocado' en la matriz de t�cnicas pendientes
    ' Se quita el dato del grid de colocados y se introduce en el de pendientes
    
    Dim dimension As Integer    ' Indica el n� de datos en la matriz de t�cnicas
    Dim i As Integer
    Dim posEnGridPend As Integer    ' Indica la posici�n del dato en el grid de pendientes
    Dim posEnGridCol As Integer     ' Indica la posici�n del dato en el grid de colocados
    
    On Error Resume Next
    dimension = UBound(arTecPend)
    On Error GoTo 0
        
    For i = 1 To dimension
        If arTecPend(i).colocado = True Then
            posEnGridCol = posEnGridCol + 1
            If nRef = arTecPend(i).nRef And Val(cPlaca) = arTecPend(i).codPlaca Then
                arTecPend(i).colocado = False
                Exit For
            End If
        Else
            posEnGridPend = posEnGridPend + 1
        End If
    Next i
    If i > dimension Then i = dimension
    ' Se borra de colocados
    grdssCultivos(1).RemoveItem posEnGridCol - 1
    
    'If posEnGridCol - 1 >= grdssCultivos(1).Rows Then
    '    grdssCultivos(1).Row = grdssCultivos(1).Rows - 1
    'Else
    '    grdssCultivos(1).Row = posEnGridCol - 1
    'End If
    
    ' Se introduce en pendientes
    Call pInsertarEnGrid(grdssCultivos(0), i, posEnGridPend)
    
    ' Se busca su posici�n en la mesa
    Call pQuitarDeMesa(i)

End Sub
Private Sub pColocarPlaca(nRef$, cPlaca$)
    ' Se da como colocada la placa
    ' Se cambia el estado 'Colocado' en la matriz de t�cnicas pendientes
    ' Se quita el dato del grid de pendientes y se introduce en el de colocados
    
    Dim dimension As Integer    ' Indica el n� de datos en la matriz de t�cnicas
    Dim i As Integer
    Dim posEnGridPend As Integer    ' Indica la posici�n del dato en el grid de pendientes
    Dim posEnGridCol As Integer     ' Indica la posici�n del dato en el grid de colocados
    
    On Error Resume Next
    dimension = UBound(arTecPend)
    On Error GoTo 0
        
    
    For i = 1 To dimension
        If arTecPend(i).colocado = False Then
            posEnGridPend = posEnGridPend + 1
            If nRef = arTecPend(i).nRef And Val(cPlaca) = arTecPend(i).codPlaca Then
                arTecPend(i).colocado = True
                Exit For
            End If
        Else
            posEnGridCol = posEnGridCol + 1
        End If
    Next i
    ' Se borra de pendientes
    grdssCultivos(0).RemoveItem posEnGridPend - 1
    'grdssCultivos(0).Row = posEnGridPend - 1
    ' Se introduce en colocados
    Call pInsertarEnGrid(grdssCultivos(1), i, posEnGridCol)
    
    ' Se busca su posici�n en la mesa
    Call pColocarEnMesa(i)
    
        
End Sub

Private Sub pInsertarEnGrid(grdSSDestino As SSDBGrid, posArray%, posGrid%)
    Dim texto As String
    
    texto = fNumeroMicro(arTecPend(posArray).nRef)
    texto = texto & Chr$(9) & arTecPend(posArray).codTecAsist
    If arTecPend(posArray).codPlaca = 0 Then
        texto = texto & Chr$(9)
    Else
        texto = texto & Chr$(9) & arTecPend(posArray).codPlaca
    End If
    texto = texto & Chr$(9) & arTecPend(posArray).desigTecnica
    texto = texto & Chr$(9) & arTecPend(posArray).desigCondicion
    texto = texto & Chr$(9) & arTecPend(posArray).nRef

    grdSSDestino.AddItem texto, posGrid
End Sub

Private Sub pMostrarPlacasPendientes()
    ' Se muestran en el grid de la izquierda las placas que hay que preparar
        
    Dim cCarpeta As String
    Dim fechaAhora As String
    
    fechaAhora = fFechaHoraActual()

    cCarpeta = grdssCarpetas(0).Columns(3).Text
    Erase arTecPend
    
    ' Se cargan primero las t�cnicas realizadas sobre la muestra  ordenadas por N� Referencia
    Call pCargarTecnicasSobreMuestra(arTecPend(), constORIGENMUESTRA, constORIGENMUESTRA, cCarpeta, fechaAhora)
    
    ' Se cargan el resto de las t�cnicas ordenadas por N� Referencia
    '   *************ACTIVARLO CUANDO SE TENGA EN CUENTA ORIGEN  ******************
    'Call pCargarTecnicasSobreMuestra(arTecPend(), constORIGENPLACA, constORIGENCOLONIA, cCarpeta, fechaAhora)
    
    ' Se muestran en pantalla las t�cnicas cargadas
    Call pMostrarTecnicasEnPantalla(arTecPend())
    
    ' Se dibuja la mesa en el grid inferior
    Call pCargarMesa

    ' Se vac�a la mesa
    Call pVaciarMesa
    
    ' Se muestra la Mesa
    Call pMostrarMesa
    
    ' Se vac�an los mask
    mskMontaje(0).Text = ""
End Sub
Private Sub pVaciarMesa()
    ' Deja el grid Mesa en blanco
    grdSSMesa(0).RemoveAll
    
End Sub


Private Sub pMostrarTecnicasEnPantalla(arTecPend() As typeTecMesa)
    ' Muestra en el Grid de 'pendientes' las t�cnicas para ver hoy
       
    Dim dimension As Integer
    Dim i As Integer
    Dim texto As String
    
    On Error Resume Next
    
    grdssCultivos(0).RemoveAll
    grdssCultivos(1).RemoveAll
    
    dimension = UBound(arTecPend)
    
    ' Se cargan el grid
    '   N� Referencia       -> visible
    '   N� T�c. en Asist    -> invisible
    '   N� Placa            -> visible
    '   Designaci�n placa   -> visible
    '   Condici�n Ambiental -> visible
    
    For i = 1 To dimension
        If arTecPend(i).colocado = False Then
            Call pInsertarEnGrid(grdssCultivos(0), i, i - 1)
        Else
            Call pInsertarEnGrid(grdssCultivos(1), i, i - 1)
        End If
    Next i
    
End Sub

Private Sub cmdColocar_Click(Index As Integer)
    ' Se recoge el n� de placa y se da como colocado o se retira de la mesa
    
    Select Case UCase(cmdColocar(0).Caption)
        Case "&COLOCAR"
            Call pColocarPlaca(mskMontaje(0).Tag, mskMontaje(1).Text)
        Case "&QUITAR"
            Call pQuitarPlaca(mskMontaje(0).Tag, mskMontaje(1).Text)
    End Select
    mskMontaje(0).Text = ""
    
End Sub

Private Sub cmdMontaje_Click(Index As Integer)
    Select Case Index
        Case 0  ' Pruebas
            
        Case 1  ' Problemas
            Call pVentanaProblemas(objPipe.PipeGet("nRef"), , objPipe.PipeGet("codTecAsist"), objPipe.PipeGet("codPlaca"))
        Case 2  ' Limpiar
            Call pLimpiar
    End Select
End Sub

Private Sub Form_Activate()
    pFormatearGridMesa
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    pActivarCodigoBarras
End Sub


Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
    
  pConfigurarGrids
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithToolBar + cwWithStatusBar + cwWithoutMenu)
  
  With objCarpetaInfo
    .strName = "CARPETA"
    Set .objFormContainer = fraCarpetas(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = Nothing
    Set .grdGrid = grdssCarpetas(0)
    .intAllowance = cwAllowReadOnly
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .blnPKAutomatic = False
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "carpetas"
    .strWhere = "cDptoSecc=" & departamento
    
    Call .FormAddOrderField("DESIGNACION", cwAscending)
  End With
   

  With objWinInfo
    Call .FormAddInfo(objCarpetaInfo, cwFormMultiLine)
     
    Call .GridAddColumn(objCarpetaInfo, "C�digo", "CCARPETA", cwNumeric, 4)
    Call .GridAddColumn(objCarpetaInfo, "Carpeta", "DESIGNACION", cwString, 25)
    
    Call .FormCreateInfo(objCarpetaInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    grdssCarpetas(0).Columns(0).Visible = False
    grdssCarpetas(0).Columns(3).Visible = False

    Call .WinRegister
    Call .WinStabilize
  End With

    ' Se ponen las medidas de la mesa
    intMesaCols = grdSSMesa(0).Cols
    'grdSSMesa(0).Rows = constMesaRows
    
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, UnloadMode As Integer)
  intCancel = objWinInfo.WinExit

End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub fraCarpetas_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraCarpetas, False, True)
End Sub

Private Sub grdssCarpetas_RowColChange(Index As Integer, ByVal LastRow As Variant, ByVal LastCol As Integer)
    grdSSMesa(0).Caption = grdssCarpetas(0).Columns(4).Text
    pMostrarPlacasPendientes
    
End Sub

Private Sub grdssCultivos_Click(Index As Integer)
    ' Se colocan los datos en la parte superior
    
    mskMontaje(0).Text = grdssCultivos(Index).Columns(0).Text
    mskMontaje(0).Tag = grdssCultivos(Index).Columns(5).Text
    mskMontaje(1).Text = grdssCultivos(Index).Columns(2).Text
    
    If cmdColocar(0).Enabled = True Then
        cmdColocar(0).SetFocus
    End If
End Sub


Private Sub grdssCultivos_DblClick(Index As Integer)
    'grdssCultivos_Click (Index)
        ' Se recoge el n� de placa y se da como colocado o se retira de la mesa
    
    If grdssCultivos(Index).Columns(5).Text <> "" Then
        Select Case Index
            Case 0
                Call pColocarPlaca(grdssCultivos(Index).Columns(5).Text, grdssCultivos(Index).Columns(2).Text)
            Case 1
                Call pQuitarPlaca(grdssCultivos(Index).Columns(5).Text, grdssCultivos(Index).Columns(2).Text)
        End Select
        mskMontaje(0).Text = ""
    End If
End Sub



Private Sub grdSSMesa_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'KeyCode = 0
End Sub


Private Sub grdSSMesa_KeyPress(Index As Integer, KeyAscii As Integer)
'    KeyAscii = 0
End Sub


Private Sub grdSSMesa_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    Dim i As Integer
    Dim posEnArray As Integer
    
    For i = 0 To grdSSMesa(0).Cols - 1
        posEnArray = grdSSMesa(0).Row * (intMesaCols - 1) + i + 1
        If posEnArray <= UBound(arMesa) Then
            grdSSMesa(0).Columns(i).Text = fNumeroMicro(arMesa(posEnArray).nRef)
            Call pColorMesa(i, arMesa(posEnArray).estado, posEnArray)
        End If
    Next i
    
End Sub


Private Sub mskMontaje_Change(Index As Integer)
    Select Case Index
        Case 0
            If mskMontaje(0).Text = "" Then
                mskMontaje(0).Tag = ""
            End If
            mskMontaje(1).Text = ""
        Case 1
            pCambioPlaca
    End Select
    
End Sub

Private Sub txtCodigoBarras_GotFocus()
    txtCodigoBarras.SelStart = 0
    txtCodigoBarras.SelLength = Len(txtCodigoBarras.Text)
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    Call objWinInfo.WinProcess(cwProcessToolBar, Button.Index, 0)
End Sub

Private Sub txtCodigoBarras_KeyPress(KeyAscii As Integer)
    Dim intRes As Integer
    
    If KeyAscii = 13 Then
        intRes = fCambioPlacaCodigoBarras()


        If intRes = True Then
            mskMontaje(0).Text = Val(Mid$(txtCodigoBarras.Text, 6, 7))
            mskMontaje(0).Tag = Left$(txtCodigoBarras.Text, 12)
            mskMontaje(1).Text = Val(Mid$(txtCodigoBarras.Text, 13))
            Select Case UCase(cmdColocar(0).Caption)
                Case "&COLOCAR"
                    Call pColocarPlaca(mskMontaje(0).Tag, mskMontaje(1).Text)
                Case "&QUITAR"
                    Call pQuitarPlaca(mskMontaje(0).Tag, mskMontaje(1).Text)
            End Select
            txtCodigoBarras.SetFocus
            txtCodigoBarras.SelStart = 0
            txtCodigoBarras.SelLength = Len(txtCodigoBarras.Text)
        Else
            txtCodigoBarras.Text = ""
            MsgBox "La placa no se encuentra en la lista de placas a colocar o en la lista de placas colocadas.", vbExclamation, "Montaje de la mesa"
        End If
    End If
End Sub

