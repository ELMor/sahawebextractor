VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "mscomm32.ocx"
Begin VB.Form frmU_Vitek 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pruebas del Vitek"
   ClientHeight    =   6285
   ClientLeft      =   1815
   ClientTop       =   2190
   ClientWidth     =   8685
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6285
   ScaleWidth      =   8685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   8685
      _ExtentX        =   15319
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Prueba Respuesta"
      Height          =   675
      Left            =   7380
      TabIndex        =   15
      Top             =   2880
      Width           =   1155
   End
   Begin VB.CommandButton cmdRecibir 
      Caption         =   "&Recibir"
      Height          =   375
      Left            =   7380
      TabIndex        =   13
      Top             =   1080
      Width           =   1155
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   7380
      TabIndex        =   10
      Top             =   1620
      Width           =   1155
   End
   Begin Threed.SSFrame fraFrame2 
      Height          =   735
      Index           =   0
      Left            =   7500
      TabIndex        =   8
      Top             =   4980
      Width           =   1155
      _Version        =   65536
      _ExtentX        =   2037
      _ExtentY        =   1296
      _StockProps     =   14
      Caption         =   "SSFrame1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox Text1 
         DataField       =   "cAutoanalizador"
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Text            =   "Text1"
         Top             =   360
         Width           =   915
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5775
      Left            =   120
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   240
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   10186
      _Version        =   327681
      Style           =   1
      MousePointer    =   99
      TabHeight       =   520
      TabCaption(0)   =   "Pendientes"
      TabPicture(0)   =   "mb00168.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Programadas"
      TabPicture(1)   =   "mb00168.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraFrame1(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Recibidas"
      TabPicture(2)   =   "mb00168.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraFrame1(2)"
      Tab(2).ControlCount=   1
      Begin VB.Frame fraFrame1 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5355
         Index           =   2
         Left            =   -74940
         TabIndex        =   11
         Top             =   360
         Width           =   6960
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5205
            Index           =   2
            Left            =   60
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   60
            Width           =   6855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   12091
            _ExtentY        =   9181
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5355
         Index           =   1
         Left            =   -74940
         TabIndex        =   6
         Top             =   360
         Width           =   6960
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5205
            Index           =   1
            Left            =   60
            TabIndex        =   7
            TabStop         =   0   'False
            Top             =   60
            Width           =   6855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   12091
            _ExtentY        =   9181
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame fraFrame1 
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5355
         Index           =   0
         Left            =   60
         MousePointer    =   99  'Custom
         TabIndex        =   4
         Top             =   360
         Width           =   6960
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5205
            Index           =   0
            Left            =   60
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   60
            Width           =   6855
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RecordSelectors =   0   'False
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   12091
            _ExtentY        =   9181
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
   End
   Begin VB.Timer TimerTimeOut 
      Enabled         =   0   'False
      Interval        =   2000
      Left            =   7680
      Top             =   4500
   End
   Begin VB.CommandButton cmdProgramar 
      Caption         =   "&Programar"
      Height          =   375
      Left            =   7380
      TabIndex        =   2
      Top             =   540
      Width           =   1155
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   0
      Top             =   6000
      Visible         =   0   'False
      Width           =   8685
      _ExtentX        =   15319
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   8100
      Top             =   4500
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      DTREnable       =   -1  'True
      Handshaking     =   1
      InBufferSize    =   10240
      InputLen        =   1
      OutBufferSize   =   10240
      RThreshold      =   1
      RTSEnable       =   -1  'True
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFF00&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Programando"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   7320
      TabIndex        =   14
      Top             =   2220
      Visible         =   0   'False
      Width           =   1275
   End
End
Attribute VB_Name = "frmU_Vitek"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim objDetailPendientes As New clsCWForm
Dim objDetailProgramadas As New clsCWForm
Dim objDetailRecibidas As New clsCWForm
Dim objMasterInfo As New clsCWForm

'VARIABLES
Dim estado As Integer       'estado del sistema durante la comunicaci�n
Dim filas As Integer        'no de filas en el grid
Dim tipoMensaje As String   'indica el tipo de mensaje enviado
Dim numRetry As Integer     'n� de reintento de programar el autoanalziador

'CONSTANES
'Constantes de los estados de la comunicaci�n
Private Const constESPERA = 0
Private Const constSOLICITUD = 1
Private Const constMASTER = 2
Private Const constCONFIRMACION = 3
Private Const constFINAL = 4

'Constantes de la comunicaci�n
Private Const constTimeOut = 15     'm�ximo tiempo de espera de una respuesta
Private Const constMaxRetry = 3     'm�ximo n� de reintentos para programar el autoanalziador
Private Const constRetryTime = 2    'tiempo de espera entre reenvios

'Constantes de tipo de mensaje
Private Const constOOS = "OOS"
Private Const constBIS = "BIS"
Private Const constMPR = "MPR"
Private Const constRSL = "RSL"

'Constantes de sensibilidad de los antibi�ticos
Private Const constAntibS = 1
Private Const constAntibMS = 2
Private Const constAntibR = 3

'Constantes del Vitek
'Test Group Code
Private Const constTGCVitekID = 1
Private Const constTGCVitekSusc = 3

'Sensibilidades de antibi�ticos
Private Const constAntibVitekS = "S"
Private Const constAntibVitekMS = "MS"
Private Const constAntibVitekR = "R"

Private Function fMensaje() As String
'se construye el mensaje de descarga de demogr�ficos en el Vitek

'NOTA:
'   <GS> = chr$(29)
'   <RS> = chr$(30)
'En el mensaje hay que poner un <RS> al menos cada 80 car�cteres. En los mensajes actuales _
    nunca se llega a ese tama�o
    
    Dim msg As String
    Dim i As Integer
    Dim sum As Integer
    
    
    If tipoMensaje = constOOS Then
        msg = "mtoos|" 'mensaje de 'fuera de servicio' para no recibir resultados
    ElseIf tipoMensaje = constBIS Then
        msg = "mtbis|" 'mensaje de 'vuelta al servicio' para recibir resultados
    ElseIf tipoMensaje = constMPR Then
        'se se�ala el registro a cuyos datos se van a enviar
        objDetailPendientes.rdoCursor.AbsolutePosition = 1
        'mensaje de programacion de demogr�ficos
        msg = "mtmpr|"
        msg = msg & "pi" & objDetailPendientes.rdoCursor("historia") & "|"
        msg = msg & "si|" 'HABR� QUE ENV�AR ALGO PARA RECONSTRUIR EL N� REF.
        'n� peticion = nRef(yyXXXXXXX) + MB20_codTecAsist(XXX)
        msg = msg & "ci" & Mid$(objDetailPendientes.rdoCursor("nRef"), 4) _
                  & Left$("000", 3 - Len(objDetailPendientes.rdoCursor("MB20_codTecAsist"))) _
                  & objDetailPendientes.rdoCursor("MB20_codTecAsist") & "|"
        msg = msg & "ctbcilnk|"
    End If
    msg = Chr$(30) & msg & Chr$(29)
    For i = 1 To Len(msg)
        sum = sum + Asc(Mid$(msg, i, 1))
        If sum >= 256 Then sum = sum - 256
    Next i
    msg = msg & LCase(Hex$(sum))
    
    fMensaje = Chr$(2) & msg & Chr$(3)
    
End Function

Private Sub pEstadosProgramarRecibir(caracter$)
'Se emplea para recibir las respuestas <ACK>/<NAK> del Vitek durante el env�o de mensajes _
    desde el sistema al propio Vitek
    
    Dim tiempo As Long
    
    'se desactive el timer de los timeouts
    TimerTimeOut.Enabled = False
    
    Select Case estado
        Case constSOLICITUD
            Select Case Asc(caracter)
                Case 6 '<ACK> conexi�n aceptada
                    estado = constMASTER
                    numRetry = 0
                Case 22 '<NAK> conexi�n rechazada
                    estado = constESPERA
                    numRetry = numRetry + 1
                    'si no se ha alcanzado el m�ximo n� de reintentos de transmisi�n, se espera antes de volver a intentarlo
                    If numRetry <= constMaxRetry Then
                        tiempo = Timer
                        Do While tiempo + constRetryTime <= Timer
                            DoEvents
                        Loop
                    Else
                        numRetry = 0
                        Screen.MousePointer = vbDefault
                        Select Case tipoMensaje
                            Case constOOS
                                MsgBox "El autoanalizador no acepta la comunicaci�n. Salir e intentarlo de nuevo.", vbExclamation, "Comunicaci�n Vitek"
                            Case constMPR, constBIS
                                Label1.Visible = False
                                MsgBox "El autoanalizador no acepta la comunicaci�n. Intentarlo de nuevo.", vbExclamation, "Comunicaci�n Vitek"
                                Exit Sub 'para no ejecutar el pEstadosProgramarEnviar
                        End Select
                    End If
            End Select
        Case constCONFIRMACION
            Select Case Asc(caracter)
                Case 6 '<ACK> mensaje aceptado
                    estado = constFINAL
                    numRetry = 0
'                    Select Case tipoMensaje
'                        Case constOOS
'                            'cuando el Vitek acepta el 'oos', ya se puede comenzar a programar demogr�ficos
'                            Screen.MousePointer = vbDefault
'                            cmdProgramar.Enabled = True
'                        Case constBIS
'                            'cuando el Vitek acepta el 'bis', ya se puede comenzar a recibir resultados
'                            Label1.Caption = "Recibiendo"
'                        Case constMPR
'                            filas = filas - 1
'                            'se actualiza la BD y se quita el registro de pantalla
'                            objDetailPendientes.rdoCursor.Edit
'                            objDetailPendientes.rdoCursor("MB34_codEstTecAsist") = constTECREALIZANDOSE
'                            objDetailPendientes.rdoCursor.Update
'                            objWinInfo.DataRefresh
'                    End Select
                Case 22 '<NAK> mensaje rechazado
                    estado = constMASTER
                    numRetry = numRetry + 1
                    'si no se ha alcanzado el m�ximo n� de reintentos de transmisi�n, se espera antes de volver a intentarlo
                    If numRetry <= constMaxRetry Then
                        tiempo = Timer
                        Do While tiempo + constRetryTime <= Timer
                            DoEvents
                        Loop
                    Else
                        estado = constESPERA
                        numRetry = 0
                        Screen.MousePointer = vbDefault
                        Select Case tipoMensaje
                            Case constOOS
                                MsgBox "El autoanalizador no acepta el mensaje previo 'oos'. Salir e intentarlo de nuevo.", vbExclamation, "Comunicaci�n Vitek"
                            Case constBIS
                                MsgBox "El autoanalizador no acepta el mensaje previo 'bis'. Intentarlo de nuevo.", vbExclamation, "Comunicaci�n Vitek"
                                Exit Sub 'para no ejecutar el pEstadosProgramarEnviar
                            Case constMPR
                                Label1.Visible = False
                                MsgBox "El autoanalizador no acepta el mensaje. Intentarlo de nuevo.", vbExclamation, "Comunicaci�n Vitek"
                                Exit Sub 'para no ejecutar el pEstadosProgramarEnviar
                        End Select
                    End If
            End Select
    End Select
    
    Call pEstadosProgramarEnviar
                
End Sub



Private Sub pEstadosResultadosRecibir(caracter$)
'Se emplea para recibir los resultados del Vitek y contestar con <ACK>/<NAK>

    Static mensaje As String
    Dim copiaMensaje As String
    
    mensaje = mensaje & caracter
    
    Select Case Asc(caracter)
        Case 5 '<ENQ>
            Label1.Visible = True
            mensaje = ""
            MSComm1.Output = Chr$(6) '<ACK> se acepta la comunicaci�n
        Case 2 '<STX>
            copiaMensaje = mensaje
            mensaje = ""
            MSComm1.Output = Chr$(6) '<ACK> se acepta el mensaje. NO SE COMPRUEBA EL CHECK
            Call pProcesarResultados(copiaMensaje)
        Case 4 '<EOT> no hace falta responder
            mensaje = ""
            Label1.Visible = False
    End Select
    
End Sub
Private Sub pProcesarResultados(msg$)
'Se descompone el mensaje de respuesta

'Mensaje: <STX><RS>xxxxxxxx...<RS>xxxxxxxx...   ...  <GS>xx<ETX>

'Los campos se separan con el carcater "|" y se identifican por dos caracteres "cc". Por _
    tanto, excepto el primer campo, todos los dem�s se pueden reconocer con 3 carateres "|cc".

    Dim historia As String
    Dim nPeticion As String
    Dim nRef As String
    Dim cTecAsist As String
    Dim numPruebasProgramadas As Integer
    Dim posicion As Integer
    Dim i As Integer
    Dim arVitekTec() As typeVitekTec
    Dim dimTec As Integer
    Dim dimAntib As Integer
    Dim msgTec As String 'parte del mensaje correspondiente a una �nica tarjeta
    Dim campo As String, texto As String
      
    'se quitan el <STX> y <GS>xx<ETX>
    msg = Mid$(msg, 2, Len(msg) - 5)
    'se quitan los <RS>
    msg = objGen.ReplaceStr(msg, Chr$(30), "", 0) '--> Alguna vez se me ha quedado colgado
    'se comprueba que el mensaje es del tipo RSL (resultados)
    If UCase(Left$(msg, 5)) <> UCase$("MT" & constRSL) Then Exit Sub
    
    'se obtiene la historia
    posicion = InStr(msg, "|pi") + 3
    historia = Mid$(msg, posicion, InStr(posicion, msg, "|") - posicion)
    'se obtiene el n� de petici�n que permite construir el n� Ref y el codTecAsist
    posicion = InStr(msg, "|ci") + 3
    nPeticion = Mid$(msg, posicion, InStr(posicion, msg, "|") - posicion)
    nRef = "B19" & Left$(nPeticion, 9) 'HAY QUE VER COMO RECONSTRUIR EL N� DE REFERENCIA
    cTecAsist = Val(Right$(nPeticion, 3))
    
    'se comprueba que la prueba est� programada y por tanto, a la espera de recibir resultados
    numPruebasProgramadas = objGen.GetRowCount(objDetailProgramadas.rdoCursor)
    If numPruebasProgramadas > 0 Then
        objDetailProgramadas.rdoCursor.MoveFirst
        For i = 1 To numPruebasProgramadas
            If objDetailProgramadas.rdoCursor("nRef") = nRef And objDetailProgramadas.rdoCursor("MB20_codTecAsist") = cTecAsist Then
                Exit For
            End If
            objDetailProgramadas.rdoCursor.MoveNext
        Next i
        If i > numPruebasProgramadas Then
            Exit Sub
        End If
    Else
        Exit Sub
    End If
       
    'se elimina la parte de mensaje ya procesada para dejar s�lo los resultados
    posicion = InStr(msg, "|rt")
    If posicion > 0 Then
        msg = Mid$(msg, posicion)
    Else 'si no hay resultados, no se hace nada
        Exit Sub
    End If
    
    'se anotan los resultados en la matriz arVitekTec
    Do While msg <> ""
        'se obtiene el trozo de mensaje correspondiente a una �nica t�cnica (mensaje-t�cnica)
        posicion = InStr(4, msg, "|rt")
        If posicion = 0 Then
            msgTec = msg
        Else
            msgTec = Mid$(msg, 1, posicion - 1)
        End If
        'se quita del mensaje original el mensaje-t�cnica que se va a procesar
        msg = Mid$(msg, Len(msgTec) + 1)
        
        'se realiza un formateo previo del msgTec
        If Left$(msgTec, 1) = "|" Then msgTec = Mid$(msgTec, 2)
        If Right$(msgTec, 1) <> "|" Then msgTec = msgTec & "|"
        
        'se busca la t�cnica, microorganismo, bion�mero y antibi�ticos en el mensaje-t�cnica
        dimTec = dimTec + 1
        dimAntib = 0
        ReDim Preserve arVitekTec(1 To dimTec)
        Do While InStr(msgTec, "|") > 0
            campo = Left$(msgTec, 2) 'identificaci�n del campo
            texto = Mid$(msgTec, 3, InStr(msgTec, "|") - 3) 'valor devuelto por Vitek
            Select Case campo
                Case "rt" 't�cnica
                    arVitekTec(dimTec).cTec = texto
'                    'SE PUEDEN FILTRAR LOS TEST QUE REALMENTE INTERESAN. HAY QUE VER SI _
'                    ES MEJOR UTILIZAR PARA ELLO EL CAMPO T2
'                    Select Case UCase(texto)
'                        Case UCase("uid-1"), UCase("gms") 't�cnicas que no interesan
'                            msgTec = "" 'con esto salimos del bucle
'                        Case Else
'                            arVitekTec(dimTec).cTec = texto
'                    End Select
                Case "t1" 'colonia
                    arVitekTec(dimTec).cCol = texto
                Case "t2" 'c�digo del grupo de test
                    Select Case Val(texto)
                        Case constTGCVitekID, constTGCVitekSusc 'Vitek ID y Vitek Susc.
                            arVitekTec(dimTec).cGrupoTest = texto
                        Case Else
                            dimTec = dimTec - 1
                            ReDim Preserve arVitekTec(1 To dimTec) 'se quita el registro que se estaba almacenando
                            msgTec = "" 'se sale del bucle
                    End Select
                Case "o1" 'microorganismo
                    arVitekTec(dimTec).cMicro = texto
                Case "o3" 'bion�mero
                    arVitekTec(dimTec).bionum = texto
                Case "a1" 'antibi�tico
                    dimAntib = dimAntib + 1
                    ReDim Preserve arVitekTec(dimTec).antib(1 To dimAntib)
                    arVitekTec(dimTec).antib(dimAntib).cAnti = texto
                Case "a3" 'concentraci�n
                    'HAY QUE FORMATEAR LA CONCENTRACI�N QUE PUEDE TENER LOS S�MBOLOS _
                    <= o >=. VER SI SE PUEDEN DAR OTROS CASOS
                    texto = objGen.ReplaceStr(texto, ">=", "", 0)
                    texto = objGen.ReplaceStr(texto, "<=", "", 0)
                    arVitekTec(dimTec).antib(dimAntib).conc = texto
                Case "a4" 'sensibilidad
                    'si este campo est� vacio, no se trata de un antibi�tico sino del _
                    resultado de una t�cnica, que no se va a tener en cuenta
                    Select Case UCase(texto)
                        Case UCase(constAntibVitekS)
                            texto = constAntibS
                        Case UCase(constAntibVitekR)
                            texto = constAntibR
                        Case UCase(constAntibVitekMS)
                            texto = constAntibMS
                        Case Else
                            texto = "" 'no se tiene en cuenta el antibi�tico
                    End Select
                    If texto = "" Then
                        dimAntib = dimAntib - 1
                        ReDim Preserve arVitekTec(dimTec).antib(1 To dimAntib)
                    Else
                        arVitekTec(dimTec).antib(dimAntib).sensib = texto
                    End If
            End Select
            msgTec = Mid$(msgTec, InStr(msgTec, "|") + 1) 'se elimina del mensaje-t�cnica el campo procesado
        Loop
    Loop
    
    'se insertan los resultados en la Base de Datos
    If dimTec > 0 Then
        Call pInsertarResultadosBD(nRef, cTecAsist, arVitekTec)
    End If
    
End Sub
Private Sub pInsertarResultadosBD(nRef$, cTecAsist$, arRes() As typeVitekTec)
'inserta los resultados le�dos del Vitek en la base de datos
    
    Dim SQL As String
    Dim qryResult As rdoQuery
    Dim qryCol As rdoQuery
    Dim qryAntib As rdoQuery
    Dim i, j As Integer
    Dim dimension As Integer
    
    'se crean los statements de insercci�n y actualizaci�n
    SQL = "UPDATE MB2700 "
    SQL = SQL & " SET MB18_codMicro = ?"
    SQL = SQL & " , MB27_fecIdent = TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB27_codCol = ?"
    Set qryCol = objApp.rdoConnect.CreateQuery("", SQL)
    
    SQL = "UPDATE MB3300"
    SQL = SQL & " SET MB33_result = ?"
    SQL = SQL & " WHERE nRef = ?"
    SQL = SQL & " AND MB20_codTecAsist = ?"
    SQL = SQL & " AND MB09_codTec = ?"
    SQL = SQL & " AND MB32_codResult = ?"
    Set qryResult = objApp.rdoConnect.CreateQuery("", SQL)
        
    SQL = "INSERT INTO MB2900 (nRef, MB20_codTecAsist, MB27_codCol, MB07_codAnti, MB40_codReac, MB29_conc)"
    SQL = SQL & " VALUES (?,?,?,?,?,?)"
    Set qryAntib = objApp.rdoConnect.CreateQuery("", SQL)
    
    On Error Resume Next
    objApp.rdoConnect.BeginTrans
        
    'se procesan los registros de la matriz de resultados
    For i = 1 To UBound(arRes)
        'se actualiza la tabla de colonias con la identificaci�n del microorganismo
        qryCol(0) = arRes(i).cMicro
        qryCol(1) = fFechaHoraActual
        qryCol(2) = nRef
        qryCol(3) = arRes(i).cCol
        qryCol.Execute
        If Err <> 0 Then GoTo label
        
        'se actualiza la tabla de resultados con el bion�mero (si el test es del tipo Vitek ID)
        If arRes(i).cGrupoTest = constTGCVitekID Then
            qryResult(0) = arRes(i).bionum
            qryResult(1) = nRef
            qryResult(2) = cTecAsist
            qryResult(3) = arRes(i).cTec
            qryResult(4) = 1
            qryResult.Execute
            If Err <> 0 Then GoTo label
        End If
        
        'se a�aden los antibi�ticos al antibiograma (si el test es del tipo Vitek Susceptibility)
        If arRes(i).cGrupoTest = constTGCVitekSusc Then
            qryAntib(0) = nRef
            qryAntib(1) = cTecAsist
            qryAntib(2) = arRes(i).cCol
            dimension = UBound(arRes(i).antib)
            If Err = 9 Then Err = 0
            If dimension > 0 Then
                For j = 1 To dimension
                    qryAntib(3) = arRes(i).antib(j).cAnti
                    qryAntib(4) = arRes(i).antib(j).sensib
                    qryAntib(5) = arRes(i).antib(j).conc
                    qryAntib.Execute
                    If Err <> 0 Then GoTo label
                Next j
            End If
        End If
    Next i
    
        
label:
    If Err = 0 Then
        'se actualiza el estado de la t�cnica en la tabla t�cnica-asistencia. El cursor _
        ya se ubic� en el registro correcto en pProcesarResultados
        objDetailProgramadas.rdoCursor.Edit
        objDetailProgramadas.rdoCursor("MB34_codEstTecAsist") = constTECREALIZADA
        objDetailProgramadas.rdoCursor.Update
        objWinInfo.DataRefresh
        objApp.rdoConnect.CommitTrans
    Else
        objApp.rdoConnect.RollbackTrans
    End If
    
End Sub
Private Sub cmdProgramar_Click()
    
    Screen.MousePointer = vbHourglass
    
    filas = objGen.GetRowCount(objDetailPendientes.rdoCursor)
    estado = constESPERA
    If filas > 0 Then
        Label1.Caption = "Programando"
        Label1.Visible = True
        numRetry = 0 'se inicializa el valor del n� de reintentos
        tipoMensaje = constMPR 'indica el tipo de mensaje a enviar
        Call pEstadosProgramarEnviar
    Else
        Screen.MousePointer = vbDefault
        MsgBox "No existe ninguna prueba para programar en el autoanalizador.", vbExclamation, "Programaci�n Vitek"
    End If
    
End Sub
Private Sub pEstadosProgramarEnviar()
'Se utiliza para todos los env�os de mensajes del sistema al Vitek.

'Mensaje OOS (fuera de servicio): se env�a al cargar la pantalla para asegurarse de que _
    el Vitek no va a enviar resultados hasta que no se le pida

'Mensaje MPR (demogr�ficos): se env�an al pulsar el bot�n Programar

'Mensaje BIS (vuelta al servicio): se env�an al pulsar el bot�n Recibir para indicar al _
    Vitek que el sistema est� dispuesto para recibir resultados
 
    Dim tiempo As Long 'PARA PROBAR RESPUESTA VITEK
    
    Select Case estado
        Case constESPERA
            'se active el timer de los TimeOuts
            TimerTimeOut.Enabled = True
            'se intenta establecer la comunicaci�n con el Vitek
            MSComm1.Output = Chr$(5) '<ENQ>
            estado = constSOLICITUD
'            tiempo = Timer 'PARA PROBAR RESPUESTA VITEK
'            Do While tiempo + 3 > Timer 'PARA PROBAR RESPUESTA VITEK
'                DoEvents 'PARA PROBAR RESPUESTA VITEK
'            Loop 'PARA PROBAR RESPUESTA VITEK
'            Call pEstadosProgramarRecibir(Chr$(6)) 'PARA PROBAR RESPUESTA VITEK
'            Call pEstadosProgramarRecibir(Chr$(22)) 'PARA PROBAR RESPUESTA VITEK
        Case constMASTER
            'se active el timer de los TimeOuts
            TimerTimeOut.Enabled = True
            'se transmite el mensaje
            MSComm1.Output = fMensaje
            estado = constCONFIRMACION
'            tiempo = Timer 'PARA PROBAR RESPUESTA VITEK
'            Do While tiempo + 3 > Timer 'PARA PROBAR RESPUESTA VITEK
'                DoEvents 'PARA PROBAR RESPUESTA VITEK
'            Loop 'PARA PROBAR RESPUESTA VITEK
'            Call pEstadosProgramarRecibir(Chr$(6)) 'PARA PROBAR RESPUESTA VITEK
'            Call pEstadosProgramarRecibir(Chr$(22)) 'PARA PROBAR RESPUESTA VITEK
        Case constFINAL
            'se active el timer de los TimeOuts
            TimerTimeOut.Enabled = False
            'se finaliza la comunicaci�n con el Vitek
            MSComm1.Output = Chr$(4) '<EOT>
            estado = constESPERA
            Select Case tipoMensaje
                Case constOOS
                    'cuando el Vitek acepta el 'oos', ya se puede comenzar a programar demogr�ficos
                    Screen.MousePointer = vbDefault
                    cmdProgramar.Enabled = True
                Case constBIS
                    'cuando el Vitek acepta el 'bis', ya se puede comenzar a recibir resultados
                    Label1.Caption = "Recibiendo"
                Case constMPR
                    'se actualiza la BD y se quita el registro de pantalla
                    objDetailPendientes.rdoCursor.Edit
                    objDetailPendientes.rdoCursor("MB34_codEstTecAsist") = constTECREALIZANDOSE
                    objDetailPendientes.rdoCursor.Update
                    objWinInfo.DataRefresh
                    'se pasa a procesar la siguiente prueba
                    filas = filas - 1
                    If filas > 0 Then
                        Call pEstadosProgramarEnviar
                    Else
                        Label1.Visible = False
                        Screen.MousePointer = vbDefault
                    End If
            End Select
            
    End Select
    
End Sub




Private Sub cmdRecibir_Click()

    Label1.Caption = "bis"
    tipoMensaje = constBIS 'mensaje de 'vuelta al servicio'
    Call pEstadosProgramarEnviar
    
End Sub

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub Command1_Click()
    Dim msg As String
    
    msg = Chr$(2)
    msg = msg & Chr$(30) & "mtrsl|pi3546787|pb01/01|psSEX|p1COD|p2LOC|"
    msg = msg & Chr$(30) & "si|ssCOD|s5SRC|stCOD|s103/03|"
    msg = msg & Chr$(30) & "ci970000018011|ctCOD|cnTYP|c105/05/05|c23:03|c3F|c4FINAL|"
    msg = msg & Chr$(30) & "rt1|t11|rr1|rnGram Negative Identificaction|t21|o11|o2ORGNAM|" _
              & Chr$(30) & "o3897657655|o4COD2|o5ORGMODNAM|"
    msg = msg & Chr$(30) & "rt2|t11|rr2|rnGram Negative Susceptibily -F1|t23|o11|o2ORGNAM|" _
              & Chr$(30) & "a11|a2Amikacin|a34|a4S|a12|a2Cephalothin|a332|a4R|a1oxid|a2Oxidase|" _
              & Chr$(30) & "a3Neg|a4|"
    msg = msg & Chr$(30) & "rt3|t11|rr3|rnGram Negative Susceptibily -F2|t23|o11|o2ORGNAM|" _
              & Chr$(30) & "a14|a2Tetracycline|a31|a4MS|a15|a2Ticarcillin/CA|a316|a4R|a1oxid|a2Oxidase|" _
              & Chr$(30) & "a3Neg|a4|"
    msg = msg & Chr$(29) & "3c" & Chr$(3)
    Call pProcesarResultados(msg)
    
End Sub

Private Sub Form_Activate()

    Screen.MousePointer = vbHourglass
    If fConfig_IO(constVITEK, MSComm1) = True Then 'se configuran los par�metros de comunicaci�n
        TimerTimeOut.Interval = constTimeOut * 1000 'tiempo de los TimeOuts en el Timer
        estado = constESPERA 'estado inicial del sitema en la comunicaci�n
        cmdProgramar.Enabled = False 'no se activa el bot�n hasta que el Vitek ha aceptado el 'oos'
        MSComm1.PortOpen = True
        tipoMensaje = constOOS 'mensaje inicial ('fuera de servicio')
        Call pEstadosProgramarEnviar
    Else
        Screen.MousePointer = vbDefault
        MsgBox "No se han podido establecer los par�metros de comunicaci�n.", vbExclamation, "Configuraci�n I/O"
        Unload Me
    End If
    
End Sub

Private Sub Form_Load()
  Dim strKey As String
  Dim SQL As String
  
  Screen.MousePointer = vbHourglass 'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithoutNothing)
  
  With objMasterInfo 'es necesario un maestro para tener luego dos detalles al mismo nivel
    .strName = "Master"
    Set .objFormContainer = fraFrame2(0)
    Set .objFatherContainer = Nothing
    .intFormModel = cwWithoutGrid + cwWithoutTab
    .strTable = "autoanalizadores"
    .strWhere = "cAutoanalizador = " & constVITEK
    .intAllowance = cwAllowReadOnly
    .blnAskPrimary = False
  End With
    
  With objDetailPendientes
    .strName = "Pendientes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = fraFrame2(0)
    Set .grdGrid = grdDBGrid1(0)
    .intAllowance = cwAllowModify
    'vista
    .strTable = "MB2001J"
    SQL = "cAutoanalizador = " & constVITEK
    SQL = SQL & " AND MB20_tiOrig = " & constORIGENCOLONIA
    SQL = SQL & " AND MB34_codEstTecAsist = " & constTECSOLICITAREALI
    .strWhere = SQL
    Call .FormAddRelation("cAutoanalizador", Text1(0))
    Call .FormAddOrderField("nRef", cwAscending)
    Call .FormAddOrderField("MB20_codTecAsist", cwAscending)
    .blnHasMaint = False
    .blnAskPrimary = False
    
  End With
  
  With objDetailProgramadas
    .strName = "Programadas"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame2(0)
    Set .grdGrid = grdDBGrid1(1)
    .intAllowance = cwAllowAdd
    'vista
    .strTable = "MB2001J"
    SQL = "cAutoanalizador = " & constVITEK
    SQL = SQL & " AND MB20_tiOrig = " & constORIGENCOLONIA
    SQL = SQL & " AND MB34_codEstTecAsist = " & constTECREALIZANDOSE
    .strWhere = SQL
    Call .FormAddRelation("cAutoanalizador", Text1(0))
    Call .FormAddOrderField("nRef", cwAscending)
    Call .FormAddOrderField("MB20_codTecAsist", cwAscending)
    .blnHasMaint = False
    .blnAskPrimary = False
    
  End With
   
  With objDetailRecibidas
    .strName = "Recibidas"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame2(0)
    Set .grdGrid = grdDBGrid1(2)
    .intAllowance = cwAllowAdd
    'vista
    .strTable = "MB2001J"
    SQL = "cAutoanalizador = " & constVITEK
    SQL = SQL & " AND MB20_tiOrig = " & constORIGENCOLONIA
    SQL = SQL & " AND MB34_codEstTecAsist = " & constTECREALIZADA
    .strWhere = SQL
    Call .FormAddRelation("cAutoanalizador", Text1(0))
    Call .FormAddOrderField("nRef", cwAscending)
    Call .FormAddOrderField("MB20_codTecAsist", cwAscending)
    .blnHasMaint = False
    .blnAskPrimary = False
    
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    
    Call .FormAddInfo(objDetailPendientes, cwFormMultiLine)
    Call .GridAddColumn(objDetailPendientes, "nRef", "nRef", cwString, 12)
    Call .GridAddColumn(objDetailPendientes, "N� Ref.", "", cwNumeric, 7)
    Call .GridAddColumn(objDetailPendientes, "MB20_codTecAsist", "MB20_codTecAsist", cwString, 3)
    Call .GridAddColumn(objDetailPendientes, "Colonia", "MB20_codOrig", cwNumeric, 2)
    Call .GridAddColumn(objDetailPendientes, "N� Placa", "MB20_codPlaca", cwNumeric, 3)
    Call .GridAddColumn(objDetailPendientes, "N� Colonia", "", cwString, 10)
    Call .GridAddColumn(objDetailPendientes, "T�cnica", "MB09_desig", cwString, 20)
    Call .GridAddColumn(objDetailPendientes, "Estado", "MB34_codEstTecAsist", cwNumeric, 1)
    Call .GridAddColumn(objDetailPendientes, "historia", "historia", cwNumeric, 7)
     
    Call .FormAddInfo(objDetailProgramadas, cwFormMultiLine)
    Call .GridAddColumn(objDetailProgramadas, "nRef", "nRef", cwString, 12)
    Call .GridAddColumn(objDetailProgramadas, "N� Ref.", "", cwNumeric, 7)
    Call .GridAddColumn(objDetailProgramadas, "MB20_codTecAsist", "MB20_codTecAsist", cwString, 3)
    Call .GridAddColumn(objDetailProgramadas, "Colonia", "MB20_codOrig", cwNumeric, 2)
    Call .GridAddColumn(objDetailProgramadas, "N� Placa", "MB20_codPlaca", cwNumeric, 3)
    Call .GridAddColumn(objDetailProgramadas, "N� Colonia", "", cwString, 10)
    Call .GridAddColumn(objDetailProgramadas, "T�cnica", "MB09_desig", cwString, 20)
    Call .GridAddColumn(objDetailProgramadas, "Estado", "MB34_codEstTecAsist", cwNumeric, 1)
    Call .GridAddColumn(objDetailProgramadas, "historia", "historia", cwNumeric, 7)
    
    Call .FormAddInfo(objDetailRecibidas, cwFormMultiLine)
    Call .GridAddColumn(objDetailRecibidas, "nRef", "nRef", cwString, 12)
    Call .GridAddColumn(objDetailRecibidas, "N� Ref.", "", cwNumeric, 7)
    Call .GridAddColumn(objDetailRecibidas, "MB20_codTecAsist", "MB20_codTecAsist", cwString, 3)
    Call .GridAddColumn(objDetailRecibidas, "Colonia", "MB20_codOrig", cwNumeric, 2)
    Call .GridAddColumn(objDetailRecibidas, "N� Placa", "MB20_codPlaca", cwNumeric, 3)
    Call .GridAddColumn(objDetailRecibidas, "N� Colonia", "", cwString, 10)
    Call .GridAddColumn(objDetailRecibidas, "T�cnica", "MB09_desig", cwString, 20)
    Call .GridAddColumn(objDetailRecibidas, "Estado", "MB34_codEstTecAsist", cwNumeric, 1)
    Call .GridAddColumn(objDetailRecibidas, "historia", "historia", cwNumeric, 7)

    Call .FormCreateInfo(objMasterInfo)
    
    Call .FormChangeColor(objDetailPendientes)
    'grdDBGrid1(0).Columns(0).Visible = False
    grdDBGrid1(0).Columns(3).Visible = False
    .CtrlGetInfo(grdDBGrid1(0).Columns(4)).blnReadOnly = True
    grdDBGrid1(0).Columns(5).Visible = False
    grdDBGrid1(0).Columns(6).Visible = False
    grdDBGrid1(0).Columns(7).Visible = False
    .CtrlGetInfo(grdDBGrid1(0).Columns(8)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(9)).blnReadOnly = True
    grdDBGrid1(0).Columns(10).Visible = False
    grdDBGrid1(0).Columns(11).Visible = False
    
    Call .FormChangeColor(objDetailProgramadas)
    'grdDBGrid1(1).Columns(0).Visible = False
    grdDBGrid1(1).Columns(3).Visible = False
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnReadOnly = True
    grdDBGrid1(1).Columns(5).Visible = False
    grdDBGrid1(1).Columns(6).Visible = False
    grdDBGrid1(1).Columns(7).Visible = False
    .CtrlGetInfo(grdDBGrid1(1).Columns(8)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(9)).blnReadOnly = True
    grdDBGrid1(1).Columns(10).Visible = False
    grdDBGrid1(1).Columns(11).Visible = False
    
    Call .FormChangeColor(objDetailRecibidas)
    'grdDBGrid1(2).Columns(0).Visible = False
    grdDBGrid1(2).Columns(3).Visible = False
    .CtrlGetInfo(grdDBGrid1(2).Columns(4)).blnReadOnly = True
    grdDBGrid1(2).Columns(5).Visible = False
    grdDBGrid1(2).Columns(6).Visible = False
    grdDBGrid1(2).Columns(7).Visible = False
    .CtrlGetInfo(grdDBGrid1(2).Columns(8)).blnReadOnly = True
    .CtrlGetInfo(grdDBGrid1(2).Columns(9)).blnReadOnly = True
    grdDBGrid1(2).Columns(10).Visible = False
    grdDBGrid1(2).Columns(11).Visible = False
   
    Call .WinRegister
    Call .WinStabilize
  End With

  fraFrame2(0).Visible = False
  Screen.MousePointer = vbDefault 'Call objApp.SplashOff
End Sub



Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  If MSComm1.PortOpen = True Then
      MSComm1.InBufferCount = 0
      MSComm1.OutBufferCount = 0
      MSComm1.PortOpen = False
  End If
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
    'se formatean las columnas del grig
    grdDBGrid1(Index).Columns(4).Text = Val(Mid$(grdDBGrid1(Index).Columns(3).Text, 6))
    grdDBGrid1(Index).Columns(8).Text = grdDBGrid1(Index).Columns(6).Text & " (P" & grdDBGrid1(Index).Columns(7).Text & ")"
End Sub

Private Sub MSComm1_OnComm()
    
    Dim ERMsg$
    Dim caracter As String * 1

    'Se selecciona el evento que ocurre en el puerto
    Select Case MSComm1.CommEvent
        'Si se produce una entrada se procede a su lectura
        'Se lee cada caracter por separado
        Case MSCOMM_EV_RECEIVE
            MSComm1.RThreshold = 0
            Do While MSComm1.InBufferCount
                DoEvents
                caracter = MSComm1.Input
                Select Case Label1.Caption
                    Case "Programando", "bis"
                        'el caption "bis" se pone mientras se env�a el mensaje 'bis' antes _
                        'de recibir
                        Call pEstadosProgramarRecibir(caracter)
                    Case "Recibiendo"
                        Call pEstadosResultadosRecibir(caracter)
                End Select
            Loop
            MSComm1.RThreshold = 1
        Case MSCOMM_ER_RXOVER
            ERMsg$ = "Buffer de entada de datos lleno."
        Case MSCOMM_ER_TXFULL
            ERMsg$ = "Buffer de salida de datos lleno."
    End Select
    
    If Len(ERMsg$) Then
        MsgBox ERMsg$, vbExclamation, "Error de comunicaci�n"
        ERMsg$ = ""
    End If
    
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)

'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  Select Case strFormName
'    Case objDetailPendientes.strName
'        Call objWinInfo.FormPrinterDialog(True, "")
'        Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'        intReport = objPrinter.Selected
'        If intReport > 0 Then
'          blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'          Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                     objWinInfo.DataGetOrder(blnHasFilter, True))
'        End If
'        Set objPrinter = Nothing
'
'  End Select

End Sub

Private Sub stbStatusBar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub TimerTimeOut_Timer()
'al producirse un TimeOut no se producen reenv�os sino que se deja el sistema en estado _
inicial por si el usuario reintenta programar

'NOTA: VER SI HAY QUE ENVIAR EL <EOT> O NO ES NECESARIO
    
    numRetry = 0
    estado = constESPERA
    Screen.MousePointer = vbDefault
    If tipoMensaje = constOOS Then
        MsgBox "El autoanalizador no contesta. Salir e intentarlo de nuevo.", vbExclamation, "Comunicaci�n Vitek"
    ElseIf tipoMensaje = constMPR Then
        Label1.Visible = False
        MsgBox "El autoanalizador no contesta. Intentarlo de nuevo.", vbExclamation, "Comunicaci�n Vitek"
    End If
    TimerTimeOut.Enabled = False
    
End Sub















Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
'  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
'  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
'  Call objWinInfo.CtrlDataChange
End Sub


Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub












