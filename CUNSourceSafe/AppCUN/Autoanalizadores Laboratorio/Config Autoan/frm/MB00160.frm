VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Begin VB.Form frmI_Informes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   7575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8370
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7575
   ScaleWidth      =   8370
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   495
      Left            =   5760
      TabIndex        =   1
      Top             =   7020
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   495
      Left            =   7080
      TabIndex        =   0
      Top             =   7020
      Width           =   1215
   End
   Begin Threed.SSPanel pnlLista 
      Height          =   3375
      Index           =   0
      Left            =   60
      TabIndex        =   2
      Top             =   60
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   5953
      _StockProps     =   15
      Caption         =   "0"
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin MSRDC.MSRDC msrdcLista 
         Height          =   330
         Index           =   0
         Left            =   240
         Top             =   3060
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   582
         _Version        =   327681
         Options         =   0
         CursorDriver    =   1
         BOFAction       =   0
         EOFAction       =   0
         RecordsetType   =   1
         LockType        =   1
         QueryType       =   0
         Prompt          =   3
         Appearance      =   1
         QueryTimeout    =   30
         RowsetSize      =   100
         LoginTimeout    =   15
         KeysetSize      =   0
         MaxRows         =   0
         ErrorThreshold  =   -1
         BatchSize       =   15
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Enabled         =   -1  'True
         ReadOnly        =   -1  'True
         Appearance      =   -1  'True
         DataSourceName  =   ""
         RecordSource    =   ""
         UserName        =   ""
         Password        =   ""
         Connect         =   ""
         LogMessages     =   ""
         Caption         =   "MSRDC1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdssLista 
         Bindings        =   "MB00160.frx":0000
         Height          =   2895
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   300
         Width           =   2775
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Selected"
         stylesets(0).ForeColor=   -2147483634
         stylesets(0).BackColor=   -2147483635
         stylesets(0).Picture=   "MB00160.frx":0018
         DividerType     =   0
         CheckBox3D      =   0   'False
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   4895
         _ExtentY        =   5106
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   1515
      Index           =   3
      Left            =   6300
      TabIndex        =   4
      Top             =   60
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   2672
      _StockProps     =   15
      Caption         =   "Fechas entre ..."
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin SSCalendarWidgets_A.SSDateCombo cbossFecha 
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   1515
         _Version        =   65537
         _ExtentX        =   2672
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpinButton      =   0
         AllowEdit       =   0   'False
         StartofWeek     =   2
      End
      Begin SSCalendarWidgets_A.SSDateCombo cbossFecha 
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   6
         Top             =   1020
         Width           =   1515
         _Version        =   65537
         _ExtentX        =   2672
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpinButton      =   0
         AllowEdit       =   0   'False
         StartofWeek     =   2
      End
      Begin VB.Label Label1 
         Caption         =   "y"
         Height          =   195
         Left            =   60
         TabIndex        =   7
         Top             =   720
         Width           =   315
      End
   End
   Begin Threed.SSPanel pnlLista 
      Height          =   3375
      Index           =   1
      Left            =   3180
      TabIndex        =   8
      Top             =   60
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   5953
      _StockProps     =   15
      Caption         =   "1"
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin MSRDC.MSRDC msrdcLista 
         Height          =   330
         Index           =   1
         Left            =   300
         Top             =   3060
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   582
         _Version        =   327681
         Options         =   0
         CursorDriver    =   0
         BOFAction       =   0
         EOFAction       =   0
         RecordsetType   =   1
         LockType        =   3
         QueryType       =   0
         Prompt          =   3
         Appearance      =   1
         QueryTimeout    =   30
         RowsetSize      =   100
         LoginTimeout    =   15
         KeysetSize      =   0
         MaxRows         =   0
         ErrorThreshold  =   -1
         BatchSize       =   15
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Enabled         =   -1  'True
         ReadOnly        =   0   'False
         Appearance      =   -1  'True
         DataSourceName  =   ""
         RecordSource    =   ""
         UserName        =   ""
         Password        =   ""
         Connect         =   ""
         LogMessages     =   ""
         Caption         =   "MSRDC1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdssLista 
         Bindings        =   "MB00160.frx":0034
         Height          =   2895
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   300
         Width           =   2775
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Selected"
         stylesets(0).ForeColor=   -2147483634
         stylesets(0).BackColor=   -2147483635
         stylesets(0).Picture=   "MB00160.frx":004C
         DividerType     =   0
         CheckBox3D      =   0   'False
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   4895
         _ExtentY        =   5106
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin Threed.SSPanel pnlLista 
      Height          =   3375
      Index           =   2
      Left            =   60
      TabIndex        =   9
      Top             =   3540
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   5953
      _StockProps     =   15
      Caption         =   "2"
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin MSRDC.MSRDC msrdcLista 
         Height          =   330
         Index           =   2
         Left            =   180
         Top             =   3000
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   582
         _Version        =   327681
         Options         =   0
         CursorDriver    =   0
         BOFAction       =   0
         EOFAction       =   0
         RecordsetType   =   1
         LockType        =   3
         QueryType       =   0
         Prompt          =   3
         Appearance      =   1
         QueryTimeout    =   30
         RowsetSize      =   100
         LoginTimeout    =   15
         KeysetSize      =   0
         MaxRows         =   0
         ErrorThreshold  =   -1
         BatchSize       =   15
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Enabled         =   -1  'True
         ReadOnly        =   0   'False
         Appearance      =   -1  'True
         DataSourceName  =   ""
         RecordSource    =   ""
         UserName        =   ""
         Password        =   ""
         Connect         =   ""
         LogMessages     =   ""
         Caption         =   "MSRDC1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdssLista 
         Bindings        =   "MB00160.frx":0068
         Height          =   2895
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   300
         Width           =   2775
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Selected"
         stylesets(0).ForeColor=   -2147483634
         stylesets(0).BackColor=   -2147483635
         stylesets(0).Picture=   "MB00160.frx":0080
         DividerType     =   0
         CheckBox3D      =   0   'False
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   4895
         _ExtentY        =   5106
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin Threed.SSPanel pnlLista 
      Height          =   3375
      Index           =   3
      Left            =   3180
      TabIndex        =   10
      Top             =   3540
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   5953
      _StockProps     =   15
      Caption         =   "3"
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   0
      Begin MSRDC.MSRDC msrdcLista 
         Height          =   330
         Index           =   3
         Left            =   360
         Top             =   3060
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   582
         _Version        =   327681
         Options         =   0
         CursorDriver    =   0
         BOFAction       =   0
         EOFAction       =   0
         RecordsetType   =   1
         LockType        =   3
         QueryType       =   0
         Prompt          =   3
         Appearance      =   1
         QueryTimeout    =   30
         RowsetSize      =   100
         LoginTimeout    =   15
         KeysetSize      =   0
         MaxRows         =   0
         ErrorThreshold  =   -1
         BatchSize       =   15
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         Enabled         =   -1  'True
         ReadOnly        =   0   'False
         Appearance      =   -1  'True
         DataSourceName  =   ""
         RecordSource    =   ""
         UserName        =   ""
         Password        =   ""
         Connect         =   ""
         LogMessages     =   ""
         Caption         =   "MSRDC1"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin SSDataWidgets_B.SSDBGrid grdssLista 
         Bindings        =   "MB00160.frx":009C
         Height          =   2895
         Index           =   3
         Left            =   120
         TabIndex        =   13
         Top             =   300
         Width           =   2775
         _Version        =   131078
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ColumnHeaders   =   0   'False
         stylesets.count =   1
         stylesets(0).Name=   "Selected"
         stylesets(0).ForeColor=   -2147483634
         stylesets(0).BackColor=   -2147483635
         stylesets(0).Picture=   "MB00160.frx":00B4
         DividerType     =   0
         CheckBox3D      =   0   'False
         AllowUpdate     =   0   'False
         AllowRowSizing  =   0   'False
         AllowGroupSizing=   0   'False
         AllowColumnSizing=   0   'False
         AllowGroupMoving=   0   'False
         AllowColumnMoving=   0
         AllowGroupSwapping=   0   'False
         AllowColumnSwapping=   0
         AllowGroupShrinking=   0   'False
         AllowColumnShrinking=   0   'False
         AllowDragDrop   =   0   'False
         SelectTypeCol   =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         CaptionAlignment=   0
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   0   'False
         _ExtentX        =   4895
         _ExtentY        =   5106
         _StockProps     =   79
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmI_Informes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim tipoInforme As Integer
Dim arListas() As Integer
Dim rptCrystal As CrystalReport

Private Const constListaDpt = 1
Private Const constListaDr = 2
Private Const constListaGenero = 3
Private Const constListaMicro = 4
Private Const constListaProblema = 5
Private Const constListaAlmacenamiento = 6
Private Const constNoLista = 0

Private Const constControl = 2  ' Tipo de departamento = control de planta
Private Const constNLista = 4
Private Const constDistanciaEntreObjetos = 105
Private Sub pImprimirInformeBionumeros()
    ' Imprime el informe estad�stico de bionumeros
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim cGenero As String
    Dim cMicro As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
    
    ' Se mira cu�l es la lista de g�neros
    nLista = fBuscarLista(constListaGenero)
    ' Se obtiene la selecci�n de g�neros
    res = fGenerosSeleccionados(nLista, cGenero)
    
    ' Se mira cu�l es la lista de microorganismos
    nLista = fBuscarLista(constListaMicro)
    res = fMicrosSeleccionados(nLista, cMicro)
    
    rptCrystal.ReportFileName = rptPath & "\mb00107.rpt"
    
    SQL = "{MB2700.MB27_FECIDENT}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2700.MB27_FECIDENT}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    If cGenero <> "" Or cMicro <> "" Then
        SQL = SQL & " AND ("
        If cGenero <> "" Then
            SQL = SQL & " {MB1800.MB11_CODGeMicro} IN [" & cGenero & "] OR "
        End If
        If cMicro <> "" Then
            SQL = SQL & " {MB2700.MB18_CODMicro} IN [" & cMicro & "] "
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    'A�ADIDO
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"

End Sub

Private Sub pImprimirInformeTecnicas()
    ' Imprime el informe estad�stico de microorganismo - t�cnicas
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim cGenero As String
    Dim cMicro As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
    
    ' Se mira cu�l es la lista de g�neros
    nLista = fBuscarLista(constListaGenero)
    ' Se obtiene la selecci�n de g�neros
    res = fGenerosSeleccionados(nLista, cGenero)
    
    ' Se mira cu�l es la lista de microorganismos
    nLista = fBuscarLista(constListaMicro)
    res = fMicrosSeleccionados(nLista, cMicro)
    
    rptCrystal.ReportFileName = rptPath & "\mb00106.rpt"
    
    SQL = "{MB2700.MB27_FECIDENT}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2700.MB27_FECIDENT}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    If cGenero <> "" Or cMicro <> "" Then
        SQL = SQL & " AND ("
        If cGenero <> "" Then
            SQL = SQL & " {MB1800.MB11_CODGeMicro} IN [" & cGenero & "] OR "
        End If
        If cMicro <> "" Then
            SQL = SQL & " {MB2700.MB18_CODMicro} IN [" & cMicro & "] "
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    'A�ADIDO
    SQL = SQL & " AND NOT ISNULL({MB3300.MB33_result})"
    SQL = SQL & " AND {MB0900.MB04_codTiTec}>= " & constBIOQUIMICAS
    SQL = SQL & " AND ({MB2700.MB36_CODESTCOL} = " & constCOLVALIDADA & " OR {MB2700.MB36_CODESTCOL} = " & constCOLINFORMADA & ")"
    SQL = SQL & " AND {MB2000.MB34_CODESTTECASIST} = " & constTECVALIDADA
    SQL = SQL & " AND ({MB3300.MB38_CODESTRESULT} = " & constRESULTVALIDADO & " OR {MB3300.MB38_CODESTRESULT} = " & constRESULTINFORMADO & ")"
    SQL = SQL & " AND {MB2000.MB20_TIORIG} = " & constORIGENCOLONIA
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"

End Sub

Private Sub pCargarLista(nLista%)
    ' Se carga cada una de las listas

    Select Case arListas(nLista)
        Case constListaDpt
            msrdcLista(nLista).SQL = "SELECT cDpt,desig,cTiDpt FROM dpt ORDER BY cTiDpt,desig"
        Case constListaDr
            msrdcLista(nLista).SQL = "SELECT cDr,upper(NVL(txtFirma,AP1||' '||AP2||', '||NOM)) as doctor FROM dr ORDER BY ap1,ap2,nom"
        Case constListaGenero
            msrdcLista(nLista).SQL = "SELECT MB11_CODGeMicro,MB11_Desig FROM MB1100 WHERE MB08_CODTiMic IN(SELECT MB08_CODTiMic FROM MB0800 WHERE cDptoSecc = " & departamento & ") ORDER BY MB11_desig"
        Case constListaMicro
            msrdcLista(nLista).SQL = "SELECT MB18_CODMicro,MB18_Desig FROM MB1800 WHERE MB11_CODGeMicro IN(SELECT MB11_CODGeMicro FROM MB1100,MB0800 WHERE MB1100.MB08_CODTiMic=MB0800.MB08_CODTiMic AND cDptoSecc = " & departamento & ") ORDER BY MB18_Desig"
        Case constListaProblema
            msrdcLista(nLista).SQL = "SELECT MB22_CODProbl,MB22_Desig FROM MB2200 ORDER BY MB22_Desig"
        Case constListaAlmacenamiento
            msrdcLista(nLista).SQL = "SELECT MB19_CODMotivo, MB19_Desig FROM MB1900 ORDER BY MB19_Desig"
        Case constNoLista
            msrdcLista(nLista).SQL = ""
    End Select
End Sub

Private Sub pFormatearGrid(nLista%)
        Select Case arListas(nLista)
        Case constListaDpt
            pnlLista(nLista).Caption = "Departamentos/Controles"
            grdssLista(nLista).Columns(0).Visible = False
            grdssLista(nLista).Columns(1).Visible = True
            grdssLista(nLista).Columns(2).Visible = False
            pnlLista(nLista).Visible = True
        Case constListaDr
            pnlLista(nLista).Caption = "Doctores"
            grdssLista(nLista).Columns(0).Visible = False
            grdssLista(nLista).Columns(1).Visible = True
            pnlLista(nLista).Visible = True
        Case constListaGenero
            pnlLista(nLista).Caption = "G�neros de Microorganismos"
            grdssLista(nLista).Columns(0).Visible = False
            grdssLista(nLista).Columns(1).Visible = True
            pnlLista(nLista).Visible = True
        Case constListaMicro
            pnlLista(nLista).Caption = "Microorganismos"
            grdssLista(nLista).Columns(0).Visible = False
            grdssLista(nLista).Columns(1).Visible = True
            pnlLista(nLista).Visible = True
        Case constListaProblema
            pnlLista(nLista).Caption = "Tipos de Problemas"
            grdssLista(nLista).Columns(0).Visible = False
            grdssLista(nLista).Columns(1).Visible = True
            pnlLista(nLista).Visible = True
        Case constListaAlmacenamiento
            pnlLista(nLista).Caption = "Motivo de Almacenamiento"
            grdssLista(nLista).Columns(0).Visible = False
            grdssLista(nLista).Columns(1).Visible = True
            pnlLista(nLista).Visible = True
        Case constNoLista
            pnlLista(nLista).Visible = False
    End Select

End Sub


Sub pFormatearVentana()
    ' Se cargan los tipos de listas a mostrar
    
    Dim i As Integer
    
    For i = 0 To constNLista - 1
        If arListas(i) <> constNoLista Then
            msrdcLista(i).Connect = objApp.rdoConnect.Connect
            Call pCargarLista(i)
            msrdcLista(i).Refresh
        ElseIf i = 2 Then
            ' Si la lista 3 est� vac�a, se formatea el formulario
            ' para que quede m�s presentable
            cmdAceptar.Top = pnlLista(0).Top + pnlLista(0).Height + constDistanciaEntreObjetos
            cmdCancelar.Top = cmdAceptar.Top
            Me.Height = 4500
        End If
    Next i

End Sub

Private Sub pImprimirInformeAislamientos()
    ' Imprime el informe estad�stico de aislamientos
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim cGenero As String
    Dim cMicro As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
    
    ' Se mira cu�l es la lista de g�neros
    nLista = fBuscarLista(constListaGenero)
    ' Se obtiene la selecci�n de g�neros
    res = fGenerosSeleccionados(nLista, cGenero)
    
    ' Se mira cu�l es la lista de microorganismos
    nLista = fBuscarLista(constListaMicro)
    res = fMicrosSeleccionados(nLista, cMicro)
    
    rptCrystal.ReportFileName = rptPath & "\mb00104.rpt"
    
    SQL = "{MB2700.MB27_FECDESAR}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2700.MB27_FECDESAR}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    If cGenero <> "" Or cMicro <> "" Then
        SQL = SQL & " AND ("
        If cGenero <> "" Then
            SQL = SQL & " {MB1800.MB11_CODGeMicro} IN [" & cGenero & "] OR "
        End If
        If cMicro <> "" Then
            SQL = SQL & " {MB2700.MB18_CODMicro} IN [" & cMicro & "] "
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    'A�ADIDO
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"

End Sub

Private Sub pImprimirInformeDesglose()
    ' Imprime el informe estad�stico de aislamientos
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
        
    rptCrystal.ReportFileName = rptPath & "\mb00108.rpt"
    
    SQL = "{SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.FECHA} >=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.FECHA}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    SQL = SQL & " AND {SEGPRUEBA.PROCESO} = " & constPROCESOVALIDACION
    SQL = SQL & " AND NOT ISNULL({PRUEBAASISTENCIA.NREF})"
    SQL = SQL & " AND {PRUEBAASISTENCIA.ESTADO} <> " & constPRUEBAANULADA
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"

End Sub

Private Sub pImprimirInformeAntibiogramas()
    ' Imprime el informe estad�stico de microorganismo - antibiograma
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim cGenero As String
    Dim cMicro As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
    
    ' Se mira cu�l es la lista de g�neros
    nLista = fBuscarLista(constListaGenero)
    ' Se obtiene la selecci�n de g�neros
    res = fGenerosSeleccionados(nLista, cGenero)
    
    ' Se mira cu�l es la lista de microorganismos
    nLista = fBuscarLista(constListaMicro)
    res = fMicrosSeleccionados(nLista, cMicro)
    
    rptCrystal.ReportFileName = rptPath & "\mb00105.rpt"
    
    SQL = "{MB2700.MB27_FECIDENT}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2700.MB27_FECIDENT}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    If cGenero <> "" Or cMicro <> "" Then
        SQL = SQL & " AND ("
        If cGenero <> "" Then
            SQL = SQL & " {MB1800.MB11_CODGeMicro} IN [" & cGenero & "] OR "
        End If
        If cMicro <> "" Then
            SQL = SQL & " {MB2700.MB18_CODMicro} IN [" & cMicro & "] "
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    'A�ADIDO
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"

End Sub

Private Sub pImprimirInformeEdades()
    ' Imprime el informe estad�stico de edades del paciente - microorganismos
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim cGenero As String
    Dim cMicro As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
    
    ' Se mira cu�l es la lista de g�neros
    nLista = fBuscarLista(constListaGenero)
    ' Se obtiene la selecci�n de g�neros
    res = fGenerosSeleccionados(nLista, cGenero)
    
    ' Se mira cu�l es la lista de microorganismos
    nLista = fBuscarLista(constListaMicro)
    res = fMicrosSeleccionados(nLista, cMicro)
    
    rptCrystal.ReportFileName = rptPath & "\mb00109.rpt"
    
    SQL = "{MB2700.MB27_FECIDENT}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2700.MB27_FECIDENT}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    If cGenero <> "" Or cMicro <> "" Then
        SQL = SQL & " AND ("
        If cGenero <> "" Then
            SQL = SQL & " {MB1800.MB11_CODGeMicro} IN [" & cGenero & "] OR "
        End If
        If cMicro <> "" Then
            SQL = SQL & " {MB2700.MB18_CODMicro} IN [" & cMicro & "] "
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
        SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    'A�ADIDO
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"
    
End Sub

Private Sub pImprimirInformeColonias()
    ' Imprime el informe estad�stico de colonias
    ' almacenadas
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cGenero As String
    Dim cMicro As String
    Dim cMotivo As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de g�neros
    nLista = fBuscarLista(constListaGenero)
    ' Se obtiene la selecci�n de g�neros
    res = fGenerosSeleccionados(nLista, cGenero)
    
    ' Se mira cu�l es la lista de microorganismos
    nLista = fBuscarLista(constListaMicro)
    res = fMicrosSeleccionados(nLista, cMicro)
        
    ' Se mira cu�l es la lista de motivos de almacenamiento
    nLista = fBuscarLista(constListaAlmacenamiento)
    res = fAlmacenamientosSeleccionados(nLista, cMotivo)
    
    rptCrystal.ReportFileName = rptPath & "\mb00103.rpt"
    
    SQL = "{MB2700.MB27_FECALM}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2700.MB27_FECALM}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cMotivo <> "" Then
        SQL = SQL & " AND {MB2700.MB19_CODMotivo} IN [" & cMotivo & "] OR "
    End If
    If cGenero <> "" Or cMicro <> "" Then
        SQL = SQL & " AND ("
        If cGenero <> "" Then
            SQL = SQL & " {MB1800.MB11_CODGeMicro} IN [" & cGenero & "] OR "
        End If
        If cMicro <> "" Then
            SQL = SQL & " {MB2700.MB18_CODMicro} IN [" & cMicro & "] "
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"

End Sub

Private Sub pImprimirInformeMuestras()
    ' Imprime el informe estad�stico de Microorganismos
    ' detectados en cada tipo de muestra
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim cProbl As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
        
    rptCrystal.ReportFileName = rptPath & "\mb00102.rpt"
    
    SQL = "{MB2700.MB27_FECIDENT}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2700.MB27_FECIDENT}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    'A�ADIDO
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD

    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"
End Sub

Sub pImprimirInformeProblemas()
    ' Imprime el informe estad�stico de problemas
    
    ' Si no hay nada seleccionado, se supone que quiere imprimir todo
    
    Dim nLista As Integer
    Dim cDpt As String
    Dim cControl As String
    Dim cDr As String
    Dim cProbl As String
    Dim res As Integer
    Dim SQL As String

    ' Se mira cu�l es la lista de departamentos
    nLista = fBuscarLista(constListaDpt)
    ' Se obtiene la selecci�n de departamentos
    ' Se diferencian departamentos de controles
    res = fDepartamentosSeleccionados(nLista, cDpt, cControl)
    
    ' Se mira cu�l es la lista de doctores
    nLista = fBuscarLista(constListaDr)
    res = fDoctoresSeleccionados(nLista, cDr)
    
    ' Se mira cu�l es la lista de problemas
    nLista = fBuscarLista(constListaProblema)
    res = fProblemasSeleccionados(nLista, cProbl)
    
    rptCrystal.ReportFileName = rptPath & "\mb00101.rpt"
    
    SQL = "{MB2800.MB28_FECPROBL}>=" & fFechaCrystal(cbossFecha(0).Text)
    SQL = SQL & " AND {MB2800.MB28_FECPROBL}<" & fFechaCrystal(DateAdd("d", 1, cbossFecha(1).Text))
    If cProbl <> "" Then
        SQL = SQL & " AND {MB2800.MB22_CODPROBL} IN [" & cProbl & "] "
    End If
    If cDpt <> "" Or cControl <> "" Or cDr <> "" Then
        SQL = SQL & " AND ("
        If cDpt <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.dptoSolicitud} IN [" & cDpt & "] OR "
        End If
        If cControl <> "" Then
            SQL = SQL & " {SEGUIMIENTOPRUEBA.respExterno} like [" & cControl & "] OR "
        End If
        If cDr <> "" Then
            SQL = SQL & " {PRUEBAASISTENCIA.drSolicitud} IN [" & cDr & "]"
        End If
        If Right(SQL, 3) = "OR " Then SQL = Left(SQL, Len(SQL) - 3)
        SQL = SQL & ")"
    End If
    SQL = SQL & " AND {carpetas.cdptoSecc}=" & departamento
    'A�ADIDO
    SQL = SQL & " AND {SEGUIMIENTOPRUEBA.PROCESO} = " & constPROCESOSOLICITUD
    rptCrystal.SelectionFormula = SQL
    rptCrystal.Formulas(0) = "fechasTitulo = '" & cbossFecha(0).Text & " - " & cbossFecha(1).Text & "'"

End Sub
Private Function fProblemasSeleccionados(nLista As Integer, cProbl As String) As Boolean
    ' Recoge de la lista de problemas los seleccionados
    Dim bkmrk As Variant    ' Bookmark de acceso a los datos de las listas
    Dim i As Integer
    
    For i = 0 To grdssLista(nLista).SelBookmarks.Count - 1
        bkmrk = grdssLista(nLista).SelBookmarks(i)
        cProbl = cProbl & grdssLista(nLista).Columns(0).CellValue(bkmrk) & ","
    Next i
    If cProbl <> "" Then
        If Right(cProbl, 1) = "," Then cProbl = Left(cProbl, Len(cProbl) - 1)
    End If
    
    fProblemasSeleccionados = True

End Function
Private Function fDoctoresSeleccionados(nLista As Integer, cDr As String) As Boolean
    ' Recoge de la lista de problemas los seleccionados
    Dim bkmrk As Variant    ' Bookmark de acceso a los datos de las listas
    Dim i As Integer
    
    For i = 0 To grdssLista(nLista).SelBookmarks.Count - 1
        bkmrk = grdssLista(nLista).SelBookmarks(i)
        cDr = cDr & "'" & grdssLista(nLista).Columns(0).CellValue(bkmrk) & "',"
    Next i
    If cDr <> "" Then
        If Right(cDr, 1) = "," Then cDr = Left(cDr, Len(cDr) - 1)
    End If
    
    fDoctoresSeleccionados = True

End Function

Private Function fAlmacenamientosSeleccionados(nLista As Integer, cMotivo As String) As Boolean
    ' Recoge de la lista de motivos de almacenamiento los seleccionados
    Dim bkmrk As Variant    ' Bookmark de acceso a los datos de las listas
    Dim i As Integer
    
    For i = 0 To grdssLista(nLista).SelBookmarks.Count - 1
        bkmrk = grdssLista(nLista).SelBookmarks(i)
        cMotivo = cMotivo & grdssLista(nLista).Columns(0).CellValue(bkmrk) & ","
    Next i
    If cMotivo <> "" Then
        If Right(cMotivo, 1) = "," Then cMotivo = Left(cMotivo, Len(cMotivo) - 1)
    End If
    
    fAlmacenamientosSeleccionados = True
    
End Function
Private Function fMicrosSeleccionados(nLista As Integer, cMicro As String) As Boolean
    ' Recoge de la lista de Microorganismos los seleccionados
    Dim bkmrk As Variant    ' Bookmark de acceso a los datos de las listas
    Dim i As Integer
    
    For i = 0 To grdssLista(nLista).SelBookmarks.Count - 1
        bkmrk = grdssLista(nLista).SelBookmarks(i)
        cMicro = cMicro & grdssLista(nLista).Columns(0).CellValue(bkmrk) & ","
    Next i
    If cMicro <> "" Then
        If Right(cMicro, 1) = "," Then cMicro = Left(cMicro, Len(cMicro) - 1)
    End If
    
    fMicrosSeleccionados = True
    
End Function

Private Function fGenerosSeleccionados(nLista As Integer, cGenero As String) As Boolean
    ' Recoge de la lista de G�neros los seleccionados
    Dim bkmrk As Variant    ' Bookmark de acceso a los datos de las listas
    Dim i As Integer
    
    For i = 0 To grdssLista(nLista).SelBookmarks.Count - 1
        bkmrk = grdssLista(nLista).SelBookmarks(i)
        cGenero = cGenero & grdssLista(nLista).Columns(0).CellValue(bkmrk) & ","
    Next i
    If cGenero <> "" Then
        If Right(cGenero, 1) = "," Then cGenero = Left(cGenero, Len(cGenero) - 1)
    End If
    
    fGenerosSeleccionados = True
    
End Function

Private Function fDepartamentosSeleccionados(nLista As Integer, cDpt As String, cControl As String) As Boolean
    ' Recoge de la lista de departamentos los seleccionados
    ' Se diferencian los departamentos de los controles de planta
    Dim bkmrk As Variant    ' Bookmark de acceso a los datos de las listas
    Dim i As Integer
    
    For i = 0 To grdssLista(nLista).SelBookmarks.Count - 1
        bkmrk = grdssLista(nLista).SelBookmarks(i)
        If Val(grdssLista(nLista).Columns(2).CellValue(bkmrk)) = constControl Then
            cControl = cControl & "'" & grdssLista(nLista).Columns(0).CellValue(bkmrk) & "*',"
        Else
            cDpt = cDpt & grdssLista(nLista).Columns(0).CellValue(bkmrk) & ","
        End If
    Next i
    If cDpt <> "" Then
        If Right(cDpt, 1) = "," Then cDpt = Left(cDpt, Len(cDpt) - 1)
    End If
    If cControl <> "" Then
        If Right(cControl, 1) = "," Then cControl = Left(cControl, Len(cControl) - 1)
    End If
    
    fDepartamentosSeleccionados = True
    
End Function

Private Function fBuscarLista(tipoLista%) As Integer
    Dim i As Integer
    
    For i = 0 To constNLista
        If arListas(i) = tipoLista Then
            fBuscarLista = i
            Exit For
        End If
    Next i
End Function
Private Sub pListas()
    Select Case tipoInforme
        Case constINFProblemas
            arListas(0) = constListaDpt
            arListas(1) = constListaDr
            arListas(2) = constListaProblema
            arListas(3) = constNoLista
            Me.Caption = "Listado de Problemas"
        Case constINFColonias
            arListas(0) = constListaGenero
            arListas(1) = constListaMicro
            arListas(2) = constListaAlmacenamiento
            arListas(3) = constNoLista
            Me.Caption = "Listado de Almacenamiento de Colonias"
        Case constINFMuestras
            arListas(0) = constListaDpt
            arListas(1) = constListaDr
            arListas(2) = constNoLista
            arListas(3) = constNoLista
            Me.Caption = "Listado de Microorganismo-Muestra"
        Case constINFAislamientos
            arListas(0) = constListaGenero
            arListas(1) = constListaMicro
            arListas(2) = constListaDpt
            arListas(3) = constListaDr
            Me.Caption = "Listado de Aislamientos"
        Case constINFAntibiogramas
            arListas(0) = constListaGenero
            arListas(1) = constListaMicro
            arListas(2) = constListaDpt
            arListas(3) = constListaDr
            Me.Caption = "Listado de Antibiogramas"
        Case constINFTecnicas
            arListas(0) = constListaGenero
            arListas(1) = constListaMicro
            arListas(2) = constListaDpt
            arListas(3) = constListaDr
            Me.Caption = "Listado de Microorganismo - T�cnica"
        Case constINFBionumeros
            arListas(0) = constListaGenero
            arListas(1) = constListaMicro
            arListas(2) = constListaDpt
            arListas(3) = constListaDr
            Me.Caption = "Listado de Microorganismo - Bion�mero"
        Case constINFCrecimientos
            arListas(0) = constListaGenero
            arListas(1) = constListaMicro
            arListas(2) = constNoLista
            arListas(3) = constNoLista
            Me.Caption = "Listado de Microorganismo - Crecimiento"
        Case constINFEdades
            arListas(0) = constListaGenero
            arListas(1) = constListaMicro
            arListas(2) = constListaDpt
            arListas(3) = constListaDr
            Me.Caption = "Listado de Microorganismo - Edad del Paciente"
        Case constINFDesglose
            arListas(0) = constListaDpt
            arListas(1) = constListaDr
            arListas(2) = constNoLista
            arListas(3) = constNoLista
            Me.Caption = "Listado de Desglose Mensual de Peticiones"
    End Select
        
End Sub

Private Sub cbossFecha_CloseUp(Index As Integer)
    If CVDate(cbossFecha(0).Text) > CVDate(cbossFecha(1).Text) Then
        Select Case Index
            Case 0
                cbossFecha(0).Text = cbossFecha(1).Text
            Case 1
                cbossFecha(1).Text = cbossFecha(0).Text
        End Select
    End If

End Sub

Private Sub cbossFecha_Change(Index As Integer)
    If CVDate(cbossFecha(0).Text) > CVDate(cbossFecha(1).Text) Then
        Select Case Index
            Case 0
                cbossFecha(0).Text = cbossFecha(1).Text
            Case 1
                cbossFecha(1).Text = cbossFecha(0).Text
        End Select
    End If
End Sub

Private Sub cmdAceptar_Click()
    Dim res As Integer
    
    
    Select Case tipoInforme
        Case constINFProblemas
            pImprimirInformeProblemas
        Case constINFColonias
            pImprimirInformeColonias
        Case constINFMuestras
            pImprimirInformeMuestras
        Case constINFAislamientos
            pImprimirInformeAislamientos
        Case constINFEdades
            pImprimirInformeEdades
        Case constINFAntibiogramas
            pImprimirInformeAntibiogramas
        Case constINFTecnicas
            pImprimirInformeTecnicas
        Case constINFBionumeros
            pImprimirInformeBionumeros
        Case constINFDesglose
            pImprimirInformeDesglose
    End Select
    
    If rptCrystal.Connect = "" Then
        rptCrystal.Connect = objApp.rdoConnect.Connect
'        rptCrystal.Connect = "DSN=Oracle7_MB;UID=MB;PWD=Enero;DSQ=prod.cun"
'        rptCrystal.Connect = "DSN=Oracle7_Micro;UID=Micro;PWD=Micro;DSQ=desa.cun"
    End If
    rptCrystal.WindowTitle = Me.Caption
    
    res = rptCrystal.PrintReport
    If res <> 0 Then
        rptCrystal.Action = 1
    End If
    
    rptCrystal.SelectionFormula = ""
    rptCrystal.WindowTitle = ""
    rptCrystal.ReportFileName = ""
    rptCrystal.Formulas(0) = ""
End Sub

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Dim fecha As String
    
    Screen.MousePointer = vbHourglass 'Call objApp.SplashOn

    tipoInforme = objPipe.PipeGet("informe")
    ReDim arListas(0 To constNLista - 1)
    pListas
    Call pFormatearVentana
    
    fecha = fFechaActual()
    cbossFecha(0).Text = Format$(DateAdd("m", -1, CVDate(fecha)), "dd/mm/yyyy")
    cbossFecha(0).MaxDate = fecha
    cbossFecha(0).MinDate = "1/1/1997"
    cbossFecha(1).Text = fecha
    cbossFecha(1).MaxDate = fecha
    cbossFecha(1).MinDate = "1/1/1997"

    Set rptCrystal = frmPrincipal.crCrystalReport1
    Screen.MousePointer = vbDefault 'Call objApp.SplashOff

End Sub

Private Sub grdssLista_InitColumnProps(intIndex As Integer)
    Call pFormatearGrid(intIndex)
End Sub


