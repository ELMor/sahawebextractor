Attribute VB_Name = "modSeguridad"
Public Function fAcceso(intFuncion%, Optional intCUser%) As Boolean
    ' Comprueba si se dispone de derechos de realizar la funci�n indicada
    Dim SQL As String
    Dim qryFuncion As rdoQuery
    Dim rsFuncion As rdoResultset
    
    If intCUser = 0 Then
        SQL = "SELECT cAcceso FROM perfilAcceso"
        SQL = SQL & " WHERE cPerfil=?"
        SQL = SQL & "   AND cAcceso = ?"

        Set qryFunciones = objApp.rdoConnect.CreateQuery("Seguridad", SQL)
        qryFunciones(0) = strPerfil
        qryFunciones(1) = intFuncion

    Else
        SQL = "SELECT cAcceso FROM perfilAcceso"
        SQL = SQL & " WHERE (cPerfil) IN "
        SQL = SQL & "       (SELECT cPerfil FROM usersPerfil"
        SQL = SQL & "        WHERE cUser = ?"
        SQL = SQL & "        AND cDptoSecc = ?"
        SQL = SQL & "       )"
        SQL = SQL & "   AND cAcceso=?"
        Set qryFunciones = objApp.rdoConnect.CreateQuery("Seguridad", SQL)
        qryFunciones(0) = strUser
        qryFunciones(1) = departamento
        qryFunciones(2) = intFuncion

    End If
    
    Set rsFunciones = qryFunciones.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If rsFunciones.EOF Then
        fAcceso = False
    Else
        fAcceso = True
    End If
    rsFunciones.Close
    qryFunciones.Close
End Function

Public Sub pAccesoConsultaResultados()
    On Error Resume Next
    If fAcceso(constCONSULTARESULTADOS) = False Then
        For i = 10 To 30
            frmPrincipal.mnuArchivoOpcion(i).Visible = False
        Next i
    Else
        For i = 10 To 30
            frmPrincipal.mnuArchivoOpcion(i).Visible = True
        Next i
    End If

End Sub

Public Sub pAccesoExplotacion()
    On Error Resume Next
    If fAcceso(constEXPLOTACION) = False Then
        For i = 20 To 120
            frmPrincipal.mnuInformesOpcion(i).Visible = False
        Next i
    Else
        For i = 20 To 120
            frmPrincipal.mnuInformesOpcion(i).Visible = True
        Next i
    End If
End Sub

Public Sub pAccesoMantenimientoSeccion()
    On Error Resume Next
    If fAcceso(constMANTENIMIENTOPRUEBAS) = False Then
        For i = 40 To 260
            frmPrincipal.mnuArchivoOpcion(i).Visible = False
        Next i
    Else
        For i = 40 To 260
            frmPrincipal.mnuArchivoOpcion(i).Visible = True
        Next i
    End If
End Sub
