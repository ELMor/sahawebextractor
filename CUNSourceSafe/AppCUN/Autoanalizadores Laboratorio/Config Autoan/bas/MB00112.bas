Attribute VB_Name = "modCrystalReport"
Option Explicit

Public Function fFechaCrystal(fecha As String) As String

    Dim fechaCrystal As String
    
    fechaCrystal = fecha
    If fechaCrystal = "" Then
        fechaCrystal = fFechaActual()
    End If
    fFechaCrystal = "DATE(" & Year(fechaCrystal) & "," & Month(fechaCrystal) & "," & Day(fechaCrystal) & ")"
    
End Function


