VERSION 5.00
Begin VB.Form frmBrowser 
   Caption         =   "Seleccione Ubicación de 'Import.seq'"
   ClientHeight    =   3750
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5895
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   3750
   ScaleWidth      =   5895
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtPath 
      Enabled         =   0   'False
      Height          =   315
      Left            =   0
      TabIndex        =   4
      Top             =   3420
      Width           =   4125
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Height          =   435
      Left            =   4290
      TabIndex        =   3
      Top             =   480
      Width           =   1515
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "&Cancelar"
      Height          =   435
      Left            =   4290
      TabIndex        =   2
      Top             =   1080
      Width           =   1515
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   30
      Width           =   4125
   End
   Begin VB.DirListBox Dir1 
      Height          =   3015
      Left            =   0
      TabIndex        =   0
      Top             =   390
      Width           =   4125
   End
End
Attribute VB_Name = "frmBrowser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim blnAceptado As Boolean

Private Sub cmdAceptar_Click()
  blnAceptado = True
  Me.Hide
End Sub

Private Sub cmdCancelar_Click()
  Me.Hide
End Sub

Private Sub Drive1_Change()
  Dir1.path = Drive1.Drive  ' Cuando cambie la unidad,
  ' establecer la ruta del directorio
End Sub

Private Sub Dir1_Change()
 'path = Dir1.path    ' Cuando cambie el directorio,
 txtPath = Dir1.path
End Sub

Public Function Mostrar(path As String) As Boolean
  Me.Show vbModal
  Mostrar = blnAceptado
  If Mostrar Then path = Dir1.path
  Unload Me
End Function

Private Sub Form_Load()
  Dir1.path = "c:"
End Sub
