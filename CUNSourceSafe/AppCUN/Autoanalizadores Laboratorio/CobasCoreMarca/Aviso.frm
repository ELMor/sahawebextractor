VERSION 5.00
Begin VB.Form frmAvisos 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Avisos Cobas Core"
   ClientHeight    =   2730
   ClientLeft      =   690
   ClientTop       =   1380
   ClientWidth     =   4275
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2730
   ScaleWidth      =   4275
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   180
      TabIndex        =   3
      Top             =   120
      Width           =   3915
      Begin VB.TextBox txtAvisos 
         Appearance      =   0  'Flat
         Height          =   1995
         Left            =   0
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   4
         Top             =   60
         Width           =   3915
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   180
      TabIndex        =   2
      Top             =   60
      Width           =   3915
      Begin VB.TextBox Text1 
         Appearance      =   0  'Flat
         Height          =   1995
         Left            =   0
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Top             =   60
         Visible         =   0   'False
         Width           =   3915
      End
   End
   Begin VB.CommandButton cmdBorrrar 
      Appearance      =   0  'Flat
      Caption         =   "&Borrar"
      Height          =   375
      Left            =   180
      TabIndex        =   1
      Top             =   2220
      Width           =   1395
   End
   Begin VB.CommandButton cmdCerrar 
      Appearance      =   0  'Flat
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   2700
      TabIndex        =   0
      Top             =   2220
      Width           =   1395
   End
End
Attribute VB_Name = "frmAvisos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdBorrrar_Click()
    txtAvisos.Text = ""
    TEXT1.Text = ""
End Sub

Private Sub cmdCerrar_Click()
    Unload frmAvisos
End Sub

Private Sub Form_Load()
    'ubicación de la pantalla de avisos
    Move frmPrincipal.Width, (frmPrincipal.Top + frmPrincipal.Height - frmAvisos.Height)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmAvisos.Tag = ""
    DoEvents
End Sub

