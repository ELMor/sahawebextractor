Attribute VB_Name = "Module1"
Option Explicit

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
Declare Function WritePrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFileName As String) As Integer

Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Enum cteDpto
  cteMarcadores = 1
  cteSerologia = 2
End Enum

Public Dpto As cteDpto

Public Sub Main()
On Error GoTo AutoAnError

  Dpto = cteMarcadores
'  Dpto = cteSerologia
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    frmPrincipal.Iniciar
    Select Case Dpto
      Case cteSerologia
        frmPrincipal.Caption = "Cobas Core(Serolog�a)"
        fCargarColumnasGridSerologia 'Definicion de las columnas que componen los 2 Grid
        If Not .fInicializar(App, cteCOBAS_CORE_SER, True, , , , , , , , True, True) Then End
      Case cteMarcadores
        frmPrincipal.Caption = "Cobas Core(Marcadores)"
        fCargarColumnasGridMarcadores
        If Not .fInicializar(App, cteCOBAS_CORE_MARC, True, , , , , , , , True, True) Then End
    End Select
     frmPrincipal.Mask.Text = Left$(frmPrincipal.pos, 6)
    .Show
  End With
   Screen.MousePointer = vbDefault
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(App, "Configurando objAutoan", "Sub Main()")
End Sub

Private Sub fCargarColumnasGridMarcadores()
On Error GoTo AutoAnError

  With objAutoAn
    'Columnas del Grid Muestras
    .AgregarColumnaAGridMuestras cteMuestraBANDEJA
    .AgregarColumnaAGridMuestras cteMuestraCOPA
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    '.AgregarColumnaAGridMuestras cteMuestraPROPERTY1, 800 'tiempo
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    'Columnas del Grid Pruebas-Resultados
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION, , InVisible
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoPROPERTY1, "Anterior1", , , , Izquierda
    .AgregarColumnaAGridPruebas cteResultadoPROPERTY2, "Anterior2", , , , Izquierda
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
  Exit Sub

AutoAnError:
    Call objError.InternalError(App, "AgregarColumnaAGridMuestras", "AgregarColumnaAGridMuestras")
End Sub

Private Sub fCargarColumnasGridSerologia()
On Error GoTo AutoAnError

  With objAutoAn
    'Columnas del Grid Muestras
    .AgregarColumnaAGridMuestras cteMuestraBANDEJA
    .AgregarColumnaAGridMuestras cteMuestraCOPA
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    '.AgregarColumnaAGridMuestras cteMuestraPROPERTY1, 800 'tiempo
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    'Columnas del Grid Pruebas-Resultados
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION, , InVisible
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoPROPERTY1, "Anterior1", , , , Izquierda
    .AgregarColumnaAGridPruebas cteResultadoPROPERTY2, "Anterior2", , , , Izquierda
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
  Exit Sub

AutoAnError:
    Call objError.InternalError(App, "AgregarColumnaAGridMuestras", "AgregarColumnaAGridMuestras")
End Sub

Private Sub Cargar_ResultadosAnteriores_Tiempos()
Dim qry1 As rdoQuery
Dim qry2 As rdoQuery
Dim rs1 As rdoResultset
Dim rs2 As rdoResultset
Dim objmuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim sql$, NumHistoria As Long, i%
On Error GoTo AutoAnError

  For Each objmuestra In objAutoAn.ColMuestras
    With objmuestra
      sql = "SELECT tiempo FROM muestraAsistencia WHERE "
      sql = sql & "cMuestra = ?"
      Set qry1 = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry1(0) = .strCodMuestra
      Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      .strProperty1 = IIf(IsNull(rs1(0)), "0", rs1(0)) 'tiempo
      rs1.Close: qry1.Close
      'se saca el n� Historia
      sql = "select ci22numhistoria from ci2200, pr0400 where"
      sql = sql & " ci2200.ci21codpersona = pr0400.ci21codpersona"
      sql = sql & " and pr0400.pr04numactplan = ?"
      Set qry1 = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry1(0) = CLng(.ColMuestrasPruebas(1).strNumAct)
      Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs1.EOF Then NumHistoria = rs1(0) Else GoTo NextMuestra
      .strProperty2 = NumHistoria
      rs1.Close: qry1.Close
      For Each objActuacion In objmuestra.ColMuestrasPruebas
        With objActuacion
          sql = "select ra.resultadoalfanumerico"
          sql = sql & " from resultadoasistencia ra, pr0400 pr04, ci2200 ci22"
          sql = sql & " where ra.pr04numactplan = pr04.pr04numactplan and"
          sql = sql & " pr04.ci21codpersona = ci22.ci21codpersona and"
          sql = sql & " ci22.ci22numhistoria = ? and"
          sql = sql & " pr04.pr01codactuacion = ?"
          sql = sql & " order by ra.fecha desc"
          Set qry2 = objAutoAn.rdoConnect.CreateQuery("", sql)
          qry2(0) = NumHistoria
          qry2(1) = .strCodAct
          Set rs2 = qry2.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
          i = 0
          Do While Not rs2.EOF And i < 2
            i = i + 1
            Select Case i
              Case 1: .strProperty1 = rs2("resultadoalfanumerico")
              Case 2: .strProperty2 = rs2("resultadoalfanumerico")
            End Select
            rs2.MoveNext
          Loop
          rs2.Close: qry2.Close
        End With
      Next
NextMuestra:
    End With
  Next
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(App, "AgregarColumnaAGridMuestras", "AgregarColumnaAGridMuestras")
End Sub
