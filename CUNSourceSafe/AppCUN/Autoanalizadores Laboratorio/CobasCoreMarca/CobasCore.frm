VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cobas Core"
   ClientHeight    =   1230
   ClientLeft      =   5535
   ClientTop       =   480
   ClientWidth     =   1965
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "CobasCore.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1230
   ScaleWidth      =   1965
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1560
      Top             =   240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSMask.MaskEdBox Mask 
      Height          =   315
      Left            =   840
      TabIndex        =   1
      Tag             =   "Posici�n Inicial"
      Top             =   720
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   556
      _Version        =   327681
      MaxLength       =   6
      Mask            =   "###/##"
      PromptChar      =   " "
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   240
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Timer TimerNuevasPruebas 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   0
      Top             =   720
   End
   Begin VB.Timer TimerRespuesta 
      Enabled         =   0   'False
      Interval        =   25000
      Left            =   420
      Top             =   720
   End
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   480
      Top             =   240
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk3 
      Left            =   960
      Top             =   240
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Posici�n Inicial:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   -30
      Width           =   1755
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim MatrizRef(1 To 16, 1 To 2) As Integer

'Variables
Dim bandejaComienzo As Integer      'Guarda la bandeja de comienzo de las muestras antes de ser cambiada
Dim copaComienzo As Integer         'Guarda la copa de comienzo de las muestras antes de ser cambiada
Dim UltimaBandeja As Integer        'Guarda la �ltima bandeja ocupada
Dim UltimaCopa As Integer           'Guarda la �ltima copa ocupada
Dim UltimoEnvio As Integer          'True: si no hay m�s env�os de prueba a programar
Dim TipoRespEsperada As Integer     'Ver constantes de respuesta
Dim NumeroEnvio As Integer          'N�mero de veces que se ha enviado el mismo mensaje
Dim dia As String

Public pos As String              'la pos inicial del mask
'Constantes de respuesta
Const constPROGRAMACION = 1
Const constRESULTADOS = 2
Const constArchivoIni = "c:\corBEGIN.txt"
'Const controldate = "c:\controld.txt"

Public Sub CargarMatrizRefs()
'tipo 1: >refmax: Positivo
'tipo 2: >refmax: Negativo
'tipo 3: >refmax: Positivo y entre Refmax-Refmin: Indeterminado
'tipo 4: >refmax: Negativo y entre Refmax-Refmin: Indeterminado
'tipo 5: >refmax: Negativo y entre Refmax-Refmin: Indeterminado

  MatrizRef(1, 1) = 579
  MatrizRef(1, 2) = 3
  
  MatrizRef(2, 1) = 56
  MatrizRef(2, 2) = 3
  
  MatrizRef(3, 1) = 575
  MatrizRef(3, 2) = 5
  
  MatrizRef(4, 1) = 585
  MatrizRef(4, 2) = 3
  
  MatrizRef(5, 1) = 549
  MatrizRef(5, 2) = 1
  
  MatrizRef(6, 1) = 57
  MatrizRef(6, 2) = 1
  
  MatrizRef(7, 1) = 987
  MatrizRef(7, 2) = 0
  
  MatrizRef(8, 1) = 397
  MatrizRef(8, 2) = 3
  
  MatrizRef(9, 1) = 393
  MatrizRef(9, 2) = 1
  
  MatrizRef(10, 1) = 398
  MatrizRef(10, 2) = 3
  
  MatrizRef(11, 1) = 396
  MatrizRef(11, 2) = 4
  
  MatrizRef(12, 1) = 394
  MatrizRef(12, 2) = 4
  
  MatrizRef(13, 1) = 395
  MatrizRef(13, 2) = 1
  
  MatrizRef(14, 1) = 391
  MatrizRef(14, 2) = 2
  
  MatrizRef(15, 1) = 392
  MatrizRef(15, 2) = 3
  
  MatrizRef(16, 1) = 399
  MatrizRef(16, 2) = 3


'  Select Case ctipo
'    Case 1: resultado = IIf(Val(resultado) > refmax, "Positivo", "Negativo")
'    Case 2: resultado = IIf(Val(resultado) > refmax, "Negativo", "Positivo")
'    Case 3
'      If Val(resultado) > refmax Then
'        resultado = "Positivo"
'      ElseIf Val(resultado) < refmin Then
'        resultado = "Negativo"
'      Else
'        resultado = "Indeterminado"
'      End If
'    Case 4
'      If Val(resultado) > refmax Then
'        resultado = "Negativo"
'      ElseIf Val(resultado) < refmin Then
'        resultado = "Positivo"
'      Else
'        resultado = "Indeterminado"
'      End If
'    Case 5
'      If Val(resultado) > refmax Then
'        resultado = "Inmune"
'      ElseIf Val(resultado) < refmin Then
'        resultado = "No Inmune"
'      Else
'        resultado = "Indeterminado"
'      End If
'  End Select
End Sub

Private Sub Bloque04(datos As String)
'Lee los mensajes que se refieren a la comunicaci�n de
'resultados por parte del autoanalizador.
Dim sql As String
Dim RES As Integer
Dim contador As Integer
Dim cMuestra As String      'string con c�digo muestra, T y D / string con T y D unicamente
Dim strMuestra As String
Dim cResultadoAuto As String
Dim intDilucion As Integer
Dim strResultado As String
Dim strCodResultado As String
Dim IntCodResultado As Integer
Dim strCodPrueba As String
Dim objResultado As clsResultado
Dim aviso As String
Dim filenum
On Error GoTo AutoAnError

  'Se separan las l�neas de resultados que env�a el autoanalizador
  Awk1 = datos
  Awk1.FS = Chr$(10)
  aviso = ""
  For contador = 1 To Awk1.NF - 1 'No se porqu� siempre sale un campo vacio dem�s
    Awk2 = Left$(Awk1.F(contador), Len(Awk1.F(contador)) - 1) 'se quita el <CR> inclu�do en el awk
    Awk2.FS = " "
    Select Case CInt(Awk2.F(1))
      Case 0
        'MatrizResultados(4) = Trim$(Awk2.F(2))
        strResultado = Trim$(Awk2.F(2))
        aviso = Trim$(Mid$(Awk1.F(contador), 16, 3))
      Case 11
        cMuestra = Trim$(Awk2.F(5))
        strMuestra = Left$(cMuestra, 8)
        'MatrizResultados(1) = Left$(cMuestra, 8)
        Awk3 = Mid$(cMuestra, 9)
        Awk3.FS = "D"
'                If Awk3.NF = 2 Then
'                    MatrizResultados(5) = Awk3.F(2)
'                Else
'                    MatrizResultados(5) = "1"
'                End If
        intDilucion = IIf(Awk3.NF = 2, Val(Awk3.F(2)), 1)
      Case 12
        'MatrizResultados(2) = Awk2.F(2)
        cResultadoAuto = Awk2.F(2)
    End Select
  Next contador
  
  If objAutoAn.blnExistMuestra(strMuestra) Then
    'Se anota el resultado afectado de la diluci�n
    If intDilucion <> 0 And intDilucion <> 1 Then _
    strResultado = CStr(Val(strResultado) * intDilucion)
    'sacamos el CodPrueba y CodResultado que corresponda al CResAuto de la Muestra
    With objAutoAn
      If .fCodPruebaConCResAuto(strMuestra, cResultadoAuto, strCodPrueba, IntCodResultado) Then
        Set objResultado = .ColMuestras(strMuestra).ColMuestrasPruebas(strCodPrueba).ColPruebasResultados(CStr(IntCodResultado))
        If .intCodAutoAn = cteCOBAS_CORE_SER Then strResultado = _
        ResultadoPosNegInd(objResultado, strResultado)
'        If .intCodAutoAn = cteCOBAS_CORE_SER Then strResultado = _
'        ResultadoPosNegInd(objResultado, CStr(IntCodResultado))
        '.ColMuestras(strMuestra).colmuestraspruebas(strCodPrueba).colpruebasresultados(intCodResultado).strResultado _
        = ResultadoPosNegInd(.ColMuestras(strMuestra).strProperty2, strCodPrueba, intCodResultado, strResultado)
        Call .pIntroResultConCResAuto(strMuestra, cResultadoAuto, strResultado, True)
      End If
    End With
  End If
  'Recogida de avisos.
  'If aviso <> "" Then
  '    If frmAvisos.txtAvisos.Text = "" Then
  '        frmAvisos.txtAvisos.Text = "Muestra: " & MatrizResultados(1)
  '    Else
  '        frmAvisos.txtAvisos.Text = frmAvisos.txtAvisos.Text & "Muestra: " & MatrizResultados(1)
  '    End If
  '    frmAvisos.txtAvisos.Text = frmAvisos.txtAvisos.Text & "     Aviso: " & aviso & Chr$(13) & Chr$(10)
  '    On Error Resume Next
  '    frmAvisos.Tag = "Visible"
  '    frmAvisos.Show MODELESS
  'End If

  
  '**** NO SE SALDR� AUTOM�TICAMENTE DEL PROGRAMA PORQUE LA LISTA ES DIN�MICA ****
  '**** Y SER� EL USUARIO EL QUE DECIDA CUANDO FINALIZAR                      ****
  'Si se han recibido todos los resultados se sale del programa
  'For contador = 1 To UBound(MatrizPruebas, 2)
  '    If MatrizPruebas(8, contador) = "AC" Then
  '        Exit For
  '    End If
  'Next contador
  'If contador > UBound(MatrizPruebas, 2) Then
  '    cmdSalir_Click
  'End If
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Bloque04", "Bloque04")
End Sub

Private Function ResultadoPosNegIndBAK(Historia As String, strCodPrueba As Integer, strCodResultado As Integer, strResultado As String) As String
'Dim rs As rdoResultset
'Dim qry As rdoQuery
'Dim cTipo As Integer
'On Error GoTo AutoAnError
'
'  ResultadoPosNegIndBAK = strResultado
'  cTipo = fTipo(strCodPrueba)
'  If cTipo <> 0 And IsNumeric(strResultado) Then
'    sql = "SELECT Refmin, Refmax FROM " & constPropietario & ".ValREF "
'    sql = sql & " Where historia = ?"
'    sql = sql & " AND cPrueba = ?"
'    sql = sql & " AND cResultado = ?"
'    Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
'    qry(0) = Historia
'    qry(1) = strCodPrueba
'    qry(2) = strCodResultado
'    Set rs = objAutoAn.rdoConnect.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'    If Not rs.EOF Then
'      refmin = rs("refmin"): refmax = rs("refmax")
'      If Val(strResultado) > refmax Then
'        Select Case cTipo
'          Case 1, 3
'            ResultadoPosNegInd = "Positivo"
'          Case 2, 4
'            ResultadoPosNegInd = "Negativo"
'          Case 5
'            ResultadoPosNegInd = "Inmune"
'        End Select
'      ElseIf Val(strResultado) < refmin Then
'        Select Case cTipo
'          Case 1, 3
'            ResultadoPosNegInd = "Negativo"
'          Case 2, 4
'            ResultadoPosNegInd = "Positivo"
'          Case 5
'            ResultadoPosNegInd = "No Inmune"
'        End Select
'      Else
'        Select Case cTipo
'          Case 1
'            ResultadoPosNegInd = "Negativo"
'          Case 2
'            ResultadoPosNegInd = "Positivo"
'          Case 3, 4, 5
'            ResultadoPosNegInd = "Indeterminado"
'        End Select
'      End If
'    End If
'  End If
'  Exit Function
'
'AutoAnError:
'    Call objError.InternalError(Me, "ResultadoPosNegInd", "strCodPrueba :" & strCodPrueba)
End Function

Private Function ResultadoPosNegInd(objResultado As clsResultado, strResultado As String) As String
Dim cTipo As Integer
On Error GoTo AutoAnError

  ResultadoPosNegInd = strResultado
  cTipo = fTipo(CInt(objResultado.Actuacion.strCodAct))
  If cTipo <> 0 And IsNumeric(strResultado) Then
    If Val(strResultado) > objResultado.DblRefMax Then
      Select Case cTipo
        Case 1, 3: ResultadoPosNegInd = "Positivo"
        Case 2, 4: ResultadoPosNegInd = "Negativo"
        Case 5: ResultadoPosNegInd = strResultado
'        Case 5: ResultadoPosNegInd = "Inmune"
      End Select
    ElseIf Val(strResultado) < objResultado.DblRefMin Then
      Select Case cTipo
        Case 1, 3: ResultadoPosNegInd = "Negativo"
        Case 2, 4: ResultadoPosNegInd = "Positivo"
        Case 5: ResultadoPosNegInd = strResultado
'        Case 5: ResultadoPosNegInd = "No Inmune"
      End Select
    Else
      Select Case cTipo
        Case 1: ResultadoPosNegInd = "Negativo"
        Case 2: ResultadoPosNegInd = "Positivo"
        Case 3, 4, 5: ResultadoPosNegInd = "Indeterminado"
      End Select
    End If
  Else
    ResultadoPosNegInd = strResultado
  End If
  
  Exit Function
  
AutoAnError:
    Call objError.InternalError(Me, "ResultadoPosNegInd", "Actuacion :" & objResultado.Actuacion.strCodAct)
End Function

Private Sub Bloque19(datos As String)
'Lee los mensajes que se refieren a la aceptaci�n
'de la programaci�n de pruebas para una muestra.
Dim contador1 As Integer
Dim T As Long
Dim RES As Integer
Dim filenum
Dim objActuacion As clsActuacion
Dim objmuestra As clsMuestra
On Error GoTo AutoAnError
    
  'Se chequea si el mensaje de programaci�n de pruebas ha sido aceptado o no.
  If Mid(datos, 4, 2) = 0 Then
    For Each objmuestra In objAutoAn.ColMuestras
      For Each objActuacion In objmuestra.ColMuestrasPruebas
        If objActuacion.strEstado = cteINTENTOPROGRAMAR Then
          objActuacion.strEstado = cteACEPTADA
        End If
      Next
    Next
    NumeroEnvio = 0
  Else
    'For contador1 = 1 To UBound(matrizpruebas, 2)
    For Each objmuestra In objAutoAn.ColMuestras
      For Each objActuacion In objmuestra.ColMuestrasPruebas
        If objActuacion.strEstado = cteINTENTOPROGRAMAR Then
          If NumeroEnvio = 5 Then
            objActuacion.strEstado = cteNOACEPTADA
          Else
            objActuacion.strEstado = cteSINPROGRAMAR
            UltimoEnvio = False
          End If
        End If
      Next
    Next 'contador1
    If NumeroEnvio = 5 Then NumeroEnvio = 0
  End If
'    cmdProgramar.Enabled = True
  'Se espera 1 segundo antes de enviar el nuevo mensaje si fuera necesario
  T = Timer + 1
  Do While T > Timer: RES = DoEvents(): Loop
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Bloque19", "Bloque19")
End Sub

Private Sub CobasCore_Espera(Mensaje As String)
'Identifica la llegada de los caracteres de solicitud de comunicaci�n y pasa al estado de Transmision.
  'On Error Resume Next
  If Left$(Mensaje, 3) = Chr$(1) & vbCrLf Then '<SOH><CR><LF> - Solicitud de comunicaci�n por parte del autoanalizador
    TimerRespuesta.Enabled = False
    objAutoAn.Estado = cteTRANSMISION
    Mensaje = "" 'Se elimina el <SOH><CR><LF> y los posibles caracteres enviados antes de establecerse la comunicaci�n
  End If
End Sub

Private Sub CobasCore_Transmision(Mensaje As String)
'Cuando se recibe el final de la transmisi�n se procede a la lectura de los datos y se
'pasa al estado de espera.
  'On Error Resume Next
  If Right$(Mensaje, 6) = Chr$(3) & vbCrLf & Chr$(4) & vbCrLf Then
    'se eliminan los caracteres <ETX><CR><LF><EOT><CR><LF>
    Mensaje = Left$(Mensaje, Len(Mensaje) - 6)
    'se lee el mensaje
    Lectura_Datos Mensaje
    objAutoAn.Estado = cteESPERA
    Mensaje = ""
  End If
End Sub

Public Sub Iniciar()
Dim Copa As Integer
Dim Bandeja As Integer
Dim filenum
Dim posic As String * 17
Dim Nu As Long, CONTAD As Long ', sql$, snp1 As Snapshot
Static FirstActivate As Boolean 'Se�ala si es la primera vez que el form se vuelve activo

On Error GoTo AutoAnError

  'Se lee la posicion de comienzo del carrusel
  Err = 0
  On Error Resume Next
  Open constArchivoIni For Binary As #1
  If Err = 76 Then
    'MsgBox "error 76"
    'MkDir "c:\archivos de programa\cun\lab"
    Open constArchivoIni For Binary As #1
  End If
  On Error GoTo AutoAnError
  Get #1, , posic
  Close #1
  'Hay 10 bandejas primarias con 15 posiciones cada una
  dia = Mid$(posic, 8)
  pos = posic
    'MsgBox "dia" & dia
  
  If dia = CStr(Format(Now, "dd/mm/yyyy")) Then
    Bandeja = Val(Left$(pos, 3))
    Copa = Val(Mid$(pos, 5, 2))
'    MsgBox "bandeja=" & Bandeja
'    MsgBox "copa=" & Copa
  Else
    Bandeja = 1
    Copa = 1
    'MsgBox "bandeja1"
    'MsgBox "copa1"
    'Se coloca el n�mero de copa pr�ximo
    UltimaCopa = UltimaCopa + 1
    pos = "001/01 " & CStr(Format(Now, "dd/mm/yyyy"))
    
    Open constArchivoIni For Binary As #1
    Put #1, , pos
    Close #1
    'MsgBox "fin"
  End If
  If Copa = 0 Then Copa = 1
  If Copa > 15 Then Copa = 1: Bandeja = Bandeja + 1
  If Bandeja = 0 Then Bandeja = 1
  If Bandeja > 10 Then Bandeja = 1
  'MsgBox "ini pos"
  pos = Left$("000", 3 - Len(CStr(Bandeja))) & Bandeja & "/" & Left$("00", 2 - Len(CStr(Copa))) & Copa
  'MsgBox "fin pos"
'  Mask.Text = Left$(pos, 6)
  bandejaComienzo = Bandeja
  copaComienzo = Copa
  UltimaBandeja = Bandeja
  UltimaCopa = Copa '- 1
  'se clasifican las pruebas por el tipo de resultado que daran
  'MsgBox "cargamatriz"
  CargarMatrizRefs
  'MsgBox "fin cargamatriz"
  'Mask_LostFocus
  objAutoAn.Borrar_Logs
  Screen.MousePointer = Default
  Exit Sub

AutoAnError:
    Call objError.InternalError(Me, "Iniciar frmPrincipal", "Iniciar frmPrincipal")
End Sub





Private Sub Form_Unload(Cancel As Integer)
Dim pos As String * 15
Dim filenum
On Error GoTo AutoAnError
  'Se coloca el n�mero de copa pr�ximo
  UltimaCopa = UltimaCopa + 1
  pos = Left$("000", 3 - Len(CStr(UltimaBandeja))) & UltimaBandeja & "/" & Left$("00", 2 - Len(CStr(UltimaCopa))) & UltimaCopa & " " & CStr(Format(Now, "dd/mm/yyyy"))
  Open constArchivoIni For Binary As #1
  Put #1, , pos
  Close #1
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Form_Unload", "Form_Unload")
End Sub

Private Function fTipo(cPrueba As Integer) As Integer
Dim i%
On Error GoTo AutoAnError
  fTipo = 0
  For i = 1 To UBound(MatrizRef, 1)
    If MatrizRef(i, 1) = cPrueba Then
      fTipo = MatrizRef(i, 2)
      Exit For
    End If
  Next
  Exit Function
  
AutoAnError:
    Call objError.InternalError(Me, "fTipo", "cPrueba: " & cPrueba)
End Function

Private Sub Lectura_Datos(datos As String)
'identifica el tipo de bloque transmitido
Dim NumBloque As Integer 'N�mero del bloque trasmitido
Dim resp As Integer
Dim sql As String
On Error GoTo AutoAnError

  NumBloque = Mid(datos, 21, 2)
  ' Datos = Mid$(Datos, 28) 'Se elimina el contenido del mensaje que no interesa
  datos = Mid$(datos, 28)
  Select Case NumBloque 'Se procede a la lectura de los datos
    Case 0 'Bloque de no exisencia de resultados disponibles
      'Se esperan 24 segundos antes de solicitar nuevos resultados
'      lblResultados.Visible = False
      '**** VER SI ES MEJOR PEDIR RESULTADOS CONTINUAMENTE O PULSANDO EL BOT�N *****
      'T = Timer + 24
      'Do While T > Timer
      '    resp = DoEvents()
      'Loop
      'cmdResultados_Click
    Case 4 'Bloque de resultados de muestras
      If TipoRespEsperada = constRESULTADOS Then
        Bloque04 datos
'        cmdResultados_Click
        Resultados
      End If
    Case 9 'Bloque de incorreci�n en la petici�n de resultados
      Select Case Val(Mid$(datos, 4, 2))
        Case 1, 2, 3
          sql = "El mensaje de petici�n de resultados no es correcto." & Chr$(10)
          sql = sql & "REINTENTAR --> Volver a solicitar resultados." & Chr$(10)
          sql = sql & "CANCELAR   --> Salir del programa (Hay que corregirlo)."
          resp = MsgBox(sql, vbQuestion + vbRetryCancel, frmPrincipal.Caption)
          Select Case resp
            Case vbRetry: Resultados
            Case vbCancel
'              lblResultados.Visible = False
              objAutoAn.Estado = cteESPERA
          End Select
        Case Else
          Resultados
      End Select
    Case 19 'Bloque de aceptaci�n de pruebas programadas
      If TipoRespEsperada = constPROGRAMACION Then
        Bloque19 datos
        If UltimoEnvio = False Then Programar 'cmdProgramar_Click
      End If
    Case 99 'NAK
      Select Case TipoRespEsperada
        Case constPROGRAMACION
          datos = "   02"
          Bloque19 datos   'Se supone que el NAK equivale a una respuesta con error del tipo de
                           'bloque n�mero 19
          If UltimoEnvio = False Then Programar 'cmdProgramar_Click
        Case constRESULTADOS
          sql = "El mensaje de petici�n de resultados no es correcto." & Chr$(10)
          sql = sql & "REINTENTAR --> Volver a solicitar resultados." & Chr$(10)
          sql = sql & "CANCELAR   --> Salir del programa (Hay que corregirlo)."
          resp = MsgBox(sql, vbQuestion + vbRetryCancel, frmPrincipal.Caption)
          Select Case resp
            Case vbRetry
'              cmdResultados_Click
              Resultados
            Case vbCancel
'              lblResultados.Visible = False
              objAutoAn.Estado = cteESPERA
              'cmdsalir_click
          End Select
      End Select
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos", "Datos :" & datos)
End Sub

Public Sub Lectura_Protocolo(caracter As String)
Static Mensaje As String   'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
'por estar declarada como Static en esta subrutina
  Mensaje = Mensaje & caracter
  CopiaMensaje = Mensaje
 
  Select Case objAutoAn.Estado
    Case cteESPERA  'Espera
      CobasCore_Espera CopiaMensaje
      Mensaje = CopiaMensaje
'      objAutoAn.AutoRecepcion "06 COBAS CORE II    04" & vbCrLf & Chr$(2) & vbCrLf & "11 001/10 001 10 ADU-5315" & vbCrLf & "12 11" & vbCrLf & "09 s qan" & Chr$(13) & Chr$(10) & "00  +0.3813 05   U 01" & Chr$(13) & Chr$(10) & Chr$(3) & Chr$(13) & Chr$(10) & Chr$(4) & Chr$(13) & Chr$(10)
    Case cteTRANSMISION  'Transmision
      CobasCore_Transmision CopiaMensaje
      Mensaje = CopiaMensaje
  End Select
End Sub

Private Sub Mask_Change()
  objAutoAn.MaskChange
End Sub



Private Sub TimerRespuesta_Timer()
'Controla los TimeOuts que produce el autoanalizador a la hora de enviar respuesta.
'Si se produce un TimeOut se da la posibilidad de volver a enviar el mensaje o de finalizar la
'ejecuci�n del programa. Los TimeOuts no cuentan como un intento de programaci�n de una prueba.
Dim sql As String
Dim resp As Integer
Dim datos As String
Dim objmuestra As clsMuestra
On Error GoTo AutoAnError

  TimerRespuesta.Enabled = False
  sql = "El autoanalizador no ha respondido todav�a al mensaje que se le envi�." & Chr$(10)
  sql = sql & "REINTENTAR --> Volver a enviar el mensaje." & Chr$(10)
  sql = sql & "CANCELAR   --> Salir del programa."
  resp = MsgBox(sql, vbQuestion + vbRetryCancel, frmPrincipal.Caption)
  Select Case resp
    Case vbRetry
      'Se toman acciones en funci�n de la respuesta que se esperaba
      Select Case TipoRespEsperada
        Case constPROGRAMACION
          datos = "   02"
          Bloque19 datos   'Se actualiza el estado de la MatrizGrid
          NumeroEnvio = NumeroEnvio - 1
          If UltimoEnvio = False Then Programar
        Case constRESULTADOS
          Resultados
      End Select
    Case vbCancel
       For Each objmuestra In objAutoAn.ColMuestras
        If objmuestra.strEstado = cteINTENTOPROGRAMAR Then
             objmuestra.strEstado = cteNOACEPTADA
            End If
        Next
'      lblResultados.Visible = False
'      cmdsalir_click
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "TimerRespuesta_Timer", "TimerRespuesta_Timer")
End Sub

Public Sub Mask_GotFocus()
   
End Sub

Public Sub Mask_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then SendKeys "{TAB}"
End Sub

Public Sub Mask_LostFocus()
Dim Bandeja As Integer
Dim Copa As Integer
Dim bandejaNueva As Integer
Dim copaNueva As Integer
Dim pos As String
Dim contador As Integer
Dim contador2 As Integer
Dim objmuestra As clsMuestra
On Error GoTo AutoAnError

  bandejaNueva = Val(Left$(Mask.Text, 3))
  copaNueva = Val(Mid$(Mask.Text, 5))
  If bandejaNueva <> bandejaComienzo Or copaNueva <> copaComienzo Then
    If copaNueva = 0 Then copaNueva = 1
    If copaNueva > 15 Then copaNueva = 1: bandejaNueva = bandejaNueva + 1
    If bandejaNueva = 0 Then bandejaNueva = 1
    If bandejaNueva > 10 Then bandejaNueva = 1
    pos = Left$("000", 3 - Len(CStr(bandejaNueva))) & bandejaNueva & "/" & Left$("00", 2 - Len(CStr(copaNueva))) & copaNueva
    Mask.Text = pos
    bandejaComienzo = bandejaNueva
    copaComienzo = copaNueva
    Open constArchivoIni For Binary As #1
    Put #1, , pos
    Close #1
            
'    Copa = copaComienzo
'    Bandeja = bandejaComienzo
    UltimaCopa = copaComienzo
    UltimaBandeja = bandejaComienzo
    For Each objmuestra In objAutoAn.ColMuestras
'      With objMuestra
'        If .strEstado = cteSINPROGRAMAR Then
'          If Copa > 15 Then Copa = 1: Bandeja = Bandeja + 1
'          If Bandeja > 10 Then Bandeja = 1
'          .strCopa = Left$("00", 2 - Len(CStr(Copa))) & Copa
'          .strBandeja = Left$("000", 3 - Len(CStr(Bandeja))) & Bandeja
'          Copa = Copa + 1
'        End If
'      End With
      pModificaMuestra objmuestra
    Next
'    ultimaBandeja = Bandeja 'Val(matrizpruebas(10, UBound(matrizpruebas, 2)))
'    ultimaCopa = Copa 'Val(matrizpruebas(9, UBound(matrizpruebas, 2)))
    'ssdgPruebas.Refresh
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Mask_LostFocus", "Mask_LostFocus")
End Sub

Public Sub pModificaMuestra(objmuestra As clsMuestra)
Dim Copa%, Bandeja%
  Copa = UltimaCopa
  Bandeja = UltimaBandeja
  With objmuestra
    If .strEstado = cteSINPROGRAMAR Then
      If Copa > 15 Then Copa = 1: Bandeja = Bandeja + 1
      If Bandeja > 10 Then Bandeja = 1
      .strCopa = Left$("00", 2 - Len(CStr(Copa))) & Copa
      .strBandeja = Left$("000", 3 - Len(CStr(Bandeja))) & Bandeja
      Copa = Copa + 1
    End If
  End With
  UltimaBandeja = Bandeja 'Val(matrizpruebas(10, UBound(matrizpruebas, 2)))
  UltimaCopa = Copa 'Val(matrizpruebas(9, UBound(matrizpruebas, 2)))
End Sub

Private Sub Escribir_Final()
Dim pos As String * 15
Dim filenum
On Error GoTo AutoAnError
  'Se coloca el n�mero de copa pr�ximo
  'UltimaCopa = UltimaCopa + 1
  pos = Left$("000", 3 - Len(CStr(UltimaBandeja))) & UltimaBandeja & "/" & Left$("00", 2 - Len(CStr(UltimaCopa))) & UltimaCopa & " " & CStr(Format(Now, "dd/mm/yyyy"))
'  MsgBox "posfinal= " & pos
  
  Open constArchivoIni For Binary As #1
  Put #1, , pos
  Close #1
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Escribir_Final", "Escribir_Final")
End Sub
  

Public Function Salir() As Boolean
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"

  
  If Not Salir Then Descargar
End Function

Public Sub Programar()
Dim cMuestraT As String     'c�digo de la muestra incluyendo la coletilla del tiempo
Dim cPrueba As String       'c�digo de la prueba/c�digo de la prueba seg�n codificaci�n del Core
Dim pruebas As String       'c�digos de las pruebas acumulados en un mismo string seg�n codificaci�n del Core
Dim contador1 As Integer
Dim contador2 As Integer
Dim cuentaPruebas As Integer
Dim Mensaje As String
Dim filenum
Dim objmuestra As clsMuestra
Dim objActuacion As clsActuacion
On Error GoTo AutoAnError

  For Each objmuestra In objAutoAn.ColMuestras
    With objmuestra
    'MsgBox " idmuestra=" & .strIDMuestra & "estado=" & .strEstado
      If .strEstado = cteSINPROGRAMAR Then
        .strEstado = cteINTENTOPROGRAMAR
        'Se comienza a formar el mensaje
        Mensaje = Chr$(1) & vbCrLf  '<SOH><CR><LF> BLOQUE CABECERA
        Mensaje = Mensaje & "06 SILAB            10" & vbCrLf 'bloque 10
        Mensaje = Mensaje & Chr$(2) & vbCrLf 'BLOQUE DATOS
        cMuestraT = .strCodMuestra '& "T" & .strProperty1
        Mensaje = Mensaje & "50 R " & .strBandeja & "/" & .strCopa & vbCrLf 'posicion
        Mensaje = Mensaje & "51 " & cMuestraT & Space$(20 - Len(cMuestraT)) & vbCrLf 'l�nea 51: codigo muestra
        'l�nea 52
        'Se buscan y se codifican todas la pruebas que hay que realizar a la muestra.
        cuentaPruebas = 0
        For Each objActuacion In .ColMuestrasPruebas
          With objActuacion
            'Se busca en la matriz de pruebas la muestra que se est� procesando y que todav�a
            'no se ha intentado programar ya que si hay m�s de 20 pruebas para una muestra, en
            'un proceso anterior se habr�n intentado programar
            'Se pregunta el estado de cada prueba y solo se programaran las que esten sin
            'programar. Esto se hace para evitar que cuando una muestra est� programada con
            'varias pruebas y llega una nueva prueba de la misma muestra esta se pone en estado no
            'programado para que la enfermera se de cuenta que tiene que volver a programar. pero
            'solo se tienen que programar las pruebas que todav�a no se han programado.
            If .strEstado = cteSINPROGRAMAR Then
              .strEstado = cteINTENTOPROGRAMAR
              If cuentaPruebas = 21 Then Exit For
'             .strEstado = cteINTENTOPROGRAMAR
              pruebas = pruebas & " " & objAutoAn.ColPruebaAuto(.strCodAct).strCodActAuto
              cuentaPruebas = cuentaPruebas + 1
            End If
          End With
        Next
        Mensaje = Mensaje & "52" & pruebas & vbCrLf
        Mensaje = Mensaje & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf 'BLOQUE FINAL
        Exit For
      End If
    End With
  Next

  'Se determina si quedan m�s muestras a programar, es decir, si es o no el �ltimo mensaje que
  'se env�a para programar la lista de trabajo.
  UltimoEnvio = Not objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR)
  'Se env�a el mensaje
  TipoRespEsperada = constPROGRAMACION
  NumeroEnvio = NumeroEnvio + 1
  'cmdProgramar.Enabled = False
  'Comm1.Output = mensaje
  objAutoAn.Enviar Mensaje
  'Text = Text & "Mensaje de Programacion: " & Chr$(13) & vbCrLf & mensaje & Chr$(13) & vbCrLf
  TimerRespuesta.Enabled = True
  'para hacer simulaciones
  objAutoAn.blnDesign = True
  If objAutoAn.blnDesign Then Probar
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Programar", "Programar")
End Sub

Public Sub Resultados()
'Se forma un mensaje que responde a la petici�n de resultados. El mensaje depende de si es el
'primer resultado pedido o no.

'Los resultados no pueden ser solicitados de forma espec�fica ya que el c�digo de las muestras
'que han sido sometidas a diluci�n es desconocido y la posici�n de las muestras que han sido
'recolocadas en la bandeja secundaria es tambi�n desconocida. Asimismo no se pueden pedir por
'pruebas ya que los resultados pueden ser enviados varias veces y no sabr�a si se trata de
'env�os repetidos o repeticiones reales. Por todo ello se piden los resultados de forma no
'espec�fia aunque luego s�lo se leen los correspondientes a las muestras (bloque 04)
Dim Mensaje As String
On Error GoTo AutoAnError

'  lblResultados.Visible = True
  'Se comienza a formar el mensaje
  Mensaje = Chr$(1) & vbCrLf 'BLOQUE CABECERA
  Mensaje = Mensaje & "06 SILAB            09" & vbCrLf 'bloque 09
  Mensaje = Mensaje & Chr$(2) & vbCrLf 'BLOQUE DATOS
  Mensaje = Mensaje & "10 09" & vbCrLf 'l�nea 10: se piden los resultados en su forma short
  Mensaje = Mensaje & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf 'BLOQUE FINAL
  'Se env�a el mensaje
  TipoRespEsperada = constRESULTADOS
  objAutoAn.Enviar Mensaje
  TimerRespuesta.Enabled = True
'  If objAutoAn.blnDesign Then Probar 'objAutoAn.AutoRecepcion "2"
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Resultados", "Resultados")
End Sub

Public Sub Probar()
Static NVez%
  NVez = NVez + 1
  Select Case NVez
    
    Case 1: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    Case 2: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    Case 3: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    Case 4: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    Case 5: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    Case 6: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    Case 7: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    Case 8: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
'    Case 4: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
'    Case 4: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 C.Core-2 33-4557 19" & vbCrLf & Chr$(2) & vbCrLf & "59 00" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf

    
     
    'Case 1: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 COBAS CORE II    01<<<<<06 01-OCT-99 11:20 c<<00  +0.0040 19     01<<<<<<<<" & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    'Case 2: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 COBAS CORE II    04" & vbCrLf & Chr$(2) & vbCrLf & "11 001/01 001 01 ADD-3569T0          " & vbCrLf & "12 24" & vbCrLf & "09 s qal" & vbCrLf & "00  +10.007 20     01" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    'Case 3: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 COBAS CORE II    04" & vbCrLf & Chr$(2) & vbCrLf & "11 001/02 001 02 ADD-3593T0          " & vbCrLf & "12 24" & vbCrLf & "09 s qal" & vbCrLf & "00  +18.193 20     01" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    'Case 4: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 COBAS CORE II    04" & vbCrLf & Chr$(2) & vbCrLf & "11 001/03 001 03 ADD-4638T0          " & vbCrLf & "12 24" & vbCrLf & "09 s qal" & vbCrLf & "00  +40.652 20     01" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    'Case 5: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 COBAS CORE II    04" & vbCrLf & Chr$(2) & vbCrLf & "11 003/01 003 01 com pos             " & vbCrLf & "12 24" & vbCrLf & "09 s qal" & vbCrLf & "00  +527.72 21     01" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
    'Case 6: objAutoAn.AutoRecepcion Chr$(1) & vbCrLf & "06 COBAS CORE II    00  +110.00 04     01" & vbCrLf & Chr$(3) & vbCrLf & Chr$(4) & vbCrLf
  End Select
End Sub

Public Sub Descargar()
  Call Escribir_Final
  End
End Sub

Public Sub pModificaResultado(objResultado As clsResultado)
Dim qry2 As rdoQuery
Dim rs2 As rdoResultset
Dim objmuestra As clsMuestra
Dim sql$, NumHistoria As Long, i%
On Error GoTo AutoAnError
  With objResultado
'    sql = "select ra.resultadoalfanumerico"
'    sql = sql & " from resultadoasistencia ra, pr0400 pr04, ci2200 ci22"
'    sql = sql & " where ra.pr04numactplan = pr04.pr04numactplan and"
'    sql = sql & " pr04.ci21codpersona = ci22.ci21codpersona and"
'    sql = sql & " ci22.ci22numhistoria = ? and"
'    sql = sql & " pr04.pr01codactuacion = ?"
'    sql = sql & " order by ra.fecha desc"
    sql = "select resultadoalfanumerico from" _
    & " resultadoasistencia ra, pruebaasistencia pa where" _
    & " ra.historia = pa.historia and ra.caso = pa.caso and" _
    & " ra.secuencia = pa.secuencia and" _
    & " ra.nrepeticion = pa.nrepeticion and" _
    & " pa.historia = ? and pa.ctipoprueba = ?" _
    & " order by ra.fecha desc"
    Set qry2 = objAutoAn.rdoConnect.CreateQuery("", sql)
    qry2(0) = .Actuacion.Muestra.lngHistoria
    qry2(1) = .Actuacion.strCodAct
    Set rs2 = qry2.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    i = 0
    Do While Not rs2.EOF And i < 2
      i = i + 1
      Select Case i
        Case 1: .strProperty1 = rs2("resultadoalfanumerico")
        Case 2: .strProperty2 = rs2("resultadoalfanumerico")
      End Select
      rs2.MoveNext
    Loop
    rs2.Close: qry2.Close
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "pModificaActuacion", "Codigo Actuaci�n :" & objResultado.Actuacion.strCodAct)
End Sub
