Attribute VB_Name = "modElectra"
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
'  App.HelpFile = "f:\laborat\ayuda\" & App.EXEName & ".hlp"
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    fCargarColumnasGrid
    If Not .fInicializar(App, cteELECTRA, True) Then End
    .Borrar_Logs
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  'columnas del grid
  With objAutoAn
    .AgregarColumnaAGridMuestras cteMuestraCOPA
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION, , 1200
    .AgregarColumnaAGridPruebas ctePruebaNUMREPETICION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO, , 1200
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO, , 1200
    .AgregarColumnaAGridPruebas cteResultadoESTADO
  End With
End Sub

Sub pModificarResultsAuto()
Dim objPruebaAuto As clsPruebaAuto
Dim objResultAuto As clsResultAuto
  For Each objPruebaAuto In objAutoAn.ColPruebaAuto
    For Each objResultAuto In objPruebaAuto.ColResultAuto
      objResultAuto.strCodResultAuto = objPruebaAuto.strCodActAuto _
      & "-" & objResultAuto.strCodResultAuto
    Next
  Next
End Sub
