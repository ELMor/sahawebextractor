VERSION 5.00
Begin VB.Form CambioEstado 
   Caption         =   "Cambio de Estado"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3870
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   3870
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      Height          =   555
      Left            =   2100
      TabIndex        =   2
      Top             =   2400
      Width           =   1275
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Aceptar"
      Height          =   555
      Left            =   300
      TabIndex        =   1
      Top             =   2400
      Width           =   1275
   End
   Begin VB.Frame Frame1 
      Caption         =   "A Estado:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   300
      TabIndex        =   0
      Top             =   240
      Width           =   3075
      Begin VB.OptionButton Option1 
         Caption         =   "Enviada (AC)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   420
         TabIndex        =   5
         Tag             =   "AC5"
         Top             =   1200
         Width           =   1755
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Sin Enviar (SP)"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   420
         TabIndex        =   4
         Tag             =   "SP4"
         Top             =   780
         Width           =   1755
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Extraida"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   420
         TabIndex        =   3
         Tag             =   "EX3"
         Top             =   360
         Width           =   1755
      End
   End
End
Attribute VB_Name = "CambioEstado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
Dim i%
For i = 0 To 2
    If Option1(i).Value = True Then
        Call Cambio_Estado(Left$(Option1(i).Tag, 2), CInt(Right$(Option1(i).Tag, 1)))
        Exit For
    End If
Next i
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub
