VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{CF85F474-6533-11D3-856D-00C04F71A8A3}#1.0#0"; "MiAwk.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Coulter Epics"
   ClientHeight    =   1605
   ClientLeft      =   2595
   ClientTop       =   690
   ClientWidth     =   1335
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Coulter.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1605
   ScaleWidth      =   1335
   Begin MyAwk.Awk Awk2 
      Left            =   690
      Top             =   480
      _ExtentX        =   979
      _ExtentY        =   979
   End
   Begin MyAwk.Awk Awk1 
      Left            =   120
      Top             =   480
      _ExtentX        =   979
      _ExtentY        =   979
   End
   Begin VB.Timer TimerCopiaArchivo 
      Left            =   0
      Top             =   60
   End
   Begin VB.Timer TimerDescompresion 
      Left            =   900
      Top             =   60
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   420
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MyAwk.Awk Awk3 
      Left            =   120
      Top             =   1050
      _ExtentX        =   979
      _ExtentY        =   979
   End
   Begin MyAwk.Awk Awk4 
      Left            =   690
      Top             =   1050
      _ExtentX        =   979
      _ExtentY        =   979
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim FirstActivate As Integer        'Se�ala si es la primera vez que el form se vualve activo
Dim infoMuestras As String          'Informaci�n de las posiciones de las muestras en el carrusel
Dim numReenvios As Integer          'Cuenta el n�mero de veces que se intenta transmitir el mismo mensaje
Dim SEGUNDAPARTE As Integer         'Indica si se espera un segundo env�o de resultados de la misma muestra

Dim procesar As Integer     'vble que paraliza la comunicacion por motivos de seguridad
Dim prim As Integer         'vble que indica que ha comenzado la comunicacion

Dim limite As Integer       'Vble que almacena el n� de filas activas para hacer programacion.
                            'Es igual al limite superior de matrizgrid cuando se le da al start.
                            'Por si acaso se ejecutan pruebas dinamicas durante el proceso,
                            'para que las programe
Dim colProtocolos As New Collection
Dim strFile As String
Dim ArchivoEpa As String
Dim FileNum As Integer
Dim blnAbierto As Boolean
Dim NumEOREOS As Integer
Dim num As Integer
Private Const PROCESS_QUERY_INFORMATION = &H400
Private Const STILL_ACTIVE = &H103
Private Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess&, ByVal bInheritHandle&, ByVal dwProcessId&) As Long
Private Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Dim LastPath As String
 
Function Path(FileName As String, FileTitle As String) As String
Dim Posicion%
  Posicion = InStr(1, FileName, FileTitle)
  If Posicion > 2 Then
    Path = Left$(FileName, Posicion - 1)
  Else
    Path = "c:\"
  End If
End Function

Sub EsperarShell(sCmd As String)
Dim hShell As Long
Dim hProc As Long
Dim codExit As Long

  ' ejecutar comando
  hShell = Shell(Environ$("Comspec") & " /c " & sCmd, 2)
  ' esperar a que se complete el proceso
  hProc = OpenProcess(PROCESS_QUERY_INFORMATION, False, hShell)
  Do
    GetExitCodeProcess hProc, codExit
    DoEvents
  Loop While codExit = STILL_ACTIVE
  'MsgBox "El comando ha acabado"
End Sub
   
Public Sub Resultados()
Dim a As Date, i As Long
Dim PosicIni As Integer
Dim PosicFin As Integer
On Error GoTo AutoAnError

  With CommonDialog1
    .InitDir = LastPath
    On Error Resume Next
    .FileName = Dir(LastPath & "*.ept")
    If Err = 52 Then
      .FileName = Dir("c:\*.ept")
      .InitDir = "C:\"
      Err = 0
    End If
    .Action = 1: DoEvents: DoEvents
    If .FileTitle <> "" Then 'seleccion de un solo archivo
'      MsgBox "1"
      LastPath = Path(.FileName, .FileTitle)
      Call DescomprimirArchivo(LastPath, .FileTitle)
    Else 'seleccion multiple (archivos separados por chr$(0))
      
      PosicIni = InStr(1, .FileName, Chr$(0))
      If PosicIni > 0 Then
        LastPath = Left$(.FileName, PosicIni - 1) & "\"
        Do While PosicFin < Len(.FileName)
          PosicFin = InStr(PosicIni + 1, .FileName, Chr$(0))
          If PosicFin = 0 Then PosicFin = Len(.FileName) + 1
          Call DescomprimirArchivo(LastPath, Mid$(.FileName, PosicIni + 1, PosicFin - PosicIni - 1))
          PosicIni = PosicFin
        Loop
      Else
'        MsgBox "2"
      End If
    End If
    objAutoAn.pQuitarEspera
  End With
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Sub Resultados")
End Sub

Private Sub DescomprimirArchivo(Path As String, FileTitle As String)
Dim t1 As Long, i%
Dim ArchivoEpt As String
Dim blnErrores As Boolean
On Error GoTo AutoAnError
  
  Select Case LCase$(Right$(FileTitle, 3))
    Case "ept"
      objAutoAn.pMostrarEspera "Descomprimiendo Archivo " & FileTitle
      On Error Resume Next
      Kill "c:\temporal\" & Left$(FileTitle, Len(FileTitle) - 3) & "epa"
      DoEvents
      ArchivoEpt = "c:\temporal\" & FileTitle
      If LCase$(Path & FileTitle) <> LCase$(ArchivoEpt) Then
        FileCopy Path & FileTitle, ArchivoEpt
        DoEvents: Sleep 2000
        t1 = GetTickCount(): DoEvents
        Do
          If GetTickCount - t1 > 10000 Then
            objAutoAn.pQuitarEspera
            MsgBox "No Se Pudo copiar el archivo " & FileTitle & " a 'C:\temporal'"
            Exit Sub
          End If
        Loop While Dir(ArchivoEpt) = ""
      End If
      If FileLen(ArchivoEpt) > 0 Then
        Shell ("c:\temporal\ept2asc2 " & ArchivoEpt)
        DoEvents: Sleep 5000
        ArchivoEpa = "c:\temporal\" & Left$(FileTitle, Len(FileTitle) - 3) & "epa"
        t1 = GetTickCount(): DoEvents
        Do
          If GetTickCount - t1 > 10000 Then
            objAutoAn.pQuitarEspera
            MsgBox "No Se Pudo Descomprimir el archivo " & FileTitle
            Exit Sub
          End If
        Loop While Dir(ArchivoEpa) = ""
        If FileLen(ArchivoEpa) > 0 Then ProcesarArchivo ArchivoEpa
      End If
    Case "epa": ProcesarArchivo (Path & FileTitle)
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Sub DescomprimirArchivo", "Path-Filetitle: " & Path & FileTitle)
End Sub

Private Sub ProcesarArchivo(strFile As String)
Dim Rs As rdoResultset, Qry As rdoQuery
Dim rs1 As rdoResultset, qry1 As rdoQuery
Dim Sql As String, strItem As String
Dim dimension As Integer
Dim strIDMuestra As String
Dim objProtocolo As clsProtocolo
Dim objResultProtocolo As clsResultProtocolo
Dim i%, StrCabMu$, StrCabPru$, j%
Dim txtPanel As String
Dim Muestra As String
Dim Panel As String

Dim MiRegistro As String * 10000
Dim posMuestra As Integer
Dim posPanel As Integer
Dim posRN As Integer
Dim posPro As Integer
Dim posPerc As Integer
Dim posRegion As Integer
'Dim objProtocolo As clsProtocolo
Dim objResultado As clsResultProtocolo
Dim Protocol As String
Dim RunNumber As String
Dim UltimoTrozo As String
Dim strBlock As String
Dim Historia As Long, Caso%, Secuencia As Long
Dim resul$
On Error GoTo AutoAnError

  If strFile <> "" Then
    objAutoAn.pMostrarEspera "Leyendo Archivo " & strFile
    Open strFile For Random As #1 Len = 10000 'Len("c:\temp\Z0026492.epa")
    Get #1, 1, MiRegistro    ' Lee el tercer registro.
    DoEvents
    Awk1 = MiRegistro
    Awk1.Fs = Chr$(34) & "--EOS--" & Chr$(34) & vbCrLf
    Awk2 = Awk1.F(1)
    Awk2.Fs = Chr$(34) & "--EOR--" & Chr$(34) & vbCrLf
    posMuestra = Posicion("Primary ID", Awk2.F(1))
    If posMuestra <> 7 Then MsgBox "Se Ha Detectado Un Posible Fallo en la Posicion de la Muestra": GoTo AutoAnError
    posPanel = Posicion("Panel Name", Awk2.F(1))
    If posPanel <> 8 Then MsgBox "Se Ha Detectado Un Posible Fallo en la Posicion del Panel": GoTo AutoAnError
    Awk3 = Awk2.F(2)
    Awk3.Fs = vbCrLf
    Muestra = objAutoAn.fQuitarStringsDeString(Awk3.F(posMuestra), Chr$(34))
    'comprobamos que esta muestra esta en la lista de muestras sin resultado
    If objAutoAn.blnExistMuestra(Muestra) Then
      Panel = objAutoAn.fQuitarStringsDeString(Awk3.F(posPanel), Chr$(34))
      Historia = objAutoAn.ColMuestras(Muestra).lnghistoria
      Sql = "SELECT a.LB21DESPROTOCOLO, b.lb22codRESULTADO, b.lb22codregion FROM "
      Sql = Sql & "LB2100 a,lb2200 b,LB2300 C WHERE a.lb21codPROTOCOLO=b.lb21codPROTOCOLO and "
      Sql = Sql & "A.LB23CODPANEL=C.LB23CODPANEL AND a.lb23codpanel=b.lb23codpanel and "
      Sql = Sql & "C.LB23DESPANEL=? order by a.lb21DESPROTOCOLO"
      Set Qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
      Qry(0) = objAutoAn.fQuitarStringsDeString((Panel), Chr$(34))
      Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Rs.EOF Then MsgBox ("Panel No Definido en GDL")
      Do While Not Rs.EOF
        Set objProtocolo = New clsProtocolo
        With objProtocolo
          .ProtocolName = Rs!lb21DESPROTOCOLO
          Do
            Set objResultProtocolo = New clsResultProtocolo
            Sql = "SELECT a.cprueba, a.cresultado, b.cunidadconvencional, " _
            & " b.Designacion, a.cResultadoAuto FROM" _
            & " resultadosautoanalizador a, pruebasresultados b" _
            & " WHERE a.cprueba = b.cprueba and" _
            & " a.cresultado = b.cresultado and A.cresultadoauto = ?"
            Set qry1 = objAutoAn.rdoConnect.CreateQuery("", Sql)
            qry1(0) = Rs!lb22codResultado
            Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If Not rs1.EOF Then
              With objResultProtocolo
                .Region = Rs!lb22codregion
                .cPrueba = rs1!cPrueba
                .cResultado = rs1!cResultado
                .cUnidad = rs1!cUnidadconvencional
                .Descripcion = rs1!designacion
                .cResultadoAuto = rs1!cResultadoAuto
              End With
              .ColResultados.Add objResultProtocolo, CStr(objResultProtocolo.Region)
              Set objResultProtocolo = Nothing
            End If
            rs1.Close
            qry1.Close
            Rs.MoveNext
            If Rs.EOF Then Exit Do
          Loop While .ProtocolName = Rs!lb21DESPROTOCOLO
          colProtocolos.Add objProtocolo, CStr(.ProtocolName)
        End With
        Set objProtocolo = Nothing
      Loop
      Rs.Close
      Qry.Close
      'correspondencia protocolos - RunNumber que contienen los resultados
      
      Awk3 = Awk1.F(2)
      Awk3.Fs = Chr$(34) & "--EOR--" & Chr$(34) & vbCrLf
  
      posRN = Posicion("Run Number", Awk3.F(1))
      If posRN <> 1 Then MsgBox "Se Ha Detectado Un Posible Fallo en la Posicion del Run Number": GoTo AutoAnError
      posPro = Posicion("Protocol Name", Awk3.F(1))
      If posPro <> 5 Then MsgBox "Se Ha Detectado Un Posible Fallo en la Posicion del Protocolo": GoTo AutoAnError
      For i = 2 To Awk3.Nf
        Awk4 = Awk3.F(i)
        Awk4.Fs = vbCrLf
        If Awk4.Nf > posPro Then
          Protocol = objAutoAn.fQuitarStringsDeString((Awk4.F(posPro)), Chr$(34))
          If blnExistProtocol(Protocol) Then
            colProtocolos(Protocol).RunNumber = objAutoAn.fQuitarStringsDeString((Awk4.F(posRN)), Chr$(34))
          End If
        End If
      Next i
      'resultados
      objAutoAn.pMostrarEspera "Leyendo Resultados..."
      Awk3 = Awk1.F(3)
      Awk3.Fs = Chr$(34) & "--EOR--" & Chr$(34) & vbCrLf
      posRegion = Posicion("Region ID", Awk3.F(1))
      If posRegion <> 3 Then MsgBox "Se Ha Detectado Un Posible Fallo en la Posicion de la Regi�n": GoTo AutoAnError
      posPerc = Posicion("Perc in region", Awk3.F(1))
      If posPerc <> 8 Then MsgBox "Se Ha Detectado Un Posible Fallo en la Posicion del Resultado": GoTo AutoAnError
      posRN = Posicion("Protocol Run Number", Awk3.F(1))
      If posRN <> 1 Then MsgBox "Se Ha Detectado Un Posible Fallo en la Posicion del Run Number": GoTo AutoAnError
      For i = 2 To Awk3.Nf - 1
        Awk4 = Awk3.F(i)
        Awk4.Fs = vbCrLf
        RunNumber = objAutoAn.fQuitarStringsDeString((Awk4.F(posRN)), Chr$(34))
        If blnExistRunNumber(RunNumber, Protocol) Then
          For Each objResultado In colProtocolos(Protocol).ColResultados
            With objResultado
              If .Region = objAutoAn.fQuitarStringsDeString((Awk4.F(posRegion)), Chr$(34)) Then
                .Valor = objAutoAn.fFormatearNumero(objAutoAn.fQuitarStringsDeString(Awk4.F(posPerc), Chr$(34)), ".", ",")
              End If
            End With
          Next
        End If
      Next i
      UltimoTrozo = Awk3.F(Awk3.Nf)
      'resultados
      For i = 2 To CInt(FileLen(strFile) / 10000) + 1
        Get #1, i, MiRegistro    ' Lee el tercer registro.
        strBlock = UltimoTrozo & MiRegistro
        Awk3 = strBlock
        Awk3.Fs = Chr$(34) & "--EOR--" & Chr$(34) & vbCrLf
        For j = 1 To Awk3.Nf - 1
          Awk4 = Awk3.F(j)
          Awk4.Fs = vbCrLf
          RunNumber = objAutoAn.fQuitarStringsDeString((Awk4.F(posRN)), Chr$(34))
          If blnExistRunNumber(RunNumber, Protocol) Then
            For Each objResultado In colProtocolos(Protocol).ColResultados
              With objResultado
                If .Region = objAutoAn.fQuitarStringsDeString((Awk4.F(posRegion)), Chr$(34)) Then
                  .Valor = objAutoAn.fFormatearNumero(objAutoAn.fQuitarStringsDeString((Awk4.F(posPerc)), Chr$(34)), ".", ",")
                End If
              End With
            Next
          End If
        Next j
        UltimoTrozo = Awk3.F(Awk3.Nf)
      Next i
      objAutoAn.pMostrarEspera "Introduciendo Resultados..."
      For Each objProtocolo In colProtocolos
        For Each objResultado In objProtocolo.ColResultados
          With objResultado
            If Not IsNull(.Valor) Then
              Call objAutoAn.pIntroResultConCResAuto(Muestra, .cResultadoAuto, .Valor)
            End If
          End With
        Next
      Next
    End If
    Close #1
  End If
  
Fin:
  Set colProtocolos = Nothing
  ArchivoEpa = ""
  
  Exit Sub
AutoAnError:
    Close #1
    Call objError.InternalError(Me, "Sub ProcesarArchivo", "Archivo:" & strFile)
End Sub

Function blnExistProtocol(id As String) As Boolean
Dim obj As clsProtocolo

  For Each obj In colProtocolos
    If obj.ProtocolName = id Then
      blnExistProtocol = True
      Exit For
    End If
  Next
End Function

Function blnExistRunNumber(RN As String, Prot As String) As Boolean
Dim obj As clsProtocolo

  For Each obj In colProtocolos
    If obj.RunNumber = RN Then
      Prot = obj.ProtocolName
      blnExistRunNumber = True
      Exit For
    End If
  Next
End Function

Function Posicion(str$, enstr$) As Integer
Dim pstr%, peos%, pvbcrlf%
Dim estrin$, npeos%
'On Error Resume Next
  peos = 1
  pstr = InStr(1, enstr, str)
  If pstr > 2 Then estrin = Mid$(enstr, peos, pstr - peos - 1) Else Posicion = 1: Exit Function
  pvbcrlf = InStr(1, estrin, vbCrLf)
  Posicion = 1
  While pvbcrlf <> 0
    pvbcrlf = InStr(pvbcrlf + 1, estrin, vbCrLf)
    Posicion = Posicion + 1
  Wend
End Function

Public Sub Descargar()
  End
End Sub

Private Sub Form_Load()
  LastPath = "a:\"
  With CommonDialog1
    .Flags = cdlOFNFileMustExist + cdlOFNPathMustExist + _
    cdlOFNAllowMultiselect + cdlOFNExplorer
    .DialogTitle = "Ubicaci�n del Archivo de Resultados"
    .Filter = "Archivos de Resultados del Coulter (*.ept,*.epa)|*.ept;*.epa"
  End With
End Sub

Public Function Salir() As Boolean
'On Error GoTo AutoAnError
'
'  objAutoAn.EscribirLog "Salir"
'  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion, Me.Caption) = vbCancel)
'  objAutoAn.EscribirLog " Pulsado Salir"
'  If Not Salir Then Descargar
'  Exit Function
'
'AutoAnError:
'    Call objError.InternalError(Me, "Salir", "Salir")
End Function

Private Sub Timer2_Timer()

End Sub
