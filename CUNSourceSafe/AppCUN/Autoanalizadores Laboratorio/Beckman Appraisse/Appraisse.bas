Attribute VB_Name = "modAppraisse"
Option Explicit
Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objError As New clsErrores

Public Sub Main()
  Screen.MousePointer = vbHourglass
  With objAutoAn
    Set .frmFormulario = frmPrincipal
    fCargarColumnasGrid
    If Not .fInicializar(App, cteBECKMAN_APPRAISE) Then End
    fCargaPT
    .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  'columnas del grid
  With objAutoAn
    '.AgregarColumnaAGridMuestras cteMuestraPOSICION, "N�Orden"
    .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
    .AgregarColumnaAGridMuestras cteMuestraURGENTE
    .AgregarColumnaAGridMuestras cteMuestraPROPERTY1, "Prot.Tot.", 1350
    .AgregarColumnaAGridMuestras cteMuestraESTADO
    .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
    
    .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
    .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
    .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO, , 1000
    .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
    .AgregarColumnaAGridPruebas ctePruebaURGENTE
    .AgregarColumnaAGridPruebas cteResultadoESTADO
'    .AgregarColumnaAGridPruebas cteResultadoREFMIN
'    .AgregarColumnaAGridPruebas cteResultadoREFMAX
    .AgregarColumnaAGridPruebas ctePruebaNUMREPETICION
  End With
End Sub

Private Sub fCargaPT()
Dim Sql As String
Dim Qry As rdoQuery
Dim Rs As rdoResultset
Dim objMuestra As clsMuestra
  
  For Each objMuestra In objAutoAn.ColMuestras
    Sql = " SELECT" _
    & " RESULTADOASISTENCIA.RESULTADOALFANUMERICO" _
    & " FROM" _
    & " MUESTRAPRUEBA MUESTRAPRUEBA," _
    & " PRUEBAASISTENCIA PRUEBAASISTENCIA," _
    & " PRUEBAASISTENCIA PRUEBAASISTENCIA2," _
    & " RESULTADOASISTENCIA RESULTADOASISTENCIA" _
    & " WHERE" _
    & " MUESTRAPRUEBA.HISTORIA = PRUEBAASISTENCIA.HISTORIA AND" _
    & " MUESTRAPRUEBA.CASO = PRUEBAASISTENCIA.CASO AND" _
    & " MUESTRAPRUEBA.SECUENCIA = PRUEBAASISTENCIA.SECUENCIA AND" _
    & " PRUEBAASISTENCIA.HISTORIA = PRUEBAASISTENCIA2.HISTORIA AND" _
    & " PRUEBAASISTENCIA.CASO = PRUEBAASISTENCIA2.CASO AND" _
    & " PRUEBAASISTENCIA.NPETICION = PRUEBAASISTENCIA2.NPETICION AND" _
    & " PRUEBAASISTENCIA2.HISTORIA = RESULTADOASISTENCIA.HISTORIA AND" _
    & " PRUEBAASISTENCIA2.CASO = RESULTADOASISTENCIA.CASO AND" _
    & " PRUEBAASISTENCIA2.SECUENCIA = RESULTADOASISTENCIA.SECUENCIA AND" _
    & " PRUEBAASISTENCIA2.NREPETICION = RESULTADOASISTENCIA.NREPETICION AND" _
    & " PRUEBAASISTENCIA.CTIPOPRUEBA = 31 AND" _
    & " PRUEBAASISTENCIA2.CTIPOPRUEBA = 527 AND" _
    & " PRUEBAASISTENCIA2.ESTADO = 9 AND" _
    & " MUESTRAPRUEBA.CMUESTRA = ?" _
    & " ORDER BY" _
    & " MUESTRAPRUEBA.CMUESTRA ASC"
    Set Qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
    Qry(0) = objMuestra.strCodMuestra
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not Rs.EOF Then objMuestra.strProperty1 = Rs("resultadoalfanumerico")
    Rs.Close
    Qry.Close
  Next
End Sub

Private Sub fCargaPTbis()
Dim Sql As String
Dim Qry As rdoQuery
Dim Rs As rdoResultset
Dim objMuestra As clsMuestra
  
  For Each objMuestra In objAutoAn.ColMuestras
    Sql = " SELECT RA.RESULTADOALFANUMERICO" _
    & " FROM" _
    & " MUESTRAPRUEBA MP," _
    & " PRUEBAASISTENCIA PA, PR0400 P41, PR0400 P42 ," _
    & " PR0300 P31, PR0300 P32, RESULTADOASISTENCIA RA" _
    & " WHERE" _
    & " MP.PR04NUMACTPLAN = P41.PR04NUMACTPLAN AND" _
    & " P41.CI21CODPERSONA = P31.CI21CODPERSONA AND" _
    & " P31.CI21CODPERSONA = P32.CI21CODPERSONA AND" _
    & " P31.PR09NUMPETICION = P32.PR09NUMPETICION AND" _
    & " P42.CI21CODPERSONA = P32.CI21CODPERSONA AND" _
    & " P42.PR04NUMACTPLAN = RA.PR04NUMACTPLAN AND" _
    & " P42.PR01CODACTUACION = 527 AND" _
    & " P41.PR01CODACTUACION = 31 AND" _
    & " RA.CRESULTADO=1 AND" _
    & " MP.CMUESTRA = ?" _
    & " ORDER BY" _
    & " MP.CMUESTRA ASC"
    '& " P42.PR37CODESTADO = 5 AND"
    Set Qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
    Qry(0) = objMuestra.strCodMuestra
    Set Rs = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    If Not Rs.EOF Then objMuestra.strProperty1 = Rs("resultadoalfanumerico")
    Rs.Close
    Qry.Close
  Next
End Sub


