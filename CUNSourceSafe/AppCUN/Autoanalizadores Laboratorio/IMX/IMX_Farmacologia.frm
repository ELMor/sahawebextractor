VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "IMX"
   ClientHeight    =   480
   ClientLeft      =   2385
   ClientTop       =   2730
   ClientWidth     =   1410
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "IMX_Farmacologia.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   480
   ScaleWidth      =   1410
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Variables que guardan la informaci�n sobre la prueba y sus resultados para actualizar la base de datos
'Dim cprueba As String               'c�digo de la prueba
'Dim res1 As String                  'c�digo del primer resultado utilizado por el autoanalizador
'Dim res2 As String                  'c�digo del segundo resultado utilizado por el autoanalizador
Dim MiNumeroListas As Integer
Dim lngListaTrabajo As Long

Public Sub probar()
Dim Awk1  As String
  
'Awk1 = "[" '& Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "DATE: 10/02/97" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "TIME: 12:52:17" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "TECH ID : _________" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "RGNT LOT: _________" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "SERIAL #:  3747" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "CRSL ID:" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "ASSAY 112 TACRO II    3C10 Revision 1" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "LOC CALIB   RATE" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "----------------" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "1    A 445.7" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "2    A 406.6" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "3    B 344.9" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "4    B 335.6" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "5    C 311.1" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "6    C 290.7" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "7    D 217.8" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "8    D 222#" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "9    E 177.7" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "10    E 159.6" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "11    F 132.5" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "12    F 122#" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "CALIB      ng/mL    AVGR    FITR    RERR" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "----------------------------------------" & Chr$(13) & Chr$(10)
'  Awk1 = Awk1 & "A           0.   426.2   426.2     0.0" & Chr$(13) & Chr$(10)
'  Awk1 = Awk1 & "B           3.   340.3   344.7    -4.4" & Chr$(13) & Chr$(10)
'  Awk1 = Awk1 & "C           6.   300.9   292.6     8.3" & Chr$(13) & Chr$(10)
'  Awk1 = Awk1 & "D          12.   219.9   224.3    -4.4" & Chr$(13) & Chr$(10)
'  Awk1 = Awk1 & "E          20.   168.7   169.0    -0.3" & Chr$(13) & Chr$(10)
'  Awk1 = Awk1 & "F          30.   127.3   126.2     1.1" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "RMSE = 3.693" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "CALIBRATION ACCEPTED" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "LOC    ID            ng/mL     RATE NOTE" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "----------------------------------------" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "3  0008026415         5.7    297.5" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "4  CONTROL M         11.6    227.8" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "5  CONTROL H         24.3    148.1" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "" & Chr$(13) & Chr$(10)
'Awk1 = Awk1 & "TESTS"
'
objAutoAn.AutoRecepcion Awk1
End Sub
Private Sub IMX_Espera(mensaje As String)
'Identifica la llegada del caracter de comienzo de transmisi�n (comienzo del mensaje) y pasa al OBJAUTOAN.ESTADO de transmisi�n.
  On Error Resume Next
  If Right$(mensaje, 1) = "[" Then
    objAutoAn.Estado = cteTRANSMISION
    mensaje = ""
  End If
End Sub

Private Sub IMX_Transmision(mensaje As String)
 If Right$(mensaje, 2) = "!!" Or _
  Right$(mensaje, 4) = "USED" Or _
  Right$(mensaje, 5) = "TESTS" Then
    Lectura_Datos mensaje
    objAutoAn.Estado = cteESPERA
    mensaje = ""
  End If
End Sub

Private Sub Lectura_Datos(Datos As String)
Dim Sql As String
Dim Qry As rdoQuery
Dim Rs1 As rdoResultset
Dim CodPrueba As String
Dim intCodresultado As Integer
Dim i%, x%, Numero%, Res%, Valor%
Dim blnResfound As Boolean
Dim objMuestra As clsMuestra
Dim Campo$

  Awk1 = Datos: Awk1.FS = vbCrLf
  For i = 1 To Awk1.NF
    'Se busca el codigo de la prueba.
    If Left$(Awk1.F(i), 5) = "ASSAY" Then
      Awk2 = Awk1.F(i)
      Awk2.FS = " "
      Sql = "SELECT cprueba FROM pruebasautoanalizador " _
      & "WHERE cAutoAnalizador = ? AND cPruebaAuto= ?"
      Set Qry = objAutoAn.rdoConnect.CreateQuery("", Sql)
      Qry(0) = objAutoAn.intCodAutoAn
      Qry(1) = Awk2.F(2)
      Set Rs1 = Qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not Rs1.EOF Then
        CodPrueba = CStr(Rs1!cprueba)
        Exit For
      End If
    End If
  Next i
  intCodresultado = 1
  blnResfound = False
  
 'Buscamos la posicion de los resultados
  Do While i <= Awk1.NF - 3 And Not blnResfound
    If Right$(Awk1.F(i), 9) = "RATE NOTE" Then
      blnResfound = True
    Else
      i = i + 1
    End If
  Loop
  If i + 1 <= Awk1.NF - 3 Then
    For x = i + 1 To Awk1.NF - 3
      'Registro de datos
      Awk2 = Awk1.F(x)
      Awk2.FS = " "
      If IsNumeric(Trim$(Mid$(Awk2.F(1), 1, 2))) Then
        Numero = CInt(Trim$(Mid$(Awk2.F(1), 1, 2))) - 2
        'Esto se hace porque siempre ponen dos controles por lo
        'tanto la primera prueba es la numero tres.
        If Numero <= objAutoAn.ColMuestras.Count Then
          Set objMuestra = objAutoAn.ColMuestras(Numero) ' van en orden
            If objMuestra.strEstado = cteACEPTADA Then
              If objAutoAn.blnExistPruebaInMuestra(objMuestra.strCodMuestra, CodPrueba) Then
                With objMuestra.ColMuestrasPruebas(CodPrueba).ColPruebasResultados(CStr(intCodresultado))
                  Res = InStr(Awk2, ">")
                  Campo = IIf(Res > 0, Awk2.F(4), Awk2.F(3))
                  If IsNumeric(Campo) Then
                    Campo = CStr(objAutoAn.fFormatearNumero(Campo, ".", ","))
                  End If
                  If Res > 0 Then Campo = " >" & Campo
                  .strResultado = Campo
                End With
              End If
            End If
          End If
        End If
      Next x
    End If
  End Sub
 Public Sub Descargar()
  End
End Sub

Public Sub Lectura_Protocolo(caracter As String)
Static mensaje As String 'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
'por estar declarada como Static en esta subrutina

  mensaje = mensaje & caracter  'Se va componiendo el mensaje
  CopiaMensaje = mensaje
  Select Case objAutoAn.Estado
    Case cteESPERA
      IMX_Espera CopiaMensaje
      mensaje = CopiaMensaje
    Case cteTRANSMISION
      IMX_Transmision CopiaMensaje
      mensaje = CopiaMensaje
  End Select
End Sub

