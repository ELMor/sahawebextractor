Attribute VB_Name = "Inmuline"
Option Explicit

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
Declare Function WritePrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFileName As String) As Integer

'objeto de la clase autoanalizador
Public objAutoAn As clsAutoAnalizador
Public objError As New clsErrores


Public Sub Main()
Screen.MousePointer = vbHourglass
Set objAutoAn = New clsAutoAnalizador
With objAutoAn
  Set .frmFormulario = frmPrincipal
  frmPrincipal.Caption = "Inmulite Hormonas"
  'definicion de las columnas que integran ls grid
  fCargarColumnasGrid
  If Not .fInicializar(App, cteINMULITE, , , , , , , , , True) Then End
  .Borrar_Logs
  .Show
End With
Screen.MousePointer = vbDefault
End Sub


Public Sub fCargarColumnasGrid()
'Columnas del grid de la tabla de muestras
'y de la obtenci�n del resultado
On Error GoTo AutoanError

With objAutoAn
'  .AgregarColumnaAGridMuestras cteMuestraCOPA
  .AgregarColumnaAGridMuestras cteMuestraCODMUESTRA
  .AgregarColumnaAGridMuestras cteMuestraIDMUESTRA
  .AgregarColumnaAGridMuestras cteMuestraURGENTE
  .AgregarColumnaAGridMuestras cteMuestraNUMREPETICION
  .AgregarColumnaAGridMuestras cteMuestraESTADO
  
  .AgregarColumnaAGridPruebas ctePruebaCODMUESTRA
  .AgregarColumnaAGridPruebas ctePruebaURGENTE
  .AgregarColumnaAGridPruebas ctePruebaDESCACTUACION
  .AgregarColumnaAGridPruebas ctepruebaNUMREPETICION
  .AgregarColumnaAGridPruebas cteResultadoDESCRESULTADO
  .AgregarColumnaAGridPruebas cteResultadoVALRESULTADO
  .AgregarColumnaAGridPruebas cteResultadoESTADO
End With
Exit Sub

AutoanError:
  Call objError.InternalError(App, "Fcargarcolumnas", "fCargarColumnas")
  
End Sub
