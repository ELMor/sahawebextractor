VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Public Estado As Integer                   'Estado del sistema
Dim res As Boolean
Dim strLog As String
Dim strArchivoLog As String

Dim blnAutoEsperaRespuesta As Integer   'Indica si el autoanalizador espera un mensaje de respuesta
Dim blnUltimoRepetido As Integer      'Indica si es el �ltimo frame a enviar
Dim intNumFrameTrans As Integer    'Indica el n�mero de frame que hay que trasmitir
Dim blnFrameRepetido As Integer    'Indica si el frame es repetido
Dim arFrames() As String          'Contiene los frames que forman el mensaje a transmitir completo
Dim strFrameTotal As String        'Recoge los distintos frames libres de caracteres de protocolo
Dim strCodMuestraQuery As String     'C�digo de la muestra enviado por el Electra en un Query
Dim strRackQuery As String         'Posicion de la muestra enviada por el Electra en un Query

Private Sub Electra_Confirmacion(Mensaje As String)
Dim contador1 As Integer
Dim objActuacion As clsActuacion
Dim objMuestra As clsMuestra
On Error Resume Next
        
  TimerEsperaREPLY.Enabled = False
  Select Case Right$(Mensaje, 1)
    Case Chr$(6), Chr$(4) '<ACK> o <EOT> - El autoanalizador ha aceptado el bloque
      '***** HABR�A QUE ESTUDIAR M�S EN PROFUNDIDAD QUE OCURRE EN EL CASO DE QUE EL   *****
      '***** AUTOANALIZADOR RESPONDA CON UN <EOT>                                     *****
      blnFrameRepetido = False
      Mensaje = ""
      If blnUltimoRepetido Then
          objAutoAn.Enviar Chr$(4) '<EOT> - Se cierra la comunicaci�n
          blnAutoEsperaRespuesta = False
          objAutoAn.Estado = cteEspera
'          For Each objActuacion In objAutoAn.ColActuaciones
'              If objActuacion.strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR Then
'                  Call objAutoAn.pModificarRegistro(1, objActuacion.strcodmuestra, objActuacion.strCodAct, objConst.constESTADOPRAUTOACEPTADA)
'              End If
'          Next
      Else
        objAutoAn.Estado = cteMaster
        'Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
        strLog = strLog & " intNumFrameTrans= " & intNumFrameTrans & Chr$(13) & Chr$(10)
        intNumFrameTrans = intNumFrameTrans + 1
        Call pTransmisionFrames
      End If
      For Each objMuestra In objAutoAn.ColMuestras
        If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
          objMuestra.strEstado = cteACEPTADA
          Exit For
        End If
      Next
    Case Chr$(21)   '<NAK> - El autoanalizador no ha aceptado el bloque
      blnFrameRepetido = True
      objAutoAn.Estado = cteMaster
'      For Each objMuestra In objAutoAn.ColMuestras
'        If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
'          objMuestra.strEstado = cteNOACEPTADA
'          Exit For
'        End If
'      Next
      Mensaje = ""
      Call pEnvioMensajes 'Se llama a esta subrutina y no directamene a pTransmisionFrames
      'para poder controlar el n�mero de repeticiones que se hacen
  End Select

End Sub

Private Sub Electra_Esclavo(Mensaje As String)
'Identifica el final del mensaje que se est� recibiendo y procede a su lectura si es le caso.
'El mensaje puede ser enviado en uno o varios frames dependiendo de su longitud
Static intNumFrame As Integer 'Guarda el anterior n�mero de frame
Dim intActualNumFrame As Integer 'Recoge el actal n�mero de frame
Dim strFrame As String 'variable de paso de Mensaje a strFrameTotal
Dim filenum As Integer
    
  TimerAUTO.Enabled = False 'Al llegar datos del autoanalizador, se desactiva el Timer
  If Right$(Mensaje, 1) = Chr$(4) Then '<EOT> - Final de la transmisi�n por parte del autoanalizador
    Mensaje = ""
    objAutoAn.Estado = cteEspera
    strLog = strLog & "Fin de Transmision" & Chr$(13) & Chr$(10)
    'Si el autoanalizador est� esperando un respuesta se contesta al Query
    If blnAutoEsperaRespuesta = True Then pEnvioMensajes 'Se env�a una nueva prueba a programar
    Exit Sub
  End If
  If Right$(Mensaje, 2) = Chr$(13) & Chr$(10) Then '<CR><LF> - Fin del frame.
    'Se elimina el comienzo del mensaje hasta el <STX> inclusive
    strLog = strLog & "Fin de Frame" & Chr$(13) & Chr$(10)
    strFrame = Mid$(Mensaje, InStr(1, Mensaje, Chr$(2)) + 1)
    Mensaje = ""
    'Se lee el n�mero de Frame
    intActualNumFrame = Val(Left$(strFrame, 1))
    'Se comprueba el n�mero frame
    Select Case intActualNumFrame
      Case intNumFrame      'Frame repetido
      Case intNumFrame + 1  'Frame correcto
        objAutoAn.Enviar Chr$(6) '<ACK> - Respuesta afirmativa
        strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ACK>" & Chr$(13) & Chr$(10)
        'Se comprueba si es el �ltimo frame
        If InStr(1, strFrame, Chr$(3)) Then
          'Se actualiza el n�mero del frame
          intNumFrame = 0
          'Se elimina el n�mero de frame y el final del mensaje desde <ETX>
          strFrame = Mid$(strFrame, 2, InStr(1, strFrame, Chr$(3)) - 2)
          strFrameTotal = strFrameTotal & strFrame
          Call pLecturaDatos(strFrameTotal)
          strFrameTotal = ""
        ElseIf InStr(1, strFrame, Chr$(23)) Then
          'Se actualiza el n�mero del frame
          Select Case intActualNumFrame
            Case 7: intNumFrame = -1
            Case Else: intNumFrame = intActualNumFrame
          End Select
          'Se elimina el n�mero de frame y el final del mensaje desde <ETB>
          strFrame = Mid$(strFrame, 2, InStr(1, strFrame, Chr$(23)) - 2)
          strFrameTotal = strFrameTotal & strFrame
        End If
        If objAutoAn.blnDesign Then Simulacion
      Case Else          'Frame incorrecto
        objAutoAn.Enviar Chr$(21) '<NAK> - Respuesta negativa
        strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <NAK>" & Chr$(13) & Chr$(10)
    End Select
    TimerAUTO.Enabled = True
    objAutoAn.EscribirLog strLog
    strLog = ""
    Exit Sub
  End If

End Sub

Private Sub Electra_Espera(Mensaje As String)
'Identifica la llegada de los caracteres de solicitud de
'comunicaci�n, la acepta y pasa al estado de Esclavo.
  If Right$(Mensaje, 1) = Chr$(5) Then '<ENQ> - Solicitud de comunicaci�n por parte del autoanalizador
    objAutoAn.Enviar Chr$(6)  '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
    objAutoAn.Estado = cteESCLAVO
    Me.Refresh
    Mensaje = ""
    TimerAUTO.Enabled = True
    If objAutoAn.blnDesign Then Simulacion
  End If
End Sub

Public Sub Probar()
  pLecturaDatos ""
  objAutoAn.AutoRecepcion Chr$(5)
End Sub

Private Sub Electra_Solicitud(Mensaje As String)
'Respuesta del autoanalizador a la solicitud para establecer una comunicaci�n.
  TimerEsperaREPLY.Enabled = False
  Select Case Right$(Mensaje, 1)
    Case Chr$(6)  '<ACK> - El autoanalizador ha aceptado la comunicaci�n
      objAutoAn.Estado = cteMaster
      Mensaje = ""
      Call pEnvioMensajes 'Se env�a el bloque de datos por parte del sistema al autoanalizador
    Case Chr$(21) '<NAK> - El autoanalizador no ha aceptado la comunicaci�n
      objAutoAn.Estado = cteEspera
      Mensaje = ""
      TimerTrasMENSAJE.Enabled = True 'Se espera 10 segundos para intentar una nueva comunicaci�n
    Case Chr$(5) '<ENQ> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
      objAutoAn.Estado = cteEspera
      Mensaje = ""
  End Select
End Sub

Private Sub pEnvioMensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar _
el caracter <ENQ> y esperar a que el autoanalizador responda afirmativamente. Luego _
se manda el mensaje propiamente dicho hasta que sea recibido correctamente por el _
autoanalizador. Finalmente se cierra la comunicaci�n enviando <EOT>.
Static intContIntentosComunic As Integer 'cuenta el n� de veces que se intenta comunicar con el autoanalizador
Static intContIntentosTrans As Integer 'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
Dim T As Long
Dim res As Integer
Dim msg As String
Dim contador1 As Integer
Dim objMuestra As clsMuestra
On Error Resume Next

  Select Case objAutoAn.Estado
    Case cteEspera 'Se intenta establecer la comunicaci�n
      intContIntentosTrans = 0 'Se inicializa el intContIntentosTrans por si se realiza una nueva comunicaci�n
      intContIntentosComunic = intContIntentosComunic + 1
      If intContIntentosComunic = 7 Then 'Fallo en la comunicaci�n despu�s de 6 intentos
        intContIntentosComunic = 0
        msg = "No se puede establecer comunicaci�n."
        Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
        Case vbRetry
          T = Timer + 5
          Do While T > Timer: res = DoEvents(): Loop
          Sistema_Espera
          intContIntentosComunic = intContIntentosComunic + 1
          Exit Sub
        Case vbCancel
          objAutoAn.Enviar Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
          objAutoAn.Estado = cteEspera 'Se pasa al estado de espera
          strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
          Exit Sub
        End Select
      Else
        Sistema_Espera
        Exit Sub
      End If
    Case cteMaster 'Se intenta transmitir los datos
      intContIntentosComunic = 0 'Se inicializa el intContIntentosComunic por si se realiza una nueva comunicaci�n
      intContIntentosTrans = intContIntentosTrans + 1
      If intContIntentosTrans = 7 Then 'Fallo en la transmisi�n despu�s de 6 intentos
        intContIntentosTrans = 0
        objAutoAn.Estado = cteEspera
        strLog = strLog & "ESTADO ESPERA"
        msg = "No se puede transmitir informaci�n."
        Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
          Case vbRetry
            T = Timer + 5
            Do While T > Timer: res = DoEvents(): Loop
            Sistema_Espera
            intContIntentosComunic = intContIntentosComunic + 1
            Exit Sub
          Case vbCancel
            objAutoAn.Enviar Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> " & Chr$(13) & Chr$(10)
            'Se actualiza el grid
'                For contador1 = 1 To UBound(arActuaciones)
'                    If arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR Then
'                        arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOSINPROGRAMAR
'                        arActuaciones(contador1).strPosicion = ""
'                        ssdgPruebas.MoveFirst
'                        ssdgPruebas.MoveRecords contador1 - 1
'                        ssdgPruebas.Columns("Copa").Text = ""
'                        ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
'                    End If
'                Next contador1
            
            For Each objMuestra In objAutoAn.ColMuestras
'              If objActuacion.strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR Then
'                Call objAutoAn.pModificarMuestra(objConst.ConstCABESTADO, objMuestra.strcodmuestra, objConst.constESTADOPRAUTOACEPTADA)
'                Call objAutoAn.pModificarMuestra(objConst.ConstCABCOPA, objMuestra.strcodmuestra, "")
'              End If
              If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
                
              End If
            Next
            objAutoAn.Estado = cteEspera 'Se pasa al estado de espera
            Exit Sub
        End Select
      Else
        If blnFrameRepetido = False Then Sistema_Master _
        Else Call pTransmisionFrames
        'Call objAutoAn.PanelCaption("Comunicacion", "CONFIRMACI�N " & intContIntentosTrans)
        Exit Sub
      End If
  End Select

End Sub

Private Sub pEstablecerCheck(Mensaje As String)
'Subrutina que establece el check del mensaje que ser� enviado al autoanalizador.
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
Dim suma As Integer         'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal

  suma = 0
  For contador = 1 To Len(Mensaje)
    suma = suma + Asc(Mid$(Mensaje, contador, 1))
    If suma >= 256 Then
      suma = suma - 256
    End If
  Next contador
  If Len(Hex$(suma)) = 1 Then
    Mensaje = Mensaje & "0" & Hex$(suma)
  Else
    Mensaje = Mensaje & Hex$(suma)
  End If
End Sub

Public Sub Lectura_Protocolo(caracter As String)
Static Mensaje As String   'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es _
la que se env�a a otras subrutinas pudiendo ser modificada en �stas. Al ser modificada _
la copia, tambi�n deber� modificarse el original. El hecho de utilizar esta copia _
se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas _
por estar declarada como Static en esta subrutina
    
  'Se va componiendo el mensaje
  Mensaje = Mensaje & caracter
  CopiaMensaje = Mensaje
    
  'Se acude a la subrutina correspondiente para la lectura del mensaje seg�n el estado.
  Select Case objAutoAn.Estado
    Case cteEspera
      Electra_Espera CopiaMensaje
      Mensaje = CopiaMensaje
    Case cteSolicitud
      Electra_Solicitud CopiaMensaje
      Mensaje = CopiaMensaje
    Case cteESCLAVO
      Electra_Esclavo CopiaMensaje
      Mensaje = CopiaMensaje
    Case cteConfirmacion
      Electra_Confirmacion CopiaMensaje
      Mensaje = CopiaMensaje
  End Select

End Sub

Public Sub pLecturaDatos(datos As String)
'Se leen los mensajes que env�a el autoanalizador. Solo se tendr�n en cuenta los mensajes _
    Query y los de Resultados.
Dim contador1 As Integer
Dim Resultado As String
Dim i As Integer
Dim filenum As Integer
Dim strIDMuestra As String
Dim strCodActAuto As String
Dim dimension As Integer
Dim colResultados As New Collection
Dim CodMuestra As String
    
    
    
'objAutoAn.colmuestras("AAY-6455").strurgente = IIf(objAutoAn.colmuestras("AAY-6455").strurgente = "NO", "SI", "NO")

'Exit Sub

'datos = "H|\^&" & Chr$(13)
'datos = datos & "P|1|0007276422" & Chr$(13)
''Call objAutoAn.pModificarRegistro(objConst.ConstCABESTADO, "AAY-6455", "1085", objConst.constESTADOPRAUTOACEPTADA)
'
'
'datos = datos & "O|1||A02|^^^001^S|||||||||||||||||199809210849|199809210849||F|F" & Chr$(13)
'datos = datos & "R|1|^^^001^0||||||F^3^0" & Chr$(13)
'datos = datos & "R|2|^^^001^1|4.9|||||F^1^0" & Chr$(13)
'datos = datos & "R|3|^^^001^2|4.7|||||F^1^0" & Chr$(13)
'datos = datos & "R|4|^^^001^3|4.8|||||F^1^0" & Chr$(13)
'datos = datos & "R|5|^^^001^4|366|2||||F^1^0" & Chr$(13)
'datos = datos & "C|1|I|1^005" & Chr$(13)
'datos = datos & "C|2|I|2" & Chr$(13)
'datos = datos & "L|1" & Chr$(13)

'datos = "H|\^&" & vbCr
'datos = datos & "P|1|0007839997" & vbCr
'datos = datos & "O|1||G02|^^^001^S|||||||||||||||||199909281103|199909281103||F|F" & vbCr
'datos = datos & "R|1|^^^001^0||||||F^4^0" & vbCr
'datos = datos & "R|2|^^^001^1|12.1|||||F^1^0" & vbCr
'datos = datos & "R|3|^^^001^2|12.1|||||F^1^0" & vbCr
'datos = datos & "R|4|^^^001^3|12.1|||||F^1^0" & vbCr
'datos = datos & "R|5|^^^001^4|100|1||||F^1^0" & vbCr
'datos = datos & "C|1|I|1^001" & vbCr
'datos = datos & "R|6|^^^001^5|1.00|||||F^1^0" & vbCr
'datos = datos & "C|1|I|2" & vbCr
'datos = datos & "L|1" & vbCr

  'Se separan los distintos tipos de registros del mensaje
  awk1 = datos
  awk1.FS = Chr$(13)
  strLog = strLog & "Awk1=Datos=" & datos & Chr$(13) & Chr$(10)
    'El awk1.F(1) contiene la cabecera (registro tipo H) y el awk1.F(awk1.NF) el terminador
    '(registro tipo L) que no sirven para nada.
    'El awk1.F(2) nos indica que tipo de mensaje es: registro tipo P (resultados) o registro tipo
    'Q (query)
  Select Case UCase$(Left$(awk1.F(2), 1))
    Case "P"
      blnAutoEsperaRespuesta = False
      Awk2.FS = "|"
      
      'Se obtiene la ID de la muestra
      Awk2 = awk1.F(2)
      strIDMuestra = Awk2.F(3)
      If Len(strIDMuestra) = 8 And Not IsNumeric(strIDMuestra) = 0 Then
        strIDMuestra = objAutoAn.fCodigoANumero(strIDMuestra)
      End If
      CodMuestra = objAutoAn.fNumeroACodigo(strIDMuestra)
      If objAutoAn.blnExistMuestra(CodMuestra) Then
        'Se obtiene el c�digo de la prueba
        Awk2 = awk1.F(3)
        Awk3.FS = "^"
        Awk3 = Awk2.F(5)
        strCodActAuto = Awk3.F(4)
        'Se recogen los resultados. Los comentarios no se tienen en cuenta por ahora
        For contador1 = 4 To awk1.NF
          If UCase$(Left$((awk1.F(contador1)), 1)) = "R" Then
            Awk2 = awk1.F(contador1)
            Resultado = IIf(Awk2.F(4) <> "", Awk2.F(4), "*****")
            'If Awk2.F(4) <> "" Then 'si el resultado no es null
            Awk3.FS = "^"
            Awk3 = Awk2.F(3)
            'Set objResultado = New clsValorResultado
            'With objResultado
            '  .strCodResultAuto = Awk3.F(5) 'awk2.F(2)   'codigo
            '  .strCodActAuto = strCodActAuto
            '  .strcodmuestra = CodMuestra
            '  resultado = Awk2.F(4)
              'i = objAutoAn.fFormatearNumero(resultado, ".", ",")
              '.strResultado = resultado   'resultado
            'End With
            'colResultados.Add objResultado
            'Set objResultado = Nothing
            Call objAutoAn.pIntroResultConCResAuto(CodMuestra, strCodActAuto & "-" & Awk3.F(5), Resultado, True)
            'End If
          End If
        Next contador1
      End If
      'Se actualiza la base de datos
      'Call objAutoAn.pActualizarDB(colResultados)
    Case "Q"
      Awk2.FS = "|"
      Awk2 = awk1.F(2)
      
      'Solo nos interesa el c�digo de la muestra y la posicion de esta en el rack
      Awk3.FS = "^"
      Awk3 = Awk2.F(3)
      
      'Se guarda el c�digo de la muestra en formato enviado por el autoanalizador
      strCodMuestraQuery = Awk3.F(1)
      strRackQuery = Awk3.F(2)
      
      CodMuestra = objAutoAn.fNumeroACodigo(strCodMuestraQuery)
      If objAutoAn.blnExistMuestra(CodMuestra) Then
        With objAutoAn.ColMuestras(CodMuestra)
          .strEstado = cteLEIDA
          .strCopa = strRackQuery
        End With
      End If
      'El autoanalizador estar� esperando una respuesta a su Query
      blnAutoEsperaRespuesta = True
  End Select

  objAutoAn.EscribirLog strLog
  strLog = ""

End Sub

Public Sub Salir()
Dim contador As Integer
Dim resp As Integer
    
  strLog = strLog & "Pulsado salir" & Chr$(13) & Chr$(10)
    'Se comprueba si existe alguna prueba que no tiene resultados y en ese caso se _
    cambia su estado a Prueba Extra�da.
'    For contador = 1 To UBound(arActuaciones)
'        If arActuaciones(contador).strEstado <> objConst.constESTADOPRAUTOREALIZADA Then
'            resp = objAutoAn.fPreguntarVolverPruebasAExtraidas()
'            If resp = vbCancel Then Exit Sub
'            Exit For
'        End If
'    Next contador
'
    'El form_unload s�lo se ejecuta si no se ha cancelado
    'Form_Unload (False)
  End
End Sub

Public Sub Descargar()
  objAutoAn.EscribirLog strLog & Chr$(13) & Chr$(10) & "Fin de Ejecucion"
  Set objAutoAn = Nothing
  End
End Sub

Private Sub Sistema_Espera()
'Se env�a el caracter de solicitud de comunicaci�n y se pasa a estado de Solicitud
  objAutoAn.Enviar Chr$(5) 'Env�o de <ENQ>, se "solicita l�nea"
  objAutoAn.Estado = cteSolicitud
  'panEstado.Caption = objAutoAn.fLeerEstado(estado)  'Especificado en pEnvioMensajes
  TimerEsperaREPLY.Enabled = True
  strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <ENQ> ESTADO SOLICITUD" & Chr$(13) & Chr$(10)
  If objAutoAn.blnDesign Then objAutoAn.AutoRecepcion Chr$(6)
End Sub

Private Sub Sistema_Master()
'Se forma el mensaje de respuesta al Query.
Dim strRegistro_H As String
Dim strRegistro_P As String
Dim strRegistro_O As String
Dim arRegistro_O() As String
Dim dimension As Integer
Dim strRegistro_L As String
Dim intNumeroDeFrames As Integer
Dim contador1 As Integer
Dim contador2 As Integer
Dim msg As String
Dim objActuacion As clsActuacion
Dim objMuestra As clsMuestra
    
    'Registro cabecera tipo H
    strRegistro_H = "H|\^&" & Chr$(13)

   
    'Registros de ordenes de pruebas tipo O
    'Se crear�n tantos registros de orden tipo O como pruebas existan para la muestra
    dimension = 0
'    For contador1 = 1 To UBound(arActuaciones)
'        If arActuaciones(contador1).strIDMuestra = strCodMuestraQuery Then
'            dimension = dimension + 1
'            ReDim Preserve arRegistro_O(1 To dimension)
'            msg = "O|" & dimension & "||" & strRackQuery & "|^^^" & arActuaciones(contador1).strCodActAuto & "|"
'            If arActuaciones(contador1).blnUrgente = True Then
'                msg = msg & "S|"
'            Else
'                msg = msg & "|"
'            End If
'            msg = msg & "|||||P||||||||||||||Q" & Chr$(13)
'            arRegistro_O(dimension) = msg
'            'se actualiza el arActuaciones y el grid
'            arActuaciones(contador1).strPosicion = strRackQuery
'            arActuaciones(contador1).strEstado = objConst.constESTADOPRAUTOINTENTOPROGRAMAR
'            ssdgPruebas.MoveFirst
'            ssdgPruebas.MoveRecords contador1 - 1
'            ssdgPruebas.Columns("Copa").Text = arActuaciones(contador1).strPosicion
'            ssdgPruebas.Columns("Estado").Text = arActuaciones(contador1).strEstado
'        End If
'    Next contador1
    For Each objMuestra In objAutoAn.ColMuestras
      With objMuestra
        If .strEstado = cteLEIDA Then
          'Registro de informaci�n del paciente tipo P
          strRegistro_P = "P|1|" & .strIDMuestra & Chr$(13)
          For Each objActuacion In .ColMuestrasPruebas
            'If objActuacion.strIDMuestra = strCodMuestraQuery Then
            dimension = dimension + 1
            'ReDim Preserve arRegistro_O(1 To dimension)
            strRegistro_O = strRegistro_O & "O|" & dimension & "||" _
            & .strCopa & "|^^^" & objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto & "|" _
            & IIf(objActuacion.blnUrgente, "S|", "|") _
            & "|||||P||||||||||||||Q" & Chr$(13)
            'arRegistro_O(dimension) = msg
            'se actualiza el arActuaciones y el grid
  '          Call objAutoAn.pModificarRegistro(objConst.ConstCABCOPA, objActuacion.strcodmuestra, objActuacion.strCodAct, strRackQuery)
  '          Call objAutoAn.pModificarRegistro(objConst.ConstCABCOPA, objActuacion.strcodmuestra, objActuacion.strCodAct, objConst.constESTADOPRAUTOINTENTOPROGRAMAR)
            'End If
          Next
          .strEstado = cteINTENTOPROGRAMAR
          Exit For
        End If
      End With
    Next
    
'    If dimension = 0 Then 'no existe nada pedido para la muestra
'        dimension = dimension + 1
'        ReDim Preserve arRegistro_O(1 To dimension)
'        arRegistro_O(dimension) = "O|1||" & strRackQuery & "|^^^000|||||||P||||||||||||||Z" & Chr$(13)
'    End If

    'Registro terminador tipo L
    strRegistro_L = "L|1|I" & Chr$(13)

    'Se juntan todos los registros en uno solo (se emplea la variable strRegistro_H)
    If strRegistro_P = "" Then strRegistro_P = "P|1|" & strCodMuestraQuery & Chr$(13)
    If strRegistro_O = "" Then strRegistro_O = "O|1||" & strRackQuery & "|^^^000|||||||P||||||||||||||Z" & Chr$(13)
    strRegistro_H = strRegistro_H & strRegistro_P & strRegistro_O & strRegistro_L
    
'    For contador1 = 1 To UBound(arRegistro_O)
'        strRegistro_H = strRegistro_H & arRegistro_O(contador1)
'    Next contador1
'    strRegistro_H = strRegistro_H & strRegistro_L

    'Se descompone el mensaje en frames
    If Len(strRegistro_H) / 240 = Int(Len(strRegistro_H) / 240) Then
      intNumeroDeFrames = Int(Len(strRegistro_H) / 240)
    Else
      intNumeroDeFrames = Int(Len(strRegistro_H) / 240) + 1
    End If
    ReDim arFrames(1 To intNumeroDeFrames)
    contador2 = 0
    For contador1 = 1 To intNumeroDeFrames
      arFrames(contador1) = Mid$(strRegistro_H, 1 + (contador1 - 1) * 240, 240)
      'Se completan los frames con los caracteres de transmision
      contador2 = contador2 + 1
      If contador2 = 8 Then contador2 = 0
      arFrames(contador1) = contador2 & arFrames(contador1) & IIf(contador1 = UBound(arFrames), Chr$(3), Chr$(23))
      Call pEstablecerCheck(arFrames(contador1))
      arFrames(contador1) = Chr$(2) & arFrames(contador1) & Chr$(13) & Chr$(10)
    Next contador1

    'Se transmiten los frames
    intNumFrameTrans = 1
    'strLog = strLog & " intNumFrameTrans= 1" & Chr$(13) & Chr$(10)
    Call pTransmisionFrames

End Sub

Public Sub pModificaPruebaAuto(objPruebaAuto As clsPruebaAuto)
Dim objResultAuto As clsResultAuto
  If Not objAutoAn.blnDesign Then
    For Each objResultAuto In objPruebaAuto.ColResultAuto
      objResultAuto.strCodResultAuto = objPruebaAuto.strCodActAuto _
      & "-" & objResultAuto.strCodResultAuto
    Next
  End If
End Sub


Private Sub TimerAUTO_Timer()
'Controla si se ha producido un TimeOut por parte del autoanalizador. Esto ocurre cuando _
    estando el ordenador en estado de esclavo pasan 30 seg. sin recibir ning�n dato.
Dim msg As String
Dim resp As Integer

  TimerAUTO.Enabled = False
  If objAutoAn.Estado = cteESCLAVO Then
    msg = "Se ha producido un Timeout durante la comunicaci�n por parte del autoanalizador."
    msg = msg & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
    resp = MsgBox(msg, vbExclamation + vbYesNo, Me.Caption)
    Select Case resp
      Case vbYes
        TimerAUTO.Enabled = True
      Case vbNo
        For Each objMuestra In objAutoAn.ColMuestras
          If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
            objMuestra.strEstado = cteNOACEPTADA
            Exit For
          End If
        Next
        strFrameTotal = ""
        objAutoAn.Enviar Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
        objAutoAn.Estado = cteEspera 'Se pasa al estado de espera
        strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
        'Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
        Exit Sub
    End Select
  End If
End Sub

Private Sub TimerEsperaREPLY_Timer()
'Controla los TimeOut que produce el autoanalizador cuando el ordenador est� a la espera _
    de una respuesta
Dim msg As String
Dim resp As Integer
    
  TimerEsperaREPLY.Enabled = False
  Select Case objAutoAn.Estado
    Case cteSolicitud, cteConfirmacion
      msg = "Se ha producido un Timeout durante la comunicaci�n por parte del autoanalizador."
      msg = msg & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
      resp = MsgBox(msg, vbExclamation + vbYesNo, Me.Caption)
      Select Case resp
        Case vbYes
          TimerEsperaREPLY.Enabled = True
        Case vbNo
          objAutoAn.Enviar Chr$(4) '<EOT> Se interrumpe el intento de comunicaci�n
          objAutoAn.Estado = cteEspera 'Se pasa al estado de espera
          strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
          'Call objAutoAn.PanelCaption("Comunicacion", objAutoAn.fLeerEstado(Estado))
          Exit Sub
      End Select
    Case Else
      msg = "Error de programaci�n. Revisar el c�digo. El TimeoOut que se ha producido solo "
      msg = msg & "deb�a producirse en el estado de SOLICITUD o CONFIRMACION"
      MsgBox msg
  End Select
End Sub

Private Sub TimerTrasMENSAJE_Timer()
'Controla los tiempos entre env�os de solicitudes
  TimerTrasMENSAJE.Enabled = False
  Call pEnvioMensajes
End Sub

Private Sub pTransmisionFrames()
'Se transmiten los frames teniendo en cuenta las respuestas del autoanalizador.
Dim contador1 As Integer
Dim SQL As String

  blnUltimoRepetido = (intNumFrameTrans = UBound(arFrames))
  objAutoAn.Enviar arFrames(intNumFrameTrans)
  objAutoAn.Estado = cteConfirmacion
  strLog = strLog & Chr$(13) & Chr$(10) & " ESTADO CONFIRMACION" & Chr$(13) & Chr$(10)
  strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: " & arFrames(intNumFrameTrans) & Chr$(13) & Chr$(10)
  TimerEsperaREPLY.Enabled = True
  objAutoAn.AutoRecepcion Chr$(6)
End Sub

Private Sub Simulacion()
Static nVez As Integer

  nVez = nVez + 1
  Select Case nVez 'Chr$(2) & "H|\^&" & vbcr & "P|1|0007758284" & vbcr & "O|1||A02|^^^001^S|||||||||||||||||199906300949|199906300949||F|F" & vbcr & "R|1|^^^001^0||||||F^4^0" & vbcr & "R|2|^^^001^1|11.2|||||F^1^0" & vbcr & "R|3|^^^001^2|10.9|||||F^1^0" & vbcr & "R|4|^^^001^3|11.1|||||F^1^0" & vbcr & "R|5|^^^001^4|118|1||||F^2^0" & vbcr & "C|1|I|1^001" & vbcr & "R|6|^^^001^5|0.92|||||F^1^0" & vbcr &"C|1|I|2" & vbcr & "L|1" & vbcr
    Case 1: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007844483" & vbCr & "O|1||D06|^^^001^S|||||||||||||||||199910010911|199910010911||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|10.6|||||F^1^0" & vbCr & "R|3|^^^001^2|10.6|||||F^1^0" & vbCr & "R|4|^^^001^3|10.6|||||F^1^0" & vbCr & "R|5|^^^001^4||1||||F^3^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^00" & Chr$(3) & "BF" & vbCrLf
    Case 2: objAutoAn.AutoRecepcion Chr$(2) & "21^5|0.88|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "95" & vbCrLf
    Case 3: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007844483" & vbCr & "O|1||D06|^^^003^S|||||||||||||||||199910010915|199910010915||F|F" & vbCr & "R|1|^^^003^0||||||F^4^0" & vbCr & "R|2|^^^003^1|26.4|||||F^1^0" & vbCr & "R|3|^^^003^2|26.6|||||F^1^0" & vbCr & "R|4|^^^003^3|26.5|||||F^1^0" & vbCr & "R|5|^^^003^5|0.91|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "86" & vbCrLf
    Case 1: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007824479^E01||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A7" & vbCrLf & Chr$(4)
    Case 2: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007839964^E02||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "AF" & vbCrLf & Chr$(4)
    Case 3: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007839997^E03||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A9" & vbCrLf & Chr$(4)
    Case 4: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007840025^E04||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "AC" & vbCrLf & Chr$(4)
    Case 5: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007840115^E05||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "AA" & vbCrLf & Chr$(4)
    Case 6: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007824479" & vbCr & "O|1||G01|^^^001^S|||||||||||||||||199909281103|199909281103||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|13.5|||||F^1^0" & vbCr & "R|3|^^^001^2|13.3|||||F^1^0" & vbCr & "R|4|^^^001^3|13.4|||||F^1^0" & vbCr & "R|5|^^^001^4|83.4|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^" & Chr$(23) & "88" & vbCrLf
    Case 7: objAutoAn.AutoRecepcion Chr$(2) & "2^^001^5|1.11|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf
    Case 8: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007839964" & vbCr & "O|1||G02|^^^001^S|||||||||||||||||199909281103|199909281103||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|12.1|||||F^1^0" & vbCr & "R|3|^^^001^2|12.1|||||F^1^0" & vbCr & "R|4|^^^001^3|12.1|||||F^1^0" & vbCr & "R|5|^^^001^4|100|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^" & Chr$(23) & "97" & vbCrLf
    Case 9: objAutoAn.AutoRecepcion Chr$(2) & "2^001^5|1.00|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "44" & vbCrLf
    Case 10: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & " P|1|0007839997" & vbCr & "O|1||G03|^^^001^S|||||||||||||||||199909281103|199909281103||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|13.1|||||F^1^0" & vbCr & "R|3|^^^001^2|12.8|||||F^1^0" & vbCr & "R|4|^^^001^3|13.0|||||F^1^0" & vbCr & "R|5|^^^001^4|87.9|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^" & Chr$(23) & "87" & vbCrLf
    Case 11: objAutoAn.AutoRecepcion Chr$(2) & "2^^001^5|1.07|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A9" & vbCrLf
'      Case 1: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758277^A01||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 2: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758284^A02||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 3: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758376^A03||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 4: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758277" & vbCr & "O|1||A01|^^^004^S|||||||||||||||||199906300949|199906300949||F|F" & vbCr & "R|1|^^^004^0||||||F^3^0" & vbCr & "R|2|^^^004^1|4.0|||||F^1^0" & vbCr & "R|3|^^^004^2|3.9|||||F^1^0" & vbCr & "R|4|^^^004^3|4.0|||||F^1^0" & vbCr & "R|5|^^^004^4|448|2||||F^1^0" & vbCr & "C|1|I|1^005" & vbCr & "C|2|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 5: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758284" & vbCr & "O|1||A02|^^^001^S|||||||||||||||||199906300949|199906300949||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|11.2|||||F^1^0" & vbCr & "R|3|^^^001^2|10.9|||||F^1^0" & vbCr & "R|4|^^^001^3|11.1|||||F^1^0" & vbCr & "R|5|^^^001^4|118|1||||F^2^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|0.92|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 6: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758284" & vbCr & "O|1||A02|^^^004^S|||||||||||||||||199906300954|199906300954||F|F" & vbCr & "R|1|^^^004^0||||||F^3^0" & vbCr & "R|2|^^^004^1|5.9|||||F^1^0" & vbCr & "R|3|^^^004^2|5.9|||||F^1^0" & vbCr & "R|4|^^^004^3|5.9|||||F^1^0" & vbCr & "R|5|^^^004^4|290|2||||F^1^0" & vbCr & "C|1|I|1^005" & vbCr & "C|2|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 7: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758284" & vbCr & "O|1||A02|^^^003^S|||||||||||||||||199906300953|199906300953||F|F" & vbCr & "R|1|^^^003^0||||||F^4^0" & vbCr & "R|2|^^^003^1|27.8|||||F^1^0" & vbCr & "R|3|^^^003^2|28.1|||||F^1^0" & vbCr & "R|4|^^^003^3|28.0|||||F^1^0" & vbCr & "R|5|^^^003^5|0.97|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 8: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758376" & vbCr & "O|1||A03|^^^001^S|||||||||||||||||199906300949|199906300949||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|12.7|||||F^1^0" & vbCr & "R|3|^^^001^2|12.5|||||F^1^0" & vbCr & "R|4|^^^001^3|12.6|||||F^1^0" & vbCr & "R|5|^^^001^4|92.9|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|1.04|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 9: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758376" & vbCr & "O|1||A03|^^^003^S|||||||||||||||||199906300953|199906300953||F|F" & vbCr & "R|1|^^^003^0||||||F^4^0" & vbCr & "R|2|^^^003^1|33.6|||||F^1^0" & vbCr & "R|3|^^^003^2|33.6|||||F^1^0" & vbCr & "R|4|^^^003^3|33.6|||||F^1^0" & vbCr & "R|5|^^^003^5|1.16|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 10: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758499^K01||||||||||O" & vbCr & "L|1 & vbcr" & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 11: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758499" & vbCr & "O|1||K01|^^^001|S||||||P||||||||||||||Q" & vbCr & "O|2||K01|^^^003|S||||||P||||||||||||||Q" & vbCr & "O|3||K01|^^^005|S||||||P||||||||||||||Q" & vbCr & "O|4||K01|^^^004|S||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 12: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758499" & vbCr & "O|1||K01|^^^001|S||||||P||||||||||||||Q" & vbCr & "O|2||K01|^^^003|S||||||P||||||||||||||Q" & vbCr & "O|3||K01|^^^005|S||||||P||||||||||||||Q" & vbCr & "O|4||K01|^^^004|S||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 13: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758406" & vbCr & "O|1||A04|^^^001|||||||P||||||||||||||Q" & vbCr & "O|2||A04|^^^003|||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 14: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758431^A05||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 15: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758431" & vbCr & "O|1||A05|^^^001|||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 16: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758499" & vbCr & "O|1||K01|^^^005^S|||||||||||||||||199906300959|199906300959||F|F" & vbCr & "R|1|^^^005^0||||||F^3^0" & vbCr & "R|2|^^^005^1|13.6|||||F^1^0" & vbCr & "R|3|^^^005^2|16.5|||||F^1^0" & vbCr & "R|4|^^^005^3|15.1|||||F^1^0" & vbCr & "R|5|^^^005^5|0.79|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 17: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758499" & vbCr & "O|1||K01|^^^001^S|||||||||||||||||199906301000|199906301000||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|12.8|||||F^1^0" & vbCr & "R|3|^^^001^2|12.6|||||F^1^0" & vbCr & "R|4|^^^001^3|12.7|||||F^1^0" & vbCr & "R|5|^^^001^4|91.6|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|1.05|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 18: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758499" & vbCr & "O|1||K01|^^^003^S|||||||||||||||||199906301003|199906301003||F|F" & vbCr & "R|1|^^^003^0||||||F^4^0" & vbCr & "R|2|^^^003^1|40.6|||||F^1^0" & vbCr & "R|3|^^^003^2|40.8|||||F^1^0" & vbCr & "R|4|^^^003^3|40.7|||||F^1^0" & vbCr & "R|5|^^^003^5|1.40|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 19: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758499" & vbCr & "O|1||K01|^^^004^S|||||||||||||||||199906301003|199906301003||F|F" & vbCr & "R|1|^^^004^0||||||F^3^0" & vbCr & "R|2|^^^004^1|6.9|||||F^1^0" & vbCr & "R|3|^^^004^2|6.7|||||F^1^0" & vbCr & "R|4|^^^004^3|6.8|||||F^1^0" & vbCr & "R|5|^^^004^4|248|2||||F^1^0" & vbCr & "C|1|I|1^005" & vbCr & "C|2|I|2" & vbCr & "L|1 & vbcr" & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 20: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758406" & vbCr & "O|1||A04|^^^001^S|||||||||||||||||199906301005|199906301005||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|12.6|||||F^1^0" & vbCr & "R|3|^^^001^2|12.2|||||F^1^0" & vbCr & "R|4|^^^001^3|12.4|||||F^1^0" & vbCr & "R|5|^^^001^4|95.6|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|1.02|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 21: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758406" & vbCr & "O|1||A04|^^^003^S|||||||||||||||||199906301007|199906301007||F|F" & vbCr & "R|1|^^^003^0||||||F^4^0" & vbCr & "R|2|^^^003^1|30.2|||||F^1^0" & vbCr & "R|3|^^^003^2|29.4|||||F^1^0" & vbCr & "R|4|^^^003^3|29.8|||||F^1^0" & vbCr & "R|5|^^^003^5|1.03|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 22: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758431" & vbCr & "O|1||A05|^^^001^S|||||||||||||||||199906301005|199906301005||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|12.8|||||F^1^0" & vbCr & "R|3|^^^001^2|12.9|||||F^1^0" & vbCr & "R|4|^^^001^3|12.9|||||F^1^0" & vbCr & "R|5|^^^001^4|89.1|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|1.07|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 23: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758445^A01||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 24: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758445" & vbCr & "O|1||A01|^^^001|||||||P||||||||||||||Q" & vbCr & "O|2||A01|^^^003|||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 25: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758445" & vbCr & "O|1||A01|^^^001|||||||P||||||||||||||Q" & vbCr & "O|2||A01|^^^003|||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 26: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758449^A02||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 27: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758449" & vbCr & "O|1||A02|^^^001|||||||P||||||||||||||Q" & vbCr & "O|2||A02|^^^003|||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 28: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758449" & vbCr & "O|1||A02|^^^001|||||||P||||||||||||||Q" & vbCr & "O|2||A02|^^^003|||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 29: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758434^A03||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'
'      Case 30: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758434" & vbCr & "O|1||A03|^^^001|||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 31: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "Q|1|0007758682^A04||||||||||O" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 32: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758682" & vbCr & "O|1||A04|^^^001|S||||||P||||||||||||||Q" & vbCr & "L|1|I" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 33: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758445" & vbCr & "O|1||A01|^^^001^S|||||||||||||||||199906301100|199906301100||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|11.1|||||F^1^0" & vbCr & "R|3|^^^001^2|10.9|||||F^1^0" & vbCr & "R|4|^^^001^3|11.0|||||F^1^0" & vbCr & "R|5|^^^001^4|120|1||||F^2^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|0.91|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 34: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758445" & vbCr & "O|1||A01|^^^003^S|||||||||||||||||199906301104|199906301104||F|F" & vbCr & "R|1|^^^003^0||||||F^4^0" & vbCr & "R|2|^^^003^1|32.5|||||F^1^0" & vbCr & "R|3|^^^003^2|32.2|||||F^1^0" & vbCr & "R|4|^^^003^3|32.4|||||F^1^0" & vbCr & "R|5|^^^003^5|1.12|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 35: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758449" & vbCr & "O|1||A02|^^^001^S|||||||||||||||||199906301100|199906301100||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|11.7|||||F^1^0" & vbCr & "R|3|^^^001^2|11.8|||||F^1^0" & vbCr & "R|4|^^^001^3|11.8|||||F^1^0" & vbCr & "R|5|^^^001^4|105|1||||F^2^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|0.98|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 36: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758449" & vbCr & "O|1||A02|^^^003^S|||||||||||||||||199906301104|199906301104||F|F" & vbCr & "R|1|^^^003^0||||||F^4^0" & vbCr & "R|2|^^^003^1|30.5|||||F^1^0" & vbCr & "R|3|^^^003^2|29.9|||||F^1^0" & vbCr & "R|4|^^^003^3|30.2|||||F^1^0" & vbCr & "R|5|^^^003^5|1.04|||||F^2^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 37: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758434" & vbCr & "O|1||A03|^^^001^S|||||||||||||||||199906301105|199906301105||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|38.8|||||F^1^0" & vbCr & "R|3|^^^001^2|37.2|||||F^1^0" & vbCr & "R|4|^^^001^3|38.0|||||F^1^0" & vbCr & "R|5|^^^001^4|19.7|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|3.14|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
'      Case 38: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&" & vbCr & "P|1|0007758682" & vbCr & "O|1||A04|^^^001^S|||||||||||||||||199906301104|199906301104||F|F" & vbCr & "R|1|^^^001^0||||||F^4^0" & vbCr & "R|2|^^^001^1|28.1|||||F^1^0" & vbCr & "R|3|^^^001^2|27.9|||||F^1^0" & vbCr & "R|4|^^^001^3|28.0|||||F^1^0" & vbCr & "R|5|^^^001^4|28.5|1||||F^1^0" & vbCr & "C|1|I|1^001" & vbCr & "R|6|^^^001^5|2.31|||||F^1^0" & vbCr & "C|1|I|2" & vbCr & "L|1" & vbCr & Chr$(3) & "A4" & vbCrLf & Chr$(4)
  End Select
End Sub

?????????????????????????????????????????????????????????
Dim strLog As String
Dim blnFrameRepetido As Integer 'Indica si el frame es repetido
Const ConstRegH = "H|\^&|||Inmulite||||8N1|||P|1|"
Const ConstRegP = "P|1||||Nombre Paciente|||||||||||||||||||||||||||||||"
Const ConstRegO = "|||"
Const ConstRegL = "L|1|N"
Dim Colmensaje As New Collection



Private Sub pEstablecerCheck(Mensaje As String)
'Subrutina que establece el check del mensaje que ser� enviado al autoanalizador.
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
Dim suma As Integer         'suma de los c�digos ASCII, m�dulo 256, de los caracteres del bloque en decimal
On Error GoTo Autoanerror

  suma = 0
  For contador = 1 To Len(Mensaje)
    suma = suma + Asc(Mid$(Mensaje, contador, 1))
    If suma >= 256 Then suma = suma - 256
  Next contador
  If Len(Hex$(suma)) = 1 Then
    Mensaje = Mensaje & "0" & Hex$(suma)
  Else
    Mensaje = Mensaje & Hex$(suma)
  End If
  Exit Sub

Autoanerror:
    Call objError.InternalError(Me, "pEstablecerCheck", "pEstablecerCheck")
End Sub
Private Sub pTransmisionFrames()
'Se transmiten los frames teniendo en cuenta las respuestas del autoanalizador
'Se transmiten los frames de uno en uno.Cuando se recibe confirmacion _
se eliminan de la coleccion.
On Error GoTo Autoanerror

If colmensjaes.Count > 0 Then
  objAutoAn.Enviar Colmensaje(1).strMensaje
Else
  objAutoAn.Enviar Chr$(4)
End If
objAutoAn.Estado = cteConfirmacion
TimerEsperaREPLY.Enabled = True
objAutoAn.AutoRecepcion Chr$(6)  '<ACK>
Exit Sub

Autoanerror:
  Call objError.InternalError(Me, "pTransmisionFrames", "pTransmisionFrames")

  
End Sub
Private Sub pAddMensaje(strMensaje As String, Optional Muestra As clsMuestra)
Static NumeroMensaje As Integer
Dim Mensaje As String
Dim objMensaje As New clsMensaje
On Error GoTo Autoanerror

NumeroMensaje = NumeroMensaje + 1
Mensaje = NumeroMensaje & strMensaje & vbCr & Chr$(3)
pEstablecerCheck Mensaje
Mensaje = Chr$(2) & Mensaje & vbCrLf
objMensaje.strMensaje = Mensaje

If Not (Muestra Is Nothing) Then
 Set objMensaje.Muestra = Muestra
End If
'A�adir el nuevo mensaje que hemos creado a la coleccion
Colmensaje.Add objMensaje
Exit Sub

Autoanerror:
  Call objError.InternalError(Me, "pAddMensaje", "AddMensaje")

End Sub
Private Sub Sistema_Master()
'Se forma el mensaje de respuesta al Query
Dim strProgramacion As String
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim dimension As Integer
Dim objMensaje As clsMensaje
On Error GoTo Autoanerror

dimension = 0
'Registro de cabecera
Call pAddMensaje(ConstRegH)
'Registro de inforamcion del paciente tipo P
Call pAddMensaje(ConstRegP)
'Registro de peticion de pruebas
For Each objMuestra In objAutoAn.ColMuestras
  With objMuestra
    If .strEstado = ctePREPROGRAMACION Then
      For Each objActuacion In .ColMuestrasPruebas
        dimension = dimension + 1
        strProgramacion = "O|" & dimension & "|" & _
        objMuestra.strIDMuestra & "||^^^" & _
        objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto & ConstRegO
        Call pAddMensaje(strProgramacion, objMuestra)
      Next
    End If
  End With
Next
Call pAddMensaje(ConstRegL)
Call pTransmisionFrames
Autoanerror:
  Call objError.InternalError(Me, "Sistema_Master", "Sistema Master")

End Sub
Private Sub pEnvioMensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es enviar _
el caracter <ENQ> y esperar a que el autoanalizador responda afirmativamente. Luego _
se manda el mensaje propiamente dicho hasta que sea correctamente recibido por el _
autoanalizador. Finalmente se cierra la comunicacion enviando un <EOT>.
Static intContIntentosComunic As Integer  'cuenta el n� de veces que se intenta comunicar con el autoanalizador
Static intContIntentosTrans As Integer  'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
Dim msg As String
Dim objMuestra As clsMuestra
On Error Resume Next

  Select Case objAutoAn.Estado
    Case cteEspera  'Se intenta establecer la comunicacion
    'Como esta en estado espera es necesario mandar primero _
    la solicitud de la comunicacion
      intContIntentosTrans = 0  'Se inicializa el intContIntentosTrans por si se realiza una nueva comunicacion
      intContIntentosComunic = intContIntentosComunic + 1 'Se intenta establecer la comunicacion
      If intContIntentosComunic = 7 Then 'Fallo en la comunicacion despues de seis intentos
        intContIntentosComunic = 0
        msg = "No se puede establecer comunicai�n."
        Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
          Case vbRetry
            Sistema_Espera
            intContIntentosComunic = intContIntentosComunic + 1
            Exit Sub
          Case vbCancel
            objAutoAn.Enviar Chr$(4)  'Se envia <EOT>. Se interuumpe el intento de comunicaci�n.
            objAutoAn.Estado = cteEspera 'Se pasa al estado de espera al autoanalizador
            strLog = strLog & Chr$(13) & Chr$(10) & " <Ord>: <EOT> ESTADO ESPERA" & Chr$(13) & Chr$(10)
            Exit Sub
        End Select
      Else
        Sistema_Espera 'para enviar el <ENQ>
        Exit Sub
      End If
    Case cteMaster  'Se intenta transmitir los datos
      intContIntentosComunic = 0 'Se inicia el intContIntentosCoomunic por si se realiza una nueva comunicacion.
      intContIntentosTrans = intContIntentosTrans + 1
      If intContIntentosTrans = 7 Then 'Fallo en la transmisi�n despu�s de 6 intentos
        intContIntentosTrans = 0
        objAutoAn.Estado = cteEspera
        strLog = strLog & "ESTADO ESPERA"
        msg = "No se puede transmitir informacion."
        Select Case MsgBox(msg, vbExclamation + vbRetryCancel, Me.Caption)
          Case vbRetry
            T = Timer + 5
            Do While T > Timer: res = DoEvents(): Loop
            Sistema_Espera
            intContIntentosComunic = intContIntentosComunic + 1
            Exit Sub
          Case vbCancel
            objAutoAn.Enviar Chr$(4)  '<EOT> Se interrumpe el intento de comunicacion
            strLog = strLog & Chr$(13) & Chr$(10) & "<Ord>: <EOT> " & Chr$(13) & Chr$(10)
            For Each objMuestra In objAutoAn.ColMuestras
              If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
                objMuestra.strEstado = cteNOACEPTADA
              End If
            Next
            objAutoAn.Estado = cteEspera 'Se pasa al estado de espera
            Exit Sub
        End Select
      Else
        If blnFrameRepetido = False Then Sistema_Master _
        Else Call pTransmisionFrames
        Exit Sub
      End If
    End Select
          
  End Sub
Private Sub Inmulite_Solicitud(Mensaje As String)
  'Respuesta del Autoanalizador a una solicitud para establecer una comunicacion
  TimerEsperaREPLY.Enabled = False
  Select Case Right$(Mensaje, 1)
    Case Chr$(6)  '<ACK> - El autoanalizador ha aceptado la comunicacion. Se pasa a estado master
      objAutoAn.Estado = cteMaster
      Mensaje = "" 'Se limpia el mensaje
      Call pEnvioMensajes 'Se envia el bloque de datos por parte del sistema al autoanalizador
    Case Chr$(12) '<NACK> - El autoanalizador no ha aceptado la comunicacion
      objAutoAn.Estado = cteEspera
      Mensaje = ""
      TimerTransMENSAJE.Enabled = True 'Se espera 10 segundos para intentar una nueva comunicacion
    Case Chr$(5)  '<ENQ> - El autoanalizador ha solicitado linea a la vez que el sistema
      objAutoAn.Estado = cteEspera
      Mensaje = ""
  End Select
End Sub

Private Sub Inmulite_Espera(Mensaje As String)
'Identifica la llegada de los caracteres de solicitud de
'comunicacion. La acepta y pasa al estado de esclavo.
On Error GoTo Autoanerror

If Right$(Mensaje, 1) = Char$(5) Then '<ENQ>-Solicitud de comunicaci�n por parte
                                      'del autoanalizador
  objAutoAn.Enviar Chr$(6)  '<ACK>- Se acepta la comunicacion(!!No se conoce motivo
                            'para rechazarla.
  objAutoAn.Estado = cteESCLAVO
  Me.Refresh
  Mensaje = ""
  TimerAUTO.Enabled = True
End If
Exit Sub
  
Autoanerror:
  Call objError.InternalError(Me, "Inmulite_Espera", "Inmulite_Espera")
                                        
End Sub
Public Sub Lectura_Protocolo(caracter As String)
Static Mensaje As String 'Contiene el mensaje que envia el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es _
la que se env�a a otras subrutinas pudiendo ser modificada en �stas. Al ser modificada _
la copia, tambi�n deber� modificarse el original. El hecho de utilizar esta copia _
se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas _
por estar declarada como Static en esta subrutina

'Se va componiendo el mensaje
Mensaje = Mensaje & caracter
CopiaMensaje = Mensaje

'Se acude a la subrutina correspondiente para la lectura _
del mensaje segun el estado
Select Case objAutoAn.Estado
  Case cteEspera
    Inmulite_Espera CopiaMensaje
    Mensaje = CopiaMensaje
  Case cteSolicitud
    Inmulite_Solicitud CopiaMensaje
    Mensaje = CopiaMensaje
  Case cteConfirmacion
    'Inmulite_Confirmacion CopiaMensaje
    Mensaje = CopiaMensaje
  End Select

End Sub

Private Sub Sistema_Espera()
'Se envia el caracter de solicitud de comunicacion
'y se pasa a estado de solicitud
objAutoAn.Enviar Chr$(5) 'Env�o de <ENQ> se "solicita linea"
objAutoAn.Estado = cteSolicitud
TimerEsperaREPLY.Enabled = True
strLog = strLog & Chr$(13) & Chr$(10) & "<Ord>: <ENQ>ESTADO SOLICITUD" & Chr$(13) & Chr$(10)
If objAutoAn.blnDesign Then objAutoAn.AutoRecepcion Chr$(6)
End Sub


