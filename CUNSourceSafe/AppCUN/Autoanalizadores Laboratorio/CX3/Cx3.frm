VERSION 5.00
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CX3-Synchron"
   ClientHeight    =   420
   ClientLeft      =   6075
   ClientTop       =   960
   ClientWidth     =   1260
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Cx3.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   420
   ScaleWidth      =   1260
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   15000
      Left            =   840
      Top             =   0
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   420
      Top             =   0
   End
   Begin VB.Timer Timer3 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   0
      Top             =   0
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "&Menu"
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "&Extraida"
         Index           =   1
      End
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "Sin &Programar"
         Index           =   2
      End
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "&Aceptada"
         Index           =   3
      End
      Begin VB.Menu mnuItemEstadoMuestra 
         Caption         =   "&Borrada"
         Index           =   4
      End
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
'Variables
Dim posComienzo As Integer       'Guarda la posici�n de comienzo del carrusel antes de ser cambiada
Dim UltimaPosicion As Integer    'Guarda la�ltima posici�n ocupada del carrusel
Dim MiPROGRAMPendiente As Integer    'Indica si existe alguna prueba que deb�a haber sido programada
                                 'pero no lo ha sido por haber solicitado l�nea el autoanalizador
                                 'al mismo tiempo
Dim MiBORRADOPendiente As Integer  'Indica si existe alguna muestra que todav�a est� pendiente de borrar
                                 'por haber un numero de muestras a borrar mayor que 7
Dim MiRepetirPendiente As Integer 'vble que indica que se tienen que hacer repeticiones que se
                                'mandaran tras recibir el ultimo mensaje 70317 de resultados
Dim MiRepetirPruebas As Integer    'Indica si las pruebas se van a repetir o no
'Estas 2 variables se definen como globales por manejarse en varias subrutinas y tener la necesidad de ser est�ticas
'dentro del procedimiento que las utiliza. (Lo mismo ocurre con la variable Mensaje en los procedimientos de Lectura
'de Protocolos pero para ella se prefiere realizar una copia ya que existen otras variables locales tambi�n llamadas
'Mensaje que si no se cambian de nombre provocar�an errores).
Dim MiRespuesta As TipoRespuesta  'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
Dim MiRespuestaAfirmativa As Boolean 'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional
'(0 - <ACK>; 1 - <ETX>)
Dim MiTipoMensEnviado As TipoMensajeEnviado   'Indica el tipo de mensaje que el sistema transmite al autoanalizador y que puede ser:
'   1 - Funci�n 01 del stream 301: programaci�n de pruebas en autoanalizador
'   3 - Funci�n 03 del stream 301: borrado de pruebas programadas para el actual carrusel
Dim MiUltimoEnvio As Integer 'True: si no hay m�s env�os de prueba a programar
                            'False: todav�a quedan pruebas a programar
Dim MiRESPUESTAEsperada As Boolean 'True: si se est� esperando un mensaje de respuesta por parte del autoanalizador. Esto
'ocurrira cuando el sistema haya enviado un mensaje que requiere respuesta
                                    'False: no se espera ning�n mensaje de respuesta. El autoanalizador puede en esta
'situaci�n enviar resultados u otros mensajes no solicitados
Dim MiFuncionRespEsperada As TipoMensajeEnviado   'Indica el tipo de mensaje que se est� esperando como respuesta
'   2 - Funci�n 02 del stream 301: admisi�n de la programaci�n de pruebas
'   4 - Funci�n 04 del stream 301: admisi�n del borrado de todas las pruebas programadas para el carrusel
Dim Salida As Integer 'nos saldremos del programa cuando este esta vble true habiendo recibido el ultimo mensaje
                      ' que ser� de tipo 17
Dim NotPrim As Integer
'Constantes de resultados de c�lculos especiales
Const constANION_GAP = "ANION GAP (1)"
Const constUREA_CREATININA = "COC. UREA/CREA"
Const constOSMOLARIDAD = "OSMOLALIDAD (1)"

'Constantes de urgencias/rutina
Const constURGENCIA = 1
Const constRUTINA = 0
Enum TipoMensajeEnviado
  ctePROGRAMACION = 1
  cteBORRADO = 3
End Enum
Enum TipoRespuesta
  cteACK = 1
  cteETX = 2
  cteNAK = 3
End Enum
Dim SubRutina As String
Dim blnRecepcion As Boolean

Private Sub cmdClearTray_Click()
    objAutoAn.EscribirLog "cmdClearTray_click"
    'Se borra las pruebas programadas la vez anterior
    TipoMensEnviado = cteBORRADO
    Envio_Mensajes
End Sub

Private Function Comprobar_Check(Mensaje As String) As Integer
'Funci�n que comprueba el check del bloque de datos.
'   Si es correcto, se devuelve True
'   Si es incorrecto, se devuelve False
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos en la comprobaci�n del check
Dim suma As Integer         'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal
On Error GoTo AutoAnError

  suma = 0
  For contador = 1 To Len(Mensaje) - 4
    suma = suma + Asc(Mid$(Mensaje, contador, 1))
    If suma >= 256 Then suma = suma - 256
  Next contador
  suma = 256 - suma
  Comprobar_Check = (Hex$(suma) = Mid$(Mensaje, Len(Mensaje) - 3, 2) Or "0" & Hex$(suma) = Mid$(Mensaje, Len(Mensaje) - 3, 2))
  Exit Function
  
AutoAnError:
    Call objError.InternalError(Me, "Comprobar_Check", "plistaTrabajo")
End Function

Private Sub CX3_Confirmacion(Mensaje As String, Caracter As String)
Dim contador As Integer
On Error GoTo AutoAnError
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
'Si el mensaje es rechazado habr� que volver a enviarlo.

    'On Error Resume Next
  'objAutoAn.EscribirLog "CX3 Confirmacion"
  SubRutina = "CX3 Confirmacion"
  Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then  'puede ser que el autoanalizador solicite ser master en caso de haber habido algun problema
      .Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
      .Estado = cteESCLAVO
      .ButtonProgramEnabled False
      Mensaje = "" 'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
      Timer3.Enabled = True
      Respuesta = cteACK 'Respuesta enviada al autoanalizador
      RespuestaAfirmativa = True 'Proxima respuesta afirmativa a enviar al autoanalizador
      Exit Sub
    End If
    Select Case Caracter
      Case Chr$(3)  '<ETX> - El autoanalizador ya ha aceptado el bloque y da por finalizada la transmision
        .Enviar Chr$(4) '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        .ButtonProgramEnabled True
        Mensaje = ""
        'cuando dejamos de ser los transmitir esperamos la respuesta a estos mensajes
        RESPUESTAEsperada = True
        Exit Sub
      Case Chr$(6) '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
          'MsgBox "Recibido un ACK en Confirmaci�n", vbExclamation, frmprincipal.Caption
      'siempre responde ETX?
      '    Envio_Mensajes 'Se enviar� un <ENQ> para que el autoanalizador vuelva a mandar la respuesta
      '    mensaje = ""
        Exit Sub
      Case Chr$(21)   '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviarlo
        If TipoMensEnviado = ctePROGRAMACION Then
          Dim objMuestra As clsMuestra
          For Each objMuestra In objAutoAn.ColMuestras
            If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
              objMuestra.strEstado = cteSINPROGRAMAR
              UltimoEnvio = False
              Exit For
            End If
          Next
        End If
        .Estado = cteMASTER
        .ButtonProgramEnabled False
        Mensaje = ""
        Timer2.Enabled = True 'Se espera 1 segundo para retransmitir el bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Confirmacion", "plistaTrabajo")
End Sub

Private Sub CX3_Esclavo(Mensaje As String, Caracter As String)
'Identifica el final de un bloque y comprueba su check pasando a su lectura
'si fuera correcto. Si el bloque ha llegado incorrectamente se contesta
'negativamente para que el autoanalizador retransmita. Tambi�n se identifica
'el final de la comunicaci�n y se vuelve al estado de espera.
Dim contador As Integer
Dim sql As String
On Error GoTo AutoAnError
  SubRutina = "CX3 Esclavo"
  'objAutoAn.EscribirLog "CX3 Esclavo"
  Timer3.Enabled = False 'Al llegar datos del autoanalizador, se desactiva el Timer
  Select Case True
    Case Caracter = Chr$(4) '<EOT> - Final de la transmisi�n por parte del autoanalizador
      objAutoAn.Estado = cteESPERA
      Mensaje = ""
      Select Case True
        Case Salida
          Salir
        Case BORRADOPendiente
          'cuando el numero de muestras a borrar supera 7 hay que mandar desde aqui sucesivos mensajes de borrado
          TipoMensEnviado = cteBORRADO
          Envio_Mensajes
        Case PROGRAMPendiente
          PROGRAMPendiente = False
          UltimoEnvio = False
          TipoMensEnviado = ctePROGRAMACION
          Envio_Mensajes
        Case RepetirPruebas
        '  objAutoAn.LabelVisible True
          'RepetirPendiente = False
          'se forma una nueva matrizgrid que contiene las repeticiones y ademas
          'Se borran las pruebas programadas la vez anterior para que al darle al start puedan reprogramarlas
          'Repetir_Pruebas
        '  RepetirPruebas = False
        '  TipoMensEnviado = cteBORRADO
        '  Envio_Mensajes
        Case RESPUESTAEsperada
          'cuando recibimos la finalizacion de las respuestas a nuestros mensajes se dejan de esperar estas
          RESPUESTAEsperada = False
          If TipoMensEnviado = ctePROGRAMACION Then
            If Not UltimoEnvio Then
              'aqui deberiamos seguir con la programacion automatica de las muestras de matrizprog solicitadas
              'con el mensaje 6
              'cmdProgramar_Click 'Se env�a una nueva prueba a programar
              Envio_Mensajes
            Else
'              Dim objMuestra As clsMuestra
'              For Each objMuestra In objAutoAn.ColMuestras
'                If objMuestra.strEstado <> cteNOACEPTADA Then
'                  Exit For
'                End If
'              Next
              'If Not objAutoAn.blnExistMuestraConEstado(cteNOACEPTADA) Then
              If Not objAutoAn.blnExistMuestraSinEstado(cteNOACEPTADA) Then
                'todas las muestras estan NA
                'No se ha admitido la programaci�n de ninguna de las pruebas
                sql = "El autoanalizador " & frmPrincipal.Caption & " no ha aceptado la "
                sql = sql & "la programaci�n de ninguna prueba." & Chr$(10) & "Volver a "
                sql = sql & "ejecutar el programa para intentarlo de nuevo."
                MsgBox sql, vbExclamation, frmPrincipal.Caption
'                Form_Unload (False)
                Descargar
              End If
            End If
          End If
      End Select
    Case Caracter = Chr$(5) '<ENQ> - El autoanalizador solicita respuesta
    'a un bloque mandado tras haber ocurrido un T.O. No es normal que ocurra
    'un T.O. en el autoanalizador ya que la la respuesta al bloque se env�a
    'una vez se ha comprobado el check
      Select Case Respuesta
        Case cteACK: objAutoAn.Enviar Chr$(6) '<ACK>
        Case cteETX: objAutoAn.Enviar Chr$(3) '<ETX>
        Case cteNAK: objAutoAn.Enviar Chr$(21) '<NAK>
      End Select
      Mensaje = ""
      Timer3.Enabled = True
    Case Right$(Mensaje, 2) = Chr$(13) & Chr$(10)  'Se ha identificado el final del bloque
      objAutoAn.Enviar IIf(RespuestaAfirmativa, Chr$(3), Chr$(6))  '<ACK>/<ETX>
      Respuesta = IIf(RespuestaAfirmativa, cteETX, cteACK) 'Respuesta enviada al autoanalizador
      RespuestaAfirmativa = Not RespuestaAfirmativa
      Lectura_Datos_CX3 Mensaje 'Se procede a la lectura de datos
      Mensaje = ""
      'para ver cuanto tiempo tarda en enviar el siguiente mensaje
      Timer3.Enabled = True
      objAutoAn.autorecepcion Chr$(4)
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Esclavo", "plistaTrabajo")
End Sub

Private Sub CX3_Espera(Mensaje As String, Caracter As String)
'Identifica la llegada de los caracteres de solicitud de comunicaci�n,
'la acepta y pasa al estado de transmisi�n.
On Error GoTo AutoAnError
  'objAutoAn.EscribirLog "CX3 Espera"
  SubRutina = "CX3 Espera"
  Select Case True
    Case (Right$(Mensaje, 2) = Chr$(4) & Chr$(1)) '<EOT><SOH> - Solicitud de comunicaci�n por parte del autoanalizador
'      If blnRecepcion Then
        objAutoAn.Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
        objAutoAn.Estado = cteESCLAVO
        
        If objAutoAn.blnDesign Then Simulacion
        Mensaje = "" 'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
        Timer3.Enabled = True
        Respuesta = cteACK 'Respuesta enviada al autoanalizador
        RespuestaAfirmativa = True 'Proxima respuesta afirmativa a enviar al autoanalizador
'      Else
'        objAutoAn.Enviar Chr$(21)
'        Mensaje = "" 'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
'        'Timer3.Enabled = True
'        'Respuesta = cteACK 'Respuesta enviada al autoanalizador
'        'RespuestaAfirmativa = True 'Proxima respuesta afirmativa a enviar al autoanalizador
'      End If
    Case (Caracter = Chr$(5))  '<ENQ> - El autoanalizador solicita respuesta
    'a un bloque mandado tras haber ocurrido un T.O. No es normal que ocurra
    'un T.O. en el autoanalizador ya que la la respuesta al bloque se env�a
    'una vez se ha comprobado el check
      objAutoAn.Enviar Chr$(4) '<EOT> - El sistema da por finalizada la comunicaci�n
      Mensaje = ""
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Espera", "plistaTrabajo")
End Sub

Private Sub Simulacion()
Static nVez As Integer

  nVez = nVez + 1
  Select Case nVez
'    Case 1: objAutoAn.autorecepcion "[ 0,701,06,0007866962 ,0007866981 ,0007867106 ,0007867164 ,           ,           ,           ]AB" & vbCrLf
'    Case 2: objAutoAn.autorecepcion "[ 0,702,01,251099,093007,13987,RG, 1, 1,RO,#########,SE,0007866490 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,251099,0944,                    ,  0,5,      ,M,                         ,#######,####,####,######,  3,43D ,46B ,48A ]AB" & vbCrLf
'    Case 3: objAutoAn.autorecepcion "[ 0,702,03,251099,093104,13987,    10993, 1, 1,0007866490 ,43D ,###,812042,##, 1,     4.22,#########,2,0,00,NR,NR,NR,0,NA,4.2152781,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]20" & vbCrLf
'    Case 4: objAutoAn.autorecepcion "[ 0,702,03,251099,094403,13987,    10993, 1, 1,0007866490 ,46B ,###,903109,31, 1,       23,#########,2,0,12,NA,NR,NA,0,NA,22.719059,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]58" & vbCrLf
'    Case 5: objAutoAn.autorecepcion "[ 0,702,03,251099,093555,13987,    10993, 1, 1,0007866490 ,48A ,###,905101,32, 1,      2.1,#########,2,0,00,NA,NR,NA,0,NA,2.1159489,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]48" & vbCrLf
'    Case 6: objAutoAn.autorecepcion "[ 0,702,05,251099,094403,13987,0007866490 , 1, 1]56" & vbCrLf
'    Case 7: objAutoAn.autorecepcion "[ 0,701,02, 0,14006, 0, 0,0007866962 ]B3" & vbCrLf

Case 1: objAutoAn.autorecepcion "[ 0,701,06,0007871146 ,0007871102 ,0007871139 ,0007871103 ,0007871091 ,           ,           ]12" & vbCrLf
Case 2: objAutoAn.autorecepcion "[ 0,701,02, 0,54882, 0, 0,0007871091 ]AE" & vbCrLf
Case 3: objAutoAn.autorecepcion "[ 0,701,02, 0,54883, 0, 0,0007871102 ]B4" & vbCrLf
Case 4: objAutoAn.autorecepcion "[ 0,701,02, 0,54884, 0, 0,0007871103 ]B2" & vbCrLf
Case 5: objAutoAn.autorecepcion "[ 0,701,02, 0,54885, 0, 0,0007871139 ]A8" & vbCrLf
Case 6: objAutoAn.autorecepcion "[ 0,701,02, 0,54886, 0, 0,0007871146 ]A9" & vbCrLf
Case 7: objAutoAn.autorecepcion "[ 0,702,01,271099,162956,54882,RG, 2, 5,ST,#########,SE,0007871091 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1625,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]D7" & vbCrLf
Case 8: objAutoAn.autorecepcion "[ 0,702,03,271099,163116,54882,     1872, 2, 5,0007871091 ,01A ,###,######,##, 1,    131.1,#########,2,0, 4,LO,NR,NR,0,NA,131.05145,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CD" & vbCrLf
Case 9: objAutoAn.autorecepcion "[ 0,702,03,271099,163117,54882,     8290, 2, 5,0007871091 ,01B ,###,######,##, 1,     5.60,#########,2,0, 4,HI,NR,NR,0,NA,5.6049566,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CA" & vbCrLf
Case 10: objAutoAn.autorecepcion "[ 0,702,03,271099,163118,54882,       55, 2, 5,0007871091 ,02A ,###,######,##, 1,     23.0,#########,2,0, 4,NA,NR,NA,0,NA,22.986301,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]15" & vbCrLf
Case 11: objAutoAn.autorecepcion "[ 0,702,03,271099,163117,54882,     1293, 2, 5,0007871091 ,04A ,###,######,##, 1,    101.0,#########,2,0, 4,NR,NR,NR,0,NA,101.01540,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D3" & vbCrLf
Case 12: objAutoAn.autorecepcion "[ 0,702,11,271099,163118,54882, 2, 5,0007871091 , 1,ANION GAP (1)       ,OK,7.1000061,        ]E7" & vbCrLf
Case 13: objAutoAn.autorecepcion "[ 0,702,05,271099,163118,54882,0007871091 , 2, 5]57" & vbCrLf
Case 14: objAutoAn.autorecepcion "[ 0,702,01,271099,163001,54884,RG, 2, 4,ST,#########,SE,0007871103 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1625,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]EE" & vbCrLf
Case 15: objAutoAn.autorecepcion "[ 0,702,03,271099,163204,54884,     5075, 2, 4,0007871103 ,01A ,###,######,##, 1,    126.0,#########,2,0, 4,LO,NR,NR,0,NA,125.95895,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BA" & vbCrLf
Case 16: objAutoAn.autorecepcion "[ 0,702,03,271099,163204,54884,     4650, 2, 4,0007871103 ,01B ,###,######,##, 1,     3.54,#########,2,0, 4,NR,NR,NR,0,NA,3.5395775,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C3" & vbCrLf
Case 17: objAutoAn.autorecepcion "[ 0,702,03,271099,163206,54884,     8317, 2, 4,0007871103 ,02A ,###,######,##, 1,     23.9,#########,2,0, 4,NA,NR,NA,0,NA,23.884216,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E7" & vbCrLf
Case 18: objAutoAn.autorecepcion "[ 0,702,03,271099,163205,54884,     8159, 2, 4,0007871103 ,04A ,###,######,##, 1,     95.8,#########,2,0, 4,NR,NR,NR,0,NA,95.848648,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]A6" & vbCrLf
Case 19: objAutoAn.autorecepcion "[ 0,702,11,271099,163206,54884, 2, 4,0007871103 , 1,ANION GAP (1)       ,OK,6.2999954,        ]C8" & vbCrLf
Case 20: objAutoAn.autorecepcion "[ 0,702,05,271099,163206,54884,0007871103 , 2, 4]5E" & vbCrLf
Case 21: objAutoAn.autorecepcion "[ 0,702,01,271099,163003,54885,RG, 2, 3,ST,#########,SE,0007871139 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1625,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]B1" & vbCrLf
Case 22: objAutoAn.autorecepcion "[ 0,702,03,271099,163249,54885,     8581, 2, 3,0007871139 ,03C ,###,######,##, 1,      1.0,#########,2,0, 0,NA,NR,NA,0,NA,0.9565214,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F4" & vbCrLf
Case 23: objAutoAn.autorecepcion "[ 0,702,05,271099,163249,54885,0007871139 , 2, 3]4E" & vbCrLf
Case 24: objAutoAn.autorecepcion "[ 0,702,01,271099,163006,54886,RG, 2, 1,ST,#########,SE,0007871146 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1625,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]B1" & vbCrLf
Case 25: objAutoAn.autorecepcion "[ 0,702,03,271099,163336,54886,     8557, 2, 1,0007871146 ,03C ,###,######,##, 1,      1.4,#########,2,0, 0,NA,NR,NA,0,NA,1.3695649,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E8" & vbCrLf
Case 26: objAutoAn.autorecepcion "[ 0,702,05,271099,163336,54886,0007871146 , 2, 1]54" & vbCrLf
Case 27: objAutoAn.autorecepcion "[ 0,702,01,271099,162959,54883,RG, 2, 2,RO,#########,SE,0007871102 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1625,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]B1" & vbCrLf
Case 28: objAutoAn.autorecepcion "[ 0,702,03,271099,163424,54883,     4057, 2, 2,0007871102 ,03C ,###,######,##, 1,      1.0,#########,2,0, 0,NA,NR,NA,0,NA,1.0434779,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]09" & vbCrLf

Case 29: objAutoAn.autorecepcion "[ 0,702,05,      ,      ,54883,0007871102 , 2, 2]50" & vbCrLf
Case 30: objAutoAn.autorecepcion "[ 0,703,17,271099,163448]D0" & vbCrLf
Case 31: objAutoAn.autorecepcion "[ 0,701,06,0007871146 ,           ,           ,           ,           ,           ,           ]0C" & vbCrLf
Case 32: objAutoAn.autorecepcion "[ 0,701,02, 0,54887, 0, 0,0007871146 ]A8" & vbCrLf
Case 33: objAutoAn.autorecepcion "[ 0,702,01,271099,163717,54887,RG, 2, 1,ST,#########,SE,0007871146 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1632,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]A9" & vbCrLf
Case 34: objAutoAn.autorecepcion "[ 0,702,03,271099,163835,54887,     5736, 2, 1,0007871146 ,03C ,###,######,##, 1,      1.4,#########,2,0, 0,NA,NR,NA,0,NA,1.3586954,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E9" & vbCrLf
Case 35: objAutoAn.autorecepcion "[ 0,702,05,271099,163835,54887,0007871146 , 2, 1]4F" & vbCrLf
Case 36: objAutoAn.autorecepcion "[ 0,703,17,271099,163858]CB" & vbCrLf
Case 37: objAutoAn.autorecepcion "[ 0,701,06,0007871091 ,           ,           ,           ,           ,           ,           ]0D" & vbCrLf
Case 38: objAutoAn.autorecepcion "[ 0,701,02, 0,54888, 0, 0,0007871091 ]A8" & vbCrLf
Case 39: objAutoAn.autorecepcion "[ 0,702,01,271099,164721,54888,RG, 2, 2,ST,#########,SE,0007871091 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1642,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]DD" & vbCrLf
Case 40: objAutoAn.autorecepcion "[ 0,702,03,271099,164842,54888,     8771, 2, 2,0007871091 ,01A ,###,######,##, 1,    131.3,#########,2,0, 4,LO,NR,NR,0,NA,131.25461,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B9" & vbCrLf
Case 41: objAutoAn.autorecepcion "[ 0,702,03,271099,164843,54888,       24, 2, 2,0007871091 ,01B ,###,######,##, 1,     5.60,#########,2,0, 4,HI,NR,NR,0,NA,5.6049566,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]ED" & vbCrLf
Case 42: objAutoAn.autorecepcion "[ 0,702,03,271099,164844,54888,     5232, 2, 2,0007871091 ,02A ,###,######,##, 1,     22.7,#########,2,0, 4,NA,NR,NA,0,NA,22.665997,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D4" & vbCrLf
Case 43: objAutoAn.autorecepcion "[ 0,702,03,271099,164844,54888,      440, 2, 2,0007871091 ,04A ,###,######,##, 1,    100.6,#########,2,0, 4,NR,NR,NR,0,NA,100.57628,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C9" & vbCrLf
Case 44: objAutoAn.autorecepcion "[ 0,702,11,      ,      ,54888, 2, 2,0007871091 , 1,ANION GAP (1)       ,OK,8.0000000,        ]DB" & vbCrLf
Case 45: objAutoAn.autorecepcion "[ 0,702,05,      ,      ,54888,0007871091 , 2, 2]44" & vbCrLf
Case 46: objAutoAn.autorecepcion "[ 0,703,17,271099,164848]CB" & vbCrLf





    Case 1: objAutoAn.autorecepcion "[ 0,701,06,0007871146 ,0007871102 ,0007871139 ,0007871103 ,0007871091 ,           ,           ]12" & vbCrLf
    Case 2: objAutoAn.autorecepcion "[ 0,701,02, 0,54882, 0, 0,0007871091 ]AE" & vbCrLf
    Case 3: objAutoAn.autorecepcion "[ 0,702,01,271099,162956,54882,RG, 2, 5,ST,#########,SE,0007871091 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,271099,1625,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]D7" & vbCrLf
    Case 4: objAutoAn.autorecepcion "[ 0,702,03,271099,163116,54882,     1872, 2, 5,0007871091 ,01A ,###,######,##, 1,    131.1,#########,2,0, 4,LO,NR,NR,0,NA,131.05145,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CD" & vbCrLf
    Case 5: objAutoAn.autorecepcion "[ 0,702,03,271099,163117,54882,     8290, 2, 5,0007871091 ,01B ,###,######,##, 1,     5.60,#########,2,0, 4,HI,NR,NR,0,NA,5.6049566,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CA" & vbCrLf
    Case 6: objAutoAn.autorecepcion "[ 0,702,03,271099,163118,54882,       55, 2, 5,0007871091 ,02A ,###,######,##, 1,     23.0,#########,2,0, 4,NA,NR,NA,0,NA,22.986301,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]15" & vbCrLf
    Case 7: objAutoAn.autorecepcion "[ 0,702,03,271099,163117,54882,     1293, 2, 5,0007871091 ,04A ,###,######,##, 1,    101.0,#########,2,0, 4,NR,NR,NR,0,NA,101.01540,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D3" & vbCrLf
    Case 8: objAutoAn.autorecepcion "[ 0,702,11,271099,163118,54882, 2, 5,0007871091 , 1,ANION GAP (1)       ,OK,7.1000061,        ]E7" & vbCrLf
    Case 9: objAutoAn.autorecepcion "[ 0,702,05,271099,163118,54882,0007871091 , 2, 5]57" & vbCrLf

'    Case 1: objAutoAn.autorecepcion "[ 0,701,06,0007821953 ,0007821999 ,0007822009 ,0007822014 ,0007822018 ,           ,           ]0E" & vbCrLf
'    Case 2: objAutoAn.autorecepcion "[ 0,701,02, 0,44650, 0, 0,0007821953 ]B4" & vbCrLf
'    Case 3: objAutoAn.autorecepcion "[ 0,701,02, 0,44651, 0, 0,0007821999 ]A9" & vbCrLf
'    Case 4: objAutoAn.autorecepcion "[ 0,701,02, 0,44652, 0, 0,0007822009 ]B9" & vbCrLf
'    Case 5: objAutoAn.autorecepcion "[ 0,701,02, 0,44653, 0, 0,0007822014 ]BC" & vbCrLf
'    Case 6: objAutoAn.autorecepcion "[ 0,701,02, 0,44654, 0, 0,0007822018 ]B7" & vbCrLf
'    Case 7: objAutoAn.autorecepcion "[ 0,702,01,140999,104655,44650,RG, 1, 1,RO,#########,SE,0007821953 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,140999,1043,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]EE" & vbCrLf
'    Case 8: objAutoAn.autorecepcion "[ 0,702,03,140999,104823,44650,     7254, 1, 1,0007821953 ,01A ,###,######,##, 1,    136.1,#########,2,0, 4,NR,NR,NR,0,NA,136.05092,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C4" & vbCrLf
'    Case 9: objAutoAn.autorecepcion "[ 0,702,03,140999,104824,44650,     1328, 1, 1,0007821953 ,01B ,###,######,##, 1,     3.92,#########,2,0, 4,NR,NR,NR,0,NA,3.9163294,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C8" & vbCrLf
'    Case 10: objAutoAn.autorecepcion "[ 0,702,03,140999,104825,44650,     2923, 1, 1,0007821953 ,02A ,###,######,##, 1,     25.4,#########,2,0, 4,NA,NR,NA,0,NA,25.360565,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]EF" & vbCrLf
'    Case 11: objAutoAn.autorecepcion "[ 0,702,03,140999,104824,44650,     6030, 1, 1,0007821953 ,04A ,###,######,##, 1,    107.1,#########,2,0, 4,NR,NR,NR,0,NA,107.11865,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C8" & vbCrLf
'    Case 12: objAutoAn.autorecepcion "[ 0,702,11,140999,104825,44650, 1, 1,0007821953 , 1,ANION GAP (1)       ,OK,3.6000061,        ]ED" & vbCrLf
'    Case 13: objAutoAn.autorecepcion "[ 0,702,05,140999,104825,44650,0007821953 , 1, 1]5E" & vbCrLf
'    Case 14: objAutoAn.autorecepcion "[ 0,702,01,140999,104658,44651,RG, 1, 2,RO,#########,SE,0007821999 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,140999,1043,                    ,   ,5,      ,M,                         ,#######,####,####,######,  2,03C ,73C ]B3" & vbCrLf
'    Case 15: objAutoAn.autorecepcion "[ 0,702,03,140999,104908,44651,     4290, 1, 2,0007821999 ,03C ,###,######,##, 1,      0.9,#########,2,0, 0,NA,NR,NA,0,NA,0.8521867,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F0" & vbCrLf
'    Case 16: objAutoAn.autorecepcion "[ 0,702,03,140999,104907,44651,      537, 1, 2,0007821999 ,73C ,###,######,##, 1,     0.28,#########,2,0, 3,NR,NR,NR,0,NA,4.5968370,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BF" & vbCrLf
'    Case 17: objAutoAn.autorecepcion "[ 0,702,05,140999,104908,44651,0007821999 , 1, 2]50" & vbCrLf
'    Case 18: objAutoAn.autorecepcion "[ 0,702,01,140999,104700,44652,RG, 1, 3,RO,#########,SE,0007822009 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,140999,1043,                    ,   ,5,      ,M,                         ,#######,####,####,######,  2,03C ,73C ]CE" & vbCrLf
'    Case 19: objAutoAn.autorecepcion "[ 0,702,03,140999,104956,44652,     3928, 1, 3,0007822009 ,03C ,###,######,##, 1,      0.8,#########,2,0, 0,NA,NR,NA,0,NA,0.7571641,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FC" & vbCrLf
'    Case 20: objAutoAn.autorecepcion "[ 0,702,03,140999,104955,44652,     6627, 1, 3,0007822009 ,73C ,###,######,##, 1,     0.48,#########,2,0, 3,NR,NR,NR,0,NA,7.9202490,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BC" & vbCrLf
'    Case 21: objAutoAn.autorecepcion "[ 0,702,05,140999,104956,44652,0007822009 , 1, 3]5C" & vbCrLf
'    Case 22: objAutoAn.autorecepcion "[ 0,702,01,140999,104702,44653,RG, 1, 4,RO,#########,SE,0007822014 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,140999,1043,                    ,   ,5,      ,M,                         ,#######,####,####,######,  6,03C ,73C ,01A ,01B ,02A ,04A ]0D" & vbCrLf
'    Case 23: objAutoAn.autorecepcion "[ 0,702,03,140999,105047,44653,     8743, 1, 4,0007822014 ,03C ,###,######,##, 1,      0.7,#########,2,0, 0,NA,NR,NA,0,NA,0.7466061,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]08" & vbCrLf
'    Case 24: objAutoAn.autorecepcion "[ 0,702,03,140999,105046,44653,     7646, 1, 4,0007822014 ,73C ,###,######,##, 1,     0.28,#########,2,0, 3,NR,NR,NR,0,NA,4.6226001,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D2" & vbCrLf
'    Case 25: objAutoAn.autorecepcion "[ 0,702,03,140999,105047,44653,     7643, 1, 4,0007822014 ,01A ,###,######,##, 1,    137.4,#########,2,0, 4,NR,NR,NR,0,NA,137.40154,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C5" & vbCrLf
'    Case 26: objAutoAn.autorecepcion "[ 0,702,03,140999,105048,44653,      374, 1, 4,0007822014 ,01B ,###,######,##, 1,     4.68,#########,2,0, 4,NR,NR,NR,0,NA,4.6759057,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D4" & vbCrLf
'    Case 27: objAutoAn.autorecepcion "[ 0,702,03,140999,105049,44653,     7860, 1, 4,0007822014 ,02A ,###,######,##, 1,     30.9,#########,2,0, 4,NA,NR,NA,0,NA,30.943398,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E8" & vbCrLf
'    Case 28: objAutoAn.autorecepcion "[ 0,702,03,140999,105049,44653,     7904, 1, 4,0007822014 ,04A ,###,######,##, 1,    103.7,#########,2,0, 4,NR,NR,NR,0,NA,103.65621,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C5" & vbCrLf
'    Case 29: objAutoAn.autorecepcion "[ 0,702,11,140999,105049,44653, 1, 4,0007822014 , 1,ANION GAP (1)       ,OK,2.8000031,        ]F5" & vbCrLf
'    Case 30: objAutoAn.autorecepcion "[ 0,702,05,140999,105049,44653,0007822014 , 1, 4]64" & vbCrLf
'    Case 31: objAutoAn.autorecepcion "[ 0,702,01,140999,104705,44654,RG, 1, 5,RO,#########,SE,0007822018 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,140999,1043,                    ,   ,5,      ,M,                         ,#######,####,####,######,  6,03C ,73C ,01A ,01B ,02A ,04A ]04" & vbCrLf
'    Case 32: objAutoAn.autorecepcion "[ 0,702,03,140999,105135,44654,     8208, 1, 5,0007822018 ,03C ,###,######,##, 1,      1.1,#########,2,0, 0,NA,NR,NA,0,NA,1.0950223,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]15" & vbCrLf
'    Case 33: objAutoAn.autorecepcion "[ 0,702,03,140999,105134,44654,     8718, 1, 5,0007822018 ,73C ,###,######,##, 1,     0.43,#########,2,0, 3,NR,NR,NR,0,NA,7.1216002,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D2" & vbCrLf
'    Case 34: objAutoAn.autorecepcion "[ 0,702,03,140999,105136,44654,     7657, 1, 5,0007822018 ,01A ,###,######,##, 1,    139.2,#########,2,0, 4,NR,NR,NR,0,NA,139.15504,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B8" & vbCrLf
'    Case 35: objAutoAn.autorecepcion "[ 0,702,03,140999,105136,44654,     1978, 1, 5,0007822018 ,01B ,###,######,##, 1,     3.97,#########,2,0, 4,NR,NR,NR,0,NA,3.9738197,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B0" & vbCrLf
'    Case 36: objAutoAn.autorecepcion "[ 0,702,03,140999,105137,44654,     8057, 1, 5,0007822018 ,02A ,###,######,##, 1,     27.7,#########,2,0, 4,NA,NR,NA,0,NA,27.730465,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E6" & vbCrLf
'    Case 37: objAutoAn.autorecepcion "[ 0,702,03,140999,105137,44654,     3800, 1, 5,0007822018 ,04A ,###,######,##, 1,    108.4,#########,2,0, 4,HI,NR,NR,0,NA,108.44177,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CF" & vbCrLf
  
  
  
'  Case 1: objAutoAn.AutoRecepcion "[ 0,701,06,0007813532 ,0007814132 ,0007814328 ,0007814890 ,0007815230 ,0007815642 ,           ]56" & vbCrLf
'Case 2: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43251, 0, 0,0007813532 ]BE" & vbCrLf
'Case 3: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43252, 0, 0,0007814132 ]C0" & vbCrLf
'Case 4: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43253, 0, 0,0007814328 ]B8" & vbCrLf
'Case 5: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43254, 0, 0,0007814890 ]B3" & vbCrLf
'Case 6: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43255, 0, 0,0007815230 ]BD" & vbCrLf
'Case 7: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43256, 0, 0,0007815642 ]B5" & vbCrLf
'Case 8: objAutoAn.AutoRecepcion "[ 0,701,06,0007815652 ,0007815656 ,0007815664 ,0007815666 ,0007815669 ,0007815740 ,0007815742 ]6E" & vbCrLf
'Case 9: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43257, 0, 0,0007815652 ]B3" & vbCrLf
'Case 10: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43258, 0, 0,0007815656 ]AE" & vbCrLf
'Case 11: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43259, 0, 0,0007815664 ]AE" & vbCrLf
'Case 12: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43260, 0, 0,0007815666 ]B4" & vbCrLf
'Case 13: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43261, 0, 0,0007815669 ]B0" & vbCrLf
'Case 14: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43262, 0, 0,0007815740 ]B9" & vbCrLf
'Case 15: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43263, 0, 0,0007815742 ]B6" & vbCrLf
'Case 16: objAutoAn.AutoRecepcion "[ 0,701,06,0007815744 ,0007815746 ,0007815761 ,0007815768 ,0007815776 ,0007815782 ,0007815801 ]6A" & vbCrLf
'Case 17: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43264, 0, 0,0007815744 ]B3" & vbCrLf
'Case 18: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43265, 0, 0,0007815746 ]B0" & vbCrLf
'Case 19: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43266, 0, 0,0007815761 ]B2" & vbCrLf
'Case 20: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43267, 0, 0,0007815768 ]AA" & vbCrLf
'Case 21: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43268, 0, 0,0007815776 ]AA" & vbCrLf
'Case 22: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43269, 0, 0,0007815782 ]AC" & vbCrLf
'Case 23: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43270, 0, 0,0007815801 ]BC" & vbCrLf
'Case 24: objAutoAn.AutoRecepcion "[ 0,701,06,0007815811 ,0007815842 ,0007815849 ,0007815863 ,           ,           ,           ]BC" & vbCrLf
'Case 25: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43271, 0, 0,0007815811 ]BA" & vbCrLf
'Case 26: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43272, 0, 0,0007815842 ]B5" & vbCrLf
'Case 27: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43273, 0, 0,0007815849 ]AD" & vbCrLf
'Case 28: objAutoAn.AutoRecepcion "[ 0,701,02, 0,43274, 0, 0,0007815863 ]B0" & vbCrLf
'Case 29: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115235,43251,RG, 1, 1,RO,#########,SE,0007813532 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C5" & vbCrLf
'Case 30: objAutoAn.AutoRecepcion "[ 0,702,03,080999,115401,43251,     1088, 1, 1,0007813532 ,03C ,###,######,##, 1,#########,#########,2,1, 0,NA,HI,NA,0,NA,#########,         ,DH,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]DF" & vbCrLf
'Case 31: objAutoAn.AutoRecepcion "[ 0,702,05,080999,115401,43251,0007813532 , 1, 1]6D" & vbCrLf
'Case 32: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115238,43252,RG, 1, 2,RO,#########,SE,0007814132 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C3" & vbCrLf
'Case 33: objAutoAn.AutoRecepcion "[ 0,702,03,080999,115530,43252,     5489, 1, 2,0007814132 ,03C ,###,######,##, 1,#########,#########,2,1, 0,NA,HI,NA,0,NA,#########,         ,DH,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]D4" & vbCrLf
'Case 34: objAutoAn.AutoRecepcion "[ 0,702,05,080999,115530,43252,0007814132 , 1, 2]6B" & vbCrLf
'Case 35: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115240,43253,RG, 1, 3,RO,#########,SE,0007814328 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C1" & vbCrLf
'Case 36: objAutoAn.AutoRecepcion "[ 0,702,03,080999,115701,43253,     3546, 1, 3,0007814328 ,03C ,###,######,##, 1,     14.6,#########,2,0, 0,NA,NR,NA,0,NA,14.623656,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F1" & vbCrLf
'Case 37: objAutoAn.AutoRecepcion "[ 0,702,05,080999,115701,43253,0007814328 , 1, 3]62" & vbCrLf
'Case 38: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115243,43254,RG, 1, 4,RO,#########,SE,0007814890 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]B8" & vbCrLf
'Case 39: objAutoAn.AutoRecepcion "[ 0,702,03,080999,115832,43254,     5780, 1, 4,0007814890 ,03C ,###,######,##, 1,#########,#########,2,1, 0,NA,HI,NA,0,NA,#########,         ,DH,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]C6" & vbCrLf
'Case 40: objAutoAn.AutoRecepcion "[ 0,702,05,080999,115832,43254,0007814890 , 1, 4]57" & vbCrLf
'Case 41: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115246,43255,RG, 1, 5,RO,#########,SE,0007815230 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]BE" & vbCrLf
'Case 42: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120003,43255,     4434, 1, 5,0007815230 ,03C ,###,######,##, 1,#########,#########,2,1, 0,NA,HI,NA,0,NA,#########,         ,DH,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]E2" & vbCrLf
'Case 43: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120003,43255,0007815230 , 1, 5]6E" & vbCrLf
'Case 44: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115251,43256,RG, 1, 7,RO,#########,SE,0007815642 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]EA" & vbCrLf
'Case 45: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120137,43256,     3214, 1, 7,0007815642 ,01A ,###,######,##, 1,    138.5,#########,2,0, 4,NR,NR,NR,0,NA,138.47794,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B1" & vbCrLf
'Case 46: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120137,43256,     2586, 1, 7,0007815642 ,01B ,###,######,##, 1,     4.17,#########,2,0, 4,NR,NR,NR,0,NA,4.1743202,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CE" & vbCrLf
'Case 47: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120138,43256,     2954, 1, 7,0007815642 ,02A ,###,######,##, 1,     25.8,#########,2,0, 4,NA,NR,NA,0,NA,25.753050,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E9" & vbCrLf
'Case 48: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120138,43256,     2360, 1, 7,0007815642 ,04A ,###,######,##, 1,    108.1,#########,2,0, 4,HI,NR,NR,0,NA,108.10872,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D2" & vbCrLf
'Case 49: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120138,43256, 1, 7,0007815642 , 1,ANION GAP (1)       ,OK,4.6000061,        ]E9" & vbCrLf
'Case 50: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120138,43256,0007815642 , 1, 7]5B" & vbCrLf
'Case 51: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115312,43257,RG, 2, 1,RO,#########,SE,0007815652 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,03C ,01A ,01B ,02A ,04A ]FC" & vbCrLf
'Case 52: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120225,43257,     2206, 2, 1,0007815652 ,03C ,###,######,##, 1,      0.9,#########,2,0, 0,NA,NR,NA,0,NA,0.9462366,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]07" & vbCrLf
'Case 53: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120225,43257,     6495, 2, 1,0007815652 ,01A ,###,######,##, 1,    139.6,#########,2,0, 4,NR,NR,NR,0,NA,139.62294,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AD" & vbCrLf
'Case 54: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120226,43257,      412, 2, 1,0007815652 ,01B ,###,######,##, 1,     2.71,#########,2,0, 4,LO,NR,NR,0,NA,2.7133260,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F6" & vbCrLf
'Case 55: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120227,43257,     3739, 2, 1,0007815652 ,02A ,###,######,##, 1,     21.0,#########,2,0, 4,NA,NR,NA,0,NA,21.031933,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FC" & vbCrLf
'Case 56: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120226,43257,     3055, 2, 1,0007815652 ,04A ,###,######,##, 1,    109.5,#########,2,0, 4,HI,NR,NR,0,NA,109.49919,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C1" & vbCrLf
'Case 57: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120227,43257, 2, 1,0007815652 , 1,ANION GAP (1)       ,OK,9.1000061,        ]ED" & vbCrLf
'Case 58: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120227,43257,0007815652 , 2, 1]5F" & vbCrLf
'Case 59: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115318,43258,RG, 2, 2,RO,#########,SE,0007815656 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  2,03C ,73C ]B7" & vbCrLf
'Case 60: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120310,43258,     8024, 2, 2,0007815656 ,03C ,###,######,##, 1,      2.0,#########,2,0, 0,NA,NR,NA,0,NA,1.9677420,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]09" & vbCrLf
'Case 61: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120309,43258,     2997, 2, 2,0007815656 ,73C ,###,######,##, 1,     0.90,#########,2,0, 3,HI,NR,NR,0,NA,14.966147,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BE" & vbCrLf
'Case 62: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120310,43258,0007815656 , 2, 2]60" & vbCrLf
'Case 63: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115321,43259,RG, 2, 3,RO,#########,SE,0007815664 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]E8" & vbCrLf
'Case 64: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120400,43259,     3841, 2, 3,0007815664 ,01A ,###,######,##, 1,    124.7,#########,2,0, 4,LO,NR,NR,0,NA,124.68864,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BA" & vbCrLf
'Case 65: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120401,43259,     8489, 2, 3,0007815664 ,01B ,###,######,##, 1,     4.13,#########,2,0, 4,NR,NR,NR,0,NA,4.1269684,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BB" & vbCrLf
'Case 66: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120402,43259,     8164, 2, 3,0007815664 ,02A ,###,######,##, 1,     26.9,#########,2,0, 4,NA,NR,NA,0,NA,26.944979,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D3" & vbCrLf
'Case 67: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120401,43259,     4572, 2, 3,0007815664 ,04A ,###,######,##, 1,     92.9,#########,2,0, 4,LO,NR,NR,0,NA,92.940651,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C1" & vbCrLf
'Case 68: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120402,43259, 2, 3,0007815664 , 1,ANION GAP (1)       ,OK,4.8999939,        ]C0" & vbCrLf
'Case 69: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120402,43259,0007815664 , 2, 3]5D" & vbCrLf
'Case 70: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115326,43260,RG, 2, 4,RO,#########,SE,0007815666 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  6,03C ,73C ,01A ,01B ,02A ,04A ]FB" & vbCrLf
'Case 71: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120449,43260,     3714, 2, 4,0007815666 ,03C ,###,######,##, 1,      1.8,#########,2,0, 0,NA,NR,NA,0,NA,1.8279570,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F5" & vbCrLf
'Case 72: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120448,43260,     8124, 2, 4,0007815666 ,73C ,###,######,##, 1,     0.27,#########,2,0, 3,NR,NR,NR,0,NA,4.4333816,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C1" & vbCrLf
'Case 73: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120449,43260,     1105, 2, 4,0007815666 ,01A ,###,######,##, 1,    137.2,#########,2,0, 4,NR,NR,NR,0,NA,137.23798,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B6" & vbCrLf
'Case 74: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120450,43260,     8193, 2, 4,0007815666 ,01B ,###,######,##, 1,     3.71,#########,2,0, 4,NR,NR,NR,0,NA,3.7142105,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D2" & vbCrLf
'Case 75: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120451,43260,     1860, 2, 4,0007815666 ,02A ,###,######,##, 1,     30.8,#########,2,0, 4,NA,NR,NA,0,NA,30.805193,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F3" & vbCrLf
'Case 76: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120451,43260,     3386, 2, 4,0007815666 ,04A ,###,######,##, 1,    101.6,#########,2,0, 4,NR,NR,NR,0,NA,101.59410,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C5" & vbCrLf
'Case 77: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120451,43260, 2, 4,0007815666 , 1,ANION GAP (1)       ,OK,4.8000031,        ]ED" & vbCrLf
'Case 78: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120451,43260,0007815666 , 2, 4]5E" & vbCrLf
'Case 79: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115330,43261,RG, 2, 5,RO,#########,SE,0007815669 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,03C ,01A ,01B ,02A ,04A ]F5" & vbCrLf
'Case 80: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120536,43261,     2776, 2, 5,0007815669 ,03C ,###,######,##, 1,      2.1,#########,2,0, 0,NA,NR,NA,0,NA,2.0752690,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FA" & vbCrLf
'Case 81: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120537,43261,     3261, 2, 5,0007815669 ,01A ,###,######,##, 1,    137.1,#########,2,0, 4,NR,NR,NR,0,NA,137.13512,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C0" & vbCrLf
'Case 82: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120537,43261,     2691, 2, 5,0007815669 ,01B ,###,######,##, 1,     4.76,#########,2,0, 4,NR,NR,NR,0,NA,4.7637258,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B1" & vbCrLf
'Case 83: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120539,43261,     4311, 2, 5,0007815669 ,02A ,###,######,##, 1,     25.7,#########,2,0, 4,NA,NR,NA,0,NA,25.684420,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E8" & vbCrLf
'Case 84: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120538,43261,     8434, 2, 5,0007815669 ,04A ,###,######,##, 1,    103.8,#########,2,0, 4,NR,NR,NR,0,NA,103.80252,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B7" & vbCrLf
'Case 85: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120539,43261, 2, 5,0007815669 , 1,ANION GAP (1)       ,OK,7.6000061,        ]DD" & vbCrLf
'Case 86: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120539,43261,0007815669 , 2, 5]52" & vbCrLf
'Case 87: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115331,43262,RG, 2, 6,RO,#########,SE,0007815740 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,03C ,01A ,01B ,02A ,04A ]FC" & vbCrLf
'Case 88: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120624,43262,     4456, 2, 6,0007815740 ,03C ,###,######,##, 1,      1.7,#########,2,0, 0,NA,NR,NA,0,NA,1.6774194,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FA" & vbCrLf
'Case 89: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120624,43262,     6572, 2, 6,0007815740 ,01A ,###,######,##, 1,    134.0,#########,2,0, 4,LO,NR,NR,0,NA,133.97824,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BE" & vbCrLf
'Case 90: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120625,43262,     2709, 2, 6,0007815740 ,01B ,###,######,##, 1,     4.95,#########,2,0, 4,NR,NR,NR,0,NA,4.9489708,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B3" & vbCrLf
'Case 91: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120626,43262,     1479, 2, 6,0007815740 ,02A ,###,######,##, 1,     28.7,#########,2,0, 4,NA,NR,NA,0,NA,28.690683,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D9" & vbCrLf
'Case 92: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120625,43262,     5222, 2, 6,0007815740 ,04A ,###,######,##, 1,    100.5,#########,2,0, 4,NR,NR,NR,0,NA,100.50459,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CD" & vbCrLf
'Case 93: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120626,43262, 2, 6,0007815740 , 1,ANION GAP (1)       ,OK,4.8000031,        ]EC" & vbCrLf
'Case 94: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120626,43262,0007815740 , 2, 6]5D" & vbCrLf
'Case 95: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115334,43263,RG, 2, 7,RO,#########,SE,0007815742 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1150,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,03C ,01A ,01B ,02A ,04A ]F5" & vbCrLf
'Case 96: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120712,43263,     4244, 2, 7,0007815742 ,03C ,###,######,##, 1,      2.4,#########,2,0, 0,NA,NR,NA,0,NA,2.3763442,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]07" & vbCrLf
'Case 97: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120712,43263,     6745, 2, 7,0007815742 ,01A ,###,######,##, 1,    133.7,#########,2,0, 4,LO,NR,NR,0,NA,133.67604,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BB" & vbCrLf
'Case 98: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120713,43263,     6417, 2, 7,0007815742 ,01B ,###,######,##, 1,     2.79,#########,2,0, 4,LO,NR,NR,0,NA,2.7912903,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C6" & vbCrLf
'Case 99: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120714,43263,     6910, 2, 7,0007815742 ,02A ,###,######,##, 1,     27.0,#########,2,0, 4,NA,NR,NA,0,NA,27.045408,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F0" & vbCrLf
'Case 100: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120713,43263,     6344, 2, 7,0007815742 ,04A ,###,######,##, 1,     94.0,#########,2,0, 4,LO,NR,NR,0,NA,93.962395,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BD" & vbCrLf
'Case 101: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120714,43263, 2, 7,0007815742 , 1,ANION GAP (1)       ,OK,12.699997,        ]C6" & vbCrLf
'Case 102: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120714,43263,0007815742 , 2, 7]5B" & vbCrLf
'Case 103: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115351,43264,RG, 3, 1,RO,#########,SE,0007815744 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,03C ,01A ,01B ,02A ,04A ]F7" & vbCrLf
'Case 104: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120759,43264,     1477, 3, 1,0007815744 ,03C ,###,######,##, 1,      1.3,#########,2,0, 0,NA,NR,NA,0,NA,1.3333334,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]03" & vbCrLf
'Case 105: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120800,43264,     3929, 3, 1,0007815744 ,01A ,###,######,##, 1,    137.3,#########,2,0, 4,NR,NR,NR,0,NA,137.34090,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BC" & vbCrLf
'Case 106: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120801,43264,     3597, 3, 1,0007815744 ,01B ,###,######,##, 1,     2.61,#########,2,0, 4,LO,NR,NR,0,NA,2.6135254,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D2" & vbCrLf
'Case 107: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120802,43264,     8192, 3, 1,0007815744 ,02A ,###,######,##, 1,     20.5,#########,2,0, 4,NA,NR,NA,0,NA,20.466921,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F2" & vbCrLf
'Case 108: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120801,43264,     7798, 3, 1,0007815744 ,04A ,###,######,##, 1,    106.7,#########,2,0, 4,NR,NR,NR,0,NA,106.73307,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B0" & vbCrLf
'Case 109: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120802,43264, 3, 1,0007815744 , 1,ANION GAP (1)       ,OK,10.100006,        ]F6" & vbCrLf
'Case 110: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120802,43264,0007815744 , 3, 1]5F" & vbCrLf
'Case 111: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115354,43265,RG, 3, 2,RO,#########,SE,0007815746 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,03C ,01A ,01B ,02A ,04A ]F0" & vbCrLf
'Case 112: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120847,43265,     7943, 3, 2,0007815746 ,03C ,###,######,##, 1,      1.1,#########,2,0, 0,NA,NR,NA,0,NA,1.0967743,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F1" & vbCrLf
'Case 113: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120848,43265,     1396, 3, 2,0007815746 ,01A ,###,######,##, 1,    128.8,#########,2,0, 4,LO,NR,NR,0,NA,128.82280,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AC" & vbCrLf
'Case 114: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120849,43265,     3756, 3, 2,0007815746 ,01B ,###,######,##, 1,     3.96,#########,2,0, 4,NR,NR,NR,0,NA,3.9614692,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AB" & vbCrLf
'Case 115: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120850,43265,     2194, 3, 2,0007815746 ,02A ,###,######,##, 1,     29.2,#########,2,0, 4,NA,NR,NA,0,NA,29.167448,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]DE" & vbCrLf
'Case 116: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120849,43265,     1146, 3, 2,0007815746 ,04A ,###,######,##, 1,     90.5,#########,2,0, 4,LO,NR,NR,0,NA,90.525520,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C7" & vbCrLf
'Case 117: objAutoAn.AutoRecepcion "[ 0,702,11,080999,120850,43265, 3, 2,0007815746 , 1,ANION GAP (1)       ,OK,9.1000061,        ]E6" & vbCrLf
'Case 118: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120850,43265,0007815746 , 3, 2]58" & vbCrLf
'Case 119: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115356,43266,RG, 3, 3,RO,#########,SE,0007815761 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]B0" & vbCrLf
'Case 120: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120932,43266,     2949, 3, 3,0007815761 ,03C ,###,######,##, 1,      1.6,#########,2,0, 0,NA,NR,NA,0,NA,1.5698925,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E9" & vbCrLf
'Case 121: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120932,43266,0007815761 , 3, 3]58" & vbCrLf
'Case 122: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115359,43267,RG, 3, 4,RO,#########,SE,0007815768 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,73C ]9D" & vbCrLf
'Case 123: objAutoAn.AutoRecepcion "[ 0,702,03,080999,120958,43267,     2570, 3, 4,0007815768 ,73C ,###,######,##, 1,     0.31,#########,2,0, 3,NR,NR,NR,0,NA,5.2182751,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B7" & vbCrLf
'Case 124: objAutoAn.AutoRecepcion "[ 0,702,05,080999,120958,43267,0007815768 , 3, 4]47" & vbCrLf
'Case 125: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115405,43268,RG, 3, 5,RO,#########,SE,0007815776 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  2,03C ,73C ]B1" & vbCrLf
'Case 126: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121108,43268,     1179, 3, 5,0007815776 ,03C ,###,######,##, 1,      0.7,#########,2,0, 0,NA,NR,NA,0,NA,0.7419356,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F3" & vbCrLf
'Case 127: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121107,43268,     5377, 3, 5,0007815776 ,73C ,###,######,##, 1,     0.38,#########,2,0, 3,NR,NR,NR,0,NA,6.4082756,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AD" & vbCrLf
'Case 128: objAutoAn.AutoRecepcion "[ 0,702,05,080999,121108,43268,0007815776 , 3, 5]52" & vbCrLf
'Case 129: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115409,43269,RG, 3, 6,RO,#########,SE,0007815782 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]A8" & vbCrLf
'Case 130: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121155,43269,      751, 3, 6,0007815782 ,03C ,###,######,##, 1,      0.9,#########,2,0, 0,NA,NR,NA,0,NA,0.9139786,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FD" & vbCrLf
'Case 131: objAutoAn.AutoRecepcion "[ 0,702,05,080999,121155,43269,0007815782 , 3, 6]51" & vbCrLf
'Case 132: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115410,43270,RG, 3, 7,RO,#########,SE,0007815801 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  6,03C ,73C ,01A ,01B ,02A ,04A ]04" & vbCrLf
'Case 133: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121247,43270,     2885, 3, 7,0007815801 ,03C ,###,######,##, 1,      1.1,#########,2,0, 0,NA,NR,NA,0,NA,1.1075270,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]0B" & vbCrLf
'Case 134: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121246,43270,     8471, 3, 7,0007815801 ,73C ,###,######,##, 1,     0.53,#########,2,0, 3,HI,NR,NR,0,NA,8.8895521,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C5" & vbCrLf
'Case 135: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121247,43270,     1815, 3, 7,0007815801 ,01A ,###,######,##, 1,    140.1,#########,2,0, 4,NR,NR,NR,0,NA,140.14615,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CE" & vbCrLf
'Case 136: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121248,43270,      399, 3, 7,0007815801 ,01B ,###,######,##, 1,     3.96,#########,2,0, 4,NR,NR,NR,0,NA,3.9614692,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C8" & vbCrLf
'Case 137: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121249,43270,     7804, 3, 7,0007815801 ,02A ,###,######,##, 1,     28.4,#########,2,0, 4,NA,NR,NA,0,NA,28.376585,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]DB" & vbCrLf
'Case 138: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121248,43270,     2442, 3, 7,0007815801 ,04A ,###,######,##, 1,    104.0,#########,2,0, 4,NR,NR,NR,0,NA,104.02553,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D0" & vbCrLf
'Case 139: objAutoAn.AutoRecepcion "[ 0,702,11,080999,121249,43270, 3, 7,0007815801 , 1,ANION GAP (1)       ,OK,7.7000122,        ]E8" & vbCrLf
'Case 140: objAutoAn.AutoRecepcion "[ 0,702,05,080999,121249,43270,0007815801 , 3, 7]5C" & vbCrLf
'Case 141: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115429,43271,RG, 4, 1,RO,#########,SE,0007815811 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1151,                    ,   ,5,      ,M,                         ,#######,####,####,######,  2,03C ,73C ]BE" & vbCrLf
'Case 142: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121331,43271,     3300, 4, 1,0007815811 ,03C ,###,######,##, 1,      0.6,#########,2,0, 0,NA,NR,NA,0,NA,0.6451614,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]1D" & vbCrLf
'Case 143: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121331,43271,     5353, 4, 1,0007815811 ,73C ,###,######,##, 1,     0.38,#########,2,0, 3,NR,NR,NR,0,NA,6.2563601,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D0" & vbCrLf
'Case 144: objAutoAn.AutoRecepcion "[ 0,702,05,080999,121331,43271,0007815811 , 4, 1]67" & vbCrLf
'Case 145: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115432,43272,RG, 4, 2,RO,#########,SE,0007815842 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1152,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]B7" & vbCrLf
'Case 146: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121419,43272,     3511, 4, 2,0007815842 ,03C ,###,######,##, 1,      1.1,#########,2,0, 0,NA,NR,NA,0,NA,1.0860215,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]14" & vbCrLf
'Case 147: objAutoAn.AutoRecepcion "[ 0,702,05,080999,121419,43272,0007815842 , 4, 2]5A" & vbCrLf
'Case 148: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115435,43273,RG, 4, 3,RO,#########,SE,0007815849 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1152,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]AB" & vbCrLf
'Case 149: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121506,43273,     8669, 4, 3,0007815849 ,03C ,###,######,##, 1,      0.8,#########,2,0, 0,NA,NR,NA,0,NA,0.8172044,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F2" & vbCrLf
'Case 150: objAutoAn.AutoRecepcion "[ 0,702,05,080999,121506,43273,0007815849 , 4, 3]54" & vbCrLf
'Case 151: objAutoAn.AutoRecepcion "[ 0,702,01,080999,115438,43274,RG, 4, 4,RO,#########,SE,0007815863 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,080999,1152,                    ,   ,5,      ,M,                         ,#######,####,####,######,  6,03C ,73C ,01A ,01B ,02A ,04A ]EF" & vbCrLf
'Case 152: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121558,43274,     7528, 4, 4,0007815863 ,03C ,###,######,##, 1,      1.1,#########,2,0, 0,NA,NR,NA,0,NA,1.0860215,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FD" & vbCrLf
'Case 153: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121558,43274,     6826, 4, 4,0007815863 ,73C ,###,######,##, 1,     0.33,#########,2,0, 3,NR,NR,NR,0,NA,5.5727429,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AB" & vbCrLf
'Case 154: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121559,43274,     1686, 4, 4,0007815863 ,01A ,###,######,##, 1,    141.9,#########,2,0, 4,NR,NR,NR,0,NA,141.93761,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]A5" & vbCrLf
'Case 155: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121600,43274,     8296, 4, 4,0007815863 ,01B ,###,######,##, 1,     4.13,#########,2,0, 4,NR,NR,NR,0,NA,4.1269684,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BC" & vbCrLf
'Case 156: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121601,43274,     1267, 4, 4,0007815863 ,02A ,###,######,##, 1,#########,#########,2,1, 4,NA,NR,NA,0,NA,#########,X        ,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]74" & vbCrLf
'Case 157: objAutoAn.AutoRecepcion "[ 0,702,03,080999,121600,43274,     3695, 4, 4,0007815863 ,04A ,###,######,##, 1,    100.1,#########,2,0, 4,NR,NR,NR,0,NA,100.07153,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C9" & vbCrLf
'Case 158: objAutoAn.AutoRecepcion "[ 0,702,05,080999,121601,43274,0007815863 , 4, 4]5A" & vbCrLf
'Case 159: objAutoAn.AutoRecepcion "[ 0,703,17,080999,121623]D4" & vbCrLf
'
'
'
'
''    Case 0: objAutoAn.AutoRecepcion "[ 0,701,06,0007779604 ,0007779605 ,0007779606 ,0007779602 ,           ,           ,           ]AD" & vbCrLf
''    Case 1: objAutoAn.AutoRecepcion "[ 0,701,02, 0, 4901, 0, 0,0007779605 ]C3" & vbCrLf
''    Case 2: objAutoAn.AutoRecepcion "[ 0,701,02, 0, 4902, 0, 0,0007779606 ]C1" & vbCrLf
''    Case 3: objAutoAn.AutoRecepcion "[ 0,701,06,0007779598 ,           ,           ,           ,           ,           ,           ]FA" & vbCrLf
''    Case 4: objAutoAn.AutoRecepcion "[ 0,701,02, 0, 4903, 0, 0,0007779598 ]B6" & vbCrLf
''    Case 5: objAutoAn.AutoRecepcion "[ 0,702,01,260799,171410, 4901,RG, 7, 2,RO,#########,SE,0007779605 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,260799,1707,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,06D ]AE" & vbCrLf
''    Case 6: objAutoAn.AutoRecepcion "[ 0,702,03,260799,171511, 4901,     3341, 7, 2,0007779605 ,06D ,###,810012,##, 1,       79,#########,2,0,00,NR,NR,NR,0,NA,78.507446,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]80" & vbCrLf
''    Case 7: objAutoAn.AutoRecepcion "[ 0,702,81,260799,171511, 4901,#########,     3341,0007779605 , 7, 2,385875980, 1,06D ,##,#,0,0,#####,#####,###,###,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,480.06400,78.507446,#########,2,00,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO, 2855, -830,  480,#####,#####,1.0000,#######################]3A" & vbCrLf
''    Case 8: objAutoAn.AutoRecepcion "[ 0,702,05,260799,171511, 4901,0007779605 , 7, 2]69" & vbCrLf
''    Case 9: objAutoAn.AutoRecepcion "[ 0,702,01,260799,171410, 4902,RG, 7, 3,RO,#########,SE,0007779606 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,260799,1707,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,06D ]AB" & vbCrLf
''    Case 10: objAutoAn.AutoRecepcion "[ 0,702,03,260799,171551, 4902,     3342, 7, 3,0007779606 ,06D ,###,810012,##, 1,       91,#########,2,0,00,NR,NR,NR,0,NA,91.304466,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]86" & vbCrLf
''    Case 11: objAutoAn.AutoRecepcion "[ 0,702,81,260799,171551, 4902,#########,     3342,0007779606 , 7, 3,385875980, 1,06D ,##,#,0,0,#####,#####,###,###,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,#########,560.28802,91.304466,#########,2,00,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO, 3395, -886,  560,#####,#####,1.0000,#######################]27" & vbCrLf
''    Case 12: objAutoAn.AutoRecepcion "[ 0,702,05,260799,171551, 4902,0007779606 , 7, 3]62" & vbCrLf
''    Case 13: objAutoAn.AutoRecepcion "[ 0,702,01,260799,171442, 4903,RG, 6, 1,RO,#########,SE,0007779598 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,260799,1708,                    ,  0,5,      ,M,                         ,#######,####,####,######,  5,30A ,31A ,36A ,11A ,12A ]D9" & vbCrLf
''    Case 14: objAutoAn.AutoRecepcion "[ 0,702,03,260799,172214, 4903,     3343, 6, 1,0007779598 ,30A ,###,902013, 8, 1,       22,#########,2,0,09,NR,NR,NR,0,NA,21.830095,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]4F" & vbCrLf
''    Case 15: objAutoAn.AutoRecepcion "[ 0,702,03,260799,172214, 4903,     3343, 6, 1,0007779598 ,30A ,###,902013, 8, 1,       22,#########,2,0,09,NR,NR,NR,0,NA,21.830095,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]4F" & vbCrLf
''    Case 16: objAutoAn.AutoRecepcion "[ 0,702,81,260799,172214, 4903,#########,     3343,0007779598 , 6, 1,385875985, 1,30A , 8,#,0,0,#####,#####,###,###,0.5137920,0.4909233,#########,-0.091930,#########,#########,-0.000362,0.5626307,0.0001879,0.0004454,-0.005719,0.4942325,0.0000963,0.0001646,-0.005719,21.830095,#########,2,09,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,#####,#####,#####,#####,#####,1.0000,#######################]CA" & vbCrLf
''    Case 17: objAutoAn.AutoRecepcion "[ 0,702,81,260799,172214, 4903,#########,     3343,0007779598 , 6, 1,385875985, 1,30A , 8,#,0,0,#####,#####,###,###,0.5137920,0.4909233,#########,-0.091930,#########,#########,-0.000362,0.5626307,0.0001879,0.0004454,-0.005719,0.4942325,0.0000963,0.0001646,-0.005719,21.830095,#########,2,09,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,#####,#####,#####,#####,#####,1.0000,#######################]CA" & vbCrLf
''    Case 18: objAutoAn.AutoRecepcion "[ 0,702,03,260799,172206, 4903,     3343, 6, 1,0007779598 ,31A ,###,902225, 7, 1,       21,#########,2,0,09,NR,NR,NR,0,NA,20.678816,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]40" & vbCrLf
''    Case 19: objAutoAn.AutoRecepcion "[ 0,702,81,260799,172206, 4903,#########,     3343,0007779598 , 6, 1,385875992, 1,31A , 7,#,0,0,#####,#####,###,###,0.5357788,0.5164491,#########,-0.091784,#########,#########,-0.000902,0.5926792,0.0001378,0.0002566,-0.005418,0.5196831,0.0001746,0.0003786,-0.005418,20.678816,#########,2,09,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,#####,#####,#####,#####,#####,1.0000,#######################]A0" & vbCrLf
''    Case 20: objAutoAn.AutoRecepcion "[ 0,702,03,260799,172206, 4903,     3343, 6, 1,0007779598 ,36A ,###,902184, 6, 1,       12,#########,2,0,09,NR,NR,NR,0,NA,12.220160,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]50" & vbCrLf
''    Case 21: objAutoAn.AutoRecepcion "[ 0,702,81,260799,172206, 4903,#########,     3343,0007779598 , 6, 1,385875998, 1,36A , 6,#,0,0,#####,#####,###,###,0.1861796,0.1936564,#########,0.0387155,#########,#########,0.0017745,0.1775204,0.0004194,0.0007872,0.0024396,0.1909810,0.0003898,0.0010272,0.0024396,12.220160,#########,2,09,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,#####,#####,#####,#####,#####,1.0000,#######################]9B" & vbCrLf
''    Case 22: objAutoAn.AutoRecepcion "[ 0,702,03,260799,172207, 4903,     3343, 6, 1,0007779598 ,11A ,###,901224, A, 1,     0.42,#########,2,0,00,NR,NR,NR,0,NA,0.4244145,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]2F" & vbCrLf
''    Case 23: objAutoAn.AutoRecepcion "[ 0,702,81,260799,172207, 4903,#########,     3343,0007779598 , 6, 1,385876008, 1,11A , A,#,0,0,#####,#####,###,###,0.0198609,0.0234056,#########,-0.095298,#########,#########,0.0043357,0.0148517,0.0010666,0.0018857,0.0035330,0.0232613,0.0013600,0.0019930,0.0084096,0.4244145,#########,2,00,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,#####,#####,#####,#####,#####,1.0000,#######################]CE" & vbCrLf
''    Case 24: objAutoAn.AutoRecepcion "[ 0,702,03,260799,172654, 4903,     3343, 6, 1,0007779598 ,12A ,###,902017, 9, 1,     0.02,#########,2,0,00,NR,NR,NR,0,NA,0.0204482,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]37" & vbCrLf
''    Case 25: objAutoAn.AutoRecepcion "[ 0,702,81,260799,172654, 4903,#########,     3343,0007779598 , 6, 1,385876009, 1,12A , 9,#,0,0,#####,#####,###,###,0.0063904,0.0066622,#########,0.0026571,#########,#########,0.0001651,0.0025383,0.0003003,0.0005256,-0.001159,0.0072343,0.0003957,0.0007472,0.0046960,0.0204482,#########,2,00,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,#####,#####,#####,#####,#####,1.0000,#######################]FF" & vbCrLf
''    Case 26: objAutoAn.AutoRecepcion "[ 0,702,11,260799,171442, 4903, 6, 1,0007779598 , 1,INDIRECT BILI       ,OK,0.4000000,mg/dL   ]60" & vbCrLf
''    Case 27: objAutoAn.AutoRecepcion "[ 0,702,05,260799,172654, 4903,0007779598 , 6, 1]55" & vbCrLf
'
'    'Case 1: objAutoAn.AutoRecepcion "[ 0,701,02, 0, 4901, 0, 0,0007779605 ]C3" &
''    Case 0: objAutoAn.AutoRecepcion "[ 0,701,06,0007769885 ,0007769889 ,0007769892 ,0007770021 ,0007770032 ,           ,           ]E4" & vbCrLf
''    Case 1: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32302, 0, 0,0007769885 ]AE" & vbCrLf
''    Case 2: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32303, 0, 0,0007769889 ]A9" & vbCrLf
''    Case 3: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32304, 0, 0,0007769892 ]AE" & vbCrLf
''    Case 4: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32305, 0, 0,0007770021 ]C5" & vbCrLf
''    Case 5: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32306, 0, 0,0007770032 ]C2" & vbCrLf
''    Case 6: objAutoAn.AutoRecepcion "[ 0,701,06,0007770101 ,           ,           ,           ,           ,           ,           ]17" & vbCrLf
''    Case 7: objAutoAn.AutoRecepcion "[ 0,701,06,0007770047 ,0007770058 ,0007770072 ,0007770075 ,0007770079 ,0007770084 ,0007770096 ]83" & vbCrLf
''    Case 8: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32307, 0, 0,0007770047 ]BB" & vbCrLf
''    Case 9: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32308, 0, 0,0007770058 ]B8" & vbCrLf
''    Case 10: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32309, 0, 0,0007770075 ]B8" & vbCrLf
''    Case 11: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32310, 0, 0,0007770084 ]C0" & vbCrLf
''    Case 12: objAutoAn.AutoRecepcion "[ 0,701,02, 0,32311, 0, 0,0007770096 ]BC" & vbCrLf
''    Case 13: objAutoAn.AutoRecepcion "[ 0,701,06,0007769885 ,0007769889 ,0007769892 ,0007770021 ,0007770032 ,           ,           ]E4" & vbCrLf
''    Case 14: objAutoAn.AutoRecepcion "[ 0,701,06,0007770101 ,           ,           ,           ,           ,           ,           ]17" & vbCrLf
''    Case 15: objAutoAn.AutoRecepcion "[ 0,701,06,0007770047 ,0007770058 ,0007770072 ,0007770075 ,0007770079 ,0007770084 ,0007770096 ]83" & vbCrLf
''    Case 16: objAutoAn.AutoRecepcion "[ 0,700,02,150799, 94828,v4.7.05.01,B,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]5F" & vbCrLf
''    '                                  [ 0,702,01,140799,162852, 4067,RG, 3, 1,RO,#########,SE,GEMMA      ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  6,01A ,01B ,04A ,02A ,73D ,03E ]C1" & vbcrlf
''    Case 17: objAutoAn.AutoRecepcion "[ 0,702,01,140799,162852, 4067,RG, 3, 1,RO,#########,SE,GEMMA      ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  6,01A ,01B ,04A ,02A ,73D ,03E ]C1" & vbCrLf
''    'Case 17: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95025,32305,RG, 3, 5,ST,#########,SE,0007770021 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0942,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]CC" & vbCrLf
''    Case 18: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95140,32305,     5703, 3, 5,0007770021 ,03C ,###,######,##, 1,      1.3,#########,2,0, 0,NA,NR,NA,0,NA,1.3291535,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]28" & vbCrLf
''    Case 19: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95140,32305,0007770021 , 3, 5]7B" & vbCrLf
''    Case 20: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95020,32302,RG, 3, 1,RO,#########,SE,0007769885 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0942,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]F6" & vbCrLf
''    Case 21: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95231,32302,     7922, 3, 1,0007769885 ,01A ,###,######,##, 1,    136.7,#########,2,0, 4,NR,NR,NR,0,NA,136.70099,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BA" & vbCrLf
''    Case 22: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95232,32302,     5510, 3, 1,0007769885 ,01B ,###,######,##, 1,     2.99,#########,2,0, 4,LO,NR,NR,0,NA,2.9940181,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D4" & vbCrLf
''    Case 23: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95233,32302,     4738, 3, 1,0007769885 ,02A ,###,######,##, 1,     28.6,#########,2,0, 4,NA,NR,NA,0,NA,28.602802,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]EF" & vbCrLf
''    Case 24: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95232,32302,     3864, 3, 1,0007769885 ,04A ,###,######,##, 1,    105.1,#########,2,0, 4,NR,NR,NR,0,NA,105.10950,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CD" & vbCrLf
''    Case 25: objAutoAn.AutoRecepcion "[ 0,702,11,150799, 95233,32302, 3, 1,0007769885 , 1,ANION GAP (1)       ,OK,3.0000000,        ] 1" & vbCrLf
''    Case 26: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95233,32302,0007769885 , 3, 1] 65" & vbCrLf
''    Case 27: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95021,32303,RG, 3, 2,RO,#########,SE,0007769889 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0942,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]EF" & vbCrLf
''    Case 28: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95319,32303,      532, 3, 2,0007769889 ,01A ,###,######,##, 1,    138.3,#########,2,0, 4,NR,NR,NR,0,NA,138.32925,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] CB" & vbCrLf
''    Case 29: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95320,32303,     6365, 3, 2,0007769889 ,01B ,###,######,##, 1,     2.96,#########,2,0, 4,LO,NR,NR,0,NA,2.9579244,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] C2" & vbCrLf
''    Case 30: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95321,32303,     3825, 3, 2,0007769889 ,02A ,###,######,##, 1,     22.7,#########,2,0, 4,NA,NR,NA,0,NA,22.656708,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] EC" & vbCrLf
''    Case 31: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95320,32303,     1292, 3, 2,0007769889 ,04A ,###,######,##, 1,    113.1,#########,2,0, 4,HI,NR,NR,0,NA,113.05339,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] DC" & vbCrLf
''    Case 32: objAutoAn.AutoRecepcion "[ 0,702,11,150799, 95321,32303, 3, 2,0007769889 , 1,ANION GAP (1)       ,OK,2.5000000,        ] F9" & vbCrLf
''    Case 33: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95321,32303,0007769889 , 3, 2] 61" & vbCrLf
''    Case 34: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95023,32304,RG, 3, 3,RO,#########,SE,0007769892 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0942,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]F1" & vbCrLf
''    Case 35: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95407,32304,     1669, 3, 3,0007769892 ,01A ,###,######,##, 1,    134.5,#########,2,0, 4,LO,NR,NR,0,NA,134.48921,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] BD" & vbCrLf
''    Case 36: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95408,32304,     7279, 3, 3,0007769892 ,01B ,###,######,##, 1,     3.48,#########,2,0, 4,LO,NR,NR,0,NA,3.4780445,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] C3" & vbCrLf
''    Case 37: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95409,32304,      419, 3, 3,0007769892 ,02A ,###,######,##, 1,     28.2,#########,2,0, 4,NA,NR,NA,0,NA,28.154236,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] 1" & vbCrLf
''    Case 38: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95408,32304,     4823, 3, 3,0007769892 ,04A ,###,######,##, 1,    103.1,#########,2,0, 4,NR,NR,NR,0,NA,103.14399,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] C3" & vbCrLf
''    Case 39: objAutoAn.AutoRecepcion "[ 0,702,11,150799, 95409,32304, 3, 3,0007769892 , 1,ANION GAP (1)       ,OK,3.1999969,        ] C6" & vbCrLf
''    Case 40: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95409,32304,0007769892 , 3, 3]5E" & vbCrLf
''    Case 41: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95026,32306,RG, 3, 7,RO,#########,SE,0007770032 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0942,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]FE" & vbCrLf
''    Case 42: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95455,32306,     5453, 3, 7,0007770032 ,01A ,###,######,##, 1,    141.7,#########,2,0, 4,NR,NR,NR,0,NA,141.74034,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] D2" & vbCrLf
''    Case 43: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95455,32306,     2485, 3, 7,0007770032 ,01B ,###,######,##, 1,     3.84,#########,2,0, 4,NR,NR,NR,0,NA,3.8362379,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] CC" & vbCrLf
''    Case 44: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95456,32306,     2558, 3, 7,0007770032 ,02A ,###,######,##, 1,     30.3,#########,2,0, 4,NA,NR,NA,0,NA,30.298893,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] F4" & vbCrLf
''    Case 45: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95456,32306,     7726, 3, 7,0007770032 ,04A ,###,######,##, 1,    105.1,#########,2,0, 4,NR,NR,NR,0,NA,105.10950,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] D2" & vbCrLf
''    Case 46: objAutoAn.AutoRecepcion "[ 0,702,11,150799, 95456,32306, 3, 7,0007770032 , 1,ANION GAP (1)       ,OK,6.3000031,        ] FE" & vbCrLf
''    Case 47: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95456,32306,0007770032 , 3, 7]6C" & vbCrLf
''    Case 48: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95028,32307,RG, 2, 1,RO,#########,SE,0007770047 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0943,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]FB" & vbCrLf
''    Case 49: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95543,32307,     2353, 2, 1,0007770047 ,01A ,###,######,##, 1,    143.7,#########,2,0, 4,NR,NR,NR,0,NA,143.73714,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] D0" & vbCrLf
''    Case 50: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95543,32307,     4168, 2, 1,0007770047 ,01B ,###,######,##, 1,     3.89,#########,2,0, 4,NR,NR,NR,0,NA,3.8870358,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] C8" & vbCrLf
''    Case 51: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95544,32307,     8750, 2, 1,0007770047 ,02A ,###,######,##, 1,     27.4,#########,2,0, 4,NA,NR,NA,0,NA,27.418941,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] F5" & vbCrLf
''    Case 52: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95544,32307,     3396, 2, 1,0007770047 ,04A ,###,######,##, 1,    112.8,#########,2,0, 4,HI,NR,NR,0,NA,112.81964,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] D4" & vbCrLf
''    Case 53: objAutoAn.AutoRecepcion "[ 0,702,11,150799, 95544,32307, 2, 1,0007770047 , 1,ANION GAP (1)       ,OK,3.5000000,        ] 5" & vbCrLf
''    Case 54: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95544,32307,0007770047 , 2, 1]6E" & vbCrLf
''    Case 55: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95029,32308,RG, 2, 2,RO,#########,SE,0007770058 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0943,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]F6" & vbCrLf
''    Case 56: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95631,32308,     2956, 2, 2,0007770058 ,01A ,###,######,##, 1,    137.6,#########,2,0, 4,NR,NR,NR,0,NA,137.61488,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] BB" & vbCrLf
''    Case 57: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95631,32308,     5015, 2, 2,0007770058 ,01B ,###,######,##, 1,     3.94,#########,2,0, 4,NR,NR,NR,0,NA,3.9448428,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] D2" & vbCrLf
''    Case 58: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95632,32308,     3917, 2, 2,0007770058 ,02A ,###,######,##, 1,     30.5,#########,2,0, 4,NA,NR,NA,0,NA,30.488865,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] F2" & vbCrLf
''    Case 59: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95632,32308,     3438, 2, 2,0007770058 ,04A ,###,######,##, 1,    103.8,#########,2,0, 4,NR,NR,NR,0,NA,103.79574,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] C2" & vbCrLf
''    Case 60: objAutoAn.AutoRecepcion "[ 0,702,11,150799, 95632,32308, 2, 2,0007770058 , 1,ANION GAP (1)       ,OK,3.3000031,        ] 1" & vbCrLf
''    Case 61: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95632,32308,0007770058 , 2, 2]6C" & vbCrLf
''    Case 62: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95031,32309,RG, 2, 4,RO,#########,SE,0007770075 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0943,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C9" & vbCrLf
''    Case 63: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95715,32309,     5625, 2, 4,0007770075 ,03C ,###,######,##, 1,      1.1,#########,2,0, 0,NA,NR,NA,0,NA,1.0548589,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] 9" & vbCrLf
''    Case 64: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95715,32309,0007770075 , 2, 4] 68" & vbCrLf
''    Case 65: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95032,32310,RG, 2, 6,RO,#########,SE,0007770084 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0943,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]00" & vbCrLf
''    Case 66: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95806,32310,     3842, 2, 6,0007770084 ,01A ,###,######,##, 1,    140.6,#########,2,0, 4,NR,NR,NR,0,NA,140.59529,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] C9" & vbCrLf
''    Case 67: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95807,32310,     4914, 2, 6,0007770084 ,01B ,###,######,##, 1,     4.25,#########,2,0, 4,NR,NR,NR,0,NA,4.2515020,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] E6" & vbCrLf
''    Case 68: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95808,32310,     6790, 2, 6,0007770084 ,02A ,###,######,##, 1,     30.0,#########,2,0, 4,NA,NR,NA,0,NA,30.045322,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]0B" & vbCrLf
''    Case 69: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95807,32310,     7283, 2, 6,0007770084 ,04A ,###,######,##, 1,    105.8,#########,2,0, 4,NR,NR,NR,0,NA,105.77156,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] C2" & vbCrLf
''    Case 70: objAutoAn.AutoRecepcion "[ 0,702,11,150799, 95808,32310, 2, 6,0007770084 , 1,ANION GAP (1)       ,OK,4.8000031,        ] FA" & vbCrLf
''    Case 71: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95808,32310,0007770084 , 2, 6]6B" & vbCrLf
''    Case 72: objAutoAn.AutoRecepcion "[ 0,702,01,150799, 95034,32311,RG, 2, 7,RO,#########,SE,0007770096 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,150799,0943,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C7" & vbCrLf
''    Case 73: objAutoAn.AutoRecepcion "[ 0,702,03,150799, 95851,32311,     7516, 2, 7,0007770096 ,03C ,###,######,##, 1,      0.9,#########,2,0, 0,NA,NR,NA,0,NA,0.9012538,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]0D" & vbCrLf
''    Case 74: objAutoAn.AutoRecepcion "[ 0,702,05,150799, 95851,32311,0007770096 , 2, 7] 68" & vbCrLf
''    Case 75: objAutoAn.AutoRecepcion "[ 0,703,17,150799, 95915] DA" & vbCrLf
''    Case 76: objAutoAn.AutoRecepcion "[ 0,701,04, 0, 1, 4, 4, 4, 4, 4, 4, 4]2B" & vbCrLf
''    Case 77: objAutoAn.AutoRecepcion "[ 0,701,06,0007770101 ,           ,           ,           ,           ,           ,           ] 17" & vbCrLf
''    Case 78: objAutoAn.AutoRecepcion "[ 0,701,06,0007770072 ,0007770079 ,           ,           ,           ,           ,           ]4B" & vbCrLf
'''
''
''
''
''
'''      Case 5: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204034,26995,     6691, 1, 1,0007746942 ,01A ,###,######,##, 1,    136.5,#########,2,0, 4,NR,NR,NR,0,NA,136.45871,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AE" & vbCrLf
'''      Case 6: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204034,26995,     5560, 1, 1,0007746942 ,01B ,###,######,##, 1,     3.47,#########,2,0, 4,LO,NR,NR,0,NA,3.4674778,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BE" & vbCrLf
'''      Case 7: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204035,26995,     2358, 1, 1,0007746942 ,02A ,###,######,##, 1,     25.3,#########,2,0, 4,NA,NR,NA,0,NA,25.278152,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]EA" & vbCrLf
'''      Case 8: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204035,26995,     6027, 1, 1,0007746942 ,04A ,###,######,##, 1,    104.8,#########,2,0, 4,NR,NR,NR,0,NA,104.82389,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B3" & vbCrLf
'''      Case 9: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204033,26995,     6192, 1, 1,0007746942 ,03C ,###,######,##, 1,      0.8,#########,2,0, 0,NA,NR,NA,0,NA,0.7638036,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FE" & vbCrLf
'''      Case 10: objAutoAn.AutoRecepcion "[ 0,702,11,200699,204035,26995, 1, 1,0007746942 , 1,ANION GAP (1)       ,OK,6.3999939,        ]C0" & vbCrLf
'''      Case 11: objAutoAn.AutoRecepcion "[ 0,702,05,200699,204035,26995,0007746942 , 1, 1]5A" & vbCrLf
'''      Case 12: objAutoAn.AutoRecepcion "[ 0,702,01,200699,203914,26996,RG, 1, 2,RO,#########,SE,0007746937 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,200699,2034,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,01A ,01B ,02A ,04A ,03C ]F2" & vbCrLf
'''      Case 13: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204121,26996,     1456, 1, 2,0007746937 ,01A ,###,######,##, 1,    139.6,#########,2,0, 4,NR,NR,NR,0,NA,139.63451,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B0" & vbCrLf
'''      Case 14: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204122,26996,     4128, 1, 2,0007746937 ,01B ,###,######,##, 1,     4.00,#########,2,0, 4,NR,NR,NR,0,NA,4.0006537,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D5" & vbCrLf
'''      Case 15: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204123,26996,     3595, 1, 2,0007746937 ,02A ,###,######,##, 1,     30.2,#########,2,0, 4,NA,NR,NA,0,NA,30.206520,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F5" & vbCrLf
'''      Case 16: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204123,26996,      127, 1, 2,0007746937 ,04A ,###,######,##, 1,    104.0,#########,2,0, 4,NR,NR,NR,0,NA,104.02087,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D9" & vbCrLf
'''      Case 17: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204121,26996,      695, 1, 2,0007746937 ,03C ,###,######,##, 1,      0.9,#########,2,0, 0,NA,NR,NA,0,NA,0.8819018,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]05" & vbCrLf
'''      Case 18: objAutoAn.AutoRecepcion "[ 0,702,11,200699,204123,26996, 1, 2,0007746937 , 1,ANION GAP (1)       ,OK,5.4000092,        ]E1" & vbCrLf
'''      Case 19: objAutoAn.AutoRecepcion "[ 0,702,05,200699,204123,26996,0007746937 , 1, 2]56" & vbCrLf
'''      Case 20: objAutoAn.AutoRecepcion "[ 0,702,01,200699,203917,26997,RG, 1, 3,RO,#########,SE,0007746951 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,200699,2034,                    ,   ,5,      ,M,                         ,#######,####,####,######,  5,01A ,01B ,02A ,04A ,03C ]F1" & vbCrLf
'''      Case 21: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204209,26997,     6717, 1, 3,0007746951 ,01A ,###,######,##, 1,    133.0,#########,2,0, 4,LO,NR,NR,0,NA,132.96053,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BA" & vbCrLf
'''      Case 22: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204210,26997,      720, 1, 3,0007746951 ,01B ,###,######,##, 1,     3.13,#########,2,0, 4,LO,NR,NR,0,NA,3.1274180,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F0" & vbCrLf
'''      Case 23: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204211,26997,     8621, 1, 3,0007746951 ,02A ,###,######,##, 1,     29.3,#########,2,0, 4,NA,NR,NA,0,NA,29.253929,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]DE" & vbCrLf
'''      Case 24: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204211,26997,      501, 1, 3,0007746951 ,04A ,###,######,##, 1,     94.8,#########,2,0, 4,LO,NR,NR,0,NA,94.774696,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C8" & vbCrLf
'''      Case 25: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204209,26997,     1952, 1, 3,0007746951 ,03C ,###,######,##, 1,      0.9,#########,2,0, 0,NA,NR,NA,0,NA,0.8604294,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F5" & vbCrLf
'''      Case 26: objAutoAn.AutoRecepcion "[ 0,702,11,200699,204211,26997, 1, 3,0007746951 , 1,ANION GAP (1)       ,OK,8.8999939,        ]B9" & vbCrLf
'''      Case 27: objAutoAn.AutoRecepcion "[ 0,702,05,200699,204211,26997,0007746951 , 1, 3]5A" & vbCrLf
'''      Case 28: objAutoAn.AutoRecepcion "[ 0,703,17,200699,204233]DE" & vbCrLf
'''
'''      Case 29: objAutoAn.AutoRecepcion "[ 0,701,04, 0, 4, 0, 0, 4, 4, 4, 4, 4]30" & vbCrLf
'''      Case 30: objAutoAn.AutoRecepcion "[ 0,701,06,0007746942 ,0007746951 ,           ,           ,           ,           ,           ]40" & vbCrLf
'''      Case 31: objAutoAn.AutoRecepcion "[ 0,701,02, 0,26998, 0, 0,0007746942 ]A1" & vbCrLf
'''      Case 32: objAutoAn.AutoRecepcion "[ 0,701,02, 0,26999, 0, 0,0007746951 ]A0" & vbCrLf
'''
'''      'Case 33: objAutoAn.AutoRecepcion "[ 0,702,01,200699,204308,26998,RG, 1, 1,RO,#########,SE,0007746942 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,200699,2038,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]E6" & vbCrLf
'''      Case 33: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204432,26998,     1667, 1, 1,0007746942 ,01A ,###,######,##, 1,    135.8,#########,2,0, 4,NR,NR,NR,0,NA,135.77238,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]A8" & vbCrLf
'''      Case 34: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204432,26998,     1573, 1, 1,0007746942 ,01B ,###,######,##, 1,     3.46,#########,2,0, 4,LO,NR,NR,0,NA,3.4560676,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C3" & vbCrLf
'''      Case 35: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204434,26998,     5487, 1, 1,0007746942 ,02A ,###,######,##, 1,     25.6,#########,2,0, 4,NA,NR,NA,0,NA,25.625652,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]DA" & vbCrLf
'''      Case 36: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204433,26998,     8362, 1, 1,0007746942 ,04A ,###,######,##, 1,    104.8,#########,2,0, 4,NR,NR,NR,0,NA,104.82389,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AA" & vbCrLf
'''      Case 37: objAutoAn.AutoRecepcion "[ 0,702,11,200699,204434,26998, 1, 1,0007746942 , 1,ANION GAP (1)       ,OK,5.3999939,        ]BB" & vbCrLf
'''      Case 38: objAutoAn.AutoRecepcion "[ 0,702,05,200699,204434,26998,0007746942 , 1, 1]54" & vbCrLf
'''
'''      Case 39: objAutoAn.AutoRecepcion "[ 0,702,01,200699,204311,26999,RG, 1, 3,RO,#########,SE,0007746951 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,200699,2038,                    ,   ,5,      ,M,                         ,#######,####,####,######,  4,01A ,01B ,02A ,04A ]E9" & vbCrLf
'''      Case 40: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204520,26999,     4692, 1, 3,0007746951 ,01A ,###,######,##, 1,    131.8,#########,2,0, 4,LO,NR,NR,0,NA,131.81174,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]B9" & vbCrLf
'''      Case 41: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204520,26999,     8070, 1, 3,0007746951 ,01B ,###,######,##, 1,     3.11,#########,2,0, 4,LO,NR,NR,0,NA,3.1064215,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]DA" & vbCrLf
'''      Case 42: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204522,26999,     8412, 1, 3,0007746951 ,02A ,###,######,##, 1,     29.6,#########,2,0, 4,NA,NR,NA,0,NA,29.648994,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CC" & vbCrLf
'''      Case 43: objAutoAn.AutoRecepcion "[ 0,702,03,200699,204521,26999,     8797, 1, 3,0007746951 ,04A ,###,######,##, 1,     93.3,#########,2,0, 4,LO,NR,NR,0,NA,93.301598,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]AD" & vbCrLf
'''      Case 44: objAutoAn.AutoRecepcion "[ 0,702,11,200699,204522,26999, 1, 3,0007746951 , 1,ANION GAP (1)       ,OK,8.9000015,        ]DB" & vbCrLf
'''      Case 45: objAutoAn.AutoRecepcion "[ 0,702,05,200699,204522,26999,0007746951 , 1, 3]53" & vbCrLf
'''      Case 46: objAutoAn.AutoRecepcion "[ 0,703,17,200699,204526]D9" & vbCrLf

    Case 1: objAutoAn.autorecepcion "[ 0,701,06,0007858490 ,0007857829 ,           ,           ,           ,           ,           ]37" & vbCrLf
    Case 2: objAutoAn.autorecepcion "[ 0,701,02, 0,12986, 0, 0,0007857829 ]A2" & vbCrLf
    Case 3: objAutoAn.autorecepcion "[ 0,701,02, 0,12987, 0, 0,0007858490 ]A6" & vbCrLf
'    Case 4: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144326,12979,RG, 9, 2,RO,#########,SE,34         ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]04" & vbCrLf
'    Case 5: objAutoAn.AutoRecepcion "[ 0,702,03,151099,145903,12979,    10103, 9, 2,34         ,03E ,###,904307,##, 1,      1.0,#########,2,0,00,NR,NR,NR,0,NA,0.9870584,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]CE" & vbCrLf
'    Case 6: objAutoAn.AutoRecepcion "[ 0,702,05,151099,145903,12979,34         , 9, 2]ED" & vbCrLf
'    Case 7: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144327,12980,RG, 9, 3,RO,#########,SE,35         ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]09" & vbCrLf
'    Case 8: objAutoAn.AutoRecepcion "[ 0,702,03,151099,145942,12980,    10104, 9, 3,35         ,03E ,###,904307,##, 1,      1.2,#########,2,0,00,HI,NR,NR,0,NA,1.2384381,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]E8" & vbCrLf
'    Case 9: objAutoAn.AutoRecepcion "[ 0,702,05,151099,145942,12980,35         , 9, 3]F0" & vbCrLf
'    Case 10: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144327,12981,RG, 9, 4,RO,#########,SE,36         ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]06" & vbCrLf
'    Case 11: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150022,12981,    10105, 9, 4,36         ,03E ,###,904307,##, 1,      1.2,#########,2,0,00,HI,NR,NR,0,NA,1.1792207,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F4" & vbCrLf
'    Case 12: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150022,12981,36         , 9, 4]FC" & vbCrLf
'    Case 13: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144406,12982,RG,10, 1,RO,#########,SE,37         ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]01" & vbCrLf
'    Case 14: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150102,12982,    10106,10, 1,37         ,03E ,###,904307,##, 1,      0.6,#########,2,0,00,NR,NR,NR,0,NA,0.6399899,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C3" & vbCrLf
'    Case 15: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150102,12982,37         ,10, 1]F6" & vbCrLf
'    Case 16: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144406,12983,RG,10, 2,RO,#########,SE,38         ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]FE" & vbCrLf
'    Case 17: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150142,12983,    10107,10, 2,38         ,03E ,###,904307,##, 1,      0.7,#########,2,0,00,NR,NR,NR,0,NA,0.6682259,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]C9" & vbCrLf
'    Case 18: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150142,12983,38         ,10, 2]EF" & vbCrLf
'    Case 19: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144407,12984,RG,10, 3,RO,#########,SE,39         ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]FA" & vbCrLf
'    Case 20: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150222,12984,    10108,10, 3,39         ,03E ,###,904307,##, 1,      0.9,#########,2,0,00,NR,NR,NR,0,NA,0.8666629,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]BF" & vbCrLf
'    Case 21: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150222,12984,39         ,10, 3]ED" & vbCrLf
'    Case 22: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144407,12985,RG,10, 4,RO,#########,SE,40         ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]00" & vbCrLf
'    Case 23: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150302,12985,    10109,10, 4,40         ,03E ,###,904307,##, 1,      1.0,#########,2,0,00,NR,NR,NR,0,NA,0.9607832,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]D5" & vbCrLf
'    Case 24: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150302,12985,40         ,10, 4]F4" & vbCrLf
'    Case 25: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144526,12946,RG, 1, 1,RO,#########,SE,1          ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]27" & vbCrLf
'    Case 26: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150342,12946,    10070, 1, 1,1          ,03E ,###,904307,##, 1,      1.0,#########,2,0,00,NR,NR,NR,0,NA,0.9862741,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]FB" & vbCrLf
'    Case 27: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150342,12946,1          , 1, 1]19" & vbCrLf
'    Case 28: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144526,12947,RG, 1, 2,RO,#########,SE,2          ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]24" & vbCrLf
'    Case 29: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150422,12947,    10071, 1, 2,2          ,03E ,###,904307,##, 1,      1.2,#########,2,0,00,HI,NR,NR,0,NA,1.1741226,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]12" & vbCrLf
'    Case 30: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150422,12947,2          , 1, 2]17" & vbCrLf
'    Case 31: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144527,12948,RG, 1, 3,RO,#########,SE,3          ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]20" & vbCrLf
'    Case 32: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150502,12948,    10072, 1, 3,3          ,03E ,###,904307,##, 1,      1.4,#########,2,0,00,HI,NR,NR,0,NA,1.4396201,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]0B" & vbCrLf
'    Case 33: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150502,12948,3          , 1, 3]15" & vbCrLf
'    Case 34: objAutoAn.AutoRecepcion "[ 0,702,01,151099,144527,12949,RG, 1, 4,RO,#########,SE,4          ,                    ,                         ,                         ,                  ,               , ,            ,                  ,      ,    ,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,03E ]1D" & vbCrLf
'    Case 35: objAutoAn.AutoRecepcion "[ 0,702,03,151099,150542,12949,    10073, 1, 4,4          ,03E ,###,904307,##, 1,      1.5,#########,2,0,00,HI,NR,NR,0,NA,1.5274657,D        ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]F7" & vbCrLf
'    Case 36: objAutoAn.AutoRecepcion "[ 0,702,05,151099,150542,12949,4          , 1, 4]0E" & vbCrLf
    Case 4: objAutoAn.autorecepcion "[ 0,702,01,151099,145902,12987,RG,01,31,RO,#########,SE,0007858490 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,151099,1510,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,79B ]7E" & vbCrLf
    Case 5: objAutoAn.autorecepcion "[ 0,702,03,151099,151314,12987,    10111,01,31,0007858490 ,79B ,###,804212, 6, 1,      341,#########,2,0,12,NA,NR,NA,0,NA,341.01898,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]48" & vbCrLf
    Case 6: objAutoAn.autorecepcion "[ 0,702,05,151099,151314,12987,0007858490 ,01,31]39" & vbCrLf
    Case 7: objAutoAn.autorecepcion "[ 0,702,01,151099,145903,12986,RG,01,32,RO,#########,SE,0007857829 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,151099,1510,                    ,  0,5,      ,M,                         ,#######,####,####,######,  1,79B ]78" & vbCrLf
    Case 8: objAutoAn.autorecepcion "[ 0,702,03,151099,151322,12986,    10110,01,32,0007857829 ,79B ,###,804212, 7, 1,      321,#########,2,0,12,NA,NR,NA,0,NA,320.60638,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################]4C" & vbCrLf
    Case 9: objAutoAn.autorecepcion "[ 0,702,05,151099,151322,12986,0007857829 ,01,32]35" & vbCrLf


    Case 1: objAutoAn.autorecepcion "[ 0,701,06,0007850117 ,           ,           ,           ,           ,           ,           ]11" & vbCrLf
    Case 2: objAutoAn.autorecepcion "[ 0,701,02, 0,50190, 0, 0,0007850117 ]BE" & vbCrLf
    Case 3: objAutoAn.autorecepcion "[ 0,702,01,061099,165750,50190,RG, 5, 1,ST,#########,SE,0007850117 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,061099,1653,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C0" & vbCrLf
    Case 4: objAutoAn.autorecepcion "[ 0,702,03,061099,165910,50190,     1729, 5, 1,0007850117 ,03C ,###,######,##, 1,#########,#########,2,1, 0,NA,LO,NA,0,NA,#########,         ,DL,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]CB" & vbCrLf
    Case 5: objAutoAn.autorecepcion "[ 0,702,05,061099,165910,50190,0007850117 , 5, 1]69" & vbCrLf
    Case 6: objAutoAn.autorecepcion "[ 0,703,17,061099,165934]D1" & vbCrLf
    Case 7: objAutoAn.autorecepcion "[ 0,701,04, 0, 4, 0, 4, 4, 4, 4, 4, 4]2C" & vbCrLf
    Case 8: objAutoAn.autorecepcion "[ 0,701,06,0007850117 ,           ,           ,           ,           ,           ,           ]11" & vbCrLf
    Case 9: objAutoAn.autorecepcion "[ 0,701,02, 0,50191, 0, 0,0007850117 ]BD" & vbCrLf
    Case 10: objAutoAn.autorecepcion "[ 0,702,01,061099,170031,50191,RG, 5, 1,ST,#########,SE,0007850117 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,061099,1656,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C8" & vbCrLf
    Case 11: objAutoAn.autorecepcion "[ 0,702,03,061099,170152,50191,     1882, 5, 1,0007850117 ,03C ,###,######,##, 1,#########,#########,2,1, 0,NA,LO,NA,0,NA,#########,         ,DL,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]D0" & vbCrLf
    Case 12: objAutoAn.autorecepcion "[ 0,702,05,061099,170152,50191,0007850117 , 5, 1]6E" & vbCrLf
    Case 13: objAutoAn.autorecepcion "[ 0,703,17,061099,170215]DD" & vbCrLf
    Case 14: objAutoAn.autorecepcion "[ 0,701,06,0007850117 ,           ,           ,           ,           ,           ,           ]11" & vbCrLf
    Case 15: objAutoAn.autorecepcion "[ 0,701,02, 0,50192, 0, 0,0007850117 ]BC" & vbCrLf
    Case 16: objAutoAn.autorecepcion "[ 0,702,01,061099,170548,50192,RG, 5, 1,ST,#########,SE,0007850117 ,                    ,                         ,                         ,                  ,               , ,            ,                  ,061099,1701,                    ,   ,5,      ,M,                         ,#######,####,####,######,  1,03C ]C3" & vbCrLf
    Case 17: objAutoAn.autorecepcion "[ 0,702,03,061099,170708,50192,     6661, 5, 1,0007850117 ,03C ,###,######,##, 1,#########,#########,2,1, 0,NA,LO,NA,0,NA,#########,         ,DL,UL,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,LO,####################]C8" & vbCrLf
    Case 18: objAutoAn.autorecepcion "[ 0,702,05,061099,170708,50192,0007850117 , 5, 1]66" & vbCrLf
    Case 19: objAutoAn.autorecepcion "[ 0,703,17,061099,170733]D8" & vbCrLf
  End Select
  'nVez = nVez + 1
End Sub

Private Sub CX3_Master(Mensaje As String, Caracter As String)
Dim contador As Integer
Dim objMuestra As clsMuestra
On Error GoTo AutoAnError
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
'Si el mensaje es rechazado habr� que volver a enviarlo.
'On Error Resume Next
  'objAutoAn.EscribirLog "Synchon Master"
  SubRutina = "CX3_Master"
  Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    Select Case Caracter
      Case Chr$(3) '<ETX> - El autoanalizador ya ha aceptado el bloque y da por finalizada la transmision
        .Enviar Chr$(4) '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        .ButtonProgramEnabled True
        Mensaje = ""
        'cuando recibimos la aceptacion y dejamos de transmitir esperamos la respuesta a estos mensajes
        RESPUESTAEsperada = True
        objAutoAn.autorecepcion Chr$(4) & Chr$(1)
      Case Chr$(6) '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
        'MsgBox "Recibido un ACK en Master", vbExclamation, frmprincipal.Caption
        'siempre responde ETX?
        .Estado = cteCONFIRMACION
        .ButtonProgramEnabled False
        Envio_Mensajes 'Se enviar� un <ENQ> para que el autoanalizador vuelva a mandar la respuesta
        Mensaje = ""
      Case Chr$(21) '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviar
        If TipoMensEnviado = ctePROGRAMACION Then
        'Se actualiza la matriz que controla el grid
          For Each objMuestra In .ColMuestras 'ColProgramMuestras
            With objMuestra
              If .strEstado = cteINTENTOPROGRAMAR Then
                .strEstado = cteSINPROGRAMAR
                UltimoEnvio = False
                Exit For
              End If
            End With
          Next
        End If
        Mensaje = ""
        Timer2.Enabled = True 'Se espera 1 segundo para retransmitir el ultimo bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Master", "plistaTrabajo")
End Sub

Private Sub CX3_Solicitud(Mensaje As String, Caracter As String)
'Tras haber realizado el sistema una solicitud para establecer una comunicaci�n, el autoanalizador responde aceptando o
'negando dicha comunicaci�n.
'On Error Resume Next
On Error GoTo AutoAnError
    'objAutoAn.EscribirLog "CX3 Solicitud"
    SubRutina = "CX3 Solicitud"
    Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
    With objAutoAn
      Select Case Caracter
        Case Chr$(6)  '<ACK> - El autoanalizador ha aceptado la comunicaci�n
          .Estado = cteMASTER
          .ButtonProgramEnabled False
          Mensaje = ""
          Envio_Mensajes 'Se env�a el bloque de datos por parte del sistema al autoanalizador '!!MODIFICAR
          Exit Sub
        Case Chr$(21), Chr$(3) '<NAK> o <ETX> - El autoanalizador no ha aceptado la comunicaci�n
          .Estado = cteESPERA
          .ButtonProgramEnabled True
          Mensaje = ""
          Timer2.Enabled = True 'Se espera 1 segundo para intentar una nueva comunicaci�n
          Exit Sub
      End Select
      If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then '<EOT><SOH> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
        If TipoMensEnviado = ctePROGRAMACION Then PROGRAMPendiente = True
        .Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
        .Estado = cteESCLAVO
        .ButtonProgramEnabled False
        Mensaje = ""
        Timer3.Enabled = True
      End If
    End With
    Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Solicitud", "plistaTrabajo")
End Sub

Private Sub Envio_Mensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar los caracteres <EOT><SOH> y esperar
'a que el autoanalizador responda afirmativamente. Luego se manda el mensaje propiamente dicho hasta que sea recibido
'correctamente por el autoanalizador. Finalmente se cierra la comunicaci�n enviando <EOT> para dar paso a la respuesta del
'autoanalizador al requerimiento del sistema.
Static ContIntentosComunic As Integer 'cuenta el n� de veces que se intenta comunicar con el autoanalizador
Static ContIntentosTrans As Integer 'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
Dim T As Long
Dim RES As Integer
Dim sql As String
Dim contador As Integer
On Error GoTo AutoAnError
  'On Error Resume Next
  SubRutina = "Envio Mensajes"
  objAutoAn.EscribirLog "Envio Mensajes"
  With objAutoAn
    Select Case .Estado
      Case cteESPERA
        ContIntentosTrans = 0 'Se inicializa el ContIntentosTrans por si se realiza una nueva comunicaci�n
        ContIntentosComunic = ContIntentosComunic + 1
        If ContIntentosComunic = 8 Then 'Fallo en la comunicaci�n despu�s de 7 intentos
          ContIntentosComunic = 1
          sql = "No se puede establecer comunicaci�n."
          Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
          Case vbRetry
            T = Timer + 5
            Do While T > Timer: RES = DoEvents(): Loop
            Sistema_Espera
            .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
            Exit Sub
          Case vbCancel
            'Form_Unload (False)
          End Select
        Else
          'solo cuando quede alguna prueba por programar, repetir o borrar
          'If PROGRAMPendiente = True Or borradopendiente = True Or repetirpruebas = True Then
          If Not UltimoEnvio Or RepetirPruebas Or BORRADOPendiente Then
            Sistema_Espera
            .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
            Exit Sub
          End If
          'End If
        End If
      Case cteMASTER
        ContIntentosComunic = 0 'Se inicializa el ContIntentosComunic por si se realiza una nueva comunicaci�n
        ContIntentosTrans = ContIntentosTrans + 1
        If ContIntentosTrans = 8 Then 'Fallo en la transmisi�n despu�s de 7 intentos
          ContIntentosTrans = 0
          objAutoAn.Estado = cteESPERA

          sql = "No se puede transmitir informaci�n."
          Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
          Case vbRetry
            T = Timer + 5
            Do While T > Timer: RES = DoEvents(): Loop
            Sistema_Espera
            ContIntentosComunic = 1
            .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
            Exit Sub
          Case vbCancel
            'Form_Unload (False)
          End Select
        Else
          Sistema_Master
          .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosTrans
          Exit Sub
        End If
    
      Case cteCONFIRMACION
        ContIntentosTrans = ContIntentosTrans + 1
        If ContIntentosTrans = 8 Then 'Fallo en la transmisi�n despu�s de 7 intentos
          ContIntentosTrans = 0
          .Estado = cteESPERA
          .ButtonProgramEnabled True
          sql = "No se puede transmitir informaci�n."
          Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
            Case vbRetry
            Dim objMuestra As clsMuestra
            For Each objMuestra In objAutoAn.ColMuestras
              If objMuestra.strEstado = cteINTENTOPROGRAMAR Then _
              objMuestra.strEstado = cteSINPROGRAMAR: Exit For
            Next
'              For contador = 1 To UBound(MatrizGrid, 2)
'                If MatrizGrid(2, contador) = "IP" Then
'                  MatrizGrid(2, contador) = "SP"
'                  Exit For
'                End If
'              Next contador
              'Se esperan 20 seg. para volver a iniciar la comunicacion
              T = Timer + 20
              Do While T > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              ContIntentosComunic = 1
              .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
              Exit Sub
            Case vbCancel
              'Form_Unload (False)
          End Select
        Else
          Sistema_Confirmacion
          .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosTrans
          Exit Sub
        End If
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Envio_Mensajes", "plistaTrabajo")
End Sub

Private Sub Establecer_Check(Mensaje As String)
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
Dim suma As Integer         'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal
On Error GoTo AutoAnError
  SubRutina = "Establecer_Check"
  suma = 0
  For contador = 1 To Len(Mensaje)
    suma = suma + Asc(Mid$(Mensaje, contador, 1))
    'If suma >= 256 Then suma = suma - 256
  Next contador
  suma = 256 - suma Mod 256
  Select Case Len(Hex$(suma))
    Case 1: Mensaje = Mensaje & "0" & Hex$(suma)
    Case Is > 2: Mensaje = Mensaje & Right$(Hex$(suma), 2)
    Case Else: Mensaje = Mensaje & Hex$(suma)
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Establecer_Check", "plistaTrabajo")
End Sub

Private Sub Form_Load()
On Error GoTo AutoAnError

  'Se inicializan las variables
  RESPUESTAEsperada = False
  RepetirPruebas = False
  RepetirPendiente = False
  PROGRAMPendiente = False
  Salida = False
  mnuItemEstadoMuestra(1).Tag = cteEXTRAIDA 'extraida
  mnuItemEstadoMuestra(2).Tag = cteSINPROGRAMAR 'sin prog
  mnuItemEstadoMuestra(3).Tag = cteACEPTADA 'aceptada
  mnuItemEstadoMuestra(4).Tag = ctePREBORRADA 'borra
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Cargando Lista de Trabajo", "plistaTrabajo")
End Sub

Public Sub Lectura_Datos_CX3bak(Datos As String)
'Los datos que llegan pueden ser esperados
'(respuesta a un mensaje enviado anteriormente) o
'expont�neos (resultados enviados por el autoanalizador.
Dim cresultadoauto As String    'C�digo del resultado definido en la tabla resultadosAutoanalizador
Dim tipoEspecie As String
Dim RES As String
Dim i As Integer, j As Integer, n As Integer, x%
Dim filenum
Dim resul As Double
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim objResultado As clsResultado
Dim code As Integer
Dim num3%, num2%
Dim sql As String
Dim resp As Integer
Dim CodeError As Integer
Dim CodeMuestra As String
Static num0%
On Error GoTo AutoAnError
'On Error Resume Next
  'datos = "[ 0,702,03,250699,183203,28477,     2091, 1, 1,0007753891 ,03C ,###,######,##, 1,      2.1,#########,2,0, 0,NA,NR,NA,0,NA,2.1242421,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] 10"

  SubRutina = "Lectura_Datos_CX3"
  objAutoAn.EscribirLog "Lectura Datos"
  objAutoAn.EscribirLog "Datos: " & Datos
  If RESPUESTAEsperada Then
  'El mensaje es esperado.
    Select Case FuncionRespEsperada
      Case ctePROGRAMACION: Stream701_Funcion02 Datos 'Respuesta a un intento de programar pruebas para una muestra.
      Case cteBORRADO:  Stream701_Funcion04 Datos 'Respuesta a un intento de borrado de las pruebas programadas.
    End Select
  Else
  
  'El mensaje es de resultados.
  'Identifica de que tipo de record se trata y lee sus datos seg�n lo expresado en el manual.
    Select Case Mid$(Datos, 5, 3) & Mid$(Datos, 9, 2)
      Case "70102"
        'Se lee la respuesta al intento de programaci�n de las pruebas de una muestra.
        CodeError = Val(Mid$(Datos, 12, 2))
        CodeMuestra = Trim$(Mid$(Datos, 27, 11))
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteINTENTOPROGRAMAR And CodeMuestra = .strIDMuestra Then
              Select Case CodeError
                Case 0: .strEstado = cteACEPTADA
                Case Else
                  sql = "La muestra " & .strCodMuestra & " no puede ser programada." & Chr$(10)
                  Select Case CodeError
                    Case 1: sql = sql & "Mensaje de programaci�n de pruebas erroneo. Error de sintaxis"
                    Case 2: sql = sql & "La pantalla de programaci�n de pruebas est� activa"
                    Case 3: sql = sql & "C�digo de prueba no identificado en el CX3"
                    Case 4: sql = sql & "Petici�n ORDAC invalida"
                    Case 5: sql = sql & "Combinaci�n de pruebas no valido (Se requiere una diluci�n o volumen de muestra excesivo)."
                    Case 6: sql = sql & "Control no configurado"
                    Case 7: sql = sql & "Solo sector de calibrador"
                    Case 8: sql = sql & "Error de modo (los modos Codigo de barras y Sector no son los mismos para el CX3 y Ordenador"
                    Case 9: sql = sql & "Error Interno en el CX3"
                    Case 10: sql = sql & "Todos los sectores ya estan programados .No se pueden a�adir mas muestras, solo modificarlas"
                    Case 11: sql = sql & "Tipos de fluido incompatibles"
                    Case 12: sql = sql & "Tipos de pruebas incompatibles"
                    Case 13: sql = sql & "Nombre de paciente incorrecto"
                    Case 14: sql = sql & "El C�digo de Muestra Ya Existe en el Autoanalizador"
                    Case 15: sql = sql & "El N� deRack debe estar entre 0000 y 9999"
                  End Select
                  resp = MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
                  Select Case resp
                    Case vbAbort: Descargar 'Form_Unload (False)
                    Case vbRetry: .strEstado = cteSINPROGRAMAR: UltimoEnvio = False
                    Case vbIgnore: .strEstado = cteNOACEPTADA
                  End Select
              End Select
              Exit For
            End If
          End With
        Next
      Case "70104"
      'Se lee la respuesta al intento de borrado de las pruebas programadas.
        code = Val(Mid$(Datos, 15, 2))
        If code <> 0 And code <> 4 Then
          sql = "No se pueden eliminar las pruebas programadas." & Chr$(10)
          Select Case code
            Case 1: sql = sql & "Mensaje de borrado de pruebas err�neo."
            Case 3: sql = sql & "Ha ocurrido un error en el CX3"
            Case 2: sql = sql & "El autoanalizador est� realizando an�lisis en este momento."
            Case 5: sql = sql & "El N� de Rack es Demasiado Largo"
          End Select
          resp = MsgBox(sql, vbExclamation + vbRetryCancel, "Borrado de Muestras en " & frmPrincipal.Caption)
          Select Case resp
            Case vbCancel: Exit Sub
            Case vbRetry:
              TipoMensEnviado = cteBORRADO
              For Each objMuestra In objAutoAn.ColMuestras
                If objMuestra.strEstado = cteINTENTOBORRADO Then objMuestra.strEstado = cteAREPETIR
              Next
              Envio_Mensajes
          End Select
        Else
          For i = 18 To 36 Step 3
            For Each objMuestra In objAutoAn.ColMuestras
              With objMuestra
                If .strEstado = cteINTENTOBORRADO Then
                  Select Case Mid$(Datos, i + 1, 1)
                    Case "0", "4"
                      num0 = num0 + 1
                      .strEstado = cteBORRADA
                      For Each objActuacion In .ColMuestrasPruebas
                        For Each objResultado In objActuacion.ColPruebasResultados
                          objResultado.EscribirEnColumna cteResultadoVALRESULTADO, ""
                        Next
                      Next
                    Case "2"
                      num2 = num2 + 1
                      .strEstado = cteNOBORRADA
                    Case "3"
                      num3 = num3 + 1
                      .strEstado = cteNOBORRADA
                  End Select
                End If
              End With
            Next
          Next i
          If num3 > 0 Then sql = "Ha Ocurrido Un Error en el CX3 ."
          If num2 > 0 Then sql = sql & Chr$(13) & "El Autoanalizador Est� Realizando An�lisis En Este Momento."
          If Not BORRADOPendiente Then
            'en caso de ir todo bien me pondra al final de todo el proceso de borrado el n�muestras borradas
            If num0 > 0 Then sql = sql & Chr$(13) & "Se Van a Repetir " & num0 & " Muestras." & Chr$(13) & Chr$(10) & " 1� -Pulse Aceptar y " & Chr$(13) & Chr$(10) & "2� -Pulse Iniciar Para Comenzar las Repeticiones"
            num0 = 0  'reinicializamos el n� muestras totales borradas
            NotPrim = 0
            If sql <> "" Then
              resp = MsgBox(sql, vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
            Else
              resp = MsgBox("Por Favor Borre Manualmente en el " & frmPrincipal.Caption & " las Muestras Con Repeticiones y Pulse Iniciar", vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
            End If
          End If
        End If
      Case "70106"
        'el CX3 pide la programacion de las muestras leidas
        'por el CB y estas se van programando
        PROGRAMPendiente = True
        For i = 12 To 84 Step 12
          If Mid$(Datos, i, 11) <> Space$(11) Then
            For Each objMuestra In objAutoAn.ColMuestras
              With objMuestra
                If .strIDMuestra = Trim$(Mid$(Datos, i, 10)) And (.strEstado = cteSINPROGRAMAR Or .strEstado = cteBORRADA) Then
                  .strEstado = cteLEIDA
                  Exit For
                End If
              End With
            Next
          End If
        Next i
        If Not objAutoAn.blnExistMuestraConEstado(cteLEIDA) Then
          UltimoEnvio = True
          PROGRAMPendiente = False
        End If
      Case "70201"
        'Se inicializa la matriz que contendr� los resultados. Esta matriz almacenar� en (1,1) la ID de la muestra, en
        '(2,1) la fecha y la hora separadas por una coma, en (1,X) el cResultadoAuto de la prueba y en (2,X) el resultado.
'            ReDim MatrizResultados(1 To 2, 1 To 1)
        Dim IdMuestra As String
        'Se obtiene la ID de la muestra y la fecha y hora
        'MatrizResultados(1, 1) = Trim$(Mid$(datos, 25, 9))
'            MatrizResultados(1, 1) = Trim$(Mid$(datos, 57, 11)) 'codigo num�rico de la muestra
        IdMuestra = Trim$(Mid$(Datos, 57, 11)) 'codigo num�rico de la muestra
        With objAutoAn
          If IsNumeric(IdMuestra) Then
            IdMuestra = .fNumeroACodigo(IdMuestra)
            If .blnExistMuestra(IdMuestra) Then
              .ColMuestras(IdMuestra).strEstado = cteRECIBIENDORESULTADOS
            End If
          End If
        End With
      Case "70203"
        'Se Introducen Resultados
        RES = CStr(Trim$(Mid$(Datos, 82, 9)))
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strIDMuestra = Trim$(Mid$(Datos, 48, 11)) Then
              Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(Datos, 60, 4)), RES, True)
              Exit For
            End If
          End With
        Next
      Case "70211"
        'Se almacenan los resultados especiales en la matriz
'            ReDim Preserve MatrizResultados(1 To 2, 1 To UBound(MatrizResultados, 2) + 1)
        'cResultadoAuto = Trim$(Mid$(datos, 22, 2))
'            cresultadoauto = Trim$(Mid$(datos, 53, 13))
'            txtrecibir = txtrecibir & "result.especial:" & cresultadoauto & Chr$(13) & Chr$(10)

        'Select Case cresultadoauto 'NOTA: PONER COMO CONSTANTES
        'Case 0, 1
        '    cresultadoauto = constANION_GAP
        'Case 2, 3
        '    cresultadoauto = constUREA_CREATININA
        'Case 4
        '    cresultadoauto = constOSMOLARIDAD
        'End Select
'            MatrizResultados(1, UBound(MatrizResultados, 2)) = cresultadoauto
        RES = CStr(Trim$(Mid$(Datos, 77, 9)))
        'i = objAutoAn.fFormatearNumero(RES, ".", ",")
        'If Left$(RES, 1) = "," Then RES = "0" & RES
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strIDMuestra = Trim$(Mid$(Datos, 38, 11)) Then
            'If .strestado = cteRECIBIENDORESULTADOS Then
              Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(Datos, 53, 20)), RES)
              Exit For
            End If
          End With
        Next
      Case "70205"
        'se recibe el indicativo de fin de transmision de todas las pruebas de esa muestra
        'Se procede a actualizar la base de datos
'        With objAutoAn
'          IdMuestra = .fNumeroACodigo(Trim$(Mid$(datos, 32, 11)))
'          If .blnExistMuestra(IdMuestra) Then
'            .ColMuestras(IdMuestra).strestado = cterealizacioncompleta
'          End If
'        End With
      Case "70317"
        'Se recibe la notificaci�n de que ya se han recibido todos los resultados
        'lblRepeticion.Visible = False 'una vez recibidas todas las repeticiones se quita el aviso
        objAutoAn.LabelVisible False
        'desde aqui es desde donde se activa la variable que mandar� repetir las pruebas al recibir EOT (CX_esclavo)
        'RepetirPruebas = RepetirPendiente
        'If Not RepetirPruebas Then Salir
        If objAutoAn.blnRepetirPendiente Then
          'objAutoAn.blnRepetirPendiente = False
          BORRADOPendiente = True
          'MsgBox "Se Van a Procesar las Muestras Pendientes de Repeticion" & vbCrLf & " 1� -Pulse Aceptar En Este Mensaje y " & vbCrLf & "2� -Pulse Iniciar En El Autoanalizador CX3"
          For Each objMuestra In objAutoAn.ColMuestras
            With objMuestra
              objAutoAn.EscribirLog .strCodMuestra & ".Estado = " & .strEstado
              If (.strEstado = cteAREPETIR Or _
              .strEstado = cteAREPETIRFIN) And _
              .strEspecimen <> "Orina" Then
                For Each objActuacion In .ColMuestrasPruebas
                  With objActuacion
                    objAutoAn.EscribirLog "CodAct:" & .strCodAct & ".Estado = " & .strEstado
                    If (.strEstado = cteAREPETIRFIN Or _
                    .strEstado = cteAREPETIR) And _
                    .intRepeticion = 1 Then
                      .intRepeticion = .intRepeticion + 1
                    Else
                      'objMuestra.ColMuestrasPruebas.Remove .strCodAct
                      objActuacion.QuitarPrueba
                    End If
                  End With
                Next
              Else
                If .strEstado = cteREALIZACIONCOMPLETA Or _
                .strEstado = cteREALIZADA Or _
                .strEstado = cteFUERADERANGO Or _
                .strEstado = cteAREPETIRFIN Or _
                .strEstado = cteFUERADERANGOFIN Then
                  'objAutoAn.ColMuestras.Remove .strCodMuestra
                  objMuestra.QuitarMuestra
                End If
              End If
            End With
          Next
          objAutoAn.pRefreshGrids
        Else
          With objAutoAn
            If .blnExistMuestraConEstado(cteREALIZACIONCOMPLETA) Then
              .pSalir
            Else
              If .blnExistMuestraConEstado(cteAREPETIRFIN) Then
                .pSalir
              Else
                If .blnExistMuestraConEstado(cteFUERADERANGOFIN) Then
                  .pSalir
                Else
                  If .blnExistMuestraConEstado(cteREALIZADA) Then
                    .pSalir
                  End If
                End If
              End If
            End If
          End With
        End If
    End Select
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos_CX3", "Lectura_Datos_CX3")
End Sub

Public Sub Lectura_Datos_CX3(Datos As String)
'Los datos que llegan pueden ser esperados
'(respuesta a un mensaje enviado anteriormente) o
'expont�neos (resultados enviados por el autoanalizador.
Dim cresultadoauto As String    'C�digo del resultado definido en la tabla resultadosAutoanalizador
Dim tipoEspecie As String
Dim RES As String
Dim i As Integer, j As Integer, n As Integer, x%
Dim filenum
Dim resul As Double
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim objResultado As clsResultado
Dim code As Integer
Dim num3%, num2%
Dim sql As String
Dim resp As Integer
Dim CodeError As Integer
Dim CodeMuestra As String
Static num0%
On Error GoTo AutoAnError
'On Error Resume Next
  'datos = "[ 0,702,03,250699,183203,28477,     2091, 1, 1,0007753891 ,03C ,###,######,##, 1,      2.1,#########,2,0, 0,NA,NR,NA,0,NA,2.1242421,         ,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,NO,1.0000,NR,####################] 10"

  SubRutina = "Lectura_Datos_CX3"
  objAutoAn.EscribirLog "Lectura Datos"
  objAutoAn.EscribirLog "Datos: " & Datos
  If RESPUESTAEsperada Then
  'El mensaje es esperado.
    Select Case FuncionRespEsperada
      Case ctePROGRAMACION: Stream701_Funcion02 Datos 'Respuesta a un intento de programar pruebas para una muestra.
      Case cteBORRADO:  Stream701_Funcion04 Datos 'Respuesta a un intento de borrado de las pruebas programadas.
    End Select
  'Else
  End If
  'El mensaje es de resultados.
  'Identifica de que tipo de record se trata y lee sus datos seg�n lo expresado en el manual.
    Select Case Mid$(Datos, 5, 3) & Mid$(Datos, 9, 2)
      Case "70102"
        'Se lee la respuesta al intento de programaci�n de las pruebas de una muestra.
        CodeError = Val(Mid$(Datos, 12, 2))
        CodeMuestra = Trim$(Mid$(Datos, 27, 11))
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteINTENTOPROGRAMAR And CodeMuestra = .strIDMuestra Then
              Select Case CodeError
                Case 0: .strEstado = cteACEPTADA
                Case Else
                  sql = "La muestra " & .strCodMuestra & " no puede ser programada." & Chr$(10)
                  Select Case CodeError
                    Case 1: sql = sql & "Mensaje de programaci�n de pruebas erroneo. Error de sintaxis"
                    Case 2: sql = sql & "La pantalla de programaci�n de pruebas est� activa"
                    Case 3: sql = sql & "C�digo de prueba no identificado en el CX3"
                    Case 4: sql = sql & "Petici�n ORDAC invalida"
                    Case 5: sql = sql & "Combinaci�n de pruebas no valido (Se requiere una diluci�n o volumen de muestra excesivo)."
                    Case 6: sql = sql & "Control no configurado"
                    Case 7: sql = sql & "Solo sector de calibrador"
                    Case 8: sql = sql & "Error de modo (los modos Codigo de barras y Sector no son los mismos para el CX3 y Ordenador"
                    Case 9: sql = sql & "Error Interno en el CX3"
                    Case 10: sql = sql & "Todos los sectores ya estan programados .No se pueden a�adir mas muestras, solo modificarlas"
                    Case 11: sql = sql & "Tipos de fluido incompatibles"
                    Case 12: sql = sql & "Tipos de pruebas incompatibles"
                    Case 13: sql = sql & "Nombre de paciente incorrecto"
                    Case 14: sql = sql & "El C�digo de Muestra Ya Existe en el Autoanalizador"
                    Case 15: sql = sql & "El N� deRack debe estar entre 0000 y 9999"
                  End Select
                  resp = MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
                  Select Case resp
                    Case vbAbort: Descargar 'Form_Unload (False)
                    Case vbRetry: .strEstado = cteSINPROGRAMAR: UltimoEnvio = False
                    Case vbIgnore: .strEstado = cteNOACEPTADA
                  End Select
              End Select
              Exit For
            End If
          End With
        Next
      Case "70104"
      'Se lee la respuesta al intento de borrado de las pruebas programadas.
        code = Val(Mid$(Datos, 15, 2))
        If code <> 0 And code <> 4 Then
          sql = "No se pueden eliminar las pruebas programadas." & Chr$(10)
          Select Case code
            Case 1: sql = sql & "Mensaje de borrado de pruebas err�neo."
            Case 3: sql = sql & "Ha ocurrido un error en el CX3"
            Case 2: sql = sql & "El autoanalizador est� realizando an�lisis en este momento."
            Case 5: sql = sql & "El N� de Rack es Demasiado Largo"
          End Select
          resp = MsgBox(sql, vbExclamation + vbRetryCancel, "Borrado de Muestras en CX3")
          Select Case resp
            Case vbCancel: Exit Sub
            Case vbRetry:
              TipoMensEnviado = cteBORRADO
              For Each objMuestra In objAutoAn.ColMuestras
                If objMuestra.strEstado = cteINTENTOBORRADO Then objMuestra.strEstado = cteAREPETIR
              Next
              Envio_Mensajes
          End Select
        Else
          For i = 18 To 36 Step 3
            For Each objMuestra In objAutoAn.ColMuestras
              With objMuestra
                If .strEstado = cteINTENTOBORRADO Then
                  Select Case Mid$(Datos, i + 1, 1)
                    Case "0", "4"
                      num0 = num0 + 1
                      .strEstado = cteBORRADA
                      For Each objActuacion In .ColMuestrasPruebas
                        For Each objResultado In objActuacion.ColPruebasResultados
                          objResultado.EscribirEnColumna cteResultadoVALRESULTADO, ""
                        Next
                      Next
                    Case "2"
                      num2 = num2 + 1
                      .strEstado = cteNOBORRADA
                    Case "3"
                      num3 = num3 + 1
                      .strEstado = cteNOBORRADA
                  End Select
                End If
              End With
            Next
          Next i
          If num3 > 0 Then sql = "Ha Ocurrido Un Error en el CX3 ."
          If num2 > 0 Then sql = sql & Chr$(13) & "El Autoanalizador Est� Realizando An�lisis En Este Momento."
          If Not BORRADOPendiente Then
            'en caso de ir todo bien me pondra al final de todo el proceso de borrado el n�muestras borradas
            If num0 > 0 Then sql = sql & Chr$(13) & "Se Van a Repetir " & num0 & " Muestras." & Chr$(13) & Chr$(10) & " 1� -Pulse Aceptar y " & Chr$(13) & Chr$(10) & "2� -Pulse Iniciar Para Comenzar las Repeticiones"
            num0 = 0  'reinicializamos el n� muestras totales borradas
            NotPrim = 0
            If sql <> "" Then
              resp = MsgBox(sql, vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
            Else
              resp = MsgBox("Por Favor Borre Manualmente en el " & frmPrincipal.Caption & " las Muestras Con Repeticiones y Pulse Iniciar", vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
            End If
          End If
        End If
      Case "70106"
        'el CX3 pide la programacion de las muestras leidas
        'por el CB y estas se van programando
        PROGRAMPendiente = True
        For i = 12 To 84 Step 12
          If Mid$(Datos, i, 11) <> Space$(11) Then
            For Each objMuestra In objAutoAn.ColMuestras
              With objMuestra
                If .strIDMuestra = Trim$(Mid$(Datos, i, 10)) And (.strEstado = cteSINPROGRAMAR Or .strEstado = cteBORRADA) Then
                  .strEstado = cteLEIDA
                  Exit For
                End If
              End With
            Next
          End If
        Next i
        If Not objAutoAn.blnExistMuestraConEstado(cteLEIDA) Then
          UltimoEnvio = True
          PROGRAMPendiente = False
        End If
      Case "70201"
        'Se inicializa la matriz que contendr� los resultados. Esta matriz almacenar� en (1,1) la ID de la muestra, en
        '(2,1) la fecha y la hora separadas por una coma, en (1,X) el cResultadoAuto de la prueba y en (2,X) el resultado.
'            ReDim MatrizResultados(1 To 2, 1 To 1)
        Dim IdMuestra As String
        'Se obtiene la ID de la muestra y la fecha y hora
        'MatrizResultados(1, 1) = Trim$(Mid$(datos, 25, 9))
'            MatrizResultados(1, 1) = Trim$(Mid$(datos, 57, 11)) 'codigo num�rico de la muestra
        IdMuestra = Trim$(Mid$(Datos, 57, 11)) 'codigo num�rico de la muestra
        With objAutoAn
          If IsNumeric(IdMuestra) Then
            IdMuestra = .fNumeroACodigo(IdMuestra)
            If .blnExistMuestra(IdMuestra) Then
              .ColMuestras(IdMuestra).strEstado = cteRECIBIENDORESULTADOS
            End If
          End If
        End With
      Case "70203"
        'Se Introducen Resultados
        RES = CStr(Trim$(Mid$(Datos, 82, 9)))
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strIDMuestra = Trim$(Mid$(Datos, 48, 11)) Then
              Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(Datos, 60, 4)), RES, True)
              Exit For
            End If
          End With
        Next
      Case "70211"
        'Se almacenan los resultados especiales en la matriz
'            ReDim Preserve MatrizResultados(1 To 2, 1 To UBound(MatrizResultados, 2) + 1)
        'cResultadoAuto = Trim$(Mid$(datos, 22, 2))
'            cresultadoauto = Trim$(Mid$(datos, 53, 13))
'            txtrecibir = txtrecibir & "result.especial:" & cresultadoauto & Chr$(13) & Chr$(10)

        'Select Case cresultadoauto 'NOTA: PONER COMO CONSTANTES
        'Case 0, 1
        '    cresultadoauto = constANION_GAP
        'Case 2, 3
        '    cresultadoauto = constUREA_CREATININA
        'Case 4
        '    cresultadoauto = constOSMOLARIDAD
        'End Select
'            MatrizResultados(1, UBound(MatrizResultados, 2)) = cresultadoauto
        RES = CStr(Trim$(Mid$(Datos, 77, 9)))
        'i = objAutoAn.fFormatearNumero(RES, ".", ",")
        'If Left$(RES, 1) = "," Then RES = "0" & RES
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strIDMuestra = Trim$(Mid$(Datos, 38, 11)) Then
            'If .strestado = cteRECIBIENDORESULTADOS Then
              Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(Datos, 53, 20)), RES)
              Exit For
            End If
          End With
        Next
      Case "70205"
        'se recibe el indicativo de fin de transmision de todas las pruebas de esa muestra
        'Se procede a actualizar la base de datos
'        With objAutoAn
'          IdMuestra = .fNumeroACodigo(Trim$(Mid$(datos, 32, 11)))
'          If .blnExistMuestra(IdMuestra) Then
'            .ColMuestras(IdMuestra).strestado = cterealizacioncompleta
'          End If
'        End With
      Case "70317"
        'Se recibe la notificaci�n de que ya se han recibido todos los resultados
        'lblRepeticion.Visible = False 'una vez recibidas todas las repeticiones se quita el aviso
        objAutoAn.LabelVisible False
        'desde aqui es desde donde se activa la variable que mandar� repetir las pruebas al recibir EOT (CX_esclavo)
        'RepetirPruebas = RepetirPendiente
        'If Not RepetirPruebas Then Salir
        If objAutoAn.blnRepetirPendiente Then
          'objAutoAn.blnRepetirPendiente = False
          BORRADOPendiente = True
          'MsgBox "Se Van a Procesar las Muestras Pendientes de Repeticion" & vbCrLf & " 1� -Pulse Aceptar En Este Mensaje y " & vbCrLf & "2� -Pulse Iniciar En El Autoanalizador CX3"
          For Each objMuestra In objAutoAn.ColMuestras
            With objMuestra
              objAutoAn.EscribirLog .strCodMuestra & ".Estado = " & .strEstado
              If (.strEstado = cteAREPETIR Or _
              .strEstado = cteAREPETIRFIN) And _
              .strEspecimen <> "Orina" Then
                For Each objActuacion In .ColMuestrasPruebas
                  With objActuacion
                    objAutoAn.EscribirLog "CodAct:" & .strCodAct & ".Estado = " & .strEstado
                    If (.strEstado = cteAREPETIRFIN Or _
                    .strEstado = cteAREPETIR) And _
                    .intRepeticion = 1 Then
                      .intRepeticion = .intRepeticion + 1
                    Else
                      'objMuestra.ColMuestrasPruebas.Remove .strCodAct
                      objActuacion.QuitarPrueba
                    End If
                  End With
                Next
              Else
                If .strEstado = cteREALIZACIONCOMPLETA Or _
                .strEstado = cteREALIZADA Or _
                .strEstado = cteFUERADERANGO Or _
                .strEstado = cteAREPETIRFIN Or _
                .strEstado = cteFUERADERANGOFIN Then
                  'objAutoAn.ColMuestras.Remove .strCodMuestra
                  objMuestra.QuitarMuestra
                End If
              End If
            End With
          Next
          objAutoAn.pRefreshGrids
        Else
          With objAutoAn
            If .blnExistMuestraConEstado(cteREALIZACIONCOMPLETA) Then
              .pSalir
            Else
              If .blnExistMuestraConEstado(cteAREPETIRFIN) Then
                .pSalir
              Else
                If .blnExistMuestraConEstado(cteFUERADERANGOFIN) Then
                  .pSalir
                Else
                  If .blnExistMuestraConEstado(cteREALIZADA) Then
                    .pSalir
                  End If
                End If
              End If
            End If
          End With
        End If
    End Select
'  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos_CX3", "Lectura_Datos_CX3")
End Sub

'Private Sub pIntroResultConCResAuto(CodMuestra As String, codResAuto As String, Modificacion As String)
'Dim objMuestra As clsMuestra
'Dim objActuacion As clsActuacion
'Dim objResultado As clsResultado
'Dim objColumna As clsCabeceraGrid
'Dim objResultadoAuto As clsResultAuto
'On Error GoTo AutoAnError
'
'  SubRutina = "pIntroResultConCResAuto"
'  objAutoAn.EscribirLog "pIntroResultConCResAuto"
'  With objAutoAn.ColMuestras(CodMuestra)
'    For Each objActuacion In .ColMuestrasPruebas
'      For Each objResultadoAuto In objAutoAn.ColPruebaAuto(objActuacion.strCodAct).ColResultAuto
'        If objResultadoAuto.strCodResultAuto = codResAuto Then
'          With .ColMuestrasPruebas(objActuacion.strCodAct).ColPruebasResultados(CStr(objResultadoAuto.intCodResult))
'            .strResultado = Modificacion
'            .strEstado = cteREALIZADA
'            If Not objAutoAn.ColMuestras(CodMuestra).blnRepetida And _
'            Not (objAutoAn.ColMuestras(CodMuestra).strEspecimen = "Orina") Then
'              If CDbl(Modificacion) < .DblRefMin Or _
'              CDbl(Modificacion) > .DblRefMax Then
'                With objActuacion
'                  .intRepeticion = .intRepeticion + 1
'                  .strEstado = cteAREPETIR
'                End With
'                objAutoAn.ColMuestras(CodMuestra).strEstado = cteAREPETIR
'                RepetirPendiente = True
'              Else
'                objAutoAn.ColMuestras(CodMuestra).strEstado = cteREALIZADA
'                objActuacion.strEstado = cteREALIZADA
'              End If
'            Else
'              objAutoAn.ColMuestras(CodMuestra).strEstado = cteREALIZADA
'              objActuacion.strEstado = cteREALIZADA
'            End If
'          End With
'          Exit Sub
'        End If
'      Next
'    Next
'  End With
'  Exit Sub
'
'AutoAnError:
'    Call objError.InternalError(Me, "pIntroResultConCResAuto", "CodMuestra" & CodMuestra)
'End Sub

Function blnExistInColProgMuestras(CodMuestra As String) As Boolean
On Error Resume Next

  blnExistInColProgMuestras = objAutoAn.ColMuestras(CodMuestra).strmuestra
  blnExistInColProgMuestras = (Err = 0)
End Function

Public Sub Lectura_Protocolo(Caracter As String)
Static Mensaje As String   'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
'por estar declarada como Static en esta subrutina
On Error GoTo AutoAnError

  Mensaje = Mensaje & Caracter
  CopiaMensaje = Mensaje
  Select Case Protocolo
    Case cteBECKMAN_CX3
      Select Case objAutoAn.Estado
        Case cteESPERA
          CX3_Espera CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteSOLICITUD
          CX3_Solicitud CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteESCLAVO
          CX3_Esclavo CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteCONFIRMACION
          CX3_Confirmacion CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteMASTER
          CX3_Master CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
      End Select
    Case cteSYNCHRON
      Select Case objAutoAn.Estado
        Case cteESPERA
          SYNCHRON_Espera CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteSOLICITUD
          SYNCHRON_Solicitud CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteESCLAVO
          SYNCHRON_Esclavo CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteCONFIRMACION
          SYNCHRON_Confirmacion CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
        Case cteMASTER
          SYNCHRON_Master CopiaMensaje, Caracter
          Mensaje = CopiaMensaje
      End Select
  End Select
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Protocolo", "plistaTrabajo")
End Sub

Private Sub Repetir_Pruebas()
'Actualiza las matrices y se�ala el comienzo de la programaci�n de las repeticiones
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim contador As Integer
Dim nRegistros As Integer
On Error GoTo AutoAnError

  SubRutina = "Repetir_Pruebas"
  objAutoAn.EscribirLog "Repetir_Pruebas"
  For Each objMuestra In objAutoAn.ColMuestras
    If objMuestra.strEstado = cteAREPETIR Then
      For Each objActuacion In objMuestra.ColMuestrasPruebas
        If objActuacion.strEstado <> cteAREPETIR Then
          objMuestra.ColMuestrasPruebas.Remove objActuacion
        End If
      Next
    Else
      objAutoAn.ColMuestras.Remove objMuestra
    End If
  Next
  'Se borran las pruebas programadas la vez anterior para que al darle al start pueda reprogramarlas
  TipoMensEnviado = cteBORRADO
  Envio_Mensajes
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Repetir_Pruebas", "plistaTrabajo")
End Sub

Private Sub Sistema_Confirmacion()
'Se solicita que el autoanalizador repita o env�e la respuesta a un
'mensaje enviado anteriormente
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Sistema_Confirmacion"
  SubRutina = "Sistema_Confirmacion"
  objAutoAn.Enviar Chr$(5) 'Env�o de <ENQ>, se pide la respuesta al mensaje enviado
  Timer1.Enabled = True 'Empieza a contar el tiempo para el TimeOut
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Confirmacion", "plistaTrabajo")
End Sub

Private Sub Sistema_Espera()
'Se env�a los caracteres de solicitud de comunicaci�n y se pasa a estado de Solicitud
On Error GoTo AutoAnError
  SubRutina = "Sistema_Espera"
  With objAutoAn
    .EscribirLog "Sistema_Espera"
    .Enviar Chr$(4) & Chr$(1) 'Env�o de <EOT><SOH>, se "solicita l�nea"
    .Estado = cteSOLICITUD
    .ButtonProgramEnabled False
  End With
  Timer1.Enabled = True 'Empieza a contar el tiempo para el TimeOut
  objAutoAn.autorecepcion Chr$(6)
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Espera", "plistaTrabajo")
End Sub

Private Sub Sistema_Master()
Dim Mensaje As String 'Contiene un string correspondiente al mensaje a enviar
Dim filenum
On Error GoTo AutoAnError
'Se transmiten datos al autoanalizador. Los datos a enviar pueden ser de
'dos tipos: -programaci�n de pruebas o -borrado de pruebas programadas.
  objAutoAn.EscribirLog "Sistema_Master"
  SubRutina = "Sistema_Master"
  Select Case Protocolo
    Case cteBECKMAN_CX3
      Select Case TipoMensEnviado
        Case ctePROGRAMACION: Stream701_Funcion01 Mensaje ' programaci�n de pruebas
        Case cteBORRADO: Stream701_Funcion03 Mensaje 'borrado de todas las pruebas programadas
      End Select
    Case cteSYNCHRON
      Select Case TipoMensEnviado
        Case ctePROGRAMACION: Stream801_Funcion01 Mensaje ' programaci�n de pruebas
        Case cteBORRADO: Stream801_Funcion03 Mensaje 'borrado de todas las pruebas programadas
      End Select
  End Select
  'Se env�a el mensaje
  If Mensaje <> "" Then
    objAutoAn.Enviar Mensaje & Chr$(13) & Chr$(10)
    Timer1.Enabled = True 'Empieza a contar el tiempo para el TimeOut
    objAutoAn.autorecepcion Chr$(3)
  Else
    objAutoAn.Estado = cteESPERA

    blnRecepcion = True
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Master", "plistaTrabajo")
End Sub

Private Sub Stream701_Funcion01(Mensaje As String)
'Se forma un mensaje que responde al formato del stream 301, funci�n 01 (programaci�n de pruebas) y se establecen otras
'caracter�sticas relacionadas con la respuesta esperada y futuros env�os
Dim cPrueba As String       'c�digo de la prueba/c�digo de la prueba seg�n codificaci�n del CX3
Dim pruebas As String       'c�digos de las pruebas acumulados en un mismo string seg�n codificaci�n del CX3
Dim cEspecimen As String    'c�digo de la especie/c�digo de la especie seg�n codificaci�n del CX3
Dim contador1 As Integer
Dim contador2 As Integer
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
'On Error Resume Next
On Error GoTo AutoAnError
    
    'For contador1 = 1 To UBound(MatrizProg, 2)
  SubRutina = "Stream701_Funcion01"
  objAutoAn.EscribirLog "Stream701_Funcion01"
  For Each objMuestra In objAutoAn.ColMuestras 'ColProgramMuestras
    With objMuestra
      If .strEstado = cteLEIDA Then
        'Se comienza a formar el mensaje
        Mensaje = "[00,701,01,"
        'el ultimo 1 es para poder programar solo nuevas pruebas a una muestra que ya exista y si no programa una nueva
        'el caso 0 hace reemplazar todo lo que exista
        Mensaje = Mensaje & "00," & "00," & "1,"
        'Se se�ala si es urgencia o no
        Mensaje = Mensaje & IIf(.blnUrgente, "ST", "RO")
        'Mensaje = Mensaje & .strProperty1 & "," & Trim$(.strCodMuestra) & " "
        '.strEspecimenAuto
        Mensaje = Mensaje & "," & .strEspecimenAuto & "," & Trim$(.strIDMuestra) & " " _
        & "," & Space$(20) & "," & Space$(25) & "," & Space$(25) _
        & "," & Space(18) & "," & Space$(15) & ", ," & Space$(12) _
        & "," & Space$(18) & "," & Format$(Now, "ddmmyy") & "," & Format$(Now, "hhmm") _
        & "," & Space$(20) & "," & Space$(3) & "," & Space$(1) & "," & Space$(6) _
        & "," & Space(1) & "," & Space$(25) & "," & Space$(7) _
        & "," & Space$(4) & "," & Space$(4) & "," & Space$(6)
        'aqui tendre que distinguir cuando realizo la programacion de todas las
        'muestras o de solo las de repeticion porque en el 1er caso me cojo
        'todas y en el 2� solo las seleccionadas (tb puedo ver si hay alguna
        'fila seleccionada y si hay me quedo con las seleccionadas y sino con todas)
        '.nada de esto es cierto pues solo programo las que me pregunta
        'y estan en matrizprog
        'Se buscan y se codifican todas la pruebas que hay que realizar a la muestra. Se sigue completando el mensaje.
        For Each objActuacion In .ColMuestrasPruebas
          With objActuacion
            pruebas = pruebas & MontarPrueba(objAutoAn.ColPruebaAuto(.strCodAct).strCodActAuto)              'End If
          End With
        Next
'        If .blnRepetida Then
'          For Each objActuacion In .ColMuestrasPruebas
'            With objActuacion
'              If .intRepeticion = 1 Then
'                pruebas = pruebas & MontarPrueba(objAutoAn.ColPruebaAuto(.strCodAct).strCodActAuto)
'              End If
'            End With
'          Next
'        Else
'          For Each objActuacion In .ColMuestrasPruebas
'            pruebas = pruebas & MontarPrueba(objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto)
'          Next
'        End If
        'hay que tener en cuenta que pruebas lleva la coma final
        Mensaje = Mensaje & "," & Right$("000", 3 - Len(CStr(Len(pruebas) / 7))) _
         & CInt(Len(pruebas) / 7) & "," & pruebas
        Mensaje = Left$(Mensaje, Len(Mensaje) - 1) & "]"
        'Se establece el check para el mensaje
        Establecer_Check Mensaje
        'Se coloca el estado "IP" para la muestra.
        .strEstado = cteINTENTOPROGRAMAR
        Exit For
      End If
    End With
  Next 'contador1
  'para que no intente mandar mensaje estando ya todo programado(mandaria "" & CRLF) y CX� rsponder�a NAK
  If Not objAutoAn.blnExistMuestraConEstado(cteINTENTOPROGRAMAR) Then
    PROGRAMPendiente = False: UltimoEnvio = True: blnRecepcion = True ': txtrecibir = txtrecibir & "ultimoenvio(stream701)=true" & Chr$(13) & Chr$(10) Else PROGRAMPendiente = True: UltimoEnvio = False: txtrecibir = txtrecibir & "ultimoenvio(stream701)=false" & Chr$(13) & Chr$(10)
  End If
  'Se establecen el tipo de respuesta esperado par parte del CX3 al mensaje que se le va a enviar.
  FuncionRespEsperada = ctePROGRAMACION
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion01", "Mensaje: " & Mensaje)
End Sub

Private Function MontarPrueba(strPruebaAutoan As String) As String
MontarPrueba = strPruebaAutoan & " ,0,"
'If Len(Trim$(strPruebaAutoan)) >= 4 Then
'  MontarPrueba = strPruebaAutoan & " ,0,"
'Else
'  MontarPrueba = Trim$(strPruebaAutoan) & Space$(4 - Len(Trim$(strPruebaAutoan))) & ",0,"
'End If
  'MontarPrueba = IIf(Len(Trim$(strPruebaAutoan)) >= 4, strPruebaAutoan _
  , Trim$(strPruebaAutoan) & Space$(4 - Len(Trim$(strPruebaAutoan)))) & ",0,"
End Function

Private Sub Stream701_Funcion02(Datos As String)
'Se lee la respuesta al intento de programaci�n de las pruebas de una muestra.
Dim sql As String
Dim resp As Integer
Dim CodeError As Integer
Dim CodeMuestra As String
Dim objMuestra As clsMuestra
'On Error Resume Next
On Error GoTo AutoAnError
  SubRutina = "Stream701_funcion2"
  objAutoAn.EscribirLog "Stream701_funcion2"
  If Mid$(Datos, 5, 3) = "701" And Mid$(Datos, 9, 2) = "02" Then
    CodeError = Val(Mid$(Datos, 12, 2))
    CodeMuestra = Trim$(Mid$(Datos, 27, 11))
    For Each objMuestra In objAutoAn.ColMuestras
      With objMuestra
        If .strEstado = cteINTENTOPROGRAMAR And CodeMuestra = .strIDMuestra Then
          Select Case CodeError
            Case 0: .strEstado = cteACEPTADA
            Case Else
              sql = "La muestra " & .strCodMuestra & " no puede ser programada." & Chr$(10)
              Select Case CodeError
                Case 1: sql = sql & "Mensaje de programaci�n de pruebas erroneo. Error de sintaxis"
                Case 2: sql = sql & "La pantalla de programaci�n de pruebas est� activa"
                Case 3: sql = sql & "C�digo de prueba no identificado en el CX3"
                Case 4: sql = sql & "Petici�n ORDAC invalida"
                Case 5: sql = sql & "Combinaci�n de pruebas no valido (Se requiere una diluci�n o volumen de muestra excesivo)."
                Case 6: sql = sql & "Control no configurado"
                Case 7: sql = sql & "Solo sector de calibrador"
                Case 8: sql = sql & "Error de modo (los modos Codigo de barras y Sector no son los mismos para el CX3 y Ordenador"
                Case 9: sql = sql & "Error Interno en el CX3"
                Case 10: sql = sql & "Todos los sectores ya estan programados .No se pueden a�adir mas muestras, solo modificarlas"
                Case 11: sql = sql & "Tipos de fluido incompatibles"
                Case 12: sql = sql & "Tipos de pruebas incompatibles"
                Case 13: sql = sql & "Nombre de paciente incorrecto"
                Case 14: sql = sql & "El C�digo de Muestra Ya Existe en el Autoanalizador"
                Case 15: sql = sql & "El N� deRack debe estar entre 0000 y 9999"
              End Select
              resp = MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
              Select Case resp
                Case vbAbort: Descargar 'Form_Unload (False)
                Case vbRetry: .strEstado = cteSINPROGRAMAR: UltimoEnvio = False
                Case vbIgnore: .strEstado = cteNOACEPTADA
              End Select
          End Select
          Exit For
        End If
      End With
    Next
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion02", "Datos: " & Datos)
End Sub

Private Sub Stream701_Funcion03(Mensaje As String)
Dim i%, ListaMuestrasABorrar$, x%, contador%
Dim objMuestra As clsMuestra
Dim NumMuestrasABorrar As Integer 'lleva el control del n� de
Const ListaVacia = "           ,           ,           ,           ,           ,           ,           ,"
'muestras a borrar pues no podemos eliminar mas de 7 en un solo mensaje
Static ultmuestraborrada As Integer 'lleva la posicion de la ultima
'muestra borrada para poder formar a partir de ella el siguiente mensaje
On Error GoTo AutoAnError
'Se forma un mensaje que responde al formato del stream 701, funci�n 03 y se establecen otras caracter�sticas relacionadas
'con la respuesta esperada y futuros env�os
  SubRutina = "Stream701_Funcion03"
  objAutoAn.EscribirLog "Stream701_Funcion03"
  ListaMuestrasABorrar = ""
  'txtrecibir = txtrecibir & "Subrutina Stream 701 Borrado pruebas" & Chr$(13) & Chr$(10)
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      'If .strEstado = cteFUERADERANGO Or _
      .strEstado = cteFUERADERANGOFIN Or _
      .strEstado = cteAREPETIR Or _
      .strEstado = cteAREPETIRFIN Then
      If .strEstado = cteAREPETIRFIN Or _
      .strEstado = ctePREBORRADA Then
        .strEstado = cteINTENTOBORRADO
        NumMuestrasABorrar = NumMuestrasABorrar + 1
        ListaMuestrasABorrar = ListaMuestrasABorrar & .strIDMuestra & Space$(11 - Len(.strIDMuestra)) & ","
        If NumMuestrasABorrar >= 7 Then Exit For
      End If
    End With
  Next
  ultmuestraborrada = contador
  ListaMuestrasABorrar = ListaMuestrasABorrar & Right$(ListaVacia, 12 * (7 - NumMuestrasABorrar))
  ListaMuestrasABorrar = Left$(ListaMuestrasABorrar, Len(ListaMuestrasABorrar) - 1)
  Mensaje = "[00,701,03,00," & ListaMuestrasABorrar & "]"
  Establecer_Check Mensaje
  FuncionRespEsperada = cteBORRADO
  UltimoEnvio = True
  BORRADOPendiente = objAutoAn.blnExistMuestraConEstado(cteAREPETIRFIN)
  If Not BORRADOPendiente Then BORRADOPendiente = _
  objAutoAn.blnExistMuestraConEstado(ctePREBORRADA)
  objAutoAn.EscribirLog "Borrado pendiente: " & BORRADOPendiente
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion03", "Mensaje: " & Mensaje)
End Sub

Private Sub Stream701_Funcion04(Datos As String)
'Se lee la respuesta al intento de borrado de las pruebas programadas.
Dim sql As String
Dim code As Integer
Dim resp As Integer
Dim num3%, num2%, i%
Static num0%
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim objResultado As clsResultado
'On Error Resume Next
On Error GoTo AutoAnError

  SubRutina = "Stream701_Funcion04"
  objAutoAn.EscribirLog "Stream701_Funcion04"
  If Mid$(Datos, 5, 3) = "701" And Mid$(Datos, 9, 2) = "04" Then
    code = Val(Mid$(Datos, 15, 2))
    If code <> 0 And code <> 4 Then
      sql = "No se pueden eliminar las pruebas programadas." & Chr$(10)
      Select Case code
        Case 1: sql = sql & "Mensaje de borrado de pruebas err�neo."
        Case 3: sql = sql & "Ha ocurrido un error en el CX3"
        Case 2: sql = sql & "El autoanalizador est� realizando an�lisis en este momento."
        Case 5: sql = sql & "El N� de Rack es Demasiado Largo"
      End Select
      resp = MsgBox(sql, vbExclamation + vbRetryCancel, "Borrado de Muestras en " & frmPrincipal.Caption)
      Select Case resp
        Case vbCancel: Exit Sub
        Case vbRetry:
          TipoMensEnviado = cteBORRADO
          For Each objMuestra In objAutoAn.ColMuestras
            If objMuestra.strEstado = cteINTENTOBORRADO Then objMuestra.strEstado = cteAREPETIR
          Next
          Envio_Mensajes
      End Select
    Else
      For i = 18 To 36 Step 3
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteINTENTOBORRADO Then
              Select Case Mid$(Datos, i + 1, 1)
                Case "0", "4"
                  num0 = num0 + 1
                  .strEstado = cteBORRADA
                  For Each objActuacion In .ColMuestrasPruebas
                    For Each objResultado In objActuacion.ColPruebasResultados
                      objResultado.EscribirEnColumna cteResultadoVALRESULTADO, ""
                    Next
                  Next
                Case "2"
                  num2 = num2 + 1
                  .strEstado = cteNOBORRADA
                Case "3"
                  num3 = num3 + 1
                  .strEstado = cteNOBORRADA
              End Select
            End If
          End With
        Next
      Next i
      If num3 > 0 Then sql = "Ha Ocurrido Un Error en el CX3 ."
      If num2 > 0 Then sql = sql & Chr$(13) & "El Autoanalizador Est� Realizando An�lisis En Este Momento."
      If Not BORRADOPendiente Then
        'en caso de ir todo bien me pondra al final de todo el proceso de borrado el n�muestras borradas
        If num0 > 0 Then sql = sql & Chr$(13) & "Se Van a Repetir " & num0 & " Muestras." & Chr$(13) & Chr$(10) & " 1� -Pulse Aceptar y " & Chr$(13) & Chr$(10) & "2� -Pulse Iniciar Para Comenzar las Repeticiones"
        num0 = 0  'reinicializamos el n� muestras totales borradas
'              ReDim MatrizProg(1 To 7, 1 To 1)
        NotPrim = 0
        If sql <> "" Then
          resp = MsgBox(sql, vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
        Else
          resp = MsgBox("Por Favor Borre Manualmente en el " & frmPrincipal.Caption & " las Muestras Con Repeticiones y Pulse Iniciar", vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
        End If
      End If
    End If
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion04", "Datos: " & Datos)
End Sub

Private Sub mnuExtraida_Click()
Stop
End Sub

Private Sub BorradoMuestras()
'Dim i%, ListaMuestrasABorrar$, x%, contador%
'Dim objMuestra As clsMuestra
'Dim NumMuestrasABorrar As Integer 'lleva el control del n� de
'Const ListaVacia = "           ,           ,           ,           ,           ,           ,           ,"
''muestras a borrar pues no podemos eliminar mas de 7 en un solo mensaje
'Static ultmuestraborrada As Integer 'lleva la posicion de la ultima
''muestra borrada para poder formar a partir de ella el siguiente mensaje
'On Error GoTo AutoAnError
''Se forma un mensaje que responde al formato del stream 701, funci�n 03 y se establecen otras caracter�sticas relacionadas
''con la respuesta esperada y futuros env�os
'  SubRutina = "BorradoMuestras"
'  objAutoAn.EscribirLog "BorradoMuestras"
'  ListaMuestrasABorrar = ""
'  'txtrecibir = txtrecibir & "Subrutina Stream 701 Borrado pruebas" & Chr$(13) & Chr$(10)
'  For Each objMuestra In objAutoAn.ColMuestras
'    With objMuestra
'      If .strEstado = ctePREBORRADA Then
'        .strEstado = cteINTENTOBORRADO
'        NumMuestrasABorrar = NumMuestrasABorrar + 1
'        ListaMuestrasABorrar = ListaMuestrasABorrar & .strIDMuestra & Space$(11 - Len(.strIDMuestra)) & ","
'        If NumMuestrasABorrar >= 7 Then Exit For
'      End If
'    End With
'  Next
'  ultmuestraborrada = contador
'  ListaMuestrasABorrar = ListaMuestrasABorrar & Right$(ListaVacia, 12 * (7 - NumMuestrasABorrar))
'  ListaMuestrasABorrar = Left$(ListaMuestrasABorrar, Len(ListaMuestrasABorrar) - 1)
'  Mensaje = "[00,701,03,00," & ListaMuestrasABorrar & "]"
'  Establecer_Check Mensaje
'  FuncionRespEsperada = cteBORRADO
'  UltimoEnvio = True
'  BORRADOPendiente = objAutoAn.blnExistMuestraConEstado(ctePREBORRADA)
'  Exit Sub
'
'AutoAnError:
'    Call objError.InternalError(Me, "Stream701_Funcion03", "Mensaje: " & Mensaje)
End Sub

Private Sub mnuItemEstadoMuestra_Click(Index As Integer)
Dim i%, BkMrk
Dim j As Boolean
Dim strCodMuestra$
Dim strCodActuacion$
Dim objMuestra As clsMuestra
Dim Nuevoestado As constEstado
Dim objActuacion As clsActuacion
Dim ColMuestras As New Collection
Dim EstadoMuestra As constEstado

  Nuevoestado = mnuItemEstadoMuestra(Index).Tag
  With objAutoAn.GridMuestras
    For i = 0 To .SelBookmarks.Count - 1
      BkMrk = .SelBookmarks(i)
      strCodMuestra = .Columns("Muestra").CellValue(BkMrk)
      Err = 0: On Error Resume Next
      j = (ColMuestras(strCodMuestra).strCodMuestra <> "")
      If Err <> 0 Then
        ColMuestras.Add objAutoAn.ColMuestras(strCodMuestra), strCodMuestra
      End If
    Next i
  End With
  For Each objMuestra In ColMuestras
    For Each objActuacion In objMuestra.ColMuestrasPruebas
      With objActuacion
        Select Case .strEstado
          Case cteAREPETIRFIN, cteNOACEPTADA, cteNOBORRADA
            If Nuevoestado = cteAREPETIR Then
              .intRepeticion = .intRepeticion + 1
            Else
              .strEstado = Nuevoestado
            End If
          Case cteSINPROGRAMAR
            If Nuevoestado = cteACEPTADA Or _
            Nuevoestado = cteEXTRAIDA Then
              .strEstado = Nuevoestado
            End If
          Case cteACEPTADA
            If Nuevoestado = cteSINPROGRAMAR Or _
            Nuevoestado = cteEXTRAIDA Then
              .strEstado = Nuevoestado
            End If
          Case cteREALIZACIONCOMPLETA, cteFUERADERANGOFIN
            If Nuevoestado = cteAREPETIR Then
              .intRepeticion = .intRepeticion + 1
            End If
        End Select
      End With
    Next
    With objMuestra
      EstadoMuestra = .strEstado
      If (EstadoMuestra <> cteREALIZACIONCOMPLETA And _
      EstadoMuestra <> cteRECIBIENDORESULTADOS And _
      EstadoMuestra <> cteREALIZADA) Or _
      Nuevoestado = ctePREBORRADA Then _
      .strEstado = mnuItemEstadoMuestra(Index).Tag
    End With
  Next
  If i > 0 Then
    objAutoAn.pRefreshGrids
    If Nuevoestado = ctePREBORRADA Then
      BORRADOPendiente = True
      TipoMensEnviado = cteBORRADO
      Envio_Mensajes
    End If
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "mnuItemEstadoMuestra_Click", "Index:" & Index)
End Sub

Private Sub Timer1_Timer()
'Controla los Time Out que produce el autoanalizador
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Timer1_Timer (TimeOut)"
  SubRutina = "Timer1_Timer"
  Timer1.Enabled = False
  Select Case objAutoAn.Estado
    Case cteSOLICITUD  'Habiendo el sistema "solicitado l�nea", el autoanalizador tarda m�s de 15 segundos en responder
      objAutoAn.Estado = cteESPERA

      RESPUESTAEsperada = False
      Envio_Mensajes 'para realizar un nuevo intento de comunicaci�n
    Case cteMASTER 'Habiendo el sistema enviado datos, el autoanalizador tarda m�s de 15 segundos en responder
      objAutoAn.Estado = cteCONFIRMACION
      Envio_Mensajes 'para realizar un nuevo intento de envio de datos
    Case cteCONFIRMACION 'Habiendo el sistema enviado datos, el autoanalizador tarda m�s de 15 segundos en responder
      objAutoAn.Estado = cteCONFIRMACION
      Envio_Mensajes 'para realizar un nuevo intento de envio de datos
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Timer1_Timer", "Timer1_Timer")
End Sub

Private Sub Timer2_Timer()
'Controla los tiempos entre env�os de solicitudes
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Timer2_Timer (Tiempos entre envios)"
  SubRutina = "Timer2_Timer"
  Timer2.Enabled = False
  Envio_Mensajes
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Timer2_Timer", "Timer2_Timer")
End Sub

Private Sub Timer3_Timer()
'Controla el tiempo que transcurre desde los �ltimo datos recibidos por el sistema. Si estando el sistema en el estado de
'esclavo pasan 60 segundos sin recibir datos o el final de la comunicaci�n <EOT> se finaliza la ejecuci�n
Dim sql As String
Dim resp As Integer
On Error GoTo AutoAnError
    objAutoAn.EscribirLog "Timer3 (Tiempo sin Recibir datos)"
    SubRutina = "Timer3_Timer"
    Timer3.Enabled = False
    If objAutoAn.Estado = cteESCLAVO Then
      sql = "Se ha producido un Timeout durante la comunicaci�n por parte del Autoanalizador." _
      & Chr$(10) & "Puede que se haya perdido la comunicaci�n." _
      & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
      resp = MsgBox(sql, vbExclamation + vbYesNo, frmPrincipal.Caption)
      Select Case resp
        Case vbYes
          Timer3.Enabled = True
        Case vbNo
          'Form_Unload (False)
          objAutoAn.Estado = cteESPERA

      End Select
      RESPUESTAEsperada = False
    End If
    Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Timer3_Timer", "Timer3_Timer")
End Sub

Public Function Salir() As Boolean
On Error GoTo AutoAnError
  SubRutina = "Salir"
  objAutoAn.EscribirLog "Salir"
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion, Me.Caption) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"
  If Not Salir Then Descargar
  Exit Function
  
AutoAnError:
    Call objError.InternalError(Me, "Salir", "Salir")
End Function

Public Sub Descargar()
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Descargar"
  End
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Descargar", "Descargar")
End Sub

Private Sub Stream801_Funcion01(Mensaje As String)
'Se forma un mensaje que responde al formato del stream 301, funci�n 01 (programaci�n de pruebas) y se establecen otras
'caracter�sticas relacionadas con la respuesta esperada y futuros env�os
Dim cPrueba As String       'c�digo de la prueba/c�digo de la prueba seg�n codificaci�n del CX3
Dim pruebas As String       'c�digos de las pruebas acumulados en un mismo string seg�n codificaci�n del CX3
Dim cEspecimen As String    'c�digo de la especie/c�digo de la especie seg�n codificaci�n del CX3
Dim contador1 As Integer
Dim contador2 As Integer
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
On Error GoTo AutoAnError
    
    'For contador1 = 1 To UBound(MatrizProg, 2)
  objAutoAn.EscribirLog "Stream801_Funcion01"
  SubRutina = "Stream801_Funcion01"
  For Each objMuestra In objAutoAn.ColMuestras 'ColProgramMuestras
    With objMuestra
      If .strEstado = cteLEIDA Then
        'Se comienza a formar el mensaje
        Mensaje = "[00,801,01,"
        'el ultimo 1 es para poder programar solo nuevas pruebas a una muestra que ya exista y si no programa una nueva
        'el caso 0 hace reemplazar todo lo que exista
        Mensaje = Mensaje & "0000," & "00," & "1,"
        'Se se�ala si es urgencia o no
        Mensaje = Mensaje & IIf(.blnUrgente, "ST", "RO") _
        & .strProperty1 & "," & Trim$(.strCodMuestra) & Space$(5) _
        & "," & Space$(20) & "," & Space$(12) & "," & Space$(25) _
        & "," & Space(18) & "," & Space$(15) & ", ," & Space$(15) _
        & "," & Space$(18) & "," & Format$(Now, "ddmmyyyy") & "," & Format$(Now, "hhmm") _
        & "," & Space$(20) & "," & Space$(3) & "," & Space$(1) _
        & "," & Space$(8) & "," & Space(1) & "," & Space$(45) _
        & "," & Space$(7) & "," & Space$(4) & "," & Space$(4) _
        & "," & Space$(2) & "," & Space$(6)
        'aqui tendre que distinguir cuando realizo la programacion de todas las
        'muestras o de solo las de repeticion porque en el 1er caso me cojo
        'todas y en el 2� solo las seleccionadas (tb puedo ver si hay alguna
        'fila seleccionada y si hay me quedo con las seleccionadas y sino con todas)
        '.nada de esto es cierto pues solo programo las que me pregunta
        'y estan en matrizprog
        'Se buscan y se codifican todas la pruebas que hay que realizar a la muestra. Se sigue completando el mensaje.
        If .blnRepetida Then
          For Each objActuacion In .ColMuestrasPruebas
            With objActuacion
              If .intRepeticion = 1 Then
                pruebas = pruebas & objAutoAn.ColPruebaAuto(.strCodAct).strCodActAuto & "0,"
              End If
            End With
          Next
        Else
          For Each objActuacion In .ColMuestrasPruebas
            pruebas = pruebas & objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto & "0,"
          Next
        End If
        'hay que tener en cuenta que pruebas lleva la coma final
        Mensaje = Mensaje & "," & Right$("000", 3 - Len(CStr(Len(pruebas) / 7))) _
         & CInt(Len(pruebas) / 7) & "," & pruebas
        Mensaje = Left$(Mensaje, Len(Mensaje) - 1) & "]"
        'Se establece el check para el mensaje
        Establecer_Check Mensaje
        'Se coloca el estado "IP" para la muestra.
        .strEstado = cteINTENTOPROGRAMAR
        Exit For
      End If
    End With
  Next 'contador1
  'para que no intente mandar mensaje estando ya todo programado(mandaria "" & CRLF) y CX� rsponder�a NAK
  If Not objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR) Then
    PROGRAMPendiente = False: UltimoEnvio = True ': txtrecibir = txtrecibir & "ultimoenvio(stream801)=true" & Chr$(13) & Chr$(10) Else PROGRAMPendiente = True: UltimoEnvio = False: txtrecibir = txtrecibir & "ultimoenvio(stream801)=false" & Chr$(13) & Chr$(10)
  End If
  'Se establecen el tipo de respuesta esperado par parte del CX3 al mensaje que se le va a enviar.
  FuncionRespEsperada = ctePROGRAMACION
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream801_Funcion01", "plistaTrabajo")
End Sub

Private Sub Stream801_Funcion02(Datos As String)
'Se lee la respuesta al intento de programaci�n de las pruebas de una muestra.
Dim sql As String
Dim resp As Integer
Dim CodeError As Integer
Dim CodeMuestra As String
Dim objMuestra As clsMuestra
On Error GoTo AutoAnError

  SubRutina = "Stream801_funcion2"
  objAutoAn.EscribirLog "Stream801_funcion2"
  If Mid$(Datos, 5, 3) = "801" And Mid$(Datos, 9, 2) = "02" Then
    CodeError = Val(Mid$(Datos, 12, 2))
    CodeMuestra = Trim$(Mid$(Datos, 29, 15))
    For Each objMuestra In objAutoAn.ColMuestras
      With objMuestra
        If .strEstado = cteINTENTOPROGRAMAR And CodeMuestra = .strCodMuestra Then
          Select Case CodeError
            Case 0: .strEstado = cteACEPTADA
            Case Else
              sql = "La muestra " & .strCodMuestra & " no puede ser programada." & Chr$(10)
              Select Case CodeError
                Case 1: sql = sql & "Mensaje de programaci�n de pruebas erroneo. Error de sintaxis"
                Case 2: sql = sql & "La pantalla de programaci�n de pruebas est� activa"
                Case 3: sql = sql & "C�digo de prueba no identificado en el CX3"
                Case 4: sql = sql & "Petici�n ORDAC invalida"
                Case 5: sql = sql & "Combinaci�n de pruebas no valido (Se requiere una diluci�n o volumen de muestra excesivo)."
                Case 6: sql = sql & "Control no configurado"
                Case 7: sql = sql & "Solo sector de calibrador"
                Case 8: sql = sql & "Error de modo (los modos Codigo de barras y Sector no son los mismos para el CX3 y Ordenador"
                Case 9: sql = sql & "Error Interno en el CX3"
                Case 10: sql = sql & "Todos los sectores ya estan programados .No se pueden a�adir mas muestras, solo modificarlas"
                Case 11: sql = sql & "Tipos de fluido incompatibles"
                Case 12: sql = sql & "Tipos de pruebas incompatibles"
                Case 13: sql = sql & "Nombre de paciente incorrecto"
                Case 14: sql = sql & "El C�digo de Muestra Ya Existe en el Autoanalizador"
                Case 15: sql = sql & "El N� deRack debe estar entre 0000 y 9999"
              End Select
              resp = MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
              Select Case resp
                Case vbAbort: Descargar 'Form_Unload (False)
                Case vbRetry: .strEstado = cteSINPROGRAMAR: UltimoEnvio = False
                Case vbIgnore: .strEstado = cteNOACEPTADA
              End Select
          End Select
          Exit For
        End If
      End With
    Next
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream801_Funcion02", "plistaTrabajo")
End Sub

Private Sub Stream801_Funcion03(Mensaje As String)
Dim i%, ListaMuestrasABorrar$, x%, contador%
Dim objMuestra As clsMuestra
Dim NumMuestrasABorrar As Integer 'lleva el control del n� de
'muestras a borrar pues no podemos eliminar mas de 7 en un solo mensaje
Static ultmuestraborrada As Integer 'lleva la posicion de la ultima
'muestra borrada para poder formar a partir de ella el siguiente mensaje

'Se forma un mensaje que responde al formato del stream 801, funci�n 03 y se establecen otras caracter�sticas relacionadas
'con la respuesta esperada y futuros env�os
On Error GoTo AutoAnError

  SubRutina = "Stream801_Funcion03"
  objAutoAn.EscribirLog "Stream801_Funcion03"
  ListaMuestrasABorrar = ""
  'txtrecibir = txtrecibir & "Subrutina Stream 801 Borrado pruebas" & Chr$(13) & Chr$(10)
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      If .strEstado = cteAREPETIR Then
        .strEstado = cteINTENTOBORRADO
        NumMuestrasABorrar = NumMuestrasABorrar + 1
        ListaMuestrasABorrar = ListaMuestrasABorrar & .strIDMuestra & Space$(15 - Len(.strIDMuestra)) & ","
        If NumMuestrasABorrar >= 4 Then Exit For
      End If
    End With
  Next
  ultmuestraborrada = contador
  ListaMuestrasABorrar = ListaMuestrasABorrar & Right$(Space$(15) & "," & Space$(15) & "," & Space$(15) & "," & Space$(15) & "," & Space$(15) & "," & Space$(15) & "," & Space$(15) & ",", 12 * (4 - NumMuestrasABorrar))
  ListaMuestrasABorrar = Left$(ListaMuestrasABorrar, Len(ListaMuestrasABorrar) - 1)
  Mensaje = "[00,801,03,0000," & ListaMuestrasABorrar & "]"
  Establecer_Check Mensaje
  FuncionRespEsperada = cteBORRADO
  UltimoEnvio = True
  BORRADOPendiente = objAutoAn.blnExistMuestraConEstado(cteAREPETIR)
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream801_Funcion03", "plistaTrabajo")
End Sub

Private Sub Stream801_Funcion04(Datos As String)
'Se lee la respuesta al intento de borrado de las pruebas programadas.
Dim sql As String
Dim code As Integer
Dim resp As Integer
Dim num3%, num2%, i%
Static num0%
Dim objMuestra As clsMuestra
On Error GoTo AutoAnError

  SubRutina = "Stream801_Funcion04"
  objAutoAn.EscribirLog "Stream801_Funcion04"
  If Mid$(Datos, 5, 3) = "801" And Mid$(Datos, 9, 2) = "04" Then
    code = Val(Mid$(Datos, 17, 2))
    If code <> 0 And code <> 4 Then
      sql = "No se pueden eliminar las pruebas programadas." & Chr$(10)
      Select Case code
        Case 1: sql = sql & "Mensaje de borrado de pruebas err�neo."
        Case 3: sql = sql & "Ha ocurrido un error en el SYNCHRON"
        Case 2: sql = sql & "El autoanalizador est� realizando an�lisis en este momento."
        Case 5: sql = sql & "El N� de Rack es Demasiado Largo"
      End Select
      resp = MsgBox(sql, vbExclamation + vbRetryCancel, "Borrado de Muestras en CX3")
      Select Case resp
        Case vbCancel: Exit Sub
        Case vbRetry:
          TipoMensEnviado = cteBORRADO
          For Each objMuestra In objAutoAn.ColMuestras
            If objMuestra.strEstado = cteINTENTOBORRADO Then objMuestra.strEstado = cteAREPETIR
          Next
          Envio_Mensajes
      End Select
    Else
      For i = 20 To 29 Step 3
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteINTENTOBORRADO Then
              Select Case Mid$(Datos, i + 1, 1)
                Case "0", "4"
                  num0 = num0 + 1
                  .strEstado = cteBORRADA
                Case "2"
                  num2 = num2 + 1
                  .strEstado = cteNOBORRADA
                Case "3"
                  num3 = num3 + 1
                  .strEstado = cteNOBORRADA
              End Select
            End If
          End With
        Next
      Next i
      If num3 > 0 Then sql = "Ha Ocurrido Un Error en el CX3 ."
      If num2 > 0 Then sql = sql & Chr$(13) & "El Autoanalizador Est� Realizando An�lisis En Este Momento."
      If Not BORRADOPendiente Then
        'en caso de ir todo bien me pondra al final de todo el proceso de borrado el n�muestras borradas
        If num0 > 0 Then sql = sql & Chr$(13) & "Se Van a Repetir " & num0 & " Muestras." & Chr$(13) & Chr$(10) & " 1� -Pulse Aceptar y " & Chr$(13) & Chr$(10) & "2� -Pulse Iniciar Para Comenzar las Repeticiones"
        num0 = 0  'reinicializamos el n� muestras totales borradas
'              ReDim MatrizProg(1 To 7, 1 To 1)
        NotPrim = 0
        If sql <> "" Then
          resp = MsgBox(sql, vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
        Else
          resp = MsgBox("Por Favor Borre Manualmente en el " & frmPrincipal.Caption & " las Muestras Con Repeticiones y Pulse Iniciar", vbInformation, "Borrado de Muestras en " & frmPrincipal.Caption)
        End If
      End If
    End If
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream801_Funcion04", "plistaTrabajo")
End Sub

Private Sub Lectura_Datos_Synchron(Datos As String)
'Los datos que llegan pueden ser esperados
'(respuesta a un mensaje enviado anteriormente) o
'expont�neos (resultados enviados por el autoanalizador.
Dim cresultadoauto As String    'C�digo del resultado definido en la tabla resultadosAutoanalizador
Dim tipoEspecie As String
Dim RES As String
Dim i As Integer, j As Integer, n As Integer, x%
Dim filenum
Dim resul As Double
Dim objMuestra As clsMuestra
On Error GoTo AutoAnError

  SubRutina = "Lectura_Datos_Synchron"
  objAutoAn.EscribirLog "Lectura_Datos_Synchron"
  If RESPUESTAEsperada Then
  'El mensaje es esperado.
    Select Case FuncionRespEsperada
      Case ctePROGRAMACION: Stream801_Funcion02 Datos 'Respuesta a un intento de programar pruebas para una muestra.
      Case cteBORRADO:  Stream801_Funcion04 Datos 'Respuesta a un intento de borrado de las pruebas programadas.
    End Select
  Else
  'El mensaje es de resultados.
  'Identifica de que tipo de record se trata y lee sus datos seg�n lo expresado en el manual.
    Select Case Mid$(Datos, 5, 3) & Mid$(Datos, 9, 2)
      Case "80106"
        'el CX3 pide la programacion de las muestras leidas
        'por el CB y estas se van programando
        PROGRAMPendiente = True
        For i = 12 To 72 Step 15
          If Mid$(Datos, i, 15) <> Space$(15) Then
            For Each objMuestra In objAutoAn.ColMuestras
              With objMuestra
                If .strIDMuestra = Trim$(Mid$(Datos, i, 15)) And .strEstado = cteSINPROGRAMAR Then
                  .strEstado = cteLEIDA
                  Exit For
                End If
              End With
            Next
          End If
        Next i
        If Not objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR) Then
          UltimoEnvio = True
          PROGRAMPendiente = False
        End If
      Case "80201"
        'Se inicializa la matriz que contendr� los resultados. Esta matriz almacenar� en (1,1) la ID de la muestra, en
        '(2,1) la fecha y la hora separadas por una coma, en (1,X) el cResultadoAuto de la prueba y en (2,X) el resultado.
'            ReDim MatrizResultados(1 To 2, 1 To 1)
        Dim IdMuestra As String
        'Se obtiene la ID de la muestra y la fecha y hora
        'MatrizResultados(1, 1) = Trim$(Mid$(datos, 25, 9))
'            MatrizResultados(1, 1) = Trim$(Mid$(datos, 57, 11)) 'codigo num�rico de la muestra
        IdMuestra = Trim$(Mid$(Datos, 61, 15)) 'codigo num�rico de la muestra
        With objAutoAn
          IdMuestra = .fNumeroACodigo(IdMuestra)
          If .blnExistMuestra(IdMuestra) Then
            .ColMuestras(IdMuestra).strEstado = cteRECIBIENDORESULTADOS
          End If
        End With
      Case "80203"
        'Se Introducen Resultados
        RES = CStr(Trim$(Mid$(Datos, 91, 9)))
        i = objAutoAn.fFormatearNumero(RES, ".", ",")
        If Left$(RES, 1) = "," Then RES = "0" & RES
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteRECIBIENDORESULTADOS Then
              'Call pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(datos, 68, 4)), RES)
            End If
          End With
        Next
      Case "80211"
        'Se almacenan los resultados especiales en la matriz
'            ReDim Preserve MatrizResultados(1 To 2, 1 To UBound(MatrizResultados, 2) + 1)
        'cResultadoAuto = Trim$(Mid$(datos, 22, 2))
'            cresultadoauto = Trim$(Mid$(datos, 53, 13))
'            txtrecibir = txtrecibir & "result.especial:" & cresultadoauto & Chr$(13) & Chr$(10)

        'Select Case cresultadoauto 'NOTA: PONER COMO CONSTANTES
        'Case 0, 1
        '    cresultadoauto = constANION_GAP
        'Case 2, 3
        '    cresultadoauto = constUREA_CREATININA
        'Case 4
        '    cresultadoauto = constOSMOLARIDAD
        'End Select
'            MatrizResultados(1, UBound(MatrizResultados, 2)) = cresultadoauto
        RES = CStr(Trim$(Mid$(Datos, 85, 9)))
        i = objAutoAn.fFormatearNumero(RES, ".", ",")
        If Left$(RES, 1) = "," Then RES = "0" & RES
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteRECIBIENDORESULTADOS Then
              Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(Datos, 61, 20)), RES)
            End If
          End With
        Next
      Case "80205"
        'se recibe el indicativo de fin de transmision de todas las pruebas de esa muestra
        'Se procede a actualizar la base de datos
        'Actualizar_BD
      Case "80317"
        'Se recibe la notificaci�n de que ya se han recibido todos los resultados
        'lblRepeticion.Visible = False 'una vez recibidas todas las repeticiones se quita el aviso
        objAutoAn.LabelVisible False
        'desde aqui es desde donde se activa la variable que mandar� repetir las pruebas al recibir EOT (CX_esclavo)
        RepetirPruebas = RepetirPendiente
        If Not RepetirPruebas Then Salir
    End Select
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos_Synchron", "Lectura_Datos_Synchron")
End Sub

Private Sub SYNCHRON_Confirmacion(Mensaje As String, Caracter As String)
Dim contador As Integer
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
'Si el mensaje es rechazado habr� que volver a enviarlo.
On Error GoTo AutoAnError
    
'  objAutoAn.EscribirLog "Synchron Confirmacion"
  SubRutina = "SYNCHRON_Confirmacion"
  Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then  'puede ser que el autoanalizador solicite ser master en caso de haber habido algun problema
      .Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
      .Estado = cteESCLAVO
      .ButtonProgramEnabled False
      Mensaje = "" 'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
      Timer3.Enabled = True
      Respuesta = cteACK 'Respuesta enviada al autoanalizador
      RespuestaAfirmativa = True 'Proxima respuesta afirmativa a enviar al autoanalizador
      Exit Sub
    End If
    Select Case Caracter
      Case Chr$(3)  '<ETX> - El autoanalizador ya ha aceptado el bloque y da por finalizada la transmision
        .Enviar Chr$(4) '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        .ButtonProgramEnabled True
        Mensaje = ""
        'cuando dejamos de ser los transmitir esperamos la respuesta a estos mensajes
        RESPUESTAEsperada = True
        Exit Sub
      Case Chr$(6) '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
          'MsgBox "Recibido un ACK en Confirmaci�n", vbExclamation, frmprincipal.Caption
      'siempre responde ETX?
      '    Envio_Mensajes 'Se enviar� un <ENQ> para que el autoanalizador vuelva a mandar la respuesta
      '    mensaje = ""
        Exit Sub
      Case Chr$(21)   '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviarlo
        If TipoMensEnviado = ctePROGRAMACION Then
          Dim objMuestra As clsMuestra
          For Each objMuestra In objAutoAn.ColMuestras
            If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
              objMuestra.strEstado = cteSINPROGRAMAR
              UltimoEnvio = False
              Exit For
            End If
          Next
        End If
        .Estado = cteMASTER
        .ButtonProgramEnabled False
        Mensaje = ""
        Timer2.Enabled = True 'Se espera 1 segundo para retransmitir el bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "SYNCHRON_Confirmacion", "plistaTrabajo")
End Sub

Private Sub SYNCHRON_Esclavo(Mensaje As String, Caracter As String)
'Identifica el final de un bloque y comprueba su check pasando a su lectura
'si fuera correcto. Si el bloque ha llegado incorrectamente se contesta
'negativamente para que el autoanalizador retransmita. Tambi�n se identifica
'el final de la comunicaci�n y se vuelve al estado de espera.
Dim contador As Integer
Dim sql As String
On Error GoTo AutoAnError

'  objAutoAn.EscribirLog "Synchron esclavo"
  SubRutina = "SYNCHRON_Esclavo"
  Timer3.Enabled = False 'Al llegar datos del autoanalizador, se desactiva el Timer
  Select Case True
    Case Caracter = Chr$(4) '<EOT> - Final de la transmisi�n por parte del autoanalizador
      objAutoAn.Estado = cteESPERA

      Mensaje = ""
      Select Case True
        Case Salida
          Salir
        Case BORRADOPendiente
          'cuando el numero de muestras a borrar supera 7 hay que mandar desde aqui sucesivos mensajes de borrado
          TipoMensEnviado = cteBORRADO
          Envio_Mensajes
        Case PROGRAMPendiente
          PROGRAMPendiente = False
          UltimoEnvio = False
          TipoMensEnviado = ctePROGRAMACION
          Envio_Mensajes
        Case RepetirPruebas
          'lblRepeticion.Visible = True
          objAutoAn.LabelVisible True
          RepetirPendiente = False
          'se forma una nueva matrizgrid que contiene las repeticiones y ademas
          'Se borran las pruebas programadas la vez anterior para que al darle al start puedan reprogramarlas
          Repetir_Pruebas
          RepetirPruebas = False
        Case RESPUESTAEsperada
          'cuando recibimos la finalizacion de las respuestas a nuestros mensajes se dejan de esperar estas
          RESPUESTAEsperada = False
          If TipoMensEnviado = ctePROGRAMACION Then
            If Not UltimoEnvio Then
              'aqui deberiamos seguir con la programacion automatica de las muestras de matrizprog solicitadas
              'con el mensaje 6
              'cmdProgramar_Click 'Se env�a una nueva prueba a programar
              Envio_Mensajes
            Else
              If Not objAutoAn.blnExistMuestraSinEstado(cteNOACEPTADA) Then
                'todas las muestras estan NA
                'No se ha admitido la programaci�n de ninguna de las pruebas
                sql = "El autoanalizador " & frmPrincipal.Caption & " no ha aceptado la "
                sql = sql & "la programaci�n de ninguna prueba." & Chr$(10) & "Volver a "
                sql = sql & "ejecutar el programa para intentarlo de nuevo."
                MsgBox sql, vbExclamation, frmPrincipal.Caption
'                Form_Unload (False)
                Descargar
              End If
            End If
          End If
      End Select
    Case Caracter = Chr$(5) '<ENQ> - El autoanalizador solicita respuesta
    'a un bloque mandado tras haber ocurrido un T.O. No es normal que ocurra
    'un T.O. en el autoanalizador ya que la la respuesta al bloque se env�a
    'una vez se ha comprobado el check
      Select Case Respuesta
        Case cteACK: objAutoAn.Enviar Chr$(6) '<ACK>
        Case cteETX: objAutoAn.Enviar Chr$(3) '<ETX>
        Case cteNAK: objAutoAn.Enviar Chr$(21) '<NAK>
      End Select
      Mensaje = ""
      Timer3.Enabled = True
    Case Right$(Mensaje, 2) = Chr$(13) & Chr$(10)  'Se ha identificado el final del bloque
      objAutoAn.Enviar IIf(RespuestaAfirmativa, Chr$(3), Chr$(6))  '<ACK>/<ETX>
      Respuesta = IIf(RespuestaAfirmativa, cteETX, cteACK) 'Respuesta enviada al autoanalizador
      RespuestaAfirmativa = Not RespuestaAfirmativa
      Lectura_Datos_Synchron Mensaje 'Se procede a la lectura de datos
      Mensaje = ""
      'para ver cuanto tiempo tarda en enviar el siguiente mensaje
      Timer3.Enabled = True
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "SYNCHRON_Esclavo", "plistaTrabajo")
End Sub

Private Sub SYNCHRON_Espera(Mensaje As String, Caracter As String)
'Identifica la llegada de los caracteres de solicitud de comunicaci�n,
'la acepta y pasa al estado de transmisi�n.
On Error GoTo AutoAnError
'  objAutoAn.EscribirLog "Synchron espera"
  SubRutina = "SYNCHRON_Espera"
  If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then '<EOT><SOH> - Solicitud de comunicaci�n por parte del autoanalizador
    objAutoAn.Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
    objAutoAn.Estado = cteESCLAVO
    Mensaje = "" 'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
    Timer3.Enabled = True
    Respuesta = cteACK 'Respuesta enviada al autoanalizador
    RespuestaAfirmativa = True 'Proxima respuesta afirmativa a enviar al autoanalizador
    Exit Sub
  End If
  If Caracter = Chr$(5) Then '<ENQ> - El autoanalizador solicita respuesta
  'a un bloque mandado tras haber ocurrido un T.O. No es normal que ocurra
  'un T.O. en el autoanalizador ya que la la respuesta al bloque se env�a
  'una vez se ha comprobado el check
    objAutoAn.Enviar Chr$(4) '<EOT> - El sistema da por finalizada la comunicaci�n
    Mensaje = ""
  End If
  Exit Sub

AutoAnError:
    Call objError.InternalError(Me, "SYNCHRON_Espera", "plistaTrabajo")
End Sub

Private Sub SYNCHRON_Master(Mensaje As String, Caracter As String)
Dim contador As Integer
Dim objMuestra As clsMuestra
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
'Si el mensaje es rechazado habr� que volver a enviarlo.
'On Error Resume Next
'  objAutoAn.EscribirLog "Synchon Master"
  SubRutina = "SYNCHRON_Master"
  Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    Select Case Caracter
      Case Chr$(3) '<ETX> - El autoanalizador ya ha aceptado el bloque y da por finalizada la transmision
        .Enviar Chr$(4) '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        .ButtonProgramEnabled True
        Mensaje = ""
        'cuando recibimos la aceptacion y dejamos de transmitir esperamos la respuesta a estos mensajes
        RESPUESTAEsperada = True
      Case Chr$(6) '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
        'MsgBox "Recibido un ACK en Master", vbExclamation, frmprincipal.Caption
        'siempre responde ETX?
        .Estado = cteCONFIRMACION
        .ButtonProgramEnabled False
        Envio_Mensajes 'Se enviar� un <ENQ> para que el autoanalizador vuelva a mandar la respuesta
        Mensaje = ""
      Case Chr$(21) '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviar
        If TipoMensEnviado = ctePROGRAMACION Then
        'Se actualiza la matriz que controla el grid
          For Each objMuestra In .ColMuestras 'ColProgramMuestras
            With objMuestra
              If .strEstado = cteINTENTOPROGRAMAR Then
                .strEstado = cteSINPROGRAMAR
                UltimoEnvio = False
                Exit For
              End If
            End With
          Next
        End If
        Mensaje = ""
        Timer2.Enabled = True 'Se espera 1 segundo para retransmitir el ultimo bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "SYNCHRON_Master", "plistaTrabajo")
End Sub

Private Sub SYNCHRON_Solicitud(Mensaje As String, Caracter As String)
'Tras haber realizado el sistema una solicitud para establecer una comunicaci�n, el autoanalizador responde aceptando o
'negando dicha comunicaci�n.
On Error GoTo AutoAnError
'    objAutoAn.EscribirLog "Synchron Solicitud"
    SubRutina = "SYNCHRON_Solicitud"
    Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
    With objAutoAn
      Select Case Caracter
        Case Chr$(6)  '<ACK> - El autoanalizador ha aceptado la comunicaci�n
          .Estado = cteMASTER
          .ButtonProgramEnabled False
          Mensaje = ""
          Envio_Mensajes 'Se env�a el bloque de datos por parte del sistema al autoanalizador '!!MODIFICAR
          Exit Sub
        Case Chr$(21), Chr$(3) '<NAK> o <ETX> - El autoanalizador no ha aceptado la comunicaci�n
          .Estado = cteESPERA
          .ButtonProgramEnabled True
          Mensaje = ""
          Timer2.Enabled = True 'Se espera 1 segundo para intentar una nueva comunicaci�n
          Exit Sub
      End Select
      If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then '<EOT><SOH> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
        If TipoMensEnviado = ctePROGRAMACION Then PROGRAMPendiente = True
        .Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
        .Estado = cteESCLAVO
        .ButtonProgramEnabled False
        Mensaje = ""
        Timer3.Enabled = True
      End If
    End With
    'If Right$(mensaje, 2) = Chr$(4) & Chr$(1) Then '<EOT><SOH> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
    '    If TipoMensEnviado = 1 Then
    '        PROGRAMPendiente = True
    '    End If
    '    OBJAUTOAN.ENVIAR Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
    '
    '
    '
    '    estado = constESCLAVO
    '    panEstado.Caption = LeerEstado(estado)
    '     txtrecibir = txtrecibir & "< " & Format(Now, "hh:mm:ss") & " " & panEstado.Caption & ">" & Chr$(13) & Chr$(10)
    '    mensaje = ""
    '    Timer3.Enabled = True
    'End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "SYNCHRON_Solicitud", "plistaTrabajo")
End Sub

Private Property Let PROGRAMPendiente(v As Integer)    'Indica si existe alguna prueba que deb�a haber sido programada
  MiPROGRAMPendiente = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " PROGRAMPendiente = " & v
End Property

Private Property Let BORRADOPendiente(v As Integer)   'Indica si existe alguna muestra que todav�a est� pendiente de borrar
  MiBORRADOPendiente = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " BORRADOPendiente = " & v
End Property

Private Property Let RepetirPendiente(v As Integer)  'vble que indica que se tienen que hacer repeticiones que se
  MiRepetirPendiente = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " RepetirPendiente = " & v
End Property

Private Property Let RepetirPruebas(v As Integer)     'Indica si las pruebas se van a repetir o no
  MiRepetirPruebas = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " RepetirPruebas = " & v
End Property

Private Property Let Respuesta(v As TipoRespuesta)   'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
  MiRespuesta = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " Respuesta = " & v
End Property

Private Property Let RespuestaAfirmativa(v As Boolean)  'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional
  MiRespuestaAfirmativa = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " RespuestaAfirmativa = " & v
End Property

Private Property Let TipoMensEnviado(v As TipoMensajeEnviado)    'Indica el tipo de mensaje que el sistema transmite al autoanalizador y que puede ser:
  MiTipoMensEnviado = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " TipoMensEnviado = " & v
End Property

Private Property Let UltimoEnvio(v As Integer)  'True: si no hay m�s env�os de prueba a programar
  MiUltimoEnvio = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " UltimoEnvio = " & v
End Property

Private Property Let RESPUESTAEsperada(v As Boolean)  'True: si se est� esperando un mensaje de respuesta por parte del autoanalizador. Esto
  MiRESPUESTAEsperada = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " RESPUESTAEsperada = " & v
End Property

Private Property Let FuncionRespEsperada(v As TipoMensajeEnviado)
  MiFuncionRespEsperada = v
  objAutoAn.EscribirLog "SRutina::" & SubRutina & " FuncionRespEsperada = " & v
End Property

Private Property Get PROGRAMPendiente() As Integer    'Indica si existe alguna prueba que deb�a haber sido programada
  PROGRAMPendiente = MiPROGRAMPendiente
End Property

Private Property Get BORRADOPendiente() As Integer   'Indica si existe alguna muestra que todav�a est� pendiente de borrar
  BORRADOPendiente = MiBORRADOPendiente
End Property

Private Property Get RepetirPendiente() As Integer  'vble que indica que se tienen que hacer repeticiones que se
  RepetirPendiente = MiRepetirPendiente
End Property

Private Property Get RepetirPruebas() As Integer     'Indica si las pruebas se van a repetir o no
  RepetirPruebas = MiRepetirPruebas
End Property

Private Property Get Respuesta() As TipoRespuesta   'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
  Respuesta = MiRespuesta
End Property

Private Property Get RespuestaAfirmativa() As Boolean  'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional
  RespuestaAfirmativa = MiRespuestaAfirmativa
End Property

Private Property Get TipoMensEnviado() As TipoMensajeEnviado    'Indica el tipo de mensaje que el sistema transmite al autoanalizador y que puede ser:
 TipoMensEnviado = MiTipoMensEnviado
End Property

Private Property Get UltimoEnvio() As Integer  'True: si no hay m�s env�os de prueba a programar
  UltimoEnvio = MiUltimoEnvio
End Property

Private Property Get RESPUESTAEsperada() As Boolean  'True: si se est� esperando un mensaje de respuesta por parte del autoanalizador. Esto
  RESPUESTAEsperada = MiRESPUESTAEsperada
End Property

Private Property Get FuncionRespEsperada() As TipoMensajeEnviado
  FuncionRespEsperada = MiFuncionRespEsperada
End Property

Public Sub Probar()
  objAutoAn.autorecepcion Chr$(4) & Chr$(1)
End Sub

Public Sub pmodificaMuestra(objMuestra As clsMuestra)
'Dim objActuacion As clsActuacion
'Dim objResultado As clsResultado
'Dim qry As rdoQuery
'Dim rs As rdoResultset
'Dim sql$
'On Error GoTo Error
'
'  For Each objMuestra In objAutoAn.ColMuestras
'    With objMuestra
'      'Especimen
'      sql = "SELECT cTipoEspecieAuto FROM TiposEspeciesAutoanalizador"
'      sql = sql & " WHERE cautoanalizador =? and (pr24codtimuestr) IN"
'      sql = sql & " (SELECT pr49codtipmuestr FROM PR2400"
'      sql = sql & " WHERE pr24codmuestra = ?)"
'      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
'      qry(0) = cteBECKMAN_CX3
'      qry(1) = .strCodMuestra
'      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'      If Not rs.EOF Then   'Si no se encuentra el especimen quedar� vac�a su posicion en la matriz
'        .strProperty1 = rs("cTipoEspecie")
'        'Las orinas solo se hacen una vez
'        If rs("ctipoespecie") = "2" Then .strProperty2 = constORINA
'      End If
'      qry.Close: rs.Close
'      'Sexo y Edad
'      sql = "Select CI30CODSEXO, CI22FECNACIM From CI2200 Where"
'      sql = sql & " CI21CODPERSONA in (Select CI21codpersona from"
'      sql = sql & " PR0400 where PR04NUMACTPLAN = ?"
'      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
'      qry(0) = CLng(.ColMuestrasPruebas(1).strNumAct)
'      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'      If Not rs.EOF Then
'        If rs("CI22FECNACIM") = Null Then
'          .strBandeja = "1"
'        Else
'          .strBandeja = CStr(DateDiff("yyyy", rs("CI22FECNACIM"), Now))
'        End If
'        .strCopa = IIf(rs("CI30CODSEXO") = 1, "M", "F") 'Hombre o Mujer
'      End If
'      qry.Close: rs.Close
''      For Each objActuacion In .ColMuestrasPruebas
''        With objActuacion
''          For Each objResultado In .ColPruebasResultados
''            sql = "SELECT referenciaMin, referenciaMax FROM pruebasValidacion"
''            sql = sql & " WHERE PR01CODACTUACION = ?  AND cResultado = ?"
''            sql = sql & " AND (sexo = ? OR sexo IS NULL)"
''            sql = sql & " AND (EdadInferior IS NULL OR EdadInferior <= ?)"
''            sql = sql & " AND (EdadSuperior IS NULL OR EdadSuperior >= ?)"
''            sql = sql & " AND (referenciaMin IS NOT NULL OR referenciaMax IS NOT NULL)"
''            Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
''            qry(0) = .strCodAct
''            qry(1) = objResultado.codResultado
''            qry(2) = objMuestra.strCopa
''            qry(3) = CInt(objMuestra.strBandeja)
''            Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
''            If Not rs.EOF Then
''              With objResultado
''                .strProperty1 = IIf(rs("referenciaMin") = "", "0", rs("referenciaMin"))
''                .strProperty2 = IIf(rs("referenciaMax") = "", "0", rs("referenciaMax"))
''              End With
''            End If
''            qry.Close: rs.Close
''          Next
''        End With
''      Next
'    End With
'  Next
'
'Error:

End Sub

Public Sub Programar()
Dim objMuestra As clsMuestra
Dim EstadoMuestra As constEstado
Dim objActuacion As clsActuacion
On Error GoTo AutoAnError

  If objAutoAn.blnRepetirPendiente And objAutoAn.Estado = cteESPERA Then
    objAutoAn.EscribirLog "Programar_click"
    'objAutoAn.blnRepetirPendiente = False
    BORRADOPendiente = True
    'MsgBox "Se Van a Procesar las Muestras Pendientes de Repeticion" & vbCrLf & " 1� -Pulse Aceptar En Este Mensaje y " & vbCrLf & "2� -Pulse Iniciar En El Autoanalizador CX3"
    For Each objMuestra In objAutoAn.ColMuestras
      With objMuestra
        objAutoAn.EscribirLog .strCodMuestra & ".Estado = " & .strEstado
        EstadoMuestra = .strEstado
        If EstadoMuestra = cteAREPETIR Or _
        EstadoMuestra = cteAREPETIRFIN Then
          For Each objActuacion In .ColMuestrasPruebas
            With objActuacion
              objAutoAn.EscribirLog "CodAct:" & .strCodAct & ".Estado = " & .strEstado
              If (.strEstado = cteAREPETIRFIN Or _
              .strEstado = cteAREPETIR) And _
              .intRepeticion = 1 And _
              objMuestra.strEspecimen <> "Orina" Then
                .intRepeticion = .intRepeticion + 1
              Else
                objActuacion.QuitarPrueba
              End If
            End With
          Next
        Else
          If EstadoMuestra = cteREALIZACIONCOMPLETA Or _
          EstadoMuestra = cteREALIZADA Or _
          EstadoMuestra = cteFUERADERANGO Or _
          EstadoMuestra = cteFUERADERANGOFIN Then
            'objAutoAn.ColMuestras.Remove .strCodMuestra
            objMuestra.QuitarMuestra
          End If
        End If
      End With
    Next
    objAutoAn.pRefreshGrids
    TipoMensEnviado = cteBORRADO
    Envio_Mensajes
  Else
    With objAutoAn
      If .blnExistMuestraConEstado(cteREALIZACIONCOMPLETA) Then
        .pSalir
      Else
        If .blnExistMuestraConEstado(cteAREPETIRFIN) Then
          .pSalir
        Else
          If .blnExistMuestraConEstado(cteFUERADERANGOFIN) Then
            .pSalir
          Else
            If .blnExistMuestraConEstado(cteREALIZADA) Then
              .pSalir
            End If
          End If
        End If
      End If
    End With
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Programar", "Programar")
End Sub
