Attribute VB_Name = "Module1"
Option Explicit

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
Declare Function WritePrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFileName As String) As Integer

Global txtrecibir As String

Public objAutoAn As New clsAutoAnalizador      'Objeto de la clase AutoAnalizador
Public objConst As New clsConst            'Objeto con todas las ctes
Public objError As New clsErrores
Global Const constORINA = "OR"

Public Sub Main()
  Screen.MousePointer = vbHourglass
  App.HelpFile = "f:\laborat\ayuda\" & App.EXEName & ".hlp"
  With objAutoAn
      .intCodAutoAn = objConst.constAUTOAXSYM  'constAUTOBECKMAN_CX3
      Set .frmFormulario = frmPrincipal
      .blnRepeticionAutomatica = True
      .blnCargaNuevasPruebas = False 'True
      .blnSalidaAutomatica = False
      fCargarColumnasGrid 'Definicion de las columnas que componen los 2 Grid
      If Not .fInicializar Then End
      fCargarEspecOrinaSexoEdadRefs
      .Show
  End With
  Screen.MousePointer = vbDefault
End Sub

Private Sub fCargarColumnasGrid()
  With objAutoAn
    'Columnas del Grid Muestras
    .AgregarColumnaAGridMuestras "Muestra", objConst.ConstCABMuestraCODMUESTRA, 1200
    .AgregarColumnaAGridMuestras "Id.Muestra", objConst.ConstCABIDMUESTRA, 1200
    .AgregarColumnaAGridMuestras "Urgente", objConst.ConstCABMuestraURGENTE, , InVisible
    .AgregarColumnaAGridMuestras "N�Rep.", objConst.ConstCABMuestraREPETICION
    .AgregarColumnaAGridMuestras "Estado", objConst.ConstCABMuestraESTADO
    .AgregarColumnaAGridMuestras "Especie", objConst.ConstCABMuestraPROPERTY1, , InVisible  'tipo especie
    .AgregarColumnaAGridMuestras "Orina", objConst.ConstCABMuestraPROPERTY2, , InVisible  'si es orina se pone 3
    'estos 2 habra que quitar mas tarde...
    .AgregarColumnaAGridMuestras "Edad", objConst.ConstCABBANDEJA   'Edad (aprovechando la columna Bandeja inutilizada)
    .AgregarColumnaAGridMuestras "Sexo", objConst.ConstCABCOPA   'Sexo(aprovechando la columna Copa inutilizada)
    'Columnas del Grid Pruebas-Resultados
    .AgregarColumnaAGridPruebas "Muestra", objConst.ConstCABPruebaCODMUESTRA
    .AgregarColumnaAGridPruebas "N�Rep.", objConst.ConstCABPruebaREPETICION, 1200, InVisible
    .AgregarColumnaAGridPruebas "Urgente", objConst.ConstCABPruebaURGENTE, 1200, InVisible
    .AgregarColumnaAGridPruebas "Prueba", objConst.ConstCABDESCRIACTUACION, 1250
    .AgregarColumnaAGridPruebas "Resultado", objConst.ConstCABDESCRIRESULTADO, 1350
    .AgregarColumnaAGridPruebas "Valor", objConst.ConstCABVALRESULTADO
    .AgregarColumnaAGridPruebas "Estado", objConst.ConstCABResultadoESTADO
    .AgregarColumnaAGridPruebas "RefMin", objConst.ConstCABResultadoPROPERTY1
    .AgregarColumnaAGridPruebas "RefMax", objConst.ConstCABResultadoPROPERTY2
  End With
End Sub

Private Sub fCargarEspecOrinaSexoEdadRefs()
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim objResultado As clsResultado
Dim qry As rdoQuery
Dim rs As rdoResultset
Dim sql$
On Error GoTo Error

  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      'Especimen
      sql = "SELECT cTipoEspecieAuto FROM TiposEspeciesAutoanalizador"
      sql = sql & " WHERE cautoanalizador =? and (pr24codtimuestr) IN"
      sql = sql & " (SELECT pr49codtipmuestr FROM PR2400"
      sql = sql & " WHERE pr24codmuestra = ?)"
      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry(0) = objConst.constAUTOBECKMAN_CX3
      qry(1) = .strCodMuestra
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs.EOF Then   'Si no se encuentra el especimen quedar� vac�a su posicion en la matriz
        .strProperty1 = rs("cTipoEspecie")
        'Las orinas solo se hacen una vez
        If rs("ctipoespecie") = "2" Then .strProperty2 = constORINA
      End If
      qry.Close: rs.Close
      'Sexo y Edad
      sql = "Select CI30CODSEXO, CI22FECNACIM From CI2200 Where"
      sql = sql & " CI21CODPERSONA in (Select CI21codpersona from"
      sql = sql & " PR0400 where PR04NUMACTPLAN = ?"
      Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
      qry(0) = CLng(.ColMuestrasPruebas(1).strnumact)
      Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
      If Not rs.EOF Then
        If rs("CI22FECNACIM") = Null Then
          .strBandeja = "1"
        Else
          .strBandeja = CStr(DateDiff("yyyy", rs("CI22FECNACIM"), Now))
        End If
        .strCopa = IIf(rs("CI30CODSEXO") = 1, "M", "F") 'Hombre o Mujer
      End If
      qry.Close: rs.Close
      For Each objActuacion In .ColMuestrasPruebas
        With objActuacion
          For Each objResultado In .ColPruebasResultados
            sql = "SELECT referenciaMin, referenciaMax FROM pruebasValidacion"
            sql = sql & " WHERE PR01CODACTUACION = ?  AND cResultado = ?"
            sql = sql & " AND (sexo = ? OR sexo IS NULL)"
            sql = sql & " AND (EdadInferior IS NULL OR EdadInferior <= ?)"
            sql = sql & " AND (EdadSuperior IS NULL OR EdadSuperior >= ?)"
            sql = sql & " AND (referenciaMin IS NOT NULL OR referenciaMax IS NOT NULL)"
            Set qry = objAutoAn.rdoConnect.CreateQuery("", sql)
            qry(0) = .strCodAct
            qry(1) = objResultado.codResultado
            qry(2) = objMuestra.strCopa
            qry(3) = CInt(objMuestra.strBandeja)
            Set rs = qry.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
            If Not rs.EOF Then
              With objResultado
                .strProperty1 = IIf(rs("referenciaMin") = "", "0", rs("referenciaMin"))
                .strProperty2 = IIf(rs("referenciaMax") = "", "0", rs("referenciaMax"))
              End With
            End If
            qry.Close: rs.Close
          Next
        End With
      Next
    End With
  Next
  
Error:

End Sub
