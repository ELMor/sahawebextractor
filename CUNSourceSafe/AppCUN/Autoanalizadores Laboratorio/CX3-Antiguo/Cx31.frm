VERSION 5.00
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Beckman CX3"
   ClientHeight    =   420
   ClientLeft      =   6075
   ClientTop       =   675
   ClientWidth     =   1275
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Cx31.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   420
   ScaleWidth      =   1275
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   15000
      Left            =   840
      Top             =   0
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   420
      Top             =   0
   End
   Begin VB.Timer Timer3 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   0
      Top             =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
'Variables
Dim posComienzo As Integer       'Guarda la posici�n de comienzo del carrusel antes de ser cambiada
Dim UltimaPosicion As Integer    'Guarda la�ltima posici�n ocupada del carrusel
Dim PROGRAMPendiente As Integer    'Indica si existe alguna prueba que deb�a haber sido programada
                                 'pero no lo ha sido por haber solicitado l�nea el autoanalizador
                                 'al mismo tiempo
Dim BORRADOPendiente As Integer  'Indica si existe alguna muestra que todav�a est� pendiente de borrar
                                 'por haber un numero de muestras a borrar mayor que 7
Dim RepetirPendiente As Integer 'vble que indica que se tienen que hacer repeticiones que se
                                'mandaran tras recibir el ultimo mensaje 70317 de resultados
Dim RepetirPruebas As Integer    'Indica si las pruebas se van a repetir o no
'Estas 2 variables se definen como globales por manejarse en varias subrutinas y tener la necesidad de ser est�ticas
'dentro del procedimiento que las utiliza. (Lo mismo ocurre con la variable Mensaje en los procedimientos de Lectura
'de Protocolos pero para ella se prefiere realizar una copia ya que existen otras variables locales tambi�n llamadas
'Mensaje que si no se cambian de nombre provocar�an errores).
Dim Respuesta As TipoRespuesta  'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
Dim RespuestaAfirmativa As Boolean 'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional
'(0 - <ACK>; 1 - <ETX>)
Dim TipoMensEnviado As TipoMensajeEnviado   'Indica el tipo de mensaje que el sistema transmite al autoanalizador y que puede ser:
'   1 - Funci�n 01 del stream 301: programaci�n de pruebas en autoanalizador
'   3 - Funci�n 03 del stream 301: borrado de pruebas programadas para el actual carrusel
Dim UltimoEnvio As Integer 'True: si no hay m�s env�os de prueba a programar
                            'False: todav�a quedan pruebas a programar
Dim RESPUESTAEsperada As Boolean 'True: si se est� esperando un mensaje de respuesta por parte del autoanalizador. Esto
'ocurrira cuando el sistema haya enviado un mensaje que requiere respuesta
                                    'False: no se espera ning�n mensaje de respuesta. El autoanalizador puede en esta
'situaci�n enviar resultados u otros mensajes no solicitados
Dim FuncionRespEsperada As TipoMensajeEnviado   'Indica el tipo de mensaje que se est� esperando como respuesta
'   2 - Funci�n 02 del stream 301: admisi�n de la programaci�n de pruebas
'   4 - Funci�n 04 del stream 301: admisi�n del borrado de todas las pruebas programadas para el carrusel
Dim Salida As Integer 'nos saldremos del programa cuando este esta vble true habiendo recibido el ultimo mensaje
                      ' que ser� de tipo 17
Dim NotPrim As Integer
'Constantes de resultados de c�lculos especiales
Const constANION_GAP = "ANION GAP (1)"
Const constUREA_CREATININA = "COC. UREA/CREA"
Const constOSMOLARIDAD = "OSMOLALIDAD (1)"

'Constantes de urgencias/rutina
Const constURGENCIA = 1
Const constRUTINA = 0
Enum TipoMensajeEnviado
  ctePROGRAMACION = 1
  cteBORRADO = 3
End Enum
Enum TipoRespuesta
  cteACK = 1
  cteETX = 2
  cteNAK = 3
End Enum

Private Sub cmdClearTray_Click()
    objAutoAn.EscribirLog "cmdClearTray_click"
    'Se borra las pruebas programadas la vez anterior
    TipoMensEnviado = cteBORRADO
    Envio_Mensajes
End Sub

Private Function Comprobar_Check(Mensaje As String) As Integer
'Funci�n que comprueba el check del bloque de datos.
'   Si es correcto, se devuelve True
'   Si es incorrecto, se devuelve False
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos en la comprobaci�n del check
Dim suma As Integer         'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal
On Error GoTo AutoAnError

  suma = 0
  For contador = 1 To Len(Mensaje) - 4
    suma = suma + Asc(Mid$(Mensaje, contador, 1))
    If suma >= 256 Then suma = suma - 256
  Next contador
  suma = 256 - suma
  Comprobar_Check = (Hex$(suma) = Mid$(Mensaje, Len(Mensaje) - 3, 2) Or "0" & Hex$(suma) = Mid$(Mensaje, Len(Mensaje) - 3, 2))
  Exit Function
  
AutoAnError:
    Call objError.InternalError(Me, "Comprobar_Check", "plistaTrabajo")
End Function

Private Sub CX3_Confirmacion(Mensaje As String, Caracter As String)
Dim contador As Integer
On Error GoTo AutoAnError
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
'Si el mensaje es rechazado habr� que volver a enviarlo.

    'On Error Resume Next
  objAutoAn.EscribirLog "CX3 Confirmacion"
  Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then  'puede ser que el autoanalizador solicite ser master en caso de haber habido algun problema
      .Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
      .Estado = cteESCLAVO
      Mensaje = "" 'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
      Timer3.Enabled = True
      Respuesta = cteACK 'Respuesta enviada al autoanalizador
      RespuestaAfirmativa = True 'Proxima respuesta afirmativa a enviar al autoanalizador
      Exit Sub
    End If
    Select Case Caracter
      Case Chr$(3)  '<ETX> - El autoanalizador ya ha aceptado el bloque y da por finalizada la transmision
        .Enviar Chr$(4) '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        Mensaje = ""
        'cuando dejamos de ser los transmitir esperamos la respuesta a estos mensajes
        RESPUESTAEsperada = True
        Exit Sub
      Case Chr$(6) '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
          'MsgBox "Recibido un ACK en Confirmaci�n", vbExclamation, frmprincipal.Caption
      'siempre responde ETX?
      '    Envio_Mensajes 'Se enviar� un <ENQ> para que el autoanalizador vuelva a mandar la respuesta
      '    mensaje = ""
        Exit Sub
      Case Chr$(21)   '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviarlo
        If TipoMensEnviado = ctePROGRAMACION Then
          Dim objMuestra As clsMuestra
          For Each objMuestra In objAutoAn.ColMuestras
            If objMuestra.strEstado = cteINTENTOPROGRAMAR Then
              objMuestra.strEstado = cteSINPROGRAMAR
              UltimoEnvio = False
              Exit For
            End If
          Next
        End If
        .Estado = cteMASTER
        Mensaje = ""
        Timer2.Enabled = True 'Se espera 1 segundo para retransmitir el bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Confirmacion", "plistaTrabajo")
End Sub

Private Sub CX3_Esclavo(Mensaje As String, Caracter As String)
'Identifica el final de un bloque y comprueba su check pasando a su lectura
'si fuera correcto. Si el bloque ha llegado incorrectamente se contesta
'negativamente para que el autoanalizador retransmita. Tambi�n se identifica
'el final de la comunicaci�n y se vuelve al estado de espera.
Dim contador As Integer
Dim sql As String
On Error GoTo AutoAnError

  objAutoAn.EscribirLog "CX3 Esclavo"
  Timer3.Enabled = False 'Al llegar datos del autoanalizador, se desactiva el Timer
  Select Case True
    Case Caracter = Chr$(4) '<EOT> - Final de la transmisi�n por parte del autoanalizador
      objAutoAn.Estado = cteESPERA
      Mensaje = ""
      Select Case True
        Case Salida
          Salir
        Case BORRADOPendiente
          'cuando el numero de muestras a borrar supera 7 hay que mandar desde aqui sucesivos mensajes de borrado
          TipoMensEnviado = cteBORRADO
          Envio_Mensajes
        Case PROGRAMPendiente
          PROGRAMPendiente = False
          UltimoEnvio = False
          TipoMensEnviado = ctePROGRAMACION
          Envio_Mensajes
        Case RepetirPruebas
          'lblRepeticion.Visible = True
          objAutoAn.LabelVisible True
          RepetirPendiente = False
          'se forma una nueva matrizgrid que contiene las repeticiones y ademas
          'Se borran las pruebas programadas la vez anterior para que al darle al start puedan reprogramarlas
          Repetir_Pruebas
          RepetirPruebas = False
        Case RESPUESTAEsperada
          'cuando recibimos la finalizacion de las respuestas a nuestros mensajes se dejan de esperar estas
          RESPUESTAEsperada = False
          If TipoMensEnviado = ctePROGRAMACION Then
            If Not UltimoEnvio Then
              'aqui deberiamos seguir con la programacion automatica de las muestras de matrizprog solicitadas
              'con el mensaje 6
              'cmdProgramar_Click 'Se env�a una nueva prueba a programar
              Envio_Mensajes
            Else
              Dim objMuestra As clsMuestra
              For Each objMuestra In ColMuestras
                If objMuestra.strEstado <> cteNOACEPTADA Then
                  Exit For
                End If
              Next
              objAutoAn.blnExistMuestraConEstado
              For contador = 1 To UBound(MatrizProg, 2)
                If MatrizProg(2, contador) <> "NA" Then
                  Exit For
                End If
              Next contador
              If contador > UBound(MatrizProg, 2) Then
                'No se ha admitido la programaci�n de ninguna de las pruebas
                sql = "El autoanalizador " & frmPrincipal.Caption & " no ha aceptado la "
                sql = sql & "la programaci�n de ninguna prueba." & Chr$(10) & "Volver a "
                sql = sql & "ejecutar el programa para intentarlo de nuevo."
                MsgBox sql, vbExclamation, frmPrincipal.Caption
'                Form_Unload (False)
                Descargar
              End If
            End If
          End If
      End Select
    Case Caracter = Chr$(5) '<ENQ> - El autoanalizador solicita respuesta
    'a un bloque mandado tras haber ocurrido un T.O. No es normal que ocurra
    'un T.O. en el autoanalizador ya que la la respuesta al bloque se env�a
    'una vez se ha comprobado el check
      Select Case Respuesta
        Case cteACK: objAutoAn.Enviar Chr$(6) '<ACK>
        Case cteETX: objAutoAn.Enviar Chr$(3) '<ETX>
        Case cteNAK: objAutoAn.Enviar Chr$(21) '<NAK>
      End Select
      Mensaje = ""
      Timer3.Enabled = True
    Case Right$(Mensaje, 2) = Chr$(13) & Chr$(10)  'Se ha identificado el final del bloque
      objAutoAn.Enviar IIf(RespuestaAfirmativa, Chr$(3), Chr$(6))  '<ACK>/<ETX>
      Respuesta = IIf(RespuestaAfirmativa, cteETX, cteACK) 'Respuesta enviada al autoanalizador
      RespuestaAfirmativa = Not RespuestaAfirmativa
      Lectura_Datos Mensaje 'Se procede a la lectura de datos
      Mensaje = ""
      'para ver cuanto tiempo tarda en enviar el siguiente mensaje
      Timer3.Enabled = True
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Esclavo", "plistaTrabajo")
End Sub

Private Sub CX3_Espera(Mensaje As String, Caracter As String)
'Identifica la llegada de los caracteres de solicitud de comunicaci�n,
'la acepta y pasa al estado de transmisi�n.
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "CX3 espera"
  If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then '<EOT><SOH> - Solicitud de comunicaci�n por parte del autoanalizador
    objAutoAn.Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
    objAutoAn.Estado = cteESCLAVO
    Mensaje = "" 'Se eliminan el <EOT><SOH> y los posibles caracteres enviados antes de establecerse la comunicaci�n
    Timer3.Enabled = True
    Respuesta = cteACK 'Respuesta enviada al autoanalizador
    RespuestaAfirmativa = True 'Proxima respuesta afirmativa a enviar al autoanalizador
    Exit Sub
  End If
  If Caracter = Chr$(5) Then '<ENQ> - El autoanalizador solicita respuesta
  'a un bloque mandado tras haber ocurrido un T.O. No es normal que ocurra
  'un T.O. en el autoanalizador ya que la la respuesta al bloque se env�a
  'una vez se ha comprobado el check
    objAutoAn.Enviar Chr$(4) '<EOT> - El sistema da por finalizada la comunicaci�n
    Mensaje = ""
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Espera", "plistaTrabajo")
End Sub

Private Sub CX3_Master(Mensaje As String, Caracter As String)
Dim contador As Integer
Dim objMuestra As clsMuestra
On Error GoTo AutoAnError
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
'Si el mensaje es rechazado habr� que volver a enviarlo.
'On Error Resume Next
  objAutoAn.EscribirLog "Synchon Master"
  Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
  With objAutoAn
    Select Case Caracter
      Case Chr$(3) '<ETX> - El autoanalizador ya ha aceptado el bloque y da por finalizada la transmision
        .Enviar Chr$(4) '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        .Estado = cteESPERA
        Mensaje = ""
        'cuando recibimos la aceptacion y dejamos de transmitir esperamos la respuesta a estos mensajes
        RESPUESTAEsperada = True
      Case Chr$(6) '<ACK> - El autoanalizador env�a un incorrecto reconocimiento positivo
        'MsgBox "Recibido un ACK en Master", vbExclamation, frmprincipal.Caption
        'siempre responde ETX?
        .Estado = cteCONFIRMACION
        Envio_Mensajes 'Se enviar� un <ENQ> para que el autoanalizador vuelva a mandar la respuesta
        Mensaje = ""
      Case Chr$(21) '<NAK> - El autoanalizador no ha aceptado el bloque y se vuelve a enviar
        If TipoMensEnviado = ctePROGRAMACION Then
        'Se actualiza la matriz que controla el grid
          For Each objMuestra In .ColMuestras 'ColProgramMuestras
            With objMuestra
              If .strEstado = cteINTENTOPROGRAMAR Then
                .strEstado = cteSINPROGRAMAR
                UltimoEnvio = False
                Exit For
              End If
            End With
          Next
'              For contador = 1 To UBound(MatrizProg, 2)
'                  If MatrizProg(2, contador) = "IP" Then
'                      MatrizProg(2, contador) = "SP"
'                      UltimoEnvio = False
'                      Call pActuGrid(MatrizProg(1, contador), contador)
'                      Exit For
'                  End If
'              Next contador
        End If
        Mensaje = ""
        Timer2.Enabled = True 'Se espera 1 segundo para retransmitir el ultimo bloque
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Master", "plistaTrabajo")
End Sub

Private Sub CX3_Solicitud(Mensaje As String, Caracter As String)
'Tras haber realizado el sistema una solicitud para establecer una comunicaci�n, el autoanalizador responde aceptando o
'negando dicha comunicaci�n.
'On Error Resume Next
On Error GoTo AutoAnError
    objAutoAn.EscribirLog "CX3 Solicitud"
    Timer1.Enabled = False 'Al producirse respuesta del autoanalizador, se desactiva el Timer
    With objAutoAn
      Select Case Caracter
        Case Chr$(6)  '<ACK> - El autoanalizador ha aceptado la comunicaci�n
          .Estado = cteMASTER
          Mensaje = ""
          Envio_Mensajes 'Se env�a el bloque de datos por parte del sistema al autoanalizador '!!MODIFICAR
          Exit Sub
        Case Chr$(21), Chr$(3) '<NAK> o <ETX> - El autoanalizador no ha aceptado la comunicaci�n
          .Estado = cteESPERA
          Mensaje = ""
          Timer2.Enabled = True 'Se espera 1 segundo para intentar una nueva comunicaci�n
          Exit Sub
      End Select
      If Right$(Mensaje, 2) = Chr$(4) & Chr$(1) Then '<EOT><SOH> - El autoanalizador ha "solicitado l�nea" a la vez que el sistema
        If TipoMensEnviado = ctePROGRAMACION Then PROGRAMPendiente = True
        .Enviar Chr$(6) '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
        .Estado = cteESCLAVO
        Mensaje = ""
        Timer3.Enabled = True
      End If
    End With
    Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "CX3_Solicitud", "plistaTrabajo")
End Sub

Private Sub Envio_Mensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar los caracteres <EOT><SOH> y esperar
'a que el autoanalizador responda afirmativamente. Luego se manda el mensaje propiamente dicho hasta que sea recibido
'correctamente por el autoanalizador. Finalmente se cierra la comunicaci�n enviando <EOT> para dar paso a la respuesta del
'autoanalizador al requerimiento del sistema.
Static ContIntentosComunic As Integer 'cuenta el n� de veces que se intenta comunicar con el autoanalizador
Static ContIntentosTrans As Integer 'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
Dim T As Long
Dim RES As Integer
Dim sql As String
Dim contador As Integer
On Error GoTo AutoAnError
  'On Error Resume Next
  objAutoAn.EscribirLog "Envio Mensajes"
  With objAutoAn
    Select Case .Estado
      Case cteESPERA
        ContIntentosTrans = 0 'Se inicializa el ContIntentosTrans por si se realiza una nueva comunicaci�n
        ContIntentosComunic = ContIntentosComunic + 1
        If ContIntentosComunic = 8 Then 'Fallo en la comunicaci�n despu�s de 7 intentos
          ContIntentosComunic = 1
          sql = "No se puede establecer comunicaci�n."
          Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
          Case vbRetry
            T = Timer + 5
            Do While T > Timer: RES = DoEvents(): Loop
            Sistema_Espera
            .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
            Exit Sub
          Case vbCancel
            'Form_Unload (False)
          End Select
        Else
          'solo cuando quede alguna prueba por programar, repetir o borrar
          'If PROGRAMPendiente = True Or borradopendiente = True Or repetirpruebas = True Then
          If Not UltimoEnvio Or RepetirPruebas Or BORRADOPendiente Then
            Sistema_Espera
            .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
            Exit Sub
          End If
          'End If
        End If
      Case cteMASTER
        ContIntentosComunic = 0 'Se inicializa el ContIntentosComunic por si se realiza una nueva comunicaci�n
        ContIntentosTrans = ContIntentosTrans + 1
        If ContIntentosTrans = 8 Then 'Fallo en la transmisi�n despu�s de 7 intentos
          ContIntentosTrans = 0
          objAutoAn.Estado = cteESPERA
          sql = "No se puede transmitir informaci�n."
          Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
          Case vbRetry
            T = Timer + 5
            Do While T > Timer: RES = DoEvents(): Loop
            Sistema_Espera
            ContIntentosComunic = 1
            .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
            Exit Sub
          Case vbCancel
            'Form_Unload (False)
          End Select
        Else
          Sistema_Master
          .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosTrans
          Exit Sub
        End If
    
      Case cteCONFIRMACION
        ContIntentosTrans = ContIntentosTrans + 1
        If ContIntentosTrans = 8 Then 'Fallo en la transmisi�n despu�s de 7 intentos
          ContIntentosTrans = 0
          .Estado = cteESPERA
          sql = "No se puede transmitir informaci�n."
          Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
            Case vbRetry
            Dim objMuestra As clsMuestra
            For Each objMuestra In objAutoAn.ColMuestras
              If objMuestra.strEstado = cteINTENTOPROGRAMAR Then _
              objMuestra.strEstado = cteSINPROGRAMAR: Exit For
            Next
'              For contador = 1 To UBound(MatrizGrid, 2)
'                If MatrizGrid(2, contador) = "IP" Then
'                  MatrizGrid(2, contador) = "SP"
'                  Exit For
'                End If
'              Next contador
              'Se esperan 20 seg. para volver a iniciar la comunicacion
              T = Timer + 20
              Do While T > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              ContIntentosComunic = 1
              .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosComunic
              Exit Sub
            Case vbCancel
              'Form_Unload (False)
          End Select
        Else
          Sistema_Confirmacion
          .PanelEstado .fLeerEstado(.Estado) & " " & ContIntentosTrans
          Exit Sub
        End If
    End Select
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Envio_Mensajes", "plistaTrabajo")
End Sub

Private Sub Establecer_Check(Mensaje As String)
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
Dim suma As Integer         'suma de los c�digoas ASCII, m�dulo 256, de los caracteres del bloque en decimal
'On Error Resume Next
On Error GoTo AutoAnError

    suma = 0
    For contador = 1 To Len(Mensaje)
      suma = suma + Asc(Mid$(Mensaje, contador, 1))
      If suma >= 256 Then
        suma = suma - 256
      End If
    Next contador
    suma = 256 - suma
    If Len(Hex$(suma)) = 1 Then
      Mensaje = Mensaje & "0" & Hex$(suma)
    Else
      Mensaje = Mensaje & Hex$(suma)
    End If
    Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Establecer_Check", "plistaTrabajo")
End Sub

Private Sub Form_Load()
On Error GoTo AutoAnError
    'Se inicializan las variables
    RESPUESTAEsperada = False
    RepetirPruebas = False
    RepetirPendiente = False
    PROGRAMPendiente = False
    Salida = False
'    ReDim MatrizMuestraDoble(0 To 0)
'    ReDim MatrizProg(1 To 7, 1 To 1)
Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Cargando Lista de Trabajo", "plistaTrabajo")
End Sub

Private Sub Lectura_Datos(datos As String)
'Los datos que llegan pueden ser esperados
'(respuesta a un mensaje enviado anteriormente) o
'expont�neos (resultados enviados por el autoanalizador.
Dim cresultadoauto As String    'C�digo del resultado definido en la tabla resultadosAutoanalizador
Dim tipoEspecie As String
Dim RES As String
Dim i As Integer, j As Integer, n As Integer, x%
Dim filenum
Dim resul As Double
Dim objMuestra As clsMuestra
On Error GoTo AutoAnError
'On Error Resume Next
  objAutoAn.EscribirLog "Lectura Datos"
  If RESPUESTAEsperada Then
  'El mensaje es esperado.
    Select Case FuncionRespEsperada
      Case ctePROGRAMACION: Stream701_Funcion02 datos 'Respuesta a un intento de programar pruebas para una muestra.
      Case cteBORRADO:  Stream701_Funcion04 datos 'Respuesta a un intento de borrado de las pruebas programadas.
    End Select
  Else
  'El mensaje es de resultados.
  'Identifica de que tipo de record se trata y lee sus datos seg�n lo expresado en el manual.
    Select Case Mid$(datos, 5, 3) & Mid$(datos, 9, 2)
      Case "70106"
        'el CX3 pide la programacion de las muestras leidas
        'por el CB y estas se van programando
        PROGRAMPendiente = True
        For i = 12 To 84 Step 12
          If Mid$(datos, i, 11) <> Space$(11) Then
            For Each objMuestra In objAutoAn.ColMuestras
              With objMuestra
                If .strIDMuestra = Trim$(Mid$(datos, i, 10)) And .strEstado = cteSINPROGRAMAR Then
                  .strEstado = cteLEIDA
                  Exit For
                End If
              End With
            Next
          End If
        Next i
        If Not objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR) Then
          UltimoEnvio = True
          PROGRAMPendiente = False
        End If
      Case "70201"
        'Se inicializa la matriz que contendr� los resultados. Esta matriz almacenar� en (1,1) la ID de la muestra, en
        '(2,1) la fecha y la hora separadas por una coma, en (1,X) el cResultadoAuto de la prueba y en (2,X) el resultado.
'            ReDim MatrizResultados(1 To 2, 1 To 1)
        Dim IdMuestra As String
        'Se obtiene la ID de la muestra y la fecha y hora
        'MatrizResultados(1, 1) = Trim$(Mid$(datos, 25, 9))
'            MatrizResultados(1, 1) = Trim$(Mid$(datos, 57, 11)) 'codigo num�rico de la muestra
        IdMuestra = Trim$(Mid$(datos, 57, 11)) 'codigo num�rico de la muestra
        With objAutoAn
          IdMuestra = .fNumeroACodigo(IdMuestra)
          If .blnExistMuestra(IdMuestra) Then
            .ColMuestras(IdMuestra).strEstado = cteRECIBIENDORESULTADOS
          End If
        End With
      Case "70203"
        'Se Introducen Resultados
        RES = CStr(Trim$(Mid$(datos, 82, 9)))
        i = objAutoAn.fFormatearNumero(RES, ".", ",")
        If Left$(RES, 1) = "," Then RES = "0" & RES
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteRECIBIENDORESULTADOS Then
              Call pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(datos, 60, 4)), RES)
            End If
          End With
        Next
      Case "70211"
        'Se almacenan los resultados especiales en la matriz
'            ReDim Preserve MatrizResultados(1 To 2, 1 To UBound(MatrizResultados, 2) + 1)
        'cResultadoAuto = Trim$(Mid$(datos, 22, 2))
'            cresultadoauto = Trim$(Mid$(datos, 53, 13))
'            txtrecibir = txtrecibir & "result.especial:" & cresultadoauto & Chr$(13) & Chr$(10)

        'Select Case cresultadoauto 'NOTA: PONER COMO CONSTANTES
        'Case 0, 1
        '    cresultadoauto = constANION_GAP
        'Case 2, 3
        '    cresultadoauto = constUREA_CREATININA
        'Case 4
        '    cresultadoauto = constOSMOLARIDAD
        'End Select
'            MatrizResultados(1, UBound(MatrizResultados, 2)) = cresultadoauto
        RES = CStr(Trim$(Mid$(datos, 77, 9)))
        i = objAutoAn.fFormatearNumero(RES, ".", ",")
        If Left$(RES, 1) = "," Then RES = "0" & RES
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteRECIBIENDORESULTADOS Then
              Call pIntroResultConCResAuto(.strCodMuestra, Trim$(Mid$(datos, 61, 20)), RES)
            End If
          End With
        Next
      Case "70205"
        'se recibe el indicativo de fin de transmision de todas las pruebas de esa muestra
        'Se procede a actualizar la base de datos
        'Actualizar_BD
      Case "70317"
        'Se recibe la notificaci�n de que ya se han recibido todos los resultados
        'lblRepeticion.Visible = False 'una vez recibidas todas las repeticiones se quita el aviso
        objAutoAn.LabelVisible False
        'desde aqui es desde donde se activa la variable que mandar� repetir las pruebas al recibir EOT (CX_esclavo)
        RepetirPruebas = RepetirPendiente
        If Not RepetirPruebas Then Salir
    End Select
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos", "plistaTrabajo")
End Sub

Private Sub pIntroResultConCResAuto(CodMuestra As String, codResAuto As Integer, Modificacion As String)
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim objResultado As clsResultado
Dim objColumna As clsCabeceraGrid
Dim objResultadoAuto As clsResultAuto
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "pIntroResultConCResAuto"
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      If .strCodMuestra = CodMuestra Then
        For Each objActuacion In .ColMuestrasPruebas
          For Each objResultadoAuto In objAutoAn.ColPruebaAuto(objActuacion.strCodAct).ColResultAuto
            With objResultadoAuto
              If .strCodResultAuto = codResAuto Then
                With objMuestra.ColMuestrasPruebas(objActuacion.strCodAct).ColPruebasResultados(CStr(.intCodResult))
                  .strResultado = Modificacion
                  .strEstado = cteREALIZADA
                  If Not objMuestra.blnRepetida And _
                  Not (objMuestra.strProperty2 = constORINA) Then
                    If CDbl(Modificacion) < CDbl(.strProperty1) Or _
                    CDbl(Modificacion) > CDbl(.strProperty2) Then
                      With objActuacion
                        .intNumRepet = .intNumRepet + 1
                        .strEstado = cteAREPETIR
                      End With
                      objMuestra.strEstado = cteAREPETIR
                      RepetirPendiente = True
                    Else
                      objMuestra.strEstado = cteREALIZADA
                      objActuacion.strEstado = cteREALIZADA
                    End If
                  Else
                    objMuestra.strEstado = cteREALIZADA
                    objActuacion.strEstado = cteREALIZADA
                  End If
                End With
                Exit Sub
              End If
            End With
          Next
        Next
      End If
    End With
  Next
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "pIntroResultConCResAuto", "plistaTrabajo") 'cwMsgWrite & strFileName)
End Sub

Function blnExistInColProgMuestras(CodMuestra As String) As Boolean
On Error Resume Next

  blnExistInColProgMuestras = objAutoAn.ColMuestras(CodMuestra).strmuestra
  blnExistInColProgMuestras = (Err = 0)
End Function

Private Sub Lectura_Protocolo(Caracter As String)
Static Mensaje As String   'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
'por estar declarada como Static en esta subrutina
On Error GoTo AutoAnError
    
    Mensaje = Mensaje & Caracter
    CopiaMensaje = Mensaje
    Select Case objAutoAn.Estado
      Case cteESPERA
        CX3_Espera CopiaMensaje, Caracter
        Mensaje = CopiaMensaje
      Case cteSOLICITUD
        CX3_Solicitud CopiaMensaje, Caracter
        Mensaje = CopiaMensaje
      Case cteESCLAVO
        CX3_Esclavo CopiaMensaje, Caracter
        Mensaje = CopiaMensaje
      Case cteCONFIRMACION
        CX3_Confirmacion CopiaMensaje, Caracter
        Mensaje = CopiaMensaje
      Case cteMASTER
        CX3_Master CopiaMensaje, Caracter
        Mensaje = CopiaMensaje
    End Select
    Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Protocolo", "plistaTrabajo")
End Sub

Private Sub Repetir_Pruebas()
'Actualiza las matrices y se�ala el comienzo de la programaci�n de las repeticiones
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
Dim contador As Integer
Dim nRegistros As Integer
On Error GoTo AutoAnError

  objAutoAn.EscribirLog "Repetir_Pruebas"
  For Each objMuestra In objAutoAn.ColMuestras
    If objMuestra.strEstado = cteAREPETIR Then
      For Each objActuacion In objMuestra.ColMuestrasPruebas
        If objActuacion.strEstado <> cteAREPETIR Then
          objMuestra.ColMuestrasPruebas.Remove objActuacion
        End If
      Next
    Else
      objAutoAn.ColMuestras.Remove objMuestra
    End If
  Next
  'Se borran las pruebas programadas la vez anterior para que al darle al start pueda reprogramarlas
  TipoMensEnviado = cteBORRADO
  Envio_Mensajes
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Repetir_Pruebas", "plistaTrabajo")
End Sub

Private Sub Sistema_Confirmacion()
'Se solicita que el autoanalizador repita o env�e la respuesta a un
'mensaje enviado anteriormente
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Sistema_Confirmacion"
  objAutoAn.Enviar Chr$(5) 'Env�o de <ENQ>, se pide la respuesta al mensaje enviado
  Timer1.Enabled = True 'Empieza a contar el tiempo para el TimeOut
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Confirmacion", "plistaTrabajo")
End Sub

Private Sub Sistema_Espera()
'Se env�a los caracteres de solicitud de comunicaci�n y se pasa a estado de Solicitud
On Error GoTo AutoAnError
  With objAutoAn
    .EscribirLog "Sistema_Espera"
    .Enviar Chr$(4) & Chr$(1) 'Env�o de <EOT><SOH>, se "solicita l�nea"
    .Estado = cteSOLICITUD
  End With
  Timer1.Enabled = True 'Empieza a contar el tiempo para el TimeOut
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Espera", "plistaTrabajo")
End Sub

Private Sub Sistema_Master()
Dim Mensaje As String 'Contiene un string correspondiente al mensaje a enviar
Dim filenum
On Error GoTo AutoAnError
'Se transmiten datos al autoanalizador. Los datos a enviar pueden ser de
'dos tipos: -programaci�n de pruebas o -borrado de pruebas programadas.
  objAutoAn.EscribirLog "Sistema_Master"
  Select Case TipoMensEnviado
    Case ctePROGRAMACION: Stream701_Funcion01 Mensaje ' programaci�n de pruebas
    Case cteBORRADO: Stream701_Funcion03 Mensaje 'borrado de todas las pruebas programadas
  End Select
  'Se env�a el mensaje
  If Mensaje <> "" Then
    objAutoAn.Enviar Mensaje & Chr$(13) & Chr$(10)
    Timer1.Enabled = True 'Empieza a contar el tiempo para el TimeOut
  Else
    objAutoAn.Estado = cteESPERA
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Master", "plistaTrabajo")
End Sub

Private Sub Stream701_Funcion01(Mensaje As String)
'Se forma un mensaje que responde al formato del stream 301, funci�n 01 (programaci�n de pruebas) y se establecen otras
'caracter�sticas relacionadas con la respuesta esperada y futuros env�os
Dim cPrueba As String       'c�digo de la prueba/c�digo de la prueba seg�n codificaci�n del CX3
Dim pruebas As String       'c�digos de las pruebas acumulados en un mismo string seg�n codificaci�n del CX3
Dim cEspecimen As String    'c�digo de la especie/c�digo de la especie seg�n codificaci�n del CX3
Dim contador1 As Integer
Dim contador2 As Integer
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
'On Error Resume Next
On Error GoTo AutoAnError
    
    'For contador1 = 1 To UBound(MatrizProg, 2)
  objAutoAn.EscribirLog "Stream701_Funcion01"
  For Each objMuestra In objAutoAn.ColMuestras 'ColProgramMuestras
    With objMuestra
      If .strEstado = cteLEIDA Then
        'Se comienza a formar el mensaje
        Mensaje = "[00,701,01,"
        'el ultimo 1 es para poder programar solo nuevas pruebas a una muestra que ya exista y si no programa una nueva
        'el caso 0 hace reemplazar todo lo que exista
        Mensaje = Mensaje & "00," & "00," & "1,"
        'Se se�ala si es urgencia o no
        Mensaje = Mensaje & IIf(.blnUrgente, "ST", "RO")
        Mensaje = Mensaje & .strProperty1 & "," & Trim$(.strCodMuestra) & " "
            Mensaje = Mensaje & "," & Space$(20) & "," & Space$(25) & "," & Space$(25)
            Mensaje = Mensaje & "," & Space(18) & "," & Space$(15) & ", ," & Space$(12)
            Mensaje = Mensaje & "," & Space$(18) & "," & Format$(Now, "ddmmyy") & "," & Format$(Now, "hhmm")
            Mensaje = Mensaje & "," & Space$(20) & "," & Space$(3) & "," & Space$(1) & "," & Space$(6)
            Mensaje = Mensaje & "," & Space(1) & "," & Space$(25) & "," & Space$(7)
            Mensaje = Mensaje & "," & Space$(4) & "," & Space$(4) & "," & Space$(6)
        'aqui tendre que distinguir cuando realizo la programacion de todas las
        'muestras o de solo las de repeticion porque en el 1er caso me cojo
        'todas y en el 2� solo las seleccionadas (tb puedo ver si hay alguna
        'fila seleccionada y si hay me quedo con las seleccionadas y sino con todas)
        '.nada de esto es cierto pues solo programo las que me pregunta
        'y estan en matrizprog
        'Se buscan y se codifican todas la pruebas que hay que realizar a la muestra. Se sigue completando el mensaje.
        If .blnRepetida Then
          For Each objActuacion In .ColMuestrasPruebas
            With objActuacion
              If .intRepeticion = 1 Then
                pruebas = pruebas & objAutoAn.ColPruebaAuto(.strCodAct).strCodActAuto & "0,"
              End If
            End With
          Next
        Else
          For Each objActuacion In .ColMuestrasPruebas
            pruebas = pruebas & objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto & "0,"
          Next
        End If
        'hay que tener en cuenta que pruebas lleva la coma final
        Mensaje = Mensaje & "," & Right$("000", 3 - Len(CStr(Len(pruebas) / 7))) _
         & CInt(Len(pruebas) / 7) & "," & pruebas
        Mensaje = Left$(Mensaje, Len(Mensaje) - 1) & "]"
        'Se establece el check para el mensaje
        Establecer_Check Mensaje
        'Se coloca el estado "IP" para la muestra.
        .strEstado = cteINTENTOPROGRAMAR
        Exit For
      End If
    End With
  Next 'contador1
  'para que no intente mandar mensaje estando ya todo programado(mandaria "" & CRLF) y CX� rsponder�a NAK
  If Not objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR) Then
    PROGRAMPendiente = False: UltimoEnvio = True ': txtrecibir = txtrecibir & "ultimoenvio(stream701)=true" & Chr$(13) & Chr$(10) Else PROGRAMPendiente = True: UltimoEnvio = False: txtrecibir = txtrecibir & "ultimoenvio(stream701)=false" & Chr$(13) & Chr$(10)
  End If
  'Se establecen el tipo de respuesta esperado par parte del CX3 al mensaje que se le va a enviar.
  FuncionRespEsperada = ctePROGRAMACION
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion01", "plistaTrabajo")
End Sub

Private Sub Stream701_Funcion02(datos As String)
'Se lee la respuesta al intento de programaci�n de las pruebas de una muestra.
Dim sql As String
Dim resp As Integer
Dim CodeError As Integer
Dim CodeMuestra As String
Dim objMuestra As clsMuestra
'On Error Resume Next
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Stream701_funcion2"
  If Mid$(datos, 5, 3) = "801" And Mid$(datos, 9, 2) = "02" Then
    CodeError = Val(Mid$(datos, 12, 2))
    CodeMuestra = Trim$(Mid$(datos, 29, 15))
    For Each objMuestra In objAutoAn.ColMuestras
      With objMuestra
        If .strEstado = cteINTENTOPROGRAMAR And CodeMuestra = .strCodMuestra Then
          Select Case CodeError
            Case 0: .strEstado = cteACEPTADA
            Case Else
              sql = "La muestra " & .strCodMuestra & " no puede ser programada." & Chr$(10)
              Select Case CodeError
                Case 1: sql = sql & "Mensaje de programaci�n de pruebas erroneo. Error de sintaxis"
                Case 2: sql = sql & "La pantalla de programaci�n de pruebas est� activa"
                Case 3: sql = sql & "C�digo de prueba no identificado en el CX3"
                Case 4: sql = sql & "Petici�n ORDAC invalida"
                Case 5: sql = sql & "Combinaci�n de pruebas no valido (Se requiere una diluci�n o volumen de muestra excesivo)."
                Case 6: sql = sql & "Control no configurado"
                Case 7: sql = sql & "Solo sector de calibrador"
                Case 8: sql = sql & "Error de modo (los modos Codigo de barras y Sector no son los mismos para el CX3 y Ordenador"
                Case 9: sql = sql & "Error Interno en el CX3"
                Case 10: sql = sql & "Todos los sectores ya estan programados .No se pueden a�adir mas muestras, solo modificarlas"
                Case 11: sql = sql & "Tipos de fluido incompatibles"
                Case 12: sql = sql & "Tipos de pruebas incompatibles"
                Case 13: sql = sql & "Nombre de paciente incorrecto"
                Case 14: sql = sql & "El C�digo de Muestra Ya Existe en el Autoanalizador"
                Case 15: sql = sql & "El N� deRack debe estar entre 0000 y 9999"
              End Select
              resp = MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
              Select Case resp
                Case vbAbort: Descargar 'Form_Unload (False)
                Case vbRetry: .strEstado = cteSINPROGRAMAR: UltimoEnvio = False
                Case vbIgnore: .strEstado = cteNOACEPTADA
              End Select
          End Select
          Exit For
        End If
      End With
    Next
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion02", "plistaTrabajo")
End Sub

Private Sub Stream701_Funcion03(Mensaje As String)
Dim i%, ListaMuestrasABorrar$, x%, contador%
Dim objMuestra As clsMuestra
Dim NumMuestrasABorrar As Integer 'lleva el control del n� de
'muestras a borrar pues no podemos eliminar mas de 7 en un solo mensaje
Static ultmuestraborrada As Integer 'lleva la posicion de la ultima
'muestra borrada para poder formar a partir de ella el siguiente mensaje
On Error GoTo AutoAnError
'Se forma un mensaje que responde al formato del stream 701, funci�n 03 y se establecen otras caracter�sticas relacionadas
'con la respuesta esperada y futuros env�os
  objAutoAn.EscribirLog "Stream701_Funcion03"
  ListaMuestrasABorrar = ""
  'txtrecibir = txtrecibir & "Subrutina Stream 701 Borrado pruebas" & Chr$(13) & Chr$(10)
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      If .strEstado = cteAREPETIR Then
        .strEstado = cteINTENTOBORRADO
        NumMuestrasABorrar = NumMuestrasABorrar + 1
        ListaMuestrasABorrar = ListaMuestrasABorrar & .strIDMuestra & Space$(15 - Len(.strIDMuestra)) & ","
        If NumMuestrasABorrar >= 4 Then Exit For
      End If
    End With
  Next
  ultmuestraborrada = contador
  ListaMuestrasABorrar = ListaMuestrasABorrar & Right$("           ,           ,           ,           ,           ,           ,           ,", 12 * (7 - NumMuestrasABorrar))
  ListaMuestrasABorrar = Left$(ListaMuestrasABorrar, Len(ListaMuestrasABorrar) - 1)
  Mensaje = "[00,701,03,00," & ListaMuestrasABorrar & "]"
  Establecer_Check Mensaje
  FuncionRespEsperada = cteBORRADO
  UltimoEnvio = True
  BORRADOPendiente = objAutoAn.blnExistMuestraConEstado(cteAREPETIR)
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion03", "plistaTrabajo")
End Sub

Private Sub Stream701_Funcion04(datos As String)
'Se lee la respuesta al intento de borrado de las pruebas programadas.
Dim sql As String
Dim code As Integer
Dim resp As Integer
Dim num3%, num2%, i%
Static num0%
Dim objMuestra As clsMuestra
'On Error Resume Next
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Stream701_Funcion04"
  If Mid$(datos, 5, 3) = "801" And Mid$(datos, 9, 2) = "04" Then
    code = Val(Mid$(datos, 15, 2))
    If code <> 0 And code <> 4 Then
      sql = "No se pueden eliminar las pruebas programadas." & Chr$(10)
      Select Case code
        Case 1: sql = sql & "Mensaje de borrado de pruebas err�neo."
        Case 3: sql = sql & "Ha ocurrido un error en el CX3"
        Case 2: sql = sql & "El autoanalizador est� realizando an�lisis en este momento."
        Case 5: sql = sql & "El N� de Rack es Demasiado Largo"
      End Select
      resp = MsgBox(sql, vbExclamation + vbRetryCancel, "Borrado de Muestras en CX3")
      Select Case resp
        Case vbCancel: Exit Sub
        Case vbRetry:
          TipoMensEnviado = cteBORRADO
          For Each objMuestra In objAutoAn.ColMuestras
            If objMuestra.strEstado = cteINTENTOBORRADO Then objMuestra.strEstado = cteAREPETIR
          Next
          Envio_Mensajes
      End Select
    Else
      For i = 18 To 36 Step 3
        For Each objMuestra In objAutoAn.ColMuestras
          With objMuestra
            If .strEstado = cteINTENTOBORRADO Then
              Select Case Mid$(datos, i + 1, 1)
                Case "0", "4"
                  num0 = num0 + 1
                  .strEstado = cteBORRADA
                Case "2"
                  num2 = num2 + 1
                  .strEstado = cteNOBORRADA
                Case "3"
                  num3 = num3 + 1
                  .strEstado = cteNOBORRADA
              End Select
            End If
          End With
        Next
      Next i
      If num3 > 0 Then sql = "Ha Ocurrido Un Error en el CX3 ."
      If num2 > 0 Then sql = sql & Chr$(13) & "El Autoanalizador Est� Realizando An�lisis En Este Momento."
      If Not BORRADOPendiente Then
        'en caso de ir todo bien me pondra al final de todo el proceso de borrado el n�muestras borradas
        If num0 > 0 Then sql = sql & Chr$(13) & "Se Van a Repetir " & num0 & " Muestras." & Chr$(13) & Chr$(10) & " 1� -Pulse Aceptar y " & Chr$(13) & Chr$(10) & "2� -Pulse Iniciar Para Comenzar las Repeticiones"
        num0 = 0  'reinicializamos el n� muestras totales borradas
'              ReDim MatrizProg(1 To 7, 1 To 1)
        NotPrim = 0
        If sql <> "" Then
          resp = MsgBox(sql, vbInformation, "Borrado de Muestras en CX3")
        Else
          resp = MsgBox("Por Favor Borre Manualmente en el CX3 las Muestras Con Repeticiones y Pulse Iniciar", vbInformation, "Borrado de Muestras en CX3")
        End If
      End If
    End If
  End If
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Stream701_Funcion04", "plistaTrabajo")
End Sub

Private Sub Timer1_Timer()
'Controla los Time Out que produce el autoanalizador
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Timer1_Timer (TimeOut)"
  Timer1.Enabled = False
  Select Case objAutoAn.Estado
    Case cteSOLICITUD  'Habiendo el sistema "solicitado l�nea", el autoanalizador tarda m�s de 15 segundos en responder
      objAutoAn.Estado = cteESPERA
      RESPUESTAEsperada = False
      Envio_Mensajes 'para realizar un nuevo intento de comunicaci�n
    Case cteMASTER 'Habiendo el sistema enviado datos, el autoanalizador tarda m�s de 15 segundos en responder
      objAutoAn.Estado = cteCONFIRMACION
      Envio_Mensajes 'para realizar un nuevo intento de envio de datos
    Case cteCONFIRMACION 'Habiendo el sistema enviado datos, el autoanalizador tarda m�s de 15 segundos en responder
      objAutoAn.Estado = cteCONFIRMACION
      Envio_Mensajes 'para realizar un nuevo intento de envio de datos
  End Select
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Timer1_Timer", "plistaTrabajo")
End Sub

Private Sub Timer2_Timer()
'Controla los tiempos entre env�os de solicitudes
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Timer2_Timer (Tiempos entre envios)"
  Timer2.Enabled = False
  Envio_Mensajes
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Timer2_Timer", "plistaTrabajo")
End Sub

Private Sub Timer3_Timer()
'Controla el tiempo que transcurre desde los �ltimo datos recibidos por el sistema. Si estando el sistema en el estado de
'esclavo pasan 60 segundos sin recibir datos o el final de la comunicaci�n <EOT> se finaliza la ejecuci�n
Dim sql As String
Dim resp As Integer
On Error GoTo AutoAnError
    objAutoAn.EscribirLog "Timer3 (Tiempo sin Recibir datos)"
    Timer3.Enabled = False
    If objAutoAn.Estado = cteESCLAVO Then
      sql = "Se ha producido un Timeout durante la comunicaci�n por parte del Autoanalizador." _
      & Chr$(10) & "Puede que se haya perdido la comunicaci�n." _
      & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
      resp = MsgBox(sql, vbExclamation + vbYesNo, frmPrincipal.Caption)
      Select Case resp
        Case vbYes
          Timer3.Enabled = True
        Case vbNo
          'Form_Unload (False)
          objAutoAn.Estado = cteESPERA
      End Select
      RESPUESTAEsperada = False
    End If
    Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Timer3_Timer", "plistaTrabajo")
End Sub

Public Function Salir() As Boolean
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Salir"
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion, Me.Caption) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"
  If Not Salir Then Descargar
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Salir", "plistaTrabajo")
End Function

Public Sub Descargar()
On Error GoTo AutoAnError
  objAutoAn.EscribirLog "Descargar"
  End
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "Descargar", "plistaTrabajo")
End Sub
