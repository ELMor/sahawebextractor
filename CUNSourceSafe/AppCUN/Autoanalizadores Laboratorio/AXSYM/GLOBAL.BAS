Option Explicit

'                               ----- (�ltima revisi�n 5-10-95) -----

Global dbCun As Database            ' Base de datos de laboratorio.

'Constantes de acceso a Oracle
Global Const constPropietario = "CUNLab"        ' Propietario de las tablas
Global Const constPwd = "ESII"                  ' Password de las tablas
Global Const constDatabase = "Oracle7"   ' Nombre del driver que se utiliza
Global Const constServerName = "x:cunlabtmp"    ' Nombre del servidor

'Constante de t�tulo del programa general que llama al resto de programas
Global Const constProgGeneral = "Controlador de autoanalizadores del SILAB"

'Constantes de estado del sistema
Global Const constESPERA = 0
Global Const constSOLICITUD = 1
Global Const constESCLAVO = 2
Global Const constCONFIRMACION = 3
Global Const constMASTER = 4
Global Const constTRANSMISION = 5

'Constantes de los procesos de las pruebas
Global Const constPROCESOSOLICITUD = 1
Global Const constPROCESOREALIZACION = 5
Global Const constPROCESOVALIDACION = 9

'Constantes de los estados de las pruebas
Global Const constPRUEBASOLICITADA = 1
Global Const constPRUEBAIMPRESA = 2
Global Const constPRUEBAEXTRAIDA = 3
Global Const constPRUEBASOLICITUDREALIZ = 4
Global Const constPRUEBAREALIZANDO = 5
Global Const constPRUEBARESULTADO = 6
Global Const constPRUEBARESULTANALIZ = 7
Global Const constPRUEBAVALIDADA = 8
Global Const constPRUEBAINSERTADA = 9
Global Const constPRUEBAENVIOPROV = 10
Global Const constPRUEBAENVIADA = 11
Global Const constPRUEBAREPETIDA = 90
Global Const constPRUEBAANULADA = 99
    
'Constantes del estado de las muestras
Global Const constMUESTRAPENDIENTE = 1
Global Const constMUESTRAEXTRAIDA = 2
Global Const constMUESTRAFINALIZADA = 3
Global Const constMUESTRAANULADAMUESTRA = 4
Global Const constMUESTRAANULADAPRUEBA = 5
Global Const constMUESTRAGUARDADA = 6

'Constantes de los estados de los resultados
Global Const constRESULTADOINTRODUCIDO = 1
Global Const constRESULTADOVALIDADO = 2
Global Const constRESULTADOREPETIDO = 3
Global Const constRESULTADOANULADO = 4

'Variables de configuraci�n de los par�metros I/O
Global Puerto As Integer            'Puerto serie utilizado por el ordenador.
Global BaudRate As Integer          'Velocidad de comunicaci�n.
Global DataBits As Integer          'N�mero de bits por caracter.
Global StopBits As Integer          'N�mero de bits de stop.
Global Parity As String             'Paridad

'C�digos de los autoanalizadores
Global Const constELECTRA = 1
Global Const constKOAGULAB = 2
Global Const constKOAG_A_MATE = 3
Global Const constAGGRECORDER = 4
Global Const constCPA_COULTER = 5
Global Const constCOULTER_T540 = 6
Global Const constCOULTER_STKS = 7
Global Const constTECHNICON = 8
Global Const constCOBAS_VEGA = 9
Global Const constVES_MATIC = 10
Global Const constBECKMAN_CX3 = 11
Global Const constHITACHI_704 = 12
Global Const constHITACHI_717 = 13
Global Const constCOBAS_FARA = 14
Global Const constCLINITEK = 15
Global Const constLABORATORIO_REF = 16
Global Const constCOBAS_CORE_MARC = 17
Global Const constAXSYM = 18
Global Const constBECKMAN_APPRAISE = 19
Global Const constIMX_PRO = 20
Global Const constBECKMAN_ARRAY = 21
Global Const constVITEK = 22
Global Const constBACK_ALERT = 23
Global Const constCOBAS_CORE_SER = 24
Global Const constIMX_SER = 25
Global Const constCERES_900 = 26
Global Const constEPICS_XL = 27
Global Const constCAP = 28
Global Const constTDX = 29
Global Const constTDX_FLX = 30
Global Const constIMX_FARM = 31
Global Const constBNA_BEHRING = 32

'Constantes del estado de las listas de trabajo
Global Const constLISTAPREALIZANDO = 1
Global Const constLISTAPRESULTANAL = 2
Global Const constLISTAPRESULTADO = 3
Global Const constLISTAPVALIDTECNI = 4

Declare Function GetPrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
Declare Function WritePrivateProfileString Lib "Kernel" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lplFileName As String) As Integer


Function CodigoANumero& (Cod$)

    Dim Num&
    If (Left$(Cod, 1) >= "A" And Left$(Cod, 1) <= "Z") Or (Left$(Cod, 1) >= "a" And Left$(Cod, 1) <= "z") Then
        Dim Izq$, Der$, idx%
        If Len(Cod) <> 8 And Len(Cod) <> 7 Then
            CodigoANumero = 0
            Exit Function
        End If
        Izq = UCase(Left$(Cod, 3))
        idx = Len(Cod)
        While IsNumeric(Mid$(Cod, idx, 1))
            Der = Mid$(Cod, idx, 1) & Der
            idx = idx - 1
        Wend
        For idx = 0 To 2
            Num = 26 * (Num + Asc(Mid$(Izq, idx + 1, 1)) - 64)
        Next idx
        Num = Num / 26
        Num = Num * 10000 + Val(Der)
        CodigoANumero = Num
    Else
        If Not IsNumeric(Cod) Then
            CodigoANumero = 0
            Exit Function
        End If
        If Val(Cod) > 9999 Then
            CodigoANumero = Val(Cod)
        Else
            Dim sql$, snp As Snapshot
            sql = "Select cMuestraLetra from CodigoPrueba"
            Set snp = dbCun.CreateSnapshot(UCase(sql), DATA_SQLPASSTHROUGH)
            CodigoANumero = CodigoANumero(snp(0) & "-" & Format(Val(Cod), "0000"))
        End If
    End If

End Function

Sub Config_IO (cAutoanalizador As Integer)

'                                   ----- (�ltima revisi�n 5-03-96) -----
    
'Establece los par�metros de Entrada/Salida de los puertos serie del ordenador para que est�n
'conformes a los par�metros de los autoanalizadores.

'                                   ***** DECLARACI�N DE VARIABLES *****
    Dim PARAMETROS As String * 80
    Dim sql As String
    Dim snp As Snapshot
    Dim RES%
'                                      ***** DESARROLLO *****
    
    'On Error GoTo label
    
    'Se accede a la base de datos
    'parametros = "DSN=" & constDatabase & ";SERVERNAME=" & constServerName & ";UID=" & constPropietario & ";PWD=" & constPwd
    RES = GetPrivateProfileString("DATABASE", "PARAMET", "", PARAMETROS, 80, "F:\LABORAT\AUTOAN\AUTOAN.INI")
    Set dbCun = OpenDatabase("ORACLE", 0, 0, PARAMETROS)
    'Se leen las caracter�sticas de la configuraci�n I/O del autoanalizador y se establecen para el Comm.
    sql = "SELECT puertoSerie, baudRate, parity, dataBits, stopBits, designacion FROM " & constPropietario & ".autoAnalizadores"
    sql = sql & " WHERE cAutoAnalizador = " & cAutoanalizador
    Set snp = dbCun.CreateSnapshot(UCase(sql), DB_SQLPASSTHROUGH)
    If snp.RecordCount > 0 Then
        Puerto = snp("puertoserie")
        BaudRate = snp("baudRate")
        Parity = snp("parity")
        DataBits = snp("dataBits")
        StopBits = snp("stopBits")
    End If
    frmPrincipal.Comm1.CommPort = Puerto
    frmPrincipal.Comm1.Notification = 1  'Modo Event Driven; 0 - Modo Polling   Ref:Q101944
    frmPrincipal.Comm1.Settings = BaudRate & "," & Parity & "," & DataBits & "," & StopBits
    frmPrincipal.Comm1.PortOpen = True
    frmPrincipal.panpuerto.Caption = "Puerto Serie " & frmPrincipal.Comm1.CommPort & ": Abierto"
    snp.Close

End Sub



Function fFormatearNumero (numero As String, sustituir As String, sustituirPor As String) As Integer

'                                   ----- (�ltima revisi�n 10-11-95) -----
    
'Formatea un n�mero decimal intercambiando puntos por comas y viceversa

'                                   ***** DECLARACI�N DE VARIABLES *****

    Dim RES As Integer
    
'                                      ***** DESARROLLO *****
    RES = InStr(numero, sustituir)
    If RES > 0 Then
        numero = Left$(numero, RES - 1) & sustituirPor & Right$(numero, Len(numero) - RES)
    End If

End Function


Function LeerEstado (status As Integer) As String

'Lee los estados del sistema y los escribe como un string
    
    Select Case status
    Case constESPERA
        LeerEstado = "ESPERA"
    Case constCONFIRMACION
        LeerEstado = "CONFIRMACION"
    Case constSOLICITUD
        LeerEstado = "SOLICITUD"
    Case constESCLAVO
        LeerEstado = "ESCLAVO"
    Case constMASTER
        LeerEstado = "MASTER"
    Case constTRANSMISION
        LeerEstado = "TRANSMISION"
    Case Else
        LeerEstado = "no definido"
    End Select

End Function

Function NumeroACodigo$ (Num&)
 
    If Num < 10000 Then
        Dim sql$, snp As Snapshot
        sql = "Select cMuestraLetra from CodigoPrueba"
        Set snp = dbCun.CreateSnapshot(UCase(sql), DATA_SQLPASSTHROUGH)
        NumeroACodigo = snp(0) & "-" & CStr(Num)
    Else
        Dim a%, b%, c%, d%
        d = Num Mod 10000
        Num = (Num - d) / 10000
        c = Num Mod 26
        Num = (Num - c) / 26
        b = Num Mod 26
        Num = (Num - b) / 26
        a = Num
        If (a < 1 Or a > 26) Or (b < 1 Or b > 26) Or (c < 1 Or c > 26) Then
            NumeroACodigo = ""
        Else
            NumeroACodigo = Chr$(64 + a) & Chr$(64 + b) & Chr$(64 + c) & "-" & Format(CStr(d), "0000")
        End If
    End If

End Function

Sub Seleccion_Lista_Trabajo (cAutoanalizador As Integer, cListaRealizacion As Long)

'                                       ----- (�ltima revisi�n 17-10-95) -----

'Subrutina que localiza, si existe, la lista de trabajo de c�digo m�nimo que todav�a tiene
'pruebas pendientes de realizar en un autoanalizador.

'Devuelve (en la variable cListaRealizacion) el c�digo de la lista de trabajo si existe o cero si
'no existe ninguna lista con pruebas pendientes de realizar.

'                                       ***** DECALRACI�N DE VARIABLES *****

    Dim MatCarpetas()  As Integer       'c�digos de las carpetas asociadas al autoanalizador
    Dim NumCarpetas As Integer
    Dim minCListaRealizacion As Long
    
    Dim snp As Snapshot
    Dim numRegistros As Integer
    Dim sql As String
    Dim contador As Integer

'                                              ***** DESARROLLO *****

    'Se determina las carpetas del autoanalizador
    ReDim MatCarpetas(0 To 0)
    sql = "SELECT cCarpeta FROM " & constPropietario & ".carpetas WHERE cAutoAnalizador = " & cAutoanalizador
    Set snp = dbCun.CreateSnapshot(UCase$(sql), DATA_SQLPASSTHROUGH)
    If snp.RecordCount > 0 Then
        snp.MoveLast
        NumCarpetas = snp.RecordCount
        snp.MoveFirst
        For contador = 1 To NumCarpetas
            ReDim Preserve MatCarpetas(0 To UBound(MatCarpetas) + 1)
            MatCarpetas(UBound(MatCarpetas)) = snp("cCarpeta")
            snp.MoveNext
        Next contador
    Else
        cListaRealizacion = -1000 'No existen carpetas asociadas al autoanalizador
        Exit Sub
    End If
    snp.Close

    'Se determina la lista de realizaci�n de menor orden que tienen pruebas pendientes de realizar
label:
    sql = "SELECT cListaRealizacion FROM " & constPropietario & ".pruebaAsistencia WHERE estado = " & constPRUEBASOLICITUDREALIZ
    sql = sql & " AND (demorada = 0 OR demorada IS NULL)  AND ("
    For contador = 1 To UBound(MatCarpetas)
        sql = sql & "cCarpeta = " & MatCarpetas(contador) & " OR "
    Next contador
    sql = Left$(sql, Len(sql) - 4) & ")"
    Set snp = dbCun.CreateSnapshot(UCase(sql), DB_SQLPASSTHROUGH)
    'Existen pruebas pendientes de realizar en el autoanalziador
    If snp.RecordCount > 0 Then
        snp.MoveLast
        numRegistros = snp.RecordCount
        snp.MoveFirst
        minCListaRealizacion = snp("cListaRealizacion")
        For contador = 1 To numRegistros
            cListaRealizacion = snp("cListaRealizacion")
            If cListaRealizacion < minCListaRealizacion Then
                minCListaRealizacion = cListaRealizacion
            End If
            snp.MoveNext
        Next contador
        If minCListaRealizacion = -1 * cAutoanalizador Then
            sql = "UPDATE " & constPropietario & ".pruebaAsistencia SET cListaRealizacion = NULL "
            sql = sql & ", estado = " & constPRUEBAEXTRAIDA & " WHERE cListaRealizacion = -" & cAutoanalizador
            sql = sql & " AND (demorada = 0 OR demorada IS NULL) AND ("
            For contador = 1 To UBound(MatCarpetas)
                sql = sql & "cCarpeta = " & MatCarpetas(contador) & " OR "
            Next contador
            sql = Left$(sql, Len(sql) - 4) & ")"
            dbCun.Execute UCase(sql), DB_SQLPASSTHROUGH
            GoTo label
        End If
        cListaRealizacion = minCListaRealizacion
    'No existen pruebas pendientes de realizar en el autoanalizador
    Else
        cListaRealizacion = 0
    End If
    snp.Close

End Sub

