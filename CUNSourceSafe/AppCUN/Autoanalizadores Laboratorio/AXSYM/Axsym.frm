VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Axsym"
   ClientHeight    =   1380
   ClientLeft      =   6225
   ClientTop       =   690
   ClientWidth     =   1425
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "Axsym.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1380
   ScaleWidth      =   1425
   Begin VsOcxLib.VideoSoftAwk Awk4 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   480
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   20000
      Left            =   930
      Top             =   960
   End
   Begin VB.Timer Timer3 
      Enabled         =   0   'False
      Interval        =   30000
      Left            =   510
      Top             =   960
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   90
      Top             =   960
   End
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   480
      Top             =   480
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk3 
      Left            =   960
      Top             =   480
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim vc As Integer
Dim conta As Integer

Dim MiObjMuestra As clsMuestra
Dim MiObjActuacion As clsActuacion

Dim contintentostrans As Integer 'cuenta el n� de veces que se intenta transmitir datos al autoanalizador
Dim contintentoscomunic As Integer 'cuenta el n� de veces que se intenta comunicar con el autoanalizador
Dim TipoFrame As Integer
Dim mensaje1, mensaje2, MENSAJE3, MENSAJE4  As String
Dim pathdefecto As String
'                               ----- (�ltima revisi�n 1-12-95) -----

'                           ***** NOTAS DE INTER�S GENERAL PARA EL FUNCIONAMIENTO *****

'Por el momento no se considera la posibilidad de que se llene el buffer de recepci�n de datos y se produzca un error RXOVER.
'Si esto pudiera llegar a ocurrir la soluci�n ser� una de las siguientes:
'   1 - Aumentar el tama�o del buffer de recepci�n
'   2 - Enviar un Xoff cuando el buffer est� proximo a llenarse

'                              ***** DECLARACI�N DE VARIABLES GLOBALES DEL FORM *****

'Public estado As Integer               'Estado del sistema
'Dim matrizpruebas() As String       'Matriz que contiene las pruebas de la lista de trabajo
'Dim matrizresultados() As String    'Matriz que contiene los resultados de las pruebas
Dim cListaRealizacion As Long       'N�mero de la lista de trabajo

Dim ultimoFrame As Integer      'Indica si es el �ltimo TipoFrame a enviar

'Constantes de urgencias
Const constURGENCIA = 1
Const constRUTINA = 0

Dim respuesta As Integer 'Respuesta �ltima enviada en comunicaci�n bidireccional (0 - <ACK>; 1 - <ETX>; 2 - <NAK>)
Dim RespuestaAfirmativa As Integer 'Respuesta pr�xima afirmativa a enviar en comunicaci�n bidireccional
'(0 - <ACK>; 1 - <ETX>)

Dim UltimoEnvio As Integer          'True: si no hay m�s env�os de prueba a programar
                                    'False: todav�a quedan pruebas a programar

'Constantes de resultados de c�lculos especiales
Const constANION_GAP = "A_GAP"
Const constUREA_CREATININA = "UR/CR"
Const constOSMOLARIDAD = "OSMOL"

'Dim Linea As Integer

Public TextoLog As String
Dim txtenviar As String

Dim archivolog As String
Enum Operation
  cteReset = 2
  cteSuma = 1
  cteResta = -1
End Enum

Private Sub AXSYM_Esclavo(Mensaje As String, caracter As String)
'Identifica el final de un bloque pasando a su lectura. Tambi�n se identifica el final de la
'comunicaci�n y se vuelve al estado de espera.
Dim contador As Integer
Dim sql As String
On Error GoTo AutoAnError

  Timer3.Enabled = False 'Al recibir algun caracter desactivamos el timer
  Select Case True
    Case caracter = Chr$(4) '<EOT> - Final de la transmisi�n por parte del autoanalizador
      objAutoAn.Estado = cteESPERA
      Mensaje = ""
    Case Right$(Mensaje, 1) = Chr$(10)
      'al haber recibido el final del TipoFrame se contesta <ACK> y se reinicia el tiempo de espera
      objAutoAn.Enviar Chr$(6)
      'se activa el timer3 que corresponde al tiempo de espera asignado para el proximo TipoFrame
      Timer3.Enabled = True
      'Se comprueba si se trata del ultimo TipoFrame del mensaje para proceder a la lectura
      'de todo el mensaje de resultado
      If Mid$(Right$(Mensaje, 9), 1, 1) = "L" And _
      Mid$(Right$(Mensaje, 8), 1, 1) = "|" Then
        Lectura_Datos (Mensaje)
        Mensaje = ""
      End If
      If objAutoAn.blnDesign Then Simulacion
  End Select
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "AXSYM_Esclavo", "AXSYM_Esclavo")
End Sub

Private Sub AXSYM_Espera(Mensaje As String, caracter As String)
'                               ----- (�ltima revisi�n 29-01-96) -----
'Identifica la llegada de los caracteres de solicitud de comunicaci�n, la acepta y pasa
'al estado de transmisi�n.
On Error GoTo AutoAnError
  If caracter = Chr$(5) Then '<ENQ> - El autoanalizador solicita respuesta a su solicitaci�n de comunicaci�n
    'por si acaso le habiamos mandado un <ENQ> simultaneo desactivamos el timer de espera
    Timer2.Enabled = False
    'Se controla el tiempo que tarda el autoanaliz. en volver a mandar el siguiente mensaje
    Timer3.Enabled = True
    objAutoAn.Enviar Chr$(6)  '<ACK> - Se acepta la comunicaci�n (!!No se conoce motivo para rechazarla)
    objAutoAn.Estado = cteESCLAVO
    Mensaje = "" 'Se eliminan los posibles caracteres enviados antes de establecerse la comunicaci�n
  End If
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "AXSYM_Espera", "AXSYM_Espera")
End Sub

Private Sub AXSYM_Master(Mensaje As String, caracter As String)
Dim contador As Integer
Dim sql As String
On Error GoTo AutoAnError
'Tras haber enviado un bloque, el autoanalizador responde acept�ndolo o rechaz�ndolo (si le ha llegado con errores).
'Si el mensaje es rechazado habr� que volver a enviarlo.
'Si es aceptado se envia el siguiente TipoFrame del mensaje. Cada mensaje esta compuesto de 4 frames:
'-Header,-Patient,-Order,-Termination cada uno de ellos precedido por un n� secuencial (0-7)
'(vble linea)
  Timer2.Enabled = False 'Se desactiva el timeout, pues ya nos ha respondido algo
  'existe la posibilidad de que el Axsym conteste con <EOT> en vez de <ACK> para solicitar que
  'acabe la comunicaci�n. En tal caso pasaremos al estado de espera
  Select Case caracter
    Case Chr$(4)
      'Timer1.Enabled = False 'Se ha recibido respuesta positiva y no habr� que volver a intentarlo de nuevo
      'duda: �Pasamos a estado espera ahora o cuando se haya terminado de transmitir todo el mensaje con sus 4 frames?
      objAutoAn.Estado = cteESPERA
    Case Chr$(6) '<ACK> - El autoanalizador env�a un reconocimiento positivo
      'Timer1.Enabled = False 'Se ha recibido respuesta positiva y no habr� que volver a intentarlo de nuevo
      contintentostrans = 0
      'cuando hemos acabado de transmitir todo
      If UltimoEnvio = True And TipoFrame = 4 Then
        objAutoAn.Enviar Chr$(4)  '<EOT> - Se cierra la comunicaci�n para esperar la respuesta del autoanalizador
        objAutoAn.Estado = cteESPERA
        MiObjActuacion.strEstado = cteACEPTADA
        MiObjActuacion.Muestra.strEstado = cteACEPTADA
        'matrizpruebas(8, conta) = "AC"
        'ssdgpruebas.Refresh
        Exit Sub
      End If
      If TipoFrame < 4 Then
        TipoFrame = TipoFrame + 1
      Else
        'se ha transmitido correctamente todo el mensaje que corresponde al registro contador1 de la matriz
        'matrizpruebas(8, conta) = "AC"
        MiObjActuacion.strEstado = cteACEPTADA
        'ssdgpruebas.Refresh
        TipoFrame = 1
      End If
      Envio_Mensajes 'Se enviar� el siguiente mensaje
      Mensaje = ""
    Case Chr$(21)   '<NAK> - El autoanalizador no ha aceptado el ultimo TipoFrame enviado
      Timer1.Enabled = True 'se activa el timer1 para volver a reenviar el ultimo TipoFrame enviado
      objAutoAn.Estado = cteMASTER
      Mensaje = ""
      'para que linea no aumente, pues se va a reenviar la ultima linea
      'Linea = Linea - 1
      Linea cteResta
    Case Else
    'parada de comprobacion para ver si se mandan otros caracteres
      sql = "Se ha recibido un caracter." + (caracter)
      MsgBox sql, vbExclamation, frmPrincipal.Caption
  End Select
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "AXSYM_Master", "AXSYM_Master")
End Sub

Private Sub AXSYM_Solicitud(Mensaje As String, caracter As String)
'Tras haber realizado el sistema una solicitud para establecer una comunicaci�n,
'el autoanalizador responde aceptando o negando dicha comunicaci�n.
'On Error Resume Next
On Error GoTo AutoAnError
  Timer2.Enabled = False 'puesto que ya nos ha respondido algo, se desactiva el timeout
  Select Case caracter
    Case Chr$(6)  '<ACK> - El autoanalizador ha aceptado la comunicaci�n
      'inicializamos el n� de intentos de establecimiento de comunicacion pues ya se ha conseguido
      contintentoscomunic = 0
      objAutoAn.Estado = cteMASTER
      Mensaje = ""
      Envio_Mensajes 'Se env�a el bloque de datos por parte del sistema al autoanalizador '!!MODIFICAR
    Case Chr$(21) '<NAK>  - El autoanalizador no ha aceptado la comunicaci�n
      objAutoAn.Estado = cteESPERA
      Mensaje = ""
      Timer1.Enabled = True 'Se espera 10 segundos para intentar una nueva comunicaci�n
    Case Chr$(5) '<ENQ>- El autoanalizador ha "solicitado l�nea" a la vez que el sistema
      'No se manda nada y se pasa a Espera para esperar a recibir el nuevo intento del Axsym
      objAutoAn.Estado = cteESPERA
      Mensaje = ""
  End Select
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "AXSYM_Solicitud", "AXSYM_Solicitud")
End Sub

Public Sub Programar()
On Error GoTo AutoAnError
'para que no empiece a solicitar comunicacion y...sin que haya algo que mandar.
  If objAutoAn.blnExistMuestraConEstado(cteSINPROGRAMAR) Then
    Envio_Mensajes
  Else
    MsgBox "�Todas las pruebas est�n ya programadas!" & vbCr & _
    "Para volver a programarlas paselas a estado 'Sin Programar'", vbExclamation, frmPrincipal.Caption
  End If
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Programar", "Programar")
End Sub

Private Sub Envio_Mensajes()
'Si se desea mandar un mensaje al autoanalizador lo primero que hay que hacer es env�ar el
'caracter <ENQ> y esperar a que el autoanalizador responda afirmativamente. Luego se manda
'el mensaje propiamente dicho hasta que sea recibido correctamente por el autoanalizador.
'Finalmente se cierra la comunicaci�n enviando <EOT> para dar paso a la respuesta del
'autoanalizador al requerimiento del sistema.
Dim T As Long
Dim RES As Integer
Dim sql As String
Dim contador As Integer
On Error GoTo AutoAnError

  With objAutoAn
    Select Case .Estado
      Case cteESPERA
        contintentoscomunic = contintentoscomunic + 1
        If contintentoscomunic = 6 Then 'Fallo en la comunicaci�n despu�s de 6 intentos
          contintentoscomunic = 1
          sql = "No se puede establecer comunicaci�n."
          Select Case MsgBox(sql, vbExclamation + vbRetryCancel, frmPrincipal.Caption)
            Case vbRetry
              T = Timer + 5
              Do While T > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              Exit Sub
            Case vbCancel
              'Form_Unload (False)
              Descargar
          End Select
        Else
          Sistema_Espera
          .PanelEstado .fLeerEstado(.Estado) & " " & contintentoscomunic
          Exit Sub
        End If
      Case cteMASTER
        contintentostrans = contintentostrans + 1
        If contintentostrans = 6 Then 'Fallo en la transmisi�n despu�s de 6 intentos
          contintentostrans = 0
          .Estado = cteESPERA
          sql = "No se puede transmitir informaci�n."
          Select Case MsgBox(sql, vbExclamation + vbAbortRetryIgnore, frmPrincipal.Caption)
            Case vbRetry
              'Se reintenta mandar todo el registro actual
              T = Timer + 5
              Do While T > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              contintentoscomunic = 1
              Exit Sub
            Case vbAbort
              'Se sale del programa y se queda la prueba sin programar
              'matrizpruebas(8, conta) = "SP"
              'ssdgpruebas.Refresh
              MiObjActuacion.strEstado = "SP"
              Descargar
            Case vbIgnore
            'Se queda la prueba como no aceptada y se sigue la transmision con el 1er TipoFrame
            'del siguiente registro en estado "SP"
      '                    matrizpruebas(8, conta) = "NA"
      '                    ssdgpruebas.Refresh
              MiObjActuacion.strEstado = "NA"
              T = Timer + 5
              Do While T > Timer: RES = DoEvents(): Loop
              Sistema_Espera
              Exit Sub
          End Select
        Else
          Sistema_Master
          Exit Sub
        End If
    End Select
  End With
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Envio_Mensajes", "Envio_Mensajes")
End Sub

Private Sub Establecer_CheckFin(Mensaje As String)
'Subrutina que establece el check del mensaje y ademas los caracteres del final del TipoFrame
'<CR><LF> que ser�n enviados al autoanalizador.
Dim contador As Integer     'cuenta el n�mero de caracteres le�dos
Dim suma As Integer         'suma de los c�digos ASCII
On Error GoTo AutoAnError

  suma = 0
  For contador = 1 To Len(Mensaje)
    suma = suma + Asc(Mid$(Mensaje, contador, 1))
  Next contador
  'If GAM <= 2 Then   PARA PRUEBAS CON ERROR!
  'GAM = GAM + 1
  'mensaje = mensaje & "aw"
  'Else
  'GAM = 0
  If Len(Hex$(suma)) = 1 Then
    Mensaje = Mensaje & "0" & Hex$(suma)
  Else
    Mensaje = Mensaje & Right$(Hex$(suma), 2)
  End If
  'End If
  Mensaje = Mensaje + Chr$(13) + Chr$(10)
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Establecer_CheckFin", "Establecer_CheckFin")
End Sub

Private Sub Lectura_Datos(datos As String)
Dim Posicion%, contador1%, sql$
Dim Resultado$, i%, filenum
Dim codMuestra$, codActuacion%
Dim codResultado%, codResultadoAuto$
Dim dblDilucion As Double
On Error GoTo AutoAnError

  'Se separan los distintos tipos de registros del mensaje
  Awk1.FS = Chr$(10): Awk1 = datos
  Awk2.FS = "|": Awk2 = Awk1.F(1)
  'El awk1.F(1) contiene la cabecera (registro tipo H) y el awk1.F(awk1.NF) el terminador
  '(registro tipo L) que no sirven para nada.
  'El awk1.F(2) nos indica que tipo de mensaje es: registro tipo P (resultados)
  If UCase$(Left$(Awk2.F(12), 1)) = "P" Then
    'Siempre va a haber un solo resultado por muestra-prueba
    Awk2.FS = "|": Awk2 = Awk1.F(3)
    Awk3.FS = "^": Awk3 = Awk2.F(4)
    'Se obtiene el c�digo de la muestra
    'codMuestra = Awk3.F(1)
    Awk4 = Awk3.F(1): Awk4.FS = "/"
    dblDilucion = 1
    For i = 1 To Awk4.NF
      Select Case i
        Case 1: codMuestra = Awk4.F(1)
        Case Else
          Select Case True
            Case Left$(Awk4.F(i), 1) = "D": Posicion = 2
            Case InStr(1, Awk4.F(i), "D") > 0: Posicion = InStr(1, Awk4.F(i), "D") + 1
          End Select
          If Posicion > 0 Then
            If IsNumeric(Val(Mid$(Awk4.F(i), Posicion))) Then
              dblDilucion = Val(Mid$(Awk4.F(i), Posicion))
            Else
              MsgBox "Se Ha Recibido Un Resultado Con Dilucion  No Determinada." & _
              "Muestra" & codMuestra, vbCritical + vbOKOnly, "Dilucion Indefinida"
            End If
          End If
      End Select
    Next i
    If objAutoAn.blnExistMuestra(codMuestra) Then
    'Se obtiene el c�digo de la prueba
      If UCase$(Mid$((Awk1.F(4)), 3, 1)) = "R" Then
        Awk2 = Awk1.F(3)
        Awk3.FS = "^": Awk3 = Awk2.F(5)
        codResultadoAuto = Awk3.F(5)
        'strDilucion = Awk3.F(6)
        'objAutoAn.EscribirLog "strDilucion=" & strDilucion
        Awk2 = Awk1.F(4)
        If Awk2.F(4) <> "" Then
          Awk3.FS = "^"
          Awk3 = Awk2.F(3)
          Resultado = Awk2.F(4)
'          If objAutoAn.ColMuestras(codMuestra).intDilucion <> 0 And _
'          objAutoAn.ColMuestras(codMuestra).intDilucion <> 1 Then
'            If IsNumeric(Resultado) Then
'              Resultado = CStr(Val(Resultado) * objAutoAn.ColMuestras(codMuestra).intDilucion)
'            Else
'              Resultado = Resultado & " * " & CStr(objAutoAn.ColMuestras(codMuestra).intDilucion)
'            End If
'          End If
          If dblDilucion <> 1 And dblDilucion <> 0 Then
            If IsNumeric(Resultado) Then
              Resultado = CStr(Val(Resultado) * dblDilucion)
            Else
              Resultado = Resultado & " (* " & CStr(dblDilucion) & ")"
            End If
          End If
          Call objAutoAn.pIntroResultConCResAuto(codMuestra, codResultadoAuto, Resultado, True)
        End If
      End If
    End If
  End If
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Lectura_Datos", "Lectura_Datos")
End Sub

Public Sub Lectura_Protocolo(caracter As String)
Static Mensaje As String   'Contiene el mensaje que env�a el autoanalizador
Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
  Mensaje = Mensaje & caracter
  CopiaMensaje = Mensaje
  Select Case objAutoAn.Estado
    Case cteESPERA
      AXSYM_Espera CopiaMensaje, caracter
      Mensaje = CopiaMensaje
    Case cteSOLICITUD
      AXSYM_Solicitud CopiaMensaje, caracter
      Mensaje = CopiaMensaje
    Case cteESCLAVO
      AXSYM_Esclavo CopiaMensaje, caracter
      Mensaje = CopiaMensaje
    Case cteMASTER
      AXSYM_Master CopiaMensaje, caracter
      Mensaje = CopiaMensaje
  End Select
End Sub

Private Sub lin()
'Subrutina que da el n� secuencial para el TipoFrame a transmitir
  'Linea = IIf(Linea < 7, Linea + 1, 0)
End Sub

Private Function Linea(Optional Sumador As Operation) As Integer
Static MiLinea As Integer
  Select Case Sumador
    Case cteReset: MiLinea = 0
    Case cteResta: MiLinea = IIf(MiLinea <> 0, MiLinea - 1, 7)
    Case Else: MiLinea = IIf(MiLinea < 7, MiLinea + 1, 0) 'suma
  End Select
  Linea = MiLinea
End Function

Public Sub Descargar()
'Fin de programa
  With objAutoAn
    .EscribirLog Format(.fFechaHoraActual, "hh:mm:ss") & " Fin de Ejecuci�n"
  End With
  End
End Sub

Public Function Salir() As Boolean
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion) = vbCancel)
  With objAutoAn
    .EscribirLog " Pulsado Salir"
  End With
  If Not Salir Then Descargar
End Function

Public Sub probar()
  If objAutoAn.blnDesign Then Simulacion
End Sub

Private Sub Records(Mensaje As String)
Dim objMuestra As clsMuestra
Dim objActuacion As clsActuacion
'En esta subrutina se define que TipoFrame es el que toca enviar seg�n la vble TipoFrame
On Error GoTo AutoAnError

  Select Case TipoFrame
    Case 1
      For Each objMuestra In objAutoAn.ColMuestras
        For Each objActuacion In objMuestra.colmuestraspruebas
          If objActuacion.strEstado = cteINTENTOPROGRAMAR Then GoTo salto
        Next
      Next
      For Each objMuestra In objAutoAn.ColMuestras
        For Each objActuacion In objMuestra.colmuestraspruebas
          If objActuacion.strEstado = cteSINPROGRAMAR Then
            objActuacion.strEstado = cteINTENTOPROGRAMAR
            'MiMuestra = objMuestra
            Set MiObjActuacion = objActuacion
            GoTo salto
          End If
        Next
      Next
salto:
      Mensaje = Trim$(Str$(Linea)) + "H|\^&||||||||||P|1" + Chr$(13) + Chr$(3)
      Establecer_CheckFin Mensaje
      Mensaje = Chr$(2) + Mensaje
    Case 2
      Mensaje = Trim$(Str$(Linea)) + "P|1||||" + Chr$(13) + Chr$(3)
      Establecer_CheckFin Mensaje
      Mensaje = Chr$(2) + Mensaje
    Case 3
      Stream301_Funcion01 Mensaje
    Case 4
      Mensaje = Trim$(Str$(Linea)) + "L|1" + Chr$(13) + Chr$(3)
      Establecer_CheckFin Mensaje
      Mensaje = Chr$(2) + Mensaje
  End Select
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "records", "records")
End Sub

Private Sub Sistema_Espera()
On Error GoTo AutoAnError
  'Se env�a el caracter de solicitud de comunicaci�n <ENQ> y se pasa a estado de Solicitud
  objAutoAn.Enviar Chr$(5)
  'Se reinicializa el contador del TipoFrame a transmitir y el n� linea secuencial
  TipoFrame = 1
  Linea cteReset
  objAutoAn.Estado = cteSOLICITUD
  Timer2.Enabled = True   'Empieza a contar el tiempo para el TimeOut
  If objAutoAn.blnDesign Then objAutoAn.AutoRecepcion Chr$(6)
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Espera", "Sistema_Espera")
End Sub

Private Sub Sistema_Master()
'Se transmiten datos al autoanalizador. Los datos a enviar pueden ser de dos tipos: programaci�n de pruebas o borrado de
'pruebas programadas.
Dim Mensaje As String 'Contiene un string correspondiente al mensaje a enviar
On Error GoTo AutoAnError
  'Se construye el mensaje para enviar
  Records Mensaje
  'Y se env�a el mensaje
  objAutoAn.Enviar Mensaje
  Timer2.Enabled = True 'Empieza a contar el tiempo para el TimeOut
  If objAutoAn.blnDesign Then objAutoAn.AutoRecepcion Chr$(6)
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Sistema_Master", "Sistema_Master")
End Sub

Private Sub Stream301_Funcion01(Mensaje As String)
'Se forma el TipoFrame que corresponde al de programacion de la muestra y prueba
'Es el unico que depende de la matrizpruebas
Dim cmuestra, sql As String     'c�digo de la muestra/c�digo de la muestra num�rico
Dim cprueba As String       'c�digo de la prueba/c�digo de la prueba
Dim pruebas As String       'c�digos de las pruebas acumulados en un mismo string
Dim cEspecimen As String    'c�digo de la especie/c�digo de la especie
'ahora conta es vble global (ver sub axsym_master)
'Dim conta As Integer
Dim contador2 As Integer
Dim mensaje1, mensaje2, MENSAJE3 As String
Dim objActuacion As clsActuacion
Dim objMuestra As clsMuestra
Dim strDilution As String
On Error GoTo AutoAnError
  
  Mensaje = ""
  For Each objMuestra In objAutoAn.ColMuestras
    With objMuestra
      'se a�ade el tiempo que corresponde a la muestra en caso de curva (y que no sea basal)
      If .intTiempo = 0 Then cmuestra = .strCodMuestra _
      Else cmuestra = .strCodMuestra + "/T" & CStr(.intTiempo)
'      If .intDilucion <> 1 And .intDilucion <> 0 Then strDilution = _
'      "^" & CStr(objAutoAn.fFormatearNumero(CStr(.intDilucion), ",", "."))
      If .intDilucion <> 1 And .intDilucion <> 0 Then cmuestra = cmuestra & _
      "/D" & CStr(objAutoAn.fFormatearNumero(CStr(.intDilucion), ",", "."))
      For Each objActuacion In .colmuestraspruebas
        If objActuacion.strEstado = cteINTENTOPROGRAMAR Then
          cprueba = objAutoAn.ColPruebaAuto(objActuacion.strCodAct).strCodActAuto
          'lin
          Mensaje = Trim$(Str$(Linea)) & "O|1|" & cmuestra & "||^^^" & _
          cprueba & "|||||||A||||||||||||||O" & vbCr & Chr$(3)
          Establecer_CheckFin Mensaje
          Mensaje = Chr$(2) & Mensaje
          Exit For
        End If
      Next 'conta
    End With
  Next
  UltimoEnvio = True
  For Each objMuestra In objAutoAn.ColMuestras
    For Each objActuacion In objMuestra.colmuestraspruebas
      If objActuacion.strEstado = cteSINPROGRAMAR _
      Then UltimoEnvio = False: Exit For
    Next
    If Not UltimoEnvio Then Exit For
  Next
  Exit Sub
    
AutoAnError:
    Call objError.InternalError(Me, "Stream301_Funcion01", "Stream301_Funcion01")
End Sub

Private Sub Timer1_Timer()
'Timer que se activa para volver a intentar transmitir
  Timer1.Enabled = False
  Select Case objAutoAn.Estado
    Case cteMASTER 'Habiendo el sistema enviado datos, el autoanalizador responde que el TipoFrame
      'era incorrecto
      Envio_Mensajes 'para realizar un nuevo intento de envio de datos
    Case cteSOLICITUD 'Habiendo el sistema "solicitado l�nea", el autoanalizador responde que
      'no da comunicacion y el sistema lo vuelve a intentar
      objAutoAn.Estado = cteESPERA
      Envio_Mensajes 'para realizar un nuevo intento de envio de datos
  End Select
End Sub

Private Sub Timer2_Timer()
'Timer utilizado para controlar los margenes de tiempo de respuesta (15 SEGUNDOS) TIMEOUT a nuestras transmisiones
    
  Timer2.Enabled = False
  With objAutoAn
    Select Case .Estado
      Case cteSOLICITUD 'Habiendo el sistema "solicitado l�nea", el autoanalizador no responde
        'nada en el margen que se le da
        .Enviar Chr$(4)
        .Estado = cteESPERA
        Envio_Mensajes 'para realizar un nuevo intento de comunicacion
        'contintentoscomunic = 1
        contintentostrans = 1
      Case cteMASTER 'Habiendo el sistema enviado un TipoFrame, el autoanalizador no responde
        'nada en el margen que se le da
        .Enviar Chr$(4)
        'Se pasa a estado espera para volver a solicitar comunic. y seguir mandando frames
        'a partir del ultimo enviado correctamente
        .Estado = cteESPERA
        Envio_Mensajes 'para realizar un nuevo intento de comunicacion
        'contintentostrans = 1
        contintentoscomunic = 1
    End Select
  End With
End Sub

Private Sub Timer3_Timer()
'Controla el tiempo que transcurre desde los �ltimo datos recibidos por el sistema en estado de exclavo.
'Si pasan 20 segundos sin recibir datos o el final de la comunicaci�n <EOT> se finaliza la ejecuci�n
Dim sql As String
Dim resp As Integer
  Timer3.Enabled = False
  sql = "Se ha producido un Timeout durante la comunicaci�n por parte del autoanalizador."
  sql = sql & Chr$(10) & "�Desea Ud. continuar esperando respuesta por parte del autoanalizador?"
  resp = MsgBox(sql, vbExclamation + vbYesNo, frmPrincipal.Caption)
  Select Case resp
    Case vbYes: Timer3.Enabled = True
    Case vbNo: Descargar 'Form_Unload (False)
  End Select
  objAutoAn.Enviar Chr$(4)    'al haberse agotado el tiempo de espera, se le manda <EOT> al autoanalizador
  objAutoAn.Estado = cteESPERA
  'EsperaMensRespuesta = False
End Sub

Public Sub RecibidosTodosResultados()

End Sub

Public Sub pModificaResultadoAuto(objResultadoAuto As clsResultAuto)
  Select Case objResultadoAuto.strCodResultAuto
    Case "TSH": objResultadoAuto.strCodResultAuto = "hTSH_II"
    Case "T4_LIBRE": objResultadoAuto.strCodResultAuto = "T4_Libre"
  End Select
End Sub

Public Sub pModificaMuestra(objMuestra As clsMuestra)
Dim qry1 As rdoQuery
Dim rs1 As rdoResultset
Dim sql As String
On Error GoTo AutoAnError
  
  With objMuestra
    sql = "SELECT tiempo FROM muestraAsistencia WHERE "
    sql = sql & "cMuestra = ?"
    Set qry1 = objAutoAn.rdoConnect.CreateQuery("", sql)
    qry1(0) = .strCodMuestra
    Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
    .strProperty1 = IIf(IsNull(rs1(0)), "", rs1(0)) 'tiempo
    rs1.Close: qry1.Close
    If .intDilucion = 0 Then .intDilucion = 1
  End With
  Exit Sub
'      sql = "select ci22numhistoria from ci2200, pr0400 where"
'      sql = sql & " ci2200.ci21codpersona = pr0400.ci21codpersona"
'      sql = sql & " and pr0400.pr04numactplan = ?"
'      Set qry1 = objAutoAn.rdoConnect.CreateQuery("", sql)
'      qry1(0) = CLng(.colmuestraspruebas(1).strnumact)
'      Set rs1 = qry1.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'      If Not rs1.EOF Then NumHistoria = rs1(0) Else GoTo NextMuestra
'      .strProperty2 = NumHistoria
'      rs1.Close: qry1.Close
AutoAnError:
    Call objError.InternalError(Me, "pModificaMuestra", "pModificaMuestra")
End Sub

Public Sub pModificaActuacion(objActuacion As clsActuacion)
Dim qry2 As rdoQuery
Dim sql As String, i%
Dim rs2 As rdoResultset
On Error GoTo AutoAnError

  With objActuacion
    sql = "select resultadoalfanumerico from" _
    & " resultadoasistencia ra, pruebaasistencia pa where" _
    & " ra.historia = pa.historia and ra.caso = pa.caso and" _
    & " ra.secuencia = pa.secuencia and" _
    & " ra.nrepeticion = pa.nrepeticion and" _
    & " pa.historia = ? and pa.ctipoprueba = ?" _
    & " order by ra.fecha desc"
    Set qry2 = objAutoAn.rdoConnect.CreateQuery("", sql)
    qry2(0) = .Muestra.lngHistoria 'NumHistoria
    qry2(1) = .strCodAct
    Set rs2 = qry2.OpenResultset(rdOpenForwardOnly, rdConcurReadOnly)
'    i = 0
'    Do While Not rs2.EOF And i < 2
'      i = i + 1
'      Select Case i
'        Case 1: .strProperty1 = rs2("resultadoalfanumerico")
'        Case 2: .strProperty2 = rs2("resultadoalfanumerico")
'      End Select
'      rs2.MoveNext
'    Loop
    If Not rs2.EOF Then
      .strProperty1 = rs2("resultadoalfanumerico")
    End If
    rs2.Close: qry2.Close
  End With
  Exit Sub
  
AutoAnError:
    Call objError.InternalError(Me, "pModificaActuacion", "pModificaActuacion")
End Sub

Sub Simulacion()
Static nvez As Integer
  nvez = nvez + 1
  Select Case nvez
    Case 1
      objAutoAn.AutoRecepcion Chr$(5)
      '<STX>3O|1||ADM-9033/T90D2^A^02|^^^55^PRL_3IS^UNDILUTED|R||||||||||||||||||||F<CR><ETX>10<CR><LF>
      objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928102612" & vbCr & Chr$(3) & "B8" & vbCrLf
    Case 2: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 3: objAutoAn.AutoRecepcion Chr$(2) & "3O|1||ADM-9033/T90/D2^A^02|^^^55^PRL_3IS^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "3E" & vbCrLf
    Case 4: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^55^PRL_3IS^UNDILUTED^^F|95.41|ng/mL||||F||ADMIN||20000104163743" & vbCr & Chr$(3) & "DF" & vbCrLf
    Case 5: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^55^PRL_3IS^UNDILUTED^^P|398.29|Rate||||F||ADMIN||20000104163743" & vbCr & Chr$(3) & "0D" & vbCrLf
    Case 6: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 7: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928115117" & vbCr & Chr$(3) & "BC" & vbCrLf
    Case 8: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 9: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADC-9435|ADC-9435^C^01|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "D1" & vbCrLf
    Case 10: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^208^hTSH_II^UNDILUTED^^F|0.000|uIU/mL||||F||ADMIN||19990928114137" & vbCr & Chr$(3) & "77" & vbCrLf
    Case 11: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^208^hTSH_II^UNDILUTED^^P|1.56|Rate||||F||ADMIN||19990928114137" & vbCr & Chr$(3) & "F0" & vbCrLf
    Case 12: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 13: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928115119" & vbCr & Chr$(3) & "C4" & vbCrLf
    Case 14: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 15: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADC-9435|ADC-9435^C^01|^^^219^T4_Libre^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "5E" & vbCrLf
    Case 16: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^219^T4_Libre^UNDILUTED^^F|52.50|pmol/L||||F||ADMIN||19990928114634" & vbCr & Chr$(3) & "4A" & vbCrLf
    Case 17: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^219^T4_Libre^UNDILUTED^^P|145.24|Rate||||F||ADMIN||19990928114634" & vbCr & Chr$(3) & "E3" & vbCrLf
    Case 18: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 19: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928115122" & vbCr & Chr$(3) & "BC" & vbCrLf
    Case 20: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 21: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADC-9435|ADC-9435^C^01|^^^274^T3_Total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "7A" & vbCrLf
    Case 22: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^274^T3_Total^UNDILUTED^^F|6.08|nmol/L||||F||ADMIN||19990928114728" & vbCr & Chr$(3) & "32" & vbCrLf
    Case 23: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^274^T3_Total^UNDILUTED^^P|107.35|Rate||||F||ADMIN||19990928114728" & vbCr & Chr$(3) & "FB" & vbCrLf
    Case 24: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 25: objAutoAn.AutoRecepcion Chr$(2) & "3H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928115125" & vbCr & Chr$(3) & "BD" & vbCrLf
    Case 26: objAutoAn.AutoRecepcion Chr$(2) & "4P|1||||" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 27: objAutoAn.AutoRecepcion Chr$(2) & "5O|1|ADC-9919|ADC-9919^C^06|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "E6" & vbCrLf
    Case 28: objAutoAn.AutoRecepcion Chr$(2) & "6R|1|^^^208^hTSH_II^UNDILUTED^^F|3.439|uIU/mL||||F||ADMIN||19990928114513" & vbCr & Chr$(3) & "8A" & vbCrLf
    Case 29: objAutoAn.AutoRecepcion Chr$(2) & "7R|2|^^^208^hTSH_II^UNDILUTED^^P|130.46|Rate||||F||ADMIN||19990928114513" & vbCr & Chr$(3) & "52" & vbCrLf
    Case 30: objAutoAn.AutoRecepcion Chr$(2) & "0L|1" & vbCr & Chr$(3) & "39" & vbCrLf
    
    Case 31: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928115127" & vbCr & Chr$(3) & "BD" & vbCrLf
    Case 32: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 33: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADC-9919|ADC-9919^C^06|^^^245^T4_total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "A8" & vbCrLf
    Case 34: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^245^T4_total^UNDILUTED^^F|168.44|nmol/L||||F||ADMIN||19990928113718" & vbCr & Chr$(3) & "BC" & vbCrLf
    Case 35: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^245^T4_total^UNDILUTED^^P|224.23|mP||||F||ADMIN||19990928113718" & vbCr & Chr$(3) & "4A" & vbCrLf
    Case 36: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 37: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120534" & vbCr & Chr$(3) & "BB" & vbCrLf
    Case 38: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 39: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADC-9696|ADC-9696^C^02|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "50" & vbCrLf
    Case 40: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^452^CA_19-9^UNDILUTED^^F|29.89|U/mL||||F||ADMIN||19990928115229" & vbCr & Chr$(3) & "44" & vbCrLf
    Case 41: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^452^CA_19-9^UNDILUTED^^P|69.68|Rate||||F||ADMIN||19990928115229" & vbCr & Chr$(3) & "A0" & vbCrLf
    Case 42: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 43: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120537" & vbCr & Chr$(3) & "C4" & vbCrLf
    Case 44: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 45: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADC-9790|ADC-9790^C^03|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "45" & vbCrLf
    Case 46: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^452^CA_19-9^UNDILUTED^^F|0.00|U/mL||||F||ADMIN||19990928115323" & vbCr & Chr$(3) & "F1" & vbCrLf
    Case 47: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^452^CA_19-9^UNDILUTED^^P|3.56|Rate||||F||ADMIN||19990928115323" & vbCr & Chr$(3) & "5A" & vbCrLf
    Case 48: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 49: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120539" & vbCr & Chr$(3) & "C4" & vbCrLf
    Case 50: objAutoAn.AutoRecepcion Chr$(2) & "6P|1|||ADC-9872D100|" & vbCr & Chr$(3) & "D7" & vbCrLf
    Case 51: objAutoAn.AutoRecepcion Chr$(2) & "7O|1||ADC-9872D100^C^04|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "54" & vbCrLf
    Case 52: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^452^CA_19-9^UNDILUTED^^F|113.99|U/mL||||F||ADMIN||19990928115421" & vbCr & Chr$(3) & "65" & vbCrLf
    Case 53: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^452^CA_19-9^UNDILUTED^^P|272.49|Rate||||F||ADMIN||19990928115421" & vbCr & Chr$(3) & "C1" & vbCrLf
    Case 54: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 55: objAutoAn.AutoRecepcion Chr$(2) & "3H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120541" & vbCr & Chr$(3) & "BB" & vbCrLf
    Case 56: objAutoAn.AutoRecepcion Chr$(2) & "4P|1||||" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 57: objAutoAn.AutoRecepcion Chr$(2) & "5O|1|ADC-9889|ADC-9889^C^05|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "5D" & vbCrLf
    Case 58: objAutoAn.AutoRecepcion Chr$(2) & "6R|1|^^^452^CA_19-9^UNDILUTED^^F|0.00|U/mL||||F||ADMIN||19990928115438" & vbCr & Chr$(3) & "FC" & vbCrLf
    Case 59: objAutoAn.AutoRecepcion Chr$(2) & "7R|2|^^^452^CA_19-9^UNDILUTED^^P|3.19|Rate||||F||ADMIN||19990928115438" & vbCr & Chr$(3) & "64" & vbCrLf
    Case 60: objAutoAn.AutoRecepcion Chr$(2) & "0L|1" & vbCr & Chr$(3) & "39" & vbCrLf
    
    Case 61: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120544" & vbCr & Chr$(3) & "BC" & vbCrLf
    Case 62: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 63: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0087|ADD-0087^C^07|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "39" & vbCrLf
    Case 64: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^452^CA_19-9^UNDILUTED^^F|0.00|U/mL||||F||ADMIN||19990928115629" & vbCr & Chr$(3) & "FC" & vbCrLf
    Case 65: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^452^CA_19-9^UNDILUTED^^P|3.71|Rate||||F||ADMIN||19990928115629" & vbCr & Chr$(3) & "62" & vbCrLf
    Case 66: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 67: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120546" & vbCr & Chr$(3) & "C4" & vbCrLf
    Case 68: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 69: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADC-9919|ADC-9919^C^06|^^^274^T3_Total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "87" & vbCrLf
    Case 70: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^274^T3_Total^UNDILUTED^^F|0.99|nmol/L||||F||ADMIN||19990928115101" & vbCr & Chr$(3) & "2A" & vbCrLf
    Case 71: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^274^T3_Total^UNDILUTED^^P|380.86|Rate||||F||ADMIN||19990928115101" & vbCr & Chr$(3) & "F8" & vbCrLf
    Case 72: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 73: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120549" & vbCr & Chr$(3) & "C5" & vbCrLf
    Case 74: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 75: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADD-0185|ADD-0185^C^08|^^^219^T4_Libre^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "5F" & vbCrLf
    Case 76: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^219^T4_Libre^UNDILUTED^^F|18.87|pmol/L||||F||ADMIN||19990928115203" & vbCr & Chr$(3) & "4D" & vbCrLf
    Case 77: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^219^T4_Libre^UNDILUTED^^P|444.05|Rate||||F||ADMIN||19990928115203" & vbCr & Chr$(3) & "DB" & vbCrLf
    Case 78: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 79: objAutoAn.AutoRecepcion Chr$(2) & "3H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120551" & vbCr & Chr$(3) & "BC" & vbCrLf
    Case 80: objAutoAn.AutoRecepcion Chr$(2) & "4P|1||||" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 81: objAutoAn.AutoRecepcion Chr$(2) & "5O|1|ADD-0241|ADD-0241^C^09|^^^219^T4_Libre^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "50" & vbCrLf
    Case 82: objAutoAn.AutoRecepcion Chr$(2) & "6R|1|^^^219^T4_Libre^UNDILUTED^^F|11.02|pmol/L||||F||ADMIN||19990928115531" & vbCr & Chr$(3) & "43" & vbCrLf
    Case 83: objAutoAn.AutoRecepcion Chr$(2) & "7R|2|^^^219^T4_Libre^UNDILUTED^^P|723.76|Rate||||F||ADMIN||19990928115531" & vbCr & Chr$(3) & "ED" & vbCrLf
    Case 84: objAutoAn.AutoRecepcion Chr$(2) & "0L|1" & vbCr & Chr$(3) & "39" & vbCrLf
    
    Case 85: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120554" & vbCr & Chr$(3) & "BD" & vbCrLf
    Case 86: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 87: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0241|ADD-0241^C^09|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "BF" & vbCrLf
    Case 88: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^208^hTSH_II^UNDILUTED^^F|11.470|uIU/mL||||F||ADMIN||19990928115131" & vbCr & Chr$(3) & "AF" & vbCrLf
    Case 89: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^208^hTSH_II^UNDILUTED^^P|371.75|Rate||||F||ADMIN||19990928115131" & vbCr & Chr$(3) & "56" & vbCrLf
    Case 90: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 91: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120556" & vbCr & Chr$(3) & "C5" & vbCrLf
    Case 92: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 93: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADD-0241|ADD-0241^C^09|^^^274^T3_Total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "62" & vbCrLf
    Case 94: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^274^T3_Total^UNDILUTED^^F|1.44|nmol/L||||F||ADMIN||19990928115519" & vbCr & Chr$(3) & "2E" & vbCrLf
    Case 95: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^274^T3_Total^UNDILUTED^^P|324.71|Rate||||F||ADMIN||19990928115519" & vbCr & Chr$(3) & "FD" & vbCrLf
    Case 96: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 97: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120559" & vbCr & Chr$(3) & "C6" & vbCrLf
    Case 98: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 99: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADD-0261|ADD-0261^C^10|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "BF" & vbCrLf
    Case 100: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^208^hTSH_II^UNDILUTED^^F|2.264|uIU/mL||||F||ADMIN||19990928115411" & vbCr & Chr$(3) & "7D" & vbCrLf
    Case 101: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^208^hTSH_II^UNDILUTED^^P|88.83|Rate||||F||ADMIN||19990928115411" & vbCr & Chr$(3) & "27" & vbCrLf
    Case 102: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 103: objAutoAn.AutoRecepcion Chr$(2) & "3H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120601" & vbCr & Chr$(3) & "B8" & vbCrLf
    Case 104: objAutoAn.AutoRecepcion Chr$(2) & "4P|1||||" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 105: objAutoAn.AutoRecepcion Chr$(2) & "5O|1|ADD-0289|ADD-0289^D^01|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "D2" & vbCrLf
    Case 106: objAutoAn.AutoRecepcion Chr$(2) & "6R|1|^^^208^hTSH_II^UNDILUTED^^F|1.325|uIU/mL||||F||ADMIN||19990928115607" & vbCr & Chr$(3) & "87" & vbCrLf
    Case 107: objAutoAn.AutoRecepcion Chr$(2) & "7R|2|^^^208^hTSH_II^UNDILUTED^^P|54.09|Rate||||F||ADMIN||19990928115607" & vbCr & Chr$(3) & "2B" & vbCrLf
    Case 108: objAutoAn.AutoRecepcion Chr$(2) & "0L|1" & vbCr & Chr$(3) & "39" & vbCrLf
    
    Case 109: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120604" & vbCr & Chr$(3) & "B9" & vbCrLf
    Case 110: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 111: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0304|ADD-0304^D^03|^^^219^T4_Libre^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "49" & vbCrLf
    Case 112: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^219^T4_Libre^UNDILUTED^^F|13.63|pmol/L||||F||ADMIN||19990928120133" & vbCr & Chr$(3) & "44" & vbCrLf
    Case 113: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^219^T4_Libre^UNDILUTED^^P|609.22|Rate||||F||ADMIN||19990928120133" & vbCr & Chr$(3) & "DF" & vbCrLf
    Case 114: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 115: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120607" & vbCr & Chr$(3) & "C2" & vbCrLf
    Case 116: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 117: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADD-0304|ADD-0304^D^03|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "B8" & vbCrLf
    Case 118: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^208^hTSH_II^UNDILUTED^^F|0.143|uIU/mL||||F||ADMIN||19990928120307" & vbCr & Chr$(3) & "79" & vbCrLf
    Case 119: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^208^hTSH_II^UNDILUTED^^P|8.07|Rate||||F||ADMIN||19990928120307" & vbCr & Chr$(3) & "ED" & vbCrLf
    Case 120: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 121: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120609" & vbCr & Chr$(3) & "C2" & vbCrLf
    Case 122: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 123: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADD-0304|ADD-0304^D^03|^^^274^T3_Total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "63" & vbCrLf
    Case 124: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^274^T3_Total^UNDILUTED^^F|1.18|nmol/L||||F||ADMIN||19990928120227" & vbCr & Chr$(3) & "25" & vbCrLf
    Case 125: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^274^T3_Total^UNDILUTED^^P|355.76|Rate||||F||ADMIN||19990928120227" & vbCr & Chr$(3) & "FC" & vbCrLf
    Case 126: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 127: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120714" & vbCr & Chr$(3) & "BB" & vbCrLf
    Case 128: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 129: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0344|ADD-0344^D^04|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "C3" & vbCrLf
    Case 130: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^208^hTSH_II^UNDILUTED^^F|2.111|uIU/mL||||F||ADMIN||19990928120414" & vbCr & Chr$(3) & "77" & vbCrLf
    Case 131: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^208^hTSH_II^UNDILUTED^^P|83.28|Rate||||F||ADMIN||19990928120414" & vbCr & Chr$(3) & "24" & vbCrLf
    Case 132: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 133: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120715" & vbCr & Chr$(3) & "C2" & vbCrLf
    Case 134: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 135: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADD-0344|ADD-0344^D^04|^^^219^T4_Libre^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "50" & vbCrLf
    Case 136: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^219^T4_Libre^UNDILUTED^^F|14.40|pmol/L||||F||ADMIN||19990928120435" & vbCr & Chr$(3) & "43" & vbCrLf
    Case 137: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^219^T4_Libre^UNDILUTED^^P|580.02|Rate||||F||ADMIN||19990928120435" & vbCr & Chr$(3) & "DE" & vbCrLf
    Case 138: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 139: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120718" & vbCr & Chr$(3) & "C3" & vbCrLf
    Case 140: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 141: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADD-0344|ADD-0344^D^04|^^^274^T3_Total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "6C" & vbCrLf
    Case 142: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^274^T3_Total^UNDILUTED^^F|1.06|nmol/L||||F||ADMIN||19990928120331" & vbCr & Chr$(3) & "1E" & vbCrLf
    Case 143: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^274^T3_Total^UNDILUTED^^P|370.81|Rate||||F||ADMIN||19990928120331" & vbCr & Chr$(3) & "F1" & vbCrLf
    Case 144: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 145: objAutoAn.AutoRecepcion Chr$(2) & "3H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928120721" & vbCr & Chr$(3) & "BB" & vbCrLf
    Case 146: objAutoAn.AutoRecepcion Chr$(2) & "4P|1||||" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 147: objAutoAn.AutoRecepcion Chr$(2) & "5O|1|ADD-0411|ADD-0411^D^06|^^^245^T4_total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "81" & vbCrLf
    Case 148: objAutoAn.AutoRecepcion Chr$(2) & "6R|1|^^^245^T4_total^UNDILUTED^^F|86.33|nmol/L||||F||ADMIN||19990928115420" & vbCr & Chr$(3) & "83" & vbCrLf
    Case 149: objAutoAn.AutoRecepcion Chr$(2) & "7R|2|^^^245^T4_total^UNDILUTED^^P|269.80|mP||||F||ADMIN||19990928115420" & vbCr & Chr$(3) & "50" & vbCrLf
    Case 150: objAutoAn.AutoRecepcion Chr$(2) & "0L|1" & vbCr & Chr$(3) & "39" & vbCrLf
    
    Case 151: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122025" & vbCr & Chr$(3) & "B8" & vbCrLf
    Case 152: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 153: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0185|ADD-0185^C^08|^^^282^T3_Libre^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "5A" & vbCrLf
    Case 154: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^282^T3_Libre^UNDILUTED^^F|3.81|pmol/L||||F||ADMIN||19990928120837" & vbCr & Chr$(3) & "1D" & vbCrLf
    Case 155: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^282^T3_Libre^UNDILUTED^^P|667.40|Rate||||F||ADMIN||19990928120837" & vbCr & Chr$(3) & "ED" & vbCrLf
    Case 156: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 157: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122027" & vbCr & Chr$(3) & "C0" & vbCrLf
    Case 158: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 159: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADD-0374|ADD-0374^D^05|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "34" & vbCrLf
    Case 160: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^452^CA_19-9^UNDILUTED^^F|2.82|U/mL||||F||ADMIN||19990928121207" & vbCr & Chr$(3) & "FB" & vbCrLf
    Case 161: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^452^CA_19-9^UNDILUTED^^P|12.63|Rate||||F||ADMIN||19990928121207" & vbCr & Chr$(3) & "86" & vbCrLf
    Case 162: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 163: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122029" & vbCr & Chr$(3) & "C0" & vbCrLf
    Case 164: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 165: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADD-0297|ADD-0297^D^02|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "3F" & vbCrLf
    Case 166: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^452^CA_19-9^UNDILUTED^^F|37.55|U/mL||||F||ADMIN||19990928120531" & vbCr & Chr$(3) & "30" & vbCrLf
    Case 167: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^452^CA_19-9^UNDILUTED^^P|88.13|Rate||||F||ADMIN||19990928120531" & vbCr & Chr$(3) & "8B" & vbCrLf
    Case 168: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 169: objAutoAn.AutoRecepcion Chr$(2) & "3H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122032" & vbCr & Chr$(3) & "B8" & vbCrLf
    Case 170: objAutoAn.AutoRecepcion Chr$(2) & "4P|1||||" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 171: objAutoAn.AutoRecepcion Chr$(2) & "5O|1|ADD-0411|ADD-0411^D^06|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "BD" & vbCrLf
    Case 172: objAutoAn.AutoRecepcion Chr$(2) & "6R|1|^^^208^hTSH_II^UNDILUTED^^F|1.878|uIU/mL||||F||ADMIN||19990928120635" & vbCr & Chr$(3) & "91" & vbCrLf
    Case 173: objAutoAn.AutoRecepcion Chr$(2) & "7R|2|^^^208^hTSH_II^UNDILUTED^^P|74.73|Rate||||F||ADMIN||19990928120635" & vbCr & Chr$(3) & "2B" & vbCrLf
    Case 174: objAutoAn.AutoRecepcion Chr$(2) & "0L|1" & vbCr & Chr$(3) & "39" & vbCrLf
    
    Case 175: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122034" & vbCr & Chr$(3) & "B8" & vbCrLf
    Case 176: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 177: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0411|ADD-0411^D^06|^^^274^T3_Total^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "60" & vbCrLf
    Case 178: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^274^T3_Total^UNDILUTED^^F|1.28|nmol/L||||F||ADMIN||19990928120754" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 179: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^274^T3_Total^UNDILUTED^^P|343.13|Rate||||F||ADMIN||19990928120754" & vbCr & Chr$(3) & "F9" & vbCrLf
    Case 180: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 181: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122036" & vbCr & Chr$(3) & "C0" & vbCrLf
    Case 182: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 183: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADD-0418|ADD-0418^D^07|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "34" & vbCrLf
    Case 184: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^452^CA_19-9^UNDILUTED^^F|2.67|U/mL||||F||ADMIN||19990928121348" & vbCr & Chr$(3) & "04" & vbCrLf
    Case 185: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^452^CA_19-9^UNDILUTED^^P|12.32|Rate||||F||ADMIN||19990928121348" & vbCr & Chr$(3) & "88" & vbCrLf
    Case 186: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 187: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122039" & vbCr & Chr$(3) & "C1" & vbCrLf
    Case 188: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 189: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADD-0451|ADD-0451^D^08|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "35" & vbCrLf
    Case 190: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^452^CA_19-9^UNDILUTED^^F|0.00|U/mL||||F||ADMIN||19990928121637" & vbCr & Chr$(3) & "F4" & vbCrLf
    Case 191: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^452^CA_19-9^UNDILUTED^^P|3.46|Rate||||F||ADMIN||19990928121637" & vbCr & Chr$(3) & "5C" & vbCrLf
    Case 192: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 193: objAutoAn.AutoRecepcion Chr$(2) & "3H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122041" & vbCr & Chr$(3) & "B8" & vbCrLf
    Case 194: objAutoAn.AutoRecepcion Chr$(2) & "4P|1||||" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 195: objAutoAn.AutoRecepcion Chr$(2) & "5O|1|ADD-0475|ADD-0475^D^09|^^^208^hTSH_II^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "D4" & vbCrLf
    Case 196: objAutoAn.AutoRecepcion Chr$(2) & "6R|1|^^^208^hTSH_II^UNDILUTED^^F|1.916|uIU/mL||||F||ADMIN||19990928120720" & vbCr & Chr$(3) & "85" & vbCrLf
    Case 197: objAutoAn.AutoRecepcion Chr$(2) & "7R|2|^^^208^hTSH_II^UNDILUTED^^P|76.11|Rate||||F||ADMIN||19990928120720" & vbCr & Chr$(3) & "20" & vbCrLf
    Case 198: objAutoAn.AutoRecepcion Chr$(2) & "0L|1" & vbCr & Chr$(3) & "39" & vbCrLf
    
    Case 199: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122044" & vbCr & Chr$(3) & "B9" & vbCrLf
    Case 200: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 201: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0543|ADD-0543^D^10|^^^25^LH^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "DB" & vbCrLf
    Case 202: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^25^LH^UNDILUTED^^F|9.59|mIU/ml||||F||ADMIN||19990928121246" & vbCr & Chr$(3) & "8E" & vbCrLf
    Case 203: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^25^LH^UNDILUTED^^P|97.65|Rate||||F||ADMIN||19990928121246" & vbCr & Chr$(3) & "47" & vbCrLf
    Case 204: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 205: objAutoAn.AutoRecepcion Chr$(2) & "7H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122046" & vbCr & Chr$(3) & "C1" & vbCrLf
    Case 206: objAutoAn.AutoRecepcion Chr$(2) & "0P|1||||" & vbCr & Chr$(3) & "2D" & vbCrLf
    Case 207: objAutoAn.AutoRecepcion Chr$(2) & "1O|1|ADD-0543|ADD-0543^D^10|^^^37^FSH^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "29" & vbCrLf
    Case 208: objAutoAn.AutoRecepcion Chr$(2) & "2R|1|^^^37^FSH^UNDILUTED^^F|6.12|mIU/ml||||F||ADMIN||19990928121723" & vbCr & Chr$(3) & "CE" & vbCrLf
    Case 209: objAutoAn.AutoRecepcion Chr$(2) & "3R|2|^^^37^FSH^UNDILUTED^^P|104.56|Rate||||F||ADMIN||19990928121723" & vbCr & Chr$(3) & "BA" & vbCrLf
    Case 210: objAutoAn.AutoRecepcion Chr$(2) & "4L|1" & vbCr & Chr$(3) & "3D" & vbCrLf
    
    Case 211: objAutoAn.AutoRecepcion Chr$(2) & "5H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122051" & vbCr & Chr$(3) & "BB" & vbCrLf
    Case 212: objAutoAn.AutoRecepcion Chr$(2) & "6P|1||||" & vbCr & Chr$(3) & "33" & vbCrLf
    Case 213: objAutoAn.AutoRecepcion Chr$(2) & "7O|1|ADD-0543|ADD-0543^D^10|^^^55^PRL_3IS^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "6A" & vbCrLf
    Case 214: objAutoAn.AutoRecepcion Chr$(2) & "0R|1|^^^55^PRL_3IS^UNDILUTED^^F|15.67|ng/mL||||F||ADMIN||19990928120917" & vbCr & Chr$(3) & "EF" & vbCrLf
    Case 215: objAutoAn.AutoRecepcion Chr$(2) & "1R|2|^^^55^PRL_3IS^UNDILUTED^^P|87.91|Rate||||F||ADMIN||19990928120917" & vbCr & Chr$(3) & "D0" & vbCrLf
    Case 216: objAutoAn.AutoRecepcion Chr$(2) & "2L|1" & vbCr & Chr$(3) & "3B" & vbCrLf
    
    Case 217: objAutoAn.AutoRecepcion Chr$(2) & "1H|\^&|||AxSYM^3.04a^2014^H1P1O1R1C1Q1L1M1|||||||P|1|19990928122554" & vbCr & Chr$(3) & "BF" & vbCrLf
    Case 218: objAutoAn.AutoRecepcion Chr$(2) & "2P|1||||" & vbCr & Chr$(3) & "2F" & vbCrLf
    Case 219: objAutoAn.AutoRecepcion Chr$(2) & "3O|1|ADD-0544|ADD-0544^E^01|^^^452^CA_19-9^UNDILUTED|R||||||||||||||||||||F" & vbCr & Chr$(3) & "31" & vbCrLf
    Case 220: objAutoAn.AutoRecepcion Chr$(2) & "4R|1|^^^452^CA_19-9^UNDILUTED^^F|33.12|U/mL||||F||ADMIN||19990928122234" & vbCr & Chr$(3) & "2B" & vbCrLf
    Case 221: objAutoAn.AutoRecepcion Chr$(2) & "5R|2|^^^452^CA_19-9^UNDILUTED^^P|77.44|Rate||||F||ADMIN||19990928122234" & vbCr & Chr$(3) & "93" & vbCrLf
    Case 222: objAutoAn.AutoRecepcion Chr$(2) & "6L|1" & vbCr & Chr$(3) & "3F" & vbCrLf
    
    Case 223: objAutoAn.AutoRecepcion Chr$(4)
  End Select
End Sub
