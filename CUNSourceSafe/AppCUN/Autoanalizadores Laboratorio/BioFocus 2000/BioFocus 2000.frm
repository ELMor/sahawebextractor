VERSION 5.00
Object = "{2037E3AD-18D6-101C-8158-221E4B551F8E}#5.0#0"; "Vsocx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "comdlg32.ocx"
Begin VB.Form frmPrincipal 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "BioFocus 2000"
   ClientHeight    =   915
   ClientLeft      =   6585
   ClientTop       =   690
   ClientWidth     =   1560
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   Icon            =   "BioFocus 2000.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   915
   ScaleWidth      =   1560
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   327681
   End
   Begin VsOcxLib.VideoSoftAwk Awk2 
      Left            =   750
      Top             =   480
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk3 
      Left            =   480
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VsOcxLib.VideoSoftAwk Awk1 
      Left            =   0
      Top             =   0
      _Version        =   327680
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private Sub BioFocus_Espera(mensaje As String)
''Identifica la llegada del caracter de comienzo de transmisi�n (comienzo del mensaje).
'On Error Resume Next
'  If Right$(mensaje, 1) = "[" Then 'Llegada del car�cter de comienzo del mensaje
'    objAutoAn.Estado = cteTRANSMISION
'    mensaje = Right$(mensaje, 1)
'  End If
'End Sub
'
'Private Sub BioFocus_Transmision(mensaje As String)
''Identifica el final del mensaje. Se comprueba el check y si es correcto se procede a la lectura de los datos. No hay que
''enviar ninguna respuesta, por tanto, si un mensaje ha llegado incorrectamente se perder� sin posibilidad de que sea
''retransmitido.
'On Error Resume Next
'
'  If Right$(mensaje, 2) = Chr$(13) & Chr$(10) Then 'Se ha identifcado el final de una transmisi�n
'    If Left$(mensaje, 2) = "[]" Then 'Indica el final de la transmisi�n
'      objAutoAn.Estado = cteESPERA
'      'cmdsalir_click
'      Salir
'    Else
'      Lectura_Datos Mid$(mensaje, 2, Len(mensaje) - 9)
'      objAutoAn.Estado = cteESPERA
'      mensaje = ""
'    End If
'  End If
'End Sub
'
'Private Sub Lectura_Datos(Datos As String)
'Dim curva As String
'Dim cortes As String
'Dim contador As Integer
'Dim contador1 As Integer
'Dim res As String
'Dim i As Integer
'Dim porcen As Single
'Dim total As Single
'Dim strPT As String
'Dim codMuestra As String
'Dim CodResultado As String
'On Error Resume Next
'
'  'Aunque se recoge tambi�n el Total de prote�nas (proteinograma en suero) y las fracciones en
'  'valor absoluto no se van a visualizar estos valores en la validaci�n ya que est�n puestos
'  'como f�rmulas
'  Awk1.FS = ","
'  Awk1 = Datos
'
'  'La matriz de resultados contiene: (1,1) cMuestra; (2,1) fecha y hora; (2,2) Total;
'  '(2,3) Ratio; (2,4)...(2,n-2) Fracciones y porcentajes; (2,n-1) curva; (2,n):puntos de corte
'  'ReDim MatrizResultados(1 To 2, 1 To Awk1.F(5) * 2 + 5)
'  'MatrizResultados(1, 1) = Trim$(Awk1.F(3)) 'ID muestra
'  codMuestra = objAutoAn.fNumeroACodigo(Trim$(Awk1.F(3)))
'  'MatrizResultados(2, 1) = Format(Now, "dd/mm/yyyy") & "-" & Format(Now, "hh:mm:ss")
'
'  'primero ponemos como total el n� que nos pasa el autoanalizador y luego si existe el la BD el correspondiente PT
'  'lo machacamos
'  'MatrizResultados(1, 2) = "TOTAL"
'  res = Awk1.F(7 + Awk1.F(5))
'  total = Val(res)
'  i = fFormatearNumero(res, ".", ",")
'
'  strPT = objAutoAn.ColMuestras(codMuestra).strProperty1
'  If IsNumeric(strPT) Then
'    Call objAutoAn.pIntroResultConCResAuto(codMuestra, "TOTAL", strPT)
'  Else
'    Call objAutoAn.pIntroResultConCResAuto(codMuestra, "TOTAL", res)
'  End If
'  'MatrizResultados(1, 3) = "RATIO"
'  res = Awk1.F(6 + Awk1.F(5))
'  i = fFormatearNumero(res, ".", ",")
'  'MatrizResultados(2, 3) = res
'  Call objAutoAn.pIntroResultConCResAuto(codMuestra, "RATIO", res)
'  contador1 = 3
'  For contador = 4 To Awk1.F(5) * 2 + 3 Step 2
'    contador1 = contador1 + 1
'    'MatrizResultados(1, contador) = "F" & contador1 - 3       'porcentajes de fracciones
'    CodResultado = "F" & contador1 - 3
'    res = Trim$(Awk1.F(2 + contador1))
'    porcen = Val(res)
'    res = CStr(Val(res))
'    i = fFormatearNumero(res, ".", ",")
'    'MatrizResultados(2, contador) = res
'    Call objAutoAn.pIntroResultConCResAuto(codMuestra, CodResultado, res)
'    'MatrizResultados(1, contador + 1) = "P" & contador1 - 3    'totales de fracciones
'    CodResultado = "P" & contador1 - 3
'    res = Format$(total * porcen / 100, "0.00")
'    'MatrizResultados(2, contador + 1) = res
'    Call objAutoAn.pIntroResultConCResAuto(codMuestra, CodResultado, res)
'  Next contador
'
'  curva = ""
'  cortes = ""
'  For contador = 1 To 350
'    Select Case UCase$(Left$(Awk1.F(contador + 49 + Awk1.F(5)), 1))
'      Case "F"
'        cortes = cortes & contador & ","
'        curva = curva & contador & "," & Awk1.F(contador + 49 + Awk1.F(5)) & ";"
'      Case "S"
'      Case Else
'        curva = curva & contador & "," & Awk1.F(contador + 49 + Awk1.F(5)) & ";"
'    End Select
'  Next contador
'  Call objAutoAn.pIntroResultConCResAuto(codMuestra, "CURVA", Left$(curva, Len(curva) - 1))
'  Call objAutoAn.pIntroResultConCResAuto(codMuestra, "CORTE", Left$(cortes, Len(cortes) - 1))
'End Sub
'
'Private Sub Lectura_Protocolo(caracter As String)
''El sistema (ordenador que comunica con el autoanalizador) puede estar en uno de estos estados:
''   Espera: no se ha establecido todav�a la comunicaci�n.
''   Transmisi�n: el autoanalizador est� mandando datos al sistema
''Cada mensaje comienza con "[" y finaliza con "]" seguido de los 4 caracteres que forman el check y de <CR><LF>.
'Static mensaje As String 'Contiene el mensaje que env�a el autoanalizador
'Dim CopiaMensaje As String 'Contiene una copia del mensaje que se est� recogiendo y es la que se env�a a otras subrutinas
'    'pudendo ser modificada en �stas. Al ser modificada la copia, tambi�n deber� modificarse el original.
'    'El hecho de utilizar esta copia se debe a que la variable Mensaje no puede modificar su contenido en otras subrutinas
'    'por estar declarada como Static en esta subrutina
'On Error Resume Next
'
'  mensaje = mensaje & caracter
'  CopiaMensaje = mensaje
'  Select Case objAutoAn.Estado
'    Case cteESPERA
'      BioFocus_Espera CopiaMensaje
'      mensaje = CopiaMensaje
'    Case cteTRANSMISION
'      BioFocus_Transmision CopiaMensaje
'      mensaje = CopiaMensaje
'  End Select
'
'End Sub

Public Function Salir() As Boolean
  Salir = (MsgBox("Desea Pasar las Muestra sin Resultados a extraidas?", vbYesNoCancel + vbQuestion, Me.Caption) = vbCancel)
  objAutoAn.EscribirLog " Pulsado Salir"
  If Not Salir Then Descargar
End Function

Public Sub Descargar()
  End
End Sub

Public Sub Resultados()
Dim Suma As Double
Dim i%, codMuestra$
Dim ArchivoResultados$
Dim CodPrueba As String
Dim CodResultado As Integer
Dim objMuestra As clsMuestra
Dim ObjResultado As clsResultado
Dim MiRegistro As String * 8500
On Error GoTo AutoAnError
'If ArchivoEPA = "" Then UbicarEPA
  ArchivoResultados = UbicarArchivo
  If ArchivoResultados <> "" Then
    Screen.MousePointer = 11: DoEvents
    Open ArchivoResultados For Random As #1 Len = 8500 'Len("c:\temp\Z0026492.epa")
    Get #1, 1, MiRegistro    ' Lee el tercer registro.
    Close #1
    Awk2 = MiRegistro
    Awk2.FS = vbCrLf
    For i = 5 To Awk2.NF Step 2
      Awk3 = Awk2.F(i)
      Awk3.FS = " "
      With objAutoAn
        codMuestra = UCase$(Left$(Awk3.F(4), 3)) & "-" & Right$(Awk3.F(4), 4)
        If .blnExistMuestra(codMuestra) Then
          Call .pIntroResultConCResAuto(codMuestra, Awk3.F(5), .fQuitarStringsDeString(Awk3.F(8), "*"))
          If Not IsNumeric(.ColMuestras(codMuestra).strproperty1) Then
            If .fCodPruebaConCResAuto(codMuestra, Awk3.F(5), CodPrueba, CodResultado) Then
              If CodPrueba = "31" Then
                .ColMuestras(codMuestra).ColMuestrasPruebas("31").ColPruebasResultados(CodResultado).strproperty1 = CStr(Val(.fQuitarStringsDeString(Awk3.F(9), "*")))
              End If
            End If
          End If
        End If
      End With
    Next i
    'Resultados Calculados Absolutos
    For Each objMuestra In objAutoAn.ColMuestras
      With objMuestra
        If .strestado = cteREALIZADA Or .strestado = cteFUERADERANGO Then
          If Not IsNull(.strproperty1) Then
            Dim Albumina As Double, res As Double
            Dim Alfa1 As Double, Alfa2 As Double
            Dim Beta As Double, Gamma As Double
            If Not IsNumeric(.strproperty1) Then
              With .ColMuestrasPruebas("31")
                Suma = CDbl(.ColPruebasResultados("1").strproperty1) _
                + CDbl(.ColPruebasResultados("3").strproperty1) _
                + CDbl(.ColPruebasResultados("5").strproperty1) _
                + CDbl(.ColPruebasResultados("7").strproperty1) _
                + CDbl(.ColPruebasResultados("9").strproperty1)
              End With
              .strproperty1 = CStr(Suma)
            End If
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, "TOTAL", .strproperty1)
            Albumina = CStr(CDbl(.ColMuestrasPruebas("31").ColPruebasResultados("1").strResultado) * CDbl(.strproperty1) * 0.01) 'albumina
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, "P1", CStr(Albumina))
            Alfa1 = CStr(CDbl(.ColMuestrasPruebas("31").ColPruebasResultados("3").strResultado) * CDbl(.strproperty1) * 0.01) 'Alfa1
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, "P2", CStr(Alfa1))
            Alfa2 = CStr(CDbl(.ColMuestrasPruebas("31").ColPruebasResultados("5").strResultado) * CDbl(.strproperty1) * 0.01) 'Alfa2
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, "P3", CStr(Alfa2))
            Beta = CStr(CDbl(.ColMuestrasPruebas("31").ColPruebasResultados("7").strResultado) * CDbl(.strproperty1) * 0.01) 'Beta
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, "P4", CStr(Beta))
            Gamma = CStr(CDbl(.ColMuestrasPruebas("31").ColPruebasResultados("9").strResultado) * CDbl(.strproperty1) * 0.01) 'Gamma
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, "P5", CStr(Gamma))
            res = Albumina / (Alfa1 + Alfa2 + Beta + Gamma)
            Call objAutoAn.pIntroResultConCResAuto(.strCodMuestra, "RATIO", CStr(res))
          End If
        End If
      End With
    Next
    Screen.MousePointer = 0: DoEvents
  End If
  Exit Sub
  
AutoAnError:
  Call objError.InternalError(Me, "Resultados. Es probable que alguna de las muestra no sea PROTEINOGRAMA codigo=31")
End Sub

Private Function UbicarArchivo() As String
Dim a As Date, i As Long
Dim t1 As Long
On Error GoTo AutoAnError

  With CommonDialog1
    .InitDir = "a:\"
    On Error Resume Next
    .filename = Dir("a:\Run*.txt")
    If Err = 52 Then .filename = Dir("c:\Run*.txt"): .InitDir = "C:\": Err = 0
    .DialogTitle = "Ubicaci�n del Archivo de Resultados"
    .Filter = "Archivos de Resultados del BioFocus (Run*.txt)|Run*.txt"
    .Flags = cdlOFNFileMustExist + cdlOFNPathMustExist + cdlOFNHideReadOnly
    .Action = 1
    UbicarArchivo = .filename
  End With
  Exit Function
  
AutoAnError:
  Call objError.InternalError(Me, "UbicarArchivo")
End Function

