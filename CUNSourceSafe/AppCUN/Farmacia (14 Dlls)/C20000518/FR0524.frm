VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form FrmEmiPed 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Emitir Pedido"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0524.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   39
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Petici�n"
      ForeColor       =   &H00FF0000&
      Height          =   1815
      Left            =   120
      TabIndex        =   81
      Top             =   5040
      Width           =   11655
      Begin Crystal.CrystalReport CrystalReport1 
         Left            =   9960
         Top             =   720
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   327680
         PrintFileLinesPerPage=   60
      End
      Begin VB.Frame Frame2 
         Caption         =   "Via de Petici�n"
         ForeColor       =   &H00FF0000&
         Height          =   735
         Left            =   4560
         TabIndex        =   82
         Top             =   600
         Visible         =   0   'False
         Width           =   4815
         Begin VB.OptionButton Option1 
            Caption         =   "Correo/Fax"
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   85
            Top             =   360
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Correo Electr�nico"
            Height          =   195
            Index           =   1
            Left            =   1560
            TabIndex        =   84
            Top             =   360
            Width           =   1815
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Tel�fono"
            Height          =   195
            Index           =   2
            Left            =   3360
            TabIndex        =   83
            Top             =   360
            Width           =   1335
         End
      End
      Begin VB.CommandButton cmdPedir 
         Caption         =   "&Pedir"
         Height          =   375
         Left            =   2760
         TabIndex        =   38
         Top             =   720
         Width           =   1695
      End
      Begin SSDataWidgets_B.SSDBCombo cboreclamar 
         Bindings        =   "FR0524.frx":000C
         Height          =   330
         Left            =   240
         TabIndex        =   37
         Tag             =   "Reclamar"
         Top             =   720
         Width           =   2130
         DataFieldList   =   "Column 0"
         _Version        =   131078
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Row.Count       =   3
         Row(0)          =   "Directo a Proveedor"
         Row(1)          =   "Delegado 1"
         Row(2)          =   "Delegado 2"
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns(0).Width=   3731
         Columns(0).Caption=   "C�digo"
         Columns(0).Name =   "C�digo"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         _ExtentX        =   3757
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483643
         DataFieldToDisplay=   "Column 0"
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Pedir a"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   36
         Left            =   240
         TabIndex        =   86
         Top             =   480
         Width           =   615
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Proveedores"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4335
      Index           =   1
      Left            =   120
      TabIndex        =   40
      Top             =   480
      Width           =   11700
      Begin TabDlg.SSTab tabTab1 
         Height          =   3855
         Index           =   0
         Left            =   120
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   360
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   6800
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0524.frx":001E
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(28)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "SSTab1"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(0)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).ControlCount=   5
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0524.frx":003A
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000B&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Proveedor"
            Top             =   360
            Width           =   840
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000B&
            DataField       =   "FR79PROVEEDOR"
            Height          =   330
            Index           =   1
            Left            =   1320
            TabIndex        =   1
            Tag             =   "Nombre Proveedor"
            Top             =   360
            Width           =   8400
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   2895
            Left            =   120
            TabIndex        =   42
            Top             =   840
            Width           =   10815
            _ExtentX        =   19076
            _ExtentY        =   5106
            _Version        =   327681
            Style           =   1
            Tabs            =   4
            TabsPerRow      =   4
            TabHeight       =   520
            TabCaption(0)   =   "Datos Generales"
            TabPicture(0)   =   "FR0524.frx":0056
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(33)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(32)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(31)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(2)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(30)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(16)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(15)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(14)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(13)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(3)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(4)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(5)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(6)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtText1(33)"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "txtText1(32)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "txtText1(31)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(3)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(30)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(17)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(16)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(15)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(14)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(4)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(5)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(6)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "txtText1(7)"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).ControlCount=   26
            TabCaption(1)   =   "Delegado 1"
            TabPicture(1)   =   "FR0524.frx":0072
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "txtText1(8)"
            Tab(1).Control(1)=   "txtText1(9)"
            Tab(1).Control(2)=   "txtText1(10)"
            Tab(1).Control(3)=   "txtText1(11)"
            Tab(1).Control(4)=   "txtText1(12)"
            Tab(1).Control(5)=   "txtText1(13)"
            Tab(1).Control(6)=   "txtText1(18)"
            Tab(1).Control(7)=   "txtText1(19)"
            Tab(1).Control(8)=   "txtText1(20)"
            Tab(1).Control(9)=   "txtText1(34)"
            Tab(1).Control(10)=   "lblLabel1(7)"
            Tab(1).Control(11)=   "lblLabel1(8)"
            Tab(1).Control(12)=   "lblLabel1(9)"
            Tab(1).Control(13)=   "lblLabel1(10)"
            Tab(1).Control(14)=   "lblLabel1(11)"
            Tab(1).Control(15)=   "lblLabel1(12)"
            Tab(1).Control(16)=   "lblLabel1(17)"
            Tab(1).Control(17)=   "lblLabel1(18)"
            Tab(1).Control(18)=   "lblLabel1(19)"
            Tab(1).Control(19)=   "lblLabel1(34)"
            Tab(1).ControlCount=   20
            TabCaption(2)   =   "Delegado 2"
            TabPicture(2)   =   "FR0524.frx":008E
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "txtText1(21)"
            Tab(2).Control(1)=   "txtText1(22)"
            Tab(2).Control(2)=   "txtText1(23)"
            Tab(2).Control(3)=   "txtText1(24)"
            Tab(2).Control(4)=   "txtText1(25)"
            Tab(2).Control(5)=   "txtText1(26)"
            Tab(2).Control(6)=   "txtText1(27)"
            Tab(2).Control(7)=   "txtText1(28)"
            Tab(2).Control(8)=   "txtText1(29)"
            Tab(2).Control(9)=   "txtText1(35)"
            Tab(2).Control(10)=   "lblLabel1(20)"
            Tab(2).Control(11)=   "lblLabel1(21)"
            Tab(2).Control(12)=   "lblLabel1(22)"
            Tab(2).Control(13)=   "lblLabel1(23)"
            Tab(2).Control(14)=   "lblLabel1(24)"
            Tab(2).Control(15)=   "lblLabel1(25)"
            Tab(2).Control(16)=   "lblLabel1(26)"
            Tab(2).Control(17)=   "lblLabel1(27)"
            Tab(2).Control(18)=   "lblLabel1(29)"
            Tab(2).Control(19)=   "lblLabel1(35)"
            Tab(2).ControlCount=   20
            TabCaption(3)   =   "Comentarios"
            TabPicture(3)   =   "FR0524.frx":00AA
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "txtText1(2)"
            Tab(3).Control(1)=   "lblLabel1(1)"
            Tab(3).ControlCount=   2
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79PAIS1"
               Height          =   330
               Index           =   7
               Left            =   7680
               TabIndex        =   7
               Tag             =   "Pa�s1"
               Top             =   1200
               Width           =   2780
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79PROVINCIA1"
               Height          =   330
               Index           =   6
               Left            =   4440
               TabIndex        =   6
               Tag             =   "Provincia1"
               Top             =   1200
               Width           =   2800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79LOCALIDAD1"
               Height          =   330
               Index           =   5
               Left            =   120
               TabIndex        =   5
               Tag             =   "Localidad1"
               Top             =   1200
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79DIRECCION1"
               Height          =   330
               Index           =   4
               Left            =   2040
               TabIndex        =   3
               Tag             =   "Direcci�n1"
               Top             =   600
               Width           =   6615
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR79COMENTARIOS"
               Height          =   1770
               Index           =   2
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   36
               Tag             =   "Comentarios"
               Top             =   840
               Width           =   10200
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79FAX1"
               Height          =   330
               Index           =   14
               Left            =   4440
               TabIndex        =   10
               Tag             =   "Fax1"
               Top             =   1800
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79TFNO1"
               Height          =   330
               Index           =   15
               Left            =   120
               TabIndex        =   8
               Tag             =   "Tel�fono1"
               Top             =   1800
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79DISTRITO1"
               Height          =   330
               Index           =   16
               Left            =   8880
               TabIndex        =   4
               Tag             =   "Distrito1"
               Top             =   600
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79EMAIL1"
               Height          =   330
               Index           =   17
               Left            =   6600
               TabIndex        =   11
               Tag             =   "E-mail1"
               Top             =   1800
               Width           =   3840
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79REPRESENT2"
               Height          =   330
               Index           =   8
               Left            =   -74880
               TabIndex        =   16
               Tag             =   "Nombre2"
               Top             =   600
               Width           =   10245
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79DIRECCION2"
               Height          =   330
               Index           =   9
               Left            =   -74880
               TabIndex        =   17
               Tag             =   "Direcci�n2"
               Top             =   1200
               Width           =   8055
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79LOCALIDAD2"
               Height          =   330
               Index           =   10
               Left            =   -74880
               TabIndex        =   19
               Tag             =   "Localidad2"
               Top             =   1800
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79PROVINCIA2"
               Height          =   330
               Index           =   11
               Left            =   -70560
               TabIndex        =   20
               Tag             =   "Provincia2"
               Top             =   1800
               Width           =   2800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79PAIS2"
               Height          =   330
               Index           =   12
               Left            =   -67440
               TabIndex        =   21
               Tag             =   "Pa�s2"
               Top             =   1800
               Width           =   2780
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79FAX2"
               Height          =   330
               Index           =   13
               Left            =   -71040
               TabIndex        =   24
               Tag             =   "Fax2"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79TFNO2"
               Height          =   330
               Index           =   18
               Left            =   -74880
               TabIndex        =   22
               Tag             =   "Tel�fono2"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79DISTRITO2"
               Height          =   330
               Index           =   19
               Left            =   -66480
               TabIndex        =   18
               Tag             =   "Distrito2"
               Top             =   1200
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79EMAIL2"
               Height          =   330
               Index           =   20
               Left            =   -69120
               TabIndex        =   25
               Tag             =   "E-mail2"
               Top             =   2400
               Width           =   4500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79REPRESENT3"
               Height          =   330
               Index           =   21
               Left            =   -74880
               TabIndex        =   26
               Tag             =   "Nombre3"
               Top             =   600
               Width           =   10245
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79DIRECCION3"
               Height          =   330
               Index           =   22
               Left            =   -74880
               TabIndex        =   27
               Tag             =   "Direcci�n3"
               Top             =   1200
               Width           =   8055
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79LOCALIDAD3"
               Height          =   330
               Index           =   23
               Left            =   -74880
               TabIndex        =   29
               Tag             =   "Localidad3"
               Top             =   1800
               Width           =   4005
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79PROVINCIA3"
               Height          =   330
               Index           =   24
               Left            =   -70560
               TabIndex        =   30
               Tag             =   "Provincia3"
               Top             =   1800
               Width           =   2800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79PAIS3"
               Height          =   330
               Index           =   25
               Left            =   -67440
               TabIndex        =   31
               Tag             =   "Pa�s3"
               Top             =   1800
               Width           =   2780
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79FAX3"
               Height          =   330
               Index           =   26
               Left            =   -71040
               TabIndex        =   34
               Tag             =   "Fax3"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79TFNO3"
               Height          =   330
               Index           =   27
               Left            =   -74880
               TabIndex        =   32
               Tag             =   "Tel�fono3"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79DISTRITO3"
               Height          =   330
               Index           =   28
               Left            =   -66480
               TabIndex        =   28
               Tag             =   "Distrito3"
               Top             =   1200
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79EMAIL3"
               Height          =   330
               Index           =   29
               Left            =   -69120
               TabIndex        =   35
               Tag             =   "E-mail3"
               Top             =   2400
               Width           =   4500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79NIF"
               Height          =   330
               Index           =   30
               Left            =   120
               TabIndex        =   2
               Tag             =   "NIF"
               Top             =   600
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79TFNO11"
               Height          =   330
               Index           =   3
               Left            =   2280
               TabIndex        =   9
               Tag             =   "Segundo Tel�fono1"
               Top             =   1800
               Width           =   2000
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79CODCONT"
               Height          =   330
               Index           =   31
               Left            =   120
               TabIndex        =   13
               Tag             =   "C�digo Contable"
               Top             =   2400
               Width           =   500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79CANTABC"
               Height          =   330
               Index           =   32
               Left            =   2280
               TabIndex        =   14
               Tag             =   "Clasificaci�n ABC Cantidad de Productos"
               Top             =   2400
               Width           =   500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79DINABC"
               Height          =   330
               Index           =   33
               Left            =   4440
               TabIndex        =   15
               Tag             =   "Clasificaci�n ABC Cantidad de Pesetas"
               Top             =   2400
               Width           =   500
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79TFNO22"
               Height          =   330
               Index           =   34
               Left            =   -72960
               TabIndex        =   23
               Tag             =   "Segundo Tel�fono2"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H8000000B&
               DataField       =   "FR79TFNO33"
               Height          =   330
               Index           =   35
               Left            =   -72960
               TabIndex        =   33
               Tag             =   "Segundo Tel�fono3"
               Top             =   2400
               Width           =   1800
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   6
               Left            =   7680
               TabIndex        =   76
               Top             =   960
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   5
               Left            =   4440
               TabIndex        =   75
               Top             =   960
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   120
               TabIndex        =   74
               Top             =   960
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Distrito"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   8880
               TabIndex        =   73
               Top             =   360
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Comentarios"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   -74760
               TabIndex        =   72
               Top             =   600
               Width           =   1050
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   13
               Left            =   120
               TabIndex        =   71
               Top             =   1560
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fax"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   14
               Left            =   4440
               TabIndex        =   70
               Top             =   1560
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   15
               Left            =   2040
               TabIndex        =   69
               Top             =   360
               Width           =   825
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "E-mail"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   16
               Left            =   6600
               TabIndex        =   68
               Top             =   1560
               Width           =   525
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   7
               Left            =   -74880
               TabIndex        =   67
               Top             =   360
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Distrito"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   -66480
               TabIndex        =   66
               Top             =   960
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   -74880
               TabIndex        =   65
               Top             =   1560
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   -70560
               TabIndex        =   64
               Top             =   1560
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   -67440
               TabIndex        =   63
               Top             =   1560
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   12
               Left            =   -74880
               TabIndex        =   62
               Top             =   2160
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fax"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   17
               Left            =   -71040
               TabIndex        =   61
               Top             =   2160
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   18
               Left            =   -74880
               TabIndex        =   60
               Top             =   960
               Width           =   825
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "E-mail"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   19
               Left            =   -69120
               TabIndex        =   59
               Top             =   2160
               Width           =   525
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Nombre"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   20
               Left            =   -74880
               TabIndex        =   58
               Top             =   360
               Width           =   660
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Distrito"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   21
               Left            =   -66480
               TabIndex        =   57
               Top             =   960
               Width           =   615
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Localidad"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   -74880
               TabIndex        =   56
               Top             =   1560
               Width           =   840
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Provincia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   23
               Left            =   -70560
               TabIndex        =   55
               Top             =   1560
               Width           =   810
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Pa�s"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   24
               Left            =   -67440
               TabIndex        =   54
               Top             =   1560
               Width           =   405
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   25
               Left            =   -74880
               TabIndex        =   53
               Top             =   2160
               Width           =   765
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Fax"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   26
               Left            =   -71040
               TabIndex        =   52
               Top             =   2160
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Direcci�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   27
               Left            =   -74880
               TabIndex        =   51
               Top             =   960
               Width           =   825
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "E-mail"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   29
               Left            =   -69120
               TabIndex        =   50
               Top             =   2160
               Width           =   525
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "NIF"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   30
               Left            =   120
               TabIndex        =   49
               Top             =   360
               Width           =   315
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "2� Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   2
               Left            =   2280
               TabIndex        =   48
               Top             =   1560
               Width           =   1005
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�digo Contable"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   31
               Left            =   120
               TabIndex        =   47
               Top             =   2160
               Width           =   1410
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   " ABC(C)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   32
               Left            =   2280
               TabIndex        =   46
               Top             =   2160
               Width           =   675
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "ABC(CE)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   33
               Left            =   4440
               TabIndex        =   45
               Top             =   2160
               Width           =   735
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "2� Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   34
               Left            =   -72960
               TabIndex        =   44
               Top             =   2160
               Width           =   1005
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "2� Tel�fono"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   35
               Left            =   -72960
               TabIndex        =   43
               Top             =   2160
               Width           =   1005
            End
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   4425
            Index           =   1
            Left            =   -74880
            TabIndex        =   77
            TabStop         =   0   'False
            Top             =   240
            Width           =   10455
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18441
            _ExtentY        =   7805
            _StockProps     =   79
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3465
            Index           =   2
            Left            =   -74880
            TabIndex        =   78
            TabStop         =   0   'False
            Top             =   120
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19076
            _ExtentY        =   6112
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Prov."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   120
            TabIndex        =   80
            Top             =   120
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Nombre Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   1320
            TabIndex        =   79
            Top             =   120
            Width           =   1590
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   12
      Top             =   7935
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   714
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmEmiPed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmEmiPed (FR0524.FRM)                                       *
'* AUTOR: JUAN CARLOS RUEDA GARCIA                                      *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: Emitir Pedido Compra                                    *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Dim mblnMandar_Correo As Boolean
'Sub Fax(CodPed As Long)
'  Dim strPed As String
'  Dim qryPed As rdoQuery
'  Dim rstPed As rdoResultset
'  Dim strNomProv As String
'  Dim strCP As String
'  Dim strDir As String
'  Dim strLoc As String
'  Dim strProvin As String
'  Dim strPais As String
'  Dim strFecPed As String
'  Dim curImpote As Currency
'
'  strPed = "SELECT FR7900.FR79PROVEEDOR, FR7900.FR79DIRECCION1, FR7900.FR79DISTRITO1, " & _
'           "       FR7900.FR79LOCALIDAD1, FR7900.FR79PROVINCIA1, FR7900.FR79PAIS1, " & _
'           "       FR7900.FR79TFNO1, FR7900.FR79FAX1, FR7900.FR79REPRESENT2, FR7900.FR79DIRECCION2, " & _
'           "       FR7900.FR79DISTRITO2, FR7900.FR79LOCALIDAD2, FR7900.FR79PROVINCIA2, " & _
'           "       FR7900.FR79PAIS2, FR7900.FR79TFNO2, FR7900.FR79FAX2, FR7900.FR79REPRESENT3, " & _
'           "       FR7900.FR79DIRECCION3, FR7900.FR79DISTRITO3, FR7900.FR79LOCALIDAD3, " & _
'           "       FR7900.FR79PROVINCIA3, FR7900.FR79PAIS3, FR7900.FR79TFNO3, FR7900.FR79FAX3, " & _
'           "       FR6200.FR62CODPEDCOMPRA, FR6200.FR62FECESTUPED, FR6200.FR62DESCPERSONALIZADA, " & _
'           "       FR6200.FR62INDREPRE, FR2500.FR73CODPRODUCTO, FR2500.FR25CANTPEDIDA, " & _
'           "       FR2500.FR25PRECIOUNIDAD, FR2500.FR25IMPORLINEA, FR2500.FR25FECPLAZENTRE, " & _
'           "       FR2500.FR25TAMENVASE, FR2500.FR25DESCUENTO, FR2500.FR25PRECNET, " & _
'           "       FR7300.FR73REFERENCIA, FR7300.FR73DESPRODUCTOPROV " & _
'           "  FROM FR7900, FR6200, FR2500, FR7300 " & _
'           " WHERE FR7900.FR79CODPROVEEDOR = FR6200.FR79CODPROVEEDOR AND " & _
'           "       FR6200.FR62CODPEDCOMPRA = FR2500.FR62CODPEDCOMPRA AND " & _
'           "       FR2500.FR73CODPRODUCTO = FR7300.FR73CODPRODUCTO AND " & _
'           "       FR6200.FR62CODPEDCOMPRA = ? " & _
'           "ORDER BY FR7900.FR79PROVEEDOR ASC, FR6200.FR62CODPEDCOMPRA ASC, FR2500.FR73CODPRODUCTO ASC "
'  Set qryPed = objApp.rdoConnect.CreateQuery("", strPed)
'  qryPed(0) = CodPed
'  Set rstPed = qryPed.OpenResultset()
'  If Not rstPed.EOF Then
'    'Tenemos pedido y comenzamos a escribir la cabacera
'    With frmprincipal.vsp
'      .Action = 3 'StartDoc
'      'Se pone la cabecera (escudo, CUN, fecha-hora)
'      .X1 = 200
'      .Y1 = 400
'      .X2 = 1000
'      .Y2 = 1200
'      .Picture = frmprincipal.imgEscudo.Picture
'
'      .FontName = "Arial"
'      .FontSize = 10
'      .FontBold = False
'      .CurrentX = 1200
'      .CurrentY = 700
'      .Text = "Cl�nica Universitaria de Navarra        Tfno.:948 35 54 00 Ext.4132 Fax: 948 17 52 78"
'
'      .CurrentX = 8500
'      .CurrentY = 600
'      .Text = "Apartado 4209"
'      .CurrentX = 8500
'      .CurrentY = 1000
'      .Text = "31080 PAMPLONA"
'
'      .CurrentY = 1500
'      .FontName = "Arial"
'      .FontSize = 30
'      .FontBold = True
'      .TableBorder = 7
'      .tablepentb = 20
'      .tablepenlr = 20
'      .Table = "^_10000;Pedido de Compras"
'
'      'Se pone el proveedor
'      If Not IsNull(rstPed.rdoColumns("FR62INDREPRE").Value) Then
'        If rstPed.rdoColumns("FR62INDREPRE").Value = 1 Then
'          strNomProv = rstPed.rdoColumns("FR79PROVEEDOR").Value
'          If Not IsNull(rstPed.rdoColumns("FR79DIRECCION1").Value) Then
'            strDir = rstPed.rdoColumns("FR79DIRECCION1").Value
'          Else
'            strDir = " "
'          End If
'          If Not IsNull(rstPed.rdoColumns("FR79DISTRITO1").Value) Then
'            strCP = rstPed.rdoColumns("FR79DISTRITO1").Value
'          Else
'            strCP = " "
'          End If
'          If Not IsNull(rstPed.rdoColumns("FR79LOCALIDAD1").Value) Then
'            strLoc = rstPed.rdoColumns("FR79LOCALIDAD1").Value
'          Else
'            strLoc = " "
'          End If
'          If Not IsNull(rstPed.rdoColumns("FR79PROVINCIA1").Value) Then
'            strProvin = rstPed.rdoColumns("FR79PROVINCIA1").Value
'          Else
'            strProvin = " "
'          End If
'        Else
'          If rstPed.rdoColumns("FR62INDREPRE").Value = 2 Then
'            If Not IsNull(rstPed.rdoColumns("FR79REPRESENT2").Value) Then
'              strNomProv = rstPed.rdoColumns("FR79REPRESENT2").Value
'              If Not IsNull(rstPed.rdoColumns("FR79DIRECCION2").Value) Then
'                strDir = rstPed.rdoColumns("FR79DIRECCION2").Value
'              Else
'                strDir = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79DISTRITO2").Value) Then
'                strCP = rstPed.rdoColumns("FR79DISTRITO2").Value
'              Else
'                strCP = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79LOCALIDAD2").Value) Then
'                strLoc = rstPed.rdoColumns("FR79LOCALIDAD2").Value
'              Else
'                strLoc = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79PROVINCIA2").Value) Then
'                strProvin = rstPed.rdoColumns("FR79PROVINCIA2").Value
'              Else
'                strProvin = " "
'              End If
'            Else
'              strNomProv = rstPed.rdoColumns("FR79PROVEEDOR").Value
'              If Not IsNull(rstPed.rdoColumns("FR79DIRECCION1").Value) Then
'                strDir = rstPed.rdoColumns("FR79DIRECCION1").Value
'              Else
'                strDir = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79DISTRITO1").Value) Then
'                strCP = rstPed.rdoColumns("FR79DISTRITO1").Value
'              Else
'                strCP = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79LOCALIDAD1").Value) Then
'                strLoc = rstPed.rdoColumns("FR79LOCALIDAD1").Value
'              Else
'                strLoc = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79PROVINCIA1").Value) Then
'                strProvin = rstPed.rdoColumns("FR79PROVINCIA1").Value
'              Else
'                strProvin = " "
'              End If
'            End If
'          Else
'            If Not IsNull(rstPed.rdoColumns("FR79REPRESENT3").Value) Then
'              strNomProv = rstPed.rdoColumns("FR79REPRESENT3").Value
'              If Not IsNull(rstPed.rdoColumns("FR79DIRECCION3").Value) Then
'                strDir = rstPed.rdoColumns("FR79DIRECCION3").Value
'              Else
'                strDir = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79DISTRITO3").Value) Then
'                strCP = rstPed.rdoColumns("FR79DISTRITO3").Value
'              Else
'                strCP = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79LOCALIDAD3").Value) Then
'                strLoc = rstPed.rdoColumns("FR79LOCALIDAD3").Value
'              Else
'                strLoc = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79PROVINCIA3").Value) Then
'                strProvin = rstPed.rdoColumns("FR79PROVINCIA3").Value
'              Else
'                strProvin = " "
'              End If
'            Else
'              strNomProv = rstPed.rdoColumns("FR79PROVEEDOR").Value
'              If Not IsNull(rstPed.rdoColumns("FR79DIRECCION1").Value) Then
'                strDir = rstPed.rdoColumns("FR79DIRECCION1").Value
'              Else
'                strDir = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79DISTRITO1").Value) Then
'                strCP = rstPed.rdoColumns("FR79DISTRITO1").Value
'              Else
'                strCP = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79LOCALIDAD1").Value) Then
'                strLoc = rstPed.rdoColumns("FR79LOCALIDAD1").Value
'              Else
'                strLoc = " "
'              End If
'              If Not IsNull(rstPed.rdoColumns("FR79PROVINCIA1").Value) Then
'                strProvin = rstPed.rdoColumns("FR79PROVINCIA1").Value
'              Else
'                strProvin = " "
'              End If
'            End If
'          End If
'        End If
'      Else
'        strNomProv = rstPed.rdoColumns("FR79PROVEEDOR").Value
'        If Not IsNull(rstPed.rdoColumns("FR79DIRECCION1").Value) Then
'          strDir = rstPed.rdoColumns("FR79DIRECCION1").Value
'        Else
'          strDir = " "
'        End If
'        If Not IsNull(rstPed.rdoColumns("FR79DISTRITO1").Value) Then
'          strCP = rstPed.rdoColumns("FR79DISTRITO1").Value
'        Else
'          strCP = " "
'        End If
'        If Not IsNull(rstPed.rdoColumns("FR79LOCALIDAD1").Value) Then
'          strLoc = rstPed.rdoColumns("FR79LOCALIDAD1").Value
'        Else
'          strLoc = " "
'        End If
'        If Not IsNull(rstPed.rdoColumns("FR79PROVINCIA1").Value) Then
'          strProvin = rstPed.rdoColumns("FR79PROVINCIA1").Value
'        Else
'          strProvin = " "
'        End If
'      End If
'
'      .CurrentY = 3600
'      .CurrentX = 400
'      .FontSize = 10
'      .Text = strNomProv
'
'      .CurrentY = 3700
'      .CurrentX = 400
'      .FontSize = 10
'      .Text = strDir
'
'      strccp = strCP & strLoc
'      .CurrentY = 3800
'      .CurrentX = 400
'      .FontSize = 10
'      .Text = strCP
'
'      .CurrentY = 3900
'      .FontSize = 10
'      .CurrentX = 400
'      .Text = strProvin
'
'      .CurrentY = 4000
'      .CurrentX = 400
'      .FontSize = 10
'      .Text = "Muy Sres. nuestros. Rogamos tomen nota del siguiente PEDIDO, seg�n las condiciones que figuran en el mismo."
'
'      'Se pone el n� de pedido
'      .CurrentY = 4200
'      .CurrentX = 400
'      .FontSize = 10
'      .Text = "Pedido N�: " & strCodPed
'
'      'Se pone la fecha del pededo
'      If Not IsNull(rstPed.rdoColumns("FR62FECESTUPED").Value) Then
'        strFecPed = rstPed.rdoColumns("FR62FECESTUPED").Value
'      Else
'        strFecPed = Date
'      End If
'      .CurrentY = 4300
'      .CurrentX = 400
'      .FontSize = 10
'      .Text = "Fecha: " & strFecPed
'
'       'Se pone la cabecera de las columnas del detalle del pedido
'      .CurrentY = 4400
'      .CurrentX = 400
'      .FontSize = 10
'      .FontBold = True
'      .FontUnderline = True
'      '.TableBorder = 7
'      '.tablepentb = 20
'      '.tablepenlr = 20
'      .Text = "Referencia    Descripci�n de Art�culo                Cantidad  Enva. Precio      %Dto.  Impote      Plazo.Ent."
'      '.Text = "Referencia    Descripci�n de Art�culo                Cantidad  Enva.   Precio  %Dto.   Impote Plazo.Ent."
'
'
'      Dim strLin As String
'      Dim lngY As Long
'      Dim intLong As Integer
'      Dim intCont As Integer
'
'      lngY = 4400
'      .FontBold = False
'      .FontUnderline = False
'      curImporte = 0
'      'Se rellena el detalle del pedido
'      While Not rstPed.EOF
'        'Se pone cada una de las l�neas del detalle
'        strLin = ""
'        lngY = lngY + 100
'        .CurrentX = 400
'        intLong = 0
'        If Not IsNull(rstPed.rdoColumns("FR73REFERENCIA").Value) Then
'            intLong = Len(rstPed.rdoColumns("FR73REFERENCIA").Value)
'            strLin = rstPed.rdoColumns("FR73REFERENCIA").Value
'            For intCont = intLong To 14
'              strLin = strLin & " "
'            Next intCont
'        Else
'            For intCont = 1 To 14
'              strLin = strLin & " "
'            Next intCont
'        End If
'
'        If Not IsNull(rstPed.rdoColumns("FR73DESPRODUCTOPROV").Value) Then
'            intLong = Len(rstPed.rdoColumns("FR73DESPRODUCTOPROV").Value)
'            strLin = rstPed.rdoColumns("FR73DESPRODUCTOPROV").Value
'            For intCont = intLong To 38
'              strLin = strLin & " "
'            Next intCont
'        Else
'            For intCont = 1 To 38
'              strLin = strLin & " "
'            Next intCont
'        End If
'
'        If Not IsNull(rstPed.rdoColumns("FR25CANTPEDIDA").Value) Then
'            intLong = Len(rstPed.rdoColumns("FR25CANTPEDIDA").Value)
'            strLin = rstPed.rdoColumns("FR25CANTPEDIDA").Value
'            For intCont = intLong To 10
'              strLin = strLin & " "
'            Next intCont
'        Else
'            For intCont = 1 To 10
'              strLin = strLin & " "
'            Next intCont
'        End If
'
'        If Not IsNull(rstPed.rdoColumns("FR25TAMENVASE").Value) Then
'            intLong = Len(rstPed.rdoColumns("FR25TAMENVASE").Value)
'            strLin = rstPed.rdoColumns("FR25TAMENVASE").Value
'            For intCont = intLong To 6
'              strLin = strLin & " "
'            Next intCont
'        Else
'            For intCont = 1 To 6
'              strLin = strLin & " "
'            Next intCont
'        End If
'
'        If Not IsNull(rstPed.rdoColumns("FR25PRECNET").Value) Then
'            intLong = Len(rstPed.rdoColumns("FR25PRECNET").Value)
'            strLin = rstPed.rdoColumns("FR25PRECNET").Value
'            For intCont = intLong To 12
'              strLin = strLin & " "
'            Next intCont
'        Else
'            For intCont = 1 To 12
'              strLin = strLin & " "
'            Next intCont
'        End If
'
'        If Not IsNull(rstPed.rdoColumns("FR25DESCUENTO").Value) Then
'            intLong = Len(rstPed.rdoColumns("FR25DESCUENTO").Value)
'            strLin = rstPed.rdoColumns("FR25DESCUENTO").Value
'            For intCont = intLong To 7
'              strLin = strLin & " "
'            Next intCont
'        Else
'            For intCont = 1 To 7
'              strLin = strLin & " "
'            Next intCont
'        End If
'
'        If Not IsNull(rstPed.rdoColumns("FR25IMPORLINEA").Value) Then
'            intLong = Len(rstPed.rdoColumns("FR25IMPORLINEA").Value)
'            strLin = rstPed.rdoColumns("FR25IMPORLINEA").Value
'            curImp = curImp + CCur(objGen.ReplaceStr(rstPed.rdoColumns("FR25IMPORLINEA").Value, ".", ",", 1))
'            For intCont = intLong To 12
'              strLin = strLin & " "
'            Next intCont
'        Else
'            For intCont = 1 To 12
'              strLin = strLin & " "
'            Next intCont
'        End If
'
'        If Not IsNull(rstPed.rdoColumns("FR25FECPLAZENTRE").Value) Then
'            strLin = rstPed.rdoColumns("FR25FECPLAZENTRE").Value
'        End If
'        .CurrentY = lngY
'        .Text = strLin
'        rstPed.MoveNext
'      Wend
'      'Se escribe el resumen
'      .CurrentX = 400
'      .CurrentY = lngY + 100
'      lngY = lngY + 100
'      For intCont = 1 To 75
'         strLin = strLin & " "
'      Next intCont
'      strLin = strLin & "Total " & curImp
'      .FontSize = 10
'      .FontBold = True
'      .FontUnderline = True
'      .Text = strLin
'
'      .FontBold = False
'      .FontUnderline = False
'      lngY = lngY + 600
'      .CurrentY = lngY
'      .CurrentX = 400
'      strLin = "Observaciones: "
'      If Not IsNull(rstPed.rdoColumns("FR62DESCPERSONALIZADA").Value) Then
'        strLin = strLin & rstPed.rdoColumns("FR62DESCPERSONALIZADA").Value
'      End If
'      .Text = strLin
'
'      lngY = lngY + 100
'      .CurrentY = lngY
'      .CurrentX = 400
'      strLin = "Pongase en contacto con nosotros a trav�s de nuestro n� de Fax: 948 17 52 78"
'      .Text = strLin
'
'      lngY = lngY + 100
'      .CurrentY = lngY
'      .CurrentX = 400
'      strLin = "Por favor se ruega comuniquen cualquier retraso que puedan tener en la entrega"
'      .Text = strLin
'
'      lngY = lngY + 300
'      .CurrentY = lngY
'      .CurrentX = 400
'      strLin = "CONDICIONES GENERALES"
'      .Text = strLin
'
'      lngY = lngY + 100
'      .CurrentY = lngY
'      .CurrentX = 400
'      strLin = "Enviar Facturas por triplicado junto con el Albar�n, indicando el n� de Pedido, al Servicio de Farmacia"
'      .Text = strLin
'
'      lngY = lngY + 100
'      .CurrentY = lngY
'      .CurrentX = 400
'      strLin = "Albar�n valorado con la mercanc�a, precios sin I.V.A."
'      .Text = strLin
'
'      .Action = 6 'End document
'      .Device = frmimpresoras.lstprinters.List(frmimpresoras.defecto)
'      Unload frmimpresoras
'    End With
'  Else
'    MsgBox "No se ha encontrado ning�n pedido para enviar", vbInformation, "Enviar por Fax"
'  End If
'End Sub
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  strWhere = "{FR7900.FR79CODPROVEEDOR} = {FR6200.FR79CODPROVEEDOR} AND " & _
             "{FR6200.FR62CODPEDCOMPRA} = {FR2500.FR62CODPEDCOMPRA} AND " & _
             "{FR2500.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO} AND " & _
             "{FR6200.FR62CODPEDCOMPRA}= " & gintProveedor(2)
             
             
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


Private Sub cmdPedir_Click()

    Dim strupdate As String
    Dim blnOK As Boolean 'Si ha habido alg�n problema al realizar el pedido se pondr� a false
    Dim strDP As String
    Dim qryDP As rdoQuery
    Dim rstDP As rdoResultset
    Dim strPrd As String
    Dim qryPRD As rdoQuery
    Dim qryUpd As rdoQuery

    cmdPedir.Enabled = False
    If cboreclamar.Text = "" Then
        Call MsgBox("Debe especificar el destinatario de la reclamaci�n", vbInformation, "Aviso")
    Else
    
        'correo electr�nico
        If Option1(1).Value = True Then
            'frmMensajeCorreo.txtText1(9).Text = txtnota.Text
            Select Case cboreclamar.Text
                Case "Directo a Proveedor"
                    If txtText1(17).Text = "" Then
                        Call MsgBox("El proveedor no tiene Correo Electr�nico", vbInformation, "Aviso")
                    Else
                        strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                                    "FR95CODESTPEDCOMPRA=? where fr62codpedcompra=?"
                        Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
                        qryUpd(0) = 2
                        qryUpd(1) = gintProveedor(2)
                        qryUpd.Execute
                        qryUpd.Close
                        Set qryUpd = Nothing
                        MsgBox "El Informe ha sido anotado como Enviado", vbExclamation, "Aviso"
                        Call Introducir_Cantidad_Bonificacion
                        frmMensajeCorreo.txtText1(0).Text = txtText1(17).Text
                        gstrLlamador = "FrmEmiPed"
                        Call objsecurity.LaunchProcess("FR0533")
                        blnOK = True
                    End If
                Case "Delegado 1"
                    If txtText1(20).Text = "" Then
                        Call MsgBox("El Delegado 1 no tiene Correo Electr�nico", vbInformation, "Aviso")
                    Else
                        strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                                    "FR95CODESTPEDCOMPRA=? where fr62codpedcompra=?"
                        Set qryUpd = objApp.rdoConnect.CreateQuery("", qryUpd)
                        qryUpd(0) = 2
                        qryUpd(1) = gintProveedor(2)
                        qryUpd.Execute
                        qryUpd.Close
                        Set qryUpd = Nothing
                        MsgBox "El Informe ha sido anotado como Enviado", vbExclamation, "Aviso"
                        Call Introducir_Cantidad_Bonificacion
                        frmMensajeCorreo.txtText1(0).Text = txtText1(20).Text
                        gstrLlamador = "FrmEmiPed"
                        Call objsecurity.LaunchProcess("FR0533")
                        blnOK = True
                    End If
                Case "Delegado 2"
                    If txtText1(29).Text = "" Then
                        Call MsgBox("El Delegado 2 no tiene Correo Electr�nico", vbInformation, "Aviso")
                    Else
                        strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                                    "FR95CODESTPEDCOMPRA=? where fr62codpedcompra=?"
                        Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
                        qryUpd(0) = 2
                        qryUpd(1) = gintProveedor(2)
                        qryUpd.Execute
                        qryUpd.Close
                        Set qryUpd = Nothing
                        MsgBox "El Informe ha sido anotado como Enviado", vbExclamation, "Aviso"
                        Call Introducir_Cantidad_Bonificacion
                        frmMensajeCorreo.txtText1(0).Text = txtText1(29).Text
                        gstrLlamador = "FrmEmiPed"
                        Call objsecurity.LaunchProcess("FR0533")
                        blnOK = True
                    End If
            End Select
        End If
        
        'Correo/Fax
        If Option1(0).Value = True Then
            strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                        "FR95CODESTPEDCOMPRA=? where fr62codpedcompra=? "
            Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
            qryUpd(0) = 2
            qryUpd(1) = gintProveedor(2)
            qryUpd.Execute
            qryUpd.Close
            Set qryUpd = Nothing
            Call Introducir_Cantidad_Bonificacion
            blnOK = True
            Call Imprimir("FR5101.RPT", 1)
            MsgBox "El Informe ha sido anotado como Enviado", vbExclamation, "Aviso"
        End If
        'Telefono
        If Option1(2).Value = True Then
            strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                        "FR95CODESTPEDCOMPRA=? where fr62codpedcompra= ?"
            Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
            qryUpd(0) = 2
            qryUpd(1) = gintProveedor(2)
            qryUpd.Execute
            qryUpd.Close
            Set qryUpd = Nothing
            MsgBox "El Informe ha sido anotado como Enviado", vbExclamation, "Aviso"
            Call Introducir_Cantidad_Bonificacion
            blnOK = True
            
        End If
    End If
    cmdPedir.Enabled = True
    If blnOK Then
      'Si se ha enviado el pedido se sale de la pantalla
      'Se modifica el precio de �ltima entrada con el precio unitario
      'strDP = "SELECT * " & _
              "  FROM FR2500 " & _
              " WHERE FR62CODPEDCOMPRA = ? "
      'Set qryDP = objApp.rdoConnect.CreateQuery("", strDP)
      'qryDP(0) = gintProveedor(2)
      'Set rstDP = qryDP.OpenResultset()
      'While Not rstDP.EOF
      '  strPrd = "UPDATE FR7300 " & _
                 " SET FR73PREULTENT = ? " & _
                 " WHERE FR73CODPRODUCTO = ? "
        'Set qryPRD = objApp.rdoConnect.CreateQuery("", strPrd)
        'qryPRD(0) = objGen.ReplaceStr(rstDP.rdoColumns("FR25PRECIOUNIDAD").Value, ".", ",", 1)
        'qryPRD(1) = rstDP.rdoColumns("FR73CODPRODUCTO").Value
        'qryPRD.Execute
        'rstDP.MoveNext
      'Wend
      'qryDP.Close
      'Set qryDP = Nothing
      'Set rstDP = Nothing
      Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
    End If
End Sub

Private Sub Imprimir_Carta()
'Imprimir Carta
End Sub
Private Sub Mandar_Correo()
'Se manda el correo al Proveedor
mblnMandar_Correo = True
End Sub



Private Sub Form_Activate()
  cboreclamar.Text = cboreclamar.Columns(0).Value
  cmdPedir.SetFocus
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Proveedores"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR7900"
    
    .intAllowance = cwAllowReadOnly
    .strWhere = "fr79codproveedor=" & gintProveedor(1)
    
    Call .FormAddOrderField("FR79CODPROVEEDOR", cwAscending)
   
    Call .objPrinter.Add("FR5101", "Pedido de Compra")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Proveedor")

    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric, 3)
    Call .FormAddFilterWhere(strKey, "FR79PROVEEDOR", "Nombre Proveedor", cwString, 50)
    
    Call .FormAddFilterWhere(strKey, "FR79NIF", "NIF", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79DIRECCION1", "Direcci�n1", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DISTRITO1", "Distrito1", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79LOCALIDAD1", "Localidad1", cwString, 50)
    Call .FormAddFilterWhere(strKey, "FR79PROVINCIA1", "Provincia1", cwString, 20)
    Call .FormAddFilterWhere(strKey, "FR79PAIS1", "Pa�s1", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79TFNO1", "Tel�fono1", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79TFNO11", "2� Tel�fono1", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79FAX1", "Fax1", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79EMAIL1", "E-mail1", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79CODCONT", "C�digo Contable", cwNumeric, 4)
    Call .FormAddFilterWhere(strKey, "FR79CANTABC", "Clasificaci�n ABC por cantidad de productos", cwString, 1)
    Call .FormAddFilterWhere(strKey, "FR79DINABC", "Clasificaci�n ABC por cantidad de pesetas", cwString, 1)
    
    Call .FormAddFilterWhere(strKey, "FR79REPRESENT2", "Nombre Representante2", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DIRECCION2", "Direcci�n2", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DISTRITO2", "Distrito2", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79LOCALIDAD2", "Localidad2", cwString, 50)
    Call .FormAddFilterWhere(strKey, "FR79PROVINCIA2", "Provincia2", cwString, 20)
    Call .FormAddFilterWhere(strKey, "FR79PAIS2", "Pa�s2", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79TFNO2", "Tel�fono2", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79TFNO22", "2� Tel�fono2", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79FAX2", "Fax2", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79EMAIL2", "E-mail2", cwString, 30)
    
    Call .FormAddFilterWhere(strKey, "FR79REPRESENT3", "Nombre Representante3", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DIRECCION3", "Direcci�n3", cwString, 100)
    Call .FormAddFilterWhere(strKey, "FR79DISTRITO3", "Distrito3", cwString, 10)
    Call .FormAddFilterWhere(strKey, "FR79LOCALIDAD3", "Localidad3", cwString, 50)
    Call .FormAddFilterWhere(strKey, "FR79PROVINCIA3", "Provincia3", cwString, 20)
    Call .FormAddFilterWhere(strKey, "FR79PAIS3", "Pa�s3", cwString, 30)
    Call .FormAddFilterWhere(strKey, "FR79TFNO3", "Tel�fono3", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79TFNO33", "2� Tel�fono3", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79FAX3", "Fax3", cwString, 15)
    Call .FormAddFilterWhere(strKey, "FR79EMAIL3", "E-mail3", cwString, 30)

    Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor")
    
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(16)).blnInFind = True
    .CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(31)).blnInFind = True
    .CtrlGetInfo(txtText1(32)).blnInFind = True
    .CtrlGetInfo(txtText1(33)).blnInFind = True
    
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(18)).blnInFind = True
    .CtrlGetInfo(txtText1(34)).blnInFind = True
    .CtrlGetInfo(txtText1(19)).blnInFind = True
    .CtrlGetInfo(txtText1(20)).blnInFind = True
    .CtrlGetInfo(txtText1(21)).blnInFind = True
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(23)).blnInFind = True
    .CtrlGetInfo(txtText1(24)).blnInFind = True
    .CtrlGetInfo(txtText1(27)).blnInFind = True
    .CtrlGetInfo(txtText1(35)).blnInFind = True
    .CtrlGetInfo(txtText1(26)).blnInFind = True
    .CtrlGetInfo(txtText1(29)).blnInFind = True
    .CtrlGetInfo(txtText1(28)).blnInFind = True
    .CtrlGetInfo(txtText1(25)).blnInFind = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strOrden As String
  
  If strFormName = "Proveedores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      strWhere = " AND FR6200.FR62CODPEDCOMPRA = " = gintProveedor(2)
      strOrden = ""
      Call objPrinter.ShowReport(strWhere, strOrden)
    End If
    Set objPrinter = Nothing
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)

    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub Introducir_Cantidad_Bonificacion()
  'funci�n que introduce en FR7300 la cantidad pedida y la bonificaci�n de los productos
  'de un pedido. La bonificaci�n correspondiente se calcula previamente.
  Dim rstProd As rdoResultset
  Dim strProd As String
  Dim rstBon As rdoResultset
  Dim strBon As String
  Dim calcularbonificacion
  Dim rsta As rdoResultset
  Dim stra As String
  Dim cantpendiente
  Dim bonificacion
  Dim strupdate As String
  Dim qryUpd1 As rdoQuery
  Dim qryUpd2 As rdoQuery
  Dim qryFR73 As rdoQuery
  Dim qryFR25 As rdoQuery
  Dim blnEntro As Boolean
      
  blnEntro = False
  strProd = "SELECT * FROM FR2500 WHERE FR62CODPEDCOMPRA=?"
  Set qryFR25 = objApp.rdoConnect.CreateQuery("", strProd)
  qryFR25(0) = gintProveedor(2)
  Set rstProd = qryFR25.OpenResultset()
  strBon = "SELECT FR73BONIFICACION,FR73CANTPEND,FR73PTEBONIF " & _
             "  FROM FR7300 " & _
             "  WHERE FR73CODPRODUCTO = ? "
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strBon)
  strupdate = "UPDATE FR7300 SET FR73CANTPEND= ? " & _
              " WHERE FR73CODPRODUCTO= ? "
  Set qryUpd1 = objApp.rdoConnect.CreateQuery("", strupdate)
  strupdate = "UPDATE FR7300 SET FR73PTEBONIF= ? " & _
              " WHERE FR73CODPRODUCTO= ? "
  Set qryUpd2 = objApp.rdoConnect.CreateQuery("", strupdate)
  While Not rstProd.EOF
    blnEntro = True
    qryFR73(0) = rstProd.rdoColumns("FR73CODPRODUCTO").Value
    Set rstBon = qryFR73.OpenResultset()
    If Not IsNull(rstProd.rdoColumns("FR25BONIF").Value) Then
      If Not IsNull(rstProd.rdoColumns("FR25TAMENVASE").Value) Then
        calcularbonificacion = (rstProd.rdoColumns("FR25CANTPEDIDA").Value * _
                        rstProd.rdoColumns("FR25TAMENVASE").Value * _
                        rstProd.rdoColumns("FR25BONIF").Value) / 100
      Else
        calcularbonificacion = (rstProd.rdoColumns("FR25CANTPEDIDA").Value * _
                                rstProd.rdoColumns("FR25BONIF").Value) / 100
      End If
    Else
        calcularbonificacion = 0
    End If
    'stra = "SELECT FR73CANTPEND,FR73PTEBONIF FROM FR7300 " & _
    '       "WHERE FR73CODPRODUCTO=" & rstProd.rdoColumns("FR73CODPRODUCTO").Value
    'Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If Not IsNull(rstBon.rdoColumns("FR73CANTPEND").Value) Then
      If Not IsNull(rstProd.rdoColumns("FR25TAMENVASE").Value) Then
        'primero se pasa a decimal transformando el punto en coma
        cantpendiente = CDec(objGen.ReplaceStr(rstBon.rdoColumns("FR73CANTPEND").Value, ".", ",", 1)) _
                    + CDec(objGen.ReplaceStr((rstProd.rdoColumns("FR25CANTPEDIDA").Value) * (rstProd.rdoColumns("FR25TAMENVASE").Value), ".", ",", 1))
      Else
        cantpendiente = CDec(objGen.ReplaceStr(rstBon.rdoColumns("FR73CANTPEND").Value, ".", ",", 1)) _
                    + CDec(objGen.ReplaceStr(rstProd.rdoColumns("FR25CANTPEDIDA").Value, ".", ",", 1))
      End If
        'despu�s se transforma la coma en punto para hacer el update
        cantpendiente = objGen.ReplaceStr(cantpendiente, ",", ".", 1)
        qryUpd1(0) = Fix(cantpendiente)
        qryUpd1(1) = rstProd.rdoColumns("FR73CODPRODUCTO").Value
        qryUpd1.Execute
    Else
        qryUpd1(0) = Fix(objGen.ReplaceStr(rstProd.rdoColumns("FR25CANTPEDIDA").Value, ",", ".", 1))
        qryUpd1(1) = rstProd.rdoColumns("FR73CODPRODUCTO").Value
        qryUpd1.Execute
    End If
    'objApp.rdoConnect.Execute strupdate, 64
    If Not IsNull(rstProd.rdoColumns("FR25BONIF").Value) Then
        bonificacion = CDec(objGen.ReplaceStr(rstBon.rdoColumns("FR73PTEBONIF").Value, ".", ",", 1)) _
                      + CDec(objGen.ReplaceStr(calcularbonificacion, ".", ",", 1))
        'bonificacion = objGen.ReplaceStr(bonificacion, ",", ".", 1)
        qryUpd2(0) = Fix(bonificacion)
        qryUpd2(1) = rstProd.rdoColumns("FR73CODPRODUCTO").Value
        qryUpd2.Execute
    Else
        qryUpd2(0) = Fix(objGen.ReplaceStr(calcularbonificacion, ",", ".", 1))
        qryUpd2(1) = rstProd.rdoColumns("FR73CODPRODUCTO").Value
        qryUpd2.Execute
    End If
    'objApp.rdoConnect.Execute strupdate, 64
    rstProd.MoveNext
  Wend
  If blnEntro Then
    qryFR73.Close
    Set qryFR73 = Nothing
    Set rstBon = Nothing
  Else
    qryFR73.Close
    Set qryFR73 = Nothing
  End If
  qryUpd1.Close
  Set qryUpd1 = Nothing
  qryUpd2.Close
  Set qryUpd2 = Nothing
  qryFR25.Close
  Set qryFR25 = Nothing
  Set rstProd = Nothing
End Sub



