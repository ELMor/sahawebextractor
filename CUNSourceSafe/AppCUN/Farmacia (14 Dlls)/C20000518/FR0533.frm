VERSION 5.00
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form frmMensajeCorreo 
   Caption         =   "FARMACIA. Enviar Mensaje Correo"
   ClientHeight    =   8595
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11625
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8595
   ScaleWidth      =   11625
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSMAPI.MAPISession MAPISession1 
      Left            =   840
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.CommandButton cmdSalir 
      Caption         =   "&Salir"
      Height          =   375
      Left            =   6000
      TabIndex        =   8
      Top             =   6600
      Width           =   1935
   End
   Begin VB.CommandButton cmdEnviarMensaje 
      Caption         =   "En&viar Correo"
      Height          =   375
      Left            =   3360
      TabIndex        =   4
      Top             =   6600
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Mensaje de Correo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   5280
      Index           =   0
      Left            =   480
      TabIndex        =   0
      Top             =   720
      Width           =   10335
      Begin VB.CommandButton cmdDestCorreo 
         Caption         =   "..."
         Height          =   330
         Left            =   4560
         TabIndex        =   9
         Top             =   720
         Width           =   330
      End
      Begin VB.TextBox txtText1 
         Height          =   2490
         Index           =   9
         Left            =   360
         TabIndex        =   3
         Tag             =   "Notas Remitente"
         ToolTipText     =   "Notas Remitente"
         Top             =   1920
         Width           =   9855
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   1
         Left            =   5400
         TabIndex        =   2
         Tag             =   "Remitente"
         ToolTipText     =   "Remitente"
         Top             =   720
         Width           =   4575
      End
      Begin VB.TextBox txtText1 
         Height          =   330
         Index           =   0
         Left            =   360
         TabIndex        =   1
         Tag             =   "Destinatario"
         ToolTipText     =   "Destinatario"
         Top             =   720
         Width           =   4215
      End
      Begin VB.Label lblLabel1 
         Caption         =   "Nota para el proveedor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   360
         TabIndex        =   7
         Top             =   1680
         Width           =   2895
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Remitente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   5400
         TabIndex        =   6
         Top             =   480
         Width           =   870
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Destinatario"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   5
         Top             =   480
         Width           =   1035
      End
   End
   Begin MSMAPI.MAPIMessages MAPIMessages1 
      Left            =   120
      Top             =   7920
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   327681
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
End
Attribute VB_Name = "frmMensajeCorreo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim mblnunload As Boolean

Private Sub cmdDestCorreo_Click()
  On Error GoTo Err_Ejecutar
    MAPIMessages1.Show
    txtText1(0).Text = MAPIMessages1.RecipDisplayName
   Exit Sub
Err_Ejecutar:
  MsgBox (Error)
End Sub

Private Sub cmdEnviarMensaje_Click()
  On Error GoTo Err_Ejecutar
    MAPIMessages1.AddressCaption = txtText1(1).Text
    MAPIMessages1.RecipDisplayName = txtText1(0).Text
    MAPIMessages1.MsgNoteText = txtText1(9).Text
    MAPIMessages1.Send (True)
    Exit Sub
Err_Ejecutar:
  Call MsgBox("No ha indicado el Destinatario del Correo", vbExclamation, "Aviso")
End Sub

Private Sub cmdsalir_Click()
  Unload Me
End Sub

Private Sub Form_Activate()
  If mblnunload = True Then
    Unload Me
  End If
End Sub

Private Sub Form_Load()
On Error GoTo Err_Ejecutar
    MAPISession1.SignOn
    MAPIMessages1.MsgIndex = -1
    MAPIMessages1.SessionID = MAPISession1.SessionID
    mblnunload = False
Exit Sub
Err_Ejecutar:
  mblnunload = True
End Sub

