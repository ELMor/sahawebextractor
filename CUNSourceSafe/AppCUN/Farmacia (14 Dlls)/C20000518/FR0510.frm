VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmRedPedCom 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar Pedido de Compra"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0510.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   36
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Buscadores"
      Height          =   735
      Left            =   1800
      TabIndex        =   34
      Top             =   7320
      Width           =   8055
      Begin VB.CommandButton cmdSolicitud 
         Caption         =   "Solicitud de &Compra"
         Height          =   375
         Left            =   360
         TabIndex        =   30
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdOfertas 
         Caption         =   "Ofer&tas"
         Height          =   375
         Left            =   2280
         TabIndex        =   31
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdMedicamentos 
         Caption         =   "&Medicamentos"
         Height          =   375
         Left            =   4200
         TabIndex        =   32
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdMasDatos 
         Caption         =   "M�&s Datos"
         Height          =   375
         Left            =   6120
         TabIndex        =   33
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pedido de Compra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2805
      Index           =   0
      Left            =   0
      TabIndex        =   26
      Top             =   480
      Width           =   11865
      Begin VB.Frame Frame4 
         Height          =   1335
         Left            =   10080
         TabIndex        =   68
         Top             =   720
         Width           =   1575
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Height          =   375
            Left            =   120
            TabIndex        =   70
            Top             =   240
            Width           =   1335
         End
         Begin VB.CommandButton cmdRechazar 
            Caption         =   "Recha&zar"
            Height          =   375
            Left            =   120
            TabIndex        =   69
            Top             =   840
            Width           =   1335
         End
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   360
         Width           =   9690
         _ExtentX        =   17092
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0510.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(11)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(10)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(21)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(23)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(24)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "dtcDateCombo1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(9)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(8)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(7)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(16)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(5)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(20)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).ControlCount=   15
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0510.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "sg02cod_pid"
            Height          =   330
            HelpContextID   =   40102
            Index           =   20
            Left            =   7080
            MaxLength       =   13
            TabIndex        =   67
            Tag             =   "C�digo Peticionario"
            Top             =   480
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr95codestpedcompra"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   5040
            MaxLength       =   13
            TabIndex        =   64
            Tag             =   "Estado Pedido"
            Top             =   480
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62indrepre"
            Height          =   330
            HelpContextID   =   40102
            Index           =   16
            Left            =   3840
            MaxLength       =   13
            TabIndex        =   46
            Tag             =   "Representante"
            Top             =   480
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   240
            TabIndex        =   19
            Tag             =   "C�digo Pedido Compra"
            Top             =   360
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            DataField       =   "fr62descpersonalizada"
            Height          =   615
            Index           =   7
            Left            =   240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   23
            Tag             =   "Descripci�n Personalizada"
            Top             =   1560
            Width           =   8880
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr79codproveedor"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   240
            TabIndex        =   21
            Tag             =   "C�digo Proveedor"
            Top             =   960
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   1320
            TabIndex        =   22
            Tag             =   "Descripci�n Proveedor"
            Top             =   960
            Width           =   7770
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2085
            Index           =   0
            Left            =   -74880
            TabIndex        =   24
            Top             =   120
            Width           =   9045
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15954
            _ExtentY        =   3678
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "fr62fecpedcompra"
            Height          =   330
            Index           =   1
            Left            =   1560
            TabIndex        =   20
            Tag             =   "Fecha Pedido"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "sg02cod_pid"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   24
            Left            =   7080
            TabIndex        =   66
            Top             =   240
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "fr95codestpedcompra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   5040
            TabIndex        =   65
            Top             =   240
            Visible         =   0   'False
            Width           =   1845
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "fr62indrepre"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   3840
            TabIndex        =   60
            Top             =   240
            Visible         =   0   'False
            Width           =   1035
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   50
            Top             =   120
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1560
            TabIndex        =   49
            Top             =   120
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   240
            TabIndex        =   48
            Top             =   1320
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   240
            TabIndex        =   47
            Top             =   720
            Width           =   885
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3825
      Index           =   1
      Left            =   0
      TabIndex        =   29
      Top             =   3360
      Width           =   11865
      Begin TabDlg.SSTab tabTab1 
         Height          =   3345
         Index           =   1
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   360
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   5900
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0510.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(3)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(15)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(16)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(17)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(18)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(19)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(9)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(57)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(56)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(7)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(12)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(20)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(22)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(5)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(25)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "lblLabel1(26)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "lblLabel1(27)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "lblLabel1(28)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "lblLabel1(29)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "lblLabel1(30)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "dtcDateCombo1(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(18)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(17)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(15)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(3)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(4)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(2)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(0)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(14)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(12)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(10)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(13)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(11)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(21)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(24)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(23)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtbonificacion"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(19)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtText1(22)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "txtText1(1)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).Control(46)=   "txtText1(25)"
         Tab(0).Control(46).Enabled=   0   'False
         Tab(0).Control(47)=   "txtprecioneto"
         Tab(0).Control(47).Enabled=   0   'False
         Tab(0).Control(48)=   "txtText1(27)"
         Tab(0).Control(48).Enabled=   0   'False
         Tab(0).Control(49)=   "txtText1(26)"
         Tab(0).Control(49).Enabled=   0   'False
         Tab(0).Control(50)=   "txtText1(28)"
         Tab(0).Control(50).Enabled=   0   'False
         Tab(0).Control(51)=   "txtText1(29)"
         Tab(0).Control(51).Enabled=   0   'False
         Tab(0).Control(52)=   "Frame2"
         Tab(0).Control(52).Enabled=   0   'False
         Tab(0).Control(53)=   "txtText1(30)"
         Tab(0).Control(53).Enabled=   0   'False
         Tab(0).ControlCount=   54
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0510.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25UDESBONIF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   30
            Left            =   3720
            TabIndex        =   84
            TabStop         =   0   'False
            Tag             =   "UdesBonif"
            Top             =   1560
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   1335
            Left            =   4680
            TabIndex        =   81
            Top             =   1920
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   29
            Left            =   4800
            Locked          =   -1  'True
            TabIndex        =   83
            TabStop         =   0   'False
            Tag             =   "Recargo"
            Top             =   2760
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25DESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   28
            Left            =   1680
            TabIndex        =   15
            Tag             =   "Descuento (%)"
            Top             =   1680
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25BONIF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   26
            Left            =   1680
            TabIndex        =   16
            Tag             =   "Bonificaci�n (%)"
            Top             =   2280
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR88CODTIPIVA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   27
            Left            =   1680
            TabIndex        =   17
            Tag             =   "IVA(%)"
            Top             =   2880
            Width           =   700
         End
         Begin VB.TextBox txtprecioneto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25PRECNET"
            Height          =   330
            HelpContextID   =   30104
            Left            =   3720
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Prec. Neto|Precio Neto"
            Top             =   1080
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25TAMENVASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   25
            Left            =   9840
            TabIndex        =   10
            Tag             =   "U.E"
            Top             =   480
            Width           =   1065
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   9720
            TabIndex        =   62
            TabStop         =   0   'False
            Tag             =   "Importe"
            Top             =   2400
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25preciounidad"
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   1680
            MaxLength       =   10
            TabIndex        =   11
            Tag             =   "Precio Unidad"
            Top             =   1080
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25CODDETPEDCOMP"
            Height          =   330
            HelpContextID   =   30101
            Index           =   19
            Left            =   6720
            TabIndex        =   79
            TabStop         =   0   'False
            Tag             =   "C�digo Detalle Pedido"
            Top             =   2760
            Visible         =   0   'False
            Width           =   825
         End
         Begin VB.TextBox txtbonificacion 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   40102
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Bonificaci�n|Unidades a Bonificar"
            Top             =   1680
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   960
            Locked          =   -1  'True
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   0
            TabStop         =   0   'False
            Tag             =   "C�d.Interno"
            Top             =   480
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   7320
            TabIndex        =   14
            Tag             =   "Moneda"
            Top             =   1080
            Width           =   705
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   6120
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "U.M"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   4920
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "FF|Forma Farmac�utica"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   5520
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   6720
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Volumen"
            Top             =   480
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   480
            Width           =   1635
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr73codproducto"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   4800
            TabIndex        =   82
            TabStop         =   0   'False
            Tag             =   "C�digo de Producto"
            Top             =   2160
            Width           =   1440
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1560
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Producto"
            Top             =   480
            Width           =   3330
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   8400
            TabIndex        =   80
            TabStop         =   0   'False
            Tag             =   "Precio Unidad"
            Top             =   2880
            Visible         =   0   'False
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25cantpedida"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   120
            MaxLength       =   10
            TabIndex        =   8
            Tag             =   "Cantidad|N�mero de envases"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25imporlinea"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   5040
            TabIndex        =   13
            Tag             =   "Importe"
            Top             =   1080
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "frh8moneda"
            Height          =   330
            HelpContextID   =   40102
            Index           =   17
            Left            =   7560
            MaxLength       =   13
            TabIndex        =   77
            TabStop         =   0   'False
            Tag             =   "Moneda"
            Top             =   2160
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40102
            Index           =   18
            Left            =   8880
            MaxLength       =   13
            TabIndex        =   78
            TabStop         =   0   'False
            Tag             =   "C�dido Pedido"
            Top             =   2160
            Visible         =   0   'False
            Width           =   495
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2880
            Index           =   1
            Left            =   -74880
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   5080
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR25FECPLAZENTRE"
            Height          =   330
            Index           =   0
            Left            =   8280
            TabIndex        =   9
            Tag             =   "Plazo Entrega"
            Top             =   1080
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Recargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   30
            Left            =   4800
            TabIndex        =   76
            Top             =   2520
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descuento (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   29
            Left            =   1680
            TabIndex        =   75
            Top             =   1440
            Width           =   1605
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonificaci�n (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   28
            Left            =   1680
            TabIndex        =   74
            Top             =   2040
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "IVA (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   27
            Left            =   1680
            TabIndex        =   73
            Top             =   2640
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Neto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   26
            Left            =   3720
            TabIndex        =   72
            Top             =   840
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.E"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   25
            Left            =   9840
            TabIndex        =   71
            Top             =   240
            Width           =   330
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   9720
            TabIndex        =   63
            Top             =   2160
            Visible         =   0   'False
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Unidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   8400
            TabIndex        =   61
            Top             =   2640
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "fr62codpedcompra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   8880
            TabIndex        =   59
            Top             =   1920
            Visible         =   0   'False
            Width           =   1590
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "frh8moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   7560
            TabIndex        =   58
            Top             =   1920
            Visible         =   0   'False
            Width           =   1005
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "FR25CODDETPEDCOMP"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   6720
            TabIndex        =   57
            Top             =   2520
            Visible         =   0   'False
            Width           =   2130
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonif. (Udes)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   120
            TabIndex        =   56
            Top             =   1440
            Width           =   1125
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1560
            TabIndex        =   55
            Top             =   240
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Seg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   960
            TabIndex        =   54
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Int"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   57
            Left            =   120
            TabIndex        =   53
            Top             =   240
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Plazo Entrega"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   8280
            TabIndex        =   52
            Top             =   840
            Width           =   1785
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   7320
            TabIndex        =   51
            Top             =   840
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   5040
            TabIndex        =   45
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   5520
            TabIndex        =   44
            Top             =   240
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   6120
            TabIndex        =   43
            Top             =   240
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   6720
            TabIndex        =   42
            Top             =   240
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   8160
            TabIndex        =   41
            Top             =   240
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   4920
            TabIndex        =   40
            Top             =   1920
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Base - PVL"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1680
            TabIndex        =   39
            Top             =   840
            Width           =   1560
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   38
            Top             =   840
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   5040
            TabIndex        =   37
            Top             =   840
            Width           =   645
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   35
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRedPedCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0510.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: ABRIL 1999                                                    *
'* DESCRIPCION: Redactar Pedido de Compra                               *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mdblCodPedCom As Double
Dim mblnError As Boolean 'True si hay errores al guardar
Dim mblnNoSaltar As Boolean



Private Sub cmdAceptar_Click()
   Dim rsta As rdoResultset
   Dim sqlstr As String
   Dim strDP As String
   Dim qryDP As rdoQuery
   Dim rstDP As rdoResultset
   
   Me.Enabled = False
   If IsNumeric(txtText1(6).Text) Then
      If IsNumeric(txtText1(8).Text) Then
         If IsNumeric(txtText1(19).Text) Then
            gintProveedor(1) = txtText1(8).Text
            gintProveedor(2) = txtText1(6).Text
            Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
            objWinInfo.DataSave
            'Se comprueba que las cantidades pedidas sean mayores que cero
            strDP = "SELECT * " & _
                    "  FROM FR2500 " & _
                    " WHERE ((FR25CANTPEDIDA = ?) OR (FR25CANTPEDIDA IS NULL)) " & _
                    "   AND FR62CODPEDCOMPRA = ? "
            Set qryDP = objApp.rdoConnect.CreateQuery("", strDP)
            qryDP(0) = 0
            qryDP(1) = txtText1(6).Text
            Set rstDP = qryDP.OpenResultset()
            If Not rstDP.EOF Then
              'En el detalle del pedido hay cantidades pedidas igual a 0 � vacias
              MsgBox "En el detalle del pedido hay cantidades pedidas igual a 0. Reviselas.", vbInformation, "Enviar Pedido"
              qryDP.Close
              Set qryDP = Nothing
              Set rstDP = Nothing
              Me.Enabled = True
              Exit Sub
            End If
            qryDP.Close
            Set qryDP = Nothing
            Set rstDP = Nothing
            If mblnError = False Then
              gstrLlamador = "FrmEstPedCom"
              Call objsecurity.LaunchProcess("FR0524")
            Else
              Me.Enabled = True
              Exit Sub
            End If
            sqlstr = "SELECT FR95CODESTPEDCOMPRA FROM FR6200 WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            If Not rsta.EOF Then
               If rsta(0).Value = 2 Then
                  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
                  Call objWinInfo.DataRefresh
                  'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
               End If
            End If
            rsta.Close
            Set rsta = Nothing
         Else
            Call MsgBox("El Pedido no tiene ning�n producto", vbExclamation, "Aviso")
         End If
      Else
         Call MsgBox("No hay ning�n Proveedor", vbExclamation, "Aviso")
      End If
   Else
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
   End If
   Me.Enabled = True
End Sub

Private Sub calcular_precio_unidad()
Dim rsta As rdoResultset
Dim sqlstr As String
          
  sqlstr = "SELECT FR73PRECBASE FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text & " AND FR00CODGRPTERAP LIKE 'Q%'"
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  If Not rsta.EOF Then
    If IsNull(rsta(0).Value) Then
      Call objWinInfo.CtrlSet(txtText1(22), "")
    Else
      Call objWinInfo.CtrlSet(txtText1(22), rsta(0).Value)
    End If
    rsta.Close
    Set rsta = Nothing
  Else
    rsta.Close
    Set rsta = Nothing
    sqlstr = "SELECT FR73PVL FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If Not rsta.EOF Then
      If IsNull(rsta(0).Value) Then
        Call objWinInfo.CtrlSet(txtText1(22), "")
      Else
        Call objWinInfo.CtrlSet(txtText1(22), rsta(0).Value)
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If

End Sub

Private Sub cmdMasDatos_Click()
    tabTab1(1).Tab = 0
    cmdMasDatos.Enabled = False
    If Not IsNumeric(txtText1(0).Text) Then
      MsgBox "Debe seleccionar un producto apretando el bot�n 'Buscar'", vbInformation, "M�s Datos"
    Else
      gstrLlamador = "FrmConExi"
      gstrCP = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0535")
    End If
    cmdMasDatos.Enabled = True
    objWinInfo.DataSave
    objWinInfo.DataRefresh
End Sub

Private Sub cmdMedicamentos_Click()
  Dim aux1
  Dim rsta As rdoResultset
  Dim stra As String
  Dim intInd As Integer

    cmdMedicamentos.Enabled = False
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If Not IsNumeric(txtText1(6).Text) Or Not IsNumeric(txtText1(8).Text) Then
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
      cmdMedicamentos.Enabled = True
      Exit Sub
    End If
    
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
      If tlbToolbar1.Buttons(4).Enabled = True Then
        cmdMedicamentos.Enabled = True
        Exit Sub
      End If
    End If
    
    gstrLlamador = "FrmGenPetOfe" 'se hace lo mismo, buscar por proveedor
    gintFR501Prov = txtText1(8).Text
    gintCont = 0
    Call objsecurity.LaunchProcess("FR0502")
    gstrLlamador = ""
    gintFR501Prov = ""
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If gintCont > 0 Then
     For intInd = 1 To gintCont
      gintProdBuscado = gaListPrd(intInd, 1)
      objWinInfo.DataSave
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
      Call objWinInfo.CtrlSet(txtText1(0), gintProdBuscado)
      'Call objWinInfo.CtrlSet(txtText1(22), txtText1(4).Text)
      Call calcular_precio_unidad
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintProdBuscado
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) Then
            Call objWinInfo.CtrlSet(txtText1(25), rsta.rdoColumns("FR73TAMENVASE").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73DESCUENTO").Value) Then
            Call objWinInfo.CtrlSet(txtText1(28), rsta.rdoColumns("FR73DESCUENTO").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73BONIFICACION").Value) Then
            Call objWinInfo.CtrlSet(txtText1(26), rsta.rdoColumns("FR73BONIFICACION").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then
            Call objWinInfo.CtrlSet(txtText1(27), rsta.rdoColumns("FR88CODTIPIVA").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) And _
           Not IsNull(rsta.rdoColumns("FR73DESVIACION").Value) Then
           If rsta.rdoColumns("FR73TAMENVASE").Value > 0 And _
              rsta.rdoColumns("FR73DESVIACION").Value < 0 Then
              Call objWinInfo.CtrlSet(txtText1(3), (rsta.rdoColumns("FR73DESVIACION").Value \ rsta.rdoColumns("FR73TAMENVASE").Value) * (-1))
           Else
             If rsta.rdoColumns("FR73DESVIACION").Value > 0 Then
               Call objWinInfo.CtrlSet(txtText1(3), 0)
             Else
              Call objWinInfo.CtrlSet(txtText1(3), 0)
             End If
           End If
        Else
          Call objWinInfo.CtrlSet(txtText1(3), 1)
        End If
      End If
      If Not IsNull(rsta.rdoColumns("FR73DESCUENTO").Value) Then
         Call objWinInfo.CtrlSet(txtText1(28), rsta.rdoColumns("FR73DESCUENTO").Value)
      Else
        Call objWinInfo.CtrlSet(txtText1(28), 0)
      End If
      
      rsta.Close
      Set rsta = Nothing
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
        'aux1 = txtText1(22) * txtText1(3)
        Call calcular_precio_neto
        aux1 = txtprecioneto * txtText1(3)
        If InStr(aux1, ",") = 0 Then
          txtText1(15).Text = aux1
        Else
          txtText1(15).Text = Format(aux1, "0.###")
        End If
      End If
      Call calcular_precio_neto
      'txtText1(0).SetFocus
      txtText1(3).SetFocus
     Next intInd
    End If
    
    cmdMedicamentos.Enabled = True
    objWinInfo.DataSave
    objWinInfo.DataRefresh

End Sub

Private Sub cmdOfertas_Click()
   Dim v As Integer
   Dim aux1
   
   cmdOfertas.Enabled = False
   Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
   If Not IsNumeric(txtText1(6).Text) Or Not IsNumeric(txtText1(8).Text) Then
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
      cmdOfertas.Enabled = True
      Exit Sub
   End If
        
   If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
      If tlbToolbar1.Buttons(4).Enabled = True Then
         cmdOfertas.Enabled = True
         Exit Sub
      End If
   End If
    
    
   gstrLlamador = "FrmRedPedCom"
   gintFR501Prov = txtText1(8).Text
   gstrLlamadorOferta = "Introducir Pedido"
   ReDim gintprodbuscado1(0)
   gintprodtotal = 0
   Call objsecurity.LaunchProcess("FR0532")
   gstrLlamadorOferta = ""
   gstrLlamador = ""
   gintFR501Prov = ""
   Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
   For v = 1 To gintprodtotal
      If gintprodbuscado1(v, 0) <> "" Then
         Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
         Call objWinInfo.CtrlSet(txtText1(0), gintprodbuscado1(v, 0))
         Call objWinInfo.CtrlSet(txtText1(22), txtText1(4).Text)
         txtText1(0).SetFocus
         txtText1(3).SetFocus
      End If
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
        'aux1 = txtText1(22) * txtText1(3)
        Call calcular_precio_neto
        aux1 = txtprecioneto * txtText1(3)
        If InStr(aux1, ",") = 0 Then
          txtText1(15).Text = aux1
        Else
          txtText1(15).Text = Format(aux1, "0.###")
        End If
      End If
      Call calcular_precio_neto
      'txtText1(0).SetFocus
      txtText1(3).SetFocus
   Next v
   gintprodtotal = 0
   cmdOfertas.Enabled = True
End Sub

Private Sub cmdRechazar_Click()
   Dim strupdate As String
    
   Me.Enabled = False
   If IsNumeric(txtText1(6).Text) Then
      strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                  "FR95CODESTPEDCOMPRA=0 where fr62codpedcompra=" & txtText1(6).Text
      objApp.rdoConnect.Execute strupdate, 64
      MsgBox "Pedido Rechazado", vbExclamation, "Aviso"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      Call objWinInfo.DataRefresh
      'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
   Else
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
   End If
   Me.Enabled = True
End Sub

Private Sub cmdSolicitud_Click()
   Dim v As Integer
   Dim aux1
   
   cmdSolicitud.Enabled = False
   Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
   If Not IsNumeric(txtText1(6).Text) Or Not IsNumeric(txtText1(8).Text) Then
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
      cmdSolicitud.Enabled = True
      Exit Sub
   End If
    
   If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
      If tlbToolbar1.Buttons(4).Enabled = True Then
         cmdSolicitud.Enabled = True
         Exit Sub
      End If
   End If
    
   gstrLlamador = "FrmRedPedCom"
   gintFR501Prov = txtText1(8).Text
   ReDim gintprodbuscado1(0)
   Call objsecurity.LaunchProcess("FR0519")
   gstrLlamador = ""
   gintFR501Prov = ""
   Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
   For v = 1 To gintprodtotal
      If gintprodbuscado1(v, 0) <> "" Then
         Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
         Call objWinInfo.CtrlSet(txtText1(0), gintprodbuscado1(v, 0))
         Call objWinInfo.CtrlSet(txtText1(22), txtText1(4).Text)
'         If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
'            txtText1(15) = Format(txtText1(22).Text * txtText1(3).Text, "##,###.00")
'         Else
'            Call objWinInfo.CtrlSet(txtText1(15), 0)
'         End If
         txtText1(0).SetFocus
         txtText1(3).SetFocus
      End If
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
        'aux1 = txtText1(22) * txtText1(3)
        Call calcular_precio_neto
        aux1 = txtprecioneto * txtText1(3)
        If InStr(aux1, ",") = 0 Then
          txtText1(15).Text = aux1
        Else
          txtText1(15).Text = Format(aux1, "0.###")
        End If
      End If
      Call calcular_precio_neto
      'txtText1(0).SetFocus
      txtText1(3).SetFocus
   Next v
   gintprodtotal = 0
   cmdSolicitud.Enabled = True

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  gstrMoneda = strMonedaActual
  Call GenerarPedidoNuevo

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .blnAskPrimary = False
    .strName = "Pedidos de Compra"
    .strTable = "FR6200"
    .intAllowance = cwAllowAll
    If IsNumeric(mdblCodPedCom) Then
      .strWhere = "FR95CODESTPEDCOMPRA=1 AND FR62CODPEDCOMPRA = " & mdblCodPedCom & " "
    Else
      .strWhere = "FR95CODESTPEDCOMPRA=1 "
    End If
    
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
   
    strKey = .strDataBase & .strTable
    
    Call .FormCreateFilterWhere(strKey, "Pedidos de Compra")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR62DESCPERSONALIZADA", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR62FECPEDCOMPRA", "Fecha Pedido", cwDate)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "Proveedor", cwNumeric)
        
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR62DESCPERSONALIZADA", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "FR62FECPEDCOMPRA", "Fecha Pedido")
    Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "Proveedor")
    
  End With
  With objDetailInfo
    .strName = "Detalle Petici�n"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FR2500"
    
    .blnAskPrimary = False
    .intCursorSize = 100
    
    .intAllowance = cwAllowAll
    
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
    Call .FormAddOrderField("FR25CODDETPEDCOMP", cwAscending)
    
    Call .FormAddRelation("FR62CODPEDCOMPRA", txtText1(6))
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Pedido")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CANTPEDIDA", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25IMPORLINEA", "Importe de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25FECPLAZENTRE", "Fecha Plazo Entrega", cwDate)
    
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido")
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
End With
   
   
   With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    '.CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    '.CtrlGetInfo(txtText1(4)).blnNegotiated = False
    .CtrlGetInfo(txtText1(15)).blnInFind = True
  
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    
    .CtrlGetInfo(txtText1(21)).blnReadOnly = True
    
    .CtrlGetInfo(txtbonificacion).blnNegotiated = False
    .CtrlGetInfo(txtbonificacion).blnReadOnly = True
    'JMRL.CtrlGetInfo(txtprecioneto).blnNegotiated = False
    .CtrlGetInfo(txtprecioneto).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "FR73DESPRODUCTOPROV")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(13), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FR73DOSIS")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR93CODUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "FR73VOLUMEN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(14), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(24), "FR73CODINTFAR")
    
    ' Precio por defecto del producto
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(4), "FR73PRECIONETCOMPRA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(23), "FR73CODINTFARSEG")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(29), "FR73RECARGO")
    
    
    Call .WinRegister
    Call .WinStabilize
  End With
  
   If gintCodProveedor <> "" Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(24)) '�ltimo
   End If
   
   gintCodProveedor = ""
   gintProdBuscado = ""
    
   'grdDBGrid1(1).Columns("FF").Position = 1
   'grdDBGrid1(1).Columns("Producto").Position = 1
   ' grdDBGrid1(1).Columns("Producto").Width = 3500
    'grdDBGrid1(1).Columns("FF").Width = 825
    'grdDBGrid1(1).Columns("Cantidad").Position = 2
    'grdDBGrid1(1).Columns("Cantidad").Width = 825
    'grdDBGrid1(1).Columns("U.E").Position = 3
    'grdDBGrid1(1).Columns("U.E").Width = 660
    'grdDBGrid1(1).Columns("Precio Unidad").Position = 4
    'grdDBGrid1(1).Columns("Precio Unidad").Width = 1215
    'grdDBGrid1(1).Columns("Importe").Position = 5
    'grdDBGrid1(1).Columns("Importe").Width = 916
    'grdDBGrid1(1).Columns("Moneda").Position = 6
    'grdDBGrid1(1).Columns("Moneda").Width = 735
    'grdDBGrid1(1).Columns("Plazo Entrega").Position = 7
    'grdDBGrid1(1).Columns("Plazo Entrega").Width = 1141
    'grdDBGrid1(1).Columns("Descuento (%)").Position = 8
    'grdDBGrid1(1).Columns("Descuento (%)").Width = 976
    'grdDBGrid1(1).Columns("Bonificaci�n (%)").Position = 9
    'grdDBGrid1(1).Columns("Bonificaci�n (%)").Width = 1020
    'grdDBGrid1(1).Columns("IVA(%)").Position = 10
    'grdDBGrid1(1).Columns("IVA(%)").Width = 391
    'grdDBGrid1(1).Columns("C�dido Pedido").Visible = False
    'grdDBGrid1(1).Columns("C�dido Pedido").Width = 1696
    'grdDBGrid1(1).Columns("C�digo Detalle Pedido").Visible = False
    'grdDBGrid1(1).Columns("C�digo Detalle Pedido").Width = 2730
    'grdDBGrid1(1).Columns("C�digo de Producto").Visible = False
    'grdDBGrid1(1).Columns("C�d.Prod").Width = 1170
    
  'Call objApp.SplashOff
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

   If strFormName = "Detalle Petici�n" Then
      tlbToolbar1.Buttons(2).Visible = False 'Nuevo
      tlbToolbar1.Buttons(3).Visible = False 'Abrir
      mnuDatosOpcion(10).Visible = False
      mnuDatosOpcion(20).Visible = False
   Else
      tlbToolbar1.Buttons(2).Visible = True
      tlbToolbar1.Buttons(3).Visible = True
      mnuDatosOpcion(10).Visible = True
      mnuDatosOpcion(20).Visible = True
   End If

End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  If grdDBGrid1(1).Rows > 0 Then
  
    'grdDBGrid1(1).Columns("FF").Position = 1
    grdDBGrid1(1).Columns("Producto").Position = 1
    grdDBGrid1(1).Columns("Producto").Width = 3500
    'grdDBGrid1(1).Columns("FF").Width = 825
    grdDBGrid1(1).Columns("Cantidad").Position = 2
    grdDBGrid1(1).Columns("Cantidad").Width = 825
    grdDBGrid1(1).Columns("U.E").Position = 3
    grdDBGrid1(1).Columns("U.E").Width = 660
    grdDBGrid1(1).Columns("Precio Unidad").Position = 4
    grdDBGrid1(1).Columns("Precio Unidad").Width = 1215
    grdDBGrid1(1).Columns("Importe").Position = 5
    grdDBGrid1(1).Columns("Importe").Width = 916
    grdDBGrid1(1).Columns("Moneda").Position = 6
    grdDBGrid1(1).Columns("Moneda").Width = 735
    grdDBGrid1(1).Columns("Plazo Entrega").Position = 7
    grdDBGrid1(1).Columns("Plazo Entrega").Width = 1141
    grdDBGrid1(1).Columns("Descuento (%)").Position = 8
    grdDBGrid1(1).Columns("Descuento (%)").Width = 976
    grdDBGrid1(1).Columns("Bonificaci�n (%)").Position = 9
    grdDBGrid1(1).Columns("Bonificaci�n (%)").Width = 1020
    grdDBGrid1(1).Columns("IVA(%)").Position = 10
    grdDBGrid1(1).Columns("IVA(%)").Width = 391
    grdDBGrid1(1).Columns("C�dido Pedido").Visible = False
    'grdDBGrid1(1).Columns("C�dido Pedido").Width = 1696
    grdDBGrid1(1).Columns("C�digo Detalle Pedido").Visible = False
    'grdDBGrid1(1).Columns("C�digo Detalle Pedido").Width = 2730
    grdDBGrid1(1).Columns("C�digo de Producto").Visible = False
    'grdDBGrid1(1).Columns("C�d.Prod").Width = 1170
    txtbonificacion.Text = txtText1(30).Text
  End If

End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim strIVA As String
  Dim qryIVA As rdoQuery
  Dim rstIVA As rdoResultset
  
  If IsNumeric(txtText1(27).Text) And strFormName = "Detalle Petici�n" Then
    'Se comprueba que el IVA sea correcto
    strIVA = "SELECT * FROM FR8800 WHERE FR88CODTIPIVA = ?"
    Set qryIVA = objApp.rdoConnect.CreateQuery("", strIVA)
    qryIVA(0) = txtText1(27).Text
    Set rstIVA = qryIVA.OpenResultset()
    If rstIVA.EOF = True Then
      'El tipo de iva introducido por el usuario no es v�lido
      MsgBox "El IVA no es v�lido." & Chr(13) & Chr(13) & _
      "Dispone de una lista asociada para rellenar este valor", vbInformation, "Realizar Pedido"
      blnCancel = True
    End If
  End If
  mblnError = blnCancel
  If IsNumeric(txtText1(3).Text) And strFormName = "Detalle Petici�n" Then
    mblnError = False
  Else
    If Not IsNumeric(txtText1(3).Text) And strFormName = "Detalle Petici�n" Then
      mblnError = True
    Else
      mblnError = False
    End If
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Departamentos Realizadores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)

  Dim objField As clsCWFieldSearch
  If strFormName = "Pedidos de Compra" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     .strOrder = "ORDER BY FR79CODPROVEEDOR ASC"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo del Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n del Proveedor "

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
  End If
  
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim linea As Variant
   Dim strFec As String
   Dim rstFec As rdoResultset
   
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  
   If btnButton.Index = 30 Then
     Exit Sub
   End If
   
   Select Case btnButton.Index
   Case 2, 3, 16, 18, 19, 21, 22, 23, 24:
      If objWinInfo.objWinActiveForm.strName = "Pedidos de Compra" Then
         If Not IsNumeric(txtText1(6).Text) And Not IsNumeric(txtText1(19).Text) Then
            Call MsgBox("El Pedido no tiene ning�n producto, debe borrarlo o introducir productos", vbExclamation, "Aviso")
            Exit Sub
         Else
          If (btnButton.Index <> 2) And (btnButton.Index <> 4) And (btnButton.Index <> 6) Then
            objWinInfo.objWinActiveForm.strWhere = "FR95CODESTPEDCOMPRA=1 "
          End If
         End If
      End If
   Case 30:
      If Not IsNumeric(txtText1(6).Text) And Not IsNumeric(txtText1(19).Text) Then
         'Call MsgBox("El Pedido no tiene ning�n producto, debe borrarlo o introducir productos", vbExclamation, "Aviso")
         Exit Sub
      End If
   End Select
   
   If btnButton.Index = 2 Then
      If objWinInfo.objWinActiveForm.strName = "Pedidos de Compra" Then
         ' Nuevo Pedido
         sqlstr = "SELECT FR62CODPEDCOMPRA_SEQUENCE.nextval FROM dual"
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         Call objWinInfo.CtrlSet(txtText1(6), rsta.rdoColumns(0).Value)
         mdblCodPedCom = rsta.rdoColumns(0).Value
         If IsNumeric(mdblCodPedCom) Then
           objWinInfo.objWinActiveForm.strWhere = "FR95CODESTPEDCOMPRA=1 AND FR62CODPEDCOMPRA = " & mdblCodPedCom & " "
         Else
           objWinInfo.objWinActiveForm.strWhere = "FR95CODESTPEDCOMPRA=1 "
         End If
         rsta.Close
         Set rsta = Nothing
         strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
         Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
         Call objWinInfo.CtrlSet(dtcDateCombo1(1), rstFec.rdoColumns(0).Value)
         rstFec.Close
         Set rstFec = Nothing
         Call objWinInfo.CtrlSet(txtText1(16), 1) ' indrepre -> 1
         Call objWinInfo.CtrlSet(txtText1(5), 1) ' codestado pedido de compra -> 1
         Call objWinInfo.CtrlSet(txtText1(20), objsecurity.strUser) ' C�digo Peticionario -> sg02cod_pid
      Else
         ' Nuevo Producto
         sqlstr = "SELECT MAX(FR25CODDETPEDCOMP) FROM FR2500 WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text
         Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
         If IsNull(rsta.rdoColumns(0).Value) Then
            linea = 1
         Else
            linea = rsta.rdoColumns(0).Value + 1
         End If
         Call objWinInfo.CtrlSet(txtText1(19), linea)
         rsta.Close
         Set rsta = Nothing
         txtText1(21).Text = gstrMoneda
         strFec = "(SELECT TO_CHAR(SYSDATE+2,'DD/MM/YYYY') FROM DUAL)"
         Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
         Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
         rstFec.Close
         Set rstFec = Nothing
      End If
   End If

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
   Select Case intIndex
   Case 10
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   Case 20
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
   Case 40
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
   Case 60
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
   Case 80
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
   Case 100
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
   Case Else
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub txtbonificacion_Change()
  Call objWinInfo.CtrlSet(txtText1(30), txtbonificacion.Text)
End Sub

Private Sub txtprecioneto_Change()
  'Se calcula el importe
  Dim dblImporte

    If IsNumeric(txtprecioneto) And IsNumeric(txtText1(3).Text) Then
      dblImporte = txtprecioneto * txtText1(3)
      If InStr(dblImporte, ",") = 0 Then
        txtText1(15).Text = dblImporte
      Else
        txtText1(15).Text = Format(dblImporte, "0.###")
      End If
    End If
    

  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  If (intIndex = 7) And (objWinInfo.objWinActiveForm.blnChanged = False) And (mblnNoSaltar = False) Then
    txtText1(3).SetFocus
  End If
End Sub

Private Sub txtText1_Change(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim aux1 As Variant

  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 Then
    txtbonificacion.Text = ""
    txtprecioneto.Text = ""
    Call calcular_precio_neto
  End If
  
   If intIndex = 4 Then
      If Not IsNumeric(txtText1(22).Text) Then
         txtText1(22).Text = txtText1(4).Text
      End If
   End If
   If intIndex = 21 Then
      txtText1(17).Text = txtText1(21).Text
   End If
  
   If intIndex = 22 Or intIndex = 3 Or intIndex = 15 Or intIndex = 27 Or intIndex = 28 Then
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) And IsNumeric(txtprecioneto.Text) Then
         aux1 = txtprecioneto.Text * txtText1(3).Text
         If InStr(aux1, ",") = 0 Then
            txtText1(15).Text = aux1
         Else
            txtText1(15).Text = Format(aux1, "0.###")
         End If
      Else
         Call objWinInfo.CtrlSet(txtText1(15), 0)
      End If
   End If
  
  'cuando se introduce la cantidad de producto a pedir, se calcula la bonificaci�n
  If intIndex = 3 And IsNumeric(txtText1(0).Text) Then
    If IsNumeric(txtText1(3).Text) Then
      sqlstr = "SELECT FR73BONIFICACION FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns(0).Value) Then
           If IsNumeric(txtText1(3).Text) And IsNumeric(rsta.rdoColumns(0).Value) Then
            If IsNumeric(txtText1(25).Text) Then
              txtbonificacion.Text = Fix(((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100) * txtText1(25).Text)
            Else
              txtbonificacion.Text = Fix((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100)
            End If
           End If
        Else
            txtbonificacion.Text = 0
        End If
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If
  
  'si cambia el descuento, el IVA o el precio_unidad hay que actualizar el precio neto
  If intIndex = 28 Or intIndex = 27 Or intIndex = 22 Then
        Call calcular_precio_neto
  End If
  
  'si cambia el %bonificaci�n o el tama�o del envase que cambie la bonificaci�n
  If intIndex = 26 Or intIndex = 25 Or intIndex = 3 Then
    If IsNumeric(txtText1(3).Text) And IsNumeric(txtText1(26).Text) Then
        If IsNumeric(txtText1(25).Text) Then
          txtbonificacion.Text = Fix(((txtText1(3).Text * txtText1(26).Text) / 100) * txtText1(25).Text)
        Else
          txtbonificacion.Text = Fix((txtText1(3).Text * txtText1(26).Text) / 100)
        End If
    Else
        txtbonificacion.Text = 0
    End If
  End If
    
   
End Sub

Private Sub GenerarPedidoNuevo()
  Dim strInsertPedido As String
  Dim strInsertProducto As String
  Dim sqlstr As String
  Dim rsta As rdoResultset
  Dim vntCodPedCompra As Variant
  Dim vntCodDetPedCompra As Variant
  Dim intInd As Integer
  Dim strPN As String
  
         
   If IsNull(gintCodProveedor) Then '1
      gintCodProveedor = ""
   End If '1
   
   If IsNull(gintProdBuscado) Then '2
      gintProdBuscado = ""
   End If '2
 
   If gintCodProveedor <> "" Then '3
      ' Nuevo Pedido
      sqlstr = "SELECT FR62CODPEDCOMPRA_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      vntCodPedCompra = rsta.rdoColumns(0).Value
      rsta.Close
      Set rsta = Nothing
     
      strInsertPedido = "Insert Into FR6200  " & _
                        "  (FR62CODPEDCOMPRA,  " & _
                        "  FR62FECPEDCOMPRA,   " & _
                        "  FR95CODESTPEDCOMPRA," & _
                        "  SG02COD_PID,        " & _
                        "  SG02COD_PES,        " & _
                        "  FR62FECESTUPED,     " & _
                        "  FR62INDVALPED,      " & _
                        "  FR62DESCPERSONALIZADA, " & _
                        "  FR79CODPROVEEDOR,   " & _
                        "  FR62INDREPRE)       " & _
                        "Values              " & _
                        "  (" & vntCodPedCompra & "," & _
                        "  TO_DATE(SYSDATE)," & _
                        "  1," & _
                        "'" & objsecurity.strUser & "'," & _
                        "  NULL, " & _
                        "  TO_DATE(SYSDATE)," & _
                        "  NULL," & _
                        "  NULL," & _
                           gintCodProveedor & ",1)"
      'Debug.Print strInsertPedido
      objApp.rdoConnect.Execute strInsertPedido, 64
      mdblCodPedCom = vntCodPedCompra
      'Se realizan los insert en el detalle
        If gintProdBuscado <> "" Then '4
          'Se calcula el mayor n� de l�nea del detalle
          sqlstr = "SELECT MAX(FR25CODDETPEDCOMP) FROM FR2500 WHERE FR62CODPEDCOMPRA=" & vntCodPedCompra
          Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
          If IsNull(rsta.rdoColumns(0).Value) Then '5
            vntCodDetPedCompra = 1
          Else
            vntCodDetPedCompra = rsta.rdoColumns(0).Value + 1
          End If '5
          rsta.Close
          Set rsta = Nothing
         
          'Se establece la moneda
          If gstrMoneda = "" Then '6
            gstrMoneda = strMonedaActual
          End If '6
                   
          strInsertProducto = "INSERT INTO fr2500      " & _
                              "  (FR62CODPEDCOMPRA,   " & _
                              "  FR25CODDETPEDCOMP,   " & _
                              "  FR73CODPRODUCTO,     " & _
                              "  FR25CANTPEDIDA,      " & _
                              "  FR25PRECIOUNIDAD,    " & _
                              "  FR25IMPORLINEA,      " & _
                              "  FR25INDENTRPROGR,    " & _
                              "  FR25OBSERV,          " & _
                              "  FRH8MONEDA,          " & _
                              "  FR25CANTENTREG,      " & _
                              "  FR25INDCONT,         " & _
                              "  FR25CANTENT, FR25FECPLAZENTRE,FR25PRECNET)         " & _
                              "Values                 " & _
                              "  (" & vntCodPedCompra & "," & _
                                 vntCodDetPedCompra & "," & _
                                 gintProdBuscado & "," & _
                                gdblCant & ","
          If gstrPrecioOfertado = "" Then '7
            gstrPrecioOfertado = 0
          Else
            If Not IsNumeric(gstrPrecioOfertado) Then '8
               gstrPrecioOfertado = 0
            End If '8
          End If '7
          strPN = objGen.ReplaceStr(Format(gstrPrecioOfertado * gdblCant, "0.00"), ",", ".", 1)
          gstrPrecioOfertado = objGen.ReplaceStr(gstrPrecioOfertado, ",", ".", 1)
         
          strInsertProducto = strInsertProducto & gstrPrecioOfertado & ","
          strInsertProducto = strInsertProducto & _
                              "0,               " & _
                              "  NULL,NULL,      " & _
                              "'" & gstrMoneda & "', " & _
                              "  0,0,0,SYSDATE+2,strPN)      "
         'Debug.Print strInsertProducto
         objApp.rdoConnect.Execute strInsertProducto, 64
         mdblCodPedCom = vntCodPedCompra
        Else 'Se viene de Gestionar existencias
          For intInd = 1 To gintCont
            If gaList(intInd, 2) = -1 Then '9
              sqlstr = "SELECT MAX(FR25CODDETPEDCOMP) FROM FR2500 WHERE FR62CODPEDCOMPRA=" & vntCodPedCompra
              Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
              If IsNull(rsta.rdoColumns(0).Value) Then '10
                vntCodDetPedCompra = 1
              Else
                vntCodDetPedCompra = rsta.rdoColumns(0).Value + 1
              End If '10
              rsta.Close
              Set rsta = Nothing
         
              'Se establece la moneda
              If gstrMoneda = "" Then '11
                gstrMoneda = strMonedaActual
              End If '11
                                 
              strInsertProducto = "INSERT INTO fr2500      " & _
                                  "  (FR62CODPEDCOMPRA,   " & _
                                  "  FR25CODDETPEDCOMP,   " & _
                                  "  FR73CODPRODUCTO,     " & _
                                  "  FR25DESCUENTO, FR25BONIF, FR88CODTIPIVA, FR25TAMENVASE, " & _
                                  "  FR25CANTPEDIDA,      " & _
                                  "  FR25PRECIOUNIDAD,    " & _
                                  "  FR25IMPORLINEA,      " & _
                                  "  FR25FECPLAZENTRE,    " & _
                                  "  FR25INDENTRPROGR,    " & _
                                  "  FR25OBSERV,          " & _
                                  "  FRH8MONEDA,          " & _
                                  "  FR25CANTENTREG,      " & _
                                  "  FR25INDCONT,         " & _
                                  "  FR25CANTENT,FR25PRECNET,FR25UDESBONIF)         " & _
                                  "Values                 " & _
                                  "  (" & vntCodPedCompra & "," & _
                                  vntCodDetPedCompra & "," & _
                                  gaList(intInd, 1) & "," & _
                                  objGen.ReplaceStr(gaList(intInd, 5), ",", ".", 1) & "," & _
                                  objGen.ReplaceStr(gaList(intInd, 6), ",", ".", 1) & "," & _
                                  gaList(intInd, 7) & "," & _
                                  gaList(intInd, 8) & "," & _
                                  gaList(intInd, 4) & ","
              
              gstrPrecioOfertado = objGen.ReplaceStr(gaList(intInd, 3), ",", ".", 1)
         
              strInsertProducto = strInsertProducto & gstrPrecioOfertado & ","
              strInsertProducto = strInsertProducto & _
                                objGen.ReplaceStr(gaList(intInd, 10), ",", ".", 1) & "," & _
                                "  SYSDATE+2,NULL,NULL,      " & _
                                "'" & gstrMoneda & "', " & _
                                "  0,0,0," & objGen.ReplaceStr(gaList(intInd, 0), ",", ".", 1) & "," & gaList(intInd, 9) & ")      "
         
              objApp.rdoConnect.Execute strInsertProducto, 64
              mdblCodPedCom = vntCodPedCompra
            End If '9
          Next intInd
        End If '4
 
   End If '3
   

End Sub


Private Sub calcular_precio_neto()
Dim Precio_Neto As Currency
Dim Precio_Base As Currency
Dim Descuento As Currency
Dim IVA As Currency
Dim Recargo As Currency
Dim PVL As Currency
Dim PVP As Currency
Dim Param_Gen As Currency
Dim stra As String
Dim rsta As rdoResultset
Dim strPrecNetComp As String
Dim strupdate As String
Dim strPrecVenta As String

'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
If IsNumeric(txtText1(22).Text) Then 'Precio_Base
  Precio_Base = txtText1(22).Text
Else
  Precio_Base = 0
End If
If IsNumeric(txtText1(28).Text) Then 'Descuento
  Descuento = Precio_Base * txtText1(28).Text / 100
Else
  Descuento = 0
End If
If IsNumeric(txtText1(29).Text) Then 'Recargo
  Recargo = Precio_Base * txtText1(29).Text / 100
Else
  Recargo = 0
End If
If IsNumeric(txtText1(27).Text) Then 'IVA
  IVA = (Precio_Base + Recargo - Descuento) * txtText1(27).Text / 100
Else
  IVA = 0
End If
'If IsNumeric(txtText1(29).Text) Then 'Recargo
'  Recargo = Precio_Base * txtText1(29).Text / 100
'Else
'  Recargo = 0
'End If
Precio_Neto = Precio_Base - Descuento + IVA + Recargo
If txtprecioneto.Text <> Format(Precio_Neto, "0.00") Then
    txtprecioneto.Text = Format(Precio_Neto, "0.00")
End If

End Sub

Private Sub txtText1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Index = 7 Then
    mblnNoSaltar = True
  Else
    mblnNoSaltar = False
  End If
End Sub
