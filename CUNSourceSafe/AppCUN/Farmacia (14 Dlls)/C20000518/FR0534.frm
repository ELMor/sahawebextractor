VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmMantenimientoLotes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Control de Lotes"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Lotes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6975
      Index           =   0
      Left            =   600
      TabIndex        =   9
      Top             =   720
      Width           =   10250
      Begin TabDlg.SSTab tabTab1 
         Height          =   6300
         Index           =   0
         Left            =   240
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   480
         Width           =   9855
         _ExtentX        =   17383
         _ExtentY        =   11113
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(4)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(6)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(7)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(8)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "dtcDateCombo1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(1)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(0)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(2)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(3)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(4)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(5)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(6)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).ControlCount=   19
         TabCaption(1)   =   "Tabla"
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   6
            Left            =   360
            TabIndex        =   8
            Tag             =   "Ubicaci�n"
            Top             =   5400
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            Index           =   5
            Left            =   1800
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Descripci�n Producto"
            Top             =   4560
            Width           =   6480
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            Index           =   4
            Left            =   360
            TabIndex        =   6
            Tag             =   "C�d.producto"
            Top             =   4560
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR49UNIDADESLOTE"
            Height          =   330
            Index           =   3
            Left            =   2880
            TabIndex        =   5
            Tag             =   "Unidades"
            Top             =   3720
            Width           =   1800
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR49MARGENSEGUR"
            Height          =   330
            Index           =   2
            Left            =   360
            TabIndex        =   4
            Tag             =   "Margen Seguridad"
            Top             =   3720
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR49DESLOTE"
            Height          =   930
            Index           =   1
            Left            =   360
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   1
            Tag             =   "Descripci�n Lote"
            Top             =   1320
            Width           =   8160
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FR49CODLOTE"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   0
            Tag             =   "C�digo Lote"
            Top             =   600
            Width           =   1200
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   5865
            Index           =   0
            Left            =   -74760
            TabIndex        =   12
            TabStop         =   0   'False
            Top             =   240
            Width           =   9015
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15901
            _ExtentY        =   10345
            _StockProps     =   79
            Caption         =   "LOTES"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR49FECENTRADALOT"
            Height          =   330
            Index           =   0
            Left            =   360
            TabIndex        =   2
            Tag             =   "Fecha Entrada"
            Top             =   2760
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR49FECCADUCIDAD"
            Height          =   330
            Index           =   1
            Left            =   5520
            TabIndex        =   3
            Tag             =   "Fecha Caducidad"
            Top             =   2760
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR49FECFABLAB"
            Height          =   330
            Index           =   2
            Left            =   2880
            TabIndex        =   22
            Tag             =   "Fecha Fabricaci�n"
            Top             =   2760
            Visible         =   0   'False
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            DefaultDate     =   ""
            MinDate         =   "1900/1/1"
            MaxDate         =   "2100/12/31"
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Fabricaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   8
            Left            =   2880
            TabIndex        =   23
            Top             =   2520
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Ubicaci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   7
            Left            =   360
            TabIndex        =   21
            Top             =   5160
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   360
            TabIndex        =   20
            Top             =   4320
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Unidades"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   6
            Left            =   2880
            TabIndex        =   19
            Top             =   3480
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Caducidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   5520
            TabIndex        =   18
            Top             =   2520
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Margen de Seguridad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   360
            TabIndex        =   17
            Top             =   3480
            Width           =   2295
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Fecha Entrada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   360
            TabIndex        =   16
            Top             =   2520
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Descripci�n Lote"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   360
            TabIndex        =   14
            Top             =   1080
            Width           =   1815
         End
         Begin VB.Label lblLabel1 
            Caption         =   "C�digo Lote"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   13
            Top             =   360
            Width           =   1815
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmMantenimientoLotes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantenimientoLotes (FR0534.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DE 1998                                                *
'* DESCRIPCION: mantenimiento de Lotes                                  *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************


Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
'Dim mensaje As String
'If intIndex = 2 Then
'  If dtcDateCombo1(2).Text <> "" And dtcDateCombo1(0).Text <> "" Then
'    If DateDiff("d", dtcDateCombo1(2).Date, dtcDateCombo1(0).Date) < 0 Then
'        mensaje = MsgBox("La fecha de Fabricaci�n es mayor que la de Entrada", vbInformation, "Aviso")
'        dtcDateCombo1(2).Text = ""
'    End If
'  End If
'End If
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
'Dim mensaje As String

'If intIndex = 2 Then
'  If dtcDateCombo1(2).Text <> "" And dtcDateCombo1(0).Text <> "" Then
'    If DateDiff("d", dtcDateCombo1(2).Date, dtcDateCombo1(0).Date) < 0 Then
'        mensaje = MsgBox("La fecha de Fabricaci�n es mayor que la de Entrada", vbInformation, "Aviso")
'        dtcDateCombo1(2).Text = ""
'    End If
'  End If
'End If
    
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
Dim mensaje As String
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub Form_Activate()
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim strFec As String
  Dim rstFec As rdoResultset
  Dim strFR49 As String
  Dim qryFR49 As rdoQuery
  Dim rstFR49 As rdoResultset
  
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  strFR49 = "SELECT COUNT(*) " & _
            "  FROM FR4900 " & _
            " WHERE FRJ1CODALBARAN = ? " & _
            "   AND FRJ3NUMLINEA = ? "
  Set qryFR49 = objApp.rdoConnect.CreateQuery("", strFR49)
  qryFR49(0) = gstrNumAlb
  qryFR49(1) = gintNumLin
  Set rstFR49 = qryFR49.OpenResultset()
  If rstFR49.rdoColumns(0).Value > 0 Then
    'ABRIR
    objWinInfo.objWinActiveForm.strWhere = "FRJ1CODALBARAN = '" & gstrNumAlb & "' AND FRJ3NUMLINEA = " & gintNumLin
    objWinInfo.DataRefresh
  Else
    'NUEVO
    Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
  
    sqlstr = "SELECT FR49CODLOTE_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0).Text = rsta.rdoColumns(0).Value
    'fecha de entrada
    strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
    rsta.Close
    Set rsta = Nothing
    rstFec.Close
    Set rstFec = Nothing
    Call objWinInfo.CtrlSet(txtText1(4), gdblCodProd)
    Call objWinInfo.CtrlSet(txtText1(3), gcurUniRec)
  End If
  qryFR49.Close
  Set qryFR49 = Nothing
  Set rstFR49 = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim strFec As String
  Dim rstFec As rdoResultset
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
  With objDetailInfo
    .strName = "Lotes"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    '.strDataBase = objEnv.GetValue("Main")
    .strTable = "FR4900"
    '.strTable = "SELECT FR73CODPRODUCTO,FR49CODLOTE,FR49DESLOTE,FR49FECENTRADALOT,FR49UNIDADESLOTE,FR49FECCADUCIDAD,FR49MARGENSEGUR,FR49UBICACION FROM FR4900"
    
    Call .FormAddOrderField("FR49CODLOTE", cwAscending)
    
    .blnHasMaint = True
    .blnAskPrimary = False
  
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Lotes")
    Call .FormAddFilterWhere(strKey, "FR49CODLOTE", "C�digo Lote", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR49DESLOTE", "Descripci�n Lote", cwString)
    Call .FormAddFilterWhere(strKey, "FR49FECENTRADALOT", "Fecha Entrada", cwDate)
    Call .FormAddFilterWhere(strKey, "FR49FECADUCIDAD", "Fecha Caducidad", cwDate)
    'Call .FormAddFilterWhere(strKey, "FR49FECFABLAB", "Fecha Fabricaci�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR49UNIDADESLOTE", "Unidades", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR49MARGENSEGUR", "Margen de Seguridad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR49UBICACION", "Ubicaci�n", cwString)
        
    
    Call .FormAddFilterOrder(strKey, "FR49CODLOTE", "C�digo Lote")
    Call .FormAddFilterOrder(strKey, "FR49DESLOTE", "Descripci�n Lote")
    Call .FormAddFilterOrder(strKey, "FR49FECENTRADALOT", "Fecha Entrada")
    Call .FormAddFilterOrder(strKey, "FR49FECADUCIDAD", "Fecha Caducidad")
    'Call .FormAddFilterOrder(strKey, "FR49FECFABLAB", "Fecha Fabricaci�n")
    Call .FormAddFilterOrder(strKey, "FR49UNIDADESLOTE", "Unidades")
    Call .FormAddFilterOrder(strKey, "FR49MARGENSEGUR", "Margen de Seguridad")
    Call .FormAddFilterOrder(strKey, "FR49UBICACION", "Ubicaci�n")
    
    
 
  End With
   
  With objWinInfo

    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objDetailInfo)
    
    'Se indica que campos son obligatorios y cuales son clave primaria
        .CtrlGetInfo(txtText1(0)).intKeyNo = 1
        .CtrlGetInfo(txtText1(1)).blnMandatory = True

    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    '.CtrlGetInfo(dtcDateCombo1(2)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnInFind = False
    
    .CtrlGetInfo(dtcDateCombo1(0)).blnReadOnly = True 'fecha de entrada
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = False 'fecha de caducidad

    .CtrlGetInfo(dtcDateCombo1(2)).blnInGrid = False

    .CtrlGetInfo(txtText1(4)).blnForeign = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "FR73CODPRODUCTO", "SELECT FR73DESPRODUCTOPROV,FRH9UBICACION FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(5), "FR73DESPRODUCTOPROV")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(6), "FRH9UBICACION")
   
    .CtrlGetInfo(txtText1(6)).blnForeign = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
     
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim rstgrupo As rdoResultset
Dim strgrupo As String
Dim rstserv As rdoResultset
Dim strserv As String
Dim fecha As Date
Dim strupdate As String
Dim rstServiFarma As rdoResultset
Dim strServiFarma As String

'se calcula la fecha de caducidad
If txtText1(4).Text = "" Or dtcDateCombo1(2).Text = "" Then
    Exit Sub
Else
    
    strServiFarma = ""
    strServiFarma = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=3"
    Set rstServiFarma = objApp.rdoConnect.OpenResultset(strServiFarma)
    strServiFarma = rstServiFarma("FRH2PARAMGEN").Value
    rstServiFarma.Close
    Set rstServiFarma = Nothing

    stra = "SELECT FR73INDCTRLLOTE FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(4).Text
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    'si el producto est� sujeto a control de lote
    If rsta.rdoColumns(0).Value = -1 Then
      strgrupo = "SELECT distinct FR41CODGRUPPROD FROM FR0900 " & _
                 "WHERE FR73CODPRODUCTO=" & txtText1(4).Text
      Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
      Do While Not rstgrupo.EOF
            strserv = "SELECT * FROM FR4100 " & _
                    "WHERE AD02CODDPTO=" & strServiFarma & " AND " & _
                    "FR41CODGRUPPROD='" & rstgrupo.rdoColumns(0).Value & "' AND " & _
                    "FR41NUMESTAB IS NOT NULL AND FR41TIPMEDIDA IS NOT NULL"
            Set rstserv = objApp.rdoConnect.OpenResultset(strserv)
            If rstserv.EOF Then
              Exit Do
            Else
              Select Case rstserv.rdoColumns("FR41TIPMEDIDA").Value
              Case "Minutos"
                 fecha = DateAdd("n", rstserv.rdoColumns("FR41NUMESTAB").Value, dtcDateCombo1(2).Text)
              Case "Horas"
                 fecha = DateAdd("h", rstserv.rdoColumns("FR41NUMESTAB").Value, dtcDateCombo1(2).Text)
              Case "D�as"
                 fecha = DateAdd("d", rstserv.rdoColumns("FR41NUMESTAB").Value, dtcDateCombo1(2).Text)
              Case "Semanas"
                 fecha = DateAdd("ww", rstserv.rdoColumns("FR41NUMESTAB").Value, dtcDateCombo1(2).Text)
              Case "Meses"
                 fecha = DateAdd("m", rstserv.rdoColumns("FR41NUMESTAB").Value, dtcDateCombo1(2).Text)
              Case "A�os"
                 fecha = DateAdd("yyyy", rstserv.rdoColumns("FR41NUMESTAB").Value, dtcDateCombo1(2).Text)
              End Select
              objWinInfo.objWinActiveForm.blnChanged = False
              strupdate = "UPDATE FR4900 SET FR49FECCADUCIDAD=TO_DATE('" & fecha & "','DD/MM/YYYY')" & _
                        " WHERE FR49CODLOTE=" & txtText1(0).Text
             objApp.rdoConnect.Execute strupdate, 64
             objWinInfo.objWinActiveForm.blnChanged = False
             'se refresca
             Call objWinInfo.WinProcess(cwProcessToolBar, 26, 0)
             Exit Do
            End If
      rstgrupo.MoveNext
      rstserv.Close
      Set rstserv = Nothing
      Loop
    rstgrupo.Close
    Set rstgrupo = Nothing
    End If
rsta.Close
Set rsta = Nothing
End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  If gcurUniRec < txtText1(3).Text Then
    MsgBox "Las unidades del lote no pueden ser superiores a " & gcurUniRec, vbInformation, "Control de Lotes"
    blnCancel = True
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Tipos de Interacci�n" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

  If strCtrl = "txtText1(6)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH900"
     
     Set objField = .AddField("FRH9UBICACION")
     objField.strSmallDesc = "Ubicaci�n"
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(6), .cllValues("FRH9UBICACION"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  If strCtrl = "txtText1(4)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo Producto"
         
     Set objField = .AddField("FR73DESPRODUCTOPROV")
     objField.strSmallDesc = "Descripci�n Producto"
     
         
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(4), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
 End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim strFec As String
Dim rstFec As rdoResultset

If btnButton.Index = 2 Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        sqlstr = "SELECT FR49CODLOTE_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        'fecha de entrada
        strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
        Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
        'SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
        rstFec.Close
        Set rstFec = Nothing
Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim strFec As String
Dim rstFec As rdoResultset

If intIndex = 10 Then
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
        sqlstr = "SELECT FR49CODLOTE_SEQUENCE.nextval FROM dual"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        txtText1(0).Text = rsta.rdoColumns(0).Value
        'fecha de entrada
        strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
        Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
        Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
        'SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
Else
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End If
 
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


