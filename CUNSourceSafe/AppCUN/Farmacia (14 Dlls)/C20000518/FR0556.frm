VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form frmListToAlb 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Lista de productos para crear el albar�n"
   ClientHeight    =   6840
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   9630
   ControlBox      =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0556.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6840
   ScaleWidth      =   9630
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdOutList 
      Caption         =   "&Quitar"
      Height          =   375
      Index           =   0
      Left            =   4440
      TabIndex        =   5
      ToolTipText     =   "Elimina un producto al albar�n"
      Top             =   7680
      Width           =   1335
   End
   Begin VB.CommandButton cmdCleanList 
      Caption         =   "Vaciar &Lista"
      Height          =   375
      Index           =   0
      Left            =   6000
      TabIndex        =   4
      ToolTipText     =   "Vacia la lista de los producto del albar�n"
      Top             =   7680
      Width           =   1335
   End
   Begin VB.Frame fraframe1 
      Caption         =   "Lista de productos para crear el albar�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6855
      Index           =   2
      Left            =   120
      TabIndex        =   0
      Tag             =   "Actuaciones Asociadas"
      Top             =   720
      Width           =   11655
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   6375
         Index           =   1
         Left            =   120
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   11370
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   20055
         _ExtentY        =   11245
         _StockProps     =   79
         Caption         =   "LISTA DE PRODUCTOS PARA CREAR EL ALBARAN"
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   6555
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmListToAlb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmMantConvUnidad (FR0056.FRM)                               *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: OCTUBRE DE 1998                                               *
'* DESCRIPCION: mantenimiento Conversi�n de Unidad                      *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

Private Sub cmdCleanList_Click(Index As Integer)
  'Se vacia la lista
  Dim intResp As Integer
  Dim intInd As Integer
  
  intResp = MsgBox("�Est� seguro que quiere vaciar la lista?", vbYesNo, "Vaciar la lista")
  
  Select Case intResp
    Case 6 'S� quiere vaciar la lista
      For intInd = 1 To 250
        gaListAlb(intInd, 1) = 0
        gaListAlb(intInd, 2) = 0
        gaListAlb(intInd, 3) = 0
      Next intInd
      gintContAlb = 0
      Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
      objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
      objWinInfo.DataRefresh
      MsgBox "La lista ha sido vaciada", vbInformation, "Vaciar la lista"
    Case 7 'No quiere vaciar la lista
  End Select

End Sub

Private Sub cmdOutList_Click(Index As Integer)
  'Se elimina el producto seleccionado de la lsita con la que se construir� el albar�n
  Dim intInd As Integer
  Dim intResp As Integer
  Dim mintNTotalSelRows As Integer
  Dim mvarBkmrk As Variant
  
  'Se comprueba que ha seleccionado un producto
  mintNTotalSelRows = 0
  'Guardamos el n�mero de filas seleccionadas
  mintNTotalSelRows = grdDBGrid1(1).SelBookmarks.Count
  If mintNTotalSelRows > 0 Then
    'Ha seleccionado alguna fila
    For intInd = 1 To gintContAlb
      If (gaListAlb(intInd, 1) = grdDBGrid1(1).Columns("N�Pedido").CellValue(mvarBkmrk)) And _
         (gaListAlb(intInd, 2) = grdDBGrid1(1).Columns("N�L�nea").CellValue(mvarBkmrk)) Then
        'Se ha encontrado el elemento aliminar de la lista
        gaListAlb(intInd, 3) = 0
        Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
        objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
        objWinInfo.DataRefresh
        intResp = MsgBox("El producto ha sido eliminado de la lista", vbInformation, "Quitar del albar�n")
        Exit For
      End If
    Next intInd
  Else
    MsgBox "No ha seleccionado ning�n producto", vbInformation, "Quitar del albar�n"
  End If
  
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
    Dim objMultiInfo As New clsCWForm

    Dim strKey As String
  
    'Call objApp.SplashOn
  
    Set objWinInfo = New clsCWWin
  
    Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
  
    With objMultiInfo
        .strName = "Lista"
        Set .objFormContainer = fraFrame1(2)
        Set .objFatherContainer = Nothing
        Set .tabMainTab = Nothing
        Set .grdGrid = grdDBGrid1(1)
        .intFormModel = cwWithGrid + cwWithoutTab + cwWithKeys
        .strTable = "FR2501J"
        .strWhere = strClausulaWhere
        .intAllowance = cwAllowReadOnly
        
        Call .FormAddOrderField("FR2501J.FR62CODPEDCOMPRA", cwAscending)
        Call .FormAddOrderField("FR2501J.FR25CODDETPEDCOMP", cwAscending)
        Call .FormAddOrderField("FR2501J.FR73DESPRODUCTOPROV", cwAscending)
    
        
        
        strKey = .strDataBase & .strTable
    
        'Se establecen los campos por los que se puede filtrar
        'Call .FormCreateFilterWhere(strKey, "Conversi�n de Unidad")
        'Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA_ORI", "C�d.Unidad Origen", cwString)
        'Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA_DES", "C�d.Unidad Destino", cwString)
        'Call .FormAddFilterWhere(strKey, "FR14FACTORCONVER", "Factor de Conversi�n", cwDecimal)
    
    
        'Se establecen los campos por los que se puede ordenar con el filtro
        'Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA_ORI", "C�d.Unidad Origen")
        'Call .FormAddFilterOrder(strKey, "FR93CODUNIMEDIDA_DES", "C�d.Unidad Destino")
        'Call .FormAddFilterOrder(strKey, "FR14FACTORCONVER", "Factor de Conversi�n")

    
    End With

    With objWinInfo
        
        Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    
        'Se indican las columnas que aparecer�n en el grid
        Call .GridAddColumn(objMultiInfo, "C�d.Int", "FR73CODINTFAR", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "DC", "FR73CODINTFARSEG", cwNumeric, 1)
        Call .GridAddColumn(objMultiInfo, "Producto", "FR73DESPRODUCTOPROV", cwString, 50)
        Call .GridAddColumn(objMultiInfo, "FF", "FRH7CODFORMFAR", cwString, 3)
        Call .GridAddColumn(objMultiInfo, "Dosis", "FR73DOSIS", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "UM", "FR93CODUNIMEDIDA", cwString, 5)
        Call .GridAddColumn(objMultiInfo, "Vol", "FR73VOLUMEN", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "Referencia", "FR73REFERENCIA", cwString, 6)
        Call .GridAddColumn(objMultiInfo, "Ped.", "FR25CANTPEDIDA", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "Acu.", "FR25CANTENT", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "Prec.Uni.", "FR25PRECIOUNIDAD", cwDecimal, 3)
        Call .GridAddColumn(objMultiInfo, "%Desc", "FR25DESCUENTO", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "%Bonif", "FR25BONIF", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "IVA", "FR88CODTIPIVA", cwNumeric, 2)
        Call .GridAddColumn(objMultiInfo, "Prec.Neto", "FR25PRECNET", cwDecimal, 3)
        Call .GridAddColumn(objMultiInfo, "Importe", "FR25IMPORLINEA", cwDecimal, 3)
        Call .GridAddColumn(objMultiInfo, "UE", "FR25TAMENVASE", cwDecimal, 2)
        Call .GridAddColumn(objMultiInfo, "N�Pedido", "FR62CODPEDCOMPRA", cwNumeric, 9)
        Call .GridAddColumn(objMultiInfo, "N�L�nea", "FR25CODDETPEDCOMP", cwNumeric, 9)
        Call .FormCreateInfo(objMultiInfo)
       
        Call .FormChangeColor(objMultiInfo)
        
   
        Call .WinRegister
        Call .WinStabilize
    End With
    grdDBGrid1(1).Columns("C�d.Int").Width = 800
    grdDBGrid1(1).Columns("DC").Width = 400
    grdDBGrid1(1).Columns("Producto").Width = 3000
    grdDBGrid1(1).Columns("FF").Width = 400
    grdDBGrid1(1).Columns("Dosis").Width = 500
    grdDBGrid1(1).Columns("UM").Width = 500
    grdDBGrid1(1).Columns("Vol").Width = 600
    grdDBGrid1(1).Columns("Referencia").Width = 1000
    grdDBGrid1(1).Columns("Ped.").Width = 700
    grdDBGrid1(1).Columns("Acu.").Width = 700
    grdDBGrid1(1).Columns("Prec.Uni.").Width = 1100
    grdDBGrid1(1).Columns("%Desc").Width = 500
    grdDBGrid1(1).Columns("%Bonif").Width = 500
    grdDBGrid1(1).Columns("IVA").Width = 500
    grdDBGrid1(1).Columns("Prec.Neto").Width = 1100
    grdDBGrid1(1).Columns("Importe").Width = 1300
    grdDBGrid1(1).Columns("UE").Width = 400
    grdDBGrid1(1).Columns("N�Pedido").Width = 1000
    grdDBGrid1(1).Columns("N�L�nea").Width = 1000
End Sub
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Conversi�n de Unidad" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, False))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
    intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
    intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub


Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
    intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
    Call objWinInfo.WinDeRegister
    Call objWinInfo.WinRemoveInfo
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------


Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
    'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
    Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
Dim mensaje As String
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
'Private Sub tabTab1_MouseDown(intIndex As Integer, _
'                              Button As Integer, _
'                              Shift As Integer, _
'                              X As Single, _
'                              Y As Single)
'    Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
    Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
'Private Sub lblLabel1_Click(intIndex As Integer)
'    Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
'End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   
    Call objWinInfo.CtrlGotFocus
   
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Function strClausulaWhere() As String
  'Toma como entrada la lista gaListAlb y crea la clausula where con el c�d. de pedido y el n� de l�nea
  Dim intInd As Integer
  Dim blnListaVacia As Boolean
  Dim strClausula As String
  Dim blnHayVariosProd As Boolean
  
  strClausula = "(FR62CODPEDCOMPRA = "
  
  If gintContAlb > 0 Then
    blnListaVacia = True
    blnHayVariosProd = False
    For intInd = 1 To gintContAlb
      If gaListAlb(intInd, 3) = -1 Then
        blnListaVacia = False
        If blnHayVariosProd = False Then
          blnHayVariosProd = True
          strClausula = strClausula & gaListAlb(intInd, 1) & " AND " _
                        & "FR25CODDETPEDCOMP = " & gaListAlb(intInd, 2) & ") "
        Else
          strClausula = strClausula & " OR "
          strClausula = strClausula & "(FR62CODPEDCOMPRA = " & gaListAlb(intInd, 1) & " AND " _
                        & "FR25CODDETPEDCOMP = " & gaListAlb(intInd, 2) & ") "
        End If
      End If
    Next intInd
    If blnListaVacia = True Then
      'La lista gaListAlb est� vacia y no hay que hacer nada
      strClausulaWhere = "-1 = 0"
    Else
      strClausulaWhere = strClausula
    End If
  Else
    'La lista gaListAlb est� vacia y no hay que hacer nada
    strClausulaWhere = " -1 = 2"
  End If
  
End Function

