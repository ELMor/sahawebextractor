VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#1.3#0"; "CRYSTL32.OCX"
Begin VB.Form FrmConSitAlb 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Consultar Situaci�n Albaranes"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0529.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   23
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Index           =   3
      Left            =   4680
      TabIndex        =   96
      Top             =   3240
      Width           =   4545
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1095
         Index           =   3
         Left            =   840
         TabIndex        =   97
         Top             =   120
         Width           =   3615
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   6376
         _ExtentY        =   1931
         _StockProps     =   79
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Facturas:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   0
         TabIndex        =   98
         Top             =   120
         Width           =   810
      End
   End
   Begin VB.Frame Frame6 
      Height          =   735
      Left            =   10080
      TabIndex        =   90
      Top             =   2520
      Width           =   1575
      Begin VB.CommandButton cmdInList 
         Caption         =   "&A�adir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   92
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Filtro del Listado"
      ForeColor       =   &H00FF0000&
      Height          =   1815
      Left            =   7200
      TabIndex        =   75
      Top             =   480
      Width           =   4695
      Begin VB.TextBox txtCuenCont 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Left            =   720
         MaxLength       =   9
         TabIndex        =   88
         Top             =   1320
         Width           =   2040
      End
      Begin VB.TextBox txtProducto 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Left            =   1920
         MaxLength       =   9
         TabIndex        =   86
         Top             =   960
         Width           =   2745
      End
      Begin VB.TextBox txtCodPrd 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Left            =   720
         MaxLength       =   9
         TabIndex        =   84
         Top             =   960
         Width           =   720
      End
      Begin VB.TextBox txtProveedor 
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Left            =   1920
         MaxLength       =   9
         TabIndex        =   82
         Top             =   600
         Width           =   2745
      End
      Begin VB.TextBox txtCodPrv 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Left            =   720
         MaxLength       =   9
         TabIndex        =   80
         Top             =   600
         Width           =   720
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcDesde 
         Height          =   330
         Left            =   720
         TabIndex        =   76
         Top             =   240
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcHasta 
         Height          =   330
         Left            =   2760
         TabIndex        =   77
         Top             =   240
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
      End
      Begin VB.Label Label5 
         Caption         =   "C.Cont."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   89
         Top             =   1440
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Prod."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   7
         Left            =   1440
         TabIndex        =   87
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "C.Prd."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   85
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "Prov."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   5
         Left            =   1440
         TabIndex        =   83
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "C.Prv."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   81
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label5 
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   2640
         TabIndex        =   79
         Top             =   360
         Width           =   135
      End
      Begin VB.Label Label5 
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   78
         Top             =   360
         Width           =   615
      End
   End
   Begin Crystal.CrystalReport CrystalReport1 
      Left            =   9840
      Top             =   2880
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   327680
      PrintFileLinesPerPage=   60
   End
   Begin VB.Frame fraFrame1 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Index           =   2
      Left            =   360
      TabIndex        =   69
      Top             =   3240
      Width           =   3825
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1095
         Index           =   2
         Left            =   840
         TabIndex        =   70
         Top             =   120
         Width           =   2295
         ScrollBars      =   2
         _Version        =   131078
         DataMode        =   2
         ColumnHeaders   =   0   'False
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   4048
         _ExtentY        =   1931
         _StockProps     =   79
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Pedidos:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   0
         TabIndex        =   71
         Top             =   120
         Width           =   750
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Criterios de B�queda"
      ForeColor       =   &H00FF0000&
      Height          =   1815
      Left            =   120
      TabIndex        =   42
      Top             =   480
      Width           =   6975
      Begin VB.CheckBox chkAlbFac 
         Caption         =   "Albaranes Facturados"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   93
         Top             =   1560
         Width           =   2415
      End
      Begin VB.TextBox txtAlbaran 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Left            =   2040
         MaxLength       =   9
         TabIndex        =   0
         Top             =   1080
         Width           =   1440
      End
      Begin VB.TextBox txtPedido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         Height          =   330
         HelpContextID   =   40102
         Left            =   3480
         MaxLength       =   9
         TabIndex        =   1
         Top             =   1080
         Width           =   1440
      End
      Begin VB.TextBox txtbuscar 
         Height          =   330
         Left            =   120
         TabIndex        =   44
         Top             =   480
         Width           =   2415
      End
      Begin VB.Frame Frame3 
         Caption         =   "Buscar en"
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   2640
         TabIndex        =   43
         Top             =   240
         Width           =   4095
         Begin VB.CheckBox Check3 
            Caption         =   "C�d.Inter."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   1320
            TabIndex        =   74
            Top             =   240
            Width           =   1215
         End
         Begin VB.CheckBox chkproducto 
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   45
            Top             =   240
            Width           =   1215
         End
         Begin VB.CheckBox chkproveedor 
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2640
            TabIndex        =   46
            Top             =   240
            Width           =   1335
         End
      End
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "&Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5520
         TabIndex        =   2
         Top             =   1080
         Width           =   1215
      End
      Begin SSCalendarWidgets_A.SSDateCombo dtcPedido 
         Height          =   330
         Left            =   120
         TabIndex        =   47
         Top             =   1080
         Width           =   1860
         _Version        =   65537
         _ExtentX        =   3281
         _ExtentY        =   582
         _StockProps     =   93
         BackColor       =   -2147483634
         BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         DefaultDate     =   ""
         AllowNullDate   =   -1  'True
      End
      Begin VB.Label Label5 
         Caption         =   "N� Pedido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   3480
         TabIndex        =   68
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "N� Albar�n"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   2040
         TabIndex        =   62
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Texto a buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Fecha Entrega"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   840
         Width           =   1695
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3315
      Index           =   1
      Left            =   120
      TabIndex        =   27
      Top             =   4800
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   2940
         Index           =   1
         Left            =   120
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   240
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   5186
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0529.frx":000C
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "lblLabel1(18)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(17)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(16)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(15)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(9)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(57)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(56)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(13)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(19)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(21)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(22)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(23)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(7)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(12)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "lblLabel1(20)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(11)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(13)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(10)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(12)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(14)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(2)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(3)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(4)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(15)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(17)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(18)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(19)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(21)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "Frame5"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(24)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtText1(23)"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(5)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtText1(25)"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(27)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(28)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(29)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtText1(0)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "txtText1(7)"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtText1(16)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).ControlCount=   45
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0529.frx":0028
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3TPBONIF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   16
            Left            =   -73560
            TabIndex        =   101
            Tag             =   "%Bonif|%Bonif"
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3DTO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   7
            Left            =   -73560
            TabIndex        =   99
            Tag             =   "%Dto|Tanto por ciento de descuento"
            Top             =   1560
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3CANTPTEFACT"
            Height          =   330
            HelpContextID   =   40102
            Index           =   0
            Left            =   -66720
            TabIndex        =   13
            Tag             =   "Pte.Fact.|Cantidad pendiente de ser facturada"
            Top             =   960
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRJ3TAMENVASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   29
            Left            =   -65040
            Locked          =   -1  'True
            TabIndex        =   21
            TabStop         =   0   'False
            Tag             =   "Envase|Unidades por envase"
            Top             =   360
            Width           =   945
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRJ3PRECNET"
            Height          =   330
            HelpContextID   =   30104
            Index           =   28
            Left            =   -71760
            Locked          =   -1  'True
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Precio Neto"
            Top             =   960
            Width           =   2100
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3IVA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   27
            Left            =   -72480
            TabIndex        =   19
            Tag             =   "IVA|%IVA"
            Top             =   2160
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3BONIF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   25
            Left            =   -74880
            TabIndex        =   17
            Tag             =   "Udes.Bon.|Unidades a Bonificar"
            Top             =   1560
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3CANTENTREG"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   -74880
            TabIndex        =   14
            Tag             =   "Rdo.|Cantudad recibida hasta la fecha"
            Top             =   960
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   -74160
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   360
            Width           =   300
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   -74880
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "C�d.Interno"
            Top             =   360
            Width           =   720
         End
         Begin VB.Frame Frame5 
            BorderStyle     =   0  'None
            Height          =   855
            Left            =   -67440
            TabIndex        =   29
            Top             =   1560
            Width           =   3495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3NUMLINEA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   21
            Left            =   -64680
            MaxLength       =   13
            TabIndex        =   31
            Tag             =   "C�digo Detalle Pedido"
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ1CODALBARAN"
            Height          =   330
            HelpContextID   =   40102
            Index           =   19
            Left            =   -65280
            MaxLength       =   13
            TabIndex        =   30
            Tag             =   "C�dido Pedido"
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "frh8moneda"
            Height          =   330
            HelpContextID   =   40102
            Index           =   18
            Left            =   -67440
            MaxLength       =   13
            TabIndex        =   22
            Tag             =   "Moneda"
            Top             =   960
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3IMPORLINEA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   17
            Left            =   -69600
            TabIndex        =   20
            Tag             =   "Importe"
            Top             =   960
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ3CANTPEDIDA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   -74880
            TabIndex        =   72
            Tag             =   "Cant Pedida||N� de envases pedidos"
            Top             =   960
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRJ3PRECIOUNIDAD"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   -73560
            TabIndex        =   16
            Tag             =   "PPE|Precio Envase"
            Top             =   960
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   -73800
            TabIndex        =   7
            Tag             =   "Descripci�n Producto"
            Top             =   360
            Width           =   4125
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr73codproducto"
            Height          =   330
            HelpContextID   =   30101
            Index           =   2
            Left            =   -67080
            TabIndex        =   3
            Tag             =   "C�d.Prod"
            Top             =   1920
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   -66720
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   360
            Width           =   1545
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   -67800
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Vol.|Volumen"
            Top             =   360
            Width           =   1020
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   -69000
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   -69600
            Locked          =   -1  'True
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "F.F"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   -68400
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "U.M"
            Top             =   360
            Width           =   540
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2520
            Index           =   1
            Left            =   120
            TabIndex        =   41
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   4445
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonif.(%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   -73560
            TabIndex        =   102
            Top             =   1920
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "%Dto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   -73560
            TabIndex        =   100
            Top             =   1320
            Width           =   450
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cant.Pte.Fact."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   -66720
            TabIndex        =   73
            Top             =   720
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Envase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   -65040
            TabIndex        =   67
            Top             =   120
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Neto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   -71760
            TabIndex        =   66
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "IVA(%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   -72480
            TabIndex        =   65
            Top             =   1920
            Width           =   570
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonif.(Udes)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   -74880
            TabIndex        =   64
            Top             =   1320
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cant.Recibida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   -74880
            TabIndex        =   63
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   -73800
            TabIndex        =   61
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Seg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   -74160
            TabIndex        =   60
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Int."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   57
            Left            =   -74880
            TabIndex        =   59
            Top             =   120
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   -67440
            TabIndex        =   50
            Top             =   720
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   -69600
            TabIndex        =   40
            Top             =   720
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cant.Ped.(E)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   -74880
            TabIndex        =   39
            Tag             =   "Cant.Pedida"
            Top             =   720
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Envase"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   -73560
            TabIndex        =   38
            Top             =   720
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   -67080
            TabIndex        =   37
            Top             =   1680
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   -66720
            TabIndex        =   36
            Top             =   120
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Vol. (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   -67800
            TabIndex        =   35
            Top             =   120
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   -68400
            TabIndex        =   34
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   -69000
            TabIndex        =   33
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   -69600
            TabIndex        =   32
            Top             =   120
            Width           =   345
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Albar�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2445
      Index           =   0
      Left            =   120
      TabIndex        =   26
      Top             =   2280
      Width           =   9705
      Begin TabDlg.SSTab tabTab1 
         Height          =   2055
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   240
         Width           =   9450
         _ExtentX        =   16669
         _ExtentY        =   3625
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0529.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(11)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(8)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(6)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "dtcDateCombo1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(9)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(8)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(1)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "Frame7"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).ControlCount=   9
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0529.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame7 
            BorderStyle     =   0  'None
            Height          =   495
            Left            =   8520
            TabIndex        =   95
            Top             =   1440
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ1ESTADO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   8520
            TabIndex        =   94
            Tag             =   "E|Estado del albar�n"
            Top             =   1560
            Width           =   390
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "FRJ1CODALBARAN"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   120
            TabIndex        =   4
            Tag             =   "N� Albar�n"
            Top             =   360
            Width           =   1150
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   3240
            TabIndex        =   53
            Tag             =   "C�d.Prov|C�digo Proveedor"
            Top             =   360
            Width           =   855
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   4200
            TabIndex        =   54
            Tag             =   "Proveedor"
            Top             =   360
            Width           =   4770
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRJ1FECHAALBAR"
            Height          =   330
            Index           =   1
            Left            =   1320
            TabIndex        =   52
            Tag             =   "Fec.Alb.|Fecha Albar�n"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1770
            Index           =   0
            Left            =   -74880
            TabIndex        =   55
            Top             =   120
            Width           =   8805
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15531
            _ExtentY        =   3122
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Albar�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   58
            Top             =   120
            Width           =   930
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Albar�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1320
            TabIndex        =   57
            Top             =   120
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   3240
            TabIndex        =   56
            Top             =   120
            Width           =   885
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Index           =   0
      Left            =   10080
      TabIndex        =   24
      Top             =   3360
      Width           =   1575
      Begin VB.CommandButton cmdFacturar 
         Caption         =   "&Facturar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   91
         Top             =   720
         Width           =   1335
      End
      Begin VB.CommandButton cmdReclamar 
         Caption         =   "Recla&mar"
         Height          =   375
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Width           =   1335
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   15
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmConSitAlb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0522.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: ABRIL 1999                                                    *
'* DESCRIPCION: Consultar Situaci�n Pedido                              *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnActCantPed As Boolean
Dim mblnActCantPend As Boolean
Dim mblnTamEnva As Boolean
Dim mdblCantPed As Double
Dim mdblCantPend As Double
Dim mdblCodProd As Double
Dim mdblTamEnva As Double
Dim mstrWhere As String
Dim mblnAux As Boolean
Dim Q As Integer
Private Sub Imprimir(strListado As String, intDes As Integer)
  'JMRL 19991125
  'Toma como par�metro el listado a imprimir y lo manda a la impresora;
  'Destino de la impresi�n --> intDes: 0 Windows,1 Printer,2 File,3 MAPI,4 Exchange
  Dim strWhere As String
  Dim strDNS As String
  Dim strUser As String
  Dim strPass As String
  Dim strPATH As String
  Dim strProveedor As String
  
  
  strDNS = ""
  strUser = ""
  strPass = ""
  strPATH = ""
  'Call LeerCWPrint(strDNS, strUser, strPass, strPATH)
  strDNS = objApp.strDataSource
  strUser = objsecurity.GetDataBaseUser
  strPass = objApp.strPassword
  strPATH = "C:\Archivos de programa\cun\rpt\"
  
  CrystalReport1.Connect = "DSN = " & strDNS & ";UID = " & strUser & ";PWD = " & strPass & ";DSQ = Administration"
  Select Case intDes
    Case 0 ' Window
      CrystalReport1.Destination = crptToWindow
    Case 1  'Printer
      CrystalReport1.Destination = crptToPrinter
    Case 2  'File
      'CrystalReport1.Destination = crptToFile
      'Falta indicar el fichero de destino
    Case 3  'MAPI
      'CrystalReport1.Destination = crptMapi
    Case 4  'Exhange
      'CrystalReport1.Destination = crptExchange
    Case Else 'Otros
  End Select
  
  CrystalReport1.ReportFileName = strPATH & strListado
  If UCase(strListado) = "FR5291.RPT" Then
    strWhere = "{FR7900.FR79CODPROVEEDOR} = {FRJ100.FR79CODPROVEEDOR} AND " & _
               "{FRJ100.FRJ1CODALBARAN} = {FRJ300.FRJ1CODALBARAN} AND " & _
               "{FRJ300.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO} " 'AND " & _
               "{FRJ300.FRJ3CANTPTEFACT} > 0 "
  Else
    If UCase(strListado) = "FR5292.RPT" Then
      strWhere = "{FR7900.FR79CODPROVEEDOR} = {FR3900.FR79CODPROVEEDOR} AND " & _
                 "{FR3900.FR39CODFACTCOMPRA} = {FR1500.FR39CODFACTCOMPRA} AND " & _
                 "{FR1500.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO} "
    Else
      If UCase(strListado) = "FR5293.RPT" Then
        strWhere = "{FR7900.FR79CODPROVEEDOR} = {FRJ100.FR79CODPROVEEDOR} AND " & _
                   "{FRJ100.FRJ1CODALBARAN} = {FRJ200.FRJ1CODALBARAN} AND " & _
                   "{FRJ200.FRJ1CODALBARAN} = {FRJ300.FRJ1CODALBARAN} AND " & _
                   "{FRJ300.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO} "
      Else
        If UCase(strListado) = "FR5296.RPT" Then
          strWhere = "{FR7900.FR79CODPROVEEDOR} = {FR6200.FR79CODPROVEEDOR} AND " & _
                     "{FR6200.FR62CODPEDCOMPRA} = {FR2500.FR62CODPEDCOMPRA} AND " & _
                     "{FR2500.FR73CODPRODUCTO} = {FR7300.FR73CODPRODUCTO} AND " & _
                     "{FR6200.FR95CODESTPEDCOMPRA} < 6"
        End If
      End If
    End If
  End If
  
  If UCase(strListado) = "FR5291.RPT" And _
     Len(Trim(dtcDesde.Date)) > 0 And Len(Trim(dtcHasta.Date)) > 0 Then
     strWhere = strWhere & " AND " & _
             "{FRJ100.FRJ1FECHAALBAR} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ")  AND " & _
             "{FRJ100.FRJ1FECHAALBAR} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
  Else
    If UCase(strListado) = "FR5291.RPT" And Len(Trim(dtcDesde.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
             "{FRJ100.FRJ1FECHAALBAR} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ") "
    End If
    If UCase(strListado) = "FR5291.RPT" And Len(Trim(dtcHasta.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
              "{FRJ100.FRJ1FECHAALBAR} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
    End If
  End If
  If UCase(strListado) = "FR5291.RPT" And IsNumeric(txtCodPrv.Text) Then
    strWhere = strWhere & " AND {FR7900.FR79CODPROVEEDOR} = " & txtCodPrv.Text
  End If
  If UCase(strListado) = "FR5291.RPT" And Len(Trim(txtProveedor.Text)) > 0 Then
    strProveedor = UCase(txtProveedor.Text)
    strWhere = strWhere & " AND {FR7900.FR79PROVEEDOR} startswith " & Chr(34) & strProveedor & Chr(34)
  End If
  If UCase(strListado) = "FR5291.RPT" And Len(Trim(txtCodPrd.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CODINTFAR} = " & Chr(34) & txtCodPrd.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5291.RPT" And Len(Trim(txtProducto.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73DESPRODUCTOPROV} startswith " & Chr(34) & txtProducto.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5291.RPT" And Len(Trim(txtCuenCont.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CUENCONT} = " & Chr(34) & txtCuenCont.Text & Chr(34)
  End If
  '*****
  If UCase(strListado) = "FR5292.RPT" And _
     Len(Trim(dtcDesde.Date)) > 0 And Len(Trim(dtcHasta.Date)) > 0 Then
     strWhere = strWhere & " AND " & _
             "{FR3900.FR39FECFACTCOMPRA} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ")  AND " & _
             "{FR3900.FR39FECFACTCOMPRA} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
  Else
    If UCase(strListado) = "FR5292.RPT" And Len(Trim(dtcDesde.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
             "{FR3900.FR39FECFACTCOMPRA} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ") "
    End If
    If UCase(strListado) = "FR5292.RPT" And Len(Trim(dtcHasta.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
              "{FR3900.FR39FECFACTCOMPRA} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
    End If
  End If
  If UCase(strListado) = "FR5292.RPT" And IsNumeric(txtCodPrv.Text) Then
    strWhere = strWhere & " AND {FR7900.FR79CODPROVEEDOR} = " & txtCodPrv.Text
  End If
  If UCase(strListado) = "FR5292.RPT" And Len(Trim(txtProveedor.Text)) > 0 Then
    strProveedor = UCase(txtProveedor.Text)
    strWhere = strWhere & " AND {FR7900.FR79PROVEEDOR} startswith " & Chr(34) & strProveedor & Chr(34)
  End If
  If UCase(strListado) = "FR5292.RPT" And Len(Trim(txtCodPrd.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CODINTFAR} = " & Chr(34) & txtCodPrd.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5292.RPT" And Len(Trim(txtProducto.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73DESPRODUCTOPROV} startswith " & Chr(34) & txtProducto.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5292.RPT" And Len(Trim(txtCuenCont.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CUENCONT} = " & Chr(34) & txtCuenCont.Text & Chr(34)
  End If
  
  '*****
  '+++++
  If UCase(strListado) = "FR5293.RPT" And _
     Len(Trim(dtcDesde.Date)) > 0 And Len(Trim(dtcHasta.Date)) > 0 Then
     strWhere = strWhere & " AND " & _
             "{FRJ100.FRJ1FECHAALBAR} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ")  AND " & _
             "{FRJ100.FRJ1FECHAALBAR} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
  Else
    If UCase(strListado) = "FR5293.RPT" And Len(Trim(dtcDesde.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
             "{FRJ100.FRJ1FECHAALBAR} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ") "
    End If
    If UCase(strListado) = "FR5293.RPT" And Len(Trim(dtcHasta.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
              "{FRJ100.FRJ1FECHAALBAR} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
    End If
  End If
  If UCase(strListado) = "FR5293.RPT" And IsNumeric(txtCodPrv.Text) Then
    strWhere = strWhere & " AND {FR7900.FR79CODPROVEEDOR} = " & txtCodPrv.Text
  End If
  If UCase(strListado) = "FR5293.RPT" And Len(Trim(txtProveedor.Text)) > 0 Then
    strProveedor = UCase(txtProveedor.Text)
    strWhere = strWhere & " AND {FR7900.FR79PROVEEDOR} startswith " & Chr(34) & strProveedor & Chr(34)
  End If
  If UCase(strListado) = "FR5293.RPT" And Len(Trim(txtCodPrd.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CODINTFAR} = " & Chr(34) & txtCodPrd.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5293.RPT" And Len(Trim(txtProducto.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73DESPRODUCTOPROV} startswith " & Chr(34) & txtProducto.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5293.RPT" And Len(Trim(txtCuenCont.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CUENCONT} = " & Chr(34) & txtCuenCont.Text & Chr(34)
  End If
  '+++++
  '00000
  If UCase(strListado) = "FR5296.RPT" And _
     Len(Trim(dtcDesde.Date)) > 0 And Len(Trim(dtcHasta.Date)) > 0 Then
     strWhere = strWhere & " AND " & _
             "{FR6200.FR62FECESTUPED} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ")  AND " & _
             "{FR6200.FR62FECESTUPED} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
  Else
    If UCase(strListado) = "FR5296.RPT" And Len(Trim(dtcDesde.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
             "{FR6200.FR62FECESTUPED} >=  Date (" & Right(dtcDesde.Text, 4) & ", " & Left(Right(dtcDesde.Text, 7), 2) & ", " & Left(dtcDesde.Text, 2) & ") "
    End If
    If UCase(strListado) = "FR5296.RPT" And Len(Trim(dtcHasta.Date)) > 0 Then
      strWhere = strWhere & " AND " & _
              "{FR6200.FR62FECESTUPED} <= Date (" & Right(dtcHasta.Text, 4) & ", " & Left(Right(dtcHasta.Text, 7), 2) & ", " & Left(dtcHasta.Text, 2) & ")"
    End If
  End If
  If UCase(strListado) = "FR5296.RPT" And IsNumeric(txtCodPrv.Text) Then
    strWhere = strWhere & " AND {FR7900.FR79CODPROVEEDOR} = " & txtCodPrv.Text
  End If
  If UCase(strListado) = "FR5296.RPT" And Len(Trim(txtProveedor.Text)) > 0 Then
    strProveedor = UCase(txtProveedor.Text)
    strWhere = strWhere & " AND {FR7900.FR79PROVEEDOR} startswith " & Chr(34) & strProveedor & Chr(34)
  End If
  If UCase(strListado) = "FR5296.RPT" And Len(Trim(txtCodPrd.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CODINTFAR} = " & Chr(34) & txtCodPrd.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5296.RPT" And Len(Trim(txtProducto.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73DESPRODUCTOPROV} startswith " & Chr(34) & txtProducto.Text & Chr(34)
  End If
  If UCase(strListado) = "FR5296.RPT" And Len(Trim(txtCuenCont.Text)) > 0 Then
    strWhere = strWhere & " AND {FR7300.FR73CUENCONT} = " & Chr(34) & txtCuenCont.Text & Chr(34)
  End If
  '00000
  CrystalReport1.SelectionFormula = strWhere
  On Error GoTo Err_imp4
  CrystalReport1.Action = 1
Err_imp4:

End Sub

Private Sub calcular_precio_neto()
Dim Precio_Neto As Currency
Dim Precio_Base As Currency
Dim Descuento As Currency
Dim IVA As Currency
Dim Recargo As Currency
Dim PVL As Currency
Dim PVP As Currency
Dim Param_Gen As Currency
Dim stra As String
Dim rsta As rdoResultset
Dim strPrecNetComp As String
Dim strupdate As String
Dim strPrecVenta As String
Dim strRecProd As String
Dim rstRecProd As rdoResultset
Dim qryFR73 As rdoQuery


'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
If txtText1(4).Text <> "" Then 'Precio_Base
  Precio_Base = txtText1(4).Text
Else
  Precio_Base = 0
End If
If txtText1(25).Text <> "" Then 'Descuento
  Descuento = Precio_Base * txtText1(25).Text / 100
Else
  Descuento = 0
End If

If txtText1(2).Text <> "" Then 'Recargo
  strRecProd = "SELECT FR73RECARGO FROM FR7300 WHERE FR73CODPRODUCTO =?"
  Set qryFR73 = objApp.rdoConnect.CreateQuery("", strRecProd)
  qryFR73(0) = txtText1(2).Text
  Set rstRecProd = qryFR73.OpenResultset()
  If Not IsNull(rstRecProd.rdoColumns("FR73RECARGO").Value) Then
    Recargo = Precio_Base * rstRecProd.rdoColumns("FR73RECARGO").Value / 100
  Else
    Recargo = 0
  End If
  qryFR73.Close
  Set qryFR73 = Nothing
  Set rstRecProd = Nothing
Else
  Recargo = 0
End If

If txtText1(27).Text <> "" Then 'IVA
  IVA = (Precio_Base - Descuento + Recargo) * txtText1(27).Text / 100
Else
  IVA = 0
End If
'txtText1(28).Text = Precio_Base - Descuento + IVA + Recargo
If txtText1(28).Text <> Format(txtText1(28).Text, "0.00") Then
  'txtText1(28).Text = Format(txtText1(28).Text, "0.00")
End If
If IsNumeric(txtText1(5).Text) And IsNumeric(txtText1(28).Text) Then
  If txtText1(5).Text > 0 And txtText1(28).Text > 0 Then
    'txtText1(1).Text = Format(txtText1(28).Text * txtText1(5).Text, "0.00")
  Else
    'txtText1(1).Text = 0
  End If
Else
  'txtText1(1).Text = 0
End If
End Sub

Private Sub cmdbuscar_Click()
  Dim strClausulaWhere As String
  Dim blnTab As Boolean

cmdbuscar.Enabled = False

strClausulaWhere = " 1 = 1 " 'AND  FRJ3CANTENTREG > 0 "

'se localiza un n� de pedido concreto
If Trim(txtAlbaran.Text) <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FRJ1CODALBARAN='" & txtAlbaran.Text & "'"
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
    If tabTab1(1).Tab = 0 Then
      blnTab = True
      tabTab1(1).Tab = 1
    Else
      blnTab = False
    End If
    Call objWinInfo.DataRefresh
    If blnTab = True Then
      tabTab1(1).Tab = 0
    End If
    'cmdbuscar.Enabled = True
    'Exit Sub
End If

If chkproveedor.Value = 1 Then
    If Trim(txtbuscar.Text) <> "" Then
        strClausulaWhere = strClausulaWhere & " AND FR79CODPROVEEDOR IN (" & _
            "SELECT FR79CODPROVEEDOR " & _
            "  FROM FR7900 " & _
            " WHERE UPPER(FR79PROVEEDOR) LIKE UPPER('%" & txtbuscar.Text & "%'))"
    End If
End If

If Check3.Value = Checked Then
  If Trim(txtbuscar.Text) <> "" Then
    strClausulaWhere = strClausulaWhere & " AND FRJ1CODALBARAN IN (" & _
            "SELECT FRJ1CODALBARAN FROM FRJ300 WHERE FR73CODPRODUCTO IN ( SELECT FR73CODPRODUCTO FROM FR7300 WHERE FR73CODINTFAR = '" & Trim(txtbuscar.Text) & "')) "
  End If
End If

   If IsNumeric(txtpedido.Text) <> 0 Then
      strClausulaWhere = strClausulaWhere & " AND FRJ1CODALBARAN IN " & _
                        "(SELECT FRJ1CODALBARAN FROM FRJ200 WHERE FR62CODPEDCOMPRA = " & txtpedido.Text & ") "
   End If


If chkproducto.Value = 1 Then
    If Trim(txtbuscar.Text) <> "" Then
        strClausulaWhere = strClausulaWhere & " AND FRJ1CODALBARAN IN (" & _
            "SELECT FRJ1CODALBARAN FROM FRJ300 WHERE FR73CODPRODUCTO IN (" & _
            "SELECT FR73CODPRODUCTO from fr7300 where " & _
             "(upper(FR73DESPRODUCTOPROV) like upper('%" & txtbuscar.Text & "%') OR UPPER(FR73DESPRODUCTO) LIKE UPPER('%" & txtbuscar.Text & "%'))" & _
             ")" & _
             ")"
    End If
End If

'fecha de pedido
If dtcpedido.Text <> "" Then
    strClausulaWhere = strClausulaWhere & " AND TO_CHAR(FRJ1FECHAALBAR,'DD/MM/YYYY')=" & "'" & dtcpedido.Text & "'"
End If
'fecha de estudio
'If dtcestudio.Text <> "" Then
'    strclausulawhere = strclausulawhere & " AND TO_CHAR(FR62FECESTUPED,'DD/MM/YY')=" & "'" & dtcestudio.Text & "'"
'End If
'fecha de entrega
'If dtcentrega.Text <> "" Then
'    strclausulawhere = strclausulawhere & " AND FR62CODPEDCOMPRA IN (" & _
'        "SELECT FR62CODPEDCOMPRA FROM FR2500 WHERE " & _
'         "TO_CHAR(FR25FECPLAZENTRE,'DD/MM/YY')=" & "'" & dtcentrega.Text & "'" & _
'         ")"
'End If
    
'persona que pide
'If txtpersona.Text <> "" Then
'    strclausulawhere = strclausulawhere & " AND FR62CODPEDCOMPRA IN (" & _
'        "SELECT FR62CODPEDCOMPRA FROM FR6200 WHERE SG02COD_PID IN (" & _
'        "SELECT SG02COD FROM SG0200 WHERE " & _
'        "upper(SG02APE1) like upper('" & txtpersona.Text & "')" & _
'        ")" & _
'        ")"
'End If
    
  If chkAlbFac.Value = Checked Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.objWinActiveForm.strWhere = strClausulaWhere & " AND FRJ1ESTADO = 1 "
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.objWinActiveForm.strWhere = ""
  Else
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.objWinActiveForm.strWhere = strClausulaWhere & " AND FRJ1ESTADO <> 1 "
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    objWinInfo.objWinActiveForm.strWhere = " FRJ3CANTPTEFACT > 0 "
  End If

  
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'If Len(strClausulaWhere) > 0 Then
  'strClausulaWhere = " AND " & strClausulaWhere
  'End If
  'objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
  If tabTab1(1).Tab = 0 Then
    blnTab = True
    tabTab1(1).Tab = 1
  Else
    blnTab = False
  End If
  Call objWinInfo.DataRefresh
  If blnTab = True Then
    tabTab1(1).Tab = 0
  End If
  cmdbuscar.Enabled = True
End Sub

'Private Sub cmdCancelar_Click()
'    Dim strupdate As String
'    If txtText1(6).Text <> "" Then
'        strupdate = "UPDATE FR6200 SET FR95CODESTPEDCOMPRA=0 where fr62codpedcompra=" & txtText1(6).Text
'        objApp.rdoConnect.Execute strupdate, 64
'        MsgBox "Pedido n� " & txtText1(6).Text & " Cancelado", vbExclamation
'        objWinInfo.DataRefresh
'    End If
'End Sub

'Private Sub cmdEmitir_Click()
'    'Dim strUpdate As String
'    If txtText1(6).Text <> "" Then
'        'strUpdate = "UPDATE FR6200 SET FR62FECESTUPEC=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
'                    "FR95ESTPEDCOMPRA=2 where fr62codpedcompra=" & txtText1(6).Text
'        'objApp.rdoConnect.Execute strUpdate, 64
'        If txtText1(8).Text <> "" Then
'            gintProveedor(1) = txtText1(8).Text
'            gintProveedor(2) = txtText1(6).Text
'            gstrLlamador = "FrmConSitPed"
'            Call objsecurity.LaunchProcess("FR0524")
'        Else
'            Call MsgBox("No hay ning�n Proveedor", vbExclamation, "Aviso")
'        End If
'    End If
'End Sub

Private Sub cmdFacturar_Click()
   
   'If gintCodsAlbs = 0 Then
   ' MsgBox "Utilice el bot�n A�adir para confeccionar la factura", vbInformation, "Facturar "
   ' Exit Sub
   'End If
   
   tabTab1(1).Tab = 0
   If txtText1(1).Text = 1 Then
      MsgBox "Albar�n Facturado", vbInformation, "A�adir a la lista"
      Exit Sub
   End If
   cmdFacturar.Enabled = False
   Me.Enabled = False
   'If IsNumeric(txtText1(8).Text) And Len(Trim(txtText1(6).Text)) > 0 Then
      ' Hay que comprobar que todos los albaranes
      ' seleccionados son del mismo proveedor
      'gintCodsAlbs = grdDBGrid1(0).SelBookmarks.Count
      'If gintCodsAlbs = 0 Then
      '   gstrCodProv = txtText1(8).Text
      '   gintCodsAlbs = 1
      '   ReDim gstrCodsAlbs(gintCodsAlbs)
      '   gstrCodsAlbs(0) = txtText1(6).Text
      'Else
      '   If IsNumeric(grdDBGrid1(0).Columns(3).CellValue(grdDBGrid1(0).SelBookmarks(0))) Then
      '      ReDim gstrCodsAlbs(gintCodsAlbs)
      '      gstrCodProv = grdDBGrid1(0).Columns(3).CellValue(grdDBGrid1(0).SelBookmarks(0))
      '      For Q% = 0 To gintCodsAlbs - 1
      '         gstrCodsAlbs(Q%) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(Q%))
      '         If gstrCodProv <> grdDBGrid1(0).Columns(3).CellValue(grdDBGrid1(0).SelBookmarks(Q%)) Then
      '            Call MsgBox("No se pueden facturar juntos albaranes de distinto proveedor", vbInformation, "Aviso")
      '            gintCodsAlbs = 0
      '            ReDim gstrCodsAlbs(gintCodsAlbs)
      '            cmdFacturar.Enabled = True
      '            Exit Sub
      '         End If
      '      Next Q%
      '   Else
      '      MsgBox "Todos los albaranes seleccionados han de ser de un proveedor", vbInformation, "Aviso"
      '   End If
      'End If
      'Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      'objWinInfo.DataSave
      gstrLlamadorFac = "CA"
      gintCodsAlbs = Q
      gintF = 0
      Dim a As Integer
      For a = 1 To 250
        gstrLF(a, 2) = 0
      Next a
      Call objsecurity.LaunchProcess("FR0527") 'Introducir Factura de Compra
      gstrLlamadorFac = ""
      gstrCodProv = ""
      gintCodsAlbs = 0
      Q = 0
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      objWinInfo.DataRefresh
      DoEvents
      Dim curS As Currency
      Dim asi As Integer
      
      If gintF > 0 Then
        For curS = 0 To 10000000
          asi = 1
        Next curS
        For a = 1 To 250
          If gstrLF(a, 2) <> 0 Then
            Dim stra As String
            Dim qryA As rdoQuery
            Dim rsta As rdoResultset
            
            stra = "SELECT FR39CODFACTCOMPRA,MAX(FR15IMPORTE) " & _
                 "  FROM FR1500 " & _
                 " WHERE FR39CODFACTCOMPRA = ? " & _
                 " GROUP BY FR39CODFACTCOMPRA "
           Set qryA = objApp.rdoConnect.CreateQuery("", stra)
           qryA(0) = gstrLF(a, 1)
           Set rsta = qryA.OpenResultset()
           Dim strB As String
           Dim rstb As rdoResultset
           If Not rsta.EOF Then
            strB = "SELECT FR15CODDETFACTCOMP " & _
                    "  FROM FR1500 " & _
                    " WHERE FR15IMPORTE >= " & objGen.ReplaceStr(rsta.rdoColumns(1).Value, ",", ".", 1) & " AND FR39CODFACTCOMPRA = '" & gstrLF(a, 1) & "'"
            Set rstb = objApp.rdoConnect.OpenResultset(strB)
            If Not rstb.EOF Then
               stra = "UPDATE FR1500 " & _
                      "   SET FR15IMPORTE = FR15IMPORTE + " & objGen.ReplaceStr(gstrLF(a, 2), ",", ".", 1) & _
                      " WHERE FR39CODFACTCOMPRA = '" & gstrLF(a, 1) & "'" & _
                      "   AND FR15CODDETFACTCOMP = " & rstb.rdoColumns(0).Value
              objApp.rdoConnect.Execute stra, 64
            End If
           End If
          End If
        Next a
      End If
      
   'Else
   '   MsgBox "Para facturar ha de tener seleccionado un albar�n que tenga proveedor", vbInformation, "Aviso"
   'End If
   cmdFacturar.Enabled = True
   Me.Enabled = True

End Sub

Private Sub cmdInList_Click()
   Dim i As Integer
   Dim intInd As Integer
   Dim blnIn As Boolean
   Dim blnEsta As Boolean
   Dim blnAF As Boolean
   Dim strAF As String
   Dim intJ As Integer
   Dim blnH As Boolean
   Dim blnMsg As Boolean
   
   blnMsg = False
   
   If objWinInfo.objWinActiveForm.strName <> "Albaranes" Then
    'Exit Sub
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
   End If
   
   strAF = "Albaranes que ya estaban facturados: " & Chr(13)
   blnAF = False
   blnIn = False
   cmdInList.Enabled = False
   Me.Enabled = False
   If tabTab1(0).Tab = 0 And IsNumeric(txtText1(1).Text) Then
    'Detalle
    If IsNumeric(txtText1(8).Text) And Len(Trim(txtText1(6).Text)) > 0 And txtText1(1).Text <> 1 Then
      ' Hay que comprobar que todos los albaranes
      ' seleccionados son del mismo proveedor
      If (Q = 0) Then
         gstrCodProv = txtText1(8).Text
         gintCodsAlbs = 1
         gstrCodsAlbs(0) = txtText1(6).Text
         blnMsg = True
         Q = 1
      Else
        blnH = False
        For intJ = 0 To Q - 1
          If gstrCodsAlbs(intJ) = txtText1(6).Text Then
            blnH = True
          End If
        Next intJ
        If (gstrCodProv = txtText1(8).Text) And (blnH = False) Then
          gintCodsAlbs = gintCodsAlbs + 1
          gstrCodsAlbs(Q) = txtText1(6).Text
          blnMsg = True
          Q = Q + 1
        Else
          If blnH Then
            MsgBox "El albar�n ya estaba a�adido", vbInformation, "A�adir a la lista"
          Else
            MsgBox "No se pueden facturar juntos albaranes de diferentes proveedores", vbInformation, "A�adir a la lista"
          End If
        End If
      End If
    Else
      If txtText1(1).Text = 1 Then
        MsgBox "Albar�n Facturado", vbInformation, "A�adir a la lista"
      End If
    End If
   Else
    'Tabla
    'Hay que comprobar que todos los albaranes seleccionados son del mismo proveedor
    gintCodsAlbs = grdDBGrid1(0).SelBookmarks.Count
    If gintCodsAlbs = 0 Then
      MsgBox "Debe seleccionar los albaranes", vbInformation, "A�adir Albar�n"
    Else
      For i = 0 To gintCodsAlbs - 1
       If grdDBGrid1(0).Columns("E").CellValue(grdDBGrid1(0).SelBookmarks(i)) = 0 Then
        If (Q = 0) And (i = 0) Then
          gstrCodProv = grdDBGrid1(0).Columns(3).CellValue(grdDBGrid1(0).SelBookmarks(0))
          gstrCodsAlbs(Q) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(0))
          blnMsg = True
          Q = Q + 1
        Else
          If gstrCodProv = grdDBGrid1(0).Columns(3).CellValue(grdDBGrid1(0).SelBookmarks(i)) Then
            blnH = False
            For intJ = 0 To Q - 1
              If gstrCodsAlbs(intJ) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(i)) Then
                blnH = True
              End If
            Next intJ
            If blnH = False Then
              gstrCodsAlbs(Q) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(i))
              blnMsg = True
              Q = Q + 1
            Else
              MsgBox "El albar�n ya estaba a�adido", vbInformation, "A�adir a la lista"
            End If
          Else
            MsgBox "No se pueden facturar albaranes de diferentes proveedores en la misma factura", vbInformation, "A�adir a la lista"
          End If
        End If
       Else
        If grdDBGrid1(0).Columns("E").CellValue(grdDBGrid1(0).SelBookmarks(i)) = 1 Then
          MsgBox "Albar�n Facturado", vbInformation, "A�adir a la lista"
        End If
       End If
      Next i
    End If
   End If
   
   If blnAF Then
    MsgBox strAF, vbInformation, "A�adir Albar�n a la Lista"
   End If
   If blnMsg = True Then
     MsgBox "Albaranes a�adidos", vbInformation, "A�ador Albar�n a la Lista"
   End If
   cmdInList.Enabled = True
   Me.Enabled = True

End Sub

Private Sub cmdreclamar_Click()
    Dim strupdate As String
    Me.Enabled = False
    If txtText1(6).Text <> "" And txtText1(8).Text <> "" Then
'        strupdate = "UPDATE FR6200 SET FR95CODESTPEDCOMPRA=3 where fr62codpedcompra=" & txtText1(6).Text
'        objApp.rdoConnect.Execute strupdate, 64
'        gintLlamadorReclamar = "FrmConSitPed"
        gstrCodAlb = txtText1(6).Text
        gstrCodProv = txtText1(8).Text
        Call objsecurity.LaunchProcess("FR0530")
        gstrCodProv = ""
        gstrCodAlb = ""
    End If
    Me.Enabled = True
End Sub

Private Sub Form_Activate()
    'txtbonificacion.Locked = True
    Q = 0
    mblnAux = False
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------



Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim strKey As String
  

  Set objWinInfo = New clsCWWin
  
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Albaranes"
      
    .strTable = "FRJ100"
    .strWhere = " FRJ1ESTADO <> 1 "
    
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FRJ1CODALBARAN", cwAscending)
    
    Call .objPrinter.Add("FR5291", "Albaranes Pendientes de Facturar")
    Call .objPrinter.Add("FR5292", "Entradas Facturadas")
    Call .objPrinter.Add("FR5293", "Entradas Por Cuenta Contable")
    Call .objPrinter.Add("FR5296", "Pedidos Realizados")
  
    
    strKey = .strDataBase & .strTable
        
    Call .FormCreateFilterWhere(strKey, "Albaranes")
    Call .FormAddFilterWhere(strKey, "FRJ1CODALBARAN", "N� Albar�n", cwString)
    Call .FormAddFilterWhere(strKey, "FRJ1FECHAALBAR", "Fecha Albar�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "N� Proveedor", cwNumeric)
        
    Call .FormAddFilterOrder(strKey, "FRJ1CODALBARAN", "N� Albar�n")
    Call .FormAddFilterOrder(strKey, "FRJ1FECHAALBAR", "Fecha Albar�n")
    Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "N� Proveedor")
    
  End With
  With objDetailInfo
    .strName = "Detalle Albar�n"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FRJ300"
    .intCursorSize = 100
    .intAllowance = cwAllowReadOnly
    .strWhere = " FRJ3CANTPTEFACT > 0 "
    
    Call .FormAddOrderField("FRJ1CODALBARAN", cwAscending)
    Call .FormAddOrderField("FRJ3NUMLINEA", cwAscending)
    
    Call .FormAddRelation("FRJ1CODALBARAN", txtText1(6))
    
    'Call .objPrinter.Add("PR1281", "Listado por Departamentos con sus Actuaciones")
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Albar�n")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ3CANTENTREG", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ3PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ3IMPORLINEA", "Importe de Linea", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
    Call .FormAddFilterOrder(strKey, "FRJ3CANTENTREG", "Cantidad")
    Call .FormAddFilterOrder(strKey, "FRJ3PRECIOUNIDAD", "Precio Unidad")
    Call .FormAddFilterOrder(strKey, "FRJ3NUMLINEA", "N� L�nea")
  End With
  
  With objMultiInfo1
    .strName = "Pedidos"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "FRJ200"
    
    
    .intAllowance = cwAllowReadOnly
            
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
    Call .FormAddRelation("FRJ1CODALBARAN", txtText1(6))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Pedidos")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "N� Pedido", cwNumeric)
  End With
   
  With objMultiInfo2
    .strName = "Facturas"
    Set .objFormContainer = fraFrame1(3)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(3)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "FRJ400"
    
    
    .intAllowance = cwAllowReadOnly
            
    Call .FormAddOrderField("FR39CODFACTCOMPRA", cwAscending)
    Call .FormAddRelation("FRJ1CODALBARAN", txtText1(6))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Facturas")
    Call .FormAddFilterWhere(strKey, "FR39CODFACTCOMPRA", "N� Factura", cwString)
  End With
   With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
    
    Call .GridAddColumn(objMultiInfo1, "Albar�n", "FRJ1CODALBARAN", cwString, 9)
    Call .GridAddColumn(objMultiInfo1, "C�d.Pedido", "FR62CODPEDCOMPRA", cwNumeric, 9)
    
    Call .GridAddColumn(objMultiInfo2, "Albar�n", "FRJ1CODALBARAN", cwString, 9)
    Call .GridAddColumn(objMultiInfo2, "N� Factura", "FR39CODFACTCOMPRA", cwString, 15)
    Call .GridAddColumn(objMultiInfo2, "Ap", "", cwBoolean, 1)
    Call .FormCreateInfo(objMasterInfo)
    
    Call .FormChangeColor(objMultiInfo1)
    
    Call .FormChangeColor(objMultiInfo2)
    
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    
      
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True 'C�d. Interno
    .CtrlGetInfo(txtText1(23)).blnReadOnly = True 'D�gito de control
    .CtrlGetInfo(txtText1(28)).blnReadOnly = True 'Precio Neto
    .CtrlGetInfo(txtText1(17)).blnReadOnly = True 'Importe
    .CtrlGetInfo(txtText1(18)).blnReadOnly = True 'Moneda
    '.CtrlGetInfo(txtText1(1)).blnReadOnly = True 'Cant. Pendiente
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True 'Acumulado recibido
    .CtrlGetInfo(txtText1(0)).blnInGrid = False
    .CtrlGetInfo(txtText1(19)).blnInGrid = False
'    .CtrlGetInfo(txtText1(20)).blnInGrid = False
    .CtrlGetInfo(txtText1(21)).blnInGrid = False
    '.CtrlGetInfo(txtText1(17)).blnInGrid = False
    '.CtrlGetInfo(txtText1(1)).blnReadOnly = False
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "FR73DESPRODUCTOPROV")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(28), "FR73PRECIONETCOMPRA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(13), "FRH7CODFORMFAR")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(10), "FR73DOSIS")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(11), "FR93CODUNIMEDIDA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(12), "FR73VOLUMEN")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(14), "FR73REFERENCIA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(4), "FR73PRECBASE")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(24), "FR73CODINTFAR")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(23), "FR73CODINTFARSEG")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(4)), "FR39CODFACTCOMPRA", "SELECT * FROM FR3900 WHERE FR39CODFACTCOMPRA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(3).Columns(4)), grdDBGrid1(3).Columns(5), "FR39INDAPROBADA")
    
    
    grdDBGrid1(2).Columns(3).Visible = False ' C�digo de albar�n el el frame pedidos
    grdDBGrid1(3).Columns(3).Visible = False ' C�digo de albar�n el el frame factuuras
    
'    grdDBGrid1(0).Columns(4).Visible = False
'    grdDBGrid1(0).Columns(5).Visible = False
'    grdDBGrid1(0).Columns(6).Visible = False
    
    Call .WinRegister
    Call .WinStabilize
  End With
 
 'cmdEmitir.Enabled = False
 cmdReclamar.Enabled = True
 'cmdCancelar.Enabled = False
 cmdFacturar.Enabled = True
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
  Dim objField As clsCWFieldSearch
  If strFormName = "Albar�n" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     '.strWhere = "WHERE FR73FECFINVIG IS NULL" 'AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR79CODPROVEEDOR ASC"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo del Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n del Proveedor "

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
  End If
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Dim intcantenvase As Integer
If grdDBGrid1(0).Rows > 0 Then
     grdDBGrid1(0).Columns(1).Width = 1000 'N�Pedido
     grdDBGrid1(0).Columns(2).Width = 1400 'FechaPedido
     grdDBGrid1(0).Columns(3).Width = 1000 'c�d.prov
     grdDBGrid1(0).Columns(4).Width = 2600 'proveedor
     grdDBGrid1(0).Columns(5).Width = 0 'c�d.estado
     grdDBGrid1(0).Columns(6).Width = 1200 'estado
     grdDBGrid1(0).Columns(7).Width = 3100 'descripci�n
     
     grdDBGrid1(0).Columns(5).Visible = False 'c�d.estado
     grdDBGrid1(0).Columns(6).Visible = False 'estado
     grdDBGrid1(0).Columns(7).Visible = False 'descripci�n
End If
If grdDBGrid1(1).Rows > 0 Then
    grdDBGrid1(1).Columns("C�d.Prod").Width = 0
    'grdDBGrid1(1).Columns("C�d.Interno").Width = 700
    'grdDBGrid1(1).Columns("Seg").Width = 450
    grdDBGrid1(1).Columns("Descripci�n Producto").Width = 3000
    'grdDBGrid1(1).Columns("F.F").Width = 450
    'grdDBGrid1(1).Columns("Dosis").Width = 600
    'grdDBGrid1(1).Columns("U.M").Width = 500
    'grdDBGrid1(1).Columns("Vol.").Width = 600
    'grdDBGrid1(1).Columns("Referencia").Width = 1000
    grdDBGrid1(1).Columns("PPE").Width = 1000
    grdDBGrid1(1).Columns("Importe").Width = 1300
    grdDBGrid1(1).Columns("Moneda").Width = 800
    grdDBGrid1(1).Columns("Envase").Width = 800
    grdDBGrid1(1).Columns("Cant Pedida").Visible = False
    'grdDBGrid1(1).Columns("Cant Pedida").Width = 1220
    grdDBGrid1(1).Columns("Rdo.").Width = 700
    'grdDBGrid1(1).Columns("Importe").Position = 7
    'grdDBGrid1(1).Columns("Cant Recibida").Width = 1400
End If
calcular_precio_neto
 'If txtText1(2).Text <> "" Then
 '  If (txtText1(15).Text <> "") And (txtText1(26).Text <> "") Then
 '   If IsNumeric(txtText1(29).Text) And (txtText1(29).Text <> "") Then
 '     intcantenvase = txtText1(29).Text
 '   Else
 '     intcantenvase = 0
 '   End If
 '    txtbonificacion.Text = Fix(((txtText1(15).Text * txtText1(26).Text) / 100) * intcantenvase)
 '  Else
 '   txtbonificacion.Text = 0
 '  End If
 'Else
 ' txtbonificacion.Text = 0
 'End If
  
    'Se ha modificado la cantidad o la bonificaci�n por lo que hay que actualizar la ficha del producto
    'If (IsNumeric(txtText1(15).Text)) And (IsNumeric(txtText1(2).Text)) Then
    '  mdblCantPed = txtText1(15).Text
    '  mdblCodProd = txtText1(2).Text
    '  If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
    '    mdblTamEnva = txtText1(29).Text
    '  Else
    '    If (IsNumeric(txtText1(2).Text)) Then
    '      mdblTamEnva = 1
    '      mdblCodProd = txtText1(2).Text
    '    End If
    '  End If
    'End If
    
    'If (IsNumeric(txtText1(26).Text)) And (IsNumeric(txtText1(2).Text)) Then
    '  mdblCantPend = txtText1(26).Text
    '  mdblCodProd = txtText1(2).Text
    '  If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
    '    mdblTamEnva = txtText1(29).Text
    '  Else
    '    If (IsNumeric(txtText1(2).Text)) Then
    '      mdblTamEnva = 1
    '      mdblCodProd = txtText1(2).Text
    '    End If
    '  End If
    'End If
    
    'If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
    '  mdblTamEnva = txtText1(29).Text
    '  mdblCodProd = txtText1(2).Text
    'Else
    '  If (IsNumeric(txtText1(2).Text)) Then
    '    mdblTamEnva = 1
    '    mdblCodProd = txtText1(2).Text
    '  End If
    'End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim strUpdProd As String
Dim dblBonifi As Double
Dim dblEnvase As Double
Dim dblOLDBonif As Double


  'If mblnActCantPend = True Then
  '  'Hay que actualizar la ficha del producto con las unidades pendientes de entregar
  '    If (IsNumeric(mdblCantPed)) And (IsNumeric(txtText1(15).Text)) Then
  '      mblnActCantPed = False
  '      If IsNumeric(txtText1(29).Text) Then
  '        dblEnvase = txtText1(29).Text
  '      Else
  '        dblEnvase = 1
  '      End If
  '      strUpdProd = "UPDATE FR7300 " & _
  '                   " SET FR73CANTPEND = FR73CANTPEND + " & objGen.ReplaceStr(((txtText1(15).Text * dblEnvase) - (mdblCantPed * mdblTamEnva)), ",", ".", 1) & _
  '                   " WHERE FR73CODPRODUCTO = " & mdblCodProd
  '      objApp.rdoConnect.Execute strUpdProd, 64
  '    End If
  '    If (IsNumeric(txtText1(26).Text)) And (IsNumeric(mdblCantPend)) And mblnActCantPend Then
  '      mblnActCantPend = True
  '      If IsNumeric(txtText1(29).Text) Then
  '        dblEnvase = txtText1(29).Text
  '      Else
  '        dblEnvase = 1
  '      End If
  '      dblBonifi = Fix(((txtText1(15).Text * txtText1(26).Text) / 100) * dblEnvase)
  '      dblOLDBonif = Fix(((mdblCantPed * mdblCantPend) / 100) * mdblTamEnva)
  '      strUpdProd = "UPDATE FR7300 " & _
  '                   " SET FR73PTEBONIF = FR73PTEBONIF + " & objGen.ReplaceStr((dblBonifi - dblOLDBonif), ",", ".", 1) & _
  '                   " WHERE FR73CODPRODUCTO = " & mdblCodProd
  '      objApp.rdoConnect.Execute strUpdProd, 64
  '    End If
  '  End If
 

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strOrden As String
  
  'If strFormName = "Proveedores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      Select Case intReport
        Case 1
          Call Imprimir("FR5291.RPT", 0)
        Case 2
          Call Imprimir("FR5292.RPT", 0)
        Case 3
          Call Imprimir("FR5293.RPT", 0)
        Case 4
          Call Imprimir("FR5296.RPT", 0)
      End Select
           
    End If
    Set objPrinter = Nothing
  'End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
  If btnButton.Index = 26 Then
    cmdbuscar_Click
  Else
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
  End If
'Localizar
'If (btnButton.Index = 16) Or (btnButton.Index = 21) Or (btnButton.Index = 22) Or (btnButton.Index = 23) Or (btnButton.Index = 24) Then
'    Select Case txtText1(16).Text
'        Case 0, 5, 7 'cancelado,recibido completo,cerrado
'            'cmdEmitir.Enabled = False
'            cmdReclamar.Enabled = False
'            'cmdCancelar.Enabled = False
'            cmdFacturar.Enabled = False
'        Case 1 'generado
'            'cmdEmitir.Enabled = True
'            cmdReclamar.Enabled = False
'            'cmdCancelar.Enabled = False
'            cmdFacturar.Enabled = False
'        Case 2, 3, 4 'emitido
'            cmdEmitir.Enabled = False
'            cmdReclamar.Enabled = True
'            cmdCancelar.Enabled = True
'            cmdFacturar.Enabled = True
'        'Case 3 'reclamado,recibido parcial
'        '    cmdEmitir.Enabled = False
'        '    cmdReclamar.Enabled = False
'        '    cmdCancelar.Enabled = True
'        '    cmdFacturar.Enabled = False
'       Case 6 'cerrado parcialmente completo
'            cmdEmitir.Enabled = False
'            cmdReclamar.Enabled = True
'            cmdCancelar.Enabled = False
'            cmdFacturar.Enabled = False
'    End Select
    
'End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
'Localizar
'If (intIndex = 10) Or (intIndex = 40) Or (intIndex = 50) Or (intIndex = 60) Or (intIndex = 70) Then
'    Select Case txtText1(16).Text
'        Case 0, 5, 7 'cancelado,recibido completo,cerrado
'            cmdEmitir.Enabled = False
'            cmdReclamar.Enabled = False
'            cmdCancelar.Enabled = False
'            cmdFacturar.Enabled = False
'        Case 1 'generado
'            cmdEmitir.Enabled = True
'            cmdReclamar.Enabled = False
'            cmdCancelar.Enabled = False
'            cmdFacturar.Enabled = False
'        Case 2, 3, 4 'emitido
'            cmdEmitir.Enabled = False
'            cmdReclamar.Enabled = True
'            cmdCancelar.Enabled = True
'            cmdFacturar.Enabled = True
'        'Case 3 'reclamado
'        '    cmdEmitir.Enabled = False
'        '    cmdReclamar.Enabled = False
'        '    cmdCancelar.Enabled = True
'        '    cmdFacturar.Enabled = False
'        Case 6  'recibido parcial,cerrado parcialmente completo
'            cmdEmitir.Enabled = False
'            cmdReclamar.Enabled = True
'            cmdCancelar.Enabled = False
'            cmdFacturar.Enabled = False
'    End Select
'End If
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
  If tabTab1(0).Caption = "Tabla" Then
    fraFrame1(2).Visible = False
    fraFrame1(3).Visible = False
  End If
  If tabTab1(0).Caption = "Detalle" Then
    fraFrame1(2).Visible = True
    fraFrame1(3).Visible = True
  End If
  
 ' tabTab1(0).Caption
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub




Private Sub txtbuscar_GotFocus()
    Me.KeyPreview = False
End Sub

Private Sub txtbuscar_LostFocus()
    Me.KeyPreview = True
End Sub

Private Sub txtpersona_GotFocus()
    Me.KeyPreview = False
End Sub

Private Sub txtpersona_LostFocus()
    Me.KeyPreview = True
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim intcantenvase As Integer
Dim curImporte As Currency
Dim intResp As Integer
    
  'If (intIndex = 15) Or (intIndex = 26) Or (intIndex = 29) Then
  '  'Se ha modificado la cantidad o la bonificaci�n por lo que hay que actualizar la ficha del producto
  '  If (IsNumeric(txtText1(15).Text)) And (IsNumeric(txtText1(2).Text)) Then
  '    mblnActCantPed = True
  '  End If
  '  If (IsNumeric(txtText1(26).Text)) And (IsNumeric(txtText1(2).Text)) Then
  '    mblnActCantPend = True
  '  End If
  '  If (IsNumeric(txtText1(29).Text)) And (IsNumeric(txtText1(2).Text)) Then
  '    mblnTamEnva = True
  '  End If
  'End If
  
  Call objWinInfo.CtrlDataChange
  
'cuando se introduce la cantidad de producto a pedir, se calcula la bonificaci�n
' If intIndex = 15 And txtText1(2).Text <> "" Then
'   If txtText1(15).Text <> "" Then
'     sqlstr = "SELECT FR25BONIF FROM FR2500 WHERE FR73CODPRODUCTO=" & txtText1(2).Text
'     Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
'     If Not rsta.EOF Then
'      If Not IsNull(rsta.rdoColumns(0).Value) Then
'           txtbonificacion.Text = (txtText1(15).Text * rsta.rdoColumns(0).Value) / 100
'      Else
'           txtbonificacion.Text = 0
'      End If
'     End If
'     rsta.Close
'     Set rsta = Nothing
'   End If
' End If
'  If (intIndex = 15) Or (intIndex = 26) Then
'    'Se ha modificado la cantidad o la bonificaci�n por lo que hay que actualizar la ficha del producto
'    mblnActualizar = True
'  End If
  If intIndex = 5 Or intIndex = 28 Then
    'Se calcula el importe
    If IsNumeric(txtText1(5).Text) And IsNumeric(txtText1(28).Text) Then
      curImporte = 0
      curImporte = txtText1(5).Text * txtText1(28).Text
      If curImporte > 100000000 Then
      '  intresp = MsgBox("Revise el pedido ya que el importe es superior a cien millones", vbInformation, "Informaci�n")
      Else
        'txtText1(17).Text = Format(curImporte, "0.###")
      End If
    End If
  End If
'If (intIndex = 15) Or (intIndex = 26) And txtText1(2).Text <> "" Then
'   If (txtText1(15).Text <> "") And (txtText1(26).Text <> "") Then
'    If IsNumeric(txtText1(29).Text) And (txtText1(29).Text <> "") Then
'      intcantenvase = txtText1(29).Text
'    Else
'      intcantenvase = 0
'    End If
'     txtbonificacion.Text = Fix(((txtText1(15).Text * txtText1(26).Text) / 100) * intcantenvase)
'   Else
'    txtbonificacion.Text = 0
'   End If
' Else
'  txtbonificacion.Text = 0
' End If
  
 If (intIndex = 4) Or (intIndex = 25) Or (intIndex = 27) Then 'Se ha cambiado el precio por unidad, el descuento o el iva
  calcular_precio_neto
 End If
 'estado del pedido
 'If intIndex = 16 Then
 '   Select Case txtText1(16).Text
 '       Case 0, 5, 7 'cancelado,recibido completo,cerrado
 '           cmdEmitir.Enabled = False
 '           cmdReclamar.Enabled = False
 '           cmdCancelar.Enabled = False
 '           cmdFacturar.Enabled = False
 '       Case 1 'generado
 '           cmdEmitir.Enabled = True
 '           cmdReclamar.Enabled = False
 '           cmdCancelar.Enabled = False
 '           cmdFacturar.Enabled = False
 '       Case 2, 3, 4 'emitido
 '           cmdEmitir.Enabled = False
 '           cmdReclamar.Enabled = True
 '           cmdCancelar.Enabled = True
 '           cmdFacturar.Enabled = True
 '       'Case 3 'reclamado
 '       '    cmdEmitir.Enabled = False
 '       '    cmdReclamar.Enabled = False
 '       '    cmdCancelar.Enabled = True
 '       '    cmdFacturar.Enabled = False
 '       Case 6  'recibido parcial,cerrado parcialmente completo
 '           cmdEmitir.Enabled = False
 '           cmdReclamar.Enabled = True
 '           cmdCancelar.Enabled = False
 '           cmdFacturar.Enabled = False
 '   End Select
 '
 'End If
 
End Sub








