VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmIntRecCom 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Albaranes"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0525.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pedidos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3285
      Index           =   2
      Left            =   8040
      TabIndex        =   23
      Top             =   600
      Width           =   3825
      Begin VB.CommandButton cmdaceptar 
         Caption         =   "&Aceptar Pedido"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   960
         TabIndex        =   24
         Top             =   2520
         Width           =   1575
      End
      Begin VB.Frame Frame1 
         Height          =   855
         Left            =   120
         TabIndex        =   26
         Top             =   2280
         Visible         =   0   'False
         Width           =   1935
         Begin VB.OptionButton optparcial 
            Caption         =   "Recibido Parcial"
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   120
            TabIndex        =   28
            Top             =   240
            Width           =   1575
         End
         Begin VB.OptionButton optcompleto 
            Caption         =   "Recibido Completo"
            ForeColor       =   &H00C00000&
            Height          =   255
            Left            =   120
            TabIndex        =   27
            Top             =   480
            Width           =   1695
         End
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1875
         Index           =   2
         Left            =   120
         TabIndex        =   25
         Top             =   360
         Width           =   2895
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   5106
         _ExtentY        =   3307
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Albar�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3285
      Index           =   0
      Left            =   150
      TabIndex        =   9
      Top             =   600
      Width           =   7785
      Begin TabDlg.SSTab tabTab1 
         Height          =   2775
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   7530
         _ExtentX        =   13282
         _ExtentY        =   4895
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0525.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(20)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(11)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label2"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).ControlCount=   11
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0525.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ1CODALBARAN"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Albar�n"
            Top             =   480
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   840
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Proveedor"
            Top             =   1920
            Width           =   5895
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ1ESTADO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   3840
            TabIndex        =   1
            Tag             =   "Estado"
            Top             =   480
            Visible         =   0   'False
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   2
            Left            =   240
            TabIndex        =   3
            Tag             =   "C�digo Proveedor"
            Top             =   1920
            Width           =   600
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2490
            Index           =   0
            Left            =   -74880
            TabIndex        =   11
            Top             =   120
            Width           =   6975
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   12303
            _ExtentY        =   4392
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRJ1FECHAALBAR"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Fecha Albar�n"
            Top             =   1185
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "1  Facturado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   4680
            TabIndex        =   17
            Top             =   600
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "0  No Facturado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00C00000&
            Height          =   195
            Left            =   4680
            TabIndex        =   16
            Top             =   360
            Visible         =   0   'False
            Width           =   1395
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Albar�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   15
            Top             =   240
            Width           =   1110
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   240
            TabIndex        =   14
            Top             =   1680
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   3840
            TabIndex        =   13
            Top             =   240
            Visible         =   0   'False
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Albar�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   240
            TabIndex        =   12
            Top             =   960
            Width           =   1245
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4065
      Index           =   1
      Left            =   120
      TabIndex        =   7
      Top             =   3960
      Width           =   11745
      Begin VB.CommandButton cmdOK 
         Caption         =   "&OK"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1200
         TabIndex        =   29
         Top             =   360
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CommandButton cmdcontrol 
         Caption         =   "&Control de Lotes"
         Height          =   330
         Left            =   5400
         TabIndex        =   22
         Top             =   360
         Width           =   1575
      End
      Begin VB.CommandButton cmdreclamar 
         Caption         =   "Rec&lamar"
         Height          =   330
         Left            =   3720
         TabIndex        =   21
         Top             =   360
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.CheckBox chkverprod 
         Caption         =   "Ver Productos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   255
         Left            =   480
         TabIndex        =   20
         Top             =   360
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.CommandButton cmdmedicamento 
         Caption         =   "&Medicamentos"
         Height          =   330
         Left            =   7440
         TabIndex        =   19
         Top             =   360
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdmaterial 
         Caption         =   "Material &Sanitario"
         Height          =   330
         Left            =   9360
         TabIndex        =   18
         Top             =   360
         Visible         =   0   'False
         Width           =   1815
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3105
         Index           =   1
         Left            =   120
         TabIndex        =   8
         Top             =   840
         Width           =   11535
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20346
         _ExtentY        =   5477
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmIntRecCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmIntRecCom (FR0525.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: Introducir Recepci�n de Compra                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnCancelar As Boolean 'False si no ha habido problemas al salvar
Dim mcurRec As Currency

Private Sub chkverprod_Click()
    If chkverprod.Value = 1 Then
      Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
      Call objWinInfo.DataRefresh
      chkverprod.Value = 0
    End If
End Sub
Private Sub Calcular_Precio_Unitario(ByVal curTam As Currency, _
                                          ByVal strGtp As String, _
                                          ByVal curPrecNeto As Currency, _
                                          ByVal curTamPrd As Currency, _
                                          ByRef strPVPU As String, _
                                          ByRef strPVLU As String)
  'Calcual el precio unitario de un producto
  Dim Precio_Neto As Currency
  Dim Precio_Base As Currency
  Dim Descuento As Currency
  Dim IVA As Currency
  Dim Recargo As Currency
  Dim PVL As Currency
  Dim PVP As Currency
  Dim Param_Gen As Currency
  Dim stra As String
  Dim rsta As rdoResultset
  Dim strPrecNetComp As String
  Dim strupdate As String
  Dim strPrecVenta As String

  If UCase(Left(strGtp, 1)) = "Q" Then
    'PVP
    Precio_Neto = curPrecNeto
    If Precio_Neto <= 2500 Then
      stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=22"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
        Param_Gen = Precio_Neto * Param_Gen / 100
      Else
        Param_Gen = 0
      End If
      rsta.Close
      Set rsta = Nothing
    ElseIf Precio_Neto <= 5000 Then
      stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=23"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
        Param_Gen = Precio_Neto * Param_Gen / 100
      Else
        Param_Gen = 0
      End If
      rsta.Close
      Set rsta = Nothing
    ElseIf Precio_Neto <= 10000 Then
      stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=24"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
        Param_Gen = Precio_Neto * Param_Gen / 100
      Else
        Param_Gen = 0
      End If
      rsta.Close
      Set rsta = Nothing
    ElseIf Precio_Neto <= 25000 Then
      stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=25"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
        Param_Gen = Precio_Neto * Param_Gen / 100
      Else
        Param_Gen = 0
      End If
      rsta.Close
      Set rsta = Nothing
    Else '>25000
      stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=26"
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
        Param_Gen = Precio_Neto * Param_Gen / 100
      Else
        Param_Gen = 0
      End If
      rsta.Close
      Set rsta = Nothing
    End If
    PVP = Precio_Neto + Param_Gen
    strPVPU = PVP
    strPVLU = 0
  Else
    'PVL
    strPVPU = 0
    strPVLU = curPrecNeto
  End If
End Sub


Private Sub cmdAceptar_Click()
'se introduce la cantidad recibida en FR3500
  Dim strupdate As String
  Dim rstori As rdoResultset
  Dim strori As String
  Dim strinsertentrada As String
  Dim strinsertsalida As String
  Dim str35 As String
  Dim rst35 As rdoResultset
  Dim str80 As String
  Dim rst80 As rdoResultset
  Dim i As Integer
  Dim struni As String
  Dim rstuni As rdoResultset
  Dim cantidadacumulada
  Dim rsta As rdoResultset
  Dim stra As String
  Dim strupdateFR6200 As String
  Dim codestadopedido As Integer
  Dim strProd As String
  Dim rstProd As rdoResultset
  Dim dblExistProd As Double
  Dim strUpdateExist As String
  Dim curPrecio_Base As Currency
  Dim curDescuento As Currency
  Dim curRecargo As Currency
  Dim strRecProd As String
  Dim rstRecProd As rdoResultset
  Dim curPrecio_Neto As Currency
  Dim curIVA As Currency
  Dim strUpdatePrecUlt As String
  Dim strInsBonif As String
  Dim dblBonificado As Double
  Dim strUpdPteBonif As String
  Dim blnOK  As Boolean
  Dim cont As Integer
  Dim blnAux As Boolean
  Dim AuxPedido As Currency
  Dim AuxAcumulado As Currency
  Dim intInd As Integer
  Dim strPedido As String
  Dim rstPedido As rdoResultset
  Dim dlbCodPedComp As Double
  Dim strDetPed As String
  Dim rstDetPed As rdoResultset
  Dim strUpdatePteFact As String
  Dim strDA As String
  Dim qryDA As rdoQuery
  Dim rstDA As rdoResultset
  Dim dblExi As Double
  Dim dblPM As Double
  Dim curPMF As Currency
  Dim strPrd As String
  Dim qryPRD As rdoQuery
  Dim rstPrd As rdoResultset
  Dim dblPU As Double
  Dim curEnv As Currency
  Dim strPVPU As String
  Dim strPVLU As String
  Dim strGtp As String
  Dim curTamPrd As Currency
  Dim strFRJ3 As String
  Dim qryFRJ3 As rdoQuery
  
  

            
  
  
Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
objWinInfo.DataSave

If grdDBGrid1(2).Rows = 0 Then
   Call MsgBox("Debe de tener un pedido de compra", vbInformation, "Aviso")
   Exit Sub
End If
  Screen.MousePointer = vbHourglass
  cmdaceptar.Enabled = False
  Me.Enabled = False
  strFRJ3 = "UPDATE FRJ300 " & _
            "   SET FRJ3IMPORLINEA = (FRJ3IMPORLINEA/FRJ3CANTPEDIDA) * FRJ3CANTENTREG " & _
            "   WHERE FRJ1CODALBARAN = ? "
 Set qryFRJ3 = objApp.rdoConnect.CreateQuery("", strFRJ3)
 If Len(Trim(txtText1(0).Text)) > 0 Then
  qryFRJ3(0) = txtText1(0).Text
  qryFRJ3.Execute
  qryFRJ3.Close
  Set qryFRJ3 = Nothing
End If

  'Se comprueba que todas las casillas OK est�n activadas
  If Len(Trim(txtText1(0).Text)) > 0 Then
    strDA = "SELECT * " & _
            "  FROM FRJ300 " & _
            " WHERE FRJ1CODALBARAN = ? " & _
            "   AND ((FRJ3INDVAL IS NULL) OR (FRJ3INDVAL <> -1))"
    Set qryDA = objApp.rdoConnect.CreateQuery("", strDA)
    qryDA(0) = txtText1(0).Text
    Set rstDA = qryDA.OpenResultset()
    If Not rstDA.EOF Then
      MsgBox "Hay l�neas del albar�n sin validar, Reviselas", vbInformation, "Aceptar Pedido"
      qryDA.Close
      Set qryDA = Nothing
      Set rstDA = Nothing
      Me.Enabled = True
      cmdaceptar.Enabled = True
      Screen.MousePointer = vbDefault
      Exit Sub
    End If
  
    'While Not rstDA.EOF
    '  'Se actualiza el precio de �ltima entrada
    '  strPrd = "UPDATE FR7300 " & _
    '          " SET FR73PREULTENT = ? " & _
   '          " WHERE FR73CODPRODUCTO = ? "
    '  Set qryPRD = objApp.rdoConnect.CreateQuery("", strPrd)
    '  qryPRD(0) = objGen.ReplaceStr(rstDA.rdoColumns("FRJ3PRECIOUNIDAD").Value, ".", ",", 1)
    '  qryPRD(1) = rstDA.rdoColumns("FR73CODPRODUCTO").Value
    '  qryPRD.Execute
   '  rstDA.MoveNext
    'Wend
    qryDA.Close
    Set qryDA = Nothing
    Set rstDA = Nothing
  End If
  
grdDBGrid1(1).MoveFirst
cont = 0
blnAux = True
Do While cont < grdDBGrid1(1).Rows
  AuxPedido = grdDBGrid1(1).Columns("Pedido").CellValue(grdDBGrid1(1).GetBookmark(cont))
  AuxAcumulado = grdDBGrid1(1).Columns("Acumulado").CellValue(grdDBGrid1(1).GetBookmark(cont))
  If AuxPedido <= AuxAcumulado Then
  Else
    blnAux = False
  End If
  cont = cont + 1
Loop

cmdaceptar.Enabled = False

If mblnCancelar = True Then
  'Hay problemas al salvar
  Me.Enabled = True
  cmdaceptar.Enabled = True
  Screen.MousePointer = vbDefault
  Exit Sub
End If
If IsNull(grdDBGrid1(1).Columns("C�d.Albar�n").Value) Or IsNull(grdDBGrid1(1).Columns("L�nea").Value) Or grdDBGrid1(1).Columns("L�nea").Value = "" Or grdDBGrid1(1).Columns("C�d.Albar�n").Value = "" Then
  Me.Enabled = True
  cmdaceptar.Enabled = True
  Screen.MousePointer = vbDefault
  Exit Sub
End If


'strori = "SELECT FR04CODALMACEN FROM FR0400,FRH200 " & _
            "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
            "AND FRH2CODPARAMGEN=3"
strori = "SELECT 0 FROM DUAL"
Set rstori = objApp.rdoConnect.OpenResultset(strori)

stra = "SELECT * FROM FRJ300 WHERE FRJ1CODALBARAN= '" & grdDBGrid1(1).Columns("C�d.Albar�n").Value & "'"

Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
    If Not IsNull(rsta.rdoColumns("FRJ3CANTENTREG").Value) Then
     If (Not rstori.EOF) And Not IsNull(rstori.rdoColumns(0).Value) Then
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str35)
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es Farmacia
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35PRECIOUNIDAD,FR35CANTPRODUCTO,FR93CODUNIMEDIDA) VALUES (" & _
                      rst35.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      "10" & "," & _
                      "TO_DATE(SYSDATE)" & "," & _
                      "NULL" & "," & _
                      rsta.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                      objGen.ReplaceStr(rsta.rdoColumns("FRJ3PRECIOUNIDAD").Value, ",", ".", 1) & ","
        
                
        If Not IsNull(rsta.rdoColumns("FRJ3CANTENTREG").Value) Then
          dblExistProd = 0
          If Not IsNull(rsta.rdoColumns("FRJ3TAMENVASE").Value) Then
             strinsertentrada = strinsertentrada & objGen.ReplaceStr(rsta.rdoColumns("FRJ3CANTENTREG").Value * rsta.rdoColumns("FRJ3TAMENVASE").Value, ",", ".", 1) & ","
             dblExistProd = objGen.ReplaceStr(Fix(rsta.rdoColumns("FRJ3CANTENTREG").Value * rsta.rdoColumns("FRJ3TAMENVASE").Value), ",", ".", 1)
          Else
            strProd = "SELECT FR73TAMENVASE FROM FR7300 WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
            Set rstProd = objApp.rdoConnect.OpenResultset(strProd)
            If Not IsNull(rstProd.rdoColumns("FR73TAMENVASE").Value) Then
              strinsertentrada = strinsertentrada & objGen.ReplaceStr(rsta.rdoColumns("FRJ3CANTENTREG").Value * rstProd.rdoColumns("FR73TAMENVASE").Value, ",", ".", 1) & ","
              dblExistProd = objGen.ReplaceStr(rsta.rdoColumns("FRJ3CANTENTREG").Value * rstProd.rdoColumns("FR73TAMENVASE").Value, ",", ".", 1)
            Else
              strinsertentrada = strinsertentrada & objGen.ReplaceStr(rsta.rdoColumns("FRJ3CANTENTREG").Value, ",", ".", 1) & ","
              dblExistProd = objGen.ReplaceStr(rsta.rdoColumns("FRJ3CANTENTREG").Value, ",", ".", 1)
            End If
            rstProd.Close
            Set rstProd = Nothing
          End If
        Else
            dblExistProd = 0
            strinsertentrada = strinsertentrada & "0" & ","
        End If
        'If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertentrada = strinsertentrada & "'" & "NE" & "'" & ")"
        'Else
        '    strinsertentrada = strinsertentrada & "1)"
        'End If
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
        rst35.Close
        Set rst35 = Nothing
        If Not IsNull(rsta.rdoColumns("FRJ3BONIF").Value) Then
          dblBonificado = Fix(rsta.rdoColumns("FRJ3BONIF").Value)
        Else
          dblBonificado = 0
        End If
            
        strUpdatePteFact = "UPDATE FRJ300 " & _
                           " SET FRJ3CANTPTEFACT = FRJ3CANTENTREG + FRJ3CANTPTEFACT " & _
                           " WHERE  FRJ1CODALBARAN = '" & rsta.rdoColumns("FRJ1CODALBARAN").Value & _
                           "'  AND  FRJ3NUMLINEA = " & rsta.rdoColumns("FRJ3NUMLINEA").Value & _
                           "  AND  FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
        objApp.rdoConnect.Execute strUpdatePteFact, 64
        
        Dim strFR73 As String
        Dim qryFR73 As rdoQuery
        Dim rstFR73 As rdoResultset
              
        strFR73 = "SELECT FR73EXISTENCIAS,FR73PRECMED FROM FR7300 WHERE FR73CODPRODUCTO = ?"
        Set qryFR73 = objApp.rdoConnect.CreateQuery("", strFR73)
        qryFR73(0) = rsta.rdoColumns("FR73CODPRODUCTO").Value
        Set rstFR73 = qryFR73.OpenResultset()
        If Not rstFR73.EOF Then
          If Not IsNull(rstFR73.rdoColumns("FR73EXISTENCIAS").Value) Then
            dblExi = rstFR73.rdoColumns("FR73EXISTENCIAS").Value
          Else
            dblExi = 1
          End If
          If Not IsNull(rstFR73.rdoColumns("FR73PRECMED").Value) Then
            dblPM = rstFR73.rdoColumns("FR73PRECMED").Value
          Else
            dblPM = 1
          End If
        Else
          dblExi = 1
          dblPM = 1
        End If
        qryFR73.Close
        Set qryFR73 = Nothing
        Set rstFR73 = Nothing
        If Not IsNull(rsta.rdoColumns("FRJ3PRECIOUNIDAD").Value) Then
          dblPU = Fix(rsta.rdoColumns("FRJ3PRECIOUNIDAD").Value)
        Else
          dblPU = 1
        End If
        
        Dim strImpAlm As Currency
        If Not IsNull(rsta.rdoColumns("FRJ3IMPORLINEA").Value) Then
          strImpAlm = Fix(rsta.rdoColumns("FRJ3IMPORLINEA").Value)
        Else
          strImpAlm = 1
        End If
        curPMF = Fix(((dblExi * dblPM) + (strImpAlm)) / (dblExi + dblExistProd + dblBonificado))
        'curPMF = Fix(((dblExi * dblPM) + (dblExistProd * dblPU)) / (dblExi + dblExistProd + dblBonificado))
        
        
        strUpdateExist = "UPDATE FR7300 " & _
                         "SET FR73EXISTENCIAS = FR73EXISTENCIAS + " & dblExistProd + dblBonificado & " ," & _
                         "   FR73CANTPEND = FR73CANTPEND - " & dblExistProd & ", " & _
                         "    FR73PRECMED = " & curPMF & "," & _
                         "    FR73IMPALMACEN = FR73IMPALMACEN + " & strImpAlm & _
                         "  , FR73PTEBONIF = FR73PTEBONIF - " & dblBonificado & _
                         " WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
        objApp.rdoConnect.Execute strUpdateExist, 64
        
        'Se introducen las unidades por binificaci�n en FR3500
        If Not IsNull(rsta.rdoColumns("FRJ3BONIF").Value) Then
          str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
          Set rst35 = objApp.rdoConnect.OpenResultset(str35)
        
          strInsBonif = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                        "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                        "FR73CODPRODUCTO,FR35PRECIOUNIDAD,FR35CANTPRODUCTO,FR93CODUNIMEDIDA) VALUES (" & _
                        rst35.rdoColumns(0).Value & "," & rstori.rdoColumns(0).Value & "," & _
                        rstori.rdoColumns(0).Value & "," & "10" & "," & "TO_DATE(SYSDATE)" & "," & "NULL" & "," & _
                        rsta.rdoColumns("FR73CODPRODUCTO").Value & "," & 0 & "," & Fix(rsta.rdoColumns("FRJ3BONIF").Value) & ", 'NE' );"
          objApp.rdoConnect.Execute strInsBonif, 64
          
          
          strUpdPteBonif = "UPDATE FRJ300 " & _
                           "SET FRJ3PTEBONIF = FRJ3PTEBONIF - FRJ3BONIF " & _
                           "WHERE  FRJ1CODALBARAN = '" & rsta.rdoColumns("FRJ1CODALBARAN").Value & _
                           "'  AND  FRJ3NUMLINEA = " & rsta.rdoColumns("FRJ3NUMLINEA").Value & _
                           "  AND  FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
          objApp.rdoConnect.Execute strUpdPteBonif, 64
          rst35.Close
          Set rst35 = Nothing
        End If
    End If
  End If
rsta.MoveNext
Wend
rstori.Close
Set rstori = Nothing
  
  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  'el campo de cantidad entregada se pone a vac�o
  'y el de cantidad acumulada=cantidad acumulada+cantidad entregada

  strPedido = "SELECT * FROM FRJ200 WHERE FRJ1CODALBARAN = '" & txtText1(0).Text & "'"
  Set rstPedido = objApp.rdoConnect.OpenResultset(strPedido)
  While Not rstPedido.EOF
    dlbCodPedComp = rstPedido.rdoColumns("FR62CODPEDCOMPRA").Value
    stra = "SELECT * FROM FR2500 WHERE FR62CODPEDCOMPRA=" & dlbCodPedComp
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    While Not rsta.EOF
      dblExistProd = 1
      If Not IsNull(rsta.rdoColumns("FR25CANTENT").Value) And Not IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
        cantidadacumulada = rsta.rdoColumns("FR25CANTENTREG").Value + rsta.rdoColumns("FR25CANTENT").Value
        dblExistProd = rsta.rdoColumns("FR25CANTENTREG").Value
      End If
      If IsNull(rsta.rdoColumns("FR25CANTENT").Value) And Not IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
        cantidadacumulada = rsta.rdoColumns("FR25CANTENTREG").Value
        dblExistProd = rsta.rdoColumns("FR25CANTENT").Value
      End If
      If Not IsNull(rsta.rdoColumns("FR25CANTENT").Value) And IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
        cantidadacumulada = rsta.rdoColumns("FR25CANTENT").Value
        dblExistProd = rsta.rdoColumns("FR25CANTENT").Value
      End If
      If IsNull(rsta.rdoColumns("FR25CANTENT").Value) And IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
        cantidadacumulada = ""
        dblExistProd = 1
      End If
      If cantidadacumulada <> "" Then
        Dim strX As String
        Dim qryX As rdoQuery
        Dim rstX As rdoResultset
        Dim strB As String
        
        
        strX = "SELECT * " & _
               "  FROM FRJ300 " & _
               " WHERE FRJ1CODALBARAN = ? " & _
               "   AND FR73CODPRODUCTO = ? "
        Set qryX = objApp.rdoConnect.CreateQuery("", strX)
        qryX(0) = txtText1(0).Text
        qryX(1) = rsta.rdoColumns("FR73CODPRODUCTO").Value
        Set rstX = qryX.OpenResultset()
        If Not rstX.EOF Then
          If IsNumeric(rstX.rdoColumns("FRJ3BONIF").Value) Then
            strB = rstX.rdoColumns("FRJ3BONIF").Value
          Else
            strB = 0
          End If
          cantidadacumulada = cantidadacumulada + rstX.rdoColumns("FRJ3CANTENTREG").Value
          strupdate = "UPDATE FR2500 " & _
                      "   SET FR25CANTENT = " & objGen.ReplaceStr(cantidadacumulada, ",", ".", 1) & "," & _
                      "       FR25UDESBONIF = FR25UDESBONIF  - " & strB & _
                      " WHERE FR62CODPEDCOMPRA = " & grdDBGrid1(2).Columns("C�d.Pedido").Value & _
                      "   AND FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
          objApp.rdoConnect.Execute strupdate, 64
          objApp.rdoConnect.Execute "Commit", 64
        End If
      End If

      '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
      'Se actualiza el precio �ltima entrada del producto con el precio neto del producto en el pedido
      'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
      If Not IsNull(rsta.rdoColumns("FR25PRECIOUNIDAD").Value) Then 'Precio_Base
        curPrecio_Base = rsta.rdoColumns("FR25PRECIOUNIDAD").Value
      Else
        curPrecio_Base = 0
      End If
      If Not IsNull(rsta.rdoColumns("FR25DESCUENTO").Value) Then 'Descuento
        curDescuento = curPrecio_Base * rsta.rdoColumns("FR25DESCUENTO").Value / 100
      Else
        curDescuento = 0
      End If
      strRecProd = "SELECT FR73RECARGO,FR73EXISTENCIAS,FR73PRECMED,FR00CODGRPTERAP,FR73TAMENVASE FROM FR7300 WHERE FR73CODPRODUCTO =" & rsta.rdoColumns("FR73CODPRODUCTO").Value
      Set rstRecProd = objApp.rdoConnect.OpenResultset(strRecProd)
      If Not IsNull(rstRecProd.rdoColumns("FR73RECARGO").Value) Then 'Recargo
        curRecargo = curPrecio_Base * rstRecProd.rdoColumns("FR73RECARGO").Value / 100
      Else
        curRecargo = 0
      End If
      
      
      If Not IsNull(rstRecProd.rdoColumns("FR73EXISTENCIAS").Value) Then
        dblExi = rstRecProd.rdoColumns("FR73EXISTENCIAS").Value
      Else
        dblExi = 1
      End If
      If Not IsNull(rstRecProd.rdoColumns("FR73PRECMED").Value) Then
        dblPM = rstRecProd.rdoColumns("FR73PRECMED").Value
      Else
        dblPM = 1
      End If
      
      If Not IsNull(rstRecProd.rdoColumns("FR00CODGRPTERAP").Value) Then
        strGtp = rstRecProd.rdoColumns("FR00CODGRPTERAP").Value
      Else
        strGtp = " "
      End If
      
      If Not IsNull(rstRecProd.rdoColumns("FR00CODGRPTERAP").Value) Then
        If rstRecProd.rdoColumns("FR73TAMENVASE").Value > 0 Then
          curTamPrd = rstRecProd.rdoColumns("FR73TAMENVASE").Value
        Else
          curTamPrd = 1
        End If
      Else
        curTamPrd = 1
      End If
      
      rstRecProd.Close
      Set rstRecProd = Nothing
      If Not IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then 'IVA
        curIVA = (curPrecio_Base - curDescuento + curRecargo) * rsta.rdoColumns("FR88CODTIPIVA").Value / 100
      Else
        curIVA = 0
      End If
      'Hay que poner la funci�n que cambia la coma decimal por el punto decimal
      If Not IsNull(rsta.rdoColumns("FR25TAMENVASE").Value) Then
        If rsta.rdoColumns("FR25TAMENVASE").Value > 0 Then
          curEnv = rsta.rdoColumns("FR25TAMENVASE").Value
        Else
          curEnv = 1
        End If
      Else
        curEnv = 1
      End If
      curPrecio_Neto = Format((curPrecio_Base - curDescuento + curIVA + curRecargo) / curEnv, "0.00")
      Call Calcular_Precio_Unitario(curEnv, strGtp, curPrecio_Neto, curTamPrd, strPVPU, strPVLU)
      'strUpdatePrecUlt = "UPDATE FR7300 " & _
      '                   "SET FR73PREULTENT = " & objGen.ReplaceStr(curPrecio_Neto, ",", ".", 1) & "," & _
      '                   "    FR73PRECIOVENTACON = " & objGen.ReplaceStr(strPVLU, ",", ".", 1) & "," & _
      '                   "    FR73PRECIOVENTA = " & objGen.ReplaceStr(strPVPU, ",", ".", 1) & _
      '                   " WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
      If strPVPU > 0 Then
        strUpdatePrecUlt = "UPDATE FR7300 " & _
                           "SET FR73PREULTENT = " & objGen.ReplaceStr(curPrecio_Neto, ",", ".", 1) & "," & _
                           "    FR73PRECIOVENTA = " & objGen.ReplaceStr(strPVPU, ",", ".", 1) & _
                           " WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
      Else
        strUpdatePrecUlt = "UPDATE FR7300 " & _
                           "SET FR73PREULTENT = " & objGen.ReplaceStr(curPrecio_Neto, ",", ".", 1) & _
                           " WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
      End If
      
      objApp.rdoConnect.Execute strUpdatePrecUlt, 64
      'Se finaliza actualizar el precio de �ltima entrada
      rsta.MoveNext
    Wend
    rsta.Close
    Set rsta = Nothing
    rstPedido.MoveNext
  Wend
  Call MsgBox("El Pedido ha sido Aceptado.", vbInformation, "Pedido Aceptado")

  'Se vacia la lista
  For intInd = 0 To 250
    gaListAlb(intInd, 3) = 0
  Next intInd
  gintContAlb = 0
  
  rstPedido.Close
  Set rstPedido = Nothing
  strPedido = "SELECT * FROM FRJ200 WHERE FRJ1CODALBARAN = '" & txtText1(0).Text & "'"
  Set rstPedido = objApp.rdoConnect.OpenResultset(strPedido)

  'Se actualiza el estado de los pedidos tratados en el albar�n
  'rstPedido tiene los registros de FRJ200
  While Not rstPedido.EOF
    strDetPed = "SELECT SUM(FR25CANTPEDIDA) - SUM(FR25CANTENT),SUM(FR25UDESBONIF)" & _
                "  FROM FR2500 " & _
                " WHERE FR62CODPEDCOMPRA = " & rstPedido.rdoColumns("FR62CODPEDCOMPRA").Value & _
                "   AND FR25CANTPEDIDA > 0 "
    Set rstDetPed = objApp.rdoConnect.OpenResultset(strDetPed)
    If Not rstDetPed.EOF Then
      If (rstDetPed.rdoColumns(0).Value > 0) Or (rstDetPed.rdoColumns(1).Value > 0) Then
        'El pedido est� pacialmente entregado
        strupdate = "UPDATE FR6200 SET FR95CODESTPEDCOMPRA= 4" & _
                    " WHERE FR62CODPEDCOMPRA=" & rstPedido.rdoColumns("FR62CODPEDCOMPRA").Value
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "Commit", 64
      Else
        'El pedido est� totalmente entregado
        strupdate = "UPDATE FR6200 SET FR95CODESTPEDCOMPRA= 5" & _
                    " WHERE FR62CODPEDCOMPRA=" & rstPedido.rdoColumns("FR62CODPEDCOMPRA").Value
        objApp.rdoConnect.Execute strupdate, 64
        objApp.rdoConnect.Execute "Commit", 64
      End If
    End If
    rstPedido.MoveNext
  Wend
  rstPedido.Close
  Set rstPedido = Nothing
'Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
'Call objWinInfo.DataRefresh
Me.Enabled = True
cmdaceptar.Enabled = True
Screen.MousePointer = vbDefault
Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
End Sub

Private Sub cmdcontrol_Click()
  Dim intResp As Integer
  
  cmdcontrol.Enabled = False
  'Call frmMantLotes.Show(vbModal)
  If Not IsNull(grdDBGrid1(1).Columns(6).CellValue(grdDBGrid1(1).Bookmark)) Then
    gdblCodProd = grdDBGrid1(1).Columns(6).CellValue(grdDBGrid1(1).Bookmark)
    If IsNumeric(grdDBGrid1(1).Columns("Envase").CellValue(grdDBGrid1(1).Bookmark)) Then
      gcurEnv = grdDBGrid1(1).Columns("Envase").CellValue(grdDBGrid1(1).Bookmark)
    Else
      gcurEnv = 1
    End If
    If IsNumeric(grdDBGrid1(1).Columns("Recibido").CellValue(grdDBGrid1(1).Bookmark)) Then
      gcurCantEntre = grdDBGrid1(1).Columns("Recibido").CellValue(grdDBGrid1(1).Bookmark)
    Else
      gcurCantEntre = 0
    End If
    gstrNumAlb = grdDBGrid1(1).Columns("C�d.Albar�n").CellValue(grdDBGrid1(1).Bookmark)
    gintNumLin = grdDBGrid1(1).Columns("L�nea").CellValue(grdDBGrid1(1).Bookmark)
    gcurUniRec = gcurCantEntre * gcurEnv
    gstrLlamador = "FrmAnaRecCom"
    Call objsecurity.LaunchProcess("FR0534")
    gdblCodProd = 0
  Else
    intResp = MsgBox("Debe seleccionar un producto", vbInformation, "Control de Lotes")
  End If
  cmdcontrol.Enabled = True

End Sub

Private Sub cmdmaterial_Click()
Dim rsta As rdoResultset
Dim stra As String
Dim strcamposoblig As String
Dim strFac As String
Dim rstFac As rdoResultset

Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)

cmdmaterial.Enabled = False

If txtText1(0).Text <> "" Then
   strFac = "SELECT FRJ1CODALBARAN FROM FRJ100 WHERE FRJ1CODALBARAN=" & "'" & txtText1(0).Text & "'"
   Set rstFac = objApp.rdoConnect.OpenResultset(strFac)
   If Not rstFac.EOF Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
    gintFR501Prov = Me.txtText1(2)
    gstrLlamador = "FrmIntRecCom"
    Call objsecurity.LaunchProcess("FR0503")
    If gintProdBuscado <> "" Then
        stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintProdBuscado
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If Not rsta.EOF Then
            grdDBGrid1(1).Columns(6).Value = gintProdBuscado  'c�d.producto
            grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73CODINTFAR").Value
            grdDBGrid1(1).Columns(8).Value = rsta.rdoColumns("FR73CODINTFARSEG").Value
            grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
                grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
                grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                grdDBGrid1(1).Columns(12).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73VOLUMEN").Value) Then
                grdDBGrid1(1).Columns(13).Value = rsta.rdoColumns("FR73VOLUMEN").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
                grdDBGrid1(1).Columns(14).Value = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
        End If
        rsta.Close
        Set rsta = Nothing
        End If
        
    Else
        Call MsgBox("El Albar�n no est� guardado.", vbInformation, "Aviso")
    End If
    rstFac.Close
    Set rstFac = Nothing
End If

cmdmaterial.Enabled = True
End Sub

Private Sub cmdmedicamento_Click()
Dim rsta As rdoResultset
Dim stra As String
Dim strcamposoblig As String
Dim strFac As String
Dim rstFac As rdoResultset

Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)

cmdmedicamento.Enabled = False

If txtText1(0).Text <> "" Then
   strFac = "SELECT FRJ1CODALBARAN FROM FRJ100 WHERE FRJ1CODALBARAN=" & "'" & txtText1(0).Text & "'"
   Set rstFac = objApp.rdoConnect.OpenResultset(strFac)
   If Not rstFac.EOF Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
    gintFR501Prov = Me.txtText1(2)
    gstrLlamador = "FrmIntRecCom"
    Call objsecurity.LaunchProcess("FR0502")
    If gintProdBuscado <> "" Then
        stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintProdBuscado
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If Not rsta.EOF Then
            grdDBGrid1(1).Columns(6).Value = gintProdBuscado  'c�d.producto
            grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73CODINTFAR").Value
            grdDBGrid1(1).Columns(8).Value = rsta.rdoColumns("FR73CODINTFARSEG").Value
            grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
                grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
                grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                grdDBGrid1(1).Columns(12).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73VOLUMEN").Value) Then
                grdDBGrid1(1).Columns(13).Value = rsta.rdoColumns("FR73VOLUMEN").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
                grdDBGrid1(1).Columns(14).Value = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
        End If
        rsta.Close
        Set rsta = Nothing
        End If
        
    Else
        Call MsgBox("El Albar�n no est� guardado.", vbInformation, "Aviso")
    End If
    rstFac.Close
    Set rstFac = Nothing
End If

cmdmedicamento.Enabled = True
End Sub

Private Sub cmdOK_Click()
  Dim intCont As Integer
  intCont = 0
  grdDBGrid1(1).MoveFirst
  Do While intCont < grdDBGrid1(1).Rows
    grdDBGrid1(1).Columns("OK").Value = Checked
    grdDBGrid1(1).Columns(2).Value = 2
    grdDBGrid1(1).Columns(0).Value = "Modificado"
    grdDBGrid1(1).MoveNext
    intCont = intCont + 1
  Loop
  If intCont > 0 Then
    objWinInfo.objWinActiveForm.blnChanged = True
    objWinInfo.DataSave
  End If
  
End Sub

Private Sub cmdreclamar_Click()

  cmdreclamar.Enabled = False
  'Call FrmRecPed.Show(vbModal)
  gstrLlamador = "FrmAnaRecCom"
  gintLlamadorReclamar = "FrmIntRecCom"
  Call objsecurity.LaunchProcess("FR0523")
  gintLlamadorReclamar = ""
  cmdreclamar.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

 Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim strKey As String
  Dim strFec As String
  Dim rstFec As rdoResultset
  Dim intInd As Integer
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
    
   With objMasterInfo
   .strName = "Albar�n"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strTable = "FRJ100"
    '.blnAskPrimary = False
    
    Call .FormAddOrderField("FRJ1CODALBARAN", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Albaranes")
    Call .FormAddFilterWhere(strKey, "FRJ1CODALBARAN", "C�digo Albar�n", cwString)
    Call .FormAddFilterWhere(strKey, "FRJ1FECHAALBAR", "Fecha Albar�n", cwDate)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ1ESTADO", "Estado", cwNumeric)

  End With
  
  With objMultiInfo
    .strName = "Productos"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "FRJ300"
    '.strWhere = "FRJ3CANTPEDIDA > FRJ3CANTENTACU"
    .intCursorSize = 100
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FRJ1CODALBARAN", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ3CANTPEDIDA", "Cantidad Pedida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ3PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ3IMPORLINEA", "Importe", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH8MONEDA", "Moneda", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRJ3CANTENTREG", "Cantidad Entregada", cwString)
    
  End With
  
  With objMultiInfo1
    .strName = "Pedidos"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "FRJ200"
            
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
    Call .FormAddRelation("FRJ1CODALBARAN", txtText1(0))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Pedidos")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d.Albar�n", "FRJ1CODALBARAN", cwString, 9)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FRJ3NUMLINEA", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "OK", "FRJ3INDVAL", cwBoolean, 1)
    Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Interno", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "Seg", "", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Volumen", "", cwDecimal, 6)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Pedido", "FRJ3CANTPEDIDA", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "Precio Neto", "FRJ3PRECIOUNIDAD", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "Importe L�nea", "FRJ3IMPORLINEA", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "Moneda", "FRH8MONEDA", cwString, 5)
    'Call .GridAddColumn(objMultiInfo, "Envase", "FRJ3TAMENVASE", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "UdesBonif", "FRJ3PTEBONIF", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "Recibido", "FRJ3CANTENTREG", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "Por Bonif", "FRJ3BONIF", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "Acumulado", "FRJ3CANTENTACU", cwDecimal, 10)
    'Call .GridAddColumn(objMultiInfo, "Pte. Recibir", "FRJ3CANTPEDIDA-FRJ3CANTENTACU", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "Envase", "FRJ3TAMENVASE", cwDecimal, 10)
    Call .GridAddColumn(objMultiInfo, "CP", "FR62CODPEDCOMPRA", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "CDP", "FR25CODDETPEDCOMP", cwNumeric, 9)
    
    Call .GridAddColumn(objMultiInfo1, "Albar�n", "FRJ1CODALBARAN", cwString, 9)
    Call .GridAddColumn(objMultiInfo1, "C�d.Pedido", "FR62CODPEDCOMPRA", cwNumeric, 9)
    
   
    
    Call .FormCreateInfo(objMasterInfo)
    

    Call .FormChangeColor(objMultiInfo)
    Call .FormChangeColor(objMultiInfo1)
      
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(7), "FR73CODINTFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(8), "FR73CODINTFARSEG")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(9), "FR73DESPRODUCTOPROV")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(10), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(11), "FR73DOSIS")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(12), "FR93CODUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(13), "FR73VOLUMEN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(6)), grdDBGrid1(1).Columns(14), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "FR79PROVEEDOR")
    
    'albaranes
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    'productos
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(19)).blnInFind = True
    
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(6)).blnForeign = True  'c�d.producto
    .CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnForeign = True  'moneda
    .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnReadOnly = True 'Cant. pedida
    .CtrlGetInfo(grdDBGrid1(1).Columns(19)).blnReadOnly = True 'Cant. pte de bonificar
    .CtrlGetInfo(grdDBGrid1(1).Columns(22)).blnReadOnly = True 'cant acumulada recibida
    .CtrlGetInfo(grdDBGrid1(1).Columns(23)).blnReadOnly = True 'Tama�o del envase
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  grdDBGrid1(1).Columns(6).Visible = False
  grdDBGrid1(1).Columns(3).Visible = False
  grdDBGrid1(1).Columns(4).Visible = False
  grdDBGrid1(1).Columns(13).Visible = False
  grdDBGrid1(1).Columns(16).Visible = False
  grdDBGrid1(1).Columns(17).Visible = False
  grdDBGrid1(1).Columns(18).Visible = False
  
  
  grdDBGrid1(1).Columns(5).Width = 400      'OK
  'grdDBGrid1(1).Columns(6).Width = 900     'c�d.prod
  grdDBGrid1(1).Columns(7).Width = 700     'c�d.interno
  grdDBGrid1(1).Columns(8).Width = 450      'c�d.seg
  grdDBGrid1(1).Columns(9).Width = 2500     'descripci�n
  grdDBGrid1(1).Columns(19).Width = 450      'F.F
  grdDBGrid1(1).Columns(11).Width = 600    'Dosis
  grdDBGrid1(1).Columns(12).Width = 700     'U.M
  grdDBGrid1(1).Columns(14).Width = 1000    'Referencia
  grdDBGrid1(1).Columns(15).Width = 900   'Pedido
  grdDBGrid1(1).Columns(19).Width = 900  'Udes a bonificar
  grdDBGrid1(1).Columns(20).Width = 900  'Recibido
  grdDBGrid1(1).Columns(21).Width = 900  'Por Bonif.
  grdDBGrid1(1).Columns(22).Width = 950   'Acumulado
  grdDBGrid1(1).Columns("CDP").Width = 0
  grdDBGrid1(1).Columns("CP").Width = 0
  
  grdDBGrid1(2).Columns(3).Visible = False
  
  'NUEVO
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
  
    strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(txtText1(1), 0)
    rstFec.Close
    Set rstFec = Nothing
    For intInd = 0 To gintContAlb
      If gaListAlb(intInd, 3) = -1 Then
        Call objWinInfo.CtrlSet(txtText1(2), gaListAlb(intInd, 0))
        Exit Sub
      End If
    Next intInd
    
    
    
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim rsta As rdoResultset
Dim stra As String

'se refrescan los option que indican el estado del pedido
If Index = 2 Then
    optparcial.Value = False
    optcompleto.Value = False
    If grdDBGrid1(2).Rows > 0 Then
        If IsNumeric(grdDBGrid1(2).Columns("C�d.Pedido").Value) Then
          stra = "SELECT * FROM FR6200 WHERE FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns("C�d.Pedido").Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If Not IsNull(rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value) Then
                If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 4 Then
                    optparcial.Value = True
                End If
                If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 5 Then
                    optcompleto.Value = True
                End If
            End If
            rsta.Close
            Set rsta = Nothing
          End If
        End If
    End If
End If
 End Sub



Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

  If strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     
     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
   Set objSearch = Nothing
  End If

  If strCtrl = "grdDBGrid1(1).C�d.Prod" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�d.Interno"
     
     Set objField = .AddField("FR73DESPRODUCTOPROV")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(6), .cllValues("FR73CODPRODUCTO"))
      Call objWinInfo.CtrlDataChange
     End If
   End With
   Set objSearch = Nothing
  End If
  
  If strCtrl = "grdDBGrid1(1).Moneda" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH800"
     
     Set objField = .AddField("FRH8MONEDA")
     objField.strSmallDesc = "Moneda"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(18), .cllValues("FRH8MONEDA"))
      Call objWinInfo.CtrlDataChange
     End If
   End With
   Set objSearch = Nothing
  End If
  
  
End Sub

Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim strdelete As String
Dim strUpd As String
Dim strUB As String
Dim strCE As String
  
  If strFormName = "Productos" Then
    If IsNumeric(grdDBGrid1(1).Columns("UdesBonif").Value) Then
      strUB = grdDBGrid1(1).Columns("UdesBonif").Value
    Else
      strUB = 0
    End If
    If IsNumeric(grdDBGrid1(1).Columns("Recibido").Value) Then
      strCE = objGen.ReplaceStr(grdDBGrid1(1).Columns("Recibido").Value, ",", ".", 1)
    Else
      strCE = 0
    End If
    
    strUpd = "UPDATE FR2500 " & _
             "   SET FR25UDESBONIF = FR25UDESBONIF + " & objGen.ReplaceStr(strUB, ",", ".", 1) & "," & _
             "       FR25CANTENT = FR25CANTENT - " & strCE & _
             " WHERE FR73CODPRODUCTO = " & grdDBGrid1(1).Columns(6).Value & _
             "   AND FR62CODPEDCOMPRA = " & grdDBGrid1(1).Columns("CP").Value & _
             "   AND FR25CODDETPEDCOMP = " & grdDBGrid1(1).Columns("CDP").Value & _
             "   AND FR25CANTENT > 0 "
            'objApp.rdoConnect.Execute strUpd, 64
            
  End If
             

  'If strFormName = "Pedidos" Then
  '  If grdDBGrid1(2).Columns(0).Value = "Eliminado" Then
  '
  '      stra = "SELECT * FROM FR2500 WHERE FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns(4).Value
  '      Set rsta = objApp.rdoConnect.OpenResultset(stra)
  '
  '      While Not rsta.EOF
  '          strdelete = "DELETE FRJ300 WHERE FRJ1CODALBARAN=" & "'" & txtText1(0).Text & "'" & _
  '                      " AND " & _
  '                      "FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO").Value
  '          objApp.rdoConnect.Execute strdelete, 64
  '          objApp.rdoConnect.Execute "Commit", 64
  '       rsta.MoveNext
  '       Wend
  '  End If
  'End If
End Sub

Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim rsta As rdoResultset
Dim stra As String
Dim strinsert As String
Dim linea As Long
Dim strlinea As String
Dim rstlinea As rdoResultset
Dim cantpedida As String
Dim preciounidad As String
Dim imporlinea As String
Dim strupdate As String
Dim cantacumulada
Dim dblTamEnvase As Double
Dim strProd As String
Dim rstProd As rdoResultset
Dim strMoneda As String
Dim strMonedaGen As String
Dim rstMonedaGen As rdoResultset
Dim dblBonif As Double
'Dim dblTamEnvase As Double
Dim dblTPBonif As Double
Dim dblIVA As Double
Dim strIVA As String
Dim rstIVA As rdoResultset
Dim dblCantEnt As Double 'Cantidad a entregar
Dim strProPRG As String
Dim rstProPRG As rdoResultset
Dim intInd As Integer
Dim curCP As Currency
Dim curBon As Currency
Dim curUB As Currency
  Dim intBoniEntreg As Currency
If strFormName = "Albar�n" Then '0
    
  For intInd = 0 To gintContAlb '1
   If gaListAlb(intInd, 3) = -1 Then '2
    'al introducir un albar�n mete autom�ticamente el pedido tra�do de la pantalla
    'FR0522(consultar situaci�n de pedido) comprobando primero que no est� ya guardado.
    stra = "SELECT * FROM FRJ200 WHERE " & _
           "FRJ1CODALBARAN=" & "'" & txtText1(0).Text & "'" & _
           " AND " & _
           "FR62CODPEDCOMPRA=" & gaListAlb(intInd, 1)
    Set rsta = objApp.rdoConnect.OpenResultset(stra)
    If rsta.EOF Then '3
        strinsert = "INSERT INTO FRJ200 (FRJ1CODALBARAN,FR62CODPEDCOMPRA) " & _
                  " VALUES (" & "'" & txtText1(0).Text & "'" & "," & gaListAlb(intInd, 1) & ")"
        objApp.rdoConnect.Execute strinsert, 64
    End If '3
   End If '2
  Next intInd '1
          
    For intInd = 0 To gintContAlb '4
      If gaListAlb(intInd, 3) = -1 Then '5
        strlinea = "SELECT MAX(FRJ3NUMLINEA) FROM FRJ300 " & _
               "WHERE FRJ1CODALBARAN=" & "'" & txtText1(0).Text & "'"
        Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
        If IsNull(rstlinea.rdoColumns(0).Value) Then '6
          linea = 1
        Else
          linea = rstlinea.rdoColumns(0).Value + 1
        End If '6
        rstlinea.Close
        Set rstlinea = Nothing
      
        stra = "SELECT * FROM FR2500 WHERE FR62CODPEDCOMPRA=" & gaListAlb(intInd, 1) & _
               " AND FR25CODDETPEDCOMP = " & gaListAlb(intInd, 2) & _
               " AND ((FR25CANTPEDIDA > FR25CANTENT) OR (FR25CANTENT IS NULL) OR (FR25BONIF > 0))"
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        While Not rsta.EOF '7
            'transforma la coma de separaci�n de los decimales por un punto FRJ3CANTENTREG
            'cantentre = rsta.rdoColumns("FR25CANTPEDIDA").Value
            cantpedida = rsta.rdoColumns("FR25CANTPEDIDA").Value
            cantpedida = objGen.ReplaceStr(cantpedida, ",", ".", 1)
            preciounidad = rsta.rdoColumns("FR25PRECIOUNIDAD").Value
            preciounidad = objGen.ReplaceStr(preciounidad, ",", ".", 1)
            imporlinea = rsta.rdoColumns("FR25IMPORLINEA").Value
            imporlinea = objGen.ReplaceStr(imporlinea, ",", ".", 1)
            If IsNull(rsta.rdoColumns("FR25BONIF").Value) Then '8
              dblBonif = 0
              dblTPBonif = 0
            Else
              dblTPBonif = rsta.rdoColumns("FR25BONIF").Value
              
              If IsNull(rsta.rdoColumns("FR25CANTPEDIDA").Value) Then '9
                cantpedida = 0
              End If '9
              If IsNull(rsta.rdoColumns("FR25TAMENVASE").Value) Then '10
                dblTamEnvase = 1
              Else
                dblTamEnvase = rsta.rdoColumns("FR25TAMENVASE").Value
              End If '10
              dblBonif = Fix(((cantpedida * dblTPBonif / 100) * dblTamEnvase))
            End If '8
            If Not IsNull(rsta.rdoColumns("FR25UDESBONIF").Value) Then
              dblBonif = rsta.rdoColumns("FR25UDESBONIF").Value
            Else
              dblBonif = 0
            End If
            If IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then '11
              dblIVA = 0
            Else
              strIVA = "SELECT FR88IVA FROM FR8800 WHERE FR88CODTIPIVA = " & rsta.rdoColumns("FR88CODTIPIVA").Value
              Set rstIVA = objApp.rdoConnect.OpenResultset(strIVA)
              If rstIVA.EOF Then '12
                dblIVA = 0
              Else
                dblIVA = rstIVA.rdoColumns(0).Value
              End If '12
              rstIVA.Close
              Set rstIVA = Nothing
            End If '11
            If IsNull(rsta.rdoColumns("FRH8MONEDA").Value) Then '13
              strMonedaGen = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = 19"
              Set rstMonedaGen = objApp.rdoConnect.OpenResultset(strMonedaGen)
              strMoneda = rstMonedaGen.rdoColumns(0).Value
            Else
              strMoneda = rsta.rdoColumns("FRH8MONEDA").Value
            End If '13
            
            
            strinsert = "INSERT INTO FRJ300(FRJ3TPBONIF,FR62CODPEDCOMPRA,FR25CODDETPEDCOMP,FRJ3INDVAL,FRJ1CODALBARAN,FRJ3NUMLINEA,FR73CODPRODUCTO," & _
                    "FRJ3CANTPEDIDA,FRJ3PRECIOUNIDAD,FRJ3IMPORLINEA,FRH8MONEDA,FRJ3CANTENTACU,FRJ3TAMENVASE,FRJ3PTEBONIF,FRJ3DTO,FRJ3IVA,FRJ3CANTENTREG,FRJ3BONIF,FRJ3PRECNET)" & _
                    " VALUES (" & objGen.ReplaceStr(dblTPBonif, ",", ".", 1) & "," & rsta.rdoColumns("FR62CODPEDCOMPRA").Value & "," & rsta.rdoColumns("FR25CODDETPEDCOMP").Value & ",0," & _
                    "'" & txtText1(0).Text & "'" & "," & _
                    linea & "," & _
                    rsta.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                    cantpedida & "," & _
                    preciounidad & "," & _
                    imporlinea & "," & _
                    "'" '& rsta.rdoColumns("FRH8MONEDA").Value & "'" & ","
            
            If Not IsNull(rsta.rdoColumns("FRH8MONEDA").Value) Then '14
              strinsert = strinsert & rsta.rdoColumns("FRH8MONEDA").Value & "'" & ","
            Else
              strinsert = strinsert & strMoneda & "'" & ","
            End If '14
            If Not IsNull(rsta.rdoColumns("FR25CANTENT").Value) Then '15
                strinsert = strinsert & objGen.ReplaceStr(rsta.rdoColumns("FR25CANTENT").Value, ",", ".", 1)
                dblCantEnt = cantpedida - objGen.ReplaceStr(rsta.rdoColumns("FR25CANTENT").Value, ",", ".", 1)
            Else
                strinsert = strinsert & "0"
                dblCantEnt = cantpedida
            End If '15
            If IsNull(rsta.rdoColumns("FR25TAMENVASE").Value) Then '16
               'Se busca el tama�o en la ficha del producto
               strProd = "SELECT NVL(FR73TAMENVASE,1) FROM FR7300 WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
               Set rstProd = objApp.rdoConnect.OpenResultset(strProd)
               dblTamEnvase = rstProd.rdoColumns(0).Value
               rstProd.Close
               Set rstProd = Nothing
            Else
              dblTamEnvase = rsta.rdoColumns("FR25TAMENVASE").Value
            End If '16
            'Se mira si el producto es de entregas programadas, ya que en ese caso la cantidad
            'a entregar est� determinada por el campo FR7300.FR73TAMPEDPRG
            strProPRG = "SELECT FR73TAMPEDPRG " & _
                        "  FROM FR7300 " & _
                        " WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value & _
                        "   AND FR73INDPROG = -1"
            Set rstProPRG = objApp.rdoConnect.OpenResultset(strProPRG)
            If rstProPRG.EOF Then '17
              'El producto no es de gesti�n programada
              strinsert = strinsert & "," & dblTamEnvase & "," & dblBonif & "," & objGen.ReplaceStr(rsta.rdoColumns("FR25DESCUENTO").Value, ",", ".", 1) & "," & objGen.ReplaceStr(dblIVA, ",", ".", 1) & "," & objGen.ReplaceStr(dblCantEnt, ",", ".", 1) & "," & dblBonif & "," & objGen.ReplaceStr(rsta.rdoColumns("FR25PRECNET").Value, ",", ".", 1) & ")"
            Else
              'El producto se gestiona de forma programada por lo que la cantidad a recibir est� en FR7300
              strinsert = strinsert & "," & dblTamEnvase & "," & dblBonif & "," & objGen.ReplaceStr(rsta.rdoColumns("FR25DESCUENTO").Value, ",", ".", 1) & "," & objGen.ReplaceStr(dblIVA, ",", ".", 1) & "," & objGen.ReplaceStr(rstProPRG.rdoColumns(0).Value, ",", ".", 1) & "," & dblBonif & "," & objGen.ReplaceStr(rsta.rdoColumns("FR25PRECNET").Value, ",", ".", 1) & ")"
            End If '17
            rstProPRG.Close
            Set rstProPRG = Nothing
            
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
            linea = linea + 1
         rsta.MoveNext
         Wend '7
         rsta.Close
         Set rsta = Nothing
       End If '5
     Next intInd '4
  End If '0
  
If strFormName = "Pedidos" Then
    strlinea = "SELECT MAX(FRJ3NUMLINEA) FROM FRJ300 " & _
                 "WHERE FRJ1CODALBARAN=" & "'" & txtText1(0).Text & "'"
    Set rstlinea = objApp.rdoConnect.OpenResultset(strlinea)
    If IsNull(rstlinea.rdoColumns(0).Value) Then
        linea = 1
    Else
        linea = rstlinea.rdoColumns(0).Value + 1
    End If
    rstlinea.Close
    Set rstlinea = Nothing
    If grdDBGrid1(2).Columns(0).Value = "A�adido" Or grdDBGrid1(2).Columns(0).Value = "Le�do" Then
        stra = "SELECT * FROM FR2500 WHERE FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns(4).Value & _
               " AND ((FR25CANTPEDIDA > FR25CANTENT) OR (FR25CANTENT IS NULL)  OR (FR25BONIF > 0))"
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        While Not rsta.EOF
            'transforma la coma de separaci�n de los decimales por un punto
            cantpedida = rsta.rdoColumns("FR25CANTPEDIDA").Value
            cantpedida = objGen.ReplaceStr(cantpedida, ",", ".", 1)
            preciounidad = rsta.rdoColumns("FR25PRECIOUNIDAD").Value
            preciounidad = objGen.ReplaceStr(preciounidad, ",", ".", 1)
            imporlinea = rsta.rdoColumns("FR25IMPORLINEA").Value
            imporlinea = objGen.ReplaceStr(imporlinea, ",", ".", 1)
            If IsNull(rsta.rdoColumns("FR25BONIF").Value) Then
              dblBonif = 0
              dblTPBonif = 0
            Else
              dblTPBonif = rsta.rdoColumns("FR25BONIF").Value
              
              If IsNull(rsta.rdoColumns("FR25CANTPEDIDA").Value) Then
                cantpedida = 0
              End If
              If IsNull(rsta.rdoColumns("FR25TAMENVASE").Value) Then
                dblTamEnvase = 1
              Else
                dblTamEnvase = rsta.rdoColumns("FR25TAMENVASE").Value
              End If
              dblBonif = Fix(((cantpedida * dblTPBonif / 100) * dblTamEnvase))
            End If
            If IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then
              dblIVA = 0
            Else
              strIVA = "SELECT FR88IVA FROM FR8800 WHERE FR88CODTIPIVA = " & rsta.rdoColumns("FR88CODTIPIVA").Value
              Set rstIVA = objApp.rdoConnect.OpenResultset(strIVA)
              If rstIVA.EOF Then
                dblIVA = 0
              Else
                dblIVA = rstIVA.rdoColumns(0).Value
              End If
              rstIVA.Close
              Set rstIVA = Nothing
            End If
            
            'Se mira si el producto es de entregas programadas, ya que en ese caso la cantidad
            'a entregar est� determinada por el campo FR7300.FR73TAMPEDPRG
            strProPRG = "SELECT FR73TAMPEDPRG " & _
                        "  FROM FR7300 " & _
                        " WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value & _
                        "   AND FR73INDPROG = -1"
            Set rstProPRG = objApp.rdoConnect.OpenResultset(strProPRG)
            If rstProPRG.EOF Then
              'El producto no es de gesti�n programada, siendo la cantidad a entregar la
              'diferencia entre lo pedido y lo acumulado
            If Not IsNull(rsta.rdoColumns("FR25CANTENT").Value) Then
                dblCantEnt = cantpedida - objGen.ReplaceStr(rsta.rdoColumns("FR25CANTENT").Value, ",", ".", 1)
            Else
                dblCantEnt = cantpedida
            End If
            Else
              'El producto se gestiona de forma programada por lo que la cantidad a recibir est� en FR7300
              dblCantEnt = rstProPRG.rdoColumns(0).Value
            End If
            rstProPRG.Close
            Set rstProPRG = Nothing
            
            strinsert = "INSERT INTO FRJ300(FRJ3TPBONIF,FR62CODPEDCOMPRA,FR25CODDETPEDCOMP,FRJ1CODALBARAN,FRJ3NUMLINEA,FR73CODPRODUCTO," & _
                    "FRJ3CANTPEDIDA,FRJ3PRECIOUNIDAD,FRJ3IMPORLINEA,FRH8MONEDA,FRJ3CANTENTACU,FRJ3TAMENVASE,FRJ3PTEBONIF,FRJ3DTO,FRJ3IVA,FRJ3CANTENTREG,FRJ3BONIF)" & _
                    " VALUES (" & objGen.ReplaceStr(dblTPBonif, ",", ".", 1) & "," & rsta.rdoColumns("FR62CODPEDCOMPRA").Value & "," & rsta.rdoColumns("FR25CODDETPEDCOMP").Value & _
                    ",'" & txtText1(0).Text & "'" & "," & _
                    linea & "," & _
                    rsta.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                    cantpedida & "," & _
                    preciounidad & "," & _
                    imporlinea & "," & _
                    "'" & rsta.rdoColumns("FRH8MONEDA").Value & "'" & ","
            If Not IsNull(rsta.rdoColumns("FR25CANTENT").Value) Then
                strinsert = strinsert & objGen.ReplaceStr(rsta.rdoColumns("FR25CANTENT").Value, ",", ".", 1)
            Else
                strinsert = strinsert & "0"
            End If
            If IsNull(rsta.rdoColumns("FR25TAMENVASE").Value) Then
               'Se busca el tama�o en la ficha del producto
               strProd = "SELECT NVL(FR73TAMENVASE,1) FROM FR7300 WHERE FR73CODPRODUCTO = " & rsta.rdoColumns("FR73CODPRODUCTO").Value
               Set rstProd = objApp.rdoConnect.OpenResultset(strProd)
               dblTamEnvase = rstProd.rdoColumns(0).Value
               rstProd.Close
               Set rstProd = Nothing
            Else
              dblTamEnvase = rsta.rdoColumns("FR25TAMENVASE").Value
            End If
            If IsNumeric(rsta.rdoColumns("FR25UDESBONIF").Value) Then
              dblBonif = rsta.rdoColumns("FR25UDESBONIF").Value
            Else
              dblBonif = 0
            End If
            strinsert = strinsert & "," & dblTamEnvase & "," & dblBonif & "," & objGen.ReplaceStr(dblTPBonif, ",", ".", 1) & "," & objGen.ReplaceStr(dblIVA, ",", ".", 1) & "," & objGen.ReplaceStr(dblCantEnt, ",", ".", 1) & "," & dblBonif & ")"
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
            linea = linea + 1
         rsta.MoveNext
         Wend
         rsta.Close
         Set rsta = Nothing
    End If
End If
'************************************************************************
'se modifica tambi�n FRJ300
If strFormName = "Productos" Then
    'primero se pasa a decimal transformando el punto en coma
    If (IsNumeric(grdDBGrid1(1).Columns(20).Value) And IsNumeric(grdDBGrid1(1).Columns(22).Value)) Then
        cantacumulada = 1 * grdDBGrid1(1).Columns(20).Value + 1 * grdDBGrid1(1).Columns(22).Value
    Else
        If IsNumeric(grdDBGrid1(1).Columns(20).Value) Then
            cantacumulada = grdDBGrid1(1).Columns(20).Value
        Else
            cantacumulada = 0
        End If
    End If
    If IsNumeric(grdDBGrid1(1).Columns(21).Value) Then
      intBoniEntreg = Fix(grdDBGrid1(1).Columns(21).Value)
    Else
      intBoniEntreg = 0
    End If
    If IsNumeric(cantacumulada) Then
        'despu�s se transforma la coma en punto para hacer el update
        cantacumulada = objGen.ReplaceStr(cantacumulada, ",", ".", 1)
        strupdate = "UPDATE FRJ300 SET FRJ3CANTENTACU=" & cantacumulada & _
                    " WHERE " & _
                    "FRJ1CODALBARAN=" & "'" & grdDBGrid1(1).Columns(3).Value & "'" & _
                    " AND " & _
                    "FRJ3NUMLINEA=" & grdDBGrid1(1).Columns(4).Value & _
                    " AND " & _
                    "FR73CODPRODUCTO=" & grdDBGrid1(1).Columns(6).Value
        objApp.rdoConnect.Execute strupdate, 64
        'se modifica la cantidad acumulada del producto en FR2500
        stra = "SELECT * FROM FR2500 WHERE FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns(4).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        While Not rsta.EOF
            strupdate = "UPDATE FR2500 SET FR25UDESBONIF= " & grdDBGrid1(1).Columns("UdesBonif").Value & _
                        " WHERE FR73CODPRODUCTO=" & grdDBGrid1(1).Columns(6).Value & _
                        " AND FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns(4).Value
            'objApp.rdoConnect.Execute strupdate, 64
            
            strupdate = "UPDATE FR2500 SET FR25CANTENT=" & cantacumulada & _
                        "                 ,FR25UDESBONIF= FR25UDESBONIF - " & intBoniEntreg & _
                        " WHERE FR73CODPRODUCTO=" & grdDBGrid1(1).Columns(6).Value & _
                        " AND FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns(4).Value
            'objApp.rdoConnect.Execute strupdate, 64
        rsta.MoveNext
        Wend
        rsta.Close
        Set rsta = Nothing
        'hay que cambiar el acumulado del producto de FRJ300 es decir, en los albaranes de
        'ese pedido.Primero se buscan los albaranes del pedido.
        stra = "SELECT FRJ1CODALBARAN FROM FRJ200 WHERE FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns(4).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        While Not rsta.EOF
            strupdate = "UPDATE FRJ300 SET FRJ3CANTENTACU=" & cantacumulada & _
                    " WHERE " & _
                    "FRJ1CODALBARAN=" & "'" & rsta.rdoColumns("FRJ1CODALBARAN").Value & "'" & _
                    " AND " & _
                    "FR73CODPRODUCTO=" & grdDBGrid1(1).Columns(6).Value & " AND FRJ1CODALBARAN<>" & "'" & grdDBGrid1(1).Columns(3).Value & "'"
            objApp.rdoConnect.Execute strupdate, 64
        rsta.MoveNext
        Wend
        rsta.Close
        Set rsta = Nothing
        End If

End If
End Sub





Private Sub objWinInfo_cwPreChanged(ByVal strFormName As String)
   Dim a
   'a = 1
   a = grdDBGrid1(1).Columns(20).Value
End Sub

Private Sub objWinInfo_cwPreChangeStatus(ByVal strFormName As String, ByVal intOldStatus As CodeWizard.cwFormStatus, ByVal intNewStatus As CodeWizard.cwFormStatus)
 'If strFormName = "Productos" Then
 '   mcurRec = grdDBGrid1(1).Columns(20).Value
 'Else
 '   mcurRec = 0
 'End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  'Se comprueba que la cantidad entregada es menor o igual que la pedida.
  'Tamb�en se comprueba que la cantidad or binificaci�n es menor o igual que la que corresponde al pedido
  Dim intResp As Integer
  
  If strFormName = "Productos" Then
    'If objWinInfo.objWinActiveForm.rdoCursor!FRJ3CANTPEDIDA < _
    'objWinInfo.objWinActiveForm.rdoCursor!FRJ3CANTENTREG + _
    'objWinInfo.objWinActiveForm.rdoCursor!FRJ3CANTENTACU Then
    '  intResp = MsgBox("No se pueden recibir m�s envases de los pedidos", vbInformation, "Informaci�n")
    '  blnCancel = True
    '  mblnCancelar = True
    '  Exit Sub
    'End If
    'objWinInfo.objWinActiveForm.rdoCursor!FRJ3INDVAL = -1
    'If grdDBGrid1(1).Columns(5).Value = -1 Then
    '  blnCancel = True
    '  Exit Sub
    'End If
    If Fix(grdDBGrid1(1).Columns(15).Value) < Fix(grdDBGrid1(1).Columns(20).Value) + Fix(grdDBGrid1(1).Columns(22).Value) Then
      intResp = MsgBox("No se pueden recibir m�s envases de los pedidos", vbInformation, "Informaci�n")
      blnCancel = True
      mblnCancelar = True
      cmdaceptar = True
      Exit Sub
    End If
    If objWinInfo.objWinActiveForm.rdoCursor!FRJ3PTEBONIF < objWinInfo.objWinActiveForm.rdoCursor!FRJ3BONIF Then
      intResp = MsgBox("No se pueden recibir m�s udes por bonificaci�n que las que nos corresponden", vbInformation, "Informaci�n")
      blnCancel = True
      mblnCancelar = True
      Exit Sub
    End If
    'If IsNumeric(grdDBGrid1(1).Columns(20).Value) Then
    '  If grdDBGrid1(1).Columns(20).Value > 0 Then
    '    objWinInfo.objWinActiveForm.rdoCursor!FRJ3IMPORLINEA = objGen.ReplaceStr((objWinInfo.objWinActiveForm.rdoCursor!FRJ3IMPORLINEA / objWinInfo.objWinActiveForm.rdoCursor!FRJ3CANTPEDIDA) * grdDBGrid1(1).Columns(20).Value, ",", ".", 1)
    '  End If
    'End If
  End If
  mblnCancelar = False
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim max As Long
Dim i As Long
Dim strFec As String
Dim rstFec As rdoResultset


If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Albar�n" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(txtText1(1), 0)
    rstFec.Close
    Set rstFec = Nothing
    txtText1(0).SetFocus

Else
    If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Productos" Then
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
        Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(3), txtText1(0).Text)
        'se introduce el n�mero de l�nea
        sqlstr = "SELECT MAX(FRJ3NUMLINEA) FROM FRJ300 " & _
                 "WHERE FRJ1CODALBARAN=" & "'" & txtText1(0).Text & "'"
        Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
        If IsNull(rsta.rdoColumns(0).Value) Then
            max = 0
        Else
            max = rsta.rdoColumns(0).Value
        End If
        'se introduce el n�mero de l�nea
        If grdDBGrid1(1).Rows = 0 Then
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), 1)
        Else
            grdDBGrid1(1).MoveFirst
            'If grdDBGrid1(1).Columns(4).Value = "" Then
            '    max = 0
            'Else
            '    max = grdDBGrid1(1).Columns(4).Value
            'End If
            For i = 0 To grdDBGrid1(1).Rows - 1
             If grdDBGrid1(1).Columns(4).Value <> "" Then
                If max < grdDBGrid1(1).Columns(4).Value Then
                    max = grdDBGrid1(1).Columns(4).Value
                End If
             End If
            grdDBGrid1(1).MoveNext
            Next i
            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), max + 1)
        End If
        SendKeys ("{TAB}")
        rsta.Close
        Set rsta = Nothing
     Else
        If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Pedidos" Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            Call objWinInfo.CtrlSet(grdDBGrid1(2).Columns(3), txtText1(0).Text)
        Else
          'If btnButton.Index = 8 And objWinInfo.objWinActiveForm.strName = "Productos" Then
          '  'Se borra un producto del detalle
          '  Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns("Por Bonif"), 0)
          '  Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns("Recibido"), 0)
          '  'objWinInfo.DataSave
          '  Call objWinInfo.WinProcess(cwProcessToolBar, 4, 0)
          '  'Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          'Else
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
          'End If
        End If
    End If
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select

End Sub
Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
Dim rsta As rdoResultset
Dim stra As String

    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    If intIndex = 1 Then
      If grdDBGrid1(1).Rows > 0 Then
        If grdDBGrid1(1).Columns("C�d.Prod").Value <> "" Then
          stra = "SELECT FR73INDCADESP FROM FR7300 WHERE FR73CODPRODUCTO=" & grdDBGrid1(1).Columns("C�d.Prod").Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            If rsta("FR73INDCADESP").Value = "-1" Then
              cmdcontrol.Enabled = True
            Else
              cmdcontrol.Enabled = False
            End If
          End If
          rsta.Close
          Set rsta = Nothing
        End If
      End If
    End If
    
    If intIndex = 2 Then
     optparcial.Value = False
     optcompleto.Value = False
     If grdDBGrid1(2).Rows > 0 Then
      If IsNumeric(grdDBGrid1(2).Columns("C�d.Pedido").Value) Then
        stra = "SELECT * FROM FR6200 WHERE FR62CODPEDCOMPRA=" & grdDBGrid1(2).Columns("C�d.Pedido").Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If Not rsta.EOF Then
          If Not IsNull(rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value) Then
            If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 4 Then
                optparcial.Value = True
            End If
            If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 5 Then
                optcompleto.Value = True
            End If
          End If
        End If
        rsta.Close
        Set rsta = Nothing
    End If
    End If
   End If

End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


