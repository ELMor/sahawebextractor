VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmRedGesCom 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Redactar Gestiones de Compra"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0511.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   30
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Buscadores"
      Height          =   2655
      Index           =   1
      Left            =   10080
      TabIndex        =   39
      Top             =   840
      Width           =   1575
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   30104
         Index           =   17
         Left            =   7440
         Locked          =   -1  'True
         TabIndex        =   43
         TabStop         =   0   'False
         Tag             =   "Situaci�n geogr�fica del Departamento"
         Top             =   840
         Width           =   3225
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   30104
         Index           =   16
         Left            =   8040
         Locked          =   -1  'True
         TabIndex        =   42
         TabStop         =   0   'False
         Tag             =   "Situaci�n geogr�fica del Departamento"
         Top             =   240
         Width           =   540
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   30104
         Index           =   15
         Left            =   7440
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Tag             =   "Situaci�n geogr�fica del Departamento"
         Top             =   240
         Width           =   540
      End
      Begin VB.TextBox txtText1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         HelpContextID   =   30104
         Index           =   12
         Left            =   8640
         Locked          =   -1  'True
         TabIndex        =   40
         TabStop         =   0   'False
         Tag             =   "Situaci�n geogr�fica del Departamento"
         Top             =   240
         Width           =   540
      End
      Begin VB.CommandButton cmdfactura 
         Caption         =   "Facturas"
         Height          =   375
         Left            =   120
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   2160
         Width           =   1335
      End
      Begin VB.CommandButton cmdpedido 
         Caption         =   "Pedidos"
         Height          =   375
         Left            =   120
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   1680
         Width           =   1335
      End
      Begin VB.CommandButton cmdoferta 
         Caption         =   "Ofertas"
         Height          =   375
         Left            =   120
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CommandButton cmdmaterial 
         Caption         =   "Mat. Sanitario"
         Height          =   375
         Left            =   120
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   720
         Width           =   1335
      End
      Begin VB.CommandButton cmdmedicamento 
         Caption         =   "Medicamentos"
         Height          =   375
         Left            =   120
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Referencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   6
         Left            =   7440
         TabIndex        =   47
         Top             =   600
         Width           =   945
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "U.M."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   16
         Left            =   8640
         TabIndex        =   46
         Top             =   0
         Width           =   420
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Dosis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   17
         Left            =   8040
         TabIndex        =   45
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "F.F."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   18
         Left            =   7440
         TabIndex        =   44
         Top             =   0
         Width           =   345
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Gestiones de Compra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7455
      Index           =   0
      Left            =   90
      TabIndex        =   24
      Top             =   450
      Width           =   9885
      Begin TabDlg.SSTab tabTab1 
         Height          =   6900
         Index           =   0
         Left            =   120
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   480
         Width           =   9615
         _ExtentX        =   16960
         _ExtentY        =   12171
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0511.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(1)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(4)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(14)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(12)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(11)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(10)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(9)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(5)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(7)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(8)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(13)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(15)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(3)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(19)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "dtcDateCombo1(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(6)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(3)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(4)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(2)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(7)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(14)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(13)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(11)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(10)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(9)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(0)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(1)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(5)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(8)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(18)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(20)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(21)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).ControlCount=   34
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0511.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000A&
            DataField       =   "FR40CODGESTCOMPRA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   21
            Left            =   6840
            TabIndex        =   0
            Tag             =   "C�digo Gesti�n Compra"
            Top             =   6120
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000A&
            DataField       =   "SG02COD"
            Height          =   330
            HelpContextID   =   40102
            Index           =   20
            Left            =   5040
            TabIndex        =   49
            Tag             =   "C�digo Persona"
            Top             =   6120
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   18
            Left            =   120
            TabIndex        =   14
            Tag             =   "C�digo Proveedor"
            Top             =   4320
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000A&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   6840
            TabIndex        =   1
            Tag             =   "C�digo Producto"
            Top             =   5520
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR39CODFACTCOMPRA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   5
            Left            =   120
            TabIndex        =   11
            Tag             =   "C�digo de factura de Compra"
            Top             =   3720
            Width           =   3000
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR62CODPEDCOMPRA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   120
            TabIndex        =   9
            Tag             =   "C�digo de Pedido de Compra"
            Top             =   2640
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   810
            HelpContextID   =   30104
            Index           =   0
            Left            =   2040
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   10
            Tag             =   "Descripci�n de Pedido de Compra"
            Top             =   2640
            Width           =   7050
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   7320
            Locked          =   -1  'True
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "F.F. Forma Farmac�utica"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   7920
            Locked          =   -1  'True
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8520
            Locked          =   -1  'True
            TabIndex        =   5
            TabStop         =   0   'False
            Tag             =   "U.M. Unidad de Medida"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   120
            Locked          =   -1  'True
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Descripci�n Producto"
            Top             =   360
            Width           =   7140
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   5520
            Locked          =   -1  'True
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   960
            Width           =   3585
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   7
            Left            =   6000
            TabIndex        =   13
            Tag             =   "Importe"
            Top             =   3720
            Width           =   2010
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   810
            HelpContextID   =   30104
            Index           =   2
            Left            =   2040
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   8
            Tag             =   "Descripci�n de oferta de Proveedor"
            Top             =   1680
            Width           =   7050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR58CODOFERTPROV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   120
            TabIndex        =   7
            Tag             =   "C�digo de Oferta proveedor"
            Top             =   1680
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   3
            Left            =   2040
            TabIndex        =   15
            Tag             =   "Proveedor"
            Top             =   4320
            Width           =   7140
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            DataField       =   "FR40TPDESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   6
            Left            =   2400
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "% Descuento Conseguido"
            Top             =   5040
            Width           =   540
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6705
            Index           =   0
            Left            =   -74910
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   90
            Width           =   9015
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15901
            _ExtentY        =   11827
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            Height          =   330
            Index           =   0
            Left            =   3600
            TabIndex        =   12
            Tag             =   "Fecha Factura"
            Top             =   3705
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   6840
            TabIndex        =   52
            Top             =   5280
            Visible         =   0   'False
            Width           =   1170
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   2040
            TabIndex        =   28
            Top             =   4080
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Gesti�n Compra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   6840
            TabIndex        =   51
            Top             =   5880
            Visible         =   0   'False
            Width           =   1995
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo Persona "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   5040
            TabIndex        =   50
            Top             =   5880
            Visible         =   0   'False
            Width           =   1410
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   48
            Top             =   4080
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Factura"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   3600
            TabIndex        =   38
            Top             =   3480
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Factura Compra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   37
            Top             =   3480
            Width           =   1350
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Pedido Compra"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   36
            Top             =   2400
            Width           =   1290
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   35
            Top             =   120
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   7320
            TabIndex        =   34
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   7920
            TabIndex        =   33
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   8520
            TabIndex        =   32
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   5520
            TabIndex        =   31
            Top             =   720
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   6000
            TabIndex        =   25
            Top             =   3480
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Oferta Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   29
            Top             =   1440
            Width           =   1470
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "% Descuento Conseguido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   27
            Top             =   5160
            Width           =   2175
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   26
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRedGesCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmRedGesCom (FR0511.FRM)                                    *
'* AUTOR: Aitor Vi�uela Garc�a                                          *
'* FECHA: Mayo DE 1999                                                  *
'* DESCRIPCION: Introducir Gestiones de compra                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1


Private Sub cmdfactura_Click()
   Dim rsta As rdoResultset
   Dim stra As String
   ' gintFacBuscado
   cmdfactura.Enabled = False
   If txtText1(1).Text <> "" Then
      gstrLlamador = "FrmRedGesCom"
      Call objsecurity.LaunchProcess("FR0514")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If IsNull(gintFacBuscado) Then
         gintFacBuscado = ""
      End If
      If gintFacBuscado <> "" Then
         stra = "SELECT * FROM FR3900 WHERE FR39CODFACTCOMPRA='" & gintFacBuscado & "'"
         Set rsta = objApp.rdoConnect.OpenResultset(stra)
         If Not rsta.EOF Then
            ' C�d. Factura
            Call objWinInfo.CtrlSet(txtText1(5), gintFacBuscado)
            ' Fecha Factura
            Call objWinInfo.CtrlSet(dtcDateCombo1(0), rsta.rdoColumns("FR39FECFACTCOMPRA").Value)
            ' Proveedor
            Call objWinInfo.CtrlSet(txtText1(7), rsta.rdoColumns("FR39IMPORTE").Value)
         End If
         rsta.Close
         Set rsta = Nothing
      End If
   Else
      MsgBox "Para buscar una Factura debe tener un Pedido", vbInformation, "Aviso"
   End If
   cmdfactura.Enabled = True
End Sub

Private Sub cmdmaterial_Click()
   Dim rsta As rdoResultset
   Dim stra As String
   
   cmdmaterial.Enabled = False
   
   If txtText1(21).Text <> "" Then
      If txtText1(4) <> "" Then
        If MsgBox(" Si cambia de Material perdera los datos de OFERTA PROVEEDOR,PEDIDO COMPRA,FACTURA COMPRA y PROVEEDOR " & Chr(13) & Chr(13) & " Desea continuar ", vbYesNo + vbInformation, "Aviso") = vbNo Then
          cmdmaterial.Enabled = True
          Exit Sub
        Else
         txtText1(4).Text = "" 'Oferta
         txtText1(2).Text = ""
         txtText1(1).Text = "" 'Pedido
         txtText1(0).Text = ""
         txtText1(5).Text = "" 'Factura
         txtText1(7).Text = ""
         dtcDateCombo1(0).Text = ""
         txtText1(18).Text = "" 'Cod.Proveedor
         txtText1(3).Text = ""
         txtText1(6).Text = ""
        End If
        Else
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo 'irene
      End If

      gstrLlamador = "FrmRedGesCom"
      gintFR501Prov = Me.txtText1(18)
      Call objsecurity.LaunchProcess("FR0503")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintProdBuscado <> "" Then
         stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintProdBuscado
         Set rsta = objApp.rdoConnect.OpenResultset(stra)
         If Not rsta.EOF Then
            txtText1(8).Text = gintProdBuscado  'c�d.producto
            txtText1(13).Text = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
               txtText1(9).Text = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
               txtText1(10).Text = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
               txtText1(11).Text = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
               txtText1(14).Text = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
         End If
         rsta.Close
         Set rsta = Nothing
      End If
   Else
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
      gstrLlamador = "FrmRedGesCom"
      gintFR501Prov = Me.txtText1(18)
      Call objsecurity.LaunchProcess("FR0503")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintProdBuscado <> "" Then
         stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintProdBuscado
         Set rsta = objApp.rdoConnect.OpenResultset(stra)
         If Not rsta.EOF Then
            txtText1(8).Text = gintProdBuscado  'c�d.producto
            txtText1(13).Text = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
               txtText1(9).Text = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
               txtText1(10).Text = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
               txtText1(11).Text = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
               txtText1(14).Text = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
         End If
         rsta.Close
         Set rsta = Nothing
      End If
   End If
   
   cmdmaterial.Enabled = True
   
End Sub

Private Sub cmdmedicamento_Click()
   Dim rsta As rdoResultset
   Dim stra As String
   
   cmdmedicamento.Enabled = False
   
   If txtText1(21).Text <> "" Then
      If txtText1(4) <> "" Then
        If MsgBox(" Si cambia de Medicamento perdera los datos de OFERTA PROVEEDOR,PEDIDO COMPRA,FACTURA COMPRA y PROVEEDOR " & Chr(13) & Chr(13) & " Desea continuar ", vbYesNo + vbInformation, "Aviso") = vbNo Then
          cmdmedicamento.Enabled = True
          Exit Sub
        Else
         txtText1(4).Text = "" 'Oferta
         txtText1(2).Text = ""
         txtText1(1).Text = "" 'Pedido
         txtText1(0).Text = ""
         txtText1(5).Text = "" 'Factura
         txtText1(7).Text = ""
         dtcDateCombo1(0).Text = ""
         txtText1(18).Text = "" 'Cod.Proveedor
         txtText1(3).Text = ""
         txtText1(6).Text = ""
        End If
        Else
            Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo 'irene
      End If

      gstrLlamador = "FrmRedGesCom"
      gintFR501Prov = Me.txtText1(18)
      Call objsecurity.LaunchProcess("FR0502")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintProdBuscado <> "" Then
         stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintProdBuscado
         Set rsta = objApp.rdoConnect.OpenResultset(stra)
         If Not rsta.EOF Then
            txtText1(8).Text = gintProdBuscado  'c�d.producto
            txtText1(13).Text = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
               txtText1(9).Text = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
               txtText1(10).Text = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
               txtText1(11).Text = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
               txtText1(14).Text = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
         End If
         rsta.Close
         Set rsta = Nothing
      End If
   Else
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
      gstrLlamador = "FrmRedGesCom"
      gintFR501Prov = Me.txtText1(18)
      Call objsecurity.LaunchProcess("FR0502")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintProdBuscado <> "" Then
         stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintProdBuscado
         Set rsta = objApp.rdoConnect.OpenResultset(stra)
         If Not rsta.EOF Then
            txtText1(8).Text = gintProdBuscado  'c�d.producto
            txtText1(13).Text = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
               txtText1(9).Text = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
               txtText1(10).Text = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
               txtText1(11).Text = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
               txtText1(14).Text = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
         End If
         rsta.Close
         Set rsta = Nothing
      End If
   End If
   
   cmdmedicamento.Enabled = True
   
End Sub

Private Sub cmdoferta_Click()
   Dim rsta As rdoResultset
   Dim stra As String
   
   cmdoferta.Enabled = False

   If txtText1(8).Text <> "" Then
      If txtText1(1).Text <> "" Then
        If MsgBox(" Si cambia de Oferta perdera los datos de PEDIDO COMPRA,FACTURA COMPRA y PROVEEDOR " & Chr(13) & Chr(13) & " Desea continuar ", vbYesNo + vbInformation, "Aviso") = vbNo Then
          cmdoferta.Enabled = True
          Exit Sub
        Else
          txtText1(1).Text = "" 'Pedido
          txtText1(0).Text = ""
          txtText1(5).Text = "" 'Factura
          txtText1(7).Text = ""
          dtcDateCombo1(0).Text = ""
          txtText1(18).Text = "" 'Cod.Proveedor
          txtText1(3).Text = ""
          txtText1(6).Text = ""
        End If
      End If
      
      'objWinInfo.objWinActiveForm.blnChanged = False
      gstrLlamador = "FrmRedGesCom"
      gintFR501Prov = Me.txtText1(18)
      ReDim gintprodbuscado1(0)
      gintprodtotal = 0
      gstrLlamadorOferta = "FrmRedGesCom"
      Call objsecurity.LaunchProcess("FR0532")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintprodbuscado1(1, 0) <> "" Then
         stra = "SELECT * FROM FR5800 WHERE FR58CODOFERTPROV=" & gintprodbuscado1(1, 0)
         Set rsta = objApp.rdoConnect.OpenResultset(stra)
         If Not rsta.EOF Then
            ' C�d.oferta
            txtText1(4).Text = gintprodbuscado1(1, 0)
            ' Descripci�n de la oferta
            If Not IsNull(rsta.rdoColumns("FR58DESOFERTA").Value) Then
                txtText1(2).Text = rsta.rdoColumns("FR58DESOFERTA").Value
            End If
            ' Proveedor
            txtText1(18).Text = rsta.rdoColumns("FR79CODPROVEEDOR").Value
         End If
         rsta.Close
         Set rsta = Nothing
      End If
   Else
      MsgBox "Para buscar una Oferta debe tener un Producto", vbInformation, "Aviso"
   End If
   
   cmdoferta.Enabled = True
   
End Sub

Private Sub cmdpedido_Click()
   Dim rsta As rdoResultset
   Dim stra As String
   
   cmdpedido.Enabled = False
   ' C�digo de Oferta proveedor
   If txtText1(4).Text <> "" Then
      If txtText1(5).Text <> "" Then
        If MsgBox(" Si cambia de Pedido perdera los datos de FACTURA COMPRA " & Chr(13) & Chr(13) & " Desea continuar ", vbYesNo + vbInformation, "Aviso") = vbNo Then
          cmdpedido.Enabled = True
          Exit Sub
        Else
          txtText1(5).Text = "" 'Factura
          txtText1(7).Text = ""
          dtcDateCombo1(0).Text = ""
        End If
      End If
      
      'objWinInfo.objWinActiveForm.blnChanged = False
      gstrLlamador = "FrmRedGesCom"
      gintPedBuscado = ""
      Call objsecurity.LaunchProcess("FR0513")
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If IsNull(gintPedBuscado) Then
         gintPedBuscado = ""
      End If
      If gintPedBuscado <> "" Then
         'c�d.pedido
         txtText1(1).Text = gintPedBuscado
         stra = "SELECT * FROM FR6200 WHERE FR62CODPEDCOMPRA=" & gintPedBuscado
         Set rsta = objApp.rdoConnect.OpenResultset(stra)
         If Not rsta.EOF Then
            ' descripcion del pedido
            txtText1(0).Text = rsta.rdoColumns("FR62DESCPERSONALIZADA").Value
         End If
         rsta.Close
         Set rsta = Nothing
      End If
      gintPedBuscado = ""
      gstrLlamador = ""
   Else
      MsgBox "Para buscar un Pedido debe tener una Oferta", vbInformation, "Aviso"
   End If
   cmdpedido.Enabled = True
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
  
  'Call objApp.SplashOn
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
   With objDetailInfo
      .strName = "Gestiones de Compra"
      Set .objFormContainer = fraFrame1(0)
      Set .objFatherContainer = Nothing
      Set .tabMainTab = tabTab1(0)
      Set .grdGrid = grdDBGrid1(0)
      .strTable = "FR4000"
      .blnAskPrimary = False
      '.intAllowance = cwAllowAll
      .intAllowance = cwAllowModify + cwAllowDelete
      
      Call .FormAddOrderField("FR40CODGESTCOMPRA", cwDescending)

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Gestiones de Compra")
      Call .FormAddFilterWhere(strKey, "FR40CODGESTCOMPRA", "C�digo Gesti�n", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR39CODFACTCOMPRA", "C�digo Factura Compra", cwString)
      Call .FormAddFilterWhere(strKey, "FR58CODOFERTPROV", "C�digo Oferta Proveedor", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR41CODGRUPPROD", "C�digo Grupo Producto", cwString)
      Call .FormAddFilterWhere(strKey, "FR40INDAMBITO", "Indicador de Ambito", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido Compra", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric)
      Call .FormAddFilterWhere(strKey, "SG02COD", "C�digo Persona", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR40TPDESCUENTO", "Descuento Porcentual", cwNumeric)
                                                                                                                  
      Call .FormAddFilterOrder(strKey, "FR40CODGESTCOMPRA", "C�digo Gesti�n")
      Call .FormAddFilterOrder(strKey, "FR39CODFACTCOMPRA", "C�digo Factura Compra")
      Call .FormAddFilterOrder(strKey, "FR58CODOFERTPROV", "C�digo Oferta Proveedor")
      Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
 
   End With

   With objWinInfo
      Call .FormAddInfo(objDetailInfo, cwFormDetail)
      Call .FormCreateInfo(objDetailInfo)

      ' Proveedor
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(18)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(18)), txtText1(3), "FR79PROVEEDOR")
      
      ' Oferta
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(4)), "FR58CODOFERTPROV", "SELECT * FROM FR5800 WHERE FR58CODOFERTPROV = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(4)), txtText1(2), "FR58DESOFERTA")
      
      ' Pedido de compra
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(1)), "FR62CODPEDCOMPRA", "SELECT * FROM FR6200 WHERE FR62CODPEDCOMPRA = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(1)), txtText1(0), "FR62DESCPERSONALIZADA")
      
      ' Factura de compra
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(5)), "FR39CODFACTCOMPRA", "SELECT * FROM FR3900 WHERE FR39CODFACTCOMPRA = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), txtText1(7), "FR39IMPORTE")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(5)), dtcDateCombo1(0), "FR39FECFACTCOMPRA")
      
      ' Producto
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(13), "FR73DESPRODUCTOPROV")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FRH7CODFORMFAR")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(10), "FR73DOSIS")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(11), "FR93CODUNIMEDIDA")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(14), "FR73REFERENCIA")
      
      .CtrlGetInfo(txtText1(18)).blnReadOnly = True
      .CtrlGetInfo(txtText1(4)).blnReadOnly = True
      .CtrlGetInfo(txtText1(1)).blnReadOnly = True
      .CtrlGetInfo(txtText1(5)).blnReadOnly = True
      
      Call .WinRegister
      Call .WinStabilize
   End With

   txtText1(20).Text = objsecurity.strUser

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------

Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   Dim rsta As rdoResultset
   Dim sqlstr As String

   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
   ' Nuevo
   If btnButton.Index = 2 Then
      sqlstr = "SELECT FR40CODGESTCOMPRA_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      txtText1(21).Text = rsta.rdoColumns(0).Value
      txtText1(20).Text = objsecurity.strUser
      rsta.Close
      Set rsta = Nothing
   End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
   Dim rsta As rdoResultset
   Dim sqlstr As String

    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
   ' Nuevo
   If intIndex = 10 Then
      sqlstr = "SELECT FR40CODGESTCOMPRA_SEQUENCE.nextval FROM dual"
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      txtText1(21).Text = rsta.rdoColumns(0).Value
      txtText1(20).Text = objsecurity.strUser
      rsta.Close
      Set rsta = Nothing
   End If
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 20 Then
     txtText1(20).Text = objsecurity.strUser
  End If
  If intIndex = 6 Then
    tlbToolbar1.Buttons(4).Enabled = True
  End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)

'  If txtText1(4).Text = "E" Or txtText1(4).Text = "S" Then
'  Else
'    MsgBox "El Tipo de entrada es incorrecto." & Chr(13) & _
'           "Valores v�lidos: E � S", vbExclamation
'    txtText1(4).Text = ""
'    blnCancel = True
'  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Tipos de Interacci�n" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

