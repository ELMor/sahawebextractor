VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmAsientos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Asientos Contables"
   ClientHeight    =   8340
   ClientLeft      =   1815
   ClientTop       =   2475
   ClientWidth     =   11910
   ControlBox      =   0   'False
   HelpContextID   =   30001
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdGenTrans 
      Caption         =   "&Transferir a Compras"
      Height          =   495
      Left            =   10560
      TabIndex        =   22
      ToolTipText     =   "Se generar el fichero txt para la conexi�n con compras de la Universidad"
      Top             =   1320
      Width           =   1215
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Detalle Factura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2655
      Index           =   2
      Left            =   120
      TabIndex        =   16
      Top             =   2760
      Width           =   11685
      Begin TabDlg.SSTab tabTab1 
         Height          =   2220
         Index           =   0
         Left            =   120
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   240
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   3916
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0547.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(0)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "txtText1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "txtText1(1)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(7)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(8)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).ControlCount=   6
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0547.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(3)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT2VOUCHERLINENUM"
            Height          =   330
            Index           =   8
            Left            =   960
            ScrollBars      =   2  'Vertical
            TabIndex        =   8
            Tag             =   "FRT2VOUCHERLINENUM"
            Top             =   1080
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT1CODCAB"
            Height          =   330
            Index           =   7
            Left            =   120
            ScrollBars      =   2  'Vertical
            TabIndex        =   7
            Tag             =   "FRT1CODCAB"
            Top             =   1080
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT2MERCHANDISE_AMT"
            Height          =   330
            Index           =   1
            Left            =   9360
            ScrollBars      =   2  'Vertical
            TabIndex        =   6
            Tag             =   "Importe|Importe de la l�nea de la factura"
            Top             =   480
            Width           =   1440
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT2DESCR"
            Height          =   330
            Index           =   0
            Left            =   120
            TabIndex        =   5
            Tag             =   "L�nea Factura|Descripci�n de la l�nea de factura"
            Top             =   480
            Width           =   9135
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1425
            Index           =   2
            Left            =   -74880
            TabIndex        =   18
            TabStop         =   0   'False
            Top             =   120
            Width           =   9015
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15901
            _ExtentY        =   2514
            _StockProps     =   79
            Caption         =   "CARRO"
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1905
            Index           =   3
            Left            =   -74880
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   120
            Width           =   10575
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18653
            _ExtentY        =   3360
            _StockProps     =   79
            Caption         =   "Detalle Factura"
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   9360
            TabIndex        =   20
            Top             =   240
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            Caption         =   "L�nea de la Factura"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   19
            Top             =   240
            Width           =   2295
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Factura"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Index           =   1
      Left            =   120
      TabIndex        =   12
      Top             =   480
      Width           =   10245
      Begin TabDlg.SSTab tabTab1 
         Height          =   1740
         Index           =   1
         Left            =   120
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   360
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   3069
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0547.frx":0038
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(4)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(20)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(2)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(3)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(5)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(1)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(5)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(2)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(6)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0547.frx":0054
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT1CODCAB"
            Height          =   330
            Index           =   6
            Left            =   6360
            ScrollBars      =   2  'Vertical
            TabIndex        =   29
            Tag             =   "FRT1CODCAB"
            Top             =   960
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT1VENDOR"
            Height          =   330
            Index           =   3
            Left            =   120
            TabIndex        =   4
            Tag             =   "C�d.Prov.|C�digo del proveedor"
            Top             =   960
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            Index           =   2
            Left            =   840
            ScrollBars      =   2  'Vertical
            TabIndex        =   27
            Tag             =   "Proveedor"
            Top             =   960
            Width           =   5400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT1GROSSAMT"
            Height          =   330
            Index           =   5
            Left            =   6360
            TabIndex        =   3
            Tag             =   "Importe|Importe total de la factura"
            Top             =   360
            Width           =   1680
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRT1INVOICEID"
            Height          =   330
            Index           =   4
            Left            =   120
            TabIndex        =   0
            Tag             =   "N�Factura|N�mero o identificador de la factura"
            Top             =   360
            Width           =   1935
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1425
            Index           =   1
            Left            =   -74880
            TabIndex        =   14
            TabStop         =   0   'False
            Top             =   120
            Width           =   9495
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16748
            _ExtentY        =   2514
            _StockProps     =   79
            Caption         =   "Factura"
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRT1INVOICEDT"
            Height          =   330
            Index           =   0
            Left            =   2170
            TabIndex        =   1
            TabStop         =   0   'False
            Tag             =   "Fec.Fact.|Fecha de la factura de compra"
            Top             =   360
            Width           =   1980
            _Version        =   65537
            _ExtentX        =   3492
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRT1DUEDT"
            Height          =   330
            Index           =   1
            Left            =   4265
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Fec.Ult.Venc.|Fecha �ltimo Vencimiento"
            Top             =   360
            Width           =   1980
            _Version        =   65537
            _ExtentX        =   3492
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   5
            Left            =   120
            TabIndex        =   28
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   3
            Left            =   6360
            TabIndex        =   26
            Top             =   120
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Ult. Vencimiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   4265
            TabIndex        =   25
            Top             =   120
            Width           =   1995
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Factura"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   2160
            TabIndex        =   24
            Tag             =   "Fec.Fact.|Fecha de la factura de compra"
            Top             =   120
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            Caption         =   "N�Factura"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   4
            Left            =   120
            TabIndex        =   23
            Top             =   120
            Width           =   1215
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Asientos Contables"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2415
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   5520
      Width           =   11685
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1905
         Index           =   0
         Left            =   240
         TabIndex        =   15
         Top             =   360
         Width           =   11415
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20135
         _ExtentY        =   3360
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   11
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmAsientos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmAsientos (FR0547.FRM)                                     *
'* AUTOR: JESUS M� RODILLA LARA                                         *
'* FECHA: AGOSTO DE 1999                                                *
'* DESCRIPCION: Muestra el transfer con compras de la Universidad       *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cmdGenTrans_Click()
   'Se generar el fichero txt con el transfer a compras de la Universidad
  Dim strCab As String
  Dim qryCab As rdoQuery
  Dim rstCab As rdoResultset
  Dim strDet As String
  Dim qryDet As rdoQuery
  Dim rstDet As rdoResultset
  Dim strAsi As String
  Dim qryAsi As rdoQuery
  Dim rstAsi As rdoResultset
  Dim strFic As String 'Fichero para realizar el transfer
  Dim strReg As String 'Registro a escribir en el transfer
  Dim strAux As String 'Valiable de apoyo para construir strReg
  Dim strUpd As String
  Dim qryUpd As rdoQuery
  Dim strHora As String
  Dim rstHora As rdoResultset
  Dim bln001 As Boolean
  Dim strP As String
  Dim strQ As Currency
  
  If Not IsNumeric(txtText1(5).Text) Then
    MsgBox "No hay facturas para realizar el transfer", vbInformation, "Generar Transfer"
    Exit Sub
  End If
  cmdGenTrans.Enabled = False
  
  'Se abre el fichero del transfer
  strHora = "SELECT TO_CHAR(SYSDATE,'HH24MISS') FROM DUAL"
  Set rstHora = objApp.rdoConnect.OpenResultset(strHora)
  strFic = rstHora.rdoColumns(0).Value
  rstHora.Close
  Set rstHora = Nothing
  strFic = strRellenar(strFic, "0", "I", 6 - Len(strFic))
  Open "C:\" & strFic & ".txt" For Output As #2
  'Se escribe la cabecera: 999VOUCHER-IN      APBUCUNEDI          APBUFACTCUNEDI
  strReg = "999VOUCHER-IN     APBUCUNEDI          APBUFACTCUNEDI"
  Print #2, strReg
  'Print #2, Chr(13)
  
  'Para cada factura/abono
  strCab = "SELECT FRT1CODCAB,FRT1ROWID,FRT1BUSINESSUNIT,FRT1INVOICEID, " & _
           "       TO_CHAR(FRT1INVOICEDT,'DD/MM/YYYY'),FRT1VENDOR,FRT1VCHRTTLLINES,FRT1GROSSAMT," & _
           "       TO_CHAR(FRT1DUEDT,'DD/MM/YYYY'),FRT1GRPAPID,FRT1INDINTERFCONT " & _
           " FROM FRT100 WHERE FRT1INDINTERFCONT = ? "
  Set qryCab = objApp.rdoConnect.CreateQuery("", strCab)
  qryCab(0) = 0
  Set rstCab = qryCab.OpenResultset()
  While Not rstCab.EOF
    'Se escribe la cabecera de la factura
    strReg = "000CUN  " 'Siguiente posicici�n: 9; FRT1ROWID & FRT1BUSINESSUNIT
    strAux = rstCab.rdoColumns("FRT1INVOICEID").Value
    strReg = strReg & strRellenar(strAux, " ", "D", 16 - Len(strAux)) 'FRT1INVOICEID
    strAux = rstCab.rdoColumns(4).Value
    strReg = strReg & strAux 'FRT1INVOICEDT
    strAux = rstCab.rdoColumns("FRT1VENDOR").Value
    strReg = strReg & strAux 'FRT1VENDOR
    strAux = rstCab.rdoColumns("FRT1VCHRTTLLINES").Value
    strAux = "1"
    strReg = strReg & strRellenar(strAux, " ", "I", 11 - Len(strAux)) 'FRT1VCHRTTLLINES
    strQ = Fix(rstCab.rdoColumns("FRT1GROSSAMT").Value)
    'strQ = strQ / 100
    strAux = strQ & ".00"
    strP = strAux
    strReg = strReg & strRellenar(strAux, " ", "I", 17 - Len(strAux)) 'FRT1GROSSAMT
    strAux = rstCab.rdoColumns(8).Value
    strReg = strReg & strAux 'FRT1DUEDT
    strReg = strReg & strRellenar(strFic, " ", "I", 6 - Len(strFic)) 'FRT1GRPAPID
    Print #2, strReg
    'Print #2, Chr(13)
    
    'Para cada l�nea de la factura
    strDet = "SELECT FRT1CODCAB,FRT2ROWID,FRT2BUSINESSUNIT,FRT2VOUCHERLINENUM, " & _
             "       FRT2TOTALDISTRIBS,FRT2DESCR,FRT2MERCHANDISE_AMT,FRH8MONEDA " & _
             "  FROM FRT200 " & _
             " WHERE FRT1CODCAB = ? "
    Set qryDet = objApp.rdoConnect.CreateQuery("", strDet)
    qryDet(0) = rstCab.rdoColumns("FRT1CODCAB").Value
    Set rstDet = qryDet.OpenResultset()
        
    bln001 = True
    While Not rstDet.EOF
      'Se escribe el detalle de las facturas
      If bln001 Then
        strReg = "001CUN  " 'FRT2ROWID & FRT2BUSINESSUNIT
        strAux = rstDet.rdoColumns("FRT2VOUCHERLINENUM").Value
        strAux = 1
        strReg = strReg & strRellenar(strAux, " ", "I", 5 - Len(strAux)) 'FRT2VOUCHERLINENUM
        strAux = rstDet.rdoColumns("FRT2TOTALDISTRIBS").Value
        strReg = strReg & strRellenar(strAux, " ", "I", 5 - Len(strAux)) 'FRT2TOTALDISTRIBS
        strAux = rstDet.rdoColumns("FRT2DESCR").Value
        strReg = strReg & strRellenar(strAux, " ", "D", 30 - Len(strAux)) 'FRT2DESCR
        'strAux = objGen.ReplaceStr(Format(rstDet.rdoColumns("FRT2MERCHANDISE_AMT").Value, "##0.00"), ",", ".", 1) 'Se cambian las comas por puntos
        strAux = strP
        strReg = strReg & strRellenar(strAux, " ", "I", 17 - Len(strAux)) 'FRT2MERCHANDISE_AMT
        Print #2, strReg
        bln001 = False
      End If
      
      'Para cada asiento contable
      strAsi = "SELECT FRT3VOUCHERLINENUM,FRT3DISTRIBLINENUM,FRT3ACCOUNT,FRT3MERCHANDISEAMT " & _
               "  FROM FRT300 " & _
               " WHERE FRT1CODCAB = ? " & _
               "   AND FRT2VOUCHERLINENUM = ? "
      Set qryAsi = objApp.rdoConnect.CreateQuery("", strAsi)
      qryAsi(0) = rstCab.rdoColumns("FRT1CODCAB").Value
      qryAsi(1) = rstDet.rdoColumns("FRT2VOUCHERLINENUM").Value
      Set rstAsi = qryAsi.OpenResultset()
      While Not rstAsi.EOF
        'Se escriben los asientos contables
        strReg = "002CUN  " 'FRT3ROWID & FRT3BUSINESSUNIT
        strAux = rstAsi.rdoColumns("FRT3DISTRIBLINENUM").Value
        strAux = 1
        strReg = strReg & strRellenar(strAux, " ", "I", 5 - Len(strAux)) 'FRT3DISTRIBLINENUM
        strAux = rstAsi.rdoColumns("FRT3VOUCHERLINENUM").Value
        strReg = strReg & strRellenar(strAux, " ", "I", 5 - Len(strAux)) 'FRT3VOUCHERLINENUM
        strAux = Left(rstAsi.rdoColumns("FRT3ACCOUNT").Value, Len(rstAsi.rdoColumns("FRT3ACCOUNT").Value) - 2)
        strReg = strReg & strRellenar(strAux, " ", "D", 6 - Len(strAux)) 'FRT3ACCOUNT
        'strAux = objGen.ReplaceStr(Format(rstAsi.rdoColumns("FRT3MERCHANDISEAMT").Value, "0.00"), ",", ".", 1) 'Se cambian las comas por puntos
        'strAux = strPonerDec(strAux)
        strAux = Fix(rstAsi.rdoColumns("FRT3MERCHANDISEAMT").Value)
        strAux = strAux & ".00"
        strReg = strReg & strRellenar(strAux, " ", "I", 17 - Len(strAux)) 'FRT3MERCHANDISEAMT
        strReg = strReg & "130215    1                    CUN  "
        Print #2, strReg
        'Print #2, Chr(13)
        rstAsi.MoveNext
      Wend
      qryAsi.Close
      Set qryAsi = Nothing
      Set rstAsi = Nothing
      rstDet.MoveNext
    Wend
    qryDet.Close
    Set qryDet = Nothing
    Set rstDet = Nothing
    strUpd = "UPDATE FRT100 SET FRT1GRPAPID = ? "
    Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
    qryUpd(0) = strFic
    qryUpd.Execute
    rstCab.MoveNext
  Wend
  qryCab.Close
  Set qryCab = Nothing
  Set rstCab = Nothing
  'Se cierra el fichero del transfer
  Print #2, "" '&H1A
  Close #2
  strUpd = "UPDATE FRT100 SET FRT1INDINTERFCONT = ?"
  Set qryUpd = objApp.rdoConnect.CreateQuery("", strUpd)
  qryUpd(0) = -1
  qryUpd.Execute
  
  MsgBox "Se ha generado el fichero C:\" & strFic & ".txt", vbInformation, "Generar Transfer"
  cmdGenTrans.Enabled = True
  
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
  objWinInfo.DataRefresh
End Sub
Private Function strPonerDec(strPal As String) As String
  'Toma como entrada strPal, que es un n�mero y lo pone con 2 decimales
  Dim intCont As Integer
  Dim blnHayPto As Boolean
  Dim intNumDec As Integer
  Dim strResto As String
  
  
  intNumDec = 2
  strResto = strPal
  blnHayPto = False
  For intCont = 1 To Len(strPal)
    'Se mira cuantos decimales tiene
    If Left(strResto, 1) = "." Then
      blnHayPto = True
      strResto = Right(strResto, Len(strResto) - 1)
    Else
      If blnHayPto = True Then
        intNumDec = intNumDec - 1
      End If
      strResto = Right(strResto, Len(strResto) - 1)
    End If
  Next intCont
  
  If blnHayPto = False Then
    strPonerDec = strPal & ".00"
  Else
    strResto = strPal
    For intCont = 1 To 2 - intNumDec
      strResto = strResto & "0"
    Next intCont
    If intNumDec < 0 Then
      strPonerDec = Left(strResto, Len(strResto) - (intNumDec * (-1)))
    Else
      strPonerDec = strResto
    End If
  End If
End Function
Private Function strRellenar(strPal As String, strRe As String, strPor As String, intNum As Integer) As String
  'Toma la palabra strPal y la devuelve rellenandola con el termino strRe tantas veces como
  'indique el campo intNum. Si strPor = D se rellenar� por la derecha, si strPor = I se re_
  'llenar� por la izquierda
  Dim intCont As Integer
  Dim strCompos As String
  
  If UCase(strPor) = "D" Then
    