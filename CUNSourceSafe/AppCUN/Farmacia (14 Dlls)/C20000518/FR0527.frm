VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmIntFacCom 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Introducir Factura "
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0527.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.TextBox txtAlbaran 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00C0C0C0&
      Height          =   330
      HelpContextID   =   40102
      Left            =   6720
      TabIndex        =   19
      TabStop         =   0   'False
      Tag             =   "C�digo Albar�n"
      ToolTipText     =   "C�digo Albar�n"
      Top             =   3360
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Ver Facturas de un  Albaran"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   2760
      TabIndex        =   18
      ToolTipText     =   "Se selecciona un albar�n y se ven solo las factuas asociadas a �l."
      Top             =   3360
      Width           =   2775
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Albaranes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3165
      Index           =   2
      Left            =   8760
      TabIndex        =   34
      Top             =   480
      Width           =   3075
      Begin VB.CommandButton cmdVer 
         Caption         =   "&Ver Albaran"
         Height          =   375
         Left            =   1680
         TabIndex        =   17
         Top             =   2640
         Width           =   1335
      End
      Begin VB.CommandButton cmdBuscar 
         Caption         =   "&Buscar Albaran"
         Height          =   375
         Left            =   120
         TabIndex        =   16
         Top             =   2640
         Width           =   1335
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2115
         Index           =   2
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   2895
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   5106
         _ExtentY        =   3731
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4305
      Index           =   1
      Left            =   90
      TabIndex        =   23
      Top             =   3720
      Width           =   11745
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   3705
         Index           =   1
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   11415
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20135
         _ExtentY        =   6535
         _StockProps     =   79
      End
      Begin VB.CommandButton cmdmaterial 
         Caption         =   "Material Sanitario"
         Height          =   330
         Left            =   9480
         TabIndex        =   35
         Top             =   360
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CommandButton cmdmedicamento 
         Caption         =   "Medicamentos"
         Height          =   330
         Left            =   7560
         TabIndex        =   36
         Top             =   360
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.CheckBox chkverprod 
         Caption         =   "Ver Productos"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   255
         Left            =   360
         TabIndex        =   37
         Top             =   360
         Visible         =   0   'False
         Width           =   1815
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Factura"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2805
      Index           =   0
      Left            =   120
      TabIndex        =   22
      Top             =   480
      Width           =   8625
      Begin TabDlg.SSTab tabTab1 
         Height          =   2295
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   21
         TabStop         =   0   'False
         Top             =   360
         Width           =   8370
         _ExtentX        =   14764
         _ExtentY        =   4048
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0527.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(8)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(10)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(11)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(12)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(20)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(21)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(0)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "chkCheck1(0)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcDateCombo1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(6)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(5)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(1)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(7)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(2)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "chkCheck1(1)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtvencimiento"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "chkCheck1(2)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "chkCheck1(3)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(0)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).ControlCount=   23
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0527.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR39REDONDEO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   0
            Left            =   4080
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Importe"
            Top             =   960
            Width           =   1200
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Interfase Contable"
            DataField       =   "FR39INDINTERFCONT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Index           =   3
            Left            =   6000
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Interfase Contable"
            Top             =   1440
            Visible         =   0   'False
            Width           =   1935
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Reclamada"
            DataField       =   "FR39INDRECLAMADA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   2
            Left            =   6000
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Reclamada"
            Top             =   1800
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.TextBox txtvencimiento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   40101
            Left            =   2160
            MaxLength       =   3
            TabIndex        =   4
            Tag             =   "C�digo Pedido"
            Top             =   960
            Width           =   435
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Aprobada"
            DataField       =   "FR39INDAPROBADA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   1
            Left            =   2760
            TabIndex        =   6
            TabStop         =   0   'False
            Tag             =   "Aprobada"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   2
            Left            =   120
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "C�digo Proveedor"
            Top             =   1560
            Width           =   600
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR39IMPORTE"
            Height          =   330
            HelpContextID   =   40102
            Index           =   7
            Left            =   5400
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Importe"
            Top             =   960
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR39BASEIMPON"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   6720
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Base Imponible"
            Top             =   960
            Visible         =   0   'False
            Width           =   1200
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   720
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Proveedor"
            Top             =   1560
            Width           =   5055
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR39CODFACTCOMPRA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   120
            MaxLength       =   16
            TabIndex        =   0
            Tag             =   "C�digo Factura"
            Top             =   360
            Width           =   3000
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2010
            Index           =   0
            Left            =   -74880
            TabIndex        =   14
            Top             =   120
            Width           =   7575
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   13361
            _ExtentY        =   3545
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR39FECFACTCOMPRA"
            Height          =   330
            Index           =   1
            Left            =   3360
            TabIndex        =   1
            Tag             =   "Fecha "
            Top             =   345
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR39FECULTVENCIM"
            Height          =   330
            Index           =   2
            Left            =   120
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "�ltimo Vencimiento"
            Top             =   945
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR39FECCONF"
            Height          =   330
            Index           =   0
            Left            =   5400
            TabIndex        =   2
            Tag             =   "Conformidad"
            Top             =   345
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Aceptada"
            DataField       =   "FR39INDACEPTADA"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Index           =   0
            Left            =   2760
            TabIndex        =   5
            Tag             =   "Aceptada"
            Top             =   960
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Redondeo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   5400
            TabIndex        =   38
            Top             =   720
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� d�as vencimiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   2160
            TabIndex        =   33
            Top             =   720
            Width           =   1740
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Conformidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   5400
            TabIndex        =   32
            Top             =   120
            Width           =   1650
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "�ltimo Vencimiento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   120
            TabIndex        =   31
            Top             =   720
            Width           =   1635
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   4080
            TabIndex        =   30
            Top             =   720
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Base Imponible"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   6720
            TabIndex        =   29
            Top             =   720
            Visible         =   0   'False
            Width           =   1305
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Factura"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   3360
            TabIndex        =   28
            Top             =   120
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   26
            Top             =   1320
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Factura"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   25
            Top             =   120
            Width           =   1110
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   24
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblAlbaran 
      AutoSize        =   -1  'True
      Caption         =   "Albar�n:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   5760
      TabIndex        =   39
      Top             =   3360
      Visible         =   0   'False
      Width           =   840
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmIntFacCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmIntFacCom (FR0527.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: Introducir Factura de Compra                            *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnInserDetFac As Boolean 'Se pone a true si ya se ha guardado el detalle de la factura (objwininfo.cwpostwrite)
Dim mdblImporteTotal As Double 'Se utiliza para almacenar el importe total de la factura (objwininfo.cwpostwrite)
Dim mstrNumFac As String 'Se utiliza para almacenar el n�mero de factura
Dim mblnYaRefres As Boolean 'Se utiliza para actualizar el n�mero de factura
Dim mblnFactApro As Boolean 'True indica que la factura est� aprobada (se modifica en el toolbar y se consulta el chkcheck1_change)
Dim mblnBorrar As Boolean 'True si se ha borrado una l�nea del detalle
Dim strNF As String
Dim strDate As String
Dim mstrCodFac As String
Dim mcurImp As Currency
Dim mcurRed As Currency
Dim mstrFV As String
Dim mintCodProv As Integer
Dim mstrProv As String
Dim mdblDPP As Currency
Dim mdifR As Currency



Private Function intDiasVenc(strCodProv As String) As Integer
  Dim strFR79 As String
  Dim qryFR79 As rdoQuery
  Dim rstFR79 As rdoResultset
  
  strFR79 = "SELECT FR79INDPRONTOPAG FROM FR7900 WHERE FR79CODPROVEEDOR = ? "
  Set qryFR79 = objApp.rdoConnect.CreateQuery("", strFR79)
  qryFR79(0) = strCodProv
  Set rstFR79 = qryFR79.OpenResultset()
  If Not rstFR79.EOF Then
    If rstFR79.rdoColumns("FR79INDPRONTOPAG").Value = -1 Then
      intDiasVenc = 30
    Else
      intDiasVenc = 90
    End If
  Else
    intDiasVenc = 90
  End If
  qryFR79.Close
  Set qryFR79 = Nothing
  Set rstFR79 = Nothing
End Function
Private Sub Check1_Click()
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim qryFR15 As rdoQuery

   lblAlbaran.Visible = False
   txtAlbaran.Visible = False
   If Check1.Value = vbChecked Then
      If Len(Trim(txtText1(6).Text)) > 0 Then
         ' Comprobar que la factura tenga productos
         sqlstr = " Select * From fr1500 Where FR39CODFACTCOMPRA = ? "
         Set qryFR15 = objApp.rdoConnect.CreateQuery("", sqlstr)
         qryFR15(0) = txtText1(6).Text
         Set rsta = qryFR15.OpenResultset()
         If rsta.EOF Then
            Call MsgBox("La factura:  - " & txtText1(6).Text & " -  no tiene ning�n producto, debe borrarla o a�adir productos", vbExclamation, "Aviso")
            Exit Sub
         End If
         qryFR15.Close
         Set qryFR15 = Nothing
         Set rsta = Nothing
         
         gstrLlamador = "FrmRedGesCom-Check1"
         gstrCodAlb = ""
         gstrCodProv = txtText1(2).Text
         Call objsecurity.LaunchProcess("FR0538") ' FrmBusAlb -> Buscar Albaranes
         If IsNull(gstrCodAlb) Then
            gstrCodAlb = ""
         End If
         If Len(Trim(gstrCodAlb)) > 0 Then
            lblAlbaran.Visible = True
            txtAlbaran.Visible = True
            Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
            objWinInfo.objWinActiveForm.strWhere = "FR39CODFACTCOMPRA IN (SELECT FR39CODFACTCOMPRA FROM FRJ400 WHERE FRJ1CODALBARAN = '" & gstrCodAlb & "')"
            Call objWinInfo.DataRefresh
         Else
            Check1.Value = vbUnchecked
         End If
      End If
   Else
      gstrCodAlb = ""
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      objWinInfo.objWinActiveForm.strWhere = ""
      Call objWinInfo.DataRefresh
   End If
   txtAlbaran.Text = gstrCodAlb
End Sub

Private Sub chkverprod_Click()
  If chkverprod.Value = 1 Then
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    Call objWinInfo.DataRefresh
    chkverprod.Value = 0
  End If
End Sub

Private Sub cmdbuscar_Click()
   ' Para insertar en la tabla que relaciona facturas - albaranes
   Dim strFacAlb As String
   
   ' Productos de la factura
   Dim strProdfac As String
   Dim strlinea As String
   Dim rstlinea As rdoResultset
   Dim intLinea As Integer
   
   ' Productos del albar�n
   Dim strProdAlb As String
   Dim rstProdAlb As rdoResultset
   
   ' Variables para transformar la coma decimal en punto
   Dim strCantRecibida As String
   Dim preciounidad As String
   Dim imporlinea As String
   Dim strupdate As String
   Dim Q%
   Dim qryFRJ3 As rdoQuery
   Dim qryFR15 As rdoQuery
   Dim qryUpd As rdoQuery
   
   Me.Enabled = False
   If IsNumeric(txtText1(7).Text) Then
    mdblImporteTotal = txtText1(7).Text
   Else
    mdblImporteTotal = 0
   End If
   
   If chkCheck1(1).Value = 1 Then
     MsgBox "La factura est� aprobada y no es modificable", vbInformation, "Factura Aprobada"
     Me.Enabled = True
     Exit Sub
   End If
   cmdbuscar.Enabled = False
   
   gintCodsAlbs = 0
   If Len(Trim(txtText1(6).Text)) > 0 Then
      gstrLlamador = "FrmRedGesCom-cmdBuscar"
      gstrCodProv = txtText1(2).Text
      Call objsecurity.LaunchProcess("FR0538") ' FrmBusAlb -> Buscar Albaranes
      'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      If gintCodsAlbs > 0 Then
         For Q% = 0 To gintCodsAlbs - 1
            mblnInserDetFac = True
            
            'Se introduce en la tabla FRJ400 que relaciona albaranes - facturas
            strFacAlb = "INSERT INTO FRJ400 (FRJ1CODALBARAN,FR39CODFACTCOMPRA) " & _
                        " VALUES ('" & gstrCodsAlbs(Q%) & "','" & txtText1(6).Text & "')"
            ' Tratamiento de errores para tratar repetidos
            On Error GoTo Tratar_Error
            objApp.rdoConnect.Execute strFacAlb, 64
            On Error GoTo 0
            
            ' Se introducen en la factura los productos del albar�n
            strProdAlb = "Select * From FRJ300 where FRJ1CODALBARAN = ? "
            Set qryFRJ3 = objApp.rdoConnect.CreateQuery("", strProdAlb)
            qryFRJ3(0) = gstrCodsAlbs(Q%)
            Set rstProdAlb = qryFRJ3.OpenResultset()
            
            strlinea = "SELECT MAX(FR15CODDETFACTCOMP) " & _
                      "   FROM FR1500 " & _
                      "  WHERE FR39CODFACTCOMPRA = ? "
            Set qryFR15 = objApp.rdoConnect.CreateQuery("", strlinea)
            qryFR15(0) = txtText1(6).Text
            Set rstlinea = qryFR15.OpenResultset()
            If IsNull(rstlinea.rdoColumns(0).Value) Then
               intLinea = 1
            Else
               intLinea = rstlinea.rdoColumns(0).Value + 1
            End If
            qryFR15.Close
            Set qryFR15 = Nothing
            Set rstlinea = Nothing
            
            While Not rstProdAlb.EOF
               ' Transforma la coma de separaci�n de los decimales por un punto
               If IsNull(rstProdAlb.rdoColumns("FRJ3CANTPTEFACT").Value) Then
                  strCantRecibida = 0
               Else
                  strCantRecibida = rstProdAlb.rdoColumns("FRJ3CANTPTEFACT").Value
                  strCantRecibida = objGen.ReplaceStr(strCantRecibida, ",", ".", 1)
               End If
               preciounidad = rstProdAlb.rdoColumns("FRJ3PRECIOUNIDAD").Value
               preciounidad = objGen.ReplaceStr(preciounidad, ",", ".", 1)
               imporlinea = rstProdAlb.rdoColumns("FRJ3IMPORLINEA").Value
               imporlinea = objGen.ReplaceStr(imporlinea, ".", ",", 1)
               mdblImporteTotal = mdblImporteTotal + CDbl(imporlinea)
               Dim tpdes As String
               If Not IsNull(rstProdAlb.rdoColumns("FRJ3DTO").Value) Then
                tpdes = objGen.ReplaceStr(rstProdAlb.rdoColumns("FRJ3DTO").Value, ",", ".", 1)
               Else
                tpdes = "0"
               End If
         
               strProdfac = "INSERT INTO FR1500 " & _
                           " (FR39CODFACTCOMPRA, " & _
                           "  FR15CODDETFACTCOMP," & _
                           "  FR73CODPRODUCTO," & _
                           "  FR15CANTIDAD," & _
                           "  FR15PRECIOUNIDAD," & _
                           "  FR15IMPORTE," & _
                           "  FRH8MONEDA," & _
                           "  FRJ1CODALBARAN," & _
                           "  FRJ3NUMLINEA,FR15TPDESCUENTO)" & _
                           " VALUES " & _
                           "('" & txtText1(6).Text & "'," & _
                           intLinea & "," & _
                           rstProdAlb.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                           strCantRecibida & "," & _
                           preciounidad & "," & _
                           objGen.ReplaceStr(imporlinea, ",", ".", 1) & "," & _
                           "'" & rstProdAlb.rdoColumns("FRH8MONEDA").Value & "'," & _
                           "'" & rstProdAlb.rdoColumns("FRJ1CODALBARAN").Value & "'," & _
                           rstProdAlb.rdoColumns("FRJ3NUMLINEA").Value & _
                           "," & tpdes & ")"
               objApp.rdoConnect.Execute strProdfac, 64
               objApp.rdoConnect.Execute "Commit", 64
               rstProdAlb.MoveNext
               intLinea = intLinea + 1
            Wend
         Next Q%
         strupdate = "UPDATE FR3900 " & _
                     " SET FR39IMPORTE = " & objGen.ReplaceStr(mdblImporteTotal, ",", ".", 1) & " , FR39REDONDEO =  " & objGen.ReplaceStr(mdblImporteTotal, ",", ".", 1) & _
                     " WHERE FR39CODFACTCOMPRA = ? "
         Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
         'qryUpd(0) = mdblImporteTotal
         'qryUpd(1) = mdblImporteTotal
         qryUpd(0) = txtText1(6).Text
         qryUpd.Execute
         qryUpd.Close
         Set qryUpd = Nothing
         qryFRJ3.Close
         Set qryFRJ3 = Nothing
         Set rstProdAlb = Nothing
         Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
         Call objWinInfo.DataRefresh
         'Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
         'Call objWinInfo.DataRefresh
         'Call objWinInfo.FormChangeActive(fraFrame1(2), False, True)
         'Call objWinInfo.DataRefresh
      End If
   Else
      MsgBox "Para buscar una Albaran debe tener una Factura", vbInformation, "Aviso"
   End If
   Me.Enabled = True
   cmdbuscar.Enabled = True

Exit Sub

Tratar_Error:
   If ERR.Number = 40002 Then
      MsgBox "El albar�n buscado ya est� asociado a la factura", vbInformation, "Aviso"
   Else
      MsgBox ERR.Description
   End If
   cmdbuscar.Enabled = True

End Sub

Private Sub cmdmaterial_Click()
  Dim rsta As rdoResultset
  Dim stra As String
  Dim strcamposoblig As String
  Dim strFac As String
  Dim rstFac As rdoResultset
  Dim qryFR39 As rdoQuery
  Dim qryFR73 As rdoQuery
  
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)

  cmdMaterial.Enabled = False

  If txtText1(6).Text <> "" Then
   strFac = "SELECT FR39CODFACTCOMPRA " & _
            "  FROM FR3900 " & _
            " WHERE FR39CODFACTCOMPRA = ? "
   Set qryFR39 = objApp.rdoConnect.CreateQuery("", strFac)
   qryFR39(0) = txtText1(6).Text
   Set rstFac = qryFR39.OpenResultset()
   If Not rstFac.EOF Then
   'If tlbToolbar1.Buttons(4).Enabled = True Then
   ' If grdDBGrid1(1).Columns(5).Value = "" And grdDBGrid1(1).Columns(14).Value = "" And grdDBGrid1(1).Columns(15).Value = "" And grdDBGrid1(1).Columns(18).Value = "" Then
   ' Else
   '   Select Case MsgBox(" �Desea salvar los cambios realizados? ", vbYesNoCancel + vbInformation, "Introducir Factura Compra")
   '   Case vbYes
   '     If grdDBGrid1(1).Columns(5).Value = "" Then
   '         strcamposoblig = " El campo C�digo Producto es obligatorio "
   '     End If
   '     If grdDBGrid1(1).Columns(14).Value = "" Then
   '         strcamposoblig = strcamposoblig & Chr(13) & " El campo Cantidad es obligatorio "
   '     End If
   '     If grdDBGrid1(1).Columns(15).Value = "" Then
   '         strcamposoblig = strcamposoblig & Chr(13) & " El campo Precio Unitario es obligatorio "
   '     End If
   '     If grdDBGrid1(1).Columns(18).Value = "" Then
   '         strcamposoblig = strcamposoblig & Chr(13) & " El campo Moneda es obligatorio "
   '     End If
   '     If strcamposoblig <> "" Then
   '         Call MsgBox(strcamposoblig, vbInformation, "Aviso")
   '         cmdmaterial.Enabled = True
   '         Exit Sub
   '     End If
   '     Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
   '   Case vbNo
   '     objWinInfo.objWinActiveForm.blnChanged = False
   '   Case vbCancel
   '     cmdmaterial.Enabled = True
   '     Exit Sub
   '   End Select
   '  Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   ' End If
  'Else
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  'End If
    gintFR501Prov = Me.txtText1(2)
    gstrLlamador = "FrmIntFacCom"
    Call objsecurity.LaunchProcess("FR0503")
    If gintProdBuscado <> "" Then
        stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set qryFR73 = objApp.rdoConnect.CreateQuery("", stra)
        qryFR73(0) = gintProdBuscado
        Set rsta = qryFR73.OpenResultset()
        If Not rsta.EOF Then
            grdDBGrid1(1).Columns(5).Value = gintProdBuscado  'c�d.producto
            grdDBGrid1(1).Columns(6).Value = rsta.rdoColumns("FR73CODINTFAR").Value
            grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73CODINTFARSEG").Value
            grdDBGrid1(1).Columns(8).Value = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
                grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
                grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73VOLUMEN").Value) Then
                grdDBGrid1(1).Columns(12).Value = rsta.rdoColumns("FR73VOLUMEN").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
                grdDBGrid1(1).Columns(13).Value = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
        End If
        qryFR73.Close
        Set qryFR73 = Nothing
        Set rsta = Nothing
        End If
        
    Else
        Call MsgBox("La Factura no est� guardada.", vbInformation, "Aviso")
    End If
    qryFR39.Close
    Set qryFR39 = Nothing
    Set rstFac = Nothing
  End If

  cmdMaterial.Enabled = True
End Sub

Private Sub cmdmedicamento_Click()
  Dim rsta As rdoResultset
  Dim stra As String
  Dim strcamposoblig As String
  Dim strFac As String
  Dim rstFac As rdoResultset
  Dim qryFR39 As rdoQuery
  Dim qryFR73 As rdoQuery
  
  Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)

  cmdMedicamento.Enabled = False

  If txtText1(6).Text <> "" Then
   strFac = "SELECT FR39CODFACTCOMPRA FROM FR3900 WHERE FR39CODFACTCOMPRA = ? "
   Set qryFR39 = objApp.rdoConnect.CreateQuery("", strFac)
   qryFR39(0) = txtText1(6).Text
   Set rstFac = qryFR39.OpenResultset()
   If Not rstFac.EOF Then
   'If tlbToolbar1.Buttons(4).Enabled = True Then
   ' If grdDBGrid1(1).Columns(5).Value = "" And grdDBGrid1(1).Columns(14).Value = "" And grdDBGrid1(1).Columns(15).Value = "" And grdDBGrid1(1).Columns(18).Value = "" Then
   ' Else
   '   Select Case MsgBox(" �Desea salvar los cambios realizados? ", vbYesNoCancel + vbInformation, "Introducir Factura Compra")
   '   Case vbYes
   '     If grdDBGrid1(1).Columns(5).Value = "" Then
   '         strcamposoblig = " El campo C�digo Producto es obligatorio "
   '     End If
   '     If grdDBGrid1(1).Columns(14).Value = "" Then
   '         strcamposoblig = strcamposoblig & Chr(13) & " El campo Cantidad es obligatorio "
   '     End If
   '     If grdDBGrid1(1).Columns(15).Value = "" Then
   '         strcamposoblig = strcamposoblig & Chr(13) & " El campo Precio Unitario es obligatorio "
   '     End If
   '     If grdDBGrid1(1).Columns(18).Value = "" Then
   '         strcamposoblig = strcamposoblig & Chr(13) & " El campo Moneda es obligatorio "
   '     End If
   '     If strcamposoblig <> "" Then
   '         Call MsgBox(strcamposoblig, vbInformation, "Aviso")
   '         cmdmedicamento.Enabled = True
   '         Exit Sub
   '     End If
   '     Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
   '   Case vbNo
   '     objWinInfo.objWinActiveForm.blnChanged = False
   '   Case vbCancel
   '     cmdmedicamento.Enabled = True
   '     Exit Sub
   '   End Select
   '   Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   ' End If
  'Else
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  'End If
    gintFR501Prov = Me.txtText1(2)
    gstrLlamador = "FrmIntFacCom"
    Call objsecurity.LaunchProcess("FR0502")
    If gintProdBuscado <> "" Then
        stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ? "
        Set qryFR73 = objApp.rdoConnect.CreateQuery("", stra)
        qryFR73(0) = gintProdBuscado
        Set rsta = qryFR73.OpenResultset()
        If Not rsta.EOF Then
            grdDBGrid1(1).Columns(5).Value = gintProdBuscado  'c�d.producto
            grdDBGrid1(1).Columns(6).Value = rsta.rdoColumns("FR73CODINTFAR").Value
            grdDBGrid1(1).Columns(7).Value = rsta.rdoColumns("FR73CODINTFARSEG").Value
            grdDBGrid1(1).Columns(8).Value = rsta.rdoColumns("FR73DESPRODUCTOPROV").Value
            If Not IsNull(rsta.rdoColumns("FRH7CODFORMFAR").Value) Then
                grdDBGrid1(1).Columns(9).Value = rsta.rdoColumns("FRH7CODFORMFAR").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73DOSIS").Value) Then
                grdDBGrid1(1).Columns(10).Value = rsta.rdoColumns("FR73DOSIS").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR93CODUNIMEDIDA").Value) Then
                grdDBGrid1(1).Columns(11).Value = rsta.rdoColumns("FR93CODUNIMEDIDA").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73VOLUMEN").Value) Then
                grdDBGrid1(1).Columns(12).Value = rsta.rdoColumns("FR73VOLUMEN").Value
            End If
            If Not IsNull(rsta.rdoColumns("FR73REFERENCIA").Value) Then
                grdDBGrid1(1).Columns(13).Value = rsta.rdoColumns("FR73REFERENCIA").Value
            End If
        End If
        qryFR73.Close
        Set qryFR73 = Nothing
        Set rsta = Nothing
        End If
        
    Else
        Call MsgBox("La Factura no est� guardada.", vbInformation, "Aviso")
    End If
    qryFR39.Close
    Set qryFR39 = Nothing
    Set rstFac = Nothing
  End If

  cmdMedicamento.Enabled = True
End Sub

Private Sub cmdVer_Click()
  If chkCheck1(1).Value = 1 Then
    MsgBox "La factura est� aprobada y no es modificable", vbInformation, "Factura Aprobada"
    Exit Sub
  End If
   Me.Enabled = False
   cmdVer.Enabled = False
   If Len(Trim(txtText1(6).Text)) > 0 Then
      gstrLlamador = "FrmRedGesCom-cmdVer"
      gstrCodAlb = grdDBGrid1(2).Columns(4).Value
      Call objsecurity.LaunchProcess("FR0538") ' FrmBusAlb -> Buscar Albaranes
   End If
   Me.Enabled = True
   cmdVer.Enabled = True
End Sub


Private Sub Form_Initialize()
  mdblImporteTotal = 0
  If gstrLlamadorFac = "CA" Then
    lblLabel1(21).Visible = True
    txtvencimiento.Visible = True
  Else
    lblLabel1(21).Visible = False
    txtvencimiento.Visible = False
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim strKey As String
  Dim strFec As String
  Dim rstFec As rdoResultset
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  
    
   With objMasterInfo
   .strName = "Factura"
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Factura"
    .strTable = "FR3900"
    
    If gstrLlamadorFac = "CA" Then
      .strWhere = "(FR39INDAPROBADA <> -1) "
    Else
      .strWhere = ""
      .intAllowance = cwAllowDelete
      
    End If
    
    Call .FormAddOrderField("FR39CODFACTCOMPRA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Facturas")
    Call .FormAddFilterWhere(strKey, "FR39CODFACTCOMPRA", "C�digo Factura", cwString)
    Call .FormAddFilterWhere(strKey, "FR39FECFACTCOMPRA", "Fecha Factura", cwDate)
    Call .FormAddFilterWhere(strKey, "FR39FECULTVENCIM", "�ltimo Vencimiento", cwDate)
    Call .FormAddFilterWhere(strKey, "FR39FECCONF", "Fecha Conformidad", cwDate)
    Call .FormAddFilterWhere(strKey, "FR39IMPORTE", "Redondeo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR39BASEIMPON", "Base Imponible", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�d.Proveedor", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR39INDAPROBADA", "Aprobada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR39INDACEPTADA", "Aceptada?", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR39INDRECLAMADA", "Reclamada?", cwBoolean)

  End With
  
  With objMultiInfo
    .strName = "Productos"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "FR1500"
    '.intAllowance = cwAllowReadOnly
    .strName = "Productos"
    .intCursorSize = 100
    If gstrLlamadorFac <> "CA" Then
      .intAllowance = cwAllowDelete
    Else
      .intAllowance = cwAllowDelete
    End If
            
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR39CODFACTCOMPRA", txtText1(6))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR15CANTIDAD", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR15PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR15TPDESCUENTO", "% Descuento", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR15IMPORTE", "Importe", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FRH8MONEDA", "Moneda", cwString)
    
  End With
  
  With objMultiInfo1
    .strName = "Albaranes"
    Set .objFormContainer = fraFrame1(2)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(2)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strTable = "FRJ400"
    
    .strName = "Albaranes"
    
    If gstrLlamadorFac <> "CA" Then
      .intAllowance = cwAllowDelete
    End If
            
    Call .FormAddOrderField("FRJ1CODALBARAN", cwAscending)
    Call .FormAddRelation("FR39CODFACTCOMPRA", txtText1(6))
    
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Albaranes")
    Call .FormAddFilterWhere(strKey, "FRJ1CODALBARAN", "C�digo Albar�n", cwString)
  End With
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d.Factura", "FR39CODFACTCOMPRA", cwString, 25)
    Call .GridAddColumn(objMultiInfo, "L�nea", "FR15CODDETFACTCOMP", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Interno", "", cwString, 6)
    Call .GridAddColumn(objMultiInfo, "DC", "", cwNumeric, 1)
    Call .GridAddColumn(objMultiInfo, "Descripci�n Producto", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "F.F", "", cwString, 3)
    Call .GridAddColumn(objMultiInfo, "Dosis", "", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "U.M", "", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Vol(mL)", "", cwDecimal, 6)
    Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
    Call .GridAddColumn(objMultiInfo, "Cantidad", "FR15CANTIDAD", cwDecimal, 20)
    Call .GridAddColumn(objMultiInfo, "Precio Unidad", "FR15PRECIOUNIDAD", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "%Dto", "FR15TPDESCUENTO", cwDecimal, 5)
    Call .GridAddColumn(objMultiInfo, "Importe", "FR15IMPORTE", cwDecimal, 9)
    Call .GridAddColumn(objMultiInfo, "Moneda", "FRH8MONEDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "Albar�n", "FRJ1CODALBARAN", cwString, 9)
    Call .GridAddColumn(objMultiInfo, "Linea Albar�n", "FRJ3NUMLINEA", cwNumeric, 9)
    
    Call .GridAddColumn(objMultiInfo1, "C�d.Factura", "FR39CODFACTCOMPRA", cwString, 25)
    Call .GridAddColumn(objMultiInfo1, "Albar�n", "FRJ1CODALBARAN", cwString, 9)
   
    
    Call .FormCreateInfo(objMasterInfo)
    

    Call .FormChangeColor(objMultiInfo)
    Call .FormChangeColor(objMultiInfo1)
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73CODINTFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(7), "FR73CODINTFARSEG")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(8), "FR73DESPRODUCTOPROV")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(9), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(10), "FR73DOSIS")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(11), "FR93CODUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(12), "FR73VOLUMEN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(13), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(5), "FR79PROVEEDOR")
    
    'facturas
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnReadOnly = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(0)).blnInFind = True
    .CtrlGetInfo(chkCheck1(1)).blnInFind = True
    .CtrlGetInfo(chkCheck1(2)).blnInFind = True
    .CtrlGetInfo(chkCheck1(3)).blnInFind = True
    'productos
    .CtrlGetInfo(grdDBGrid1(1).Columns(3)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(4)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(14)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(15)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(16)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnInFind = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnInFind = True
    
    
    .CtrlGetInfo(txtText1(2)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(1).Columns(5)).blnForeign = True  'c�d.producto
    .CtrlGetInfo(grdDBGrid1(1).Columns(18)).blnForeign = True  'moneda
    
    .CtrlGetInfo(grdDBGrid1(1).Columns(17)).blnReadOnly = True  'importe
    .CtrlGetInfo(dtcDateCombo1(2)).blnReadOnly = True
    
    .CtrlGetInfo(txtvencimiento).blnNegotiated = False
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
  grdDBGrid1(1).Columns(3).Visible = False 'Estado de la fila del grid
  grdDBGrid1(1).Columns(4).Visible = False 'N� de l�nea
  grdDBGrid1(1).Columns(5).Visible = False 'C�digo Producto
  grdDBGrid1(1).Columns(6).Width = 700 'C�d. Int. Farmacia
  grdDBGrid1(1).Columns(7).Width = 400 'D�gito de control
  grdDBGrid1(1).Columns(8).Width = 2500 'Descripci�n del producto
  grdDBGrid1(1).Columns(9).Width = 450 'F.F.
  grdDBGrid1(1).Columns(10).Width = 600 'Dosis
  grdDBGrid1(1).Columns(11).Width = 600 'U.M.
  grdDBGrid1(1).Columns(12).Width = 700 'Volumen
  grdDBGrid1(1).Columns(13).Width = 1000 'Referencia
  grdDBGrid1(1).Columns(14).Width = 900 'Cantidad
  grdDBGrid1(1).Columns(15).Width = 900 'Precio Unidad
  grdDBGrid1(1).Columns(16).Width = 550 '%Descuento
  grdDBGrid1(1).Columns(17).Width = 900 'Importe
  grdDBGrid1(1).Columns(18).Visible = False 'Moneda
  
  grdDBGrid1(2).Columns(3).Visible = False
  
    
  'NUEVO
  If gstrLlamadorFac = "CA" Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
    strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY')  FROM DUAL)"
    Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
    Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
    Call objWinInfo.CtrlSet(txtText1(1), 0)
    rstFec.Close
    Set rstFec = Nothing
    Call objWinInfo.CtrlSet(txtText1(2), gstrCodProv)
    txtvencimiento = intDiasVenc(gstrCodProv)
  End If

End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch

  If strCtrl = "txtText1(2)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     
     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(2), .cllValues("FR79CODPROVEEDOR"))
      txtvencimiento = intDiasVenc(.cllValues("FR79CODPROVEEDOR"))
     End If
   End With
   Set objSearch = Nothing
  End If

  If strCtrl = "grdDBGrid1(1).C�d.Producto" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�digo"
     
     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�d.Interno"
     
     Set objField = .AddField("FR73DESPRODUCTOPROV")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(5), .cllValues("FR73CODPRODUCTO"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  If strCtrl = "grdDBGrid1(1).Moneda" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH800"
     
     Set objField = .AddField("FRH8MONEDA")
     objField.strSmallDesc = "Moneda"
     
     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(18), .cllValues("FRH8MONEDA"))
     End If
   End With
   Set objSearch = Nothing
  End If
  
  
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
  Dim strFac As String
  Dim qryFac As rdoQuery
  Dim rstFac As rdoResultset
  Dim strupdate As String
  Dim qryUpd As rdoQuery
  
  If (strFormName = "Productos") And (mblnBorrar = True) And (Len(Trim(txtText1(6).Text)) > 0) Then
    'Hay que recalcular el importe total de la factura
    strFac = "SELECT SUM(FR15IMPORTE) " & _
             "  FROM FR1500 " & _
             " WHERE FR39CODFACTCOMPRA = ? "
    Set qryFac = objApp.rdoConnect.CreateQuery("", strFac)
    qryFac(0) = txtText1(6).Text
    Set rstFac = qryFac.OpenResultset()
    If Not rstFac.EOF Then
      mdblImporteTotal = rstFac.rdoColumns(0).Value
      strupdate = "UPDATE FR3900 " & _
                  " SET FR39IMPORTE = ? ,FR39REDONDEO = ? " & _
                  " WHERE FR39CODFACTCOMPRA = ? "
      Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
      qryUpd(0) = mdblImporteTotal
      qryUpd(1) = mdblImporteTotal
      qryUpd(2) = txtText1(6).Text
      qryUpd.Execute
      qryUpd.Close
      Set qryUpd = Nothing
    End If
    qryFac.Close
    Set qryFac = Nothing
    Set rstFac = Nothing
    mblnBorrar = False
    'Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    'objWinInfo.DataRefresh
  End If

End Sub



Private Sub objWinInfo_cwPostDelete(ByVal strFormName As String, ByVal blnError As Boolean)
  Dim rsta As rdoResultset
  Dim stra As String
  Dim strdelete As String
  Dim strFac As String
  Dim qryFac As rdoQuery
  Dim rstFac As rdoResultset
  Dim strupdate As String
  Dim qryDel As rdoQuery
  Dim qryUpd As rdoQuery
  
  If strFormName = "Albaranes" Then
    If grdDBGrid1(2).Columns(0).Value = "Eliminado" Then
        strdelete = "DELETE FR1500 " & _
                   " WHERE FR39CODFACTCOMPRA = ? " & _
                   "   AND FRJ1CODALBARAN = ? "
        Set qryDel = objApp.rdoConnect.CreateQuery("", strdelete)
        qryDel(0) = txtText1(6).Text
        qryDel(1) = grdDBGrid1(2).Columns(4).Value
        qryDel.Execute
        qryDel.Close
        Set qryDel = Nothing
        strFac = "SELECT SUM(FR15IMPORTE) " & _
                 "  FROM FR1500 " & _
                 " WHERE FR39CODFACTCOMPRA = ? "
        Set qryFac = objApp.rdoConnect.CreateQuery("", strFac)
        qryFac(0) = txtText1(6).Text
        Set rstFac = qryFac.OpenResultset()
        If Not rstFac.EOF Then
          If IsNull(rstFac.rdoColumns(0).Value) Then
            mdblImporteTotal = 0
          Else
            mdblImporteTotal = rstFac.rdoColumns(0).Value
          End If
          strupdate = "UPDATE FR3900 " & _
                      " SET FR39IMPORTE = ? , FR39REDONDEO = ? " & _
                      " WHERE FR39CODFACTCOMPRA = ? "
          Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
          qryUpd(0) = mdblImporteTotal
          qryUpd(1) = mdblImporteTotal
          qryUpd(2) = txtText1(6).Text
          qryUpd.Execute
          qryUpd.Close
          Set qryUpd = Nothing
        End If
        qryFac.Close
        Set qryFac = Nothing
        Set rstFac = Nothing
    End If
    mblnBorrar = False
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.DataRefresh
  End If
  If strFormName = "Factura" Then
    mblnBorrar = False
  End If
        
End Sub



Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
  
 If chkCheck1(1).Value = 1 Then
  ' 'Si la factura est� aprobada no se puede modificar
  '  Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  '  objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
  '  'txtText1(0).Enabled = False
  '  'txtText1(1).Enabled = False
  '  'txtText1(2).Enabled = False
  '  'txtText1(6).Enabled = False
  '  'txtText1(7).Enabled = False
  '  'txtvencimiento.Enabled = False
  '  'chkCheck1(1).Enabled = False
  '  cmdbuscar.Enabled = False
  '  cmdVer.Enabled = False
  '  Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
  '  objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
  '  Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
  '  objWinInfo.objWinActiveForm.intAllowance = cwAllowReadOnly
  'Else
  '  'La factura no est� aprobada
  '  Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
  '  objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
  '  'txtText1(0).Enabled = True
  '  'txtText1(1).Enabled = True
  '  'txtText1(2).Enabled = True
  '  'txtText1(6).Enabled = True
  '  'txtText1(7).Enabled = True
  '  'txtvencimiento.Enabled = True
  '  'chkCheck1(1).Enabled = True
  '  cmdbuscar.Enabled = True
  '  cmdVer.Enabled = True
  '  Call objWinInfo.FormChangeActive(fraframe1(1), False, True)
  '  objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
  '  Call objWinInfo.FormChangeActive(fraframe1(2), False, True)
  '  objWinInfo.objWinActiveForm.intAllowance = cwAllowAll
 End If
End Sub



Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
   Dim rsta As rdoResultset
   Dim stra As String
   Dim strinsert As String
   Dim linea As Long
   Dim strlinea As String
   Dim rstlinea As rdoResultset
   Dim cantpedida As String
   Dim preciounidad As String
   Dim imporlinea As String
   Dim strFacAlb As String
   Dim rstFacAlb As rdoResultset
   Dim strupdate As String
   Dim dblCantPteFac As String
   Dim Q%
   Dim strFac As String
   Dim qryFac As rdoQuery
   Dim rstFac As rdoResultset
   Dim qryUpd As rdoQuery
   Dim qryFR15 As rdoQuery
   Dim qryFRJ3 As rdoQuery
   Dim qryFR39 As rdoQuery
   Dim strFR79 As String
   Dim qryFR79 As rdoQuery
   Dim rstFR79 As rdoResultset
   Dim dblDto As Double
   
  If (strFormName = "Productos") And (Len(Trim(txtText1(6).Text)) > 0) Then
    'Hay que recarcular en importe de la factura
    strFac = "SELECT SUM(FR15IMPORTE) " & _
             "  FROM FR1500 " & _
             " WHERE FR39CODFACTCOMPRA = ? "
    Set qryFac = objApp.rdoConnect.CreateQuery("", strFac)
    qryFac(0) = txtText1(6).Text
    Set rstFac = qryFac.OpenResultset()
    If Not rstFac.EOF Then
      mdblImporteTotal = rstFac.rdoColumns(0).Value
      
      
      strFR79 = "SELECT FR79TPDTO,FR79INDPRONTOPAG FROM FR7900 WHERE FR79CODPROVEEDOR = ? "
      Set qryFR79 = objApp.rdoConnect.CreateQuery("", strFR79)
      qryFR79(0) = txtText1(2).Text
      Set rstFR79 = qryFR79.OpenResultset()
      If Not rstFR79.EOF Then
        If Not IsNull(rstFR79.rdoColumns(0).Value) And Not IsNull(rstFR79.rdoColumns(1).Value) Then
          If rstFR79.rdoColumns(0).Value > 0 And rstFR79.rdoColumns(1).Value = -1 Then
            dblDto = rstFR79.rdoColumns(0).Value
            mdblDPP = (mdblImporteTotal * dblDto / 100)
            mdblImporteTotal = mdblImporteTotal - (mdblImporteTotal * dblDto / 100)
          End If
        End If
      End If
      
      
      strupdate = "UPDATE FR3900 " & _
                  " SET FR39IMPORTE = ? ,FR39REDONDEO = ? " & _
                  " WHERE FR39CODFACTCOMPRA = ? "
      Set qryUpd = objApp.rdoConnect.CreateQuery("", strupdate)
      qryUpd(0) = mdblImporteTotal
      qryUpd(1) = mdblImporteTotal
      qryUpd(2) = txtText1(6).Text
      qryUpd.Execute
      qryUpd.Close
      Set qryUpd = Nothing
    End If
    qryFac.Close
    Set qryFac = Nothing
    Set rstFac = Nothing
    mblnBorrar = False
  End If


   If (strFormName = "Factura") And (Len(Trim(txtText1(6).Text)) > 0) And (gintCodsAlbs > 0) And (mblnInserDetFac = False) Then
      mblnInserDetFac = True
      strlinea = "SELECT MAX(FR15CODDETFACTCOMP) " & _
                    "  FROM FR1500 " & _
                    " WHERE FR39CODFACTCOMPRA = ? "
      Set qryFR15 = objApp.rdoConnect.CreateQuery("", strlinea)
      stra = "SELECT * " & _
                "  FROM FRJ300 " & _
                " WHERE FRJ1CODALBARAN = ? " & _
                " AND FRJ3CANTPTEFACT > ? "
      Set qryFRJ3 = objApp.rdoConnect.CreateQuery("", stra)
      'strupdate = "UPDATE FR3900 " & _
                        " SET FR39IMPORTE = ? , FR39REDONDEO = ? " & _
                        " WHERE FR39CODFACTCOMPRA = ? "
      'Set qryFR39 = objApp.rdoConnect.CreateQuery("", strupdate)
      For Q% = 0 To gintCodsAlbs - 1
         'Se introduce en la tabla FRJ400
         Dim strSQL As String
         Dim qrySql As rdoQuery
         Dim rstSQL As rdoResultset
         
         strSQL = "SELECT COUNT(*) " & _
                  "  FROM FRJ400 " & _
                  " WHERE FRJ1CODALBARAN = ? " & _
                  "   AND FR39CODFACTCOMPRA = ? "
         Set qrySql = objApp.rdoConnect.CreateQuery("", strSQL)
         qrySql(0) = gstrCodsAlbs(Q%)
         qrySql(1) = txtText1(6).Text
         Set rstSQL = qrySql.OpenResultset()
         If rstSQL.rdoColumns(0).Value = 0 Then
            strFacAlb = "INSERT INTO FRJ400 (FRJ1CODALBARAN,FR39CODFACTCOMPRA) " & _
                        " VALUES ('" & gstrCodsAlbs(Q%) & "','" & txtText1(6).Text & "')"
            objApp.rdoConnect.Execute strFacAlb, 64
         End If
         qrySql.Close
         Set qrySql = Nothing
         Set rstSQL = Nothing
         'Se introducen las l�neas de la factura
         qryFR15(0) = txtText1(6).Text
         Set rstlinea = qryFR15.OpenResultset()
         If IsNull(rstlinea.rdoColumns(0).Value) Then
            linea = 1
         Else
            linea = rstlinea.rdoColumns(0).Value + 1
         End If
         Set rstlinea = Nothing
         
         'If grdDBGrid1(2).Columns(0).Value = "A�adido" Then
         qryFRJ3(0) = gstrCodsAlbs(Q%)
         qryFRJ3(1) = 0
         Set rsta = qryFRJ3.OpenResultset()
         While Not rsta.EOF
            'transforma la coma de separaci�n de los decimales por un punto
            dblCantPteFac = rsta.rdoColumns("FRJ3CANTPTEFACT").Value
            dblCantPteFac = objGen.ReplaceStr(dblCantPteFac, ",", ".", 1)
            cantpedida = rsta.rdoColumns("FRJ3CANTENTREG").Value
            cantpedida = objGen.ReplaceStr(cantpedida, ",", ".", 1)
            preciounidad = rsta.rdoColumns("FRJ3PRECIOUNIDAD").Value
            preciounidad = objGen.ReplaceStr(preciounidad, ",", ".", 1)
            imporlinea = rsta.rdoColumns("FRJ3IMPORLINEA").Value
            imporlinea = objGen.ReplaceStr(imporlinea, ".", ",", 1)
            mdblImporteTotal = mdblImporteTotal + CDbl(imporlinea)
            Dim tpdes As String
            If Not IsNull(rsta.rdoColumns("FRJ3DTO").Value) Then
              tpdes = objGen.ReplaceStr(rsta.rdoColumns("FRJ3DTO").Value, ",", ".", 1)
            Else
              tpdes = "0"
            End If
            strFR79 = "SELECT FR79TPDTO,FR79INDPRONTOPAG FROM FR7900 WHERE FR79CODPROVEEDOR = ? "
            Set qryFR79 = objApp.rdoConnect.CreateQuery("", strFR79)
            qryFR79(0) = txtText1(2).Text
            Set rstFR79 = qryFR79.OpenResultset()
            If Not rstFR79.EOF Then
              If Not IsNull(rstFR79.rdoColumns(0).Value) And Not IsNull(rstFR79.rdoColumns(1).Value) Then
                If rstFR79.rdoColumns(0).Value > 0 And rstFR79.rdoColumns(1).Value = -1 Then
                  dblDto = rstFR79.rdoColumns(0).Value
                  'mdblDPP = (mdblImporteTotal * dblDto / 100)
                  'mdblImporteTotal = mdblImporteTotal - (mdblImporteTotal * dblDto / 100)
                  mdblDPP = mdblDPP + (CDbl(imporlinea) * dblDto / 100)
                  mdblImporteTotal = mdblImporteTotal - (CDbl(imporlinea) * dblDto / 100)
                End If
              End If
            End If
               
            strinsert = "INSERT INTO FR1500(FR39CODFACTCOMPRA,FR15CODDETFACTCOMP,FR73CODPRODUCTO," & _
            "FR15CANTIDAD,FR15PRECIOUNIDAD,FR15IMPORTE,FRH8MONEDA,FRJ1CODALBARAN,FRJ3NUMLINEA,FR15TPDESCUENTO) VALUES (" & _
            "'" & txtText1(6).Text & "'" & "," & _
            linea & "," & _
            rsta.rdoColumns("FR73CODPRODUCTO").Value & "," & _
            dblCantPteFac & "," & _
            preciounidad & "," & _
            objGen.ReplaceStr(imporlinea, ",", ".", 1) & "," & _
            "'" & rsta.rdoColumns("FRH8MONEDA").Value & "'," & _
            "'" & rsta.rdoColumns("FRJ1CODALBARAN").Value & "'," & _
            rsta.rdoColumns("FRJ3NUMLINEA").Value & _
            "," & tpdes & ")"
            objApp.rdoConnect.Execute strinsert, 64
            objApp.rdoConnect.Execute "Commit", 64
            linea = linea + 1
            rsta.MoveNext
         Wend
         'rsta.Close
         Set rsta = Nothing
         
         If mdblImporteTotal > 0 And Len(Trim(txtText1(6).Text)) > 0 Then
          strFR79 = "SELECT FR79TPDTO,FR79INDPRONTOPAG FROM FR7900 WHERE FR79CODPROVEEDOR = ? "
          Set qryFR79 = objApp.rdoConnect.CreateQuery("", strFR79)
          qryFR79(0) = txtText1(2).Text
          Set rstFR79 = qryFR79.OpenResultset()
          If Not rstFR79.EOF Then
            If Not IsNull(rstFR79.rdoColumns(0).Value) And Not IsNull(rstFR79.rdoColumns(1).Value) Then
              If rstFR79.rdoColumns(0).Value > 0 And rstFR79.rdoColumns(1).Value = -1 Then
                dblDto = rstFR79.rdoColumns(0).Value
                'mdblDPP = (mdblImporteTotal * dblDto / 100)
                'mdblImporteTotal = mdblImporteTotal - (mdblImporteTotal * dblDto / 100)
              End If
            End If
          End If
   
            'strupdate = "UPDATE FR3900  SET FR39IMPORTE = " &  WHERE FR39CODFACTCOMPRA = ? "
            'Set qryFR39 = objApp.rdoConnect.CreateQuery("", strupdate)
            strupdate = "UPDATE FR3900 " & _
                        " SET FR39IMPORTE = " & objGen.ReplaceStr(mdblImporteTotal, ",", ".", 1) & " , FR39REDONDEO = " & objGen.ReplaceStr(mdblImporteTotal, ",", ".", 1) & _
                        " WHERE FR39CODFACTCOMPRA = ? "
            Set qryFR39 = objApp.rdoConnect.CreateQuery("", strupdate)
            'qryFR39(0) = objGen.ReplaceStr(mdblImporteTotal, ",", ".", 1)
            'qryFR39(1) = objGen.ReplaceStr(mdblImporteTotal, ",", ".", 1)
            qryFR39(0) = txtText1(6).Text
            qryFR39.Execute
            mstrNumFac = txtText1(6).Text
         End If
         
         'End If
      Next Q%
      qryFR15.Close
      Set qryFR15 = Nothing
      qryFRJ3.Close
      Set qryFRJ3 = Nothing
      qryFR39.Close
      Set qryFR39 = Nothing
   End If
   
End Sub




Private Sub objWinInfo_cwPreRead(ByVal strFormName As String)
'  If (Len(Trim(mstrNumFac)) > 0) And (mblnYaRefres = False) Then
'    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
'    objWinInfo.objWinActiveForm.strWhere = "FR01CODALBARAN = '" & mstrNumFac & "'"
'    mblnYaRefres = True
'  End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  Dim strWhere As String
  Dim strOrden As String

  Call objWinInfo.FormPrinterDialog(True, "")
  Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
  intReport = objPrinter.Selected
  If intReport > 0 Then
     'Call Imprimir("FR5292.RPT", 0)
  End If
  Set objPrinter = Nothing
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(Index), False, True)
End Sub

Private Sub tabTab1_DblClick(Index As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(Index), False, True)
End Sub

Private Sub tabTab1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
  Call objWinInfo.FormChangeActive(fraFrame1(Index), False, True)
  If Index = 0 Then
    objWinInfo.DataRefresh
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim max As Long
   Dim i As Long
   Dim strFec As String
   Dim rstFec As rdoResultset
   Dim qryFR15 As rdoQuery
   
   If (objWinInfo.objWinActiveForm.strName = "Factura") And (btnButton.Index = 8) And (Len(Trim(txtText1(6).Text)) > 0) And (gstrLlamadorFac <> "CA") Then
     Dim strFRT3 As String
     strFRT3 = "DELETE FRJ600 WHERE FR39CODFACTCOMPRA = '" & txtText1(6).Text & "'"
     objApp.rdoConnect.Execute strFRT3, 64
   End If
  
   mblnFactApro = True
   If btnButton.Index = 8 Then
     mblnBorrar = True
   End If
   
   Select Case btnButton.Index
   '  2 nuevo
   '  3 abrir
   ' 16 localizar
   ' 18 poner filtro
   ' 19 quitar filtro
   ' 21 primero
   ' 22 anterior
   ' 23 siguiente
   ' 24 ultimo
   ' 30 salir
   ' comprobar que la factura tenga productos, si no tiene -> borrarla
   Case 2, 3, 16, 18, 19, 21, 22, 23, 24, 30:
'      If objWinInfo.objWinActiveForm.strName = "Factura" Then
         If Len(Trim(txtText1(6).Text)) > 0 Then
          If gstrLlamadorFac = "CA" Then
            sqlstr = "Select * " & _
                     "  From fr1500 " & _
                     " Where FR39CODFACTCOMPRA = ? "
            Set qryFR15 = objApp.rdoConnect.CreateQuery("", sqlstr)
            qryFR15(0) = txtText1(6).Text
            Set rsta = qryFR15.OpenResultset()
            If rsta.EOF Then
               Call MsgBox("La factura:  - " & txtText1(6).Text & " -  no tiene ning�n producto, debe borrarla o a�adir productos", vbExclamation, "Aviso")
               qryFR15.Close
               Set qryFR15 = Nothing
               Set rsta = Nothing
               Exit Sub
            Else
               qryFR15.Close
               Set qryFR15 = Nothing
               Set rsta = Nothing
            End If
           End If
         End If
'      End If
   End Select


   If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Factura" Then
      Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
      
      strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
      Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
      Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
      Call objWinInfo.CtrlSet(txtText1(1), 0)
      rstFec.Close
      Set rstFec = Nothing
      Call objWinInfo.CtrlSet(txtText1(2), gstrCodProv)
      txtvencimiento = intDiasVenc(gstrCodProv)

      
      
      'sqlstr = "SELECT FR39CODFACTCOMPRA_SEQUENCE.nextval FROM dual"
      'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      'txtText1(6).Text = rsta.rdoColumns(0).Value
      'SendKeys ("{TAB}")
      'rsta.Close
      'Set rsta = Nothing
   Else
      If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Productos" Then
         ' No se puede hacer nuevo
         Call MsgBox("Para introducir productos en la factura pulse el boron 'Buscar Albaran'", vbInformation, "Aviso")
         cmdbuscar.SetFocus
      
'        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
'        Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(3), txtText1(6).Text)
'        'se introduce el n�mero de l�nea
'        'primero se mira el mayor n� de l�nea en la tabla
'        sqlstr = "SELECT MAX(FR15CODDETFACTCOMP) FROM FR1500 " & _
'                 "WHERE FR39CODFACTCOMPRA=" & "'" & txtText1(6).Text & "'"
'        Set RSTA = objApp.rdoConnect.OpenResultset(sqlstr)
'        If IsNull(RSTA.rdoColumns(0).Value) Then
'            max = 0
'        Else
'            max = RSTA.rdoColumns(0).Value
'        End If
'        'luego se mira en el grid de pantalla pues puede haber productos en el grid a�n no guardados
'        If grdDBGrid1(1).Rows = 0 Then
'            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), 1)
'        Else
'            grdDBGrid1(1).MoveFirst
'            'If grdDBGrid1(1).Columns(4).Value = "" Then
'            '    max = 0
'            'Else
'            '    max = grdDBGrid1(1).Columns(4).Value
'            'End If
'            For i = 0 To grdDBGrid1(1).Rows - 1
'             If grdDBGrid1(1).Columns(4).Value <> "" Then
'                If max < grdDBGrid1(1).Columns(4).Value Then
'                    max = grdDBGrid1(1).Columns(4).Value
'                End If
'             End If
'            grdDBGrid1(1).MoveNext
'            Next i
'            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), max + 1)
'        End If
'        SendKeys ("{TAB}")
'        RSTA.Close
'        Set RSTA = Nothing
      Else
         If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Albaranes" Then
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
            Call objWinInfo.CtrlSet(grdDBGrid1(2).Columns(3), txtText1(6).Text)
         Else
            Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
         End If
      End If
   End If
   If btnButton.Index = 8 Then
    Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
    objWinInfo.DataRefresh
   End If
   
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim max As Long
   Dim i As Long
   Dim strFec As String
   Dim rstFec As rdoResultset
   Dim qryFR15 As rdoQuery

   Select Case intIndex
   '  10 nuevo
   '  20 abrir
   ' 100 salir
   ' comprobar que la factura tenga productos, si no tiene -> borrarla
   Case 10, 20, 100:
      If objWinInfo.objWinActiveForm.strName = "Factura" Then
         If Len(Trim(txtText1(6).Text)) > 0 Then
            sqlstr = "Select * " & _
                     "  From fr1500 " & _
                     " Where FR39CODFACTCOMPRA = ? "
            Set qryFR15 = objApp.rdoConnect.CreateQuery("", sqlstr)
            qryFR15(0) = txtText1(6).Text
            Set rsta = qryFR15.OpenResultset()
            If rsta.EOF Then
               Call MsgBox("La factura:  - " & txtText1(6).Text & " -  no tiene ning�n producto, debe borrarla o a�adir productos", vbExclamation, "Aviso")
               Exit Sub
            End If
            qryFR15.Close
            Set qryFR15 = Nothing
            Set rsta = Nothing
         End If
      End If
   End Select


   If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Factura" Then
      Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
      
      strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
      Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
      Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
      Call objWinInfo.CtrlSet(txtText1(1), 0)
      rstFec.Close
      Set rstFec = Nothing
      Call objWinInfo.CtrlSet(txtText1(2), gstrCodProv)
      txtvencimiento = intDiasVenc(gstrCodProv)

      
      
      'sqlstr = "SELECT FR39CODFACTCOMPRA_SEQUENCE.nextval FROM dual"
      'Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      'txtText1(6).Text = rsta.rdoColumns(0).Value
      'SendKeys ("{TAB}")
      'rsta.Close
      'Set rsta = Nothing
   Else
      If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Productos" Then
         ' No se puede hacer nuevo
         Call MsgBox("Para introducir productos en la factura pulse el bot�n 'Buscar Albaran'", vbInformation, "Aviso")
         cmdbuscar.SetFocus
    
'         Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
'         grdDBGrid1(1).Columns(3).Value = txtText1(6).Text
'         'se introduce el n�mero de l�nea
'         sqlstr = "SELECT MAX(FR15CODDETFACTCOMP) FROM FR1500 " & _
'                  "WHERE FR39CODFACTCOMPRA=" & "'" & txtText1(6).Text & "'"
'         Set RSTA = objApp.rdoConnect.OpenResultset(sqlstr)
'         If IsNull(RSTA.rdoColumns(0).Value) Then
'            max = 0
'         Else
'            max = RSTA.rdoColumns(0).Value
'         End If
'         'se introduce el n�mero de l�nea
'         If grdDBGrid1(1).Rows = 0 Then
'            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), 1)
'         Else
'            grdDBGrid1(1).MoveFirst
'            'If grdDBGrid1(1).Columns(4).Value = "" Then
'            '    max = 0
'            'Else
'            '    max = grdDBGrid1(1).Columns(4).Value
'            'End If
'            For i = 0 To grdDBGrid1(1).Rows - 1
'               If grdDBGrid1(1).Columns(4).Value <> "" Then
'                  If max < grdDBGrid1(1).Columns(4).Value Then
'                     max = grdDBGrid1(1).Columns(4).Value
'                  End If
'               End If
'               grdDBGrid1(1).MoveNext
'            Next i
'            Call objWinInfo.CtrlSet(grdDBGrid1(1).Columns(4), max + 1)
'         End If
'         SendKeys ("{TAB}")
'         RSTA.Close
'         Set RSTA = Nothing
      Else
         If intIndex = 10 And objWinInfo.objWinActiveForm.strName = "Albaranes" Then
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
            grdDBGrid1(2).Columns(3).Value = txtText1(6).Text
         Else
            Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
         End If
      End If
   End If

End Sub
Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim qryFR15 As rdoQuery
   
   Select Case intIndex
   ' 10 poner filtro
   ' 20 quitar filtro
   ' comprobar que la factura tenga productos, si no tiene -> borrarla
   Case 10, 20:
      If objWinInfo.objWinActiveForm.strName = "Factura" Then
         If Len(Trim(txtText1(6).Text)) > 0 Then
            sqlstr = "Select * " & _
                     "  From fr1500 " & _
                     " Where FR39CODFACTCOMPRA = ?"
            Set qryFR15 = objApp.rdoConnect.CreateQuery("", sqlstr)
            qryFR15(0) = txtText1(6).Text
            Set rsta = qryFR15.OpenResultset()
            If rsta.EOF Then
               Call MsgBox("La factura:  - " & txtText1(6).Text & " -  no tiene ning�n producto, debe borrarla o a�adir productos", vbExclamation, "Aviso")
               Exit Sub
            End If
            qryFR15.Close
            Set qryFR15 = Nothing
            Set rsta = Nothing
         End If
      End If
   End Select
  
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim qryFR15 As rdoQuery
   
   Select Case intIndex
   ' 10 localizar
   ' 40 primero
   ' 50 anterior
   ' 60 siguiente
   ' 70 ultimo
   ' comprobar que la factura tenga productos, si no tiene -> borrarla
   Case 10, 40, 50, 60, 70:
      If objWinInfo.objWinActiveForm.strName = "Factura" Then
         If Len(Trim(txtText1(6).Text)) > 0 Then
            sqlstr = "Select * " & _
                     "  From fr1500 " & _
                     " Where FR39CODFACTCOMPRA = ? "
            Set qryFR15 = objApp.rdoConnect.createquey("", sqlstr)
            qryFR15(0) = txtText1(6).Text
            Set rsta = qryFR15.OpenResultset()
            If rsta.EOF Then
               Call MsgBox("La factura:  - " & txtText1(6).Text & " -  no tiene ning�n producto, debe borrarla o a�adir productos", vbExclamation, "Aviso")
               Exit Sub
            End If
            qryFR15.Close
            Set qryFR15 = Nothing
            Set rsta = Nothing
         End If
      End If
   End Select
    
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  
  Call objWinInfo.CtrlDataChange
  If intIndex = 1 Then
    If IsNumeric(grdDBGrid1(1).Columns(14).Value) And IsNumeric(grdDBGrid1(1).Columns(15).Value) Then
       If IsNumeric(grdDBGrid1(1).Columns(16).Value) Then
            grdDBGrid1(1).Columns(17).Value = grdDBGrid1(1).Columns(14).Value * grdDBGrid1(1).Columns(15).Value * _
                                             (100 - (1 * grdDBGrid1(1).Columns(16).Value)) / 100
       Else
            grdDBGrid1(1).Columns(17).Value = grdDBGrid1(1).Columns(14).Value * grdDBGrid1(1).Columns(15).Value
       End If
    End If
  End If
      
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
   Call objWinInfo.CtrlLostFocus
   If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) < 0 Then
         Call MsgBox("La fecha de Conformidad ha de ser mayor que la fecha de Factura", vbInformation, "Aviso")
         dtcDateCombo1(intIndex).Text = ""
      End If
   End If
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  
   Call objWinInfo.CtrlDataChange
   If dtcDateCombo1(1).Text <> "" And dtcDateCombo1(0).Text <> "" Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) < 0 Then
         Call MsgBox("La fecha de Conformidad ha de ser mayor que la fecha de Factura", vbInformation, "Aviso")
         dtcDateCombo1(intIndex).Text = ""
      End If
   End If
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  mblnFactApro = False
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Dim strDF As String
  Dim qryDF As rdoQuery
  Dim rstDF As rdoResultset
  Dim strDA As String
  Dim qryDA As rdoQuery
  Dim rstDA As rdoResultset
  Dim strSQL As String
  Dim qrySql As rdoQuery
  Dim rstSQL As rdoResultset
  Dim strCA As String
  Dim qryCA As rdoQuery
  Dim strNumFac As String
  Dim curDif As Currency
  Dim stra As String
  Dim qryA As rdoQuery
  Dim rsta As rdoResultset
    
    
  
  curDif = 0
  If IsNumeric(txtText1(0).Text) And IsNumeric(txtText1(7).Text) Then
    curDif = Format(txtText1(7).Text - txtText1(0).Text, "0.00")
  End If
  Call objWinInfo.CtrlDataChange
  
  If gstrLlamadorFac <> "CA" Then
    Exit Sub
  End If
  Me.Enabled = False
  Screen.MousePointer = vbHourglass
  If Len(Trim(txtText1(6).Text)) > 0 Then
    mstrCodFac = txtText1(6).Text
  Else
    mstrCodFac = ""
  End If
  If IsNumeric(txtText1(7).Text) Then
    mcurImp = txtText1(7).Text
  Else
    mcurImp = 0
  End If
  
  If IsNumeric(txtText1(0).Text) Then
    mcurRed = 0 'txtText1(0).Text
  Else
    mcurRed = 0
  End If
  
  If Len(Trim(dtcDateCombo1(2).Text)) > 0 Then
    mstrFV = dtcDateCombo1(2).Text
  Else
    If Len(Trim(mstrFV)) = 0 Then
      mstrFV = "01/01/1955"
    End If
  End If
  If IsNumeric(txtText1(2).Text) Then
    mintCodProv = txtText1(2).Text
  Else
    mintCodProv = 0
  End If
  If Len(Trim(txtText1(5).Text)) > 0 Then
    mstrProv = txtText1(5).Text
  Else
    mstrProv = ""
  End If

  
  If (Len(Trim(txtText1(6).Text)) > 0) And (intIndex = 1) And (chkCheck1(1).Value = vbChecked) And (mblnFactApro = False) Then
    strDF = "SELECT * FROM FR1500 WHERE FR39CODFACTCOMPRA = ?"
    Set qryDF = objApp.rdoConnect.CreateQuery("", strDF)
    qryDF(0) = txtText1(6).Text
    gstrNumFac = txtText1(6).Text
    Set rstDF = qryDF.OpenResultset()
    If rstDF.EOF Then
      'La factura no tiene productos
      MsgBox "No se puede aprobar una factura sin productos", vbInformation, "Aprobar factura"
      chkCheck1(1).Value = vbUnchecked
      Me.Enabled = True
      Screen.MousePointer = vbDefault
    Else
      If MsgBox("�Est� seguro de aprobar la factura?, si se aprueba la factura no podr� ser modificada ni borrada", vbYesNo, "Aviso") = vbNo Then
        'El usuario no quiere aprobar la factura
        chkCheck1(1).Value = vbUnchecked
        Me.Enabled = True
        Screen.MousePointer = vbDefault
      Else
        Screen.MousePointer = vbHourglass
        strDate = dtcDateCombo1(1).Text
        strNF = txtText1(6).Text
  
        Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
        objWinInfo.DataSave
        objWinInfo.DataRefresh
        Dim curS As Currency
        Dim asi As Integer
        DoEvents
        For curS = 0 To 10000000
          asi = 1
        Next curS
        
        'El usuario quiere aprobar la factura y esta tiene productos
        'Se actualizan las cantidades pendientes de facturar en FRJ300 detalle albar�n
        While Not rstDF.EOF
          'strDA = "UPDATE FRJ300 " & _
          '        "SET FRJ3CANTPTEFACT = FRJ3CANTPTEFACT - ? " & _
          '        " WHERE FRJ1CODALBARAN = ? " & _
          '        "   AND FRJ3NUMLINEA = ? "
          'Set qryDA = objApp.rdoConnect.CreateQuery("", strDA)
          'qryDA(0) = rstDF.rdoColumns("FR15CANTIDAD").Value
          'qryDA(1) = rstDF.rdoColumns("FRJ1CODALBARAN").Value
          'qryDA(2) = rstDF.rdoColumns("FRJ3NUMLINEA").Value
          'qryDA.Execute
          '�****
          strDA = "UPDATE FRJ300 " & _
                  "SET FRJ3CANTPTEFACT = FRJ3CANTPTEFACT - " & 2 * rstDF.rdoColumns("FR15CANTIDAD").Value & _
                  " WHERE FRJ1CODALBARAN = '" & rstDF.rdoColumns("FRJ1CODALBARAN").Value & _
                  "'   AND FRJ3NUMLINEA =  " & rstDF.rdoColumns("FRJ3NUMLINEA").Value
          objApp.rdoConnect.Execute strDA, 64
          'Se comprueba si el albar�n est� cerrado
          strSQL = "SELECT * FROM FRJ300 WHERE FRJ1CODALBARAN = ? AND FRJ3CANTPTEFACT > ?"
          Set qrySql = objApp.rdoConnect.CreateQuery("", strSQL)
          qrySql(0) = rstDF.rdoColumns("FRJ1CODALBARAN").Value
          qrySql(1) = 0
          Set rstSQL = qrySql.OpenResultset()
          If rstSQL.EOF Then
            'Todas las l�neas del albar�n han sido totalmente facturadas
             strCA = "UPDATE FRJ100 SET FRJ1ESTADO = ? WHERE FRJ1CODALBARAN = ? "
             Set qryCA = objApp.rdoConnect.CreateQuery("", strCA)
             qryCA(0) = 1
             qryCA(1) = rstDF.rdoColumns("FRJ1CODALBARAN").Value
             qryCA.Execute
          End If
          
          gblnModFac = True
          gstrNumFac = strNumFac
          qrySql.Close
          Set qrySql = Nothing
          Set rstSQL = Nothing
          rstDF.MoveNext
        Wend
        'qryDA.Close
        rstDF.Close
        Set rstDF = Nothing
        mdifR = 0
        If curDif <> 0 Then
          gintF = gintF + 1
          gstrLF(gintF, 1) = strNF
          gstrLF(gintF, 2) = curDif
          mdifR = curDif
        End If
        
        'La factura est� aprobada:
        'Hay que crear el asiento contable en FRT100, FRT200 y FRT300
        Call Crear_Asientos
        Me.Enabled = True
        Screen.MousePointer = vbDefault
      End If
    End If
         
  End If
  Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
  'objWinInfo.DataSave
  'objWinInfo.DataRefresh
  mblnFactApro = False
  'Me.Enabled = True
  'objwin
  'Call objWinInfo.WinProcess(cwProcessToolBar, 30, 0)
End Sub

Private Sub Crear_Asientos()
  'Se realizan los inserts en las tablas FRT100, FRT200 y FRT300
  Dim strIns As String
  Dim strSQL As String
  Dim rstSQL As rdoResultset
  Dim strNumLin As String
  Dim qryNumLin As rdoQuery
  Dim rstNumLin As rdoResultset
  Dim intNumLin As Integer
  Dim dblImporte As Double
  Dim dblRedondeo As Double
  Dim dblTotal As Double
  Dim dblCod As Double
  Dim strPrd As String
  Dim qryPRD As rdoQuery
  Dim rstPrd As rdoResultset
  Dim strDesPrd As String
  Dim strSqlT2 As String
  Dim qrySqlT2 As rdoQuery
  Dim rstSqlT2 As rdoResultset
  Dim strMoneda As String
  Dim strSqlH2 As String
  Dim qrySqlH2 As rdoQuery
  Dim rstSqlH2 As rdoResultset
  Dim strCodFac As String
  Dim strCodDetFac As String
  Dim dblTPDes As Double
  Dim strFUV As String
  Dim strFR79 As String
  Dim qryFR79 As rdoQuery
  Dim rstFR79 As rdoResultset
  Dim strNProv As String
  Dim blnPP As Boolean
  Dim dblDptPP As Double
  Dim dblDtpPP As Double
  
  strSQL = "SELECT FRT1CODCAB_SEQUENCE.NEXTVAL FROM DUAL"
  Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
  dblCod = rstSQL.rdoColumns(0).Value
  rstSQL.Close
  Set rstSQL = Nothing
  
  strNumLin = "SELECT COUNT(*) FROM FR1500 WHERE FR39CODFACTCOMPRA = ? "
  Set qryNumLin = objApp.rdoConnect.CreateQuery("", strNumLin)
  qryNumLin(0) = mstrCodFac
  Set rstNumLin = qryNumLin.OpenResultset()
    
  If IsNumeric(mcurImp) Then
    dblImporte = mcurImp
  Else
    dblImporte = 0
  End If
  
  If IsNumeric(mcurRed) Then
    dblRedondeo = mcurRed
  Else
    dblRedondeo = 0
  End If
  dblTotal = dblRedondeo + dblImporte
  If Len(Trim(mstrFV)) = 0 Then
    strFUV = "01/01/1955"
  Else
    strFUV = mstrFV
  End If
  
  blnPP = False
  strFR79 = "SELECT FR79CODCOMP,FR79TPDTO,FR79INDPRONTOPAG FROM FR7900 WHERE FR79CODPROVEEDOR = ? "
  Set qryFR79 = objApp.rdoConnect.CreateQuery("", strFR79)
  qryFR79(0) = mintCodProv
  Set rstFR79 = qryFR79.OpenResultset()
  If Not rstFR79.EOF Then
    If Not IsNull(rstFR79.rdoColumns(0).Value) Then
      strNProv = rstFR79.rdoColumns(0).Value
      If rstFR79.rdoColumns("FR79INDPRONTOPAG").Value = -1 Then
        blnPP = True
        If Not IsNull(rstFR79.rdoColumns("FR79TPDTO").Value) Then
          dblDtpPP = rstFR79.rdoColumns("FR79TPDTO").Value
        Else
          dblDtpPP = 0
        End If
      End If
    Else
      strNProv = Completar(Trim(Str(mintCodProv)))
    End If
  Else
    strNProv = Completar(Trim(Str(mintCodProv)))
  End If
  qryFR79.Close
  Set qryFR79 = Nothing
  Set rstFR79 = Nothing
  
  strIns = "INSERT INTO FRT100 (FRT1CODCAB,FRT1INVOICEID,FRT1INVOICEDT,FRT1VENDOR," & _
                      "FRT1VCHRTTLLINES,FRT1GROSSAMT,FRT1DUEDT,FRT1GRPAPID) VALUES (" & _
                      dblCod & ",'" & _
                      strNF & "',TO_DATE('" & _
                      strDate & "','DD/MM/YYYY'),'" & _
                      strNProv & "'," & _
                      rstNumLin.rdoColumns(0).Value & "," & _
                      objGen.ReplaceStr(dblTotal, ",", ".", 1) & ",TO_DATE('" & _
                      strFUV & "','DD/MM/YYYY'),'0')"
  objApp.rdoConnect.Execute strIns, 64
  
  qryNumLin.Close
  Set qryNumLin = Nothing
  Set rstNumLin = Nothing
  
  strNumLin = "SELECT * FROM FR1500 WHERE FR39CODFACTCOMPRA = ? "
  Set qryNumLin = objApp.rdoConnect.CreateQuery("", strNumLin)
  qryNumLin(0) = mstrNumFac
  Set rstNumLin = qryNumLin.OpenResultset()
  
  intNumLin = 0
  While Not rstNumLin.EOF
    'Se realizan los inserts en FRT200
    intNumLin = intNumLin + 1 'Se incrementa el n�mero de l�nea
    If Len(Trim(mstrProv)) > 0 Then
      strDesPrd = mstrProv
    Else
      strDesPrd = "PROVEEDOR DESCONOCIDO"
    End If
    If IsNull(rstNumLin.rdoColumns("FR15IMPORTE").Value) Then
      dblImporte = 0
    Else
      dblImporte = rstNumLin.rdoColumns("FR15IMPORTE").Value
      Dim strZ As String
      Dim qryZ As rdoQuery
      Dim rstZ As rdoResultset
      strZ = "SELECT MAX(FR15IMPORTE) FROM FR1500 WHERE FR39CODFACTCOMPRA = ? "
      Set qryZ = objApp.rdoConnect.CreateQuery("", strZ)
      qryZ(0) = mstrNumFac
      Set rstZ = qryZ.OpenResultset()
      If Not rstZ.EOF Then
        If Not IsNull(rstZ.rdoColumns(0).Value) Then
          If rstZ.rdoColumns(0).Value = rstNumLin.rdoColumns("FR15IMPORTE").Value Then
            If mdifR <> 0 Then
              dblImporte = dblImporte + mdifR
              mdifR = 0
            End If
          End If
        End If
        qryZ.Close
        Set qryZ = Nothing
        Set rstZ = Nothing
      End If
    End If
    If IsNull(rstNumLin.rdoColumns("FRH8MONEDA").Value) Then
      strSqlH2 = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN = ?"
      Set qrySqlH2 = objApp.rdoConnect.CreateQuery("", strSqlH2)
      qrySqlH2(0) = 19
      Set rstSqlH2 = qrySqlH2.OpenResultset()
      strMoneda = rstSqlH2.rdoColumns(0).Value
      qrySqlH2.Close
      Set qrySqlH2 = Nothing
      Set rstSqlH2 = Nothing
    Else
      strMoneda = rstNumLin.rdoColumns("FRH8MONEDA").Value
    End If
    If IsNull(rstNumLin.rdoColumns("FR39CODFACTCOMPRA").Value) Then
      strCodFac = " "
    Else
      strCodFac = rstNumLin.rdoColumns("FR39CODFACTCOMPRA").Value
    End If
    If IsNull(rstNumLin.rdoColumns("FR15CODDETFACTCOMP").Value) Then
      strCodDetFac = " "
    Else
      strCodDetFac = rstNumLin.rdoColumns("FR15CODDETFACTCOMP").Value
    End If
    If IsNull(rstNumLin.rdoColumns("FR15TPDESCUENTO").Value) Then
      dblTPDes = 0
    Else
      dblTPDes = rstNumLin.rdoColumns("FR15TPDESCUENTO").Value
    End If
    
    strIns = "INSERT INTO FRT200 (FRT1CODCAB,FRT2VOUCHERLINENUM,FRT2TOTALDISTRIBS,FRT2DESCR," & _
                              "FRT2MERCHANDISE_AMT,FRH8MONEDA,FRT1CODFACTCOMPRA,FRT2CODDETFACTCOMP," & _
                              "FR73CODPRODUCTO,FRT2CANTIDAD,FRT2PRECIOUNIDAD,FRT2TPDESCUENTO) VALUES (" & _
                              dblCod & "," & intNumLin & ",1,'" & objGen.ReplaceStr(Left(strDesPrd, 30), ",", ".", 1) & "'," & objGen.ReplaceStr(dblImporte, ",", ".", 1) & ",'" & _
                              strMoneda & "','" & objGen.ReplaceStr(strCodFac, ",", ".", 1) & "'," & _
                              strCodDetFac & "," & rstNumLin.rdoColumns("FR73CODPRODUCTO").Value & "," & _
                              objGen.ReplaceStr(rstNumLin.rdoColumns("FR15CANTIDAD").Value, ",", ".", 1) & "," & objGen.ReplaceStr(rstNumLin.rdoColumns("FR15PRECIOUNIDAD").Value, ",", ".", 1) & "," & _
                              objGen.ReplaceStr(dblTPDes, ",", ".", 1) & ")"
    objApp.rdoConnect.Execute strIns, 64
    rstNumLin.MoveNext
  Wend
  
  If blnPP Then
    intNumLin = intNumLin + 1
    Dim dblTot As Currency
    Dim strTot As String
    
    'dblTot = Format((dblTotal * dblDtpPP / 100) * (-1), "0.00")
    
    strTot = objGen.ReplaceStr(mdblDPP * -1, ",", ".", 1)
    
    strIns = "INSERT INTO FRT200 (FRT1CODCAB,FRT2VOUCHERLINENUM,FRT2TOTALDISTRIBS,FRT2DESCR," & _
                              "FRT2MERCHANDISE_AMT,FRH8MONEDA,FRT1CODFACTCOMPRA,FRT2CODDETFACTCOMP," & _
                              "FR73CODPRODUCTO,FRT2CANTIDAD,FRT2PRECIOUNIDAD,FRT2TPDESCUENTO) VALUES (" & _
                              dblCod & "," & intNumLin & ",1,'" & objGen.ReplaceStr(Left(strDesPrd, 30), ",", ".", 1) & "'," & objGen.ReplaceStr(strTot, ",", ".", 1) & ",'" & _
                              strMoneda & "','" & objGen.ReplaceStr(strCodFac, ",", ".", 1) & "'," & _
                              strCodDetFac & "," & 999999998 & "," & _
                              1 & "," & strTot & "," & _
                              0 & ")"
    objApp.rdoConnect.Execute strIns, 64
    
  End If
  
  strSqlT2 = "SELECT * FROM FRT200 WHERE FRT1CODCAB = ?"
  Set qrySqlT2 = objApp.rdoConnect.CreateQuery("", strSqlT2)
  qrySqlT2(0) = dblCod
  Set rstSqlT2 = qrySqlT2.OpenResultset()
  intNumLin = 0
  While Not rstSqlT2.EOF
    'Se realizan los inserts en FRT300
    intNumLin = intNumLin + 1
    strPrd = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ? "
    Set qryPRD = objApp.rdoConnect.CreateQuery("", strPrd)
    qryPRD(0) = rstSqlT2.rdoColumns("FR73CODPRODUCTO").Value
    Set rstPrd = qryPRD.OpenResultset()
    strIns = "INSERT INTO FRT300 (FRT1CODCAB,FRT2VOUCHERLINENUM,FRT3VOUCHERLINENUM,FRT3DISTRIBLINENUM," & _
                                 "FRT3ACCOUNT,FRT3MERCHANDISEAMT) VALUES (" & _
                                 dblCod & "," & rstSqlT2.rdoColumns("FRT2VOUCHERLINENUM").Value & "," & _
                                 rstSqlT2.rdoColumns("FRT2VOUCHERLINENUM").Value & "," & intNumLin & ",'" & _
                                 rstPrd.rdoColumns("FR73CUENCONT").Value & "'," & objGen.ReplaceStr(rstSqlT2.rdoColumns("FRT2MERCHANDISE_AMT").Value, ",", ".", 1) & ")"
    objApp.rdoConnect.Execute strIns, 64
    rstSqlT2.MoveNext
  Wend
  qrySqlT2.Close
  Set qrySqlT2 = Nothing
  Set rstSqlT2 = Nothing
  'qryPRD.Close
  Set qryPRD = Nothing
  Set rstPrd = Nothing
End Sub
Private Function Completar(strP1 As String) As String
  'Rellena con ceros por la izquierda. Toma una cadena de longitud menor que 3 y le pone 0s
  'por la izquierda hasta que la cadena tenga tres caracteres
  Select Case Len(strP1)
    Case 0
      Completar = "000"
    Case 1
      Completar = "00" & strP1
    Case 2
      Completar = "0" & strP1
    Case Else
      Completar = strP1
  End Select
End Function
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
  If intIndex = 7 Then
    tlbToolbar1.Buttons(4).Enabled = False
  End If
  
  
End Sub

Private Sub txtvencimiento_Change()
Dim intdias As Integer
Dim rstFec As rdoResultset
Dim strFec As String

If IsNumeric(txtvencimiento.Text) Then
    strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
    Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
    intdias = txtvencimiento.Text
    dtcDateCombo1(2).Date = DateAdd("d", intdias, rstFec.rdoColumns(0).Value)
Else
    If txtvencimiento.Text = "" Then
        dtcDateCombo1(2).Text = ""
    Else
        Call MsgBox("El n� de d�as para la fecha de vencimiento no es num�rico", vbInformation, "Aviso")
        txtvencimiento.Text = ""
    End If
End If

End Sub


Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
   Dim Q%
   Dim sqlstr As String
   Dim rsta As rdoResultset
   Dim qryFRJ3 As rdoQuery
   Dim Estado_Linea
   Dim CodAlbaran
   Dim NumLinea
   Dim Producto
   Dim curCantidadFacturada As Currency
   
   
   ' comprobar linea por linea que no se facturen mas productos
   ' que los que hay en el albaran dentro de cada linea
   sqlstr = "Select FRJ3CANTPTEFACT " & _
            "  From frj300 " & _
            " Where FRJ1CODALBARAN = ? " & _
            "   and FRJ3NUMLINEA = ? "
   Set qryFRJ3 = objApp.rdoConnect.CreateQuery("", sqlstr)
         
   For Q% = 0 To grdDBGrid1(1).Rows - 1
      Estado_Linea = grdDBGrid1(1).Columns(0).Value
      CodAlbaran = grdDBGrid1(1).Columns("Albar�n").Value
      NumLinea = grdDBGrid1(1).Columns("Linea Albar�n").Value
      Producto = grdDBGrid1(1).Columns("Descripci�n Producto").Value
      curCantidadFacturada = grdDBGrid1(1).Columns("Cantidad").Value

      If Estado_Linea <> "Eliminado" Then
         ' seleccionar los que quedan por facturar de cada linea
         qryFRJ3(0) = CodAlbaran
         qryFRJ3(1) = NumLinea
         Set rsta = qryFRJ3.OpenResultset()
         If rsta.EOF Then
            Call MsgBox("L�nea: " & Q% & Chr(13) & _
               " Producto: " & Producto & Chr(13) & _
               " No tiene ninguna linea asociada en el albaran referenciado", _
               vbCritical, "Error")
            Set rsta = Nothing
            blnCancel = True
            Exit Sub
         End If
         If curCantidadFacturada <= 0 Then
            Call MsgBox("L�nea: " & Q% & Chr(13) & _
               " Producto: " & Producto & Chr(13) & _
               " La cantidad facturada: " & curCantidadFacturada & Chr(13) & _
               " ha de ser mayor que cero", vbExclamation, "Aviso")
            'rsta.Close
            Set rsta = Nothing
            blnCancel = True
            Exit Sub
         End If
         If curCantidadFacturada > rsta.rdoColumns("FRJ3CANTPTEFACT").Value Then
            'Call MsgBox("L�nea: " & Q% & Chr(13) &
            '   " Producto: " & Producto & Chr(13) & _
            '   " La cantidad facturada: " & curCantidadFacturada & Chr(13) & _
            '   " no puede ser mayor que la que est� pendiente de facturar: " & RSTA.rdoColumns("FRJ3CANTPTEFACT").Value, vbExclamation, "Aviso")
            'rsta.Close
            Set rsta = Nothing
            'blnCancel = True
            'Exit Sub
         End If
      End If
   Next Q%
   qryFRJ3.Close
   Set qryFRJ3 = Nothing
   
   If Q% > 0 Then
'      RSTA.Close
      Set rsta = Nothing
   ElseIf chkCheck1(1).Value = vbChecked Then
      MsgBox "La factura ha de tener al menos un producto para ser aprobada", vbExclamation, "Aviso"
   End If
   
End Sub

