VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmAnaRecCom 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Aceptar Recepci�n Compra"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0526.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdaceptar 
      Caption         =   "Aceptar Pedido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   10080
      TabIndex        =   63
      Top             =   2760
      Width           =   1575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      ForeColor       =   &H00FF0000&
      Height          =   975
      Left            =   120
      TabIndex        =   40
      Top             =   480
      Width           =   11655
      Begin VB.TextBox txtbuscar 
         Height          =   330
         Left            =   240
         TabIndex        =   54
         Top             =   480
         Width           =   5175
      End
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9840
         TabIndex        =   44
         Top             =   430
         Width           =   1215
      End
      Begin VB.Frame Frame2 
         Caption         =   "Buscarlo en"
         ForeColor       =   &H00FF0000&
         Height          =   615
         Left            =   6240
         TabIndex        =   41
         Top             =   240
         Width           =   3135
         Begin VB.CheckBox chkproveedor 
            Caption         =   "Proveedor"
            DataField       =   "sal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1680
            TabIndex        =   43
            Top             =   240
            Width           =   1425
         End
         Begin VB.CheckBox chkproducto 
            Caption         =   "Producto"
            DataField       =   "sal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   120
            TabIndex        =   42
            Top             =   240
            Width           =   1425
         End
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a Buscar "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   7
         Left            =   240
         TabIndex        =   45
         Top             =   240
         Width           =   1365
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3465
      Index           =   1
      Left            =   120
      TabIndex        =   9
      Top             =   4560
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   3060
         Index           =   1
         Left            =   120
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   5398
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0526.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(18)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(17)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(16)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(15)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(2)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(3)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(4)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(12)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(13)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(19)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(20)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(21)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(22)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(23)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "dtcDateCombo1(0)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(11)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(13)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(10)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(12)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(14)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(0)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(2)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(4)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(3)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(15)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(17)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(18)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "chkCheck1(0)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(16)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(19)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(20)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "cmdcontrol"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "cmdreclamar"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(21)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).ControlCount=   36
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0526.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   4800
            TabIndex        =   66
            Tag             =   "Moneda"
            Top             =   960
            Width           =   705
         End
         Begin VB.CommandButton cmdreclamar 
            Caption         =   "Reclamar"
            Height          =   375
            Left            =   5520
            TabIndex        =   65
            Top             =   2520
            Width           =   1575
         End
         Begin VB.CommandButton cmdcontrol 
            Caption         =   "Control de Lotes"
            Height          =   375
            Left            =   3840
            TabIndex        =   64
            Top             =   2520
            Width           =   1575
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25CODDETPEDCOMP"
            Height          =   330
            HelpContextID   =   40101
            Index           =   20
            Left            =   120
            MaxLength       =   2
            TabIndex        =   61
            Tag             =   "L�nea"
            Top             =   2640
            Visible         =   0   'False
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25CANTENT"
            Height          =   330
            HelpContextID   =   40102
            Index           =   19
            Left            =   1920
            TabIndex        =   59
            Tag             =   "Cantidad Acumulada"
            Top             =   1560
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR62CODPEDCOMPRA"
            Height          =   330
            HelpContextID   =   30101
            Index           =   16
            Left            =   1440
            TabIndex        =   55
            Tag             =   "C�digo Pedido"
            Top             =   2640
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.CheckBox chkCheck1 
            Caption         =   "Caducidad Controlada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   120
            TabIndex        =   53
            Tag             =   "Caducidad Controlada?"
            Top             =   2040
            Width           =   2265
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25OBSERV"
            Height          =   615
            Index           =   18
            Left            =   3840
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   51
            Tag             =   "Observaciones"
            Top             =   1560
            Width           =   6960
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25CANTENTREG"
            Height          =   330
            HelpContextID   =   40102
            Index           =   17
            Left            =   120
            TabIndex        =   47
            Tag             =   "Entregado"
            Top             =   1560
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25IMPORLINEA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   3240
            TabIndex        =   38
            Tag             =   "Importe"
            Top             =   960
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25CANTPEDIDA"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   120
            TabIndex        =   36
            Tag             =   "Cantidad"
            Top             =   960
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR25PRECIOUNIDAD"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   1680
            TabIndex        =   28
            Tag             =   "Precio Unidad"
            Top             =   960
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1320
            TabIndex        =   27
            Tag             =   "Producto"
            Top             =   360
            Width           =   4845
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   120
            TabIndex        =   26
            Tag             =   "C�d.Producto"
            Top             =   360
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   25
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   960
            Width           =   3225
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   9360
            Locked          =   -1  'True
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "Volumen"
            Top             =   360
            Width           =   1500
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   6960
            Locked          =   -1  'True
            TabIndex        =   23
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   6360
            Locked          =   -1  'True
            TabIndex        =   22
            TabStop         =   0   'False
            Tag             =   "Forma"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8400
            Locked          =   -1  'True
            TabIndex        =   21
            TabStop         =   0   'False
            Tag             =   "U.M"
            Top             =   360
            Width           =   900
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2520
            Index           =   1
            Left            =   -74880
            TabIndex        =   8
            TabStop         =   0   'False
            Top             =   120
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   4445
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR25FECPLAZENTRE"
            Height          =   330
            Index           =   0
            Left            =   5640
            TabIndex        =   49
            Tag             =   "Fecha Entrega"
            Top             =   960
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   23
            Left            =   4800
            TabIndex        =   67
            Top             =   720
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "L�nea"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   22
            Left            =   120
            TabIndex        =   62
            Top             =   2400
            Visible         =   0   'False
            Width           =   510
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad Acumulada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   1920
            TabIndex        =   60
            Top             =   1320
            Width           =   1755
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   1440
            TabIndex        =   56
            Top             =   2400
            Visible         =   0   'False
            Width           =   990
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   3840
            TabIndex        =   52
            Top             =   1320
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Entrega"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   5640
            TabIndex        =   50
            Top             =   720
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad Entregada"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   120
            TabIndex        =   48
            Top             =   1320
            Width           =   1695
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   3240
            TabIndex        =   39
            Top             =   720
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad Pedida"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   37
            Top             =   720
            Width           =   1410
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Unidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1680
            TabIndex        =   35
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   34
            Top             =   120
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   7560
            TabIndex        =   33
            Top             =   720
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   9360
            TabIndex        =   32
            Top             =   120
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   8400
            TabIndex        =   31
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   6960
            TabIndex        =   30
            Top             =   120
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   6360
            TabIndex        =   29
            Top             =   120
            Width           =   345
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pedido de Compra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Index           =   0
      Left            =   120
      TabIndex        =   6
      Top             =   1560
      Width           =   9705
      Begin TabDlg.SSTab tabTab1 
         Height          =   2415
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   360
         Width           =   9450
         _ExtentX        =   16669
         _ExtentY        =   4260
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0526.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(8)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(11)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(9)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "cboSSDBCombo1(0)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "dtcDateCombo1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(6)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(5)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(1)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(7)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(8)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(9)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtestado"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).ControlCount=   16
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0526.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtestado 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   40102
            Left            =   6360
            TabIndex        =   58
            Tag             =   "Estado"
            Top             =   285
            Width           =   2415
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   720
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Proveedor"
            Top             =   1920
            Width           =   7680
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   120
            TabIndex        =   18
            Tag             =   "C�d.Proveedor"
            Top             =   1920
            Width           =   360
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR62DESCPERSONALIZADA"
            Height          =   615
            Index           =   7
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Tag             =   "Descripci�n"
            Top             =   960
            Width           =   8280
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR62RECARGO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   4800
            TabIndex        =   2
            Tag             =   "Recargo"
            Top             =   300
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR62PORTES"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   3240
            TabIndex        =   1
            Tag             =   "Portes"
            Top             =   300
            Width           =   1400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR62CODPEDCOMPRA"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Pedido"
            Top             =   300
            Width           =   1080
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2130
            Index           =   0
            Left            =   -74910
            TabIndex        =   5
            Top             =   90
            Width           =   8925
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   15743
            _ExtentY        =   3757
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR62FECPEDCOMPRA"
            Height          =   330
            Index           =   1
            Left            =   1320
            TabIndex        =   15
            Tag             =   "Fecha Pedido"
            Top             =   300
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
            DataField       =   "FR95CODESTPEDCOMPRA"
            Height          =   330
            Index           =   0
            Left            =   8760
            TabIndex        =   57
            Tag             =   "Estado Pedido"
            Top             =   285
            Width           =   285
            DataFieldList   =   "Column 0"
            AllowInput      =   0   'False
            _Version        =   131078
            DataMode        =   2
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RowHeight       =   423
            Columns.Count   =   2
            Columns(0).Width=   820
            Columns(0).Caption=   "C�D"
            Columns(0).Name =   "C�DIGO"
            Columns(0).DataField=   "Column 0"
            Columns(0).DataType=   8
            Columns(0).FieldLen=   256
            Columns(1).Width=   9525
            Columns(1).Caption=   "DESCRIPCI�N"
            Columns(1).Name =   "DESCRIPCI�N"
            Columns(1).DataField=   "Column 1"
            Columns(1).DataType=   8
            Columns(1).FieldLen=   256
            _ExtentX        =   503
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   -2147483643
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   6360
            TabIndex        =   46
            Top             =   105
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   120
            TabIndex        =   19
            Top             =   1680
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   17
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1320
            TabIndex        =   16
            Top             =   105
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Recargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   4800
            TabIndex        =   13
            Top             =   105
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Portes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   3240
            TabIndex        =   12
            Top             =   105
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�digo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   11
            Top             =   105
            Width           =   600
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   10
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmAnaRecCom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmIntRecCom (FR0526.FRM)                                    *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: Analizar Recepci�n de Compra                            *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Private Sub cmdAceptar_Click()
'se introduce la cantidad recibida en FR3500
Dim strupdate As String
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String
Dim str35 As String
Dim rst35 As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim i As Integer
Dim struni As String
Dim rstuni As rdoResultset
Dim cantidadacumulada
Dim rsta As rdoResultset
Dim stra As String

If txtText1(6).Text = "" Then
   MsgBox "Debe seleccionar un pedido de compra", vbInformation
   Exit Sub
End If

cmdAceptar.Enabled = False

'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
strori = "SELECT FR04CODALMACEN FROM AD0200,FRH200 " & _
            "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
            "AND FRH2CODPARAMGEN=3"
Set rstori = objApp.rdoConnect.OpenResultset(strori)
'se buscan los productos del pedido
stra = "SELECT * FROM FR2500 WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text
Set rsta = objApp.rdoConnect.OpenResultset(stra)
While Not rsta.EOF
    If Not IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
     If (Not rstori.EOF) And Not IsNull(rstori.rdoColumns(0).Value) Then
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str35)
            
        struni = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
        Set rstuni = objApp.rdoConnect.OpenResultset(struni)
        
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es Farmacia
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                      rst35.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      "8" & "," & _
                      "SYSDATE" & "," & _
                      "NULL" & "," & _
                      txtText1(0).Text & "," & _
                      rsta.rdoColumns("FR25CANTENTREG").Value & "," & _
                      rsta.rdoColumns("FR25PRECIOUNIDAD").Value & ","
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertentrada = strinsertentrada & "'" & rstuni.rdoColumns(0).Value & "'" & ")"
        Else
            strinsertentrada = strinsertentrada & "1)"
        End If
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
        rstuni.Close
        Set rstuni = Nothing
        rst35.Close
        Set rst35 = Nothing
    End If
  End If
  
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'el campo de cantidad entregada se pone a vac�o
'y el de cantidad acumulada=cantidad acumulada+cantidad entregada
If Not IsNull(rsta.rdoColumns("FR25CANTENT").Value) And Not IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
    cantidadacumulada = CDec(rsta.rdoColumns("FR25CANTENTREG").Value) + CDec(rsta.rdoColumns("FR25CANTENT").Value)
End If
If IsNull(rsta.rdoColumns("FR25CANTENT").Value) And Not IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
    cantidadacumulada = CDec(rsta.rdoColumns("FR25CANTENTREG").Value)
End If
If Not IsNull(rsta.rdoColumns("FR25CANTENT").Value) And IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
    cantidadacumulada = CDec(rsta.rdoColumns("FR25CANTENT").Value)
End If
If IsNull(rsta.rdoColumns("FR25CANTENT").Value) And IsNull(rsta.rdoColumns("FR25CANTENTREG").Value) Then
    cantidadacumulada = ""
End If
If cantidadacumulada <> "" Then
    strupdate = "UPDATE FR2500 SET FR25CANTENTREG=''," & _
                "FR25CANTENT=" & cantidadacumulada & _
              " WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text & _
              " AND FR25CODDETPEDCOMP=" & rsta.rdoColumns("FR25CODDETPEDCOMP").Value
    objApp.rdoConnect.Execute strupdate, 64
    objApp.rdoConnect.Execute "Commit", 64
End If
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
rsta.MoveNext
Wend
rstori.Close
Set rstori = Nothing
rsta.Close
Set rsta = Nothing

Call MsgBox("La petici�n ha sido aceptada.", vbInformation, "Petici�n Aceptada")

Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
Call objWinInfo.DataRefresh

cmdAceptar.Enabled = True
End Sub

Private Sub cmdAceptar2_Click()  'NO SE USA
'se introduce la cantidad recibida en FR3500
Dim strupdate As String
Dim rstori As rdoResultset
Dim strori As String
Dim strinsertentrada As String
Dim strinsertsalida As String
Dim str35 As String
Dim rst35 As rdoResultset
Dim str80 As String
Dim rst80 As rdoResultset
Dim i As Integer
Dim struni As String
Dim rstuni As rdoResultset
Dim cantidadacumulada


cmdAceptar.Enabled = False
'se mira que el grid de productos tenga alg�n registro
If txtText1(0).Text = "" Then
    cmdAceptar.Enabled = True
    Exit Sub
End If

If txtText1(17).Text = "" Then
    cmdAceptar.Enabled = True
    Call MsgBox("El producto ya est� aceptado.", vbInformation, "Aviso")
    Exit Sub
End If

'se obtiene el almac�n del servicio 1(Farmacia) que es el que dispensa el producto
strori = "SELECT FR04CODALMACEN FROM AD0200,FRH200 " & _
            "WHERE AD02CODDPTO=FRH2PARAMGEN " & _
            "AND FRH2CODPARAMGEN=3"
Set rstori = objApp.rdoConnect.OpenResultset(strori)

If (Not rstori.EOF) And Not IsNull(rstori.rdoColumns(0).Value) Then
  
        str35 = "SELECT FR35CODMOVIMIENTO_SEQUENCE.nextval FROM dual"
        Set rst35 = objApp.rdoConnect.OpenResultset(str35)
            
        struni = "SELECT FR93CODUNIMEDIDA FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
        Set rstuni = objApp.rdoConnect.OpenResultset(struni)
        
        'se actualiza el inventario de entrada(FR3500) en el que el origen de entrada es
        'Farmacia y el destino es Farmacia
        strinsertentrada = "INSERT INTO FR3500 (FR35CODMOVIMIENTO,FR04CODALMACEN_ORI," & _
                      "FR04CODALMACEN_DES,FR90CODTIPMOV,FR35FECMOVIMIENTO,FR35OBSMOVI," & _
                      "FR73CODPRODUCTO,FR35CANTPRODUCTO,FR35PRECIOUNIDAD,FR93CODUNIMEDIDA) VALUES (" & _
                      rst35.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      rstori.rdoColumns(0).Value & "," & _
                      "8" & "," & _
                      "SYSDATE" & "," & _
                      "NULL" & "," & _
                      txtText1(0).Text & "," & _
                      txtText1(17).Text & "," & _
                      txtText1(4).Text & ","
        If IsNull(rstuni.rdoColumns(0).Value) = False Then
            strinsertentrada = strinsertentrada & "'" & rstuni.rdoColumns(0).Value & "'" & ")"
        Else
            strinsertentrada = strinsertentrada & "1)"
        End If
        objApp.rdoConnect.Execute strinsertentrada, 64
        objApp.rdoConnect.Execute "Commit", 64
        rstuni.Close
        Set rstuni = Nothing
        rst35.Close
        Set rst35 = Nothing
End If

rstori.Close
Set rstori = Nothing

'el campo de cantidad entregada se pone a vac�o
'y el de cantidad acumulada=cantidad acumulada+cantidad entregada
If txtText1(19).Text <> "" Then
    cantidadacumulada = CDec(txtText1(17).Text) + CDec(txtText1(19).Text)
Else
    cantidadacumulada = CDec(txtText1(17).Text)
End If
strupdate = "UPDATE FR2500 SET FR25CANTENTREG=''," & _
            "FR25CANTENT=" & cantidadacumulada & _
          " WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text & _
          " AND FR25CODDETPEDCOMP=" & txtText1(20).Text
objApp.rdoConnect.Execute strupdate, 64
objApp.rdoConnect.Execute "Commit", 64

Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
Call objWinInfo.DataRefresh

cmdAceptar.Enabled = True
End Sub

Private Sub cmdbuscar_Click()
Dim strclausulawhere As String

cmdbuscar.Enabled = False
strclausulawhere = "-1=-1 "
If chkproveedor.Value = 1 Then
    If txtbuscar.Text <> "" Then
        strclausulawhere = strclausulawhere & " AND FR79CODPROVEEDOR IN (" & _
            "SELECT FR79CODPROVEEDOR from fr7900 where " & _
            "upper(FR79PROVEEDOR) like upper('" & txtbuscar.Text & "%')" & _
            ")"
    End If
End If


If chkproducto.Value = 1 Then
    If txtbuscar.Text <> "" Then
        strclausulawhere = strclausulawhere & " AND FR62CODPEDCOMPRA IN (" & _
           "SELECT FR62CODPEDCOMPRA FROM FR2500 WHERE FR73CODPRODUCTO IN (" & _
           "SELECT FR73CODPRODUCTO from fr7300 where " & _
           "upper(FR73DESPRODUCTO) like upper('" & txtbuscar.Text & "%')" & _
           ")" & _
           ")"
    End If
End If

If (chkproveedor.Value = 0 And chkproducto.Value = 0) Or (txtbuscar.Text = "") Then
    strclausulawhere = strclausulawhere & " AND FR62CODPEDCOMPRA IS NULL"
End If

Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.objWinActiveForm.strWhere = strclausulawhere
Call objWinInfo.DataRefresh


cmdbuscar.Enabled = True
End Sub

Private Sub cmdcontrol_Click()
cmdcontrol.Enabled = False
    'Call frmMantLotes.Show(vbModal)
    gstrLlamador = "FrmAnaRecCom"
    Call objsecurity.LaunchProcess("FR0534")
cmdcontrol.Enabled = True
End Sub

Private Sub cmdreclamar_Click()
cmdReclamar.Enabled = False
    'Call FrmRecPed.Show(vbModal)
    gstrLlamador = "FrmAnaRecCom"
    Call objsecurity.LaunchProcess("FR0523")
cmdReclamar.Enabled = True
End Sub





Private Sub Form_Activate()
   txtestado.Locked = True
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Pedidos de Compra"
    
    .strTable = "FR6200"
    
    .intAllowance = cwAllowModify
    '(Recibido Parcial,Recibido Completo,Cerrado Parcialmente Completo)
    .strWhere = " FR95CODESTPEDCOMPRA IN (4,5,6)"
    
    
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Pedidos de Compra")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR62FECPEDCOMPRA", "Fecha Pedido", cwDate)
    Call .FormAddFilterWhere(strKey, "FR62PORTES", "Portes", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR62RECARGO", "Recargo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�d.Proveedor", cwNumeric)

  With objDetailInfo
    .strName = "Productos"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FR2500"
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
    Call .FormAddRelation("FR62CODPEDCOMPRA", txtText1(6))
    
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CANTPEDIDA", "Cantidad Pedida", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25FECPLAZENTRE", "Fecha Plazo Entrega", cwDate)
End With

  End With
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
  
   
    
    Call .FormCreateInfo(objMasterInfo)
    
    'pedidos
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    'productos
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    
    'pedido
    '.CtrlGetInfo(txtText1(6)).blnReadOnly = True
    .CtrlGetInfo(txtText1(7)).blnReadOnly = True
    .CtrlGetInfo(txtText1(8)).blnReadOnly = True
    .CtrlGetInfo(txtText1(5)).blnReadOnly = True
    .CtrlGetInfo(txtText1(1)).blnReadOnly = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnReadOnly = True
    
    .CtrlGetInfo(txtestado).blnNegotiated = False
    
    .CtrlGetInfo(txtText1(20)).blnInGrid = False
    .CtrlGetInfo(txtText1(16)).blnInGrid = False
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "FR73DESPRODUCTO")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(13), "FRH7CODFORMFAR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FR73DOSIS")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR93CODUNIMEDIDA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "FR73VOLUMEN")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(14), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")


    .CtrlGetInfo(cboSSDBCombo1(0)).blnInFind = True
    .CtrlGetInfo(cboSSDBCombo1(0)).strSQL = "SELECT FR95CODESTPEDCOMPRA,FR95DESESTPEDCOMPRA FROM FR9500 ORDER BY FR95CODESTPEDCOMPRA"
    
    Call .WinRegister
    Call .WinStabilize
    
  End With
 
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub
Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Dim rsta As rdoResultset
Dim stra As String
    
'cada vez que se cambie de producto que se limpie la caja de texto txtestado
    txtestado = ""

'cuando se cambie de pedido que se llene txtestado con la descripci�n
'de estado(cbossdbcombo1(0))
    If cboSSDBCombo1(0).Value <> "" Then
        stra = "SELECT FR95DESESTPEDCOMPRA FROM FR9500 WHERE FR95CODESTPEDCOMPRA=" & _
              cboSSDBCombo1(0).Value
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        txtestado.Text = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
    End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub





' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub






' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
If intIndex = 0 Then
    txtestado.Text = cboSSDBCombo1(0).Columns(1).Value
End If
End Sub
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
    Call objWinInfo.CtrlLostFocus
End Sub
Private Sub cboSSDBCombo1_Change(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub

