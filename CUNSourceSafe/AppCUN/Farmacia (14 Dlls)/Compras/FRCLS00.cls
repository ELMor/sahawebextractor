VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCWLauncher"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' **********************************************************************************
' Class clsCWLauncher
' Coded by SIC Donosti
' **********************************************************************************

'COMPRAS
Const PRWinGenPetOfe                         As String = "FR0501"
Const PRWinBusMed                            As String = "FR0502"
Const PRWinBusMat                            As String = "FR0503"
Const PRWinVerPetOfe                         As String = "FR0504"
Const PRWinIntOfe                            As String = "FR0505"
Const PRWinRecOfe                            As String = "FR0506"
Const PRWinVerDatPrv                         As String = "FR0507"
Const PRWinValOfe                            As String = "FR0508"
Const PRWinBusOfe                            As String = "FR0509"
Const PRWinRedPedCom                         As String = "FR0510"
Const PRWinRedGesCom                         As String = "FR0511"
'Const PRWinBusPrd                            As String = "FR0512"
Const PRWinBusPed                            As String = "FR0513"
Const PRWinBusFac                            As String = "FR0514"
Const PRWinRedSolCom                         As String = "FR0515"
Const PRWinConExi                            As String = "FR0516"
Const PRWinRedPedCom1                        As String = "FR0517"
Const PRWinEstSolCom                         As String = "FR0518"
Const PRWinBusSolCom                         As String = "FR0519"

'Const PRWinEstPedCom                         As String = "FR0520"
Const PRWinConSitPed                         As String = "FR0522"
Const PRWinRecPed                            As String = "FR0523"
Const PRWinEmiPed                            As String = "FR0524"
Const PRWinIntRecCom                         As String = "FR0525"
'Const PRWinAnaRecCom                         As String = "FR0526"
Const PRWinIntFacCom                         As String = "FR0527"
'Const PRWinEstFac                            As String = "FR0528"
Const PRWinLocEnt                            As String = "FR0537"
Const PRWinRecFac                            As String = "FR0530"
Const PRWinConEntPen                         As String = "FR0531"
Const PRWinBusOfe1                           As String = "FR0532"
Const PRWinMensajeCorreo                     As String = "FR0533"
Const PRWinMantenimientoLotes                As String = "FR0534"
Const FRWinDefProd                           As String = "FR0535" 'Visi�n de Definir Producto
Const FRWinAbono                             As String = "FR0536"
Const FRWinConSitAlb                         As String = "FR0529" 'Consultar situaci�n albaranes
Const FRWinBusAlb                            As String = "FR0538" 'Buscar albaranes
Const FRWinListToAlb                         As String = "FR0556" 'Lista para crear el albar�n
Const FRWinAsientos                          As String = "FR0547" 'Asientos contables
Const FRRepPedido                            As String = "FR5101" 'Listado de pedido
Const FRWinGenPed                            As String = "FR0991"
Const FRRepPteFac                            As String = "FR5291"
Const FRRepPropPep                           As String = "FR9911"
Const FRRepBajoStock                         As String = "FR5161"
Const FRRepEntFact                           As String = "FR5292"
Const FRRepEntCC                             As String = "FR5293"
Const FRRepPedRea                            As String = "FR5296"
Const FRRepPedRecla                          As String = "FR5297"
Const FRRepPedPendRecla                      As String = "FR5298"
Const FRRepABCPedPendRecla                   As String = "FR5241" 'ABC Entradas por Cuenta contable
Const FRRepABCProveedores                    As String = "FR5242" 'ABC Proveedores
Const FRRepABCProductos                      As String = "FR5243" 'ABC Productos
Const FRWinModAlbaran                        As String = "FR0560"
Const FRWinRealInventCompras                 As String = "FR0561"
Const FRWinFaltas                            As String = "FR0570"
Const FRWinImprimirEtiquetas                 As String = "FR0540"
Const FRWinEliminarFacturas                  As String = "FR0541"
'Const FRReptxtAsientos                      As String = "FR5251" 'txt de Asientos Contables
'esto va en Definir Productos ++++++++++++++++++++++++++++++++++++++++
Const FRRepMaestroMedicacion                 As String = "FR0356"
Const FRRepMaestroMaterial                   As String = "FR0357"
Const FRRepMaestroImplantes                  As String = "FR0358"
Const FRRepMaestroProductosporCuentaContable As String = "FR0350"


Public Sub OpenCWServer(ByVal mobjCW As clsCW)
  Set objApp = mobjCW.objApp
  Set objPipe = mobjCW.objPipe
  Set objGen = mobjCW.objGen
  Set objError = mobjCW.objError
  Set objEnv = mobjCW.objEnv
  Set objmouse = mobjCW.objmouse
  Set objsecurity = mobjCW.objsecurity
End Sub


' el argumento vntData puede ser una matriz teniendo en cuenta que
' el l�mite inferior debe comenzar en 1, es decir, debe ser 1 based
Public Function LaunchProcess(ByVal strProcess As String, _
                              Optional ByRef vntData As Variant) As Boolean


  
   'Case Nombre_ventana
      'Load frm.....
      'Call objSecurity.AddHelpContext(??)
      'Call frm......Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload frm.....
      'Set frm..... = Nothing
  On Error Resume Next
  
  ' fija el valor de retorno a verdadero
  LaunchProcess = True
  
  ' comienza la selecci�n del proceso
  Select Case strProcess
  
    Case PRWinGenPetOfe
      Load FrmGenPetOfe
      'Call objsecurity.AddHelpContext(528)
      Call FrmGenPetOfe.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmGenPetOfe
      Set FrmGenPetOfe = Nothing
    Case PRWinBusMed
      Load FrmBusMed
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusMed.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusMed
      Set FrmBusMed = Nothing
    Case PRWinBusMat
      Load FrmBusMat
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusMat.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusMat
      Set FrmBusMat = Nothing
    Case PRWinVerPetOfe
      Load FrmVerPetOfe
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerPetOfe.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmVerPetOfe
      Set FrmVerPetOfe = Nothing
    Case PRWinIntOfe
      Load FrmIntOfe
      'Call objsecurity.AddHelpContext(528)
      Call FrmIntOfe.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmIntOfe
      Set FrmIntOfe = Nothing
    Case PRWinRecOfe
      Load FrmRecOfe
      'Call objsecurity.AddHelpContext(528)
      Call FrmRecOfe.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRecOfe
      Set FrmRecOfe = Nothing
    Case PRWinVerDatPrv
      Load FrmVerDatPrv
      'Call objsecurity.AddHelpContext(528)
      Call FrmVerDatPrv.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmVerDatPrv
      Set FrmVerDatPrv = Nothing
    Case PRWinValOfe
      Load FrmValOfe
      'Call objsecurity.AddHelpContext(528)
      Call FrmValOfe.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmValOfe
      Set FrmValOfe = Nothing
    Case PRWinBusOfe
      Load FrmBusOfe
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusOfe.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusOfe
      Set FrmBusOfe = Nothing
    Case PRWinRedPedCom
      Load FrmRedPedCom
      'Call objsecurity.AddHelpContext(528)
      Call FrmRedPedCom.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRedPedCom
      Set FrmRedPedCom = Nothing
    Case PRWinRedGesCom
      Load FrmRedGesCom
      'Call objsecurity.AddHelpContext(528)
      Call FrmRedGesCom.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRedGesCom
      Set FrmRedGesCom = Nothing
    Case FRWinAsientos
      Load frmAsientos
      'Call objsecurity.AddHelpContext(528)
      Call frmAsientos.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmAsientos
      Set frmAsientos = Nothing
    Case PRWinBusPed
      Load FrmBusPed
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusPed.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusPed
      Set FrmBusPed = Nothing
    Case PRWinBusFac
      Load FrmBusFac
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusFac.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusFac
      Set FrmBusFac = Nothing
    Case PRWinRedSolCom
      Load FrmRedSolCom
      'Call objsecurity.AddHelpContext(528)
      Call FrmRedSolCom.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRedSolCom
      Set FrmRedSolCom = Nothing
      
    Case PRWinConExi
      Load FrmConExi
      'Call objsecurity.AddHelpContext(528)
      Call FrmConExi.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmConExi
      Set FrmConExi = Nothing
    Case PRWinRedPedCom1
      Load FrmRedPedCom1
      'Call objsecurity.AddHelpContext(528)
      Call FrmRedPedCom1.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRedPedCom1
      Set FrmRedPedCom1 = Nothing
    Case PRWinEstSolCom
      Load FrmEstSolCom
      'Call objsecurity.AddHelpContext(528)
      Call FrmEstSolCom.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmEstSolCom
      Set FrmEstSolCom = Nothing
    Case PRWinBusSolCom
      Load FrmBusSolCom
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusSolCom.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusSolCom
      Set FrmBusSolCom = Nothing
    Case FRWinGenPed
      Load FrmRedPedCom3
      'Call objsecurity.AddHelpContext(528)
      Call FrmRedPedCom3.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRedPedCom3
      Set FrmRedPedCom3 = Nothing
    Case PRWinConSitPed
      Load FrmConSitPed
      'Call objsecurity.AddHelpContext(528)
      Call FrmConSitPed.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmConSitPed
      Set FrmConSitPed = Nothing
    Case PRWinRecPed
      Load FrmRecPed
      'Call objsecurity.AddHelpContext(528)
      Call FrmRecPed.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRecPed
      Set FrmRecPed = Nothing
    Case PRWinEmiPed
      Load FrmEmiPed
      'Call objsecurity.AddHelpContext(528)
      Call FrmEmiPed.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmEmiPed
      Set FrmEmiPed = Nothing
    Case PRWinIntRecCom
      Load FrmIntRecCom
      'Call objsecurity.AddHelpContext(528)
      Call FrmIntRecCom.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmIntRecCom
      Set FrmIntRecCom = Nothing
    'Case PRWinAnaRecCom
    '  Load FrmAnaRecCom
    '  'Call objsecurity.AddHelpContext(528)
    '  Call FrmAnaRecCom.Show(vbModal)
    '  'Call objSecurity.RemoveHelpContext
    '  Unload FrmAnaRecCom
    '  Set FrmAnaRecCom = Nothing
    Case PRWinIntFacCom
      Load FrmIntFacCom
      'Call objsecurity.AddHelpContext(528)
      Call FrmIntFacCom.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmIntFacCom
      Set FrmIntFacCom = Nothing
    'Case PRWinEstFac
    '  Load FrmEstFac
    '  'Call objsecurity.AddHelpContext(528)
    '  Call FrmEstFac.Show(vbModal)
    '  'Call objSecurity.RemoveHelpContext
    '  Unload FrmEstFac
    '  Set FrmEstFac = Nothing
    Case PRWinLocEnt
      'Load FrmLocEnt
      'Call objsecurity.AddHelpContext(528)
      'Call FrmLocEnt.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      'Unload FrmLocEnt
      'Set FrmLocEnt = Nothing
    Case PRWinRecFac
      Load FrmRecFac
      'Call objsecurity.AddHelpContext(528)
      Call FrmRecFac.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmRecFac
      Set FrmRecFac = Nothing
    Case PRWinConEntPen
      Load FrmConEntPen
      'Call objsecurity.AddHelpContext(528)
      Call FrmConEntPen.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmConEntPen
      Set FrmConEntPen = Nothing
    Case PRWinBusOfe1
      Load FrmBusOfe1
      'Call objsecurity.AddHelpContext(528)
      Call FrmBusOfe1.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload FrmBusOfe1
      Set FrmBusOfe1 = Nothing
    Case PRWinMensajeCorreo
      Load frmMensajeCorreo
      'Call objsecurity.AddHelpContext(528)
      Call frmMensajeCorreo.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmMensajeCorreo
      Set frmMensajeCorreo = Nothing
    Case PRWinMantenimientoLotes
      Load frmMantenimientoLotes
      'Call objsecurity.AddHelpContext(528)
      Call frmMantenimientoLotes.Show(vbModal)
      'Call objSecurity.RemoveHelpContext
      Unload frmMantenimientoLotes
      Set frmMantenimientoLotes = Nothing
    Case FRWinDefProd
      Load frmDefProducto
      Call frmDefProducto.Show(vbModal)
      Unload frmDefProducto
      Set frmDefProducto = Nothing
    Case FRWinAbono
      Load FrmAbono
      Call FrmAbono.Show(vbModal)
      Unload FrmAbono
      Set FrmAbono = Nothing
    Case FRWinConSitAlb
      Load FrmConSitAlb
      Call FrmConSitAlb.Show(vbModal)
      Unload FrmConSitAlb
      Set FrmConSitAlb = Nothing
    Case FRWinBusAlb
      Load FrmBusAlb
      Call FrmBusAlb.Show(vbModal)
      Unload FrmBusAlb
      Set FrmBusAlb = Nothing
    Case FRWinListToAlb
      Load frmListToAlb
      Call frmListToAlb.Show(vbModal)
      Unload frmListToAlb
      Set frmListToAlb = Nothing
    Case FRWinModAlbaran
      If Not IsMissing(vntData) Then
        Call objPipe.PipeSet("NUMALBARAN", vntData)
      End If
      Load FrmModAlbaran
      Call FrmModAlbaran.Show(vbModal)
      Set FrmModAlbaran = Nothing
    Case FRWinRealInventCompras
      Load frmRealInventCompras
      Call frmRealInventCompras.Show(vbModal)
      Unload frmRealInventCompras
      Set frmRealInventCompras = Nothing
    Case FRWinFaltas
      Load FrmFaltas
      Call FrmFaltas.Show(vbModal)
      Unload FrmFaltas
      Set FrmFaltas = Nothing
    Case FRWinImprimirEtiquetas
      Load frmImprimirEtiquetas
      Call frmImprimirEtiquetas.Show(vbModal)
      Unload frmImprimirEtiquetas
      Set frmImprimirEtiquetas = Nothing
    Case FRWinEliminarFacturas
      Load frmEliminarFacturas
      Call frmEliminarFacturas.Show(vbModal)
      Unload frmEliminarFacturas
      Set frmEliminarFacturas = Nothing
      
  End Select
  Call ERR.Clear
End Function


Public Sub GetProcess(ByRef aProcess() As Variant)
  ' Hay que devolver la informaci�n para cada proceso
  ' blnMenu indica si el proceso debe aparecer en el men� o no
  ' Cuidado! la descripci�n se trunca a 40 caracteres
  ' El orden de entrada a la matriz es indiferente
  
  ' Redimensionar la matriz
  ReDim aProcess(1 To 58, 1 To 4) As Variant
      
  ' VENTANAS
  'MANTENIMIENTO
  
  aProcess(1, 1) = PRWinGenPetOfe
  aProcess(1, 2) = "Introducir Petici�n Oferta"
  aProcess(1, 3) = True
  aProcess(1, 4) = cwTypeWindow

  aProcess(2, 1) = PRWinBusMed
  aProcess(2, 2) = "Buscar Medicamentos"
  aProcess(2, 3) = False
  aProcess(2, 4) = cwTypeWindow
  
  aProcess(3, 1) = PRWinBusMat
  aProcess(3, 2) = "Buscar Material"
  aProcess(3, 3) = False
  aProcess(3, 4) = cwTypeWindow

  aProcess(4, 1) = PRWinVerPetOfe
  aProcess(4, 2) = "Introducir Ofertas Proveedores"
  aProcess(4, 3) = True
  aProcess(4, 4) = cwTypeWindow

  aProcess(5, 1) = PRWinIntOfe
  aProcess(5, 2) = "Introducir Ofertas"
  aProcess(5, 3) = False
  aProcess(5, 4) = cwTypeWindow

  aProcess(6, 1) = PRWinRecOfe
  aProcess(6, 2) = "Reclamar Ofertas"
  aProcess(6, 3) = True
  aProcess(6, 4) = cwTypeWindow
  
  aProcess(7, 1) = PRWinRecOfe
  aProcess(7, 2) = "Ver Datos Proveedor (Reclamar)"
  aProcess(7, 3) = False
  aProcess(7, 4) = cwTypeWindow

  aProcess(8, 1) = PRWinValOfe
  aProcess(8, 2) = "Valorar Ofertas"
  aProcess(8, 3) = True
  aProcess(8, 4) = cwTypeWindow

  aProcess(9, 1) = PRWinBusOfe
  aProcess(9, 2) = "Adjudicar Ofertas"
  aProcess(9, 3) = True
  aProcess(9, 4) = cwTypeWindow

  aProcess(10, 1) = PRWinRedPedCom
  aProcess(10, 2) = "Redactar Pedido Compra"
  aProcess(10, 3) = False
  aProcess(10, 4) = cwTypeWindow

  aProcess(11, 1) = FRWinGenPed
  aProcess(11, 2) = "Propuesta de Pedidos"
  aProcess(11, 3) = True
  aProcess(11, 4) = cwTypeWindow

  aProcess(12, 1) = FRRepPteFac
  aProcess(12, 2) = "Pte. Fac."
  aProcess(12, 3) = False
  aProcess(12, 4) = cwTypeReport

  aProcess(13, 1) = PRWinBusPed
  aProcess(13, 2) = "Buscar Pedidos"
  aProcess(13, 3) = False
  aProcess(13, 4) = cwTypeWindow

  aProcess(14, 1) = PRWinBusFac
  aProcess(14, 2) = "Buscar Facturas"
  aProcess(14, 3) = False
  aProcess(14, 4) = cwTypeWindow

  aProcess(15, 1) = PRWinRedSolCom
  aProcess(15, 2) = "Editar Solicitud Compra"
  aProcess(15, 3) = True
  aProcess(15, 4) = cwTypeWindow

  aProcess(16, 1) = PRWinRedPedCom1
  aProcess(16, 2) = "Realizar Pedido"
  aProcess(16, 3) = True
  aProcess(16, 4) = cwTypeWindow

  aProcess(17, 1) = PRWinEstSolCom
  aProcess(17, 2) = "Aprobar Solicitud Compra"
  aProcess(17, 3) = True
  aProcess(17, 4) = cwTypeWindow

  aProcess(18, 1) = PRWinBusSolCom
  aProcess(18, 2) = "Buscar Solicitudes de Compra"
  aProcess(18, 3) = False
  aProcess(18, 4) = cwTypeWindow

  aProcess(19, 1) = FRWinAsientos
  aProcess(19, 2) = "Asientos Contables"
  aProcess(19, 3) = True
  aProcess(19, 4) = cwTypeWindow

  aProcess(20, 1) = PRWinConSitPed
  aProcess(20, 2) = "Consultar Situaci�n Pedidos"
  aProcess(20, 3) = True
  aProcess(20, 4) = cwTypeWindow

  aProcess(21, 1) = PRWinRecPed
  aProcess(21, 2) = "Reclamar Pedido"
  aProcess(21, 3) = True
  aProcess(21, 4) = cwTypeWindow

  aProcess(22, 1) = PRWinEmiPed
  aProcess(22, 2) = "Emitir Pedido"
  aProcess(22, 3) = False
  aProcess(22, 4) = cwTypeWindow

  aProcess(23, 1) = PRWinIntRecCom
  aProcess(23, 2) = "Albaranes"
  aProcess(23, 3) = False
  aProcess(23, 4) = cwTypeWindow

  aProcess(24, 1) = FRRepPedido
  aProcess(24, 2) = "Pedido de Compra"
  aProcess(24, 3) = False
  aProcess(24, 4) = cwTypeReport

  aProcess(25, 1) = PRWinIntFacCom
  aProcess(25, 2) = "Consultar Facturas"
  aProcess(25, 3) = True
  aProcess(25, 4) = cwTypeWindow

  aProcess(26, 1) = FRWinListToAlb
  aProcess(26, 2) = "Lista de productos para el albar�n"
  aProcess(26, 3) = False
  aProcess(26, 4) = cwTypeWindow

  aProcess(27, 1) = PRWinLocEnt
  aProcess(27, 2) = "Localizar Entradas"
  aProcess(27, 3) = False
  aProcess(27, 4) = cwTypeWindow

  aProcess(28, 1) = PRWinRecFac
  aProcess(28, 2) = "Reclamar Factura"
  aProcess(28, 3) = False
  aProcess(28, 4) = cwTypeWindow

  aProcess(29, 1) = PRWinConEntPen
  aProcess(29, 2) = "Reclamar Mercanc�as"
  aProcess(29, 3) = True
  aProcess(29, 4) = cwTypeWindow

  aProcess(30, 1) = PRWinBusOfe1
  aProcess(30, 2) = "Buscar Ofertas"
  aProcess(30, 3) = False
  aProcess(30, 4) = cwTypeWindow
  
  aProcess(31, 1) = PRWinMensajeCorreo
  aProcess(31, 2) = "Enviar Mensaje de Correo"
  aProcess(31, 3) = False
  aProcess(31, 4) = cwTypeWindow
  
  aProcess(32, 1) = PRWinMantenimientoLotes
  aProcess(32, 2) = "Mantenimiento de Lotes"
  aProcess(32, 3) = False
  aProcess(32, 4) = cwTypeWindow
  
  aProcess(33, 1) = PRWinConExi
  aProcess(33, 2) = "Gestionar Existencias"
  aProcess(33, 3) = True
  aProcess(33, 4) = cwTypeWindow
  
  aProcess(34, 1) = FRWinDefProd
  aProcess(34, 2) = "Ver Ficha Producto"
  aProcess(34, 3) = False
  aProcess(34, 4) = cwTypeWindow
  
  aProcess(35, 1) = FRWinAbono
  aProcess(35, 2) = "Abonos"
  aProcess(35, 3) = True
  aProcess(35, 4) = cwTypeWindow
  
  aProcess(36, 1) = FRWinConSitAlb
  aProcess(36, 2) = "Consultar Albaranes"
  aProcess(36, 3) = True
  aProcess(36, 4) = cwTypeWindow
  
  aProcess(37, 1) = FRWinBusAlb
  aProcess(37, 2) = "Buscar Albaranes"
  aProcess(37, 3) = False
  aProcess(37, 4) = cwTypeWindow
                    
  aProcess(38, 1) = FRRepPropPep
  aProcess(38, 2) = "Listado Propuestas de Pedido"
  aProcess(38, 3) = False
  aProcess(38, 4) = cwTypeReport
  
  aProcess(39, 1) = FRRepBajoStock
  aProcess(39, 2) = "Listado Productos Bajo Stock"
  aProcess(39, 3) = False
  aProcess(39, 4) = cwTypeReport
  
  aProcess(40, 1) = FRRepEntFact
  aProcess(40, 2) = "Entradas Facturadas"
  aProcess(40, 3) = False
  aProcess(40, 4) = cwTypeReport
  
  aProcess(41, 1) = FRRepEntCC
  aProcess(41, 2) = "Entradas Por Cuenta Contable"
  aProcess(41, 3) = False
  aProcess(41, 4) = cwTypeReport

  aProcess(42, 1) = FRRepPedRea
  aProcess(42, 2) = "Pedidos Realizados"
  aProcess(42, 3) = False
  aProcess(42, 4) = cwTypeReport
  
  aProcess(43, 1) = FRWinModAlbaran
  aProcess(43, 2) = "Modificar Albaranes"
  aProcess(43, 3) = True
  aProcess(43, 4) = cwTypeWindow

  aProcess(44, 1) = FRRepPedRecla
  aProcess(44, 2) = "Pedidos Reclamados"
  aProcess(44, 3) = False
  aProcess(44, 4) = cwTypeReport

  aProcess(45, 1) = FRRepPedPendRecla
  aProcess(45, 2) = "Pedidos pendiente de Reclamar"
  aProcess(45, 3) = False
  aProcess(45, 4) = cwTypeReport
  
  aProcess(46, 1) = FRWinRealInventCompras
  aProcess(46, 2) = "Realizar Inventario"
  aProcess(46, 3) = False
  aProcess(46, 4) = cwTypeWindow
  
  aProcess(47, 1) = FRWinFaltas
  aProcess(47, 2) = "Faltas"
  aProcess(47, 3) = True
  aProcess(47, 4) = cwTypeWindow
  
  aProcess(48, 1) = FRWinImprimirEtiquetas
  aProcess(48, 2) = "Imprimir Etiquetas"
  aProcess(48, 3) = False
  aProcess(48, 4) = cwTypeWindow
  
  aProcess(49, 1) = FRWinEliminarFacturas
  aProcess(49, 2) = "Eliminar Facturas"
  aProcess(49, 3) = True
  aProcess(49, 4) = cwTypeWindow
  
  aProcess(50, 1) = FRRepABCPedPendRecla
  aProcess(50, 2) = "ABC Entradas por Cuenta Contable"
  aProcess(50, 3) = False
  aProcess(50, 4) = cwTypeReport
  
  aProcess(51, 1) = FRRepABCProveedores
  aProcess(51, 2) = "ABC Proveedores"
  aProcess(51, 3) = False
  aProcess(51, 4) = cwTypeReport
  
  aProcess(52, 1) = FRRepABCProductos
  aProcess(52, 2) = "ABC Productos"
  aProcess(52, 3) = False
  aProcess(52, 4) = cwTypeReport
  
 ' aProcess(53, 1) = FRReptxtAsientos
 ' aProcess(53, 2) = "txt Asientos Contables"
 ' aProcess(53, 3) = False
 ' aProcess(53, 4) = cwTypeReport
 
  aProcess(54, 1) = FRRepMaestroMedicacion
  aProcess(54, 2) = "Maestro Medicaci�n"
  aProcess(54, 3) = False
  aProcess(54, 4) = cwTypeReport
  
  aProcess(55, 1) = FRRepMaestroMaterial
  aProcess(55, 2) = "Maestro Material"
  aProcess(55, 3) = False
  aProcess(55, 4) = cwTypeReport
  
  aProcess(56, 1) = FRRepMaestroImplantes
  aProcess(56, 2) = "Maestro Implantes"
  aProcess(56, 3) = False
  aProcess(56, 4) = cwTypeReport
  
  aProcess(57, 1) = FRRepMaestroProductosporCuentaContable
  aProcess(57, 2) = "Maestro Productos por Cuenta Contable"
  aProcess(57, 3) = False
  aProcess(57, 4) = cwTypeReport
 

End Sub
