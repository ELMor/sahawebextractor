VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "comctl32.ocx"
Object = "{F6125AB1-8AB1-11CE-A77F-08002B2F4E98}#2.0#0"; "msrdc20.ocx"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Begin VB.Form FrmFaltas 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Faltas"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0570.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton cmdImprimir 
      Caption         =   "Imprimir"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   8160
      TabIndex        =   11
      Top             =   600
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Caption         =   "Faltas  "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   6735
      Left            =   240
      TabIndex        =   8
      Top             =   1200
      Width           =   11535
      Begin SSDataWidgets_B.SSDBGrid SSDBGrid1 
         Bindings        =   "FR0570.frx":000C
         Height          =   6240
         Left            =   240
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   360
         Width           =   11055
         _Version        =   131078
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         _ExtentX        =   19500
         _ExtentY        =   11007
         _StockProps     =   79
         Caption         =   "FALTAS"
      End
   End
   Begin VB.CommandButton cmdBuscar 
      BackColor       =   &H00FF0000&
      Caption         =   "BUSCAR"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   6480
      TabIndex        =   7
      Top             =   600
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   4440
      TabIndex        =   5
      Top             =   480
      Width           =   1815
      Begin VB.CheckBox chkServicio 
         Caption         =   "Por Servicio"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame fraOpcion 
      Caption         =   "Seleccione opci�n"
      ForeColor       =   &H00FF0000&
      Height          =   615
      Left            =   240
      TabIndex        =   2
      Top             =   480
      Width           =   4095
      Begin VB.OptionButton optQuir 
         Caption         =   "Quir./Anest."
         Height          =   255
         Left            =   2520
         TabIndex        =   10
         Top             =   240
         Width           =   1455
      End
      Begin VB.OptionButton optCestas 
         Caption         =   "Cestas"
         Height          =   195
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optPlantas 
         Caption         =   "Plantas"
         Height          =   255
         Left            =   1560
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin MSRDC.MSRDC MSRDC1 
      Height          =   450
      Left            =   10560
      Top             =   480
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   794
      _Version        =   327681
      Options         =   0
      CursorDriver    =   1
      BOFAction       =   0
      EOFAction       =   0
      RecordsetType   =   3
      LockType        =   1
      QueryType       =   0
      Prompt          =   3
      Appearance      =   1
      QueryTimeout    =   30
      RowsetSize      =   100
      LoginTimeout    =   15
      KeysetSize      =   0
      MaxRows         =   0
      ErrorThreshold  =   -1
      BatchSize       =   15
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Enabled         =   -1  'True
      ReadOnly        =   0   'False
      Appearance      =   -1  'True
      DataSourceName  =   ""
      RecordSource    =   ""
      UserName        =   ""
      Password        =   ""
      Connect         =   ""
      LogMessages     =   ""
      Caption         =   "MSRDC1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmFaltas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1

Private Sub cmdbuscar_Click()
    Screen.MousePointer = vbHourglass
    Dim sql As String
    Dim qryFTP As rdoQuery
    Dim rstFTP As rdoResultset
    If optCestas.Value = True Then ' b�squeda faltas de cestas
      If chkServicio.Value = 1 Then 'agrupar por servicio
        sql = "SELECT /*+ ORDERED INDEX (FR2800 FR2801) */"
        sql = sql & " fr7300.fr73codintfar CODIGO, "
        sql = sql & " FR7300.FR73DESPRODUCTO DESCRIPCION,"
        sql = sql & " FR7300.FR73REFERENCIA Refer,"
        sql = sql & " ad0200.ad02desdpto DPTO,"
        sql = sql & " SUM(FR2800.FR28CANTIDAD) Cant,"
        sql = sql & " FR7300.FR73CANTPEND Pend,"
        sql = sql & " FR7300.FR73PTEBONIF Bonif"
        sql = sql & " from fr6600,fr2800,fr7300,ad0200"
        sql = sql & " where "
        sql = sql & " FR6600.FR26CODESTPETIC = 9 and"
        sql = sql & " Fr6600.FR66CODPETICION = FR2800.FR66CODPETICION and "
        sql = sql & " FR2800.FR28CANTDISP = 0 and"
        sql = sql & " FR2800.FR28CANTIDAD <> 0 and"
        sql = sql & " FR2800.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO and "
        sql = sql & " Fr6600.ad02coddpto = ad0200.ad02coddpto"
        sql = sql & " group by fr7300.fr73codintfar,FR7300.FR73DESPRODUCTO,"
        sql = sql & " FR7300.FR73REFERENCIA,ad0200.ad02desdpto,"
        sql = sql & "FR7300.FR73CANTPEND,FR7300.FR73PTEBONIF"
        sql = sql & " order by FR7300.FR73DESPRODUCTO,ad0200.ad02desdpto"
      Else ' no agrupar por servicio
        sql = "SELECT /*+ ORDERED INDEX (FR2800 FR2801) */"
        sql = sql & " fr7300.fr73codintfar CODIGO,"
        sql = sql & " FR7300.FR73DESPRODUCTO DESCRIPCION,"
        sql = sql & " FR7300.FR73REFERENCIA Refer,"
        sql = sql & " SUM(FR2800.FR28CANTIDAD) Cant,"
        sql = sql & " FR7300.FR73CANTPEND Pend,"
        sql = sql & " FR7300.FR73PTEBONIF Bonif"
        sql = sql & " from fr6600,fr2800,fr7300"
        sql = sql & " where "
        sql = sql & " FR6600.FR26CODESTPETIC = 9 and"
        sql = sql & " Fr6600.FR66CODPETICION = FR2800.FR66CODPETICION and "
        sql = sql & " FR2800.FR28CANTDISP = 0 and"
        sql = sql & " FR2800.FR28CANTIDAD <> 0 and"
        sql = sql & " FR2800.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO "
        sql = sql & " group by fr7300.fr73codintfar,FR7300.FR73DESPRODUCTO,FR7300.FR73REFERENCIA,FR7300.FR73CANTPEND,FR7300.FR73PTEBONIF "
        sql = sql & " order by FR7300.FR73DESPRODUCTO"
      End If
    ElseIf optPlantas.Value = True Then ' b�squeda por m�dulos
      If chkServicio.Value = 1 Then 'agrupar por servicio
'        SQL = "SELECT /*+ ORDERED INDEX (FR1900 FR1901) */"
        sql = "SELECT "
        sql = sql & " fr7300.fr73codintfar CODIGO, "
        sql = sql & " FR7300.FR73DESPRODUCTO DESCRIPCION,"
        sql = sql & " FR7300.FR73REFERENCIA Refer,"
        sql = sql & " ad0200.ad02desdpto DPTO,"
        sql = sql & " SUM(FR1900.FR19CANTNECESQUIR) Cant,"
        sql = sql & " FR7300.FR73CANTPEND Pend,"
        sql = sql & " FR7300.FR73PTEBONIF Bonif"
        sql = sql & " from fr5500,fr1900,fr7300,ad0200"
        sql = sql & " where "
        sql = sql & " FR5500.FR26CODESTPETIC = 9 and"
        sql = sql & " Fr5500.FR55CODNECESUNID = FR1900.FR55CODNECESUNID and "
        sql = sql & " FR1900.FR19CANTSUMIFARM = 0 and"
        sql = sql & " FR1900.FR19CANTNECESQUIR <> 0 and"
        sql = sql & " FR1900.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO and "
        sql = sql & " Fr5500.ad02coddpto = ad0200.ad02coddpto"
        sql = sql & " group by fr7300.fr73codintfar,FR7300.FR73DESPRODUCTO,FR7300.FR73REFERENCIA,ad0200.ad02desdpto,FR7300.FR73CANTPEND,FR7300.FR73PTEBONIF "
        sql = sql & " order by FR7300.FR73DESPRODUCTO,ad0200.ad02desdpto"
      Else ' no agrupar por servicio
        sql = "SELECT "
        sql = sql & " fr7300.fr73codintfar CODIGO,"
        sql = sql & " FR7300.FR73DESPRODUCTO DESCRIPCION,"
        sql = sql & " FR7300.FR73REFERENCIA Refer,"
        sql = sql & " SUM(FR1900.FR19CANTNECESQUIR) Cant,"
        sql = sql & " FR7300.FR73CANTPEND Pend,"
        sql = sql & " FR7300.FR73PTEBONIF Bonif"
        sql = sql & " from fr5500,fr1900,fr7300"
        sql = sql & " where "
        sql = sql & " FR5500.FR26CODESTPETIC = 9 and"
        sql = sql & " Fr5500.FR55CODNECESUNID = FR1900.FR55CODNECESUNID and "
        sql = sql & " FR1900.FR19CANTSUMIFARM = 0 and"
        sql = sql & " FR1900.FR19CANTNECESQUIR <> 0 and "
        sql = sql & " FR1900.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO "
        sql = sql & " group by fr7300.fr73codintfar,FR7300.FR73DESPRODUCTO,FR7300.FR73REFERENCIA,FR7300.FR73CANTPEND,FR7300.FR73PTEBONIF "
        sql = sql & " order by FR7300.FR73DESPRODUCTO"
      End If
    ElseIf optQuir.Value = True Then ' b�squeda por quir�fano
        sql = "SELECT "
        sql = sql & " fr7300.fr73codintfar C�digo,"
        sql = sql & " FR7300.FR73DESPRODUCTO Descripci�n,"
        sql = sql & " FR7300.FR73REFERENCIA Refer,"
        sql = sql & " SUM(FRK500.FRK5CANTSUMIFARM) Cant,"
        sql = sql & " FR7300.FR73CANTPEND Pend,"
        sql = sql & " FR7300.FR73PTEBONIF Bonif"
        sql = sql & " From frk300,frk500,fr7300"
        sql = sql & " Where "
        sql = sql & " FRK300.FR26CODESTPETIC = 9 and"
        sql = sql & " FRK300.FRK3CODNECESUNID=FRK500.FRK3CODNECESUNID and"
        sql = sql & " FRK500.FRK5CANTULTFARM = 0 and"
        sql = sql & " FRK500.FRK5CANTSUMIFARM <> 0 and"
        sql = sql & " FRK500.FR73CODPRODUCTO=FR7300.FR73CODPRODUCTO "
        sql = sql & " Group By fr7300.fr73codintfar,fr7300.fr73desproducto,FR7300.FR73REFERENCIA,FR7300.FR73CANTPEND,FR7300.FR73PTEBONIF"
        sql = sql & " Order By fr7300.FR73DESPRODUCTO"
    End If
    Set qryFTP = objApp.rdoConnect.CreateQuery("", sql)
    Set rstFTP = qryFTP.OpenResultset(rdOpenKeyset, rdConcurReadOnly)
     
     SSDBGrid1.Redraw = False
     SSDBGrid1.Reset
     Set MSRDC1.Resultset = rstFTP
     If chkServicio.Value = 1 And optQuir.Value = False Then
       SSDBGrid1.Columns(0).Width = 900
       SSDBGrid1.Columns(1).Width = 5000
       SSDBGrid1.Columns(2).Width = 1500
       SSDBGrid1.Columns(3).Width = 1900
       SSDBGrid1.Columns(4).Width = 700
     Else
       SSDBGrid1.Columns(0).Width = 900
       SSDBGrid1.Columns(1).Width = 5000
       SSDBGrid1.Columns(2).Width = 1500
       SSDBGrid1.Columns(3).Width = 700
     End If
     SSDBGrid1.Redraw = True
     Screen.MousePointer = vbDefault
'     cmdImprimir.Enabled = True
'
End Sub



Private Sub cmdImprimir_Click()
    Dim strWhere As String
    strWhere = "1 = 1"
    If optCestas.Value = True Then ' b�squeda faltas de cestas
      Call Imprimir_API(strWhere, "FR0570.rpt", , , , True)
    ElseIf optPlantas.Value = True Then ' b�squeda por m�dulos
      Call Imprimir_API(strWhere, "FR0571.rpt", , , , True)
    ElseIf optQuir.Value = True Then ' b�squeda por quir�fano
      Call Imprimir_API(strWhere, "FR0572.rpt", , , , True)
    End If
End Sub





Private Sub Form_Activate()
  Call Inicializar_Toolbar
End Sub


Private Sub Form_Load()
    Dim objMasterInfo As New clsCWForm
    Dim objMultiInfo As New clsCWForm
    Dim strKey As String
    Dim stra As String
    Dim rsta As rdoResultset
    Dim strpaciente As String
  
    Set objWinInfo = New clsCWWin
    Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                    Me, tlbToolbar1, stbStatusBar1, _
                                    cwWithAll)
    
    cmdbuscar.Enabled = True
End Sub
'Private Sub pImprimirPeticion(ssGrid As SSDBGrid)
'    Dim i%, SQL$
'
'    If ssGrid.SelBookmarks.Count = 1 Then
'      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "Peticion.rpt"
'    Else
'      crtCrystalReport1.ReportFileName = objApp.strReportsPath & "PetAgrup.rpt"
'    End If
'    With crtCrystalReport1
'        .PrinterCopies = 1
'        .Destination = crptToPrinter
'        For i = 0 To ssGrid.SelBookmarks.Count - 1
'            SQL = SQL & "{PR0457J.PR04NUMACTPLAN}= " & ssGrid.Columns("Num. Act.").CellText(ssGrid.SelBookmarks(i)) & " OR "
'        Next i
'        SQL = Left$(SQL, Len(SQL) - 4)
'        .SelectionFormula = "(" & SQL & ")"
'        .Connect = objApp.rdoConnect.Connect
'        .DiscardSavedData = True
'        Screen.MousePointer = vbHourglass
'        .Action = 1
'        Screen.MousePointer = vbDefault
'    End With
'End Sub

Private Sub Inicializar_Toolbar()
   tlbToolbar1.Buttons.Item(2).Enabled = False
   tlbToolbar1.Buttons.Item(3).Enabled = False
   tlbToolbar1.Buttons.Item(4).Enabled = False
   tlbToolbar1.Buttons.Item(6).Enabled = False
   tlbToolbar1.Buttons.Item(8).Enabled = False
   tlbToolbar1.Buttons.Item(10).Enabled = False
   tlbToolbar1.Buttons.Item(11).Enabled = False
   tlbToolbar1.Buttons.Item(12).Enabled = False
   tlbToolbar1.Buttons.Item(14).Enabled = False
   tlbToolbar1.Buttons.Item(16).Enabled = False
   tlbToolbar1.Buttons.Item(18).Enabled = False
   tlbToolbar1.Buttons.Item(19).Enabled = False
   tlbToolbar1.Buttons.Item(28).Enabled = False
   mnuDatosOpcion(10).Enabled = False
   mnuDatosOpcion(20).Enabled = False
   mnuDatosOpcion(40).Enabled = False
   mnuDatosOpcion(60).Enabled = False
   mnuDatosOpcion(80).Enabled = False
   mnuEdicionOpcion(10).Enabled = False
   mnuEdicionOpcion(30).Enabled = False
   mnuEdicionOpcion(40).Enabled = False
   mnuEdicionOpcion(50).Enabled = False
   mnuEdicionOpcion(60).Enabled = False
   mnuEdicionOpcion(62).Enabled = False
   mnuEdicionOpcion(80).Enabled = False
   mnuEdicionOpcion(90).Enabled = False
   mnuFiltroOpcion(10).Enabled = False
   mnuFiltroOpcion(20).Enabled = False
   mnuRegistroOpcion(10).Enabled = False
   mnuRegistroOpcion(20).Enabled = False
   mnuOpcionesOpcion(20).Enabled = False
   mnuOpcionesOpcion(40).Enabled = False
   mnuOpcionesOpcion(50).Enabled = False
End Sub


Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
    Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub



Private Sub tlbtoolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
Select Case Button.Index
  Case 21:
          Call SSDBGrid1.MoveFirst
  Case 22:
          Call SSDBGrid1.MovePrevious
  Case 23:
          Call SSDBGrid1.MoveNext
  Case 24:
          Call SSDBGrid1.MoveLast
  Case 26:
          Call cmdbuscar_Click
  Case 30
    Unload Me
End Select
End Sub
