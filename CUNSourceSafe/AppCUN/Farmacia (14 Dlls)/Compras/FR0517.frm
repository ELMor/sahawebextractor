VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmRedPedCom1 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Realizar Pedido de Compra"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0517.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   45
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Buscadores"
      Height          =   735
      Left            =   1800
      TabIndex        =   41
      Top             =   7320
      Width           =   8175
      Begin VB.CommandButton cmdMasDatos 
         Caption         =   "M�&s Datos"
         Height          =   375
         Left            =   6120
         TabIndex        =   32
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cmdMedicamentos 
         Caption         =   "&Medicamentos"
         Height          =   375
         Left            =   4200
         TabIndex        =   31
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdOfertas 
         Caption         =   "Ofer&tas"
         Height          =   375
         Left            =   2280
         TabIndex        =   30
         Top             =   240
         Width           =   1815
      End
      Begin VB.CommandButton cmdSolicitud 
         Caption         =   "Solicitud de &Compra"
         Height          =   375
         Left            =   360
         TabIndex        =   29
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3825
      Index           =   1
      Left            =   0
      TabIndex        =   40
      Top             =   3480
      Width           =   11865
      Begin TabDlg.SSTab tabTab1 
         Height          =   3300
         Index           =   1
         Left            =   120
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   360
         Width           =   11655
         _ExtentX        =   20558
         _ExtentY        =   5821
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0517.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(18)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(17)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(16)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(15)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(14)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(2)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(3)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(4)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(9)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "lblLabel1(56)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "lblLabel1(57)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "lblLabel1(12)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "lblLabel1(1)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "lblLabel1(0)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "lblLabel1(5)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "lblLabel1(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "lblLabel1(19)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "lblLabel1(20)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "lblLabel1(21)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "dtcDateCombo1(0)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "txtText1(11)"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(13)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(10)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).Control(24)=   "txtText1(12)"
         Tab(0).Control(24).Enabled=   0   'False
         Tab(0).Control(25)=   "txtText1(14)"
         Tab(0).Control(25).Enabled=   0   'False
         Tab(0).Control(26)=   "txtText1(0)"
         Tab(0).Control(26).Enabled=   0   'False
         Tab(0).Control(27)=   "txtText1(2)"
         Tab(0).Control(27).Enabled=   0   'False
         Tab(0).Control(28)=   "txtText1(3)"
         Tab(0).Control(28).Enabled=   0   'False
         Tab(0).Control(29)=   "txtText1(15)"
         Tab(0).Control(29).Enabled=   0   'False
         Tab(0).Control(30)=   "txtText1(17)"
         Tab(0).Control(30).Enabled=   0   'False
         Tab(0).Control(31)=   "txtText1(18)"
         Tab(0).Control(31).Enabled=   0   'False
         Tab(0).Control(32)=   "txtText1(19)"
         Tab(0).Control(32).Enabled=   0   'False
         Tab(0).Control(33)=   "txtText1(22)"
         Tab(0).Control(33).Enabled=   0   'False
         Tab(0).Control(34)=   "txtText1(23)"
         Tab(0).Control(34).Enabled=   0   'False
         Tab(0).Control(35)=   "txtText1(24)"
         Tab(0).Control(35).Enabled=   0   'False
         Tab(0).Control(36)=   "txtbonificacion"
         Tab(0).Control(36).Enabled=   0   'False
         Tab(0).Control(37)=   "txtText1(1)"
         Tab(0).Control(37).Enabled=   0   'False
         Tab(0).Control(38)=   "txtprecioneto"
         Tab(0).Control(38).Enabled=   0   'False
         Tab(0).Control(39)=   "txtText1(25)"
         Tab(0).Control(39).Enabled=   0   'False
         Tab(0).Control(40)=   "txtText1(26)"
         Tab(0).Control(40).Enabled=   0   'False
         Tab(0).Control(41)=   "txtText1(27)"
         Tab(0).Control(41).Enabled=   0   'False
         Tab(0).Control(42)=   "txtText1(28)"
         Tab(0).Control(42).Enabled=   0   'False
         Tab(0).Control(43)=   "Frame5"
         Tab(0).Control(43).Enabled=   0   'False
         Tab(0).Control(44)=   "txtCodInt1(24)"
         Tab(0).Control(44).Enabled=   0   'False
         Tab(0).Control(45)=   "txtCodInt1(23)"
         Tab(0).Control(45).Enabled=   0   'False
         Tab(0).Control(46)=   "txtText1(4)"
         Tab(0).Control(46).Enabled=   0   'False
         Tab(0).ControlCount=   47
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0517.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25UDESBONIF"
            Height          =   330
            HelpContextID   =   40102
            Index           =   4
            Left            =   3600
            MaxLength       =   13
            TabIndex        =   75
            TabStop         =   0   'False
            Tag             =   "UniBonif"
            Top             =   1560
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtCodInt1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   960
            Locked          =   -1  'True
            MaxLength       =   1
            TabIndex        =   7
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   360
            Width           =   300
         End
         Begin VB.TextBox txtCodInt1 
            BackColor       =   &H00FFFFFF&
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   120
            Locked          =   -1  'True
            MaxLength       =   6
            TabIndex        =   5
            Tag             =   "C�d.Interno"
            Top             =   360
            Width           =   720
         End
         Begin VB.Frame Frame5 
            BorderStyle     =   0  'None
            Height          =   855
            Left            =   3840
            TabIndex        =   74
            Top             =   1800
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   28
            Left            =   4200
            Locked          =   -1  'True
            TabIndex        =   72
            TabStop         =   0   'False
            Tag             =   "Recargo"
            Top             =   2040
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR88CODTIPIVA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   27
            Left            =   1560
            TabIndex        =   23
            Tag             =   "IVA"
            Top             =   2760
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25BONIF"
            Height          =   330
            HelpContextID   =   30104
            Index           =   26
            Left            =   1560
            TabIndex        =   22
            Tag             =   "Bonificaci�n (%)"
            Top             =   2160
            Width           =   700
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25DESCUENTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   25
            Left            =   1560
            TabIndex        =   21
            Tag             =   "Descuento (%)"
            Top             =   1560
            Width           =   700
         End
         Begin VB.TextBox txtprecioneto 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25PRECNET"
            Height          =   330
            HelpContextID   =   30104
            Left            =   3600
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Precio Neto"
            Top             =   960
            Width           =   1185
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR25TAMENVASE"
            Height          =   330
            HelpContextID   =   30104
            Index           =   1
            Left            =   10200
            TabIndex        =   14
            Tag             =   "U.E"
            Top             =   360
            Width           =   1065
         End
         Begin VB.TextBox txtbonificacion 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   40102
            Left            =   120
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Bonificaci�n|Unidades que nos entregan en concepto de bonificaci�n"
            Top             =   1560
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   24
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   27
            TabStop         =   0   'False
            Tag             =   "C�d.Interno"
            Top             =   2640
            Visible         =   0   'False
            Width           =   720
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   23
            Left            =   9000
            Locked          =   -1  'True
            TabIndex        =   28
            TabStop         =   0   'False
            Tag             =   "Seg"
            Top             =   2640
            Visible         =   0   'False
            Width           =   300
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25preciounidad"
            Height          =   330
            HelpContextID   =   30104
            Index           =   22
            Left            =   1560
            TabIndex        =   15
            Tag             =   "Precio Unidad"
            Top             =   960
            Width           =   1785
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25coddetpedcomp"
            Height          =   330
            HelpContextID   =   40102
            Index           =   19
            Left            =   6840
            MaxLength       =   13
            TabIndex        =   25
            TabStop         =   0   'False
            Tag             =   "C�digo Detalle Pedido"
            Top             =   2160
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40102
            Index           =   18
            Left            =   6120
            MaxLength       =   13
            TabIndex        =   24
            TabStop         =   0   'False
            Tag             =   "C�dido Pedido"
            Top             =   2160
            Visible         =   0   'False
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "frh8moneda"
            Height          =   330
            HelpContextID   =   40102
            Index           =   17
            Left            =   7320
            MaxLength       =   13
            TabIndex        =   18
            TabStop         =   0   'False
            Tag             =   "Moneda"
            Top             =   960
            Width           =   615
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr25imporlinea"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   5040
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Importe"
            Top             =   960
            Width           =   2055
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr25cantpedida"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   120
            TabIndex        =   6
            Tag             =   "Cantidad|N�mero de envases"
            Top             =   960
            Width           =   1215
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1560
            TabIndex        =   8
            TabStop         =   0   'False
            Tag             =   "Descripci�n Prod"
            Top             =   360
            Width           =   3570
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr73codproducto"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   6600
            TabIndex        =   26
            TabStop         =   0   'False
            Tag             =   "C�d.Prod"
            Top             =   2520
            Visible         =   0   'False
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   8400
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   360
            Width           =   1665
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   6960
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "Volumen"
            Top             =   360
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   5760
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   5160
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "F.F"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   6360
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "U.M"
            Top             =   360
            Width           =   540
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2880
            Index           =   1
            Left            =   -74880
            TabIndex        =   38
            TabStop         =   0   'False
            Top             =   240
            Width           =   11055
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19500
            _ExtentY        =   5080
            _StockProps     =   79
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR25FECPLAZENTRE"
            Height          =   330
            Index           =   0
            Left            =   8400
            TabIndex        =   19
            Tag             =   "Plazo Entrega"
            Top             =   960
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Recargo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   21
            Left            =   4200
            TabIndex        =   73
            Top             =   1800
            Width           =   735
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "IVA (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   1560
            TabIndex        =   71
            Top             =   2520
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonificaci�n (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   1560
            TabIndex        =   70
            Top             =   1920
            Width           =   1380
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descuento (%)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   1560
            TabIndex        =   69
            Top             =   1320
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Neto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   3600
            TabIndex        =   68
            Top             =   720
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.E"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   10200
            TabIndex        =   67
            Top             =   120
            Width           =   330
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Bonif.(Udes)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   66
            Top             =   1320
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   1560
            TabIndex        =   65
            Top             =   120
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d.Int"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   57
            Left            =   120
            TabIndex        =   64
            Top             =   120
            Width           =   630
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Seg"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   56
            Left            =   960
            TabIndex        =   63
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Plazo Entrega"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   8400
            TabIndex        =   62
            Top             =   720
            Width           =   1785
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   7320
            TabIndex        =   60
            Top             =   720
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Importe"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   5040
            TabIndex        =   56
            Top             =   720
            Width           =   645
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   55
            Top             =   720
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Base - PVL"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   1560
            TabIndex        =   54
            Top             =   720
            Width           =   1560
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   8400
            TabIndex        =   53
            Top             =   120
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Vol. (ml)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   6960
            TabIndex        =   52
            Top             =   120
            Width           =   705
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   6360
            TabIndex        =   51
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   5760
            TabIndex        =   50
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   5160
            TabIndex        =   49
            Top             =   120
            Width           =   345
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pedido de Compra"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2925
      Index           =   0
      Left            =   0
      TabIndex        =   37
      Top             =   480
      Width           =   11865
      Begin VB.Frame Frame4 
         Height          =   1335
         Left            =   10080
         TabIndex        =   61
         Top             =   720
         Width           =   1575
         Begin VB.CommandButton cmdAceptar 
            Caption         =   "&Aceptar"
            Height          =   375
            Left            =   120
            TabIndex        =   33
            Top             =   240
            Width           =   1335
         End
         Begin VB.CommandButton cmdRechazar 
            Caption         =   "Recha&zar"
            Height          =   375
            Left            =   120
            TabIndex        =   34
            Top             =   840
            Width           =   1335
         End
      End
      Begin TabDlg.SSTab tabTab1 
         Height          =   2415
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   360
         Width           =   9810
         _ExtentX        =   17304
         _ExtentY        =   4260
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0517.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(8)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(10)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(11)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "txtText1(21)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "txtText1(20)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(1)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(6)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(7)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(8)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(9)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(16)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "Frame2"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).ControlCount=   13
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0517.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'None
            Height          =   615
            Left            =   7080
            TabIndex        =   57
            Top             =   240
            Width           =   2175
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr95codestpedcompra"
            Height          =   330
            HelpContextID   =   40102
            Index           =   16
            Left            =   7920
            MaxLength       =   13
            TabIndex        =   42
            Tag             =   "Estado"
            Top             =   360
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   1440
            TabIndex        =   3
            TabStop         =   0   'False
            Tag             =   "Proveedor"
            Top             =   960
            Width           =   5850
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr79codproveedor"
            Height          =   330
            HelpContextID   =   40102
            Index           =   8
            Left            =   240
            TabIndex        =   2
            Tag             =   "C�d.Prov"
            Top             =   960
            Width           =   975
         End
         Begin VB.TextBox txtText1 
            DataField       =   "fr62descpersonalizada"
            Height          =   735
            Index           =   7
            Left            =   240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   4
            Tag             =   "Descripci�n Personalizada"
            Top             =   1560
            Width           =   8640
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H0000FFFF&
            DataField       =   "fr62codpedcompra"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   240
            TabIndex        =   0
            Tag             =   "N� Pedido"
            Top             =   360
            Width           =   1150
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2130
            Index           =   0
            Left            =   -74910
            TabIndex        =   35
            Top             =   90
            Width           =   9285
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   16378
            _ExtentY        =   3757
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "fr62fecpedcompra"
            Height          =   330
            Index           =   1
            Left            =   1800
            TabIndex        =   1
            Tag             =   "Fecha Pedido"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "sg02cod_pid"
            Height          =   330
            HelpContextID   =   40102
            Index           =   20
            Left            =   8520
            MaxLength       =   13
            TabIndex        =   58
            Tag             =   "C�digo Peticionario"
            Top             =   360
            Width           =   495
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00FFFFFF&
            DataField       =   "fr62indrepre"
            Height          =   330
            HelpContextID   =   40102
            Index           =   21
            Left            =   7320
            MaxLength       =   13
            TabIndex        =   59
            Tag             =   "Representante"
            Top             =   360
            Width           =   495
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   240
            TabIndex        =   48
            Top             =   720
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   240
            TabIndex        =   47
            Top             =   1320
            Width           =   1020
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   1800
            TabIndex        =   46
            Top             =   120
            Width           =   1185
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "N� Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   44
            Top             =   120
            Width           =   870
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   43
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmRedPedCom1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FR0517.FRM                                                   *
'* AUTOR: JUAN CARLOS RUEDA GARC�A                                      *
'* FECHA: ABRIL 1999                                                    *
'* DESCRIPCION: Introducir Pedido de Compra                             *
'* ARGUMENTOS:                                                          *
'* ACTUALIZACIONES:                                                     *
'************************************************************************
Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnError As Boolean 'True si hay errores al salvar el detalle de la petici�n

Private Sub cmdAceptar_Click()
  Dim rsta As rdoResultset
  Dim sqlstr As String
  Dim strDP As String
  Dim qryDP As rdoQuery
  Dim rstDP As rdoResultset
   
  Me.Enabled = False
  If IsNumeric(txtText1(6).Text) Then
      If IsNumeric(txtText1(8).Text) Then
        If IsNumeric(txtText1(19).Text) Then
          gintProveedor(1) = txtText1(8).Text
          gintProveedor(2) = txtText1(6).Text
          gstrLlamador = "FrmEstPedCom"
          Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
          objWinInfo.DataSave
          'Se comprueba que las cantidades pedidas sean mayores que cero
            strDP = "SELECT * " & _
                    "  FROM FR2500 " & _
                    " WHERE ((FR25CANTPEDIDA = ?) OR (FR25CANTPEDIDA IS NULL)) " & _
                    "   AND FR62CODPEDCOMPRA = ? "
            Set qryDP = objApp.rdoConnect.CreateQuery("", strDP)
            qryDP(0) = 0
            qryDP(1) = txtText1(6).Text
            Set rstDP = qryDP.OpenResultset()
            If Not rstDP.EOF Then
              'En el detalle del pedido hay cantidades pedidas igual a 0 � vacias
              MsgBox "En el detalle del pedido hay cantidades pedidas igual a 0. Reviselas.", vbInformation, "Enviar Pedido"
              qryDP.Close
              Set qryDP = Nothing
              Set rstDP = Nothing
              Me.Enabled = True
              Exit Sub
            End If
            qryDP.Close
            Set qryDP = Nothing
            Set rstDP = Nothing
          If mblnError = False Then
            sqlstr = "SELECT FR95CODESTPEDCOMPRA FROM FR6200 WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text
            Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
            If Not rsta.EOF Then
              If rsta(0).Value = 2 Then
                Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
                Call objWinInfo.DataRefresh
              End If
            End If
            rsta.Close
            Set rsta = Nothing
            Call objsecurity.LaunchProcess("FR0524")
          Else
            Me.Enabled = True
            Exit Sub
          End If
          
        Else
          Call MsgBox("El Pedido no tiene ning�n producto", vbExclamation, "Aviso")
        End If
      Else
          Call MsgBox("No hay ning�n Proveedor", vbExclamation, "Aviso")
      End If
  Else
    Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
  End If
  Me.Enabled = True
End Sub

Private Sub cmdMasDatos_Click()
    tabTab1(1).Tab = 0
    cmdMasDatos.Enabled = False
    If Not IsNumeric(txtText1(0).Text) Then
      MsgBox "Debe seleccionar un producto apretando el bot�n 'Buscar'", vbInformation, "M�s Datos"
    Else
      gstrLlamador = "FrmConExi"
      gstrCP = txtText1(0).Text
      Call objsecurity.LaunchProcess("FR0535")
    End If
    cmdMasDatos.Enabled = True
    objWinInfo.DataSave
    objWinInfo.DataRefresh
End Sub

Private Sub cmdRechazar_Click()
   Dim strupdate As String
    
   Me.Enabled = False
   If IsNumeric(txtText1(6).Text) Then
      'strupdate = "UPDATE FR6200 SET FR62FECESTUPED=to_date(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy')," & _
                  "FR95CODESTPEDCOMPRA=0 where fr62codpedcompra=" & txtText1(6).Text
      strupdate = "UPDATE FR6200 SET FR62FECESTUPED=FR62FECPEDCOMPRA," & _
                  "FR95CODESTPEDCOMPRA=0 where fr62codpedcompra=" & txtText1(6).Text
      objApp.rdoConnect.Execute strupdate, 64
      MsgBox "Pedido Rechazado", vbExclamation, "Aviso"
      Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
      Call objWinInfo.DataRefresh
      'Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
   Else
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
   End If
   Me.Enabled = True
End Sub


Private Sub cmdMedicamentos_Click()
  Dim aux1
  Dim rsta As rdoResultset
  Dim stra As String
  Dim intInd As Integer

    cmdMedicamentos.Enabled = False
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If Not IsNumeric(txtText1(6).Text) Or Not IsNumeric(txtText1(8).Text) Then
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
      cmdMedicamentos.Enabled = True
      Exit Sub
    End If
    
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
      If tlbToolbar1.Buttons(4).Enabled = True Then
        cmdMedicamentos.Enabled = True
        Exit Sub
      End If
    End If
    
    gstrLlamador = "FrmGenPetOfe" 'se hace lo mismo, buscar por proveedor
    gintFR501Prov = txtText1(8).Text
    gintCont = 0
    Call objsecurity.LaunchProcess("FR0502")
    gstrLlamador = ""
    gintFR501Prov = ""
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If gintCont > 0 Then
     For intInd = 1 To gintCont
      gintprodbuscado = gaListPrd(intInd, 1)
      objWinInfo.DataSave
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
      Call objWinInfo.CtrlSet(txtText1(0), gintprodbuscado)
      'Call objWinInfo.CtrlSet(txtText1(22), txtText1(4).Text)
      Call calcular_precio_unidad
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintprodbuscado
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) Then
            Call objWinInfo.CtrlSet(txtText1(1), rsta.rdoColumns("FR73TAMENVASE").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73DESCUENTO").Value) Then
            Call objWinInfo.CtrlSet(txtText1(25), rsta.rdoColumns("FR73DESCUENTO").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73BONIFICACION").Value) Then
            Call objWinInfo.CtrlSet(txtText1(26), rsta.rdoColumns("FR73BONIFICACION").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then
            Call objWinInfo.CtrlSet(txtText1(27), rsta.rdoColumns("FR88CODTIPIVA").Value)
        End If
        'If Not IsNull(rsta.rdoColumns("FR73DESCUENTO").Value) Then
        '  Call objWinInfo.CtrlSet(txtText1(28), rsta.rdoColumns("FR73DESCUENTO").Value)
        'Else
          Call objWinInfo.CtrlSet(txtText1(28), 0)
        'End If
        If Not IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) And _
           Not IsNull(rsta.rdoColumns("FR73DESVIACION").Value) Then
           If rsta.rdoColumns("FR73TAMENVASE").Value > 0 And _
              rsta.rdoColumns("FR73DESVIACION").Value < 0 Then
              Call objWinInfo.CtrlSet(txtText1(3), (rsta.rdoColumns("FR73DESVIACION").Value \ rsta.rdoColumns("FR73TAMENVASE").Value) * (-1))
           Else
             If rsta.rdoColumns("FR73DESVIACION").Value > 0 Then
               Call objWinInfo.CtrlSet(txtText1(3), 0)
             Else
               Call objWinInfo.CtrlSet(txtText1(3), 0)
             End If
           End If
        Else
          Call objWinInfo.CtrlSet(txtText1(3), 1)
        End If
      End If
      
      rsta.Close
      Set rsta = Nothing
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
        'aux1 = txtText1(22) * txtText1(3)
        Call calcular_precio_neto
        aux1 = txtprecioneto * txtText1(3)
        If InStr(aux1, ",") = 0 Then
          txtText1(15).Text = aux1
        Else
          txtText1(15).Text = Format(aux1, "0.###")
        End If
      End If
      Call calcular_precio_neto
      'txtText1(0).SetFocus
      txtText1(3).SetFocus
     Next intInd
    End If
    
    cmdMedicamentos.Enabled = True
    objWinInfo.DataSave
    objWinInfo.DataRefresh
End Sub

Private Sub cmdOfertas_Click()
Dim v As Integer
Dim aux1
Dim rsta As rdoResultset
Dim stra As String

    cmdOfertas.Enabled = False
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If Not IsNumeric(txtText1(6).Text) Or Not IsNumeric(txtText1(8).Text) Then
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
      cmdOfertas.Enabled = True
      Exit Sub
    End If
    
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
      If tlbToolbar1.Buttons(4).Enabled = True Then
        cmdOfertas.Enabled = True
        Exit Sub
      End If
    End If
    
    gstrLlamador = "FrmRedPedCom1"
    gintFR501Prov = txtText1(8).Text
    gstrLlamadorOferta = "Introducir Pedido"
    ReDim gintprodbuscado1(0)
    gintprodtotal = 0
    Call objsecurity.LaunchProcess("FR0532")
    gstrLlamadorOferta = ""
    gstrLlamador = ""
    gintFR501Prov = ""
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    For v = 1 To gintprodtotal
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
      Call objWinInfo.CtrlSet(txtText1(0), gintprodbuscado1(v, 0))
      'Call objWinInfo.CtrlSet(txtText1(22), txtText1(4).Text)
      Call calcular_precio_unidad
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintprodbuscado1(v, 0)
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) Then
            Call objWinInfo.CtrlSet(txtText1(1), rsta.rdoColumns("FR73TAMENVASE").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73DESCUENTO").Value) Then
            Call objWinInfo.CtrlSet(txtText1(25), rsta.rdoColumns("FR73DESCUENTO").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73BONIFICACION").Value) Then
            Call objWinInfo.CtrlSet(txtText1(26), rsta.rdoColumns("FR73BONIFICACION").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then
            Call objWinInfo.CtrlSet(txtText1(27), rsta.rdoColumns("FR88CODTIPIVA").Value)
        End If
      End If
      rsta.Close
      Set rsta = Nothing
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
        'aux1 = txtText1(22) * txtText1(3)
        Call calcular_precio_neto
        aux1 = txtprecioneto * txtText1(3)
        If InStr(aux1, ",") = 0 Then
          txtText1(15).Text = aux1
        Else
          txtText1(15).Text = Format(aux1, "0.###")
        End If
      End If
      Call calcular_precio_neto
      'txtText1(0).SetFocus
      txtText1(3).SetFocus
    Next v
    gintprodtotal = 0
    cmdOfertas.Enabled = True
    
End Sub

Private Sub cmdSolicitud_Click()
Dim v As Integer
Dim aux1
Dim rsta As rdoResultset
Dim stra As String

    cmdSolicitud.Enabled = False
    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    If Not IsNumeric(txtText1(6).Text) Or Not IsNumeric(txtText1(8).Text) Then
      Call MsgBox("Debe tener un pedido", vbInformation, "Aviso")
      cmdSolicitud.Enabled = True
      Exit Sub
    End If
    
    If tlbToolbar1.Buttons(4).Enabled = True Then
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
      If tlbToolbar1.Buttons(4).Enabled = True Then
        cmdSolicitud.Enabled = True
        Exit Sub
      End If
    End If
    
    gstrLlamador = "FrmRedPedCom1"
    gintFR501Prov = txtText1(8).Text
    ReDim gintprodbuscado1(0)
    Call objsecurity.LaunchProcess("FR0519")
    gstrLlamador = ""
    gintFR501Prov = ""
    
    For v = 1 To gintprodtotal
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
      Call objWinInfo.CtrlSet(txtText1(0), gintprodbuscado1(v, 0))
      'Call objWinInfo.CtrlSet(txtText1(22), txtText1(4).Text)
      Call calcular_precio_unidad
      stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & gintprodbuscado1(v, 0)
      Set rsta = objApp.rdoConnect.OpenResultset(stra)
      If Not rsta.EOF Then
        If Not IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) Then
            Call objWinInfo.CtrlSet(txtText1(1), rsta.rdoColumns("FR73TAMENVASE").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73DESCUENTO").Value) Then
            Call objWinInfo.CtrlSet(txtText1(25), rsta.rdoColumns("FR73DESCUENTO").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR73BONIFICACION").Value) Then
            Call objWinInfo.CtrlSet(txtText1(26), rsta.rdoColumns("FR73BONIFICACION").Value)
        End If
        If Not IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then
            Call objWinInfo.CtrlSet(txtText1(27), rsta.rdoColumns("FR88CODTIPIVA").Value)
        End If
      End If
      rsta.Close
      Set rsta = Nothing
      If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
        'aux1 = txtText1(22) * txtText1(3)
        Call calcular_precio_neto
        aux1 = txtprecioneto * txtText1(3)
        If InStr(aux1, ",") = 0 Then
          txtText1(15).Text = aux1
        Else
          txtText1(15).Text = Format(aux1, "0.###")
        End If
      End If
      Call calcular_precio_neto
      'txtText1(0).SetFocus
      txtText1(3).SetFocus
    Next v
    gintprodtotal = 0
    cmdSolicitud.Enabled = True
    
End Sub


Private Sub Form_Activate()
  Screen.MousePointer = vbDefault
  txtText1(3).SelStart = 0
  txtText1(3).SelLength = Len(txtText1(3).Text)
  If gblnDescargarPantalla = True Then
    gblnDescargarPantalla = False
    Unload Me
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String

  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Pedidos de Compra"
    .strTable = "FR6200"

    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
   
    strKey = .strDataBase & .strTable
    If gstrLlamadorFac <> "PropPed" Then
      .strWhere = "FR95CODESTPEDCOMPRA=1"
    ElseIf gstrLlamadorFac = "PropPed" Then
      .strWhere = "FR95CODESTPEDCOMPRA=1 AND FR62CODPEDCOMPRA=" & gstrCodPed
      .intAllowance = cwAllowDelete + cwAllowModify
    End If
    
    .blnAskPrimary = False
    
    Call .FormCreateFilterWhere(strKey, "Pedidos de Compra")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR62DESCPERSONALIZADA", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR62FECPEDCOMPRA", "Fecha Pedido", cwDate)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "Proveedor", cwNumeric)
        
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR62DESCPERSONALIZADA", "Descripci�n")
    Call .FormAddFilterOrder(strKey, "FR62FECPEDCOMPRA", "Fecha Pedido")
    Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "Proveedor")
  End With
  
  With objDetailInfo
    .strName = "Detalle Petici�n"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FR2500"
    
    .blnAskPrimary = False
    .intCursorSize = 100
    Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
    Call .FormAddOrderField("FR25CODDETPEDCOMP", cwAscending)
    
    Call .FormAddRelation("FR62CODPEDCOMPRA", txtText1(6))
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Pedido")
    Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25CANTPEDIDA", "Cantidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25PRECIOUNIDAD", "Precio Unidad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25IMPORLINEA", "Importe de Linea", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR25FECPLAZENTRE", "Fecha Plazo Entrega", cwDate)
    
    Call .FormAddFilterOrder(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido")
    Call .FormAddFilterOrder(strKey, "FR25CODDETPEDCOMP", "C�digo Detalle Pedido")
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
  End With
   
   
   With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
    
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    
    .CtrlGetInfo(txtText1(20)).blnInGrid = False
    .CtrlGetInfo(txtText1(21)).blnInGrid = False
    .CtrlGetInfo(txtText1(16)).blnInGrid = False
  
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(17)).blnForeign = True
    .CtrlGetInfo(txtText1(27)).blnForeign = True
    .CtrlGetInfo(txtText1(17)).blnReadOnly = True
    .CtrlGetInfo(txtText1(0)).blnForeign = True
    .CtrlGetInfo(txtText1(15)).blnReadOnly = True
    
    .CtrlGetInfo(txtbonificacion).blnNegotiated = False
    .CtrlGetInfo(txtbonificacion).blnReadOnly = True
    '.CtrlGetInfo(txtprecioneto).blnNegotiated = False
    .CtrlGetInfo(txtprecioneto).blnReadOnly = True
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(13), "FRH7CODFORMFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FR73DOSIS")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR93CODUNIMEDIDA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "FR73VOLUMEN")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(14), "FR73REFERENCIA")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(4), "FR73PRECIONETCOMPRA")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(24), "FR73CODINTFAR")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(23), "FR73CODINTFARSEG")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(28), "FR73RECARGO")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "FR73DESPRODUCTOPROV")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7314J WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(4), "PRECIOUNIDAD")
    
    .CtrlGetInfo(txtCodInt1(24)).blnNegotiated = False
    .CtrlGetInfo(txtCodInt1(24)).intType = cwString
    .CtrlGetInfo(txtCodInt1(23)).blnNegotiated = False
    .CtrlGetInfo(txtCodInt1(23)).intType = cwNumeric
    .CtrlGetInfo(txtCodInt1(23)).blnReadOnly = True
    
    Call .WinRegister
    Call .WinStabilize
  End With
  If gstrLlamadorFac = "PropPed" Then
    objWinInfo.DataMoveFirst
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)

Select Case intKeyAscii
Case 27, 115
  intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Select

End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
                         
Select Case intKeyCode
Case 27, 115
  intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Select

If (intKeyCode = 33 Or intKeyCode = 34) Then 'And objwininfo.strName  = "Detalle Petici�n" Then
  If intKeyCode = 34 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, 23, 0)
  End If
  If intKeyCode = 33 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, 22, 0)
  End If
  txtText1(3).SelStart = 0
  txtText1(3).SelLength = Len(txtText1(3).Text)
  txtText1(3).SetFocus
End If

End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostChangeForm(ByVal strFormName As String)

If strFormName = "Detalle Petici�n" Then
  tlbToolbar1.Buttons(2).Visible = False
  tlbToolbar1.Buttons(3).Visible = False
  mnuDatosOpcion(10).Visible = False
  mnuDatosOpcion(20).Visible = False
Else
  tlbToolbar1.Buttons(2).Visible = True
  tlbToolbar1.Buttons(3).Visible = True
  mnuDatosOpcion(10).Visible = True
  mnuDatosOpcion(20).Visible = True
End If

End Sub


Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

If txtText1(0).Text <> "" Then
    grdDBGrid1(0).Columns(1).Width = 1000
    grdDBGrid1(0).Columns(2).Width = 1100
    grdDBGrid1(0).Columns(3).Width = 800
    grdDBGrid1(0).Columns(4).Width = 1300
End If

If grdDBGrid1(1).Rows > 0 Then
  grdDBGrid1(1).Columns("F.F").Position = 1
  grdDBGrid1(1).Columns("F.F").Width = 2776
  grdDBGrid1(1).Columns("Cantidad").Position = 2
  grdDBGrid1(1).Columns("Cantidad").Width = 825
  grdDBGrid1(1).Columns("U.E").Position = 3
  grdDBGrid1(1).Columns("U.E").Width = 660
  grdDBGrid1(1).Columns("Precio Unidad").Position = 4
  grdDBGrid1(1).Columns("Precio Unidad").Width = 1215
  grdDBGrid1(1).Columns("Importe").Position = 5
  grdDBGrid1(1).Columns("Importe").Width = 916
  grdDBGrid1(1).Columns("Moneda").Position = 6
  grdDBGrid1(1).Columns("Moneda").Width = 735
  grdDBGrid1(1).Columns("Plazo Entrega").Position = 7
  grdDBGrid1(1).Columns("Plazo Entrega").Width = 1141
  grdDBGrid1(1).Columns("Descuento (%)").Position = 8
  grdDBGrid1(1).Columns("Descuento (%)").Width = 976
  grdDBGrid1(1).Columns("Bonificaci�n (%)").Position = 9
  grdDBGrid1(1).Columns("Bonificaci�n (%)").Width = 1020
  grdDBGrid1(1).Columns("IVA").Position = 10
  grdDBGrid1(1).Columns("IVA").Width = 391
  grdDBGrid1(1).Columns("C�dido Pedido").Visible = False
  'grdDBGrid1(1).Columns("C�dido Pedido").Width = 1696
  grdDBGrid1(1).Columns("C�digo Detalle Pedido").Visible = False
  'grdDBGrid1(1).Columns("C�digo Detalle Pedido").Width = 2730
  grdDBGrid1(1).Columns("C�d.Prod").Visible = False
  'grdDBGrid1(1).Columns("C�d.Prod").Width = 1170
   txtbonificacion.Text = txtText1(4).Text
End If

End Sub




Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim strIVA As String
  Dim qryIVA As rdoQuery
  Dim rstIVA As rdoResultset
  
  If IsNumeric(txtText1(27).Text) And strFormName = "Detalle Petici�n" Then
    'Se comprueba que el IVA sea correcto
    strIVA = "SELECT * FROM FR8800 WHERE FR88CODTIPIVA = ?"
    Set qryIVA = objApp.rdoConnect.CreateQuery("", strIVA)
    qryIVA(0) = txtText1(27).Text
    Set rstIVA = qryIVA.OpenResultset()
    If rstIVA.EOF = True Then
      'El tipo de iva introducido por el usuario no es v�lido
      MsgBox "El IVA no es v�lido." & Chr(13) & Chr(13) & _
             "Dispone de una lista asociada para rellenar este valor", vbInformation, "Realizar Pedido"
      mblnError = True
      blnCancel = True
    Else
      mblnError = False
    End If
    qryIVA.Close
    'rstIVA.Close
    Set rstIVA = Nothing
  Else
    mblnError = False
  End If
  If IsNumeric(txtText1(3).Text) And strFormName = "Detalle Petici�n" Then
    mblnError = False
  Else
    If Not IsNumeric(txtText1(3).Text) And strFormName = "Detalle Petici�n" Then
      mblnError = True
    Else
      mblnError = False
    End If
  End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Departamentos Realizadores" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
  'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
  
  If strFormName = "Pedidos de Compra" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"
     '.strWhere = "WHERE FR73FECFINVIG IS NULL" 'AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     .strOrder = "ORDER BY FR79CODPROVEEDOR ASC"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo del Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n del Proveedor "

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
  End If
  
  If strFormName = "Detalle Petici�n" And strCtrl = "txtText1(17)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH800"
     .strOrder = "ORDER BY FRH8MONEDA ASC"

     Set objField = .AddField("FRH8MONEDA")
     objField.strSmallDesc = "Moneda"

     Set objField = .AddField("FRH8FACTCONVVSEURO")
     objField.strSmallDesc = "Factor de Conversi�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(17), .cllValues("FRH8MONEDA"))
     End If
   End With
  End If
  
  If strFormName = "Detalle Petici�n" And strCtrl = "txtText1(27)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR8800"
     .strOrder = "ORDER BY FR88CODTIPIVA ASC"

     Set objField = .AddField("FR88CODTIPIVA")
     objField.strSmallDesc = "C�digo"

     Set objField = .AddField("FR88DESCIVA")
     objField.strSmallDesc = "Descripci�n"
     
     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(27), .cllValues("FR88CODTIPIVA"))
     End If
   End With
  End If
  
  If strFormName = "Detalle Petici�n" And strCtrl = "txtText1(0)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7300"
     .strWhere = "WHERE FR73FECFINVIG IS NULL AND FR73FECINIVIG<(SELECT SYSDATE FROM DUAL)"
     If txtText1(8).Text <> "" Then
      .strWhere = " AND FR79CODPROVEEDOR_A=" & txtText1(8).Text & " OR FR79CODPROVEEDOR_B=" & txtText1(8).Text & " OR FR79CODPROVEEDOR_C=" & txtText1(8).Text
     End If
     .strOrder = "ORDER BY FR73DESPRODUCTOPROV ASC"

     Set objField = .AddField("FR73CODPRODUCTO")
     objField.strSmallDesc = "C�d.Producto"

     Set objField = .AddField("FR73CODINTFAR")
     objField.strSmallDesc = "C�d.Int.Farmacia"

     Set objField = .AddField("FR73CODINTFARSEG")
     objField.strSmallDesc = "C�d.Seg.Farmacia"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "Forma Farmace�tica"

     Set objField = .AddField("FR73DOSIS")
     objField.strSmallDesc = "Dosis"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad Medida"

     Set objField = .AddField("FR73VOLUMEN")
     objField.strSmallDesc = "Volumen"

     Set objField = .AddField("FR73REFERENCIA")
     objField.strSmallDesc = "Referencia"

     Set objField = .AddField("FR73PRECIONETCOMPRA")
     objField.strSmallDesc = "Precio Neto Compra"
     objField.blnInGrid = False

     Set objField = .AddField("FRH8MONEDA")
     objField.strSmallDesc = "Moneda"
     objField.blnInGrid = False

     If .Search Then
       Call objWinInfo.CtrlSet(txtText1(0), .cllValues("FR73CODPRODUCTO"))
       Call objWinInfo.CtrlSet(txtText1(22), .cllValues("FR73PRECIONETCOMPRA"))
       Call objWinInfo.CtrlSet(txtText1(17), .cllValues("FRH8MONEDA"))
     End If
   End With
  End If


End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub


Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)
If Index = 1 And tabTab1(1).Tab = 0 Then
  txtText1(3).SelStart = 0
  txtText1(3).SelLength = Len(txtText1(3).Text)
  txtText1(3).SetFocus
End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim sqlstr As String
Dim rsta As rdoResultset
Dim linea As Long
Dim rstFec As rdoResultset
Dim strFec As String
Dim rstcontrolOP As rdoResultset
Dim strcontrolOP As String
  
  Select Case btnButton.Index
    Case 2, 3, 16, 18, 19, 21, 22, 23, 24:
      If objWinInfo.objWinActiveForm.strName = "Pedidos de Compra" Then
        If txtText1(6).Text <> "" And txtText1(19).Text = "" Then
          Call MsgBox("El Pedido no tiene ning�n producto, debe borrarlo o introducir productos", vbExclamation, "Aviso")
          Exit Sub
        End If
      End If
    Case 30:
        If txtText1(6).Text <> "" And txtText1(19).Text = "" Then
          Call MsgBox("El Pedido no tiene ning�n producto, debe borrarlo o introducir productos", vbExclamation, "Aviso")
          Exit Sub
        End If
  End Select
  Select Case btnButton.Index
    Case 2 'Nuevo
        Select Case objWinInfo.objWinActiveForm.strName
            Case "Pedidos de Compra"
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                sqlstr = "SELECT FR62CODPEDCOMPRA_SEQUENCE.nextval FROM dual"
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                Call objWinInfo.CtrlSet(txtText1(6), rsta.rdoColumns(0).Value)
                rsta.Close
                Set rsta = Nothing
                strFec = "(SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL)"
                Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
                Call objWinInfo.CtrlSet(dtcDateCombo1(1), rstFec.rdoColumns(0).Value)
                Call objWinInfo.CtrlSet(txtText1(16), 1)
                Call objWinInfo.CtrlSet(txtText1(21), 1)
                Call objWinInfo.CtrlSet(txtText1(20), objsecurity.strUser)
                rstFec.Close
                Set rstFec = Nothing
                Exit Sub
            Case "Detalle Petici�n"
                Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
                sqlstr = "SELECT MAX(FR25CODDETPEDCOMP) FROM FR2500 WHERE FR62CODPEDCOMPRA=" & txtText1(6).Text
                Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
                If IsNull(rsta.rdoColumns(0).Value) Then
                  linea = 1
                Else
                  linea = rsta.rdoColumns(0).Value + 1
                End If
                strFec = "(SELECT TO_CHAR(SYSDATE+2,'DD/MM/YYYY') FROM DUAL)"
                Set rstFec = objApp.rdoConnect.OpenResultset(strFec)
                Call objWinInfo.CtrlSet(dtcDateCombo1(0), rstFec.rdoColumns(0).Value)
                rstFec.Close
                Set rstFec = Nothing
                Call objWinInfo.CtrlSet(txtText1(19), linea)
                Call objWinInfo.CtrlSet(txtText1(17), strMonedaActual)
                rsta.Close
                Set rsta = Nothing
        End Select
    Case Else
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    End Select
  If btnButton.Index = 21 Or btnButton.Index = 22 Or btnButton.Index = 23 Or btnButton.Index = 24 Then
    txtText1(3).SelStart = 0
    txtText1(3).SelLength = Len(txtText1(3).Text)
  End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
  Select Case intIndex
  Case 10
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
  Case 20
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
  Case 40
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
  Case 60
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
  Case 80
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
  Case 100
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
  End Select

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   If intIndex = 1 Then
    txtText1(3).SelStart = 0
    txtText1(3).SelLength = Len(txtText1(3).Text)
    txtText1(3).SetFocus
   End If
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
  Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
  Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  'If (intIndex = 0) And (txtText1(18).Text <> "") And (txtText1(19).Text <> "") Then
  '  txtText1(1).SetFocus
  'End If
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
  
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub






Private Sub txtbonificacion_Change()
  Call objWinInfo.CtrlSet(txtText1(4), txtbonificacion.Text)
End Sub

Private Sub txtCodInt1_Change(Index As Integer)
Dim CodInt As Long
Dim SUMA As Integer
Dim SEIS As Integer
Dim CINCO As Integer
Dim CUATRO As Integer
Dim TRES As Integer
Dim DOS As Integer
Dim UNO As Integer
   
   'If IsNumeric(txtCodInt1(24).Text) Then
   ' CodInt = txtCodInt1(24).Text
   ' If Len(txtCodInt1(24).Text) <> 0 Then
   '    If Len(txtCodInt1(24).Text) <> 6 Then
   '    Else
   '       UNO = Left(Right(CodInt, 1), 1)
   '       DOS = Left(Right(CodInt, 2), 1)
   '       TRES = Left(Right(CodInt, 3), 1)
   '       CUATRO = Left(Right(CodInt, 4), 1)
   '       CINCO = Left(Right(CodInt, 5), 1)
   '       SEIS = Left(Right(CodInt, 6), 1)
   '       SUMA = SEIS * 7 + CINCO * 6 + CUATRO * 5 + TRES * 4 + DOS * 3 + UNO * 2
   '       SUMA = 11 - (SUMA Mod 11)
          'txtCodInt1(23).Text = SUMA
   '    End If
   ' End If
  'End If

End Sub

Private Sub txtCodInt1_GotFocus(Index As Integer)

    Call objWinInfo.FormChangeActive(fraFrame1(1), False, True)
    txtCodInt1(24).SetFocus

End Sub

Private Sub txtCodInt1_KeyPress(Index As Integer, KeyAscii As Integer)
Dim stra As String
Dim rsta As rdoResultset
Dim aux1

  If txtText1(6).Text = "" Then
    KeyAscii = 0
    Exit Sub
  End If

  If tlbToolbar1.Buttons(4).Enabled = True Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4))
    If tlbToolbar1.Buttons(4).Enabled = True Then
      KeyAscii = 0
      Exit Sub
    Else
      txtCodInt1(24).SetFocus
    End If
  End If

  'Borrar campos
  txtText1(2).Text = ""
  txtText1(13).Text = ""
  txtText1(10).Text = ""
  txtText1(11).Text = ""
  txtText1(12).Text = ""
  txtText1(14).Text = ""
  txtText1(1).Text = ""
  txtText1(3).Text = ""
  txtText1(22).Text = ""
  txtText1(15).Text = ""
  txtText1(17).Text = ""
  txtText1(25).Text = ""
  txtText1(26).Text = ""
  txtText1(27).Text = ""
  txtText1(18).Text = ""
  txtText1(19).Text = ""
  'txtText1(4).Text = ""
  txtText1(0).Text = ""
  dtcDateCombo1(0).Text = ""
  txtprecioneto.Text = ""
  txtbonificacion.Text = ""

  Select Case Index
  Case 24
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case 8
      'RETROCESO, para poder borrar
    Case 13
      'INTRO, nuevo registro
      If Len(txtCodInt1(24).Text) <> 6 Then
         'el c�digo interno debe tener 6 d�gitos
         MsgBox "El C�digo Interno debe tener 6 d�gitos.", vbInformation, "Aviso"
         txtCodInt1(24).SetFocus
         Exit Sub
      Else
        stra = "SELECT * FROM FR7300 WHERE FR73CODINTFAR='" & txtCodInt1(24).Text & "' AND FR73FECINIVIG < SYSDATE AND FR73FECFINVIG IS NULL "
        Set rsta = objApp.rdoConnect.OpenResultset(stra)
        If Not rsta.EOF Then
         If Not IsNull(rsta.rdoColumns("FR73CODPRODUCTO").Value) Then
          'NUEVO
          Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
          Call objWinInfo.CtrlSet(txtText1(0), rsta.rdoColumns("FR73CODPRODUCTO").Value)
          stra = "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=" & rsta.rdoColumns("FR73CODPRODUCTO").Value
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          'Call objWinInfo.CtrlSet(txtText1(22), txtText1(4).Text)
          Call calcular_precio_unidad
          Dim curDes As Currency
          If Not IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) Then
              Call objWinInfo.CtrlSet(txtText1(1), rsta.rdoColumns("FR73TAMENVASE").Value)
          End If
          If Not IsNull(rsta.rdoColumns("FR73DESVIACION").Value) Then
            If IsNull(rsta.rdoColumns("FR73TAMENVASE").Value) Then
              curDes = rsta.rdoColumns("FR73DESVIACION").Value
              If curDes < 0 Then
                Call objWinInfo.CtrlSet(txtText1(3), curDes * -1)
              Else
                Call objWinInfo.CtrlSet(txtText1(3), 0)
              End If
            Else
              If rsta.rdoColumns("FR73TAMENVASE").Value > 0 Then
                curDes = Fix(rsta.rdoColumns("FR73DESVIACION").Value / rsta.rdoColumns("FR73TAMENVASE").Value)
                If curDes < 0 Then
                  Call objWinInfo.CtrlSet(txtText1(3), curDes * -1)
                Else
                  Call objWinInfo.CtrlSet(txtText1(3), 0)
                End If
              Else
                curDes = rsta.rdoColumns("FR73DESVIACION").Value
                If curDes < 0 Then
                  Call objWinInfo.CtrlSet(txtText1(3), curDes * -1)
                Else
                  Call objWinInfo.CtrlSet(txtText1(3), 0)
                End If
              End If
            End If
          Else
            Call objWinInfo.CtrlSet(txtText1(3), 0)
          End If
          If Not IsNull(rsta.rdoColumns("FR73CODINTFARSEG").Value) Then
              Call objWinInfo.CtrlSet(txtCodInt1(23), rsta.rdoColumns("FR73CODINTFARSEG").Value)
          End If
          If Not IsNull(rsta.rdoColumns("FR73DESCUENTO").Value) Then
              Call objWinInfo.CtrlSet(txtText1(25), rsta.rdoColumns("FR73DESCUENTO").Value)
          End If
          If Not IsNull(rsta.rdoColumns("FR73BONIFICACION").Value) Then
              Call objWinInfo.CtrlSet(txtText1(26), rsta.rdoColumns("FR73BONIFICACION").Value)
          End If
          If Not IsNull(rsta.rdoColumns("FR88CODTIPIVA").Value) Then
              Call objWinInfo.CtrlSet(txtText1(27), rsta.rdoColumns("FR88CODTIPIVA").Value)
          End If
          If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
            'aux1 = txtText1(22) * txtText1(3)
            Call calcular_precio_neto
            aux1 = txtprecioneto * txtText1(3)
            If InStr(aux1, ",") = 0 Then
              txtText1(15).Text = aux1
            Else
              txtText1(15).Text = Format(aux1, "0.###")
            End If
          End If
          Call calcular_precio_neto
          'txtText1(0).SetFocus
          txtText1(3).SetFocus
          
         Else
          MsgBox "El C�digo Interno: " & txtCodInt1(24).Text & " " & txtCodInt1(23).Text & " no existe.", vbInformation, "Aviso"
          txtCodInt1(24).SetFocus
          rsta.Close
          Set rsta = Nothing
          Exit Sub
         End If
         rsta.Close
         Set rsta = Nothing
        Else
         MsgBox "El C�digo Interno: " & txtCodInt1(24).Text & " " & txtCodInt1(23).Text & " no existe.", vbInformation, "Aviso"
         txtCodInt1(24).SetFocus
         rsta.Close
         Set rsta = Nothing
         Exit Sub
        End If
        
      End If
      
    Case Else
      KeyAscii = 0
    End Select
  End Select
  

End Sub

Private Sub txtprecioneto_Change()
Dim aux1

    If IsNumeric(txtprecioneto) And IsNumeric(txtText1(3).Text) Then
      aux1 = txtprecioneto * txtText1(3)
      If InStr(aux1, ",") = 0 Then
        txtText1(15).Text = aux1
      Else
        txtText1(15).Text = Format(aux1, "0.###")
      End If
    End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)

  'DECIMALES
  Select Case Index
  Case 3, 15, 22
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), Asc(",")
    Case 8
      'RETROCESO, para poder borrar
    Case Else
      KeyAscii = 0
    End Select
  End Select
  
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim aux1

  Call objWinInfo.CtrlLostFocus
  If intIndex = 22 Or intIndex = 3 Then
    If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
        'aux1 = txtText1(22) * txtText1(3)
        Call calcular_precio_neto
        aux1 = txtprecioneto * txtText1(3)
        If InStr(aux1, ",") = 0 Then
          txtText1(15).Text = aux1
        Else
          txtText1(15).Text = Format(aux1, "0.###")
        End If
    End If
  End If

  'If (intIndex = 3) And (txtText1(18).Text <> "") And (txtText1(19).Text <> "") Then
  '  dtcDateCombo1(0).SetFocus
  'End If
  'If (intIndex = 1) And (txtText1(18).Text <> "") And (txtText1(19).Text <> "") Then
  '  txtCodInt1(24).SetFocus
  'End If
  
  'If intIndex = 0 And txtText1(0).Text <> "" Then
  '  If txtText1(4).Text <> "" Then
  '    If txtText1(22).Text = "" Then
  '      txtText1(22).Text = txtText1(4).Text
  '    End If
  '  End If
  'End If

End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim rsta As rdoResultset
Dim sqlstr As String
Dim aux1

  Call objWinInfo.CtrlDataChange
  
  If intIndex = 0 And tlbToolbar1.Buttons(4).Enabled = True Then
    txtbonificacion.Text = ""
    txtprecioneto.Text = ""
    txtText1(3).Text = ""
    txtText1(22).Text = ""
    txtText1(15).Text = ""
    Call calcular_precio_neto
  End If
  
  If intIndex = 22 Or intIndex = 3 Then
    If IsNumeric(txtText1(22).Text) And IsNumeric(txtText1(3).Text) Then
      'aux1 = txtText1(22) * txtText1(3)
      Call calcular_precio_neto
      aux1 = txtprecioneto * txtText1(3)
      If InStr(aux1, ",") = 0 Then
        txtText1(15).Text = aux1
      Else
        txtText1(15).Text = Format(aux1, "0.###")
      End If
    End If
   End If
  
  'si cambia la %bonificaci�n que cambie la bonificaci�n o el tama�o del envase
  If (intIndex = 26) Or (intIndex = 1) Or (intIndex = 3) Then
    If IsNumeric(txtText1(3).Text) And IsNumeric(txtText1(26).Text) Then
      If IsNumeric(txtText1(1).Text) Then
        txtbonificacion.Text = Fix(((txtText1(3).Text * txtText1(26).Text) / 100) * txtText1(1).Text)
      Else
        txtbonificacion.Text = Fix((txtText1(3).Text * txtText1(26).Text) / 100)
      End If
    Else
        txtbonificacion.Text = 0
    End If
  End If
  
  'cuando se introduce la cantidad de producto a pedir, se calcula la bonificaci�n
  If intIndex = 3 And txtText1(0).Text <> "" Then
    If txtText1(3).Text <> "" Then
      sqlstr = "SELECT FR73BONIFICACION, FR73TAMENVASE FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
      Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
      If Not rsta.EOF Then
       If Not IsNull(rsta.rdoColumns(0).Value) Then
        If Not IsNull(rsta.rdoColumns(1).Value) Then
          txtbonificacion.Text = Fix(((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100) * rsta.rdoColumns(1).Value)
        Else
          txtbonificacion.Text = Fix((txtText1(3).Text * rsta.rdoColumns(0).Value) / 100)
        End If
       Else
            txtbonificacion.Text = 0
       End If
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If

  'If intIndex = 4 Then
  '  If txtText1(22).Text = "" Then
  '    txtText1(22).Text = txtText1(4).Text
  '  End If
  'End If
  
  'si cambia el descuento, el IVA o el precio_unidad hay que actualizar el precio neto
  If intIndex = 25 Or intIndex = 27 Or intIndex = 22 Then
        Call calcular_precio_neto
  End If

  If intIndex = 24 Then
    txtCodInt1(24).Text = txtText1(24).Text
  End If
  If intIndex = 23 Then
    txtCodInt1(23).Text = txtText1(23).Text
  End If


End Sub

Private Sub calcular_precio_neto()
Dim Precio_Neto As Currency
Dim Precio_Base As Currency
Dim Descuento As Currency
Dim IVA As Currency
Dim Recargo As Currency
Dim PVL As Currency
Dim PVP As Currency
Dim Param_Gen As Currency
Dim stra As String
Dim rsta As rdoResultset
Dim strPrecNetComp As String
Dim strupdate As String
Dim strPrecVenta As String

'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
If txtText1(22).Text <> "" Then 'Precio_Base
  Precio_Base = txtText1(22).Text
Else
  Precio_Base = 0
End If
If txtText1(25).Text <> "" Then 'Descuento
  Descuento = Precio_Base * txtText1(25).Text / 100
Else
  Descuento = 0
End If
If txtText1(28).Text <> "" Then 'Recargo
  Recargo = Precio_Base * txtText1(28).Text / 100
Else
  Recargo = 0
End If

If txtText1(27).Text <> "" Then 'IVA
  IVA = (Precio_Base - Descuento + Recargo) * txtText1(27).Text / 100
Else
  IVA = 0
End If
'If txtText1(28).Text <> "" Then 'Recargo
'  Recargo = Precio_Base * txtText1(28).Text / 100
'Else
'  Recargo = 0
'End If
Precio_Neto = Precio_Base - Descuento + IVA + Recargo
If txtprecioneto.Text <> Format(Precio_Neto, "0.00") Then
    txtprecioneto.Text = Format(Precio_Neto, "0.00")
End If

End Sub


Private Sub calcular_precio_unidad()
Dim rsta As rdoResultset
Dim sqlstr As String
          
  sqlstr = "SELECT FR73PRECBASE FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text & " AND FR00CODGRPTERAP LIKE 'Q%'"
  Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
  If Not rsta.EOF Then
    If IsNull(rsta(0).Value) Then
      Call objWinInfo.CtrlSet(txtText1(22), "")
    Else
      Call objWinInfo.CtrlSet(txtText1(22), rsta(0).Value)
    End If
    rsta.Close
    Set rsta = Nothing
  Else
    rsta.Close
    Set rsta = Nothing
    sqlstr = "SELECT FR73pvl FROM FR7300 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    If Not rsta.EOF Then
      If IsNull(rsta(0).Value) Then
        Call objWinInfo.CtrlSet(txtText1(22), "")
      Else
        Call objWinInfo.CtrlSet(txtText1(22), rsta(0).Value)
      End If
      rsta.Close
      Set rsta = Nothing
    End If
  End If

End Sub

