VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmBusOfe 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Buscar Ofertas"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "FR0509.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   36
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame Frame1 
      Caption         =   "Criterios de B�squeda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   975
      Left            =   120
      TabIndex        =   56
      Top             =   480
      Width           =   11655
      Begin VB.Frame Frame2 
         Caption         =   "Buscarlo en"
         ForeColor       =   &H00008000&
         Height          =   615
         Left            =   5640
         TabIndex        =   57
         Top             =   240
         Width           =   3135
         Begin VB.CheckBox chkproducto 
            Caption         =   "Producto"
            DataField       =   "sal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   120
            TabIndex        =   24
            Top             =   240
            Width           =   1425
         End
         Begin VB.CheckBox chkproveedor 
            Caption         =   "Proveedor"
            DataField       =   "sal"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1680
            TabIndex        =   25
            Top             =   240
            Width           =   1425
         End
      End
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "&Buscar"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   9240
         TabIndex        =   26
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox txtbuscar 
         Height          =   330
         Left            =   240
         TabIndex        =   23
         Top             =   480
         Width           =   5175
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Texto a Buscar "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   195
         Index           =   7
         Left            =   240
         TabIndex        =   58
         Top             =   240
         Width           =   1365
      End
   End
   Begin VB.CommandButton cmdGenerar 
      Caption         =   "&Generar Pedido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4920
      TabIndex        =   22
      Top             =   7680
      Width           =   2055
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2595
      Index           =   1
      Left            =   120
      TabIndex        =   31
      Top             =   5040
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   2145
         Index           =   1
         Left            =   135
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   360
         Width           =   11430
         _ExtentX        =   20161
         _ExtentY        =   3784
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0509.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(9)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(0)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(14)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(15)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(16)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(17)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(18)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(19)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(2)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(0)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(14)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(12)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(10)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(13)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(11)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(3)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(20)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(21)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(16)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0509.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH8MONEDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   16
            Left            =   2160
            TabIndex        =   19
            Tag             =   "Moneda"
            Top             =   960
            Width           =   705
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR23CODDETOFERTA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   21
            Left            =   5880
            TabIndex        =   55
            Tag             =   "Codigo Detalle Oferta"
            Top             =   960
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR58CODOFERTPROV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   20
            Left            =   4920
            TabIndex        =   54
            Tag             =   "Codigo Oferta"
            Top             =   960
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR23OBSERVACIONES"
            Height          =   495
            Index           =   3
            Left            =   240
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   21
            Tag             =   "Observaciones"
            Top             =   1560
            Width           =   10680
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR93CODUNIMEDIDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8880
            Locked          =   -1  'True
            TabIndex        =   16
            TabStop         =   0   'False
            Tag             =   "Unidad de Medida"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH7CODFORMFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   7680
            Locked          =   -1  'True
            TabIndex        =   14
            TabStop         =   0   'False
            Tag             =   "Forma Farmace�tica"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DOSIS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   8280
            Locked          =   -1  'True
            TabIndex        =   15
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   360
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73VOLUMEN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   9480
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Volumen"
            Top             =   360
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73REFERENCIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   7680
            Locked          =   -1  'True
            TabIndex        =   20
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   960
            Width           =   3225
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr73codproducto"
            Height          =   330
            HelpContextID   =   30101
            Index           =   0
            Left            =   240
            TabIndex        =   12
            Tag             =   "C�digo Producto"
            Top             =   360
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTOPROV"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   1800
            TabIndex        =   13
            Tag             =   "Descripci�n Producto"
            Top             =   360
            Width           =   5850
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR23PRECFERTADO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   240
            TabIndex        =   18
            Tag             =   "Precio Ofertado"
            Top             =   960
            Width           =   1785
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   1800
            Index           =   1
            Left            =   -74880
            TabIndex        =   30
            TabStop         =   0   'False
            Top             =   240
            Width           =   10815
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19076
            _ExtentY        =   3175
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Moneda"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   19
            Left            =   2160
            TabIndex        =   60
            Top             =   720
            Width           =   690
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   18
            Left            =   7680
            TabIndex        =   49
            Top             =   120
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   17
            Left            =   8280
            TabIndex        =   48
            Top             =   120
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   16
            Left            =   8880
            TabIndex        =   47
            Top             =   120
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   15
            Left            =   9480
            TabIndex        =   46
            Top             =   120
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   7680
            TabIndex        =   45
            Top             =   720
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   240
            TabIndex        =   35
            Top             =   120
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   240
            TabIndex        =   34
            Top             =   1320
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Ofertado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   240
            TabIndex        =   33
            Top             =   720
            Width           =   1350
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Oferta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3320
      Index           =   0
      Left            =   120
      TabIndex        =   28
      Top             =   1560
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   2825
         HelpContextID   =   9050
         Index           =   0
         Left            =   120
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   360
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   4974
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0509.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(8)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(1)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(11)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(12)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(13)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(3)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "dtcDateCombo1(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "dtcDateCombo1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "dtcDateCombo1(1)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(7)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(1)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(5)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(6)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(8)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(9)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(15)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(17)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).Control(21)=   "Frame3"
         Tab(0).Control(21).Enabled=   0   'False
         Tab(0).Control(22)=   "txtText1(19)"
         Tab(0).Control(22).Enabled=   0   'False
         Tab(0).Control(23)=   "txtText1(18)"
         Tab(0).Control(23).Enabled=   0   'False
         Tab(0).ControlCount=   24
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0509.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr58codofertprov"
            Height          =   330
            HelpContextID   =   30104
            Index           =   18
            Left            =   6240
            TabIndex        =   52
            Tag             =   "Codigo Oferta"
            Top             =   120
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr99codestofertprov"
            Height          =   330
            HelpContextID   =   30104
            Index           =   19
            Left            =   7200
            TabIndex        =   53
            Tag             =   "Estado Oferta"
            Top             =   120
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.Frame Frame3 
            BorderStyle     =   0  'None
            Height          =   495
            Left            =   6120
            TabIndex        =   59
            Top             =   120
            Width           =   1695
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30104
            Index           =   17
            Left            =   120
            TabIndex        =   0
            Tag             =   "Estado Oferta"
            Top             =   360
            Width           =   3105
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr58valoracion"
            Height          =   330
            HelpContextID   =   30104
            Index           =   15
            Left            =   3360
            TabIndex        =   1
            Tag             =   "Valoraci�n"
            Top             =   360
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   30101
            Index           =   9
            Left            =   1680
            TabIndex        =   11
            Tag             =   "Descripci�n Proveedor"
            Top             =   2400
            Width           =   8865
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr79codproveedor"
            Height          =   330
            HelpContextID   =   30101
            Index           =   8
            Left            =   120
            TabIndex        =   10
            Tag             =   "C�digo Proveedor"
            Top             =   2400
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR58RECARGOFINANC"
            Height          =   330
            HelpContextID   =   30101
            Index           =   6
            Left            =   9120
            TabIndex        =   9
            Tag             =   "Recargos"
            Top             =   1800
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr58embalajes"
            Height          =   330
            HelpContextID   =   30101
            Index           =   5
            Left            =   7560
            TabIndex        =   8
            Tag             =   "Embalajes"
            Top             =   1800
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr58portes"
            Height          =   330
            HelpContextID   =   30101
            Index           =   1
            Left            =   6000
            TabIndex        =   7
            Tag             =   "Portes"
            Top             =   1800
            Width           =   1425
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "fr58desoferta"
            Height          =   615
            Index           =   7
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Tag             =   "Descripci�n Oferta"
            Top             =   960
            Width           =   10440
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2610
            Index           =   0
            Left            =   -74910
            TabIndex        =   27
            Top             =   120
            Width           =   10965
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19341
            _ExtentY        =   4604
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "fr58fecrecepofert"
            Height          =   330
            Index           =   1
            Left            =   120
            TabIndex        =   4
            Tag             =   "Fecha Recepci�n"
            Top             =   1785
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   12632256
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "fr58feciniofert"
            Height          =   330
            Index           =   2
            Left            =   2040
            TabIndex        =   5
            Tag             =   "Fecha Inicio"
            Top             =   1785
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   12632256
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "fr58fecfinofert"
            Height          =   330
            Index           =   3
            Left            =   3960
            TabIndex        =   6
            Tag             =   "Fecha Fin"
            Top             =   1785
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   12632256
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            BackColorSelected=   12632256
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Valoraci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   3360
            TabIndex        =   51
            Top             =   120
            Width           =   915
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   50
            Top             =   120
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   120
            TabIndex        =   44
            Top             =   2160
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Recargos"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   9120
            TabIndex        =   43
            Top             =   1560
            Width           =   825
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Embalajes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   7560
            TabIndex        =   42
            Top             =   1560
            Width           =   870
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Portes"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   6000
            TabIndex        =   41
            Top             =   1560
            Width           =   555
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Fin"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   3960
            TabIndex        =   40
            Top             =   1560
            Width           =   855
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Inicio"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   2040
            TabIndex        =   39
            Top             =   1560
            Width           =   1065
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Recepci�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   38
            Top             =   1560
            Width           =   1515
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n de la Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   37
            Top             =   720
            Width           =   2085
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   32
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmBusOfe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusOfe(FR0509.FRM)                                        *
'* AUTOR: JUAN CARLOS RUEDA                                             *
'* FECHA: ABRIL DE 1999                                                 *
'* DESCRIPCION: buscar ofertas                                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Private Sub cmdbuscar_Click()
'Dim rsta As rdoResultset
'Dim stra As String
Dim strClausulaWhere As String
'Dim listaproveedores As String
'Dim listaproductos As Variant
'Dim listapedidos As String

cmdBuscar.Enabled = False
strClausulaWhere = " FR99CODESTOFERTPROV = 3 "
strClausulaWhere = strClausulaWhere & " AND FR58FECINIOFERT<=SYSDATE AND FR58FECFINOFERT>=SYSDATE "
If chkproveedor.Value = 1 Then
    If txtbuscar.Text <> "" Then
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        strClausulaWhere = strClausulaWhere & " AND FR79CODPROVEEDOR IN (" & _
         "SELECT FR79CODPROVEEDOR from fr7900 where " & _
             "upper(FR79PROVEEDOR) like upper('" & txtbuscar.Text & "%'))"
    End If
End If


If chkproducto.Value = 1 Then
    If txtbuscar.Text <> "" Then
        strClausulaWhere = strClausulaWhere & " AND FR58CODOFERTPROV IN (" & _
                            "SELECT FR58CODOFERTPROV FROM FR2300 WHERE FR73CODPRODUCTO IN (" & _
                            "SELECT FR73CODPRODUCTO from fr7300 where " & _
                            "upper(FR73DESPRODUCTOPROV) like upper('" & txtbuscar.Text & "%')))"
    End If
End If

If (chkproveedor.Value = 0 And chkproducto.Value = 0) Then
    strClausulaWhere = strClausulaWhere & " AND FR58CODOFERTPROV IS NULL"
End If

Call objWinInfo.FormChangeActive(fraFrame1(0), False, True)
objWinInfo.objWinActiveForm.strWhere = strClausulaWhere
Call objWinInfo.DataRefresh


cmdBuscar.Enabled = True
End Sub


Private Sub cmdGenerar_Click()
Dim mensaje As String

    cmdGenerar.Enabled = False
    If txtText1(0).Text <> "" Then
        gintProdBuscado = txtText1(0).Text
        gintCodProveedor = txtText1(8).Text
        gstrMoneda = txtText1(16).Text
        gstrPrecioOfertado = txtText1(4).Text
        gstrLlamador = "FrmBusOfe"
        Call objsecurity.LaunchProcess("FR0510")
        gintProdBuscado = ""
        gintCodProveedor = ""
        gstrMoneda = ""
        gstrPrecioOfertado = ""
        gstrLlamador = """"
    Else
        mensaje = MsgBox("No ha seleccionado ning�n producto", vbInformation, "Aviso")
    End If
    cmdGenerar.Enabled = True
End Sub




' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objDetailInfo As New clsCWForm
  Dim strKey As String
    
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraFrame1(0)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(0)
    
    .strName = "Ofertas"
      
    .strTable = "FR5800"
    .strWhere = "FR99CODESTOFERTPROV = 3" ' Ofertas Aceptadas
    .strWhere = .strWhere & " AND FR58FECINIOFERT<=SYSDATE AND FR58FECFINOFERT>=SYSDATE "
    .intAllowance = cwAllowReadOnly
    
    Call .FormAddOrderField("FR58CODOFERTPROV", cwAscending)
   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Ofertas")
    Call .FormAddFilterWhere(strKey, "FR58DESOFERTA", "Descripci�n Oferta", cwString)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�d. Proveedor", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR99CODESTOFERTPROV", "Estado", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR58FECRECEPOFERT", "Fecha Recepci�n", cwBoolean)
    Call .FormAddFilterWhere(strKey, "FR58FECINIOFERT", "Fecha Inicio", cwDate)
    Call .FormAddFilterWhere(strKey, "FR58FECFINOFERT", "Fecha Fin", cwDate)
    Call .FormAddFilterWhere(strKey, "FR58PORTES", "Portes", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR58EMBALAJES", "Embalajes", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR58RECARGOFINANC", "Recargo", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR58VALORACION", "Valoraci�n", cwNumeric)
  End With
  
  With objDetailInfo
    .strName = "Detalle Oferta"
    Set .objFormContainer = fraFrame1(1)
    Set .objFatherContainer = fraFrame1(0)
    Set .tabMainTab = tabTab1(1)
    Set .grdGrid = grdDBGrid1(1)
    .strTable = "FR2301J"
    
    '.blnAskPrimary = False
    .intAllowance = cwAllowReadOnly
    Call .FormAddOrderField("FR58CODOFERTPROV", cwAscending)
    Call .FormAddOrderField("FR23CODDETOFERTA", cwAscending)
    
    Call .FormAddRelation("FR58CODOFERTPROV", txtText1(18))
 
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Detalle Oferta")
    Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTOPROV", "Descripci�n", cwString)
    Call .FormAddFilterWhere(strKey, "FR23PRECFERTADO", "Precio Ofertado", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR23OBSERVACIONES", "Observaciones", cwString)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmaceutica", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FR73VOLUMEN", "Volumen", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73REFERENCIA", "Referencia", cwString)
    
    
    Call .FormAddFilterOrder(strKey, "FR73CODPRODUCTO", "C�digo Producto")
    Call .FormAddFilterOrder(strKey, "FR23PRECFERTADO", "Precio Ofertado")
    Call .FormAddFilterOrder(strKey, "FR23OBSERVACIONES", "Observaciones")
End With
   
   
   With objWinInfo
   
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
    Call .FormCreateInfo(objMasterInfo)
    
    
    '.CtrlGetInfo(txtText1(17)).blnInFind = True
    .CtrlGetInfo(txtText1(15)).blnInFind = True
    .CtrlGetInfo(txtText1(7)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(5)).blnInFind = True
    .CtrlGetInfo(txtText1(6)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    '.CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(0)).blnInFind = True
    .CtrlGetInfo(txtText1(2)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(3)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(11)).blnInFind = True
    .CtrlGetInfo(txtText1(12)).blnInFind = True
    .CtrlGetInfo(txtText1(13)).blnInFind = True
    .CtrlGetInfo(txtText1(14)).blnInFind = True
    
    
    .CtrlGetInfo(dtcDateCombo1(1)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(2)).blnInFind = True
    .CtrlGetInfo(dtcDateCombo1(3)).blnInFind = True
    
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(8)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(8)), txtText1(9), "FR79PROVEEDOR")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(2), "FR73DESPRODUCTOPROV")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(13), "FRH7CODFORMFAR")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(10), "FR73DOSIS")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(11), "FR93CODUNIMEDIDA")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(12), "FR73VOLUMEN")
    
    'Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(0)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO=?")
    'Call .CtrlAddLinked(.CtrlGetInfo(txtText1(0)), txtText1(14), "FR73REFERENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "FR99CODESTOFERTPROV", "SELECT * FROM FR9900 WHERE FR99CODESTOFERTPROV=?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(17), "FR99DESESTOFERTPROV")
    
    Call .WinRegister
    Call .WinStabilize
  End With
End Sub
Private Sub tabTab1_GotFocus(Index As Integer)
On Error Resume Next
    If Index = 0 Then
        grdDBGrid1(0).Columns(1).Width = 1000
        grdDBGrid1(0).Columns(2).Width = 3000
        grdDBGrid1(0).Columns(3).Width = 1500
        grdDBGrid1(0).Columns(4).Width = 1200
        grdDBGrid1(0).Columns(5).Width = 1000
        grdDBGrid1(0).Columns(6).Width = 1000
        grdDBGrid1(0).Columns(7).Width = 1000
        grdDBGrid1(0).Columns(8).Width = 1000
        grdDBGrid1(0).Columns(9).Width = 0
        'grdDBGrid1(0).Columns(10).Width = 0
        grdDBGrid1(0).Columns(11).Width = 0
        grdDBGrid1(0).Columns(12).Width = 0
    Else
        grdDBGrid1(1).Columns(1).Width = 0
        grdDBGrid1(1).Columns(2).Width = 3000
        grdDBGrid1(1).Columns(3).Width = 1300
        grdDBGrid1(1).Columns(4).Width = 6300
        grdDBGrid1(1).Columns(5).Width = 0
        grdDBGrid1(1).Columns(6).Width = 0
    End If
End Sub
Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
  
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub


Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Aportaciones Pendientes" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, True))
    End If
    Set objPrinter = Nothing
  End If


End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub






' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
        Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   'Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub




