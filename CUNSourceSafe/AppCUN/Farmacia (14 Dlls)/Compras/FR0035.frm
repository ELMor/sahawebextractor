VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.1#0"; "COMDLG32.OCX"
Begin VB.Form frmDefProducto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DEFINIR FARMACIA. Definir Productos"
   ClientHeight    =   8295
   ClientLeft      =   2505
   ClientTop       =   3855
   ClientWidth     =   12210
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8295
   ScaleWidth      =   12210
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   126
      Top             =   0
      Width           =   12210
      _ExtentX        =   21537
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Restricciones"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2505
      Index           =   2
      Left            =   600
      TabIndex        =   256
      Top             =   3360
      Width           =   9375
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1995
         Index           =   1
         Left            =   120
         TabIndex        =   257
         Top             =   360
         Width           =   9105
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   16060
         _ExtentY        =   3519
         _StockProps     =   79
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Principios Activos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2385
      Index           =   0
      Left            =   480
      TabIndex        =   228
      Top             =   5400
      Width           =   10455
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "Seleccionar Principios Activos"
         Height          =   855
         Left            =   9240
         TabIndex        =   229
         Top             =   600
         Width           =   975
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1905
         Index           =   0
         Left            =   120
         TabIndex        =   230
         Top             =   360
         Width           =   8985
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         SelectTypeRow   =   1
         RowNavigation   =   1
         CellNavigation  =   1
         ForeColorEven   =   0
         BackColorEven   =   -2147483643
         BackColorOdd    =   -2147483643
         RowHeight       =   423
         SplitterPos     =   1
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         UseDefaults     =   -1  'True
         _ExtentX        =   15849
         _ExtentY        =   3360
         _StockProps     =   79
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Descripci�n del producto"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Index           =   0
      Left            =   480
      TabIndex        =   212
      Top             =   6120
      Width           =   10455
      Begin VB.TextBox aux1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   8
         Left            =   7920
         Locked          =   -1  'True
         TabIndex        =   240
         TabStop         =   0   'False
         Tag             =   "Volumen(mL)"
         ToolTipText     =   "Unidad de Medida"
         Top             =   1080
         Width           =   975
      End
      Begin VB.TextBox aux1 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   220
         TabStop         =   0   'False
         ToolTipText     =   "C�d.Int.Farmacia"
         Top             =   480
         Width           =   975
      End
      Begin VB.TextBox aux1 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   1
         Left            =   1320
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   219
         TabStop         =   0   'False
         ToolTipText     =   "C�digo Seguridad"
         Top             =   480
         Width           =   255
      End
      Begin VB.TextBox aux1 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   2
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   218
         TabStop         =   0   'False
         ToolTipText     =   "Descripci�n del Producto"
         Top             =   480
         Width           =   6495
      End
      Begin VB.TextBox aux1 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   4
         Left            =   1920
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   217
         TabStop         =   0   'False
         ToolTipText     =   "Referencia"
         Top             =   1080
         Width           =   2655
      End
      Begin VB.TextBox aux1 
         BackColor       =   &H00C0C0C0&
         Height          =   315
         Index           =   3
         Left            =   240
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   216
         TabStop         =   0   'False
         ToolTipText     =   "C�digo Nacional"
         Top             =   1080
         Width           =   735
      End
      Begin VB.TextBox aux1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   7
         Left            =   6840
         Locked          =   -1  'True
         TabIndex        =   215
         TabStop         =   0   'False
         Tag             =   "C�d. de la unidad de medida de la dosis"
         ToolTipText     =   "Unidad de Medida"
         Top             =   1080
         Width           =   975
      End
      Begin VB.TextBox aux1 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   6
         Left            =   5760
         Locked          =   -1  'True
         TabIndex        =   214
         TabStop         =   0   'False
         Tag             =   "Dosis"
         ToolTipText     =   "Dosis"
         Top             =   1080
         Width           =   975
      End
      Begin VB.TextBox aux1 
         BackColor       =   &H00C0C0C0&
         Height          =   330
         Index           =   5
         Left            =   4920
         Locked          =   -1  'True
         TabIndex        =   213
         TabStop         =   0   'False
         Tag             =   "C�digo de la Forma Farmac�utica"
         ToolTipText     =   "Forma Farmace�tica"
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Vol(mL)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   52
         Left            =   7920
         TabIndex        =   241
         Top             =   840
         Width           =   645
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�d.Int.Farmacia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   32
         Left            =   240
         TabIndex        =   227
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Descripci�n del producto"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   33
         Left            =   1920
         TabIndex        =   226
         Top             =   240
         Width           =   2145
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Referencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   35
         Left            =   1920
         TabIndex        =   225
         Top             =   840
         Width           =   945
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "C�digo Nacional"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   36
         Left            =   240
         TabIndex        =   224
         Top             =   840
         Width           =   1410
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Unid.Med."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   37
         Left            =   6840
         TabIndex        =   223
         Top             =   840
         Width           =   885
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Dosis"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   38
         Left            =   5760
         TabIndex        =   222
         Top             =   840
         Width           =   480
      End
      Begin VB.Label lblLabel1 
         AutoSize        =   -1  'True
         Caption         =   "Forma F."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   39
         Left            =   4920
         TabIndex        =   221
         Top             =   840
         Width           =   750
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7575
      Index           =   1
      Left            =   120
      TabIndex        =   127
      Top             =   480
      Width           =   11820
      Begin TabDlg.SSTab tabTab1 
         Height          =   7095
         Index           =   0
         Left            =   120
         TabIndex        =   128
         TabStop         =   0   'False
         Top             =   360
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   12515
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0035.frx":0000
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "SSTab1"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0035.frx":001C
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(2)"
         Tab(1).ControlCount=   1
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   6465
            Index           =   2
            Left            =   -74880
            TabIndex        =   129
            TabStop         =   0   'False
            Top             =   120
            Width           =   10935
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            stylesets.count =   1
            stylesets(0).Name=   "PRODUCTOS-NO-VISIBLES"
            stylesets(0).BackColor=   12566463
            stylesets(0).Picture=   "FR0035.frx":0038
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   19288
            _ExtentY        =   11404
            _StockProps     =   79
         End
         Begin TabDlg.SSTab SSTab1 
            Height          =   6855
            Left            =   120
            TabIndex        =   130
            Top             =   120
            Width           =   10815
            _ExtentX        =   19076
            _ExtentY        =   12091
            _Version        =   327681
            Style           =   1
            Tabs            =   7
            TabsPerRow      =   7
            TabHeight       =   520
            TabCaption(0)   =   "Especialidad"
            TabPicture(0)   =   "FR0035.frx":0054
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblLabel1(22)"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblLabel1(14)"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblLabel1(2)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lblLabel1(5)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lblLabel1(18)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lblLabel1(27)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "lblLabel1(1)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "lblLabel1(3)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "lblLabel1(4)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "lblLabel1(8)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "lblLabel1(9)"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "lblLabel1(10)"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "lblLabel1(11)"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "CommonDialog1"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "dtcDateCombo1(0)"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "dtcDateCombo1(1)"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "txtText1(0)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "txtText1(22)"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "txtText1(1)"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).Control(19)=   "txtText1(17)"
            Tab(0).Control(19).Enabled=   0   'False
            Tab(0).Control(20)=   "txtText1(35)"
            Tab(0).Control(20).Enabled=   0   'False
            Tab(0).Control(21)=   "txtText1(36)"
            Tab(0).Control(21).Enabled=   0   'False
            Tab(0).Control(22)=   "txtText1(30)"
            Tab(0).Control(22).Enabled=   0   'False
            Tab(0).Control(23)=   "txtText1(3)"
            Tab(0).Control(23).Enabled=   0   'False
            Tab(0).Control(24)=   "txtText1(4)"
            Tab(0).Control(24).Enabled=   0   'False
            Tab(0).Control(25)=   "Frame1"
            Tab(0).Control(25).Enabled=   0   'False
            Tab(0).Control(26)=   "txtText1(5)"
            Tab(0).Control(26).Enabled=   0   'False
            Tab(0).Control(27)=   "txtText1(9)"
            Tab(0).Control(27).Enabled=   0   'False
            Tab(0).Control(28)=   "txtText1(10)"
            Tab(0).Control(28).Enabled=   0   'False
            Tab(0).Control(29)=   "txtText1(11)"
            Tab(0).Control(29).Enabled=   0   'False
            Tab(0).Control(30)=   "cmdWord"
            Tab(0).Control(30).Enabled=   0   'False
            Tab(0).Control(31)=   "txtText1(8)"
            Tab(0).Control(31).Enabled=   0   'False
            Tab(0).Control(32)=   "Picture1"
            Tab(0).Control(32).Enabled=   0   'False
            Tab(0).Control(33)=   "cmdExplorador(0)"
            Tab(0).Control(33).Enabled=   0   'False
            Tab(0).Control(34)=   "cmdExplorador(1)"
            Tab(0).Control(34).Enabled=   0   'False
            Tab(0).ControlCount=   35
            TabCaption(1)   =   "Datos Cl�nicos"
            TabPicture(1)   =   "FR0035.frx":0070
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "Frame3"
            Tab(1).ControlCount=   1
            TabCaption(2)   =   "Compras"
            TabPicture(2)   =   "FR0035.frx":008C
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "Frame4"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).ControlCount=   1
            TabCaption(3)   =   "Almac�n"
            TabPicture(3)   =   "FR0035.frx":00A8
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "chkCheck1(18)"
            Tab(3).Control(0).Enabled=   0   'False
            Tab(3).Control(1)=   "Frame5"
            Tab(3).Control(1).Enabled=   0   'False
            Tab(3).ControlCount=   2
            TabCaption(4)   =   "Par�metros"
            TabPicture(4)   =   "FR0035.frx":00C4
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "Frame6"
            Tab(4).Control(0).Enabled=   0   'False
            Tab(4).ControlCount=   1
            TabCaption(5)   =   "Consumos"
            TabPicture(5)   =   "FR0035.frx":00E0
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "Frame7"
            Tab(5).Control(0).Enabled=   0   'False
            Tab(5).ControlCount=   1
            TabCaption(6)   =   "Indicaciones"
            TabPicture(6)   =   "FR0035.frx":00FC
            Tab(6).ControlEnabled=   0   'False
            Tab(6).Control(0)=   "lblLabel1(59)"
            Tab(6).Control(0).Enabled=   0   'False
            Tab(6).Control(1)=   "Frame9"
            Tab(6).Control(1).Enabled=   0   'False
            Tab(6).Control(2)=   "txtText1(85)"
            Tab(6).Control(2).Enabled=   0   'False
            Tab(6).ControlCount=   3
            Begin VB.TextBox txtText1 
               DataField       =   "FR73DESTRAM"
               Height          =   1650
               Index           =   85
               Left            =   -74760
               MultiLine       =   -1  'True
               ScrollBars      =   3  'Both
               TabIndex        =   253
               Tag             =   "Indicaciones autorizadas"
               Top             =   720
               Width           =   6975
            End
            Begin VB.Frame Frame9 
               Caption         =   "Tramitaciones"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1455
               Left            =   -67080
               TabIndex        =   250
               Top             =   720
               Width           =   2175
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Uso Compasivo"
                  DataField       =   "FR73INDUSOCOM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   43
                  Left            =   240
                  TabIndex        =   252
                  Tag             =   "Uso Compasivo"
                  Top             =   960
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Extranjero"
                  DataField       =   "FR73INDEXTRANJERO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   6
                  Left            =   240
                  TabIndex        =   251
                  Tag             =   "Extr.|Extranjero?"
                  Top             =   480
                  Width           =   1695
               End
            End
            Begin VB.CommandButton cmdExplorador 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Index           =   1
               Left            =   4920
               TabIndex        =   234
               Top             =   3660
               Width           =   495
            End
            Begin VB.CommandButton cmdExplorador 
               Caption         =   "..."
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   525
               Index           =   0
               Left            =   5520
               TabIndex        =   233
               Top             =   2820
               Width           =   495
            End
            Begin VB.PictureBox Picture1 
               Height          =   1575
               Left            =   6240
               ScaleHeight     =   1515
               ScaleWidth      =   1755
               TabIndex        =   207
               Top             =   2820
               Width           =   1815
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FRH7CODFORMFAR"
               Height          =   330
               Index           =   8
               Left            =   4200
               TabIndex        =   6
               Tag             =   "F.F.|C�digo de la Forma Farmac�utica"
               Top             =   1620
               Width           =   735
            End
            Begin VB.Frame Frame6 
               Caption         =   "Par�metros"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   4575
               Left            =   -74880
               TabIndex        =   200
               Top             =   660
               Width           =   10455
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Kit"
                  DataField       =   "FR73INDKIT"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   45
                  Left            =   2640
                  TabIndex        =   262
                  Tag             =   "Kit|Kit"
                  Top             =   3120
                  Width           =   1935
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "PRN"
                  DataField       =   "FR73INDPRN"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   44
                  Left            =   2640
                  TabIndex        =   261
                  Tag             =   "PRN|El medicamento solo se puede prescribir mediante PRN"
                  Top             =   2880
                  Width           =   2055
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Visible"
                  DataField       =   "FR73INDVISIBLE"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   42
                  Left            =   120
                  TabIndex        =   249
                  Tag             =   "Visible"
                  Top             =   3120
                  Width           =   2055
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PEDPRGPRIOD"
                  Height          =   330
                  Index           =   81
                  Left            =   8520
                  TabIndex        =   244
                  Tag             =   "N�D�as Entre P.P|N� de d�as entre pedidos programado"
                  Top             =   2640
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73TAMPEDPRG"
                  Height          =   330
                  Index           =   80
                  Left            =   7440
                  TabIndex        =   243
                  Tag             =   "N�EnvPP|N� de Envases a recibir en cada entrega del pedido programado"
                  Top             =   2640
                  Width           =   1005
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "EXO"
                  DataField       =   "FR73INDEXO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   41
                  Left            =   120
                  TabIndex        =   90
                  Tag             =   "EXO Excluido de la Oferta de la Seguridad Social"
                  Top             =   2640
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "EFP"
                  DataField       =   "FR73INDEFP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   40
                  Left            =   120
                  TabIndex        =   89
                  Tag             =   "EFP Especialidad Farmac�utica Publicitaria"
                  Top             =   2400
                  Width           =   2175
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "EFG"
                  DataField       =   "FR73INDEFG"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   9
                  Left            =   120
                  TabIndex        =   91
                  Tag             =   "EFG Especialidad Farmac�utica Gen�rica"
                  Top             =   2880
                  Width           =   2055
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Facturable x Dosis"
                  DataField       =   "FR73INDFACTDOSIS"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   39
                  Left            =   2640
                  TabIndex        =   101
                  Tag             =   "F.D.|En vez de facturar por el campo de salida real, se har�a por dosis"
                  Top             =   2640
                  Width           =   2055
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Principios Activos"
                  DataField       =   "FR73INDPRINACT"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   38
                  Left            =   4800
                  TabIndex        =   110
                  Tag             =   "P.A.|Meter Principios Activos en la Tabla de Productos"
                  Top             =   2400
                  Width           =   1935
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "TLD"
                  DataField       =   "FR73INDUSOENF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   37
                  Left            =   120
                  TabIndex        =   88
                  Tag             =   "TLD|Tratamiento de larga duraci�n"
                  Top             =   2160
                  Width           =   2055
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Reconstituyente"
                  DataField       =   "FR73INDPRODREC"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   36
                  Left            =   4800
                  TabIndex        =   112
                  Tag             =   "Rec.|Reconstituyente?"
                  Top             =   2880
                  Width           =   2295
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Infusi�n Intravenosa"
                  DataField       =   "FR73INDINFIV"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   35
                  Left            =   4800
                  TabIndex        =   111
                  Tag             =   "I.I.|Infusi�n Intravenosa?"
                  Top             =   2640
                  Width           =   2535
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Materia Prima"
                  DataField       =   "FR73INDMATPRIMA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   4
                  Left            =   2640
                  TabIndex        =   92
                  Tag             =   "M.P.|Materia Prima?"
                  Top             =   480
                  Width           =   1575
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Producto RUM"
                  DataField       =   "FR73INDMEDRUM"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   12
                  Left            =   2640
                  TabIndex        =   95
                  Tag             =   "RUM|Medicamento sujeto a revisi�n?"
                  Top             =   1200
                  Width           =   1575
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Estupefaciente"
                  DataField       =   "FR73INDESTUPEFACI"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   16
                  Left            =   120
                  TabIndex        =   81
                  Tag             =   "Est.|Estupefaciente?"
                  Top             =   480
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "V�a No Modificable"
                  DataField       =   "FR73INDVIAOPCION"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   17
                  Left            =   4800
                  TabIndex        =   102
                  Tag             =   "V�a F�ja|La V�a No  se puede variar?"
                  Top             =   480
                  Width           =   2175
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Psicotropo"
                  DataField       =   "FR73INDPSICOT"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   82
                  Tag             =   "Psic.|Psicotropo?"
                  Top             =   720
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Uso Hospitalario"
                  DataField       =   "FR73INDUSOHOSP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   2
                  Left            =   120
                  TabIndex        =   83
                  Tag             =   "U.H.|Uso Hospitalario?"
                  Top             =   960
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Diagn�stico Hosp."
                  DataField       =   "FR73INDDIAGHOSP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   3
                  Left            =   120
                  TabIndex        =   84
                  Tag             =   "D.H.|Diagn�stico Hospitalario?"
                  Top             =   1200
                  Width           =   1935
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Especial Control"
                  DataField       =   "FR73INDESPCONT"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   5
                  Left            =   120
                  TabIndex        =   85
                  Tag             =   "C.E.|Especial Control?"
                  Top             =   1440
                  Width           =   1815
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Investigaci�n Cl�nica"
                  DataField       =   "FR73INDINVCLI"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   7
                  Left            =   120
                  TabIndex        =   86
                  Tag             =   "I.C."
                  Top             =   1680
                  Width           =   2175
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Uso Restringido"
                  DataField       =   "FR73INDUSOREST"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   8
                  Left            =   120
                  TabIndex        =   87
                  Tag             =   "U.Rest.|Uso Restringido?"
                  Top             =   1920
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "F�rmula Magistral"
                  DataField       =   "FR73INDFABRICABLE"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   10
                  Left            =   2640
                  TabIndex        =   93
                  Tag             =   "F.M.|F�rmula Magistral?"
                  Top             =   720
                  Width           =   1935
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Producto A"
                  DataField       =   "FR73INDPRODA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   11
                  Left            =   2640
                  TabIndex        =   94
                  Tag             =   "Tip.A|Producto tipo A?"
                  Top             =   960
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Antiinfeccioso"
                  DataField       =   "FR73INDANTINF"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   13
                  Left            =   2640
                  TabIndex        =   96
                  Tag             =   "Antii|Antiinfeccioso?"
                  Top             =   1440
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Dosis x Sup. Corp."
                  DataField       =   "FR73INDDOSISSUPCORP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   14
                  Left            =   2640
                  TabIndex        =   97
                  Tag             =   "D.S.P|Dosis en funci�n de la Superficie Corporal?"
                  Top             =   1680
                  Width           =   1935
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Dosis x Peso"
                  DataField       =   "FR73INDDOSISPESO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   15
                  Left            =   2640
                  TabIndex        =   98
                  Tag             =   "D.F.|Dosis en funci�n del Peso?"
                  Top             =   1920
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "P.V.P. autom�tico"
                  DataField       =   "FR73INDPVPAUTOMAT"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   19
                  Left            =   2640
                  TabIndex        =   99
                  Tag             =   "PVP Aut.|El precio de venta se calcula autom�ticamente ?"
                  Top             =   2160
                  Width           =   1935
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Regularizaci�n"
                  DataField       =   "FR73INDREGULARIZA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   20
                  Left            =   2640
                  TabIndex        =   100
                  Tag             =   "Reg.|Sujeto a regularizaci�n?"
                  Top             =   2400
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Reenvasado"
                  DataField       =   "FR73INDREENV"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   21
                  Left            =   4800
                  TabIndex        =   103
                  Tag             =   "Reen.|Reenvasado?"
                  Top             =   720
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Reenvasado Centro Gal�nico"
                  DataField       =   "FR73INDREECGAL"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   22
                  Left            =   4800
                  TabIndex        =   104
                  Tag             =   "Reen.G.|Se reenvasa en el Centro Gal�nico?"
                  Top             =   960
                  Width           =   2895
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Material de envasado"
                  DataField       =   "FR73INDMATENV"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   23
                  Left            =   4800
                  TabIndex        =   105
                  Tag             =   "M.E.|Material de envasado?"
                  Top             =   1200
                  Width           =   2175
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Protecci�n luz"
                  DataField       =   "FR73INDPROTLUZ"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   24
                  Left            =   4800
                  TabIndex        =   106
                  Tag             =   "P.L.|Se debe proteger de la luz?"
                  Top             =   1440
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Frigor�fico (4�C)"
                  DataField       =   "FR73INDFRIGO"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   25
                  Left            =   4800
                  TabIndex        =   107
                  Tag             =   "4�C|Se debe guardar en el frigor�fico?"
                  Top             =   1680
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Congelador (-20�C)"
                  DataField       =   "FR73INDCONGE"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   26
                  Left            =   4800
                  TabIndex        =   108
                  Tag             =   "-20�C|Se debe guardar en el congelador?"
                  Top             =   1920
                  Width           =   1935
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Caducidad Controlada"
                  DataField       =   "FR73INDCADESP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   27
                  Left            =   4800
                  TabIndex        =   109
                  Tag             =   "C.C.|Caducidad Controlada?"
                  Top             =   2160
                  Width           =   2295
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Gesti�n por Consumos"
                  DataField       =   "FR73INDGESTCONS"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   28
                  Left            =   7800
                  TabIndex        =   113
                  Tag             =   "G.C.|Gesti�n por consumos?"
                  Top             =   480
                  Width           =   2295
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Transito"
                  DataField       =   "FR73INDTRANS"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   29
                  Left            =   7800
                  TabIndex        =   114
                  Tag             =   "Trans.|De transito?"
                  Top             =   720
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Programado"
                  DataField       =   "FR73INDPROG"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   30
                  Left            =   7800
                  TabIndex        =   115
                  Tag             =   "Prog.|Compra de forma programada?"
                  Top             =   960
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Gesti�n Fija"
                  DataField       =   "FR73INDGESTFIJA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   31
                  Left            =   7800
                  TabIndex        =   116
                  Tag             =   "G.F.|Se gestiona de forma fija?"
                  Top             =   1200
                  Width           =   1695
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Material Implantable"
                  DataField       =   "FR73INDMATIMP"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   32
                  Left            =   7800
                  TabIndex        =   117
                  Tag             =   "M.I.|Material Implantable?"
                  Top             =   1440
                  Width           =   2055
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Producto Sanitario"
                  DataField       =   "FR73INDPRODSAN"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   33
                  Left            =   7800
                  TabIndex        =   118
                  Tag             =   "M.S.|Material Sanitario?"
                  Top             =   1680
                  Width           =   2055
               End
               Begin VB.Frame Frame8 
                  Caption         =   "Pedidos Programados"
                  ForeColor       =   &H8000000D&
                  Height          =   1095
                  Left            =   7320
                  TabIndex        =   242
                  Top             =   2040
                  Width           =   3015
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "D�as Entre Entregas"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   54
                     Left            =   1200
                     TabIndex        =   246
                     Top             =   360
                     Width           =   1740
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "N�Envases"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   53
                     Left            =   120
                     TabIndex        =   245
                     Top             =   360
                     Width           =   945
                  End
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Prescripci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   -1  'True
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   93
                  Left            =   120
                  TabIndex        =   205
                  Top             =   240
                  Width           =   1320
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Gesti�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   -1  'True
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   94
                  Left            =   2640
                  TabIndex        =   204
                  Top             =   240
                  Width           =   810
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Producto"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   -1  'True
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   95
                  Left            =   4800
                  TabIndex        =   203
                  Top             =   240
                  Width           =   945
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Compras/Almacen"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   -1  'True
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   240
                  Index           =   96
                  Left            =   7800
                  TabIndex        =   201
                  Top             =   240
                  Width           =   1920
               End
            End
            Begin VB.Frame Frame7 
               Caption         =   "Consumos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   4575
               Left            =   -74880
               TabIndex        =   197
               Top             =   660
               Width           =   10455
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CONSMES"
                  Height          =   330
                  Index           =   77
                  Left            =   8280
                  TabIndex        =   121
                  Tag             =   "Cons. Mes|Consumo Mensual"
                  Top             =   1560
                  Width           =   1485
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR73ABCECON"
                  Height          =   330
                  Index           =   73
                  Left            =   8280
                  TabIndex        =   124
                  Tag             =   "ABC (CE)|Categor�a ABC por criterios econ�micos"
                  Top             =   3600
                  Width           =   285
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR73ABCCONS"
                  Height          =   330
                  Index           =   28
                  Left            =   8280
                  TabIndex        =   123
                  Tag             =   "ABC(C)|Categor�a ABC por consumos"
                  Top             =   2760
                  Width           =   285
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "Consumo Regular"
                  DataField       =   "FR73INDCONSREGULAR"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   34
                  Left            =   8280
                  TabIndex        =   119
                  Tag             =   "C.R.|Consumo regular?"
                  Top             =   360
                  Width           =   1815
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CONSDIARIO"
                  Height          =   330
                  Index           =   72
                  Left            =   8280
                  TabIndex        =   122
                  Tag             =   "Cons. D�a|Consumo Diario"
                  Top             =   2160
                  Width           =   1485
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CONSANUAL"
                  Height          =   330
                  Index           =   71
                  Left            =   8280
                  TabIndex        =   120
                  Tag             =   "Cons. A�o|Consumo Anual"
                  Top             =   960
                  Width           =   1485
               End
               Begin MSChartLib.MSChart MSChart1 
                  Height          =   4095
                  Left            =   240
                  OleObjectBlob   =   "FR0035.frx":0118
                  TabIndex        =   206
                  Top             =   360
                  Width           =   7695
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Consumo Mensual"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   49
                  Left            =   8280
                  TabIndex        =   237
                  Top             =   1320
                  Width           =   1545
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "ABC por criterios econ�micos"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   555
                  Index           =   43
                  Left            =   8280
                  TabIndex        =   232
                  Top             =   3120
                  Width           =   1665
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "ABC por consumos"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   41
                  Left            =   8280
                  TabIndex        =   231
                  Top             =   2520
                  Width           =   1605
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Consumo Diario"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   98
                  Left            =   8280
                  TabIndex        =   199
                  Top             =   1920
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Consumo Anual"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   97
                  Left            =   8280
                  TabIndex        =   198
                  Top             =   720
                  Width           =   1320
               End
            End
            Begin VB.Frame Frame5 
               Caption         =   "Almac�n"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   4575
               Left            =   -74880
               TabIndex        =   184
               Top             =   660
               Width           =   10455
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CONSANUAL"
                  Height          =   330
                  Index           =   79
                  Left            =   8040
                  TabIndex        =   69
                  Tag             =   "Consumo Anual"
                  Top             =   960
                  Width           =   1485
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CONSDIARIO"
                  Height          =   330
                  Index           =   78
                  Left            =   8040
                  TabIndex        =   77
                  Tag             =   "Consumo Diario"
                  Top             =   2400
                  Width           =   1485
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CONSMES"
                  Height          =   330
                  Index           =   75
                  Left            =   8040
                  TabIndex        =   74
                  Tag             =   "Cons. Mes|Consumo Mensual"
                  Top             =   1680
                  Width           =   1485
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PTEBONIF"
                  Height          =   330
                  Index           =   23
                  Left            =   6120
                  TabIndex        =   73
                  Tag             =   "Pte. Bonif.|Cantidad de Producto Pendiente de ser Entregada por Bonificaciones"
                  Top             =   1680
                  Width           =   1335
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CANTPEND"
                  Height          =   330
                  Index           =   70
                  Left            =   6120
                  TabIndex        =   68
                  Tag             =   "Pte. Recibir|Cantidad pendiente de recibir"
                  Top             =   960
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PREULTENT"
                  Height          =   330
                  Index           =   69
                  Left            =   4440
                  TabIndex        =   80
                  Tag             =   "P.U.E.|Precio de la �ltima entrada"
                  Top             =   3120
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73IMPALMACEN"
                  Height          =   330
                  Index           =   68
                  Left            =   4440
                  TabIndex        =   76
                  Tag             =   "Importe Almac�n"
                  Top             =   2400
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PRECMED"
                  Height          =   330
                  Index           =   67
                  Left            =   4440
                  TabIndex        =   72
                  Tag             =   "Pec. Med.|Precio medio "
                  Top             =   1680
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73EXISTENCIAS"
                  Height          =   330
                  Index           =   66
                  Left            =   4440
                  TabIndex        =   67
                  Tag             =   "Existencias|Existencias en el almac�n de farmacia"
                  Top             =   960
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73DESVIACION"
                  Height          =   330
                  Index           =   65
                  Left            =   2760
                  TabIndex        =   79
                  Tag             =   "Desv.|Desviaci�n"
                  Top             =   3120
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73STOCKMAX"
                  Height          =   330
                  Index           =   64
                  Left            =   2760
                  TabIndex        =   75
                  Tag             =   "Stock M�x.|Stock m�ximo"
                  Top             =   2400
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73STOCKMIN"
                  Height          =   330
                  Index           =   63
                  Left            =   2760
                  TabIndex        =   71
                  Tag             =   "Stock M�n.|Stock m�nimo"
                  Top             =   1680
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PTOPED"
                  Height          =   330
                  Index           =   62
                  Left            =   2760
                  TabIndex        =   66
                  Tag             =   "Pto. Ped.|Punto de pedido"
                  Top             =   960
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRH9UBICACION"
                  Height          =   330
                  Index           =   61
                  Left            =   480
                  TabIndex        =   78
                  Tag             =   "Ubicaci�n|Ubicaci�n del producto"
                  Top             =   3120
                  Width           =   1845
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73TAMENVASE"
                  Height          =   330
                  Index           =   52
                  Left            =   480
                  TabIndex        =   70
                  Tag             =   "Envase de|N� de unidades del envase"
                  Top             =   1680
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73UNIDADSALREAL"
                  Height          =   330
                  Index           =   50
                  Left            =   480
                  TabIndex        =   65
                  Tag             =   "U.S.R.|Unidad de Salida Real"
                  Top             =   960
                  Width           =   855
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Consumo Anual"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   51
                  Left            =   8040
                  TabIndex        =   239
                  Top             =   720
                  Width           =   1320
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Consumo Diario"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   50
                  Left            =   8040
                  TabIndex        =   238
                  Top             =   2160
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Consumo mensual"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   45
                  Left            =   8040
                  TabIndex        =   236
                  Top             =   1440
                  Width           =   1695
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Pendiente Bonificar"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   40
                  Left            =   6120
                  TabIndex        =   211
                  Tag             =   "Cantidad de Producto Pendiente de ser Entregada por Bonificaciones"
                  Top             =   1440
                  Width           =   2175
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Pendiente Recibir"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   92
                  Left            =   6120
                  TabIndex        =   196
                  Top             =   720
                  Width           =   1530
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Precio Ultima Entrada"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   91
                  Left            =   4440
                  TabIndex        =   195
                  Top             =   2880
                  Width           =   1860
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Importe almac�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   90
                  Left            =   4440
                  TabIndex        =   194
                  Top             =   2160
                  Width           =   1410
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Precio medio"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   89
                  Left            =   4440
                  TabIndex        =   193
                  Top             =   1440
                  Width           =   1110
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Existencias"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   88
                  Left            =   4440
                  TabIndex        =   192
                  Top             =   720
                  Width           =   975
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Desviaci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   87
                  Left            =   2760
                  TabIndex        =   191
                  Top             =   2880
                  Width           =   960
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Stock m�ximo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   86
                  Left            =   2760
                  TabIndex        =   190
                  Top             =   2160
                  Width           =   1185
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Stock m�nimo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   85
                  Left            =   2760
                  TabIndex        =   189
                  Top             =   1440
                  Width           =   1170
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Punto de Pedido"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   84
                  Left            =   2760
                  TabIndex        =   188
                  Top             =   720
                  Width           =   1425
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Ubicaci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   79
                  Left            =   480
                  TabIndex        =   187
                  Top             =   2880
                  Width           =   870
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tama�o Envase"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   0
                  Left            =   480
                  TabIndex        =   186
                  Top             =   1440
                  Width           =   1380
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Unidad de Salida Real"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   42
                  Left            =   480
                  TabIndex        =   185
                  Top             =   720
                  Width           =   2175
               End
            End
            Begin VB.Frame Frame4 
               Caption         =   "Compras"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   4455
               Left            =   -74880
               TabIndex        =   167
               Tag             =   "Bonf.|Bonificaci�n"
               Top             =   660
               Width           =   10455
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00FFFFFF&
                  DataField       =   "FR73DESPRODUCTOPROV"
                  Height          =   330
                  Index           =   84
                  Left            =   240
                  TabIndex        =   263
                  Tag             =   "Producto|Descripci�n Producto Proveedor"
                  Top             =   1440
                  Width           =   6540
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PRECIOVENTACON"
                  Height          =   330
                  Index           =   83
                  Left            =   8400
                  TabIndex        =   58
                  Tag             =   "PVPC|Precio de Venta en los Conciertos Econ�micos"
                  Top             =   3240
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PRECIOVENTA"
                  Height          =   330
                  Index           =   6
                  Left            =   7080
                  TabIndex        =   57
                  Tag             =   "PVP|Precio de Venta"
                  Top             =   3240
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   76
                  Left            =   6480
                  TabIndex        =   63
                  TabStop         =   0   'False
                  Tag             =   "Descripci�n de Grupo Contable"
                  Top             =   3840
                  Width           =   3765
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PVL"
                  Height          =   330
                  Index           =   2
                  Left            =   240
                  TabIndex        =   59
                  Tag             =   "P.V.L.|Precio de Venta de Laboratorio"
                  Top             =   3840
                  Width           =   1215
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73RECARGO"
                  Height          =   330
                  Index           =   58
                  Left            =   4080
                  TabIndex        =   61
                  Tag             =   "%Recargo|Tanto por ciento de recargo"
                  Top             =   3840
                  Width           =   885
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PRECIONETCOMPRA"
                  Height          =   330
                  Index           =   7
                  Left            =   2400
                  TabIndex        =   60
                  Tag             =   "P.N.C.|Precio Neto de Compra"
                  Top             =   3840
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR73CUENCONT"
                  Height          =   330
                  Index           =   60
                  Left            =   5520
                  TabIndex        =   62
                  Tag             =   "Grupo|C�digo de Grupo Contable"
                  Top             =   3840
                  Width           =   885
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRH8MONEDA"
                  Height          =   330
                  Index           =   59
                  Left            =   5520
                  TabIndex        =   56
                  Tag             =   "Moneda"
                  Top             =   3240
                  Width           =   885
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73OTROSCOSTES"
                  Height          =   330
                  Index           =   53
                  Left            =   5520
                  TabIndex        =   52
                  Tag             =   "Otros costes"
                  Top             =   2640
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73BONIFICACION"
                  Height          =   330
                  Index           =   51
                  Left            =   4080
                  TabIndex        =   55
                  Tag             =   "%Bonf.|Tanto por ciento de Bonificaci�n"
                  Top             =   3240
                  Width           =   885
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PRECOFR"
                  Height          =   330
                  Index           =   49
                  Left            =   2400
                  TabIndex        =   54
                  Tag             =   "Prec. Ofert.|Precio Oferta de Compra"
                  Top             =   3240
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73TAMPEDI"
                  Height          =   330
                  Index           =   48
                  Left            =   240
                  TabIndex        =   53
                  Tag             =   "Tama�o Ped.|Tama�o del pedido en d�as"
                  Top             =   3240
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73RAPPEL"
                  Height          =   330
                  Index           =   47
                  Left            =   4080
                  TabIndex        =   51
                  Tag             =   "%Rappel|Tanto por ciento de Rappel"
                  Top             =   2640
                  Width           =   885
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PRECBASE"
                  Height          =   330
                  Index           =   46
                  Left            =   2400
                  TabIndex        =   50
                  Tag             =   "Prec. Base|Precio Base de Compra"
                  Top             =   2640
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73DIASSEG"
                  Height          =   330
                  Index           =   45
                  Left            =   240
                  TabIndex        =   49
                  Tag             =   "D�as Seg.|D�as de Seguridad"
                  Top             =   2640
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73DESCUENTO"
                  Height          =   330
                  Index           =   44
                  Left            =   4080
                  TabIndex        =   46
                  Tag             =   "%Desc.|Tanto por ciento de Descuento"
                  Top             =   2040
                  Width           =   885
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73PRECESTD"
                  Height          =   330
                  Index           =   43
                  Left            =   2400
                  TabIndex        =   45
                  Tag             =   "Prec. Std|Precio Estandar"
                  Top             =   2040
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73TIEMPOREPOSI"
                  Height          =   330
                  Index           =   42
                  Left            =   240
                  TabIndex        =   44
                  Tag             =   "D�as Rep.|Dias de Reposici�n"
                  Top             =   2040
                  Width           =   1245
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR79CODPROVEEDOR_C"
                  Height          =   330
                  Index           =   41
                  Left            =   7800
                  TabIndex        =   42
                  Tag             =   "C�d.Proveedor C"
                  Top             =   1680
                  Visible         =   0   'False
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   40
                  Left            =   9000
                  TabIndex        =   43
                  Tag             =   "Desc.Proveedor C"
                  Top             =   1680
                  Visible         =   0   'False
                  Width           =   1140
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR79CODPROVEEDOR_B"
                  Height          =   330
                  Index           =   39
                  Left            =   240
                  TabIndex        =   40
                  Tag             =   "C�d.Proveedor B"
                  Top             =   840
                  Width           =   1092
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   38
                  Left            =   1440
                  TabIndex        =   41
                  Tag             =   "Desc.Proveedor B"
                  Top             =   840
                  Width           =   8460
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   57
                  Left            =   6240
                  TabIndex        =   48
                  TabStop         =   0   'False
                  Tag             =   "Desc.Tipo IVA"
                  Top             =   2040
                  Width           =   4005
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR88CODTIPIVA"
                  Height          =   330
                  Index           =   56
                  Left            =   5520
                  TabIndex        =   47
                  Tag             =   "IVA|C�d.Tipo IVA"
                  Top             =   2040
                  Width           =   612
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR79CODPROVEEDOR_A"
                  Height          =   330
                  Index           =   55
                  Left            =   240
                  TabIndex        =   38
                  Tag             =   "C�d.Proveedor A"
                  Top             =   480
                  Width           =   1092
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   54
                  Left            =   1440
                  TabIndex        =   39
                  Tag             =   "Desc.Proveedor A"
                  Top             =   480
                  Width           =   8460
               End
               Begin VB.Frame Frame10 
                  Caption         =   "Precios Unitarios"
                  ForeColor       =   &H8000000D&
                  Height          =   1215
                  Left            =   6960
                  TabIndex        =   258
                  Top             =   2520
                  Width           =   3015
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "P.V.P."
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   6
                     Left            =   120
                     TabIndex        =   260
                     Top             =   480
                     Width           =   555
                  End
                  Begin VB.Label lblLabel1 
                     AutoSize        =   -1  'True
                     Caption         =   "P.V.L.Unitario"
                     BeginProperty Font 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   195
                     Index           =   57
                     Left            =   1440
                     TabIndex        =   259
                     Top             =   480
                     Width           =   1200
                  End
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "Descripci�n Producto Proveedor"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   58
                  Left            =   240
                  TabIndex        =   264
                  Top             =   1200
                  Width           =   3255
               End
               Begin VB.Label lblLabel1 
                  Caption         =   "P.V.L.Envase"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   99
                  Left            =   240
                  TabIndex        =   210
                  Top             =   3600
                  Width           =   2175
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "C�d Grupo"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   83
                  Left            =   5520
                  TabIndex        =   183
                  Top             =   3600
                  Width           =   915
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Moneda"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   82
                  Left            =   5520
                  TabIndex        =   182
                  Top             =   3000
                  Width           =   690
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Otros Costes"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   80
                  Left            =   5520
                  TabIndex        =   181
                  Top             =   2400
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Recargo (%)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   81
                  Left            =   4080
                  TabIndex        =   180
                  Top             =   3600
                  Width           =   1050
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Bonificaci�n (%)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   78
                  Left            =   4080
                  TabIndex        =   179
                  Top             =   3000
                  Width           =   1380
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Precio Oferta"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   77
                  Left            =   2400
                  TabIndex        =   178
                  Top             =   3000
                  Width           =   1140
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tama�o Pedido"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   76
                  Left            =   240
                  TabIndex        =   177
                  Top             =   3000
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Rappel (%)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   75
                  Left            =   4080
                  TabIndex        =   176
                  Top             =   2400
                  Width           =   930
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Precio Base"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   74
                  Left            =   2400
                  TabIndex        =   175
                  Top             =   2400
                  Width           =   1035
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "D�as Seguridad"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   73
                  Left            =   240
                  TabIndex        =   174
                  Top             =   2400
                  Width           =   1335
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Descuento (%)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   72
                  Left            =   4080
                  TabIndex        =   173
                  Top             =   1800
                  Width           =   1245
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Precio Estandar"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   48
                  Left            =   2400
                  TabIndex        =   172
                  Top             =   1800
                  Width           =   1365
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "D�as Reposici�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   34
                  Left            =   240
                  TabIndex        =   171
                  Top             =   1800
                  Width           =   1425
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Tipo IVA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   47
                  Left            =   5520
                  TabIndex        =   170
                  Top             =   1800
                  Width           =   750
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Proveedores"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   46
                  Left            =   120
                  TabIndex        =   169
                  Top             =   240
                  Width           =   1080
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Precio Neto"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   7
                  Left            =   2400
                  TabIndex        =   168
                  Top             =   3600
                  Width           =   1020
               End
            End
            Begin VB.Frame Frame3 
               Caption         =   "Datos Cl�nicos"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   4575
               Left            =   -74880
               TabIndex        =   150
               Top             =   660
               Width           =   10455
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73VELPERFUSION"
                  Height          =   330
                  Index           =   82
                  Left            =   3960
                  TabIndex        =   247
                  Tag             =   "Perfusi�n|Velocidad de Perfusi�n"
                  Top             =   4080
                  Width           =   975
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR93CODUNIMEDIDA_RED"
                  Height          =   375
                  Index           =   74
                  Left            =   1320
                  TabIndex        =   26
                  Tag             =   "U.R.|Unidades del redondeo"
                  Top             =   2280
                  Width           =   975
               End
               Begin VB.TextBox txtprodreconstituyente 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   240
                  Locked          =   -1  'True
                  TabIndex        =   28
                  Tag             =   "Rec.|Reconstituir con"
                  Top             =   2880
                  Width           =   2730
               End
               Begin VB.TextBox txtproddiluyente 
                  Alignment       =   1  'Right Justify
                  Height          =   330
                  Left            =   240
                  Locked          =   -1  'True
                  TabIndex        =   31
                  Tag             =   "I.I.V|Infusi�n Intravenosa en "
                  Top             =   3480
                  Width           =   2730
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR73OBSERVACION"
                  Height          =   1050
                  Index           =   37
                  Left            =   6840
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   37
                  Tag             =   "Observaciones al producto"
                  Top             =   3360
                  Width           =   3480
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR73DOSIFICACION"
                  Height          =   1050
                  Index           =   34
                  Left            =   6840
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   36
                  Tag             =   "Explicaci�n de la dosificaci�n"
                  Top             =   1920
                  Width           =   3480
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR73INDICACIONES"
                  Height          =   1050
                  Index           =   33
                  Left            =   6840
                  MultiLine       =   -1  'True
                  ScrollBars      =   2  'Vertical
                  TabIndex        =   35
                  Tag             =   "Indicaciones sobre el uso del producto"
                  Top             =   480
                  Width           =   3480
               End
               Begin VB.CheckBox chkCheck1 
                  Caption         =   "A la baja"
                  DataField       =   "FR73INDREDALABAJA"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   255
                  Index           =   0
                  Left            =   2520
                  TabIndex        =   27
                  Tag             =   "Red.B.|Redondeo a la baja"
                  Top             =   2280
                  Width           =   1095
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73REDONDEO"
                  Height          =   330
                  Index           =   32
                  Left            =   240
                  TabIndex        =   25
                  Tag             =   "M.F.D.|M�nima fracci�n dosificable"
                  Top             =   2280
                  Width           =   960
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73TIEMINF"
                  Height          =   330
                  Index           =   31
                  Left            =   240
                  TabIndex        =   33
                  Tag             =   "Tiem. Inf.|Tiempo de Infusi�n en minutos"
                  Top             =   4080
                  Width           =   800
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CADUCIDAD"
                  Height          =   330
                  Index           =   29
                  Left            =   1800
                  TabIndex        =   34
                  Tag             =   "Cad.|Caducidad de la infusi�n en horas"
                  Top             =   4080
                  Width           =   960
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73VOLINFIV"
                  Height          =   330
                  Index           =   27
                  Left            =   3360
                  TabIndex        =   32
                  Tag             =   "Vol.I.I.V.|Volumen en mililitros de la infusi�n intravenosa"
                  Top             =   3480
                  Width           =   960
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73CONCENTRACION"
                  Height          =   330
                  Index           =   24
                  Left            =   4680
                  TabIndex        =   30
                  Tag             =   "Con.|Concentraci�n en mililitros (Dosis/Volumen Rec.)"
                  Top             =   2880
                  Width           =   960
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73VOLREC"
                  Height          =   330
                  Index           =   21
                  Left            =   3360
                  TabIndex        =   29
                  Tag             =   "Vol. Rec.|Volumen en mililitros de reconstituyente"
                  Top             =   2880
                  Width           =   960
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   20
                  Left            =   3720
                  TabIndex        =   24
                  Tag             =   "Frec. M�xima"
                  Top             =   1680
                  Width           =   2160
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRG4CODFRECUENCIA_MAX"
                  Height          =   330
                  Index           =   19
                  Left            =   3000
                  TabIndex        =   23
                  Tag             =   "Frec.M.|C�d. Frecuencia M�xima"
                  Top             =   1680
                  Width           =   600
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73DOSISMAX"
                  Height          =   330
                  Index           =   18
                  Left            =   240
                  TabIndex        =   21
                  Tag             =   "D.M.|Dosis M�xima"
                  Top             =   1680
                  Width           =   975
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR93CODUNIMEDIDA_MAX"
                  Height          =   330
                  Index           =   16
                  Left            =   1320
                  TabIndex        =   22
                  Tag             =   "U.M.D.M.|C�d. Unidad Medida Dosis M�xima"
                  Top             =   1680
                  Width           =   960
               End
               Begin VB.TextBox txtText1 
                  Height          =   330
                  Index           =   15
                  Left            =   3720
                  TabIndex        =   20
                  Tag             =   "Frec. Usual"
                  Top             =   1080
                  Width           =   2160
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FRG4CODFRECUENCIA_USU"
                  Height          =   330
                  Index           =   14
                  Left            =   3000
                  TabIndex        =   19
                  Tag             =   "Frec.U.|C�d. Frecuencia usual"
                  Top             =   1080
                  Width           =   600
               End
               Begin VB.TextBox txtText1 
                  Alignment       =   1  'Right Justify
                  DataField       =   "FR73DOSISUSUAL"
                  Height          =   330
                  Index           =   13
                  Left            =   240
                  TabIndex        =   17
                  Tag             =   "D.U.|Dosis Usual"
                  Top             =   1080
                  Width           =   975
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR93CODUNIMEDIDA_USU"
                  Height          =   330
                  Index           =   12
                  Left            =   1320
                  TabIndex        =   18
                  Tag             =   "U.M.D.U.|C�d. Unidad Medida Dosis Usual"
                  Top             =   1080
                  Width           =   960
               End
               Begin VB.TextBox txtText1 
                  DataField       =   "FR34CODVIA"
                  Height          =   330
                  Index           =   26
                  Left            =   240
                  TabIndex        =   15
                  Tag             =   "V�a"
                  Top             =   480
                  Width           =   615
               End
               Begin VB.TextBox txtText1 
                  BackColor       =   &H00C0C0C0&
                  Height          =   330
                  Index           =   25
                  Left            =   960
                  TabIndex        =   16
                  TabStop         =   0   'False
                  Tag             =   "Desc.V�a"
                  Top             =   480
                  Width           =   5400
               End
               Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
                  DataField       =   "FR73CODPRODUCTO_DIL"
                  Height          =   330
                  Index           =   0
                  Left            =   240
                  TabIndex        =   208
                  Tag             =   "Producto Diluyente"
                  Top             =   3480
                  Width           =   2985
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�D"
                  Columns(0).Name =   "C�DIGO"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   8943
                  Columns(1).Caption=   "DESCRIPCI�N"
                  Columns(1).Name =   "DESCRIPCI�N"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5265
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin SSDataWidgets_B.SSDBCombo cboSSDBCombo1 
                  DataField       =   "FR73CODPRODUCTO_REC"
                  Height          =   330
                  Index           =   1
                  Left            =   240
                  TabIndex        =   209
                  Tag             =   "Producto Reconstituyente"
                  Top             =   2880
                  Width           =   2985
                  DataFieldList   =   "Column 0"
                  AllowInput      =   0   'False
                  _Version        =   131078
                  DataMode        =   2
                  BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  RowHeight       =   423
                  Columns.Count   =   2
                  Columns(0).Width=   3200
                  Columns(0).Visible=   0   'False
                  Columns(0).Caption=   "C�D"
                  Columns(0).Name =   "C�DIGO"
                  Columns(0).DataField=   "Column 0"
                  Columns(0).DataType=   8
                  Columns(0).FieldLen=   256
                  Columns(1).Width=   8943
                  Columns(1).Caption=   "DESCRIPCI�N"
                  Columns(1).Name =   "DESCRIPCI�N"
                  Columns(1).DataField=   "Column 1"
                  Columns(1).DataType=   8
                  Columns(1).FieldLen=   256
                  _ExtentX        =   5265
                  _ExtentY        =   582
                  _StockProps     =   93
                  BackColor       =   -2147483643
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "ml/hora"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   55
                  Left            =   5040
                  TabIndex        =   255
                  Top             =   4170
                  Width           =   660
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Velocidad Perfusi�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   56
                  Left            =   3960
                  TabIndex        =   248
                  Top             =   3840
                  Width           =   1935
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Observaciones"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   31
                  Left            =   6840
                  TabIndex        =   166
                  Top             =   3120
                  Width           =   1275
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosificaci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   30
                  Left            =   6840
                  TabIndex        =   165
                  Top             =   1680
                  Width           =   1065
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Indicaciones"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   29
                  Left            =   6840
                  TabIndex        =   164
                  Top             =   240
                  Width           =   1095
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Redondeo   Unidades"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   28
                  Left            =   240
                  TabIndex        =   163
                  Top             =   2040
                  Width           =   1860
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Caducidad (horas)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   26
                  Left            =   1800
                  TabIndex        =   162
                  Top             =   3840
                  Width           =   1560
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Infusi�n (minutos)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   25
                  Left            =   240
                  TabIndex        =   161
                  Top             =   3840
                  Width           =   1515
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen (mL)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   23
                  Left            =   3360
                  TabIndex        =   160
                  Top             =   3240
                  Width           =   1155
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Infusi�n I.V. en"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   21
                  Left            =   240
                  TabIndex        =   159
                  Top             =   3240
                  Width           =   1320
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Concentraci�n (Ud/mL)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   20
                  Left            =   4680
                  TabIndex        =   158
                  Top             =   2640
                  Width           =   1995
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Volumen (mL)"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   19
                  Left            =   3360
                  TabIndex        =   157
                  Top             =   2640
                  Width           =   1155
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Reconstituir con"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   17
                  Left            =   240
                  TabIndex        =   156
                  Top             =   2640
                  Width           =   1410
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "por"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   16
                  Left            =   2520
                  TabIndex        =   155
                  Top             =   1800
                  Width           =   285
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis M�xima"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   15
                  Left            =   240
                  TabIndex        =   154
                  Top             =   1440
                  Width           =   1170
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "por"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   13
                  Left            =   2520
                  TabIndex        =   153
                  Top             =   1200
                  Width           =   285
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "Dosis Usual"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   12
                  Left            =   240
                  TabIndex        =   152
                  Top             =   840
                  Width           =   1020
               End
               Begin VB.Label lblLabel1 
                  AutoSize        =   -1  'True
                  Caption         =   "V�a de Administraci�n"
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   195
                  Index           =   24
                  Left            =   240
                  TabIndex        =   151
                  Top             =   240
                  Width           =   1875
               End
            End
            Begin VB.CommandButton cmdWord 
               Caption         =   "W"
               BeginProperty Font 
                  Name            =   "MS Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   495
               Left            =   5520
               TabIndex        =   149
               Top             =   3660
               Width           =   495
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73VOLUMEN"
               Height          =   330
               Index           =   11
               Left            =   7200
               TabIndex        =   9
               Tag             =   "Volumen|Volumen en mililitros en la que se diluye la dosis"
               Top             =   1620
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR93CODUNIMEDIDA"
               Height          =   330
               Index           =   10
               Left            =   6120
               TabIndex        =   8
               Tag             =   "U.M.|C�d. de la unidad de medida de la dosis"
               Top             =   1620
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73DOSIS"
               Height          =   330
               Index           =   9
               Left            =   5040
               TabIndex        =   7
               Tag             =   "Dosis"
               Top             =   1620
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PATHINSPAC"
               Height          =   330
               Index           =   5
               Left            =   120
               TabIndex        =   14
               Tag             =   "Archivo Inf. Paciente Producto"
               Top             =   3780
               Width           =   4725
            End
            Begin VB.Frame Frame1 
               ForeColor       =   &H00000000&
               Height          =   2175
               Left            =   8520
               TabIndex        =   139
               Top             =   2220
               Width           =   2055
               Begin VB.CommandButton cmdprocfab 
                  Caption         =   "Proceso de Fabricaci�n"
                  Height          =   375
                  Left            =   120
                  TabIndex        =   143
                  Top             =   1680
                  Width           =   1815
               End
               Begin VB.CommandButton cmdsustalergia 
                  Caption         =   "Sustituci�n Alergias"
                  Height          =   375
                  Left            =   120
                  TabIndex        =   142
                  Top             =   1200
                  Width           =   1815
               End
               Begin VB.CommandButton cmdsustitutos 
                  Caption         =   "Sustitutos"
                  Height          =   375
                  Left            =   120
                  TabIndex        =   141
                  Top             =   720
                  Width           =   1815
               End
               Begin VB.CommandButton cmdsinonimos 
                  Caption         =   "Sin�nimos"
                  Height          =   375
                  Left            =   120
                  TabIndex        =   140
                  Top             =   240
                  Width           =   1815
               End
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73REFERENCIA"
               Height          =   330
               Index           =   4
               Left            =   1440
               TabIndex        =   5
               Tag             =   "Referencia"
               Top             =   1620
               Width           =   2655
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73CODNAC"
               Height          =   330
               Index           =   3
               Left            =   120
               TabIndex        =   4
               Tag             =   "C�d. Nac.|C�digo Nacional"
               Top             =   1620
               Width           =   735
            End
            Begin VB.CheckBox chkCheck1 
               Caption         =   "Producto Tipo A"
               DataField       =   "FR73INDPRODA"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   -67920
               TabIndex        =   125
               Tag             =   "Tipo A?"
               Top             =   2580
               Width           =   2055
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODINTFARSEG"
               Height          =   330
               Index           =   30
               Left            =   1200
               TabIndex        =   1
               Tag             =   "C.S.|C�digo Interno Seguridad"
               Top             =   1020
               Width           =   255
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR00CODGRPTERAP"
               Height          =   330
               Index           =   36
               Left            =   120
               TabIndex        =   11
               Tag             =   "Grp. Terap�.|Grupo Terape�tico"
               Top             =   2220
               Width           =   1800
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00C0C0C0&
               Height          =   330
               Index           =   35
               Left            =   2040
               TabIndex        =   12
               TabStop         =   0   'False
               Tag             =   "Desc.Grupo Terape�tico"
               Top             =   2220
               Width           =   6045
            End
            Begin VB.TextBox txtText1 
               DataField       =   "FR73PATHIMAGEN"
               Height          =   330
               Index           =   17
               Left            =   120
               TabIndex        =   13
               Tag             =   "Imagen"
               Top             =   2940
               Width           =   5325
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H00FFFFFF&
               DataField       =   "FR73DESPRODUCTO"
               Height          =   330
               Index           =   1
               Left            =   1680
               TabIndex        =   2
               Tag             =   "Producto|Descripci�n Producto"
               Top             =   1020
               Width           =   6540
            End
            Begin VB.TextBox txtText1 
               Alignment       =   1  'Right Justify
               DataField       =   "FR73CODINTFAR"
               Height          =   330
               Index           =   22
               Left            =   120
               TabIndex        =   0
               Tag             =   "C�d. Int.|C�digo Interno Farmacia"
               Top             =   1020
               Width           =   975
            End
            Begin VB.TextBox txtText1 
               BackColor       =   &H0000FFFF&
               DataField       =   "FR73CODPRODUCTO"
               Height          =   330
               Index           =   0
               Left            =   6600
               TabIndex        =   202
               Tag             =   "C�digo Producto"
               Top             =   2940
               Width           =   1020
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR73FECFINVIG"
               Height          =   330
               Index           =   1
               Left            =   8520
               TabIndex        =   10
               Tag             =   "Fin Vig.|Fecha Fin Vigencia"
               Top             =   1620
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
               DataField       =   "FR73FECINIVIG"
               Height          =   330
               Index           =   0
               Left            =   8520
               TabIndex        =   3
               Tag             =   "Inicio Vig.|Fecha Inicio Vigencia"
               Top             =   1020
               Width           =   1620
               _Version        =   65537
               _ExtentX        =   2857
               _ExtentY        =   582
               _StockProps     =   93
               BackColor       =   -2147483643
               BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               DefaultDate     =   ""
               MinDate         =   "1900/1/1"
               MaxDate         =   "2100/12/31"
               Format          =   "DD/MM/YYYY"
               AllowNullDate   =   -1  'True
               ShowCentury     =   -1  'True
               Mask            =   2
               StartofWeek     =   2
            End
            Begin MSComDlg.CommonDialog CommonDialog1 
               Left            =   5520
               Top             =   4140
               _ExtentX        =   847
               _ExtentY        =   847
               _Version        =   327681
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Indicaciones autorizadas para el medicamento"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   59
               Left            =   -74760
               TabIndex        =   254
               Top             =   480
               Width           =   4665
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Volumen (mL)"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   11
               Left            =   7200
               TabIndex        =   148
               Top             =   1380
               Width           =   1155
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Unid.Med."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   10
               Left            =   6120
               TabIndex        =   147
               Top             =   1380
               Width           =   885
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Dosis"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   9
               Left            =   5040
               TabIndex        =   146
               Top             =   1380
               Width           =   480
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Forma F."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   8
               Left            =   4200
               TabIndex        =   145
               Top             =   1380
               Width           =   750
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Archivo Inf. Paciente Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   4
               Left            =   120
               TabIndex        =   144
               Top             =   3540
               Width           =   2640
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "Referencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   3
               Left            =   1440
               TabIndex        =   138
               Top             =   1380
               Width           =   945
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d.Nacional"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   137
               Top             =   1380
               Width           =   1155
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Grupo Terape�tico"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   27
               Left            =   120
               TabIndex        =   136
               Top             =   1980
               Width           =   2055
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Archivo Imagen Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   18
               Left            =   120
               TabIndex        =   135
               Top             =   2700
               Width           =   2655
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Fin Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   5
               Left            =   8520
               TabIndex        =   134
               Top             =   1380
               Width           =   1215
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Inicio Vigencia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Index           =   2
               Left            =   8520
               TabIndex        =   133
               Top             =   780
               Width           =   1455
            End
            Begin VB.Label lblLabel1 
               Caption         =   "Descripci�n Producto"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Index           =   14
               Left            =   1680
               TabIndex        =   132
               Top             =   780
               Width           =   2535
            End
            Begin VB.Label lblLabel1 
               AutoSize        =   -1  'True
               Caption         =   "C�d.Int.Farmacia"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   195
               Index           =   22
               Left            =   120
               TabIndex        =   131
               Top             =   780
               Width           =   1455
            End
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   64
      Top             =   8010
      Width           =   12210
      _ExtentX        =   21537
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Label lblLabel1 
      AutoSize        =   -1  'True
      Caption         =   "Redondeo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   44
      Left            =   0
      TabIndex        =   235
      Top             =   0
      Width           =   885
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda"
         Index           =   10
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "frmDefProducto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmDefProducto (FR0035.FRM)                                  *
'* AUTOR: IRENE V�ZQUEZ MART�NEZ                                        *
'* FECHA: AGOSTO DE 1998                                                *
'* DESCRIPCION: definici�n de productos                                 *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim intmantenimiento As Integer

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboSSDBCombo1_GotFocus(intIndex As Integer)
    Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboSSDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboSSDBCombo1_CloseUp(intIndex As Integer)
Call objWinInfo.CtrlDataChange
If intIndex = 0 Then
    txtproddiluyente.Text = cboSSDBCombo1(0).Columns(1).Value
End If
If intIndex = 1 Then
    txtprodreconstituyente.Text = cboSSDBCombo1(1).Columns(1).Value
End If
End Sub

Private Sub cboSSDBCombo1_Change(Index As Integer)

Call objWinInfo.CtrlDataChange

End Sub

Private Sub cboSSDBCombo1_Click(Index As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


Private Sub cmdbuscar_Click()
Dim v As Integer
Dim i As Integer
Dim noinsertar As Boolean
Dim mensaje As String

If txtText1(0).Text = "" Then
    mensaje = MsgBox("No hay seleccionado ning�n Producto", vbInformation)
    Exit Sub
End If

cmdbuscar.Enabled = False
noinsertar = True
Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
Call objsecurity.LaunchProcess("FR0053")
Call objWinInfo.FormChangeActive(fraframe1(0), False, True)
For v = 0 To gintpricipioactivototal - 1
      'se mira que no se inserte 2 veces el mismo producto
      If grdDBGrid1(0).Rows > 0 Then
        grdDBGrid1(0).MoveFirst
        For i = 0 To grdDBGrid1(0).Rows - 1
            If grdDBGrid1(0).Columns(3).Value = "" & gintpricipioactivobuscado(v, 0) & "" _
               And grdDBGrid1(0).Columns(0).Value <> "Eliminado" Then
               'no se inserta el protocolo pues ya est� insertado en el grid
                noinsertar = False
                Exit For
            Else
                noinsertar = True
            End If
        grdDBGrid1(0).MoveNext
        Next i
      End If
      If noinsertar = True Then
        Call objWinInfo.WinProcess(cwProcessToolBar, 2, 0)
        grdDBGrid1(0).Columns(3).Value = gintpricipioactivobuscado(v, 0)
        grdDBGrid1(0).Columns(4).Value = gintpricipioactivobuscado(v, 1)
        grdDBGrid1(0).Columns(7).Value = txtText1(0).Text 'Cod.Producto
      Else
        mensaje = MsgBox("El principio: " & gintpricipioactivobuscado(v, 1) & " ya est� guardado." & _
                       Chr(13), vbInformation)
      End If
      noinsertar = True
Next v

gintpricipioactivototal = 0

cmdbuscar.Enabled = True
End Sub

Private Sub cmdExplorador_Click(Index As Integer)
Dim intRetVal
Dim strOpenFileName As String
Dim pathFileName As String
Dim finpath As Integer

If txtText1(0).Text = "" Then
  Exit Sub
End If

If Index = 0 Then
    On Error Resume Next
    pathFileName = Dir(txtText1(17).Text)
    finpath = InStr(1, txtText1(17).Text, pathFileName, 1)
    pathFileName = Left(txtText1(17).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(17).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If ERR.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(17).SetFocus
        Call objWinInfo.CtrlSet(txtText1(17), "")
        txtText1(17).SetFocus
        Call objWinInfo.CtrlSet(txtText1(17), strOpenFileName)
    End If
Else
    On Error Resume Next
    pathFileName = Dir(txtText1(5).Text)
    finpath = InStr(1, txtText1(5).Text, pathFileName, 1)
    pathFileName = Left(txtText1(5).Text, finpath - 1)
    CommonDialog1.InitDir = pathFileName
    CommonDialog1.filename = txtText1(5).Text
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "*.*"
    CommonDialog1.ShowOpen
    If ERR.Number <> 32755 Then    ' El usuario eligi� Cancelar.
        strOpenFileName = CommonDialog1.filename
        txtText1(5).SetFocus
        Call objWinInfo.CtrlSet(txtText1(5), "")
        txtText1(5).SetFocus
        Call objWinInfo.CtrlSet(txtText1(5), strOpenFileName)
    End If
End If


End Sub


Private Sub cmdprocfab_Click()
Dim mensaje As String
cmdprocfab.Enabled = False
If txtText1(0).Text <> "" Then
    If (chkCheck1(10).Value = 1) Or (chkCheck1(45).Value = 1) Then
        Call objsecurity.LaunchProcess("FR0041")
    Else
        mensaje = MsgBox("El producto no es fabricable.", vbInformation, "Aviso")
    End If
End If
cmdprocfab.Enabled = True
End Sub

Private Sub cmdsinonimos_Click()
cmdsinonimos.Enabled = False
frmDefSinonimo.txtProd(0).Text = txtText1(0).Text
frmDefSinonimo.txtProd(1).Text = txtText1(1).Text
frmDefSinonimo.txtProd(2).Text = txtText1(22).Text
Call objsecurity.LaunchProcess("FR0038")
cmdsinonimos.Enabled = True
End Sub

Private Sub cmdsustalergia_Click()
cmdsustalergia.Enabled = False
frmDefSustAlergia.txtProd(0).Text = txtText1(0).Text
frmDefSustAlergia.txtProd(1).Text = txtText1(1).Text
frmDefSustAlergia.txtProd(2).Text = txtText1(22).Text
Call objsecurity.LaunchProcess("FR0037")
cmdsustalergia.Enabled = True
End Sub

Private Sub cmdsustitutos_Click()
cmdsustitutos.Enabled = False
frmDefSustituto.txtProd(0).Text = txtText1(0).Text
frmDefSustituto.txtProd(1).Text = txtText1(1).Text
frmDefSustituto.txtProd(2).Text = txtText1(22).Text
Call objsecurity.LaunchProcess("FR0036")
cmdsustitutos.Enabled = True
End Sub

Private Sub cmdWord_Click()
Dim stra As String
Dim rsta As rdoResultset
Dim strpathword As String
  
On Error GoTo error_shell
  
'Par�metros generales
'5,Ubicaci�n de Word,C:\Archivos de programa\Microsoft Office\Office\winword.exe,TEXTO
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=5"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strpathword = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el Path del Word en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 5 " & Chr(13) & _
           "Ej.: C:\Archivos de programa\Microsoft Office\Office\winword.exe", vbInformation
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  If txtText1(5).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del documento", vbInformation
    Exit Sub
  End If
  
  'Shell "C:\Archivos de programa\Microsoft Office\Office\winword.exe " & txtText1(5).Text, vbMaximizedFocus
  Shell strpathword & " " & txtText1(5).Text, vbMaximizedFocus

error_shell:

  If ERR.Number <> 0 Then
    MsgBox "Error N�mero: " & ERR.Number & Chr(13) & ERR.Description, vbCritical
  End If

End Sub


Private Sub Form_Activate()
If txtText1(0).Text = "" Then
    cmdsinonimos.Enabled = False
    cmdsustitutos.Enabled = False
    cmdsustalergia.Enabled = False
    cmdprocfab.Enabled = False
Else
    cmdsinonimos.Enabled = True
    cmdsustitutos.Enabled = True
    cmdsustalergia.Enabled = True
    cmdprocfab.Enabled = True
End If
txtproddiluyente.Locked = True
txtprodreconstituyente.Locked = True
txtproddiluyente.ZOrder (0)
txtprodreconstituyente.ZOrder (0)

fraframe1(2).Visible = False

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()

  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim strKey As String
  
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
  With objMasterInfo
    Set .objFormContainer = fraframe1(1)
    Set .objFatherContainer = Nothing
    Set .tabMainTab = tabTab1(0)
    Set .grdGrid = grdDBGrid1(2)
    
    .strName = "Productos"
      
    .strTable = "FR7300"
    .blnAskPrimary = False
    .blnHasMaint = True
    
    Call .FormAddOrderField("FR7300.FR73DESPRODUCTO", cwAscending)
    
    Call .objPrinter.Add("FR0351", "Especialidad")
    Call .objPrinter.Add("FR0352", "Datos Cl�nicos")
    Call .objPrinter.Add("FR0353", "Compras")
    Call .objPrinter.Add("FR0354", "Almac�n")
    Call .objPrinter.Add("FR0355", "Maestro")

   
    strKey = .strDataBase & .strTable
    Call .FormCreateFilterWhere(strKey, "Productos")
    Call .FormAddFilterWhere(strKey, "FR73CODINTFAR", "C�digo Interno", cwString)
    Call .FormAddFilterWhere(strKey, "FR73CODINTFARSEG", "C�digo Seguridad", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTO", "Descripci�n Producto", cwString)
    Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmace�tica", cwString)
    Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwDecimal)
    Call .FormAddFilterWhere(strKey, "FR93CODUNIMEDIDA", "Unidad Medida", cwString)
    Call .FormAddFilterWhere(strKey, "FR73REFERENCIA", "Referencia", cwString)
    Call .FormAddFilterWhere(strKey, "FR00CODGRPTERAP", "Grupo Terap�utico", cwString)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR_A", "C�d. Proveedor A", cwNumeric)
    Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR_B", "C�d. Proveedor B", cwNumeric)
    'Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR_C", "C�d. Proveedor C", cwNumeric)
    
    Call .FormAddFilterOrder(strKey, "FR73CODINTFAR", "C�digo Interno")
    Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTO", "Descripci�n Producto")
   
  
  End With
  
  With objMultiInfo
    Set .objFormContainer = fraframe1(0)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(0)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Principios Activos"
    
    .strTable = "FR6400"
    
    Call .FormAddOrderField("FR68CODPRINCACTIV", cwAscending)
    Call .FormAddRelation("FR73CODPRODUCTO", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  With objMultiInfo1
    Set .objFormContainer = fraframe1(2)
    Set .objFatherContainer = fraframe1(1)
    Set .tabMainTab = Nothing
    Set .grdGrid = grdDBGrid1(1)
    .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
    .strName = "Restricciones"
    
    .strTable = "FRA900"
    
    Call .FormAddOrderField("FRA9DESREST", cwAscending)
    Call .FormAddRelation("FR73CODPRODUCTO", txtText1(0))
    
    strKey = .strDataBase & .strTable
    
  End With
  
  
  With objWinInfo
    Call .FormAddInfo(objMasterInfo, cwFormDetail)
    Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
    Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
  
    Call .GridAddColumn(objMultiInfo, "C�d.Princ.Act.", "FR68CODPRINCACTIV", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo, "Descripci�n Principio Activo", "", cwString, 50)
    Call .GridAddColumn(objMultiInfo, "Dosis", "FR64DOSIS", cwDecimal, 2)
    Call .GridAddColumn(objMultiInfo, "Unidad de Medida", "FR93CODUNIMEDIDA", cwString, 5)
    Call .GridAddColumn(objMultiInfo, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    
    Call .GridAddColumn(objMultiInfo1, "C�d.Producto", "FR73CODPRODUCTO", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "C�d.Restricci�n", "FRA9CODREST", cwNumeric, 9)
    Call .GridAddColumn(objMultiInfo1, "Restricci�n", "FRA9DESREST", cwString, 65)
    
    Call .FormCreateInfo(objMasterInfo)
    
    ' la primera columna es la 3 ya que hay 1 de estado y otras 2 invisibles
    .CtrlGetInfo(grdDBGrid1(0).Columns(3)).intKeyNo = 1
    
    'Se indica que campos son obligatorios y cuales son clave primaria
    .CtrlGetInfo(txtText1(0)).intKeyNo = 1
    .CtrlGetInfo(txtText1(1)).blnMandatory = True
    

    Call .FormChangeColor(objMultiInfo)
    Call .FormChangeColor(objMultiInfo1)
    
    .CtrlGetInfo(txtText1(22)).blnInFind = True
    .CtrlGetInfo(txtText1(30)).blnInFind = True
    .CtrlGetInfo(txtText1(1)).blnInFind = True
    .CtrlGetInfo(txtText1(8)).blnInFind = True
    .CtrlGetInfo(txtText1(9)).blnInFind = True
    .CtrlGetInfo(txtText1(10)).blnInFind = True
    .CtrlGetInfo(txtText1(4)).blnInFind = True
    .CtrlGetInfo(txtText1(55)).blnInFind = True  'proveedor_A
    .CtrlGetInfo(txtText1(39)).blnInFind = True  'proveedor_B
    '.CtrlGetInfo(txtText1(41)).blnInFind = True  'proveedor_C
    .CtrlGetInfo(txtText1(84)).blnInFind = True  'descripci�n del producto del proveedor
    
    .CtrlGetInfo(txtproddiluyente).blnNegotiated = False
    .CtrlGetInfo(txtprodreconstituyente).blnNegotiated = False
    
    'Se rellenan las combos
    .CtrlGetInfo(cboSSDBCombo1(0)).strSQL = "SELECT fr73codproducto,FR73DESPRODUCTO" & _
    " FROM FR7300 WHERE FR73INDINFIV=-1 order by FR73DESPRODUCTO asc"
    
    .CtrlGetInfo(cboSSDBCombo1(1)).strSQL = "SELECT fr73codproducto,FR73DESPRODUCTO" & _
    " FROM FR7300 WHERE FR73INDPRODREC=-1 order by FR73DESPRODUCTO asc"

    Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), "FR68CODPRINCACTIV", "SELECT * FROM FR6800 WHERE FR68CODPRINCACTIV = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(0).Columns(3)), grdDBGrid1(0).Columns(4), "FR68DESPRINCACTIV")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(14)), "FRG4CODFRECUENCIA_USU", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(14)), txtText1(15), "FRG4DESFRECUENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(19)), "FRG4CODFRECUENCIA_MAX", "SELECT * FROM FRG400 WHERE FRG4CODFRECUENCIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(19)), txtText1(20), "FRG4DESFRECUENCIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(36)), "FR00CODGRPTERAP", "SELECT * FROM FR0000 WHERE FR00CODGRPTERAP = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(36)), txtText1(35), "FR00DESGRPTERAP")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(26)), "FR34CODVIA", "SELECT * FROM FR3400 WHERE FR34CODVIA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(26)), txtText1(25), "FR34DESVIA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(55)), "FR79CODPROVEEDOR_A", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(55)), txtText1(54), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(39)), "FR79CODPROVEEDOR_B", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(39)), txtText1(38), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(41)), "FR79CODPROVEEDOR_C", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(41)), txtText1(40), "FR79PROVEEDOR")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(56)), "FR88CODTIPIVA", "SELECT * FROM FR8800 WHERE FR88CODTIPIVA = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(56)), txtText1(57), "FR88DESCIVA")
    
    Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(60)), "FR41CODGRUPPROD", "SELECT FR41CODGRUPPROD,FR41DESGRUPPROD FROM FR4100 WHERE FR41CODGRUPPROD = ?")
    Call .CtrlAddLinked(.CtrlGetInfo(txtText1(60)), txtText1(76), "FR41DESGRUPPROD")
    
    .CtrlGetInfo(txtText1(8)).blnForeign = True
    .CtrlGetInfo(txtText1(10)).blnForeign = True
    .CtrlGetInfo(txtText1(36)).blnForeign = True
    .CtrlGetInfo(txtText1(26)).blnForeign = True
    .CtrlGetInfo(txtText1(12)).blnForeign = True
    .CtrlGetInfo(txtText1(16)).blnForeign = True
    .CtrlGetInfo(txtText1(14)).blnForeign = True
    .CtrlGetInfo(txtText1(19)).blnForeign = True
    .CtrlGetInfo(txtText1(55)).blnForeign = True
    .CtrlGetInfo(txtText1(39)).blnForeign = True
    .CtrlGetInfo(txtText1(41)).blnForeign = True
    .CtrlGetInfo(txtText1(56)).blnForeign = True
    .CtrlGetInfo(txtText1(59)).blnForeign = True
    .CtrlGetInfo(txtText1(60)).blnForeign = True
    .CtrlGetInfo(txtText1(61)).blnForeign = True
    .CtrlGetInfo(txtText1(74)).blnForeign = True
    .CtrlGetInfo(grdDBGrid1(0).Columns(6)).blnForeign = True
    
    .CtrlGetInfo(txtText1(30)).blnReadOnly = True 'Cod Interno Seguridad
    .CtrlGetInfo(txtText1(24)).blnReadOnly = True 'Cod Interno Seguridad
    
    .CtrlGetInfo(txtText1(62)).blnReadOnly = True
    .CtrlGetInfo(txtText1(67)).blnReadOnly = True
    .CtrlGetInfo(txtText1(66)).blnReadOnly = True
    .CtrlGetInfo(txtText1(70)).blnReadOnly = True
    .CtrlGetInfo(txtText1(79)).blnReadOnly = True
    .CtrlGetInfo(txtText1(75)).blnReadOnly = True
    .CtrlGetInfo(txtText1(78)).blnReadOnly = True
    .CtrlGetInfo(txtText1(23)).blnReadOnly = True
    .CtrlGetInfo(txtText1(68)).blnReadOnly = True
    .CtrlGetInfo(txtText1(69)).blnReadOnly = True
    
    .CtrlGetInfo(txtText1(28)).blnReadOnly = True
    .CtrlGetInfo(txtText1(73)).blnReadOnly = True
    .CtrlGetInfo(txtText1(71)).blnReadOnly = True
    .CtrlGetInfo(txtText1(77)).blnReadOnly = True
    .CtrlGetInfo(txtText1(72)).blnReadOnly = True
    
    grdDBGrid1(0).Columns(0).Visible = False
    grdDBGrid1(0).Columns(3).Visible = False
    grdDBGrid1(0).Columns(7).Visible = False
    
    grdDBGrid1(1).Columns(3).Visible = False
    grdDBGrid1(1).Columns(4).Visible = False
   
    Call .WinRegister
    Call .WinStabilize
    
  End With

  grdDBGrid1(0).Columns(4).Width = 5500
  grdDBGrid1(2).Columns("Imagen").Width = 2000
  grdDBGrid1(2).Columns("Archivo Inf. Paciente Producto").Width = 2000
  grdDBGrid1(2).Columns("Indicaciones sobre el uso del producto").Width = 2000
  grdDBGrid1(2).Columns("Explicaci�n de la dosificaci�n").Width = 2000
  grdDBGrid1(2).Columns("Observaciones al producto").Width = 2000
  grdDBGrid1(2).Columns("Descripci�n de Grupo Contable").Width = 2000
  
  grdDBGrid1(2).Columns("EFP Especialidad Farmac�utica Publicitaria").Caption = "EFP"
  grdDBGrid1(2).Columns("EXO Excluido de la Oferta de la Seguridad Social").Caption = "EXO"
  grdDBGrid1(2).Columns("EFG Especialidad Farmac�utica Gen�rica").Caption = "EFG"
  grdDBGrid1(2).Columns("EFP").Width = 600
  grdDBGrid1(2).Columns("EXO").Width = 600
  grdDBGrid1(2).Columns("EFG").Width = 600
  
  
  grdDBGrid1(2).Columns("Indicaciones autorizadas").Width = 2000
  
  fraframe1(0).Visible = True
  Frame2(0).Visible = False
  'Se ocultan la fecha de inicio y fin de vigencia
  grdDBGrid1(2).Columns(4).Visible = False
  grdDBGrid1(2).Columns(11).Visible = False
Call objWinInfo.WinProcess(cwProcessToolBar, 3, 0)

Call crear_grafico

End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
Dim i As Integer
    
If Index = 2 Then
  If IsNumeric(grdDBGrid1(2).Columns(0).Value) Then
    If grdDBGrid1(2).Columns("Visible").Value = False Then
      For i = 1 To 136
          grdDBGrid1(2).Columns(i).CellStyleSet "PRODUCTOS-NO-VISIBLES"
      Next i
    End If
  End If
End If
End Sub

Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
  Dim intReport As Integer
  Dim objPrinter As clsCWPrinter
  Dim blnHasFilter As Boolean
  
  If strFormName = "Productos" Then
    Call objWinInfo.FormPrinterDialog(True, "")
    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
    intReport = objPrinter.Selected
    If intReport > 0 Then
      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
                                 objWinInfo.DataGetOrder(blnHasFilter, False))
    End If
    Set objPrinter = Nothing
  End If
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub




Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
Dim objField As clsCWFieldSearch
Dim mensaje As String
 
 If strFormName = "Principios Activos" And strCtrl = "grdDBGrid1(0).Unidad de Medida" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "Unidad de Medida"

     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n Unidad de Medida"

     If .Search Then
      Call objWinInfo.CtrlSet(grdDBGrid1(0).Columns(6), .cllValues("FR93CODUNIMEDIDA"))
      Call objWinInfo.CtrlDataChange
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(8)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH700"

     Set objField = .AddField("FRH7CODFORMFAR")
     objField.strSmallDesc = "C�digo Forma Farmace�tica"

     Set objField = .AddField("FRH7DESFORMFAR")
     objField.strSmallDesc = "Descripci�n Forma Farmace�tica"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(8), .cllValues("FRH7CODFORMFAR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(10)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad Medida"

     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n Unidad Medida"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(10), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(36)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR0000"

     Set objField = .AddField("FR00CODGRPTERAP")
     objField.strSmallDesc = "C�digo Grupo Terape�tico"

     Set objField = .AddField("FR00DESGRPTERAP")
     objField.strSmallDesc = "Descripci�n Grupo Terape�tico"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(36), .cllValues("FR00CODGRPTERAP"))
     End If
   End With
   Set objSearch = Nothing
 End If

 If strFormName = "Productos" And strCtrl = "txtText1(26)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR3400"

     Set objField = .AddField("FR34CODVIA")
     objField.strSmallDesc = "C�digo V�a"

     Set objField = .AddField("FR34DESVIA")
     objField.strSmallDesc = "Descripci�n V�a"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(26), .cllValues("FR34CODVIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(12)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad Medida"

     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n Unidad Medida"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(12), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(16)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad Medida"

     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n Unidad Medida"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(16), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(74)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR9300"

     Set objField = .AddField("FR93CODUNIMEDIDA")
     objField.strSmallDesc = "C�digo Unidad Medida"

     Set objField = .AddField("FR93DESUNIMEDIDA")
     objField.strSmallDesc = "Descripci�n Unidad Medida"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(74), .cllValues("FR93CODUNIMEDIDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(14)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRG400"

     Set objField = .AddField("FRG4CODFRECUENCIA")
     objField.strSmallDesc = "C�digo Frecuencia"

     Set objField = .AddField("FRG4DESFRECUENCIA")
     objField.strSmallDesc = "Descripci�n Frecuencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(14), .cllValues("FRG4CODFRECUENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(19)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRG400"

     Set objField = .AddField("FRG4CODFRECUENCIA")
     objField.strSmallDesc = "C�digo Frecuencia"

     Set objField = .AddField("FRG4DESFRECUENCIA")
     objField.strSmallDesc = "Descripci�n Frecuencia"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(19), .cllValues("FRG4CODFRECUENCIA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(55)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n Proveedor"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(55), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(39)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n Proveedor"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(39), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(41)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR7900"

     Set objField = .AddField("FR79CODPROVEEDOR")
     objField.strSmallDesc = "C�digo Proveedor"

     Set objField = .AddField("FR79PROVEEDOR")
     objField.strSmallDesc = "Descripci�n Proveedor"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(41), .cllValues("FR79CODPROVEEDOR"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(56)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR8800"

     Set objField = .AddField("FR88CODTIPIVA")
     objField.strSmallDesc = "C�digo Tipo IVA"

     Set objField = .AddField("FR88DESCIVA")
     objField.strSmallDesc = "Descripci�n Tipo IVA"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(56), .cllValues("FR88CODTIPIVA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(60)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FR4100"
     .strWhere = "where FRI7CODTIPGRP = 1"
   
     Set objField = .AddField("FR41CODGRUPPROD")
     objField.strSmallDesc = "C�digo Grupo Producto"

     Set objField = .AddField("FR41DESGRUPPROD")
     objField.strSmallDesc = "Descripci�n Grupo Producto"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(60), .cllValues("FR41CODGRUPPROD"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(61)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH900"

     Set objField = .AddField("FRH9UBICACION")
     objField.strSmallDesc = "Ubicaci�n"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(61), .cllValues("FRH9UBICACION"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
 If strFormName = "Productos" And strCtrl = "txtText1(59)" Then
    Set objSearch = New clsCWSearch
    With objSearch
     .strTable = "FRH800"

     Set objField = .AddField("FRH8MONEDA")
     objField.strSmallDesc = "Moneda"

     If .Search Then
      Call objWinInfo.CtrlSet(txtText1(59), .cllValues("FRH8MONEDA"))
     End If
   End With
   Set objSearch = Nothing
 End If
 
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
'si me posiciono en grupo terape�tico o subgrupo terape�tico que se active el mantenimiento
Select Case intmantenimiento
    Case 36
        Call objsecurity.LaunchProcess("FR0058")
    Case 10, 12, 16
        Call objsecurity.LaunchProcess("FR0020")
    Case 26
        Call objsecurity.LaunchProcess("FR0009")
    Case 14, 19
        Call objsecurity.LaunchProcess("FR0057")
    Case 41, 39, 55
        Call objsecurity.LaunchProcess("FR0026")
    Case 56
        Call objsecurity.LaunchProcess("FR0017")
    Case 8
        Call objsecurity.LaunchProcess("FR0070")
    Case 61
        Call objsecurity.LaunchProcess("FR0071")
End Select
End Sub







Private Sub objWinInfo_cwPostWrite(ByVal strFormName As String, ByVal blnError As Boolean)
Dim Precio_Neto As Currency
Dim Precio_Base As Currency
Dim Descuento As Currency
Dim IVA As Currency
Dim Recargo As Currency
Dim PVL As Currency
Dim PVP As Currency
Dim Param_Gen As Currency
Dim stra As String
Dim rsta As rdoResultset
Dim strPrecNetComp As String
Dim strupdate As String
Dim strPrecVenta As String

  If blnError = False Then
    'PRECIO NETO
    If UCase(Left(Me.txtText1(36), 1)) = "Q" Then
      'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
      If txtText1(46).Text <> "" Then 'Precio_Base
        Precio_Base = txtText1(46).Text
      Else
        Precio_Base = 0
      End If
      If txtText1(44).Text <> "" Then 'Descuento
        Descuento = Precio_Base * txtText1(44).Text / 100
      Else
        Descuento = 0
      End If
      If txtText1(58).Text <> "" Then 'Recargo
        Recargo = Precio_Base * txtText1(58).Text / 100
      Else
        Recargo = 0
      End If
      If txtText1(56).Text <> "" Then 'IVA
        IVA = (Precio_Base + Recargo - Descuento) * txtText1(56).Text / 100
      Else
        IVA = 0
      End If
      'If txtText1(58).Text <> "" Then 'Recargo
      '  Recargo = Precio_Base * txtText1(58).Text / 100
      'Else
      '  Recargo = 0
      'End If
      Precio_Neto = Precio_Base - Descuento + IVA + Recargo
      If txtText1(7).Text <> Format(Precio_Neto, "0.00") Then
        strPrecNetComp = objGen.ReplaceStr(txtText1(7).Text, ",", ".", 1)
        strupdate = "UPDATE FR7300 SET FR73PRECIONETCOMPRA=" & strPrecNetComp & " WHERE FR73CODPRODUCTO=" & txtText1(0).Text
        objApp.rdoConnect.Execute strupdate, 64
      End If
    Else
      'Precio_Neto=PVL-Descuento+IVA+Recargo
      If txtText1(2).Text <> "" Then 'PVL
        PVL = txtText1(2).Text
      Else
        PVL = 0
      End If
      If txtText1(44).Text <> "" Then 'Descuento
        Descuento = PVL * txtText1(44).Text / 100
      Else
        Descuento = 0
      End If
      If txtText1(58).Text <> "" Then 'Recargo
        Recargo = PVL * txtText1(58).Text / 100
      Else
        Recargo = 0
      End If
      If txtText1(56).Text <> "" Then 'IVA
        IVA = (PVL + Recargo - Descuento) * txtText1(56).Text / 100
      Else
        IVA = 0
      End If
      'If txtText1(58).Text <> "" Then 'Recargo
      '  Recargo = PVL * txtText1(58).Text / 100
      'Else
      '  Recargo = 0
      'End If
      Precio_Neto = PVL - Descuento + IVA + Recargo
      If txtText1(7).Text <> Format(Precio_Neto, "0.00") Then
        strPrecNetComp = objGen.ReplaceStr(txtText1(7).Text, ",", ".", 1)
        strupdate = "UPDATE FR7300 SET FR73PRECIONETCOMPRA=" & strPrecNetComp & " WHERE FR73CODPRODUCTO=" & txtText1(0).Text
        objApp.rdoConnect.Execute strupdate, 64
      End If
    End If

    'PVP
    If UCase(Left(Me.txtText1(36), 2)) = "QI" Then
      'PROTESIS PVP=Precio_Neto
      If txtText1(7).Text <> "" Then 'Precio_Neto
        Precio_Neto = txtText1(7).Text
      Else
        Precio_Neto = ""
      End If
      PVP = Precio_Neto
      If txtText1(6).Text <> Format(PVP, "0.000") Then
        'strPrecVenta = objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1)
        'strupdate = "UPDATE FR7300 SET FR73PRECIOVENTA=" & strPrecVenta & " WHERE FR73CODPRODUCTO=" & txtText1(0).Text
        'objApp.rdoConnect.Execute strupdate, 64
      End If
    Else
      If UCase(Left(Me.txtText1(36), 1)) = "Q" Then
      'PVP
        If txtText1(7).Text <> "" Then 'Precio_Neto
          Precio_Neto = txtText1(7).Text
        Else
          Precio_Neto = ""
        End If
        
        If Precio_Neto <= 2500 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=22"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        ElseIf Precio_Neto <= 5000 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=23"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        ElseIf Precio_Neto <= 10000 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=24"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        ElseIf Precio_Neto <= 25000 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=25"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        Else '>25000
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=26"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        End If
        PVP = Precio_Neto + Param_Gen
        If txtText1(6).Text <> Format(PVP, "0.000") Then
          'strPrecVenta = objGen.ReplaceStr(txtText1(6).Text, ",", ".", 1)
          'strupdate = "UPDATE FR7300 SET FR73PRECIOVENTA=" & strPrecVenta & " WHERE FR73CODPRODUCTO=" & txtText1(0).Text
          'objApp.rdoConnect.Execute strupdate, 64
        End If
      End If
    End If

  End If

'
'si se ha pinchado la check de sujeto a control de lote se debe mirar si existe alg�n
'grupo de productos que contenga a ese producto cuyo c�digo de servicio sea
'Farmacia(c�digo 1) y tenga los campos <Tiempo de estabilidad> y <Tipo de medida>
'rellenados.
'Esto influye para que luego el producto tenga caducidad o no.
'intnoexiste = True
'If chkCheck1(15).Value = 1 Then
'    strServicio = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=3"
'    Set rstServicio = objApp.rdoConnect.OpenResultset(strServicio)
'    Servicio = rstServicio("FRH2PARAMGEN").Value
'    rstServicio.Close
'    Set rstServicio = Nothing
'
'    stra = "SELECT * FROM FR0900 WHERE FR73CODPRODUCTO=" & txtText1(0).Text
'    Set rsta = objApp.rdoConnect.OpenResultset(stra)
'    Do While Not rsta.EOF
'        strgrupo = "SELECT count(*) FROM FR4100 WHERE " & _
'                 "FR41CODGRUPPROD=" & rsta.rdoColumns("FR41CODGRUPPROD").Value & " AND " & _
'                 "AD02CODDPTO=" & Servicio & " AND " & _
'                 "FR41NUMESTAB IS NOT NULL AND FR41TIPMEDIDA IS NOT NULL"
'        Set rstgrupo = objApp.rdoConnect.OpenResultset(strgrupo)
'        If rstgrupo.rdoColumns(0).Value > 0 Then
'            intnoexiste = False
'            Exit Do
'        Else
'            intnoexiste = True
'        End If
'        rstgrupo.Close
'        Set rstgrupo = Nothing
'    rsta.MoveNext
'    Loop
'    rsta.Close
'    Set rsta = Nothing
'    If intnoexiste = True Then
'        mensaje = MsgBox("Es necesario definir un Grupo de Productos ya que el producto" & _
'        Chr(13) & " tiene activado el indicador de Sujeto a Control de Lote.", vbInformation)
'    End If
'End If
'
End Sub

'
'Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Aportaciones Pendientes" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, False))
'    End If
'    Set objPrinter = Nothing
'  End If
'
'End Sub



Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)
Dim rsta As rdoResultset
Dim stra As String
Dim rdoQ As rdoQuery

'cada vez que se cambie de producto que se limpie la caja de texto txtproddiluyente y
'txtprodreconstituyente
    txtproddiluyente = ""
    txtprodreconstituyente = ""
    
'cuando se cambie de producto que se llene txtproddiluyente con la descripci�n
'de producto_diluyente(cbossdbcombo1(0))
    If cboSSDBCombo1(0).Value <> "" Then
        'bind variables
        'stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=" & _
              cboSSDBCombo1(0).Value
        stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
        rdoQ(0) = cboSSDBCombo1(0).Value
        Set rsta = rdoQ.OpenResultset(stra)
        txtproddiluyente.Text = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
    End If
'cuando se cambie de producto que se llene txtprodreconstituyente con la descripci�n
'de producto_reconstituyente(cbossdbcombo1(1))
    If cboSSDBCombo1(1).Value <> "" Then
        'bind variables
        'stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=" & _
              cboSSDBCombo1(1).Value
        stra = "SELECT FR73DESPRODUCTO FROM FR7300 WHERE FR73CODPRODUCTO=?"
        Set rdoQ = objApp.rdoConnect.CreateQuery("", stra)
        rdoQ(0) = cboSSDBCombo1(1).Value
        Set rsta = rdoQ.OpenResultset(stra)
        txtprodreconstituyente.Text = rsta.rdoColumns(0).Value
        rsta.Close
        Set rsta = Nothing
    End If
    
    aux1(0).Text = txtText1(22).Text
    aux1(1).Text = txtText1(30).Text
    aux1(2).Text = txtText1(1).Text
    aux1(3).Text = txtText1(3).Text
    aux1(4).Text = txtText1(4).Text
    aux1(5).Text = txtText1(8).Text
    aux1(6).Text = txtText1(9).Text
    aux1(7).Text = txtText1(10).Text
    aux1(8).Text = txtText1(11).Text
    
    'Para controlar que aparezcan los campos referentes al tama�o y decalage de los pedidos programados
    If chkCheck1(30).Value <> 0 Then
      'El producto se pide de forma programada
      txtText1(80).Visible = True
      txtText1(80).BackColor = &HFFFF00
      txtText1(81).Visible = True
      txtText1(81).BackColor = &HFFFF00
      lblLabel1(53).Visible = True
      lblLabel1(54).Visible = True
      Frame8.Visible = True
    Else
      'El producto se gestiona de forma no programada
      txtText1(80).Visible = False
      txtText1(81).Visible = False
      lblLabel1(53).Visible = False
      lblLabel1(54).Visible = False
      Frame8.Visible = False
    End If
'si est� clicada la casilla de Uso Restringido que se active el grid de Restricciones
If chkCheck1(8) <> 0 Then
    fraframe1(2).Enabled = True
Else
    fraframe1(2).Enabled = False
End If
'si se activan las casillas Extranjero o Uso Compasivo se activan que deje meter
'instrucciones autorizadas
If chkCheck1(6) <> 0 Or chkCheck1(43) <> 0 Then
    txtText1(85).Locked = False
    txtText1(85).BackColor = &HFFFFFF
Else
 If chkCheck1(6) = 0 And chkCheck1(43) = 0 Then
    txtText1(85).Locked = True
    txtText1(85).BackColor = &HC0C0C0
    txtText1(85).Text = ""
  End If
End If
End Sub

Private Sub objWinInfo_cwPreValidate(ByVal strFormName As String, blnCancel As Boolean)
  Dim vntA As Variant 'Para los mensages de error
  
  If Comprobar_Codigo_Seguridad = 0 Then
    Call Meter_Codigo_Seguridad(txtText1(22).Text)
  Else
    blnCancel = True
    Exit Sub
  End If
  If (chkCheck1(30).Value <> 0) And (strFormName = "Productos") Then
    'El producto se compra de forma programada
    If IsNumeric(txtText1(80).Text) Then
      'Se ha introducido un n�mero en la cantidad a recibir en cada entrega
      If txtText1(80).Text <= 0 Then
        'La cantidad a recibir es negativa y hay que sacar un mensage de error
        'ERROR
        Call objError.SetError(cwCodeMsg, "El N� de Envases a entregar en un pedido programado debe ser mayor que 0", vntA)
        vntA = objError.Raise
        blnCancel = True
        Exit Sub
      End If
    Else
      'No se ha introducido la cantidad a recibir por cada entrega y hay que sacar un mensage de error
      'ERROR
      Call objError.SetError(cwCodeMsg, "El campo N� Envases para un pedido programado debe ser tecleado obligatoriamente", vntA)
      vntA = objError.Raise
      blnCancel = True
      Exit Sub
    End If
    If IsNumeric(txtText1(81).Text) Then
      'Se ha introducido un n�mero para determinar el n�mero de d�as entre entrega
      If txtText1(81).Text <= 0 Then
        'el n� de dias entre entrega es negativa y hay que sacar un mensage de error
        'ERROR
        Call objError.SetError(cwCodeMsg, "El N� de d�as entre entregas programadas debe ser mayor que 0", vntA)
        vntA = objError.Raise
        blnCancel = True
        Exit Sub
      End If
    Else
      'No se ha introducido la cantidad a recibir por cada entrega y hay que sacar un mensage de error
      'ERROR
      Call objError.SetError(cwCodeMsg, "El N� de d�as entre entregas programadas es obligatorio", vntA)
      vntA = objError.Raise
      blnCancel = True
      Exit Sub
    End If
      
  End If
   
End Sub

Private Sub Picture1_DblClick()
Dim stra As String
Dim rsta As rdoResultset
Dim strpathword As String
  
On Error GoTo error_shell2
  
'Par�metros generales
'9,Ubicaci�n de Paint,C:\WINNT\system32\mspaint.exe,TEXTO
  stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=9"
  Set rsta = objApp.rdoConnect.OpenResultset(stra)
  If Not rsta.EOF Then
    strpathword = rsta(0).Value
  Else
    rsta.Close
    Set rsta = Nothing
    MsgBox "Debe introducir el Path del Programa de tratamiento de im�genes en par�metros generales." & Chr(13) & _
           "C�digo par�metros generales : 9 " & Chr(13) & _
           "Ej.: C:\WINNT\system32\mspaint.exe", vbInformation
    Exit Sub
  End If
  rsta.Close
  Set rsta = Nothing
  
  If txtText1(17).Text = "" Then
    MsgBox "Debe introducir el Path y el nombre del fichero del gr�fico", vbInformation
    Exit Sub
  End If
  
  'Shell "C:\WINNT\system32\pbrush.exe " & txtText1(17).Text, vbMaximizedFocus
  Shell strpathword & " " & txtText1(17).Text, vbMaximizedFocus

error_shell2:

  If ERR.Number <> 0 Then
    MsgBox "Error N�mero: " & ERR.Number & Chr(13) & ERR.Description, vbCritical
  End If


End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)

  If SSTab1.Tab = 0 Then
    fraframe1(0).Visible = True
    Frame2(0).Visible = False
  Else
    fraframe1(0).Visible = False
    Frame2(0).Visible = True
  End If
  
  If SSTab1.Tab = 6 Then
    fraframe1(2).Visible = True
  Else
    fraframe1(2).Visible = False
  End If
  

  If SSTab1.Tab = 5 Then
    Call crear_grafico
  End If

    aux1(0).Text = txtText1(22).Text
    aux1(1).Text = txtText1(30).Text
    aux1(2).Text = txtText1(1).Text
    aux1(3).Text = txtText1(3).Text
    aux1(4).Text = txtText1(4).Text
    aux1(5).Text = txtText1(8).Text
    aux1(6).Text = txtText1(9).Text
    aux1(7).Text = txtText1(10).Text
    aux1(8).Text = txtText1(11).Text
  
End Sub

Private Sub SSTab1_DblClick()

  If SSTab1.Tab = 0 Then
    fraframe1(0).Visible = True
    Frame2(0).Visible = False
  Else
    fraframe1(0).Visible = False
    Frame2(0).Visible = True
  End If
  
  If SSTab1.Tab = 6 Then
    fraframe1(2).Visible = True
  Else
    fraframe1(2).Visible = False
  End If

    aux1(0).Text = txtText1(22).Text
    aux1(1).Text = txtText1(30).Text
    aux1(2).Text = txtText1(1).Text
    aux1(3).Text = txtText1(3).Text
    aux1(4).Text = txtText1(4).Text
    aux1(5).Text = txtText1(8).Text
    aux1(6).Text = txtText1(9).Text
    aux1(7).Text = txtText1(10).Text
    aux1(8).Text = txtText1(11).Text

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTab1_Click(Index As Integer, PreviousTab As Integer)

  If tabTab1(0).Tab = 0 Then
    If SSTab1.Tab = 0 Then
      fraframe1(0).Visible = True
      Frame2(0).Visible = False
    Else
      fraframe1(0).Visible = False
      Frame2(0).Visible = True
    End If
  Else
    fraframe1(0).Visible = False
    Frame2(0).Visible = False
  End If

End Sub

Private Sub tabTab1_DblClick(Index As Integer)

  If tabTab1(0).Tab = 0 Then
    If SSTab1.Tab = 0 Then
      fraframe1(0).Visible = True
      Frame2(0).Visible = False
    Else
      fraframe1(0).Visible = False
      Frame2(0).Visible = True
    End If
  Else
    fraframe1(0).Visible = False
    Frame2(0).Visible = False
  End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "Numero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
Dim rsta As rdoResultset
Dim sqlstr As String

  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Principios Activos" Then
    MsgBox "S�lo puede a�adir principios activos mediante el buscador", vbExclamation
    Exit Sub
  End If
  
  If btnButton.Index = 2 And objWinInfo.objWinActiveForm.strName = "Restricciones" Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FRA9CODREST_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    grdDBGrid1(1).Columns(4).Value = rsta.rdoColumns(0).Value
    SendKeys "{TAB}", True
    SendKeys "{TAB}", True
    rsta.Close
    Set rsta = Nothing
    Exit Sub
  End If
  
  If btnButton.Index = 3 And objWinInfo.objWinActiveForm.strName = "Productos" Then
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(16)) 'Localizar
    Exit Sub
  End If
  
  If btnButton.Index = 2 Then
    Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
    sqlstr = "SELECT FR73CODPRODUCTO_SEQUENCE.nextval FROM dual"
    Set rsta = objApp.rdoConnect.OpenResultset(sqlstr)
    txtText1(0).Text = rsta.rdoColumns(0).Value
    dtcDateCombo1(0).Date = Date
    rsta.Close
    Set rsta = Nothing
    Exit Sub
  End If
     
  Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  
  Select Case intIndex
  Case 10 'Nuevo
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2))
  Case 20 'Abrir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3))
  Case 60 'Eliminar
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8))
  Case 100 'Salir
    Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30))
  Case Else
    Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
  End Select
  
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
   
  If intIndex = 2 Then
    If SSTab1.Tab = 0 Then
      fraframe1(0).Visible = True
    Else
      fraframe1(0).Visible = False
    End If
  End If
   
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
    Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    
    Call objWinInfo.CtrlDataChange

End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraframe1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Dim mensaje As String
  
  If intIndex = 1 Then
    If dtcDateCombo1(0).Date <> "" And dtcDateCombo1(1).Date <> "" Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
          dtcDateCombo1(1).Text = ""
      End If
    End If
  End If
  If intIndex = 0 Then
    If dtcDateCombo1(0).Date <> "" And dtcDateCombo1(1).Date <> "" Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
          dtcDateCombo1(0).Text = ""
      End If
    End If
  End If
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
Dim mensaje As String
  If intIndex = 1 Then
    If dtcDateCombo1(0).Date <> "" And dtcDateCombo1(1).Date <> "" Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          mensaje = MsgBox("La fecha de Fin de Vigencia es menor que la de Inicio", vbInformation, "Aviso")
          dtcDateCombo1(1).Text = ""
      End If
    End If
  End If
  If intIndex = 0 Then
    If dtcDateCombo1(0).Date <> "" And dtcDateCombo1(1).Date <> "" Then
      If DateDiff("d", dtcDateCombo1(1).Date, dtcDateCombo1(0).Date) > 0 Then
          mensaje = MsgBox("La fecha de Inicio de Vigencia es mayor que la de Fin", vbInformation, "Aviso")
          dtcDateCombo1(0).Text = ""
      End If
    End If
  End If
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
Dim rsta As rdoResultset
Dim stra As String
  
  Call objWinInfo.CtrlDataChange
  
  If intIndex = 14 Then
     If chkCheck1(14).Value <> 0 Then
        chkCheck1(15).Value = 0
     End If
  End If

  If intIndex = 15 Then
     If chkCheck1(15).Value <> 0 Then
        chkCheck1(14).Value = 0
     End If
  End If

  If intIndex = 28 Then
     If chkCheck1(28).Value <> 0 Then
        chkCheck1(29).Value = 0
        chkCheck1(30).Value = 0
        chkCheck1(31).Value = 0
        txtText1(80).Visible = False
        txtText1(81).Visible = False
        lblLabel1(53).Visible = False
        lblLabel1(54).Visible = False
        Frame8.Visible = False
     End If
  End If

  If intIndex = 29 Then
     If chkCheck1(29).Value <> 0 Then
        chkCheck1(28).Value = 0
        chkCheck1(30).Value = 0
        chkCheck1(31).Value = 0
        txtText1(80).Visible = False
        txtText1(81).Visible = False
        lblLabel1(53).Visible = False
        lblLabel1(54).Visible = False
        Frame8.Visible = False
     End If
  End If

  If intIndex = 30 Then
     'Pedidos Programados
     If chkCheck1(30).Value <> 0 Then
        chkCheck1(28).Value = 0
        chkCheck1(29).Value = 0
        chkCheck1(31).Value = 0
        txtText1(80).Visible = True
        txtText1(80).BackColor = &HFFFF00
        txtText1(81).Visible = True
        txtText1(81).BackColor = &HFFFF00
        lblLabel1(53).Visible = True
        lblLabel1(54).Visible = True
        Frame8.Visible = True
     End If
  End If

  If intIndex = 31 Then
     If chkCheck1(31).Value <> 0 Then
        chkCheck1(28).Value = 0
        chkCheck1(29).Value = 0
        chkCheck1(30).Value = 0
        txtText1(80).Visible = False
        txtText1(81).Visible = False
        lblLabel1(53).Visible = False
        lblLabel1(54).Visible = False
        Frame8.Visible = False
     End If
  End If
  
  If intIndex = 43 Then  'Uso Compasivo
    If chkCheck1(43).Value <> 0 Then 'indicaciones autorizadas
      txtText1(85).Locked = False
    Else
      txtText1(85).Locked = True
    End If
  End If
  
  If intIndex = 6 Then  'Extranjero
    If chkCheck1(6).Value <> 0 Then
       txtText1(85).Locked = False  'indicaciones autorizadas
    Else
      txtText1(85).Locked = True
    End If
  End If
  
  If intIndex = 8 Then  'Uso Restringido
      'si est� clicada la casilla de Uso Restringido que se active el grid de Restricciones
      If chkCheck1(8) <> 0 Then
          fraframe1(2).Enabled = True
      Else
          fraframe1(2).Enabled = False
      End If
  End If
  
'si se activan las casillas Extranjero o Uso Compasivo se activan que deje meter
'instrucciones autorizadas
If intIndex = 6 Or intIndex = 43 Then
  If chkCheck1(6) <> 0 Or chkCheck1(43) <> 0 Then
      txtText1(85).Locked = False
      txtText1(85).BackColor = &HFFFFFF
  Else
      If chkCheck1(6) = 0 And chkCheck1(43) = 0 Then
         txtText1(85).Locked = True
         txtText1(85).BackColor = &HC0C0C0
         txtText1(85).Text = ""
       End If
  End If
End If
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub



' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
  intmantenimiento = intIndex
End Sub

Private Sub txtText1_KeyPress(Index As Integer, KeyAscii As Integer)

  If Index = 22 Then
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9")
    Case Else
      KeyAscii = 0
    End Select
  End If

  Select Case Index
  Case 46, 44, 56, 58, 2, 6, 7
    Select Case KeyAscii
    Case Asc("0"), Asc("1"), Asc("2"), Asc("3"), Asc("4"), Asc("5"), Asc("6"), Asc("7"), Asc("8"), Asc("9"), Asc(",")
    Case Else
      KeyAscii = 0
    End Select
  End Select

End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
On Error GoTo ERR:
  Call objWinInfo.CtrlLostFocus
  Select Case intIndex
    Case 22:
        Call Meter_Codigo_Seguridad(txtText1(22).Text)
    Case 17:
        Picture1.Picture = LoadPicture(txtText1(17).Text)
  End Select
ERR:
    Picture1.Cls
  Exit Sub
End Sub

Private Sub txtText1_Change(intIndex As Integer)
Dim Precio_Neto As Currency
Dim Precio_Base As Currency
Dim Descuento As Currency
Dim IVA As Currency
Dim Recargo As Currency
Dim PVL As Currency
Dim PVP As Currency
Dim Param_Gen As Currency
Dim stra As String
Dim rsta As rdoResultset

  Call objWinInfo.CtrlDataChange
   
  If intIndex = 36 Then
    If UCase(Left(Me.txtText1(36), 1)) = "Q" Then
      'txtText1(2) FR73PVL
      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = True
      txtText1(2).Locked = True
      txtText1(2).BackColor = txtText1(35).BackColor
      If txtText1(2).Text <> "" Then
        txtText1(2).Text = ""
      End If
      'txtText1(6) FR73PRECIOVENTA
      'objWinInfo.CtrlGetInfo(txtText1(6)).blnReadOnly = True
      'txtText1(6).Locked = True
      'txtText1(6).BackColor = txtText1(35).BackColor
      'txtText1(46) FR73PRECBASE
      objWinInfo.CtrlGetInfo(txtText1(46)).blnReadOnly = False
      txtText1(46).Locked = False
      txtText1(46).BackColor = vbWhite
    Else
      'txtText1(2) FR73PVL
      objWinInfo.CtrlGetInfo(txtText1(2)).blnReadOnly = False
      txtText1(2).Locked = False
      txtText1(2).BackColor = vbWhite
      'txtText1(6) FR73PRECIOVENTA
      'objWinInfo.CtrlGetInfo(txtText1(6)).blnReadOnly = False
      'txtText1(6).Locked = False
      'txtText1(6).BackColor = vbWhite
      'txtText1(46) FR73PRECBASE
      objWinInfo.CtrlGetInfo(txtText1(46)).blnReadOnly = True
      txtText1(46).Locked = True
      txtText1(46).BackColor = txtText1(35).BackColor
      If txtText1(46).Text <> "" Then
        txtText1(46).Text = ""
      End If
    End If
  End If
  
  Select Case intIndex
  Case 36, 46, 44, 56, 58, 2
    'PRECIO NETO
    If UCase(Left(Me.txtText1(36), 1)) = "Q" Then
      'Precio_Neto=Precio_Base-Descuento+IVA+Recargo
      If txtText1(46).Text <> "" Then 'Precio_Base
        Precio_Base = txtText1(46).Text
      Else
        Precio_Base = 0
      End If
      If txtText1(44).Text <> "" Then 'Descuento
        Descuento = Precio_Base * txtText1(44).Text / 100
      Else
        Descuento = 0
      End If
      If txtText1(58).Text <> "" Then 'Recargo
        Recargo = Precio_Base * txtText1(58).Text / 100
      Else
        Recargo = 0
      End If
      If txtText1(56).Text <> "" Then 'IVA
        IVA = (Precio_Base + Recargo - Descuento) * txtText1(56).Text / 100
      Else
        IVA = 0
      End If
      'If txtText1(58).Text <> "" Then 'Recargo
      '  Recargo = Precio_Base * txtText1(58).Text / 100
      'Else
      '  Recargo = 0
      'End If
      Precio_Neto = Precio_Base - Descuento + IVA + Recargo
      txtText1(7) = Format(Precio_Neto, "0.00")
    Else
      'Precio_Neto=PVL-Descuento+IVA+Recargo
      If txtText1(2).Text <> "" Then 'PVL
        PVL = txtText1(2).Text
      Else
        PVL = 0
      End If
      If txtText1(44).Text <> "" Then 'Descuento
        Descuento = PVL * txtText1(44).Text / 100
      Else
        Descuento = 0
      End If
      If txtText1(58).Text <> "" Then 'Recargo
        Recargo = PVL * txtText1(58).Text / 100
      Else
        Recargo = 0
      End If
      If txtText1(56).Text <> "" Then 'IVA
        IVA = (PVL + Recargo - Descuento) * txtText1(56).Text / 100
      Else
        IVA = 0
      End If
      'If txtText1(58).Text <> "" Then 'Recargo
      '  Recargo = PVL * txtText1(58).Text / 100
      'Else
      '  Recargo = 0
      'End If
      Precio_Neto = PVL - Descuento + IVA + Recargo
      txtText1(7) = Format(Precio_Neto, "0.00")
    End If
  End Select

  Select Case intIndex
  Case 36, 7
    'PVP
    If UCase(Left(Me.txtText1(36), 2)) = "QI" Then
      'PROTESIS PVP=Precio_Neto
      If txtText1(7).Text <> "" Then 'Precio_Neto
        Precio_Neto = txtText1(7).Text
      Else
        Precio_Neto = ""
      End If
      PVP = Precio_Neto
      'txtText1(6) = Format(PVP, "0.000")
    Else
      If UCase(Left(Me.txtText1(36), 1)) = "Q" Then
      'PVP
        If txtText1(7).Text <> "" Then 'Precio_Neto
          Precio_Neto = txtText1(7).Text
        Else
          Precio_Neto = ""
        End If
        If Precio_Neto <= 2500 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=22"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        ElseIf Precio_Neto <= 5000 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=23"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        ElseIf Precio_Neto <= 10000 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=24"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        ElseIf Precio_Neto <= 25000 Then
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=25"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        Else '>25000
          stra = "SELECT FRH2PARAMGEN FROM FRH200 WHERE FRH2CODPARAMGEN=26"
          Set rsta = objApp.rdoConnect.OpenResultset(stra)
          If Not rsta.EOF Then
            Param_Gen = CCur(objGen.ReplaceStr(rsta(0).Value, ".", ",", 1))
            Param_Gen = Precio_Neto * Param_Gen / 100
          Else
            Param_Gen = 0
          End If
          rsta.Close
          Set rsta = Nothing
        End If
        PVP = Precio_Neto + Param_Gen
        'txtText1(6) = Format(PVP, "0.000")
      End If
    End If
  End Select

  If txtText1(0).Text = "" Then
    cmdsinonimos.Enabled = False
    cmdsustitutos.Enabled = False
    cmdsustalergia.Enabled = False
    cmdprocfab.Enabled = False
  Else
    cmdsinonimos.Enabled = True
    cmdsustitutos.Enabled = True
    cmdsustalergia.Enabled = True
    cmdprocfab.Enabled = True
  End If

  If intIndex = 13 Or intIndex = 21 Then
    ' CALCULAR CONCENTRACI�N
    If IsNumeric(txtText1(13).Text) _
     And IsNumeric(txtText1(21).Text) _
     And txtText1(21).Text <> "0" Then
      txtText1(24) = txtText1(13) / txtText1(21)
    Else
      txtText1(24) = ""
    End If
  End If
  

On Error GoTo ERR
  If intIndex = 17 Then
      Picture1.Picture = LoadPicture(txtText1(17).Text)
  End If

ERR:
    Picture1.Cls
Exit Sub

'   Select Case intIndex
'   Case 36, 2, 44, 49, 47, 51, 56
'      If UCase(Left(Me.txtText1(36), 1)) = "Q" Then
'         ' Es material
'         If IsNumeric(Me.txtText1(2)) Then
'            cPVL = Me.txtText1(2)
'         Else
'            cPVL = 0
'         End If
'         If IsNumeric(Me.txtText1(44)) Then
'            ' Descuento
'            cPrecio_Oferta = cPVL - cPVL * Me.txtText1(44) / 100
'         Else
'            cPrecio_Oferta = cPVL
'         End If
'      Else
'         ' Es Medicamento
'         If IsNumeric(Me.txtText1(49)) Then
'            cPrecio_Oferta = Me.txtText1(49)
'         Else
'            cPrecio_Oferta = 0
'         End If
'      End If
'      If IsNumeric(Me.txtText1(47)) Then
'         ' Rappel
'         cPrecio_Con_Rappel = cPrecio_Oferta - cPrecio_Oferta * Me.txtText1(47) / 100
'      Else
'         cPrecio_Con_Rappel = cPrecio_Oferta
'      End If
'      If IsNumeric(Me.txtText1(51)) Then
'         ' Bonificaci�n
'         cPrecio_Con_Bonificaci�n = cPrecio_Con_Rappel - cPrecio_Con_Rappel * Me.txtText1(51) / 100
'      Else
'         cPrecio_Con_Bonificaci�n = cPrecio_Con_Rappel
'      End If
'
'      If Trim(Me.txtText1(56)) <> "" And IsNumeric(Me.txtText1(56)) Then
'         ' IVA
'         strIVA = "SELECT FR88IVA FROM FR8800 WHERE FR88CODTIPIVA = " & Me.txtText1(56)
'         Set rstIVA = objApp.rdoConnect.OpenResultset(strIVA)
'         If Not rstIVA.EOF Then
'            cPrecio_Con_IVA = cPrecio_Con_Bonificaci�n + cPrecio_Con_Bonificaci�n * rstIVA(0).Value / 100
'         Else
'            cPrecio_Con_IVA = cPrecio_Con_Bonificaci�n
'         End If
'         rstIVA.Close
'         Set rstIVA = Nothing
'      Else
'         cPrecio_Con_IVA = cPrecio_Con_Bonificaci�n
'      End If
'
'      Me.txtText1(7) = Format(cPrecio_Con_IVA, "0.00")
'   End Select

End Sub


Private Sub Meter_Codigo_Seguridad(ByVal CodInt)
   Dim SUMA As Integer
   
   Dim SEIS As Integer
   Dim CINCO As Integer
   Dim CUATRO As Integer
   Dim TRES As Integer
   Dim DOS As Integer
   Dim UNO As Integer
   
   If Len(txtText1(22).Text) <> 0 Then
      If Len(txtText1(22).Text) <> 6 Then
         'el c�digo interno debe tener 6 d�gitos
         MsgBox "El C�digo Interno debe tener 6 d�gitos."
         Call objWinInfo.CtrlSet(txtText1(22), "")
         txtText1(22).SetFocus
      Else
         UNO = Left(Right(CodInt, 1), 1)
         DOS = Left(Right(CodInt, 2), 1)
         TRES = Left(Right(CodInt, 3), 1)
         CUATRO = Left(Right(CodInt, 4), 1)
         CINCO = Left(Right(CodInt, 5), 1)
         SEIS = Left(Right(CodInt, 6), 1)
         
         SUMA = SEIS * 7 + CINCO * 6 + CUATRO * 5 + TRES * 4 + DOS * 3 + UNO * 2
         SUMA = 11 - (SUMA Mod 11)
      
         txtText1(30).Text = SUMA
         'Call objWinInfo.CtrlSet(txtText1(30), SUMA)
      End If
   End If

End Sub


Private Sub crear_grafico()
Dim rowLabelCount As Integer
Dim columnLabelCount As Integer
Dim rowCount As Integer
Dim columnCount As Integer
Dim datagrid As datagrid
Dim labelindex As Integer
Dim column As Integer
Dim row As Integer
Dim i As Integer
Dim strmeses As String
Dim rstmeses As rdoResultset
Dim intmeses As Integer
Dim strconsumo As String
Dim rstconsumo As rdoResultset
Dim strfecha As String
Dim rstfecha As rdoResultset
Dim intmes As Integer
Dim rdoQ As rdoQuery

  If txtText1(0).Text = "" Then
    intmeses = 0
  Else
    'bind variables
    'strmeses = "SELECT COUNT(*) FROM"
    'strmeses = strmeses & " (SELECT DISTINCT TO_CHAR(FR80FECMOVIMIENTO,'MON') FROM FR8000"
    'strmeses = strmeses & " WHERE FR80FECMOVIMIENTO >= (SYSDATE - 365 + 31 + TO_CHAR(SYSDATE,'DD'))"
    'strmeses = strmeses & " AND FR73CODPRODUCTO=" & txtText1(0).Text & ")"
    strmeses = "SELECT COUNT(*) FROM"
    strmeses = strmeses & " (SELECT DISTINCT TO_CHAR(FR80FECMOVIMIENTO,'MON') FROM FR8000"
    strmeses = strmeses & " WHERE FR80FECMOVIMIENTO >= (SYSDATE - 365 + 31 + TO_CHAR(SYSDATE,'DD'))"
    strmeses = strmeses & " AND FR73CODPRODUCTO=?)"
    
    Set rdoQ = objApp.rdoConnect.CreateQuery("", strmeses)
    rdoQ(0) = txtText1(0).Text
    Set rstmeses = rdoQ.OpenResultset(strmeses)
    intmeses = rstmeses(0).Value
    rstmeses.Close
    Set rstmeses = Nothing
  End If
  
  strfecha = "SELECT TO_CHAR(SYSDATE,'MM') FROM DUAL"
  Set rstfecha = objApp.rdoConnect.OpenResultset(strfecha)
  intmes = rstfecha(0).Value
  rstfecha.Close
  Set rstfecha = Nothing
    
  Set datagrid = MSChart1.datagrid
  'MSChart1.ShowLegend = True
  ' Establece los par�metros del gr�fico con
  ' los m�todos.
  
  MSChart1.chartType = VtChChartType2dLine
  rowLabelCount = 12
  columnLabelCount = 1
  rowCount = 12
  columnCount = 1
  MSChart1.datagrid.SetSize rowLabelCount, columnLabelCount, rowCount, columnCount
      
  labelindex = 1
  row = 12
      
  If intmeses = 0 Then
    For i = 12 To 1 Step -1
      MSChart1.datagrid.RowLabel(row, labelindex) = CADENA_MES(intmes) & "-0"
      If intmes - 1 > 0 Then
        intmes = intmes - 1
      Else
        intmes = 12
      End If
      Call MSChart1.datagrid.SetData(row, 1, 0, 0)
      row = row - 1
    Next i
  Else
    For i = intmeses To 1 Step -1
      'bind variables
      'strconsumo = "SELECT SUM(FR80UNISALEN),TO_CHAR(FR80FECMOVIMIENTO,'MM') FROM FR8000"
      'strconsumo = strconsumo & " WHERE FR80FECMOVIMIENTO >=(SYSDATE - 365 + 31 + TO_CHAR(SYSDATE,'DD'))"
      'strconsumo = strconsumo & " AND FR73CODPRODUCTO=" & txtText1(0).Text
      'strconsumo = strconsumo & " AND TO_CHAR(FR80FECMOVIMIENTO,'MM')=" & intmes
      'strconsumo = strconsumo & " GROUP BY TO_CHAR(FR80FECMOVIMIENTO,'MM'),FR73CODPRODUCTO"
      strconsumo = "SELECT SUM(FR80UNISALEN),TO_CHAR(FR80FECMOVIMIENTO,'MM') FROM FR8000"
      strconsumo = strconsumo & " WHERE FR80FECMOVIMIENTO >=(SYSDATE - 365 + 31 + TO_CHAR(SYSDATE,'DD'))"
      strconsumo = strconsumo & " AND FR73CODPRODUCTO=" & txtText1(0).Text
      strconsumo = strconsumo & " AND TO_CHAR(FR80FECMOVIMIENTO,'MM')=" & intmes
      strconsumo = strconsumo & " GROUP BY TO_CHAR(FR80FECMOVIMIENTO,'MM'),FR73CODPRODUCTO"
      Set rdoQ = objApp.rdoConnect.CreateQuery("", strconsumo)
      'rdoQ(0) = txtText1(0).Text
      'rdoQ(1) = intmes
      Set rstconsumo = rdoQ.OpenResultset(strconsumo)
      
      If Not rstconsumo.EOF Then
        If Not IsNull(rstconsumo(0).Value) Then
          Call MSChart1.datagrid.SetData(row, 1, rstconsumo(0).Value, 0)
          MSChart1.datagrid.RowLabel(row, labelindex) = CADENA_MES(intmes) & "-" & rstconsumo(0).Value
        Else
          Call MSChart1.datagrid.SetData(row, 1, 0, 0)
          MSChart1.datagrid.RowLabel(row, labelindex) = CADENA_MES(intmes) & "-0"
        End If
      Else
        Call MSChart1.datagrid.SetData(row, 1, 0, 0)
        MSChart1.datagrid.RowLabel(row, labelindex) = CADENA_MES(intmes) & "-0"
      End If
      
      If intmes - 1 > 0 Then
        intmes = intmes - 1
      Else
        intmes = 12
      End If
      
      row = row - 1
    
      rstconsumo.Close
      Set rstconsumo = Nothing

    Next i
        
    For i = 1 To 12 - intmeses
      strconsumo = "SELECT SUM(FR80UNISALEN),TO_CHAR(FR80FECMOVIMIENTO,'MM') FROM FR8000"
      strconsumo = strconsumo & " WHERE FR80FECMOVIMIENTO >= (SYSDATE - 365 + 31 + TO_CHAR(SYSDATE,'DD')) "
      strconsumo = strconsumo & " AND FR73CODPRODUCTO=" & txtText1(0).Text
      strconsumo = strconsumo & " AND TO_CHAR(FR80FECMOVIMIENTO,'MM')=" & intmes
      strconsumo = strconsumo & " GROUP BY TO_CHAR(FR80FECMOVIMIENTO,'MM'),FR73CODPRODUCTO"
      Set rstconsumo = objApp.rdoConnect.OpenResultset(strconsumo)
      
      
      If Not rstconsumo.EOF Then
        Call MSChart1.datagrid.SetData(row, 1, rstconsumo(0).Value, 0)
        MSChart1.datagrid.RowLabel(row, labelindex) = CADENA_MES(intmes) & "-" & rstconsumo(0).Value
      Else
        Call MSChart1.datagrid.SetData(row, 1, 0, 0)
        MSChart1.datagrid.RowLabel(row, labelindex) = CADENA_MES(intmes) & "-0"
      End If
      
      If intmes - 1 > 0 Then
        intmes = intmes - 1
      Else
        intmes = 12
      End If
      
      row = row - 1
    
      rstconsumo.Close
      Set rstconsumo = Nothing
    Next i

  End If


End Sub

Private Function CADENA_MES(ByVal intmes As Integer) As String

  Select Case intmes
  Case 1
    CADENA_MES = "Enero"
  Case 2
    CADENA_MES = "Febrero"
  Case 3
    CADENA_MES = "Marzo"
  Case 4
    CADENA_MES = "Abril"
  Case 5
    CADENA_MES = "Mayo"
  Case 6
    CADENA_MES = "Junio"
  Case 7
    CADENA_MES = "Julio"
  Case 8
    CADENA_MES = "Agosto"
  Case 9
    CADENA_MES = "Septiembre"
  Case 10
    CADENA_MES = "Octubre"
  Case 11
    CADENA_MES = "Noviembre"
  Case 12
    CADENA_MES = "Diciembre"
  End Select

End Function

Private Function Comprobar_Codigo_Seguridad() As Integer
   If Len(txtText1(22).Text) <> 6 Then
      'el c�digo interno debe tener 6 d�gitos
      MsgBox "El C�digo Interno debe tener 6 d�gitos."
      Call objWinInfo.CtrlSet(txtText1(22), "")
      txtText1(22).SetFocus
      Comprobar_Codigo_Seguridad = -1
      Exit Function
   Else
      Comprobar_Codigo_Seguridad = 0
   End If
End Function
