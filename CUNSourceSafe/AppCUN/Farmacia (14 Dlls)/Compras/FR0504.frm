VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmVerPetOfe 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Ver Peticiones de Ofertas"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   ClipControls    =   0   'False
   HelpContextID   =   30001
   Icon            =   "FR0504.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   31
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Introducir Oferta"
      Height          =   375
      Left            =   5160
      TabIndex        =   39
      Top             =   7680
      Width           =   1695
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3945
      Index           =   1
      Left            =   120
      TabIndex        =   22
      Top             =   3600
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   3420
         Index           =   1
         Left            =   120
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   360
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   6033
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0504.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(2)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(9)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(3)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(0)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(10)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "lblLabel1(11)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "lblLabel1(12)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "lblLabel1(13)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "lblLabel1(14)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "lblLabel1(4)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(4)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(2)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).Control(12)=   "txtText1(9)"
         Tab(0).Control(12).Enabled=   0   'False
         Tab(0).Control(13)=   "txtText1(10)"
         Tab(0).Control(13).Enabled=   0   'False
         Tab(0).Control(14)=   "txtText1(11)"
         Tab(0).Control(14).Enabled=   0   'False
         Tab(0).Control(15)=   "txtText1(12)"
         Tab(0).Control(15).Enabled=   0   'False
         Tab(0).Control(16)=   "txtText1(13)"
         Tab(0).Control(16).Enabled=   0   'False
         Tab(0).Control(17)=   "txtText1(14)"
         Tab(0).Control(17).Enabled=   0   'False
         Tab(0).Control(18)=   "txtText1(0)"
         Tab(0).Control(18).Enabled=   0   'False
         Tab(0).Control(19)=   "txtText1(15)"
         Tab(0).Control(19).Enabled=   0   'False
         Tab(0).Control(20)=   "txtText1(16)"
         Tab(0).Control(20).Enabled=   0   'False
         Tab(0).ControlCount=   21
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0504.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(1)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR29CODUNIMEDIDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   16
            Left            =   1320
            TabIndex        =   15
            Tag             =   "U.M.C. Unidad de Medida de la Cantidad"
            Top             =   1080
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000A&
            DataField       =   "FR73CODPRODUCTO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   15
            Left            =   120
            TabIndex        =   8
            Tag             =   "C�digo Producto"
            Top             =   480
            Width           =   1095
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000A&
            DataField       =   "FR29OBSERV"
            Height          =   735
            Index           =   0
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   18
            Tag             =   "Observaciones"
            Top             =   1680
            Width           =   10920
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73REFERENCIA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   14
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   17
            TabStop         =   0   'False
            Tag             =   "Referencia"
            Top             =   1080
            Width           =   3465
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DESPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   13
            Left            =   1320
            Locked          =   -1  'True
            TabIndex        =   9
            TabStop         =   0   'False
            Tag             =   "Descripci�n Producto"
            Top             =   480
            Width           =   6180
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73VOLUMEN"
            Height          =   330
            HelpContextID   =   30104
            Index           =   12
            Left            =   9360
            Locked          =   -1  'True
            TabIndex        =   13
            TabStop         =   0   'False
            Tag             =   "Volumen mL"
            Top             =   480
            Width           =   1380
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73CODUNIMEDIDA"
            Height          =   330
            HelpContextID   =   30104
            Index           =   11
            Left            =   8760
            Locked          =   -1  'True
            TabIndex        =   12
            TabStop         =   0   'False
            Tag             =   "U.M. Unidad de Medida"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR73DOSIS"
            Height          =   330
            HelpContextID   =   30104
            Index           =   10
            Left            =   8160
            Locked          =   -1  'True
            TabIndex        =   11
            TabStop         =   0   'False
            Tag             =   "Dosis"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            DataField       =   "FRH7CODFORMFAR"
            Height          =   330
            HelpContextID   =   30104
            Index           =   9
            Left            =   7560
            Locked          =   -1  'True
            TabIndex        =   10
            TabStop         =   0   'False
            Tag             =   "F.F. Forma Farmac�utica"
            Top             =   480
            Width           =   540
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR29CANTPRODUCTO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   2
            Left            =   120
            TabIndex        =   14
            Tag             =   "Cantidad"
            Top             =   1080
            Width           =   1050
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00C0C0C0&
            DataField       =   "FR29PRECIO"
            Height          =   330
            HelpContextID   =   30104
            Index           =   4
            Left            =   2520
            TabIndex        =   16
            Tag             =   "Precio Unidad"
            Top             =   1080
            Width           =   1305
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   3120
            Index           =   1
            Left            =   -74910
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   90
            Width           =   11055
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            BevelColorFrame =   0
            BevelColorHighlight=   16777215
            AllowUpdate     =   0   'False
            MultiLine       =   0   'False
            AllowRowSizing  =   0   'False
            AllowGroupSizing=   0   'False
            AllowGroupMoving=   0   'False
            AllowColumnMoving=   2
            AllowGroupSwapping=   0   'False
            AllowGroupShrinking=   0   'False
            AllowDragDrop   =   0   'False
            SelectTypeCol   =   0
            SelectTypeRow   =   1
            MaxSelectedRows =   0
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   -1  'True
            _ExtentX        =   19500
            _ExtentY        =   5503
            _StockProps     =   79
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M.C."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   4
            Left            =   1320
            TabIndex        =   41
            Top             =   840
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Referencia"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   14
            Left            =   7560
            TabIndex        =   38
            Top             =   840
            Width           =   945
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Volumen (mL)"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   13
            Left            =   9360
            TabIndex        =   37
            Top             =   240
            Width           =   1155
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "U.M."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   12
            Left            =   8760
            TabIndex        =   36
            Top             =   240
            Width           =   420
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Dosis"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   8160
            TabIndex        =   35
            Top             =   240
            Width           =   480
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "F.F."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   10
            Left            =   7560
            TabIndex        =   34
            Top             =   240
            Width           =   345
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Observaciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   30
            Top             =   1440
            Width           =   1275
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Cantidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   29
            Top             =   840
            Width           =   765
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Producto"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   28
            Top             =   240
            Width           =   780
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Precio Unidad"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   2
            Left            =   2520
            TabIndex        =   27
            Top             =   840
            Width           =   1215
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Peticiones de Oferta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3045
      Index           =   0
      Left            =   90
      TabIndex        =   21
      Top             =   450
      Width           =   11745
      Begin TabDlg.SSTab tabTab1 
         Height          =   2415
         HelpContextID   =   90001
         Index           =   0
         Left            =   135
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   360
         Width           =   11490
         _ExtentX        =   20267
         _ExtentY        =   4260
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "FR0504.frx":0044
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(6)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(5)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(1)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(7)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "lblLabel1(8)"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "dtcDateCombo1(0)"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "txtText1(6)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(5)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(8)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(3)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).Control(11)=   "txtText1(7)"
         Tab(0).Control(11).Enabled=   0   'False
         Tab(0).ControlCount=   12
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "FR0504.frx":0060
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000A&
            Height          =   375
            Index           =   7
            Left            =   120
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            Tag             =   "Representante"
            Top             =   960
            Width           =   10920
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000A&
            DataField       =   "FR67NUMREP"
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   1560
            TabIndex        =   4
            Tag             =   "C�digo Representante"
            Top             =   720
            Visible         =   0   'False
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000A&
            DataField       =   "FR67DESOFERTA"
            Height          =   690
            HelpContextID   =   40102
            Index           =   8
            Left            =   120
            TabIndex        =   6
            Tag             =   "Descripci�n de la Petici�n"
            Top             =   1560
            Width           =   10935
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000A&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   3360
            TabIndex        =   2
            Tag             =   "C�digo Proveedor"
            Top             =   360
            Width           =   1335
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H8000000A&
            DataField       =   "FR79PROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   5
            Left            =   4800
            TabIndex        =   3
            Tag             =   "Descripci�n Proveedor"
            Top             =   360
            Width           =   6255
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H8000000A&
            DataField       =   "FR67CODPETOFERT"
            Height          =   330
            HelpContextID   =   40101
            Index           =   6
            Left            =   120
            TabIndex        =   0
            Tag             =   "C�digo Oferta"
            Top             =   360
            Width           =   1150
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FR67FECPETOFERT"
            Height          =   330
            Index           =   0
            Left            =   1440
            TabIndex        =   1
            Tag             =   "Fecha Pedido"
            Top             =   360
            Width           =   1860
            _Version        =   65537
            _ExtentX        =   3281
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2130
            Index           =   0
            Left            =   -74880
            TabIndex        =   40
            Top             =   120
            Width           =   10725
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            SelectTypeRow   =   1
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   18918
            _ExtentY        =   3757
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Representante"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   33
            Top             =   720
            Width           =   1260
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   7
            Left            =   1440
            TabIndex        =   32
            Top             =   120
            Width           =   1290
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   1
            Left            =   3360
            TabIndex        =   26
            Top             =   120
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Descripci�n de la Petici�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   25
            Top             =   1320
            Width           =   2250
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Oferta"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   24
            Top             =   120
            Width           =   990
         End
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   23
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmVerPetOfe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: FrmVerPetOfe (FR0504.FRM)                                    *
'* AUTOR: Aitor Vi�uela Garc�a                                          *
'* FECHA: Mayo DE 1999                                                  *
'* DESCRIPCION: Introducir Ofertas Proveedores                          *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit
Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los botones
' -----------------------------------------------
Private Sub Command1_Click()
   Dim intResp As Integer
   Dim strMsg As String
   Dim strupdate As String
   
   
   Command1.Enabled = False
   Me.Enabled = False
   If txtText1(6).Text <> "" Then
      strMsg = " La oferta que va a introducir " & Chr(13) & Chr(13)
      strMsg = strMsg & "�Hace referencia a la petici�n de oferta que acaba de ver?"
      intResp = MsgBox(strMsg, vbYesNoCancel, "Pregunta")
   Else
      intResp = 7
   End If
   Select Case intResp
   Case 2   'Cancelar
   Case 6   'Hace referencia la petici�n de oferta vista
      gintCodProveedor = txtText1(1).Text
      gintFR67CODPETOFERT = txtText1(6).Text
      ' Ponerla como FR99CODESTOFERTPROV = 1 "Recibida"
      If txtText1(6).Text <> "" Then
         strupdate = "Update FR6700 SET FR99CODESTOFERTPROV = 1 WHERE FR67CODPETOFERT = " & txtText1(6).Text
         MsgBox "Oferta Anotada como Recibida", vbExclamation, "Aviso"
         objApp.rdoConnect.Execute strupdate, 64
      End If
'      Call FrmIntOfe.Show(vbModal)
      gstrLlamador = "FrmVerPetOfe"
      Call objsecurity.LaunchProcess("FR0505")
   Case 7   'No hace referencia la petici�n de oferta vista
      gintCodProveedor = ""
      gintFR67CODPETOFERT = ""
'      Call FrmIntOfe.Show(vbModal)
      gstrLlamador = "FrmVerPetOfe"
      Call objsecurity.LaunchProcess("FR0505")
   End Select
   Me.Enabled = True
   Command1.Enabled = True

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------
Private Sub Form_Load()
   Dim objMasterInfo As New clsCWForm
   Dim objDetailInfo As New clsCWForm
   Dim strKey As String
   Dim StrSelect As String
  

   Set objWinInfo = New clsCWWin
  
  
   Call objWinInfo.WinCreateInfo(cwModeSingleEmpty, _
                                 Me, tlbToolbar1, stbStatusBar1, _
                                 cwWithAll)
   With objMasterInfo
      Set .objFormContainer = fraFrame1(0)
      Set .objFatherContainer = Nothing
      Set .tabMainTab = tabTab1(0)
      Set .grdGrid = grdDBGrid1(0)
    
      .strName = "Petici�n de Oferta"
      
      .strTable = "FR6701J"
      .strWhere = "FR99CODESTOFERTPROV = 0"
      .intAllowance = cwAllowReadOnly
      Call .FormAddOrderField("FR67CODPETOFERT", cwAscending)
   
      strKey = .strDataBase & .strTable
      '.blnAskPrimary = False
    
      Call .FormCreateFilterWhere(strKey, "Petici�n de Oferta")
      Call .FormAddFilterWhere(strKey, "FR67CODPETOFERT", "C�digo Petici�n Oferta", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR67DESOFERTA", "Descripci�n Petici�n Oferta", cwString)
      Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR79PROVEEDOR", "Nombre Proveedor", cwString)
      Call .FormAddFilterWhere(strKey, "FR67NUMREP", "Representante 1,2 � 3", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR67FECPETOFERT", "Fecha Petici�n Oferta", cwDate)
      
      Call .FormAddFilterOrder(strKey, "FR67CODPETOFERT", "C�digo Petici�n Oferta")
      Call .FormAddFilterOrder(strKey, "FR67DESOFERTA", "Descripci�n Petici�n Oferta")
      Call .FormAddFilterOrder(strKey, "FR67FECPETOFERT", "Fecha Petici�n Oferta")
      Call .FormAddFilterOrder(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor")
      Call .FormAddFilterOrder(strKey, "FR79PROVEEDOR", "Nombre Proveedor")
      Call .FormAddFilterOrder(strKey, "FR67NUMREP", "Representante 1,2 � 3")
    
   End With
  
   With objDetailInfo
      .strName = "Detalle Petici�n de Oferta"
      Set .objFormContainer = fraFrame1(1)
      Set .objFatherContainer = fraFrame1(0)
      Set .tabMainTab = tabTab1(1)
      Set .grdGrid = grdDBGrid1(1)
      .strTable = "FR2901J"
      .intAllowance = cwAllowReadOnly

      '.blnAskPrimary = False

      Call .FormAddOrderField("FR67CODPETOFERT", cwAscending)
      Call .FormAddOrderField("FR29CODDETPETOFERT", cwAscending)

      Call .FormAddRelation("FR67CODPETOFERT", txtText1(6))

      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Detalle Petici�n de Oferta")
      Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Producto", cwString)
      Call .FormAddFilterWhere(strKey, "FRH7CODFORMFAR", "Forma Farmac�utica", cwString)
      Call .FormAddFilterWhere(strKey, "FR73DOSIS", "Dosis", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FR73CODUNIMEDIDA", "Unidad de Medida del Producto", cwString)
      Call .FormAddFilterWhere(strKey, "FR73VOLUMEN", "Volumen mL", cwDecimal)
      Call .FormAddFilterWhere(strKey, "FR73REFERENCIA", "Referencia", cwString)
      Call .FormAddFilterWhere(strKey, "FR29CANTPRODUCTO", "Cantidad Producto", cwDecimal)
      Call .FormAddFilterWhere(strKey, "FR29PRECIO", "Precio Producto", cwDecimal)
      Call .FormAddFilterWhere(strKey, "FR29OBSERV", "Observaciones", cwString)

      Call .FormAddFilterOrder(strKey, "FR29CODDETPETOFERT", "C�digo Detalle Petici�n Oferta")
      Call .FormAddFilterOrder(strKey, "FR73DESPRODUCTOPROV", "Descripci�n Producto")
      Call .FormAddFilterOrder(strKey, "FRH7CODFORMFAR", "Forma Farmac�utica")
      Call .FormAddFilterOrder(strKey, "FR73DOSIS", "Dosis")
      Call .FormAddFilterOrder(strKey, "FR73CODUNIMEDIDA", "Unidad de Medida del Producto")
      Call .FormAddFilterOrder(strKey, "FR73VOLUMEN", "Volumen mL")
      Call .FormAddFilterOrder(strKey, "FR73REFERENCIA", "Referencia")
      Call .FormAddFilterOrder(strKey, "FR29CANTPRODUCTO", "Cantidad Producto")
      Call .FormAddFilterOrder(strKey, "FR29PRECIO", "Precio Producto")
      Call .FormAddFilterOrder(strKey, "FR29OBSERV", "Observaciones")
   End With

   With objWinInfo
   
      Call .FormAddInfo(objMasterInfo, cwFormDetail)
      Call .FormAddInfo(objDetailInfo, cwFormDetail)
    
      Call .FormCreateInfo(objMasterInfo)
      
      .CtrlGetInfo(txtText1(1)).blnForeign = True
      
      StrSelect = "(SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'1' FR67NUMREP,FR79PROVEEDOR PROVEEDOR FROM FR7900 UNION"
      StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'2' FR67NUMREP,FR79REPRESENT2 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT2 IS NOT NULL UNION"
      StrSelect = StrSelect & " SELECT FR79CODPROVEEDOR,FR79PROVEEDOR,'3' FR67NUMREP,FR79REPRESENT3 PROVEEDOR FROM FR7900 WHERE FR79REPRESENT3 IS NOT NULL)"
      
      ' C�digo oferta
      .CtrlGetInfo(txtText1(6)).blnInFind = True
      ' C�digo Proveedor
      .CtrlGetInfo(txtText1(1)).blnInFind = True
      ' Nombre Proveedor
      .CtrlGetInfo(txtText1(5)).blnInFind = True
      ' Representante
      '.CtrlGetInfo(txtText1(7)).blnInFind = True -> no funciona, porque es un linked
      ' Descripci�n de la Petici�n
      .CtrlGetInfo(txtText1(8)).blnInFind = True
      ' Fecha Petici�n
      .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
      ' C�digo de Producto
      .CtrlGetInfo(txtText1(15)).blnInFind = True
      .CtrlGetInfo(txtText1(15)).blnForeign = True
      ' Descripci�n del Producto
      .CtrlGetInfo(txtText1(13)).blnInFind = True
      ' F.F. del Producto
      .CtrlGetInfo(txtText1(9)).blnInFind = True
      ' Dosis del Producto
      .CtrlGetInfo(txtText1(10)).blnInFind = True
      ' U.M. del Producto
      .CtrlGetInfo(txtText1(11)).blnInFind = True
      ' Volumen del Producto
      .CtrlGetInfo(txtText1(12)).blnInFind = True
      ' Referencia del Producto
      .CtrlGetInfo(txtText1(14)).blnInFind = True
      ' Cantidad
      .CtrlGetInfo(txtText1(2)).blnInFind = True
      ' Precio Unidad
      .CtrlGetInfo(txtText1(4)).blnInFind = True
      ' Observaciones
      .CtrlGetInfo(txtText1(0)).blnInFind = True

      Call .WinRegister
      Call .WinStabilize
   End With
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
   intKeyAscii = objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, _
                         intShift As Integer)
   intKeyCode = objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
   Call objWinInfo.WinDeRegister
   Call objWinInfo.WinRemoveInfo
End Sub

Private Sub objWinInfo_cwPostRead(ByVal strFormName As String)

   Dim strSQL As String
   Dim rstSQL As rdoResultset

   If txtText1(1).Text <> "" Then
      Select Case txtText1(3).Text
      Case 1
         strSQL = "SELECT FR79REPRESENT1 FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
         Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
         Call objWinInfo.CtrlSet(txtText1(7), rstSQL(0).Value)
         rstSQL.Close
         Set rstSQL = Nothing
      Case 2
         strSQL = "SELECT FR79REPRESENT2 FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
         Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
         Call objWinInfo.CtrlSet(txtText1(7), rstSQL(0).Value)
         rstSQL.Close
         Set rstSQL = Nothing
      Case 3
         strSQL = "SELECT FR79REPRESENT3 FROM FR7900 WHERE FR79CODPROVEEDOR=" & txtText1(1).Text
         Set rstSQL = objApp.rdoConnect.OpenResultset(strSQL)
         Call objWinInfo.CtrlSet(txtText1(7), rstSQL(0).Value)
         rstSQL.Close
         Set rstSQL = Nothing
      End Select
   Else
      Call objWinInfo.CtrlSet(txtText1(7), "")
   End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de CodeWizard
' -----------------------------------------------
Private Sub objWinInfo_cwPrint(ByVal strFormName As String)
'  Dim intReport As Integer
'  Dim objPrinter As clsCWPrinter
'  Dim blnHasFilter As Boolean
'
'  If strFormName = "Departamentos Realizadores" Then
'    Call objWinInfo.FormPrinterDialog(True, "")
'    Set objPrinter = objWinInfo.objWinActiveForm.objPrinter
'    intReport = objPrinter.Selected
'    If intReport > 0 Then
'      blnHasFilter = objWinInfo.objWinActiveForm.blnFilterOn
'      Call objPrinter.ShowReport(objWinInfo.DataGetWhere(blnHasFilter), _
'                                 objWinInfo.DataGetOrder(blnHasFilter, True))
'    End If
'    Set objPrinter = Nothing
'  End If
End Sub

Private Sub objWinInfo_cwMaint(ByVal strFormName As String)
   'Call MsgBox("Se ha generado el evento de mantenimiento sobre el formulario " & strFormName)
End Sub

Private Sub objWinInfo_cwForeign(ByVal strFormName As String, ByVal strCtrl As String)
'
End Sub

Private Sub objWinInfo_cwLinked(ByVal strFormName As String, ByVal strCtrlName As String, aValues() As Variant)
'
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
   Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

Private Sub tabTab1_GotFocus(Index As Integer)
   Dim Q%
   
   If Index = 1 Then
      On Error Resume Next
      ' C�digo Producto
      grdDBGrid1(1).Columns(1).Width = 15 * 120
      ' Descripci�n Producto
      grdDBGrid1(1).Columns(2).Width = 20 * 120
      ' FF Producto
      grdDBGrid1(1).Columns(3).Width = 5 * 120
      ' D�sis
      grdDBGrid1(1).Columns(4).Width = 5 * 120
      ' U.M.
      grdDBGrid1(1).Columns(5).Width = 5 * 120
      ' Volumen
      grdDBGrid1(1).Columns(6).Width = 5 * 120
      ' Cantidad
      grdDBGrid1(1).Columns(7).Width = 10 * 120
      ' U.M.C.
      grdDBGrid1(1).Columns(8).Width = 6 * 120
      ' Precio U
      grdDBGrid1(1).Columns(9).Width = 12 * 120
      ' Referencia
      grdDBGrid1(1).Columns(10).Width = 12 * 120
      On Error GoTo 0
   End If

End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 And Shift = 1 Then
    Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
  End If
End Sub
Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
        
   Select Case intIndex
   Case 10
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(2)) 'Nuevo
   Case 20
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(3)) 'Abrir
   Case 40
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(4)) 'Guardar
   Case 60
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(8)) 'Borrar
   Case 80
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(6)) 'Imprimir
   Case 100
      Call tlbtoolbar1_ButtonClick(tlbToolbar1.Buttons(30)) 'Salir
   End Select

End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
   Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
      Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
   Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del tab
' -----------------------------------------------
Private Sub tabTab1_MouseDown(intIndex As Integer, _
                              Button As Integer, _
                              Shift As Integer, _
                              X As Single, _
                              Y As Single)
   Call objWinInfo.FormChangeActive(tabTab1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
   Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del las etiquetas
' -----------------------------------------------
Private Sub lblLabel1_Click(intIndex As Integer)
   Call objWinInfo.FormChangeActive(lblLabel1(intIndex).Container, False, True)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
   Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
End Sub
' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
   Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
   Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
   Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
   Call objWinInfo.CtrlDataChange
End Sub
