Attribute VB_Name = "DEFvar"
Option Explicit

Type LineaAnterior
   linea As Long
   Producto As String
   CantidadPedida As Long
   PrecioUnitario As Single
   Descuento As Single
   IVA As Single
   PrecioNeto As Single
   ImporteLinea As Single
   CantidadEntregada As Long
   bonificacion As Single
   PendienteBonificacion As Integer
   Envase As Integer
   Pendientefacturar As Long
   NumeroPedido As Long
   LineaPedido As Long
   Recargo As Single
   CodigoProducto As Long
   CantidadBonificada As Integer
   GrupoTerapeutico As String
   Referencia As String
End Type

Type LineaActual
   linea As Long
   Producto As String
   CantidadPedida As Long
   PrecioUnitario As Single
   Descuento As Single
   IVA As Single
   PrecioNeto As Single
   ImporteLinea As Single
   CantidadEntregada As Long
   bonificacion As Single
   PendienteBonificacion As Integer
   Envase As Integer
   Pendientefacturar As Long
   NumeroPedido As Long
   LineaPedido As Long
   Recargo As Single
   CodigoProducto As Long
   CantidadBonificada As Integer
   GrupoTerapeutico As String
   Referencia As String
End Type
