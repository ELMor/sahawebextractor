VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.2#0"; "COMCTL32.OCX"
Object = "{E8671A8B-E5DD-11CD-836C-0000C0C14E92}#1.0#0"; "SSCALA32.OCX"
Object = "{BC496AED-9B4E-11CE-A6D5-0000C0BE9395}#2.0#0"; "SSDATB32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmBusAlb 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FARMACIA. Buscar Albaranes"
   ClientHeight    =   8340
   ClientLeft      =   630
   ClientTop       =   2280
   ClientWidth     =   11910
   HelpContextID   =   30001
   Icon            =   "Fr0538.frx":0000
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8340
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar tlbToolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   741
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   327682
      BorderStyle     =   1
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Facturas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1875
      Index           =   3
      Left            =   7200
      TabIndex        =   24
      Top             =   600
      Width           =   4665
      Begin VB.CommandButton cmdSeleccionar 
         Caption         =   "Traer &Facturas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3480
         TabIndex        =   25
         ToolTipText     =   "Selecciona las facturas asociadas a ese albar�n."
         Top             =   720
         Visible         =   0   'False
         Width           =   1095
      End
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1395
         Index           =   3
         Left            =   120
         TabIndex        =   26
         Top             =   360
         Width           =   3255
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   5741
         _ExtentY        =   2461
         _StockProps     =   79
      End
   End
   Begin VB.CommandButton cmdTraer 
      Caption         =   "Traer &Albaranes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5280
      TabIndex        =   23
      ToolTipText     =   "Traer Albaranes para Facturar"
      Top             =   7560
      Visible         =   0   'False
      Width           =   1935
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Pedidos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1725
      Index           =   2
      Left            =   7200
      TabIndex        =   18
      Top             =   2520
      Width           =   4695
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   1300
         Index           =   2
         Left            =   120
         TabIndex        =   19
         Top             =   330
         Width           =   4455
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   7858
         _ExtentY        =   2293
         _StockProps     =   79
      End
      Begin VB.Frame Frame1 
         Height          =   1335
         Left            =   3360
         TabIndex        =   20
         Top             =   240
         Visible         =   0   'False
         Width           =   1215
         Begin VB.OptionButton optparcial 
            Caption         =   "Recibido Parcial"
            ForeColor       =   &H00C00000&
            Height          =   435
            Left            =   120
            TabIndex        =   22
            Top             =   240
            Width           =   975
         End
         Begin VB.OptionButton optcompleto 
            Caption         =   "Recibido Completo"
            ForeColor       =   &H00C00000&
            Height          =   495
            Left            =   120
            TabIndex        =   21
            Top             =   720
            Width           =   975
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Albar�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3645
      Index           =   0
      Left            =   30
      TabIndex        =   9
      Top             =   600
      Width           =   7065
      Begin TabDlg.SSTab tabTab1 
         Height          =   3135
         HelpContextID   =   90001
         Index           =   0
         Left            =   120
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   360
         Width           =   6810
         _ExtentX        =   12012
         _ExtentY        =   5530
         _Version        =   327681
         TabOrientation  =   3
         Style           =   1
         Tabs            =   2
         TabsPerRow      =   2
         TabHeight       =   529
         WordWrap        =   0   'False
         ShowFocusRect   =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Detalle"
         TabPicture(0)   =   "Fr0538.frx":000C
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "lblLabel1(20)"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).Control(1)=   "lblLabel1(11)"
         Tab(0).Control(1).Enabled=   0   'False
         Tab(0).Control(2)=   "lblLabel1(5)"
         Tab(0).Control(2).Enabled=   0   'False
         Tab(0).Control(3)=   "lblLabel1(6)"
         Tab(0).Control(3).Enabled=   0   'False
         Tab(0).Control(4)=   "Label1"
         Tab(0).Control(4).Enabled=   0   'False
         Tab(0).Control(5)=   "Label2"
         Tab(0).Control(5).Enabled=   0   'False
         Tab(0).Control(6)=   "dtcDateCombo1(0)"
         Tab(0).Control(6).Enabled=   0   'False
         Tab(0).Control(7)=   "txtText1(2)"
         Tab(0).Control(7).Enabled=   0   'False
         Tab(0).Control(8)=   "txtText1(1)"
         Tab(0).Control(8).Enabled=   0   'False
         Tab(0).Control(9)=   "txtText1(3)"
         Tab(0).Control(9).Enabled=   0   'False
         Tab(0).Control(10)=   "txtText1(0)"
         Tab(0).Control(10).Enabled=   0   'False
         Tab(0).ControlCount=   11
         TabCaption(1)   =   "Tabla"
         TabPicture(1)   =   "Fr0538.frx":0028
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "grdDBGrid1(0)"
         Tab(1).ControlCount=   1
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ1CODALBARAN"
            Height          =   330
            HelpContextID   =   40101
            Index           =   0
            Left            =   240
            TabIndex        =   0
            Tag             =   "C�digo Albar�n"
            Top             =   480
            Width           =   1080
         End
         Begin VB.TextBox txtText1 
            BackColor       =   &H00C0C0C0&
            Height          =   330
            HelpContextID   =   40102
            Index           =   3
            Left            =   840
            TabIndex        =   4
            TabStop         =   0   'False
            Tag             =   "Proveedor"
            Top             =   1920
            Width           =   4695
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FRJ1ESTADO"
            Height          =   330
            HelpContextID   =   40102
            Index           =   1
            Left            =   1680
            TabIndex        =   1
            Tag             =   "Estado"
            Top             =   480
            Width           =   400
         End
         Begin VB.TextBox txtText1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            DataField       =   "FR79CODPROVEEDOR"
            Height          =   330
            HelpContextID   =   40102
            Index           =   2
            Left            =   240
            TabIndex        =   3
            Tag             =   "C�digo Proveedor"
            Top             =   1920
            Width           =   600
         End
         Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
            Height          =   2730
            Index           =   0
            Left            =   -74880
            TabIndex        =   11
            Top             =   120
            Width           =   6255
            _Version        =   131078
            DataMode        =   2
            Col.Count       =   0
            AllowUpdate     =   0   'False
            AllowRowSizing  =   0   'False
            ForeColorEven   =   0
            BackColorOdd    =   16777215
            RowHeight       =   423
            SplitterVisible =   -1  'True
            Columns(0).Width=   3200
            Columns(0).DataType=   8
            Columns(0).FieldLen=   4096
            UseDefaults     =   0   'False
            _ExtentX        =   11033
            _ExtentY        =   4815
            _StockProps     =   79
            ForeColor       =   0
         End
         Begin SSCalendarWidgets_A.SSDateCombo dtcDateCombo1 
            DataField       =   "FRJ1FECHAALBAR"
            Height          =   330
            Index           =   0
            Left            =   240
            TabIndex        =   2
            TabStop         =   0   'False
            Tag             =   "Fecha Albar�n"
            Top             =   1185
            Width           =   1980
            _Version        =   65537
            _ExtentX        =   3492
            _ExtentY        =   582
            _StockProps     =   93
            BackColor       =   16777215
            BeginProperty DropDownFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   "DD/MM/YYYY"
            AllowNullDate   =   -1  'True
            ShowCentury     =   -1  'True
            Mask            =   2
            StartofWeek     =   2
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "1  : Facturado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800080&
            Height          =   195
            Left            =   2400
            TabIndex        =   17
            Top             =   600
            Width           =   990
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "0  : No Facturado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800080&
            Height          =   195
            Left            =   2400
            TabIndex        =   16
            Top             =   360
            Width           =   1245
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "C�d. Albar�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   6
            Left            =   240
            TabIndex        =   15
            Top             =   240
            Width           =   1110
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Proveedor"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   5
            Left            =   240
            TabIndex        =   14
            Top             =   1680
            Width           =   885
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Estado"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   11
            Left            =   1680
            TabIndex        =   13
            Top             =   240
            Width           =   600
         End
         Begin VB.Label lblLabel1 
            AutoSize        =   -1  'True
            Caption         =   "Fecha Albar�n"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Index           =   20
            Left            =   240
            TabIndex        =   12
            Top             =   960
            Width           =   1245
         End
      End
   End
   Begin VB.Frame fraFrame1 
      Caption         =   "Productos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3105
      Index           =   1
      Left            =   0
      TabIndex        =   7
      Top             =   4320
      Width           =   11865
      Begin SSDataWidgets_B.SSDBGrid grdDBGrid1 
         Height          =   2625
         Index           =   1
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   11655
         _Version        =   131078
         DataMode        =   2
         Col.Count       =   0
         BevelColorFrame =   0
         BevelColorHighlight=   16777215
         AllowColumnMoving=   2
         SelectTypeCol   =   0
         SelectTypeRow   =   1
         MaxSelectedRows =   0
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         SplitterVisible =   -1  'True
         Columns(0).Width=   3200
         Columns(0).DataType=   8
         Columns(0).FieldLen=   4096
         _ExtentX        =   20558
         _ExtentY        =   4630
         _StockProps     =   79
      End
   End
   Begin ComctlLib.StatusBar stbStatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   8055
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   503
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
      EndProperty
   End
   Begin VB.Menu mnuDatos 
      Caption         =   "&Datos"
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "N&uevo"
         Index           =   10
         Shortcut        =   ^U
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Abrir"
         Index           =   20
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Guardar"
         Index           =   40
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   50
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Eliminar"
         Index           =   60
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Imprimir"
         Index           =   80
         Shortcut        =   ^P
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "-"
         Index           =   90
      End
      Begin VB.Menu mnuDatosOpcion 
         Caption         =   "&Salir"
         Index           =   100
      End
   End
   Begin VB.Menu mnuEdicion 
      Caption         =   "&Edici�n"
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Des&hacer"
         Index           =   10
         Shortcut        =   ^Z
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Cor&tar"
         Index           =   30
         Shortcut        =   ^X
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Copiar"
         Index           =   40
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Pegar"
         Index           =   50
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "Borr&ar"
         Index           =   60
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "-"
         Index           =   70
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Mantener "
         Index           =   80
      End
      Begin VB.Menu mnuEdicionOpcion 
         Caption         =   "&Recuperar �ltimo valor"
         Index           =   90
      End
   End
   Begin VB.Menu mnuFiltro 
      Caption         =   "&Filtro"
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Poner filtro"
         Index           =   10
      End
      Begin VB.Menu mnuFiltroOpcion 
         Caption         =   "&Quitar filtro"
         Index           =   20
      End
   End
   Begin VB.Menu mnuRegistro 
      Caption         =   "&Registro"
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Localizar"
         Index           =   10
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Restaurar"
         Index           =   20
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Primero        CTRL+Inicio"
         Index           =   40
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Anterior        Re Pag"
         Index           =   50
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Siguiente     Av PAg"
         Index           =   60
      End
      Begin VB.Menu mnuRegistroOpcion 
         Caption         =   "&Ultimo          CTRL+Fin"
         Index           =   70
      End
   End
   Begin VB.Menu mnuOpciones 
      Caption         =   "&Opciones"
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Refrescar registros"
         Index           =   10
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Mantenimiento"
         Index           =   20
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "-"
         Index           =   30
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "&Colores"
         Index           =   40
      End
      Begin VB.Menu mnuOpcionesOpcion 
         Caption         =   "Alta &masiva"
         Index           =   50
      End
   End
   Begin VB.Menu mnuAyuda 
      Caption         =   "&?"
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "Temas de Ay&uda                F1"
         Index           =   10
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "-"
         Index           =   20
      End
      Begin VB.Menu mnuAyudaOpcion 
         Caption         =   "&Acerca de ..."
         Index           =   30
      End
   End
End
Attribute VB_Name = "FrmBusAlb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'************************************************************************
'* PROYECTO: FARMACIA                                                   *
'* NOMBRE: frmBusAlb (FR0538.FRM)                                       *
'* AUTOR: Aitor Vi�uela Garc�a                                          *
'* FECHA: Septiembre DE 1999                                            *
'* DESCRIPCION: Buscar Albaranes                                        *
'* ARGUMENTOS:  <NINGUNO>                                               *
'* ACTUALIZACIONES:                                                     *
'************************************************************************

Option Explicit

Dim WithEvents objWinInfo As clsCWWin
Attribute objWinInfo.VB_VarHelpID = -1
Dim WithEvents objSearch As clsCWSearch
Attribute objSearch.VB_VarHelpID = -1
Dim mblnCancelar As Boolean 'False si no ha habido problemas al salvar

Private Sub cmdSeleccionar_Click()
   gstrCodAlb = grdDBGrid1(3).Columns(3).Value
   Unload Me
End Sub

Private Sub cmdTraer_Click()
   Dim Q%
   Dim vntEstado As Variant
   
   cmdTraer.Enabled = False
   
   gintCodsAlbs = grdDBGrid1(0).SelBookmarks.Count
   If gintCodsAlbs = 0 Then
      If Len(Trim(txtText1(0).Text)) > 0 And IsNumeric(txtText1(2).Text) Then
         If txtText1(1).Text <> 1 Then '
'            gstrCodProv = txtText1(2).Text
'            -> ahora gstrCodProv se rellena en la pantalla llamante
            gintCodsAlbs = 1
            'ReDim gstrCodsAlbs(gintCodsAlbs)
            gstrCodsAlbs(0) = txtText1(0).Text
         Else
            MsgBox "El albar�n seleccionado ya esta facturado", vbInformation, "Aviso"
            gintCodsAlbs = 0
            '   ReDim gstrCodsAlbs(gintCodsAlbs)
            cmdTraer.Enabled = True
            Exit Sub
         End If
      Else
         MsgBox "Debe seleccionar al menos un albar�n que tenga proveedor", vbInformation, "Aviso"
         gintCodsAlbs = 0
         '      ReDim gstrCodsAlbs(gintCodsAlbs)
         cmdTraer.Enabled = True
         Exit Sub
      End If
   Else
      If IsNumeric(grdDBGrid1(0).Columns(4).CellValue(grdDBGrid1(0).SelBookmarks(0))) Then
         'ReDim gstrCodsAlbs(gintCodsAlbs)
'         gstrCodProv = grdDBGrid1(0).Columns(4).CellValue(grdDBGrid1(0).SelBookmarks(0))
'         ahora gstrCodProv se rellena en la pantalla llamante
         For Q% = 0 To gintCodsAlbs - 1
            gstrCodsAlbs(Q%) = grdDBGrid1(0).Columns(1).CellValue(grdDBGrid1(0).SelBookmarks(Q%))
            If gstrCodProv <> grdDBGrid1(0).Columns(4).CellValue(grdDBGrid1(0).SelBookmarks(Q%)) Then
               Call MsgBox("No se pueden facturar juntos albaranes de distinto proveedor", vbInformation, "Aviso")
               gintCodsAlbs = 0
               'ReDim gstrCodsAlbs(gintCodsAlbs)
               cmdTraer.Enabled = True
               Exit Sub
            End If
            vntEstado = grdDBGrid1(0).Columns(2).CellValue(grdDBGrid1(0).SelBookmarks(Q%))
            If IsNull(vntEstado) Then
               vntEstado = 0
            End If
            If vntEstado = 1 Then
               Call MsgBox("El albaran: - " & gstrCodsAlbs(Q%) & " - ya esta facturado", vbInformation, "Aviso")
               gintCodsAlbs = 0
               'ReDim gstrCodsAlbs(gintCodsAlbs)
               cmdTraer.Enabled = True
               Exit Sub
            End If
         Next Q%
      Else
         MsgBox "Todos los albaranes seleccionados han de ser de un proveedor", vbInformation, "Aviso"
      End If
   End If
   
   cmdTraer.Enabled = True
   
   Unload Me
End Sub

Private Sub Form_Activate()
  gintCodsAlbs = 0
  Call objWinInfo.DataRefresh
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del formulario
' -----------------------------------------------

Private Sub Form_Load()
  Dim objMasterInfo As New clsCWForm
  Dim objMultiInfo As New clsCWForm
  Dim objMultiInfo1 As New clsCWForm
  Dim objMultiInfo2 As New clsCWForm
  Dim strKey As String
  Dim strFec As String
  Dim rstFec As rdoResultset
   
  Set objWinInfo = New clsCWWin
  
  Call objWinInfo.WinCreateInfo(cwModeMultiLineEdit, _
                                Me, tlbToolbar1, stbStatusBar1, _
                                cwWithAll)
    
   With objMasterInfo
      .strName = "Albar�n"
      Set .objFormContainer = fraFrame1(0)
      Set .objFatherContainer = Nothing
      Set .tabMainTab = tabTab1(0)
      Set .grdGrid = grdDBGrid1(0)
    
      .strTable = "FRJ100"
      '.blnAskPrimary = False
      .intAllowance = cwAllowReadOnly
    
      Select Case gstrLlamador
      Case "FrmRedGesCom-Check1"
         cmdSeleccionar.Visible = True
      Case "FrmRedGesCom-cmdBuscar"
         cmdTraer.Visible = True
         If Len(Trim(gstrCodProv)) > 0 Then
            .strWhere = " FR79CODPROVEEDOR = '" & gstrCodProv & "' "
         End If
      Case "FrmRedGesCom-cmdVer"
         If Not IsNull(gstrCodAlb) Then
            If Len(Trim(gstrCodAlb)) > 0 Then
               .strWhere = " FRJ1CODALBARAN = '" & gstrCodAlb & "' "
            End If
         End If
      End Select
      
      Call .FormAddOrderField("FRJ1CODALBARAN", cwAscending)
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Albaranes")
      Call .FormAddFilterWhere(strKey, "FRJ1CODALBARAN", "C�digo Albar�n", cwString)
      Call .FormAddFilterWhere(strKey, "FRJ1FECHAALBAR", "Fecha Albar�n", cwDate)
      Call .FormAddFilterWhere(strKey, "FR79CODPROVEEDOR", "C�digo Proveedor", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FRJ1ESTADO", "Estado", cwNumeric)

   End With
  
   With objMultiInfo
      .strName = "Productos"
      Set .objFormContainer = fraFrame1(1)
      Set .objFatherContainer = fraFrame1(0)
      Set .tabMainTab = Nothing
      Set .grdGrid = grdDBGrid1(1)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
      .strTable = "FRJ300"
      '.strWhere = "FRJ3CANTPEDIDA > FRJ3CANTENTACU"
      .intAllowance = cwAllowReadOnly
              
      Call .FormAddOrderField("FR73CODPRODUCTO", cwAscending)
      Call .FormAddRelation("FRJ1CODALBARAN", txtText1(0))
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Productos")
      Call .FormAddFilterWhere(strKey, "FR73CODPRODUCTO", "C�digo Producto", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FRJ3CANTPEDIDA", "Cantidad Pedida", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FRJ3PRECIOUNIDAD", "Precio Unidad", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FRJ3IMPORLINEA", "Importe", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FRH8MONEDA", "Moneda", cwNumeric)
      Call .FormAddFilterWhere(strKey, "FRJ3CANTENTREG", "Cantidad Entregada", cwString)
      
   End With
  
   With objMultiInfo1
      .strName = "Pedidos"
      Set .objFormContainer = fraFrame1(2)
      Set .objFatherContainer = fraFrame1(0)
      Set .tabMainTab = Nothing
      Set .grdGrid = grdDBGrid1(2)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
      .strTable = "FRJ200"
      .intAllowance = cwAllowReadOnly
              
      Call .FormAddOrderField("FR62CODPEDCOMPRA", cwAscending)
      Call .FormAddRelation("FRJ1CODALBARAN", txtText1(0))
      
      strKey = .strDataBase & .strTable
      Call .FormCreateFilterWhere(strKey, "Pedidos")
      Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
   End With
  
   With objMultiInfo2
      .strName = "Facturas"
      Set .objFormContainer = fraFrame1(3)
      Set .objFatherContainer = fraFrame1(0)
      Set .tabMainTab = Nothing
      Set .grdGrid = grdDBGrid1(3)
      .intFormModel = cwWithGrid + cwWithoutTab + cwWithoutKeys
      .strTable = "FRJ400"
      .intAllowance = cwAllowReadOnly
              
      Call .FormAddOrderField("FR39CODFACTCOMPRA", cwAscending)
      Call .FormAddRelation("FRJ1CODALBARAN", txtText1(0))
      
      strKey = .strDataBase & .strTable
      '  Call .FormCreateFilterWhere(strKey, "Pedidos")
      '  Call .FormAddFilterWhere(strKey, "FR62CODPEDCOMPRA", "C�digo Pedido", cwNumeric)
   End With
  
   With objWinInfo
      Call .FormAddInfo(objMasterInfo, cwFormDetail)
      Call .FormAddInfo(objMultiInfo, cwFormMultiLine)
      Call .FormAddInfo(objMultiInfo1, cwFormMultiLine)
      Call .FormAddInfo(objMultiInfo2, cwFormMultiLine)
      
      Call .GridAddColumn(objMultiInfo, "C�d.Albar�n", "FRJ1CODALBARAN", cwString, 9)
      Call .GridAddColumn(objMultiInfo, "L�nea", "FRJ3NUMLINEA", cwNumeric, 9)
      Call .GridAddColumn(objMultiInfo, "C�d.Prod", "FR73CODPRODUCTO", cwNumeric, 9)
      Call .GridAddColumn(objMultiInfo, "Interno", "", cwString, 6)
      Call .GridAddColumn(objMultiInfo, "Seg", "", cwNumeric, 1)
      Call .GridAddColumn(objMultiInfo, "Producto", "", cwString, 50)
      Call .GridAddColumn(objMultiInfo, "F.F", "", cwString, 3)
      Call .GridAddColumn(objMultiInfo, "Dosis", "", cwDecimal, 9)
      Call .GridAddColumn(objMultiInfo, "U.M", "", cwString, 5)
      Call .GridAddColumn(objMultiInfo, "Vol.", "", cwDecimal, 6)
      Call .GridAddColumn(objMultiInfo, "Referencia", "", cwString, 15)
      Call .GridAddColumn(objMultiInfo, "Pte.Fact", "FRJ3CANTPTEFACT", cwDecimal, 12)
      Call .GridAddColumn(objMultiInfo, "Rdo.", "FRJ3CANTENTACU", cwDecimal, 10)
      Call .GridAddColumn(objMultiInfo, "PPU", "FRJ3PRECIOUNIDAD", cwDecimal, 9)
      Call .GridAddColumn(objMultiInfo, "%Dto", "FRJ3DTO", cwDecimal, 2)
      Call .GridAddColumn(objMultiInfo, "IVA", "FRJ3IVA", cwDecimal, 2)
      Call .GridAddColumn(objMultiInfo, "Importe L�nea", "FRJ3IMPORLINEA", cwDecimal, 9)
      Call .GridAddColumn(objMultiInfo, "Envase", "FRJ3TAMENVASE", cwDecimal, 10)
      Call .GridAddColumn(objMultiInfo, "Moneda", "FRH8MONEDA", cwString, 5)
      Call .GridAddColumn(objMultiInfo, "UdesBonif", "FRJ3PTEBONIF", cwDecimal, 10)
      'Call .GridAddColumn(objMultiInfo, "Recibido", "FRJ3CANTENTREG", cwDecimal, 10)
      Call .GridAddColumn(objMultiInfo, "Por Bonif", "FRJ3BONIF", cwDecimal, 10)
      Call .GridAddColumn(objMultiInfo, "Pedido", "FRJ3CANTPEDIDA", cwDecimal, 10)
      
      
      Call .GridAddColumn(objMultiInfo1, "Albar�n", "FRJ1CODALBARAN", cwString, 9)
      Call .GridAddColumn(objMultiInfo1, "C�d.Pedido", "FR62CODPEDCOMPRA", cwNumeric, 9)
      
      Call .GridAddColumn(objMultiInfo2, "Albar�n", "FRJ1CODALBARAN", cwString, 9)
      Call .GridAddColumn(objMultiInfo2, "C�d.Factura", "FR39CODFACTCOMPRA", cwString, 25)
      
      Call .FormCreateInfo(objMasterInfo)
      
      Call .FormChangeColor(objMultiInfo)
      Call .FormChangeColor(objMultiInfo1)
      Call .FormChangeColor(objMultiInfo2)
      
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(6), "FR73CODINTFAR")
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(7), "FR73CODINTFARSEG")
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(8), "FR73DESPRODUCTOPROV")
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(9), "FRH7CODFORMFAR")
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(10), "FR73DOSIS")
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(11), "FR93CODUNIMEDIDA")
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(12), "FR73VOLUMEN")
      Call .CtrlCreateLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), "FR73CODPRODUCTO", "SELECT * FROM FR7300 WHERE FR73CODPRODUCTO = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(grdDBGrid1(1).Columns(5)), grdDBGrid1(1).Columns(13), "FR73REFERENCIA")
      Call .CtrlCreateLinked(.CtrlGetInfo(txtText1(2)), "FR79CODPROVEEDOR", "SELECT * FROM FR7900 WHERE FR79CODPROVEEDOR = ?")
      Call .CtrlAddLinked(.CtrlGetInfo(txtText1(2)), txtText1(3), "FR79PROVEEDOR")
      
      'albaranes
      .CtrlGetInfo(txtText1(0)).blnInFind = True
      .CtrlGetInfo(txtText1(1)).blnInFind = True
      .CtrlGetInfo(txtText1(2)).blnInFind = True
      .CtrlGetInfo(dtcDateCombo1(0)).blnInFind = True
      
      'productos
      .CtrlGetInfo(grdDBGrid1(1).Columns("C�d.Prod")).blnInFind = True
      '.CtrlGetInfo(grdDBGrid1(1).Columns("Pedido")).blnInFind = True
      .CtrlGetInfo(grdDBGrid1(1).Columns("PPU")).blnInFind = True
      .CtrlGetInfo(grdDBGrid1(1).Columns("Importe L�nea")).blnInFind = True
      .CtrlGetInfo(grdDBGrid1(1).Columns("Moneda")).blnInFind = True
      .CtrlGetInfo(grdDBGrid1(1).Columns("UdesBonif")).blnInFind = True
      
      .CtrlGetInfo(txtText1(2)).blnForeign = True
      .CtrlGetInfo(grdDBGrid1(1).Columns("C�d.Prod")).blnForeign = True  'c�d.producto
      .CtrlGetInfo(grdDBGrid1(1).Columns("Moneda")).blnForeign = True  'moneda
      '.CtrlGetInfo(grdDBGrid1(1).Columns("Pedido")).blnReadOnly = True 'Cant. pedida
      .CtrlGetInfo(grdDBGrid1(1).Columns("UdesBonif")).blnReadOnly = True 'Cant. pte de bonificar
      .CtrlGetInfo(grdDBGrid1(1).Columns("Rdo.")).blnReadOnly = True 'cant acumulada recibida
      .CtrlGetInfo(grdDBGrid1(1).Columns("Envase")).blnReadOnly = True 'Tama�o del envase
      .CtrlGetInfo(grdDBGrid1(1).Columns("Pte.Fact")).blnReadOnly = True 'Pendiente de Facturar
      
      Call .WinRegister
      Call .WinStabilize
   End With
   
   grdDBGrid1(1).Columns("C�d.Prod").Visible = False
   grdDBGrid1(1).Columns("C�d.Albar�n").Visible = False
   grdDBGrid1(1).Columns("L�nea").Visible = False
   'grdDBGrid1(1).Columns("Vol.").Visible = False
   'grdDBGrid1(1).Columns("PPU").Visible = False
   'grdDBGrid1(1).Columns("Importe L�nea").Visible = False
   grdDBGrid1(1).Columns("Moneda").Visible = False
   grdDBGrid1(1).Columns("Pedido").Visible = False

   'grdDBGrid1(1).Columns("C�d.Prod").Width = 900     'c�d.prod
   grdDBGrid1(1).Columns("Interno").Width = 700     'c�d.interno
   grdDBGrid1(1).Columns("Seg").Width = 450      'c�d.seg
   grdDBGrid1(1).Columns("Producto").Width = 2500     'descripci�n
   grdDBGrid1(1).Columns("F.F").Width = 450      'F.F
   grdDBGrid1(1).Columns("Dosis").Width = 600    'Dosis
   grdDBGrid1(1).Columns("U.M").Width = 500     'U.M
   grdDBGrid1(1).Columns("Referencia").Width = 1000    'Referencia
   'grdDBGrid1(1).Columns("Pedido").Width = 900   'Pedido
   grdDBGrid1(1).Columns("UdesBonif").Width = 900  'Udes a bonificar
   grdDBGrid1(1).Columns("Rdo.").Width = 700  'Recibido acumulado
   grdDBGrid1(1).Columns("Por Bonif").Width = 900  'Por Bonif.
   grdDBGrid1(1).Columns("Rdo.").Width = 700   'Acumulado
   grdDBGrid1(1).Columns("Pte.Fact").Width = 700   'Pte de facturar
   grdDBGrid1(1).Columns("IVA").Width = 450   'IVA
   grdDBGrid1(1).Columns("%Dto").Width = 550   '% Dto
   grdDBGrid1(1).Columns("Vol.").Width = 600   'Volumen
   grdDBGrid1(1).Columns("PPU").Width = 1000   'Precio uNIDAD
   grdDBGrid1(2).Columns(3).Visible = False
   
   grdDBGrid1(3).Columns(3).Visible = False
  
End Sub

Private Sub Form_KeyPress(intKeyAscii As Integer)
  Call objWinInfo.WinProcess(cwProcessAsciiKeys, intKeyAscii, 0)
End Sub

Private Sub Form_KeyDown(intKeyCode As Integer, intShift As Integer)
  Call objWinInfo.WinProcess(cwProcessKeys, intKeyCode, intShift)
End Sub

Private Sub Form_QueryUnload(intCancel As Integer, _
                             intUnloadMode As Integer)
  intCancel = objWinInfo.WinExit
End Sub

Private Sub Form_Unload(intCancel As Integer)
  Call objWinInfo.WinDeRegister
  Call objWinInfo.WinRemoveInfo
End Sub

Private Sub grdDBGrid1_RowLoaded(Index As Integer, ByVal Bookmark As Variant)
   Dim rsta As rdoResultset
   Dim stra As String
   Dim qryFR62 As rdoQuery
   
   'se refrescan los option que indican el estado del pedido
   If Index = 2 Then
      optparcial.Value = False
      optcompleto.Value = False
      If grdDBGrid1(2).Rows > 0 Then
         If IsNumeric(grdDBGrid1(2).Columns("C�d.Pedido").Value) Then
            stra = "SELECT * FROM FR6200 WHERE FR62CODPEDCOMPRA= ? "
            Set qryFR62 = objApp.rdoConnect.CreateQuery("", stra)
            qryFR62(0) = grdDBGrid1(2).Columns("C�d.Pedido").Value
            Set rsta = qryFR62.OpenResultset()
            If Not IsNull(rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value) Then
               If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 4 Then
                  optparcial.Value = True
               End If
               If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 5 Then
                  optcompleto.Value = True
               End If
            End If
            qryFR62.Close
            Set qryFR62 = Nothing
            Set rsta = Nothing
         End If
      End If
   End If
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de estado
' -----------------------------------------------
Private Sub stbStatusbar1_PanelDblClick(ByVal panPanel As Panel)
  Call objWinInfo.WinProcess(cwProcessStatusBar, panPanel.Index, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de la barra de botones
' -----------------------------------------------
Private Sub tlbToolbar1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
   If Button = 2 And Shift = 1 Then
      Call MsgBox("Versi�n:" & gstrversion, vbInformation, "N�mero de Versi�n")
   End If
End Sub

Private Sub tlbtoolbar1_ButtonClick(ByVal btnButton As Button)
   Call objWinInfo.WinProcess(cwProcessToolBar, btnButton.Index, 0)
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los menues
' -----------------------------------------------
Private Sub mnuDatosOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessData, intIndex, 0)
End Sub

Private Sub mnuEdicionOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessEdit, intIndex, 0)
End Sub

Private Sub mnuFiltroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessFilter, intIndex, 0)
End Sub

Private Sub mnuRegistroOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessRegister, intIndex, 0)
End Sub

Private Sub mnuOpcionesOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessOptions, intIndex, 0)
End Sub

Private Sub mnuAyudaOpcion_Click(intIndex As Integer)
  Call objWinInfo.WinProcess(cwProcessHelp, intIndex, 0)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del grid
' -----------------------------------------------
Private Sub grdDBGrid1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub grdDBGrid1_DblClick(intIndex As Integer)
   Call objWinInfo.GridDblClick
End Sub

Private Sub grdDBGrid1_RowColChange(intIndex As Integer, _
                                    ByVal vntLastRow As Variant, _
                                    ByVal intLastCol As Integer)
   Dim rsta As rdoResultset
   Dim stra As String
   Dim qryFR62 As rdoQuery

   Call objWinInfo.GridChangeRowCol(vntLastRow, intLastCol)
    
   If intIndex = 2 Then
      optparcial.Value = False
      optcompleto.Value = False
      If grdDBGrid1(2).Rows > 0 Then
         If IsNumeric(grdDBGrid1(2).Columns("C�d.Pedido").Value) Then
            stra = "SELECT * FROM FR6200 WHERE FR62CODPEDCOMPRA=?"
            Set qryFR62 = objApp.rdoConnect.CreateQuery("", stra)
            qryFR62(0) = grdDBGrid1(2).Columns("C�d.Pedido").Value
            Set rsta = qryFR62.OpenResultset()
            If Not IsNull(rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value) Then
               If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 4 Then
                  optparcial.Value = True
               End If
               If rsta.rdoColumns("FR95CODESTPEDCOMPRA").Value = 5 Then
                  optcompleto.Value = True
               End If
            End If
            qryFR62.Close
            Set qryFR62 = Nothing
            Set rsta = Nothing
         End If
      End If
   End If

End Sub

Private Sub grdDBGrid1_Change(intIndex As Integer)
    Call objWinInfo.CtrlDataChange
End Sub


' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del frame
' -----------------------------------------------
Private Sub fraFrame1_Click(intIndex As Integer)
  Call objWinInfo.FormChangeActive(fraFrame1(intIndex), False, True)
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Combo Box
' -----------------------------------------------
Private Sub cboCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboCombo1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub cboCombo1_Change(Index As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del Date Combo
' -----------------------------------------------
Private Sub dtcDateCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub dtcDateCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub dtcDateCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

Private Sub dtcDateCombo1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del CheckBox
' -----------------------------------------------
Private Sub chkCheck1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub chkCheck1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub chkCheck1_Click(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos del DBCombo
' -----------------------------------------------
Private Sub cboDBCombo1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub cboDBCombo1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub cboDBCombo1_CloseUp(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub

' -----------------------------------------------
' NUEVA SECCI�N
'
' eventos de los Text Box
' -----------------------------------------------
Private Sub txtText1_GotFocus(intIndex As Integer)
  Call objWinInfo.CtrlGotFocus
End Sub

Private Sub txtText1_LostFocus(intIndex As Integer)
  Call objWinInfo.CtrlLostFocus
End Sub

Private Sub txtText1_Change(intIndex As Integer)
  Call objWinInfo.CtrlDataChange
End Sub
